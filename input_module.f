
!BOP
!!MODULE: input
!!INTERFACE:
      module input
!!DESCRIPTION:
! this module provides routines to initialize run-input {\sc mecca} structure
! (type IniFile)
!

!!USES:
      use mecca_constants
      use mecca_types
      use raymethod, only : sDosInpParams,sBsfInpParams,sFsInpParams
      use raymethod, only : gDosBsfInput,gFsInput

!!DO_NOT_PRINT
       implicit none
       private
!!DO_NOT_PRINT

!!PUBLIC MEMBER FUNCTIONS:
       public :: initInput,readInput,saveInput,read2intFile
       public :: fnum2nunit,defineKmesh,setGenName
!!PRIVATE MEMBER FUNCTIONS:
! setIniFile, getGenName, initElement, read_new_sections, readFailed,
! nunit2num, initIOfiles, printline
!
!!PUBLIC DATA MEMBERS:
       public :: def_tag,def_name,max_nonrel_z
!!REVISION HISTORY:
! Initial Version - A.S. - 2013
!!REMARKS:
!  length of lines in input files is limited by 80 symbols
!EOP
!
!BOC
       integer, save :: num2nunit(1:ipunit) = -1
       integer, parameter :: dflt_nonrel = 23     ! deafaut non-relat
       integer, parameter :: dflt_rel    =  1     ! default rel.parameter (scalar-relat.)
       integer, parameter :: max_nonrel_z = 0     ! Z=18 (Ar)

!       real(8), parameter :: dflt_spltscale = one   ! see mecca_constants

       character(1), parameter :: def_tag='#'          ! default tag to mark beginning of a new section in input file
       character(4), parameter :: def_name='.ini'      ! default extension for input file created by the code
       character(7), parameter :: def_genname='unknown'
!
       character(3), parameter :: xc_tag=def_tag//'XC'
       character(11), parameter :: xpot_corr_kword='X-pot-corr:'
       character(13), parameter :: old_magn_tag=def_tag//'extMagnField'
       character(13), parameter :: magnet_tag=def_tag//'extEXptnl'
       character(4), parameter :: dosplot_tag=def_tag//'dos'
       character(4), parameter :: bsfplot_tag=def_tag//'bsf'
       character(4), parameter :: fsplot_tag=def_tag//'fer'
       character(*), parameter :: so_power_tag = def_tag//'so_power' ! spin-orbit switch
       character(*), parameter :: xrstep_tag = def_tag//'logR_mesh_step'
       character(*), parameter :: eftol_tag  = def_tag//'ef_tol'
       character(*), parameter :: etol_tag   = def_tag//'E_tol'
       character(*), parameter :: tctol_tag  = def_tag//'tc_tol'
       character(*), parameter :: qtol_tag   = def_tag//'q_tol'
       character(*), parameter :: zpten_tag  = def_tag//'zero-point-E'
       character(*), parameter :: nuclmod_tag =def_tag//'point-nucl'

       interface
        subroutine true_name(line,symb,low)
        character(*), intent(inout) :: line
        character(1), intent(in), optional :: symb
        logical, intent(in), optional :: low
        character(1), external :: toLower
        end subroutine
       end interface

!EOC
       CONTAINS

!c*******************************************************************

!BOP
!!IROUTINE: setIniFile
!!INTERFACE:
       subroutine setIniFile(inFile)
       implicit none
!!DESCRIPTION:
!  inFile structure initialization with default parameters values
!
!!ARGUMENTS:
!       type (IniFile), target, optional :: inFile_in
!       type (IniFile), pointer :: inFile
       type (IniFile), intent(out) :: inFile
!!REVISION HISTORY:
! Initial Version - A.S. - 2013
!EOP
!
!BOC
       integer :: st_env

!       if ( present(inFile_in) ) then
!           inFile => inFile_in
!       else
!           inFile => inFile0
!       end if

       inFile%name = def_name
       call setGenName(trim(def_genname),inFile,1)
       inFile%istop = 'return'
       inFile%task = 'serial'     ! executable name, only for parallel jobs
       inFile%nproc = 1
       inFile%ntask = 0
       inFile%icryst = 0    ! structure is desribed in a separate file
       inFile%imethod = 0   ! k-space (see DefMethod for other options))
       inFile%sprstech = 0
       inFile%iprint = -1
!       inFile%iprint = 0
       inFile%lmax = 3

       inFile%mtasa = 2     ! ASA (0: MT, 2: ASA/VP+rho0_PBC)
       call gEnvVar('ENV_M2013',.false.,st_env)
       if ( st_env>=0 ) inFile%mtasa = st_env

!       inFile%intgrsch = 0  ! "classical", ASA/MT
       inFile%intgrsch = 1  !  ASA-integrals
!       inFile%intgrsch = 2  ! VP-integrals
       call gEnvVar('ENV_VAR_MTZ',.false.,st_env)
       if ( st_env>=0 ) inFile%intgrsch = st_env

       inFile%nspin = 1     ! non-magn (2 - ferro, 3 - dlm)

       inFile%nrel = dflt_rel

       inFile%iXC = XC_LDA_VOSKO

       inFile%Rrsp = 0.d0   !  the RS translation vectors maximum
       inFile%Rksp = 4.d0  !  the KS translation vectors maximum
       inFile%Rclstr = 0.d0 !  the RS cluster radius
       inFile%enmax = 0.7d0 !  Ewald parameter, best value is about (efermi+delta)*(alat/2/pi)**2
       inFile%eneta = 2.5d0 !  Ewald parameter, (between 0 and 2.8 -- limitation from d003-routine)
       inFile%nmesh = 1
       inFile%nKxyz(1:3,1:ipmesh) = 0
       inFile%nKxyz(1:3,1) = 8
       inFile%enKlim(1:2,1:ipmesh-1) = 0.d0
!       inFile%systemID = TRIM(ADJUSTL(inFile%genName))
       inFile%alat = 0.d0
       inFile%ba = 0.d0
       inFile%ca = 0.d0
       inFile%nelements = 0
!      inFile%elements(1:ipelmnts)   ! type (Element)
       inFile%ncpa = 1
       inFile%nsubl = 0
!      inFile%sublat(1:ipsublat)  ! type ( Sublattice )
       inFile%igrid = 2
       inFile%ebot = 0.d0
       inFile%etop = 0.0d0
       inFile%eibot = 0.003d0
       inFile%npts = 20
       inFile%eitop = 0.003d0
       inFile%npar = 0

       inFile%nscf = -1
       inFile%alphmix = 0.04d0/inFile%nspin
       inFile%betamix = 0.8d0
       inFile%imixtype = 1             ! 0-charge, 1 - potential
       inFile%mixscheme = 1            ! Broyden mixing (0 - simple, >1 -- Broyden+simple)
       return
!EOC
       end subroutine setIniFile

!c*******************************************************************

!BOP
!!IROUTINE: initInput
!!INTERFACE:
       subroutine initInput(strname,strtype,alat,boa,coa,inFile)
       implicit none
!!DESCRIPTION:
! inFile structure initialization based on input information
! {\bv
!      strname - chemical description
!      strtype: fcc,bcc,sc or from the file <strtype>
!  \ev}
!
!!ARGUMENTS:
!       type (IniFile), target, optional :: inFile_in
!       type (IniFile), pointer :: inFile
       type (IniFile), intent(out) :: inFile
       character*(*), intent(in) :: strname
       character*(*), intent(in) :: strtype
       real(8), intent(in) :: alat,boa,coa
!!REVISION HISTORY:
! Initial Version - A.S. - 2013
!EOP
!
!BOC
       interface
        subroutine iostr_subl(io,iflag,nsubl)
        integer, intent(in) :: io
        integer, intent(in) :: iflag
        integer, allocatable, intent(out) :: nsubl(:)
        end subroutine iostr_subl
       end interface

       integer i,ic,ie, j,jn, k,k1, n, z_ik
       real*8 ebot
       real*8 a0,an0,v0,ba,ca,unitV
       real*8 reducK,ak0,ql(3)
!       integer :: st_env
       integer :: nbasis
       integer, allocatable :: nsubl(:)
       type ( Sublattice ), allocatable :: compound(:)

       real*8 dfltBottom
       external dfltBottom
       integer dfltMixType
       external dfltMixType
       real*8 dfltAlphmix
       external dfltAlphmix
       real*8 dfltRWS
       external dfltRWS
       real*8 unitVolm
       external unitVolm

       real(8), parameter :: uV_fcc=0.25d0,uV_bcc=0.5d0
       real(8), parameter :: third = one/three
!       real(8),  parameter :: pi4d3 = 4.d0*pi/3.d0

!       if ( present(inFile_in) ) then
!         inFile => inFile_in
!         call setIniFile(inFile_in)
!       else
!         inFile => inFile0
!         call setIniFile()
!       end if
       call setIniFile(inFile)
       if ( allocated(inFile%sublat) ) deallocate(inFile%sublat)
       if ( allocated(nsubl) ) deallocate(nsubl)

       nbasis = 0
       ba = one
       ca = one
       ql = one

       inFile%io(nu_xyz)%name = 'str.xyz'
       select case ( trim(strtype) )
       case ( 'fcc' )
        inFile%icryst = 1
        unitV = uV_fcc ! one/4
        nbasis = 1
        allocate( inFile%sublat(nbasis) )
        inFile%sublat(1)%ns = 1
       case ( 'bcc' )
        inFile%icryst = 2
        unitV = uV_bcc ! one/2
        nbasis = 1
        allocate( inFile%sublat(nbasis) )
        inFile%sublat(1)%ns = 1
       case ( 'sc' )
        inFile%icryst = 3
        unitV = one
        nbasis = 1
        allocate( inFile%sublat(nbasis) )
        inFile%sublat(1)%ns = 1
       case ( 'b1' )
        inFile%icryst = 7
        unitV = one
        nbasis = 2
        allocate( inFile%sublat(nbasis) )
        inFile%sublat(1:nbasis)%ns = 1
       case ( 'b2' )
        inFile%icryst = 4
        unitV = one
        nbasis = 2
        allocate( inFile%sublat(nbasis) )
        inFile%sublat(1:nbasis)%ns = 1
       case default
        inFile%icryst = 0
        inFile%io(nu_xyz)%name = TRIM(ADJUSTL(strtype))
        open(inFile%io(nu_xyz)%nunit,file=inFile%io(nu_xyz)%name,err=10)
         call iostr_subl(inFile%io(nu_xyz)%nunit,1,nsubl)
        close(inFile%io(nu_xyz)%nunit)
        if ( allocated(nsubl) ) then
         nbasis = nsubl(0)
        end if
        if ( nbasis>0 ) then
         allocate( inFile%sublat(nbasis) )
        end if
        ba = zero
        ca = zero
        if ( boa.ne.zero ) ba = boa
        if ( coa.ne.zero ) ca = coa
        unitV = unitVolm(inFile%io(nu_xyz)%name,ba,ca,ql)
10      if ( unitV .le. zero ) then
         write(0,*) ' ERROR: structure description file ',              &
     &               trim(inFile%io(nu_xyz)%name)                       &
     &,              ' is either corrupted or not found.'
         stop
        end if
       end select
       if ( .not.allocated(nsubl) ) then
        allocate(nsubl(0:size(inFile%sublat)))
        nsubl(1:) = inFile%sublat(1:)%ns
        nsubl(0) = sum(nsubl(1:))
       end if

       call stringToCmpnd(strname,compound,nbasis)
       if (nbasis .le. 0 .or. .not.allocated(compound)) then
         write(*,*) ' initInput :: ERROR ::',                           &
     &              ' incorrect syntax of the chemical specification'
         stop
       else if ( size(compound)<nbasis ) then
         write(*,*) ' nbasis = ',nbasis
         write(*,*) ' strname =<',strname,'>'
         write(*,*) ' initInput :: ERROR ::',                           &
     &   ' chemical specification and number of atoms are incompatible'
!       else if (inFile%nsubl .gt. ipsublat) then
!         write(*,*) ' initInput :: ERROR ::'
!         write(*,*) ' nsubl = ',inFile%nsubl,' > ipsublat = ',ipsublat
!         write(*,*) 'Please increase ipsublat in mecca_constants module'
!         inFile%nsubl = -inFile%nsubl
!         return
       end if

!       nbasis = size(inFile%sublat)
       n = 0
       do i=1,nbasis
        do k=1,compound(i)%ncomp
         z_ik = compound(i)%compon(k)%zID
         jn = 0
         do j=1,i-1
          if ( jn .ne. 0 ) exit
          do k1=1,compound(j)%ncomp
           if ( compound(j)%compon(k1)%zID == z_ik ) then
            jn = 1
            exit
           end if
          end do
         end do
         if ( jn==0 ) n = n+1
        end do
       end do

       inFile%nelements = n
       if ( allocated(inFile%elements) ) deallocate(inFile%elements)
       allocate(inFile%elements(1:inFile%nelements))
       n = 0
       do i=1,nbasis
        do k=1,compound(i)%ncomp
         z_ik = compound(i)%compon(k)%zID
         jn = 0
         do j=1,i-1
          if ( jn .ne. 0 ) exit
          do k1=1,compound(j)%ncomp
           if ( compound(j)%compon(k1)%zID == z_ik ) then
            jn = 1
            exit
           end if
          end do
         end do
         if ( jn==0 ) then
          n = n+1
          call initElement(compound(i)%compon(k)%zID,inFile%elements(n))
          if ( compound(i)%compon(k)%zID > max_nonrel_z ) then
           if ( inFile%nrel == dflt_nonrel ) inFile%nrel = dflt_rel
          end if
         end if
        end do
       end do

       k = sum(nsubl(1:nsubl(0)))
       k1 = sum(compound(1:nbasis)%ns)
       if ( k>=k1 ) then
        jn = mod(k,k1)
       else
        jn = mod(k1,k)
       end if
       if ( jn.ne.0 ) then
        write(6,*) ' Number of sites in the structure is ',k
        write(6,*) ' Number of atoms in the chemical description is ',k1
        call fstop('Chemical formula is incompatible with structure')
       end if
       if ( k>k1 ) then
        compound(1:nbasis)%ns = compound(1:nbasis)%ns*(k/k1)
       else if (k<k1) then
        compound(1:nbasis)%ns = compound(1:nbasis)%ns/(k1/k)
       end if

       inFile%nsubl = nsubl(0)
       k = 1
       do i=1,nbasis
        jn = 0
        k1 = 0
        do j=k,inFile%nsubl
         jn = jn + nsubl(j)
         if ( jn == compound(i)%ns ) then
           do k1=k,j
            inFile%sublat(k1) = compound(i)
            inFile%sublat(k1)%ns = nsubl(k1)
            if ( j>k ) then
             do ic=1,inFile%sublat(k1)%ncomp
              do ie=1,inFile%nelements
               if ( trim(adjustl(inFile%sublat(k1)%compon(ic)%name)) == &
     &              trim(adjustl(inFile%elements(ie)%name)) ) then
                write(inFile%sublat(k1)%compon(ic)%name,'(a,i0.0)')     &
     &                   trim(adjustl(inFile%elements(ie)%symb)),k1
                exit
               end if
              end do
             end do
            end if
           end do
           k1 = j+1
           exit
         end if
        end do
        if ( jn.ne.compound(i)%ns .or. k1==0 ) then
         write(6,*) ' i=',i,' nbasis=',nbasis
         write(6,*) ' k1=',k1,' k=',k
         write(6,*) ' jn=',jn,' compound(i)%ns=',compound(i)%ns
       call fstop('Chemical description is incompatible with structure')
        end if
        k=k1
       end do

       forall (i=1:inFile%nsubl) inFile%sublat(i)%indx = i

!       do i=1,inFile%nsubl
!        do k=1,inFile%sublat(i)%ncomp
!         jn = 0
!         do j=1,n
!          if ( inFile%sublat(i)%compon(k)%zID .eq.                      &
!     &                               inFile%elements(j)%ztot ) then
!           inFile%sublat(i)%compon(k)%zcor =                            &
!     &      inFile%sublat(i)%compon(k)%zID-inFile%elements(j)%zval
!           jn = j
!           exit
!          end if
!         end do
!         if ( jn==0 ) then
!          write(6,*) ' Element with Z=',inFile%sublat(i)%compon(k)%zID, &
!     &               ' is not found'
!          write(6,*) ' sublat # ',i, ' component # ',k
!          call p_fstop(' incorrect compound description')
!         end if
!        end do
!       end do

       ebot = zero
!       do i=1,n
!        ebot = min(ebot,dfltBottom(dble(inFile%elements(i)%ztot)))
!       end do

       select case ( maxval(inFile%elements(1:n)%ztot) )
       case (0)         ! empty
        inFile%lmax = 1
       case (1:2)       ! I
        inFile%lmax = 2
       case (3:10)      ! II
        inFile%lmax = 2
       case (11:18)     ! III
        inFile%lmax = 3
       case (19:36)     ! IV
        inFile%lmax = 3
       case (37:54)     ! V
        inFile%lmax = 3
       case (55:57)     ! VI
        inFile%lmax = 3
       case (58:)       ! VI and above
        inFile%lmax = 4
       case default
        inFile%lmax = 3
       end select
!
       inFile%ba = ba
       inFile%ca = ca
!
       if ( alat==zero ) then
        v0 = zero
        do i=1,inFile%nsubl
         do k=1,inFile%sublat(i)%ncomp
          a0 = dfltRWS(dble(inFile%sublat(i)%compon(k)%zID))
          if ( inFile%sublat(i)%compon(k)%zID .gt. 0 ) then
           v0 = v0 + inFile%sublat(i)%ns*                               &
     &              inFile%sublat(i)%compon(k)%concentr*a0**3
          end if
         end do
        end do
        v0 = (four*pi*third)*v0
!c         now v0 = sum<rws-volume>
        inFile%alat = (v0/unitV)**third
       else
        inFile%alat = alat
       end if

       an0 = zero
       do i=1,inFile%nsubl
        do k=1,inFile%sublat(i)%ncomp
          if ( inFile%sublat(i)%compon(k)%zID .gt. 0 ) then
           an0 = an0 + inFile%sublat(i)%ns*                             &
     &                inFile%sublat(i)%compon(k)%concentr
          end if
        end do
       end do

       reducK = (one/an0)**third
       ql = ql / maxval(ql)

       inFile%nmesh = 2
       inFile%nKxyz(1:3,1:ipmesh) = 0
       ak0 = 18
!
       ak0 = ceiling(reducK*ak0)
       k = max(2,((nint(ak0*ql(1))+1)/2)*2)
       inFile%nKxyz(1,1) = k
       k = max(2,((nint(ak0*ql(2))+1)/2)*2)
       inFile%nKxyz(2,1) = k
       k = max(2,((nint(ak0*ql(3))+1)/2)*2)
       inFile%nKxyz(3,1) = k
       ak0 = 12
!
       ak0 = ceiling(reducK*ak0)
       k = max(2,((nint(ak0*ql(1))+1)/2)*2)
       inFile%nKxyz(1,2) = k
       k = max(2,((nint(ak0*ql(2))+1)/2)*2)
       inFile%nKxyz(2,2) = k
       k = max(2,((nint(ak0*ql(3))+1)/2)*2)
       inFile%nKxyz(3,2) = k
       inFile%enKlim(1:2,1:ipmesh-1) = zero
       if ( size(inFile%enKlim,2)>0 ) then
        inFile%enKlim(1,1) = zero
        inFile%enKlim(2,1) = 0.25d0
       end if

       inFile%imixtype = 1             ! 0-charge, 1 - potential
!       if ( inFile%nsubl.eq.1 .or. inFile%nelements.eq.1 ) then
!        inFile%imixtype = 0             ! 0-charge, 1 - potential
!        do i=1,inFile%nelements
!         if ( dfltMixType(dble(inFile%elements(i)%ztot)) .ne. 0) then
!          inFile%imixtype = dfltMixType(dble(inFile%elements(i)%ztot))
!          exit
!         end if
!        end do
!       end if

       inFile%mixscheme = 1            ! Broyden mixing (0 - simple, >1 -- Broyden+simple)

       if ( inFile%nscf <= 0 ) then
        inFile%nscf = maxval(inFile%elements(1:inFile%nelements)%ztot)  &
     &                         + 10*(1+inFile%nelements)
!        inFile%nscf = max(25,inFile%nscf)
        inFile%nscf = max(100,inFile%nscf)
        if ( count(inFile%elements(1:inFile%nelements)%ztot==0)>0 )     &
     &    inFile%nscf = inFile%nscf + 20
        if ( inFile%nspin>1 ) inFile%nscf = inFile%nscf + 40
        if ( inFile%ncpa>0 ) inFile%nscf = inFile%nscf + 40
       end if

!c       inFile%alphmix = dfltAlphmix(inFile%imixtype,-1)

       do i=1,inFile%nelements
         if ( i == 1 ) then
          inFile%alphmix = dfltAlphmix(inFile%imixtype,                 &
     &                                 inFile%elements(i)%ztot)
         else
          inFile%alphmix = min(inFile%alphmix,                          &
     &      dfltAlphmix(inFile%imixtype,inFile%elements(i)%ztot))
         end if
       end do
       if ( inFile%nspin .ne. 1 ) then
        inFile%alphmix = 0.7d0*inFile%alphmix
       end if

       if ( index(trim(inFile%genName),trim(def_genname))>0 ) then
        call getGenName(inFile)
       end if
       call setGenName(adjustl(inFile%genName),inFile,1)

!       inFile%systemID = TRIM(ADJUSTL(inFile%genName))

       if ( allocated(nsubl) ) deallocate(nsubl)
       return
!EOC
       end subroutine initInput

!c*******************************************************************

!BOP
!!IROUTINE: setGenName
!!INTERFACE:
       subroutine setGenName(runid,inFile,iflag)
       implicit none
!!DESCRIPTION:
! set run-specific name inFile%genName; if optional argument iflag presents,
! I/O files with changed names are also initiated.
!
!!ARGUMENTS:
       character(*), intent(in) :: runid
       type (IniFile) :: inFile
       integer, intent(in), optional :: iflag
!!PARAMETERS:
!!REVISION HISTORY:
! Initial Version - A.S. - 2019
!EOP
!
!BOC
       inFile%genName = trim(adjustl(runid))
       inFile%name = trim(adjustl(runid))//def_name
       if ( present(iflag) ) then
        call initIOfiles(inFile%genName,inFile%numIOfiles,inFile%io)
       end if

       return
!EOC
       end subroutine setGenName

!BOP
!!IROUTINE: getGenName
!!INTERFACE:
       subroutine getGenName(inFile)
       implicit none
!!DESCRIPTION:
! generation of run-specific name inFile%genName
!
!!ARGUMENTS:
!       type (IniFile) :: inFile
       type (IniFile) :: inFile
!!PARAMETERS:
!!REVISION HISTORY:
! Initial Version - A.S. - 2013
!EOP
!
!BOC
       integer i,k,j
       character(6) :: chA,ch6
       character(4) :: ch4
       character(3), parameter :: eon=':::'
       integer :: maxlen,len0

       inFile%genName = ' '
       maxlen = len(inFile%genName)
       len0 = len(eon) + len(ch6) + len_trim(inFile%genName)
       if ( inFile%icryst .ne. 0 ) then
        len0 = len0 + 1 + len(chA)
       end if
       do i=1,inFile%nsubl
        if ( len0>maxlen ) then
         len0 = 2*maxlen
         exit
        end if
        do k=1,inFile%sublat(i)%ncomp
         if ( len0>maxlen ) then
          len0 = 2*maxlen
          exit
         end if
         do j=1,inFile%nelements
          if ( inFile%sublat(i)%compon(k)%zID .eq.                      &
     &          inFile%elements(j)%ztot ) then
           if ( len0 + len_trim(inFile%genName) +                       &
     &        len_trim(inFile%elements(j)%symb) <= maxlen  ) then
             inFile%genName = TRIM(inFile%genName)                      &
     &                      //trim(inFile%elements(j)%symb)
             if ( abs(                                                  &
     &         nint(inFile%sublat(i)%compon(k)%concentr) -              &
     &              inFile%sublat(i)%compon(k)%concentr                 &
     &                ) .lt. 0.01d0 ) then
              if ( inFile%sublat(i)%ns > 1 .or.                         &
     &             inFile%sublat(i)%compon(k)%zID == 0 ) then
               write(ch6,'(i6)') inFile%sublat(i)%ns
               inFile%genName = TRIM(inFile%genName)//                  &
     &                          TRIM(ADJUSTL(ch6))
              end if
             else
              write(ch4,'(f4.2)')                                       &
     &                  inFile%sublat(i)%compon(k)%concentr
              inFile%genName = TRIM(inFile%genName)//TRIM(ch4)
             end if
            len0 = len_trim(inFile%genName)
           else
            len0 = 2*maxlen
           end if
           exit
          end if
         end do
         if ( len0>maxlen ) then
          len0 = 2*maxlen
          exit
         end if
         if ( inFile%sublat(i)%ncomp.gt.1                               &
     &       .and. k.eq.inFile%sublat(i)%ncomp                          &
     &     ) then
              if ( inFile%sublat(i)%ns > 1 .or.                         &
     &             inFile%sublat(i)%compon(k)%zID == 0 ) then
               write(ch6,'(i6)') inFile%sublat(i)%ns
               inFile%genName = TRIM(inFile%genName)                    &
     &                        //'-'//TRIM(ADJUSTL(ch6))
              end if
              if(i.lt.inFile%nsubl)                                     &
     &           inFile%genName = TRIM(inFile%genName)//'-'
         end if
         len0 = len_trim(inFile%genName)
        end do
       end do

       if( inFile%icryst .ne. 0 ) then
        call crystStrType(inFile%icryst,chA)
        inFile%genName = TRIM(inFile%genName)//'_'//TRIM(chA)
       end if
       if ( len0==2*maxlen ) then
        inFile%genName = trim(inFile%genName)//eon
       end if

       return
!EOC
       end subroutine getGenName

!c*******************************************************************

!BOP
!!IROUTINE: initElement
!!INTERFACE:
       subroutine initElement(iZ,ID)
       implicit none
!!DESCRIPTION:
! ID (of type Element) structure initialization
!
!!ARGUMENTS:
        integer iZ
        type (Element) :: ID
!!REVISION HISTORY:
! Initial Version - A.S. - 2013
!EOP
!
!BOC
        integer nValence
        external nValence
        ID%ztot = iZ
        ID%zval = nValence(iZ)
        call atomIDc(dble(iZ),ID%symb,ID%name)
!c        ID%rws = fccalat(z) * (3.d0/16.d0*pi)**(1.d0/3.d0)
        return
!EOC
       end subroutine initElement

!c*******************************************************************

!BOP
!!IROUTINE: saveInput
!!INTERFACE:
       subroutine saveInput(inFile,fname)
       implicit none
!!DESCRIPTION:
!  to save inFile (of type IniFile) structure in a file,
!  fname provides name of the file
!
!!ARGUMENTS:
!       type (IniFile), target, optional :: inFile_in
!       type (IniFile), pointer :: inFile
       type (IniFile) :: inFile
       character*(*) fname
!!PARAMETERS
       integer, parameter :: unit=21 ! number of fortran unit used to access file
!!REVISION HISTORY:
! Initial Version - A.S. - 2013
!!REMARKS:
!  not all information from inFile is saved in the file <fname>,
!  however it can be used as input file to reproduce results
!
!EOP
!
!BOC

       type(DosBsf_data), pointer :: db_box => null()
       type(FS_data),  pointer :: fs_box => null()
       integer :: i, k, nspin, mtasa, nc, iunit, nq(3)
!       integer nrel_
       real(8) :: ck,par5
       character(8) :: raystring
       integer :: exc_type
       logical :: disordered
       integer :: nucl_mod

       if ( len_trim(fname)==0 ) then
        iunit = 6
       else
        iunit = unit
        open(iunit,file=trim(fname),status='unknown')
       end if

       call printline(iunit,                                            &
     & ' the name of MECCA input file [a32]...........................*'&
     &,63)
       if ( iunit==6 ) then
        write(iunit,'(a)') trim(inFile%genName)//def_name
       else
        write(iunit,'(a)') trim(fname)
       end if

       call printline(iunit,                                            &
     & ' no. of node processors, tasks, executable name ..............*'&
     &,63)
       write(iunit,100) inFile%nproc,inFile%ntask                       &
     &, ''''//trim(inFile%task)//''''
100    format(i4,i4,2x,a)

       write(iunit,'(a)') trim(inFile%io(nu_print)%name)

       call printline(iunit,                                            &
     & '.....file names.........................................end.> *'&
     &,63)
       do i = 1,inFile%numIOfiles
        if(inFile%io(i)%nunit==0) exit
        if ( inFile%io(i)%nunit.lt.0 ) cycle
        if ( len(trim(inFile%io(i)%name)) < 1 ) cycle
        select case ( i )
        case (nu_mix1)
           cycle
        case (nu_mix2)
           cycle
        case (nu_print)
           cycle
        case (nu_dop)
           cycle
        case (nu_mdlng)
           cycle
        case (nu_spkpt)
           cycle
        case (nu_yymom)
           cycle
        case (nu_dos)
           cycle
        case (nu_xyz)
         if ( inFile%icryst.ne.0 ) cycle
         write(iunit,101) inFile%io(i)%nunit                            &
     &,                  inFile%io(i)%status                            &
     &,                  inFile%io(i)%form                              &
     &,                  trim(inFile%io(i)%name)
        case default
         if ( index(trim(inFile%io(i)%name),trim(def_genname))==0 ) then
          write(iunit,101) inFile%io(i)%nunit                           &
     &,                    inFile%io(i)%status                          &
     &,                    inFile%io(i)%form                            &
     &,                  trim(inFile%io(i)%name)
101       format(1x,i2,1x,2a1,1x,a)
         end if
        end select
       end do
       write(iunit,'(a)') '  0'

       call printline(iunit,                                            &
     & ' method[i2], sprstech[i2], subr.stop level ...................*'&
     &, 63)
       write(iunit,103) inFile%imethod,inFile%sprstech,inFile%istop
103    format(2i2,1x,a10)

       call printline(iunit,                                            &
     &' print level [ipr=-100 gets min. o/p, other choices -10,-1,0  *' &
     &, 63)
       write(iunit,'(i4)') inFile%iprint

       call printline(iunit,                                            &
     &' maximum angular momentum allowed -- lmax ....................*' &
     &, 63)
       write(iunit,'(i3,2x,i2)') inFile%lmax

       call printline(iunit,                                            &
     & ' mt(asa)=0(1) :: (non)spinplrzed=(1)2 :: (Non)Relatvstc=(>1)0  '&
     &, 63)
       nspin = inFile%nspin
       if ( inFile%nspin==2 ) then
        if ( infile%nMM==1 )  nspin=3
        if ( infile%nMM==-1 ) nspin=-2
       end if

       mtasa = inFile%intgrsch*divisor+inFile%mtasa
       write(iunit,104) mtasa,nspin,inFile%nrel
104    format(i4,1x,i4,1x,i4,1x,i4)

       call printline(iunit,                                            &
     &' max of the real/reciprocal space translation vectors, Rclustr ' &
     &, 63)
       write(iunit,105) inFile%Rrsp,inFile%Rksp,inFile%Rclstr
105    format(f9.4,1x,f9.4,1x,f9.4)

       call printline(iunit,                                            &
     &' Ewald parameter (enmax, eneta) -- only for KS-str.consts ....*' &
     &, 63)
       write(iunit,105) inFile%enmax,inFile%eneta

       call printline(iunit,                                            &
     &' <nmesh/iq1,iq2,iq3,enlim_re,enlim_im> K-space intgrtn .......*' &
     &, 63)
       write(iunit,104) inFile%nmesh
       write(iunit,104)  inFile%nKxyz(1,1)                              &
     &,                  inFile%nKxyz(2,1)                              &
     &,                  inFile%nKxyz(3,1)
       do i=2,inFile%nmesh
        write(iunit,106) inFile%nKxyz(1,i)                              &
     &,                  inFile%nKxyz(2,i)                              &
     &,                  inFile%nKxyz(3,i)                              &
     &,                  inFile%enKlim(1,i-1)                           &
     &,                  inFile%enKlim(2,i-1)
106     format(i4,1x,i4,1x,i4,2x,f8.4,1x,f8.4)
       end do

       call printline(iunit,                                            &
     &' text to identify the system               (a64 format).!.....*' &
     &, 63)
       write(iunit,'(a)') trim(inFile%genName)

       call printline(iunit,                                            &
     &' the crystal structure type, icryst ..........................*' &
     &, 63)
       write(iunit,104) inFile%icryst

       call printline(iunit,                                            &
     &' lattice constant (at.un.), B/A, C/A .........................*' &
     &, 63)
       write(iunit,'(3(1x,g21.14))') inFile%alat,inFile%ba,inFile%ca

       call printline(iunit,                                            &
     &' number of species with different Z ..........................*' &
     &, 63)
        write(iunit,104) inFile%nelements

       call printline(iunit,                                            &
     &' number, valence, semicore and core charge of alloying species ' &
     &, 63)
       do i=1,inFile%nelements
        write(iunit,'(i4,1x,i3,1x,i2,1x,i3,5x,a2,1x,a)')                &
     &               inFile%elements(i)%ztot                            &
     &,              inFile%elements(i)%zval                            &
     &,              0                                                  &
     &,              inFile%elements(i)%ztot-inFile%elements(i)%zval    &
     &,              (trim(inFile%elements(i)%symb))                    &
     &,              (trim(inFile%elements(i)%name))
       end do

       call printline(iunit,                                            &
     &' number of sublattices and CPA/ATA (1/-1) control ............*' &
     &, 63)
       write(iunit,104) inFile%nsubl,inFile%ncpa

       call printline(iunit,                                            &
     &' sublatt.#,compon.#, MT radius/ ID#,concentration,ID-name of'    &
     &  //' each compon'                                                &
     &, 72)
        call checkForAFM(inFile%nMM,inFile%sublat(1:inFile%nsubl))
        disordered = maxval(inFile%sublat(1:inFile%nsubl)%ncomp) > 1
        do  i=1,inFile%nsubl
         nc = inFile%sublat(i)%ncomp
         ck = 1
         if ( inFile%sublat(i)%isDLM ) then
          if ( mod(nc,2)==0 ) then
           nc = nc/2
           ck = 2
          end if
         end if
         if ( disordered ) then
          write(iunit,107) i                                            &
     &,     nc                                                          &
     &,     zero
         else
          write(iunit,107) i                                            &
     &,     nc                                                          &
     &,     inFile%sublat(i)%rIS
         end if
107      format(i4,1x,i4,1x,es16.9)
         do k=1,nc
          if ( abs(inFile%nMM)==1 .or.                                  &
     &       inFile%sublat(i)%compon(k)%ex_ini_splt.ne.dflt_spltscale ) &
     &    then
          write(iunit,108) inFile%sublat(i)%compon(k)%zID               &
     &,               ck*inFile%sublat(i)%compon(k)%concentr            &
     &,               trim(inFile%sublat(i)%compon(k)%name)             &
     &,               inFile%sublat(i)%compon(k)%ex_ini_splt
          else
          write(iunit,108) inFile%sublat(i)%compon(k)%zID               &
     &,               ck*inFile%sublat(i)%compon(k)%concentr            &
     &,               trim(inFile%sublat(i)%compon(k)%name)
108       format(10x,i6,1x,f16.13,1x,a,1x,g13.4)
          end if
         end do
        end do

       if ( inFile%Tempr == zero ) then
        call printline(iunit,                                           &
     & ' igrid, ebot, etop, eitop, eibot, npts/0.1 ry,  npar .........*'&
     &, 63)
        par5 = inFile%eibot
       else
        call printline(iunit,                                           &
     & ' igrid, ebot, etop, eitop, Tempr, npts>1000,    npar .........*'&
     &, 63)
        par5 = inFile%Tempr
       end if
!?       if ( inFile%igrid==2 .or. inFile%igrid==4 ) then
!?       else
!?       end if
       write(iunit,109)                                                 &
     &      inFile%igrid                                                &
     &,     inFile%ebot                                                 &
     &,     inFile%etop                                                 &
     &,     inFile%eitop                                                &
     &,     par5                                                        &
     &,     inFile%npts                                                 &
     &,     inFile%npar
109    format(1x,i2,4(1x,g14.7),1x,i6,1x,i3)

       call printline(iunit,                                            &
     & ' nscf:  alpha,beta:  ichg:chg(pot)=0(1); imixrho:simple(broyden'&
     &//')=0(1)'                                                        &
     &,69)
       if ( nspin==1 ) then
        inFile%betamix = zero
       end if
       write(iunit,110)                                                 &
     &      inFile%nscf                                                 &
     &,     inFile%alphmix                                              &
     &,     inFile%betamix                                              &
     &,     inFile%imixtype                                             &
     &,     inFile%mixscheme
110    format(1x,i3,1x,f7.3,1x,f7.3,1x,i2,1x,i2)

       write(iunit,'(a,1x,a)') xc_tag                                   &
     &, ' format: XXXCCC [e.g. 116133 (PBEsol GGA), 001007 (VWN LDA)]'
       call getEXCtype(exc_type)
       if ( inFile%iXC > 999 ) then
        write(iunit,'(1x,i0.7,2x,i4)') inFile%iXC,exc_type
       else
        write(iunit,'(1x,i7,2x,i4)') inFile%iXC,exc_type
       end if
       if ( inFile%magnField.ne.zero ) then
        write(iunit,'(a,1x,a)') magnet_tag                              &
     &, ' (in Ry): '
        write(iunit,'(1x,g14.6)') inFile%magnField
!        write(iunit,'(1x,f10.3)') inFile%magnField/amuBoT*mu0     ! UNITS????
       end if
       call gDosBsfInput(db_box)
       if ( associated(db_box) ) then
         if ( db_box%idosplot == 0 ) then
          if ( db_box%dosbsf_esmear == zero ) then
           if ( inFile%eitop > 0 ) db_box%dosbsf_esmear=inFile%eitop/2
          end if
         end if
         write(iunit,'(a,1x,a)') dosplot_tag                            &
     &, ' <1st line> dosplot,scheme,Nepts,min,max,smear'//              &
     &  ' <2nd> nx,ny,nz [,ray_nrad,ray_nrec,type]'
         write(iunit,'(i3,1x,i2,2x,i4,2x,3(1x,e12.5))') db_box%idosplot,&
     &     db_box%dosbsf_kintsch,db_box%dosbsf_nepts,db_box%dosbsf_emin,&
     &                          db_box%dosbsf_emax,db_box%dosbsf_esmear
         if ( db_box%nx*db_box%ny*db_box%nz .ne. 0 ) then
           nq(1) = db_box%nx
           nq(2) = db_box%ny
           nq(3) = db_box%nz
         else
          if ( db_box%idosplot == 0 ) then
           nq = inFile%nKxyz(1:3,1)              !
          end if
         end if
         if ( db_box%idosplot == 0 .and. db_box%dosbsf_kintsch == 1 )   &
     &    then
          write(iunit,'(3(1x,i3),1x,2(1x,i4),2x,a)') nq(1:3)            &
     &                                  ,abs(nint(sum(nq)/3.)),0,'box'  ! print example for ray-method
         else      ! ray-method
          if ( db_box%ray_fixed ) then
           raystring = 'box'
          else
           raystring = 'rec'
          end if
          write(iunit,'(3(1x,i3),1x,2(1x,i4),2x,a)')                    &
     &          nq(1:3),db_box%ray_nrad,db_box%ray_nrec,trim(raystring)
         end if
!        if ( db_box%ibsfplot .ne. 0 ) then
         if ( bsf_k_latt_coord ) then
          write(iunit,'(a,1x,a)') bsfplot_tag                           &
     &      //' <1st line> bsfplot,n_of_directions,samplerate /'        &
     &      //' <2:1+n> K1,K2,K3,label (in k-space lattice vectors)'
         else
          write(iunit,'(a,1x,a)') bsfplot_tag                           &
     &      //' <1st line> bsfplot,n_of_directions,samplerate /'        &
     &      //' <2:1+n> Kx,Ky,Kz,label (in Cartsn.units of 2pi/a)'
         end if
         if  ( db_box%bsf_nkwaypts == 0 .and. db_box%ibsfplot==0 ) then
          db_box%bsf_ksamplerate = 10
          db_box%bsf_nkwaypts = 1
          if ( .not.allocated(db_box%bsf_kwaypts) ) then
           allocate(db_box%bsf_kwaypts(1:3,1:db_box%bsf_nkwaypts))
           allocate(db_box%bsf_kptlabel(1:db_box%bsf_nkwaypts))
          end if
          db_box%bsf_kwaypts(1:3,1) = zero
          db_box%bsf_kptlabel(1) = 'G'
         end if
         write(iunit,'(1x,i2,1x,2(1x,i4))') db_box%ibsfplot,            &
     &                       db_box%bsf_nkwaypts,db_box%bsf_ksamplerate
         do i = 1, db_box%bsf_nkwaypts    ! n-1 directions are defined by n points
          write(iunit,'(1x,3(g14.4,1x),a)') db_box%bsf_kwaypts(1:3,i),  &
     &                                   trim(db_box%bsf_kptlabel(i))
         end do
!        end if
       end if
       call gFsInput(fs_box)
       if ( associated(fs_box) ) then
        if ( fs_box%ifermiplot .ne. 0 ) then
         if ( bsf_k_latt_coord ) then
          write(iunit,'(a,1x,a)')  fsplot_tag                           &
     &      //'  <1st line> fermiplot,en_offset,nksymmpts,grid_sampling'&
     &      //',no._rays /'                                             &
     &      //' <2:1+n> K1,K2,K3,label (in k-space lattice vectors)'
         else
          write(iunit,'(a,1x,a)')  fsplot_tag                           &
     &      //'  <1st line> fermiplot,en_offset,nksymmpts,grid_sampling'&
     &      //',no._rays /'                                             &
     &      //' <2:1+n> Kx,Ky,Kz,label (in Cartsn.units of 2pi/a)'
         end if
         write(iunit,'(1x,i2,1x,e14.6,2x,i4,1x,i4,1x,i4)')              &
     &           fs_box%ifermiplot,fs_box%fermi_en,fs_box%fermi_nkpts+1,&
     &             fs_box%fermi_gridsampling,fs_box%fermi_raysampling
         write(iunit,'(1x,3(g14.4,1x),a)') fs_box%fermi_korig(1:3),     &
     &                                  trim(fs_box%fermi_orilabel)
!         write(iunit,'(''%%'')')
         do i = 1, fs_box%fermi_nkpts
          write(iunit,'(1x,3(g14.4,1x),a)') fs_box%fermi_kpts(1:3,i),   &
     &                                   trim(fs_box%fermi_kptlabel(i))
         end do
        end if
       end if

! so_power
       write(iunit,'(a,1x,a)') so_power_tag
       write(iunit,'(1x,es13.5)') inFile%so_power
! logR-mesh-step
       write(iunit,'(a,1x,a)') xrstep_tag
       write(iunit,'(1x,es13.5)') gXRmeshStep()
! ef_tol
       write(iunit,'(a,1x,a)') eftol_tag
       write(iunit,'(1x,es11.3)') gEftol()
! e_tol
       write(iunit,'(a,1x,a)') etol_tag
       write(iunit,'(1x,es11.3)') gEtol()
! tc_tol
       write(iunit,'(a,1x,a)') tctol_tag
       write(iunit,'(1x,es11.3)') gTcTol()
! q_tol
       write(iunit,'(a,1x,a)') qtol_tag
       write(iunit,'(1x,es11.3)') gQtol()
! zpten
       write(iunit,'(a,1x,a)') zpten_tag
       write(iunit,'(1x,i1)') gZeroPt()
! nuclmod
       write(iunit,'(a,1x,a)') nuclmod_tag
       nucl_mod = 0
       if ( gPointNucleus() ) nucl_mod=1
       write(iunit,'(1x,i1)') nucl_mod

       call printline(iunit,                                            &
     & ' end of data. . . . . . . . . . . . . . . . . . . . . . . . . *'&
     &,63)

       if ( iunit .ne. 6 ) close(iunit)
       return
!EOC
       end subroutine saveInput

!c*******************************************************************

!BOP
!!IROUTINE: readInput
!!INTERFACE:
       subroutine readInput(nrec,charfile,intfile)
       implicit none
!!DESCRIPTION:
! to initialize run-input structure {\em intfile} (of type IniFile)
! from {\em charfile}, a character array
!
!!ARGUMENTS:
       integer, intent(in) :: nrec
       character(80), intent(in)  :: charfile(nrec)
       type(IniFile), intent(out) :: intfile
!!REVISION HISTORY:
! Initial Version - A.S. - 2013
!!REMARKS:
! length of each line in charfile is 80 symbols
!EOP
!
!BOC
       type(IOFile)  :: io_tmp

       real(8) :: tc,par5
       integer :: i,j,k,nl,n,nspin,nsubl,multi_c,nrel_
       integer :: st_env
       character(1) :: char1,char2

       call setIniFile(intfile)

      nl = 2
      read(charfile(nl),'(a)') intfile%name
      call true_name(intfile%name,'/',low=.true.)
      nl = 4
      read(charfile(nl),*) intfile%nproc,intfile%ntask,intfile%task
      nl = 6
      do i=1,ipunit+1
       read(charfile(nl+i),*) n
       if ( n==0 ) exit
      end do
      if ( n .ne. 0 ) then
          write(*,*) ' ERROR :: there are too many files in the input'
          call fstop('readInput')
          return
      end if
      n = i
      do i=1,n
       nl = nl+1
       read(charfile(nl),101) io_tmp%nunit                              &
     &,                   io_tmp%status                                 &
     &,                   io_tmp%form                                   &
     &,                   io_tmp%name
101    format(1x,i2,1x,2a1,1x,a)
       j = nunit2num(io_tmp%nunit)
       if ( j>0 ) then
         intfile%io(j)%nunit = io_tmp%nunit
         intfile%io(j)%status = io_tmp%status
         intfile%io(j)%form = io_tmp%form
         intfile%io(j)%name = io_tmp%name
       end if
      end do
      nl = nl+1
      nl = nl+1
      read(charfile(nl),*)                                              &
     &                 intfile%imethod,intfile%sprstech,intfile%istop
      nl = nl+1
      nl = nl+1
      read(charfile(nl),*) intfile%iprint
      nl = nl+1
      nl = nl+1
      read(charfile(nl),*) intfile%lmax
      nl = nl+1
      nl = nl+1
      read(charfile(nl),*) i,nspin,intfile%nrel
      nrel_ = intfile%nrel
      if ( i==1 ) then
        intfile%intgrsch=1
        intfile%mtasa=2
      else
        intfile%mtasa = g_unpack(i,intfile%intgrsch)  !  (i.e. input_mtasa) = ivar_mtz*divisor + mtasa
      end if
      call gEnvVar('ENV_M2013',.false.,st_env)
      if ( st_env>=0 .and. st_env .ne. intfile%mtasa ) then
       intfile%mtasa = st_env
       write(6,*) ' ENV_M2013 ==> mtasa = ',intfile%mtasa
      end if
      call gEnvVar('ENV_VAR_MTZ',.false.,st_env)
      if ( st_env>=0 .and. st_env .ne. intfile%intgrsch ) then
       intfile%intgrsch = st_env
       write(6,*) ' ENV_VAR_MTZ ==> ivar_mtz = ',intfile%intgrsch
      end if
      if ( nspin.gt.2 ) then
        intfile%nMM = 1
        intfile%nspin = 2
      else if ( nspin==-2 ) then
        intfile%nMM = -1
        intfile%nspin = 2
      else
        intfile%nspin = nspin
        if ( nspin==1 ) intfile%nMM = 0
      end if
      nl = nl+1
      nl = nl+1
      read(charfile(nl),*) intfile%Rrsp,intfile%Rksp,intfile%Rclstr
      nl = nl+1
      nl = nl+1
      read(charfile(nl),*) intfile%enmax,intfile%eneta
      nl = nl+1
      nl = nl+1
      read(charfile(nl),*) intfile%nmesh
      nl = nl+1
      read(charfile(nl),*) intfile%nKxyz(1,1)                           &
     &,                   intfile%nKxyz(2,1)                            &
     &,                   intfile%nKxyz(3,1)
      do i=2,intfile%nmesh
        nl = nl+1
        read(charfile(nl),*) intfile%nKxyz(1,i)                         &
     &,                   intfile%nKxyz(2,i)                            &
     &,                   intfile%nKxyz(3,i)                            &
     &,                   intfile%enKlim(1,i-1)                         &
     &,                   intfile%enKlim(2,i-1)
      end do
!
      intfile%nKxyz(1:3,2:) = abs(intfile%nKxyz(1:3,2:))  ! only 1st grid may have negative nKxyz
!
      nl = nl+1
      nl = nl+1
      read(charfile(nl),*) intfile%genName
      nl = nl+1
      nl = nl+1
      read(charfile(nl),*) intfile%icryst
      nl = nl+1
      nl = nl+1
      read(charfile(nl),*) intfile%alat,intfile%ba,intfile%ca
      nl = nl+1
      nl = nl+1
      read(charfile(nl),*) intfile%nelements
      nl = nl+1
      if ( allocated(intfile%elements) ) deallocate(intfile%elements)
      allocate(intfile%elements(1:intfile%nelements))
      do i=1,intfile%nelements
        nl = nl+1
        read(charfile(nl),*)                                            &
     &                intfile%elements(i)%ztot                          &
     &,               intfile%elements(i)%zval                          &
     &,               k                                                 &
     &,               j                                                 &
     &,               intfile%elements(i)%symb                          &
     &,               intfile%elements(i)%name
      end do
      nl = nl+1
      nl = nl+1
      read(charfile(nl),*) intfile%nsubl,intfile%ncpa
      multi_c = 0
      nl = nl+1
      if ( allocated(intfile%sublat) ) deallocate(intfile%sublat)
      allocate(intfile%sublat(1:intfile%nsubl))
      do  i=1,intfile%nsubl
        nl = nl+1
        read(charfile(nl),*) j                                          &
     &,      intfile%sublat(i)%ncomp                                    &
     &,      intfile%sublat(i)%rIS
        do k=1,intfile%sublat(i)%ncomp
          nl = nl+1
          tc = tiny(tc)
          read(charfile(nl),*,END=100,ERR=100)                          &
     &                intfile%sublat(i)%compon(k)%zID                   &
     &,               intfile%sublat(i)%compon(k)%concentr              &
     &,               intfile%sublat(i)%compon(k)%name,tc
100       continue
          if ( intfile%nspin>1 ) then
           if ( tc == tiny(tc) ) then
            if ( intfile%sublat(i)%compon(k)%zID > 0 ) then
             tc = dflt_spltscale
            else
             tc = zero
            end if
           end if
           intfile%sublat(i)%compon(k)%ex_ini_splt = tc
          end if
!
          if ( intfile%sublat(i)%compon(k)%zID > max_nonrel_z ) then
            if ( nrel_ == dflt_nonrel ) intfile%nrel = dflt_rel
          end if
          do j=1,intfile%nelements
            if ( intfile%sublat(i)%compon(k)%zID .eq.                   &
     &                       intfile%elements(j)%ztot ) then
              intfile%sublat(i)%compon(k)%zcor =                        &
     &           intfile%sublat(i)%compon(k)%zID -                      &
     &                     intfile%elements(j)%zval
              exit
            end if
          end do
        end do
        multi_c = multi_c + (intfile%sublat(i)%ncomp-1)
      end do
!
      if ( intfile%ncpa==1 ) then  ! CPA with default number of iterations
       if ( multi_c>0 ) then
        intfile%ncpa = maxcpa
       else
        if ( intfile%nMM .ne. 1 ) then
         intfile%ncpa = 0
        else
         intfile%ncpa = maxcpa
        end if
       end if
      end if
!
      do  i=1,intfile%nsubl
         tc = sum(intfile%sublat(i)%compon(1:intfile%sublat(i)%ncomp)   &
     &            %concentr)
       if ( tc .ne. one ) then
        if ( abs(tc - one) <= 1.d-5 ) then
        intfile%sublat(i)%compon(1:intfile%sublat(i)%ncomp)%concentr =  &
     &  intfile%sublat(i)%compon(1:intfile%sublat(i)%ncomp)%concentr/tc
        else
         write(6,*) ' Sum of concentrations for sublattice ',i,          &
     &   ' is not equal to 1; sum(concentr)=',tc
         call fstop('readInput ERROR :: sum(concentrations) .NE. 1')
        end if
       end if
      end do
!
      if ( nspin>1 ) then
!************************************************************************
      if ( intfile%nMM == -1 ) then
! check if there are DLM-components
       do  i=1,intfile%nsubl
        if ( mod(intfile%sublat(i)%ncomp,2)==0 ) then
         do k=1,intfile%sublat(i)%ncomp,2
          if ( intfile%sublat(i)%compon(k)%zID ==                       &
     &                      intfile%sublat(i)%compon(k+1)%zID   .and.   &
     &         intfile%sublat(i)%compon(k)%concentr ==                  &
     &                    intfile%sublat(i)%compon(k+1)%concentr ) then
           intfile%sublat(i)%compon(k+1)%ex_ini_splt =                  &
     &                       -intfile%sublat(i)%compon(k)%ex_ini_splt
           intfile%sublat(i)%isDLM = .true.
           intfile%nMM = 0
          end if
          multi_c = multi_c + 1
         end do
        end if
       end do
      end if
200   continue

      if ( intfile%nMM == -1 ) then
! non-DLM components
       nsubl = intfile%nsubl
       intfile%sublat(1:nsubl)%isDLM=.false.
       if ( mod(nsubl,2)==0 ) then
        do  i=1,nsubl/2
         if (isSublatAFMeqv (intfile%sublat(i),                         &
     &                           intfile%sublat(i+nsubl/2)) ) then
          intfile%sublat(i)%isflipped=.false.
          intfile%sublat(i+nsubl/2)%isflipped=.true.
          intfile%sublat(i)%flippoint=i
          intfile%sublat(i+nsubl/2)%flippoint=i
          do k=1,intfile%sublat(i)%ncomp
           intfile%sublat(i+nsubl/2)%compon(k)%ex_ini_splt =            &
     &          -intfile%sublat(i)%compon(k)%ex_ini_splt
          end do
         end if
        end do
       end if
      end if
!
      if ( intfile%nMM==1 .or.                                          &
     &      any(intfile%sublat(1:intfile%nsubl)%isDLM) ) then
       if ( intfile%nMM==1 )intfile%sublat(1:intfile%nsubl)%isDLM=.true.
       do  i=1,intfile%nsubl
        if ( intfile%sublat(i)%isDLM ) then
         do k=1,intfile%sublat(i)%ncomp
          j = k + intfile%sublat(i)%ncomp
          if ( j>size(intfile%sublat(i)%compon) ) then
           write(6,'('' ERROR: Too many components on the same ''//     &
     &                ''sublattice due to DLM mode'')')
           write(6,'(a,i2)')                                            &
     &       ' Max. number of components per sublattice is ',           &
     &                               size(intfile%sublat(i)%compon)
           call fstop(' readInput')
          end if
          intfile%sublat(i)%compon(j) = intfile%sublat(i)%compon(k)
          intfile%sublat(i)%compon(j)%ex_ini_splt =                     &
     &                     -intfile%sublat(i)%compon(k)%ex_ini_splt
         end do
         intfile%sublat(i)%ncomp = intfile%sublat(i)%ncomp*2
         tc = sum(intfile%sublat(i)%compon(1:intfile%sublat(i)%ncomp)   &
     &            %concentr)
         intfile%sublat(i)%compon(1:intfile%sublat(i)%ncomp)%concentr = &
     &   intfile%sublat(i)%compon(1:intfile%sublat(i)%ncomp)%concentr/tc
        end if
       end do
      end if
!************************************************************************
      end if    !  nspin>1
!
      nl = nl+1
      nl = nl+1
      read(charfile(nl),*)                                              &
     &      intfile%igrid                                               &
     &,     intfile%ebot                                                &
     &,     intfile%etop                                                &
     &,     intfile%eitop                                               &
     &,     par5                                                        &
     &,     intfile%npts                                                &
     &,     intfile%npar
      if ( intfile%npts > 1000 ) then
!         intfile%Tempr = abs(intfile%eibot)/(pi*0.5d0)
        if ( par5.ne.zero ) then
         intfile%Tempr = abs(par5)    ! Tempr in Ry
         intfile%eibot = (pi*0.5d0)*intfile%Tempr
        else
         write(6,'(a,i0,a)') ' WARNING :: Tempr = 0, npts = ',          &
     &                           intfile%npts,' > 1000!'
         intfile%npts = mod(intfile%npts,1000)
         write(6,'(a)') '       :: number of E-points for calculation'//&
     &    ' is corrected to be less than 1000, new npts =',intfile%Tempr
         intfile%Tempr = zero
        end if
      else
       intfile%Tempr = zero
       if ( par5<zero ) then
        write(6,'(a,i0,a)') ' ERROR :: T=0,  eibot < 0'
        call fstop('BAD INPUT DATA')
       end if
       intfile%eibot = par5
      end if
!
      if ( intfile%ebot>zero ) then
       do  i=1,intfile%nsubl
        do k=1,intfile%sublat(i)%ncomp
         do j=1,intfile%nelements
          if ( intfile%sublat(i)%compon(k)%zID .eq.                     &
     &                                   intfile%elements(j)%ztot ) then
        intfile%sublat(i)%compon(k)%zcor=intfile%sublat(i)%compon(k)%zID
           intfile%elements(j)%zval = zero
           exit
          end if
         end do
        end do
       end do
      end if
!
      nl = nl+1
      nl = nl+1
      read(charfile(nl),*)                                              &
     &      intfile%nscf                                                &
     &,     intfile%alphmix                                             &
     &,     intfile%betamix                                             &
     &,     intfile%imixtype                                            &
     &,     intfile%mixscheme
      if ( len_trim(adjustl(intfile%genName))<1 ) then
       call getGenName(intfile)
      end if
!
      do while ( nl<nrec )
       nl = nl+1
       if ( len_trim(charfile(nl)) < 2 ) cycle
       read(charfile(nl),'(2a1)') char1,char2
       if ( char1 .ne. def_tag .or.  char2 .eq. ' ' ) cycle
       call read_new_sections(nrec,charfile,nl,intfile)
      end do
!
      if ( intfile%nspin == 1 ) then
       intfile%betamix = zero
      end if
!
      if ( readFailed(nl,nrec,charfile) ) then
          write(6,*) ' filename: ',intfile%name
          call fstop('readInput ERROR :: nl>nrec')
      end if

      return
!EOC
      end subroutine readInput

!c*******************************************************************

!BOP
!!IROUTINE: read_new_sections
!!INTERFACE:
      subroutine read_new_sections(nrec,charfile,nl,intfile)
      implicit none
!!DESCRIPTION:
! to update
! {\bv
!   eXchange-Correlation functional,
!   magnetic field,
!   density of states,
!   Bloch spectral function,
!   or Fermi surface
! \ev}
! run-input information in {\em intfile} (of type IniFile) from
! {\em charfile}, a character array
!
!!ARGUMENTS:
      integer, intent(in) :: nrec
      character(80), intent(in) :: charfile(nrec)
      integer, intent(inout) :: nl
      type(IniFile), intent(inout) :: intfile
!!REVISION HISTORY:
! Initial Version - A.S. - 2013
!!REMARKS:
! length of each line in charfile is 80 symbols
!EOP
!
!BOC
!
      integer i
      logical :: isdis,read_fail
      character(80) :: tmpname,tmpline
      character(1)  :: char80(len(tmpname))
!
      integer :: idosplot,dosbsf_kintsch,dos_nepts
      real(8) :: dos_emin,dos_emax,dos_esmear
      integer :: nx,ny,nz,iray_nrad,iray_nrec
      character(8) :: raystring
      logical :: iray_fixed
      integer :: ibsfplot,bsf_nkwaypts,bsf_ksamplerate
!      character(80) :: bsf_file
!      integer :: bsf_fileno
      real(8), allocatable :: bsf_kwaypts(:,:)      ! (3,ipbsfkpts)
      character(32), allocatable :: bsf_kptlabel(:) ! (ipbsfkpts)
      integer :: ifermiplot,fermi_nkpts
      integer :: fermi_gridsampling,fermi_raysampling
      real(8), allocatable :: fermi_kpts(:,:)   ! (3,ipbsfkpts)
      real(8) :: fermi_en,fermi_korig(1:3)
!      character(80) :: fermi_file(2)
!      integer :: fermi_fileno(2)
      character(80) :: fermi_orilabel
      character(32), allocatable :: fermi_kptlabel(:)  ! (ipbsfkpts)
      type(DosBsf_data), pointer :: db_box => null()
      integer :: exc_type,iXC
      logical :: do_ignore = .false.
!
      real(8) :: so_power,xr_step,ef_tol,e_tol,tc_tol,q_tol,ext_ex_ptnl
      real(8) :: zpE,nuclmod
!
      character(1) :: ch1
      integer, parameter :: nkw = len(xpot_corr_kword)
      character(nkw) :: tmpword
      integer :: k,nw
!
      interface
       subroutine setXpotCorr(xpot_corr_id,xpot_corr_par)
       integer :: xpot_corr_id
       real(8), optional :: xpot_corr_par(:)
       end subroutine
      end interface
!
999   format(/'WARNING: unable to read section ',a,'...')
!
      read_fail = .false.
      tmpline = charfile(nl)
      call true_name(tmpline)
      nw = index(tmpline,' ')
      tmpname = ''
      if ( nw>1 ) then
       read(tmpline,'(a)') tmpname(1:nw-1)
      else
       tmpname = tmpline
      end if
      select case (tmpname)
       case( xc_tag )
        read_fail = .true.
        nl = nl+1
        read(charfile(nl),*,end=1004,err=1004) intfile%iXC
        exc_type = -1
        read(charfile(nl),*,end=1000,err=1000) iXC,exc_type
1000    continue
        call setEXCtype(exc_type)
        read_fail = .false.
        if ( nl+1 > nrec ) return
!
        read(charfile(nl+1),'(a1)') ch1
        if ( ch1 == ' ' ) then
         nl = nl+1
         read_fail = .true.
         read(charfile(nl),*,end=1004,err=1004) tmpword
!
         if ( tmpword==xpot_corr_kword ) then  ! X-potential-correction
          char80 = ""
          read_fail = .true.                   ! parameters
          nw = index(charfile(nl),xpot_corr_kword)
          nw = nw + len(xpot_corr_kword)
          read(charfile(nl),'(80a1)') char80
          k = get_k(size(char80)-nw,char80(nw+1:size(char80)))   ! number of entries in the line
          allocate(intfile%x_pot_corr%params(max(1,k-1)))
          write(tmpline,*) char80(nw+1:size(char80))
          if ( k>1 ) then
           read(tmpline,*,err=1004,end=1004)                            &
     &             intfile%x_pot_corr%id,intfile%x_pot_corr%params
           call setXpotCorr(intfile%x_pot_corr%id,                      &
     &                                      intfile%x_pot_corr%params)
          else
           read(tmpline,*,err=1004,end=1004) intfile%x_pot_corr%id
           call setXpotCorr(intfile%x_pot_corr%id)
          end if
          read_fail = .false.
         else
          read_fail = .true.
         end if
        end if
1004    continue
        if ( read_fail ) then
          write(6,'(/'' unable to read section '',a,''...''/)') xc_tag
          call fstop('ERROR: incorrect syntax in input file')
        end if
!
       case( old_magn_tag )
        write(6,'(/''ERROR: input tag '',a,'' is not in use anymore '', &
     &             ''and should be replaced by '',a/)')                 &
     &                                  old_magn_tag,magnet_tag
        read_fail = .true.
        call fstop('unrecognized input keyword (tag) '//old_magn_tag)
!
       case( dosplot_tag )
!
! density of states section
        read_fail = .true.
        nl = nl+1
        read(charfile(nl),*,end=1012,err=1012) idosplot,dosbsf_kintsch, &
     &                   dos_nepts,dos_emin,dos_emax,dos_esmear
        if ( idosplot>0 ) then
         if( dosbsf_kintsch < 0 .or. 3 < dosbsf_kintsch ) then
          call fstop("unrecognized k-space integration scheme for DOS")
         else if ( dos_esmear <= zero ) then
          write(6,'('' in '',a,'' section  esmear = '',i2,              &
     &                         '', but should be positive'')')          &
     &                                dosplot_tag,dos_esmear
          call fstop("bad smearing for DOS")
         end if
        end if
        if ( dosbsf_kintsch==1 ) then
         nl = nl+1
         read(charfile(nl),*,end=1012,err=1012) nx,ny,nz
         call sDosInpParams(nx=nx,ny=ny,nz=nz)
        else
         nl = nl+1
         read(charfile(nl),*,end=1012,err=1012) nx,ny,nz                &
     &                                 ,iray_nrad,iray_nrec,raystring
         if( trim(raystring) == 'box' ) then
           iray_fixed = .true.
         else if( trim(raystring) == 'rec' ) then
          iray_fixed = .false.
         else
          call fstop("unrecognized keyword: "//raystring)
         end if
         call sDosInpParams(nx=nx,ny=ny,nz=nz,                          &
     &       fixed_ray=iray_fixed,nrec_ray=iray_nrec,nrad_ray=iray_nrad)
        end if
        call sDosInpParams(iplot=idosplot,kintsch=dosbsf_kintsch)
        call sDosInpParams(nepts=dos_nepts,emin=dos_emin,emax=dos_emax, &
     &                                                esmear=dos_esmear)
        read_fail = .false.
!        if( idosplot.ne.0 )then
!          write(6,'(''  Print the DOS after convergence '',t40)')
!        else
!          write(6,'(''  DOS will not be written in a separate file ''
!     &                  ,t40)')
!        endif
1012    continue
        if ( read_fail ) then
          write(6,'(/''WARNING: unable to read section '',a,            &
     & '' (error in input parameters: values,types,their number?)''/)') &
     &            dosplot_tag
        end if
!
       case( bsfplot_tag )
!
! bsf section
        call true_name(tmpline,low=.true.)
        if ( index(tmpline,'kx,ky,kz') > 0 ) then
         call setBsfCrtsn()
        end if
        do_ignore = .false.
        read_fail = .true.
        nl = nl+1
        read(charfile(nl),*,end=1023,err=1023) ibsfplot,bsf_nkwaypts,   &
     &                                                  bsf_ksamplerate
        if( bsf_nkwaypts > ipbsfkpts ) then
          write(6,*) 'WARNING: max bsf_nkwaypts allowed = ', ipbsfkpts
          bsf_nkwaypts = ipbsfkpts
!        call fstop("max. no. of k-space waypts exceeded")
!        else if (bsf_nkwaypts < 2 ) then
!         write(6,'(a,i2,a) 'WARNING: BSF calculations are disabled, '   &
!     &              bsf_nkwaypts = ',bsf_nkwaypts
!         ibsfplot = 0
        end if

!        if( ibsfplot.ne.0 ) then
!         bsf_file = 'akeBAND.dat'
!         bsf_fileno = 90
!         open(unit=1,file=bsf_file,status='replace',iostat=iec)
!         if(iec.ne.0) call fstop('failed to open AkE file')
!         close(1)
!        end if
         allocate(bsf_kwaypts(3,max(1,bsf_nkwaypts)),                   &
     &                                    bsf_kptlabel(bsf_nkwaypts))
        do i = 1, bsf_nkwaypts
          nl = nl+1
          read(charfile(nl),*,end=1023,err=1023) bsf_kwaypts(1:3,i),    &
     &                                                   bsf_kptlabel(i)
        end do
        do_ignore = .false.
        call gDosBsfInput(db_box)
        idosplot = -1
        dosbsf_kintsch = -1
        if ( .not.associated(db_box) ) then
           do_ignore = .true.
        else
         if ( db_box%idosplot <= 0 .or. db_box%dosbsf_kintsch<2 ) then
            do_ignore = .true.
          idosplot = db_box%idosplot
          dosbsf_kintsch = db_box%dosbsf_kintsch
         end if
        end if
        if ( do_ignore ) then
         read_fail = .false.
         if ( ibsfplot>0 ) then
          write(6,'(/''WARNING: section '',a,'' is ignored'')')          &
     &                            bsfplot_tag
          if ( idosplot <1 ) then
           write(6,'(3x,a,'' section is not activated'',                 &
     &         '', but it should'')')   dosplot_tag
          else
           if ( dosbsf_kintsch <=1 ) then
            write(6,'('' in '',a,'' section  ksintsch = '',i2,           &
     &                         '', but 2 is expected '')')              &
     &                                dosplot_tag,dosbsf_kintsch
           end if
          end if
         end if
        else
         call sBsfInpParams(iplot=ibsfplot,ksamplerate=bsf_ksamplerate)
         call sBsfInpParams(kwaypts=bsf_kwaypts,kptlabel=bsf_kptlabel)
        end if
        read_fail = .false.
1023    continue
        if ( read_fail ) then
         write(6,999) tmpname
        end if
        if ( allocated(bsf_kwaypts) ) deallocate(bsf_kwaypts)
        if ( allocated(bsf_kptlabel) ) deallocate(bsf_kptlabel)
!
       case( fsplot_tag )
!
! Fermi surface section
        do_ignore = .false.
        read_fail = .true.
        nl = nl+1
        read(charfile(nl),*,end=1034,err=1034) ifermiplot,fermi_en,     &
     &                  fermi_nkpts,fermi_gridsampling,fermi_raysampling
        nl = nl+1
        fermi_nkpts = fermi_nkpts-1
        read(charfile(nl),*,end=1034,err=1034) fermi_korig(1:3),        &
     &                                                    fermi_orilabel

!        nl = nl+1
!        read(charfile(nl),*,end=1034,err=1034)
      ! is the system disordered?
        isdis = .false.
        do i = 1, intfile%nsubl
         if(intfile%sublat(i)%ncomp>1) isdis = .true.
        end do
        allocate(fermi_kpts(3,fermi_nkpts),fermi_kptlabel(fermi_nkpts))
        do i = 1, fermi_nkpts
          nl = nl+1
          read(charfile(nl),*,end=1034,err=1034) fermi_kpts(1:3,i),     &
     &                                      fermi_kptlabel(i)
        end do
        do_ignore = .false.
        if( fermi_nkpts+1<3 ) then
           do_ignore = .true.
        end if
        call gDosBsfInput(db_box)
        idosplot = -1
        dosbsf_kintsch = -1
        dos_esmear = zero
        if ( .not.associated(db_box) ) then
           do_ignore = .true.
        else
         idosplot = db_box%idosplot
         dosbsf_kintsch = db_box%dosbsf_kintsch
         dos_esmear = db_box%dosbsf_esmear
         if ( idosplot <= 0                                             &
     &    .or.dosbsf_kintsch<2                                          &
     &    .or.dos_esmear<=zero                                          &
     &     ) then
            do_ignore = .true.
         end if
        end if
        if ( do_ignore ) then
         read_fail = .false.
         write(6,'(/''WARNING: section '',a,'' is ignored'')')          &
     &                            fsplot_tag
         if( fermi_nkpts+1<3 ) then
          write(6,'(''  fermi_nkpts = '',i2,                            &
     &       '', fermi surface plot requires at least 3 input kpts'')') &
     &           fermi_nkpts+1
         end if
         if ( idosplot <1 ) then
          write(6,'(3x,a,'' section is not activated'',                 &
     &         '', but it should'')')        dosplot_tag
         else
          if ( dosbsf_kintsch <=1 ) then
           write(6,'('' in '',a,'' section  ksintsch = '',i2,           &
     &                         '', but 2 is expected '')')              &
     &                                dosplot_tag,dosbsf_kintsch
          end if
          if ( dos_esmear <= zero ) then
           write(6,'('' in '',a,'' section  esmear = '',i2,             &
     &                         '', but should be positive'')')          &
     &                                dosplot_tag,dos_esmear
          end if
         end if
        else
         call sFsInpParams(iplot=ifermiplot,en=fermi_en,                &
     &       korig=fermi_korig,orilabel=fermi_orilabel,                 &
     &    gridsampling=fermi_gridsampling,raysampling=fermi_raysampling)
         call sFsInpParams(kpts=fermi_kpts,kptlabel=fermi_kptlabel)
!        if(ifermiplot.ne.0) then
!         fermi_fileno(1) = 91
!         fermi_fileno(2) = 92
!         if(nspin == 1) then
!          if(ifermiplot.eq.1) then
!            fermi_file(1) = 'akeBZarea.dat'
!!            open(unit=fermi_fileno(1),file='akeBZarea.dat',             &
!!     &                                      status='replace',iostat=iec)
!          else if(isdis) then
!            fermi_file(1) = 'akeBZray.dat'
!!            open(unit=fermi_fileno(1),file='akeBZrays.dat',             &
!!     &                                      status='replace',iostat=iec)
!          else
!            fermi_file(1) = 'fermi.dat'
!!            open(unit=fermi_fileno(1),file='fermi.dat',                 &
!!     &                                      status='replace',iostat=iec)
!          end if
!!          if(iec.ne.0) call fstop('failed to open Fermi file')
!         else
!          if(ifermiplot.eq.1) then
!            fermi_file(1) = 'akeBZarea_up.dat'
!            fermi_file(2) = 'akeBZarea_dn.dat'
!!            open(unit=fermi_fileno(1),file='akeBZarea_up.dat',          &
!!     &                                      status='replace',iostat=iec)
!!            if(iec.ne.0) call fstop('failed to open Fermi file')
!!            open(unit=fermi_fileno(2),file='akeBZarea_dn.dat',          &
!!     &                                      status='replace',iostat=iec)
!!            if(iec.ne.0) call fstop('failed to open Fermi file')
!          else if(isdis) then
!            fermi_file(1) = 'akeBZray_up.dat'
!            fermi_file(2) = 'akeBZray_dn.dat'
!!            open(unit=fermi_fileno(1),file='akeBZrays_up.dat',          &
!!     &                                      status='replace',iostat=iec)
!!            if(iec.ne.0) call fstop('failed to open Fermi file')
!!            open(unit=fermi_fileno(2),file='akeBZrays_dn.dat',          &
!!     &                                      status='replace',iostat=iec)
!!            if(iec.ne.0) call fstop('failed to open Fermi file')
!          else
!            fermi_file(1) = 'fermi_up.dat'
!            fermi_file(2) = 'fermi_dn.dat'
!!            open(unit=fermi_fileno(1),file='fermi_up.dat',              &
!!     &                                      status='replace',iostat=iec)
!!            if(iec.ne.0) call fstop('failed to open Fermi file')
!!            open(unit=fermi_fileno(2),file='fermi_dn.dat',              &
!!     &                                      status='replace',iostat=iec)
!!            if(iec.ne.0) call fstop('failed to open Fermi file')
!          end if
!         end if
!        end if
         read_fail = .false.
        end if
1034    continue
        if ( allocated(fermi_kpts) ) deallocate(fermi_kpts)
        if ( allocated(fermi_kptlabel) ) deallocate(fermi_kptlabel)
        if ( read_fail ) then
         write(6,999) tmpname
        end if
!
       case( so_power_tag )
! so_power
        call one_param_section(nl,charfile,read_fail,so_power)
        if ( read_fail ) then
         write(6,999) tmpname
        else
         intfile%so_power = so_power
        end if
!
       case( xrstep_tag )
!
! logR-mesh-step
        call one_param_section(nl,charfile,read_fail,xr_step)
        if ( read_fail ) then
         write(6,999) tmpname
        else
         call setXRmeshStep(xr_step)
        end if
!
       case( eftol_tag )
! ef_tol
        call one_param_section(nl,charfile,read_fail,ef_tol)
        if ( read_fail ) then
         write(6,999) tmpname
        else
         call setEfTol(ef_tol)
        end if
!
       case( etol_tag )
! e_tol
        call one_param_section(nl,charfile,read_fail,e_tol)
        if ( read_fail ) then
         write(6,999) tmpname
        else
         call setEtol(e_tol)
        end if
!
       case( tctol_tag )
! tc_tol
        call one_param_section(nl,charfile,read_fail,tc_tol)
        if ( read_fail ) then
         write(6,999) tmpname
        else
         call setTcTol(tc_tol)
        end if
!
       case( qtol_tag )
! q_tol
        call one_param_section(nl,charfile,read_fail,q_tol)
        if ( read_fail ) then
         write(6,999) tmpname
        else
         call setQtol(q_tol)
        end if
!
       case( magnet_tag )
! ext_ex_ptnl (former "magnetic field section")
        call one_param_section(nl,charfile,read_fail,ext_ex_ptnl)
        if ( read_fail ) then
         write(6,999) tmpname
        else
         intfile%magnField = ext_ex_ptnl ! in Ry units
                                         !? OR ext_ex_ptnl = ext_ex_ptnl*amuBoT/mu0
        end if
!
       case( zpten_tag )
! zpten
        call one_param_section(nl,charfile,read_fail,zpE)
        if ( .not.read_fail ) call setZeroPt(nint(zpE))
!
       case( nuclmod_tag )
! nuclmod
        call one_param_section(nl,charfile,read_fail,nuclmod)
        if ( read_fail ) then
         write(6,999) tmpname
        else
         call setFiniteNucl(nint(nuclmod))
        end if
!
       case default
        return
      end select

!*******************************************************************************
!
      return
!EOC
      contains

      function get_k(n,x_c)
!
! counts number of entities divided by one or more spaces
!
      integer :: get_k
      character(*) x_c(*)
      integer, intent(in) :: n
      integer, allocatable :: mask(:)
      integer :: i,k
      get_k = 0
      if ( n<=0 ) return
      allocate(mask(0:n))
      mask(0) = 0
      do i=1,n
       if ( x_c(i) .ne. ' ' ) then
        if ( mask(i-1).eq.0 ) k=k+1
        mask(i) = 1
       else
        mask(i) = 0
       end if
      end do
      get_k = k
      deallocate(mask)
      return
      end function get_k
!
      subroutine one_param_section(nl,charfile,read_fail,val)
      implicit none
      integer, intent(inout) :: nl
      character(*), intent(in) :: charfile(:)
      logical, intent(out) :: read_fail
      real(8), intent(out) :: val
      read_fail = .true.
      nl = nl+1
      read(charfile(nl),*,end=10,err=10) val
      read_fail = .false.
10    continue
      return
      end subroutine one_param_section
!
      end subroutine read_new_sections

!c*******************************************************************

!BOP
!!IROUTINE: readFailed
!!INTERFACE:
      function readFailed(nl,nrec,charfile)
!!REVISION HISTORY:
! Initial Version - A.S. - 2013
!EOP
!
!BOC
      implicit none
      logical :: readFailed
      integer, intent(in) :: nl,nrec
      character(*), intent(in) :: charfile(nrec)
      integer i
      readFailed = .false.
      if ( nl.ne.nrec ) then
        if ( nl > nrec ) then
          write(6,'(/''ERROR: memory is corrupted in input_module,'',   &
     &     '' nl='',i4,'' > nrec='',i4/)') nl,nrec
         readFailed = .true.
        else
          write(6,'(/''WARNING: '',i3,'' records in input file were '', &
     &    ''ignored; file has '',i4,'' lines''/)') nrec-nl,nrec
          write(6,*) ' UNREAD LINES:'
          do i=nl+1,nrec
           write(6,*) charfile(i),'#'
          end do
          write(6,*) ' #END OF UNREAD LINES#'
        end if
      end if
      return
!EOC
      end function readFailed

!c*******************************************************************

!BOP
!!IROUTINE: read2intFile
!!INTERFACE:
      subroutine read2intFile(fname,intfile,ierr)
      use mpi
      implicit none
!!DESCRIPTION:
! to read file into run-input structure {\em infile} (type IniFile)
!
!!ARGUMENTS:
      character*(*) fname
      type(IniFile), intent(out) :: intfile
      integer, intent(out) :: ierr    ! ierr=0: normal exit, ierr=-1: no file or read permission
!!REVISION HISTORY:
! Initial Version - A.S. - 2013
!!REMARKS:
!  length of lines in input file is limited by 80 symbols
!EOP
!
!BOC
      integer n,ne
      integer, parameter :: nrec=80
      character(nrec), allocatable :: in_file(:)
      character(nrec) :: infile          ! ='.mecca_copy_of_input_file'
      integer :: intbuff(nrec),nl
      character(:), allocatable :: tmpln
      logical ext

      integer myrank, mpi_ierr, i

      call mpi_comm_rank(mpi_comm_world, myrank, mpi_ierr)

      infile='.mecca_copy_of_input_file'
      ierr = 0
      tmpln = trim(adjustl(fname))
      if ( myrank == 0 ) then
       call delete_file(infile)
       inquire (file=tmpln,EXIST=ext)
       if ( .not. ext ) then
         ierr = -1
         return
       end if
      end if

      n = 0
      if ( len(tmpln) <= nrec ) then
       infile = tmpln
      else
       if(myrank == 0) call cptxtfile(tmpln,infile)
      end if
      if(myrank == 0 ) then
        call readIniFile(infile,n)
      endif

      call mpi_bcast(n,1,MPI_INTEGER,0,mpi_comm_world,mpi_ierr)
      nl = len(infile)
      call char2int(infile,intbuff,nl)
      call mpi_bcast(intbuff(1),nl,MPI_INTEGER,0,mpi_comm_world,        &
     &                                                        mpi_ierr)
      call int2char(intbuff,infile,nl)

      if (n .gt. 0) then
        allocate(in_file(n+1))
        in_file(1) = infile
        ne = 1

        if(myrank == 0) then
          call readIniFile(in_file,ne)
        endif
        call mpi_bcast(ne,1,MPI_INTEGER,0,mpi_comm_world,mpi_ierr)
        do i=1,n+1 ! loop over lines in case there are alignment issues
          nl = len(in_file(i))
          call char2int(in_file(i),intbuff,nl)
          call mpi_bcast(intbuff(1),nl,MPI_INTEGER,0,mpi_comm_world,    &
     &       mpi_ierr)
          call int2char(intbuff,in_file(i),nl)
        enddo

        if (ne .gt. n) then
           write(*,*)                                                   &
     & ' ERROR in read2intFile :: not enough memory has been allocated',&
     & ' n=',n,'  ne=',ne
          ierr = 2
          return
        else if ( ne .le. 0 )  then
          write(*,*) ' read2intFile :: input error? '
          ierr = 3
          return
        end if

        call readInput(ne,in_file,intfile)
        deallocate(in_file)
      else if ( n==0 ) then
        ierr = 1
!         write(*,*)                                                     &
!     &  ' ERROR :: read-error for file '//trim(fname)
      else
        ierr = -2
         write(*,*)                                                     &
     & ' ERROR in read2intFile :: no access to file <'//trim(fname)//'>'
      end if
      return
!EOC
      end subroutine read2intFile

!c*******************************************************************

!BOP
!!IROUTINE: nunit2num
!!INTERFACE:
       function nunit2num( nu )
!!REVISION HISTORY:
! Initial Version - A.S. - 2013
!EOP
!
!BOC
       implicit none
       integer, intent(in) :: nu
       integer :: nunit2num
       integer :: i
       nunit2num = -1
       do i=1,size(num2nunit)
         if ( num2nunit(i) == nu ) then
           nunit2num = i
           exit
         end if
       end do
       return
!EOC
       end function nunit2num
!
!BOP
!!IROUTINE: fnum2nunit
!!INTERFACE:
       function fnum2nunit(i)
!!REVISION HISTORY:
! Initial Version - A.S. - 2013
!EOP
!
!BOC
       integer fnum2nunit
       integer, intent(in) :: i
       if ( i>0 .and. i<=size(num2nunit) ) then
        fnum2nunit = num2nunit(i)
       else
        fnum2nunit = 11
       end if
       return
!EOC
       end function fnum2nunit
!
!BOP
!!IROUTINE: initIOfiles
!!INTERFACE:
       subroutine initIOfiles(genName,numIOfiles,iofiles)
!!DESCRIPTION:
! initialization of {\em iofiles} structure (type IOFile);
! also fortran unit numbers used in {\sc mecca} and default prefixes
! (e.g. \#scflog, \#br1, etc.) are defined here
!
!!REVISION HISTORY:
! Initial Version - A.S. - 2013
!EOP
!
!BOC
       implicit none
       character*(*) genName
       integer, intent(out) :: numIOfiles
       type(IOFile) iofiles(*)
       integer i
       logical :: first = .true.

       if ( first ) then
         first = .false.
         do i=1,size(num2nunit)
          select case ( i )
          case(nu_xyz)       ! structure-file
            num2nunit(i) = 60
          case(nu_mix1)      ! mixing-file
            num2nunit(i) = 61
          case(nu_mix2)      ! mixing-file
            num2nunit(i) = 62
          case(nu_mdlng)     ! madelung-file
            num2nunit(i) = 64
          case(nu_print)     ! print-file
            num2nunit(i) = 6
          case(nu_info)      ! scflog-file
            num2nunit(i) = 63
          case(nu_inpot)     ! in-potential file
            num2nunit(i) = 22
          case(nu_outpot)    ! out-potential file
            num2nunit(i) = 23
          case(nu_spkpt)     ! special k-points-file
            num2nunit(i) = 65
          case(nu_dop)       ! dop-matrices file
            num2nunit(i) = 66
          case(nu_yymom)     ! YY-moments file
            num2nunit(i) = 67
          case(nu_dos)       ! DOS
            num2nunit(i) = 24
          case(nu_bsf)       ! BlochSpectrFun-output
            num2nunit(i) = 25
          case(nu_fs1)       ! FermiSurface, non-magn or spin-up
            num2nunit(i) = 26
          case(nu_fs2)       ! FermiSurface, spin-dn
            num2nunit(i) = 27
          case default
            num2nunit(i) = -1
          end select
         end do
       end if
       iofiles(1:size(num2nunit))%nunit = num2nunit
       numIOfiles = 0                        ! # of last defined unit
       do i=1,size(num2nunit)
        if ( iofiles(i)%nunit>0 ) then
         numIOfiles=numIOfiles+1
         iofiles(i)%form   = 'f'
         iofiles(i)%status = 'u'
        end if
       end do
       iofiles(nu_mix1)%status = 'r'
       iofiles(nu_mix1)%form = 'u'
       iofiles(nu_mix2)%status = 'r'
       iofiles(nu_mix2)%form = 'u'

       iofiles(nu_inpot)%name = trim(genName)//'.pot'
       call delete_file(iofiles(nu_inpot)%name)
       iofiles(nu_inpot)%status = 'n'

       iofiles(nu_print)%name = ' '
!       iofiles(nu_xyz)%name = 'str.xyz'
       iofiles(nu_mix1)%name = def_tag//'br1.'//trim(genName)
       call delete_file(iofiles(nu_mix1)%name)
       iofiles(nu_mix2)%name = def_tag//'br2.'//trim(genName)
       call delete_file(iofiles(nu_mix2)%name)
       iofiles(nu_outpot)%name = def_tag//'pot.'//trim(genName)
       iofiles(nu_info)%name = def_tag//'scflog.'//trim(genName)
       iofiles(nu_mdlng)%name = def_tag//'mdln.'//trim(genName)
       iofiles(nu_spkpt)%name = def_tag//'spkpt.'//trim(genName)
       iofiles(nu_dop)%name = def_tag//'dop'
       iofiles(nu_yymom)%name = def_tag//'yymom'
       iofiles(nu_dos)%name = trim(genName)//'.dos'

       return
!EOC
       end subroutine initIOfiles

!c*******************************************************************

!BOP
!!IROUTINE: printline
!!INTERFACE:
       subroutine printline(iunit,line,n)
!!REVISION HISTORY:
! Initial Version - A.S. - 2005
!EOP
!
!BOC
       implicit  none
       integer iunit
       character*(*) line
       integer n
       integer m,nmax
       parameter (nmax=110)
       if(n.ge.0) then
        m = min(n,nmax)
        write(iunit,'(a)') (def_tag//' ')//trim(line(1:m))
       end if
       return
!EOC
       end subroutine printline

!BOP
!!IROUTINE: stringToCmpnd
!!INTERFACE:
       subroutine stringToCmpnd(name,compound,nsubl)
!!DESCRIPTION:
! intializes {\tt compound} (type Sublattice) with
! {\tt nsubl} sublattices using an input compound name {\em name}
!
!!USES:
       USE mecca_types

!!DO_NOT_PRINT
       implicit none
!!DO_NOT_PRINT

!!ARGUMENTS:
       character*1 name(*)
       type ( Sublattice ), allocatable :: compound(:)
       integer, intent(inout) :: nsubl
!!REVISION HISTORY:
! Initial Version - A.S. - 2007
! sublattice array is allocated internally - A.S. - 2015
!EOP
!
!BOC
       integer numbs
       integer i, is, j, k, ncomp, nsmax
       logical inParenth
       character*2 symb
       real*8 z, conc, w
       character*80 line80

       logical  isParenthL, isParenthR, isLetter, isDigit
       external isParenthL, isParenthR, isLetter, isDigit
       integer idToZ
       external idToZ

       if ( allocated(compound) ) then
        nsmax = size(compound)
       else
        nsmax = 0
       end if

       DO is=1,2

       i = 1
       do while (name(i) .eq. ' ')
        i = i+1
       end do
       k = 0
       j = i
       do while ( name(j).ne.' ')
        if ( isDigit(name(j)) ) k = k+1
        j = j+1
       end do

!       if ( nsmax>0 ) then
!        if ( (j-i).eq.k .and. k.gt.0 .and. k.lt.4) then
!           write(line80,'(4a1)') name(i:j)
!           read(line80,*) k
!           if ( nsubl.le.nsmax) then
!            compound(1)%ns = 1
!            compound(1)%ncomp = ncomp
!            compound(1)%rIS = 0.d0
!            compound(1)%compon(1)%name = ''
!            call atomIDc(dble(k),                                       &
!     &                   symb,compound(1)%compon(1)%name)
!            compound(1)%compon(1)%concentr = 1.d0
!            compound(1)%compon(1)%zID = k
!            return
!           else
!            nsubl = 0
!            deallocate(compound)
!            nsmax = 0
!           end if
!        end if
!       end if

       nsubl = 0
       ncomp = 0
       inParenth = .FALSE.
       do while ( name(i) .ne. ' ' )
        if ( isLetter(name(i)) ) then
         z = idToZ(name(i:i+1))
         if ( z .lt. 0 ) then
          symb = ' '//name(i)
          z = idToZ(symb)
          if ( z .lt. 0 ) then    ! ERROR
           write(*,*) ' z=',z,' for ',name(i:i+1)
           nsubl = -1
           exit
          end if
          i = i-1
         end if
         if ( .NOT. inParenth ) then
          ncomp = 1
          nsubl = nsubl + 1
          if ( nsubl.le.nsmax) then
           compound(nsubl)%ncomp = ncomp
           compound(nsubl)%rIS = 0.d0
           compound(nsubl)%ns = 1
           compound(nsubl)%compon(ncomp)%name = ''
           call atomIDc(z,symb,compound(nsubl)%compon(ncomp)%name)
           compound(nsubl)%compon(ncomp)%concentr = 1.d0
           compound(nsubl)%compon(ncomp)%zID = z
          end if
         else
          ncomp = ncomp + 1
          if ( nsubl.le.nsmax) then
           compound(nsubl)%ncomp = ncomp
           compound(nsubl)%rIS = 0.d0
           compound(nsubl)%compon(ncomp)%name = ''
           call atomIDc(z,symb,compound(nsubl)%compon(ncomp)%name)
           if ( ncomp > 1 ) then
            if ( compound(nsubl)%compon(ncomp-1)%concentr == 0.d0)      &
     &         compound(nsubl)%compon(ncomp-1)%concentr = 1.d0
           end if
           compound(nsubl)%compon(ncomp)%concentr = 0.d0
           compound(nsubl)%compon(ncomp)%zID = z
          end if
         end if
         if( isLetter(name(i+1)) ) i = i+1
        else if ( isDigit(name(i)) ) then
         if (i .eq. 1) then
!c         ERROR
          write(*,*) ' ERROR: i.eq.1'
          nsubl = -1
          exit
         else if ( .not. isLetter(name(i-1))                            &
     &        .and.                                                     &
     &              (name(i-1).ne.')') ) then
!c         ERROR
          write(*,*) ' ERROR: not Letter',name(i-1:i)
          nsubl = -1
          exit
         end if
         k = 1
         do while ( isDigit(name(i+k) ) )
          k = k+1
         end do
         if ( name(i+k) .eq. '.' ) then
          if ( inParenth ) then
           k = k+1
           do while ( isDigit(name(i+k) ) )
            k = k+1
           end do
          else
!c          ERROR
           write(*,*) ' ERROR: not inParenth'
           nsubl = -1
           exit
          end if
         end if
         write(line80,*) ' ',name(i:i+k-1),' '
         if ( nsubl.le.nsmax) then
          if ( .not.inParenth) then
           read(line80,*) numbs
           compound(nsubl)%ns = numbs
          else if ( ncomp.ne.0 ) then
           read(line80,*) conc
           compound(nsubl)%compon(ncomp)%concentr = conc
          else
           write(*,*) ' ERROR: unexpected digits'
           nsubl = -1
           exit
          end if
         end if
         i = i+k
         cycle
        else if ( name(i) .eq. '(' ) then
         inParenth = .TRUE.
         nsubl = nsubl + 1
         if (nsubl.le.nsmax) compound(nsubl)%ns = 1
         ncomp = 0
         i = i+1
         cycle
        else if ( name(i) .eq. ')' ) then
         if (i .eq. 1) then
!c         ERROR
          nsubl = -1
          write(*,*) ' ERROR: wrong place for parenthR'
          exit
         else if ( .not. isLetter(name(i-1))                            &
     &        .and.                                                     &
     &              .not. isDigit(name(i-1)) ) then
!c         ERROR
          write(*,*) ' ERROR: not Letter, not Digit ',name(i-1:i)
          nsubl = -1
          exit
         end if
         if ( inParenth ) then
          if ( nsubl.le.nsmax) then
           if (compound(nsubl)%compon(ncomp)%concentr.eq.0.d0)          &
     &       compound(nsubl)%compon(ncomp)%concentr = 1.d0
           w = 0.d0
           do k=1,ncomp
            w = w+compound(nsubl)%compon(k)%concentr
           end do
           do k=1,ncomp
            compound(nsubl)%compon(k)%concentr =                        &
     &      compound(nsubl)%compon(k)%concentr / w
           end do
          end if
          inParenth = .FALSE.
          ncomp = 0
          i = i+1
          cycle
         else
!c         ERROR
          write(*,*) ' ERROR: incorrect parenth-s ',name(i-1:i)
          nsubl = -1
          exit
         end if
        else
!c          ERROR
          write(*,*) ' ERROR: wrong symbol ',name(i)
          nsubl = -1
         exit
        end if
        i = i+1
       end do

       if ( nsubl>0 .and. nsmax==0 ) then
        allocate(compound(nsubl))
        nsmax = nsubl
       else
        exit
       end if

       END DO

       return
!EOC
       end subroutine stringToCmpnd

       logical function isSublatAFMeqv(sublat1,sublat2)
       implicit none
       type(Sublattice), intent(in) :: sublat1,sublat2
       integer :: k

       isSublatAFMeqv = .false.
       if ( sublat1%ns .ne. sublat2%ns )       goto 10
       if ( sublat1%ncomp .ne. sublat2%ncomp ) goto 10
       do k=1,sublat1%ncomp
         if (sublat1%compon(k)%zID .ne.                                 &
     &             sublat2%compon(k)%zID) goto 10
         if (sublat1%compon(k)%concentr .ne.                            &
     &             sublat2%compon(k)%concentr) goto 10
       end do
       isSublatAFMeqv = .true.
10     continue
       return
       end function isSublatAFMeqv

       logical function isSublatAFM(sublat1,sublat2)
       implicit none
       type(Sublattice), intent(in) :: sublat1,sublat2
       integer :: nc
       isSublatAFM = .false.
       nc = sublat1%ncomp
       if ( isSublatAFMeqv(sublat1,sublat2) ) then
        if ( all ( (sublat1%compon(1:nc)%ex_ini_splt                    &
     &             +sublat2%compon(1:nc)%ex_ini_splt)==zero             &
     &           ) ) isSublatAFM = .true.
       end if
       return
       end function isSublatAFM

       subroutine checkForAFM(nMM,sublattices)
! check if all sublattices are appropriate for enforced AFM
       implicit none
       integer, intent(in) :: nMM
       type(Sublattice) :: sublattices(:)
       integer :: i,j,nc,nsubl
       if ( nMM==-1 ) then
        nsubl = size(sublattices)
        if ( mod(nsubl,2)==0 ) then
         do  i=1,nsubl/2
          if (.not. isSublatAFMeqv (sublattices(i),                     &
     &                                sublattices(i+nsubl/2)) ) then
           goto 200
          end if
         end do
         do  i=1,nsubl/2
           j = i+nsubl/2
           nc = sublattices(i)%ncomp
           sublattices(j)%compon(1:nc)%ex_ini_splt =                    &
     &                         -sublattices(i)%compon(1:nc)%ex_ini_splt
         end do
        end if
       end if
200    return
       end subroutine checkForAFM

!BOP
!!IROUTINE: defineKmesh
!!INTERFACE:
       subroutine defineKmesh( def_kset, nmesh, nKxyz )
!!DESCRIPTION:
! defines a set of {\tt k-meshes}: number of meshes {\tt nmesh}, respective
! grid-sizes {\tt nKxyz}
!

!!DO_NOT_PRINT
       implicit none
!!DO_NOT_PRINT

!!ARGUMENTS:
       character(len=*), intent(in) :: def_kset
       integer, intent(inout) :: nmesh
       integer, intent(inout) :: nKxyz(:,:)
!!REVISION HISTORY:
! Initial Version - A.S. - 2019
!EOP
!
!BOC
       logical :: two_grids
       logical, external :: isDigit
       integer :: i,i0,i1,j,ldef,l0,neg,nm
       integer, parameter :: c2=2,clim=30
       character(c2) :: def

       ldef = len_trim(def_kset)
       if ( ldef>clim ) then
        write(*,*) ' def_kset:',def_kset
        call fstop(' INCORRECT INPUT DEFINITION OF K-POINTS SET')
       else if (ldef<=0 ) then
        return
       end if
       l0 = len(def_kset)-ldef
       def = def_kset(l0+1:l0+2)
       if ( ldef.ne.2 .and. def.ne.'1:') then
         if ( isDigit(def_kset(l0+1:l0+1)) .or.                          &
     &                        def_kset(l0+1:l0+1)=='-') then
         def = '  '
        end if
       end if
       nm = nmesh
       two_grids = .false.
       select case(def)
       case('-1')
        nKxyz(:,1)   = abs(nKxyz(:,1))
        nKxyz(1,1)   = -nKxyz(1,1)
       case('-2')
        nKxyz(:,1)   = abs(nKxyz(:,1))
        nKxyz(1:2,1) = -nKxyz(1:2,1)
       case('-3')
        nKxyz(1:3,1)   = -abs(nKxyz(1:3,1))
       case default
        if ( def == '1:' ) then
         nm  = 1
         i0 = 3
        else if (isDigit(def_kset(l0+1:l0+1)) .or.                      &
     &                        def_kset(l0+1:l0+1)=='-') then
         if ( def=='  ' ) two_grids = .true.
         nm  = 1
         i0 = 1
        else
         nm = 0
         go to 10
        end if
        i0 = l0+i0
        i1 = i0
        do while (nm <= min(2,size(nKxyz,2)))
         j = 0
         neg = 0
         do i=i0,ldef
           if ( def_kset(i:i)=='-' .and. neg==0) then
             neg = 1
             cycle
           end if
           if  ( i==ldef ) then
             j = j+1
             if ( j.ne.3 .or. .not.isDigit(def_kset(i:i)) ) then
              write(6,*) ' j=',j
              go to 10
             else
              read(def_kset(i1:i),*,err=10) nKxyz(j,nm)
              i1 = i+1
             end if
             go to 1
           else
            if ( isDigit(def_kset(i:i)) ) cycle
            neg = 0
            if ( def_kset(i:i)==',' ) then
              j = j+1
              read(def_kset(i1:i-1),*,err=10) nKxyz(j,nm)
              i1 = i+1
             cycle
            else if ( def_kset(i:i)=='@' ) then
             if ( ldef-i < 7 .or. j.ne.2 ) then
              write(6,*) ' i=',i,' ldef=',ldef,' j=',j
              go to 10
             end if
             j = j+1
             read(def_kset(i1:i-1),*,err=10) nKxyz(j,nm)
             i1 = i+1
             write(def,'(i1,a1)') nm+1,':'
             if ( def == def_kset(i1:i1+1) ) then
              i1 = i+3
              nm = nm + 1
              if ( nm==2 ) two_grids = .true.
              exit
             else
              write(6,*) def//'.ne.'//def_kset(i1:i1+1)
              go to 10
             end if
            else
             go to 10
            end if
           end if
         end do
         i0 = i1
         if ( i0>ldef ) exit
        end do
       end select
1      continue
       if ( .not.two_grids ) then
        nKxyz(:,nm+1:) = 0
       else
        nm = nmesh
       end if
       nmesh = 0
       do i=1,min(nm,size(nKxyz,2))
        if ( all( nKxyz(:,i).ne.0 ) ) then
         nmesh = nmesh + 1
        else
         nm = i
         go to 10
        end if
       end do

!       deallocate(p_def)
       return
10     continue
       write(*,*) ' def_kset: <',def_kset,'>'
       write(*,*) ' check description for mesh ',nm
       call fstop(' INCORRECT INPUT DEFINITION OF K-POINTS SET')
       end subroutine defineKmesh

!c*******************************************************************

!DELETE!BOP
!DELETE!!IROUTINE: read2Input
!DELETE!!INTERFACE:
!DELETE      subroutine read2Input(nrec,charfile,intfile)
!DELETE      implicit none
!DELETE!!DESCRIPTION:
!DELETE! to initialize run-input structure {\em intfile} (of type IniFile)
!DELETE! from {\em charfile}, a character array.
!DELETE!
!DELETE!   Ames (simplified) format is assumed for {\em charfile}
!DELETE!
!DELETE!!ARGUMENTS:
!DELETE      integer, intent(in) :: nrec
!DELETE      character(80), intent(in) :: charfile(nrec)
!DELETE      type(IniFile), intent(out) :: intfile
!DELETE!!REVISION HISTORY:
!DELETE! Initial Version - A.S. - 2013
!DELETE!!REMARKS:
!DELETE! length of each line in charfile is 80 symbols
!DELETE!EOP
!DELETE!
!DELETE!BOC
!DELETE
!DELETE       interface
!DELETE        subroutine iostr_head(io,iflag,natom,alat,ba,ca)
!DELETE        integer, intent(in) :: io
!DELETE        integer, intent(in) :: iflag
!DELETE        integer, intent(out), optional :: natom
!DELETE        real(8), intent(out), optional :: alat
!DELETE        real(8), intent(out), optional :: ba
!DELETE        real(8), intent(out), optional :: ca
!DELETE        end subroutine iostr_head
!DELETE       end interface
!DELETE
!DELETE      type(IOFile)  :: io_tmp
!DELETE
!DELETE      character(3), parameter :: dlmtag  = 'dlm'
!DELETE      character(1) :: char1,char2
!DELETE      character*80 tmpname
!DELETE      integer i,j,k,multi_c,nl,n,nspin,nu,nat_,nrel_
!DELETE      integer ebflag
!DELETE      type(Element) :: chem_id(110)
!DELETE      real(8) :: zval,boa,coa,alat,par7,tc
!DELETE      integer :: dlm_pos
!DELETE
!DELETE      call setIniFile(intfile)
!DELETE
!DELETE      nl = 2
!DELETE      read(charfile(nl),'(a)') intfile%name
!DELETE      call true_name(intfile%name,'/',low=.true.)
!DELETE      do i=1,ipunit+1
!DELETE       read(charfile(nl+i),*,err=100) n
!DELETE       if ( n==0 ) exit
!DELETE      end do
!DELETE      if ( n .ne. 0 ) then
!DELETE          write(*,*) ' ERROR :: there are too many files in the input'
!DELETE          call fstop('read2Input')
!DELETE      end if
!DELETE      intfile%numIOfiles = i
!DELETE
!DELETE      do i=1,intfile%numIOfiles
!DELETE       nl = nl+1
!DELETE       read(charfile(nl),101) io_tmp%nunit                              &
!DELETE     &,                   io_tmp%status                                 &
!DELETE     &,                   io_tmp%form                                   &
!DELETE     &,                   io_tmp%name
!DELETE101    format(1x,i2,1x,2a1,1x,a)
!DELETE       j = nunit2num(io_tmp%nunit)
!DELETE       if ( j>0 ) then
!DELETE         intfile%io(j)%nunit = io_tmp%nunit     ! input nunit should be consistent with  default definitions
!DELETE         intfile%io(j)%status = io_tmp%status
!DELETE         intfile%io(j)%form = io_tmp%form
!DELETE         intfile%io(j)%name = io_tmp%name
!DELETE       end if
!DELETE      end do
!DELETE      nl = nl+1
!DELETE      nl = nl+1
!DELETE      read(charfile(nl),*) intfile%lmax,intfile%intgrsch
!DELETE      nl = nl+1
!DELETE      nl = nl+1
!DELETE      read(charfile(nl),*) intfile%mtasa,i,nspin,intfile%nrel
!DELETE      nrel_ = intfile%nrel
!DELETE      intfile%intgrsch = i
!DELETE      if ( nspin.gt.2 ) then
!DELETE        intfile%nMM = 1
!DELETE        intfile%nspin = 2
!DELETE      else
!DELETE        intfile%nMM = 0
!DELETE        intfile%nspin = nspin
!DELETE      end if
!DELETE      nl = nl+1
!DELETE      nl = nl+1
!DELETE      read(charfile(nl),*) intfile%nmesh
!DELETE      nl = nl+1
!DELETE      read(charfile(nl),*) intfile%nKxyz(1,1)                           &
!DELETE     &,                   intfile%nKxyz(2,1)                            &
!DELETE     &,                   intfile%nKxyz(3,1)
!DELETE      do i=2,intfile%nmesh
!DELETE        nl = nl+1
!DELETE        read(charfile(nl),*) intfile%nKxyz(1,i)                         &
!DELETE     &,                   intfile%nKxyz(2,i)                            &
!DELETE     &,                   intfile%nKxyz(3,i)
!DELETE        intfile%enKlim(1,i-1) = 0.d0
!DELETE        intfile%enKlim(2,i-1) = 0.25d0
!DELETE      end do
!DELETE!
!DELETE      intfile%nKxyz(1:3,2:) = abs(intfile%nKxyz(1:3,2:))  ! only 1st grid may have negative nKxyz
!DELETE!
!DELETE      intfile%nelements = 0
!DELETE      nl = nl+1
!DELETE      nl = nl+1
!DELETE      read(charfile(nl),*) intfile%nsubl,intfile%ncpa
!DELETE      multi_c = 0
!DELETE      nl = nl+1
!DELETE      if ( allocated(intfile%sublat) ) deallocate(intfile%sublat)
!DELETE      allocate(intfile%sublat(1:intfile%nsubl))
!DELETE      do  i=1,intfile%nsubl
!DELETE        nl = nl+1
!DELETE        read(charfile(nl),*) j                                          &
!DELETE     &,      intfile%sublat(i)%ncomp                                    &
!DELETE     &,      intfile%sublat(i)%rIS
!DELETE        if ( nspin>1 ) then
!DELETE          tmpname = charfile(nl)
!DELETE          call true_name(tmpname)
!DELETE          dlm_pos = index(tmpname,dlmtag)
!DELETE          if( dlm_pos > 0 ) then
!DELETE           write(6,'('' ERROR :: component-dependent DLM case is not '',&
!DELETE     &               ''implemented yet...  '')')
!DELETE           call fstop(' read2Input')
!DELETE           intfile%nMM = 1
!DELETE           intfile%sublat(i)%isDLM = .true.
!DELETE!           dlmstring = tmpname(dlm_pos+3:len(tmpname))
!DELETE!           read(dlmstring,*) dlm_mix
!DELETE!         icmpn = 2*icmpn
!DELETE          else
!DELETE           intfile%sublat(i)%isDLM = .false.
!DELETE          end if
!DELETE        end if
!DELETE        do k=1,intfile%sublat(i)%ncomp
!DELETE          nl = nl+1
!DELETE          read(charfile(nl),*) intfile%sublat(i)%compon(k)%zID          &
!DELETE     &,                        zval                                     &
!DELETE     &,               intfile%sublat(i)%compon(k)%concentr              !&
!DELETE!     &,               intfile%sublat(i)%compon(k)%name
!DELETE          if ( intfile%sublat(i)%compon(k)%zID > max_nonrel_z ) then
!DELETE            if ( nrel_ == dflt_nonrel ) intfile%nrel = dflt_rel
!DELETE          end if
!DELETE!          if (.not. any( intfile%elements(1:intfile%nelements)%ztot ==  &
!DELETE!     &                   intfile%sublat(i)%compon(k)%zID ) ) then
!DELETE          if (.not. any( chem_id(1:intfile%nelements)%ztot ==           &
!DELETE     &                   intfile%sublat(i)%compon(k)%zID ) ) then
!DELETE            intfile%nelements = intfile%nelements + 1
!DELETE            call initElement(intfile%sublat(i)%compon(k)%zID,           &
!DELETE     &                        chem_id(intfile%nelements))
!DELETE            chem_id(intfile%nelements)%zval = zval
!DELETE          end if
!DELETE          intfile%sublat(i)%compon(k)%zcor =                            &
!DELETE     &             intfile%sublat(i)%compon(k)%zID - nint(zval)
!DELETE          intfile%sublat(i)%compon(k)%name = ''
!DELETE          call atomIDc(dble(intfile%sublat(i)%compon(k)%zID),           &
!DELETE     &                 intfile%sublat(i)%compon(k)%name,tmpname)
!DELETE        end do
!DELETE        if ( intfile%nMM==1 ) then
!DELETE         do k=1,intfile%sublat(i)%ncomp
!DELETE          j = k + intfile%sublat(i)%ncomp
!DELETE          if ( j>size(intfile%sublat(i)%compon) ) then
!DELETE           write(6,'('' ERROR: Too many components on the same ''//     &
!DELETE     &                ''sublattice due to DLM mode'')')
!DELETE           call fstop(' read2Input')
!DELETE          end if
!DELETE          intfile%sublat(i)%compon(j) = intfile%sublat(i)%compon(k)
!DELETE         end do
!DELETE         intfile%sublat(i)%ncomp = intfile%sublat(i)%ncomp*2
!DELETE        end if
!DELETE        tc = sum(intfile%sublat(i)%compon(1:intfile%sublat(i)%ncomp)    &
!DELETE     &            %concentr)
!DELETE        intfile%sublat(i)%compon(1:intfile%sublat(i)%ncomp)%concentr =  &
!DELETE     &  intfile%sublat(i)%compon(1:intfile%sublat(i)%ncomp)%concentr/tc
!DELETE        multi_c = multi_c + (intfile%sublat(i)%ncomp-1)
!DELETE      end do
!DELETE!
!DELETE      do  i=1,intfile%nsubl
!DELETE         tc = sum(intfile%sublat(i)%compon(1:intfile%sublat(i)%ncomp)   &
!DELETE     &            %concentr)
!DELETE       if ( tc .ne. one ) then
!DELETE        if ( abs(tc - one) < intfile%sublat(i)%ncomp*1.d-5 ) then
!DELETE        intfile%sublat(i)%compon(1:intfile%sublat(i)%ncomp)%concentr =  &
!DELETE     &  intfile%sublat(i)%compon(1:intfile%sublat(i)%ncomp)%concentr/tc
!DELETE        else
!DELETE         write(6,*) ' Sum of concetrations for sublattice ',i,          &
!DELETE     &   ' is not equal to 1; sum(concentr)=',tc
!DELETE         call fstop('read2Input ERROR :: sum(concentrations) .NE. 1')
!DELETE        end if
!DELETE       end if
!DELETE      end do
!DELETE!
!DELETE      do j=1,intfile%nelements
!DELETE       do  i=1,intfile%nsubl
!DELETE        do k=1,intfile%sublat(i)%ncomp
!DELETE         if ( intfile%sublat(i)%compon(k)%zID == chem_id(j)%ztot ) then
!DELETE          intfile%elements(j) = chem_id(j)
!DELETE          exit
!DELETE         end if
!DELETE        end do
!DELETE        if ( k>intfile%sublat(i)%ncomp ) exit
!DELETE       end do
!DELETE      end do
!DELETE!
!DELETE      if ( multi_c>0 .and. intfile%ncpa==1 ) then ! CPA with default number of iterations
!DELETE        intfile%ncpa = maxcpa
!DELETE      end if
!DELETE      if ( intfile%nMM==1 ) then
!DELETE        if ( .not. any(intfile%sublat(1:intfile%nsubl)%isDLM) ) then
!DELETE          intfile%sublat(1:intfile%nsubl)%isDLM=.true.
!DELETE        end if
!DELETE      end if
!DELETE      intfile%igrid = 2
!DELETE      intfile%etop = zero
!DELETE      intfile%npar = 0
!DELETE      nl = nl+1
!DELETE      nl = nl+1
!DELETE      read(charfile(nl),*)                                              &
!DELETE     &      intfile%nscf                                                &
!DELETE     &,     intfile%npts                                                &
!DELETE     &,     intfile%alphmix                                             &
!DELETE     &,     intfile%betamix                                             &
!DELETE     &,     ebflag                                                      &
!DELETE     &,     intfile%ebot                                                &
!DELETE     &,     par7
!DELETE      if ( intfile%nspin == 1 ) intfile%betamix = zero
!DELETE
!DELETE!TODO: improve Tempr input?!
!DELETE      if ( intfile%npts > 1000 ) then
!DELETE!         intfile%Tempr = abs(intfile%eibot)/(pi*0.5d0)
!DELETE        intfile%Tempr = abs(par7)    ! Tempr in Ry
!DELETE        intfile%eibot = (pi*0.5d0)*intfile%Tempr
!DELETE        intfile%eitop = 0.003d0     !?????
!DELETE      else
!DELETE        intfile%Tempr = zero
!DELETE        intfile%eitop = par7
!DELETE        intfile%eibot = intfile%eitop
!DELETE      end if
!DELETE!
!DELETE      intfile%imixtype = 1
!DELETE      intfile%mixscheme = 1
!DELETE      call getGenName(intfile)
!DELETE
!DELETE      if ( intfile%alat == zero ) then
!DELETE        alat = zero
!DELETE        boa = one; coa = one
!DELETE        nu = intfile%io(nu_xyz)%nunit
!DELETE        open(nu,file=trim(intfile%io(nu_xyz)%name),err=110)
!DELETE        call iostr_head(nu,2,alat=alat,ba=boa,ca=coa,natom=nat_)
!DELETE        close(nu)
!DELETE110     if ( alat == zero ) then
!DELETE          write(6,'('' ERROR: lattice constant is undefined'')')
!DELETE          call fstop("input files may be corrupted")
!DELETE        end if
!DELETE        intfile%alat=alat
!DELETE        intfile%ba = boa
!DELETE        intfile%ca = coa
!DELETE      end if
!DELETE!
!DELETE      do while ( nl<nrec )
!DELETE       nl = nl+1
!DELETE       if ( len_trim(charfile(nl)) < 2 ) cycle
!DELETE       read(charfile(nl),'(2a1)') char1,char2
!DELETE       if ( char1 .ne. def_tag .or.  char2 .eq. ' ' ) cycle
!DELETE       call read_new_sections(nrec,charfile,nl,intfile)
!DELETE      end do
!DELETE!
!DELETE      if ( readFailed(nl,nrec,charfile) ) then
!DELETE          write(6,*) ' filename: ',intfile%name
!DELETE          call fstop('read2Input ERROR :: nl>nrec')
!DELETE      end if
!DELETE      return
!DELETE
!DELETE100   continue
!DELETE      return
!DELETE!EOC
!DELETE      end subroutine read2Input

!c*******************************************************************
      end module input
!c*******************************************************************

!BOP
!!ROUTINE: unitVolm
!!INTERFACE:
       real*8 function unitVolm(strFile,ba,ca,ql)
       implicit none
!!DESCRIPTION:
! calculation of unit volume for a structure described in a file;
! {\bv
! strFile - name of the file
! ba,ca   - b/a-, c/a-ratio
!  \ev}
!
!!ARGUMENTS:
       character*(*) strFile
       real(8), intent(out) :: ba,ca
       real(8) :: ql(3)
!
!!REVISION HISTORY:
! Initial Version - A.S. - 2005
! revised         - A.S. - 2015
!!REMARKS:
! real cell volume is {\it ba*ca*unitVolm};
! fortran unit 99 is used to read file
!EOP
!
!BOC
       integer natom,i
       real(8) :: ba_,ca_
       real(8) :: rslatt(3,3), kslatt(3,3)
       logical :: ext
       integer, parameter :: io=99
       real(8), external :: cellvolm

       unitVolm = 0.d0
       inquire (file=TRIM(strFile),EXIST=ext)
       if ( .not. ext ) return
       natom = 0
       open(io,file=TRIM(strFile),err=200)
       read(io,*,err=200,end=200) natom,ba_,ca_
       if ( ba==0.d0 ) ba = ba_
       if ( ca==0.d0 ) ca = ca_
       if(natom.le.0.or.ba.le.0.d0.or.ca.le.0.d0) return
       do i=1,3
        read(io,*,err=200,end=200) rslatt(1,i),rslatt(2,i),rslatt(3,i)
       end do
       close(io)
       rslatt(2,:) = rslatt(2,:)*ba
       rslatt(3,:) = rslatt(3,:)*ca
       unitVolm = abs(cellvolm(rslatt,kslatt,1))
       ql(1) = sqrt(dot_product(kslatt(1:3,1),kslatt(1:3,1)))
       ql(2) = sqrt(dot_product(kslatt(1:3,2),kslatt(1:3,2)))
       ql(3) = sqrt(dot_product(kslatt(1:3,3),kslatt(1:3,3)))
200    return
!EOC
       end function unitVolm

!c*******************************************************************
      subroutine true_name(line,symb,low)
      implicit none
      character(*), intent(inout) :: line
      character(1), intent(in), optional :: symb
      logical, intent(in), optional :: low
      character(1), external :: toLower
      character(80) :: tmpname
      integer :: pos,i,j
!
      tmpname = trim(adjustl(line))
      if ( present(symb) ) then
       pos = index(tmpname,symb)
      else
       pos = 0
      end if
      do j=pos+1,len(tmpname)
       i = j-pos
       line(i:i) = tmpname(j:j)
      end do
      line(i+1:len(tmpname)) = ''
      if ( present(low) ) then
       if ( low ) then
        do i=1,len(line)
         line(i:i) = toLower(line(i:i))
        end do
       end if
      end if
!
      return
      end subroutine true_name
