!BOP
!!ROUTINE: derv5
!!INTERFACE:
      subroutine derv5(y,dy,x,nn)
!!DESCRIPTION:
! wrapper to calculate derivative {\tt dy} of function {\tt y}=f({\tt x})
!
!!USES:
      use polyintrpl, only : derv_expmesh 
!
!!REVISION HISTORY:
! Initial Version - A.S. - 2013
!!REMARKS:
! current version assumes exponential mesh
!EOP
!
!BOC
      implicit none
!c
      integer nn
      real*8 x(nn)
      real*8 y(nn)
      real*8 dy(nn)
      if ( nn>=5 ) then
       call derv_expmesh(y,dy,x,nn)
      else
       call dy_expmesh(y,dy,x,nn)
      end if
      return
!EOC
      end subroutine derv5

!BOP
!!ROUTINE: derv_expmesh
!!INTERFACE:
      subroutine dy_expmesh(y,dy,r,nn)
!!DESCRIPTION:
!   dy is d(y)/d(r); it is assumed that r(i) = exp(x0+h*i)      
!
!!REVISION HISTORY:
! Initial Version - A.S. - 2013
!!REMARKS:
! 5-point scheme
!EOP
!
!BOC
      implicit none
      real(8), intent(in) :: y(nn)
      real(8), intent(out) :: dy(nn)
      real(8), intent(in) :: r(nn)   
      integer, intent(in) :: nn
!      
      real(8), parameter :: a1(5)=[-1.5d0,2.d0,-0.5d0,0.d0,0.d0]
      real(8), parameter :: a2(5)=                                      &
     &                [-2.d0/6.d0,-0.5d0,1.d0,-1.d0/6.d0,0.d0]
      real(8), parameter :: c(5)=                                       &
     &                [1.d0/12.d0,-2.d0/3.d0,0.d0,2.d0/3.d0,-1.d0/12.d0]
      real(8), parameter :: b2(5)=[0.d0,1.d0/6.d0,-1.d0,0.5d0,2.d0/6.d0]
      real(8), parameter :: b1(5)=[0.d0,0.d0,0.5d0,-2.d0,1.5d0]
      integer :: i
      real(8) :: h=0
      if ( nn>=5 ) then
       dy(1)    = dot_product(a1(1:3),y(1:3))
       dy(nn)   = dot_product(b1(3:5),y(nn-2:nn))
       dy(2)    = dot_product(a2(1:4),y(1:4))
       dy(nn-1) = dot_product(b2(2:5),y(nn-3:nn))
       do i=3,nn-2
        dy(i) = dot_product(c(1:5),y(i-2:i+2))
       end do
      else
       select case (nn)
        case(4)
         dy(1)   = dot_product(a1(1:3),y(1:3))
         dy(nn)  = dot_product(b1(3:5),y(nn-2:nn))
         dy(2)   = dot_product(a2(1:4),y(1:4))
         dy(nn-1)= dot_product(b2(2:5),y(nn-3:nn))
        case(3)
         dy(1) = dot_product(a1(1:3),y(1:3))
         dy(2) = 0.5d0*(y(3)-y(1))
         dy(nn)= dot_product(b1(3:5),y(nn-2:nn))
        case(2)
         dy = y(2)-y(1)
        case default 
         if ( nn==1 ) dy(1)=0
         return
       end select
      end if
      h = log(r(2)/r(1))
      dy = dy/(h*r)
      return
!EOC
      end subroutine dy_expmesh
!c
!BOP
!!ROUTINE: derv3expmesh
!!INTERFACE:
      subroutine derv3expmesh(y,dy,r,nn)
!!DESCRIPTION:
!  dy is d(y)/d(r); it is assumed that r(i) = exp(x0+h*i)      
!
!!REVISION HISTORY:
! Initial Version - A.S. - 2013
!!REMARKS:
! 3-point scheme
!EOP
!
!BOC
      implicit none
      real(8), intent(in) :: y(nn)
      real(8), intent(out) :: dy(nn)
      real(8), intent(in) :: r(nn)   
      integer, intent(in) :: nn
!      
      integer :: i
      real(8) :: h=0

      if ( nn>=2 ) then
       dy(1)    = (y(2)-y(1))*2
       dy(nn)   = (y(nn)-y(nn-1))*2
       do i=2,nn-1
        dy(i) = y(i+1)-y(i-1)
       end do
       h = log(r(2)/r(1))
       dy = dy/((2.d0*h)*r) 
!DEBUG
!       do i=1,nn
!        dy(i) = dy(i)*0.5
!       end do 
!DEBUG
      else
       if ( nn==1 ) dy(1)=0
      end if
      return
!EOC
      end subroutine derv3expmesh
!
!BOP
!!ROUTINE: derv5_VP
!!INTERFACE:
      subroutine derv5_VP(y,dy,x,nstart,nn)
!!DESCRIPTION:
!  dy is d(y)/d(r); no assumption about mesh {\tt x}
!
!!REMARKS:
! no check if 0 < nstart < nn
!EOP
!
!BOC
      implicit real*8 (a-h,o-z)
!c
      integer, intent(in) :: nstart,nn
      real*8, intent(in) :: x(nn)
      real*8, intent(in) :: y(nn)
      real*8, intent(out) :: dy(nn)
      real*8 :: a,b,c,d,e
      integer :: i,i1
      do i=nstart,nn
         i1=max0(i-2,1)
         i1=min0(i1,nn-4)
         a=y(i1)
         b=(y(i1+1)-a)/(x(i1+1)-x(i1))
         c=(y(i1+2)-a)/(x(i1+2)-x(i1))
         c=(c-b)/(x(i1+2)-x(i1+1))
         d=(y(i1+3)-a)/(x(i1+3)-x(i1))
         d=(d-b)/(x(i1+3)-x(i1+1))
         d=(d-c)/(x(i1+3)-x(i1+2))
         e=(y(i1+4)-a)/(x(i1+4)-x(i1))
         e=(e-b)/(x(i1+4)-x(i1+1))
         e=(e-c)/(x(i1+4)-x(i1+2))
         e=(e-d)/(x(i1+4)-x(i1+3))
         dy(i)=b+c*( (x(i)-x(i1)) + (x(i)-x(i1+1)) )                    &
     &        + d*(  (x(i)-x(i1))*(x(i)-x(i1+1))                        &
     &             + (x(i)-x(i1+1))*(x(i)-x(i1+2))                      &
     &             + (x(i)-x(i1+2))*(x(i)-x(i1))      )                 &
     &      +e*( (x(i)-x(i1))*(x(i)-x(i1+1))*(x(i)-x(i1+2))             &
     &      +  (x(i)-x(i1+3))*(x(i)-x(i1+1))*(x(i)-x(i1+2))             &
     &      +  (x(i)-x(i1))*(x(i)-x(i1+1))*(x(i)-x(i1+3))               &
     &      +  (x(i)-x(i1))*(x(i)-x(i1+2))*(x(i)-x(i1+3)) )
      enddo
!c     dy(1)=dy(2)
      return
!EOC
      end subroutine derv5_VP
