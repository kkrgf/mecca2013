!BOP
!!MODULE: coresolver
!!INTERFACE:
      module coresolver
!!DESCRIPTION:
! core-electrons eigenstate solver
! (Dirac equation)

!!USES:
      use mecca_constants

!!DO_NOT_PRINT
      implicit none
      private
!!DO_NOT_PRINT

!!PUBLIC MEMBER FUNCTIONS:
      public deepst,deepst_sr,deepst_ha, outws2, outws2b, outws3

!!PRIVATE MEMBER FUNCTIONS:
! subroutine inws
! subroutine outws
! subroutine invals1
! function invPoint
! subroutine deriv_dirac
! subroutine jacob1
! subroutine dirac_matrix
! subroutine set_dirac_params
! subroutine zinterp
!!REVISION HISTORY:
! Adapted - A.S. - 2013
!EOP
!
!BOC
      type, public :: outws3_results
         real(8) :: r                      = 0.0d0
         real(8) :: c                      = 0.0d0
         real(8) :: rg                     = 0.0d0
         real(8) :: rf                     = 0.0d0
         real(8) :: logarithmic_derivative = 0.0d0
         real(8) :: potential_function     = 0.0d0
         real(8) :: logarithmic_derivative_xero_function = 0.0d0
         real(8) :: band_center_function   = 0.0d0
         integer :: number_of_nodes        = 0
         integer :: kappa                  = 0
         integer :: l                      = 0
      end type outws3_results
!      real(8), parameter :: m_e = one/two   ! electron (rest) mass
      real(8), parameter :: ode_err=1.d-9
      real(8), parameter :: cmax_nonrel=1.d+20
      integer, parameter :: debug_print=-1
      real(8), parameter :: r_zero = 1.d-6
      real(8), parameter :: log_tiny = log(epsilon(dble(1))/4.d0)
!
      real(8) :: c_,dk_,en_
!
      integer :: ipot(2)
      real(8) :: rfit(2),zfit(2)
!EOC
      contains
!c
      subroutine deepst_ha(nqn,lqn,kqn,en,rv,r,x,rg,rf,h,z,c,            &
     &       nitmax,tol,nmt,nws,nlast,iter,ifail)

!!DO_NOT_PRINT
      implicit none
!!DO_NOT_PRINT
!
!!ARGUMENTS:
      integer, intent(in) :: nqn,lqn,kqn
      real(8), intent(inout) :: en
      real(8), intent(in) :: rv(:),r(:),x(:)
      real(8), intent(out) :: rg(:),rf(:)
      real(8), intent(in) :: h,z,c
      integer, intent(in) :: nitmax
      real(8), intent(in) :: tol
      integer, intent(in) :: nmt,nws,nlast
      integer, intent(out) :: iter,ifail
!!REVISION HISTORY:
! Initial version - A.S. - 2016

      integer, parameter :: m = 4
      real(8), allocatable :: rv_ha(:),r_ha(:),x_ha(:),rf_ha(:),rg_ha(:)
      real(8) :: h_ha,xs
      integer :: nmt_ha,nws_ha,nlast_ha
      integer :: i,nr
      real(8), external :: spl3
      real(8) :: rx(4)
      real(8), allocatable :: zspl(:),fn(:)

      if ( kqn == 0 ) then
       en = zero
       rg = zero
       rf = zero
       iter = 0
       ifail = 0
       return
      end if

      nmt_ha = nmt * m
      nws_ha = nws * m
      nlast_ha = nlast * m
      h_ha = h / m

      nr = size(r)
      allocate(rv_ha(1:1+m*nr))
      allocate(r_ha(1:1+m*nr))
      allocate(x_ha(1:1+m*nr))
      allocate(rf_ha(1:1+m*nr))
      allocate(rg_ha(1:1+m*nr))

      xs = log(r(1))-h
      do i=1,size(r_ha)
       x_ha(i) = xs + (i-1)*h_ha
      end do
      r_ha = exp(x_ha)

      allocate(zspl(nr),fn(nr))
      fn(1:nr) = rv(1:nr) + 2*z
      call zspl3(nr,x,fn,zspl)
      do i=m+1,size(r_ha)
       rv_ha(i) = spl3(nr,x,fn,zspl,x_ha(i)) - 2*z
      end do
      rx(1) = zero
      rx(2:4) = r(1:3)
      fn(1) = zero
      fn(2:4) = rv(1:3) + 2*z
      call zspl3(4,rx,fn(1:4),zspl(1:4))
      do i=1,m
       rv_ha(i) = spl3(4,rx,fn(1:4),zspl(1:4),r_ha(i)) - 2*z
      end do
      deallocate(fn,zspl)

      call deepst(nqn,lqn,kqn,en,rv_ha,r_ha,x_ha,rg_ha,rf_ha,h_ha,z,c,   &
     &       nitmax,tol,nmt_ha,nws_ha,nlast_ha,iter,ifail)
      do i=1,nr
       rg(i) = rg_ha(1+i*m)
      end do
      do i=1,nr
       rf(i) = rf_ha(1+i*m)
      end do
      deallocate(rv_ha,r_ha,x_ha,rf_ha,rg_ha)
      return
      end subroutine deepst_ha
!c =====================================================================
!BOP
!!IROUTINE: deepst
!!INTERFACE:
      subroutine deepst(nqn,lqn,kqn,en,rv,r,x,rg,rf,h,z,c,              &
     &       nitmax,tol,nmt,nws,nlast,iter,ifail)
!!DESCRIPTION:
! core states solver
! {\bv
! ....bg...june 1990.....
! energy eigenvalues are found by newton-raphson method matching at
! the classical inversion point the f/g ratio. the energy derivative
! of f/g is evaluated analitically: see rose's book, chapter 4, pages
! 163-169, and also jp desclaux code...................................
!
! calls: outws(invals) and inws
!
! variables explanation:
! nqn principal quantum number; lqn orbital quantum number.............
! kqn kappa quantum number; en energy; rv potential in rydbergs times r
! r radial log. grid; rg big component: rf small component.............
! h: exp. step; z atomic number; nitmax number of iterations...........
! tol: energy tolerance; nmt muffin-tin radius index...................
! nws: last tabulation point; c speed of light in rydbergs.............
! gam: first power for small r expansion; slp:slope sign at the origin.
! \ev}

!!DO_NOT_PRINT
      implicit none
!!DO_NOT_PRINT
!
!!ARGUMENTS:
      integer, intent(in) :: nqn,lqn,kqn
      real(8), intent(inout) :: en
      real(8), intent(in) :: rv(:),r(:),x(:)
      real(8), intent(out) :: rg(:),rf(:)
      real(8), intent(in) :: h,z,c
      integer, intent(in) :: nitmax
      real(8), intent(in) :: tol
      integer, intent(in) :: nmt,nws,nlast
      integer, intent(out) :: iter,ifail
!!REVISION HISTORY:
! Adapted - A.S. - 2013
!!REMARKS:
! LOW ACCURACY/RESOLUTION IS USED FOR LEVELS CLOSE TO ZERO
!EOP
!
!BOC
!
      character sname*10,coerr*80
      integer ipdeq,ipdeq2
      integer invp
!c ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
      parameter (ipdeq=5,ipdeq2=2*ipdeq)
      parameter  (sname='deepst')
!c ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!c
      real(8) :: tmp(size(r)),qmp(size(r))
      integer :: imm,nout,nodes,lll,j,nmax
      real(8) :: dk,gam,slp,rfm,rgm,fnrm,rose,de,val,gnrm1,vr0
      real(8) :: eold,enew,elim,abs_err
      real(8), parameter :: eclose_to_zero = -0.05d0
      integer :: icheck
      real(8) :: emin,emax

!      dimension drg(ipdeq2),drf(ipdeq2)

      if ( kqn == 0 ) then
       en = zero
       rg = zero
       rf = zero
       iter = 0
       ifail = 0
       return
      end if

!c **********************************************************************
!c     initialize quantities
!c **********************************************************************
!
      emin=0; emax=0
      imm=0
      iter=0
      dk=dble(kqn)
      ifail=0
      if ( nlast>size(r) ) then
        write(6,*) ' nlast=',nlast,' size(r)=',size(r)
        call p_fstop(sname//': unexpected input, nlast>size(r)')
      end if
      nmax = size(r)
      nout = nmt+1
      rg = zero
      rf = zero
!c
!c     no. of nodes for the current states big component
!c **********************************************************************
      nodes=nqn-lqn
!c
!c     first power of small r expansion
!c **********************************************************************
!      gam=sqrt(dk*dk-4.d0*z*z/(c*c))
      call twoZ(z,zero,1,vr0)

      if ( vr0.ne.zero ) then
       gam = sqrt(dk**2-(vr0/c)**2)
      else
       gam = abs(dk)
      end if
!c
!c     slope of the wavefcns at the origin
!c **********************************************************************
      if(nodes-2*(nodes/2).ne.0) then
        slp = kqn/abs(kqn)
      else
        slp = -kqn/abs(kqn)
      endif

      if ( c>cmax_nonrel ) then
       if ( kqn>0 ) then
        dk = -lqn-1
        slp = -slp
       end if
       gam = abs(dk)
      end if
!c
!c     find the lowest energy limit
!c **********************************************************************
      lll=(lqn*(lqn+1))/2
      elim=-2*z*z/(0.25d0*nqn*nqn+1.d0)
!c
!c     check potential
!c **********************************************************************
      if(elim.ge.0) then
        coerr='   v+l*(l+1)/r**2 always positive'
        if ( debug_print .ge. 0 ) then
         write(6,*) sname//'::'//coerr, ' l=',lqn,' n=',nqn,' k=',kqn
        end if
        en = zero
        ifail = 1
        return
!CAB    call p_fstop(sname//':'//coerr)
      endif
!c **********************************************************************
      invp = nmt
      if(en.le.elim.or.en.ge.zero) then
       if(nitmax.gt.1) then
        en=elim*0.75d0
       else
        ifail = -1    !
        coerr = sname//':: WARNING '
        if ( en <= elim ) then
         write(6,'(a,'' en='',e16.7,'' <= elim='',e16.7)')              &
     &                                             trim(coerr),en,elim
        else
         if ( debug_print .ge. 0 ) then
          write(6,'(a,'' en='',e16.7,'' >= 0'')') trim(coerr),en
         end if
        end if
        return
       end if
      end if
      icheck = 0
  2   continue
      if ( iter==0 ) then
        emin = en
        emax = en
      else
       if ( icheck == 0 ) then
        if ( en>emin .and. en<emax ) then
          icheck = 1
        else
          emin = min(emin,en)
          emax = max(emax,en)
        end if
       else
        if ( en > eold ) then
           emin=eold
        else
           emax = eold
        end if
        if ( en<emin .or. en > emax ) then
          en = (emin+emax)/2
        end if
       end if
      end if
       eold = en
!      if(iter.gt.0) then
      if ( imm == 0 ) then
       nout = min(nws+1,nmax)
       invp = invPoint(ipdeq,lll,nout,r,rv,en)
      end if
      call outws(invp,rg,rf,rv,r,en,c,elim,z,                           &
     &                     gam,slp,tol,imm,lll,dk,nodes,nout)
!      call outws_sr(invp,rg,rf,rv,r,en,c,elim,z,                        &
!     &                     tol,imm,lqn,nodes,nout)

      if (en.ge.zero) then
       ifail = 5
       return
      end if
!      end if

      tmp(1:invp) = rg(1:invp)**2
      if ( c<=cmax_nonrel ) then
       tmp(1:invp) = tmp(1:invp) + rf(1:invp)**2
      end if
      call qexpup(1,tmp,invp,x,qmp)
      rose=qmp(invp) + r(1)*tmp(1)/(gam+gam+one)

!c **********************************************************************
!c
!c     store the inversion point values
!c **********************************************************************
      rfm=rf(invp)
      rgm=rg(invp)
!c
!c     routine inws performs the inward integration
!c **********************************************************************
!??????      nmax = nout

      call inws (invp,nmax,rg,rf,rv,r,en,c,dk,imm)
!      call inws_sr (invp,nmax,rg,rf,rv,r,en,c,lqn,imm)
!c
!c components match
!c **********************************************************************
      fnrm=rgm/rg(invp)
!c
!c check first the sign of the big component
!c **********************************************************************
      if (fnrm.lt.zero) then
         ! coerr='wrong big component change'
         en = zero
         ifail = 2
         return
      endif
!c **********************************************************************
      rg(invp:nmax) = rg(invp:nmax)*fnrm
      rf(invp:nmax) = rf(invp:nmax)*fnrm
!c
!c energy derivative of the wvfcns "log. derivative"
!c **********************************************************************
      tmp(invp:nmax) = rg(invp:nmax)**2
      if ( c<=cmax_nonrel ) then
       tmp(invp:nmax) = tmp(invp:nmax) + rf(invp:nmax)**2
      end if
      call qexpup(1,tmp(invp:nmax),nmax-invp+1,x(invp:nmax),            &
     &                                         qmp(invp:nmax))
      rose=rose+qmp(nmax)+h*r(invp)*(rfm*rfm-rf(invp)*rf(invp))/3.d0
!c
!c energy update
!c **********************************************************************
      de=rg(invp)*(rfm-rf(invp))*c/rose
      if (nitmax.le.1) then
       val = zero
      else
       val=abs(de/en)
      end if
      enew = en+de
      if (iter>1.and.enew<zero.and.(val.le.1.d-02.or.abs(de)<1.d-02))   &
     & then
        imm=1
      else
        imm=0
      end if
      if ( val>0.4d0 ) then
       if ( enew>zero ) then
        imm=0
        if ( eold<=en+0.01d0*abs(eold) ) then
         enew = min(0.9d0*en,en+1.d0)
        else
         enew = max(1.2d0*eold,eold-2.d0)
        end if
       else if ( val>1.d0) then
        enew = max(1.2d0*en,en-2.d0)
       else
        enew = max(enew,eold-5.d0)
       end if
      end if
!       write(6,'(a,i3,2g18.9,2x,2g12.4)') 'DEBUG_DC1:',iter,            &
!     &    eold,en,de,abs(en-eold)
      abs_err = max(abs(de),abs(enew-eold))
      if ( abs_err<etol/abs(5*kqn) .or. nitmax==0 ) go to 7        ! normal exit

      if ( (max(abs(en),abs(eold))<=abs(eclose_to_zero/5)               &
     &                                     .and.en+de>zero) ) then
        enew = -0.5d0*tol    ! eignevalue, if exists, is not a core state
        ifail = 3
        go to 7
      end if

      if ( abs(de)<2*epsilon(de) ) then
        ifail = 4
        if ( val>10*tol ) then
         write(6,('(a,g12.4,a,g12.4,a,g18.10,3i3,a,i3)'))               &
     &                  sname//' WARNING: no convergence, val=',val,    &
     &            '  > tol=',tol,' en=',en,nqn,lqn,kqn,' iter=',iter
        end if
        go to 7
      end if

      if ( enew>zero ) then
       enew = eold*0.5d0
       imm = 0
       if (eold .gt. eclose_to_zero) then
        ! coerr=' zero energy'
        ifail = 5
        en = zero
        return
       end if
      end if
!c **********************************************************************
!c
!c     no convergence yet: try again
!c **********************************************************************
      en = enew
      iter=iter+1
      if ( iter.le.nitmax) go to 2

      ifail = 1 + nitmax
!c
!c     not converged, too small tolerance or too small nitmax
!c **********************************************************************
      if (en.lt.-0.2d0) then
       write(6,'(a,i3,a,g12.4,a,g12.4,a,g20.10,a,g12.4,a,g12.4)')       &
     &                               sname//': iter=',iter,             &
     &           ' WARNING: too many energy iterations, rel_err='       &
     &           ,val, ', tol=',tol,' en=',en                           &
     &          ,' abs_err=',abs_err,' ec_tol=',etol/abs(5*kqn)
      end if
!c **********************************************************************
!c
!c     got convergence: exit
!c     normalize the wavefunctions
!c **********************************************************************
  7   continue

      if ( nitmax > 1 ) then
       if ( enew > eclose_to_zero ) then
        en = zero
       else
        en = enew
       end if
      end if

!c
!c wave-fct. normalization is required in the whole space (and not only within sphere for ASA case)
!      if ( invp<nmt/2 ) then
!       nmax2 = min(nmax,nws)
!      end if
!c
      if ( en<zero ) then
       call qexpup(1,tmp,nmax,x,qmp)
       rose = qmp(nmax) + r(1)*tmp(1)/(gam+gam+one)
       rose = rose + h*r(invp)*(rfm*rfm-rf(invp)*rf(invp))/3.d0
!
       gnrm1 = rose
!

!       if ( invp>10.9d0*nmt ) then   ! integration of Riccati-Hankel tails
!!DEBUG_SR       if ( invp>0.9d0*nmt ) then   ! integration of Riccati-Hankel tails
!!        pr = two*sqrt(abs(en*(one+en/(c*c))))*r(nmax)
!        Rbig = r(nmax)+prmax/sqrt(abs(en*(one+en/(c*c))))
!        mx = min(nBig,size(r))
!        xh = min(0.01d0,log(Rbig/r(nmax))/(mx-1))
!        xr(1) = log(r(nmax))
!        do j=2,mx
!         xr(j) = xr(1) + (j-1)*xh
!        end do
!        xtmp = exp(xr)
!        call inwhnk (1,xg,xf,dxg,dxf,xtmp(1:mx),en,c,dk)
!        fnrm=rg(nmax)/xg(1)
!        xg(1:mx) = xg(1:mx)*fnrm
!        xtmp = xg**2
!        if ( c<=cmax_nonrel ) then
!         xf(1:mx) = xf(1:mx)*fnrm
!         xtmp = xtmp + xf**2
!        end if
!        call qexpup(1,xtmp,mx,xr,xqmp)
!        rose=rose+(xqmp(mx)-xqmp(1))
!!
!       write(6,*) ' en=',sngl(en),' RENORM2d=',sqrt(rose/gnrm1)
!!
!       end if

       gnrm1 = one/sqrt(rose)
       rg(1:nmax) = rg(1:nmax)*gnrm1
       rf(1:nmax) = rf(1:nmax)*gnrm1
       rg(nmax+1:) = zero
       rf(nmax+1:) = zero
      else
       rg(:) = zero
       rf(:) = zero
      end if
!
      return
!EOC
      end subroutine deepst
!c =====================================================================
!BOP
!!IROUTINE: deepst_sr
!!INTERFACE:
      subroutine deepst_sr(nqn,lqn,kqn,en,rv,r,x,rg,rf,h,z,c,           &
     &       nitmax,tol,nmt,nws,nlast,iter,ifail)
!!DESCRIPTION:
! scalar-relativistic core states solver
! {\bv
! variables explanation:
! nqn principal quantum number; lqn orbital quantum number.............
! kqn kappa quantum number; en energy; rv potential in rydbergs times r
! r radial log. grid; rg big component: rf small component.............
! h: exp. step; z atomic number; nitmax number of iterations...........
! tol: energy tolerance; nmt muffin-tin radius index...................
! nws: last tabulation point; c speed of light in rydbergs.............
! gam: first power for small r expansion; slp: slope sign at the origin
! \ev}

!!DO_NOT_PRINT
      implicit none
!!DO_NOT_PRINT
!
!!ARGUMENTS:
      integer, intent(in) :: nqn,lqn,kqn
      real(8), intent(inout) :: en
      real(8), intent(in) :: rv(:),r(:),x(:)
      real(8), intent(out) :: rg(:),rf(:)
      real(8), intent(in) :: h,z,c
      integer, intent(in) :: nitmax
      real(8), intent(in) :: tol
      integer, intent(in) :: nmt,nws,nlast
      integer, intent(out) :: iter,ifail
!!REVISION HISTORY:
! Created - A.S. - 2015
!!REMARKS:
!
!EOP
!
!BOC
!
      character sname*10,coerr*80
      integer ipdeq,ipdeq2
      integer invp
!c ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
      parameter (ipdeq=5,ipdeq2=2*ipdeq)
      parameter  (sname='deepst_sr')
!c ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!c
      real(8) :: tmp(size(r)),qmp(size(r)),tmp1(size(r))
      integer :: imm,nout,nodes,lll,j,nmax,jt
      real(8) :: cinv,mass
      real(8) :: dlt,rfm,rgm,fnrm,rose,de,val,gnrm1
      real(8) :: eold,enew,elim,abs_err
      real(8), parameter :: eclose_to_zero = -0.05d0
      integer :: icheck
      real(8) :: emin,emax
      real(8), parameter :: half = one/two
      real(8) :: fz2oc2,power

!      dimension drg(ipdeq2),drf(ipdeq2)

      if ( kqn == 0 ) then
       en = zero
       rg = zero
       rf = zero
       iter = 0
       ifail = 0
       return
      end if

!c **********************************************************************
!c     initialize quantities
!c **********************************************************************
!
      if ( nlast>size(r) ) then
        write(6,*) ' nlast=',nlast,' size(r)=',size(r)
        call p_fstop(sname//': unexpected input, nlast>size(r)')
      end if
      emin=0; emax=0
      cinv = one/c
      imm=0
      iter=0
      ifail=0
      nout = nmt+1
      rg = zero
      rf = zero
!c
!c     no. of nodes for the current states big component
!c **********************************************************************
      nodes=nqn-lqn
!c
!c     find the lowest energy limit
!c **********************************************************************
      lll=lqn*(lqn+1)
      if ( lqn>0 ) then
       elim=-z*z/(0.75d0*lqn*lqn)
       elim=max(elim,                                                   &
     &          minval((lll/r(1:nlast)+rv(1:nlast))/r(1:nlast)))
      else                            ! lll=0
       elim = minval(rv(1:nlast)/r(1:nlast))
       elim = max(elim,-z*z/0.01d0)
      endif
!c
!c     check potential
!c **********************************************************************
      if(elim.ge.0) then
        coerr='   v+l*(l+1)/r**2 always positive'
        if ( debug_print .ge. 0 ) then
         write(6,*) sname//'::'//coerr, ' l=',lqn,' n=',nqn,' k=',kqn
        end if
        en = zero
        ifail = 1
        return
      endif
!c **********************************************************************
      invp = nmt
      if(en.le.elim.or.en.ge.zero) then
       if(nitmax.gt.1) then
        en=elim*0.75d0
       else
        ifail = -1    !
        coerr = sname//':: ERROR '
        if ( en <= elim ) then
       write(6,'(a,'' en='',e16.7,'' <= elim='',e16.7)') coerr,en,elim
        else
       write(6,'(a,'' en='',e16.7,'' >= 0'')') coerr,en
        end if
        return
       end if
      end if
      icheck = 0
  2   continue
      nmax = size(r)
      if ( iter==0 ) then
        emin = en
        emax = en
      else
       if ( icheck == 0 ) then
        if ( en>emin .and. en<emax ) then
          icheck = 1
        else
          emin = min(emin,en)
          emax = max(emax,en)
        end if
       else
        if ( en > eold ) then
           emin=eold
        else
           emax = eold
        end if
        if ( en<emin .or. en > emax ) then
          en = (emin+emax)/2
        end if
       end if
      end if
       eold = en
!      if(iter.gt.0) then
      if ( imm == 0 ) then
       invp = invPoint(ipdeq,lll,min(nmt,nmax),r,rv,en)
       nout = min(nws+1,nmax)
      end if
      call outws_sr(invp,rg,rf,rv,r,en,c,elim,z,                        &
     &                     tol,imm,lqn,nodes,nout)
      if (en.ge.zero) then
       ifail = 5
       return
      end if
!      end if

      tmp(1:invp) = rg(1:invp)**2
!DEBUG      if ( c<=cmax_nonrel ) then
!DEBUG       tmp(1:invp) = tmp(1:invp) + rf(1:invp)**2
!DEBUG      end if
      call qexpup(1,tmp,invp,x,qmp)
      fz2oc2  = four*z*z/(c*c)
      power = sqrt(max(one,dble(lqn**2))-fz2oc2)
      rose=qmp(invp) + r(1)*tmp(1)/(two*power+one)

!c **********************************************************************
!c
!c     store the inversion point values
!c **********************************************************************
      rfm=rf(invp)
      rgm=rg(invp)
!c
!c     routine inws_sr performs the inward integration
!c **********************************************************************
      call inws_sr (invp,nmax,rg,rf,rv,r,en,c,lqn,imm                   &
     &             ,v_outside=rv(nws)/r(nws))
!c
!c components match
!c **********************************************************************
      fnrm=rgm/rg(invp)
!c
!c check first the sign of the big component
!c **********************************************************************
      if ( fnrm<zero ) then
         ! coerr='wrong big component change'
         en = zero
         ifail = 2
         return
      endif
!c **********************************************************************
      rg(invp:nmax) = rg(invp:nmax)*fnrm
      rf(invp:nmax) = rf(invp:nmax)*fnrm
!c
!c energy derivative of the wvfcns "log. derivative"
!c **********************************************************************
      tmp(invp:nmax) = rg(invp:nmax)**2
!DEBUG      if ( c<=cmax_nonrel ) then
!DEBUG       tmp(invp:nmax) = tmp(invp:nmax) + rf(invp:nmax)**2
!DEBUG      end if

!DEBUG      do j=nmax,1+max(invp,nmt),-1
!DEBUG       if ( tmp(j-1)>tmp(j) ) then
!DEBUG        exit
!DEBUG       end if
!DEBUG      end do
!DEBUG      nmax = j

      call qexpup(1,tmp(invp:nmax),nmax-invp+1,x(invp:nmax),            &
     &                                         qmp(invp:nmax))
      rose=rose+qmp(nmax)+h*r(invp)*(rfm*rfm-rf(invp)*rf(invp))/three
!c
!c energy update
!c **********************************************************************
      de=rg(invp)*(rfm-rf(invp))*c/rose
      if (nitmax.le.1) then
       val = zero
      else
       val=abs(de/en)
      end if
      enew = en+de
      if (iter>1.and.enew<zero.and.(val.le.1.d-02.or.abs(de)<1.d-02))   &
     & then
        imm=1
      else
        imm=0
      end if
      if ( val>0.4d0 ) then
       if ( enew>zero ) then
        imm=0
        if ( eold<=en+0.01d0*abs(eold) ) then
         enew = min(0.9d0*en,en+1.d0)
        else
         enew = max(1.2d0*eold,eold-2.d0)
        end if
       else if ( val>1.d0) then
        enew = max(1.2d0*en,en-2.d0)
       else
        enew = max(enew,eold-5.d0)
       end if
      end if
!       write(6,'(a,i3,2g18.9,2x,2g12.4)') 'DEBUG_DC1:',iter,            &
!     &    eold,en,de,abs(en-eold)
      abs_err = max(abs(de),abs(enew-eold))
      if ( abs_err<etol/abs(5*kqn) .or. nitmax==0 ) go to 7        ! normal exit

      if ( (max(abs(en),abs(eold))<=abs(eclose_to_zero/5)               &
     &                                     .and.en+de>zero) ) then
        enew = -0.5d0*tol    ! eignevalue, if exists, is not a core state
        ifail = 3
        go to 7
      end if

      if ( abs(de)<2*epsilon(de) ) then
        ifail = 4
        if ( val>10*tol ) then
         write(6,('(a,g12.4,a,g12.4,a,g18.10,3i3,a,i3)'))               &
     &                  sname//' WARNING: no convergence, val=',val,    &
     &            '  > tol=',tol,' en=',en,nqn,lqn,kqn,' iter=',iter
        end if
        go to 7
      end if

      if ( enew>zero ) then
       enew = eold*0.5d0
       imm = 0
       if (eold .gt. eclose_to_zero) then
        ! coerr=' zero energy'
        ifail = 5
        en = zero
        return
       end if
      end if
!c **********************************************************************
!c
!c     no convergence yet: try again
!c **********************************************************************
      en = enew
      iter=iter+1
      if ( iter.le.nitmax) go to 2

      ifail = 1 + nitmax
!c
!c     not converged, too small tolerance or too small nitmax
!c **********************************************************************
      if (en.lt.-0.2d0) then
       write(6,'(a,i3,a,g12.4,a,g12.4,a,g20.10,a,g12.4,a,g12.4)')       &
     &                               sname//': iter=',iter,             &
     &           ' WARNING: too many energy iterations, rel_err='       &
     &           ,val, ', tol=',tol,' en=',en                           &
     &          ,' abs_err=',abs_err,' ec_tol=',etol/abs(5*kqn)
      end if
!c **********************************************************************
!c
!c     got convergence: exit
!c     normalize the wavefunctions
!c **********************************************************************
  7   continue

      if ( nitmax > 1 ) then
       if ( enew > eclose_to_zero ) then
        en = zero
       else
        en = enew
       end if
      end if

      if ( en < zero ) then

!      if ( lll>0 ) then
!       do j=1,nmax
!        mass = 2*m_e + (en*r(j)-rv(j))/r(j)*cinv**2
!        rg(j) = rg(j)*sqrt(one + dble(lll)/(mass*c*r(j))**2)
!!          to add (rg)**2 * l(l+1)/r**2 / (mass*c)**2, see Koelling-Harmon paper
!       end do
!      end if

      tmp(1:nmax) = rg(1:nmax)**2
!DEBUG      if ( c<=cmax_nonrel ) then
!DEBUG       tmp(1:nmax) = tmp(1:nmax) + rf(1:nmax)**2
!DEBUG      end if
!
!  wave-fct. normalization is required in the whole space,
!  here it is limited by a sphere with R=maxval(r)
!
       call qexpup(1,tmp,nmax,x,qmp)
       rose = qmp(nmax) + r(1)*tmp(1)/(power+power+one)
       rose = rose + h*r(invp)*(rfm*rfm-rf(invp)*rf(invp))/three
!
       gnrm1 = one/sqrt(rose)
       rg(1:nmax) = rg(1:nmax)*gnrm1
       rf(1:nmax) = rf(1:nmax)*gnrm1
       rg(nmax+1:) = zero
       rf(nmax+1:) = zero
!DEBUGPRINT
!      call derv5(rg,qmp,r,nmax)
!      qmp(nmt:nmax) = one+qmp(nmt:nmax)/(rg(nmt:nmax)+1.d-40)
!      write(100+lqn+10*nqn,*) ' K=',kqn,' N=',nqn,' SZ_R=',size(r)
!      write(100+lqn+10*nqn,'(8g14.4)') qmp(nmt:nmax)
!      write(100+lqn+10*nqn,*)
!      write(6,*) ' N=',nqn,' NLAST=',nlast,' NOUT=',nout
!      write(6,*) ' L=',lqn,' NMT=',nmt,' NWS=',nws,' NMAX=',nmax
!      write(6,*) ' DEBUG: minloc=',nmt-1+minloc(qmp(nmt:nmax)),         &
!     &           ' minval=',minval(qmp(nmt:nmax))
!DEBUGPRINT
      else
       rg = zero
       rf = zero
      end if

      return
!EOC
      end subroutine deepst_sr
!c
!c ======================================================================
!BOP
!!IROUTINE: inws
!!INTERFACE:
      subroutine inws (invp,nmax,rg,rf,rv,r,en,c,dk,imm)
!!DESCRIPTION:
! adams 5 points  diff. eq. solver for dirac equations
! (inward solution)

!!DO_NOT_PRINT
      implicit none
!!DO_NOT_PRINT

!!ARGUMENTS:
      integer, intent(in) :: invp
      integer, intent(inout) :: nmax
      real(8), intent(in) :: r(:),rv(:)
      real(8), intent(out) :: rg(:),rf(:)
      real(8), intent(in) :: en,c
      real(8), intent(in) :: dk
      integer, intent(in) :: imm
!!REMARKS:
! private module procedure
!EOP
!
!BOC
      integer, parameter :: ipdeq=5
!c ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!c    drg,drf derivatives of dp and dq times r;  c speed of light
!c    rv potential times r; r: radial grid; en energy guess
!c *********************************************************************
!c
      real(8) :: drf(ipdeq+1),drg(ipdeq+1)
      real(8) :: rf_(ipdeq+1),rg_(ipdeq+1)
      real(8) :: rgtmp,rftmp,rtmp(ipdeq),exp_h
      integer :: nmax_

      integer :: j,i,itop,l
      real(8) :: dm,p,dpr,dqr,dp,dq,er,emvoc
      real(8), parameter :: coef1=dble(475)/dble(502)
      real(8), parameter :: coef2=dble(27)/dble(502)
      real(8), parameter ::                                             &
     &       predictor(5)=[251.d0,-1274.d0,2616.d0,-2774.d0,1901.d0],   &
     &       corrector(5)=[-19.d0,106.d0,-264.d0,646.d0,251.d0]
      real(8), save :: w_ode=0
!c
      if ( w_ode==0 ) then
       w_ode=sum(predictor)
      end if
!c
!c at first find the last point where the wavefcns are not zero
!c **********************************************************************
!c
      if ( en>=zero ) then
        return
      end if
      nmax_ = nmax
      p=sqrt(abs(en*(one+en/(c*c))))
!      pq=-p/(c+en/c)
      if( imm .ne. 1 ) then
       do nmax_=size(r),ipdeq+1,-1
        if ( -p*r(nmax_) > log_tiny ) exit
       end do
      end if
      rg(nmax_+1:) = zero
      rf(nmax_+1:) = zero
!c
!c initial values for inward integration
!c **********************************************************************
      exp_h = exp(log(r(nmax_)/r(nmax_-1)))
      rtmp(ipdeq) = r(nmax_)
      do i=ipdeq-1,1,-1
       rtmp(i) = rtmp(i+1)*exp_h
      end do
      rg_ = zero
      rf_ = zero
      call inwhnk (1,rg_(1:ipdeq),rf_(1:ipdeq),                         &
     &                 drg(1:ipdeq),drf(1:ipdeq),rtmp(1:ipdeq),en,c,dk)
      rg(nmax_) = rg_(ipdeq)
      rf(nmax_) = rf_(ipdeq)
!c
!c     solve dirac equations now
!c **********************************************************************
      dm = log(r(invp)/r(invp-1))/w_ode
      itop=nmax_-1
      do 50 i=invp,itop
        j=itop-i+invp
!c
!c     5 points predictor
!c
        dpr = rg(j+1) - dm*dot_product(predictor,drg(1:ipdeq))
        dqr = rf(j+1) - dm*dot_product(predictor,drf(1:ipdeq))
!c
!c     dirac equations (log. mesh)
!c
        er=en*r(j)
        emvoc=(er-rv(j))/c
        drg(ipdeq+1)=-dk*dpr+(c*r(j)+emvoc)*dqr
        drf(ipdeq+1)=dk*dqr-emvoc*dpr
!c
!c     5 points corrector
!c
        dp = rg(j+1) - dm*dot_product(corrector,drg(2:ipdeq+1))
        dq = rf(j+1) - dm*dot_product(corrector,drf(2:ipdeq+1))
!c
!c     mixing
!c
        dp=coef1*dp+coef2*dpr
        dq=coef1*dq+coef2*dqr
        rg(j)=dp
        rf(j)=dq
!c
!c     update derivative
!c
        drg(ipdeq+1)=-dk*dp+(c*r(j)+emvoc)*dq
        drf(ipdeq+1)=dk*dq-emvoc*dp
!c
!c     shift derivatives
!c
        do l=2,ipdeq+1
         drg(l-1)=drg(l)
         drf(l-1)=drf(l)
        end do
!c **********************************************************************
  50  continue
      nmax = nmax_
!c
      return
!EOC
      end subroutine inws
!c ======================================================================
      subroutine inwhnk (invp,rg,rf,drg,drf,r,en,c,dk)
!c ======================================================================
!      use mecca_constants
      implicit none
      complex(8), parameter :: sqrtm1=(0.d0,1.d0)
      complex(8), parameter :: mi=(0.d0,-1.d0)

!      integer, parameter :: ipdeq=5
!c ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!c
!c   drg,drf derivatives of dp and dq times r;  c speed of light
!c   free solution..... riccati-hankel functions    fixed DDJ & FJP 1991
!c *********************************************************************
!c
      integer :: invp
      real(8) :: r(:),rg(:),rf(:)
      real(8) :: drf(:),drg(:)
      real(8) :: en,c
      real(8) :: dk
      integer, parameter :: lmaxcore=20
      complex(8) :: bh(lmaxcore),dh(lmaxcore)
      complex(8) :: x,p,mil
      real(8) :: mass

      real(8) :: cinv,c2inv,ak,sgnk,factor
      integer :: l,lp,ll,n,ndrpts
!c
!c **********************************************************************
!c
      ndrpts = size(r)
      cinv=one/c
      c2inv=cinv*cinv
      mass=(one + en*c2inv)
      p=sqrt( dcmplx(en*mass,zero) )
!c
!c      kappa=dk
      ak=abs(dk)
      sgnk=dk/ak
      factor= sgnk*sqrt( -en/mass )*cinv
!c
!c officially this is L+1
      l = ak + (1+sgnk)/2
      lp=l - sgnk
      mil=sqrtm1**(l-1)
!c
!c maximum l needed for this kappa
      ll=max(l,lp)

      if(ll.gt.lmaxcore) then
       write(*,*) 'INWHNK: LL=',ll,' LMAXCORE=',lmaxcore
       call fstop('INWHNK:  increase LMAXCORE')
      end if
!c
      do 5 n=ndrpts,invp,-1
      x=p*r(n)
!c==============================
      call richnk(ll,x,bh,dh)
!c==============================
!c
!c NOTE: working on log-mesh so need extra factor of r for drg and drf.
!c
      rg(n)=dreal(-mi*mil*bh(l))
!????      drg(n)=-x*mil*dh(l)
      drg(n)=dreal(-mi*x*mil*dh(l))
      rf(n)=-factor*dreal(mil*bh(lp))
      drf(n)=-factor*dreal(x*mil*dh(lp))
  5   continue
!      drg(ipdeq)=-x*(mi*mil*dh(l))
!      drf(ipdeq)=-x*factor*mil*dh(lp)
!c
!c     write(6,*)'  rg(invp), rf(invp) ',rg(invp), rf(invp)
      return
      end subroutine inwhnk
!c
!c ======================================================================
!BOP
!!IROUTINE: outws
!!INTERFACE:
      subroutine outws(invp,rg,rf,rv,r,en,c,elim,                       &
     &                 z,gam,slp,tol,imm,lll,dk,nodes,nws)
!!DESCRIPTION:
! adams 5 points  diff. eq. solver for dirac equations
! (outward solution)

!!DO_NOT_PRINT
      implicit none
      integer, parameter :: ipdeq=5
!!DO_NOT_PRINT

!!ARGUMENTS:
      integer :: invp
      real(8) :: r(:),rv(:),rg(:),rf(:)
      real(8) :: en,c
      real(8) :: drf(ipdeq),drg(ipdeq)
      real(8) :: elim,z,gam,slp,tol
      integer :: imm,lll
      real(8) :: dk
      integer :: nodes,nws
!!REMARKS:
! private module procedure
!EOP
!
!BOC
!c ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!c     drg,drf derivatives of dp and dq times r;  c speed of light
!c     rv potential times r; r: radial grid; en energy guess
!c *********************************************************************
!c
      character(80) :: coerr
      character(5), parameter ::sname='outws'

      integer :: nd,j,k,l
      real(8) :: dm,dp,dpr,dq,dqr,er,emvoc
      real(8) :: enmax,enmin
      real(8) :: a11,a12,a21,a22
      real(8), parameter :: coef1=dble(475)/dble(502)
      real(8), parameter :: coef2=dble(27)/dble(502)
!c     parameter (coef1=.9462151394422310,coef2=.0537848605577689)
      real(8), parameter ::                                             &
     &       predictor(5)=[251.d0,-1274.d0,2616.d0,-2774.d0,1901.d0],   &
     &       corrector(5)=[-19.d0,106.d0,-264.d0,646.d0,251.d0]
      real(8), save :: w_ode=0
!c
      if ( w_ode==0 ) then
       w_ode=sum(predictor)
      end if
!c
      nd=0
      enmax = zero
      enmin = en
!c
!c     find classical inversion point
!c *********************************************************************
!c
 10   if( imm .eq. 1 ) go to 40
!c
      invp = invPoint(1,lll,nws,r,rv,en)
!c
      if( invp .gt. ipdeq ) go to 40
!c
      en = en - max(enmin*0.1d0,en*0.4d0)
!c
      if( en .lt. -tol .and. nd .le. nodes ) go to 10

      if ( debug_print.ge.0 ) then
       write(6,'(1x,''l='',i2,'' en='',g12.5,'' nd='',i2,'' <= nodes='',&
     &     i2)') lll,en,nd,nodes
      end if
!c
!        coerr=' ERROR :: screwed potential'
!      call p_fstop(sname//':'//coerr)
      en = zero
      return
!c **********************************************************************
!c initial values for outward integration
!c **********************************************************************
 40   call invals1(deriv_dirac,rg(1:ipdeq),rf(1:ipdeq),r(1:ipdeq),      &
     &                                                slp,gam,          &
     &             rv(1:ipdeq),z,drg(1:ipdeq),drf(1:ipdeq),en,c,dk)
!c **********************************************************************
      nd=1
      do 60 j=2,ipdeq
        if( rg(j-1) .eq. zero ) cycle
        if( (rg(j)/rg(j-1)) .gt. zero ) cycle
!c
        nd=nd+1
 60   continue
!c
!c **********************************************************************
!c
!c     solve dirac eqs. now
!c **********************************************************************
      dm = log(r(invp)/r(invp-1))/w_ode
      do 100 j=ipdeq+1,invp
!c
!c     5 points predictor
!c
        dpr = rg(j-1) + dm*dot_product(predictor,drg(1:ipdeq))
        dqr = rf(j-1) + dm*dot_product(predictor,drf(1:ipdeq))
!c
!c     shift derivatives
!c
        do 90 l=2,ipdeq
          drg(l-1)=drg(l)
          drf(l-1)=drf(l)
 90     continue
!c
!c     dirac equations (log. mesh)
!c
        er=en*r(j)
        emvoc=(er-rv(j))/c
        a11 = -dk
        a12 = c*r(j)+emvoc
        a21 = -emvoc
        a22 = dk
        drg(ipdeq)= a11*dpr + a12*dqr
        drf(ipdeq)= a21*dpr + a22*dqr
!c
!c     5 points corrector
!c
        dp = rg(j-1) + dm*dot_product(corrector,drg(1:ipdeq))
        dq = rf(j-1) + dm*dot_product(corrector,drf(1:ipdeq))
!c
!c     mixing
!c
        dp=coef1*dp+coef2*dpr
        dq=coef1*dq+coef2*dqr
        rg(j)=dp
        rf(j)=dq
!c
!c     update derivative
!c
        drg(ipdeq)= a11*dp + a12*dq
        drf(ipdeq)= a21*dp + a22*dq
!c
!c     check number of nodes
!c **********************************************************************
!c
        if(  rg(j-1) .eq. zero ) go to 100
        if( (rg(j)/rg(j-1)) .gt. zero ) go to 100
!c
        nd=nd+1
!c
        if( nd .gt. nodes ) go to 110
!c
 100  continue
!c
!c     if no. of nodes is too small increase the energy and start again
!c **********************************************************************
!c
      if( nd .eq. nodes ) go to 130
!c
      en=min((en+enmax)*0.5d0,0.8d0*en)
!c
      if( en .lt. -tol ) go to 10

      en = zero
      return
!c
!c
!c     this energy guess has become too high
!c **********************************************************************
!c     if no. of nodes is too big decrease the energy and start again
!c **********************************************************************
 110  continue
      enmax = en
!      if ( en>0.9*elim ) then
!       en = (en+elim)*0.5d0
!      else
       en=en + max(-2.d0,0.21d0*en)
!      end if
      if( en .ge. 2*elim ) go to 10
!c
!c     this energy guess has become too low
!c **********************************************************************
      write(6,'('' en/elim='',2g14.6,'' nd/nodes='',2i5,'' lll='',i2)') &
     &                en,elim,nd,nodes,lll
      coerr=' ERROR :: too many nodes'
      call p_fstop(sname//':'//coerr)
!c **********************************************************************
 130  return
!c
!EOC
      end subroutine outws

!!! canonical_bands


!c ======================================================================
!BOP
!!IROUTINE: outws2
!!INTERFACE:
      subroutine outws2(np,rg,rf,rv,r,en,c,z,gam,slp,dk,nodes)
!!DESCRIPTION:
! adams 5 points  diff. eq. solver for dirac equations
! (outward solution)

!!DO_NOT_PRINT
      implicit none
      integer, parameter :: ipdeq=5
!!DO_NOT_PRINT

!!ARGUMENTS:
      integer, intent(in)  :: np
      real(8), intent(in)  :: r(:),rv(:)
      real(8), intent(out) :: rg(:),rf(:)
      real(8), intent(in)  :: en,c


      real(8), intent(in)  :: z,gam,slp
      real(8), intent(in)  :: dk                                                  !!! (double) kappa
      integer, intent(out) :: nodes

      real(8) :: drf(ipdeq),drg(ipdeq)
!!REMARKS:
! private module procedure
!EOP
!
!BOC
!c ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!c     drg,drf derivatives of dp and dq times r;  c speed of light
!c     rv potential times r; r: radial grid; en energy guess
!c *********************************************************************
!c
      character(80) :: coerr
      character(5), parameter ::sname='outws2'

      integer :: nd,j,k,l
      real(8) :: dm,dp,dpr,dq,dqr,er,emvoc
      real(8) :: a11,a12,a21,a22
      real(8), parameter :: coef1=dble(475)/dble(502)
      real(8), parameter :: coef2=dble(27)/dble(502)
      real(8), parameter ::                                             &
     &       predictor(5)=[251.d0,-1274.d0,2616.d0,-2774.d0,1901.d0],   &
     &       corrector(5)=[-19.d0,106.d0,-264.d0,646.d0,251.d0]
      real(8), save :: w_ode=0
!c
      if ( w_ode==0 ) then
            w_ode=sum(predictor)
      end if
!c
      nd=0

!c **********************************************************************
!c initial values for outward integration
!c **********************************************************************
 40   call invals1(deriv_dirac,rg(1:ipdeq),rf(1:ipdeq),r(1:ipdeq),      &
     &                                                slp,gam,          &
     &             rv(1:ipdeq),z,drg(1:ipdeq),drf(1:ipdeq),en,c,dk)
!c **********************************************************************
!      nd=1                                                                   !!! ???
!      do j=2,ipdeq
        !if( rg(j-1) .eq. zero ) cycle
!        if( (rg(j)*rg(j-1)) .gt. zero ) cycle
!c
 !       nd=nd+1
 !     enddo ! j
!c
!c     check consistence of signs of big and small component
!c **********************************************************************
!      k=-1+2*(nodes-2*(nodes/2))
!c
!      if ( k*dk*rf(ipdeq) < zero .or. rg(ipdeq)*k <= zero ) then
!          write(*,*) '  k=',k
!          write(*,*) ' dk=',dk
!          write(*,*) ' rg(ipdeq)=',rg(ipdeq)
!          write(*,*) ' rf(ipdeq)=',rf(ipdeq)
!        coerr=' ERROR :: errors in initial values for outward intgr.'
!        call p_fstop(sname//':'//coerr)
!      end if
!c
!c **********************************************************************
!c
!c     solve dirac eqs. now
!c **********************************************************************
      dm = log(r(np)/r(np-1))/w_ode
      do j=ipdeq+1,np
!c
!c     5 points predictor
!c
        dpr = rg(j-1) + dm*dot_product(predictor,drg(1:ipdeq))
        dqr = rf(j-1) + dm*dot_product(predictor,drf(1:ipdeq))
!c
!c     shift derivatives
!c
        do l=2,ipdeq
          drg(l-1)=drg(l)
          drf(l-1)=drf(l)
        enddo ! l
!c
!c     dirac equations (log. mesh)
!c
        er=en*r(j)
        emvoc=(er-rv(j))/c
        a11 = -dk
        a12 = c*r(j)+emvoc
        a21 = -emvoc
        a22 = dk
        drg(ipdeq)= a11*dpr + a12*dqr
        drf(ipdeq)= a21*dpr + a22*dqr
!c
!c     5 points corrector
!c
        dp = rg(j-1) + dm*dot_product(corrector,drg(1:ipdeq))
        dq = rf(j-1) + dm*dot_product(corrector,drf(1:ipdeq))
!c
!c     mixing
!c
        dp=coef1*dp+coef2*dpr
        dq=coef1*dq+coef2*dqr
        rg(j)=dp
        rf(j)=dq
!c
!c     update derivative
!c
        drg(ipdeq)= a11*dp + a12*dq
        drf(ipdeq)= a21*dp + a22*dq
!c
!c     check number of nodes
!c **********************************************************************
        if( (rg(j)*rg(j-1)) .lt. zero )  nd=nd+1

        enddo ! j
!c
        nodes=nd
!c
!EOC
      end subroutine outws2



!c ======================================================================
!BOP
!!IROUTINE: outws2b
!!INTERFACE:
      subroutine outws2b(np,rg,rf,rv,r,en,c,z,gam,slp,dk,nodes)
            !!DESCRIPTION:
            ! adams 5 points  diff. eq. solver for dirac equations
            ! (outward solution)

            !!DO_NOT_PRINT
                  implicit none
                  integer, parameter :: ipdeq=5
            !!DO_NOT_PRINT

            !!ARGUMENTS:
                  integer, intent(in)  :: np
                  real(8), intent(in)  :: r(:),rv(:)
                  real(8), intent(out) :: rg(:),rf(:)
                  real(8), intent(in)  :: en,c


                  real(8), intent(in)  :: z,gam,slp
                  real(8), intent(in)  :: dk                                                  !!! (double) kappa
                  integer, intent(out) :: nodes

                  real(8) :: drf(ipdeq),drg(ipdeq)
!!REMARKS:
! private module procedure
!EOP
!
!BOC
!c ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!c     drg,drf derivatives of dp and dq times r;  c speed of light
!c     rv potential times r; r: radial grid; en energy guess
!c *********************************************************************
!c
      character(80) :: coerr
      character(5), parameter ::sname='outws2b'

      integer :: nd,j,k,l
      real(8) :: dm,dp,dpr,dq,dqr,er,emvoc
      real(8) :: a11,a12,a21,a22,intsum,norm,hx
      real(8), parameter :: coef1=dble(475)/dble(502)
      real(8), parameter :: coef2=dble(27)/dble(502)
      real(8), parameter ::                                             &
     &       predictor(5)=[251.d0,-1274.d0,2616.d0,-2774.d0,1901.d0],   &
     &       corrector(5)=[-19.d0,106.d0,-264.d0,646.d0,251.d0]
      real(8), save :: w_ode=0
!c
      if ( w_ode==0 ) then
         w_ode=sum(predictor)
      end if
!c
      nd=0
      hx=(log(r(np))-log(r(1)))/real(np-1,kind(0.0d0))

!c **********************************************************************
!c initial values for outward integration
!c **********************************************************************
 40   call invals1(deriv_dirac,rg(1:ipdeq),rf(1:ipdeq),r(1:ipdeq),      &
     &                                                slp,gam,          &
     &             rv(1:ipdeq),z,drg(1:ipdeq),drf(1:ipdeq),en,c,dk)
!c **********************************************************************
!c normalizing everything using first points
      intsum=r(1)*(rg(1)*rg(1)+rf(1)*rf(1))/2.0d0
      do j=2,ipdeq
          intsum=intsum+hx*r(j)*(rg(j)*rg(j)+rf(j)*rf(j))
      enddo ! j
      norm         = sqrt(intsum)
      rg(1:ipdeq)  = rg(1:ipdeq)/norm
      rf(1:ipdeq)  = rf(1:ipdeq)/norm
      drg(1:ipdeq) = drg(1:ipdeq)/norm
      drf(1:ipdeq) = drf(1:ipdeq)/norm
      intsum       = 1.0d0

!c **********************************************************************
!c
!c     solve dirac eqs. now
!c **********************************************************************
      dm = log(r(np)/r(np-1))/w_ode
      do j=ipdeq+1,np
!c
!c     5 points predictor
!c
        dpr = rg(j-1) + dm*dot_product(predictor,drg(1:ipdeq))
        dqr = rf(j-1) + dm*dot_product(predictor,drf(1:ipdeq))
!c
!c     shift derivatives
!c
        do l=2,ipdeq
          drg(l-1)=drg(l)
          drf(l-1)=drf(l)
        enddo ! l
!c
!c     dirac equations (log. mesh)
!c
        er=en*r(j)
        emvoc=(er-rv(j))/c
        a11 = -dk
        a12 = c*r(j)+emvoc
        a21 = -emvoc
        a22 = dk
        drg(ipdeq)= a11*dpr + a12*dqr
        drf(ipdeq)= a21*dpr + a22*dqr
!c
!c     5 points corrector
!c
        dp = rg(j-1) + dm*dot_product(corrector,drg(1:ipdeq))
        dq = rf(j-1) + dm*dot_product(corrector,drf(1:ipdeq))
!c
!c     mixing
!c
        dp=coef1*dp+coef2*dpr
        dq=coef1*dq+coef2*dqr
        rg(j)=dp
        rf(j)=dq
!c
!c     update derivative
!c
        drg(ipdeq)= a11*dp + a12*dq
        drf(ipdeq)= a21*dp + a22*dq
!c
!c     check number of nodes
!c **********************************************************************
        if( (rg(j)*rg(j-1)) .lt. zero )  nd=nd+1

!c     renormalizing (all points up to j)
        if(j.eq.np) then
            intsum=intsum+hx*r(j)*(rg(j)*rg(j)+rf(j)*rf(j))/2.0d0
        else
           intsum=intsum+hx*r(j)*(rg(j)*rg(j)+rf(j)*rf(j))
        endif
        norm         = sqrt(intsum)
        rg(1:j)      = rg(1:j)/norm
        rf(1:j)      = rf(1:j)/norm
        drg(1:ipdeq) = drg(1:ipdeq)/norm
        drf(1:ipdeq) = drf(1:ipdeq)/norm
        intsum       = 1.0d0


        enddo ! j
!c
        nodes=nd
!c
!EOC
            end subroutine outws2b


!c ======================================================================
!BOP
!!IROUTINE: outws3
!!INTERFACE:
      subroutine outws3(np,rv,r,en,c,z,gam,slp,dk,results)
      !!DESCRIPTION:
      ! adams 5 points  diff. eq. solver for dirac equations
      ! (outward solution)
      ! solution renormalizes automatically, returns only data for r=r(np)
      ! normaliziation is done with a very simple schema, using the midpoint rules,
      ! which should be sufficient to keep numerical values within number range when the solution diverges.

!!DO_NOT_PRINT
            implicit none
            integer, parameter :: ipdeq=5
            real(8)            :: drf(ipdeq),drg(ipdeq)
            real(8)            :: rf0(ipdeq),rg0(ipdeq)
!!DO_NOT_PRINT

!!ARGUMENTS:
            integer,              intent(in)  :: np
            real(8),              intent(in)  :: r(:),rv(:)
            real(8),              intent(in)  :: en,c
            real(8),              intent(in)  :: z,gam,slp
            real(8),              intent(in)  :: dk                                      !!! (double) kappa
            type(outws3_results), intent(out) :: results
!!REMARKS:
! private module procedure
!EOP
!
!BOC
!c ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!c     drg,drf derivatives of dp and dq times r;  c speed of light
!c     rv potential times r; r: radial grid; en energy guess
!c *********************************************************************
!c
            character(80) :: coerr
            character(5), parameter ::sname='outws3'

            integer :: nd,j,k,l
            real(8) :: dm,dp,dpr,dq,dqr,er,emvoc
            real(8) :: a11,a12,a21,a22,intsum,norm,hx,dl
            real(8) :: rg,rf,rglast,rflast,sc
            real(8), parameter :: coef1=dble(475)/dble(502)
            real(8), parameter :: coef2=dble(27)/dble(502)
            real(8), parameter ::                                       &
     &       predictor(5)=[251.d0,-1274.d0,2616.d0,-2774.d0,1901.d0],   &
     &       corrector(5)=[-19.d0,106.d0,-264.d0,646.d0,251.d0]
            real(8), save :: w_ode=0
            real(8), parameter :: zero=0.0d0
!c
            if ( w_ode==0 ) then
                  w_ode=sum(predictor)
            end if
!c
            nd=0
            hx=(log(r(np))-log(r(1)))/real(np-1,kind(0.0d0))

            results%r                      = r(np)
            results%c                      = c
            results%kappa                  = int(dk)
            if(results%kappa.gt.0) then
               results%l                   = results%kappa
            else
               results%l                   = -results%kappa-1
            endif
            dl                             = real(results%l,kind(0.0d0))
            sc                             = results%r*c

!c **********************************************************************
!c initial values for outward integration
!c **********************************************************************
      call invals1(deriv_dirac,rg0(1:ipdeq),rf0(1:ipdeq),r(1:ipdeq),    &
     &                                                slp,gam,          &
     &             rv(1:ipdeq),z,drg(1:ipdeq),drf(1:ipdeq),en,c,dk)
!c **********************************************************************
!c normalizing everything using first points
      intsum=r(1)*(rg0(1)*rg0(1)+rf0(1)*rf0(1))/2.0d0
      do j=2,ipdeq
         intsum=intsum+hx*r(j)*(rg0(j)*rg0(j)+rf0(j)*rf0(j))
      enddo ! j
      norm         = sqrt(intsum)
      rglast       = rg0(ipdeq)/norm
      rflast       = rf0(ipdeq)/norm
      drg(1:ipdeq) = drg(1:ipdeq)/norm
      drf(1:ipdeq) = drf(1:ipdeq)/norm
      intsum       = 1.0d0

!c **********************************************************************
!c
!c     solve dirac eqs. now
 !c **********************************************************************
      dm = log(r(np)/r(np-1))/w_ode
      do j=ipdeq+1,np
!c
!c     5 points predictor
!c
            dpr = rglast + dm*dot_product(predictor,drg(1:ipdeq))
            dqr = rflast + dm*dot_product(predictor,drf(1:ipdeq))
!c
!c     shift derivatives
!c
            do l=2,ipdeq
                drg(l-1)=drg(l)
                drf(l-1)=drf(l)
            enddo ! l
!c
!c     dirac equations (log. mesh)
!c
            er=en*r(j)
            emvoc=(er-rv(j))/c
            a11 = -dk
            a12 = c*r(j)+emvoc
            a21 = -emvoc
            a22 = dk
            drg(ipdeq)= a11*dpr + a12*dqr
            drf(ipdeq)= a21*dpr + a22*dqr
!c
!c     5 points corrector
!c
            dp = rglast + dm*dot_product(corrector,drg(1:ipdeq))
            dq = rflast + dm*dot_product(corrector,drf(1:ipdeq))
!c
!c     mixing
!c
            dp=coef1*dp+coef2*dpr
            dq=coef1*dq+coef2*dqr
            rg=dp
            rf=dq
!c
!c     update derivative
!c
            drg(ipdeq)= a11*dp + a12*dq
            drf(ipdeq)= a21*dp + a22*dq
!c
!c     check number of nodes
!c **********************************************************************
            if(rg.eq.zero) then
               if(rglast.ne.zero) nd=nd+1
            else
               if( (rg*rglast) .le. zero )  nd=nd+1
            endif
!c
!c     renormalizing (all points up to j)
            if(j.eq.np) then
                intsum=intsum+hx*r(j)*(rg*rg+rf*rf)/2.0d0
            else
                intsum=intsum+hx*r(j)*(rg*rg+rf*rf)
            endif
            norm         = sqrt(intsum)
            rg           = rg/norm
            rf           = rf/norm
            drg(1:ipdeq) = drg(1:ipdeq)/norm
            drf(1:ipdeq) = drf(1:ipdeq)/norm
            intsum       = 1.0d0
            rglast       = rg
            rflast       = rf
      enddo ! j
!c
      results%rg                     = rg
      results%rf                     = rf
      results%logarithmic_derivative = -(dk+1.0d0) + sc*rf/rg
      results%potential_function     = 2.0d0*(2.0d0*dl+1.0d0) *         &
     & (results%logarithmic_derivative+dl+1.0d0)/                       &
     & (results%logarithmic_derivative-dl)
      results%logarithmic_derivative_xero_function =                    &
     &                             (dk+1.0d0)*rg - sc*rf
      results%band_center_function   =                                  &
     &((dk+1.0d0)-(dl+1.0d0)) *rg - sc*rf
      results%number_of_nodes        = nd
!c
!EOC
      end subroutine outws3

!!! end canonical_bands
!
!BOP
!!IROUTINE: invals1
!!INTERFACE:
      subroutine invals1(deriv,rg,rf,r,slp,gam,vr,z,drg,drf,en,c,dk)
!!DESCRIPTION:
! initial values for outward dirac integration
!
! {\bv
! variables explanation:
! rg big component times r rf: small component times r;
! r; radial log. grid; slp: slope at the origin; gam 1st term power;
! v potential at the first point; z atomic number;
! tol check for convergence; drg,drf derivatives times r;
! c: speed of light in rydbergs; en; energy in rydbergs;
! dk; spin angular quantum number;
! \ev}

!!DO_NOT_PRINT
      implicit none
!!DO_NOT_PRINT

!!ARGUMENTS:
      external :: deriv
      interface
       subroutine deriv(r,y,yp)
       real(8), intent(in) :: r
       real(8), intent(in) :: y(2)
       real(8), intent(out) :: yp(2)
       end subroutine deriv
      end interface
      real(8), intent(out) :: rg(:),rf(:)
      real(8), intent(in) :: r(:),vr(:)
      real(8), intent(in) :: slp,gam,z
      real(8), intent(out) :: drg(:),drf(:)
      real(8), intent(in) :: en,c,dk
!!REMARKS:
! private module procedure
!EOP
!
!BOC
      integer :: ipdeq
      character(7) ::  sname='invals1'

      integer j,m
      real(8) :: dm,pow1,pow2,rfr,rgr,sm
      real(8) :: rv0(0:size(drf)),r0(0:size(drf)),rg0,rf0
      real(8) :: ys(2),rj,rj1,eps,hmin
      real(8) :: dys(2)  !DEBUG
      integer :: ipot(2)
!c     sub-step information
      integer nss,ist
!      integer, save :: ierr=0
!      real(8) xss(1000)
!      real(8) rvss(1000)
!      real(8) yss(2,1000)
!      real(8) dyss(2,1000)
!      real(8) errss(2,1000)
      real(8), parameter :: abserr=1.d-10
      integer iwork(5)
      real(8) :: dydr(3+6*2)
!DEBUG_SR
      integer :: l
      real(8) :: a0,a1,a2,dg,a11,a12,a21,a22
!DEBUG_SR
!c
      ipdeq = size(drf)
      rg(1:ipdeq) = zero
      rf(1:ipdeq) = zero
!      r0(0) = zero
      r0(1:ipdeq) = r(1:ipdeq)
      rv0(1:ipdeq) = vr(1:ipdeq)
!c
!      rj = 1.d-6
!      vj = ((rv0(1)-rv0(0))*rj+(rv0(0)*r0(1)-rv0(1)*r0(0)))             &
!     &            /(r0(1)-r0(0))
      r0(0) = r_zero
      call twoZ(z,r0(0),1,rv0(0))

!      if ( dk<zero ) then
!        pow1 = slp*sign(one,dk-gam)
!        pow2 = slp/c
!!DEBUG        else if ( rv0(0) == zero ) then
!!DEBUG         pow1 = sign(one,dk-gam)
!!DEBUG        else
!!DEBUG!         pow1 = c*(dk-gam)/abs(rv0(0))
!!DEBUG         pow1 = (dk-gam)/abs(rv0(0))
!!DEBUG        end if
!      else if ( dk>=zero ) then
!        pow1 = slp*sign(one,dk+gam)/c
!        pow2 = slp
!!DEBUG        else if ( rv0(0) == zero ) then
!!DEBUG         pow1 = sign(one,dk+gam)
!!DEBUG        else
!!DEBUG         pow1 = abs(rv0(0))/(dk+gam)/c
!!DEBUG        end if
!      end if
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
      if ( dk<zero ) then
       pow1 = c*(dk-gam)/abs(rv0(0))
      else if ( dk>=zero ) then
       pow1 = abs(rv0(0))/(dk+gam)/c
      end if
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
      pow1 = pow1*slp
      pow2 = slp
      rg0 = pow1*r0(0)
      rf0 = pow2*r0(0)

!DEBUG-SR
!      l = abs(nint(dk))
!      if ( dk<0 ) l = l-1
!      a0 = one
!      a1 = -rv0(0)/(2*l+1)
!      a2 = (2*rv0(0)**2/(2*l+1)-en)/(2*l+3)
!      rg0 = r0(0)**gam*(one+r0(0)*(a1 + r(0)*a2))
!      dg = gam*rg0/r0(0) + r0(0)**gam*(a1 + two*a2*r(0))
!      pow2 = l+1
!      a11 = -pow2
!      a21 = -(en*r0(0) - rv0(0))/c
!      a12 = c*r0(0) - a21
!      rf0 = (dg-rg0*a11)/a12
!      rg0 = slp*(one+r0(0)*a1)
!      rf0 = slp*rf0
!DEBUG-SR
!
      call set_dirac_params(dk_in=dk,en_in=en,c_in=c)
      hmin=0.0d0
      eps=ode_err
      nss=0 ;
      rj = log(r0(0))
      ys(1) = rg0
      ys(2) = rf0
      do j=1,ipdeq
        rj1 = log(r(j))
        ipot(1)=j-1
        ipot(2)=j
        call set_dirac_params(ipot0=ipot,rfit0=r0(j-1:j),               &
     &                                             zfit0=rv0(j-1:j))
        ist = +1
!       call rkf45(deriv_dirac,2,ys,rj,rj1,eps,abserr,                   &
!     &            ist,dydr,iwork)
       call rkf45(deriv,2,ys,rj,rj1,eps,abserr,                         &
     &            ist,dydr,iwork)
       if(abs(ist).ne.2) then
         if ( ist >=5 ) then
             ist = -1
             call rkf45(deriv_dirac,2,ys,rj,rj1,eps,                    &
     &          max(10*abserr,1.d-8),ist,dydr,iwork)
         end if
         if(abs(ist).ne.2) then
          write(*,*) '     J=',j
          write(*,*) ' Xj1,Xj=',rj1,rj
          write(*,*) '     rg=',ys(1)
          write(*,*) '     rf=',ys(2)
          write(*,*) '     en=',en
          write(*,*) ' relerr=',eps
          write(*,*) ' abserr=',abserr
          write(*,*) ' IFLAG=',ist
          if ( ist.ne.6 ) then
           call fstop(sname//                                           &
     &         ': wrong IFLAG after RKF45: accuracy is not reached')
          end if
         end if
       end if
!
!DEBUG        call odeint1(rj,y,rj1,ys,ist,eps,hmin,                          &
!DEBUG     &               xss,yss,dyss,rvss,errss,nss,size(xss))
!DEBUG        if(ist.ne.0) then
!DEBUG          if ( ist == 3 ) then
!DEBUG            if ( ierr.le.10 ) then
!DEBUG             ierr = ierr+1
!DEBUG!DEBUG               write(*,*) 'odeint1 error (outward intgr.)=',ist
!DEBUG!DEBUG               if ( ierr.eq.10 ) then
!DEBUG!DEBUG          write(*,*) ' no more warnings for odeint1 (outward intgr.)...'
!DEBUG!DEBUG                end if
!DEBUG            end if
!DEBUG          else
!DEBUG            write(*,*) 'odeint1 error (outward intgr.)=',ist
!DEBUG            write(201,'(''# nss='',i5)') nss
!DEBUG            do k=1,nss
!DEBUG                write(201,'(1x,i5,8(1x,g18.10))')                       &
!DEBUG     &    k,xss(k),rvss(k),yss(1,k),yss(2,k),dyss(1,k),dyss(2,k),       &
!DEBUG     &                                   errss(1,k),errss(2,k)
!DEBUG            end do
!DEBUG            call fstop(' ODEINT1 ERROR')
!DEBUG          end if
!DEBUG        endif
        rg(j) = ys(1)
        rf(j) = ys(2)
        rj = log(r(j))
        drg(j) = dydr(1)
        drf(j) = dydr(2)
      end do
!c
      return
!EOC
      end subroutine invals1
!
!BOP
!!IROUTINE: inws_sr
!!INTERFACE:
      subroutine inws_sr(invp,nmax,rg,rf,rv,r,en,c,lqn,imm,v_outside)
!!DESCRIPTION:
! adams 5 points  diff. eq. solver for scalar-relativistic equation
! (inward solution)

!!DO_NOT_PRINT
      implicit none
!!DO_NOT_PRINT

!!ARGUMENTS:
      integer, intent(in) :: invp
      integer, intent(inout) :: nmax
      real(8), intent(in) :: r(:),rv(:)
      real(8), intent(out) :: rg(:),rf(:)
      real(8), intent(in) :: en,c
      integer, intent(in) :: lqn
      integer, intent(in) :: imm
      real(8), intent(in), optional :: v_outside
!!REMARKS:
! private module procedure
!EOP
!
!BOC
      integer, parameter :: ipdeq=5
!c ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!c    drg,drf derivatives of dp and dq times r;  c speed of light
!c    rv potential times r; r: radial grid; en energy guess
!c *********************************************************************
!c
      real(8) :: drf(ipdeq+1),drg(ipdeq+1)
      real(8) :: rf_(ipdeq+1),rg_(ipdeq+1)
      real(8) :: rgtmp,rftmp,rtmp(ipdeq),exp_h
      real(8) :: a11,a12,a21,a22
      integer :: nmax_

      integer :: j,i,itop,l
      real(8) :: cinv,c2inv,dll1,dm,p,dpr,dqr,dp,dq,rinv,emv
      real(8) :: en_outside
      real(8) :: mass ! relat. mass
      real(8), parameter :: coef1=dble(475)/dble(502)
      real(8), parameter :: coef2=dble(27)/dble(502)
      real(8), parameter ::                                             &
     &       predictor(5)=[251.d0,-1274.d0,2616.d0,-2774.d0,1901.d0],   &
     &       corrector(5)=[-19.d0,106.d0,-264.d0,646.d0,251.d0]
      real(8), save :: w_ode=0
!c
      if ( w_ode==0 ) then
       w_ode=sum(predictor)
      end if
      if ( present(v_outside) ) then
       en_outside = en - v_outside
      else
       en_outside = en
      end if
!c
!c at first find the last point where the wavefcns are not zero
!c **********************************************************************
!c
      if ( en_outside>=zero ) then
        return
      end if
      nmax_ = nmax
      p=sqrt(abs(en*(one+en/(c*c))))
!      pq=-p/(c+en/c)
      if( imm .ne. 1 ) then
       do nmax_=size(r),ipdeq+1,-1
        if ( -p*r(nmax_) > log_tiny ) exit
       end do
       do i=nmax+1,nmax_
        rg(i) = zero
        rf(i) = zero
       end do
      end if
      rg(nmax_+1:) = zero
      rf(nmax_+1:) = zero
!c
!c initial values for inward integration
!c **********************************************************************
      exp_h = exp(log(r(nmax_)/r(nmax_-1)))
      rtmp(ipdeq) = r(nmax_)
      do i=ipdeq-1,1,-1
       rtmp(i) = rtmp(i+1)*exp_h
      end do
      rg_ = zero
      rf_ = zero
      call inwhnk (1,rg_(1:ipdeq),rf_(1:ipdeq),                         &
     &             drg(1:ipdeq),drf(1:ipdeq),rtmp(1:ipdeq),             &
     &             en_outside,c,dble(-lqn-1))
      rg(nmax_) = rg_(ipdeq)
      rf(nmax_) = rf_(ipdeq)
!c
!c     solve scalar-relativistic equation now
!c **********************************************************************

      cinv = one/c
      c2inv = cinv*cinv
      dm = log(r(invp)/r(invp-1))/w_ode
      dll1 = lqn*(lqn+1)
      itop=nmax_-1
      do 50 i=invp,itop
        j=itop-i+invp
!c
!c     5 points predictor
!c
        dpr = rg(j+1) - dm*dot_product(predictor,drg(1:ipdeq))
        dqr = rf(j+1) - dm*dot_product(predictor,drf(1:ipdeq))
!c
!c     equation (log. mesh)
!c
!............................................................
        emv = en*r(j)-rv(j)
        rinv = one/r(j)
        mass = 2*m_e + emv*rinv*c2inv
        a11 = one
        a12 = c*mass*r(j)
        a21 = cinv*(dll1*rinv/mass-emv)
        a22 = -one
        drg(ipdeq+1)= a11*dpr + a12*dqr
        drf(ipdeq+1)= a21*dpr + a22*dqr
!............................................................
!c
!c     5 points corrector
!c
        dp = rg(j+1) - dm*dot_product(corrector,drg(2:ipdeq+1))
        dq = rf(j+1) - dm*dot_product(corrector,drf(2:ipdeq+1))
!c
!c     mixing
!c
        dp=coef1*dp+coef2*dpr
        dq=coef1*dq+coef2*dqr
        rg(j)=dp
        rf(j)=dq
!c
!c     update derivative
!c
!............................................................
        drg(ipdeq+1)= a11*dp + a12*dq
        drf(ipdeq+1)= a21*dp + a22*dq
!............................................................
!c
!c     shift derivatives
!c
        do l=2,ipdeq+1
         drg(l-1)=drg(l)
         drf(l-1)=drf(l)
        end do
!c **********************************************************************
  50  continue
      nmax = nmax_
!c
      return
!EOC
      end subroutine inws_sr
!c ======================================================================
!
!BOP
!!IROUTINE: outws_sr
!!INTERFACE:
      subroutine outws_sr(invp,rg,rf,rv,r,en,c,elim,                    &
     &                    z,tol,imm,lqn,nodes,nws)
!!DESCRIPTION:
! adams 5 points  diff. eq. solver for scalar-relativistic equation
! (outward solution)

!!DO_NOT_PRINT
      implicit none
!!DO_NOT_PRINT

!!ARGUMENTS:
      integer, intent(inout) :: invp
      real(8), intent(in) :: r(:),rv(:)
      real(8), intent(out) :: rg(:),rf(:)
      real(8), intent(inout) :: en
      real(8), intent(in) :: c,elim,z,tol
      integer, intent(in) :: imm,lqn
      integer, intent(in) :: nodes,nws
!!REMARKS:
! private module procedure
!EOP
!
!BOC
!c ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!c     drg,drf derivatives of dp and dq times r;  c speed of light
!c     rv potential times r; r: radial grid; en energy guess
!c *********************************************************************
!c
      character(80) :: coerr
      character(5), parameter ::sname='outws_sr'
      integer, parameter :: ipdeq=5

      integer :: nd,j,k,l,lll,nstrt
      real(8) :: dm,dp,dpr,dq,dqr,emv,vr0
      real(8) :: cinv,dk,dl1,dll1,gam,rinv,slp
      real(8) :: drf(ipdeq),drg(ipdeq)
      real(8) :: tmprf(ipdeq),tmprg(ipdeq)
      real(8) :: tmpdrf(ipdeq),tmpdrg(ipdeq)
      real(8) :: enmax,enmin
      real(8) :: mass ! relat. mass
      real(8) :: a11,a12,a21,a22,c2inv
      real(8), parameter :: coef1=dble(475)/dble(502)
      real(8), parameter :: coef2=dble(27)/dble(502)
!c     parameter (coef1=.9462151394422310,coef2=.0537848605577689)
      real(8), parameter ::                                             &
     &       predictor(5)=[251.d0,-1274.d0,2616.d0,-2774.d0,1901.d0],   &
     &       corrector(5)=[-19.d0,106.d0,-264.d0,646.d0,251.d0]
      real(8), save :: w_ode=0
!c
      if ( w_ode==0 ) then
       w_ode=sum(predictor)
      end if
      nd=0
      enmax = zero
      enmin = en
      lll = lqn*(lqn+1)
!c
!c     find classical inversion point
!c *********************************************************************
!c
 10   if( imm .eq. 1 ) go to 40
!c
      invp = invPoint(1,lll,nws,r,rv,en)
!c
      if( invp .gt. ipdeq ) go to 40
!c
      en = en - max(enmin*0.1d0,en*0.4d0)
!c
      if( en .lt. -tol .and. nd .le. nodes ) go to 10

      if ( debug_print.ge.0 ) then
       write(6,'(1x,''l='',i2,'' en='',g12.5,'' nd='',i2,'' <= nodes='',&
     &     i2)') lll,en,nd,nodes
      end if
!c
!        coerr=' ERROR :: screwed potential'
!      call p_fstop(sname//':'//coerr)
      en = zero
      return
!c **********************************************************************
!c initial values for outward integration
!c **********************************************************************
 40   continue
      dl1 = dble(lqn+1)
!      gam = sqrt(dl1**2-(two*z/c)**2)
      call twoZ(z,r_zero,1,vr0)
      if ( c<=cmax_nonrel .and. vr0.ne.zero ) then
       gam = sqrt(dl1**2-(vr0/c)**2)
      else
       gam = dl1
      end if
      if(nodes-2*(nodes/2).ne.0) then
        slp = -one
      else
        slp = one
      endif
      nstrt = ipdeq
      if ( lqn<2 ) nstrt = size(tmprg)
      call invals1(deriv_dirac,tmprg(1:nstrt),tmprf(1:nstrt),r(1:nstrt) &
     &                                                     ,slp,gam,    &
     &        rv(1:nstrt),z,tmpdrg(1:nstrt),tmpdrf(1:nstrt),en,c,-dl1)
      rg(1:nstrt) = tmprg(1:nstrt) * dl1
      rf(1:nstrt) = tmprf(1:nstrt) * dl1
      drg(1:ipdeq) = tmpdrg(nstrt-ipdeq+1:nstrt)*dl1
      drf(1:ipdeq) = tmpdrf(nstrt-ipdeq+1:nstrt)*dl1
!
      if ( lqn > 0 .and. c<=cmax_nonrel ) then
       dk = dble(lqn)
       gam = sqrt(dk*dk-(vr0/c)**2)
       if(nodes-2*(nodes/2).ne.0) then
        slp = one
       else
        slp = -one
       endif
       nstrt = ipdeq
       if ( lqn<2 ) nstrt = size(tmprg)
       call invals1(deriv_dirac,tmprg(1:nstrt),tmprf(1:nstrt),r(1:nstrt)&
     &                                                     ,slp,gam,    &
     &         rv(1:nstrt),z,tmpdrg(1:nstrt),tmpdrf(1:nstrt),en,c,dk)
       rg(1:nstrt) = rg(1:nstrt) + tmprg(1:nstrt)*dk
       rf(1:nstrt) = rf(1:nstrt) + tmprf(1:nstrt)*dk
       drg(1:ipdeq) = drg(1:ipdeq) + tmpdrg(nstrt-ipdeq+1:nstrt)*dk
       drf(1:ipdeq) = drf(1:ipdeq) + tmpdrf(nstrt-ipdeq+1:nstrt)*dk
      else
       dk = zero
      end if
!
      rg(1:nstrt) = rg(1:nstrt) / (dl1+dk)
      drg(1:ipdeq) = drg(1:ipdeq)/(dl1+dk)
      rf(1:nstrt) = rf(1:nstrt)  / (dl1+dk)
      drf(1:ipdeq) = drf(1:ipdeq)/(dl1+dk)
!???      drf(1:ipdeq) = drf(1:ipdeq)*cinv

!c **********************************************************************
!c
!c     solve scalar-relat. eq. now
!c **********************************************************************
      dm = log(r(invp)/r(invp-1))/w_ode

      dll1 = dble(lqn*(lqn+1))
      cinv = one/c
      c2inv = cinv*cinv

      do 100 j=nstrt+1,invp
!c
!c     5 points predictor
!c
        dpr = rg(j-1) + dm*dot_product(predictor,drg(1:ipdeq))
        dqr = rf(j-1) + dm*dot_product(predictor,drf(1:ipdeq))
!c
!c     shift derivatives
!c
        do l=2,ipdeq
          drg(l-1)=drg(l)
          drf(l-1)=drf(l)
        end do
!c
!c      equation (log. mesh)
!c
!............................................................
        emv = en*r(j)-rv(j)
        rinv = one/r(j)
        mass = 2*m_e + emv*rinv*c2inv
        a11 = one
        a12 = c*mass*r(j)
        a21 = cinv*(dll1*rinv/mass-emv)
        a22 = -one
        drg(ipdeq)= a11*dpr + a12*dqr
        drf(ipdeq)= a21*dpr + a22*dqr
!............................................................
!c
!c     5 points corrector
!c
        dp = rg(j-1) + dm*dot_product(corrector,drg(1:ipdeq))
        dq = rf(j-1) + dm*dot_product(corrector,drf(1:ipdeq))
!c
!c     mixing
!c
        dp=coef1*dp+coef2*dpr
        dq=coef1*dq+coef2*dqr
        rg(j)=dp
        rf(j)=dq
!c
!c     update derivative
!c
!............................................................
        drg(ipdeq)= a11*dp + a12*dq
        drf(ipdeq)= a21*dp + a22*dq
!............................................................
!c
 100  continue
!c
!c     check number of nodes
!c **********************************************************************
!c
      nd=1
      do j=2,invp
       if ( rg(j-1) == zero ) then
        cycle
       else if ( rg(j)*sign(one,rg(j-1)) < zero ) then
        nd=nd+1
       end if
      end do
!c
!c     if no. of nodes is too small, increase the energy and start again
!c **********************************************************************
!c
      if( nd < nodes ) then
       en=min((en+enmax)*0.5d0,0.8d0*en)
       if( en .lt. -tol ) go to 10
       en = zero
      else if ( nd > nodes ) then
!c
!c     this energy guess has become too high;
!c     if no. of nodes is too big, decrease the energy and start again
!c **********************************************************************
       enmax = en
!      if ( en>0.9*elim ) then
!       en = (en+elim)*0.5d0
!      else
       en=en + max(-2.d0,0.21d0*en)
!      end if
       if( en .ge. 2*elim ) go to 10
!c
!c     this energy guess has become too low
!c **********************************************************************
       write(6,'('' en/elim='',2g14.6,'' nd/nodes='',2i5,'' lll='',i2)')&
     &                en,elim,nd,nodes,lll
       coerr=' ERROR :: too many nodes'
       call p_fstop(sname//':'//coerr)
      end if
!c **********************************************************************
      return
!c
!EOC
      end subroutine outws_sr
!c
!c ======================================================================
!
!BOP
!!IROUTINE: invPoint
!!INTERFACE:
      integer function invPoint(ipdeq,lll,nr,r,rv,en)
!!DESCRIPTION:
! estimates radial index for inversion point
!

!!DO_NOT_PRINT
      implicit none
!!DO_NOT_PRINT

!!ARGUMENTS:
      integer ipdeq, lll
      integer nr
      real*8 r(nr), rv(nr)
      real*8 en
!!REMARKS:
! private module procedure
!EOP
!BOC
      real*8 rj
      real*8 al
      integer j
      al = -lll
      invPoint = nr
      do j = ipdeq+2, nr+1-ipdeq
       invPoint = nr+1-j
       rj = r(invPoint)
       if( (rj*(rv(invPoint)-en*rj)).le.al) exit
      end do
      return
!EOC
      end function invPoint
!c
!BOP
!!IROUTINE: deriv_sr
!!INTERFACE:
      subroutine deriv_sr(r,Y,Ydot)
!!DESCRIPTION:
! calculates derivatives for scalar-relativistic equation

!!DO_NOT_PRINT
      implicit none
!!DO_NOT_PRINT

!!ARGUMENTS:
      real(8), intent(in) :: r
      real(8), intent(in) :: Y(2)
      real(8), intent(out) :: Ydot(2)
!!REMARKS:
! private module procedure
!EOP
!
!BOC
      real(8) :: A(2,2),z,dz
      call scalrel_matrix(r,A,z,dz)
      Ydot(1) = A(1,1)*Y(1)+A(1,2)*Y(2)
      Ydot(2) = A(2,1)*Y(1)+A(2,2)*Y(2)
      return
!EOC
      end subroutine deriv_sr
!c
!BOP
!!IROUTINE: deriv_dirac
!!INTERFACE:
      subroutine deriv_dirac(r,Y,Ydot)
!!DESCRIPTION:
! calculates derivatives

!!DO_NOT_PRINT
      implicit none
!!DO_NOT_PRINT

!!ARGUMENTS:
      real(8), intent(in) :: r
      real(8), intent(in) :: Y(2)
      real(8), intent(out) :: Ydot(2)
!!REMARKS:
! private module procedure
!EOP
!
!BOC
      real(8) :: A(2,2),z,dz
      call dirac_matrix(r,A,z,dz)
      Ydot(1) = A(1,1)*Y(1)+A(1,2)*Y(2)
      Ydot(2) = A(2,1)*Y(1)+A(2,2)*Y(2)
      return
!EOC
      end subroutine deriv_dirac

!BOP
!!IROUTINE: jacob1
!!INTERFACE:
      subroutine jacob1(x,F,Fr,AJ)
!!DESCRIPTION:
! calculate jacobians

!!DO_NOT_PRINT
      implicit none
!!DO_NOT_PRINT

!!ARGUMENTS:
      real(8), intent(in) :: x
      real(8), intent(in) :: F(2)
      real(8), intent(out) :: Fr(2)
      real(8), intent(out) :: AJ(2,2)
!!REMARKS:
! private module procedure
!EOP
!
!BOC
      real(8) :: z,dz,r
      call dirac_matrix(x,AJ,z,dz)
      r = exp(x)
      Fr(1) = r*(c_+en_/c_-dz/c_)*F(2)
      Fr(2) = -r*(en_/c_-dz)*F(1)
      return
!EOC
      end subroutine jacob1

!BOP
!!IROUTINE: scalrel_matrix
!!INTERFACE:
      subroutine scalrel_matrix(x,A,rv0,dz)
!!DESCRIPTION:
! calculates matrix of scalar-relativistic equation coefficients

!!DO_NOT_PRINT
      implicit none
!!DO_NOT_PRINT

!!ARGUMENTS:
      real(8), intent(in) :: x
      real(8), intent(out) :: A(2,2)
      real(8), intent(out) :: rv0,dz
!!REMARKS:
! private module procedure
!EOP
!
!BOC
      real(8) :: r,rinv,emv,mass
      real(8), save :: cinv2=-1
          if ( cinv2<=zero ) then
           cinv2 = one/c_**2
          end if
          r = exp(x)
          call zinterp(r,rv0,dz)
          rinv = one/r
          emv = en_*r-rv0
          mass = 2*m_e + emv*rinv*cinv2
!          A(1,1) = rinv
!          A(1,2) = mass
!          A(2,1) = (dk_*rinv/mass-emv)*rinv ! dk = l*(l+1)
!          A(2,2) = -rinv
          A(1,1) = one
          A(1,2) = c_*mass*r
          A(2,1) = (dk_*rinv/mass-emv)/c_ ! dk = l*(l+1)
          A(2,2) = -one
      return
!EOC
      end subroutine scalrel_matrix

!BOP
!!IROUTINE: dirac_matrix
!!INTERFACE:
      subroutine dirac_matrix(x,A,rv0,dz)
!!DESCRIPTION:
! calculates matrix of dirac equation coefficients

!!DO_NOT_PRINT
      implicit none
!!DO_NOT_PRINT

!!ARGUMENTS:
      real(8), intent(in) :: x
      real(8), intent(out) :: A(2,2)
      real(8), intent(out) :: rv0,dz
!!REMARKS:
! private module procedure
!EOP
!
!BOC
      real(8) r
          r = exp(x)
          call zinterp(r,rv0,dz)
          A(1,1) = -dk_
          A(2,1) = -(en_*r - rv0)/c_
          A(1,2) = c_*r - A(2,1)
          A(2,2) = dk_
      return
!EOC
      end subroutine dirac_matrix

!BOP
!!IROUTINE: set_dirac_params
!!INTERFACE:
      subroutine set_dirac_params(c_in,en_in,dk_in,ipot0,rfit0,zfit0)
!!DESCRIPTION:
! initilizes parameters for dirac solver

!!DO_NOT_PRINT
      implicit none
!!DO_NOT_PRINT

!!ARGUMENTS:
      real(8), intent(in), optional :: c_in
      real(8), intent(in), optional :: en_in
      real(8), intent(in), optional :: dk_in
      integer, intent(in), optional :: ipot0(2)
      real(8), intent(in), optional :: rfit0(2)
      real(8), intent(in), optional :: zfit0(2)
!!REMARKS:
! private module procedure
!EOP
!
!BOC
      if ( present(rfit0) ) then
        rfit = rfit0
      end if
      if ( present(zfit0) ) then
        zfit = zfit0
      end if
      if ( present(ipot0) ) then
        ipot = ipot0
      end if
      if ( present(c_in) ) then
        c_ = c_in
      end if
      if ( present(en_in) ) then
        en_ = en_in
      end if
      if ( present(dk_in) ) then
        dk_ = dk_in
      end if
      return
!EOC
      end subroutine set_dirac_params

!BOP
!!IROUTINE: zinterp
!!INTERFACE:
      subroutine zinterp(x,z,dz)
!!DESCRIPTION:
! interpolates function and its derivative,
! and returns the value of $z(x)$ and $d/dx z(x)$
! between mesh points;
! $z(x)$ is assumed to be monotonically decreasing,
! $log$ fit of $z(x)$ is used when possible

!!DO_NOT_PRINT
      implicit none
!!DO_NOT_PRINT

!!ARGUMENTS:
      real(8), intent(in) :: x
      real(8), intent(out) :: z,dz
!!REMARKS:
! private module procedure;
! there is no assumption for continuity of slope
! between adjacent grid intervals
!EOP
!
!BOC
      real(8) :: dx,dr,p,s
!      integer ipot
!      real*8 rfit
!      real*8 zfit
!      common/zfit_com/rfit(2),zfit(2),ipot(2)

!c     ------------------------------------------------------------------

      z=0.0d0
      dz=0.0d0
      dx=x-rfit(1)
      dr=rfit(2)-rfit(1)
      p=0.0d0
      if(zfit(1).gt.zero) then
        p=zfit(2)/zfit(1)
      endif
      if(p.gt.0.0d0 .and. p.lt.1.0d0) then
        s=log(p)/dr
        z =zfit(1)*exp( dx*s )
        dz=zfit(1)*exp( dx*s )*s
      else
        s=( zfit(2)-zfit(1) )/dr
        z =zfit(1)+dx*s
        dz=s
      endif
      return
!EOC
      end subroutine zinterp
!
      end module coresolver

