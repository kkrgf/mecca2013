#!/bin/csh -f

set SYS=`uname -s`
set FC=$1
#

if ( $1 == "sprs" ) then
 if ( -f Makefile ) then
  rm Makefile
 endif
 ln -s Makefile.sprs Makefile
 ls -l Makefile
 make SYS=${SYS}_gf rmLib
 exit
endif
#
if ( $1 == "nosprs" ) then
 if ( -f Makefile ) then
  rm Makefile
 endif
 ln -s Makefile.nosprs Makefile
 ls -l Makefile
 make SYS=${SYS}_gf rmLib
 exit
endif

if ( -f ${SYS}_${FC}.makein ) then
# ls -l ${SYS}_${FC}.makein
else
 echo "File  ${SYS}_${FC}.makein  not found"
 exit 1
endif

if ( -f Makefile ) then
# ls -l Makefile
else
 ln -s Makefile.nosprs Makefile
endif

set OD=obj_${FC}
set FM=finclude_${FC}
set LB=lib_${FC}
#set PG=mecca_${FC}
set PG=ameskkr_${FC}

if ( ! ${?MECCAHOME} ) then 
 setenv MECCAHOME `pwd`
else
  mkdir -p ${MECCAHOME}
endif
echo MECCAHOME=${MECCAHOME}

if ( $2 == "clean" ) then
 make SYS=${SYS}_${FC} MECCAHOME=${MECCAHOME} ODIR=${OD} FMDIR=${FM} LIBTMP=${LB} PROGRAM=${PG} realclean 
 exit
endif

if ( $2 == "libxc" ) then
 if ( ${?LIBXC} ) then
  echo LIBXC=${LIBXC}
 else
  cd src/libxc
  if ( ${FC} == "idb" || ${FC} == "gdb-ia" ) set FC=ifort
  set FF=`which $FC`
  if ( ${#FF} != 1 ) then
   if ( ${FC} == "gf" ) then
     set FF=`which gfortran`
   endif
  endif
  if ( ${#FF} != 1 ) then
   echo "Unable to find fortran compiler"
   echo "Please use original libxc installation procedure..." 
   exit 1
  endif
  set FPP=`which fpp` 
  if ( ${#FPP} != 1 ) then
   set FPP=`which cpp`
  endif
  if ( ${#FPP} > 1 ) then
   echo "Unable to find preprocessor"
   echo "Please use original libxc installation procedure..." 
   exit 1
  endif
  ../../libxc.sh ${MECCAHOME} $FF $FPP 
#  make distclean
 endif
 exit
endif

if ( $2 == "pdf_docs" || $2 == "ps_docs" ) then
 which pdflatex
 if ( $status>0 ) then
  echo "Unable to find pdflatex"
  exit 1
 endif 
 setenv PATH ${PWD}/docs/bin:${PATH}
 which noprint.awk
 if ( $status>0 ) then
  echo "DEBUG: Unable to find noprint.awk"
  exit 1
 endif 
 make SYS=${SYS}_${FC} $2 
 cd ${PWD}/docs
 pdflatex ${PG}intro.tex
 pdflatex ${PG}intro.tex
 pdflatex ${PG}intro.tex
 rm -f ${PG}intro.toc ${PG}intro.out ${PG}intro.aux part2_libvp.aux part2_libmecca.aux part2_mainprog.aux
 exit
endif

if ( ${?LIBXC} ) then
 echo ""
else
 setenv LIBXC ${MECCAHOME}
endif
echo LIBXC=${LIBXC}

mkdir -p ${OD} ${FM} ${LB}

if ( $1 == "gdb" || $1 == "idb" || $1 == "idb-ia" ) then
 set NP=1
else
 set NP=4
endif

if ( $2 == "MECCA" || $2 == "AMESKKR" ) then
 make SYS=${SYS}_${FC} MECCAHOME=${MECCAHOME} ODIR=${OD} FMDIR=${FM} LIBTMP=${LB} LIBXC=${LIBXC} PROGRAM=${PG} mobjs
 sleep 0.5
 make -j ${NP}  SYS=${SYS}_${FC} MECCAHOME=${MECCAHOME} ODIR=${OD} FMDIR=${FM} LIBTMP=${LB} LIBXC=${LIBXC} PROGRAM=${PG}  $2
 exit
endif

if ( $2 == "all" ) then
 make SYS=${SYS}_${FC} MECCAHOME=${MECCAHOME} ODIR=${OD} FMDIR=${FM} LIBTMP=${LB} LIBXC=${LIBXC} PROGRAM=${PG} mobjs
 sleep 0.5
 make SYS=${SYS}_${FC} MECCAHOME=${MECCAHOME} ODIR=${OD} FMDIR=${FM} LIBTMP=${LB} LIBXC=${LIBXC} PROGRAM=${PG} $2
 /bin/cp -a src/XYZ ${MECCAHOME}
# ( cd src ; tar chf - ./XYZ | tar x -C ${MECCAHOME} )
 exit
endif

set NA=`( echo $argv[*] ) | wc -w`

if ( ${NA} >= 2 ) then
 shift
 make SYS=${SYS}_${FC} MECCAHOME=${MECCAHOME} ODIR=${OD} FMDIR=${FM} LIBTMP=${LB} LIBXC=${LIBXC} PROGRAM=${PG} $*  
 exit
endif

