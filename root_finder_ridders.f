      module root_finder_ridders_module
      implicit none

      public :: root_finder_Ridders
      integer,  parameter, private :: dp           =                    &
     &                                       selected_real_kind(15,307)
      integer,  parameter, private :: MAXITER      = 120
      real(dp), parameter, private :: xacc_default = 1.0D-9


!! root function algorithm: Ridders' method
!! The root needs to be bracketed between x1 and x2 for the algorithm to work.

      contains
      function root_finder_ridders(func,x1,x2,ans,deltax,quiet)         &
     &                                                 result(found)
        implicit none
        interface
            function func(x) result(res)
               real(selected_real_kind(15,307)), intent(in) :: x
               real(selected_real_kind(15,307))             :: res
            end function func
        end interface
        real(dp),           intent(in ) :: x1,x2
        real(dp),           intent(out) :: ans
        real(dp), optional, intent(in ) :: deltax
        logical,  optional, intent(in ) :: quiet                            !!! prints error messages if false
        logical                         :: found

        integer  :: j
        real(dp) :: xacc
        real(dp) :: fl,fh,fm,xl,xh,xm,s,xnew,factor,fnew
        real(dp), parameter :: zero = 0.0_dp
        logical  :: do_print

        xacc=xacc_default
        if(present(deltax)) then
            xacc = deltax
        endif

        do_print=.true.
        if(present(quiet)) then
            do_print = .not. quiet
        endif

        found = .false.
        ans   = 1D+300
        
        fl=func(x1)
        fh=func(x2)

        if( (fl.gt.zero .and. fh.lt.zero)  .or.                         &
     &                             (fl.lt.zero .and. fh.gt.zero)) then
            xl=x1
            xh=x2
            do j=0,MAXITER
                xm=(xl+xh)/2.0d0
                fm=func(xm)
                s=sqrt(fm*fm-fl*fh)
                if(s.eq.zero) then
                    found=.true.
                    return
                endif
                if(fl.ge.fh) then
                    factor=1.0_dp
                else
                    factor=-1.0_dp
                endif
                xnew=xm+(xm-xl)*factor*fm/s
                if(abs(xnew-ans).lt.xacc) then
                    found=.true.
                    return
                endif
                ans=xnew
                fnew=func(ans)
                if(fnew.eq.zero) then
                    found=.true.
                    return
                endif

                if(sign(fm,fnew).ne.fm) then
                    xl=xm
                    fl=fm
                    xh=ans
                    fh=fnew
                 else if (sign(fl,fnew).ne.fl) then
                    xh=ans
                    fh=fnew
                 else if(sign(fh,fnew).ne.fh) then
                    xl=ans
                    fl=fnew
                 else
                    if(do_print) write(*,'(a)')                         &
     &                                   "error in root_finder_Ridders"
                    return
                 endif

                 if(abs(xh-xl).le.xacc) then
                    found=.true.
                    return
                 endif

            enddo ! j
            if(do_print) write(*,'(a)')                                 &
     &        "error in root_finder_Ridders, "//                        &
     &        "maximum number of iterations reached."

        else
            if(fl.eq.zero) then
                ans   = x1
                found = .true.
                return
            endif
            if(fh.eq.zero) then
                ans   = x2
                found = .true.
                return
            endif
            if(do_print)write(*,'(a)')                                  &
     &         "error in root_finder_Ridders, function values must "//   &
     &         "be positive and negative."
        endif

      end function root_finder_ridders



      end module root_finder_ridders_module
