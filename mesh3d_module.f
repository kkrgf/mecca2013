!BOP
!!MODULE: mesh3d
!!INTERFACE:
      module mesh3d
!!DESCRIPTION:
! mesh generators for k-space
!
!!USES:
      use mecca_types
!DEBUGPRINT
      use mpi
!DEBUGPRINT

!!DO_NOT_PRINT
      private
!!DO_NOT_PRINT

!!PUBLIC MEMBER FUNCTIONS:
      public :: gMPmesh,updateMPmesh  ! Monkhorst-Pack mesh generator
!!PRIVATE MEMBER FUNCTIONS:
! subroutine spkpt1
! subroutine bzdefi
! subroutine bzrduc
! function inbz
!!REVISION HISTORY:
! Initial Version - A.S. - 2013
!EOP
!
!BOC
      logical, external :: hexSymm 
      real(8), parameter :: eps=1.d-06  ! M-P parameter
      integer, parameter :: logunit=6
      integer :: ierr
!EOC
      contains

!BOP
!!IROUTINE: updateMPmesh
!!INTERFACE:
      subroutine updateMPmesh(nq,ks_in,mecca_state,invadd)
!!DESCRIPTION:
! makes copy from input {\tt ks\_in} (type KS\_Str) to {\tt mecca\_state\%ks\_box}
! and generates Monkhorst-Pack mesh with new parameters defined by input {\tt nq}
!

!!DO_NOT_PRINT
      implicit none
!!DO_NOT_PRINT

!!ARGUMENTS:
      integer, intent(in) :: nq(3)
      type(KS_Str), intent(in) :: ks_in
      type(RunState), target :: mecca_state
      integer, intent(in), optional :: invadd
!!REVISION HISTORY:
! Initial Version - A.S. - 2013
!EOP
!
!BOC
      type(KS_Str), pointer :: ks_out=>null()

!      real(8) :: rot(49,3,3)=0
!      integer :: ib(48)=0
!      complex(8), allocatable :: dop(:,:)   !  dop((lmax+1)*(lmax+1),(lmax+1)*(lmax+1),*)
!      integer, allocatable :: nqpt(:)             !  nqpt(nmesh)
!      real(8), allocatable :: qmesh(:,:,:)   !  qmesh(3,ndimq,nmesh)
!      integer, allocatable :: wghtq(:,:)    !  wghtq(ndimq,nmesh)
!      real(8), allocatable :: twght(:)       !  twght(nmesh)
!      integer, allocatable :: lrot(:,:)     !  lrot(48,:)
!      integer, allocatable :: ngrp(:),kptgrp(:,:)
!      integer, allocatable :: kptset(:,:),kptindx(:,:)
      integer nmesh,imesh,inv
      real(8), external :: g_pi
      real(8) :: pi2
      pi2 = 2.d0*g_pi()
      ks_out => mecca_state%ks_box
      call deallocKSstr(ks_out)
      ks_out%kslatt = ks_in%kslatt
      ks_out%nrot = ks_in%nrot
      ks_out%lmax = ks_in%lmax
      ks_out%rot(1:ks_out%nrot,:,:) = ks_in%rot(1:ks_out%nrot,:,:)
      ks_out%ib = ks_in%ib
      allocate( ks_out%dop(size(ks_in%dop,1),size(ks_in%dop,2)))
      ks_out%dop(:,1:ks_out%nrot) = ks_in%dop(:,1:ks_out%nrot)
      ks_out%ndimq   = abs(product(nq(1:3)))
      if ( sum(nq(1:3)/abs(nq(1:3)))==-3 ) ks_out%ndimq=ks_out%ndimq*2

      allocate( ks_out%nqpt(1) )
      allocate( ks_out%qmesh(1:3,1:ks_out%ndimq,1) ) 
      allocate( ks_out%wghtq(1:ks_out%ndimq,1) ) 
      allocate( ks_out%twght(1) ) 
      allocate( ks_out%lrot(1:ks_out%ndimq,1) ) 
      allocate( ks_out%ngrp(1) ) 
      allocate(ks_out%kptgrp(1:size(ks_in%kptgrp,1),1))
      allocate(ks_out%kptset(1:size(ks_in%kptset,1),1))
      allocate( ks_out%kptindx(1:ks_out%ndimq,1) )
      nmesh=1
      if ( mecca_state%intfile%nKxyz(1,1).ne.nq(1) .or.                 &
     &     mecca_state%intfile%nKxyz(2,1).ne.nq(2) .or.                 &     
     &     mecca_state%intfile%nKxyz(3,1).ne.nq(3) ) then
       ks_out%nqpt=0
       ks_out%qmesh=0
       ks_out%wghtq=0
       ks_out%twght=0
       ks_out%lrot=0
       ks_out%ngrp=0
       ks_out%kptgrp=0
       ks_out%kptset=0
       ks_out%kptindx=0
!
       inv = 0
       if ( present(invadd) ) then
           if ( invadd .ne. 0 ) inv = 1
       end if

       imesh=1
       call gMPmesh(nmesh,nq,mecca_state%rs_box%rslatt(:,1)/pi2,        &
     &         mecca_state%rs_box%rslatt(:,2)/pi2,                      &
     &         mecca_state%rs_box%rslatt(:,3)/pi2,                      &
     &         ks_out%kslatt(:,1),ks_out%kslatt(:,2),ks_out%kslatt(:,3),&
     &         inv,ks_out%nrot,ks_out%ib,ks_out%rot,ks_out%ndimq,       &
     &         ks_out%nqpt,ks_out%wghtq,ks_out%qmesh,ks_out%lrot)

       ks_out%twght(1) = sum(ks_out%wghtq(:,1))

       call grpkpnt(ks_out%nqpt(imesh),ks_out%wghtq(:,imesh),           &
     &   ks_out%lrot(:,imesh),                                          &
     &   ks_out%ngrp(imesh),ks_out%kptgrp(:,imesh),                     &
     &   ks_out%kptset(:,imesh),ks_out%kptindx(:,imesh))

      else
       ks_out%nqpt(1)    = ks_in%nqpt(1)
       ks_out%qmesh(1:3,1:ks_out%ndimq,1) =                             &
     &                             ks_in%qmesh(1:3,1:ks_out%ndimq,1)
       ks_out%wghtq(1:ks_out%ndimq,1)     =                             &
     &                                 ks_in%wghtq(1:ks_out%ndimq,1)
       ks_out%twght(1)   = ks_in%twght(1)
       ks_out%lrot(1:ks_out%ndimq,1)  = ks_in%lrot(1:ks_out%ndimq,1)
       ks_out%ngrp(1) = ks_in%ngrp(1)
       ks_out%kptgrp(1:size(ks_in%kptgrp,1),1)  =                       &
     &                        ks_in%kptgrp(1:size(ks_in%kptgrp,1),1)
       ks_out%kptset(1:size(ks_in%kptset,1),1)  =                       &
     &                        ks_in%kptset(1:size(ks_in%kptset,1),1)
       ks_out%kptindx(1:ks_out%ndimq,1) =                               &
     &                               ks_in%kptindx(1:ks_out%ndimq,1)
      end if

      return
!EOC
      end subroutine updateMPmesh

!BOP
!!IROUTINE: gMPmesh
!!INTERFACE:
      subroutine gMPmesh(nmesh,nq,a1in,a2in,a3in,b1,b2,b3,invadd,nrot,  &
     &                                    ib,r,nx,nktot,lwght,wvkl,lrot)
!!DESCRIPTION:
! generates Monkhorst-Pack (special k-points) mesh
!
! if all components {\tt nq(1),nq(2),nq(3)} are positive, then original
! special k-points (in reduced, by symmetry, zone) is generated; 
!
! if one of {\tt nq}-components is negative, then mesh is generated 
! for full zone integration;
!
! if two of {\tt nq}-components are negative, then MP-mesh is generated and 
! shifted to Gamma-point;
!
! if all three {\tt nq}-components are negative, then output mesh is 
! a union of original and shifted to Gamma-point MP-mesh;
!

!!DO_NOT_PRINT
      implicit none
!!DO_NOT_PRINT

!!ARGUMENTS:
      integer, intent(in) :: nmesh,nq(1:3,1:nmesh)
      real(8), intent(in), target :: a1in(3),a2in(3),a3in(3)  ! direct-space latt
      real(8), intent(in) :: b1(3),b2(3),b3(3)  ! conjug.-space latt
      integer, intent(in) :: invadd
      integer, intent(in) :: nrot               ! number of group operations
      integer, intent(in) :: ib(*)              ! group operation number for r()-array
      real(8), intent(in) :: r(*)               ! rotation matrices (49,3,3)
      integer, intent(in) :: nx
      integer, intent(out) :: nktot(nmesh)         ! total for each mesh
      integer, intent(out) :: lwght(nx,nmesh)    ! weights
      real(8), intent(out) :: wvkl(3,size(lwght,1),nmesh) ! points coordinates
      integer, intent(out) :: lrot(size(lwght,1),nmesh)   ! 
!!REVISION HISTORY:
! Adapted - A.S. - 1997
!!REMARKS:
! it is assumed that none of components nq(1),nq(2),nq(3) is equal to zero;
! if nq(1:3)==1 then mesh has one k-point {0,0,0};
!EOP
!
!BOC

!c     /         '  Generation of special points',
!c     /'  (iq1,iq2,iq3 are the (generalized) monkhorst-pack parameters'
!c      (they are not multiplied by 2*pi because b1,2,3  were not,either)
      real(8), pointer :: a1(:)=>null(),a2(:)=>null(),a3(:)=>null()
      integer :: ilog,imesh,iout,i,l,nc,nminus,nshift,iq1,iq2,iq3
      integer :: kcheck(size(lwght,1))
      integer :: istriz=0
      real(8) :: ur1,ur2,ur3,wvk0(3,2),proj1,proj2,proj3
      character(10), parameter :: sname='gMPmesh'

      a1=>a1in
      a2=>a2in
      a3=>a3in

      ilog = logunit
      do imesh=1,nmesh
       nc = nrot
       iq1 = abs(nq(1,imesh))
       iq2 = abs(nq(2,imesh))
       iq3 = abs(nq(3,imesh))
       ur1 = 1.d0/dble(2*iq1)
       ur2 = 1.d0/dble(2*iq2)
       ur3 = 1.d0/dble(2*iq3)

       ! The number of minus signs define the mesh

       ! for non-hex. systems ( hexSymm is .false. )
        
       ! 0 = standard Monkhorst-Pack
       ! 1 = Full Zone integration (i.e. no symm reduction)
       ! 2 = Shift the grid to include Gamma point
       ! 3 = Use both shifted and unshifted grids in conjunction

       ! for hex. systems ( hexSymm is .true.)

       ! 0,1 = Shift the grid to include Gamma point
       ! 2 = standard Monkhorst-Pack
       
       nminus = 0
       do i=1,3
        if(nq(i,imesh).ne.abs(nq(i,imesh))) nminus = nminus+1
       end do

       wvk0 = 0.d0
       if ( iq1==1 .and. iq2==1 .and. iq3==1 ) then
         nshift = 1
         do i=1,3
          wvk0(i,1) = -ur1*b1(i) - ur2*b2(i) - ur3*b3(i)
         end do
       else
        if(nminus.eq.0) then
         nshift = 1
         if ( hexSymm() ) then
          do i=1,3
           wvk0(i,1) = -ur1*b1(i) - ur2*b2(i) - ur3*b3(i)
          end do
         end if
        else if(nminus.eq.1) then           ! Full Zone integration
         nshift = 1
         if ( hexSymm() ) then
          do i=1,3
           wvk0(i,1) = -ur1*b1(i) - ur2*b2(i) - ur3*b3(i)
          end do
         end if
         nc = 1
        else if(nminus.eq.2) then           ! Shift to Gamma-point
         nshift = 1
         if ( .not. hexSymm() ) then
          do i=1,3
           wvk0(i,1) = -ur1*b1(i) - ur2*b2(i) - ur3*b3(i)
          end do
         end if
        else                                ! Unshift.+Shift_to_Gamma
         wvk0(:,1) = -ur1*b1(:) - ur2*b2(:) - ur3*b3(:)
         if ( hexSymm() ) then
          nshift = 1
         else
          nshift = 2
          wvk0(:,2) = 0.d0
         end if
         
        end if
       end if

!cab       istriz = 0

       if ( imesh>1 ) then
        iout = -1 
       else
        iout = 1
       end if
       call spkpt1(iout,iq1,iq2,iq3,wvk0,nshift,size(lwght,1),          &
     &      b1,b2,b3,                                                   &
     &      invadd,nc,ib,r,nktot(imesh),wvkl(:,:,imesh),lwght(:,imesh), &
     &      kcheck,lrot(:,imesh),istriz)

       if ( ilog >=0 ) then
        write(ilog,20) imesh,iq1,iq2,iq3,nktot(imesh)
20      format(' IMESH=',i3,' IQ1=',i3,' IQ2=',i3,' IQ3=',i3,           &
     &  ' ==> number of special k-pts = ',i5)
       end if

       if (nktot(imesh) .le. 0) then
        write(*,40) nktot(imesh),size(lwght,1)
40      format(1x,'dimensions special k points nk, nkx ',2i5)
        call fstop(sname//' NK .LE. 0')
       endif

!c     set near-zeroes equal to zero:

       do l=1,nktot(imesh)
        do i = 1,3
         if (abs(wvkl(i,l,imesh)) .lt. eps) wvkl(i,l,imesh) = 0.0d0
        end do

!c       express special points in basis
!       snk: what's the point of this??
        proj1 = dot_product(wvkl(1:3,l,imesh),a1(1:3))
        proj2 = dot_product(wvkl(1:3,l,imesh),a2(1:3))
        proj3 = dot_product(wvkl(1:3,l,imesh),a3(1:3))
        do i=1,3
          wvkl(i,l,imesh) = proj1*b1(i) + proj2*b2(i) + proj3*b3(i)
        end do 
       end do 

      end do   ! imesh-cycle

      nullify(a1,a2,a3)

!DEBUGPRINT
       call mpi_barrier(mpi_comm_world, ierr)
!DEBUGPRINT
      return
!EOC
      contains

!BOP
!!IROUTINE: spkpt1
!!INTERFACE:
      subroutine spkpt1(ilog,iq1,iq2,iq3,wvk0,nshift,nkx,               &
     &                  b1,b2,b3,                                       &
     &                  inv,nc,ib,r,ntot,wvkl,lwght,kcheck,lrot,istriz)
!!DESCRIPTION:
! {\bv
! generation of special points for an arbitrary lattice, following the
! method of monkhorst and pack, phys. rev. b13 (1976) 5188 modified by
! macdonald, phys. rev. b18 (1978) 5897
! in the cases where the point group of the crystal does not contain
! inversion, the latter is added (see comment to the switch inv).
! reduction to the 1st Brillouin zone is done by adding g-vectors
! to find the shortest wave-vector.
! the rotations of the bravais lattice are applied to the monkhorst-pack
! mesh in order to find all k-points that are related by symmetry
! \ev}
!EOP
!
! {\bv
! input data:
!      iq1,iq2,iq3 .. parameter q of monkhorst and pack,
!               generalized and different for the 3 directions b1,
!               b2 and b3
!      wvk0 ... the 'arbitrary' shift of the whole mesh, denoted k0
!               in macdonald. wvk0 = 0 corresponds to the original
!               scheme of monkhorst and pack.
!               units: 2pi/(units of length  used in a1, a2, a3),
!               i.e. the same  units as the generated special points.
!      nkx  ... variable dimension of the (output) arrays wvkl,
!               lwght,lrot, i.e. space reserved for the special
!               points and accessories.
!               it has to be nkx .ge. ntot (total number of special
!               points), but the subroutine does not check on this.
!      istriz . indicates whether additional mesh points should be
!               generated by applying group operations to the mesh
!               abs(istriz) .eq. 1 means symmetrize
!               abs(istriz) .ne. 1 means don't symmetrize
!                   istriz  .lt. 0 means don't exclude equivalent points
!      b1,b2,b3 .. reciprocal lattice vectors, not multiplied by
!               any 2pi (in units reciprocal to those of a1,a2,a3)
!      inv .... code indicating whether we wish  to add the inversion
!               to the point group of the crystal or not (in the
!               case that the point group does not contain any).
!               inv=0 means: do not add inversion
!               inv.ne.0 means: add the inversion
!               inv.ne.0 should be the standard choice when spkpt
!               is used in reciprocal space - in order to make
!               use of the hermiticity of hamiltonian.
!               when used in direct space, the right choice of inv
!               will depend on the nature of the physical problem.
!               in the cases where the inversion is added by the
!               switch inv, the list ib will not be modified but in
!               the output list lrot some of the operations will
!               appear with negative sign; this means that they have
!               to be applied multiplied by inversion.
!      nc ..... total number of elements in the point group of the
!               crystal
!      ib ..... list of the rotations constituting the point group
!               of the crystal. the numbering is that defined in
!               worlton and warren, i.e. the one materialized in the
!               array r (see below)
!               only the first nc elements of the array ib are
!               meaningful
!      r ...... list of the 3 x 3 rotation matrices
!               (xyz representation of the o(h) or d(6)h groups)
!               all 48 or 24 matrices are listed.
!      ncbrav . total number of elements in rbrav
!      ibrav .. list of ncbrav operations of the bravais lattice
! output data:
!      ntot ... total number of special points
!               if ntot appears negative, this is an error signal
!               which means that the dimension nkx was chosen
!               too small so that the arrays wvkl etc. cannot
!               accomodate all the generated special points.
!               in this case the arrays will be filled up to nkx
!               and further generation of new points will be
!               interrupted.
!      wvkl ... list of special points .
!               cartesian coordinates and not multiplied by 2*pi.
!               only the first ntot vectors are meaningful
!               although no 2 points from the list are equivalent
!               by symmetry, this subroutine still has a kind of
!               'beauty defect': the points finally
!               selected are not necessarily situated in a
!               'compact' irreducible brill.zone; they might lie in
!               different irreducible parts of the b.z. - but they
!               do represent an irreducible set for integration
!               over the entire b.z.
!     lwght ... the list of weights of the corresponding points.
!               these weights are not normalized (just integers)
!      lrot ... for each special point the 'unfolding rotations'
!               are listed. if e.g. the weight of the i-th special
!               point is lwght(i), then the rotations with numbers
!                     (lrot -- between 1 and nc)
!               lrot(j,i), j=1,2,...,lwght(i) will 'spread' this
!               single point from the irreducible part of b.z. into
!               several points in an elementary unit cell
!               (parallelopiped) of the reciprocal space.
!               some operation numbers in the list lrot may appear
!               negative, this means that the corresponding rotation
!               has to be applied with inversion (the latter having
!               been artificially added as symmetry operation in
!               case inv.ne.0).no other effort was taken,to renumber
!               the rotations with minus sign or to extend the
!               list of the point-group operations in the list nb.
! \ev}

!EOP
!
!BOC
!DEBUG      implicit real*8 (a-h,o-z)
      implicit none
!      parameter(eps=1.d-6)
      integer :: nshift
      real(8), intent(in) :: b1(3),b2(3),b3(3)
      real(8) :: wvk0(3,nshift),r(49,3,3)
      integer :: ilog,iq1,iq2,iq3
      integer :: ib(48)
      integer :: inv,nc,ntot,istriz
      integer :: nkx
      real(8) :: wvkl(3,nkx)
      integer :: lwght(nkx),kcheck(nkx)
      integer :: lrot(nkx)

      real(8) :: diff,ur1,ur2,ur3,summ,wvkmax
      integer :: i,i1,i2,i3,inv0,iout,iqp1,iqp2,iqp3,ishift,iremov,j
      integer :: ibsign,k,kcount,m,n,ninv,nlrot
!DELETE      integer, parameter :: nrsdir=200
      integer :: nrsdir
      integer, save :: nplane
      real(8), allocatable, save :: rsdir(:,:)   ! (4,nrsdir)
      real(8) :: wvk(3),wva(3),proja(3),projb(3)

      character*10 sname/'spkpt'/

!cab
      iout = abs(ilog)
!cab

!c     define the 1st brillouin zone
      if ( ilog>=0 ) then
       call bzdefi(b1,b2,b3,rsdir,nplane,iout)
      end if
      nrsdir = nplane

!c     generation of the mesh (they are not multiplied by 2*pi)
!c     by the monkhorst/pack algorithm, supplemented by all rotations

!DELETE!c     zero index array
!DELETE      do k=1,nkx
!DELETE      kcheck(k) = 0
!DELETE      enddo

!c     odd iqs should not end up on origin
      iqp1=iq1
      iqp2=iq2
      iqp3=iq3
      if( mod(iq1,2).eq.1 ) iqp1=iq1-1
      if( mod(iq2,2).eq.1 ) iqp2=iq2-1
      if( mod(iq3,2).eq.1 ) iqp3=iq3-1

      kcount = 0
      do i1=1,iq1
       do i2=1,iq2
        do i3=1,iq3

         ur1=dble(1 + iqp1 - 2*i1)/dble(2*iq1)
         ur2=dble(1 + iqp2 - 2*i2)/dble(2*iq2)
         ur3=dble(1 + iqp3 - 2*i3)/dble(2*iq3)

         do ishift=1,nshift

          do i=1,3
          wvk(i)=ur1*b1(i)+ur2*b2(i)+ur3*b3(i) + wvk0(i,ishift)
          enddo

!c     reduce wvk to the 1st brillouin zone
          call bzrduc(wvk,a1,a2,a3,b1,b2,b3,rsdir,nrsdir,nplane)

          if (abs(istriz) .eq. 1) then
           write(*,*) ' ISTRIZ=',istriz
           write(*,*) ' It will work only when the same <type>',        &
     &                ' of symmetry operations'
           write(*,*) ' (cubic/hexagonal) will apply to the',           &
     &                ' crystal as well as the bravais lattice,'
           write(*,*) ' i.e. when rbrav(*) == r(*)'
       call fstop(sname//': |istriz|=1 is not implemented properly')
!CAB
!CABc     apply all the bravais lattice operations to wvk
!CAB            do iop = 1,ncbrav
!CAB             do i=1,3
!CAB              wva(i) = 0.0d0
!CAB                do j = 1,3
!CAB                wva(i) = wva(i) + r(ibrav(iop),i,j)*wvk(j)
!CAB                enddo
!CAB             enddo
!CAB
!CABc     check that wva is inside the 1st bz.
!CAB             if (inbz(wva,rsdir,nrsdir,nplane) .eq. 0)
!CAB     +       call fstop(sname//' rotated k point outside 1st bz')
!CAB
!CABc     include the very first point
!CAB             if (i1.eq.1.and.i2.eq.1.and.i3.eq.1.and.iop.eq.1) then
!CAB              kcount = kcount + 1
!CAB              wvkl(1,kcount) = wva(1)
!CAB              wvkl(2,kcount) = wva(2)
!CAB              wvkl(3,kcount) = wva(3)
!CAB              kcheck(kcount) = 1
!CAB             else
!CABc     has this point been encountered before?
!CAB              do j=1,kcount
!CAB                sum = abs(wva(1) - wvkl(1,j)) +
!CAB     +                abs(wva(2) - wvkl(2,j)) +
!CAB     +                abs(wva(3) - wvkl(3,j))
!CAB                if (sum .lt. eps) goto 10
!CAB              enddo
!CABc     this is a new point
!CAB              kcount = kcount + 1
!CAB              if (kcount .gt. nkx) call fstop(sname//'dimensions: nkx !')
!CAB              wvkl(1,kcount) = wva(1)
!CAB              wvkl(2,kcount) = wva(2)
!CAB              wvkl(3,kcount) = wva(3)
!CAB              kcheck(kcount) = 1
!CAB10            continue
!CAB
!CAB             endif
!CAB            enddo
!CAB

          else

            kcount = kcount + 1
            if( kcount .gt. nkx ) then
              write(logunit,'('' 2nd kcount,nkx '',2i10)') kcount,nkx
              call fstop(sname//' dimensions: nkx')
            endif
            wvkl(1,kcount) = wvk(1)
            wvkl(2,kcount) = wvk(2)
            wvkl(3,kcount) = wvk(3)
            kcheck(kcount) = 1

          endif

         enddo                     ! ishift-enddo

        enddo
       enddo
      enddo

      if ( iout==logunit ) then
       write (iout,20) kcount
20     format(2x,'the wavevector mesh contains ',i7,' points')
      end if

      if (abs(istriz) .eq. 1) then

!c     figure out if any special point difference (k - k') is an
!c     integral multiple of a reciprocal-space vector

       iremov = 0
       do i=1,(kcount-1)

        do k=1,3
        wva(k) = wvkl(k,i)
        enddo

!c     project wva onto b1,2,3:
        proja(1) = 0.d0
        proja(2) = 0.d0
        proja(3) = 0.d0
        do k=1,3
        proja(1) = proja(1) + wva(k)*a1(k)
        proja(2) = proja(2) + wva(k)*a2(k)
        proja(3) = proja(3) + wva(k)*a3(k)
        enddo

!c     loop over the rest of the mesh points
        do 30 j=(i+1),kcount
        if (kcheck(j) .eq. 0) goto 30

          do k=1,3
          wvk(k) = wvkl(k,j)
          enddo

!c     project wvk onto b1,2,3:
          projb(1) = 0.d0
          projb(2) = 0.d0
          projb(3) = 0.d0
          do k=1,3
          projb(1) = projb(1) + wvk(k)*a1(k)
          projb(2) = projb(2) + wvk(k)*a2(k)
          projb(3) = projb(3) + wvk(k)*a3(k)
          enddo

!c     check whether (proja-projb) is integral?
          do k=1,3
          diff = proja(k) - projb(k)
          if (abs(nint(diff)-diff) .gt. eps) goto 30
          enddo
!c     diff is integral: remove wvk from mesh
          kcheck(j) = 0
          iremov = iremov + 1
30        continue
       enddo

      if ( iremov .gt. 0 .and. iout==logunit ) then
      write (iout,40) iremov
40    format(2x,'some of these mesh points are related by lattice transl&
     &ation vectors'/1x,i6,' of the mesh points removed.'/)
      end if

!c     reshuffle
      j=0
      do 50 i=1,kcount
      if (kcheck(i) .eq. 0) goto 50
      j=j+1
        do k=1,3
        wvkl(k,j) = wvkl(k,i)
        enddo
        kcheck(j) = kcheck(i)
50    continue
      kcount = j

      endif

!c     in the mesh of wavevectors, now search for equivalent points:
!c     the inversion (time reversal !) may be used.

      if(inv.ne.0) then
       ninv = 2                         ! to add inversion
      else
       ninv = 1                         ! no "artificial" inversion
      end if

      ntot = 0
      nlrot = 0
      do 60 k=1,kcount
        if (kcheck(k) .eq. 0) cycle

        ntot = ntot + 1
        nlrot = nlrot+1
        lwght(ntot) = 1
!CAB              lrot(lwght(ntot),ntot) = 1
        lrot(nlrot) = 1

        if (istriz.lt.0) cycle
        if (k .eq. kcount) exit
        wvkmax = max(abs(wvkl(1,k)),abs(wvkl(2,k)),abs(wvkl(3,k)))
        if (wvkmax .lt. eps) cycle            ! to exclude k=(0,0,0)

!c         find all the equivalent points
        do 70 n=1,nc
!c          rotate:

          do i=1,3
           wvk(i) = 0.d0
           do j=1,3
            wvk(i) = wvk(i) + r(ib(n),i,j)*wvkl(j,k)
           enddo
          enddo

          do inv0 = 1,ninv

           if(inv0.eq.1) then
            wva = wvk
            ibsign = 1
           else
            wva = -wvk
            ibsign = -1
           end if

           if(ib(n).ne.1.or.inv0.ne.1) then


!CDEBUG          ibsign = 1
!c          find out which point this is (could have been discarded!)
!CDEBUG85        continue
            do 80 m=k+1,kcount
             if (kcheck(m) .eq. 0) goto 80
             summ = abs(wva(1)-wvkl(1,m)) +                             &
     &            abs(wva(2)-wvkl(2,m)) +                               &
     &            abs(wva(3)-wvkl(3,m))
             if (summ .lt. eps) then
              lwght(ntot) = lwght(ntot) + 1
!CAB              lrot(lwght(ntot),ntot) = ib(n) * ibsign
              nlrot = nlrot+1
              lrot(nlrot) = n * ibsign
              kcheck(m) = 0
!CDEBUG              goto 75
              exit
             endif
80          continue

!c          use time-reversal symmetry if the inversion does not exit
!CDEBUG75        if (inv .eq. 1 .and. ibsign .eq. 1) then
!CDEBUG75        if (inv .eq. 1
!CDEBUG     *       .and. ibsign .eq. 1
!CDEBUG     *       .and. lrot(nlrot).eq.n
!CDEBUG     *       ) then
!CDEBUG           ibsign = -1
!CDEBUG           do i=1,3
!CDEBUG            wva(i) = -wva(i)
!CDEBUG           enddo
!CDEBUG           goto 85
!CDEBUG          endif
           end if
          end do                        ! "inversion" loop

70      continue
60    continue

      if(nlrot.gt.nkx) then
        write(logunit,*) ' NLROT=',nlrot
        write(logunit,*) ' NKX=',nkx
        call p_fstop(sname//':: wrong dimension for LROT?')
      end if

!c     reshuffle
      j=0
      do 90 i=1,kcount
      if (kcheck(i) .eq. 0) goto 90
      j=j+1
        do k=1,3
        wvkl(k,j) = wvkl(k,i)
        enddo
        kcheck(j) = kcheck(i)
90    continue
      kcount = j

      if (kcount .ne. ntot) then
        write(logunit,'('' kcount,ntot '',2i15)') kcount,ntot
        call p_fstop(sname//' error number of k points')
      endif

!c     total number of special points: ntot
!c     before using the list wvkl as wave vectors, they have to be
!c     multiplied by 2*pi
!c     the list of weights lwght is not normalized

      return

!EOC
      end subroutine spkpt1

      end subroutine gMPmesh


!BOP
!!IROUTINE: bzdefi
!!INTERFACE:
      subroutine bzdefi(b1,b2,b3,rsdir,nplane,iout)
!!DESCRIPTION:
! find the vectors whose halves define the 1st Brillouin zone
!
!EOP
! on output, nplane tells how many elements of rsdir contain
!            normal vectors defining the planes
! method: starting with the parallelopiped spanned by b1,2,3
! around the origin, vectors inside a sufficiently large sphere
! are tested to see whether the planes at 1/2*b will
! further confine the 1bz.
! the resulting vectors are not cleaned to avoid redundant planes.
!

!EOP
!
!BOC
      implicit none

      real(8), intent(in) :: b1(3),b2(3),b3(3)
      integer, intent(in) :: iout
      real(8), allocatable, intent(out) :: rsdir(:,:) ! (4,nrsdir)
      integer, intent(out) :: nplane
      integer :: nrsdir
      real(8) :: bvec(3),b1len,b2len,b3len,bmax,projct
      integer :: i,i1,i2,i3,j,n,n1,n2,n3
      integer :: nb1,nb2,nb3
      integer :: nnb1,nnb2,nnb3
!      parameter ( eps = 1.0d-6 )
!c
!c-----------------------------------------------------------------------
      b1len = b1(1)**2 + b1(2)**2 + b1(3)**2
      b2len = b2(1)**2 + b2(2)**2 + b2(3)**2
      b3len = b3(1)**2 + b3(2)**2 + b3(3)**2
!c     lattice containing entirely the brillouin zone
      bmax = b1len + b2len + b3len
      nb1 = int( sqrt(bmax/b1len) + eps)
      nb2 = int( sqrt(bmax/b2len) + eps)
      nb3 = int( sqrt(bmax/b3len) + eps)
      nnb1 = 2*nb1 + 1
      nnb2 = 2*nb2 + 1
      nnb3 = 2*nb3 + 1
      nrsdir = nnb1*nnb2*nnb3+3
      if ( allocated(rsdir) ) deallocate(rsdir)
      allocate(rsdir(4,nrsdir))
!c     1bz is certainly confined inside the 1/2(b1,b2,b3) parallelopiped
      do i = 1,3
        rsdir(i,1) = b1(i)
        rsdir(i,2) = b2(i)
        rsdir(i,3) = b3(i)
      enddo
      rsdir(4,1) = b1len
      rsdir(4,2) = b2len
      rsdir(4,3) = b3len
!c     starting confinement: 3 planes
      nplane = 3
!c
      do 150 n1 = 1,nnb1
        i1 = nb1 + 1 - n1
        do 150 n2 = 1,nnb2
        i2 = nb2 + 1 - n2
        do 150 n3 = 1,nnb3
        i3 = nb3 + 1 - n3
        if (i1.eq.0 .and. i2.eq.0 .and. i3.eq.0) cycle
        do i = 1,3
          bvec(i) = dble(i1)*b1(i) + dble(i2)*b2(i) +                   &
     &              dble(i3)*b3(i)
        enddo
!c       does the plane of 1/2*bvec narrow down the 1bz ?
        do n = 1,nplane
          projct = 0.5d0*(rsdir(1,n)*bvec(1) + rsdir(2,n)*bvec(2) +     &
     &                  rsdir(3,n)*bvec(3) ) / rsdir(4,n)
!c         1/2*bvec is outside the bz - skip this direction
!c         the 1.0e-6 takes care of single points touching the bz, and
!c         of the -(plane)
          if (abs(projct) .gt. 0.5d0 - eps) cycle
        enddo
!c       1/2*bvec further confines the 1bz - include into rsdir
        nplane = nplane + 1
!c       print *,nplane,' plane included, i1,2,3 = ',i1,i2,i3
        if (nplane .gt. nrsdir) goto 470
        rsdir(1:3,nplane) = bvec(1:3)
!c       length squared
        rsdir(4,nplane) = dot_product(bvec,bvec)
150     continue
!c
!c     print information
      if ( iout == 6 ) then
       write (iout,160) nplane,((rsdir(i,n),i=1,3),n=1,nplane)
160    format(/2x,'The 1st brillouin zone is confined by (at most)',    &
     & i3,' planes'/                                                    &
     & ' as defined by the +/- halves of the vectors:'/100(1x,3f10.4/) )
      end if
      return
!c-----------------------------------------------------------------------
!c     error messages
470   write (*,480) nrsdir
480   format(' bzdefi :: too many planes, increase nrsdir = ',i5)
      call fstop(' fatal error in BZDEFI')
!c
      return
!EOC
      end subroutine bzdefi

!BOP
!!IROUTINE: bzrduc
!!INTERFACE:
      subroutine bzrduc(wvk,a1,a2,a3,b1,b2,b3,rsdir,                    &
     &                  nrsdir,nplane)
!!DESCRIPTION:
! reduce wvk to lie entirely within the 1st Brillouin zone
! by adding b-vectors
!
!EOP
!
!BOC
      implicit none

      real(8) :: wvk(3),a1(3),a2(3),a3(3),b1(3),b2(3),b3(3)
      integer :: nrsdir,nplane
      real(8) :: rsdir(4,nrsdir)
      real(8) :: wva(3),wb(3)
      integer :: i,i1,i2,i3,k,n1,n2,n3,nn1,nn2,nn3
!c     look around +/- "nzones" to locate vector
      integer, parameter :: nzones=2, nnn=2*nzones+1, nn=nzones+1
      integer, parameter :: yes = 1, no = 0
!c
!c-----------------------------------------------------------------------
!c     wvk already inside 1bz
      if (inbz(wvk,rsdir,nrsdir,nplane) .eq. yes) return
!c
!c     express wvk in the basis of b1,2,3.
!c     this permits an estimate of how far wvk is from the 1bz.
      wb(1) = wvk(1)*a1(1) + wvk(2)*a1(2) + wvk(3)*a1(3)
      wb(2) = wvk(1)*a2(1) + wvk(2)*a2(2) + wvk(3)*a2(3)
      wb(3) = wvk(1)*a3(1) + wvk(2)*a3(2) + wvk(3)*a3(3)
      nn1 = nint(wb(1))
      nn2 = nint(wb(2))
      nn3 = nint(wb(3))
!c
!c     look around the estimated vector for the one truly inside the 1bz
      do n1 = 1,nnn
      i1 = nn - n1 - nn1
      do n2 = 1,nnn
      i2 = nn - n2 - nn2
      do n3 = 1,nnn
      i3 = nn - n3 - nn3
      do i = 1,3
        wva(i) = wvk(i) + dble(i1)*b1(i) + dble(i2)*b2(i) +             &
     &           dble(i3)*b3(i)
      enddo
      k = inbz(wva,rsdir,nrsdir,nplane)
      if ( nplane<=nrsdir ) then
        if ( k == yes ) goto 210
      else 
       go to 201
      end if

      enddo
      enddo
      enddo

!c-----------------------------------------------------------------------
!c     fatal error
      if ( nplane<=nrsdir ) then
       write (*,200) wvk
200    format(' bzrduc :: *** fatal error ***'/                         &
     &         ' wavevector ',3f10.4,' could not be reduced to the 1bz')
       write(*,*) ' NRSDIR=',nrsdir
       write(*,*) ' NPLANE=',nplane
       do i=1,nplane
        write(*,*) (real(rsdir(k,i)),k=1,4)
       end do
      end if
201   continue
      if ( nplane>nrsdir ) then
       write (*,205) nrsdir
205    format(' bzrduc :: too many vectors inside the 1bz',             &
     &      ', increase nrsdir = ',i5)
      end if
      call fstop(' fatal error in BZRDUC')
!c
!c-----------------------------------------------------------------------
!c     the reduced vector
210   do i = 1,3
        wvk(i) = wva(i)
      enddo
      return
!EOC
      end subroutine bzrduc

!BOP
!!IROUTINE: inbz
!!INTERFACE:
      function inbz(wvk,rsdir,nrsdir,nplane)
!!DESCRIPTION:
! checks if point {\tt wvk} is in the 1st Brillouin zone
!
!EOP
!
!BOC
      implicit none
   
      integer :: inbz
      integer :: nrsdir,nplane
      real(8) :: wvk(3),rsdir(4,nrsdir)
!c
!c     is wvk in the 1st brillouin zone ?
!c     check whether wvk lies inside all the planes that define the 1bz.
!c
      integer :: n
      real(8) :: projct
      integer yes,no
      parameter ( yes = 1, no = 0 )
!      parameter ( eps = 1.0d-6 )
!c-----------------------------------------------------------------------
      inbz = no
      do 320 n = 1,nplane
        projct = (rsdir(1,n)*wvk(1) + rsdir(2,n)*wvk(2) +               &
     &            rsdir(3,n)*wvk(3) ) / rsdir(4,n)
!c       wvk is outside the bz
        if (abs(projct) .gt. 0.5d0 + eps) return
320     continue
      inbz = yes
      return
!EOC
      end function inbz

      end module mesh3d
