      subroutine ge2ctrpl(a,ndima,n,eps,value,index,ne,iflag)
      implicit none
!c
!c    general matrix --> compressed triplet storage
!c
      integer ndima,n,ne,iflag
      real*8 eps
!c       complex*16 a(ndima,n)
      complex*16 a(*)

      integer     index(*)
      complex*16  value(*)

      character sname*10
      parameter (sname='gen2ctrpl')

!c      integer erralloc
      integer i,j,j0,k
      complex*16  aij

      if(iflag.eq.1) then
       ne=0
       do j=1,n
        j0 = (j-1)*ndima
        do i=1,n
         aij = a(i+j0)
         if(max(abs(dreal(aij)),abs(dimag(aij))).gt.eps) then
          ne=ne+1
         else
          a(i+j0) = (0.d0,0.d0)
         end if
        end do
       end do
       return
      end if

      if(iflag.eq.2) then

       k = 0
       do j=1,n
        j0 = (j-1)*ndima
        do i=1,n
         aij = a(i+j0)
         if(max(abs(dreal(aij)),abs(dimag(aij))).gt.eps) then
          k = k+1
          value(k) = aij
          index(k) = i
          index(k+ne) = j
         end if
        end do
       end do

       return
      end if

      iflag = 0
      return
      end

      subroutine ge2crs(a,ndima,n,eps,value,iptrow,indcol,ne,iflag)
      implicit none
!c
!c    general matrix --> compressed row storage
!c
      integer ndima,n,ne,iflag
      real*8 eps
!c       complex*16 a(ndima,n)
      complex*16 a(*)

      integer     indcol(*),iptrow(*)
      complex*16  value(*)

      character sname*10
      parameter (sname='gen2crs')

!c      integer erralloc
      integer i,i0,j,j0,k
      complex*16  aij

      if(iflag.eq.1) then
       ne=0
       do j=1,n
        j0 = (j-1)*ndima
        do i=1,n
         aij = a(i+j0)
         if(max(abs(dreal(aij)),abs(dimag(aij))).gt.eps) then
          ne=ne+1
         else
          a(i+j0) = (0.d0,0.d0)
         end if
        end do
       end do
       return
      end if

      if(iflag.eq.2) then

       k = 0
       iptrow(1) = 1
       do i=1,n
        i0 = i-ndima
        do j=1,n
         aij = a(i0+j*ndima)
         if(max(abs(dreal(aij)),abs(dimag(aij))).gt.eps) then
          k = k+1
          value(k) = aij
          indcol(k) = j
         end if
        end do
        iptrow(i+1) = k+1
       end do

       return
      end if

      iflag = 0
      return
      end

      subroutine ge2ccs(a,ndima,n,eps,value,iptcol,indrow,ne,iflag)
      implicit none
!c
!c    general matrix --> compressed column storage
!c
      integer ndima,n,ne,iflag
      real(8) :: eps
!c       complex*16 a(ndima,n)
      complex(8) :: a(*)

      integer     indrow(*),iptcol(*)
      complex(8) ::  value(*)

      character sname*10
      parameter (sname='gen2ccs')

!c      integer erralloc
      integer i,i0,j,j0,k
      complex(8) ::  aij
      real(8) :: re_aij,im_aij

      if(iflag.eq.1) then
       ne=0
       do j=1,n
        j0 = (j-1)*ndima
        do i=1,n
         aij = a(i+j0)
         re_aij = dreal(aij)
         im_aij = dimag(aij)
         if ( re_aij<-eps.or.re_aij>eps.or.im_aij<-eps.or.im_aij>eps )  &
     &     then 
!!         if(max(abs(dreal(aij)),abs(dimag(aij))).gt.eps) then
          ne=ne+1
         else
          a(i+j0) = (0.d0,0.d0)
         end if
        end do
       end do
       return
      end if

      if(iflag.eq.2) then

       k = 0
       iptcol(1) = 1
       do j=1,n
        j0 = (j-1)*ndima
        do i=1,n
         aij = a(i+j0)
!!         if(max(abs(dreal(aij)),abs(dimag(aij))).gt.eps) then
         if ( re_aij<-eps.or.re_aij>eps.or.im_aij<-eps.or.im_aij>eps )  &
     &     then 
          k = k+1
          value(k) = aij
          indrow(k) = i
         end if
        end do
        iptcol(j+1) = k+1
       end do

       return
      end if

      iflag = 0
      return
      end

