!BOP
!!MODULE: initpot
!!INTERFACE:
      module initpot
!!DESCRIPTION:
! provides subroutines to generate starting potentials
!

!!USES:
      use mecca_constants
      use mecca_types
      use atom,   only : get_atom, atom_rdata
      use scf_io, only : check_version,c_dlm,c_up,c_dn
      use gfncts_interface, only : g_zvaltot,g_efestim
      use mecca_interface

!!DO_NOT_PRINT
      implicit none
!!DO_NOT_PRINT

!!PUBLIC MEMBER FUNCTIONS:
! init_potential
! getpot1
! potred
! getpot7
! genRmesh
! potred7
! estimatEbot
! getThoFerPot

!!PRIVATE MEMBER FUNCTIONS:
!!PUBLIC DATA MEMBERS:
!      integer :: initp = mecca_ames
      integer :: initp = mecca_2005
      logical :: thomas_fermi  = .false.
!      logical :: thomas_fermi  = .true.
!!REVISION HISTORY:
! Initial Version - A.S. - 2013
!EOP
!
!BOC

      type(RunState), pointer :: M_S => null()
      integer, parameter :: io22 = 1  ! unit number used to read files
      logical :: open22 = .false.
      integer :: ipot_read=0             ! POT file does not exist
      real(8), external :: ylag
      interface
       subroutine coreOrder(numc,enc,nc,lc,kc,n_hcl)
       implicit none
       integer :: numc
       real(8) :: enc(numc)
       integer :: nc(numc),lc(numc),kc(numc)
       integer, optional :: n_hcl
       end subroutine
      end interface
!EOC
      contains

!BOP
!!IROUTINE: init_potential
!!INTERFACE:
      subroutine init_potential(mecca_state)
!!DESCRIPTION:
! main module subrotine for generation of appropriate starting SCF potentials
!
!!REVISION HISTORY:
! Initial Version - A.S. - 2013
!EOP
!
!BOC
      implicit none
      type(RunState), target :: mecca_state

      integer :: nspin
      integer :: nbasis
      real(8) :: alat
      integer, dimension(mecca_state%intfile%nsubl) :: komp
      integer, dimension(mecca_state%intfile%nsubl) :: numbsub
      integer, allocatable :: jmt(:,:)
      integer, allocatable :: jws(:,:)
!      integer, allocatable :: numc(:,:,:)
!      integer, allocatable :: nc(:,:,:,:)
!      integer, allocatable :: lc(:,:,:,:)
!      integer, allocatable :: kc(:,:,:,:)
!c
      real(8), dimension(mecca_state%intfile%nsubl) :: rmtstr
      real(8), allocatable :: atcon(:,:)
!      real(8), allocatable :: ztot(:,:)
!      real(8), allocatable :: zcor(:,:)
      real(8), allocatable :: rcs(:,:)
      real(8), allocatable :: rws(:,:)
      real(8), allocatable :: rmt(:,:)
      real(8), allocatable :: xmt(:,:)
      real(8), allocatable :: h(:,:)
      real(8), allocatable :: xst(:,:)
      real(8), allocatable :: xr(:,:,:)
      real(8), allocatable :: rr(:,:,:)
!     real(8), allocatable :: xvalws(:,:,:)   ! it can be calculated when it is needed
!     real(8), allocatable :: ec(:,:,:,:)

!???
      integer :: ibot_cont = 0
      integer :: iset_b_cont = 0
      real(8) :: vdif = 0
      real(8), allocatable :: zcors(:,:)
      real(8), allocatable :: zvals(:,:)
!      logical, allocatable :: isDLM(:)
!      logical, allocatable :: isflipped(:)
!      integer, allocatable :: flippoint(:)
      logical, pointer :: isDLM(:) => null()
      logical, pointer :: isflipped(:) => null()
      integer, pointer :: flippoint(:) => null()
!???
      real(8) :: efpot

      type(Sublattice), pointer :: sublat(:)
      type(RS_Str), pointer :: rs_box
      type(SphAtomDeps), pointer :: v_rho_box

      integer :: ipot_read
      integer :: nsub, mxkomp
      integer :: nsites
      integer :: mtasa
      real(8) :: zt,r_mt,r_ws,r_cs
      character(10) :: istp=' '
      integer :: iprint,nk
      integer :: ndrpts,ndmesh
      real(8), allocatable :: xr0(:),rr0(:)
      character(80) :: tmpname
      integer, external :: indRmesh

      M_S => mecca_state
      iprint = M_S%intfile%iprint
      mtasa = M_S%intfile%mtasa
      nspin = M_S%intfile%nspin
      nbasis = M_S%intfile%nsubl
      alat = M_S%intfile%alat
      sublat => M_S%intfile%sublat(1:nbasis)
      komp = sublat(1:nbasis)%ncomp
      mxkomp = maxval(komp)     ! to simplify memory allocation
      numbsub = sublat(1:nbasis)%ns
      nsites = sum(numbsub)

      allocate(jmt(mxkomp,nbasis)) ; jmt = -1
      allocate(jws(mxkomp,nbasis)) ; jws = -1
!      allocate(numc(mxkomp,nbasis,2)); numc = 0
      allocate(atcon(mxkomp,nbasis)) ; atcon =-1
      do nsub=1,nbasis
        atcon(1:komp(nsub),nsub) =                                      &
     &      sublat(nsub)%compon(1:komp(nsub))%concentr
       jmt(1:komp(nsub),nsub) = sublat(nsub)%compon(1:komp(nsub))%      &
     &                          v_rho_box%jmt
!       jws(1:komp(nsub),nsub) = sublat(nsub)%compon(1:komp(nsub))%      &
!     &                          v_rho_box%jws
!       numc(1:komp(nsub),nsub,1:2)= sublat(nsub)%compon(1:komp(nsub))%  &
!     &                          v_rho_box%numc
      end do

!      allocate(nc(ipeval,mxkomp,nbasis)) ; nc = 0
!      allocate(lc(ipeval,mxkomp,nbasis)) ; lc = 0
!      allocate(kc(ipeval,mxkomp,nbasis)) ; kc = 0
!      allocate(ec(ipeval,mxkomp,nbasis)) ; ec = 0
!      do nsub=1,nbasis
!       do k=1,komp(k)
!        nc(:,k,nsub) = sublat(nsub)%compon(k)%v_rho_box%nc(:)
!        lc(:,k,nsub) = sublat(nsub)%compon(k)%v_rho_box%lc(:)
!        kc(:,k,nsub) = sublat(nsub)%compon(k)%v_rho_box%kc(:)
!        ec(:,k,nsub) = sublat(nsub)%compon(k)%v_rho_box%ec(:)
!       end do
!      end do

      rmtstr = alat*sublat(:)%rIS
      rs_box => M_S%rs_box
      allocate(rmt(mxkomp,nbasis)) ; rmt = 0
      allocate(rws(mxkomp,nbasis)) ; rws = 0
      allocate(rcs(mxkomp,nbasis)) ; rcs = 0
!      do nsub=1,nbasis
!        rmt(:,nsub) = rs_box%rmt(nsub)
!        rws(:,nsub) = rs_box%rws(nsub)
!        rcs(:,nsub) = rs_box%rcs(nsub)
!      end do
      allocate(h(mxkomp,nbasis)) ; h = 0
      allocate(xst(mxkomp,nbasis)) ; xst = 0
      allocate(xmt(mxkomp,nbasis)) ; xmt = 0

!      allocate(xvalws(mxkomp,nbasis,2)) ; xvalws = 0

      allocate(zvals(mxkomp,nbasis)) ; zvals = 0
      allocate(zcors(mxkomp,nbasis)) ; zcors = 0
!      allocate(flippoint(nbasis)); flippoint = 0
!      allocate(isflipped(nbasis)); isflipped = .false.
!      allocate(isDLM(nbasis)); isDLM = .false.
      flippoint => sublat(1:nbasis)%flippoint
      isflipped => sublat(1:nbasis)%isflipped
      isDLM => sublat(1:nbasis)%isDLM

      if ( initp == mecca_2005 ) then
        ndrpts = 0
      else
        ndrpts = iprpts
      end if
      do nsub=1,nbasis
       if (  initp == mecca_2005 ) then
        zt = dot_product(                                               &
     &              dble(sublat(nsub)%compon(1:sublat(nsub)%ncomp)%zID),&
     &              sublat(nsub)%compon(1:sublat(nsub)%ncomp)%concentr)
        r_mt = rs_box%rmt(nsub)
        r_ws = rs_box%rws(nsub)
        r_cs = rs_box%rcs(nsub)
        ndmesh = 0
        allocate(xr0(1),rr0(1)) ; xr0=0; rr0=0
        call genRmesh(mtasa,r_mt,r_ws,r_cs,zt,ndmesh,                   &
     &             xr0,rr0,h(1,nsub),xst(1,nsub),                       &
     &             jmt(1,nsub),jws(1,nsub))
        if ( ndmesh <=0 ) then
            write(6,'(2(a,i4),2(a,f8.3))')                              &
     &      ' ndmesh=',ndmesh,' nsub=',nsub,' r_mt=',r_mt,' r_ws=',r_ws
            call fstop(' init_potential:: ndmesh<=0 error')
        end if
        deallocate(xr0,rr0)
       else
        ndmesh = ndrpts
       end if
       do nk=1,komp(nsub)
         sublat(nsub)%compon(nk)%v_rho_box%ndrpts = ndmesh   ! TODO: is it really needed to have array "sublat"?
         sublat(nsub)%compon(nk)%v_rho_box%ndchg  = ndmesh
         sublat(nsub)%compon(nk)%v_rho_box%ndcor  = ndmesh
         nullify(v_rho_box)
       end do
       ndrpts = max(ndrpts,ndmesh)
      end do
      allocate(xr(ndrpts,mxkomp,nbasis)) ; xr = 0
      allocate(rr(ndrpts,mxkomp,nbasis)) ; rr = 0
      allocate(xr0(ndrpts),rr0(ndrpts)) ; xr0=0; rr0=0

      do nsub=1,nbasis
        zt = dot_product(                                               &
     &              dble(sublat(nsub)%compon(1:sublat(nsub)%ncomp)%zID),&
     &              sublat(nsub)%compon(1:sublat(nsub)%ncomp)%concentr)
!        zt = dble( maxval( sublat(nsub)%compon(1:sublat(nsub)%ncomp)%   &
!     &             zID ) )
        r_mt = rs_box%rmt(nsub)
        r_ws = rs_box%rws(nsub)
        r_cs = rs_box%rcs(nsub)
        nk = 1
        ndmesh = sublat(nsub)%compon(nk)%v_rho_box%ndrpts
        call genRmesh(mtasa,r_mt,r_ws,r_cs,zt,ndmesh,                   &
     &             xr0,rr0,h(nk,nsub),xst(nk,nsub),                     &
     &             jmt(nk,nsub),jws(nk,nsub))
        rmt(:,nsub) = r_mt
        rws(:,nsub) = r_ws
        rcs(:,nsub) = r_cs
        jmt(:,nsub) = jmt(nk,nsub)
        jws(:,nsub) = jws(nk,nsub)
        xst(:,nsub) = xst(nk,nsub)
        h(:,nsub) = h(nk,nsub)
        xmt(:,nsub) = xst(:,nsub) + (jmt(:,nsub)-1)*h(:,nsub)
        zt = max(r_ws,r_cs)
        ndmesh = 1+indRmesh(zt,ndmesh,rr0)
        if ( rr0(ndmesh)<=zt ) ndmesh=ndmesh+1
        do nk=1,mxkomp
         xr(1:ndmesh,nk,nsub) = xr0(1:ndmesh)
         rr(1:ndmesh,nk,nsub) = rr0(1:ndmesh)
        end do
        do nk=1,komp(nsub)
          v_rho_box => sublat(nsub)%compon(nk)%v_rho_box
          call allocSphAtomDeps(v_rho_box,ndmesh,nspin)
          v_rho_box%xst = xst(nk,nsub)
          v_rho_box%xmt = xmt(nk,nsub)
          v_rho_box%jmt = jmt(nk,nsub)
!          v_rho_box%ndrpts = ndmesh
!          v_rho_box%ndchg  = ndmesh
!          v_rho_box%ndcor  = ndmesh
          v_rho_box%ztot_T(1:nspin) = sublat(nsub)%compon(nk)%zID/nspin
          v_rho_box%zcor(1:nspin)   = sublat(nsub)%compon(nk)%zcor/nspin
          v_rho_box%zcor_T(1:nspin) = sublat(nsub)%compon(nk)%zcor/nspin
          nullify(v_rho_box)
        end do
      end do

      efpot = M_S%intfile%etop

      call scf_delete_files(M_S%intfile%genName)

      ipot_read = 0
      tmpname = M_S%intfile%io(nu_inpot)%name
      call readIniFile(tmpname,ipot_read)
      if ( ipot_read > 0 .and.                                          &
     &    trim(char2status(M_S%intfile%io(nu_inpot)%status))=='NEW' )   &
     &  then
       write(6,'(/'' WARNING: input potential file '',a,'' is ignored,''&
     &  /10x,''default procedure is used to generate initial potential''&
     &         /)') trim(tmpname)
        ipot_read = -1
      else
       if ( ipot_read<=0 ) then
        if ( ipot_read.ne.-1 )                                          &
     &   write(6,'(/'' WARNING: input potential file '',a,'' is missed''&
     &          )') trim(tmpname)
        if ( trim(char2status(M_S%intfile%io(nu_inpot)%status))=='OLD' )&
     &   then
         write(6,'(a)')                                                 &
     &       trim(M_S%intfile%io(nu_inpot)%name)//' is not available'
         call p_fstop(' INPUT POTENTIAL FILE IS NOT FOUND')
        else
         if ( M_S%intfile%nscf>0 ) then
         write(6,'(/'' INFO: default procedure is used to generate '',  &
     &            ''initial potential''/)')
          ipot_read = -1
         else
          write(6,'(/'' ERROR: input potential file must be '',         &
     &                                    ''available, when nscf=0''/)')
          call p_fstop(' INPUT POTENTIAL FILE IS NOT FOUND')
         end if
        end if
       end if
      end if

      inquire(unit=io22,opened=open22 )
      if ( open22 ) then
       if( ipot_read==-1 ) then
        write(6,'(/a,i3,a/)') ' WARNING: File on unit',io22,            &
     &                        ' is deleting init_potential'
        close(io22,status='DELETE',err=1)
1       continue
       else
        close(io22)
       end if
      end if

      if ( nspin>1 ) then
       if(trim(char2status(M_S%intfile%io(nu_inpot)%status))=='NEW')then
        if ( M_S%intfile%magnField == zero ) then
         M_S%intfile%magnField = gSpinsplt()
        end if
       end if
      end if

      if ( ipot_read > 0 ) then
       open(io22,file=tmpname)   ! M_S%intfile%io(nu_inpot)%name)
       initp = mecca_2005
      end if

      if ( .not. gPointNucleus() ) then
       write(6,'(/a)') '    Radii of atomic nuclei:'
       do nk=1,M_S%intfile%nelements ! printing only
        call twoZ(dble(M_S%intfile%elements(nk)%ztot),xr0,0,rr0)
       end do
      end if

      select case (initp)
      case (mecca_ames)
        call     getpot1(nspin,nbasis,komp,                             &
     &                 jmt,jws,rmt,rws,                                 &
     &                 xr,rr,h,xst,                                     &
     &                 mtasa,iprint,istp,                               &
     &                 vdif,zvals,zcors,                                &
     &                 ibot_cont,iset_b_cont,                           &
     &                 isDLM,isflipped,flippoint)
      case (mecca_2005)
        call     getpot7(nspin,nbasis,komp,                             &
     &                  jmt(1,:),jws(1,:),rmtstr(:),rws(1,:),           &
     &                  xr(:,1,:),rr(:,1,:),h(1,:),xst(1,:),            &
     &                  mtasa,iprint,istp,                              &
     &                  rmt(:,:),rcs(1,:),atcon(:,:),numbsub(:),        &
     &                  isDLM)
      case default
        call     getpot1(nspin,nbasis,komp,                             &
     &                 jmt,jws,rmt,rws,                                 &
     &                 xr,rr,h,xst,                                     &
     &                 mtasa,iprint,istp,                               &
     &                 vdif,zvals,zcors,                                &
     &                 ibot_cont,iset_b_cont,                           &
     &                 isDLM,isflipped,flippoint)
      end select

      inquire(unit=io22,opened=open22 )
      if ( open22 ) close(io22)
!c     ===============================================================
      deallocate(jmt,jws,atcon)
      deallocate(rmt,rws,rcs)
      deallocate(h,xst,xmt,xr,rr)
      deallocate(xr0,rr0)
      deallocate(zvals,zcors)
!      deallocate(flippoint,isflipped,isDLM)
      nullify(flippoint)
      nullify(isflipped)
      nullify(isDLM)
      nullify(sublat)
      nullify(M_S)
!c     ===============================================================

      return

!EOC
      end subroutine init_potential

!c     cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
!BOP
!!IROUTINE: getpot1
!!INTERFACE:
      subroutine getpot1(nspin,nbasis,                                  &
     &                 komp,jmt,jws,rmt,rws,xr,rr,h,xstart,             &
     &                 mtasa,iprint,istop,                              &
     &                 vdif,zvals,zcors,                                &
     &                 ibot_cont,iset_b_cont,                           &
     &                 isDLM,isflipped,flippoint)
!!DESCRIPTION:
! generation of starting SCF potential based on either atomic Herman-Skillman
! potential or reading in potential (Ames format) file
!
!!REVISION HISTORY:
! Initial Version - DDJ, A.S. - 1998
! Modified - mecca_ames - 2010
! Revised  - A.S. - 2013
!EOP
!
!BOC
      implicit none
      integer :: nspin
      integer :: nbasis
      integer   komp(nbasis)
      integer   jws(:,:)
      integer   jmt(:,:)
!      real*8    rmt_ovlp(ipcomp,nbasis)
      real*8    rmt(:,:)
      real*8    rws(:,:)
!      real*8    r_circ(ipcomp,nbasis)
      real*8    xr(:,:,:)
      real*8    rr(:,:,:)
      real*8    h(:,:)
      real*8    xstart(:,:)
      integer :: mtasa,iprint
      character(10) :: istop
!     real*8    vr(iprpts,ipcomp,ipsublat,nspin)
      real(8) :: vdif
!     real*8    rhotot(iprpts,ipcomp,ipsublat,nspin)
!     real*8    corden(iprpts,ipcomp,ipsublat,nspin)
!     real*8    ztot(ipcomp,nbasis)
!     real*8    zcor(ipcomp,nbasis)
      real*8    zvals(:,:)
      real*8    zcors(:,:)
!     integer   numc(ipcomp,ipsublat,nspin)
!     integer   nc(ipeval,ipcomp,ipsublat,nspin)
!     integer   lc(ipeval,ipcomp,ipsublat,nspin)
!     integer   kc(ipeval,ipcomp,ipsublat,nspin)
!     real*8    ec(ipeval,ipcomp,ipsublat,nspin)
!      real(8) :: ebot
      integer :: ibot_cont,iset_b_cont
      logical :: isDLM(:)
      logical :: isflipped(:)
      integer :: flippoint(:)
!
      character(*), parameter :: sname='getpot1'
      integer :: ipot_read
      real(8) :: xvalws(size(zvals,1),nbasis,nspin)
      real(8) :: efpotnk
      real(8), pointer :: efpot
      type(SphAtomDeps), pointer :: v_rho_box, v_rho_box0
      real(8) :: zval

      type(atom_rdata) :: atomic

!vdif      real(8) :: rnn_min

      integer :: nspinp,iefpot,nsub,nk,ns,nk0,nsub0,ns0,initialize
!      integer   nspinp,nspin
!      integer   num,niter
!      integer   natin,nbasin,ityp1(natom)
!C     integer   natom,nbasis,ityp1(natom)
!c
!      real*8    rASA(ipcomp,nbasis)
!      real*8    X(ipcomp,nbasis)
!      real*8    xmom(ipcomp,nbasis)
!      real*8    rslatt(3,3)          ! In units of Alat/(2*pi)
!      real*8    pos(3,natom)
!      real*8    transV(3,3)
!      real*8    basisV(3,natom)
!      real*8    vdif,vdif0,alp,alat,volume,ebot

!      character(80) header

!c---------------------------------------------------------------
!c      Zeroout working arrays
!c---------------------------------------------------------------
      efpot => M_S%intfile%etop
      ipot_read = 0

!CALAM  ***************************************************************
!c       Read in the starting potential and charge densities for
!c             various components, sublattices and spins.
!c       For spin polarized calc. vdif measures the difference between
!c       mt-zero for maj. and min. spin electrons (assume vmtz(maj)=0)
!CALAM  ***************************************************************

!CDDJ   Allow user to supply Ef guess in input for starting potential
       initialize=1
       efpotnk=0.0d0
        if(abs(efpot).gt.0.0001d0) then
           efpotnk=efpot
         write(6,'("  Use user-supplied guess: Ef =",f10.6,/)') efpot
        endif
      nspinp = nspin
      inquire(unit=io22,opened=open22 )
      if ( .not. open22 ) go to 100
!      read(io22,'(a80)',end=100) header
      read(io22,'(a80)',end=100)
      read(io22,*,err=100,end=100)nspinp,vdif
      if(nspinp.gt.2) then
        write(6,'(''   nspinp = '',i5,'' > 2 '')')nspinp
        go to 100
      endif
!c
      write(6,'(/''  Read starting potentials and charge densities'')')
      ipot_read=1             ! POT file exists
      iset_b_cont=0           ! POT file exists
      iefpot = 0
      do nsub=1,nbasis
        do nk=1,komp(nsub)
          v_rho_box => M_S%intfile%sublat(nsub)%compon(nk)%v_rho_box
          v_rho_box%v0 = 0
          v_rho_box%vr = 0
          v_rho_box%chgtot = 0
          v_rho_box%chgcor = 0
          v_rho_box%numc = 0
          v_rho_box%nc = 0
          v_rho_box%kc = 0
          v_rho_box%lc = 0
          v_rho_box%ec = 0
          do ns=1,nspinp

!cSUFF automatic DLM and Patterned magnetism file

             if( .not. isflipped(nsub) .and.                            &
     &             (.not. isDLM(nsub) .or. nk <= komp(nsub)/2) ) then
!                  initialize=0
                if ( iprint>= -1 ) then
                 if ( nsub<=50 .or. iprint>=0 ) then
                  write(6,'(''   Sublat.='',i2,'' Spin='',i2,           &
     &            '' Comp.='',i2,'' V(r)'',t40,''is READ'')') nsub,ns,nk
                  if ( iprint==-1 .and. nsub==50 )                          &
     &        write(6,*) ' printing is skipped for other sublattices...'
                 end if
                end if
!c                ------------------------------------------------------
                 call potred(jmt(nk,nsub),rmt(nk,nsub),jws(nk,nsub),    &
     &                       rws(nk,nsub),xr(:,nk,nsub),                &
     &                       rr(:,nk,nsub),h(nk,nsub),                  &
     &                       xstart(nk,nsub),                           &
     &                       v_rho_box%vr(:,ns),                        &
     &                       v_rho_box%chgtot(:,ns),                    &
     &                       v_rho_box%chgcor(:,ns),                    &
     &                       xvalws(nk,nsub,ns),                        &
     &                   dble(M_S%intfile%sublat(nsub)%compon(nk)%zID), &
     &                   dble(M_S%intfile%sublat(nsub)%compon(nk)%zcor),&
     &                       zvals(nk,nsub),zcors(nk,nsub),             &
     &                       v_rho_box%numc(ns),                        &
     &                       v_rho_box%nc(:,ns),v_rho_box%lc(:,ns),     &
     &                       v_rho_box%kc(:,ns),v_rho_box%ec(:,ns),     &
     &                       M_S%intfile%alat,efpotnk,initialize,       &
     &                       ibot_cont,iset_b_cont,mtasa,iprint,istop)
!c               -----------------------------------------------------
             else
!                 initialize=0
                  if ( iprint>= -1 ) then
                   if ( nsub<=50 .or. iprint>=0 ) then
                  write(6,'(''   Sublat.='',i2,'' Spin='',i2,           &
     &         '' Comp.='',i2,'' V(r)'',t40,''is COPIED'')') nsub,ns,nk
                    if ( iprint==-1 .and. nsub==50 )                    &
     &        write(6,*) ' printing is skipped for other sublattices...'
                   end if
                  end if

                  if( isDLM(nsub) .and. nk > komp(nsub)/2 )  then
                     nk0 = 1+mod(nk-1,komp(nsub)/2); nsub0 = nsub
                  else
                     nk0 = nk; nsub0 = flippoint(nsub)
                  end if
                  v_rho_box0 =>                                         &
     &                  M_S%intfile%sublat(nsub0)%compon(nk0)%v_rho_box

                  if( ns == 1 ) ns0 = 2
                  if( ns == 2 ) ns0 = 1

                  jmt(nk,nsub) = jmt(nk0,nsub0)
                  rmt(nk,nsub) = rmt(nk0,nsub0)
                  jws(nk,nsub) = jws(nk0,nsub0)
                  xr(:,nk,nsub) = xr(:,nk0,nsub0)
                  rr(:,nk,nsub) = rr(:,nk0,nsub0)
                  h(nk,nsub) = h(nk0,nsub0)
                  xstart(nk,nsub) = xstart(nk0,nsub0)
                  xvalws(nk,nsub,ns) = xvalws(nk0,nsub0,ns0)
                  zvals(nk,nsub) = zvals(nk0,nsub0)
                  zcors(nk,nsub) = zcors(nk0,nsub0)
                  v_rho_box%ztot_T = v_rho_box0%ztot_T
                  v_rho_box%zcor = v_rho_box0%zcor
                  v_rho_box%zcor_T = v_rho_box0%zcor_T
                  v_rho_box%vr(:,ns) =                                  &
     &                     v_rho_box0%vr(:,ns0)
                  v_rho_box%chgtot(:,ns) =                              &
     &                     v_rho_box0%chgtot(:,ns0)
                  v_rho_box%chgcor(:,ns) =                              &
     &                     v_rho_box0%chgcor(:,ns0)
                  v_rho_box%numc(ns) =                                  &
     &                     v_rho_box0%numc(ns0)
                  v_rho_box%nc(:,ns) =                                  &
     &                      v_rho_box0%nc(:,ns0)
                  v_rho_box%lc(:,ns) =                                  &
     &                      v_rho_box0%lc(:,ns0)
                  v_rho_box%kc(:,ns) =                                  &
     &                      v_rho_box0%kc(:,ns0)
                  v_rho_box%ec(:,ns) =                                  &
     &                      v_rho_box0%ec(:,ns0)

             end if

             if(iefpot.eq.0) then
                 efpot = efpotnk
                 write(6,*) 'efpot = ', efpot
                 iefpot = 1
             end if
          enddo
          nullify(v_rho_box)
         enddo
      enddo

!C=======  Reading of rV(r) and 4*pi*r^2*rho(r)   COMPLETE  ==========
100   continue
      if ( ipot_read <= 0) then  ! to generate a starting POT
       write(6,'(''====================================================&
     &=============='')')
       write(6,'(/''Generate starting potentials...'')')
!CALAM================================================================
!C    Interpolate this new overlapping charge density from the
!C    herman-skillman grid [rhsk] to our logarithmic radial grid [rr]
!C    Hence calculate the potential corresponding to this new rho
!CALAM================================================================
       if(efpot.lt.0.0001d0)efpot = 0.65d0
                                          ! initial guess (OK for 3D TM metal)
       write(6,'('' GETPOT_OVLP: Use INIOVLP_POT to guess a starting pot&
     & from the '')')
       write(6,'(''             overlapping atomic charge density'')')
       write(6,'(/a,f10.7)') 'Made Fermi level guess of = ', efpot

       do ns=1,nspinp
        do nsub=1,nbasis
         do nk=1,komp(nsub)
          call get_atom(M_S%intfile%sublat(nsub)%compon(nk)%zID,atomic, &
     &                                                               -1)
          v_rho_box => M_S%intfile%sublat(nsub)%compon(nk)%v_rho_box
          if ( iprint>=0 ) then
           write(6,*)
           write(6,'(''   Sublat.='',i2,'' Spin='',i2,                  &
     &               '' Comp.='',i2)') nsub,ns,nk
          end if
          call iniovlp_pot(jmt(nk,nsub),rmt(nk,nsub),                   &
     &                  jws(nk,nsub),rws(nk,nsub),                      &
     &                  xr(:,nk,nsub),rr(:,nk,nsub),h(nk,nsub),         &
     &                  xstart(nk,nsub),                                &
     &                  dble(M_S%intfile%sublat(nsub)%compon(nk)%zID),  &
     &                  dble(M_S%intfile%sublat(nsub)%compon(nk)%zcor), &
     &                  atomic%r,atomic%rho/nspinp,                     &
     &                  v_rho_box%vr(:,ns),v_rho_box%v0(ns),            &
     &                  v_rho_box%chgcor(:,ns),                         &
     &                  v_rho_box%chgtot(:,ns),xvalws(nk,nsub,ns),      &
     &                  v_rho_box%numc(ns),v_rho_box%nc(:,ns),          &
     &                  v_rho_box%lc(:,ns),v_rho_box%kc(:,ns),          &
     &                  v_rho_box%ec(:,ns),mtasa,iprint)
          v_rho_box%v0(ns) = v_rho_box%vr(jmt(nk,nsub),ns)/rmt(nk,nsub)
          nullify(v_rho_box)
         enddo
        enddo
       enddo
       zval = g_zvaltot(M_S%intfile,.true.)
       M_S%intfile%etop = g_efestim(zval,2.d0*M_S%rs_box%volume)
      else
       if ( efpot == zero ) then
        zval = g_zvaltot(M_S%intfile,.true.)
        M_S%intfile%etop = g_efestim(zval,2.d0*M_S%rs_box%volume)
       end if
      end if

!C====================================================================
!c   Magn. --> Non-Magn.
!C====================================================================
      if(nspin.eq.1.and.nspinp.eq.2) then
       vdif = zero
       do nsub=1,nbasis
         xvalws(1:komp(nsub),nsub,1)=xvalws(1:komp(nsub),nsub,1)+       &
     &                                xvalws(1:komp(nsub),nsub,2)
        do nk=1,komp(nsub)
         v_rho_box => M_S%intfile%sublat(nsub)%compon(nk)%v_rho_box
         v_rho_box%chgtot(:,1)=v_rho_box%chgtot(:,1)+                   &
     &                                 v_rho_box%chgtot(:,2)
         v_rho_box%chgcor(:,1)=v_rho_box%chgcor(:,1)+                   &
     &                                 v_rho_box%chgcor(:,2)
         v_rho_box%vr(:,1)=0.5d0*(v_rho_box%vr(:,1)+                    &
     &                                    v_rho_box%vr(:,2))
         nullify(v_rho_box)
        enddo
       enddo
      end if
!C====================================================================
!c   Non-Mag. --> Magn.
!C====================================================================
      if(nspin.eq.2.and.nspinp.eq.1) then
       if(abs(vdif).lt.1.d-10) then
        write(6,*)
        write(6,'(''   nspinp .ne. nspin '', 2i5)')nspinp,nspin
        write(6,*) ' vdif=',vdif
!vdif        rnn_min = M_S%intfile%alat
!vdif        do nsub=1,nbasis
!vdif         if ( maxval(M_S%intfile%sublat(nsub)%compon(1:komp(nsub))%     &
!vdif     &             zID)>0 ) then
!vdif          rnn_min=min(rnn_min,2*(minval(rws(1:komp(nsub),nsub))/1.3d0))
!vdif         end if
!vdif        end do
!vdif        vdif = vdif0/rnn_min
!        vdif = vdif0
        write(6,*) ' Non-magnetic initial potential is used for',       &
     &             ' magnetic case'
!        write(6,*) ' VDIF has been changed to',vdif
        write(6,*)
       end if
       do nsub=1,nbasis
        xvalws(1:komp(nsub),nsub,2)=0.5d0*xvalws(1:komp(nsub),nsub,1)
        xvalws(1:komp(nsub),nsub,1)=xvalws(1:komp(nsub),nsub,2)
        do nk=1,komp(nsub)
         v_rho_box => M_S%intfile%sublat(nsub)%compon(nk)%v_rho_box
         v_rho_box%numc(2)=v_rho_box%numc(1)
         v_rho_box%nc(:,2)=v_rho_box%nc(:,1)
         v_rho_box%lc(:,2)=v_rho_box%lc(:,1)
         v_rho_box%kc(:,2)=v_rho_box%kc(:,1)
         v_rho_box%ec(:,2)=v_rho_box%ec(:,1) - vdif
         v_rho_box%chgtot(:,1)=v_rho_box%chgtot(:,1)/2
         v_rho_box%chgtot(:,2)=v_rho_box%chgtot(:,1)
         v_rho_box%chgcor(:,1)=v_rho_box%chgcor(:,1)/2
         v_rho_box%chgcor(:,2)=v_rho_box%chgcor(:,1)
         v_rho_box%vr(:,2) = v_rho_box%vr(:,1) - vdif*rr(:,nk,nsub)
         nullify(v_rho_box)
        enddo
       enddo
      end if
!c     ===============================================================
      if (istop.eq.sname) then
         call fstop(sname)
      endif
!c
      return
!EOC
      contains
!BOP
!!IROUTINE: iniovlp_pot
!!INTERFACE:
      subroutine iniovlp_pot(jmt,rmt,jws,rws,xr,rr,h,xstart,ztot,zcor,  &
     &                  rhsk,rhohsk,vr,v0,corden,rhotot,xvalws,numc,    &
     &                  nc,lc,kc,ec,mtasa,iprint)
!!DESCRIPTION:
!CALAM================================================================
!
!  Interpolates the new overlapping charge density from the
!  herman-skillman grid (rhsk) to {\sc mecca} logarithmic radial grid.
!  Hence calculate the potential corresponding to this new rho.
!
!CALAM================================================================
!

!!REVISION HISTORY:
! Initial Version - mecca_ames - 2010
! Revised         - A.S. - 2013
!EOP
!
!BOC
      implicit none
!c
        integer :: jmt,jws,iprint,mtasa,iex,ir,j,numc
        integer :: nc(:),lc(:),kc(:)
        real(8) :: rmt,rws,h,xstart,ztot,zcor
        real(8) :: xr(:)
        real(8) :: rr(:)
        real(8) :: rhsk(:)
        real(8) :: rhohsk(:)
        real(8) :: vr(:),v0
        real(8) :: corden(:)
        real(8) :: rhotot(:)
        real(8) :: xvalws
        real(8) :: rho1int(size(xr))
        real(8) :: rho2int(size(xr))
        real(8) :: vx(size(xr))
        real(8) :: ec(:)

      real*8    vhart(size(xr))
      real*8    ro3,dummy
      real(8), external :: alpha2

      integer ndrpts,ndhsk
      ndrpts = size(xr)
      ndhsk = size(rhsk)
!C===============================================================
!c set number of points up to sphere boundary, i.e. m.t. or a.s.a

!c set log-grid starting point
!cab         xstart=-11.13096740d+00
!c
!cab           if(mtasa.le.0) then
!cab            jmt=iprpts-50            ! additional points used in "mixing"
!cab           else
!cab            jmt=iprpts-38            ! additional points used in "mixing"
!cab            jws=jmt
!cab           endif
!cab!c
!cab         xlast=log(rmt)
!cab         h=(xlast-xstart)/(jmt-1)
!cab         do j=1,iprpts
!cab           xr(j)=xstart+(j-1)*h
!cab           rr(j)=exp(xr(j))
!cab!c
!cab!c set number of pts. to w.s. radius
!cab           if(mtasa.le.0 .and. rr(j).lt.rws) jws= j + 1
!cab         enddo
!cab
!cab         if(jws.ge.iprpts-1) then
!cab           write(6,'('' jws='',i3,2x,''iprpts='',i3)') jws,iprpts
!cab           call fstop(sname//':'//' jws greater than iprpts-1')
!cab         endif
!cab         if(iprint.ge.1) then
!cab           write(6,'(''  iniovlp_pot: jmt,h,rr(1),rmt'',i7,3e14.6)')    &
!cab     &                          jmt,h,rr(1),rmt
!cab         endif
!C===================================================================
!C      Interpolate the charge density from the herman-skillman
!C      grid (rhsk) to the logarithmic radial grid as constructed
!C      above.
!C===================================================================
         do j=1,ndrpts
            rhotot(j)=ylag(rr(j),rhsk,rhohsk,0,3,ndhsk,iex)
         enddo
!C===================================================================
!C       Construct the potential corresponding to new charge density
!C===================================================================
         call qexpup(1,rhotot,ndrpts,xr,rho2int)

         call qexpup(0,rhotot,ndrpts,xr,rho1int)

!c
!c Hartree piece
         vhart(1:jmt) = 2.0d0*( (-ztot+rho2int(1:jmt))/rr(1:jmt) +      &
     &                   (rho1int(jmt)-rho1int(1:jmt)) )
         vhart(jmt+1:ndrpts) = (vhart(jmt)*rr(jmt))/rr(jmt+1:ndrpts)
         do ir=1,ndrpts      !calculate new potential
!c
!c exchange-correlation potential
!c            if(rhotot(ir).le. 0.0d0) rhotot(ir)=rhotot(ir-1)

            if(rhotot(ir).le. 0.0d0) then
             if(ir==1) then
              rhotot(1) =1.0d-15 !SK
             else
              rhotot(ir)=rhotot(ir-1)
             end if
            end if

            ro3=(3.0d0*(rr(ir)**2)/rhotot(ir))**(1.0d0/3.0d0)
!c
            vx(ir)=alpha2(ro3,zero,one,20,dummy) !spin is real*8

!c  totrho->0 ro3-> large, some times this causes an error in alpha2
!c  in this case the vx=0
            if(vx(ir).eq.0.0d0)  then
               vx(ir)=vx(ir-1)
            end if
!c
!c new potential, including mean-field, charge-correlated CPA
!c         vrnew(ir,ik)=( vhart + vx(ir,ik) - vmt1 + vqeff(ik) )*rr(ir)
         enddo
         vr(1:ndrpts)=(vhart(1:ndrpts)+vx(1:ndrpts))*rr(1:ndrpts)

          call scf_atom_pot(ndrpts,jmt,jws,h,rr,xr,ztot,vr,v0,rhotot,   &
     &                      corden,1,numc,nc,lc,kc,ec,mtasa,zcor,       &
     &                      xvalws,iprint,' ')

      return

!EOC
      end subroutine iniovlp_pot
!c
      end subroutine getpot1
      !c
!c     cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
!BOP
!!IROUTINE: potred
!!INTERFACE:
      subroutine potred(jmt,rmt,jws,rws,xr,rr,h,xstart,                 &
     &                   vr,                                            &
     &                   rhotot,corden,                                 &
     &                   xvalsws,ztot,zcor,zvals,zcors,                 &
     &                   numc,nc,lc,kc,ec,                              &
     &                   alat,efpot,                                    &
     &                   initialize,                                    &
     &                   ibot_cont,iset_b_cont,mtasa,iprint,istop)
!!DESCRIPTION:
! To read-in potential file in Ames (old) format
!
!!REVISION HISTORY:
! Initial Version - DDJ,A.S. - 1998
! Modified        - mecca_ames - 2010
!!REMARKS:
! starting point of radial mesh is fixed (xstart=-11.13096740)
! radial mesh size is defined by a global parameter (iprpts)
!EOP
!
!BOC
      implicit real*8 (a-h,o-z)
!c
      integer :: jmt,jws,numc,initialize
      integer :: ibot_cont,mtasa,iprint
      character jttl2*80
      character lj(ipeval)*5
      character istop*10
      character sname*10
      character coerr*80
!c
      integer   nc(ipeval)
      integer   lc(ipeval)
      integer   kc(ipeval)
      integer   iset_b_cont
!c
      real*8    ec(ipeval)
      real*8    xr(iprpts)
      real*8    rr(iprpts)
      real*8    xrold(2*iprpts)
      real*8    rrold(2*iprpts)
      real*8    vr(iprpts)
      real*8    vrold(2*iprpts)
      real*8    rhotot(iprpts)
      real*8    corden(iprpts)
      real*8    chgold(2*iprpts)
      real*8    corold(2*iprpts)
      real*8    a(iprpts)
      real*8    b(iprpts)
      real*8    v0
      real*8    qmtnew
      real*8    qmtold
      real*8    tol
      real*8    ebot_read,xlast

      integer :: j,jmt0,jj,ntchg,ntcor,iup,iex
!c
!c ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
      parameter (tol=(one/ten)**5)
      parameter (sname='potred')
!c ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!c set number of points up to sphere boundary, i.e. m.t. or a.s.a
!c NOTE: For Total Energy calculation (incrs-1) must be divisible by 4
!c ---------------------------------------------------------------------- !--
!c set log-grid starting point
        xstart=-11.13096740d+00
!c
      if(initialize.eq.0) then
           if(mtasa.le.0) then
            jmt=iprpts-50            ! additional points used in "mixing"
           else
            jmt=iprpts-38            ! additional points used in "mixing"
            jws=jmt
           endif
!c
        xlast=log(rmt)
        h=(xlast-xstart)/(jmt-1)
        do j=1,iprpts
           xr(j)=xstart+(j-1)*h
           rr(j)=exp(xr(j))
!c
!c set number of pts. to w.s. radius
           if(mtasa.le.0 .and. rr(j).lt.rws) jws= j + 1
        enddo

        if(jws.ge.iprpts-1) then
          coerr=' jws greater than iprpts-1'
          write(6,'('' jws='',i3,2x,''iprpts='',i3)') jws,iprpts
          call fstop(sname//':'//coerr)
       endif
      endif
      if(iprint.ge.1) then
        write(6,'(''  potred: jmt,h,rr(1),rmt'',i7,3e14.6)')            &
     &                          jmt,h,rr(1),rmt
      endif
!c
!c initialize changed so not to redo grid
      initialize=1
!c     ----------------------------------------------------------
      call zeroout(vrold,2*iprpts)
!c     ----------------------------------------------------------
      call zeroout(chgold,2*iprpts)
!c     ----------------------------------------------------------
      call zeroout(corold,2*iprpts)
!c     ----------------------------------------------------------
!c     ==========================================================
!c     read in potential deck....................................
      read(io22,'(a80)',end=100) jttl2
      read(io22,'(f5.0,13x,f12.5,f5.0,f12.5,e20.13)',err=100,end=100)     &
     &          azed,aa,zcd,ebot_read,efpot
      read(io22,*) xst,xmt,jmt0
      read(io22,'(4e20.13)') (vrold(j),j=1,jmt0)
      read(io22,'(35x,e20.13)') v0
      if(iprint.ge.1) then
        write(6,'('' potred: jttl2'',a80)') jttl2
        write(6,'('' potred: azed,aa,zcd,efpot'',4e12.4)')              &
     &                       azed,aa,zcd,efpot
        write(6,'('' potred: xst,xmt,jmt0'',9x,2e20.13,i5)')            &
     &                       xst,xmt,jmt0
        write(6,'('' potred: v0'',e12.4)') v0
      endif
!c
      if(abs(ztot-azed).gt.tol) then
         write(6,'('' potred: ztot.ne.azed:'',2d14.6)')                 &
     &                         ztot,azed
         call fstop(sname)
      endif
      if(abs(zcor-zcd).gt.tol) then
         write(6,'('' WARNING: potred: zcor.ne.zcd; Make it equal'',    &
     &              2d14.6)')                                           &
     &                                 zcor,zcd
        zcor=zcd
        zcors=zcor
        zvals=ztot-zcor
      endif
!c     ==========================================================
!c     generate grid for potential read-in ......................
!c     set the energy zero correctly for maj./min. spins.........
      hh=(xmt-xst)/(jmt0-1)
      do j=1,2*iprpts
         xrold(j)=xst+(j-1)*hh
         rrold(j)=exp(xrold(j))
      enddo
      do jj=jmt0+1,2*iprpts
        if(rrold(jj).gt.rmt) then
!c
         a0 = vrold(jmt0)
         a1 = (vrold(jmt0-2)-4.0d0*vrold(jmt0-1)+3.0d0*vrold(jmt0))/    &
     &                             (2*hh)
         a2 = (vrold(jmt0-2)-2.0d0*vrold(jmt0-1)+vrold(jmt0))/          &
     &                             (2*hh*hh)
!c
         a2 =0.5d0*a2
         do j=jmt0+1,jj                  ! "Taylor expansion"
          deltax = xrold(j)-xrold(jmt0)
          vrold(j) = a0 + a1*deltax + a2*deltax*deltax
         end do
         exit
        end if
      end do
      do j=1,2*iprpts
         vrold(j)=vrold(j) - v0*rrold(j)
      enddo
!c
!c     ==========================================================
!c     read in the charge density ...............................
      read(io22,'(i5,e20.13)') ntchg,xvalsws
      if(iprint.ge.1) then
         write(6,'(i5,e20.13)') ntchg,xvalsws
      endif
      if(ntchg.gt.0) then
         read(io22,'(4e20.13)') (chgold(j),j=1,ntchg)
      endif
!c     read in core state definition ............................
      read(io22,'(3i5)') numc,ntcor
      if(iprint.ge.2) then
         write(6,'('' potred: numc,ntcor'',3i5)') numc,ntcor
      endif
      if(numc.gt.0) then
        do j=1,numc
         read(io22,'(3i5,f12.5,9x,a)') nc(j),lc(j),kc(j),ec(j),lj(j)
        enddo
        if(iprint.ge.2) then
          do j=1,numc
          write(6,'('' potred: nc,lc,kc,ec,lj'',3i3,e12.4,2x,a5)')      &
     &        nc(j),lc(j),kc(j),ec(j),lj(j)
          enddo
        endif
      endif
!c     ==========================================================
!c     read in the core density cccccccccccccccccccccccc
      if(ntcor.gt.0) then
         read(io22,'(4e20.13)') (corold(j),j=1,ntcor)
      endif
!c     ==========================================================
!c     interpolate chg density,cor density onto rsw mesh
      if(iprint.ge.2) then
         iup=max(jmt0,ntchg)
         iup=max(iup,ntcor)
         write(6,*) '  potred: prior interp:',ntchg,ntcor
         write(6,*) '  potred: number of points iup=',iup
         write(6,'(''  potred: j,r,v(r),chgold(r),corold(r)'')')
         write(6,'(i4,4e12.4)')                                         &
     &   (j,rrold(j),vrold(j),chgold(j),corold(j),j=1,iup,01)
      endif

      do j=1,jmt
         vr(j)=ylag(xr(j),xrold,vrold,0,3,jmt0,iex)
      enddo
      do j=jmt+1,iprpts
         vr(j)= -v0*rr(j)
      end do
      if(ntchg.gt.0) then
         do j=1,jws
            rhotot(j)=ylag(xr(j),xrold,chgold,0,3,ntchg,iex)
         enddo
      endif
      if(ntcor.gt.0) then
         do j=1,jws
            corden(j)=ylag(xr(j),xrold,corold,0,3,ntcor,iex)
         enddo
      endif

!CAB      if ( abs(alat-aa).gt.1.d-5
      if ((abs(alat-aa).gt.1.d-5.or.mtasa.eq.1)                         &
     &          .and.  ntchg.gt.0)  then

!c
         do j=1,ntchg
           a(j)=(chgold(j)-corold(j))*rrold(j)
         enddo
!c
!c    ===========================================
         call simpun(xrold,a,ntchg,1,b)
!c    ===========================================
         qmtold=b(jmt0)
!c
         do j=1,jws
           a(j)=(rhotot(j)-corden(j))*rr(j)
         enddo
!c    ===========================================
         call simpun(xr,a,jws,1,b)
!c    ===========================================
         qmtnew=b(jmt)
!c
         if(abs(qmtnew).gt.1.d-14) then
          fac=qmtold/qmtnew
         else
          fac = 1.d0
         end if

         deltq = xvalsws*(fac-1.d0)
         xvalsws = xvalsws+deltq
         do j=1,jws
           rhotot(j)=fac*rhotot(j)+(1.0d0-fac)*corden(j)
           x = rr(j)/rws
           vr(j) = vr(j) + deltq*x*(3.d0-x*x)
!c
!c   3/2*(Q^{new}-Q^{old})/R*{1-r^2/(3 R^2)}  -- electrostatic potential
!c   correction due to changes in charges
!c
         enddo
         efpot = efpot + 2.d0*deltq/rws
!c
      endif


!c SUFFIAN, move to input file
        if(ibot_cont.ne.0)then
!c           if ( abs(alat-aa).gt.1.d-5)then
              iset_b_cont = 1
!c           else
!c              iset_b_cont = 0
!c              ebot = ebot_read
!c           endif
        else
              iset_b_cont = 0
!C              ebot = ebot_read
        endif

!c
      if(iprint.ge.1) then
        write(6,'(''  potred: after interp:'',2i5)')
        write(6,'(''  potred: j,r,v(r),vrold(r) '')')
        write(6,'(i4,3e16.7)')(j,rr(j),vr(j),vrold(j),j=1,jws,60)
      endif
!c
!c     ============================================================
      if (istop.eq.sname) then
         call fstop(sname)
      endif

      return
!c
100   coerr =  ' POT-FILE is corrupted????'
      write(6,'('' ztot='',f3.0,(a))') ztot,coerr
      call fstop(sname//':'//coerr)

      return

!EOC
      end subroutine potred


      !=================================================================
      ! MECCA-2005
      !================================================================
!c     cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
!BOP
!!IROUTINE: getpot7
!!INTERFACE:
      subroutine getpot7(nspin,nbasis,komp,                             &
     &                  jmt,jws,rmtstr,rws,xr,rr,h,xstart,              &
     &                  mtasa,iprint,istop,                             &
     &                  rmt,rcs,atcon,numbsub,                          &
     &                  isDLM)
!!DESCRIPTION:
! generation of starting SCF potential based on Thomas-Fermi or
! pre-calculated (for a crystal) table potential or
! reading in potential file
!
!!REVISION HISTORY:
! Initial Version - A.S. - 2005
! Revised         - A.S. - 2013
!EOP
!
!BOC
      use mpi, only: mpi_barrier,mpi_comm_rank,MPI_COMM_WORLD
      implicit none
!c
      character  sname*10
!c
!c     ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
      parameter (sname='getpot7')
!c     ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!c
      character  istop*10
      character  header*80
!c
      integer   nspin,nbasis
      integer   jmt(:)
      integer   jws(:)
      integer   komp(nbasis)
      integer   numbsub(nbasis)
      logical, intent(in) :: isDLM(:)
      integer :: mtasa,iprint
!      integer   numc(ipcomp,ipsublat,ipspin)
!      integer   nc(ipeval,ipcomp,ipsublat,ipspin)
!      integer   lc(ipeval,ipcomp,ipsublat,ipspin)
!      integer   kc(ipeval,ipcomp,ipsublat,ipspin)
      integer   nspinp
      integer   nsites
!c
      real*8    atcon(:,:)
      real*8    rmtstr(:)
      real*8    rws(:)
      real*8    rcs(:)
!      real*8    ztot(ipcomp,nbasis)
!      real*8    zcor(ipcomp,nbasis)
      real*8    xr(:,:)
      real*8    rr(:,:)
      real*8    h(:)
      real*8    xstart(:)
      real*8    rmt(:,:)
!      real*8    vr(iprpts,ipcomp,ipsublat,ipspin)
      real*8    xvalws(size(atcon,1),nbasis,2)
!      real*8    rhotot(iprpts,ipcomp,ipsublat,ipspin)
!      real*8    corden(iprpts,ipcomp,ipsublat,ipspin)
!      real*8    ec(ipeval,ipcomp,ipsublat,ipspin)
      real*8    eBotnk, ebot_in

      real(8) :: efpotnk,renorm
!vdif     real(8) :: rnn_min
      real(8) :: v0i(2),vdif,v0itmp,vshift
      integer :: ns,nk,nsub,ndr
      integer :: komp_nsub,mk

      real(8), target  :: efpot_t
      real(8), pointer :: efpot
      real(8) :: eBottom,zval
      type(SphAtomDeps), pointer :: v_rho_box,v_rho_box1
      integer :: vers
      character(80) :: msg
      character(16) :: c_w1,c_w2
      character(2)  :: c_w3
      integer       :: ierr,myrank
!c
!c     ******************************************************************
!c     cccccccccc read in the potentials for current sublattice if cccccc
!c     cccccccccc these are not equivalent to some other sublattice ccccc
!c     cc   for spin polarized calc. vdif measures the difference between
!c     cc   mt-zero for maj. and min. spin electrons (assume vmtz(maj)=0)
!c     ******************************************************************
!c
!?      header = ' nspin  vdif :::::::::'

      ebot_in = M_S%intfile%ebot
      eBottom = M_S%intfile%ebot
      efpot_t = M_S%intfile%etop
      if ( efpot_t == zero ) then
        efpot   => M_S%intfile%etop
      else
        efpot => efpot_t
      end if
      nspinp = -1
      vdif   = zero
      vers = -1

!DEBUG
!      inquire(unit=io22,opened=open22 )
!      if ( .not. open22 ) then
!          call p_fstop(' unexpected error: FILE IS NOT OPENED?!')
!      end if
!DEBUG
      read(io22,'(a80)',err=2,end=2,iomsg=msg) header
      call check_version(header,vers)
      if ( vers <= 0 ) then
        read(io22,*,err=2,end=2)nspinp,vdif
      end if
2     continue
      if ( nspinp .lt. 1 ) then
        if ( vers>0 ) then
          nspinp = nspin
          rewind(io22)
        else
         nspinp = 1
         call mpi_comm_rank(MPI_COMM_WORLD,myrank,ierr)
         if(myrank.ne.0) close(io22)
         call mpi_barrier(MPI_COMM_WORLD,ierr)
         if(myrank.eq.0) close(io22,status='DELETE',err=3)
         call mpi_barrier(MPI_COMM_WORLD,ierr)
        end if
3       continue
      else if(nspinp.gt.2) then
       write(6,'(''   nspinp = '',i5,'' > 2 '')')nspinp
       call p_fstop(sname)
      endif
!c
!c     ----------------------------------------------------------
!      call zeroout(vr,iprpts*ipcomp*ipsublat*ipspin)
!c     ----------------------------------------------------------
!      call zeroout(rhotot,iprpts*ipcomp*ipsublat*ipspin)
!c     ----------------------------------------------------------
!      call zeroout(corden,iprpts*ipcomp*ipsublat*ipspin)
!c     ----------------------------------------------------------
      nsites = sum(numbsub)
      efpot = zero
      eBottom = 10000.d0
      v0i = zero
      do ns=1,nspinp
         do nsub=1,nbasis
            komp_nsub = komp(nsub)
            renorm = one
            if ( vers>0 ) then
                if ( isDLM(nsub) ) then
                 read(io22,'(a80)',err=2,end=2,iomsg=msg) header
                 backspace(io22)
                 read(header,*) c_w1,c_w2,c_w3
                 if ( trim(c_w3) == c_up .or. trim(c_w3) == c_dn ) then
                    komp_nsub = komp(nsub)/2
                    renorm = two
                 else if ( trim(c_w3) == c_dlm ) then
                    komp_nsub = komp(nsub)
                 else
                  write(6,*) ' header=',header
                  write(6,*) ' c_w1=',c_w1
                  write(6,*) ' c_w2=',c_w2
                  write(6,*) ' c_w3=',c_w3
                  write(6,'(''   spin-polarised or DLM potential file ''&
     &           ,''is required for DLM calculations, nsub='',i5)') nsub
                   call p_fstop(sname)
                 end if
                end if
            end if
            do nk=1,komp_nsub
             if ( iprint>= -1 ) then
              if ( nsub==1 .and. nk==1 ) write(6,*)
              if ( nsub<=50 .or. iprint>=0 ) then
                  write(6,'(''   Sublat.='',i2,'' Spin='',i2,''  '',a,  &
     &                                                 t40,'' V(r)'')') &
     &               nsub,ns,M_S%intfile%sublat(nsub)%compon(nk)%name
               if ( iprint==-1 .and. nsub==50 )                         &
     &        write(6,*) ' printing is skipped for other sublattices...'
              end if
             end if
!c             ------------------------------------------------------
             v_rho_box => M_S%intfile%sublat(nsub)%compon(nk)%v_rho_box
             ndr = v_rho_box%ndrpts
             call potred7(jmt(nsub),rmtstr(nsub),                       &
     &                     jws(nsub),rws(nsub),rcs(nsub),               &
     &                     xr(1:v_rho_box%ndrpts,nsub),                 &
     &                     rr(1:v_rho_box%ndrpts,nsub),                 &
     &                     h(nsub),xstart(nsub),                        &
     &                     v_rho_box%vr(1:v_rho_box%ndrpts,ns),v0itmp,  &
     &                     v_rho_box%chgtot(1:v_rho_box%ndchg,ns),      &
     &                     v_rho_box%chgcor(1:v_rho_box%ndcor,ns),      &
     &                     xvalws(nk,nsub,ns),                          &
     &                   dble(M_S%intfile%sublat(nsub)%compon(nk)%zID), &
     &                   dble(M_S%intfile%sublat(nsub)%compon(nk)%zcor),&
     &                     v_rho_box%numc(ns),                          &
     &                     v_rho_box%nc(:,ns),v_rho_box%lc(:,ns),       &
     &                     v_rho_box%kc(:,ns),v_rho_box%ec(:,ns),       &
     &                     eBotnk,                                      &
     &                     M_S%intfile%alat,efpotnk,                    &
     &                     mtasa,iprint,istop)
!c             -----------------------------------------------------
             efpot = efpot + efpotnk*renorm*atcon(nk,nsub)*numbsub(nsub)
             v_rho_box%ec(1:v_rho_box%numc(ns),ns) =                    &
     &         v_rho_box%ec(1:v_rho_box%numc(ns),ns) + v0itmp
             eBottom = min(eBottom,eBotnk)
             v0i(ns)=v0i(ns)+v0itmp*renorm*atcon(nk,nsub)*numbsub(nsub)
             nullify(v_rho_box)
            enddo
            do nk=komp_nsub+1,komp(nsub)
             mk = nk-komp_nsub
             v_rho_box => M_S%intfile%sublat(nsub)%compon(mk)%v_rho_box
             v_rho_box1=> M_S%intfile%sublat(nsub)%compon(nk)%v_rho_box
             v_rho_box1%ec(:,1) = v_rho_box%ec(:,1)
             xvalws(nk,nsub,ns) = xvalws(mk,nsub,ns)
             v_rho_box1%ndchg   = v_rho_box%ndchg
             v_rho_box1%chgtot(1:v_rho_box%ndcor,ns) =                  &
     &                      v_rho_box%chgtot(1:v_rho_box%ndcor,ns)
             v_rho_box1%ndcor   = v_rho_box%ndcor
             v_rho_box1%chgcor(1:v_rho_box%ndchg,ns) =                  &
     &                      v_rho_box%chgcor(1:v_rho_box%ndchg,ns)
             v_rho_box1%ndrpts  = v_rho_box%ndrpts
             v_rho_box1%vr(1:v_rho_box%ndrpts,ns) =                     &
     &                       v_rho_box%vr(1:v_rho_box%ndrpts,ns)
             v_rho_box1%numc(ns) = v_rho_box%numc(ns)
             v_rho_box1%nc(:,ns) = v_rho_box%nc(:,ns)
             v_rho_box1%lc(:,ns) = v_rho_box%lc(:,ns)
             v_rho_box1%kc(:,ns) = v_rho_box%kc(:,ns)
             v_rho_box1%ec(:,ns) = v_rho_box%ec(:,ns)
             nullify(v_rho_box)
             nullify(v_rho_box1)
            end do
         enddo
         v0i(ns) = v0i(ns)/nsites
      enddo
      efpot = efpot/(nspinp*nsites)
      if ( M_S%intfile%etop == zero ) then
        zval = g_zvaltot(M_S%intfile,.true.)
        M_S%intfile%etop = g_efestim(zval,2.d0*M_S%rs_box%volume)
      end if
      do nsub=1,nbasis
       do nk=1,komp(nsub)
         v_rho_box => M_S%intfile%sublat(nsub)%compon(nk)%v_rho_box
         do ns=1,nspinp
          v_rho_box%ec(1:v_rho_box%numc(ns),ns) =                       &
     &  v_rho_box%ec(1:v_rho_box%numc(ns),ns)-sum(v0i(1:nspinp))/nspinp
         end do
         nullify(v_rho_box)
       end do
      end do
!c
!c   Magn. --> Non-Magn.
!c
      if(nspin.eq.1.and.nspinp.eq.2) then
       vdif = zero
       do nsub=1,nbasis
        do nk=1,komp(nsub)
       v_rho_box => M_S%intfile%sublat(nsub)%compon(nk)%v_rho_box
       xvalws(nk,nsub,1)=xvalws(nk,nsub,1)+xvalws(nk,nsub,2)
       v_rho_box%chgtot(1:v_rho_box%ndchg,1) =                          &
     &                      v_rho_box%chgtot(1:v_rho_box%ndchg,1) +     &
     &                            v_rho_box%chgtot(1:v_rho_box%ndchg,2)
       v_rho_box%chgcor(1:v_rho_box%ndcor,1) =                          &
     &                      v_rho_box%chgcor(1:v_rho_box%ndcor,1) +     &
     &                            v_rho_box%chgcor(1:v_rho_box%ndcor,2)
       v_rho_box%vr(1:v_rho_box%ndrpts,1) = 0.5d0 *                     &
     &                      (v_rho_box%vr(1:v_rho_box%ndrpts,1) +       &
     &                               v_rho_box%vr(1:v_rho_box%ndrpts,2))
       nullify(v_rho_box)
        enddo
       enddo
       v0i(1) = 0.5d0*(v0i(1)+v0i(nspin))
      end if
!c
!c   Non-Mag. --> Magn.
!c
      vshift = zero
      if(nspin.eq.2.and.nspinp.eq.1) then
       if(abs(vdif).lt.1.d-7) then
!        vshift = vdif0/2
!vdif        rnn_min = M_S%intfile%alat
!vdif        do nsub=1,nbasis
!vdif         if ( maxval(M_S%intfile%sublat(nsub)%compon(1:komp(nsub))%     &
!vdif     &             zID)>0 ) then
!vdif          rnn_min = min(rnn_min,2*rmtstr(nsub))
!vdif         end if
!vdif        end do
!vdif        vshift = -vdif0/rnn_min
        write(6,*)
        write(6,'(''   nspinp .ne. nspin '', 2i5)')nspinp,nspin
        write(6,'(2a,g12.4,a)')                                         &
     &  ' Non-magn. initial potential is used for magnetic case'
!     &            ,' with spin split = ',vdif0,' Ry'
!vdif     &             ' magnetic case: vdif0=',vdif0/rnn_min
        write(6,*)
       else
        vshift = vdif/2
       end if
       v0i(nspin) = v0i(1) + vshift
       v0i(1) = v0i(1) - vshift
       do nsub=1,nbasis
        do nk=1,komp(nsub)
         v_rho_box => M_S%intfile%sublat(nsub)%compon(nk)%v_rho_box
         xvalws(nk,nsub,2)=0.5d0*xvalws(nk,nsub,1)
         xvalws(nk,nsub,1)=xvalws(nk,nsub,2)
         v_rho_box%numc(2)=v_rho_box%numc(1)
         v_rho_box%nc(:,2)=v_rho_box%nc(:,1)
         v_rho_box%lc(:,2)=v_rho_box%lc(:,1)
         v_rho_box%kc(:,2)=v_rho_box%kc(:,1)
         v_rho_box%ec(:,2)=v_rho_box%ec(:,1)
         v_rho_box%chgtot(1:v_rho_box%ndchg,1) =                        &
     &                     0.5d0*v_rho_box%chgtot(1:v_rho_box%ndchg,1)
         v_rho_box%chgcor(1:v_rho_box%ndcor,1) =                        &
     &                     0.5d0*v_rho_box%chgcor(1:v_rho_box%ndcor,1)
         v_rho_box%chgtot(1:v_rho_box%ndchg,2) =                        &
     &                           v_rho_box%chgtot(1:v_rho_box%ndchg,1)
         v_rho_box%chgcor(1:v_rho_box%ndcor,2) =                        &
     &                           v_rho_box%chgcor(1:v_rho_box%ndcor,1)
         v_rho_box%vr(1:v_rho_box%ndrpts,1) =                           &
     &                           v_rho_box%vr(1:v_rho_box%ndrpts,1) +   &
     &                                vshift*rr(1:v_rho_box%ndrpts,nsub)
         v_rho_box%vr(1:v_rho_box%ndrpts,2) =                           &
     &                           v_rho_box%vr(1:v_rho_box%ndrpts,1) -   &
     &                                vshift*rr(1:v_rho_box%ndrpts,nsub)
         nullify(v_rho_box)
        enddo
       enddo
      end if
      do nsub=1,nbasis
       do nk=1,komp(nsub)
        v_rho_box => M_S%intfile%sublat(nsub)%compon(nk)%v_rho_box
        v_rho_box%v0(1:nspin) = v0i(1:nspin)
        nullify(v_rho_box)
       enddo
      enddo
      M_S%intfile%V0 = v0i
      if ( abs(M_S%intfile%ebot) <=  epsilon(one) ) then
       if ( M_S%intfile%iset_ebot > 0 ) then
        M_S%intfile%ebot=min(eBottom,ebot_in)
       end if
      end if

!c     ===============================================================
      if (istop.eq.sname) then
       call p_fstop(sname)
      else
         return
      endif
!c
!EOC
      end subroutine getpot7
!c
!c =====================================================================
!BOP
!!IROUTINE: genRmesh
!!INTERFACE:
      subroutine genRmesh(mtasa,rmt,rws,Rcs,ztot,nmesh,                 &
     &                   xr,rr,h,xstart,jmt,jws)
!!DESCRIPTION:
! radial- and log-mesh generator
!
!!REVISION HISTORY:
! Initial Version - A.S. - 2013
!!REMARKS:
! set number of points, jmt, up to sphere boundary, rmt, i.e. m.t. or a.s.a
! For Total Energy calculation (jmt-1) must be divisible by 4
!EOP
!
!BOC
      implicit none
      integer mtasa
      real*8  rmt
      real*8  rws
      real*8  Rcs
      real*8  ztot
      integer nmesh
      real*8  xr(*)
      real*8  rr(*)
      real*8  h
      real*8  xstart
      integer jmt
      integer jws

      integer j,nrmx
      integer indRmesh
      external indRmesh
      real(8) :: Rmx,vr0

      character*10 sname
      parameter (sname='genRmesh')
!c
!c set log-grid starting point
!c
!      real(8), parameter :: xstart=-11.1309674d0  ! ~1.5*10^(-5) Bohr
      real(8) :: hm,hmesh
      integer :: iz_mod,st_env
      logical, external :: isLDAxc

      hm = gXRmeshStep()             ! to have reasonable accuracy with LDA, it should be 0.01 or less
      if ( Rcs > rws ) then
        Rmx = Rcs
      else
        Rmx = rws*1.43333d0
      end if
      if ( mtasa>0 ) rmt = rws
      call twoZ(ztot,zero,1,vr0)
      xstart = log(2.d0*Rnucl(one))
      if ( ztot==zero ) then
       xstart = log(1.6d-3)
      end if
      if ( isLDAxc()  ) then
       hmesh = hm
      else
       hmesh = hm/2   ! better accuracy is needed with non-LDA functionals
      end if
!      nrmx = 1 + ceiling((log(Rmx)-xstart)/hmesh)
      nrmx = 1 + ceiling((log(rmt)-xstart)/hmesh)
      h = (log(rmt) - xstart)/(nrmx-1)
      nrmx = 1 + ceiling((log(Rmx)-xstart)/h)
      if ( nmesh <= 0 ) then
          nmesh = nrmx+1
          return
      end if
      if ( nrmx > nmesh-1 ) then
        nrmx = nmesh-1
        xstart = log(Rmx) - h*(nrmx-1)
      end if
      h = (log(Rmx) - xstart)/(nrmx-1)
      if ( h > hm ) then
          write(6,'('' WARNING: radial step-size '',f7.3,               &
     &  '' may be not small enough to provide acceptable accuracy'')') h
      end if
!c
      jmt = (log(rmt) - xstart)/h+1
!c
      xstart = log(rmt) - h*(jmt-1)
      do j=1,nmesh
        xr(j)=xstart+(j-1)*h
        rr(j)=exp(xr(j))
      enddo
      rr(jmt) = rmt
!c
!c set number of pts. to w.s. radius
      if (mtasa.le.0) then
       jws = indRmesh(rws,nmesh,rr)
      else
       jws = jmt
      end if

      if ( jws > nmesh-1 ) then
        write(6,*) ' rmt=',rmt,' rws=',rws
        write(6,*) ' xstart=',xstart
        write(6,*) ' h=',h
        write(6,*) ' nmesh=',nmesh
        write(6,*) ' jws=',jws
        call p_fstop(sname//': jws>nmesh-1?????')
      end if

      if(Rcs.gt.rr(nmesh-1)) then
        write(*,*) ' Rcs=',Rcs
        write(*,*) ' nmesh=',nmesh
        write(*,*) ' rr(nmesh-1)=',rr(nmesh-1)                          &
     &,            ' rr(nmesh)=',rr(nmesh)
        call p_fstop(sname//': Rcs>rr(nmesh-1)?????')
      end if

      return
!EOC
      end subroutine genRmesh
!c
!c =====================================================================
!BOP
!!IROUTINE: potred7
!!INTERFACE:
      subroutine potred7(jmt,rmtin,jws,rws,Rcs,                         &
     &                   xr,rr,h,xstart,                                &
     &                   vr,v0i,                                        &
     &                   rhotot,corden,                                 &
     &                   xvalsws,ztot,zcor,                             &
     &                   numc,nc,lc,kc,ec,eBottom,                      &
     &                   alat,efpot,                                    &
     &                   mtasa,iprint,istop)
!!DESCRIPTION:
! To read-in potential file or to generate default potential
!
!!REVISION HISTORY:
! Initial Version - A.S. - 2005
! Revised         - A.S. - 2013
!EOP
!
!BOC
      implicit real*8 (a-h,o-z)

      integer :: jmt,jws,numc,mtasa,iprint
!c
      character jttl2*80
      character lj(ipeval)*5
      character istop*10
      character sname*10
!c
      integer   nc(:)
      integer   lc(:)
      integer   kc(:)
!c
      real*8    ec(:)
      real*8    eBottom
      real*8    xr(:)
      real*8    rr(:)
      real*8    vr(:)
      real*8    rhotot(:)
      real*8    corden(:)

      integer   ip2mesh
      integer :: jlast,j,jwsold,ntchg,ntcor,iup,iex,nelt,ndrpts

!C      parameter (ip2mesh = 2*iprpts)
!c      real*8    xrold(2*iprpts)
!c      real*8    rrold(2*iprpts)
!c      real*8    vrold(2*iprpts)
!c      real*8    chgold(2*iprpts)
!c      real*8    corold(2*iprpts)
!c      real*8    a(2*iprpts)
!c      real*8    b(2*iprpts)

      real*8,  allocatable :: xrold(:),rrold(:),vrold(:),vr_nucl(:)
      real*8,  allocatable :: chgold(:),corold(:)
      real*8,  allocatable :: a(:),b(:)

      real*8    v0i
      real*8    qmtnew,qmtold,ebot_read

!c parameter
      real*8    tol
!c
!c ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
      parameter (tol=(one/ten)**5)

      real*8 h
      integer vers
      parameter (sname='potred7')

      ndrpts = size(vr)
      if ( size(rr) < ndrpts ) then
        write(6,'('' ERROR: inconsistent memory size, size(rr)='',i5,   &
     &            '' < size(vr)='',i5)') size(rr),size(vr)
      end if
      if ( size(xr) < ndrpts ) then
        write(6,'('' ERROR: inconsistent memory size, size(xr)='',i5,   &
     &            '' < size(vr)='',i5)') size(rr),size(vr)
      end if
      if ( max(size(rr),size(xr))<size(vr) ) then
       call fstop('unexpected memory issue')
      end if

      if(iprint.ge.1) then
        write(6,'(3x,a,'': jmt,h,rr(1),rmt '',i4,3e14.6)')              &
     &                          sname,jmt,h,rr(1),rmtin
      endif
!c
      INQUIRE( unit=io22,opened=open22 )

      if ( .not. open22 ) go to 100

!c     read in potential deck....................................
      read(io22,'(a80)',err=100,end=100) jttl2
      call check_version(jttl2,vers)
!
      select case(vers)
      case(:0)
       read(io22,'(f5.0,13x,f12.5,f5.0,f12.5,e20.13)',err=100,end=100)  &
     &          azed,aa,zcd,ebot_read,efpot
       read(io22,*) xst,xlast,jwsold
       jlast = jwsold
      case(1:)
!       read(io22,'(f5.0,17x,f12.5,f5.0,e20.13)',err=100,end=100)        &
       read(io22,*,err=100,end=100)                                     &
     &          azed,aa,zcd,efpot
       read(io22,*) xst,xlast,jwsold
       read(io22,*) jlast
      end select
!
      hh=(xlast-xst)/(jwsold-1)
      ip2mesh = max(2*ndrpts,2*jlast)
      allocate(                                                         &
     &    xrold(ip2mesh)                                                &
     &,   rrold(0:ip2mesh)                                              &
     &,   vrold(0:ip2mesh)                                              &
     &,   vr_nucl(1:ip2mesh)                                            &
     &,   chgold(0:ip2mesh)                                             &
     &,   corold(ip2mesh)                                               &
     &,   a(ip2mesh)                                                    &
     &,   b(ip2mesh)                                                    &
     &        )

!c     ----------------------------------------------------------
      call zeroout(vrold,ip2mesh+1)
      call zeroout(chgold,ip2mesh+1)
      call zeroout(corold,ip2mesh)
!c     ----------------------------------------------------------
      call zeroout(rhotot,ndrpts)
      call zeroout(corden,ndrpts)
!c     ----------------------------------------------------------
!c     generate grid for potential read-in ......................
      do j=1,ip2mesh
         xrold(j)=xst+(j-1)*hh
         rrold(j)=exp(xrold(j))
      enddo
!c     ----------------------------------------------------------
      read(io22,'(4e20.13)') (vrold(j),j=1,jlast)
      read(io22,'(35x,e20.13)') v0i
      if(iprint.ge.1) then
        write(6,'(1x,a,'': jttl2'',a80)') sname,jttl2
        write(6,'(1x,a,'': azed,aa,zcd,efpot'',4e12.4)')                &
     &                       sname,azed,aa,zcd,efpot
        write(6,'(1x,a,'': xst,xlast,jlast'',9x,2e20.13,2i5)')          &
     &                       sname,xst,xlast,jwsold,jlast
        write(6,'(1x,a,'': v0i'',e12.4)') sname,v0i
      endif
      nc = 0
      lc = 0
      kc = 0
      ec = zero
!c
!c     ==========================================================
!c     read in the charge density ...............................
!c
      ntchg = 0 ; ntcor = 0 ; numc = 0
      select case(vers)
      case(:0)
       read(io22,'(i5,e20.13)') ntchg,xvalsws
       read(io22,*) (chgold(j),j=1,ntchg)
       read(io22,'(i5,i5)') numc,ntcor
       if ( numc > 0 ) then
        do j=1,numc    ! read in core state definition ...................
         read(io22,'(3i5,f12.5,9x,a5)') nc(j),lc(j),kc(j),ec(j),lj(j)
        enddo
        if(ntcor.gt.0) then
         read(io22,*) (corold(j),j=1,ntcor)
        endif
       endif
      case(1:)
       read(io22,'(i5,e20.13)') ntchg,xvalsws
       if ( ntchg>0 ) then
        read(io22,'(4e20.13)') (chgold(j),j=1,ntchg)
        read(io22,'(i5)') numc
        if ( numc > 0 ) then
         do j=1,numc    ! read in core state definition ..................
          read(io22,'(3i5,f12.5,9x,a5)') nc(j),lc(j),kc(j),ec(j),lj(j)
         enddo
         read(io22,'(i5)') ntcor
         if(ntcor.gt.0) then
          read(io22,'(4e20.13)') (corold(j),j=1,ntcor)
         endif
        end if
       end if
      end select
!
      if(iprint.ge.1) then
       write(6,'(i5,1x,e20.13,1x,i5)') ntchg,xvalsws,jwsold
       write(6,'(1x,a,'': numc'',2i5)') sname,numc
       if(numc.gt.0) then
         do j=1,numc
          write(6,'(1x,a,'': nc,lc,kc,ec,lj'',3i3,e12.4,2x,a)')         &
     &        sname,nc(j),lc(j),kc(j),ec(j),lj(j)
         enddo
       endif
      endif
!c
!c     ==========================================================
!c
!c     set the energy zero correctly for maj./min. spins.........
!c
      rrold(0)  = zero
      chgold(0) = zero

      if ( vers > 0 ) then
        vrold(0) = vrold(1)
      else
        vrold(0) = vrold(1)                                             &
     &         + (vrold(1)-vrold(2))*rrold(1)/(rrold(2)-rrold(1))
        if ( abs(nint(vrold(0))) > 0 ) then
         vrold(0) = nint(vrold(0))
        else
         vrold(0) = zero
        end if
      end if

      xendchg = xrold(ntchg)

!c     interpolate chg density,cor density onto rsw mesh
      if(iprint.ge.1) then
       iup=max(jlast,ntchg,ntcor)
         write(6,*) sname//': prior interp:',ntchg,ntcor
         write(6,*) sname//': number of points iup=',iup
         write(6,'(1x,a,'': j,r,v(r),chgold(r),corold(r)'')') sname
         write(6,'(i4,4e12.4)')                                         &
     &   (j,rrold(j),vrold(j),chgold(j),corold(j),j=1,iup,01)
      endif

      do j=1,ndrpts
       if(xr(j).lt.-2.d0-tiny(2.d0)) then
      vr(j)=ylag(rr(j),rrold(0),vrold(0),0,3,jlast,iex)
       else if(xr(j)<=xrold(jlast)) then
      vr(j)=ylag(xr(j),xrold(1),vrold(1),0,3,jlast,iex)
       else
        if ( vers>0 ) then
          vr(j) = vrold(jlast)
        else
          vr(j) = vrold(jlast)*rr(j)/rr(jlast)
        end if
       end if
      enddo
      if ( vers>0 ) then
        call twoZ(ztot,rr(1:ndrpts),ndrpts,vr_nucl(1:ndrpts))
        vr(1:ndrpts) = vr(1:ndrpts)*rr(1:ndrpts) + vr_nucl(1:ndrpts) ! vr_nucl=-2*ztot
      end if
      if ( vers==0 ) then
        v0i = vr(jws)/rr(jws)
      end if

!c
      if(ntchg.gt.0) then
       do j=1,ndrpts
        if(xr(j).lt.xrold(min(ntchg+1,ip2mesh)) ) then
         if(xr(j).lt.-2.d0-tiny(2.d0)) then
          rhotot(j)=ylag(rr(j),rrold(0),chgold(0),0,3,ntchg+1,iex)
         else if(rr(j).lt.rws-tiny(rws)) then
          rhotot(j)=ylag(xr(j),xrold(1),chgold(1),0,3,ntchg,iex)
         else
          rhotot(j)=ylag(xr(j),xrold(1),chgold(1),0,2,ntchg,iex)
         end if
        else
          rhotot(j)=(chgold(ntchg)/rrold(ntchg)**2)*rr(j)**2
        end if
       enddo
      endif

      if(ntcor.gt.0) then
         do j=1,jws
        if(xr(j).lt.xrold(min(jwsold+1,ip2mesh))-tiny(rws)) then
          corden(j)=ylag(xr(j),xrold,corold,0,3,ntcor,iex)
        end if
       enddo
      endif
      nelt = 0
      call coreOrder(numc,ec,nc,lc,kc)
      ec(1:numc) = ec(1:numc) - v0i

      select case(vers)
      case(:0)
        eBottom = ebot_read
      case(1:)
        call estimatEbot(zcor,numc,ec,lc,kc,eBottom)
        if(abs(ztot-azed).gt.tol) then
         write(6,'(1x,a,'': ztot.ne.azed:'',2d14.6)')                   &
     &                         sname,ztot,azed
         call zeroout(rhotot,ndrpts)
         call zeroout(corden,ndrpts)
!c      ----------------------------------------------------------
         ec(1:numc) = ec(1:numc) + v0i
         v0i = -0.25d0
         ec(1:numc) = ec(1:numc) - v0i
         return
        endif
      end select

      if ((rr(jws).ne.rrold(jwsold).or.jws.ne.jwsold).and.mtasa.ne.0    &
     &          .and. ntchg>0 .and. ztot>0 )  then
!c
       do j=1,jwsold
           a(j)=(chgold(j)-corold(j))*rrold(j)
         enddo
!c
!c    ==================================
       call simpun(xrold,a,jwsold,1,b)
!c    ==================================
!c
       qmtold=b(jwsold)
!c
         do j=1,jws
           a(j)=(rhotot(j)-corden(j))*rr(j)
         enddo
!c
!c    =============================
         call simpun(xr,a,jws,1,b)
!c    =============================
!c
       qmtnew=b(jws)
!c
       if(max(abs(qmtold),abs(qmtnew)).gt.1.d-8) then
        fac=qmtold/qmtnew
        if(abs(fac-1.d0).gt.0.2d0*rws/abs(xvalsws))                     &
     &     fac = 1.d0+sign(0.2d0*rws/abs(qmtold),fac-1.d0)
       else
        fac = 1.d0
       end if
       deltq = xvalsws*(fac-1.d0)
       xvalsws = xvalsws+deltq

       do j=1,ndrpts
         rhotot(j)=fac*rhotot(j)+(1.0d0-fac)*corden(j)
         x = rr(j)/rws
         if(x.le.1.d0) then
          vr(j) = vr(j) + deltq*x*(3.d0-x*x)
!c
!c   3/2*(Q^{new}-Q^{old})/R*{1-r^2/(3 R^2)}  -- electrostatic potential
!c   correction due to changes in charges
!c
         else
          vr(j) = vr(j) + deltq*2.d0
         end if
       enddo
!       efpot = efpot + 2.d0*deltq/rws
!c
      endif
!c
      if(iprint.ge.0) then
      write(6,'(2x,a,'': after interp:'',2i5)') sname,jmt,jws
      write(6,'(2x,a,'': j,r,r*v,v(r)'')') sname
      write(6,'(i4,3e16.7)')(j,rr(j),vr(j),vr(j)/rr(j),j=1,jws,60)
      write(6,'(i4,3e16.7)') jws,rr(jws),vr(jws),vr(jws)/rr(jws)
      endif
      if ( vers>0 ) then
        vr(1:ndrpts) = vr(1:ndrpts) - v0i*rr(1:ndrpts)
      end if
!c
!c     ============================================================

      deallocate(                                                       &
     &    xrold                                                         &
     &,   rrold                                                         &
     &,   vrold                                                         &
     &,   vr_nucl                                                       &
     &,   chgold                                                        &
     &,   corold                                                        &
     &,   a                                                             &
     &,   b                                                             &
     &        )

      if (istop.eq.sname) then
         call fstop(sname)
      else
         return
      endif
!c
100   continue

      if ( allocated(xrold) ) then
        deallocate(                                                     &
     &    xrold                                                         &
     &,   rrold                                                         &
     &,   vrold                                                         &
     &,   chgold                                                        &
     &,   corold                                                        &
     &,   a                                                             &
     &,   b                                                             &
     &        )
      end if

!c     ----------------------------------------------------------
      call zeroout(vr,ndrpts)
      call zeroout(rhotot,ndrpts)
      call zeroout(corden,ndrpts)
!c     ----------------------------------------------------------
      v0i = 0.d0
      call getDfltPot(ztot,ndrpts,rr,eBottom,v0i,                       &
     &            vr,numc,nc,lc,kc,ec,zcor,                             &
     &            xvalsws,efpot)
      vr(1:ndrpts) = vr(1:ndrpts) - v0i*rr(1:ndrpts)

!c      v0i = 0.d0
!c      call inipot(jws,rws,rr,ztot,
!c     ,            vr,rhotot,numc,nc,lc,kc,ec,zcor,
!c     *            xvalsws,efpot)

      return
!EOC
      end subroutine potred7
!c
!c =====================================================================
!BOP
!!IROUTINE: getDfltPot
!!INTERFACE:
      subroutine getDfltPot(ztot,ndimx,rr,eBottom,v0i,                  &
     &                   vr,numc,nc,lc,kc,ec,zcor,                      &
     &                   zval,efpot)
!!DESCRIPTION:
! default (either Thomas-Fermi or table) potential generator
!
!!REVISION HISTORY:
! Initial Version - A.S. - 2013
!!REMARKS:
! this is INTERNAL (private) module subroutine
!EOP
!
!BOC
      implicit none
      real*8  ztot
      integer ndimx
      real*8 rr(*)
      real*8 eBottom
      real*8 v0i
      real*8  vr(*)

      integer numc
      integer nc(*),lc(*),kc(*)
      real*8  ec(*)
      real*8  zcor,zval,efpot

      real*8 vrold(0:2*ndimx)
      real*8 xst,xmt,x0,x1,h,vs
      integer nregion
      integer nbrgn(ndimx),norder(ndimx),nchb
      real*8 chbcoeff(ndimx)
      integer i0,i,irgn,k0

      integer jlast,iex
      real*8 xrold(2*ndimx),rrold(0:2*ndimx),vr_nucl(2*ndimx)

      real*8 chebev
      external chebev

      character sname*10
      parameter (sname='getDfltPot')

      efpot = 0.55d0
      if(nint(ztot).le.0) then
       zcor = 0.d0
       zval = 0.d0
       numc = 0
       v0i =  -0.1d0
!DELETE       vr(1:ndimx) = v0i * exp(-(0.5d0*rr(1:ndimx))**2)*rr(1:ndimx)
       vr(1:ndimx) = v0i * rr(1:ndimx)
       return
      end if

      call initVr(ztot,xst,xmt                                          &
     &,                  nregion,nbrgn,norder,nchb,chbcoeff             &
     &,                  numc,ec,nc,lc,kc                               &
     &,                  v0i,efpot)

      ec(1:numc) = ec(1:numc) - v0i      ! everywhere in the code (except read/write), they are relative to MT-zero
      call estimatEbot(zcor,numc,ec,lc,kc,eBottom)
!c      call coreOrder(numc,ec,nc,lc,kc,v0i,eBottom,zcor)
      zval = ztot - zcor

      IF ( .not. thomas_fermi .and. ztot<=86.d0 ) THEN

      if ( nchb .gt. ndimx) then
       write(*,*) ' nchb=',nchb,' ndimx=',ndimx
       call fstop(sname//':: ERROR : please increase ndimx ')
      end if

      k0 = 1
      i0 = 1
      x0 = dble(1)
      x1 = dble(nbrgn(1))
      h = (xmt-xst)/(nbrgn(nregion)-1)
      jlast = nbrgn(nregion)
      vrold(0:jlast) = 0.d0
      do irgn=1,nregion
       if (irgn.eq.1) then
        x0 = dble(1)
        i0 = 1
       else
        i0 = nbrgn(irgn-1)
        x0 = dble(nbrgn(irgn-1))
       end if
       x1 = dble(nbrgn(irgn))
       if (irgn.ne.nregion) x1 = x1 + dble(1)

       rrold(i0) = exp(xst + (i0-1)*h)
       if (irgn.eq.1) then
        vrold(i0) = chebev(x0,x1,                                       &
     &                chbcoeff(k0),norder(irgn),dble(i0))
       else
        vrold(i0) = vrold(i0) + 0.5d0*chebev(x0,x1,                     &
     &                chbcoeff(k0),norder(irgn),dble(i0))
       end if

!C       write(111,'(1x,i4,1x,f18.13)') i0,vrold(i0)

       do i=i0+1,nbrgn(irgn)-1
        rrold(i) = exp(xst + (i-1)*h)
        vrold(i) = chebev(x0,x1,                                        &
     &                chbcoeff(k0),norder(irgn),dble(i))

!C        write(111,'(1x,i4,1x,f18.13)') i,vrold(i)

       end do

       rrold(nbrgn(irgn)) = exp(xst + (nbrgn(irgn)-1)*h)
       if (irgn.eq.nregion) then
        vrold(nbrgn(irgn)) = chebev(x0,x1,                              &
     &                chbcoeff(k0),norder(irgn),dble(nbrgn(irgn)))
       else
        vrold(nbrgn(irgn)) = 0.5d0*chebev(x0,x1,                        &
     &                chbcoeff(k0),norder(irgn),dble(nbrgn(irgn)))
       end if
!C       write(111,'(1x,i4,1x,f18.13)') nbrgn(irgn),vrold(nbrgn(irgn))

       k0 = k0 + norder(irgn)
      end do

!C      close(111)

!c      do i=1+nbrgn(nregion),ndimx
!c       vr(i) = vr(nbrgn(nregion))*exp(-xmt+xst + (i-1)*h)
!c      end do
      if( nbrgn(nregion) .gt. 2*ndimx) then
       write(*,*) ' nbrgn(nregion)=',nbrgn(nregion),' > 2*',ndimx
       call fstop(sname//':: ERROR : please increase ndimx ')
      end if
      call twoZ(ztot,rrold(1:jlast),jlast,vr_nucl(1:jlast))
      vrold(1:jlast) = vrold(1:jlast) + vr_nucl(1:jlast)
      rrold(0) = 1.d-6
      call twoZ(ztot,rrold(0),1,vrold(0))

      xrold(1:jlast) = log(rrold(1:jlast))
      do i=1,ndimx
       if(log(rr(i)).lt.-2.d0) then
        vr(i)=ylag(rr(i),rrold(0),vrold(0),0,3,jlast+1,iex)
       else if(log(rr(i)).lt.xmt) then
        vr(i)=ylag(log(rr(i)),xrold(1),vrold(1),0,3,jlast,iex)
       else
        vs = (vrold(jlast)-vrold(jlast-1))/(rrold(jlast)-rrold(jlast-1))
        vr(i) = vrold(jlast)+vs*(rr(i)-rrold(jlast))
       end if
!DEBUG       vr(i) = vr(i) + v0i*rr(i)
       vr(i) = vr(i) - v0i*rr(i)
      enddo

      ELSE
       call getThoFerPot(ztot,ndimx,rr,vr)
      END IF

!c Here vr(i) = r*Potential(r), i.e. it is an interpolated potential,
!c and not what is usually used in ASA-code;
!c in MECCA vr(i) = r*(Potential-v0i)

      return
!EOC
      end subroutine getDfltPot
!c
!c =====================================================================
!BOP
!!IROUTINE: estimatEbot
!!INTERFACE:
      subroutine estimatEbot(zcor,numc,ec,lc,kc,eBottom)
!!DESCRIPTION:
!  to estimate a position of energy contour bottom for given zcor
!
!!REVISION HISTORY:
! Initial Version - A.S. - 2013
!!REMARKS:
! core levels energies assumed to be ordered
!EOP
!
!BOC
      implicit none
      real*8  zcor
      integer numc
      real*8 ec(numc)
      integer lc(numc),kc(numc)
      real*8 eBottom
      real*8 zc,eup,dEc
      parameter (dEc = 0.3d0)
      integer i, nl, i0

      real(8) :: v0i=0.d0
      real*8 dfltBottom
      external dfltBottom

      if(numc.gt.0) then

       eBottom = v0i
       zc = 0.d0
       do i=1,numc
        nl = 2*lc(i)+1
        if(kc(i).ge.0) then
         nl = (nl-1)
        else
         nl = (nl+1)
        end if
        zc = zc + dble(nl)
        if (nint(zc).ge.nint(zcor)) then
         if (nint(zc).gt.nint(zcor)) then
          zc = zc - dble(nl)
          i0 = i-1
         else
          i0 = i
         end if
         if (i0.lt.numc) then
           eup = min(v0i,ec(i0+1))
         else
           eup = v0i
         end if

         if (i0.gt.0) then
          if(eBottom.lt.ec(i0)) then
           eBottom = min(ec(i0) + dEc,0.5d0*(ec(i0)+eup))
          else if(eBottom.gt.eup) then
           eBottom = max(  eup - dEc,0.5d0*(ec(i0)+eup))
          end if
         end if
         ec(i0+1:numc) = 0.d0
         exit
        end if
       end do
       zcor = zc
      else
       if (eBottom .eq. 0.d0) eBottom = dfltBottom(0.d0)
       zcor = 0.d0
      end if
      return
!EOC
      end subroutine estimatEbot
!c
!BOP
!!IROUTINE: getThoFerPot
!!INTERFACE:
      subroutine getThoFerPot(zed,nmx,r,rv)

!!DO_NOT_PRINT
      implicit none
!!DO_NOT_PRINT

!!ARGUMENTS:
      integer, intent(in) :: nmx
      real(8), intent(in) :: zed
      real(8), intent(in) :: r(nmx)
      real(8), intent(out) :: rv(nmx)  ! r*potential
!!REVISION HISTORY:
! Initial Version - A.S. - 2013
!EOP
!
!BOC
      real(8), parameter :: fact=1.0d+3
      real(8), parameter :: rf=four/(three*pi),oneth=one/three
      integer i
      real(8) fac,rr,root,rvozin,scl
!c =====================================================================
      if(zed.le.10.0d0)then
       fac=fact
      else
       fac=1.0d0
      endif

      scl=fac*(zed**oneth)/0.88534138d0
!c
!c =====================================================================
      do i=1,nmx
        rr = r(i)*scl
        root = sqrt(rr)
        rvozin=one            + 0.02747d0 *root                         &
     &      + 1.243d0  *rr    - 0.14860d0 *rr*root                      &
     &      + .2302d0  *rr*rr + 0.007298d0*rr*rr*root                   &
     &      + .006944d0*rr*rr*rr
        rv(i) = -two*zed/rvozin
      end do
!c =====================================================================
      return
!EOC
      end subroutine getThoFerPot
!c _____________________________________________________________________
!EOC
      end module initpot

!BOP
!!ROUTINE: getDfltPot
!!INTERFACE:
      subroutine getDfltPot(zed,r,rv,kind)
!!DESCRIPTION:
! default (either Thomas-Fermi or table) potential generator
!
!!USES:
      use initpot, only : getThoFerPot,getIniPot => getDfltPot

!!DO_NOT_PRINT
      implicit none
!!DO_NOT_PRINT

!!ARGUMENTS:
      real(8), intent(in) :: zed,r(:)
      real(8), intent(out) :: rv(:)
      integer, intent(in) :: kind
!!REVISION HISTORY:
! Initial Version - A.S. - 2013
!!REMARKS:
! this subroutine is API for module subroutine, i.e.
! for use outside "initpot" module
!EOP
!
!BOC
      integer :: numc,nr
      integer :: nc(100),lc(100),kc(100)
      real(8) :: eBottom,v0i,ec(100),zc,zv,ef

      nr = size(r)
      if ( kind == 0 ) then
       eBottom = 0
       v0i = 0
       call getIniPot(zed,nr,r,eBottom,v0i,rv,numc,nc,lc,kc,ec,zc,zv,ef)
      else
       call getThoFerPot(zed,nr,r,rv)
      end if

      return
!EOC
      end subroutine getDfltPot

