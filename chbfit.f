!BOP
!!ROUTINE: chbfit
!!INTERFACE:
      subroutine chbfit(x,y,sig,ndata,c,nmax,order,goal,error)
!!DESCRIPTION:
! produces {\tt ndata}-sized data table, maps it to the
! one-dimensional unit ball, produces Chebyshev coefficients, {\em c}, to fit
! the data and reports the error in the fitting procedures.
! {\bv
! Variables:
!
!    'x' is an ascending array of distinct domain coordinates.
!    'y' is the array of range coordinates for each value of x.
!    'sig' is the array of error-bars on the data.
!    'ndata' is the size of the coordinate-data arrays.
!    'c' is the storage array for the Chebyshev coefficients.
!    'nmax' is the maximum number of coefficients to be considered.
!    'order' is the number of coefficients needed for convergance.
!    'goal' is the requested precision of the Chebyshev fit routine.
!    'error' is the reported error of the Chebyshev fit routine.
! \ev}

!!REVISION HISTORY:
! Initial Version - A.S. - 2007
!EOP
!
!BOC
         implicit        none

         integer         i
         integer         order, ktmp
         integer         ndata, n
         integer         nmax

!c-----------------------------------------------------------------------
!c        Storage for data, coefficients and errors.
!c-----------------------------------------------------------------------
         real*8          goal
         real*8          x(ndata), y(ndata), sig(ndata)
         real*8          error, errtmp, deltmp
         real*8          v(nmax,nmax)
         real*8          c(nmax), pl(nmax,ndata)
         real*8          design(nmax,ndata)
         real*8          errcsq(nmax)

!c-----------------------------------------------------------------------
!c        List of subroutines and functions to be used.
!c-----------------------------------------------------------------------
         external        fchbev
         external        lsqfit
         external        chsqev

!c-----------------------------------------------------------------------
!c        Fit Chebyshev coefficients to data table and compute error.
!c-----------------------------------------------------------------------
         n  = min0(nmax,ndata)
         call fchbev(x(1),x(ndata),x,sig,ndata,pl,design,n,nmax)
         call lsqfit(x,y,sig,ndata,c,n,v,nmax,design)
         call chsqev(errcsq,c,n,y,sig,ndata,pl,nmax)
         deltmp = max(dble(ndata - n),goal*goal)
         error  = errcsq(n) / deltmp
         ktmp   = n
         errtmp = error

!c-----------------------------------------------------------------------
!c        Check for convergence of error.
!c-----------------------------------------------------------------------
         if (n .gt. 1) then
            do 11 i = n - 1 , 1 , -1
               deltmp = max(dble(ndata - i),goal*goal)
               error = errcsq(i)/deltmp
               if ((error .le. errtmp) .or.                             &
     &             (error .le. goal*goal)) then
                  errtmp = error
                  ktmp = i
               endif
11          continue
         endif

         if ((ndata .le. nmax) .and.                                    &
     &       (goal*goal .gt. errtmp) .and.                              &
     &       (errcsq(n) .lt. errtmp)) then
            errtmp    = errcsq(n)
            ktmp      = n
         endif

!c-----------------------------------------------------------------------
!c        Output the results.
!c-----------------------------------------------------------------------
         order   = ktmp
         if (order .lt. nmax) then
            do 12 i = order+1, nmax
               c(i) = 0.0d0
12          continue
         endif
         error = sqrt(errtmp)
         return
!EOC
      end subroutine chbfit
!
!BOP
!!ROUTINE: chsqev
!!INTERFACE:
      subroutine chsqev(errcsq,c,n,y,sig,ndata,pl,nmax)
!!DESCRIPTION:
! gives $\chi^2$ error of the fitted polynomial.
!
!!REVISION HISTORY:
! Initial Version - A.S. - 2007
!EOP
!
!BOC
      implicit        none
      integer         n
      integer         nmax
      real*8          c(n)
      integer         i, j, k
      integer         ndata
      real*8          y(ndata)
      real*8          sig(ndata)
      real*8          siginv
      real*8          pl(nmax,ndata)
      real*8          errcsq(n)
      real*8          sum(n)

      errcsq(1:n) = 0.d0
      do i = 1, ndata
         siginv = 1.0d0 / sig(i)
         sum(1) = c(1)*pl(1,i)
         do j = 2, n
            sum(j) = sum(j-1) + c(j)*pl(j,i)
         end do
         do j = 1, n
            errcsq(j) = errcsq(j) + ((y(i) - sum(j))*siginv)**2
         end do
      end do
      return
!EOC
      end subroutine chsqev
