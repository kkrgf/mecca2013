!BOP
!!ROUTINE: conbox
!!INTERFACE:
      subroutine conbox(ebot,etop,eitop,eibot,epar,egrd,nume,npar,      &
     &                  dele1,npts,iprint,istop)
!!DESCRIPTION:
! constructs rectangular energy contour
! {\bv
!                             ! Im(e)
!              ---------------!----------------  <= eitop
!              !              !               !
! linear grid=>!              !               !<=log grid
!              !              !               !
!              !              !               !  <=eibot
!      -----------------------!-------------------------
!            ebot             !    Real(e)   etop
!
!
!      ebot    : bottom of the contour on the real axis
!      etop    : top of the contour on the real axis
!      eitop   : top of contour in complex plane
!      eibot   : bottom of contour in complex plane
!      npts    : number of divisions in .1 rydberg
!      dele    : energy increment  =  0.1 / npts
!      egrd(n) : array of grid energies  (n=1,nume)
!      nume    : total number of points on energy grid
!      iprint  : print level control (>0 for print of energies)
!
!      n.b.    : if npts<99 bottom of contour gets adusted from
!                ebot such that (etop-ebot)/dele is integer

!  requires    : ebot,etop,eitop,eibot,npts,iprint
!  returns     : egrd(1:nume),nume,dele1(1:nume)
!
!  special cases:
!
!   100 < npts        :  sets up a grid  parallel to real axis,
!                        shifted into complex plane by eitop
!                        with  npts-100 points per 0.1 Ryd
!   npts == 100       :  reads in energies from file EGRD.INPUT,
!                        useful for testing
!   npts == 99        :  one point calculation, (etop,eitop)
!
!   npar > 0          : to calculate DOS near (etop,eibot)
! \ev}
!
!!USES:
      use universal_const

!!DO_NOT_PRINT
      implicit real*8(a-h,o-z)
!!DO_NOT_PRINT

!!ARGUMENTS:
      real(8), intent(inout) :: ebot,eitop
      real(8), intent(in) :: etop,eibot,epar
      integer :: nume,npar,npts
      integer, intent(in) :: iprint
      complex(8), intent(out) :: egrd(:)
      complex(8), intent(out) :: dele1(:)
      character(10), intent(in) :: istop
!!REVISION HISTORY:
! Initial Version - DDJ - 1989
! Adapted - A.S. - 2013
!EOP
!
!BOC
!c
      real(8) :: el(size(egrd)),eltmp,eltmp1
      integer :: i,ie,istt,isum,n,nbas,nlog,nlog0,nptlog,numi,numr
      real(8) :: dele,delei,delelog,dep,eibotlog,etop2
      real(8) :: eitop_,eibot_
!c
!c parameters
!c     +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
      character(*), parameter :: sname='conbox'
      character(*), parameter :: egrd_input='EGRD.INPUT'
      real(8), parameter :: tenm1=one/ten,tenm2=tenm1**2
      real(8), parameter :: tenm10=tenm1**10
      complex(8), parameter :: sqrtm1=(0.d0,1.d0)
!c     +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
!c     read in energies from data file npts>300........................
      dele1 = czero
!c
!c     ****************************************************************
!c     set up case of contour parallel to real axis (equidistant points)
      if(npts.gt.100) then
         npts=max(npts-100,1)
         dele=tenm1/npts
         numr=int((etop-ebot)/dele+tenm2*dele)
         if(numr.gt.size(egrd)) then
            write(6,'(''  conbox: too many points on contour '',        &
     &      '':: nume='',i5)') numr
            write(6,*) ' IPEPTS=',size(egrd)
            write(6,*) '   NPTS=',npts+100
            write(6,*) '   EBOT=',ebot
            write(6,*) '   ETOP=',etop
            call fstop(sname//' :: increase IPEPTS')
         endif
         do n=1,numr
            egrd(n)=dcmplx(ebot+(n-1)*dele,eitop)
         enddo
         nume=numr+1
         egrd(nume)=dcmplx(etop,eitop)
         go to 10
      endif
!c
!c     ****************************************************************
!c     set up case of contour read from the file.....................
      if(npts.eq.100) then
         open(unit=11,file=egrd_input,status='old',err=2001)
         npar = 0
!TODO
         write(6,'(''  the following energies have been input:'')')
         nume = size(egrd)
         do ie=1,size(egrd)
            read (11,*,iostat=istt) etop2,eitop
            if ( istt .ne. 0 ) then
             nume = ie-1
             exit
            end if
            egrd(ie) = dcmplx(etop2,eitop)
            write(6,'(''  energy='',2g17.8)') egrd(ie)
         enddo
         close(11)
         if ( nume>0 ) go to 10
 2001    call fstop(sname//' :: no input file '//egrd_input//           &
     &                                ' in run-directory')
      endif
      if(npts.eq.99) then
         npar = 0
         nume=1
         egrd(1)=dcmplx(etop,eitop)
         go to 10
      endif
      if(npts.eq.0) then
         npar = 0
         nume=0
         go to 10
      endif
!c
!c     ****************************************************************
!c     set up case of rectangular contour...............................
      if(npts.lt.99) then
         dele  =tenm1/npts
         numi=max(2,ceiling(eitop/dele))
         delei =min(dele,eitop/numi)
         if(delei.le.eibot) then
            write(6,'(''  conbox: delei.lt.eibot '',2f10.5)')           &
     &      delei,eibot
            eibot_ = zero
!            call fstop(sname)
         else
          eibot_ = eibot
         endif
         numr=ceiling((etop-ebot)/dele)
         ebot=etop-dele*numr
!c        =============================================================

         
!         if(numi.lt.2) then
!            write(6,'('' conbox: numi.lt.2:: numi='',i5)') numi
!            call fstop(sname)
!         endif
         eitop_ = numi*delei

!c        set up log/linear grid to be used on upper leg...............

         if(abs(eitop).le.abs(eibot_)) then
            write(6,'('' cont1: eitop set too small'')')
            eibot_ = eitop
!            call fstop(sname)
         else

          eibotlog = dele
          if ( eibot_<=zero ) then
           nptlog = 1
           delelog = zero
          else
           nptlog = eibotlog/(2*eibot_)
           nptlog = max(npts,nptlog)
           delelog = log(eibotlog/(2*eibot_))/nptlog
          end if
          nlog = nptlog - 1

          if(delelog.gt.zero) then
           do i=1,nlog
            el(i) = eibotlog*exp((i-nptlog)*delelog)
           end do
           nlog0 = nlog
          else
           el(1) = eibotlog
           nlog0 = 1
          end if

          nlog = (eitop_-el(nlog0))/dele

          if ( nlog0+nlog > size(el) ) then
            write(6,'('' cont1: array size is too small, '',            &
     &                  i4,''<'',i4)')  size(el),nlog0+nlog
            call fstop(sname)
          end if
          do i=1,nlog
           el(i+nlog0) = el(nlog0) + dele*i
          end do
          el(nlog0+nlog) = eitop_
          nlog = nlog0+nlog

         endif
!CABc        write(6,'('' i,el(i) :'',i5,f14.5)') (i,el(i),i=1,nlog)
!CABc        if log grid too coarse far of in c-plane replace by linear
!CAB         do i=2,nlog
!CAB            if(el(i)-el(i-1) .gt. delei) then
!CAB               do j=1,numi
!CAB                  el(i-1+j)=el(i-1+j-1)+delei
!CAB                  if(el(i-1+j) .ge. eitop) then
!CAB                     el(i-1+j)=eitop
!CAB                     it=(i-1+j)
!CAB                     go to 1
!CAB                  endif
!CAB               enddo
!CAB               write(6,'('' conbox: trouble setting up log grid'')')
!CAB               call fstop(sname)
!CAB            endif
!CAB         enddo
!CAB 1       continue
!CAB         nlog=it
!CAB         if(nlog.lt.2) then
!CAB            write(6,'('' conbox: nlog.lt.2:: nlog='',i5)') nlog
!CAB            write(6,*) ' eitop=',eitop,numi,dele
!CAB            write(6,*) ' eibot=',eibot
!CAB            call fstop(sname)
!CAB         endif
!c        write(6,'('' i,el(i) :'',i5,f14.5)') (i,el(i),i=1,nlog)
!c
!c        =============================================================
!c        set total number of points on grid...........................
!CAB         isum= 1 + numr + numi + nlog

         isum = numi + numr + nlog

         if(epar.gt.tenm10) then
            isum=isum+2*npts+1
         endif
         if(isum.gt.size(egrd)) then
            write(6,'(''  conbox: too many points on contour '',        &
     &      '':: nume='',i5)') isum
            call fstop(sname)
         endif
!c        =============================================================
!c        set up first point on contour................................
!CAB         egrd(1)=ebot+eibot*sqrtm1
!CAB         nbas=1

         egrd(1) = dcmplx(ebot,delei/2)
         dele1(1) = dcmplx(zero,delei)

!c        set up first leg of contour..................................
!CAB         do n=1,numi
!CAB            egrd(nbas+n)=ebot+n*delei*sqrtm1
         do n=2,numi
          dele1(n) = dcmplx(zero,delei)
          egrd(n) = egrd(n-1) + dele1(n)
         enddo
!c        =============================================================
!c        set up second leg of contour.................................
!CAB         nbas=nbas+numi
         nbas = numi
         egrd(nbas+1) = egrd(nbas)+dcmplx(dele/2,delei/2)
         dele1(nbas+1) = dcmplx(dele,zero)
         do n=2,numr
          dele1(nbas+n) = dcmplx(dele,zero)
          egrd(nbas+n)=egrd(nbas+n-1)+dele1(nbas+n)
         enddo
!c        =============================================================
!c        set up third leg of contour..................................

         nbas=nbas+numr
         etop2 = dreal(egrd(nbas)+dele1(nbas)/2)

         if(abs(etop2-etop).gt.1.d-6*dele) then
          write(6,*) ' ETOP=',etop
          write(6,*) ' NBAS=',nbas,' EGRD(NBAS)=',egrd(nbas)
          call fstop(sname//': error in contour construction?')
         end if

         eltmp = el(nlog)
         do n=1,nlog-1
          eltmp1 = el(nlog-n)
          egrd(nbas+n) = dcmplx(etop,(eltmp+eltmp1)/2)
          dele1(nbas+n) = dcmplx(zero,eltmp1-eltmp)
          eltmp = eltmp1
!CAB            egrd(nbas+n)=etop + sqrtm1*el(nlog+1-n)
         enddo
         nbas = nbas+(nlog-1)
         egrd(nbas+1) = dcmplx(etop,(eltmp+zero)/2)
         dele1(nbas+1) = dcmplx(zero,zero-eltmp)
         nbas = nbas+1
         nume=nbas

!c        =============================================================
!c        set up points parallel to real axis..........................
         if(npar.gt.0) then
            dep=epar/npts
            npar=2*npts+1
            do n=1,npar
               egrd(nume+n)=etop-epar+(n-1)*dep+sqrtm1*eibot_
            enddo
         else
            npar=0
         endif
!c        =============================================================
!c        set up de for integration over the contour to obtain charge
!c        densities .................................................

!CAB         dele1(1)=( egrd(2)+egrd(1) )*half-ebot
!CAB         dele1(nume)=zero
!CAB         do ie=2,nume-1
!CAB            dele1(ie)=( egrd(ie+1)-egrd(ie-1) )*half
!CAB         enddo
!CAB         dele1(nume)=zero
         do ie=1,npar
            dele1(nume+ie)=zero
         enddo
      endif
!c
!c     ================================================================
!c     write out grid and return.......................................
 10   continue
      if(iprint.gt.-10) then
       if ( npts<=0 ) then
        write(6,'(''  No contour generation, npts = '',i4)') npts
       else
        if ( npts<200 ) then
         write(6,'(''  Bottom of contour'',t40,''='',f10.5)') ebot
         write(6,'(''  Top    of contour'',t40,''='',f10.5)') etop
        end if
        write(6,'(''  No. of energies: main part'',t40,''='',i4)') nume
        write(6,'(''  No. of energies: parallel '',t40,''='',i4)') npar
        if(iprint.ge.0) then
         write(6,'('' n='',i5,'' e='',2e12.4,'' de='',2d12.4)')         &
     &   (n,egrd(n),dele1(n),n=1,nume+npar)
        endif
       endif
       write(6,'(/)')
      end if
!c
      if(istop.eq.sname) then
         call fstop(sname)
      else
         return
      endif
!EOC
      end subroutine conbox
