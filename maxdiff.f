!BOP
!!ROUTINE: maxdiff
!!INTERFACE:
      subroutine maxdiff(ndrpts,ndcomp,ndsublat,                        &
     &                   n1,n2,nsublat,nspin,                           &
     &                   vector1,vector2,dvmax)
!!DESCRIPTION:
! finds abs.value of maximum difference ({\t dvmax}) for each first component of 
! array difference {\tt vector1(:,:,:,:)-vector2(:,:,:,:)}

!!DO_NOT_PRINT
      implicit none
!!DO_NOT_PRINT

!!ARGUMENTS:
      integer ndrpts,ndcomp,ndsublat
      integer nsublat,n1(nsublat),n2(nsublat),nspin
      real*8 vector1(ndrpts,ndcomp,ndsublat,1:nspin)
      real*8 vector2(ndrpts,ndcomp,ndsublat,1:nspin)
      real*8 dvmax(ndcomp,ndsublat,nspin)
!!REVISION HISTORY:
! Initial version - A.S. - 2005
!!REMARKS:
!EOP
!
!BOC
      integer i1,i2,i3,i4
      real*8 dv

      dvmax = 0.d0
      do i4=1,nspin
       do i3=1,nsublat
        do i2=1,n2(i3)
       dvmax(i2,i3,i4) = maxval(abs(                                    &
     &                      vector1(1:n1(i3),i2,i3,i4)-                 &
     &                                vector2(1:n1(i3),i2,i3,i4)        &
     &                                ))
        end do
       end do
      end do

      return
!EOC
      end subroutine maxdiff
