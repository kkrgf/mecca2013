!BOP
!!ROUTINE: readIniFile
!!INTERFACE:
      subroutine readIniFile(input,ne)
!!DESCRIPTION:
! if input ne$\leq$0 then the subroutine counts number of
!   records, output {\tt ne}, in file {\tt input(1)}
! if ne$>$0 then the subroutine reads the file {\tt input(1)}      
!   in {\tt input(:)}
!       

!!DO_NOT_PRINT
      implicit none
!!DO_NOT_PRINT

!!ARGUMENTS:
      character(80), intent(inout) :: input(*)
      integer, intent(inout) :: ne
!!REVISION HISTORY:
! Initial Version - A.S. - 2013
!EOP
!
!BOC
      character(80) filename
      integer i
      logical readin
      if ( ne .le. 0) then
       readin = .FALSE.
      else
       readin = .TRUE.
      end if
      filename = trim(input(1))
      open(1,file=filename,err=100,status='OLD')
!c
!c  Internal file "input" is a copy of input file
!c
      i = 0
1     i = i + 1
      if ( i.eq.2) then
       read(1,'(a80)',err=10,end=20)
       if(readin) input(2) = filename
      else
       if(readin) then
        read(1,'(a80)',err=10,end=20) input(i)
       else
        read(1,'(a80)',err=10,end=20)
       end if
      end if
      ne = i
      go to 1
20    close(1)
      return
10    continue
      write(6,'('' The error has been found in line '',i7               &
     &,'' (input file <'',a,''>)/'')')                                  &
     &       i,trim(input(1))
      ne = i-1
      close(1)
      return
100   continue
!      write(6,'('' File <'',a,''> cannot be opened.'')')
!     . filename
      ne = -1
      return
!EOC
      end subroutine readIniFile
