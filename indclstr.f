!BOP
!!ROUTINE: indclstr
!!INTERFACE:
      function indclstr(iatom,jatom,iatom0,rslat1,rslat2,rslat3,aij,    &
     &    mapstr,ndmap,numnbi,jclust,                                   &
     &    rcut,mapsnni,ndimnn,rsij,isave)
!!DESCRIPTION:
! returns cluster index for site {\tt jatom}, 
! which is a neighbor of site {\tt iatom}
! {\bv
!  iatom,iatom0 -- equivalent sites; calculation has been done for
!                  iatom0 -->( isub )--> jclust(1..numnbi),
!  i.e. all cluster information is defined for iatom0
!
!  aij() -- R(iatom)-R(jatom)
!  rsij() -- Rnn(ij)=R(iatom0)-R(j), j -- jclust(1..numnbi)
! \ev}

!!DO_NOT_PRINT
      implicit none
      integer indclstr
!!DO_NOT_PRINT

!!ARGUMENTS:
      integer iatom,jatom,iatom0
      real*8  rslat1(3),rslat2(3),rslat3(3)
      real*8  aij(3),rcut
      integer ndmap,mapstr(ndmap,*)
      integer numnbi,jclust(numnbi),ndimnn,mapsnni(ndimnn,numnbi)
      real*8  rsij(3,*)
      integer isave
!EOP
!
!BOC
      real*8 one,epsrs
      parameter (one=1.d0,epsrs=1.d-10)

      real*8 rx,ry,rz,b
      integer m1,m2,m3
      integer ia,k,n1,n2,n3,jatom0

      real*8 rsl1,rsl2,rsl3
      save rsl1,rsl2,rsl3

      ia = mapstr(iatom,jatom)

!c   direct neighbours
!c
      do k=1,numnbi
       jatom0 = jclust(k)
       if(mapstr(iatom0,jatom0).eq.ia) then
      indclstr = jatom0
      return
       end if
      end do

!c   "boundary" neighbours
!c
      if(isave.eq.0) then
       isave=1
       rsl1 = 1.d0/sqrt(dot_product(rslat1,rslat1))
       rsl2 = 1.d0/sqrt(dot_product(rslat2,rslat2))
       rsl3 = 1.d0/sqrt(dot_product(rslat3,rslat3))
      end if

      rx = -aij(1)
      ry = -aij(2)
      rz = -aij(3)

      b = rsl1*dot_product(rslat1,aij)
      m1 = nint(sign(one,b))
      if(abs(b).gt.rcut) then           ! this is not very strict condition
       rx = rx + sign(rslat1(1),b)
       ry = ry + sign(rslat1(2),b)
       rz = rz + sign(rslat1(3),b)
      end if
      b = rsl2*dot_product(rslat2,aij)
      m2 = nint(sign(one,b))
      if(abs(b).gt.rcut) then           ! this is not very strict condition
       rx = rx + sign(rslat2(1),b)
       ry = ry + sign(rslat2(2),b)
       rz = rz + sign(rslat2(3),b)
      end if
      b = rsl3*dot_product(rslat3,aij)
      m3 = nint(sign(one,b))
      if(abs(b).gt.rcut) then           ! this is not very strict condition
       rx = rx + sign(rslat3(1),b)
       ry = ry + sign(rslat3(2),b)
       rz = rz + sign(rslat3(3),b)
      end if

      do k=2,numnbi

       ia = mapsnni(1,k)
       if(                                                              &
     & max(abs(rsij(1,ia)-rx),abs(rsij(2,ia)-ry),abs(rsij(3,ia)-rz))    &
     & .lt.epsrs) then
       indclstr = jclust(k)
       return
       end if
      end do

      do n1=0,m1,m1
      do n2=0,m2,m2
      do n3=0,m3,m3
      rx = -aij(1) + n1*rslat1(1)+n2*rslat2(1)+n3*rslat3(1)
      ry = -aij(2) + n1*rslat1(2)+n2*rslat2(2)+n3*rslat3(2)
      rz = -aij(3) + n1*rslat1(3)+n2*rslat2(3)+n3*rslat3(3)
      do k=2,numnbi
       ia = mapsnni(1,k)
       if(                                                              &
     & max(abs(rsij(1,ia)-rx),abs(rsij(2,ia)-ry),abs(rsij(3,ia)-rz))    &
     & .lt.epsrs) then
        indclstr = jclust(k)
        return
       end if
      end do
      end do
      end do
      end do

      indclstr=0

      return
!EOC
      end function indclstr
