!BOP
!!ROUTINE: ricbes
!!INTERFACE:
      subroutine ricbes(n,x,bj,bn,dj,dn)
!!DESCRIPTION:
! returns regular and irregular riccati-bessel function of complex x; \\
! this version also calculates the derivatives \\
!\begin{alignat}{4}
! j_0(x) &= sin(x)  & \quad &  n_0(x) &= -cos(x) & \nonumber \\
! j_1(x) &= -cos(x) &+ sin(x) / x \qquad &  n1(x) &= -sin(x) &- cos(x)/x \nonumber 
!\end{alignat}

!!DO_NOT_PRINT
      implicit complex*16 (a-h,o-z)
!!DO_NOT_PRINT

!!ARGUMENTS:
      integer n    !   n = lmax+1
      complex*16 x
      complex*16 bj(n),bn(n)
      complex*16 dj(n),dn(n)
!!REMARKS:
! indexing of arrays starts at 1 to hold l=0
!EOP
!
!BOC
      complex*16 xinv

      complex*16 cone
      parameter (cone=(1.d0,0.d0))
      integer l,l1,lmax
!c     ==================================================================


      if (abs(x).eq.0.d0) then
        write(6,99) x
   99   format(' trouble in ricbes: argument= ',2f10.5)
        call fstop(' trouble in RICBES')
      endif

      lmax=n-1
      call bessjy(lmax,x,bj(1:lmax+1),bn(1:lmax+1))

      do l=0,lmax
        l1=l+1
        bj(l1)=x*bj(l1)
        bn(l1)=x*bn(l1)
      enddo

      xinv = cone/x
      dj(1) = -bn(1)
      dn(1) =  bj(1)
      do l=1,lmax
        l1=l+1
        dj(l1) = bj(l) - l*bj(l1)*xinv
        dn(l1) = bn(l) - l*bn(l1)*xinv
      enddo

      return
!EOC
      end subroutine ricbes
!
!BOP
!!ROUTINE: ricbes0
!!INTERFACE:
      subroutine ricbes0(n,x,bj,bn)
!!DESCRIPTION:
! returns regular and irregular riccati-bessel function of complex x
!   (no derivatives)  \\     
!\begin{alignat}{4}
! j_0(x) &= sin(x)  & \quad &  n_0(x) &= -cos(x) & \nonumber \\
! j_1(x) &= -cos(x) &+ sin(x) / x \qquad &  n1(x) &= -sin(x) &- cos(x)/x \nonumber 
!\end{alignat}

!!DO_NOT_PRINT
      implicit none
!!DO_NOT_PRINT

!!ARGUMENTS:
      integer, intent(in) :: n    !   n = lmax+1
      complex(8), intent(in) :: x !   x .NE. 0
      complex(8), intent(out) :: bj(0:n-1),bn(0:n-1)
!!REMARKS:
! n - number of elements in arrays bj,bn; lmax=n-1      
!EOP
!
!BOC
      complex(8), parameter :: cone=(1.d0,0.d0)
      complex(8) :: bbj(0:n),bbn(0:n)
      integer lmax
!c     ==================================================================

!      if (abs(x).eq.0.d0) then
!        write(6,99) x
!   99   format(' trouble in ricbes: argument= ',2f10.5)
!        call fstop(' trouble in RICBES')
!      endif

      lmax=n-1
      call bessjy(lmax,x,bbj,bbn)
      bj(0:lmax) = x*bbj(0:lmax)
      bn(0:lmax) = x*bbn(0:lmax)
      return
!EOC
      end subroutine ricbes0
!
!BOP
!!ROUTINE: bessjy
!!INTERFACE:
      subroutine bessjy(lmax,z,bj,by)
!!DESCRIPTION:
! calculates spherical bessel functions satisfying the recursion relation \\
! $ f(m+1,z) + f(m-1,z) = (2m+1)/z f(m,z) \qquad $ for $f = j$  or $y$ \\
! $ f(0,z) = sin(z) / z $

!!DO_NOT_PRINT
      implicit complex*16 (a-h,o-z)
!!DO_NOT_PRINT

!!ARGUMENTS:
      integer lmax
      complex(8) :: z
      complex(8) :: bj(0:lmax)
      complex(8) :: by(0:lmax)
!EOP
!
!BOC
      integer l,l0,llo,lup,inc
      complex(8) :: zi,zi2,z2,prod,rat
!c     ==================================================================

      zi=(1.0d0,0.d0)/z
      zi2=zi*zi
      bj(0)=sin(z)*zi
      by(0)=-cos(z)*zi

      if ( lmax==0 ) return

!c     generate j_l(z) with forward recurrence when z > 10*lmax
!c     otherwise use backward recurrence

      if (abs(z).gt.10*lmax) then

!c       generate j_l(z) with forward recurrence relations
!        bj(0)=sin(z)*zi
        bj(1)=sin(z)*zi2 - cos(z)*zi
        do l=2,lmax
          bj(l)=dble(2*l-1)*zi*bj(l-1)-bj(l-2)
        end do

      else
!c       generate j_l(z) with backward recurrence relations
         z2=z*z
        l0=abs(z)+1
        inc=15+abs(z)/10.0d0
        llo=lmax+1
        lup=max0(llo,l0)+inc
!c       approximate rat(lup+1) by unity
        rat=(1.0d0,0.d0)
        do l=lup,llo,-1
          prod=dble( (2*l+1)*(2*l+3) )
          rat=prod/(prod-z2*rat)
        end do
!c       temporarily store rat(l)== { (2*l+1)/z }{ j(l)/j(l-1) }
!c       in the array bj(l)
        do l=lmax,1,-1
          prod=dble( (2*l+1)*(2*l+3) )
          rat=prod/(prod-z2*rat)
          bj(l)=rat
        end do
!        bj(0)=sin(z)/z
        do l=1,lmax
          bj(l)=bj(l)*bj(l-1)*z/dble(2*l+1)
        end do
      endif

!c     generate y_l(z) with forward recurrence relations
!      by(0)=-cos(z)*zi
      by(1)=-cos(z)*zi2-sin(z)*zi
      do l=2,lmax
        if ( bj(l) == (0.d0,0.d0) ) then
         by(l:lmax) = (0.d0,0.d0)   
         exit
        end if
        by(l)= dble(2*l-1)*zi*by(l-1) - by(l-2)
      end do

      return
!EOC
      end subroutine bessjy
