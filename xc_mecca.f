
!!MODULE: xc_mecca
!!INTERFACE:
      module xc_mecca
!!DESCRIPTION:
!
!    API for {\em libxc} (exchange-correlation functional library) \\
!    http://www.tddft.org/programs/octopus/wiki/index.php/Libxc
!
!    Units used in {\em libxc} are Hartree and Bohr
!
!!USES:
       use universal_const
       use xc_f90_lib_m
       use mecca_constants, only : XC_LDA_HEDIN,XC_LDA_VOSKO,XC_GGA_AM05
!
       implicit none

!!PUBLIC TYPES:
       type, public :: xc_mecca_pointer
        type(xc_f90_pointer_t) :: px
        type(xc_f90_pointer_t) :: info_px
        type(xc_f90_pointer_t) :: pc
        type(xc_f90_pointer_t) :: info_pc
        integer :: idc=0
        integer :: idx=0
        integer :: iexch=0
        integer :: lda_only=0
       end type xc_mecca_pointer


!!PUBLIC DATA MEMBERS:
      real(8), parameter :: xc_minrho=one/(ten**18)
      logical :: xc_rel=.false.                      ! non-relativistic exchange
!      logical, parameter :: xc_rel=.true.
!
      integer, parameter :: max_nonlibxc_id = 160    ! IDs > max_nonlibxc_id are reserved for LibXC
      integer, parameter :: max_nonlibxc_lda = 99    ! nonlibxc lda IDs <=  max_nonlibxc_lda
      integer, parameter :: max_XC_id = 1000000      ! any libxc ID < max_XC_id
      integer, parameter :: d_xc_id=1000             ! XC_id = iexch * d_xc_id + icorr
!
      integer, parameter, public :: GGA_X_CHACHIYO=298
      integer, parameter, public :: LDA_XC_KSDT=259
      integer, parameter, public :: LDA_X_REL=532
      integer, parameter, public :: LDA_XC_GDSMFB=577
!
      integer, parameter, public :: ID_VOSKO = XC_LDA_X * d_xc_id       &
     &                                       + XC_LDA_C_VWN
      integer, parameter, public :: ID_relVOSKO = LDA_X_REL * d_xc_id    &
     &                                       + XC_LDA_C_VWN
!
!parameters for X_GGA_vLB
      integer, parameter, public :: X_GGA_vLB=100    ! spin-dependent vLB (original)
      integer, parameter, public :: X_GGA_vLBi=101    ! spin-independent vLB (based on total density)
      integer, parameter, public :: X_GGA_vLBs=102    ! original vLB with a self-consistent shift
      real(DBL_R), protected :: beta  = 0.05d0
      real(DBL_R), protected  :: ionizpot = 0
      real(DBL_R), protected  :: gam   = 1
!      real(DBL_R), save:: alpha = 0.d0
!      real(DBL_R), save :: aa
!
      real(DBL_R), protected :: w_x(2) = [ 0.15d0, 1.d0 ]           ! weights
!
!
      type(xc_mecca_pointer), target, protected :: es_libxc_p            ! to be used with empty spheres only
      integer, parameter :: es_xc_id = ID_VOSKO  ! type of XC function for empty spheres
      integer :: es_xc_type = 1                      ! to prevent instability (due to GGA) in empty spheres: 1 - lda_only (no rho gradients)
!
!!PUBLIC MEMBER FUNCTIONS:
! subroutine xc_alpha
! real(DBL_R) function xc_alpha2
!
!!REVISION HISTORY:
! Initial Version - A.S. - 2013
!!REMARKS:
!EOP
      private :: avfltr,avintrfltr,damp_grad,damping_fct
      private :: dervfit,derv5fltr
!
      contains

      logical function xc_is_gga(p_libxc)
      implicit none
      type(xc_mecca_pointer), intent(in) :: p_libxc
      xc_is_gga = .false.
      if ( xc_f90_family_from_id(p_libxc%idx) == XC_FAMILY_GGA .or.     &
     &       xc_f90_family_from_id(p_libxc%idc) == XC_FAMILY_GGA .or.   &
     &      xc_f90_family_from_id(p_libxc%idx) == XC_FAMILY_HYB_GGA .or.&
     &      xc_f90_family_from_id(p_libxc%idc) == XC_FAMILY_HYB_GGA     &
     &     ) then
        xc_is_gga = .true.
      else if ( p_libxc%iexch > max_nonlibxc_lda .and.                  &
     &               p_libxc%iexch <= max_nonlibxc_id ) then
        xc_is_gga = .true.
      end if
      return
      end function xc_is_gga

!BOP
!!IROUTINE: xc_alpha
!!DESCRIPTION:
! to calculate exchange-correlation energy and potential (array version)
!
!!INTERFACE:
      subroutine xc_alpha(nspin,n,rr,rhorr_up,rhorr_dn,                 &
     &                                enxc,vxc_up,vxc_dn,libxc_p)
      implicit none
!
!!INPUT ARGUMENTS:
      integer, intent(in) :: nspin
      integer, intent(in) :: n
      real(DBL_R), intent(in) :: rr(:)
      real(DBL_R), intent(in) :: rhorr_up(:)   !  4*pi*r^2*rho_up
!  if nspin==1 then rhorr_dn and vxc_dn are ignored, rhorr_up is considered as rhorr_total
      real(DBL_R), intent(in) :: rhorr_dn(:)   !  4*pi*r^2*rho_dn
      type(xc_mecca_pointer), target  :: libxc_p
!!OUTPUT ARGUMENTS:
      real(DBL_R), intent(out) :: enxc(:)
      real(DBL_R), intent(out) :: vxc_up(:)
      real(DBL_R), intent(out) :: vxc_dn(:)
!EOP
!
!BOC
      type(xc_mecca_pointer) :: p_libxc,p_tmp
      integer :: iexch

      integer :: i,ii,ni
      real(DBL_R), parameter :: up=1, dn=-1
      real(DBL_R) :: rs,dz
      real(DBL_R) :: exc(n),rhoup(n),rhodn(n),vup(n),vdn(n)
      real(DBL_R) :: gup(n),gdn(n)
      real(DBL_R) :: gup2(n),gupdn(n),gdn2(n),lup(n),ldn(n)
      real(DBL_R) :: v2rho2_uu(n),v2rho2_ud(n),v2rho2_dd(n)
      real(DBL_R) :: exc_h
!        real(DBL_R) :: hmesh,Fl,F0,Fr,dzl,dz0,dzr,df
!        real(DBL_R), external :: am05
      real(DBL_R), parameter :: pi4=four*pi, pi4d3 = pi4/three
      real(DBL_R), parameter :: half=one/two,third=one/three
      integer, parameter :: nmax=3
      real(DBL_R), parameter :: fsigma=5.d-4    ! regularisation parameter
      real(DBL_R) :: eps_rho,step,dstep
!      real(DBL_R) :: tmp(n)
      integer :: izup,izdn,iz
      logical :: do_libxc
      integer :: st_env

      if ( nspin<1 .or. nspin>2 ) then
          write(6,*) ' nspin = ',nspin
          stop ' incorrect XC input parameter nspin'
      end if
      if ( n<=0 ) return

      if ( es_libxc_p%iexch>0 ) then
        p_libxc = es_libxc_p
      else
        p_libxc = libxc_p
      end if
!!      step = 2*min(fsigma,(rr(n)-rr(1))/n)
      step = zero ! min(fsigma,(rr(n)-rr(1))/n)
      dstep = min(fsigma,(rr(n)-rr(1))/n)
      iz = 1
      izup=0
      izdn=0

      iexch = p_libxc%iexch
      do_libxc = .false.
      if (p_libxc%idx==0.and.p_libxc%idc==0.and.p_libxc%iexch==0) then
          call gXCmecca(iexch,p_libxc)
      end if
      if ( p_libxc%idx>0 .or. p_libxc%idc>0 ) do_libxc = .true.

      if ( iexch == 0 ) then
       enxc = zero
       vxc_up = zero
       vxc_dn = zero
       return
      end if

      vdn = one/(pi4*rr(1:n)**2)
      if ( nspin==1 ) then
       rhoup(1:n) = rhorr_up(1:n)*vdn(1:n)
       rhoup = half*rhoup
       rhodn = rhoup
      else
       rhoup = rhorr_up(1:n)*vdn
       rhodn = rhorr_dn(1:n)*vdn
      end if
! to eliminate very small and negative values

      eps_rho = xc_minrho*1.01d0
      ni = n
      do i=n,1,-1
       if ( rhoup(i)<eps_rho ) then
        cycle
       else
        ni = i
        rhoup(i+1:n) = eps_rho
        gup(i+1:n)   = zero
        lup(i+1:n)   = zero
        gup2(i+1:n)  = zero
        gupdn(i+1:n) = zero
        exit
       end if
      end do
      eps_rho = xc_minrho
      izup = 0
      do i=ni,1,-1
       if ( rhoup(i)<eps_rho ) then
        izup = i
        rhoup(1:i) = eps_rho
        gup(1:i)   = zero
        lup(1:i)   = zero
        gup2(1:i)  = zero
        gupdn(1:i) = zero
        exit
       end if
      end do

      eps_rho = xc_minrho*1.01d0
      ni = n
      do i=n,1,-1
       if ( rhodn(i)<eps_rho ) then
        cycle
       else
        ni = i
        rhodn(i+1:n) = eps_rho
        gdn(i+1:n)   = zero
        ldn(i+1:n)   = zero
        gdn2(i+1:n)  = zero
        gupdn(i+1:n) = zero
        exit
       end if
      end do
      eps_rho = xc_minrho
      izdn = 0
      do i=ni,1,-1
       if ( rhodn(i)<eps_rho ) then
        izdn = i
        rhodn(1:i) = eps_rho
        gdn(1:i)   = zero
        ldn(1:i)   = zero
        gdn2(1:i)  = zero
        gupdn(1:i) = zero
        exit
       end if
      end do

      if ( do_libxc ) then
        if ( xc_f90_family_from_id(p_libxc%idx) == XC_FAMILY_GGA .or.   &
     &       xc_f90_family_from_id(p_libxc%idc) == XC_FAMILY_GGA .or.   &
     &       xc_f90_family_from_id(p_libxc%idx) == XC_FAMILY_HYB_GGA.or.&
     &       xc_f90_family_from_id(p_libxc%idc) == XC_FAMILY_HYB_GGA    &
     &     ) then
!DEBUG
          call gEnvVar('ENV_XC_WX',.false.,st_env)
          if ( st_env>=0 .and. st_env<=100 ) then
           dz = dble(st_env)/dble(100)
           if ( dz .ne. w_x(1) ) then
            w_x(1) = dz
            write(6,'(/a,2f6.3)')' ENV_XC_WX variable changes parameter'&
     &             //' W_X(1), new value is ',w_x(1)
           end if
          end if
!DEBUG
          call gEnvVar('ENV_DISABLE_XC_GRADCORR',.false.,st_env)
          if ( max(izup,izdn)>0 ) then
           if ( n-max(izup,izdn) < 10 ) st_env=1
          end if
!
          if ( p_libxc%lda_only==1 .or. st_env==1 ) then
!! there is a Nan bug in libxc 4.2.1 version if grad(rho)/rho is very small
           if ( p_libxc%idx==GGA_X_CHACHIYO                              &
     &          .or. p_libxc%idc==GGA_X_CHACHIYO                         &
     &        ) then
            p_libxc%lda_only =  GGA_X_CHACHIYO
!!  it is expected to have p_libxc%lda_only=0 after call gXC_libxc
           end if
          end if
!
          if ( p_libxc%lda_only==1 .or. st_env==1 ) then
           gup  = zero
           gdn  = zero
           lup  = zero
           ldn  = zero
          else
           iz = max(1,izup)
           call dervfit(n-iz+1,rr(iz:n),rhoup(iz:n),eps_rho,            &
     &                            dstep,nmax,gup(iz:n),lup(iz:n))
           lup(iz:n) = lup(iz:n)+two*gup(iz:n)/rr(iz:n)
           if ( nspin == 1 ) then
            rhodn(1:n) = rhoup(1:n)
            gdn(1:n) = gup(1:n)
            ldn(1:n) = lup(1:n)
           else
            iz = max(1,izdn)
            call dervfit(n-iz+1,rr(iz:n),rhodn(iz:n),eps_rho,           &
     &                            dstep,nmax,gdn(iz:n),ldn(iz:n))
            ldn(iz:n) = ldn(iz:n)+two*gdn(iz:n)/rr(iz:n)
           end if
          end if
          gupdn(1:n) = gup(1:n)*gdn(1:n)
          gup2(1:n) = gup(1:n)*gup(1:n)
          gdn2(1:n) = gdn(1:n)*gdn(1:n)
!DEBUGPRINT
!             call dump_print(rhoup(1:n),gup (1:n),rr(1:n),n,            &
!     &                                 p_libxc%idx,p_libxc%idc)
!            call derv5fltr(zero,rhoup(izup:n),tmp(izup:n),rr(izup:n),   &
!     &                                                         n-izup+1)
!            tmp(1:izup) = zero
!             call dump_print(rhoup(1:n),tmp (1:n),rr(1:n),n,            &
!     &                                 p_libxc%idx,p_libxc%idc)
!             call dump_print(rhoup(1:n),lup (1:n),rr(1:n),n,            &
!     &                                 p_libxc%idx,p_libxc%idc)
!....................
!          do i=1,n
!            write(4,'(1x,i5,1x,f8.4,1x,7(1x,g18.9))') i,rr(i),          &
!     &                  rhoup(i)+rhodn(i),abs(gup(i)),abs(gdn(i)),      &
!     &                  lup(i),ldn(i),                                  &
!     &                  gup2(i),gdn2(i)
!          end do
!DEBUGPRINT
          enxc = zero
          vxc_up = zero
          vxc_dn = zero
          if ( xc_f90_family_from_id(p_libxc%idx)==XC_FAMILY_LDA        &
     &         .or. xc_f90_family_from_id(p_libxc%idc)==XC_FAMILY_LDA   &
     &       ) then
           if (xc_f90_family_from_id(p_libxc%idx)==XC_FAMILY_LDA) then
            call gXCmecca(p_libxc%idx*d_xc_id,p_tmp)
            p_libxc%idx = 0
            p_libxc%iexch = p_libxc%idx*d_xc_id  + p_libxc%idc
           else if (xc_f90_family_from_id(p_libxc%idc)==XC_FAMILY_LDA)  &
     &     then
            call gXCmecca(p_libxc%idc*d_xc_id,p_tmp)
            p_libxc%idc = 0
            p_libxc%iexch = p_libxc%idx*d_xc_id  + p_libxc%idc
           end if
           call gXC_libxc(n=n,libxc_p=p_tmp,                            &
     &                rhoup=rhoup,rhodn=rhodn,                          &
     &                exc=enxc,vxc_up=vxc_up,vxc_dn=vdn)
           if ( nspin>1 ) then
            vxc_dn(1:n) = vdn(1:n)
           end if
           call gXCmecca(-1,p_tmp)
          end if
          call gXC_libxc(n=n,libxc_p=p_libxc,                           &
     &                rhoup=rhoup,rhodn=rhodn,                          &
     &                gup2=gup2,gupdn=gupdn,gdn2=gdn2,                  &
     &                exc=exc,vxc_up=vup,vxc_dn=vdn,                    &
     &                vsigma_uu=v2rho2_uu,                              &
     &                vsigma_ud=v2rho2_ud,vsigma_dd=v2rho2_dd)
!DEBUGPRINT
!             call dump_print(vxc_up(1:5),vdn(1:5),rr(1:5),5,            &
!     &                                 p_libxc%idx,p_libxc%idc)
!             call dump_print(v2rho2_uu(1:5),v2rho2_ud(1:5),rr(1:5),5,   &
!     &                                 p_libxc%idx,p_libxc%idc)
!DEBUGPRINT

          v2rho2_uu(1:izup) = zero
          iz = max(1,izup)
          call derv5fltr(step,v2rho2_uu(iz:),gup2(iz:),rr(iz:n),n-iz+1)
          gup2(1:izup) = zero
          v2rho2_ud(1:max(izup,izdn)) = zero
          iz = max(1,max(izup,izdn))
          call derv5fltr(step,v2rho2_ud(iz:),gupdn(iz:),rr(iz:n),n-iz+1)
          gupdn(1:iz) = zero
!DEBUGPRINT
!             call dump_print(gup2(1:5),gupdn(1:5),rr(1:5),5,            &
!     &                                 p_libxc%idx,p_libxc%idc)
!             call dump_print(lup(1:5),ldn(1:5),rr(1:5),5,               &
!     &                                 p_libxc%idx,p_libxc%idc)
!DEBUGPRINT
          vup(1:n) = vup(1:n) -                                         &
     &         ( two*gup(1:n)*gup2(1:n)+gdn(1:n)*gupdn(1:n) )           &
     &       - ( two*lup(1:n)*v2rho2_uu(1:n) + ldn(1:n)*v2rho2_ud(1:n) )
          enxc = enxc + exc
          vxc_up(1:n) = vxc_up(1:n) + vup(1:n)
          if ( izup>1 ) then
           vxc_up(izup)=0.5d0*(vxc_up(izup-1)+vxc_up(izup+1))
           exc_h = 0.5d0*(enxc(izup-1)+enxc(izup+1))
          end if
!DEBUGPRINT
!             call dump_print(rhoup(1:5),vxc_up(1:5),rr(1:5),5,          &
!     &                                 p_libxc%idx,p_libxc%idc)
!DEBUGPRINT
          if ( nspin>1 ) then
           v2rho2_dd(1:izdn) = zero
           iz = max(1,izdn)
           call derv5fltr(step,v2rho2_dd(iz:),gdn2(iz:),rr(iz:n),n-iz+1)
           gdn2(1:izdn) = zero
           vdn(1:n) = vdn(1:n) -                                        &
     &          ( two*gdn(1:n)*gdn2(1:n)+gup(1:n)*gupdn(1:n) )          &
     &       - ( two*ldn(1:n)*v2rho2_dd(1:n) + lup(1:n)*v2rho2_ud(1:n) )
           vxc_dn(1:n) = vxc_dn(1:n) + vdn(1:n)
           if ( izdn>1 ) then
            vxc_dn(izdn)=0.5d0*(vxc_dn(izdn-1)+vxc_dn(izdn+1))
            enxc(izdn) = 0.5d0*(enxc(izdn-1)+enxc(izdn+1))
           end if
           if ( izup>1 ) enxc(izup) = exc_h
          end if
!
          if ( p_libxc%lda_only==GGA_X_CHACHIYO) libxc_p%lda_only=1
!
        else                      ! XC_FAMILY_LDA
!          gup2=0; gupdn=0; gdn2=0;
!          lup = 0; ldn = 0
!          v2rho2_uu = 0; v2rho2_dd = 0; v2rho2_ud = 0
          call gXC_libxc(n=n,libxc_p=p_libxc,                           &
     &                rhoup=rhoup,rhodn=rhodn,                          &
     &                exc=enxc,vxc_up=vxc_up,vxc_dn=vdn)
          if ( nspin>1 ) then
           vxc_dn(1:n) = vdn(1:n)
          end if
        end if
      else
       if ( iexch <= max_nonlibxc_lda ) then
         gup = rhoup(1:n)+rhodn(1:n) ! rho+ ->gup
         gdn = rhoup(1:n)-rhodn(1:n) ! rho- ->gdn
         do i=1,n
          if ( gup(i)<=(eps_rho+eps_rho) ) then
           rs = (three/(pi4*(eps_rho+eps_rho)))**third
           dz = zero
          else
           rs=(three/(pi4*gup(i)))**third
           dz = gdn(i)/gup(i)
          end if
          vxc_up(i) = xc_alpha2(rs,dz,up,iexch,exc_h)
          enxc(i) = exc_h
          if ( nspin>1 ) then
           vxc_dn(i) = xc_alpha2(rs,dz,dn,iexch,exc_h)  ! enxc is spin-independent
           if ( abs(enxc(i)-exc_h)>1.d-8 ) then
              write(6,*) ' enxc: ',enxc(i),exc_h
              call fstop                                                &
     &            ('xc_alpha: unexpected error, enxc is spin-dependent')
           end if
          end if
         end do
       else
        if ( p_libxc%lda_only == 0 .and.                                &
     &     (iexch == X_GGA_vLB                                          &
     &     .or. iexch == X_GGA_vLBi                                     &
     &     .or. iexch == X_GGA_vLBs)                                    &
     &     ) then
         if ( iexch == X_GGA_vLBi ) then
          rhoup(1:n) = rhoup(1:n)+rhodn(1:n)
          rhodn(1:n) = rhoup(1:n)
         end if
         gup(1:izup) = zero
         gdn(1:izdn) = zero
         iz = max(1,izup)
!         dstep = fsigma
         call dervfit(n-iz+1,rr(iz:n),rhoup(iz:n),eps_rho,              &
     &                            dstep,nmax,gup(iz:n))
         if ( nspin == 1 ) then
           gdn(1:n) = gup(1:n)
         else
          iz = max(1,izdn)
          call dervfit(n-iz+1,rr(iz:n),rhodn(iz:n),eps_rho,             &
     &                            dstep,nmax,gdn(iz:n))
         end if
         do i=1,n
          call x_gga_lbmod(rhoup(i),rhodn(i),gup(i),gdn(i),             &
     &                                          vxc_up(i),vxc_dn(i))
         end do
         vxc_up(1:n) = vxc_up(1:n)/ry2H
         if ( nspin>1 ) vxc_dn(1:n) = vxc_dn(1:n)/ry2H
!
        else if ( iexch == XC_GGA_AM05 ) then
!
!         gup2=0; gupdn=0; gdn2=0
         gup(1:izup) = zero
         gdn(1:izdn) = zero
         lup(1:izup) = zero
         ldn(1:izdn) = zero
         iz = max(1,izup)
         call dervfit(n-iz+1,rr(iz:n),rhoup(iz:n),eps_rho,              &
     &                            dstep,nmax,gup(iz:n),lup(iz:n))
         lup(iz:n) = lup(iz:n) + two*gup(iz:n)/rr(iz:n)
!DEBUGPRINT
!         if ( n>1 ) then
!             call dump_print(rhoup(1:n),gup2(1:n),rr(1:n),n,            &
!     &                                 0,iexch)
!         end if
!DEBUGPRINT
         if ( nspin == 1 ) then
           gdn(1:n) = gup(1:n)
           ldn(1:n) = lup(1:n)
         else
          iz = max(1,izdn)
          call dervfit(n-iz+1,rr(iz:n),rhodn(iz:n),eps_rho,             &
     &                            dstep,nmax,gdn(iz:n),ldn(iz:n))
          ldn(iz:n) = ldn(iz:n) + two*gdn(iz:n)/rr(iz:n)
         end if
!         gup2 = abs(gup2) ; gdn2 = abs(gdn2)
         gupdn(1:n) = gup(1:n)*gdn(1:n)
         gup2(1:n)  = gup(1:n)*gup(1:n)
         gdn2(1:n)  = gdn(1:n)*gdn(1:n)
         do i=1,n
           call am05pgjs(rhoup(i),rhodn(i),gup2(i),gdn2(i),gupdn(i),    &
     &                enxc(i),vxc_up(i),vdn(i),                         &
     &                v2rho2_uu(i),v2rho2_dd(i),v2rho2_ud(i))
         end do
         call derv5fltr(step,v2rho2_uu,gup2,rr,n)
         gup2(1:izup) = zero
         call derv5fltr(step,v2rho2_ud,gupdn,rr,n)
         gupdn(1:izup) = zero
         vxc_up(1:n) = vxc_up(1:n) -                                    &
     &         ( two*gup(1:n)*gup2(1:n)+gdn(1:n)*gupdn(1:n) )           &
     &       - ( two*lup(1:n)*v2rho2_uu(1:n) + ldn(1:n)*v2rho2_ud(1:n) )
         if ( nspin>1 ) then
          call derv5fltr(step,v2rho2_dd,gdn2,rr,n)
          gdn2(1:izdn) = zero
          gupdn(1:izdn) = zero
          vxc_dn(1:n) = vdn(1:n) -                                      &
     &          ( two*gdn(1:n)*gdn2(1:n)+gup(1:n)*gupdn(1:n) )          &
     &       - ( two*ldn(1:n)*v2rho2_dd(1:n) + lup(1:n)*v2rho2_ud(1:n) )
         end if
         enxc(1:n) = enxc(1:n)/(ry2H*(rhoup(1:n)+rhodn(1:n)))    ! now in Ry
         vxc_up(1:n) = vxc_up(1:n)/ry2H
         if ( nspin>1 ) vxc_dn(1:n) = vxc_dn(1:n)/ry2H
        else
         enxc = 0
         vxc_up(1:) = 0
         if ( nspin>1 ) vxc_dn(1:) = 0
        end if
       end if
      end if
!
!      vxc_up(1:izup) = zero
!      vxc_dn(1:izdn) = zero
!      enxc(1:min(izup,izdn))   = zero
!      enxc(min(izup,izdn)+1:max(izup,izdn)) =                           &
!     &     enxc(min(izup,izdn)+1:max(izup,izdn))/two
!
      return
!EOC
      end subroutine xc_alpha

!BOP
!!IROUTINE: xc_alpha2
!!INTERFACE:
      function xc_alpha2(rs,dz,sp,iexch,exchg,libxc_p)
      implicit none
!!DESCRIPTION:
!  xc\_alpha2 is to calculate exchange-correlation potential (vxc) and energy (exchg)
!
!
!!INPUT ARGUMENTS:
      real (DBL_R), intent(in) :: rs  ! 4*pi/3*rs**3 = 1/rho
      real (DBL_R), intent(in) :: dz  ! dz = (rho_up-rho_dn)/(rho_up+rho_dn), if non-polarized, then dz=0
      real (DBL_R), intent(in) :: sp  ! spin_up: sp>0, spin_dn: sp<0
      integer, intent(in) :: iexch    ! XC kind
      type(xc_mecca_pointer), optional :: libxc_p
!!OUTPUT ARGUMENTS:
      real (DBL_R), intent(out) :: exchg ! exchg - energy per 1 site
!
      real (DBL_R) :: xc_alpha2  ! xc_alpha2 is vxc
!!REMARKS:
!      LSDA function (even for GGA functionals)
!EOP
!
!BOC

      real (DBL_R) :: rho(2), rho_up(1), rho_dn(1)
      real (DBL_R) :: grho2(1),gup2(1)
      real (DBL_R) :: gupdn(1),gdn2(1)
      real (DBL_R)        :: exc(1), vxc_up(1), vxc_dn(1)
!      real (DBL_R)        :: vxc(1)
      integer             :: idx, idc, nsp
      real (DBL_R), parameter :: pi4d3 = four*pi/three
      real (DBL_R), external  :: alpha2   !  MECCA exchange-correlation function
      integer :: keep_tmp

      xc_alpha2 = zero
      exchg    = zero

      if ( iexch == 0 ) return

      rho(1) = one/(pi4d3*rs**3)
      grho2(1) = zero ; gup2(1) = zero
      gupdn(1) = zero ; gdn2(1) = zero
      if ( present(libxc_p) ) then
       if ( libxc_p%idx==GGA_X_CHACHIYO                                 &
     &         .or. libxc_p%idc==GGA_X_CHACHIYO ) then
!! there is a Nan bug in libxc 4.2.1 version if grad(rho)/rho is very small
        grho2(1) = 2.d-24*rho(1)**2 ; gup2(1) = 1.d-24*rho(1)**2
        gupdn(1) = 1.d-24*rho(1)**2 ; gdn2(1) = 1.d-24*rho(1)**2
       end if
      end if
!      grho2(1) = ( rho(1)/0.5)**2
!      if ( abs(dz)<1.e-13 ) then
!        nsp = 1
!      else
        nsp = 2
        rho_up(1) = rho(1)*(one+dz)/two
        rho_dn(1) = rho(1)*(one-dz)/two
!        gup2(1) = ( rho_up(1)/0.5)**2
!        gdn2(1) = ( rho_dn(1)/0.5)**2
!        gupdn(1) = dz*sqrt(gup2(1)*gdn2(1))
!      end if
!        rhopls = rho1+rho2
!        rhomns = rho1-rho2
!         ro3=sign((one/max(1.d-24,(pi4d3*rhopls)))**third,rhopls)
!         dz = rhomns/rhopls
!        vxc1 = alpha2(ro3,dz,one,iexch,exc1)
!        vxc(ir) = vxc1*rho1
!        exc(ir) = exc1*rho1
!        if(nspin.eq.1) then
!         vxcdn(ir) = vxc(ir)
!         excdn(ir) = exc(ir)
!        else
!         vxc2 = alpha2(ro3,dz,-1.d0,iexch,exc2)
!         vxcdn(ir) = vxc2*rho2
!      excdn(ir) = exc2*rho2

      if ( iexch<=max_nonlibxc_id .and. iexch>0 ) then
       xc_alpha2 = alpha2(rs,dz,sp,iexch,exchg)
      else
       if ( present(libxc_p) ) then
        if (libxc_p%idx==0.and.libxc_p%idc==0) then
         call gXCmecca(iexch,libxc_p)
        end if
        idx = libxc_p%idx
        idc = libxc_p%idc
        keep_tmp = libxc_p%lda_only
        libxc_p%lda_only = 1
        if ( idx>0 .or. idc>0 ) then
         if ( initXC_libxc(idx,idc,xc_rel,libxc_p) == 0 ) then
            call gXC1_libxc(libxc_p,                                    &
     &                grho2=grho2(1),gup2=gup2(1),                      &
     &                gupdn=gupdn(1),gdn2=gdn2(1),                      &
     &                rhoup=rho_up(1),rhodn=rho_dn(1),exc=exc(1),       &
     &                vxc_up=vxc_up(1),vxc_dn=vxc_dn(1))
            if ( sp > 0 ) then
              exchg = exc(1)
              xc_alpha2 = vxc_up(1)
            else if ( sp < 0 ) then
              exchg = exc(1)
              xc_alpha2 = vxc_dn(1)
            end if
            return    ! ######
         end if
        end if
        libxc_p%lda_only = keep_tmp
       end if
      end if
      return
!EOC
      end function xc_alpha2

!BOP
!!IROUTINE: gLibXCid
!!INTERFACE:
      subroutine gLibXCid(iexch,idx,idc)
!
!EOP
!
!BOC
      implicit none
      integer, intent(in) :: iexch
      integer, intent(out) :: idx,idc
      integer, parameter :: d=d_xc_id
      idx = 0 ; idc = 0
      if ( iexch == 0 ) then   ! default
        idx = XC_LDA_X ; idc = XC_LDA_C_VWN    ! vosko-wilk-nusair exch-corr potential
!        idx = XC_LDA_X ; idc = XC_LDA_C_vBH    ! von barth-hedin  exch-corr potential
!        idx = XC_GGA_X_AM05 ; idc = XC_GGA_C_AM05  ! AM05: Armiento & Mattsson 05
      else
        idc = mod(iexch,d)
        idx = iexch/d
      end if
      return
!EOC
      end subroutine gLibXCid
!
!BOP
!!IROUTINE: initXC_libxc
!!INTERFACE:
      integer function initXC_libxc(idx,idc,rel,libxc_p)
!
!EOP
!
!BOC
        implicit none
        integer, intent(in) :: idx
        integer, intent(in) :: idc
        logical, intent(in) :: rel
        type(xc_mecca_pointer) :: libxc_p
        integer, parameter :: nspin=XC_POLARIZED
        initXC_libxc = 10
        if ( idx<=0 .and. idc<=0 ) then
          call endXC_libxc(libxc_p)
          initXC_libxc=-1
          return
        else
          if ( idx .ne. libxc_p%idx ) then
            if ( libxc_p%idx>0 ) call xc_f90_func_end(libxc_p%px)
            libxc_p%idx = 0
          end if
          if ( idx > 0 ) then
           libxc_p%idx = idx
           if ( rel .and. idx==XC_LDA_X ) libxc_p%idx = LDA_X_REL
           call xc_f90_func_init(libxc_p%px,libxc_p%info_px,libxc_p%idx,&
     &                                                            nspin)
!           if ( idx==XC_LDA_XC_KSDT .or. idx==XC_LDA_XC_GDSMFB ) then
           if ( idx==LDA_XC_KSDT .or. idx==LDA_XC_GDSMFB ) then
             call xc_tempr_setpar(libxc_p%px)
           end if
          end if
          if ( idc .ne. libxc_p%idc ) then
           if ( libxc_p%idc>0 ) call xc_f90_func_end(libxc_p%pc)
           libxc_p%idc = 0
          end if
          if ( idc>0 ) then
           libxc_p%idc = idc
           if ( rel .and. idc==XC_LDA_X ) libxc_p%idc = XC_LDA_X_REL
           call xc_f90_func_init(libxc_p%pc,libxc_p%info_pc,libxc_p%idc,&
     &                                                            nspin)
          else
           if ( libxc_p%idc>0 ) call xc_f90_func_end(libxc_p%pc)
           libxc_p%idc = 0
          end if
          initXC_libxc=0
        end if
        return
!EOC
      end function initXC_libxc
!
!BOP
!!IROUTINE: defXCforES
!!INTERFACE:
      subroutine defXCforES(lda_only,id)
!!DESCRIPTION:
! to set-up empty spheres exchange-correlation type
!
!!REMARKS:
!   arguments are optional
!EOP
!
!BOC
      implicit none
      logical, optional :: lda_only
      integer, optional :: id
      if ( present(id) ) then
       es_libxc_p%iexch = id
      else
       es_libxc_p%iexch = es_xc_id
      end if
      if ( present(lda_only) ) then
       if ( lda_only ) then
        es_xc_type = 1
       else
        es_xc_type = 0
       end if
      end if
      es_libxc_p%lda_only = es_xc_type
      return
!EOC
      end subroutine defXCforES
!
!BOP
!!IROUTINE: undefXCforES
!!INTERFACE:
      subroutine undefXCforES()
!EOP
!
!BOC
      implicit none
      call endXC_libxc(es_libxc_p)
      es_libxc_p%lda_only = es_xc_type
      return
!EOC
      end subroutine undefXCforES
!
!BOP
!!IROUTINE: gXCmecca
!!INTERFACE:
      subroutine gXCmecca(iexch,libxc_p)
!!DESCRIPTION:
! to set-up kind of exchange-correlation functional {\tt libxc\_p}
!
!EOP
!
!BOC
        implicit none
        integer, intent(in) :: iexch
        type(xc_mecca_pointer) :: libxc_p
        integer :: idx,idc,i,iXC
        if ( iexch==-1 ) then
         call endXC_libxc(libxc_p)
        else
         iXC = mod(iexch,max_XC_id)
         libxc_p%iexch = iXC
         if ( iexch > max_nonlibxc_id ) then
          call gLibXCid(iXC,idx,idc)
          i = initXC_libxc(idx,idc,xc_rel,libxc_p)
          if ( iXC<iexch ) libxc_p%lda_only = 1
         else
          libxc_p%idx = 0
          libxc_p%idc = 0
         end if
        end if
      return
!EOC
      end subroutine gXCmecca
!
!BOP
!!IROUTINE: endXC_libxc
!!INTERFACE:
      subroutine endXC_libxc(libxc_p)
!!DESCRIPTION:
! to nullify XC-related pointers
!
!EOP
!
!BOC
        implicit none
        type(xc_mecca_pointer) :: libxc_p
        if ( libxc_p%iexch > max_nonlibxc_id ) then
         if ( libxc_p%idx>0 ) call xc_f90_func_end(libxc_p%px)
         if ( libxc_p%idc>0 ) call xc_f90_func_end(libxc_p%pc)
         libxc_p%lda_only = 0
        end if
        libxc_p%idx = 0
        libxc_p%idc = 0
        libxc_p%iexch = 0
        return
!EOC
      end subroutine endXC_libxc

!BOP
!!IROUTINE: gXC1_libxc
!
!!INTERFACE:
      subroutine gXC1_libxc(libxc_p,                                    &
     &                rho,rhoup,rhodn,                                  &
     &                grho2,gup2,gupdn,gdn2,                            &
     &                exc,vxc,vxc_up,vxc_dn,                            &
     &                ex,ec,vx,vc,vxup,vxdn,vcup,vcdn)
!!DESCRIPTION:
! 1-point XC calculation\\
!
! INPUT:
! {\bv
!   id_x, id_c : types of exchange and correlation
!                  functionals in "libxc", respectively
!                  (in, integer,optional)
!   rho    : spin-unpolarised charge density (in,real,optional)
!   rhoup  : spin-up charge density (in,real,optional)
!   rhodn  : spin-down charge density (in,real,optional)
!   grho2  : |grad rho|^2 (in,real,optional)
!   gup2   : |grad rhoup|^2 (in,real,optional)
!   gdn2   : |grad rhodn|^2 (in,real,optional)
!   gupdn  : (grad rhoup).(grad rhodn) (in,real,optional)
!  \ev}
!
! OUTPUT:
! {\bv
!   exc    : exchange-correlation energy density (out,real,optional)
!   vxc    : spin-unpolarised exchange-correlation potential (out,real,optional)
!   vxc_up : spin-up exchange-correlation potential (out,real,optional)
!   vxc_dn : spin-down exchange-correlation potential (out,real,optional)
!   ex     : exchange energy density (out,real,optional)
!   ec     : correlation energy density (out,real,optional)
!   vx     : spin-unpolarised exchange potential (out,real,optional)
!   vc     : spin-unpolarised correlation potential (out,real,optional)
!   vxup   : spin-up exchange potential (out,real,optional)
!   vxdn   : spin-down exchange potential (out,real,optional)
!   vcup   : spin-up correlation potential (out,real,optional)
!   vcdn   : spin-down correlation potential (out,real,optional)
!  \ev}
!
!!REMARKS:
!  Most arguments are optional
!EOP
!
!BOC
      implicit none
      type(xc_mecca_pointer) :: libxc_p
! optional (in) arguments
      real(8), optional, intent(in) :: rho
      real(8), optional, intent(in) :: rhoup
      real(8), optional, intent(in) :: rhodn
      real(8), optional, intent(in) :: grho2
      real(8), optional, intent(in) :: gup2
      real(8), optional, intent(in) :: gdn2
      real(8), optional, intent(in) :: gupdn
! optional (out) arguments
      real(8), optional, intent(out) :: exc
      real(8), optional, intent(out) :: vxc
      real(8), optional, intent(out) :: vxc_up
      real(8), optional, intent(out) :: vxc_dn
      real(8), optional, intent(out) :: ex
      real(8), optional, intent(out) :: ec
      real(8), optional, intent(out) :: vx
      real(8), optional, intent(out) :: vc
      real(8), optional, intent(out) :: vxup
      real(8), optional, intent(out) :: vxdn
      real(8), optional, intent(out) :: vcup
      real(8), optional, intent(out) :: vcdn
! local variables
      integer :: idx,idc
      integer :: xcf,id,k,nspin
      real(DBL_R) :: r(2),sigma(3),vsigma(3)
!      real(8), allocatable :: dxdg2(:),dcdg2(:)

      real(DBL_R) :: zex(1),zec(1),zvx(2),zvc(2)

      interface

       subroutine xc_f90_lda_exc_vxc(p, np, rho, e, v)
       use xc_f90_types_m
       type(xc_f90_pointer_t), intent(in)  :: p
       integer,                 intent(in)  :: np
       real(xc_f90_kind),       intent(in)  :: rho(*)   ! rho(nspin) the density
       real(xc_f90_kind),       intent(out) :: e(*)       ! the energy per unit particle
       real(xc_f90_kind),       intent(out) :: v(*)     ! v(nspin) the potential
       end subroutine xc_f90_lda_exc_vxc

       subroutine xc_f90_gga_exc_vxc(p,np,rho,sigma, zk,vrho,vsigma)
        use xc_f90_types_m
        type(xc_f90_pointer_t), intent(in)  :: p
        integer,                 intent(in)  :: np
        real(xc_f90_kind),       intent(in)  :: rho(*)
        real(xc_f90_kind),       intent(in)  :: sigma(*)
        real(xc_f90_kind),       intent(out) :: zk(*)
        real(xc_f90_kind),       intent(out) :: vrho(*)
        real(xc_f90_kind),       intent(out) :: vsigma(*)
       end subroutine xc_f90_gga_exc_vxc

      end interface

      idx=libxc_p%idx ; idc=libxc_p%idc
      if ( idx==0 .and. idc==0 ) return
      if (present(rhoup).and.present(rhodn)) then
        nspin=2
      else if (present(rho)) then
        nspin=1
      else
        write(*,*)
        write(*,'("Error(mecca_libxc): missing rho-arguments")')
        write(*,*)
        stop 10001
      end if
      zex = zero; zec = zero; zvx = zero; zvc = zero
      if (present(exc)) exc=zero
      if (present(vxc)) vxc=zero
      if (present(vxc_up)) vxc_up=zero
      if (present(vxc_dn)) vxc_dn=zero
      if (present(ex)) ex=zero
      if (present(vx)) vx=zero
      if (present(vxup)) vxup=zero
      if (present(vxdn)) vxdn=zero
      if (present(ec)) ec=zero
      if (present(vc)) vc=zero
      if (present(vcup)) vcup=zero
      if (present(vcdn)) vcdn=zero
! loop over functional kinds (exchange or correlation)
      zec=zero ; zex=zero; zvx=zero; zvc=zero
      do k=1,2
        select case (k)
         case (1)
           id = idx
         case (2)
           id = idc
         case default
           id = 0
        end select
        if (id.gt.0) then
          if ( nspin==1 ) then
           r(1)=rho;  ! r(2)=rho
          else
           r(1)=rhoup; r(2)=rhodn
          end if
          xcf=xc_f90_family_from_id(id)
          select case(xcf)
           case(XC_FAMILY_LDA)
!-------------------------------!
!           LDA functionals     !
!-------------------------------!
            if (id == idx) then ! exchange
              call xc_f90_lda_exc_vxc(libxc_p%px,1,r,zex,zvx)
            else if (id == idc) then ! correlation
              call xc_f90_lda_exc_vxc(libxc_p%pc,1,r,zec,zvc)
            end if
           case(XC_FAMILY_GGA,XC_FAMILY_HYB_GGA)
!-------------------------------!
!           GGA functionals     !
!-------------------------------!
! if ( .not. LDA) grho2, gup2,gupdn,gdn2 must be arguments of gXC_libxc
!
            if ( nspin==1 ) then
             sigma(1)=grho2 !/4;sigma(2)=grho2/4;sigma(3)=grho2/4
            else
             sigma(1)=gup2; sigma(2)=gupdn; sigma(3)=gdn2
            end if

!            if ( grho2.ne.zero .or. gup2.ne.zero .or. gupdn.ne.zero .or.&
!     &                              gdn2.ne.zero ) then
!              call fstop(' GGA is not implemented yet in gXC1_libxc')
!              ! use direct call xc_f90_gga_exc_vxc
!            end  if
!
            if (id == idx) then ! exchange
             call xc_f90_gga_exc_vxc(libxc_p%px,1,r,                    &
     &                           sigma,zex,zvx,vsigma)
              if ( id==GGA_X_CHACHIYO ) then
               if (xc_f90_info_kind(libxc_p%info_pc)                    &
     &                                ==XC_EXCHANGE_CORRELATION) then
                call xc_slater(1,nspin,r(1:),sigma(1),sigma(2:1+nspin))
                zex(1) = (zex(1)-sigma(1))*w_x(1)
                zvx(1:nspin) = (zvx(1:nspin)-sigma(2:1+nspin))*w_x(1)
               end if
              end if

            else if (id == idc) then ! correlation
             call xc_f90_gga_exc_vxc(libxc_p%pc,1,r,                    &
     &                           sigma,zec,zvc,vsigma)
             if ( id==GGA_X_CHACHIYO ) then
              if (xc_f90_info_kind(libxc_p%info_px)                     &
     &                             ==XC_EXCHANGE_CORRELATION) then
                call xc_slater(1,nspin,r(1),sigma(1),sigma(2:))
                zec(1) = (zec(1)-sigma(1))*w_x(1)
                zvc(1:nspin) = (zvc(1:nspin)-sigma(2:1+nspin))*w_x(1)
              end if
             end if
            end if
!          case(XC_FAMILY_MGGA)
           case default
            write(*,*)
            write(*,'(''Warning(mecca_libxc): unsupported libxc''       &
     &                //'' functional family '',I8)') xcf
            write(*,*)
            stop 10002
          end select
        end if
      end do
      zex=zex/ry2H; zec=zec/ry2H   ! now in Ry
      zvx=zvx/ry2H; zvc=zvc/ry2H
      if (present(exc)) exc = zex(1) + zec(1)
      if (present(ex))   ex = zex(1)
      if (present(ec))   ec = zec(1)
      if ( nspin==1 ) then
        if (present(vxc)) vxc = zvx(1)+zvc(1)
        if (present(vx))   vx = zvx(1)
        if (present(vc))   vc = zvc(1)
      else if ( nspin==2 ) then
        if (present(vxc_up)) vxc_up = zvx(1)+zvc(1)
        if (present(vxc_dn)) vxc_dn = zvx(2)+zvc(2)
        if (present(vxup)) vxup = zvx(1)
        if (present(vxdn)) vxdn = zvx(2)
        if (present(vcup)) vcup = zvc(1)
        if (present(vcdn)) vcdn = zvc(2)
      end if
      return
!EOC
      end subroutine gXC1_libxc

!BOP
!!IROUTINE: gXC_libxc
!!INTERFACE:
      subroutine gXC_libxc(n,libxc_p,                                   &
     &                rho,rhoup,rhodn,                                  &
     &                grho2,gup2,gupdn,gdn2,                            &
     &                exc,vxc,vxc_up,vxc_dn,                            &
     &                ex,ec,vx,vc,vxup,vxdn,vcup,vcdn,                  &
     &                vsigma_uu,vsigma_ud,vsigma_dd)
!!DESCRIPTION:
! n-point XC calculation
!
! INPUT:
! {\bv
!   n      : number of density points (in,integer)
!   id_x, id_c : (in, integer,optional)
!            types of exchange and correlation
!            functionals in "libxc", respectively.
!   rho    : spin-unpolarised charge density (in,real(n),optional)
!   rhoup  : spin-up charge density (in,real(n),optional)
!   rhodn  : spin-down charge density (in,real(n),optional)
!   grho2  : |grad rho|^2 (in,real(n),optional)
!   gup2   : |grad rhoup|^2 (in,real(n),optional)
!   gdn2   : |grad rhodn|^2 (in,real(n),optional)
!   gupdn  : (grad rhoup).(grad rhodn) (in,real(n),optional)
! \ev}
!
! OUTPUT:
! {\bv
!   vxc,vx,vc are only for nspin=1; vxc_up/dn,vxup/vxdn,vcup/vcdn are only for nspin=2
!   exc    : exchange-correlation energy density (out,real(n),optional)
!   vxc    : spin-unpolarised exchange-correlation potential (out,real(n),optional)
!   vxc_up : spin-up exchange-correlation potential (out,real(n),optional)
!   vxc_dn : spin-down exchange-correlation potential (out,real(n),optional)
!   ex     : exchange energy density (out,real(n),optional)
!   ec     : correlation energy density (out,real(n),optional)
!   vx     : spin-unpolarised exchange potential (out,real(n),optional)
!   vc     : spin-unpolarised correlation potential (out,real(n),optional)
!   vxup   : spin-up exchange potential (out,real(n),optional)
!   vxdn   : spin-down exchange potential (out,real(n),optional)
!   vcup   : spin-up correlation potential (out,real(n),optional)
!   vcdn   : spin-down correlation potential (out,real(n),optional)
! \ev}
!
!!REMARKS:
! Most arguments are optional
!EOP
!
!BOC
      implicit none
! mandatory arguments
      integer, intent(in) :: n
      type(xc_mecca_pointer), target :: libxc_p
! optional (in) arguments
!  either (non-magnetic)
      real(8), optional, intent(in) :: rho(n)
!
      real(8), optional, intent(in) :: grho2(n)
!  or      (magnetic)
      real(8), optional, intent(in) :: rhoup(n)
      real(8), optional, intent(in) :: rhodn(n)
!
      real(8), optional, intent(in) :: gup2(n)
      real(8), optional, intent(in) :: gdn2(n)
      real(8), optional, intent(in) :: gupdn(n)
! optional (out) arguments
      real(8), optional, intent(out) :: exc(n)
      real(8), optional, intent(out) :: ex(n)
      real(8), optional, intent(out) :: ec(n)
!  either (non-magnetic)
      real(8), optional, intent(out) :: vxc(n)
      real(8), optional, intent(out) :: vx(n)
      real(8), optional, intent(out) :: vc(n)
!if gga:
      real(8), optional, intent(out) :: vsigma_uu(n)
!  or      (magnetic)
      real(8), optional, intent(out) :: vxc_up(n)
      real(8), optional, intent(out) :: vxc_dn(n)
      real(8), optional, intent(out) :: vxup(n)
      real(8), optional, intent(out) :: vxdn(n)
      real(8), optional, intent(out) :: vcup(n)
      real(8), optional, intent(out) :: vcdn(n)
!if gga:
!      real(8), optional, intent(out) :: vsigma_uu(n)
      real(8), optional, intent(out) :: vsigma_ud(n)
      real(8), optional, intent(out) :: vsigma_dd(n)
! local variables
      integer :: idx,idc
      integer :: xcf,id,k,nspin
! allocatable arrays
      real(DBL_R), allocatable, target :: zkx(:),zkc(:)
      real(DBL_R), allocatable, target :: vrhox(:,:),vrhoc(:,:)
      real(DBL_R), allocatable :: vsigma(:,:)
      real(DBL_R), allocatable :: vsigma1(:,:)
      real(DBL_R), allocatable :: r(:,:),sigma(:,:)
      real(DBL_R), allocatable :: slater(:,:)
!      real(DBL_R), allocatable :: dxdg2(:),dcdg2(:)
      real(DBL_R), pointer :: zk(:),vrho(:,:)
      type(xc_f90_pointer_t), pointer :: xc_p
      if (present(rhoup).and.present(rhodn)) then
        nspin=2
      else if (present(rho)) then
        nspin=1
      else
        write(*,*)
        write(*,'("Error(mecca_libxc): missing rho-arguments")')
        write(*,*)
        stop 20001
      end if

      allocate(r(nspin,n))
      allocate(zkx(n),zkc(n)); zkx=0; zkc=0
      allocate(vrhox(nspin,n),vrhoc(nspin,n)); vrhox=0;vrhoc=0

      idx=libxc_p%idx ; idc=libxc_p%idc

      if ( nspin==1 ) then
       allocate(vsigma(1,n))
       allocate(sigma(1,n),vsigma1(1,n))
      else
       allocate(vsigma(3,n))
       allocate(sigma(3,n),vsigma1(3,n))
      end if
      allocate(slater(3,n)); slater = zero
!      sigma = zero
!      vsigma1= zero
      vsigma = zero

      if (present(exc)) exc(:)=zero
      if (present(vxc)) vxc(:)=zero
      if (present(vxc_up)) vxc_up(:)=zero
      if (present(vxc_dn)) vxc_dn(:)=zero
      if (present(ex)) ex(:)=zero
      if (present(vx)) vx(:)=zero
      if (present(vxup)) vxup(:)=zero
      if (present(vxdn)) vxdn(:)=zero
      if (present(ec)) ec(:)=zero
      if (present(vc)) vc(:)=zero
      if (present(vcup)) vcup(:)=zero
      if (present(vcdn)) vcdn(:)=zero
      if (present(vsigma_uu)) vsigma_uu(:)=zero
      if (present(vsigma_ud)) vsigma_ud(:)=zero
      if (present(vsigma_dd)) vsigma_dd(:)=zero
! loop over functional kinds (exchange or correlation)
      xc_p => null()
      zk => null()
      vrho => null()
      do k=1,2
        select case (k)
         case (1)
           id = idx
           zk => zkx
           vrho => vrhox
           xc_p => libxc_p%px
         case (2)
           id = idc
           zk => zkc
           vrho => vrhoc
           xc_p => libxc_p%pc
         case default
           id = 0
           xc_p => null()
        end select
        if ( associated(zk) ) zk = zero
        if ( associated(vrho) ) vrho = zero
        if ( id==0 ) then
         cycle
        end if
        if (id.gt.0) then
          if ( nspin==1 ) then
           r(1,:)=rho(:); ! r(2,:)=0
          else
           r(1,:)=rhoup(:); r(2,:)=rhodn(:)
          end if
          xcf=xc_f90_family_from_id(id)
          select case(xcf)
           case(XC_FAMILY_LDA)
!-------------------------------!
!           LDA functionals     !
!-------------------------------!
            call xc_f90_lda_exc_vxc(xc_p,n,r(1,1),zk(1),vrho(1,1))

           case(XC_FAMILY_GGA,XC_FAMILY_HYB_GGA)
!-------------------------------!
!           GGA functionals     !
!-------------------------------!

! if ( .not. LDA) grho2, gup2,gupdn,gdn2 must be arguments of gXC_libxc

            if ( nspin==1 ) then
             sigma(1,:)=grho2(:)
            else
             sigma(1,:)=gup2(:); sigma(2,:)=gupdn(:); sigma(3,:)=gdn2(:)
            end if
            call xc_f90_gga_exc_vxc(xc_p,n,r(1,1),sigma(1,1),           &
     &                                 zk(1),vrho(1,1),vsigma1(1,1))
            if ( id==GGA_X_CHACHIYO ) then
             if ( (id.ne.idx .and. xc_f90_info_kind(libxc_p%info_px)    &
     &                                ==XC_EXCHANGE_CORRELATION) .or.   &
     &            (id.ne.idc .and. xc_f90_info_kind(libxc_p%info_pc)    &
     &                                ==XC_EXCHANGE_CORRELATION)        &
     &          ) then
              call xc_slater(n,nspin,r(1,1),slater(1,1),slater(2,1))
              if (libxc_p%lda_only==GGA_X_CHACHIYO ) then ! non-gradient part of XC_GGA_X_CHACHIYO is Slater's term
               zk(1:n) = slater(1,1:n)
               vrho(1:nspin,1:n)=slater(2:1+nspin,1:n)
              else
               if ( (xc_f90_family_from_id(idx)==XC_FAMILY_HYB_GGA      &
     &               .and. id==idc) .or. (id==idx .and.                 &
     &              xc_f90_family_from_id(idc)==XC_FAMILY_HYB_GGA)      &
     &            ) then
                zk(1:n) = zk(1:n)*w_x(1)
                vrho(1:nspin,1:n) =                                     &
     &                 vrho(1:nspin,1:n)*w_x(1)
                vsigma1 = w_x(1)*vsigma1
               else
                zk(1:n) = (zk(1:n)-slater(1,1:n))*w_x(1)
                vrho(1:nspin,1:n) =                                     &
     &                 (vrho(1:nspin,1:n)-slater(2:1+nspin,1:n))*w_x(1)
                vsigma1 = w_x(1)*vsigma1
               end if
              end if
             end if
            end if
            vsigma = vsigma + vsigma1/ry2H
!          case(XC_FAMILY_MGGA)
           case default
            write(*,*)
            write(*,'("Warning(mecca_libxc): unsupported libxc"         &
     &                //" functional family ",I8)') xcf
            write(*,*)
            stop 20002
          end select
        end if
      end do
      zkx=zkx/ry2H; zkc=zkc/ry2H
      vrhox=vrhox/ry2H; vrhoc=vrhoc/ry2H
      if (present(exc)) exc(:) = zkx(:) + zkc(:)
      if (present(ex)) ex(:) = zkx(:)
      if (present(ec)) ec(:) = zkc(:)
      if ( nspin==1 ) then
        if (present(vxc)) vxc = vrhox(1,:)+vrhoc(1,:)
        if (present(vx))   vx = vrhox(1,:)
        if (present(vc))   vc = vrhoc(1,:)
        if (present(vsigma_uu))   vsigma_uu = vsigma(1,:)
      else if ( nspin==2 ) then
        if (present(vxc_up)) vxc_up = vrhox(1,:)+vrhoc(1,:)
        if (present(vxc_dn)) vxc_dn = vrhox(2,:)+vrhoc(2,:)
        if (present(vxup)) vxup = vrhox(1,:)
        if (present(vxdn)) vxdn = vrhox(2,:)
        if (present(vcup)) vcup = vrhoc(1,:)
        if (present(vcdn)) vcdn = vrhoc(2,:)
        if (present(vsigma_uu))   vsigma_uu = vsigma(1,:)
        if (present(vsigma_ud))   vsigma_ud = vsigma(2,:)
        if (present(vsigma_dd))   vsigma_dd = vsigma(3,:)
      end if
          if ( allocated(r) ) deallocate(r)
          if ( allocated(zkx) ) deallocate(zkx)
          if ( allocated(vrhox) ) deallocate(vrhox)
          if ( allocated(zkc) ) deallocate(zkc)
          if ( allocated(vrhoc) ) deallocate(vrhoc)
          if ( allocated(vsigma) ) deallocate(vsigma)
          if ( allocated(vsigma1) ) deallocate(vsigma1)
          if ( allocated(sigma) ) deallocate(sigma)
          nullify(zk,vrho)
      return
!EOC
      end subroutine gXC_libxc

!BOP
!!IROUTINE: gInfo_libxc
!!INTERFACE:
      subroutine gInfo_libxc(libxc_p,xc_descr)
!!DESCRIPTION:
! to generate XC-info description (as a character line xc\_descr)
!
!EOP
!
!BOC
      implicit none
! arguments
      type(xc_mecca_pointer) :: libxc_p
      character(*), intent(out) :: xc_descr
! local variables
      integer :: id_x
      integer :: id_c
      integer fmly,st_env
      character(256) name
      character(512) xcdescr

      id_x = libxc_p%idx
      id_c = libxc_p%idc
      if ( max(id_x,id_c)>0 ) then
        write(xcdescr,'(''('',i3,'';'',i3,'')'')') id_x,id_c
      end if
      if ( id_x>0 ) then
        call xc_f90_info_name(libxc_p%info_px,name)
        fmly=xc_f90_family_from_id(id_x)
        select case (xc_f90_info_kind(libxc_p%info_px))
         case (XC_EXCHANGE)
          xcdescr=trim(xcdescr)//' exchange: '//trim(name)
          if ( id_x==GGA_X_CHACHIYO ) then
           if ( xc_f90_info_kind(libxc_p%info_pc) ==                    &
     &                XC_EXCHANGE_CORRELATION ) then
            xcdescr=trim(xcdescr)//' (w/o Slater part)'
           end if
          end if
         case (XC_CORRELATION)
          xcdescr=trim(xcdescr)//' correlation: '//trim(name)
         case (XC_EXCHANGE_CORRELATION)
          xcdescr=trim(xcdescr)//' exchange-correlation: '//trim(name)
         case default
          xcdescr=''
        end select
        if ( libxc_p%lda_only==0 ) then
         if ( fmly.eq.XC_FAMILY_LDA ) xcdescr=trim(xcdescr)//', LDA'
         if (fmly.eq.XC_FAMILY_GGA .or. fmly.eq.XC_FAMILY_HYB_GGA) then
          call gEnvVar('ENV_DISABLE_XC_GRADCORR',.false.,st_env)
!
          if ( libxc_p%idx==GGA_X_CHACHIYO ) then
!! there is a Nan bug in libxc 4.2.1 version if grad(rho)/rho is very small
           st_env = 0
          end if
!
          if ( st_env==1 ) then
           xcdescr=trim(xcdescr)//', GGA+lda_only'
          else
           if ( fmly.eq.XC_FAMILY_GGA ) then
             xcdescr=trim(xcdescr)//', GGA'
           elseif ( fmly.eq.XC_FAMILY_HYB_GGA ) then
            xcdescr=trim(xcdescr)//', hyb-GGA'
           else
            xcdescr=trim(xcdescr)//', unknown'
           end if
          end if
         end if
        else
         xcdescr=trim(xcdescr)//', LDA'
        end if
      else
        if ( id_c <=0 ) then
         select case(libxc_p%iexch)
          case (XC_LDA_HEDIN)
           xcdescr='mecca_xc: Hedin&Lundqvist, LDA'
          case (XC_LDA_VOSKO)
           xcdescr='mecca_xc: Vosko,Wilk,Nusair, LDA '
          case (X_GGA_vLB)
           xcdescr='mecca_xc: vLeeuwen&Baerends, GGA'
          case (X_GGA_vLBi)
           xcdescr='mecca_xc: spin-independent vLeeuwen&Baerends, GGA'
          case (X_GGA_vLBs)
           xcdescr='mecca_xc: vLeeuwen&Baerends - v0, GGA'
          case (XC_GGA_AM05)
           xcdescr='mecca_xc: Armiento&Mattsson 05, GGA'
          case default
           xcdescr='mecca_xc:  no exchange-correlation '
         end select
        else
         xcdescr=trim(xcdescr)//' exchange: none'
        end if
      end if
      if ( id_c>0 ) then
        call xc_f90_info_name(libxc_p%info_pc,name)
        fmly=xc_f90_family_from_id(id_c)
        select case (xc_f90_info_kind(libxc_p%info_pc))
         case (XC_EXCHANGE)
          xcdescr=trim(xcdescr)//'; exchange: '//trim(name)
          if ( id_c==GGA_X_CHACHIYO ) then
           if ( xc_f90_info_kind(libxc_p%info_px) ==                    &
     &                XC_EXCHANGE_CORRELATION ) then
            xcdescr=trim(xcdescr)//' (w/o Slater part)'
           end if
          end if
         case (XC_CORRELATION)
          xcdescr=trim(xcdescr)//'; correlation: '//trim(name)
         case (XC_EXCHANGE_CORRELATION)
          xcdescr=trim(xcdescr)//'; exchange-correlation: '//trim(name)
         case default
          xcdescr=''
        end select
        if ( libxc_p%lda_only==0 ) then
         if ( fmly.eq.XC_FAMILY_LDA ) xcdescr=trim(xcdescr)//', LDA'
         if (fmly.eq.XC_FAMILY_GGA .or. fmly.eq.XC_FAMILY_HYB_GGA) then
          call gEnvVar('ENV_DISABLE_XC_GRADCORR',.false.,st_env)
          if ( libxc_p%idc==GGA_X_CHACHIYO ) then
!! there is a Nan bug in libxc 4.2.1 version if grad(rho)/rho is very small
           st_env = 0
          end if
          if ( st_env==1 ) then
           xcdescr=trim(xcdescr)//', GGA+lda_only'
          else
           xcdescr=trim(xcdescr)//', GGA'
          end if
         end if
        else
         xcdescr=trim(xcdescr)//', LDA'
        end if
      else
!        if ( id_x>0 ) then
!          xcdescr=trim(xcdescr)//'; correlation: none'
!        end if
      end if
      xc_descr = trim(xcdescr(1:min(len(xcdescr),len(xc_descr))))
      return
!EOC
      end subroutine gInfo_libxc
!
!BOP
!!IROUTINE: dump_print
!!INTERFACE:
      subroutine dump_print(f,df,r,n,ix,ic)
!
!EOP
!
!BOC
      implicit none
      integer, save :: io=200
      real(8), intent(in) :: f(:),df(:),r(:)
      real(8) :: t
      integer, intent(in) :: n,ix,ic
      integer i
      character(100) :: base
      if ( n <=1 ) return
      write(base,'(a,BZ,i6,''.'',i3)') 'F',ix*d_xc_id+ic,io
      if ( len(trim(base))<=0 ) then
      open(io,form='formatted')
      else
      open(io,file=trim(base),status='unknown',form='formatted')
      end if
      do i=1,n
!!!       if ( f(i)>0 ) then
!!!       t = 0.252d0*abs(df(i))/f(i)**(7.d0/6.d0)
!!!       else
!!!        t=-1.d0
!!!       end if
!!!       write(io,'(1x,i5,1x,4(1x,e18.8))') i,r(i),f(i),df(i),t
       write(io,'(1x,4(1x,e23.15))') r(i),                              &
     &      abs(df(i))/f(i)**(dble(4)/dble(3)),r(i)**2*f(i),abs(df(i))
      end do
      close(io,status='keep')
      io = io+1
      return
!EOC
      end subroutine dump_print
!
!BOP
!!IROUTINE: damp_grad
!!INTERFACE:
      subroutine damp_grad(r,rho,drho,n)
!EOP
!
!BOC
      implicit none
      integer, intent(in) :: n
      real(DBL_R), intent(in) :: r(n),rho(n)
      real(DBL_R), intent(inout) :: drho(n)
      real(DBL_R), parameter ::                                         &
     &    ct=sqrt(pi/four/(three*pi**2)**(one/three))/two
!!      real(8), parameter :: t0=3.3d0
!!      real(8), parameter :: tmax=4*t0
!      real(DBL_R), parameter :: t0=10*3.3d0
!      real(DBL_R), parameter :: tmax=0.1d0*t0
!      real(DBL_R), parameter :: f7d6=seven/six
      real(DBL_R) :: t,tx,xx
      real(DBL_R) :: dns(n)
      integer :: i,i0

      call damping_fct(n,r,dns)
      drho(1:n) = drho(1:n)*dns(1:n)   ! damping cannot depend on charge density
                                       ! (otherwise correction to energy/potential is required)
!!      do i=1,n
!!       if ( rho(i)>0 ) then
!!        xx = rho(i)**f7d6/ct
!!        tx = drho(i)/xx
!!        t  = abs(tx)   !  t = ct*abs(drho(i))/rho(i)**(7.d0/6.d0)
!!        if ( t>t0 ) then
!!!??        if ( tx>t0 ) then
!!!          t = t0 + tmax*log(one+(t-t0)/tmax)
!!          t = t0 + tmax*atan((t-t0)/tmax)
!!          if ( tx<zero ) then
!!            drho(i) = -t*xx
!!          else
!!            drho(i) = t*xx
!!          end if
!!        end if
!!       else
!!        drho(i) = zero
!!       end if
!!      end do
      return
!EOC
      end subroutine damp_grad

      subroutine derv5fltr(sigm,fin,df,r,n)
      implicit none
      integer, intent(in) :: n
      real(8), intent(in) :: sigm
      real(8), intent(in) :: fin(n)
      real(8), intent(out) :: df(n)
      real(8), intent(in) :: r(n)

      integer :: i
      real(8) :: z(n),f(n)
      real(8) :: bi,hi,zi,zi1
      real(8), parameter :: twhird=two/three, d6=one/six
      integer, parameter :: nfl=7

      hi = abs(sigm)
      if ( sigm<zero ) then

       f = fin
       call avintrfltr(n,f,r,hi)
       call zspl3(n,r,f,z)
       zi1 = z(1)
       do i=1,n-1
        hi = r(i+1)-r(i)
        bi = (f(i+1)-f(i))/hi
        zi = zi1
        zi1 = z(i+1)
        df(i) = bi-hi*(twhird*zi+d6*zi1)
       end do
       df(n) = df(n-1)+(z(n-1)+z(n))*(r(n)-r(n-1))/2

      else if ( sigm>zero ) then

       f = fin
       call avintrfltr(n,f,r,hi)
       call derv5(f,df,r,n)
       call avintrfltr(n,df,r,hi)

      else

       call derv5(fin,df,r,n)
       hi = max(r(nfl)-r(1),1.d-4)
       call avintrfltr(n,df,r,hi)

      end if
      return
      end subroutine derv5fltr
!
!BOP
!!ROUTINE: dervfit
!!INTERFACE:
      subroutine dervfit(ndata,r,f,eps,step,nmax,df,df2)
!!DESCRIPTION:
! calculation of 1st ({\tt df}) and 2nd ({\tt df2}) derivatives of a function
! defined by table function {\tt f(r)} (segments with {\tt f(r)$<$eps} are ignored)
! by fitting of input data.
! {\bv
! Variables:
!
!    'r' is an ascending array of distinct non-negative coordinates.
!    'f' is the array of function values for each value of r.
!    'ndata' is the size of the coordinate-data arrays.
!    'nmax' is the polynomial fitting order.
! \ev}

!!REVISION HISTORY:
! Initial Version - A.S. - 2016
!EOP
!
!BOC
      implicit        none
      integer, intent(in) :: ndata
      real(8), intent(in) :: r(ndata)
      real(8), intent(inout) :: f(ndata)
      real(8), intent(in) :: eps
      real(8), intent(in) :: step
      integer, intent(in) :: nmax
      real(8), intent(out) :: df(ndata)
      real(8), intent(out), optional :: df2(ndata)

      real(8), external :: ylag

      integer :: i,ind,it,ix,iz,nf,nz,nrpt,nm
      real(DBL_R) :: h,dv
      real(DBL_R), allocatable :: x(:)
      real(DBL_R), allocatable :: y(:),y1(:),y2(:),f1(:)

      iz = 1
      do i=ndata-1,1,-1
       if ( f(i)<=eps ) then
        iz = i
        exit
       end if
      end do

      nz = ndata-iz+1
      nm = min(nmax,nz)      ! poly-order for interpolation
      if ( step==zero) then
       nf = ndata-iz
      else
       nf = nint((r(ndata)-r(iz))/step)
      end if
      if ( nf<1) then
       df = 0
       if ( present(df2) ) df2 = 0
       return
      end if
      h = (r(ndata)-r(iz))/nf
!
      it = iz
      if ( step==zero ) then
        do i=iz+1,ndata
         if (r(i)-r(i-1)>h/nm) then
          nf = nint((r(ndata)-r(i))/h)
          h = (r(ndata)-r(i))/nf
          nf = (i-iz) + nf
          it = i
          exit
         end if
        end do
      end if
!
      nf = nf+1
      allocate(x(max(nf,ndata)))
      allocate(y(size(x)),y1(size(x)),y2(size(x)),f1(size(x)))

      f1 = f
      if ( step == zero) then
       do i=iz,it-1
        x(i) = r(i)
        y(i) = f1(i)
       end do
       do i=it,nf
        x(i) = r(it) + h*(i-it)
       end do
       call steff_interpolation(ndata-iz+1,r(iz:),f1(iz:),              &
     &                                            nf-it+1,x(it:),y(it:))
      else
       call avintrfltr(ndata,f1,r,step)
       ind=0
       do i=1,nf
        x(i) = r(iz) + h*(i-1)
        y(i) = ylag(x(i),r(iz:ndata),f1(iz:ndata),ind,nm,nz,ix)
       end do
!       call steff_interpolation(ndata-iz+1,r(iz:),f1(iz:),                &   ! failed to work
!     &                                            nf,x(1:),y(1:))
      end if
      f(iz) = max(eps,f(iz))

      call derv5_VP(y,y1,x,1,nf)

      call steff_interpolation(nf,x(1:),y1(1:),                         &
     &                                       ndata-iz+1,r(iz:),df(iz:))

      if ( present(df2) ) then
       call damp_grad(x,y,y1,nf)  ! (4pi)^(1/6) is ignored
       if ( iz>1 ) then
        y1(iz) = 0
       end if
       nrpt=4
       do i=1,nrpt
        call avfltr(nf,y1,3)
       end do

!       call derv5fltr(step,df(1:ndata),df2(1:ndata),r(1:ndata),ndata)
       call derv5_VP(y1,y2,x,1,nf)
       nrpt=4
       do i=1,nrpt
        call avfltr(nf,y2,3)
       end do

       call steff_interpolation(nf,x(1:),y2(1:),                        &
     &                                       ndata-iz+1,r(iz:),df2(iz:))

       if ( iz > 1 ) then
        dv = (df2(iz+1)-df2(iz))/(x(iz+1)-x(iz))
        df2(1:iz-1) = df2(iz) + dv*x(1:iz-1)
       end if
      end if

      if ( iz>1 ) then
       dv = (df(iz+1)-df(iz))/(x(iz+1)-x(iz))
       df(1:iz-1) = df(iz) + dv*x(1:iz-1)
      end if

      deallocate(x,y,y1,y2,f1)
      return

!EOC
      end subroutine dervfit
!
!BOP
!!ROUTINE: avintrfltr
!!INTERFACE:
      subroutine avintrfltr(n,f,r,step)
      implicit none
!!DESCRIPTION:
!  7-points averaging filter is applied to interpolated function f(x),
!  x(i) = r(1) + h*(i-1), h is as close as possible to input parameter {\tt step}
!  taking into account that last point on x-mesh is equal to r(n).
!!ARGUMENTS:
      integer, intent(in) :: n
      real(8), intent(inout) :: f(n)
      real(8), intent(in) :: r(n)
      real(8), intent(in) :: step
!EOP
!
!BOC
      real(8), external :: ylag
      integer, parameter :: nmax=3,nrpt=4
      integer :: i,ix,nf,ind
      real(DBL_R), allocatable :: x(:),y(:)
      real(DBL_R) :: h

      nf = nint((r(n)-r(1))/step)
      h = (r(n)-r(1))/nf
      nf = nf+1
      allocate(x(nf),y(nf))
      ind=0
      do i=1,nf
       x(i) = r(1) + h*(i-1)
      end do
      call steff_interpolation(n,r(1:),f(1:),nf,x(1:),y(1:))
      do i=1,nrpt   ! (2*3+1)-points filter
       call avfltr(nf,y,3)
      end do
      call steff_interpolation(nf,x(1:),y(1:),n,r(1:),f(1:))
      deallocate(x,y)
      return
!EOC
      end subroutine avintrfltr
!
!BOP
!!ROUTINE: avfltr
!!INTERFACE:
      subroutine avfltr(n,f,nav)
      implicit none
!!DESCRIPTION:
!  averaging filter with {\tt 2*nav+1} points
!
!!ARGUMENTS:
      integer, intent(in) :: n
      real(8), intent(inout) :: f(n)
      integer, intent(in) :: nav
!EOP
!
!BOC
      real(DBL_R), parameter :: zero=0
      integer :: i
      real(DBL_R) :: ff(1-nav:n+nav),w(2*nav+1)
      if ( nav<=0 ) return
      w = dble(1)/dble(2*nav+1)
      ff(1:n) = f(1:n)
      ff(:0) = f(1)
      ff(n+1:) = f(n)
      do i=1,n
       f(i) = dot_product(w(1:2*nav+1),ff(i-nav:i+nav))
      end do
      return
!EOC
      end subroutine avfltr
!
!BOP
!!ROUTINE: damping_fct
!!INTERFACE:
      subroutine damping_fct(n,r,f)
      implicit none
!!DESCRIPTION:
!  damping function
!
!!ARGUMENTS:
      integer, intent(in) :: n
      real(8), intent(in) :: r(n)
      real(8), intent(out) :: f(n)
!EOP
!
!BOC
!      integer, parameter :: irglrz = 0
!      integer, parameter :: irglrz = 1
      integer, parameter :: irglrz = 2
      real(DBL_R), parameter :: rn=0.010d0
!      real(8), parameter :: rn=0.005d0   ! it is about 265 fm
!      real(8), parameter :: rn=0.0005d0   ! it is about 26.5 fm
!      real(DBL_R), parameter :: rn=0.0d0   ! to disable damping
      real(DBL_R) :: x,fx
      integer :: i

      f = one
      do i=1,n
       if ( r(i)<=rn ) then
        x = r(i)/rn
        fx = x**2*(one-(one-x**2)**4)*two/(one+(x**2)**2)
        select case(irglrz)
         case(1)
          f(i) = sqrt( fx )
         case(2)
          f(i) = fx
         case default
          exit
        end select
       else
        exit
       end if
      end do
      return
!EOC
      end subroutine damping_fct
!
!BOP
!!ROUTINE: x_gga_lbmod
!!INTERFACE:
      subroutine x_gga_lbmod(rho1,rho2,grd1,grd2,vx1,vx2)
!!DESCRIPTION:
!  van Leeuwen & Baerends modified potential
!
!!REVISION HISTORY:
! Initial Version - A.S. - 2018
!EOP
!
!BOC
      implicit none
      real(8), intent(in), target  :: rho1,rho2,grd1,grd2
      real(8), intent(inout), target :: vx1,vx2

      real(DBL_R), pointer :: ds=>null(),grd=>null(),vx=>null()
      real(DBL_R) :: dso3,f,gx,x
      real(DBL_R), parameter :: thrd = dble(1)/dble(3)
      integer :: i

      do i=1,2
       if ( i==1 ) then
        ds=>rho1
        grd=>grd1
        vx => vx1
       else
        ds=>rho2
        grd=>grd2
        vx => vx2
       end if

       if ( ds<=0 ) then
        cycle
       else

        vx = 0.d0
!        if ( ionizpot > 0.d0  ) then
!         aa = 2.d0*dsqrt(2.d0*ionizpot)
!        else
!         aa = 0.5d0
!        end if

        dso3 = ds**thrd
        x = abs(grd)/(ds*dso3)    ! grad(rho)/rho^(4/3)
        gx = gam*x
        f = -(beta*x)*x/(1.d0 + 3.d0*(beta*x)*dasinh(gx))
!!        if (gx < 300.d0) then
!!          f = -(beta*x)*x/(1.d0 + 3.d0*(beta*x)*dasinh(gx))
!!        else
!!          f = -x/(3.d0*dlog(2.d0*gx))
!!        end if
        vx = vx + f * dso3
       end if

      end do

      nullify(vx,grd,ds)

      return

      end subroutine x_gga_lbmod
!
      subroutine xc_setpar( ext_beta,ext_ipot,ext_gam )
      implicit none
      real(8), intent(in), optional :: ext_beta
      real(8), intent(in), optional :: ext_ipot
      real(8), intent(in), optional :: ext_gam

      if ( present(ext_beta) )   then
       if ( ext_beta .ne. dble(0) ) then
        if ( beta .ne. ext_beta ) write(6,'(/a,f7.4)')                  &
     &                 ' Parameter beta (vLB potential) is ',ext_beta
        beta  = ext_beta
       end if
      end if
      if ( present(ext_ipot) )  then
       if ( ionizpot .ne. ext_ipot ) write(6,'(/a,f7.4)')               &
     &             ' Parameter ionizpot (vLB potential) is ',ext_ipot
       ionizpot = ext_ipot
      end if
      if ( present(ext_gam) ) then
       if ( gam .ne. ext_gam ) write(6,'(/a,f7.4)')                     &
     &                  ' Parameter gam (vLB potential) is ',ext_gam
       gam      = ext_gam
      end if

      return
      end subroutine xc_setpar

      subroutine xc_slater(n,spin,rho,ex,vx)
!
!    Slater functional:  xc_slater(rho_up) + xc_slater(rho_dn)
!
      implicit none
      integer, intent(in) :: n,spin
      real(DBL_R), intent(in) :: rho(*)
      real(DBL_R), intent(out) :: ex(*)
      real(DBL_R), intent(out) :: vx(*)

      type(xc_f90_pointer_t), save :: xc_func,xc_info
      integer, save :: S=0

      if ( S==0 ) then
       S = spin
       if ( S.ne.XC_POLARIZED .and. S.ne.XC_UNPOLARIZED ) then
        write(6,*)                                                      &
     &   ' Incorrect array dimensions in xc_slater, S-factor=',S
        call fstop(' Error in xc_slater (xc_mecca)')
       end if
       call xc_f90_func_init(xc_func, xc_info, XC_LDA_X, S)
      else
       if ( n<0 ) then
        call xc_f90_func_end(xc_func)
        S = 0
        return
       end if
      end if
      call xc_f90_lda_exc_vxc(xc_func,n,rho(1),ex(1),vx(1))

      return
      end subroutine xc_slater

      end module xc_mecca

      integer function maxXCid()
      use xc_mecca, only : max_XC_id
      maxXCid = max_XC_id
      return
      end function maxXCid

      integer function maxNonLibxcID()
      use xc_mecca, only : max_nonlibxc_id
      maxNonLibxcID = max_nonlibxc_id
      return
      end function maxNonLibxcID

      subroutine setXpotCorr(xpot_corr_id,xpot_corr_par)
       use xc_mecca, only : X_GGA_vLB,X_GGA_vLBi,X_GGA_vLBs,xc_setpar
!
       implicit none
       integer :: xpot_corr_id
       real(8), optional :: xpot_corr_par(:)

       real(8), allocatable :: params(:)
       integer :: i

       if ( present(xpot_corr_par) ) then
        allocate(params(size(xpot_corr_par)))
        params = xpot_corr_par
       else
        allocate(params(1))
        params(1) = 0
       end if

       if ( xpot_corr_id==X_GGA_vLB                                     &
     &      .or. xpot_corr_id==X_GGA_vLBi                               &
     &      .or. xpot_corr_id==X_GGA_vLBs                               &
     &    ) then
        do i=1,size(params)
         select case(i)
         case(1)
          call xc_setpar(ext_beta=params(i))
         case(2)
          call xc_setpar(ext_ipot=params(i))
         case(3)
          call xc_setpar(ext_gam =params(i))
         case default
         end select
        end do
       end if

       return
      end subroutine setXpotCorr
!BOP
!!ROUTINE: ext_libxc
!!DESCRIPTION:
! external procedure to calculate exchange-correlation energy and potential (array version);
! output is in Hartree atom.units
!
!!INTERFACE:
      subroutine ext_libxc(r,rhorr_up,rhorr_dn,enxc,vxc_up,vxc_dn,dirac,&
     &                                               output_in_Ha_units)
      use mecca_run, only : getMS
      use universal_const, only : ry2H
      use xc_mecca, only : xc_mecca_pointer,xc_alpha,gXCmecca,xc_is_gga
!      use mecca_constants, only : XC_LDA_HEDIN,XC_LDA_VOSKO,XC_GGA_AM05
      use xc_mecca, only : LDA_XC_GDSMFB
      use xc_mecca, only : ID_VOSKO,ID_relVOSKO
!DELETE after resolving convergence issue for GGA functionals
!      use xc_mecca, only : gInfo_libxc,ID_VOSKO
!DELETE after resolving convergence issue for GGA functionals
      implicit none
!
!!INPUT ARGUMENTS:
      real(8), intent(in) :: r(:)
      real(8), intent(in) :: rhorr_up(:)   !  4*pi*r^2*rho_up
!  if nspin==1 then rhorr_dn and vxc_dn are ignored, rhorr_up is considered as rhorr_total
      real(8), intent(in) :: rhorr_dn(:)   !  4*pi*r^2*rho_dn
      logical, intent(in) :: dirac         ! if .true. -> relativistic
      logical, intent(in), optional :: output_in_Ha_units
!!OUTPUT ARGUMENTS:
      real(8), intent(out) :: enxc(:)
      real(8), intent(out) :: vxc_up(:)
      real(8), intent(out) :: vxc_dn(:)
!!REMARKS:
! for non-magnetic case:
!  size(rhorr_up) must be .NE. size(rhorr_dn),
!  sizes of up-arrays must be .GE. size(r),
!  output vxc_dn is considered as undefined;
! for magnetic case sizes of all arrays must be .GE. size(r)
!EOP
!
!BOC
      type(xc_mecca_pointer) :: libxc_p
      integer :: i,n,nspin
      integer :: xc_id        !!,v1,v1m,v1mm

      xc_id = gXCid()
      n = size(r)
      if ( size(rhorr_up) == size(rhorr_dn) ) then
        nspin = 2
      else
        nspin = 1
      end if
      if ( xc_id == 0 ) then
       if ( dirac ) then
        xc_id = ID_relVOSKO
       else
        xc_id = ID_VOSKO
       end if
!!      xc_id = LDA_XC_GDSMFB  ! libxc #577: there is a bug in libxc 4.*.*
!!                                          TO DO: use libxc 5.*.* (only for KSDT and GDSMFB)
!!       call xc_f90_version(v1, v1m, v1mm)
!!       if ( v1<4 ) xc_id = ID_VOSKO
      end if
      call gXCmecca(xc_id,libxc_p)
!      if ( xc_id == 0 ) libxc_p%iexch = XC_LDA_VOSKO  ! mecca VWN
!
      call xc_alpha(nspin,n,r,rhorr_up,rhorr_dn,                        &
     &                                 enxc,vxc_up,vxc_dn,libxc_p)
      enxc(n+1:) = enxc(n)
      vxc_up(n+1:) = vxc_up(n)
      if ( nspin==2 ) vxc_dn(n+1:) = vxc_dn(n)
      call gXCmecca(-1,libxc_p)

      if ( present(output_in_Ha_units) ) then
       enxc = enxc*ry2H
       vxc_up = vxc_up*ry2H
       if ( nspin==2 ) vxc_dn = vxc_dn*ry2H
      end if

      return

      contains
!BOP
!!IROUTINE: gXCid
!!INTERFACE:
      integer function gXCid()
!
!EOP
!!USES:
      use mecca_types, only : RunState
      use mecca_run, only : getMS
!
!BOC
      implicit none
!      integer, intent(out), optional :: nspin
      type(RunState), pointer :: M_S
      M_S => getMS()
      if ( associated(getMS()) ) then
       gXCid = M_S%intfile%iXC
      else
       gXCid = 0
      end if
      return
!EOC
      end function gXCid
!
      end subroutine ext_libxc
