!BOP
!!ROUTINE: tcpa2all
!!INTERFACE:
      subroutine tcpa2all(natom,nbasis,iorig,itype,numbsub,if0,nop,     &
     &                    kkrsz,ndkkr,dop,tcpa,tcpaineq,iprint)
!!DESCRIPTION:
!  constructs tcpa-matrix for all sites
!  (tcpa-matrices for equivalent sites can differ!) \\
!
! {\tt natom}  -- total number of sites in the cell \\
! {\tt nbasis} -- number of sublattices             \\
! {\tt numbsub}(nsub) -- number of sites for sublattice "nsub" \\
! {\tt itype}(isite) -- type of sublattice for "isite" \\
! {\tt iorig}(nsub) -- number of site, which is "origin" for sublattice "nsub" \\

!!DO_NOT_PRINT
      implicit none
!!DO_NOT_PRINT

!!ARGUMENTS:
      integer natom,nbasis,iorig(nbasis),itype(*),numbsub(nbasis)
      integer nop,if0(48,*),kkrsz,ndkkr
      complex*16 dop(kkrsz,kkrsz,nop)
      complex*16 tcpa(ndkkr,ndkkr,nbasis)
      complex*16 tcpaineq(ndkkr,ndkkr,*)
      integer iprint
!!REVISION HISTORY:
! Initial Version - A.S. - 1996
!EOP
!
!BOC
      complex*16 w2(ndkkr,ndkkr)

      integer nsub,isite0,isite,iatsub,isite1,ir,n1,n2,j
      complex*16 tautmp,taun1n2
      character sname*10
      parameter (sname='tcpa2all')

      if(nbasis.eq.natom .or. nop.eq.1) then
       do isite=1,natom
        call zcopy(ndkkr*ndkkr,tcpa(1,1,itype(isite)),1,                &
     &                         tcpaineq(1,1,isite),1)
       end do
      else
       do nsub=1,nbasis
        isite0 = iorig(nsub)

        do iatsub=1,numbsub(nsub)
         if(iatsub.eq.1) then
          isite = isite0
         else
          do isite=isite1,natom
           if(itype(isite).eq.nsub) go to 20
          end do
          call fstop(sname//':: you have wrong NUMBSUB ?')
         end if
20       isite1 = isite+1

         do ir=1,nop
          if(if0(ir,isite0).eq.isite) go to 30
         end do
         if(iprint.ge.1) then
          write(6,1020) isite,nsub,isite0
1020      format(' You try to find <tcpa> for ISITE=',i5,               &
     &           ' (nsub=',i5,' isite0=',i5,'), but'/                   &
     &           ' atoms are not equivalent?!')
!     &' you have wrong IF0 or Atoms are not equivalent by symmetry?')
          if(iprint.eq.1000) call fstop(sname//' :: wrong symmetry?')
         end if
         ir = 1
30       continue

         call zerooutC(w2,ndkkr*ndkkr)
         do n2=1,kkrsz
          do n1=1,kkrsz
           taun1n2 = tcpa(n1,n2,nsub)

          do j=1,kkrsz
           tautmp = conjg( dop(j,n1,ir) )*taun1n2
           call zaxpy(kkrsz,tautmp,dop(1,n2,ir),1,w2(j,1),ndkkr)
          enddo

!CCC           do j=1,kkrsz
!CCC            tautmp = conjg(dop(j,n1,ir))*taun1n2
!CCC            do i=1,kkrsz
!CCC             w2(j,i) = w2(j,i) + tautmp*dop(i,n2,ir)
!CCC            enddo
!CCC           enddo

          enddo
         enddo

!c        do i=1,kkrsz
!c         tcpaineq(1:kkrsz,i,isite) = w2(1:kkrsz,i)
!c        enddo
         call zcopy(ndkkr*ndkkr,w2,1,tcpaineq(1,1,isite),1)

        end do

       end do

      end if

      return
      end subroutine tcpa2all

!BOP
!!ROUTINE: symmrot
!!INTERFACE:
      subroutine symmrot(ngrp,kptgrp,                                   &
     &                   kptset,kptindx,                                &
     &                   lwght,lrot,                                    &
     &                   tcpa,ndkkr,kkrsz,                              &
     &                   if0,dop,nbasis,                                &
     &                   iorig)
!!DESCRIPTION:
! applies symmetry operations {\tt dop} to t-matrix {\tt tcpa}
!

!!DO_NOT_PRINT
      implicit none
!!DO_NOT_PRINT

!!ARGUMENTS:
      integer    ngrp,kptgrp(*)
      integer    kptset(*),kptindx(*)
      integer    lwght(*),lrot(*)
      integer    ndkkr,kkrsz
      integer    nbasis
      complex*16 tcpa(ndkkr,ndkkr,nbasis)     ! input/output
      integer    if0(48,*)
      complex*16 dop(kkrsz,kkrsz,*)
      integer    iorig(*)
!!REVISION HISTORY:
! Initial Version - A.S. - 2013
!!REMARKS:
!EOP
!
!BOC
      character    sname*10
!      integer, parameter :: lkkrmax=(iplmax+1)*(iplmax+1)
      complex*16 ztmp0(kkrsz,kkrsz)
      complex*16 ztmp(kkrsz,kkrsz)
      complex*16 ztmp1(kkrsz,kkrsz)

      complex*16 zfac
      real*8  anrot
      integer nsub,nirot,nrot,igrp,nrsum

      integer I,NMX
      parameter (NMX=15)
      real*8 toldlt,dlt,dlt0
      parameter (toldlt=1.d-13)

!c     ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
      parameter    (sname='symmrot')

!      lkkrmax = kkrsz
      do nsub=1,nbasis
!c
       ztmp0(1:kkrsz,1:kkrsz) = tcpa(1:kkrsz,1:kkrsz,nsub)

       dlt0 = abs(ztmp0(1,1))

       DO I=1,NMX

        ztmp = 0
        anrot = 0.d0
        do igrp=1,ngrp
         nirot = kptset(igrp)
         nrot = lwght(kptindx(kptgrp(igrp)))
         call avgrot(nsub,ztmp0,kkrsz,kkrsz,                            &
     &              if0(1,iorig(nsub)),                                 &
     &              nrot,lrot(nirot),dop,                               &
     &              iorig,ztmp,nrsum)
         anrot = anrot+nrsum
        enddo

        zfac = dcmplx(1.d0/max(anrot,1.d0))
        ztmp1 = ztmp0
        ztmp0 = ztmp*zfac

        dlt = maxval(abs(ztmp1-ztmp0))

        if(dlt.lt.toldlt) EXIT

        if(dlt.gt.dlt0) then
         ztmp0 = ztmp1
         EXIT
        end if
        dlt0 = dlt

       END DO

       tcpa(1:kkrsz,1:kkrsz,nsub) = ztmp0(1:kkrsz,1:kkrsz)
!c
      enddo

      return

!EOC
      contains

!BOP
!!IROUTINE: avgrot
!!INTERFACE:
      subroutine avgrot(nsub,tcpa,ndkkr,kkrsz,if0,                      &
     &                   nrot,lrot,dop,                                 &
     &                   iorig,tsum,nrsum)
!!DESCRIPTION:
! given integral of tau00 over irreducible part of Brillouin Zone
! calculates the full integral by summing over rotations
! {\bv
! Tau_rot = U * Tau * U^(-1), U -- rotation matrix for matrices,
!                   U = conjg[ D ], D -- rotation matrix for Ylm,
!                   and, BTW, D^(-1)_LM = conjg[ D_ML]
!
! Therefore
!    Tau_rot = conjg[ D ] * Tau * transposed [ D ]
!
!    Tsum = Tsum+Sum{1:nrsum|Tau_rot}
! \ev}
!
!!REMARKS:
! private procedure of subroutine symmrot
!EOP
!
!BOC
      implicit none
!c
      character    sname*10
!c
!c     ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
      integer      nsub,ndkkr,kkrsz
      complex*16   tcpa(ndkkr,kkrsz)
      integer      if0(48)
      integer      nrot,lrot(nrot)
      complex*16   dop(kkrsz,kkrsz,*)
      integer      iorig(*),nrsum
      complex*16   tsum(ndkkr,kkrsz)

      complex*16   taun1n2
      complex*16   tautmp,czero
      parameter    (czero=(0.d0,0.d0))

      integer      nzzz
      complex*16   zzztau(kkrsz,kkrsz)

!c     ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
      parameter    (sname='avgrot')

      integer j,n1,n2,ir,irrot,jsite
      logical laddinv
       nzzz = kkrsz*kkrsz
       laddinv = .false.
       do ir=1,nrot
        if(lrot(ir).lt.0) then
         laddinv = .true.
         exit
        end if
       end do

       call zerooutC(zzztau,nzzz)

       nrsum = 0

       do ir=1,nrot
        irrot = lrot(ir)
        jsite = if0(abs(irrot))

        if(iorig(nsub).eq.jsite) then

         if(irrot.gt.0) then

          nrsum = nrsum+1

          do n2=1,kkrsz
           do n1=1,kkrsz
           taun1n2 = tcpa(n1,n2)

            do j=1,kkrsz
             tautmp = conjg( dop(j,n1,irrot) ) * taun1n2

             call zaxpy(kkrsz,tautmp,dop(1,n2,irrot),1,                 &
     &                               zzztau(j,1),kkrsz)

            enddo
           enddo
          enddo

         end if

        end if

       enddo

       if(nrsum.gt.0) then

         call zgeadd(tsum(1,1),ndkkr,'N',                               &
     &              zzztau,kkrsz,'N',                                   &
     &              tsum(1,1),ndkkr,kkrsz,kkrsz)


        if(laddinv) then
         nrsum = nrsum + nrsum
         call zgeadd(tsum(1,1),ndkkr,'N',                               &
     &              zzztau,kkrsz,'T',                                   &
     &              tsum(1,1),ndkkr,kkrsz,kkrsz)
        end if

       else

        nrsum = 1
        call zgeadd(tsum(1,1),ndkkr,'N',                                &
     &              tcpa(1,1),ndkkr,'N',                                &
     &              tsum(1,1),ndkkr,kkrsz,kkrsz)

       end if
!c
      return
!c
!EOC
      end subroutine avgrot

      end subroutine symmrot
