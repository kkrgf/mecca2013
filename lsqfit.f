!BOP
!!ROUTINE: lsqfit
!!INTERFACE:
      subroutine lsqfit(x,y,sig,ndata,a,ma,covar,npc,design)
!!DESCRIPTION:
! the least-squares minimization
!
!!REVISION HISTORY:
! Initial Version - A.S. - 2007
!!REMARKS:
! based on Numerical Recipes 2nd Edition, Section 15.4
!EOP
!
!BOC
      implicit none
      integer ma, npc, ndata, indx(ma)
      real*8 a(ma), covar(npc,npc), x(ndata), y(ndata), sig(ndata)
      integer i, l, m
      real*8 design(npc,ndata)
      real*8 siginv
      external ludcmp, lubksb
      do 12 l=1,ma
        do 11 m=1,ma
          covar(l,m)=0.d0
11      continue
        a(l)=0.d0
12    continue
      do 15 i=1,ndata
        siginv = 1.d0 / sig(i)
        do 14 l=1,ma
          do 13 m=1,l
            covar(l,m) = covar(l,m) + design(l,i) * design(m,i)
13        continue
          a(l) = a(l) + y(i) * siginv * design(l,i)
14      continue
15    continue
      do 17 l=2,ma
        do 16 m=1,l-1
          covar(m,l)=covar(l,m)
16      continue
17    continue
      call ludcmp(covar,ma,npc,indx,ndata)
      call lubksb(covar,ma,npc,indx,a)
      return
!EOC
      end subroutine lsqfit

!c=======================================================================
!BOP
!!ROUTINE: ludcmp
!!INTERFACE:
      subroutine ludcmp(a,n,np,indx,nmax)
!!DESCRIPTION:
! LU-decomposition, part I

!!REVISION HISTORY:
! Initial Version - A.S. - 2007
!!REMARKS:
! based on Numerical Recipes 2nd Edition, Section 2.3
!EOP
!
!BOC
      implicit none
      integer n, np, indx(n), nmax
      real*8 a(np,np), tiny
      parameter (tiny=1.0d-36)
      integer i, imax, j, k
      real*8 aamax, dum, sum, vv(nmax)
      do 12 i=1,n
        aamax=0.d0
        do 11 j=1,n
          if (abs(a(i,j)).gt.aamax) aamax=abs(a(i,j))
11      continue
        vv(i)=1.d0/aamax
12    continue
      do 19 j=1,n
        do 14 i=1,j-1
          sum=a(i,j)
          do 13 k=1,i-1
            sum=sum-a(i,k)*a(k,j)
13        continue
          a(i,j)=sum
14      continue
        aamax=0.d0
        do 16 i=j,n
          sum=a(i,j)
          do 15 k=1,j-1
            sum=sum-a(i,k)*a(k,j)
15        continue
          a(i,j) = sum
          dum=vv(i)*abs(sum)
          if (dum.ge.aamax) then
            imax=i
            aamax=dum
          endif
16      continue
        if (j.ne.imax)then
          do 17 k=1,n
            dum=a(imax,k)
            a(imax,k)=a(j,k)
            a(j,k)=dum
17        continue
          vv(imax)=vv(j)
        endif
        indx(j)=imax
        if(a(j,j).eq.0.d0)a(j,j)=tiny
        if(j.ne.n)then
          dum=1.d0/a(j,j)
          do 18 i=j+1,n
            a(i,j)=a(i,j)*dum
18        continue
        endif
19    continue
      return
!EOC
      end subroutine ludcmp
!
!BOP
!!ROUTINE: lubksb
!!INTERFACE:
      subroutine lubksb(a,n,np,indx,b)
!!DESCRIPTION:
! LU-decomposition, part II
!
!!REVISION HISTORY:
! Initial Version - A.S. - 2007
!!REMARKS:
! based on Numerical Recipes 2nd Edition, Section 2.3
!EOP
!
!BOC
      implicit none
      integer n, np, indx(n)
      real*8 a(np,np), b(n)
      integer i, ii, j, ll
      real*8 sum
      ii=0
      do 12 i=1,n
        ll=indx(i)
        sum=b(ll)
        b(ll)=b(i)
        if (ii.ne.0)then
          do 11 j=ii,i-1
            sum=sum-a(i,j)*b(j)
11        continue
        else if (sum.ne.0.d0) then
          ii=i
        endif
        b(i)=sum
12    continue
      do 14 i=n,1,-1
        sum=b(i)
        do 13 j=i+1,n
          sum=sum-a(i,j)*b(j)
13      continue
        b(i)=sum/a(i,i)
14    continue
      return
      end subroutine lubksb
!c
!c=======================================================================
!c=======================================================================
!BOP
!!ROUTINE: fchbev
!!INTERFACE:
      subroutine fchbev(a,b,x,sig,ndata,pl,design,nl,nmax)
!!DESCRIPTION:
! generates Chebyshev polynomials at postition, {\tt x}.
! {\tt design} is the design-matrix of the fitting problem.
!
!!REVISION HISTORY:
! Initial Version - A.S. - 2007
!!REMARKS:
! based on Numerical Recipes 2nd Edition, Section 15.4
!EOP
!
!BOC
      implicit none
      integer ndata
      integer nmax
      integer nl
      real*8 a, b, y, pl(nmax,ndata), design(nmax,ndata)
      real*8 x(ndata), sig(ndata), siginv
      integer i, j
      real*8 twoy
      do 12 i = 1, ndata
         siginv      = 1.d0 / sig(i)
         y           = (2.d0*x(i) - a - b) / (b - a)
         pl(1,i)     = 1.d0
         pl(2,i)     = y
         design(2,i) = pl(2,i) * siginv
         twoy=2.d0*y
         do 11 j=3,nl
            pl(j,i)     = twoy*pl(j-1,i) - pl(j-2,i)
            design(j,i) = pl(j,i) * siginv
11       continue
         pl(1,i)     = 0.5d0
         design(1,i) = pl(1,i) * siginv
12    continue
      return
!EOC
      end subroutine fchbev
