!BOP
!!ROUTINE: vectprd
!!INTERFACE:
      pure subroutine vectprd(a,b,c)
!!DESCRIPTION:
! vector product c = [a,b]
!
!!REVISION HISTORY:
! Initial Version - A.S. - 2001
!EOP
!
!BOC
      implicit none
      real(8), intent(in) :: a(3),b(3)
      real(8), intent(out) :: c(3)
      c(1) = a(2)*b(3) - b(2)*a(3)
      c(2) = a(3)*b(1) - b(3)*a(1)
      c(3) = a(1)*b(2) - b(1)*a(2)
      return
!EOC
      end subroutine vectprd

!BOP
!!ROUTINE: cellvolm
!!INTERFACE:
      function cellvolm(rslatt,kslatt,iflag)
!!DESCRIPTION:
! calculates volume of the cell defined by vectors {\tt rslatt}
! and (if {\tt iflag.ne.0}) reciprocal lattice vectors {\tt kslatt}

!!DO_NOT_PRINT
      implicit none
      real(8) :: cellvolm
!!DO_NOT_PRINT

!!ARGUMENTS:
      real(8), intent(in) :: rslatt(3,3)
      real(8) :: kslatt(*)
      integer, intent(in) :: iflag
!!REVISION HISTORY:
! Initial Version - A.S. - 2001
!EOP
!
!BOC
      cellvolm =                                                        &
     &    ( rslatt(2,1)*rslatt(3,2) - rslatt(3,1)*rslatt(2,2) )         &
     &         * rslatt(1,3) +                                          &
     &    ( rslatt(3,1)*rslatt(1,2) - rslatt(1,1)*rslatt(3,2) )         &
     &         * rslatt(2,3) +                                          &
     &    ( rslatt(1,1)*rslatt(2,2) - rslatt(2,1)*rslatt(1,2) )         &
     &         * rslatt(3,3)

       if ( iflag.ne.0 ) then
         call vectprd(rslatt(:,2),rslatt(:,3),kslatt(1:3))
         call vectprd(rslatt(:,3),rslatt(:,1),kslatt(4:6))
         call vectprd(rslatt(:,1),rslatt(:,2),kslatt(7:9))
         kslatt(1:9) = kslatt(1:9)/cellvolm
       end if

      return
      end function cellvolm

!BOP
!!ROUTINE: CalcVol
!!INTERFACE:
       subroutine  CalcVol(n,points,dV)
!!DESCRIPTION:
! calculates volume, {\tt dV}, of polyhedron defined by
! its {\tt n} vertices, {\tt points(1:3,1:n)}
!
!!USES:
       use mecca_interface
!
!!REVISION HISTORY:
! Initial Version - A.S. - 2001
!EOP
!
!BOC
       implicit none
       integer n
       real*8 points(3,n)
       real*8 dV

       real*8 rs(3,3)
       real*8 vi,tmp(1)
       integer i

       dV = 0.d0

       rs(1:3,1) = points(1:3,1)

       do i=2,n-1
      rs(1:3,2) = points(1:3,i)
      rs(1:3,3) = points(1:3,i+1)
      vi = cellvolm(rs,tmp,0)
      dV = dV + abs(vi)
       end do
       dV = dV/6.d0
       return
!EOC
       end subroutine  CalcVol

!BOP
!!ROUTINE: pnts2plane
!!INTERFACE:
       subroutine pnts2plane(P1,P2,P3,A,d)
!!DESCRIPTION:
! finds coefficients {\tt A,d} of equation of plane
!   A(1:3)*R(1:3) + d = 0, \\
! defined by three (non collinear) points, {\tt P1,P1,P3}
!
!!REVISION HISTORY:
! Initial Version - A.S. - 2001
!EOP
!
!BOC
       implicit none
       real*8 P1(3),P2(3),P3(3)
       real*8 A(3),d
       real*8 dy21,dy31,dz31,dz21,dx31,dx21

       dy31 = P3(2) - P1(2)
       dy21 = P2(2) - P1(2)
       dz31 = P3(3) - P1(3)
       dz21 = P2(3) - P1(3)
       dx31 = P3(1) - P1(1)
       dx21 = P2(1) - P1(1)

       A(1) = dy21*dz31 - dz21*dy31
       A(2) = dz21*dx31 - dx21*dz31
       A(3) = dx21*dy31 - dy21*dx31

       d = sqrt(dot_product(A(1:3),A(1:3)))
       A = A / d
       d = -dot_product(P2(1:3),A(1:3))
       return
!EOC
       end subroutine pnts2plane
