!BOP
!!MODULE: mpi
!!INTERFACE:
       module mpi
!!DESCRIPTION:
! to provide dummy mpi module, i.e. replacement 
! for mpi module if there are no mpi libraries
!
!!PUBLIC MEMBER FUNCTIONS:
       public mpi_abort
       public mpi_init
       public mpi_finalize
       public mpi_initialized
       public mpi_barrier
       public mpi_comm_create
       public mpi_comm_free
       public mpi_comm_group
       public mpi_comm_rank
       public mpi_comm_size
       public mpi_group_free
       public mpi_wtime

!!PUBLIC DATA MEMBERS:
       integer :: MPI_IN_PLACE=0
       integer, parameter :: MPI_COMM_WORLD=0
       integer, parameter :: MPI_GROUP_EMPTY=1
       integer, parameter :: MPI_SUM=3
       integer, parameter :: MPI_COMPLEX            = 18
       integer, parameter :: MPI_COMPLEX8           = 19
       integer, parameter :: MPI_COMPLEX16          = 20
       integer, parameter :: MPI_DOUBLE_COMPLEX     = 22
       integer, parameter :: MPI_REAL               = 13
       integer, parameter :: MPI_REAL4              = 14
       integer, parameter :: MPI_REAL8              = 15
       integer, parameter :: MPI_DOUBLE_PRECISION   = 17
!for C/C++ only       integer, parameter :: MPI_DOUBLE             = 46
       integer, parameter :: MPI_INTEGER            =  7
       integer, parameter :: MPI_LOGICAL            =  6
       integer, parameter :: MPI_LOGICAL1           = 29 
       integer, parameter :: MPI_STATUS_SIZE        = 1
       integer, parameter :: MPI_ANY_SOURCE         = -1
       integer, parameter :: MPI_ANY_TAG            = -1
       integer :: MPI_STATUS_IGNORE(MPI_STATUS_SIZE)
!!REVISION HISTORY:
! Initial Version - A.S. - 2014
!!REMARKS:
! functions mpi_allreduce, mpi_bcast and mpi_group_incl 
! are defined outside the module
!EOP
!
!BOC
!       external mpi_allreduce
!       external mpi_bcast
!       external mpi_group_incl
!EOC
       CONTAINS

       subroutine mpi_init(ierr)
       integer ierr
       ierr=0
       return
       end subroutine mpi_init

       subroutine mpi_initialized(flag,ierr)
       logical flag
       integer ierr
       flag = .false.
       ierr=0
       return
       end subroutine mpi_initialized

       subroutine mpi_finalize(ierr)
       integer ierr
       ierr=0
       return
       end subroutine mpi_finalize

       subroutine mpi_abort(comm,errcom,ierr)
       integer comm,errcom,ierr
       ierr=0
       return
       end subroutine mpi_abort

       subroutine mpi_barrier(myworld,ierr)
       integer myworld,ierr
       ierr=0
       return
       end subroutine mpi_barrier

       subroutine mpi_comm_create(comm,grp,comm1,ierr)
       integer comm,grp,comm1,ierr
       ierr=0
       return
       end subroutine mpi_comm_create

       subroutine mpi_comm_free(comm,ierr)
       integer comm,ierr
       ierr=0
       return
       end subroutine mpi_comm_free

       subroutine mpi_comm_group(comm,group,ierr)
       integer comm,group,ierr
       ierr=0
       return
       end subroutine mpi_comm_group

       subroutine mpi_comm_rank(myworld, myrank, ierr)
       integer myworld,myrank,ierr
       myrank = 0
       ierr = 0
       return
       end subroutine mpi_comm_rank

       subroutine mpi_comm_size(myworld, np, ierr)
       integer myworld,np,ierr
       np = 1
       ierr = 0
       return
       end subroutine mpi_comm_size

       subroutine mpi_group_free(group,ierr)
       integer group,ierr
       ierr = 0
       return
       end subroutine mpi_group_free

       double precision function mpi_wtime()
       real(8) :: time
       call cpu_time(time)
       mpi_wtime = time
       end function mpi_wtime

       end module mpi
!
!BOP
!!ROUTINE: mpi_allreduce 
!!INTERFACE:
       subroutine mpi_allreduce(isend,irecv,cnt,dtype,op,comm,ierr)
!!DESCRIPTION:
! dummy mpi\_allreduce subroutine
!
!!REVISION HISTORY:
! Initial Version - A.S. - 2014
!EOP
!
!BOC       
       integer isend(*),irecv(*)
       integer cnt,dtype,op,comm,ierr
       ierr=0
       return
!EOC       
       end subroutine mpi_allreduce

!BOP
!!ROUTINE: mpi_bcast
!!INTERFACE:
       subroutine mpi_bcast(buff,cnt,dtype,root,comm,ierr)
!!DESCRIPTION:
! dummy mpi\_bcast subroutine
!
!!REVISION HISTORY:
! Initial Version - A.S. - 2014
!EOP
!
!BOC       
       integer :: buff
       integer cnt,dtype,root,comm,ierr
       ierr=0
       return
!EOC       
       end subroutine mpi_bcast

!BOP
!!ROUTINE: mpi_recv
!!INTERFACE:
       subroutine mpi_recv(buff,cnt,dtype,src,tag,comm,stats,ierr)
!!DESCRIPTION:
! dummy mpi\_recv subroutine
!
!!REVISION HISTORY:
! Initial Version - A.S. - 2014
!EOP
!
!BOC       
       integer :: buff(*)
       integer cnt,dtype,src,tag,comm,stats(*),ierr
       ierr=0
       return
!EOC       
       end subroutine mpi_recv

!BOP
!!ROUTINE: mpi_send
!!INTERFACE:
       subroutine mpi_send(buff,cnt,dtype,src,tag,comm,ierr)
!!DESCRIPTION:
! dummy mpi\_send subroutine
!
!!REVISION HISTORY:
! Initial Version - A.S. - 2014
!EOP
!
!BOC       
       integer :: buff(*)
       integer cnt,dtype,src,tag,comm,ierr
       ierr=0
       return
!EOC       
       end subroutine mpi_send

!BOP
!!ROUTINE: mpi_isend
!!INTERFACE:
       subroutine mpi_isend(buff,cnt,dtype,src,tag,comm,req,ierr)
!!DESCRIPTION:
! dummy mpi\_isend subroutine
!
!!REVISION HISTORY:
! Initial Version - A.S. - 2016
!EOP
!
!BOC
       integer :: buff(*)
       integer cnt,dtype,src,tag,comm,req,ierr
       ierr=0
       return
!EOC
       end subroutine mpi_isend

!BOP
!!ROUTINE: mpi_group_incl 
!!INTERFACE:
       subroutine mpi_group_incl(group,n,ranks,grpout,ierr)
!!DESCRIPTION:
! dummy mpi\_group\_incl subroutine
!
!!REVISION HISTORY:
! Initial Version - A.S. - 2014
!EOP
!
!BOC       
       integer group,n,ranks,grpout,ierr
       ierr = 0
       return
!EOC       
       end subroutine mpi_group_incl

!BOP
!!ROUTINE: mpi_wait
!!INTERFACE:
       subroutine mpi_wait(req,status,ierr)
!!DESCRIPTION:
! dummy mpi\_wait subroutine
!
!!REVISION HISTORY:
! Initial Version - A.S. - 2016
!EOP
!
!BOC
       integer req,status(*),ierr
       ierr=0
       return
!EOC
       end subroutine mpi_wait

