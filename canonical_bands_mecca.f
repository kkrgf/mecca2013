!BOP
!!IROUTINE: calcCore
!!INTERFACE:
      subroutine calcCanonicalBands( mecca_state )
!!DESCRIPTION:
! TO DO

!!USES:
      use canonical_bands_module
      use mecca_types
      use mpi

!!DO_NOT_PRINT
      implicit none
!!DO_NOT_PRINT

!!ARGUMENTS:
      type(RunState), target :: mecca_state
!!PARAMETERS:
!!REVISION HISTORY:
! Initial Version - M.D. - 2020
!!REMARKS:
!EOP
!
!BOC
!
      type(IniFile),         pointer :: inFile
      type(RS_Str),          pointer :: rs_box
      type(Sublattice),      pointer :: sublat
      type(SphAtomDeps),     pointer :: v_rho_box
      type(canonical_bands_t)        :: canonical_bands
      type(canonical_bands_result_t) :: results
      real(8), allocatable :: rr(:)
      real(8) :: Rscat,Rasa,hx,ebot_est_bandwidth
      integer :: nsub,ic,is,i
      integer :: nbasis, nspin, npmax
      integer           :: printunit
      integer           :: rank,ierror
      character(len=32) :: gpfile
      real(8),parameter :: mingap                 =   0.6D0
      real(8),parameter :: ebotmax                =  -0.15D0
      real(8),parameter :: maxbandwidth           =   0.05D0
      real(8),parameter :: speed_of_light_scaling =   1.0D0

      call MPI_Comm_rank(MPI_COMM_WORLD, rank, ierror)
      if(rank.eq.0) then
          gpfile="canonical_bands_results.txt"
      else
          gpfile = ""
      endif



      rs_box => mecca_state%rs_box
      inFile => mecca_state%intfile

      nbasis = inFile%nsubl 
      nspin = inFile%nspin


      printunit = 6
      call canonical_bands%set_print_unit(printunit)

      npmax=0
      do nsub=1,nbasis
        sublat => inFile%sublat(nsub)
        do ic=1,sublat%ncomp
            v_rho_box => sublat%compon(ic)%v_rho_box
            npmax=max(v_rho_box%ndrpts,npmax)
        enddo ! ic
      enddo ! nsub
      
      allocate(rr(npmax))

      ebot_est_bandwidth = ebotmax

      do nsub=1,nbasis
         sublat => inFile%sublat(nsub)
         do ic=1,sublat%ncomp
            write(printunit,'(a)') repeat("-",79)
            
            v_rho_box => sublat%compon(ic)%v_rho_box
            hx=(v_rho_box%xmt-v_rho_box%xst)/                           &
     &         real(v_rho_box%jmt-1,kind(0.0d0))
            do i=1,v_rho_box%ndrpts
                rr(i)=exp(v_rho_box%xst+real(i-1,kind(0.0d0))*hx)
            enddo ! i

            do is=1,nspin
                 write(printunit,'(a,i4,1x,a,i3,1x,a,i2,3x,a)')         &
     &             "canonical bands:  Sub-lattice =",nsub,              &
     &             "Component =",ic,                                    &
     &             "Spin =",is,                                         &
     &             trim(sublat%compon(ic)%name)
                call canonical_bands%init(                              &
     &                               np=v_rho_box%jmt,                  &
     &                               rv=v_rho_box%vr(:,is),             &
     &                                r=rr,                             &
     &           speed_of_light_scaling=speed_of_light_scaling)
                call canonical_bands%run(                               &
     &                                    kappa_max=0,                  &
     &                                   energy_max=ebotmax,            &
     &                                       mingap=mingap,             &
     &                                 maxbandwidth=maxbandwidth)
                ebot_est_bandwidth = min(ebot_est_bandwidth,            &
     &           canonical_bands%results%ebot_local_bandwidth_intervals)
                do i=1,canonical_bands%results%ndata
                    call results%add_interval                           &
     &                    (canonical_bands%results%interval(i),mingap)
                enddo ! i
            enddo ! is
         enddo ! ic

      enddo ! nsub
      deallocate(rr)

      !! ebot estimate that is in a gap region of combined intervals
      ebot_est_bandwidth = results%energy_in_intervals(                 &
     &                             ebot_est_bandwidth,ebotmax,mingap)

!!    write gnuplot file with intervals
      call results%intervals_to_gnuplot(gpfile)

      write(printunit,'("")')
      write(printunit,'(a)') repeat("-",39)
      write(printunit,'(6x,a,20x,a,3(a,f12.8,";",1x))')                 &
     &                              "canonical bands intervals",        &
     &                              "parameters: ",                     &
     &                              "mingap =",mingap,                  &
     &                              "emax =",ebotmax,                   &
     &                              "maxbandwidth =",maxbandwidth
      write(printunit,'(a)') repeat("-",39)
      call results%print(printunit)
      write(printunit,'("")')
      write(printunit,'(5x,a,f16.8)')                                   &
     &            "recommended ebot from width of canonical bands = ",  &
     &                                             ebot_est_bandwidth
      write(printunit,'("")')

      call results%clear
      call canonical_bands%clear
!
!EOC
      end subroutine calcCanonicalBands
