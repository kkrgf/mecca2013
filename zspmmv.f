      SUBROUTINE ZSPMMV (NROW,A,IA,JA,X,B)
!C
!C     Purpose:
!C     This subroutine computes B = A * X, for a sparse matrix A.
!C
!C     Parameters:
!C     NROW = the number of rows in the matrix (input).
!C     A    = the matrix elements (input).
!C     IA   = the array of row pointers (input).
!C     JA   = the array of column indices (input).
!C     X    = the vector to be multiplied by A (input).
!C     B    = the result of the multiplication (output).
!C
!C**********************************************************************
!C
      INTEGER JA(*), NROW, IA(NROW+1)
      DOUBLE COMPLEX A(*), B(NROW), X(NROW)
!CALAM      DOUBLE COMPLEX Z1(*)
!C
!C     Miscellaneous parameters.
!C
      DOUBLE COMPLEX ZZERO
      PARAMETER (ZZERO = (0.0D0,0.0D0))
      DOUBLE COMPLEX ZDOTUI
      EXTERNAL ZDOTUI
!C
!C     Local variables.
!C
!C      INTEGER I, J, K
      INTEGER I
      INTEGER I0,NK
!C
!C     Multiply by A.
!C
       DO 20 I = 1, NROW
        I0 = IA(I)
        NK = IA(I+1)-I0
        B(I) = ZDOTUI(NK,A(I0),JA(I0),X)
 20      CONTINUE
!C
      RETURN
      END
