!BOP
!!MODULE: eos_data
!!INTERFACE:
      module eos_data
!!DESCRIPTION:
! equation-of-state (EOS) fitting
! 
! available EOS types are {\tt MURNAGHAN, VINET}
!

!!DO_NOT_PRINT
      implicit none
      private
!!DO_NOT_PRINT

!!PUBLIC MEMBER FUNCTIONS:
      public :: EOS
      public :: murng1
      public :: vinet
      public :: eosid
!!PRIVATE MEMBER FUNCTIONS:
! subroutine deallocate_eos
! subroutine allocate_eos
! real*8 function FUNEOS
!!REVISION HISTORY:
! Initial Version - A.S. - 2013
!EOP
!
!BOC
      real*8, allocatable :: EDATA(:)   ! energy
      real*8, allocatable :: VDATA(:)   ! volume
      integer :: ITYPE = 1    ! EOS type
      integer :: NNN = 0      ! data size
      character(9), parameter, dimension(3) ::                          &
     &     eosid=[ character(9) :: "POLYNM2","MURNAGHAN","VINET" ]
      real*8, parameter :: ergToEv = 1.60217662d0
!C     1 ELECTRONVOLT = 1.60217662 D-12 ERG

!EOC
      CONTAINS

!BOP
!!IROUTINE: deallocate_eos
!!INTERFACE:
      subroutine deallocate_eos()
!EOP
!
!BOC
      if ( allocated(edata) ) then
        deallocate(edata)
      end if
      if ( allocated(vdata) ) then
        deallocate(vdata)
      end if
      NNN = 0
!EOC
      end subroutine deallocate_eos

!BOP
!!IROUTINE: allocate_eos
!!INTERFACE:
      subroutine allocate_eos( n )
!EOP
!
!BOC
      integer n
      call deallocate_eos()
      if ( n>0 ) then
       allocate(edata(n))
       allocate(vdata(n))
       NNN = n
      end if 
!EOC
      end subroutine allocate_eos

!BOP
!!IROUTINE: EOS
!!INTERFACE:
      subroutine EOS(IFLAG,AUNIT,ENUNIT,n,vinp,einp                     &
     &,                   VOL0,E0,B0,B0PRIM,ERR)
!!DESCRIPTION:
! finds equilibrium volume VOL0 (in Ansgtroem$^3$) and respective
! E0 (energy in eV), 
! B0 (bulk modulus in Mbar) and 
! B0PRIM (pressure derivative of bulk modulus).
! Input data are assumed to be ordered by volume (vinp).

!!DO_NOT_PRINT
      implicit real*8 (a-h,o-z)
!!DO_NOT_PRINT

!!ARGUMENTS:
      integer, intent(in)  :: IFLAG
      real(8), intent(in)  :: aunit,enunit ! rescaling to A**3 and eV
      integer, intent(in)  :: n
      real(8), intent(in)  :: einp(n),vinp(n)
      real(8), intent(out) :: VOL0,E0,B0,B0PRIM
      real(8), intent(out), optional :: ERR
!!REVISION HISTORY:
! Adapted Version - A.S. - 2013
!EOP
!
!BOC
      real*8 X(100*n),AUX(100*n)
      real*8 B0MIN,B0MAX,B0PMIN,B0PMAX
      integer I,IE0,IE1,IE2,IERR,LIM,NMAX,NVAR
      real(8) :: v00,e00,volmin,volmax
      integer :: ierr_min
      real(8) :: err_min,X_out(4)
      integer, parameter :: ndigit=7

      call allocate_eos(n)

! ndigit transformation ensures a value with exactly "ndigit" digits (after ".")
! to provide compatibility with external routines relying on printed output
! in log-file

      EDATA(1:NNN) = dnint(einp(1:NNN)*10.d0**ndigit)/10.d0**ndigit
      VDATA(1:NNN) = dnint(vinp(1:NNN)*10.d0**ndigit)/10.d0**ndigit
      EDATA(1:NNN) = enunit*EDATA(1:NNN)            ! it should be in eV
      VDATA(1:NNN) = aunit**3*VDATA(1:NNN)          ! it should be in Angstroem^3

      ITYPE = IFLAG
      FFF  = 0.d0

!C
!C  STARTING VALUES FOR ITERATION:
!C
!C     MIMIMUM IN EDATA(I):
!C
      iE0 = 1
      E0=EDATA(iE0)
      VOL0=VDATA(iE0)
      DO I=2,NNN
        IF(EDATA(I) .lt. E0) THEN
         iE0 = I
         E0=EDATA(I)
         VOL0=VDATA(I)
        END IF
      END DO
      NVAR=min(4,NNN)
!C
!C     FOR OTHER VARIABLES, WE CHOOSE:
!c
      IERR = 0
      X(:) = 0.d0
      AUX(:) = 0.d0
      B0=5.D0
      B0PRIM=5.D0
!C
!C     A UNIFORM SHIFT OF ALL THE ENERGIES
!C     (WILL NOT INFLUENCE THE B0, B0PRIME, VOL0, ONLY SHIFTS E0):
!C
      volmin = minval(vdata(1:NNN))
      volmax = maxval(vdata(1:NNN))
      SHIFT=-E0-0.01d0
      DO 20 I=1,NNN
   20 EDATA(I)=EDATA(I)+SHIFT

      X(1) = VDATA(iE0)
      X(2) = EDATA(iE0)
      if (NNN.ge.3) then
        X(3)=B0
        X(4)=B0PRIM
        LIM = 200
        NMAX = NNN
        call plnm2eos(e00,v00,B0)
        if ( B0 <= 0 ) then
         v00 = X(1)
         e00 = X(2)
        else
         X(1) = v00
         X(2) = e00
         if ( ITYPE==1 ) then
          NMAX=0
          X(3) = B0*v00*ergToeV    !  in Mbar
          X(4) = 0.d0
         else
          X(1) = max(min(X(1),volmax),volmin)
          volmax = max(volmax,v00)
          volmin = min(volmin,v00)
         end if
        end if
!!!
!        write(6,'(a,4f12.5)') ' X(1:4)=',X(1:4)
!!!
!C
        ierr_min = 0
        err_min = 1.d10
        DO I = 1, NMAX
         IERR = 0
         AUX = 0
         if ( I>1 ) then
          X(1) = VDATA(I)
          X(2) = EDATA(I)
         end if
         CALL DFMND(FUNEOS,X,FFF,NVAR,LIM,AUX,IERR)
         if ( iE0 >1 .and. iE0<NNN ) then
          if ( X(1)<volmin .or. X(1)>volmax ) IERR = 17
         end if
         if ( FFF<err_min .and. (IERR==0 .or. IERR==1)) then
          err_min = FFF
          ierr_min = IERR
          X_out(1:4) = X(1:4)
         end if
         if ( I==NMAX ) exit
         if ( IERR==0 ) then
          if ( X(3)>0.d0 .and. X(1)>0.d0 ) exit
         end if
         X(4) = 4.d0
         if ( X(1)>=volmin .and. X(1)<=volmax ) then
          X(3) = 3.d0
         else
          if ( X(3)<0.d0 ) then
           X(3) = 1.d-3
          else
           X(3) = 3.d0
          end if
          if ( VDATA(1) <= VDATA(NNN) ) then
           if ( X(1)<volmin ) then
            X(1) = volmin
           else
            X(1) = volmax
           end if
          else
           if ( X(1)<volmin ) then
            X(1) = volmin
           else
            X(1) = volmax
           end if
          end if
         end if
        END DO

        if ( ITYPE.ne.1 ) then
         X(1:4) = X_out(1:4)
         FFF = err_min
         IERR = ierr_min
        end if

        if (IERR.NE.0.and.IERR.NE.1) then
         write(6,*) ' IERR = ', IERR, ' NMAX = ', NMAX
         write(6,*) ' IERR .ne. 0/1 after NMAX iterations'
         X(1) = v00
         X(2) = e00
        end if
      else
        X(1)=VOL0
        X(2)=E0+SHIFT
      end if
!C
      call deallocate_eos()   ! NNN=0 after deallocation

      E0    = (X(2)-SHIFT)/ENUNIT
      VOL0  = X(1)/AUNIT**3
      if ( IERR.ne.0 .and. IERR.ne.1 ) then
         B0 = 0.d0
         B0PRIM = 0.d0
      else
       if (NVAR.ge.3) then
        B0=X(3)
       else
        B0 = 1.d0
       end if
       if (NVAR.ge.4) then
        B0PRIM=X(4)
       else
        B0PRIM = 4.d0
       end if
      end if

      if ( present(ERR) ) ERR = FFF
!c
      return
!EOC
      END SUBROUTINE EOS
!BOP
!!IROUTINE: PLNM2EOS
!!INTERFACE:
      subroutine plnm2eos(E0,V0,B0)
!!DESCRIPTION:
! {\bv
! subroutine calculates parameters of EOS approximated by a second-order polynomial
! (only three points closest to minimum are used)
!
!   E(V) = E0 + B0/2*(V-V0)**2
! \ev}
!
!!REVISION HISTORY:
! Initial Version - A.S. - 2018
!EOP
!
!BOC
      implicit none
      real(8), intent(out) :: E0,V0,B0
      real(8) :: e1,e2,e3,v1,v2,v3,d12,d23
      real(8), parameter :: tol = 1.d-9
      integer :: imin(1)

      E0 = 0; V0 = 0; B0 = 0
      imin = minloc(EDATA(1:NNN))
      if ( imin(1)==1 ) then
       e1 = EDATA(1)
       v1 = VDATA(1)
       e2 = EDATA(2)
       v2 = VDATA(2)
       e3 = EDATA(3)
       v3 = VDATA(3)
      else if ( imin(1)==NNN ) then
       e1 = EDATA(NNN-2)
       v1 = VDATA(NNN-2)
       e2 = EDATA(NNN-1)
       v2 = VDATA(NNN-1)
       e3 = EDATA(NNN)
       v3 = VDATA(NNN)
      else
       e1 = EDATA(imin(1)-1)
       v1 = VDATA(imin(1)-1)
       e2 = EDATA(imin(1))
       v2 = VDATA(imin(1))
       e3 = EDATA(imin(1)+1)
       v3 = VDATA(imin(1)+1)
      end if

      d12 = (e2-e1)/(v2-v1)
      d23 = (e3-e2)/(v3-v2)
      if ( abs(d12-d23) < epsilon(d12) ) then
       write(6,'(a)') ' EOS is a straight line'
       return
      end if

      V0 = ( (v2+v3)/2*d12 - (v1+v2)/2*d23 ) / (d12 - d23)
      if ( V0<0 ) V0=v1

      B0 =(2*(e3-e1) / (  (v3-V0)**2 - (v1-V0)**2 )                     &
     &   + 2*(e1-e2) / (  (v1-V0)**2 - (v2-V0)**2 )                     &
     &   + 2*(e2-e3) / (  (v2-V0)**2 - (v3-V0)**2 ))/3
      E0 = e2 - B0/2*V0**2*(v2/V0-1)**2

      d12 = abs((E0-e1) + B0/2*V0**2*(v1/V0-1)**2)
      d23 = abs((E0-e3) + B0/2*V0**2*(v3/V0-1)**2)
      if ( d12>tol .or. d23>tol ) then
       write(6,'(a,2e14.4,a,e10.2)') ' discrepancies = ',d12,d23,       &
     &                              ' tolerance = ',tol
       write(6,'(a)') ' WARNING: inaccurate output in plnm2eos'
      end if

      RETURN
!EOC
      END SUBROUTINE plnm2eos

!BOP
!!IROUTINE: FUNEOS
!!INTERFACE:
      REAL*8 FUNCTION FUNEOS(X)
!!DESCRIPTION:
! {\bv
! FUNCTION TO BE MINIMIZED IN THE LEAST-SQUARES FIT (BY SUBROUTINE DFMND)
! FUNCTION CALCULATES THE SUM OF THE  A B S O L U T E  DEVIATIONS
!            (E(THEOR)-E(EXP))**2
! DIVIDED BY THE NUMBER OF EXP. POINTS,
! ASSUMING FOR EQUATION OF STATE THE MURNAGHAN OR VINET EXPRESSION.
!
! MEANING OF VARIABLES:
!      X(1) .... VOL0
!      X(2) .... E0
!      X(3) .... B0
!      X(4) .... B0PRIM
! \ev}
!
!!REVISION HISTORY:
! Adapted Version - A.S. - 2013
!EOP
!
!BOC
      IMPLICIT REAL*8 (A-H,O-Z)
      real*8 X(4),da,v_min
      integer i

      if ( X(1)<=0 ) return

      VOL0=X(1)
      E0=X(2)
      if (NNN.ge.3) then
       B0=X(3)
      else
       B0 = 1.d0
      end if
      if (NNN.ge.4) then
       B0PRIM=X(4)
      else
       B0PRIM = 4.d0
      end if
!C
      SM=0.D0
      w = 0.d0
!C     THE SUM OF SQUARES:
      DO 10 I=1,NNN
      VOLACT=VDATA(I)
      wi = 1.d0
      if ( itype .eq. 2 ) then
       if ( abs(B0PRIM)<1.d-3 ) then
        da = (VOLACT/VOL0) - 1.d0
        ETOT = E0 + B0 * da**2 * (1.d0 + B0PRIM * da)
       else
        if ( abs(B0PRIM-1.d0)<epsilon(1.d0) ) then
         B0PRIM=B0PRIM+2*epsilon(1.d0)
        end if
        CALL murng1(VOLACT,VOL0,B0,B0PRIM,E0,ETOT)
       end if
!       wi = 1.d0 / (1.d0 + max(0.d0,(abs(VOLACT/v_min-1.d0)-dv))**2)
      else if ( itype .eq. 3) then
       if ( abs(B0PRIM-1.d0)<epsilon(1.d0) ) then
         B0PRIM=B0PRIM+2*epsilon(1.d0)
       end if
       CALL vinet(VOLACT,VOL0,B0,B0PRIM,E0,ETOT)
      else if ( itype .eq. 1 ) then
!       da = (VOLACT/VOL0)**(dble(1)/dble(3)) - 1.d0
       da = (VOLACT/VOL0) - 1.d0
       ETOT = E0 + B0 * da**2 !!!! * (1.d0 + B0PRIM * da)
      else
       stop ' UNKNOWN EOS TYPE '
      end if
      ETOT = min(ETOT,1.d+6)
      SM=SM+(ETOT-EDATA(I))**2.d0
   10 CONTINUE
      FUNEOS = sqrt(SM/NNN)
      RETURN
!EOC
      END FUNCTION FUNEOS

!BOP
!!IROUTINE: murng1
!!INTERFACE:
      SUBROUTINE murng1(VOL,VOL0,B0,B0PRIM,E0,ETOT)
!!DESCRIPTION:
!
! EVALUATION OF THE MURNAGHAN EXPRESSION FOR ENERGY AS A FUNCTION
! OF VOLUME.
!
! {\bv
! INPUT DATA:
!
!      VOL ..... VOLUME, IN THE ANGSTROEMS CUBED.
!      VOL0 .... VOLUME AT THE ENERGY MINIMUM, IN THE SAME UNITS.
!      B0 ...... BULK MODULUS, IN UNITS MEGABAR.
!      B0PRIM .. PRESSURE DERIVATIVE OF THE BULK MODULUS.
!                SHOULD BE CLOSE NEITHER TO 0 NOR TO 1.
!      E0 ...... AN ADDITIVE CONSTANT (IN ELECTRONVOLTS), ADDED
!                TO THE ENERGY EXPRESSION.
!                (SEE,  PR B28, p 5484: Fu and Ho)
!
! OUTPUT DATA:
!
!      ETOT .... THE ENERGY, INCLUDING THE E0 CONSTANT, IN ELECTRONVOLTS
! \ev}
!
!!REVISION HISTORY:
! Initial Version - A.S. - 2013
!EOP
!
!BOC
      IMPLICIT REAL*8 (A-H,O-Z)
!C
!C     CONVERSION FACTOR FROM ERG TO ELECTRONVOLT:
!      real*8 ergToEv
!      PARAMETER( ergToEv = 1.60209D0 )
!C     1 ELECTRONVOLT = 1.60209 D-12 ERG

!C
      ETOT = E0 - B0*VOL0/B0PRIM *                                      &
     & (((VOL/VOL0)**(1.D0-B0PRIM)-1.D0)/(1.D0-B0PRIM)-VOL/VOL0+1.D0)   &
     & /ergToEv
!C
      RETURN
!EOC
      END SUBROUTINE murng1

!BOP
!!IROUTINE: vinet
!!INTERFACE:
       SUBROUTINE vinet(VOL,VOL0,k0,kp,E0,E)
!!DESCRIPTION:
! Vinet equation of state (Angstr.-eV)
! {\bv
! E = E0 + 9*k0*vol0/eta**2*{1-[1-eta*(1-x)]*exp(eta*(1-x))} ,
!  here eta = 3/2*(kp-1), x = (vol/vol0)**(1/3)
! \ev}
!
!!REVISION HISTORY:
! Initial Version - A.S. - 2013
!EOP
!
!BOC
      implicit none
      integer n
      real*8 VOL, VOL0, k0, kp, E0, E

      real*8 pi4d3
      real*8 x, xm1, kp1, v0, fex
      real*8 tmp,eta
      integer i
      real*8 third
      parameter (third = 1.d0/3.d0)
!      real*8 ergToEv
!      PARAMETER( ergToEv = 1.60209D0 )

       x = (VOL/VOL0)**third
       eta = 1.5d0*(kp-1.d0)
       fex = exp(eta*(1.d0-x))

       E = 9.d0*k0*VOL0/eta**2 * (1.d0-(1.d0-eta*(1.d0-x))*fex)
       E = e0 + E/ergToeV

!       xm1 = x-1.d0
!       kp1 = kp-1.d0
!       fex = exp(-1.5d0*kp1*xm1)
!       E = e0 + (2.d0*k0*VOL0)/kp1**2*                                  &
!     &              2*(1.d0+1.5d0*kp1*xm1)*fex                          &
!     &            /ergToEv
!!     &               (2.d0-(5.d0+kp*xm1-3d0*x)*fex)                     &

      return
!EOC
      end subroutine vinet

      end module eos_data
