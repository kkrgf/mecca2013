!BOP
!!ROUTINE: latvect
!!INTERFACE:
      subroutine latvect(r_bas,bases,aij,k_bas,nbasis,                  &
     &                     qmesh,ndimq,nmesh,nqpt,                      &
     &                     Rksp,xknlat,ndimk,nksn,                      &
     &                     Rrsp,xrnlat,ndimr,nrsn,                      &
     &                     iprint)
!!DESCRIPTION:
! sets up real and reciprocal space vectors, {\tt xrnlat} and {\tt xknlat}
!
!!USES:
      use mecca_interface, only : genvec

!!DO_NOT_PRINT
      implicit none
!!DO_NOT_PRINT

!!ARGUMENTS:
      integer, intent(in) ::  nbasis
      real(8), intent(in) ::  r_bas(3,3),bases(3,nbasis),k_bas(3,3)
      real(8), intent(out) :: aij(3,*)
      integer, intent(in) ::  nmesh,ndimq
      integer, intent(in) :: nqpt(nmesh)
      real(8), intent(in) :: qmesh(3,ndimq,*)
      real(8), intent(in) ::  Rksp,Rrsp
      integer, intent(out) :: nksn(nmesh),nrsn
      real(8), allocatable :: xknlat(:)    ! (ndimk*3*nmesh)
      real(8), allocatable :: xrnlat(:)    ! (ndimr*3)
      integer, intent(out) :: ndimk,ndimr
      integer, intent(in) :: iprint
!!REVISION HISTORY:
! Adapted - A.S. - 2013
!EOP
!
!BOC
      real(8), allocatable :: tmpvec(:),tmpxkn(:)
      integer :: i,j,icount,imesh,n1
      integer :: j0,k0,j1,k1,nj,nk
!      integer memsize
!c      real*8 aoc,r2ksn,r2rsn,r2aij,aijmax
      real*8 zero
      parameter (zero=0.d0)
!c
!c                   aij = b_j-b_i
!c
      nrsn = 0
      nksn = 0
      aij(1,1) = zero
      aij(2,1) = zero
      aij(3,1) = zero
      icount = 1
      do i=1,nbasis
       do j=i+1,nbasis
         icount = icount+1
         aij(1,icount) = bases(1,j) - bases(1,i)
         aij(2,icount) = bases(2,j) - bases(2,i)
         aij(3,icount) = bases(3,j) - bases(3,i)
         icount = icount+1
         aij(1,icount) = bases(1,i) - bases(1,j)
         aij(2,icount) = bases(2,i) - bases(2,j)
         aij(3,icount) = bases(3,i) - bases(3,j)
       enddo
      enddo

      if(iprint.ge.-10) then
       write(6,*)
       write(6,'('' Input R-space cut-off: '',d12.4)') Rrsp
       write(6,'('' Input K-space cut-off: '',d12.4)') Rksp
      end if

!      if ( .not. allocated(xrnlat) ) then
!        nrsn = 1
!        ndimr = 1
!        allocate (xrnlat(1:ndimr*3))
!      end if
!      call genvec(r_bas,Rrsp,aij,icount,xrnlat,1,nrsn,iprint,memsize)
!      if ( size(xrnlat,1) < memsize ) deallocate ( xrnlat ) 
!      if ( .not. allocated(xrnlat) ) then
!        ndimr = memsize
!        allocate (xrnlat(1:ndimr*3))
!      end if
      call genvec(r_bas,Rrsp,aij,icount,xrnlat,ndimr,nrsn,iprint)
!c
      if(iprint.ge.-10) then
       write(6,*)
       write(6,1000) nrsn
1000   format('  Number of R-space vectors',t40,'=',i5)
      end if

!      memsize = 0
      if ( nmesh > 0 ) then
!       if (  .not. allocated(xknlat) ) then
!        nksn(1:nmesh) = 1   
!        ndimk = nmesh
!        allocate (xknlat(1:3*nmesh))
!       end if
!       n2 = 0   
!       do imesh=1,nmesh
!        n1 = n2+1
!        n2 = (n1-1)+nksn(imesh)*3
!        call genvec(k_bas,Rksp,qmesh(1,1,imesh),nqpt(imesh),            &
!     &                             xknlat(n1:n2),1,nksn(imesh),iprint,i)
!        memsize = memsize+i
!       end do
!       if ( size(xknlat,1) < 3*memsize ) deallocate ( xknlat ) 
!       if ( .not. allocated(xknlat) ) then
!        ndimk = memsize
!        allocate (xknlat(1:ndimk*3))
!       end if

       if (allocated(xknlat)) deallocate(xknlat)
       n1 = 0
       ndimk = 0
       do imesh=1,nmesh
        call genvec(k_bas,Rksp,qmesh(1,1,imesh),nqpt(imesh),            &
     &                           tmpxkn,nk,nksn(imesh),iprint)
        if ( nksn(imesh)>0 ) then
         ndimk = ndimk+nksn(imesh)
         n1 = 3*(ndimk-nksn(imesh))
         if (allocated(xknlat)) then
           allocate(tmpvec(size(xknlat,1)))
           tmpvec = xknlat
           deallocate(xknlat)
           allocate(xknlat(3*ndimk))
           xknlat(1:n1) = tmpvec(1:n1)
           deallocate(tmpvec)
         end if
         if ( .not.allocated(xknlat) ) allocate(xknlat(1:3*ndimk))
         k0 = 1
         j0 = n1+1
         nj = nksn(imesh)
         do i=1,3
          j1 = j0+nj-1
          k1 = k0+nj-1
          xknlat(j0:j1) = tmpxkn(k0:k1)
          j0 = j1+1 
          k0 = k0+nk
         end do
         deallocate(tmpxkn)
        else
         if ( .not.allocated(xknlat) ) allocate(xknlat(1:1))
        end if 
        if(iprint.ge.-11) then
         write(6,1001) nksn(imesh),imesh
1001     format('  Number of K-space vectors',t40,'= ',i6,              &
     &                                               '      imesh =',i3)
        end if 
       end do
      end if
!c
      write(6,*)
!c
      return
!EOC
      end subroutine latvect
