!BOP
!!ROUTINE: gettref
!!INTERFACE:
      subroutine gettref(lmax,energy,pnrel,V0,Rmt0,                     &
     &                   trefinv,                                       &
     &                   iprint,istop)
!!DESCRIPTION:
! finds phase shifts {\tt trefinv(0:lmax)} for reference system: \\
! constant potential {\tt V0} in the sphere of radius {\tt Rmt0} \\
! (uses Ricatti-Bessel fcts. for solution)
!

!!DO_NOT_PRINT
      implicit none
!!DO_NOT_PRINT

!!ARGUMENTS:
      integer, intent(in) :: lmax,iprint
      complex(8), intent(in) :: energy,pnrel
      real(8), intent(in) :: V0,Rmt0
      complex(8), intent(out) :: trefinv(0:lmax)
      character(10), intent(in) :: istop
!!REVISION HISTORY:
! Adapted - A.S. - 2013
!!REMARKS:
! input argument energy is used only for printing;
! procedure stops if energy or V0 or Rmt0 are very close to zero
!EOP
!
!BOC
!      use mecca_constants
      integer :: l,lmin
!c
      complex(8) :: bjl(lmax+2)      ! (iplmax+2)
      complex(8) :: bnl(lmax+2)      ! (iplmax+2)
      complex(8) :: djl(lmax+2)      ! (iplmax+2)
      complex(8) :: dnl(lmax+2)      ! (iplmax+2)
      complex(8) :: bjlv(lmax+2)      ! (iplmax+2)
      complex(8) :: bnlv(lmax+2)      ! (iplmax+2)
      complex(8) :: djlv(lmax+2)      ! (iplmax+2)
      complex(8) :: dnlv(lmax+2)      ! (iplmax+2)
      complex(8) :: dradsl,radsl,radsolmt,fsn,dfsn,fsj,dfsj
      complex(8) :: pr,prv

      complex(8), parameter :: sqrtm1=(0.d0,1.d0)
      real(8), parameter :: tole=1.d-10
      character(*), parameter :: sname='gettref'
      if(iprint.ge.0) then
         write(6,'('' in GETTREF iprint '',i5)') iprint
         write(6,'(''           : lmax='',i5)') lmax
         write(6,'(''           : energy='',2d16.8)') energy
         write(6,'(''           : pnrel='',2d16.8)') pnrel
         write(6,'(''           : V0='',2d16.8)') V0
         write(6,'(''           : Rmt='',d16.8)') Rmt0
      endif

      pr=pnrel*Rmt0
      if( abs(pr)**2 .lt. tole ) then ! cannot solve for energy equal to 0.0
        call fstop(sname//':: energy=zero')
      endif

!c      prv = dcmplx(0.d0,Rmt0*sqrt(V0*function(E)),
!c              function(E) > 0
!c          AND function(E) = 1, if Im(E)=0
!c          AND function(E) decreases when Im(E) increases

      prv=dcmplx(0.d0,Rmt0*sqrt(V0))        ! then V=Energy+V0 -- scr.pot.
      if( abs(prv)**2 .lt. tole ) then ! cannot solve for V0 equal to 0.0
        call fstop(sname//':: V0=zero')
      endif
!c     =================================================================

      lmin=max(2,lmax+1)
      call ricbes(lmin,prv,bjlv(:),bnlv(:),djlv(:),dnlv(:))
      call ricbes(lmin,pr,bjl(:),bnl(:),djl(:),dnl(:))

      do l=0,lmax
       radsl = bjlv(l+1)/prv
       dradsl = (djlv(l+1) - radsl)
       radsolmt = dradsl/radsl

       fsn = bnl(l+1)/pr
       dfsn = (dnl(l+1)-fsn)
       fsj = bjl(l+1)/pr
       dfsj = (djl(l+1)-fsj)

       trefinv(l) = pnrel*(sqrtm1-                                      &
     &                     (dfsn-fsn*radsolmt)/(dfsj-fsj*radsolmt))
!c
!c         t^(-1) = k*( sqrt(-1) - cotan(delta_l))
!c
      end do

      if(istop.eq.sname) then
         call fstop(sname)
      else
         return
      endif
!EOC
      end subroutine gettref
