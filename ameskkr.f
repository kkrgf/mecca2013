!BOP
!!ROUTINE: ameskkr
!!INTERFACE:
      program ameskkr
!!DESCRIPTION:
! Advanced Materials Exploration and Studies with KKR,
! main program to perform SCF and EOS {\sc ameskkr} calculations
!

!!USES:
      use mecca_constants
      use mecca_types
      use mecca_interface, only : buildDate
      use mecca_scf_interface
      use input
      use mpi
!
!!ARGUMENTS:
!  it is a program, which gets input information from command line
! 
!!REVISION HISTORY:
! Revised         - A.S. - 2019
! Revised         - A.S. - 2013
! Initial Version - A.S. - 2005
!EOP
!
!BOC
      implicit none
      type(IniFile) :: inFile
      type(RunState) :: mecca_state
      integer narg
      character(2048) :: buffer
      character(120)  :: outinfo
      character(80)   :: buffer80
      character(80)   :: valbuf
      logical :: echo_command_line

      character(1024) cmpndName
      character(80) strType
      integer Neos
      integer nspin,nMM
      character(5) :: MM
      character(48) :: runID
      real(8) :: delt0=0.015
      real(8) :: delt_a
      character(80) :: def_eos
      real(8) ::  alat
      real(8) ::  boa
      real(8) ::  coa
      real(8) ::  so_power
      real(8) ::  Tempr
      integer lmax,mtasa,ncpa,nrel
      real(8) ::  ebot,etop
      integer npts,nscf
      integer ichg
      integer imix,igrid
      integer iXC,iprint
      real(8) ::  alphmix,betamix
      integer :: imethod,sprstech
      integer :: pointnucleus
      character(3) :: yes
      character(10) :: ccmod
      real(8) :: Rclstr
      real(8) :: val
      integer :: freeze_core
      integer :: i,izpE
      integer, external :: idToZ

      ! MPI related variables
      integer myrank, nproc, ierr
      logical mecca_mpi
!      character(len=32) logfile

      nproc = 0
      ! initialize MPI
      call mpi_init(ierr)
      call mpi_comm_size(mpi_comm_world, nproc, ierr)

      myrank = -1
      call mpi_comm_rank(mpi_comm_world, myrank, ierr)
      if( myrank > 0 ) then
        open(unit=6,file='/dev/null')
        ! write(logfile,'(a,i2,a)') 'rank', myrank, '.log'
        ! open(unit=6, file = trim(logfile) )
      endif

      call mpi_initialized(mecca_mpi,ierr)

      narg = command_argument_count()   ! iargc()

!     printing all command line arguments (by default)
      if ( myrank == 0 ) then
       echo_command_line = .true.
       if ( narg == 1 ) then
        call get_command_argument(1,buffer)
        if ( scan(buffer,'-') .eq. 1 ) then
         echo_command_line = .false.
        else
         echo_command_line = .true.
        end if
       else
        call get_command_argument(narg,buffer)
        if ( trim(buffer) .ne. '-noecho' ) then
         do i=2,narg-1
          call get_command_argument(i,buffer)
          if ( trim(buffer) == '-noecho' ) then  ! error: -noecho must be the last argument
           echo_command_line = .true.
           go to 10
          end if
          if ( trim(buffer) == '-Neos' ) then
           call get_command_argument(i+1,valbuf)
           read(valbuf,*) Neos
           if ( Neos == 0 ) then
            echo_command_line = .false.
            go to 10
           end if
          end if
         enddo
        else
         echo_command_line = .false.
         narg = narg-1    ! to remove "-noecho" argument from command line
        end if
       end if
10     continue
       if ( echo_command_line ) then
        write(6,'(a)') 'command line arguments:'
        do i=0,narg
         if(i>0) write(6,'(1x)',ADVANCE='NO')
         call get_command_argument(i,buffer)
         write(6,'(a)',ADVANCE='NO') buffer(1:len_trim(buffer))
        enddo
        write(6,'(/)')
       endif
      endif
      if ( narg == 1 ) then
       if ( mecca_mpi ) then
        write(6,'(1x,a,i4,a)') 'MPI initialized with ',                 &
     &                                          nproc, ' processes.'
       end if
       call get_command_argument(1,buffer)
       if ( scan(buffer,'-') .eq. 1 ) then
!        if ( scan(buffer,'-v') .eq. 1 ) then
        if ( trim(buffer)=='-v' .or. trim(buffer)=='-version' ) then
           call mecca_scf(mecca_state,buffer)
           call mpi_finalize(ierr)
           stop 
        else
         call get_command_argument(0,buffer)
         if ( myrank==0 ) call printHelp(buffer)
         call mpi_finalize(ierr)
         stop 1
        end if
       else
!......................................................................
        call run_mecca(trim(buffer),outinfo)
!......................................................................
       end if
      elseif ( mod(narg,2) == 0 .and. narg > 1 ) then
       call get_command_argument(1,cmpndName)
       if ( scan(cmpndName,'-') .eq. 1 ) then
         write(0,*) ' ERROR: compound name ',trim(cmpndName)            &
     &,             ' cannot begin with ''-'''
         call get_command_argument(0,buffer)
         if( myrank == 0 ) call printHelp(buffer)
         call mpi_finalize(ierr)
         stop 1
       end if
       call get_command_argument(2,strType)
       if ( scan(strType,'-') .eq. 1 ) then
         write(0,*) ' ERROR: structure-description file '               &
     &,             trim(strType)                                       &
     &,             ' cannot begin with ''-'''
         call get_command_argument(0,buffer)
         if( myrank == 0 ) call printHelp(buffer)
         call mpi_finalize(ierr)
         stop 1
       end if
!
       alat = zero
       boa  = zero
       coa  = zero
       do i=3,narg,2
        call get_command_argument(i,buffer80)
        call get_command_argument(i+1,valbuf)
        select case( buffer80 )
         case( '-alat' )
          read(valbuf,*) alat
         case( '-boa' )
          read(valbuf,*) boa
         case( '-coa' )
          read(valbuf,*) coa
         case default
          cycle
        end select
       end do
!
       call initInput(cmpndName,strType,alat,boa,coa,inFile)
       call mpi_barrier(mpi_comm_world,ierr)
!
       def_eos = ''
       Neos   = 1
       delt_a   = delt0
       do i=3,narg,2
        call get_command_argument(i,buffer80)
        call get_command_argument(i+1,valbuf)
        select case( buffer80 )
          case( '-alat' )
           cycle
         case( '-boa' )
           cycle
         case( '-coa' )
           cycle
         case( '-runID' )
          runID = trim(adjustl(valbuf))
          call setGenName(runID,inFile,1)
         case( '-Neos' )
          read(valbuf,*) Neos
         case( '-delt_a' )
          if ( len_trim(def_eos)>0 ) then
          write(6,*) ' def_eos = <',def_eos,'>'
          stop '-delt_a: Re-definition of the parameter is not allowed'
          end if
          def_eos = trim(adjustl(valbuf))
          read(valbuf,*) delt_a
         case( '-T=const' )
          if ( len_trim(def_eos)>0 ) then
          write(6,*) ' def_eos = <',def_eos,'>'
          stop '-T=const: Re-definition of the parameter is not allowed'
          end if
          def_eos = 'T:'//trim(adjustl(valbuf))
         case( '-V=const' )
          if ( len_trim(def_eos)>0 ) then
          write(6,*) ' def_eos = <',def_eos,'>'
          stop '-V=const: Re-definition of the parameter is not allowed'
          end if
          def_eos = 'V:'//trim(adjustl(valbuf))
         case( '-nspin' )
          read(valbuf,*) MM
          select case ( trim(adjustl(MM)) )
           case('1','NM','1.NM')
            nspin = 1
            nMM   = 0
           case('2','FM','2.FM')
            nspin = 2
            nMM   = 0
           case('3','DLM','2.DLM')
            nspin = 3
            nMM   = 1
           case('-2','AFM','2.AFM')
            nspin = 2
            nMM   = -1
           case default
            nspin = 1
            nMM   = 0
          end select
          inFile%nspin = nspin
          inFile%nMM = nMM
         case( '-so_power' )
          read(valbuf,*) so_power
          inFile%so_power  = so_power
         case( '-Tempr' )
          read(valbuf,*) Tempr
          inFile%Tempr  = Tempr
         case( '-mtasa' )
          read(valbuf,*) mtasa
          inFile%mtasa= mod(mtasa,divisor)
          inFile%intgrsch = mtasa/divisor
         case( '-ncpa' )
          read(valbuf,*) ncpa
          inFile%ncpa= ncpa
         case( '-ebot' )
          read(valbuf,*) ebot
          inFile%ebot= ebot
         case( '-etop' )
          read(valbuf,*) etop
          inFile%ebot= etop
         case( '-npts' )
          read(valbuf,*) npts
          inFile%npts = npts
         case( '-nscf' )
          read(valbuf,*) nscf
          inFile%nscf = nscf
         case( '-ichg' )
          read(valbuf,*) ichg
          inFile%imixtype  = ichg
         case( '-imix' )
          read(valbuf,*) imix
          inFile%mixscheme = imix
         case( '-igrid' )
          read(valbuf,*) igrid
          inFile%igrid= igrid
         case( '-iprint' )
          read(valbuf,*) iprint
          inFile%iprint = iprint
         case( '-iXC' )
          read(valbuf,*) iXC
          inFile%iXC = iXC
         case( '-alphmix' )
          read(valbuf,*) alphmix
          inFile%alphmix = alphmix
         case( '-betamix' )
          read(valbuf,*) betamix
          inFile%betamix = betamix
         case( '-lmax' )
          read(valbuf,*) lmax
          inFile%lmax = lmax
         case( '-nrel' )
          read(valbuf,*) nrel
          inFile%nrel= nrel
         case( '-Kpoints')
          call defineKmesh( trim(adjustl(valbuf)),                      &
     &                           inFile%nmesh,inFile%nKxyz )
         case( '-pointnucleus' )
          read(valbuf,*) pointnucleus
          call setFiniteNucl(pointnucleus)
         case( '-setBsfCrtsn' )
          read(valbuf,*) yes
          if ( yes == 'yes' .or. yes == 'YES' ) then
           call setBsfCrtsn()
          end if
         case( '-cc_cpa_model' )
          read(valbuf,*) ccmod
          call setCCcpa(trim(ccmod))
! sparse version parameters
         case( '-imethod' )
          read(valbuf,*) imethod
          inFile%imethod = imethod
         case( '-sprstech' )
          read(valbuf,*) sprstech
          inFile%sprstech = sprstech
         case( '-Rclstr' )
          read(valbuf,*) Rclstr
          inFile%Rclstr = Rclstr
! accuracy control parameters -----------------------------
         case( '-logR_mesh_step' )
          read(valbuf,*) val
          call setXRmeshStep(val)
         case( '-ef_tol' )
          read(valbuf,*) val
          call setEfTol(val)
         case( '-E_tol' )
          read(valbuf,*) val
          call setEtol(val)
         case( '-tc_tol' )
          read(valbuf,*) val
          call setTcTol(val)
         case( '-q_tol' )
          read(valbuf,*) val
          call setQtol(val)
!----------------------------------------------------------
         case( '-extEXptnl' )
          read(valbuf,*) val
          inFile%magnField = val
         case( '-zpE' )
          read(valbuf,*) izpE
          call setZeroPt(izpE)
         case( '-freeze_core' )
          read(valbuf,*) freeze_core
          inFile%freeze_core= freeze_core
         case default
          write(0,*)                                                    &
     &     ' WARNING: Argument <',trim(buffer80),'> is unknown'         &
     &,     ' and ignored'
        end select
       end do
       buffer80 = inFile%name
       if( myrank == 0 ) then
        if ( Neos == 0 ) then
         buffer80 =''
        else
         if ( mecca_mpi ) then
           write(6,'(1x,a,i4,a)') 'MPI initialized with ',              &
     &                                          nproc, ' processes.'
         end if
        end if
        call saveInput(inFile,buffer80)
       end if
!DEBUGPRINT
!        write(6,*) ' gXRmeshStep() = ',gXRmeshStep()
!        write(6,*) ' gEfTol() = ',gEfTol()
!        write(6,*) ' gEtol() = ',gEtol()
!        write(6,*) ' gTcTol() = ',gTcTol()
!        write(6,*) ' gQtol() = ',gQtol()
!        write(6,*) ' gZeroPt()= ',gZeroPt()
!        write(6,*) ' gPointNucleus()= ',gPointNucleus()
!        write(6,*) '2 ---'
!DEBUGPRINT

       if ( Neos>=1 ) then
        if ( len_trim(def_eos)<1 ) then
         write(def_eos,*) delt_a
         def_eos = adjustl(def_eos)
        end if
!......................................................................
        call eos_mecca (Neos,def_eos,buffer80)
!......................................................................
       else if ( Neos < 0 ) then
         write(0,*) ' Parameter Neos must be non-negative'
       else
!         if( myrank == 0 ) then
!          write(6,'(a)') trim(buffer80)
!         end if
       end if
       call mpi_barrier(mpi_comm_world,ierr)
      else
       if (narg > 0) write(0,*)                                         &
     &           ' ERROR: Incorrect command-line arguments list;'
       call get_command_argument(0,buffer)
       if ( myrank==0 ) call printHelp(buffer)
       call mpi_finalize(ierr)
       stop 1 
      end if

      call mpi_finalize(ierr)
      stop 

!EOC
      CONTAINS
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!BOP
!!IROUTINE: printHelp
!!INTERFACE:
      subroutine printHelp(exename)
!!ARGUMENTS:
      character*(*) exename
!!REVISION HISTORY:
! Revised Version - A.S. - 2019
! Initial Version - A.S. - 2013
!EOP
!
!BOC
      character(16) :: vers
      logical :: sparse_version

      vers = adjustl(trim(buildDate()))
      sparse_version = index(vers,'sp ')>0
   1  format(1x,a)
      write(0,*)
      write(0,1) ' Proper usage:'
      write(0,*)
      write(0,*) trim(exename),' input_file_name'
      write(0,*)
      write(0,1) '   OR with compound name (like Ni3Pt, '               &
     &                      //'"(Al0.2Fe0.80)"),'
      write(0,1) ' structure-description file (or type of structure: '  &
     &                      //'fcc, bcc, sc, b1, b2),'
      write(0,*) ' and some other parameters, e.g. number of points to' &
     &      //' calculate EOS (-Neos),',' exchange-correlation (-iXC),' &
     &      //' magnetism type (-nspin), etc.:'
      write(0,*)
      write(0,1) trim(exename)                                          &
     &                      //'  compound_name structure_description'
      write(0,1) '   Optional arguments are'
      write(0,1) ' [-Neos #_of_Etot-points_for_EOS_estimate'            &
     &            //' (1 is default, 0 is to generate input file only)]'
      write(0,'(a,f5.3,a)') '  [-delt_a lattice_param_relat_step_in_EOS'&
     &                      //'_calcs (',delt0,' is default)]'
      write(0,1) ' [-runID your_run_name (not more than 48 characters;' &
     &             //' default name is based on compound name)]'
      write(0,1) ' [-alat lattice_parameter_in_Bohr'                    &
     &                     //' (internal procedure provides a default)]'
      write(0,1) ' [-boa B/A-ratio (1.0 is default)]'
      write(0,1) ' [-coa C/A-ratio (1.0 is default)]'
      write(0,1) ' [-nspin magnetism_type (NM, FM, AFM, DLM;'           &
     &                     //' non-magnetic (NM, also 1) is a default)]'
      write(0,1) ' [-mtasa type_of_sph.approx. (11 - ASA, 12 - ASA+PBC,'&
     &                      //' 22 - VP+PBC; 12 is default)]'
      write(0,1) ' [-iXC eXchange-Correlation_functional'               &
     & //' (default value is 001007, i.e. X=001 (Slater), C=007 (VWN))]'
      write(0,1) ' [-igrid grid_type]'
      write(0,1) ' [-ebot Bottom_of_the_E-contour_in_Ry'                &
     &                      //' (default value depends on Z)]'
      write(0,1) ' [-npts #_of_E-contour_points]'
      write(0,1) ' [-Kpoints definition_of_kpoints_set ('               &
     & //'1:n1,n2,n3@2:m1,m2,m3 defines a two-grid set, '
      write(0,1) '   a one-grid set is defined with 1:n1,n2,n3 record,'
      write(0,1) '   other options are applied only for first grid: '
      write(0,1) '   n1,n2,n3 is used to redefine 1st grid in a default'&
     & //' mesh, -1 is for full-zone integration, '
      write(0,1) '   -2 is to shift a grid origin to (from) Gamma-point'&
     & //' for lattice with cubic'
      write(0,1) '   (hexagonal) symmetry, -3 is to have a union of '   &
     & // 'shifted and non-shifted grids (cubic))]'
      write(0,1) ' [-nscf max_#_of_SCF_iterations]'
      write(0,1) ' [-imix mixing_scheme (1 is default, i.e. Broyden)]'
      write(0,1) ' [-ichg mixing_type (1-potential, 0-charge_density]'
      write(0,1) ' [-alphmix non-magnetic_mix_coefficient]'
      write(0,1) ' [-betamix magnetic_mix_coefficient]'
      write(0,1) ' [-Tempr electronic_temperature_in_Ry (0 is default;' &
     &         //' non-zero Tempr parameter must be used together with' &
     &         //' parameter npts>1000)]'
      write(0,1) '  [-nrel relativistic_control_parameter '             &
     &            //'(nrel=0: Dirac core, Dirac SS valence,'            &
     &            //'scalar-relat. MS,'
      write(0,1) '   nrel=1: Dirac core, scalar-relat. valence,'        &
     &         //'   nrel=200: scalar-relat. for all electrons, etc.)]'
      write(0,1) ' [-so_power spin_orbit_control_parameter_for_valence '&
     &            //'(1:Dirac, 0:scalar-relat.)'
      write(0,1) ' [-lmax max_multi-scattering_wave-function'           &
     &                      //'_orbital_number]'
      write(0,1) ' [-pointnucleus nucleus_model (1: point-nucleus, '    &
     &                      //'0: non-point nucleus]'
      write(0,1) ' [-cc_cpa_model ccmod_value (MF is default,'          &
     &                       //' other values: NUL,FCC,BCC,SIM)]'
      write(0,'(a,i2,a)') '  [-ncpa CPA_control_parameter '             &
     &            //'(ncpa=1 activates CPA with ',maxcpa,' iterations)]'
      write(0,1) ' [-setBsfCrtsn yes (to use Cartesian vectors for '    &
     &         //'BSF/FS input k-points description, in units of 2pi/a;'
      write(0,1) '   otherwise reciprocal lattice vectors basis is '    &
     &            //'assumed)'
      if ( sparse_version ) then
       write(0,1) ' [-imethod screend-KKR_switch    (0 is default)]'
       write(0,1) ' [-sprstech sparse-matrix_switch (0 is default)]'
       write(0,1) ' [-Rclstr screening-cluster_parameter]'
      end if
      write(0,1) ' [-iprint verbosity (default value is -1,'            &
     &            //' more detailed output: iprint=0,'                  &
     &            //' minimum output: iprint=-1000,'
      write(0,1) '   iprint>0 produces high output volume)]'
      write(0,1) ' [-extEXptnl external_exchange_potential_in_Ry '      &
     &            //'("magnetic splitting")]'
      write(0,1) ' [-logR_mesh_step value (<0.01)]'
      write(0,1) ' [-q_tol value]'
      write(0,1) ' [-E_tol value_in_Ry]'
      write(0,1) ' [-ef_tol value_in_Ry]'
      write(0,*)
      write(0,1) ' [-noecho] argument should be the last one in the'    &
     &           //' command line, it prevents printing of all'         &
     &           //' command-line arguments.'
      write(0,*)
      write(0,1) ' To generate input file without actual run, '         &
     &           //'use -Neos 0 (redirect output to the file).'
      write(0,*)
      return
!EOC
      end subroutine printHelp

      end program ameskkr
