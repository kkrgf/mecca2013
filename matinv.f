!BOP
!!ROUTINE: matinv
!!INTERFACE:
      subroutine matinv(amt,bmt,w1,kkrsz,iprint,istop)
!!DESCRIPTION:
! inverts matrix {\tt amt}
!

!!DO_NOT_PRINT
      implicit none
!!DO_NOT_PRINT

!!ARGUMENTS:
      integer, intent(in) :: kkrsz
      complex(8), intent(in) :: amt(:,:)
      complex(8), intent(out) :: bmt(:,:)   ! amt^(-1)
      complex(8) :: w1(*)
      integer :: iprint
      character(10), intent(in), optional :: istop
!!REVISION HISTORY:
! Adapted - A.S. - 2013
!!REMARKS:
! argument w1 is not used in this implementation
!EOP
!
      interface
       subroutine gsvein1(amt,ndmat,                                    &
     &                  cof,ndcof,                                      &
     &                  kkrsz,natom,                                    &
     &                  logdet)
       integer, intent(in) :: ndmat,ndcof,kkrsz,natom
       complex(8) :: amt(ndmat,kkrsz*natom) 
       complex(8) :: cof(*) 
       complex(8), intent(out), optional :: logdet
       end subroutine gsvein1
      end interface
      interface
        subroutine wrtmtx(x,n,istop)
        implicit none
        integer, intent(in) :: n
        complex(8), intent(in) :: x(:,:)
        character(10), intent(in), optional :: istop
        end subroutine wrtmtx
      end interface
!
!      complex*16 czero
!      complex*16 cone
!      parameter (czero=(0.d0,0.d0))
!      parameter (cone=(1.d0,0.d0))
!
!BOC
      character  sname*10
      parameter (sname='matinv')
!c     --------------------------------------------------------------

      bmt(1:kkrsz,1:kkrsz) = amt(1:kkrsz,1:kkrsz)
      call gsvein1(bmt,size(bmt(:,1)),                                  &
     &                  w1,0,                                           &
     &                  kkrsz,1)
!c
!c     =========================================================


      if (iprint.ge.2) then
         write(6,'('' minv: input'')')
         call wrtmtx(amt,kkrsz)
         write(6,'('' minv: inverse'')')
         call wrtmtx(bmt,kkrsz)
      endif
!c
!c     =========================================================
      if ( present(istop) ) then
       if(istop.eq.sname) then
         call fstop(sname)
       endif
      endif
!c
      return
!EOC
      end subroutine matinv
