!BOP
!!MODULE: ShapeCorr
!!INTERFACE:
      module ShapeCorr
!!DESCRIPTION:
! container for shape-related data
!
!!REVISION HISTORY:
! Initial version - A.S. - 2007
!EOP
!
!BOC
      real*8, allocatable :: ShCsaved(:)
      integer, save :: i0=0,i1=-10
      character*28 sname/'V.P. energy shape-correction'/
!c      save ShCsaved
      end module ShapeCorr
!c
!BOP
!!ROUTINE: ShCModule
!!INTERFACE:
      subroutine ShCModule(iflag,nsub1,nsub2,vdata)
!!DESCRIPTION:
! provides access to data of module ShapeCorr
! {\bv
!   iflag= 0 -- read
!   iflag= 1 -- write
!   iflag= 2 -- alocate
!   iflag=-1 -- deallocate
!   iflag=-2 -- info
!   iflag=-3 -- print
! \ev}
!!USES:
      use ShapeCorr

!!DO_NOT_PRINT
      implicit none
!!DO_NOT_PRINT

!!ARGUMENTS:
      integer iflag,nsub1,nsub2
      real*8  vdata(nsub1:nsub2)
!!REVISION HISTORY:
! Initial version - A.S. - 2007
!EOP
!
!BOC
      integer i

      if(iflag.eq.0) then              ! read
       if(nsub2.lt.nsub1) return
       vdata(nsub1:nsub2) = ShCsaved(nsub1:nsub2)
      else if(iflag.eq.1) then         ! write
       if(nsub2.lt.nsub1) return
       ShCsaved(nsub1:nsub2) = vdata(nsub1:nsub2)
      else if(iflag.eq.2) then         ! allocate
       if(allocated(ShCsaved)) then
      if(nsub1.lt.i0.or.nsub2.gt.i1) then
       deallocate(ShCsaved)
      else
       return
      end if
       end if
       if(nsub2.lt.nsub1) return
       allocate(ShCsaved(nsub1:nsub2))
       i0 = nsub1
       i1 = nsub2
      else if(iflag.eq.-1) then        ! deallocate
       if(allocated(ShCsaved)) deallocate(ShCsaved)
      else if(iflag.eq.-2) then        ! info
       if(allocated(ShCsaved)) then
      nsub1 = i0
      nsub2 = i1
       else
      nsub1 = 0
      nsub2 = -10
       end if
      else if(iflag.eq.-3) then        ! print
       do i=nsub1,nsub2
      write(6,'(''  nsub='',i4,2x,a,''='',d14.6)')                      &
     &        i,sname,ShCsaved(i)
       end do
      end if

      return
!EOC
      end subroutine ShCModule
!
!BOP
!!ROUTINE: ioVPEmom
!!INTERFACE:
      subroutine ioVPEmom(io)
!!DESCRIPTION:
! provides data exchange between module ShapeCorr and a file
! \\ \\
! $io<0$ -- writing to file \\
! $io>0$ -- reading from file \\
! abs({\tt io}) is fortran unit attached to a file
!
!!USES:
      use ShapeCorr
!!REVISION HISTORY:
! Initial version - A.S. - 2007
!EOP
!
!BOC
      implicit none
      integer io

      integer nch, i0r, i1r, i

      if(io.eq.0) return
      if(.not.allocated(ShCsaved)) return

      nch = abs(io)

      if(io.lt.0) then
       write(nch,'(2(1x,i8),''    nsub0,nsub1'')') i0,i1
      else
       read(nch,*)  i0r,i1r
       if(i0r.lt.i0) call p_fstop(sname//':  Read i0-error???')
       if(i1r.gt.i1) call p_fstop(sname//':  Read i1-error???')
      end if

      if(io.lt.0) then        ! write to file
       do i=i0,i1
      write(nch,'(1x,f18.15)') ShCsaved(i)
       end do
      else                               !      to read from file
       do i=i0r,i1
      if(i.ge.i0) then
       read(nch,*) ShCsaved(i)
      else
       read(nch,*)
      end if
       end do
      end if

      return
!EOC
      end subroutine ioVPEmom
