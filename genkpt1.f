!BOP
!!ROUTINE: genkpt1
!!INTERFACE:
      subroutine genkpt1(rslatt,kslatt,tau0,ity,nat,wvkl,lwght,nmesh,   &
     &            istriz,invadd,                                        &
     &            r,ib,if0,if0temp,rx,jf0,kcheck,lrot,                  &
     &            nkx,nq,nk,nrot,gen_mesh)
!!DESCRIPTION:
! determines point group of the bravais lattice and group symmetry
! of the lattice, generates respective rotation matrices and
! Monkhorst-Pack meshes
!
!!USES:
      use mesh3d, only : gMPmesh
!
!!ARGUMENTS:
      implicit real*8 (a-h,o-z)
      real*8 rslatt(3,3),kslatt(3,3)
      integer nat
      real*8  tau0(3,nat)
      integer ity(nat)
      integer nmesh
      real*8  wvkl(3,nkx,nmesh)
      integer lwght(nkx,nmesh)
      integer, intent(in) :: istriz,invadd
      real*8  r(49,3,3)
      integer ib(48)
      integer if0(48,nat),if0temp(nat)
      real*8  rx(3,nat)
      integer jf0(nat,nat)
      integer nkx
      integer kcheck(nkx)
      integer lrot(nkx,nmesh)
      integer nq(3,nmesh),nk(nmesh)
      integer nrot
      logical gen_mesh
!
!!REVISION HISTORY:
! Adapted  - A.S. - 2013
!EOP
!
!BOC
      dimension if00(48)
!      real*8 wvk0(3,3)

      dimension a1(3),a2(3),a3(3),b1(3),b2(3),b3(3)
      dimension v(3,48)

      dimension x0(3),r0(49,3,3),v0(3,48),ib0(48)
      real(8) :: kslatt_(3,3)

!???      character*10 sname/'genkpt1'/
      data x0 /3*0.0d0/
      parameter (eps=1.d-06)
      parameter (ilog=6)
      integer, parameter :: li13=13,li25=25
      logical :: no_trs

      integer, external :: hexSymm

      do i=1,3
       a1(i) =  rslatt(i,1)
       a2(i) =  rslatt(i,2)
       a3(i) =  rslatt(i,3)
       b1(i) =  kslatt(i,1)
       b2(i) =  kslatt(i,2)
       b3(i) =  kslatt(i,3)
      enddo
      kslatt_(1,1:3) = b1(1:3)
      kslatt_(2,1:3) = b2(1:3)
      kslatt_(3,1:3) = b3(1:3)

!c     determine group symmetry of the lattice
!c
!c      ihg .... point group of the primitive lattice, holohedral
!c               group number:
!c               ihg=1 stands for triclinic system
!c               ihg=2 stands for monoclinic system
!c               ihg=3 stands for orthorhombic system
!c               ihg=4 stands for tetragonal system
!c               ihg=5 stands for cubic system
!c               ihg=6 stands for trigonal system
!c               ihg=7 stands for hexagonal system
!c      ihc .... code distinguishing between hexagonal and cubic
!c               groups
!c               ihc=0 stands for hexagonal groups
!c               ihc=1 stands for cubic groups
!c      isy .... code indicating whether the space group is
!c               symmorphic or nonsymmorphic
!c               isy=0 means nonsymmorphic group
!c               isy=1 means symmorphic group
!c               the group is considered symmorphic if for each
!c               operation of the point group the sum of the 3
!c               components of abs(v(n)) ( nonprimitive translation,
!c               see below) is lt. 0.0005
!c      li ..... code indicating whether the point group
!c               of the crystal contains inversion or not
!c               (operations 13 or 25 in respectively hexagonal
!c               or cubic groups).
!c               li=0 means: does not contain inversion
!c               li.gt.0 means: there is inversion in the point
!c                              group of the crystal
!c      nrot.... total number of elements in the point group of the
!c               crystal
!c      ib ..... list of the rotations constituting the point group
!c               of the crystal. the numbering is that defined in
!c               worlton and warren, i.e. the one materialized in the
!c               array r (see below)
!c               only the first nrot elements of the aray ib are
!c               meaningful
!c      v  ..... nonprimitive translations (in the case of nonsymmor-
!c               phic groups). v(i,n) is the i-th component
!c               of the translation connected with the n-th element
!c               of the point group (i.e. with the rotation
!c               number ib(n) ).
!c               attention: v(i) are not cartesian components,
!c               they refer to the system a1,a2,a3.
!c      if0 .... the function defined in maradudin, ipatova by
!c               eq. (3.2.12): atom transformation table.
!c               the element if0(n,kapa) means that the n-th
!c               operation of the space group (i.e. operation number
!c               ib(n), together with an eventual nonprimitive
!c               translation  v(n)) transfers the atom kapa into the
!c               atom if0(n,kapa).
!c      r ...... list of the 3 x 3 rotation matrices
!c               (xyz representation of the o(h) or d(6)h groups)
!c               all 48 or 24 matrices are listed.

      write(ilog,*)

!c     generate the bravais lattice

      write(ilog,15)
15    format(/,1x,' The (unstrained) bravais lattice '/                 &
     &' (used for generating the largest possible mesh in the b.z.)')

      call grpsymm(ilog,rslatt,kslatt_,1,x0,ity,r0,                     &
     &             ib0,if00,if0temp,v0,                                 &
     &             rx,ihg0,ihc0,nc0,li0,isy0)

      if ( ihc0==1 ) then
         lin = li25
      else
         lin = li13
      end if
      no_trs = all(ib0(1:nc0).ne.lin)

      write(ilog,*) '  IHG0=',ihg0,' IHC0=',ihc0
      write(ilog,*) '  ISY0=',isy0,'  LI0=',li0
      write(ilog,*) '  NC0 = ',nc0,' TRS=',.not.no_trs

!c     it is assumed that the same 'type' of symmetry operations
!c     (cubic/hexagonal) will apply to the crystal as well as the bravais
!c     lattice.

      call grpsymm(ilog,rslatt,kslatt_,nat,tau0,ity,r,                  &
     &            ib,if0,if0temp,v,                                     &
     &            rx,ihg,ihc,nrot,li,isy)

      if ( ihc==1 ) then
         lin = li25
      else
         lin = li13
      end if
      write(ilog,*) '  IHG=',ihg,' IHC=',ihc
      write(ilog,*) '  ISY=',isy,'  LI=',li
      write(ilog,*) '  NROT = ',nrot
      no_trs = all(ib(1:nrot).ne.lin)
      write(ilog,*) '  TRS=',.not.all(ib(1:nrot).ne.lin)

!c     check the symmetry

      call checksymm(ilog,rslatt,kslatt_,if0,jf0,ib,r,v,nat,nat,nrot)

      ib(nrot+1:) = 0

      ninvadd = 0

      if ( invadd==1 .and. no_trs ) then
       nrot=nrot+1
       ib(nrot) = lin
       do n=1,nrot
        ibn = ib(n)
        if ( ibn<lin ) then
         ibadd = ibn+lin-1
        else
         ibadd = ibn-lin-1
        end if
       end do
        write(ilog,10)
!10      format(/,1x,'Although the point group of the crystal does not'/ &
!     &    ' contain inversion, the special point generation algorithm'/ &
!     &    ' will consider it as a symmetry operation')
      end if

      if(invadd.ne.0) then
       if (li .eq. 0) then
        write(ilog,10)
10      format(/,1x,'Although the point group of the crystal does not'/ &
     &    ' contain inversion, the special point generation algorithm'/ &
     &    ' will consider it as a symmetry operation')
        do n=1,nrot
         ibn = ib(n)
         if(ihc == 1) then
           if(ibn.le.24) then
            ibadd = ibn+24
           else
            ibadd = ibn-24
           end if
         else
           if(ibn.le.12) then
            ibadd = ibn+12
           else
            ibadd = ibn-12
           end if
         end if
         do nn=1,nrot+ninvadd
          if(ibadd.eq.ib(nn)) go to 5
         end do
         ninvadd = ninvadd+1
         ib(nrot+ninvadd) = ibadd
         do iat=1,nat
          jat = if0(n,iat)
          if0(nrot+ninvadd,jat) = iat
         end do
5        continue
        end do
       endif
      endif
      nrot = nrot+ninvadd
!DELETE
!      if ( .not.no_trs .and. .true. ) then
!       ninvadd = 0
!       write(ilog,*) 'DEBUG ib(1:nrot): ',ib(1:nrot),nrot
!       do n=1,nrot
!        if ( ib(n)>=lin ) then
!         ib(n) = 0
!        end if
!       end do
!       do n=1,nrot
!        if ( ib(n)<1 ) cycle
!        ninvadd=ninvadd+1
!        ib(ninvadd) = ib(n)
!       end do
!       nrot=ninvadd
!       write(ilog,*) 'DEBUG OUT: ',ib(1:nrot),nrot
!       write(ilog,'(1x,''time-reversal  symmetry is disabled'')')
!      end if
!DELETE

!c     /         '  Generation of special points',
!c     /'  (iq1,iq2,iq3 are the (generalized) monkhorst-pack parameters'
!c      (they are not multiplied by 2*pi because b1,2,3  were not,either)

!      call gMPmesh(nmesh,nq,a1,a2,a3,b1,b2,b3,invadd,nrotadd,ib,r,      &
!     &                               size(lwght,1),nk,lwght,wvkl,lrot)

!
!      do imesh=1,nmesh
!
!!CDEBUG
!      if(istriz.eq.-2) then
!       nc = 1         ! full zone
!      else
!       nc = nrotadd
!      end if
!!CDEBUG
!
!       iq1 = abs(nq(1,imesh))
!       iq2 = abs(nq(2,imesh))
!       iq3 = abs(nq(3,imesh))
!       ur1 = 1.d0/dfloat(2*iq1)
!       ur2 = 1.d0/dfloat(2*iq2)
!       ur3 = 1.d0/dfloat(2*iq3)
!
!        nminus = 0
!        do i=1,3
!         if(nq(i,imesh).ne.abs(nq(i,imesh))) nminus = nminus+1
!        end do
!
!       if(nminus.eq.0) then
!        nshift = 1
!        wvk0(1:3,1) = 0.d0
!       else if(nminus.eq.1) then           ! Full Zone integration
!        nshift = 1
!        wvk0(1:3,1) = 0.d0
!        nc = 1
!       else if(nminus.eq.2) then           ! Shift to Gamma-point
!        nshift = 1
!        do i=1,3
!         wvk0(i,1) = -ur1*b1(i) - ur2*b2(i) - ur3*b3(i)
!        end do
!       else                                ! Unshift.+Shift_to_Gamma
!        nshift = 2
!        do i=1,3
!         wvk0(i,1) = 0.d0
!         wvk0(i,2) = -ur1*b1(i) - ur2*b2(i) - ur3*b3(i)
!        end do
!       end if
!
!!cab       istriz = 0
!
!       call spkpt1(ilog,iq1,iq2,iq3,wvk0,nshift,size(lwght,1),          &
!     &      b1,b2,b3,                                                   &
!     &      invadd,nc,ib,r,nk(imesh),wvkl(1,1,imesh),lwght(1,imesh),    &
!     &      kcheck,lrot(1,imesh),istriz)
!
!       write(ilog,20) imesh,iq1,iq2,iq3,nk(imesh)
!20     format(' IMESH=',i3,' IQ1=',i3,' IQ2=',i3,' IQ3=',i3,            &
!     &  ' ==> number of special k-pts = ',i5)
!
!       if (nk(imesh) .le. 0) then
!        write(ilog,40) nk(imesh),size(lwght,1)
!        call fstop(sname//' NK .LE. 0')
!       endif
!40     format(1x,'dimensions special k points nk, nkx ',2i5)
!
!!c     set near-zeroes equal to zero:
!
!       do 55 l=1,nk(imesh)
!        do 60 i = 1,3
!         if (abs(wvkl(i,l,imesh)) .lt. eps) wvkl(i,l,imesh) = 0.0d0
!60      continue
!
!!c       express special points in basis
!
!        proj1 = 0.0d0
!        proj2 = 0.0d0
!        proj3 = 0.0d0
!        do 65 i = 1,3
!         proj1 = proj1 + wvkl(i,l,imesh)*a1(i)
!         proj2 = proj2 + wvkl(i,l,imesh)*a2(i)
!         proj3 = proj3 + wvkl(i,l,imesh)*a3(i)
!65      continue
!
!        do 70 i = 1,3
!         wvkl(i,l,imesh) = proj1*b1(i) + proj2*b2(i) + proj3*b3(i)
!70      continue
!
!55     continue
!
!!cab       do i=1,nk(imesh),5
!!cab        lmax=lwght(i,imesh)
!!cab        write(ilog,121) dble(lmax),(wvkl(j,i,imesh),j=1,3)
!!cab       enddo
!!cab121    format(4d20.13)
!
!      end do

      do n=1,nrot
       ibn = ib(n)
       do i=1,3
        do j=1,3
         r0(n,j,i) = r(ibn,j,i)
        enddo
       enddo
      enddo

      do n=1,nrot
       do i=1,3
        do j=1,3
         r(n,j,i) = r0(n,j,i)
         ib(n) = n
        enddo
       enddo
      enddo

      if ( gen_mesh ) then
       call gMPmesh(nmesh,nq,a1,a2,a3,b1,b2,b3,invadd,nrot,ib,r,        &
     &                               size(lwght,1),nk,lwght,wvkl,lrot)
      end if

      return

!EOC
      CONTAINS

!BOP
!!IROUTINE: grpsymm
!!INTERFACE:
      subroutine grpsymm(ilog,a,ai,nat,x,ity,r,ib,if0,if0temp,v,        &
     &rx,ihg,ihc,nc,li,isy)
!EOP
!
!BOC
      implicit real*8 (a-h,o-z)

      dimension a(3,3),ai(3,3)
      dimension x(3,nat),ity(nat)
      dimension v(3,48),r(49,3,3),ib(48)
      dimension if0(48,nat),if0temp(nat)
      dimension rx(3,nat)

!c     determine point group of the bravais lattice

      call pgl1(a,ai,ihc,nc,ib,ihg,r)
!
!c     determine space group of the crystal
      call spacegrp(ilog,ai,nat,x,ity,r,ib,if0,if0temp,v,rx,            &
     &ihg,nc,li,isy)

      return
!EOC
      end subroutine grpsymm

!BOP
!!IROUTINE: spacegrp
!!INTERFACE:
      subroutine spacegrp(ilog,ai,nat,x,ity,r,ib,if0,if0temp,v,         &
     &rx,ihg,nc,li,isy)
!EOP
!
!BOC
      implicit real*8 (a-h,o-z)
!      parameter(eps=1.0d-6 )
      dimension ai(3,3)
      dimension x(3,nat),ity(nat)
      dimension v(3,48),r(49,3,3),ib(48)
      dimension if0(48,nat),if0temp(nat)
      dimension rx(3,nat)
      dimension vr(3),vt(3),xb(3),index(48)
      logical   lcell

      character*10 sname/'spacegrp'/
      character*12 icst(7)
      data icst /'triclinic','monoclinic','orthorhombic','tetragonal',  &
     &'cubic','trigonal','hexagonal'/

      lcell = .false.

!c     zero index array
      do n=1,48
      index(n) = 0
      enddo

!c     loop over elements of the group of the lattice
      do n=1,nc
      icell = 0

!c     rotate the atoms x to position rx
      l=ib(n)
        do k=1,nat
          do i=1,3
          rx(i,k)=0.0d0
            do j=1,3
            rx(i,k)=rx(i,k)+r(l,i,j)*x(j,k)
            enddo
          enddo
        enddo

!c     consider the first rotated atom k1=1
5       k1=1

!c     loop over original atoms in the cell
        do 10 k2 = 1,nat
          if (ity(k1) .ne. ity(k2)) cycle

!c     check to see whether k2 rotates into k1 under n with nonsymmorphic
!c     translation vr

          do i=1,3
          xb(i)=rx(i,k1)-x(i,k2)
          enddo

!c     subroutine rlv removes a direct lattice vector from xb leaving the
!c     remainder in vr.  if a nonzero lattice vector was removed, il is
!c     made nonzero.
!c     vr stands for v-reference
          call rlv3 (ai,xb,vr,il)

!c     loop over rotated atoms
          do 20 k3 = 1,nat

!c     loop over original atoms
            do 30 k4 = 1,nat
            if (ity(k3) .ne. ity(k4)) cycle

!c     check to see whether each atom in the original system is found in the
!c     rotated system with corresponding nonprimitive translation vr. if a
!c     single match cannot be made, then k2 does not rotate into k1 undern. if
!c     all the atoms match up, then if0(n,k) is the atom transformation table
!c     with nonsymmorphic translation vr. we do not stop here, however, but
!c     check to see whether there exits more than one nonsymmorphic translation
!c     vr corresponding to the group element n which could be the case ina
!c     supercell geometry. in this case, we artificially change the internal
!c     symmetry of the cell by 'painting atom number 1 a different color'

            do i=1,3
            xb(i) = rx(i,k3) - x(i,k4)
            enddo
!c     vt stands for v-test
            call rlv3 (ai,xb,vt,il)

            dif = 0.0d0
            do i = 1,3
            da = abs(vr(i) - vt(i)) + eps
            dif = dif + mod(da,1.d0)
            enddo

            if (dif .gt. 0.001d0) cycle

!c     atom k4 rotates into k3 with nonsymmorphic translation vr
            if0temp(k3)=k4
!c     check for the next rotated atom k3
            goto 20

30          continue
!c     cannot find all the atoms in the rotated system for this choice of vr
          goto 10

20        continue
!c     an exact match is made: n is an element of the group with nonsymmorphic
!c     translation vr which is not given in cartesian coordinates but in the
!c     system a1,a2,a3.

      icell = icell + 1
      index(n) = 1

        do i=1,3
        v(i,n) = vr(i)
        enddo
        do k=1,nat
        if0(n,k) = if0temp(k)
        enddo

10      continue

        if (icell .gt. 1) then
          if (lcell)                                                    &
     &    call p_fstop(sname//' cannot reduce symmetries of supercell')
          lcell = .true.
          write(ilog,35) icell
          icell = 0
!c     change the internal symmetry by changing the atomic number of atom1
          ity(1) = -ity(1)
          goto 5
        endif

      enddo

35    format(5x,'A nonprimitive supercell was chosen with',i5,' times', &
     &/,5x,'as many more space group operations than that for',         &
     &/,5x,'the corresponding primitive unit cell',/)

!c     change back the atomic number of atom 1
      if (lcell) ity(1) = -ity(1)

      i  = 0
!c     inverse of hexagonal group
      ni = 13
!c     inverse of cubic group
      if (ihg .lt. 6) ni = 25
      li = 0
!c     reshuffle
      do 40 n=1,nc
        if (index(n) .eq. 0) go to 40
        i = i + 1
        ib(i) = ib(n)
        if (ib(i) .eq. ni) li = i
          do k = 1,nat
          if0(i,k) = if0(n,k)
          enddo
          v(1,i)   = v(1,n)
          v(2,i)   = v(2,n)
          v(3,i)   = v(3,n)
40    continue
      nc = i

      if (ihg .eq. 7 .and. nc .eq. 24) go to 50
      if (ihg .eq. 5 .and. nc .eq. 48) go to 50

      write (ilog,60) icst(ihg),(ib(i),i=1,nc)
60    format (' The crystal system is ',a,' with operations:'/          &
     &2(3x,24i3/))
      goto 70

50    write (ilog,80) icst(ihg)
80    format (' The point group of the crystal is the full ',a,         &
     &' group')

!c     check symmorphic nature of group
70    vs = 0.0d0
      do n = 1,nc
        do i = 1,3
        vs = vs + abs(v(i,n))
        enddo
      enddo
      epsil = 0.0005d0
      epsil = eps
      if (vs .gt. epsil) go to 100
      write (ilog,*) 'the space group of the crystal is symmorphic'
      isy = 1
      return
100   write (ilog,110)
110   format (' The space group is non-symmorphic,'/                    &
     &' or else a non standard origin of coordinates was used.')
      isy = 0

      return
!EOC
      end subroutine spacegrp

!BOP
!!IROUTINE: checksymm
!!INTERFACE:
      subroutine checksymm(ilog,a,ai,if0,jf0,ib,r,v,natx,nat,nc)
!EOP
!
!BOC
      implicit real*8 (a-h,o-z)
      real(8), intent(in) :: a(3,3),ai(3,3)
      real(8), intent(in) :: v(3,48),r(49,3,3)
      integer, intent(in) :: ib(48)
      integer, intent(in) :: if0(48,*)
      integer, intent(out) :: jf0(natx,*)
      integer, intent(in) :: natx,nat,nc
      dimension tmp(3,3),multab(48,48)
      dimension r0(49,3,3)
      logical ltestf,ltest,lcheck
      character*10 :: sname = 'checksymm'
!      data eps /1.d-06/

!c===>    test the group theoretical information (m.w. 8-94)
      ltest=.true.
      ltestf=.true.

!c--->       test if0
      do j=1,nat
         do i=1,nat
            jf0(i,j)=0
         enddo
         do iopt=1,nc
            io=iopt
            in=if0(io,j)
            if(in.le.0.or.in.gt.natx) then
             write(*,*) ' CHECKSYMM: io=',io,' j=',j,' in=',in
             write(*,*) ' CHECKSYMM: natx=',natx
             call p_fstop(sname//' Wrong index IN')
            end if
            jf0(in,j)=jf0(in,j)+1
         enddo
      enddo

      do j=1,nat
       in=jf0(j,j)
       if(in.ne.0) then
!c--->       test that nc/(equivalent atoms) is an integer
         if(mod(nc,in).ne.0) then
            ltestf=.false.
         endif
!c
         is=0
         do i=1,nat
            is=is+jf0(i,j)
         enddo
!c--->       test that one atom is generated for each operation
         if(is.ne.nc) then
            ltestf=.false.
         endif
!c
         do i=1,nat
!c--->       test that all equivalent atoms are generated equally
            if(jf0(i,j).ne.0) then
               if(jf0(i,j).ne.in) then
                  ltestf=.false.
               endif
!c--->       test that all equivalent atoms are generated equivalently
               if(jf0(i,j).ne.jf0(j,i))then
                  ltestf=.false.
               endif
            endif
         enddo
       end if
      enddo

      if(.not.ltestf) then
        write(ilog,'(1x//,1x,70(''-'')/)')
        write(ilog,'(a)') ' ERROR: if0 is incorrect'
        write(ilog,'(a)') ' not-equivalent number of atoms (jf0):'
!         do j=1,nat
!            write(ilog,2000) j, (jf0(i,j),i=1,nat)
!         enddo
         write(ilog,'(1x/,1x,70(''-'')/)')
      endif
 2000 format(1x/,' Atom',i4,':',12i4/,(10x,12i4))
!c--->       test the closure of the group
!c-->           first put rotation matrices in lattice basis
      do iopt=1,nc
         in=ib(iopt)
         do j=1,3
            do i=1,3
               s=0.0d0
               do k=1,3
                  s=s+r(in,i,k)*a(k,j)
               enddo
               tmp(i,j)=s
            enddo
         enddo
         do j=1,3
            do i=1,3
               s=0.0d0
               do k=1,3
                  s=s+ai(i,k)*tmp(k,j)
               enddo
               r0(in,i,j)=s
            enddo
         enddo
      enddo
!c==>           set-up multiplication table
      do iopt=1,nc
         in=ib(iopt)
         do jopt=1,nc
            jn=ib(jopt)
            multab(jopt,iopt)=0
!c--->    get r0(in)r0(jn)
            do j=1,3
               do i=1,3
                  s=0.0d0
                  do k=1,3
                     s=s+r0(in,i,k)*r0(jn,k,j)
                  enddo
                  tmp(i,j)=s
               enddo
            enddo
!c--->    determine which element of the group this is
            do kopt=1,nc
               kn=ib(kopt)
               lcheck=.true.
               do kj=1,3
                do ki=1,3
                 if(abs(r0(kn,ki,kj)-tmp(ki,kj)).gt.eps) lcheck=.false.
                enddo
               enddo
               if(lcheck) then
                  multab(jopt,iopt)=kopt
                  irotn=kopt
                  go to 901
               endif
            enddo
            ltest=.false.
  901       continue
!c--->    check nonsymorphic part
            do i=1,3
               s=0.0d0
               do j=1,3
                  s=s+r0(in,i,j)*v(j,jopt)
               enddo
               s0=s+v(i,iopt)
               s=abs(s0-v(i,irotn))
               do itest=0,2
                  if(abs(s).lt.eps) go to 902
                  s=s-1.0d0
               enddo
               ltest=.false.
               multab(jopt,iopt)=-multab(jopt,iopt)
               go to 903
  902          continue
            enddo
  903       continue
         enddo
      enddo
!c--->       test the closure property
         mtot=(nc*(nc+1))/2
      do i=1,nc
         ms=0
         do j=1,nc
            ms=ms+abs(multab(j,i))
         enddo
         if(ms.ne.mtot) then
            ltest=.false.
         endif
      enddo
      do j=1,nc
         ms=0
         do i=1,nc
            ms=ms+abs(multab(j,i))
         enddo
         if(ms.ne.mtot) then
            ltest=.false.
         endif
      enddo
      if(ltest.and.ltestf) then
         write(ilog,*)
         write(ilog,904)
      else
         write(ilog,905)
         write(ilog,'(1x/)')
         if(.not.ltest) then
            write(ilog,'(a)') ' Multiplication table (- denotes'//      &
     &                     ' error in non-symmorphic part)'
            write(ilog,'(1x/)')
            do i=1,nc
               write(ilog,910) i,(multab(j,i),j=1,nc)
            enddo
         endif
      endif
  904 format(2x,'Symmetry appears to be correct')
  905 format(2x,'***** symmetry checks failed *********')
  910 format(1x/,' i=',i3,':',12i4/,(7x,12i4))
  911 format(5x,'( ',3f8.3,' )         ( ',f8.3,' )')
!c--->    write out the operations in lattice form:
      if((.not.ltest).or.(.not.ltestf)) then
         write(ilog,905)
         call p_fstop(sname)
         write(ilog,930)
         do iopt=1,nc
            in=ib(iopt)
            write(ilog,931) iopt,in
            do ii=1,3
               write(ilog,911) (r0(in,ii,jj),jj=1,3),v(ii,iopt)
            enddo
         enddo
         write(ilog,'(1x/)')
      endif
 930  format(1x/,70('-')/,' Operations given in lattice form')
 931  format(1x/,' operation number',i3,' (',i3,')')

!EOC
      end subroutine checksymm

      end subroutine genkpt1
!
!
!
!BOP
!!ROUTINE: pgl1
!!INTERFACE:
      subroutine pgl1 (a,ai,ihc,nc,ib,ihg,r)
!!DESCRIPTION:
! {\bv
! determines the point group of the lattice and the crystal system
! written on september 11th, 1979 - from acmi complex
!
! a ..... direct lattice vectors
! ai .... reciprocal lattice vectors
!
!      ihg .... point group of the primitive lattice, holohedral
!               group number:
!               ihg=1 stands for triclinic system
!               ihg=2 stands for monoclinic system
!               ihg=3 stands for orthorhombic system
!               ihg=4 stands for tetragonal system
!               ihg=5 stands for cubic system
!               ihg=6 stands for trigonal system
!               ihg=7 stands for hexagonal system
!      ihc .... code distinguishing between hexagonal and cubic
!               groups
!               ihc=0 stands for hexagonal groups
!               ihc=1 stands for cubic groups
! \ev}

!EOP
!
!BOC
      implicit none

      integer, intent(out) :: ihc,nc,ib(48),ihg
      real(8), intent(in) :: a(3,3),ai(3,3)
      real(8), intent(out):: r(49,3,3)
      real(8) :: vr(3),xa(3)

      integer :: i,j,k,lx,nr,n
      real(8) :: tr
      logical :: remove_trs=.true.
      real(8) :: trs_d(3)=dble([-1,-1,-1]),d3(3)

      character(72) c_env
      integer :: st_env,nrt,nrot,ib_ext(size(ib))
!c
      ib_ext = 0
      nrot = 0
      call get_environment_variable('ENV_NROT',value=c_env,             &
     &                                                  status=st_env)
      if ( st_env==0 ) then
       write(6,*) ' DEBUG: c_env='//trim(c_env)
       read(c_env,'(24i2)',err=1,end=1) ib_ext
1      continue
       do nrt=1,24
        if ( ib_ext(nrt)==0 ) exit
       end do
       nrot = max(1,nrt-1)
       ib_ext(nrot+1:) = 0
       write(6,*) '  NROT = ',nrot
      end if
!c-----------------------------------------------------------------------
      ihc = 0
!c     ihc is 0 for hexagonal groups and 1 for cubic groups.
      nr = 24
100   nc = 0
      call rot1 (ihc,r)
!      if ( remove_trs ) then   ! to remove time-reversal/inversion symmetry operation
!!       nr = nr/2
!       do i=1,nr
!        d3 = abs([r(i,1,1),r(i,2,2),r(i,3,3)]-trs_d)
!        if ( sum(d3)<3*epsilon(1.d0) ) then
!         r(i:nr-1,1:3,1:3) = r(i+1:nr,1:3,1:3)
!         exit
!        end if
!       end do
!       if ( nr<=i ) nr=nr-1
!      end if
      do 140 n = 1,nr
        ib(n) = 0
        tr = 0.0d0
!c       rotate the a1,2,3 vectors by rotation no. n
        do 130 k = 1,3
          do i = 1,3
            xa(i) = 0.0d0
            do j = 1,3
              xa(i) = xa(i) + r(n,i,j)*a(j,k)
            enddo
          enddo
          call rlv3 (ai,xa,vr,lx)
          do 120 i = 1,3
            tr = tr + abs(vr(i))
120         continue
!c         if vr.ne.0, then xa cannot be a multiple of a lattice vector
          if (tr .gt. 0.001d0) goto 140
130       continue
!c
        nc = nc + 1
        if ( nrot==0 ) then
         ib(nc) = n
        else
          do nrt=1,nrot
           if ( ib_ext(nrt)==n ) then
            ib(nc) = n
            exit
           end if
          end do
        end if
140     continue

!c
!c     ihg stands for holohedral group number.
      if (ihc .eq. 0)  go to 200
!c     cubic group:
      if (nc  .lt. 4)  ihg = 1
      if (nc  .eq. 4)  ihg = 2
      if (nc  .gt. 4)  ihg = 3
      if (nc  .eq. 16) ihg = 4
      if (nc  .gt. 16) ihg = 5
      return
!c
!c     hexagonal group:
200   if (nc  .eq. 12) ihg = 6
      if (nc  .gt. 12) ihg = 7
      if (nc  .ge. 12) return
!c     too few operations, try cubic group:
      nr  = 48
      ihc = 1
      goto 100
!EOC
      end subroutine pgl1

!BOP
!!ROUTINE: rot1
!!INTERFACE:
      subroutine rot1 (ihc,r)
!!DESCRIPTION:
! {\bv
! generation of the x,y,z-transformation matrices 3x3 for hexagonal and
! cubic groups
!
! written on february 17th, 1976
!
! this is identical with the subroutine rot of worlton-warren (in the
! ac-complex), only the way of transferring the data was changed
! input data
!      ihc...switch determining if we desire the hexagonal group (ihc=0)
!            or the cubic group (ihc=1)
! output data
!      r...the 3x3 matrices of the desired coordinate representation
!          their numbering corresponds to the symmetry elements as listed
!          in worlton-warren
!          for ihc=0 the first 24 matrices of the array r represent
!                                          the full hexagonal group d(6h)
!          for ihc=1 the first 48 matrices of the array r represent
!                                          the full cubic group o(h)
! \ev}

!EOP
!
!BOC
      implicit none
      integer, intent(in) :: ihc
      real(8), intent(out) :: r(49,3,3)
      integer :: i,j,k,n,nv
      real(8) :: f
!c-----------------------------------------------------------------------
      do j=1,3
      do i=1,3
      do n=1,49
        r(n,i,j)=0.d0
      enddo
      enddo
      enddo
      if (ihc .gt. 0) goto 160
!c
!c     define the generators for the rotation matrices--hexagonal group
!c
      f = 0.5d0*dsqrt(3.0d0)
      r(2,1,1) = 0.5d0
      r(2,1,2) = - f
      r(2,2,1) = f
      r(2,2,2) = 0.5d0
      r(7,1,1) = -0.5d0
      r(7,1,2) = -f
      r(7,2,1) = -f
      r(7,2,2) = 0.5d0
      do 120 n      = 1,6
        r(n,3,3)    = 1.d0
        r(n+18,3,3) = 1.d0
        r(n+6,3,3)  = - 1.d0
        r(n+12,3,3) = - 1.d0
120     continue
!c
!c     generate the rest of the rotation matrices
!c
      do i = 1,2
        r(1,i,i) = 1.d0
        do j = 1,2
          r(6,i,j) = r(2,j,i)
          do k = 1,2
            r(3,i,j)  = r(3,i,j) +  r(2,i,k)*r(2,k,j)
            r(8,i,j)  = r(8,i,j) +  r(2,i,k)*r(7,k,j)
            r(12,i,j) = r(12,i,j) + r(7,i,k)*r(2,k,j)
          enddo
        enddo
      enddo
      do i = 1,2
        do j = 1,2
          r(5,i,j) = r(3,j,i)
          do k = 1,2
            r(4,i,j)  = r(4,i,j)  + r(2,i,k)*r(3,k,j)
            r(9,i,j)  = r(9,i,j)  + r(2,i,k)*r(8,k,j)
            r(10,i,j) = r(10,i,j) + r(12,i,k)*r(3,k,j)
            r(11,i,j) = r(11,i,j) + r(12,i,k)*r(2,k,j)
          enddo
        enddo
      enddo

      do n = 1,12
        nv = n + 12
        do i = 1,2
          do j = 1,2
            r(nv,i,j) = - r(n,i,j)
          enddo
        enddo
      enddo
!c
      return
!c
!c     define the generators for the rotation matrices--cubic group
!c
160   r(9,1,3)  = 1.d0
      r(9,2,1)  = 1.d0
      r(9,3,2)  = 1.d0
      r(19,1,1) = 1.d0
      r(19,2,3) = - 1.d0
      r(19,3,2) = 1.d0
      do i = 1,3
        r(1,i,i) = 1.d0
        do j = 1,3
          r(20,i,j) = r(19,j,i)
          r(5,i,j)  = r(9,j,i)
          do k  = 1,3
            r(2,i,j)  = r(2,i,j)  + r(19,i,k)*r(19,k,j)
            r(16,i,j) = r(16,i,j) + r(9,i,k)*r(19,k,j)
            r(23,i,j) = r(23,i,j) + r(19,i,k)*r(9,k,j)
          enddo
        enddo
      enddo
      do i = 1,3
      do j = 1,3
      do k = 1,3
        r(6,i,j)  = r(6,i,j)  + r(2,i,k)*r(5,k,j)
        r(7,i,j)  = r(7,i,j)  + r(16,i,k)*r(23,k,j)
        r(8,i,j)  = r(8,i,j)  + r(5,i,k)*r(2,k,j)
        r(10,i,j) = r(10,i,j) + r(2,i,k)*r(9,k,j)
        r(11,i,j) = r(11,i,j) + r(9,i,k)*r(2,k,j)
        r(12,i,j) = r(12,i,j) + r(23,i,k)*r(16,k,j)
        r(14,i,j) = r(14,i,j) + r(16,i,k)*r(2,k,j)
        r(15,i,j) = r(15,i,j) + r(2,i,k)*r(16,k,j)
        r(22,i,j) = r(22,i,j) + r(23,i,k)*r(2,k,j)
        r(24,i,j) = r(24,i,j) + r(2,i,k)*r(23,k,j)
      enddo
      enddo
      enddo
      do i=1,3
      do j=1,3
      do k=1,3
        r(3,i,j)  = r(3,i,j)  + r(5,i,k)*r(12,k,j)
        r(4,i,j)  = r(4,i,j)  + r(5,i,k)*r(10,k,j)
        r(13,i,j) = r(13,i,j) + r(23,i,k)*r(11,k,j)
        r(17,i,j) = r(17,i,j) + r(16,i,k)*r(12,k,j)
        r(18,i,j) = r(18,i,j) + r(16,i,k)*r(10,k,j)
        r(21,i,j) = r(21,i,j) + r(12,i,k)*r(15,k,j)
      enddo
      enddo
      enddo

      do n = 1,24
        nv = n + 24
        do i = 1,3
        do j = 1,3
          r(nv,i,j) = - r(n,i,j)
        enddo
        enddo
      enddo

      return
!EOC
      end subroutine rot1

!BOP
!!ROUTINE: rlv3
!!INTERFACE:
      subroutine rlv3 (ai,xb,vr,il)
!!DESCRIPTION:
! {\bv
!  subroutine rlv removes a direct lattice vector from xb leaving the
!  remainder in vr.  if a nonzero lattice vector was removed, il is
!  made nonzero.  vr stands for v-reference.
!  ai(i,j) are the reciprocal lattice vectors,
!  b(i) = ai(i,j),j=1,2,3
!  vr is not given in cartesian coordinates, but
!  in the system a1,a2,a3.     k.k., 23.10.1979
! \ev}
!EOP
!
!BOC
      implicit none
      real(8), intent(in) :: ai(3,3),xb(3)
      real(8), intent(out) :: vr(3)
!c            written on september 11th, 1979 - from acmi complex
      real(8) :: del(3)
      real(8), parameter :: eps = 1.0d-6
!c-----------------------------------------------------------------------
      integer :: i,il,j
      real(8) :: ts
!c-----------------------------------------------------------------------
      il = 0
      ts = 0.d0
      do 100 i = 1,3
        del(i) = 0.d0
        vr(i) = 0.d0
        ts = ts + abs(xb(i))
100     continue
      if (ts .le. eps) return
      do 120 i = 1,3
        do 110 j = 1,3
          vr(i) = vr(i) + ai(i,j)*xb(j)
110       continue
      if (vr(i) .gt.  0.9d0) del(i) = eps
      if (vr(i) .lt. -0.9d0) del(i) = - eps
!c     del is added in to eliminate roundoff errors in the function amod.
      vr(i) = vr(i) + del(i)
      il    = il + abs(vr(i))
      vr(i) = - mod(vr(i),1.0d0)
      vr(i) = vr(i) + del(i)
120   continue
      return
!EOC
      end subroutine rlv3
