!BOP
!!ROUTINE: entrpterm
!!INTERFACE:
      subroutine entrpterm(Tempr,efermi,egrd,                           &
     &                     dele1,nume,                                  &
     &                     dostspn,                                     &
     &                     dostef,nspin,                                &
     &                     avnatom,entropy,iprint,istop)
!!DESCRIPTION:
! calculates entropy per cell
!  $= -\int de n(e) ( f*log(f) + (1-f) log(1-f) ) $ \\
!  (and prints {\tt Tempr*entropy})
!
!!USES:
      use universal_const

!!DO_NOT_PRINT
      implicit none
!!DO_NOT_PRINT

!!ARGUMENTS:
      real(8), intent(in) :: Tempr,efermi
      integer, intent(in) :: nume
      complex(8), intent(in) :: egrd(:),dele1(:)
      complex(8), intent(in) :: dostspn(:,:)
      complex(8), intent(in) :: dostef(:)
      integer, intent(in) :: nspin
      real(8), intent(in) :: avnatom
      real(8), intent(out) :: entropy
      integer, intent(in) :: iprint
      character(*), intent(in) :: istop
!!REVISION HISTORY:
! Initial Version - A.S. - 2013
!EOP
!
!BOC
      character(*), parameter :: sname='entrpterm'
!c
      complex(8), external :: TemprFun
      complex(8) ::  ztmp,ffz,f0
      real(8) ::  dosef
      real(8) ::  somm_estim_entropy
      integer ie,is

!      pi = 4.d0*atan(1.d0)
      entropy = zero
      if(Tempr.le.zero) return

      dosef = zero
      do is=1,nspin
       do ie=1,nume
!
        if ( aimag(dostspn(is,ie)) <= zero ) cycle
!
        if ( abs(dele1(ie)) > epsilon(one) ) then
         ztmp = (egrd(ie) - efermi)/Tempr
         ffz = TemprFun(ztmp,1)
         f0  = TemprFun(ztmp,0)
         if ( abs(f0)>epsilon(one) .and. abs(ffz)>epsilon(one) ) then
           entropy = entropy + aimag(dostspn(is,ie)*ffz*(dele1(ie)/f0))
         end if
        end if
       end do
       dosef = dosef + aimag(dostef(is))
      enddo
      if ( nspin==1 ) then
       entropy = entropy+entropy
       dosef   = dosef+dosef
      end if
!
      somm_estim_entropy = Tempr*dosef/avnatom*(pi**2)/3  !  T-first order



      if(iprint.ge.-1) then
       if ( dosef > zero ) then
        write(6,'(''        sommerfeld estimation & entropy:'',         &
     & t40,''='',f16.11,2x,'' vs '',f16.11)') somm_estim_entropy,entropy
       end if
       write(6,'(''             DOS(Ef) per atom:'',                    &
     &     t40,''='',f16.11)') dosef/avnatom
       write(6,'(''     +++++++++++++++++++++++++++++++++++'',          &
     &           ''++++++++++++++++++++++++++++++++++'')')
!       if ( dosef > zero ) then
!        write(6,'(''        Sommerfeld estimation:'',                   &
!     &  t40,''='',f16.11)') -Tempr*Tempr*dosef/avnatom*pi**2/3
!       end if
!       write(6,'(''     +++++++++++++++++++++++++++++++++++'',          &
!     &           ''++++++++++++++++++++++++++++++++++'')')

      endif

      if ( efermi > zero ) then
       if ( Tempr/efermi < 0.025d0 ) entropy = somm_estim_entropy
      end if

!c    *************************************************************
      if(istop.eq.sname) call p_fstop(sname)
!c    *************************************************************

      return
!EOC
      end subroutine entrpterm
