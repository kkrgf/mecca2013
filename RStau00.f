!BOP
!!ROUTINE: RStau00
!!INTERFACE:
      subroutine RStau00(                                               &
     &                 lmax,ndkkr,nstot,                                &
     &                 rytodu,aij,itype,natom,iorig,                    &
     &                 mapstr,ndimbas,                                  &
     &                 pdu,                                             &
     &                 naij,                                            &
     &                 tcpa,                                            &
     &                 tau00,                                           &
     &                 rslatt,Rnncut,Rsmall,                            &
     &                 r_ref,                                           &
     &                 isprs,                                           &
     &                 iprint,istop)
!!DESCRIPTION:
! computes tau00 in real space \\
! RS-screening + RS-inversion ("screened LSMS")
!
!!USES:
      use universal_const
      use mtrx_interface
      use screening, only : calcTref,clstrRegEst
      use screening, only : g_scr_potpar,g_calcref
!DEBUGPRINT
      use sparse, only : wrt1ccs,wrt2ccs
!DEBUG

!!DO_NOT_PRINT
      implicit none
!!DO_NOT_PRINT

!!ARGUMENTS:
      integer, intent(in) :: lmax,ndkkr
      integer, intent(in) :: nstot
      real(8), intent(in) :: rytodu
      integer, intent(in) :: naij
      real(8), intent(in) :: aij(3,naij)
      integer, intent(in) :: natom,itype(natom)
      integer, intent(in) :: iorig(nstot)
      integer, intent(in) :: ndimbas
      integer, intent(in) :: mapstr(ndimbas,natom)
      complex(8), intent(in) :: pdu    ! momentum in D.U.
      complex(8) :: tcpa(ndkkr,ndkkr,natom)
      complex(8), intent(out) :: tau00(ndkkr,ndkkr,nstot)
      real(8), intent(in) :: rslatt(3,3)   ! Alat*rslatt(1..3,*)/(2*pi) -- in <at.un.>/(2*pi)
      real(8), intent(in) :: Rnncut,Rsmall ! in at.unit.
      real(8), intent(in) :: r_ref(:)
!      complex(8), intent(out) :: deltat(ndkkr,ndkkr,*)
!      complex(8), intent(out) :: deltinv(ndkkr,ndkkr,*)
      integer, intent(in) :: isprs(:)
      integer, intent(in) :: iprint
      character(10), intent(in) :: istop
!
!!REVISION HISTORY:
! Adapted - A.S. - 2013
!EOP
!
!BOC
      real*8 dutory
!c
!c      complex*16 taun1n2,tautmp

!c
!c     +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
      character(*), parameter :: sname='RStau00'
!c     +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

!      complex*16 cone,czero
!      parameter (czero=(0.d0,0.d0),cone=(1.d0,0.d0))
      integer nlatt(3)
      integer erralloc
!c      complex*16, allocatable  ::  greenrs(:,:)

      complex(8) :: wfac

!C      integer  isprs(3)
!c      integer  numnbi(ipsublat),nsmall(ipsublat),nrsij(ipsublat)

      integer, allocatable  ::  ntau(:,:)
      integer, allocatable  ::  jclust(:,:)
      integer, allocatable  ::  ntypecl(:,:)
      integer, allocatable  ::  mappnni(:,:),mapnnij(:,:)  !  working arrays
      integer, allocatable  ::  mapsnni(:,:)
      real(8), allocatable  ::  rsij(:,:)
      real(8), allocatable  ::  rijcl(:,:)
      complex(8), allocatable :: trefinv(:,:,:)
      complex(8), allocatable :: tref(:,:,:)
      complex(8), allocatable :: d_trefinv(:,:)
      complex(8), allocatable :: d_tref(:,:)

!c      integer  nnptr(0:1,ipbase)
!c      integer, allocatable  ::   nnclmn(:,:,:)

!c      logical  ialloc

      integer :: invswitch,invparam=0
      integer :: kkrsz,ndmatr,iatom,is,j,nsub
      real(8) :: d_nn
!      real(8) : time1,time2
      real(8) :: Rsm_2pi,Rnn_2pi
      integer :: icalcref


      integer :: ndimnn,nsubsend,nsubrecv,ntmp
      integer, parameter :: itag1=11,itag2=21,itag3=31,itag4=41

!      call timel(time2)

      if (isprs(1)==0 ) then
       invswitch = 1       !  1 -> LAPACK, 2 -> Sparse matrix inversion
      else
       invswitch = 2
      end if

      kkrsz = (lmax+1)**2

      allocate(tref(ndkkr,ndkkr,nstot))
      allocate(trefinv(ndkkr,ndkkr,nstot))

!c.......................................................................

      ndmatr = kkrsz*natom
!c      ndmtr1 = ndmatr*ndmatr

      call zerooutC(tref,ndkkr*ndkkr*nstot)
      call zerooutC(trefinv,ndkkr*ndkkr*nstot)
      call zerooutC(tau00,ndkkr*ndkkr*nstot)
      dutory= one/rytodu
      Rnn_2pi = Rnncut*dutory
      Rsm_2pi = Rsmall*dutory
      wfac = dcmplx(rytodu,zero)
      icalcref = g_calcref()

      tcpa = dutory * tcpa

!      r3ws = 0
!      do nsub=1,nstot
!        do nk=1,komp(nsub)
!         r3ws = r3ws + atcon(nk,nsub)*numbsub(nsub)*rws(nk,nsub)**3
!        enddo
!      end do
!      r3ws = r3ws/natom
!      rmt0 = r3ws**(one/three)


!------------------------------------------------------------------------
      allocate(d_tref(ndkkr,nstot))
      allocate(d_trefinv(ndkkr,nstot))
      call calcTref(nstot,lmax,pdu,rytodu,r_ref,icalcref,               &
     &                                      d_tref,d_trefinv,iprint>=0)
      do j=1,kkrsz
       tref(j,j,1:nstot) = d_tref(j,1:nstot)
       trefinv(j,j,1:nstot) = d_trefinv(j,1:nstot)
      end do
      deallocate(d_tref,d_trefinv)
!
!  Now trefinv[1:kkrsz] = tref^(-1) -- in D.U.
!
      ntmp = 1

      call clstrRegEst(rslatt,Rnn_2pi,ndimnn,nlatt)
!c      write(6,*) ' nndim=',ndimnn
      ndimnn = 1 + ndimnn*natom

      allocate(                                                         &
     &   rsij(1:ndimnn*ndimnn*3,1:ntmp),                                &
     &   mapsnni(1:ndimnn*ndimnn,1:ntmp),                               &
     &   mappnni(1:ndimnn*ndimnn,1:ntmp),                               &
     &   mapnnij(1:ndimnn*ndimnn*2,1:ntmp),                             &
     &   ntau(1:ndimnn,1:ntmp),                                         &
     &   jclust(1:ndimnn,1:ntmp),                                       &
     &   ntypecl(1:ndimnn,1:ntmp),                                      &
     &   rijcl(1:3,ndimnn*ndimnn),                                      &
     &  stat=erralloc)


      if(erralloc.ne.0) then
        write(6,*) '     NTMP=',ntmp
        write(6,*) '   NDIMNN=',ndimnn
        write(6,*) '    NATOM=',natom
        write(6,*) '   RNNCUT=',Rnncut
        write(6,*) '   D_NN=',d_nn
        write(6,*) ' ERRALLOC=',erralloc
        call fstop(sname//' :: ALLOCATION MEMORY PROBLEM')
      end if

      nsub = 0
      is = 0
      nsubsend = 0
      nsubrecv = 0

       do nsub=1,nstot
         iatom = iorig(nsub)
         call RStausub(iatom,natom,lmax,itype,                          &
     &                 mapstr,ndimbas,                                  &
     &                 aij,naij,                                        &
     &                 Rnn_2pi,Rsm_2pi,                                 &
     &                 rslatt,nlatt,                                    &
     &                                     pdu,wfac,                    &
     &                 tcpa,tref,trefinv,tau00(1,1,nsub),ndkkr,         &
     &                 ndimnn,mapsnni,mappnni,mapnnij,                  &
     &                 ntau,jclust,ntypecl,rsij,rijcl,                  &
     &                 invswitch,invparam,                              &
     &                 iprint,istop)
       end do

      deallocate(                                                       &
     &            rsij,                                                 &
     &            mapsnni,                                              &
     &            mappnni,                                              &
     &            mapnnij,                                              &
     &            ntau,                                                 &
     &            jclust,                                               &
     &            ntypecl,                                              &
     &            rijcl                                                 &
     &            )
      deallocate(tref,trefinv)

      tcpa = rytodu * tcpa
!
!      ==============================================================
!      if(iprint.ge.1) then
!        write(6,1001) sname//':: TAU00 ::',pdu
!1001    format(2x,a22,2d18.10,1x,2d18.10)
!         do nsub = 1,nstot
!          write(6,*) sname//':: nsub=',nsub
!          call wrtmtx(tau00(:,:,nsub),kkrsz,istop)
!         enddo
!      endif

!      time1 = time2
!      call timel(time2)
!      if(iprint.ge.0) then
!        write(6,*) sname//' :: RS-time=',real(time2-time1)              &
!     &            ,real(time2-time1)/nstot
!      end if

!c     ==============================================================
      if( istop .eq. sname ) then
        call fstop(sname)
      endif
!c
      return
!EOC
      contains

!BOP
!!IROUTINE: RStausub
!!INTERFACE:
      subroutine RStausub(iatom,natom,lmax,itype,                       &
     &                    mapstr,ndimbas,                               &
     &                    aij,naij,                                     &
     &                    Rnncut,Rsmall,                                &
     &                    rslatt,nlatt,                                 &
     &                                        pdu,wfac,                 &
     &                    tcpa,tref,trefinv,tau00,ndkkr,                &
     &                    ndimnn,mapsnni,mappnni,mapnnij,               &
     &                    ntau,jclust,ntypecl,rsij,rijcl,               &
     &                    invswitch,invparam,                           &
     &                    iprint,istop)
!!DESCRIPTION:
! computes tau00 in real space for site {\tt iatom} \\
! (engine of {\em RStau00} subroutine) 
!
!!USES:
      use universal_const
      use mtrx_interface
      use freespaceG, only : gf0_rs
      use sparse, only : inv1sprs,g_ge21mgt
!
!EOP
!
!BOC
      implicit none
      integer, intent(in) :: iatom,natom,lmax,itype(natom)
      integer, intent(in) :: ndimbas
      integer, intent(in) :: mapstr(ndimbas,natom)
      integer, intent(in) :: naij
      real(8), intent(in) :: aij(3,naij)
      real(8), intent(in) :: Rnncut,Rsmall         ! in at.un.*(2*pi)
      real(8), intent(in) :: rslatt(3,3)           ! Alat*rslatt(1..3,*)*(2*pi) -- in at.un.
      integer, intent(in) :: nlatt(3)
      complex(8), intent(in) :: pdu,wfac
      integer, intent(in) :: ndkkr
      complex(8), intent(in) :: tcpa(ndkkr,ndkkr,natom)
      complex(8), intent(in) :: tref(ndkkr,ndkkr,*)
      complex(8), intent(in) :: trefinv(ndkkr,ndkkr,*)
!
      complex(8), intent(out) :: tau00(ndkkr,ndkkr,*)
!
      integer, intent(in) :: ndimnn
      integer, intent(in) :: mapsnni(*),mappnni(*),mapnnij(*)
      integer, intent(in) :: ntau(*),jclust(*),ntypecl(*)
      real(8), intent(in) :: rsij(*),rijcl(1:3,*)
!DELETE      complex(8), intent(in) :: deltat(ndkkr,ndkkr,*)
!DELETE      complex(8), intent(in) :: deltinv(ndkkr,ndkkr,*)
      integer, intent(in) :: invswitch,invparam
      integer, intent(in) :: iprint
      character(10), intent(in) :: istop
!c
      character(10), parameter ::  sname='RStausub'
!c
      integer erralloc
      complex(8), allocatable  ::  greenRS(:)

      complex(8), allocatable :: deltat(:,:,:)   ! (kkrsz,kkrsz,*)
      complex(8), allocatable :: deltinv(:,:)    ! (kkrsz,kkrsz)
      complex(8), allocatable :: wsmall(:)       ! kkrsz*kkrsz

      integer ::  kkrsz,numnbi,nsmall,nrsij,ndmtr3
      integer ::  j

      real(8), parameter :: twopi=2*pi

      kkrsz = (lmax+1)**2

      if ( float(kkrsz*ndimnn)**2 > float(huge(0)) ) then
       write(6,*) ' kkrsz=',kkrsz,' ndimnn=',ndimnn
       write(6,*) ' (kkrsz*ndimnn)**2=',(float(kkrsz)*ndimnn)**2
       call fstop(sname//' :: MATRIX SIZE IS TOO LARGE'//               &
     &                   ' (integer*8 is required)')
      end if

!c=======================================================================
!c   Calculation of NN-cluster for site "iatom"
!c=======================================================================
       numnbi = ndimnn
       call nncluster(iatom,natom,mapstr,ndimbas,                       &
     &   aij,Rnncut,Rsmall,rslatt,itype,nlatt,                          &
     &   numnbi,nsmall,                                                 &
     &   jclust(1),ntypecl(1),ntau(1),                                  &
     &   rsij(1),nrsij,                                                 &
     &   mapsnni(1),ndimnn,mappnni(1),mapnnij(1)                        &
     &   ,rijcl(1,1)                                                    &
     &    )

       if(iprint.ge.0) then
         write(6,'('' IATOM='',i5,                                      &
     &            '' NUMNBI='',i4,'' Rnncut='',f5.2,                    &
     &            '' NSMALL='',i4,'' Rsmall='',f5.2)')                  &
     &    iatom,numnbi,Rnncut/twopi,                                    &
     &    nsmall,Rsmall/twopi
       end if

       if(max(nsmall,numnbi).gt.ndimnn) then
         write(6,'('' IATOM='',i5,                                      &
     &            '' NUMNBI='',i4,'' Rnncut='',f5.2,                    &
     &            '' NSMALL='',i4,'' Rsmall='',f5.2)')                  &
     &    iatom,numnbi,Rnncut/twopi,                                    &
     &    nsmall,Rsmall/twopi
         write(6,'('' NDIMNN='',i5)') ndimnn

          call fstop(sname//':: NDIMNN is too small')

       end if

       ndmtr3 = (kkrsz*numnbi)*(kkrsz*numnbi)
       allocate(greenrs(1:ndmtr3),                                      &
     &                   stat=erralloc)

       if(erralloc.ne.0) then
        write(6,*) ' KKRSZ=',kkrsz
        write(6,*) ' INVSWITCH=',invswitch
        write(6,*) ' NDMTR3=',ndmtr3
        write(6,*) ' ERRALLOC=',erralloc
        call fstop(sname//' :: ALLOCATION MEMORY PROBLEM')
       end if


        call gf0_rs(lmax,pdu,                                           &
     &          greenRS(1),                                             &
     &          ndimnn,numnbi,                                          &
     &          rsij(1),mapsnni(1),                                     &
     &          1,                                                      & ! 1 - complex matrix
     &          istop)

         call calgreen(                                                 &
     &              greenRS(1),                                         &
     &              kkrsz,numnbi,                                       &
     &              tref,trefinv,size(tref,1),0,ntypecl(1),             &
     &              invswitch,invparam,                                 &
     &              iprint,istop)

!CDEBUG        if(nsmall.lt.numnbi) then
!CDEBUG
!CDEBUGc  Calculation of DeltaT = t - t_ref ;   t = tref_new
!CDEBUG
!CDEBUG         do inn=1,numnbi
!CDEBUG          nnsub = ntypecl(inn)
!CDEBUG          nnatom = jclust(inn)
!CDEBUG
!CDEBUG          if(inn.le.nsmall) then
!CDEBUG           call zeroout(deltat(1,1,nnatom),2*ndkkr*ndkkr)
!CDEBUGCDEBUG
!CDEBUG           do j=1,kkrsz
!CDEBUG            deltat(j,j,nnatom) = (1.d-14,0.d0)
!CDEBUG           end do
!CDEBUGCDEBUG
!CDEBUG          else
!CDEBUG           do j=1,kkrsz
!CDEBUG            do i=1,kkrsz
!CDEBUG             deltat(i,j,nnatom) = tcpa(i,j,nnatom)-tref(i,j,nnsub)
!CDEBUG     *                                            -(1.d-14,0.d0)
!CDEBUG            end do
!CDEBUG           end do
!CDEBUG          end if
!CDEBUG
!CDEBUG          do j=1,kkrsz
!CDEBUG           call zcopy(kkrsz,deltat(j,1,nnatom),ndkkr,wsmall(j),kkr !sz)
!CDEBUG          end do
!CDEBUG
!CDEBUG          call invmatr(wsmall(1),logdet,
!CDEBUG     >            kkrsz,1,
!CDEBUG     *            1,1,
!CDEBUG     >            iprint,istop)
!CDEBUG
!CDEBUG          do j=1,kkrsz
!CDEBUG           call zcopy(kkrsz,wsmall(j),kkrsz,deltinv(j,1,nnatom),nd !kkr)
!CDEBUG          end do
!CDEBUG
!CDEBUG         end do
!CDEBUG
!CDEBUG         call calgreen(
!CDEBUG     *              greenrs(1),
!CDEBUG     *              kkrsz,numnbi,
!CDEBUG     *              deltat,deltinv,ndkkr,0,jclust(1),
!CDEBUGCDEBUG  mapij,mapstr,mappnt,naij,ndimbas, -- are not used !!!
!CDEBUG     *              mapij,mapstr,mappnt,naij,ndimbas,
!CDEBUG     *              invswitch,invparam,
!CDEBUG     *              iprint,istop)
!CDEBUG
!CDEBUG
!CDEBUG         do inn=nsmall+1,numnbi
!CDEBUG          nnsub = ntypecl(inn)
!CDEBUG          nnatom = jclust(inn)
!CDEBUG
!CDEBUG          call zeroout(deltat(1,1,nnatom),2*ndkkr*ndkkr)
!CDEBUGCDEBUG
!CDEBUG           do j=1,kkrsz
!CDEBUG            deltat(j,j,nnatom) = (1.d-14,0.d0)
!CDEBUG           end do
!CDEBUGCDEBUG
!CDEBUG
!CDEBUG         end do
!CDEBUG
!CDEBUG        end if

        allocate(deltat(kkrsz,kkrsz,numnbi),wsmall(kkrsz*kkrsz))
        call zerooutc(deltat,size(deltat))

!c  Calculation of DeltaT = t - t_ref

        deltat(1:kkrsz,1:kkrsz,1:numnbi) =                              &
     &         tcpa(1:kkrsz,1:kkrsz,ntypecl(1:numnbi)) -                &
     &              tref(1:kkrsz,1:kkrsz,jclust(1:numnbi))

!c=======================================================================
!c  Now tcpa = t, DeltaT = t-tref (in DU)
!c=======================================================================

!         allocate(deltinv(kkrsz,kkrsz,numnbi))
         allocate(deltinv(kkrsz,kkrsz))
         deltinv(1:kkrsz,1:kkrsz) = deltat(1:kkrsz,1:kkrsz,1)
         call invmatr(deltinv,kkrsz,1,1,1,iprint,istop)

!c=======================================================================
!c  Now deltinv = DeltaT^(-1) = (t-tref)^(-1),
!c      greenRS = Gref
!c=======================================================================

! Dyson equation for structural matrix G:
!  G = Gref * A^(-1)
!  A = I - Gref * (t-tref)

         if ( invswitch==1 ) then
          call inv1mgt(greenRS,kkrsz*numnbi,kkrsz,numnbi,deltat,        &
     &                  size(deltat,1),0,jclust,                        &
     &                  1,invparam,iprint,istop)
         else if ( invswitch==2 ) then
          call sp_inv1mgt(greenRS,kkrsz,numnbi,deltat,size(deltat,1))
         end if

!c=======================================================================
!c  Now  greenRS = A^(-1) = (1-Gref*DeltaT)^(-1)
!c=======================================================================
!DEBUGPRINT
!DELETE        write(6,*) ' DEBUG A^(-1):'
!DELETE         call wrt2ccs(kkrsz,greenRS(1),1.d-9)
!DEBUGPRINT

         do j=1,kkrsz
          call zcopy(kkrsz,deltinv(j,1),size(deltinv,1),wsmall(j),kkrsz)
         end do
         call zgemm('N','N',kkrsz,kkrsz,kkrsz,cone,                     &
     &            greenRS,kkrsz*numnbi,                                 &
     &            wsmall,kkrsz,                                         &
     &            -cone,deltinv,size(deltinv,1))

!c=======================================================================
! deltinv = G = A^(-1)*(1-A)*DeltaT^(-1) = A^(-1)*DeltaT(-1)-DeltaT(-1)
!c=======================================================================

         call gr2tauz(                                                  &
     &        deltinv,size(deltinv,1),tcpa,size(tcpa,1),jclust,         &
     &              1,kkrsz,tau00)

!c=======================================================================
!c  Now  tau00 = t + t*G*t
!c=======================================================================


        if ( allocated(greenRS) ) deallocate(greenRS)
        if ( allocated(deltat) ) deallocate(deltat)
        if ( allocated(deltinv) ) deallocate(deltinv)
        if ( allocated(wsmall) ) deallocate(wsmall)

        tau00(1:kkrsz,1:kkrsz,1) = tau00(1:kkrsz,1:kkrsz,1) * wfac

!DEBUGPRINT
!        write(6,1001) sname//' :: TAU00 ::',pdu
!1001    format(2x,a22,2d18.10,1x,2d18.10)
!         call wrtmtx(tau00(:,:,1),kkrsz,istop)
!        stop 'DEBUG RStausub return'
!DEBUGPRINT

        return
!EOC
        end subroutine RStausub

!BOP
!!ROUTINE: sp_inv1mgt
!!INTERFACE:
      subroutine sp_inv1mgt(greens,kkrsz,natom,T,ndkkr)
!!DESCRIPTION:
! {\bv
! Sparse-analogue of subroutine inv1mgt
!
! input:
!         greens -- zero-Green's function (G0), order of KKRSZ*NATOM,
!         T -- block-diagonal (KKRSZ x KKRSZ) of t-matrix, T(1:ndkkr,1:ndkkr,1:natom),
!         Tinv -- block-diagonal (KKRSZ x KKRSZ) of T^(-1) for first atom,
!         ndkkr -- rank of Tinv matrix
!
! output:
!         greens -- Green's function for first atom,
!                == [1-G0*t]^(-1)
! \ev}

!!USES:
      use sparse, only : inv1sprs,g_ge21mgt
      use sparse, only : wrt2ccs
!!DO_NOT_PRINT
      implicit none
!!DO_NOT_PRINT

!!ARGUMENTS:
      complex(8), intent(inout) :: greens(*)
      integer, intent(in) :: kkrsz,natom,ndkkr
      complex(8), intent(in) :: T(ndkkr,ndkkr,natom)
!!REVISION HISTORY:
! Initial - A.S. - 2018
!EOP
!
!BOC
      integer :: ne
      integer, pointer :: iptcol(:)
      integer, pointer :: indrow(:)=> null()
      complex(8), pointer :: zvalue(:)=> null()
      complex(8) :: green1(ndkkr,ndkkr)
      integer :: j,lj

!c     ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
      character(10), parameter :: sname='sp_inv1mgt'
!c     ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

      allocate(iptcol(kkrsz*natom+1))

      call g_ge21mgt(greens,kkrsz,natom,T,ndkkr,zvalue,                 &
     &                     iptcol,indrow,ne)
!  Now zvalue = [1-G*deltat]
      call inv1sprs(                                                    &
     &              zvalue,iptcol,indrow,                               &
     &              kkrsz,natom,T(1:ndkkr,1:ndkkr,1:natom),             &
     &              ndkkr,green1,                                       &
     &              0)
!c  Now greens = [1-G*t]^(-1) (first block-diagonal only)
      if ( associated(zvalue) ) deallocate(zvalue)
      if ( associated(indrow) ) deallocate(indrow)
      if ( associated(iptcol) ) deallocate(iptcol)

      do j=1,kkrsz
       lj = (j-1)*(kkrsz*natom)
       greens(lj+1:lj+kkrsz) = green1(1:kkrsz,j)
      end do

      return
!EOC
      end subroutine sp_inv1mgt

      end subroutine RStau00
