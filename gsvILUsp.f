      subroutine gsvILUsp(a,ndmat,                                      &
     &                   nnptr,nnclmn,ndimnn,bmt,                       &
     &                   cof,ndcof,                                     &
     &                   itype,kkrsz,natom,precond,                     &
     &                   invparam,niter,tolinv,iprint)
!c     ================================================================
!c
      implicit real*8(a-h,o-z)
!c
!c      To find solution of equation:  A*X=1  (A=1-G*t)
!c      using iterative Transpose-Free-Quasi-Residual Algorithm
!c
!c      ILU-preconditioner is used
!c
!c      and then output:  cof = X  (tau = t*cof)
!c
!c      niter -- maximum number of iterations
!c
!c     ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!c
      integer ndmat,natom,kkrsz
      complex*16 a(*)
!c  a(ndmat,1:natom), ndmat .ge. maxclstr*kkrsz**2

      integer nnptr(0:1,natom),ndimnn,nnclmn(2,ndimnn,natom)
!CALAM      integer ndbmt
      complex*16 bmt(*)
!c      complex*16 vecs(kkrsz*natom,*)
      integer ndcof
!CALAM      complex*16 t(ndcof,ndcof,*)
      complex*16 cof(ndcof,ndcof,natom)
      integer    itype(natom)
      logical precond
      integer invparam,niter,iprint
      real*8 tolinv

!c      complex*16 sum,sum0
      integer cx,cb,revcom
      complex*16 czero,cone
      parameter (czero=(0.d0,0.d0),cone=(1.d0,0.d0))
      real*8 zero,one,MB
      parameter (zero=0.d0,one=1.d0,MB=1024.d0*1024.d0)

      integer erralloc
      complex*16,  allocatable :: value(:)
      complex*16,  allocatable :: vecs(:,:)
      integer nd2vecs
      parameter (nd2vecs=9)

!c      complex*16,  allocatable :: vecstmp(:,:)
!c      complex*16,  allocatable :: tmpmtr(:,:)
!c      complex*16,  allocatable :: tmpmtr1(:,:)
!c      complex*16,  allocatable :: tmpmtr2(:,:)

      integer,     allocatable :: iptrow(:)
      integer,     allocatable :: indcol(:)
!CDLU
!c     ==================================================================
!c     Storage for ILU
!c     ==================================================================
      integer  nfill
      integer  nrow
      integer  precon
      integer NZLMAX
      integer NZUMAX
      real*8 tolilu
!c      logical,     allocatable :: JT(iprmat)
      logical,     allocatable :: JT(:)
      integer,     allocatable :: IL(:)
      integer,     allocatable :: JL(:)
      integer,     allocatable :: IU(:)
      integer,     allocatable :: JU(:)
      integer,     allocatable :: JI(:)
      real*8,      allocatable :: DN(:)
      real*8,      allocatable :: DS(:)
      complex*16,  allocatable :: L(:)
      complex*16,  allocatable :: U(:)
      complex*16,  allocatable :: DR(:)
      complex*16,  allocatable :: DUINV(:)
      complex*16,  allocatable :: XTMP(:)

      complex*16,  allocatable :: Z1TMP(:)
      complex*16,  allocatable :: Z2TMP(:)

      complex*16   ZZERO
      complex*16   ZONE
      parameter (ZZERO=czero)
      parameter (ZONE=cone)

      real*8 alu/0.5d0/
      real*8 R0,gamma
      parameter ( R0=6.283d0*1.6d0 , gamma=0.65d0 )
      save alu
!CDEBUG      real*8 fncILU
!CDEBUG      fncILU(natom,kkrsz) = dble(kkrsz)*dble(natom)**gamma

!CDLU
      integer INFO(4)
      real*8  TOL
      real*8  param1,bigmem

      character sname*10
      parameter (sname='gsvILUsp')

      complex*16 aij
      integer isave/0/,j,ii,i,ierr
!c      integer isave/0/,ntry,j,jj,ii,i,l1mx,l1mn,ierr
      integer nrmat,ne,nlim,nlen,jn,jns,ndim
      integer ns1,nsub1,nsub2,l1
      real*8 time1,time2
      save isave
!CDEBUG
      real timeEn,timeIneq
      common /Alltime/timeEn,timeIneq
!CDEBUG

      integer nzlsv,nzusv,nzlsv0,nzusv0
      save    nzlsv,nzusv,nzlsv0,nzusv0

      if(isave.eq.0) then
       write(6,*)
       write(6,*)                                                       &
     & ' ROUTINE  '//sname//' BASED ON SPARSE'                          &
     &            ,' ITERATIVE <<TFQMR>> ALGORITHM'
       write(6,*)                                                       &
     &     '          IS USED TO INVERT MATRIX'
       write(6,*) '  MAXIMUM NUMBER OF ITERATIONS = ',niter,            &
     &     ' AND TOLINV=',real(tolinv)
       write(6,*)
       isave=1
      end if

!CDEBUG
      time2 = zero
      call timel(time2)
!CDEBUG
      nrmat = kkrsz*natom

      allocate(vecs(1:nrmat,nd2vecs),                                   &
     &         iptrow(1:nrmat+1),                                       &
     &         stat=erralloc)
      if(erralloc.ne.0) then
      write(6,*) ' NRMAT=',nrmat
      write(6,*) ' ND2VECS+1=',nd2vecs+1
      write(6,*) ' ERRALLOC=',erralloc
      call fstop(sname//' :: ALLOCATION MEMORY PROBLEM')
      end if

      ne = 0
      i=0
      do iatom=1,natom
      indI = (iatom-1)*ndmat
      do lmi=1,kkrsz
!c         i = (iatom-1)*kkrsz+lmi
       i = i + 1
       indLI = indI+lmi-kkrsz
       do jnn=1,nnptr(0,iatom)
        indIJ = indLI+(jnn-1)*(kkrsz*kkrsz)
        do lmj=1,kkrsz
         if(a(indIJ+lmj*kkrsz).ne.czero) then
          ne = ne+1
           end if
        end do
       end do
      end do
      end do

      if(iprint.ge.0) then
       write(6,*) ' NE=',ne,' NRMAT*NRMAT=',nrmat*nrmat
      end if

      allocate(value(1:ne),                                             &
     &         indcol(1:ne),                                            &
     &         stat=erralloc)
      if(erralloc.ne.0) then
      write(6,*) ' NE=',ne
      write(6,*) ' NRMAT=',nrmat
      write(6,*) ' NDMAT=',ndmat
      write(6,*) ' ALLOCATION (C16) =',ne+nrmat+(nrmat+1+ne)/4+1
      write(6,*) ' ERRALLOC=',erralloc
      call fstop(sname//' :: ALLOCATION MEMORY PROBLEM')
      end if

      kelem = 0
      iptrow(1) = 1

      i = 0
      do iatom=1,natom
       indI = (iatom-1)*ndmat
       do lmi=1,kkrsz
!c        i = (iatom-1)*kkrsz+lmi
      i = i + 1
      do inn=1,nnptr(0,iatom)
       jatom = nnclmn(1,inn,iatom)
       indJ = (lmi-kkrsz) + (inn-1)*(kkrsz*kkrsz)
       do lmj=1,kkrsz
        j = (jatom-1)*kkrsz+lmj
        lmij = lmj*kkrsz +indJ
!c                          lmij = (lmj-1)*kkrsz + lmi
!c                          aij = a(lmij,iatom)
        aij = a(indI+lmij)
        if(aij.ne.czero) then
         kelem = kelem+1
         value(kelem) = aij
         indcol(kelem) = j
        end if
       end do
      end do
      iptrow(i+1) = kelem+1
       end do
      end do

      if(iprint.ge.0) then
       itmax=0
       itmin=niter
       anlim = zero
      end if

!CDLU
      if(precond) then
       PRECON = 1
      else
       PRECON = 0
      end if

      if(PRECON.eq.1) then
!c     ==================================================================
       ALLOCATE(                                                        &
     &            DUINV(1:NRMAT),                                       &
     &            DR(1:NRMAT),                                          &
     &            XTMP(1:NRMAT),                                        &
     &            DN(1:NRMAT),                                          &
     &            DS(1:NRMAT),                                          &
     &            JI(1:NRMAT),                                          &
     &            JT(1:NRMAT),                                          &
     &            IL(1:NRMAT+1),                                        &
     &            IU(1:NRMAT+1),                                        &
     &            Z1TMP(1:NRMAT),                                       &
     &            Z2TMP(1:NRMAT),                                       &
     &            stat=erralloc)
       if(erralloc.ne.0) then
         write(6,*) ' NRMAT=',nrmat
         write(6,*) ' ALLOCATION =',                                    &
     &                  3*16*NRMAT+2*8*NRMAT+4*4*NRMAT+2*4*(NRMAT+1)
         write(6,*) ' ERRALLOC=',erralloc
         call fstop(sname//'ALLOCATION MEMORY PROBLEM')
       endif
!c     ==================================================================

       param1 = dreal(bmt(1))
       bigmem = dimag(bmt(1))
       if(param1.gt.zero) then
         ftmp = min(one,0.5d0*(one+(one/(param1*R0)**3)))
       else
         ftmp = one
       end if

!c     ==================================================================
       if(invparam.eq.1) then
        NZLMAX = max(kkrsz*kkrsz,nint(0.4d0*nrmat*nrmat-1))
        if(bigmem.ge.one) then
         LUsize = (16 + 4) * 2                ! C16+I4 -- for L and U
!CDEBUG           NZLMAX = min(nint(bigmem*MB/LUsize),NZLMAX)
         NZLMAX = min(nint(bigmem*MB/LUsize),nrmat*nrmat/2)
        end if
        NZUMAX = NZLMAX
3         continue
        if(NZLMAX.lt.1.or.NZUMAX.lt.1) then
         write(6,*) ' NE=',ne
         write(6,*) ' NRMAT=',nrmat
         write(6,*) ' NZLMAX=',nzlmax
         write(6,*) ' NZUMAX=',nzumax
         call fstop(sname//':: MEMORY PROBLEM')
        end if
        ALLOCATE(                                                       &
     &            L(1:NZLMAX),                                          &
     &            U(1:NZUMAX),                                          &
     &            JL(1:NZLMAX),                                         &
     &            JU(1:NZUMAX),                                         &
     &            stat=erralloc)
        if(erralloc.ne.0) then
         if(allocated(l)) deallocate(l)
         if(allocated(u)) deallocate(u)
         if(allocated(jl)) deallocate(jl)
         if(allocated(ju)) deallocate(ju)
         NZLMAX = dble(NZLMAX)*0.9d0
         NZUMAX = NZLMAX
         go to 3
        endif
        nzlsv0 = NZLMAX
        nzusv0 = NZUMAX
        nzlsv = NZLMAX
        nzusv = NZUMAX

!CDEBUG          NFILL = 1+dble(NZLMAX+NZUMAX)/dble(2*nrmat)
        NFILL = kkrsz*(1+0.023*nrmat)
        alu = dble(NFILL)/dble(nrmat)

       endif
!c     ==================================================================

!CDEBUG         dneff = sqrt(dble(ne)/dble(nrmat))
!CDEBUG         ftmp  = ftmp*fncILU(natom,kkrsz)*(dneff/nrmat)

5        continue

!CDEBUG         TOLILU = tolinv*10.d0
       TOLILU = 1.d-4

       alpha = alu*ftmp

       NFILL = alpha*nrmat
!CDEBUG         NFILL = min(nrmat/2,NFILL)

       NZLMAX = nzlsv
       NZUMAX = nzusv

       if(NZLMAX+NZUMAX.gt.NRMAT*NRMAT.or.NFILL.lt.kkrsz) then
        write(6,*) ' ALU=',alu
        write(6,*) ' NFILL=',nfill,' NRMAT=',nrmat
        write(6,*) ' WARNING:: it is better to use other method'
        niter = -2
        PRECON = 0
        deallocate(DUINV,DR,XTMP,DN,DS,JI,JT,IL,IU,                     &
     &            Z1TMP,Z2TMP,                                          &
     &            stat=erralloc)
        go to 20
       end if

!c     ==================================================================

       if(.not.allocated(l))                                            &
     &    ALLOCATE(                                                     &
     &           L(1:NZLMAX),                                           &
     &           U(1:NZUMAX),                                           &
     &           JL(1:NZLMAX),                                          &
     &           JU(1:NZUMAX),                                          &
     &           stat=erralloc)
       if(erralloc.ne.0) then
         write(6,*) ' NE=',ne
         write(6,*) ' NRMAT=',nrmat
         write(6,*) ' NZLMAX=',nzlmax
         write(6,*) ' NZUMAX=',nzumax
         write(6,*) ' ALLOCATION =',                                    &
     &                  16*NZLMAX+16*NZUMAX+4*NZLMAX+4*NZUMAX
         write(6,*) ' ERRALLOC=',erralloc
         call fstop(sname//'ALLOCATION MEMORY PROBLEM')
       endif
!c     ==================================================================

         continue

       if(iprint.ge.0)                                                  &
     &   write(6,'('' NFILL, NFILL/NRMAT, NZLMAX, NZUMAX, TOLILU '',    &
     &                i8,f9.5,2i8,d10.1)')                              &
     &       NFILL, float(nfill)/float(nrmat),NZLMAX, NZUMAX, TOLILU

       CALL ZUILSTM ( NRMAT,value,IPTROW,INDCOL,L,IL,JL,U,IU,JU,        &
     &                   JT,JI,DN,DR,DS,                                &
     &                   DUINV,NZLMAX,NZUMAX, NFILL, TOLILU, IERR )
       if(iprint.ge.0) then
      WRITE (6,'(A15,I10)') 'Elements in L: ', IL(NRMAT+1)+NRMAT-1
      WRITE (6,'(A15,I10)') 'Elements in U: ', IU(NRMAT+1)+NRMAT-1
       end if

       if(ierr.ne.0) then
        write(6,*) ' NRMAT=',nrmat
        write(6,*) ' ALU=',alu
!c          write(6,*) ' BLU=',blu
        write(6,*) ' NZLMAX=',nzlmax
        write(6,*) ' NZUMAX=',nzumax
        write(6,*) ' NFILL=',nfill
        write(6,*) ' IERR=',ierr,                                       &
     &               ' not enough memory?'

        alu = alu*0.9d0

        deallocate(l,u,jl,ju,stat=erralloc)
        if(erralloc.ne.0) then
         write(6,*) ' ERRALLOC=',erralloc
         call fstop(sname//                                             &
     &          ' :: L-,U-,JL-,JU-DEALLOCATION MEMORY PROBLEM')
        end if

        go to 5
       else

        nzlsv = (nzlsv0+(IL(NRMAT+1)+NRMAT))/2+1
        nzusv = (nzusv0+(IU(NRMAT+1)+NRMAT))/2+1

       end if

      endif
!CDLU

!CDEBUG
      time1 = time2
      call timel(time2)
      timePreit = time2-time1
      if(iprint.ge.0) then
       write(*,*) sname//' :: ILU-PRECOND-time=',real(timePreit)
!c     *           ,nfill,dneff
      end if
!CDEBUG

      ntime = 0
!c      nsub0 = 0
      do nsub1=1,natom

       ns1 = itype(nsub1)
       Jns = (nsub1-1)*kkrsz

       do nsub2=1,nsub1-1
      if(ns1.eq.itype(nsub2)) then            ! copy
       do l1 = 1,kkrsz
        do i=1,kkrsz
         cof(i,l1,nsub1) = cof(i,l1,nsub2)
        end do
       end do
       go to 10
      end if
       end do

       ntime = ntime+1

       do l1=1,kkrsz
      j = Jns+l1

      call zerooutC(vecs(1,2),natom*kkrsz)

      vecs(j,2) = cone
!c
!c                                        VECS(*,2) = 1*Precond[A]
!c

      NDIM = nrmat
      NLEN = nrmat
!c     NROW = nrmat
      NLIM = niter
      TOL = tolinv
      INFO(1) = 0
      INFO(2) = 0
      INFO(3) = 0
      INFO(4) = 0

      if( PRECON .eq. 1 ) then
          CALL ZUIL1IM (NRMAT,L,IL,JL,U,IU,JU,DUINV,PRECON,             &
     &                   VECS(1,2),Z1TMP,Z2TMP)
      endif

80      CALL ZUTFXm (NDIM,NLEN,NLIM,VECS,TOL,INFO,jns+1,kkrsz)

!CDEBUG80      CALL ZUTFX (NDIM,NLEN,NLIM,VECS,TOL,INFO)

!C
!C     Perform matrix-vector multiplications when needed.
!C
      IERR   = INFO(1)
      REVCOM = INFO(2)
      CX     = INFO(3)
      CB     = INFO(4)

!C
!C     Multiply VECS(1,CX) with the preconditioned matrix.
!C
      IF (REVCOM.EQ.1) THEN

            if( PRECON .ne. 1 ) then
!CDEBUG        call ZSPAXB (nlen,value,iptrow,indcol,' U ',vecs(1,cx),vec !s(1,cb))
      call ZSPMMV(nlen,value,iptrow,indcol,vecs(1,cx),vecs(1,cb))
          else

!CDEBUG        CALL ZAXPBY (NLEN,XTMP,ZONE,VECS(1,CX),ZZERO,XTMP)
      XTMP(1:NLEN) = VECS(1:NLEN,CX)

      CALL ZUIL2IM (NRMAT,L,IL,JL,U,IU,JU,DUINV,PRECON,                 &
     &                XTMP,Z1TMP,Z2TMP)
!CDEBUG        CALL ZSPAXB (NLEN,VALUE,IPTROW,INDCOL,' U ',
!CDEBUG     >                     XTMP,VECS(1,CB))
      CALL ZSPMMV(NLEN,VALUE,IPTROW,INDCOL,XTMP,VECS(1,CB))
      CALL ZUIL1IM (NRMAT,L,IL,JL,U,IU,JU,DUINV,PRECON,                 &
     &                     VECS(1,CB),Z1TMP,Z2TMP)
          end if
       GO TO 80
!C
!C     Multiply VECS(1,CX) with the preconditioned transpose.
!C
      ELSE IF (REVCOM.EQ.2) THEN

            if( PRECON .ne. 1 ) then
      call ZSPATX (NLEN,value,iptrow,indcol,' U ',vecs(1,cx),vecs(1,cb))
          else

!CDEBUG        CALL ZAXPBY (NLEN,XTMP,ZONE,VECS(1,CX),ZZERO,XTMP)
      XTMP(1:NLEN) = VECS(1:NLEN,CX)

      CALL ZUIL1T (NRMAT,L,IL,JL,U,IU,JU,DUINV,PRECON,XTMP)
      CALL ZSPATX (NLEN,VALUE,IPTROW,INDCOL,' U ',                      &
     &                     XTMP,VECS(1,CB))
      CALL ZUIL2T (NRMAT,L,IL,JL,U,IU,JU,DUINV,PRECON,                  &
     &                     VECS(1,CB))
          end if
       GO TO 80
      END IF
!C
!C     Check why the solver stopped (this could be more compact).
!C
      IF (IERR.EQ.0) THEN
!c         WRITE (6,'(A32)') 'The residual norm has converged.'
         GO TO 90
      ELSE IF (IERR.EQ.1) THEN
       WRITE (6,*) sname//': IERR=1 '
       WRITE (6,*) 'Invalid reverse communication call.'
       call fstop(sname//'Invalid reverse communication call.')
      ELSE IF (IERR.EQ.2) THEN
       WRITE (6,*) sname//': IERR=2 '
       WRITE (6,*) 'Invalid inputs encountered.'
       call fstop(sname//'Invalid inputs encountered.')
      ELSE IF (IERR.EQ.4) THEN
       WRITE (6,*) sname//': IERR=4 '
       WRITE (6,*) ' The algorithm did not converge.'
!c         call fstop(sname//'The algorithm did not converge.')
      ELSE IF (IERR.EQ.8) THEN
       WRITE (6,*) sname//': IERR=8 '
       WRITE (6,*) 'The algorithm broke down.'
       call fstop(sname//'The iterative algorithm broke down.')
      ELSE
       WRITE (6,*) ' Unknown INFO code:  IERR=', IERR
!c         call fstop(sname//' Unknown IERR code ')
      END IF
      write(6,*) ' NE=',ne
      write(6,*) ' NRMAT=',nrmat
      write(6,*) ' NDMAT=',ndmat

      WRITE (6,*) 'WARNING:: The algorithm did not converge after ',    &
     &                niter,' iterations'
      niter = -1

      go to 20


90      continue

      if(iprint.ge.0) then
       itmax = max(itmax,nlim)
       itmin = min(itmin,nlim)
       anlim = anlim + NLIM
      end if

      if( PRECON .eq. 1 ) then
       CALL ZUIL2IM (NRMAT,L,IL,JL,U,IU,JU,DUINV,PRECON,                &
     &                   VECS(1,1),Z1TMP,Z2TMP)
!c     ==================================================================
      endif

!c        Jns = (nsub1-1)*kkrsz

      do ii=1,kkrsz
!CDEBUG         sum = czero
!CDEBUG         do jj=1,kkrsz
!CDEBUGc          Jn = (nsub1-1)*kkrsz+jj
!CDEBUG          Jn = Jns +jj
!CDEBUG          sum = sum + t(ii,jj,ns1)*vecs(Jn,1)
!CDEBUG         end do
!CDEBUG         cof(ii,l1,nsub1) = sum

!c          Jn = (nsub1-1)*kkrsz+ii
        Jn = Jns +ii
        cof(ii,l1,nsub1) = vecs(Jn,1)
!CDEBUG
      end do
       end do

10     continue
      end do

!CDEBUG
      time1 = time2
      call timel(time2)
      tsolve = (time2-time1)/ntime
      if(iprint.ge.0) then
       write(*,*) sname//' :: SPARSEINV-time=',real(tsolve)             &
     &            ,real(tsolve*natom),real(tsolve*natom+timePreit)
      end if
      timeIneq = timeIneq + tsolve*natom+timePreit
!CDEBUG

20    continue

      if(PRECON.eq.1) then
      DEALLOCATE(L,U,DUINV,DR,XTMP,Z1TMP,Z2TMP,stat=erralloc)
        if(erralloc.ne.0) then
          write(6,*) ' NE=',ne
          write(6,*) ' NRMAT=',nrmat
          write(6,*) ' DEALLOCATION (C16) =',                           &
     &                 16*NZLMAX+16*NZUMAX+4*16*NRMAT
          write(6,*) ' ERRALLOC=',erralloc
        call fstop(sname//'DEALLOCATION MEMORY PROBLEM')
        endif
!c     ==================================================================
        DEALLOCATE(DN,DS,stat=erralloc)
        if(erralloc.ne.0) then
          write(6,*) ' NE=',ne
          write(6,*) ' NRMAT=',nrmat
          write(6,*) ' DEALLOCATION (R8) =',2*8*NRMAT
          write(6,*) ' ERRALLOC=',erralloc
        call fstop(sname//'DEALLOCATION MEMORY PROBLEM')
        endif
!c     ==================================================================
      DEALLOCATE(IL,JL,IU,JU,JI,JT,stat=erralloc)
        if(erralloc.ne.0) then
          write(6,*) ' NE=',ne
          write(6,*) ' NRMAT=',nrmat
          write(6,*) ' DEALLOCATION (I4) =',                            &
     &                 2*4*(NRMAT+1)+ 4*NRMAT + 4*(NZLMAX+NZUMAX)
          write(6,*) ' ERRALLOC=',erralloc
        call fstop(sname//'DEALLOCATION MEMORY PROBLEM')
        endif
      endif

      deallocate(value,vecs,iptrow,indcol,stat=erralloc)

      if(iprint.ge.0.and.niter.gt.0) then
       if(precond) then
      write(6,1001) real(anlim/nrmat),itmin,itmax
1001    format(f10.6,2i5,                                               &
     &           ' ITERAT/NRMAT,ITMAX,ITMIN (PTFX-SPRS-ILU)')
       else
      write(6,1002) real(anlim/nrmat),itmin,itmax
1002    format(f10.6,2i5,                                               &
     &           ' ITERAT/NRMAT,ITMAX,ITMIN (TFX-SPRS)')
       end if
      end if

      if(erralloc.ne.0) then
      write(6,*) ' NE=',ne
      write(6,*) ' ERRALLOC=',erralloc
      call fstop(sname//' :: DEALLOCATION MEMORY PROBLEM')
      end if

      return
      end
