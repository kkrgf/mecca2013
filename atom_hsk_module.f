!BOP
!!MODULE: atom_hsk
!!INTERFACE:
      module atom_hsk
!!DESCRIPTION:
! Hermann-Skillman (atomic) Schroedinger equation solver

!!DO_NOT_PRINT
      implicit none
      private
!!DO_NOT_PRINT
 
!!PUBLIC MEMBER FUNCTIONS:
      public :: fjp_atom,intqud,nmx,zID
!      public :: g_rho_hsk,alloc_rho_hsk,dealloc_rho_hsk
!
!!REVISION HISTORY:
! Initial Version - A.S. - 2015
!!REMARKS:
! In current implementation external subroutine getDfltPot is required
! to generate initial atomic potential.
! LDA Hedin-Lundqvist exchange-correlation functional is used.
!EOP
!
!BOC

      integer, parameter :: nmx = 1001
      integer, parameter :: ndgand = 4
      integer, parameter :: dbprint = -1

      real(8), allocatable, target :: r_hsk(:,:,:)
      real(8), allocatable, target :: rho_hsk(:,:,:,:)

      real(8), allocatable, private :: vrin(:,:,:)
      real(8), allocatable, private :: vrout(:,:,:)

      interface
       subroutine getDfltPot(zed,r,rv,iflag)
       real(8), intent(in) :: zed,r(:)
       real(8), intent(out) :: rv(:)
       integer :: iflag
       end subroutine getDfltPot
      end interface

!EOC
      contains

!BOP
!!ROUTINE: fjp_atom
!!INTERFACE:
      subroutine fjp_atom(zed,xmom,numsp,alfa,mesh,r,rho,rv,tote,iunot)
!c _____________________________________________________________________
!!DESCRIPTION:
!   F.J.P. version of the herman-skillman (hsk) program    jan/feb 1987
!   (non-relativistic calculations)
!
!!DO_NOT_PRINT
           implicit real(8)(a-h,o-z), integer(i-n)
!!DO_NOT_PRINT

!!ARGUMENTS:
           real(8), intent(in) :: zed,xmom
           integer, intent(in) :: numsp
           real(8), intent(in) :: alfa
           integer, intent(out) :: mesh
           real(8), pointer :: r(:),rho(:,:)
           real(8), pointer :: rv(:,:)
           real(8), intent(out) :: tote
           integer, intent(in), optional :: iunot
!!REVISION HISTORY:
! Initial Version - A.S. - 2015
!EOP
!
!BOC

!c /////////////////////////////////////////////////////////////////////
!c  fjp version of the herman-skillman program       jan/feb 1987
!c             spin-polarized     and      total energy
!c /////////////////////////////////////////////////////////////////////
!c _____________________________________________________________________
!c
!c                      order of subroutines
!c
!c        subroutine    .........   1  ....  redin
!c        subroutine    .........   2  ....  xsetup
!c        subroutine    .........   3  ....  config
!c        subroutine    .........   5  ....  intqud
!c        subroutine    .........   6  ....  smpold
!c        subroutine    .........   7  ....  potgen
!c        subroutine    .........   8  ....  alpha2
!c        subroutine    .........   9  ....  derv5_ini
!c        subroutine    .........  10  ....  bound
!c        subroutine    .........  11  ....  outsch
!c        subroutine    .........  12  ....  inwsch
!c        subroutine    .........  13  ....  qrfit
!c        subroutine    .........  14  ....  savout
!c        subroutine    .........  15  ....  savin
!c        subroutine    .........  16  ....  dpdif
!c        subroutine    .........  17  ....  dgand
!c        subroutine    .........  18  ....  mxlneq
!c _____________________________________________________________________
!c
           parameter (three=3.d0,third=1.d0/three,twoth=2.d0/three)
           character*2 sym
!           character*2 symmagn(2)/'NM','FM'/
           character*15 name
           logical :: abad,ok_to_exit
           character(8), parameter :: ok='found it',trouble='trouble !'
           dimension x(nmx),temp(nmx)
           dimension q2(nmx),tmp(nmx)
           dimension occ(20,2),ncore(20,2),lcore(20,2),numcor(2),       &
     &                    elcnum(2),ecore(20,2)
           dimension rvnew(nmx,2),rhoold(nmx,2)
           real(8) :: excpo(nmx,2),excen(nmx)
           dimension occup(20),ncoup(20),lcoup(20),ecoup(20)
           dimension occdn(20),ncodn(20),lcodn(20),ecodn(20)

           integer :: maxit_scf
!           common/excorr/excpo(1001,2),excen(1001)
!      common/vin/vrin(1001,2,4)
!      common/vout/vrout(1001,2,4)
!c _____________________________________________________________________
!c
               equivalence (occup,occ(1,1))
               equivalence (occdn,occ(1,2))
               equivalence (ncoup,ncore(1,1))
               equivalence (ncodn,ncore(1,2))
               equivalence (lcoup,lcore(1,1))
               equivalence (lcodn,lcore(1,2))
               equivalence (numcup,numcor(1))
               equivalence (numcdn,numcor(2))
               equivalence (ecoup,ecore(1,1))
               equivalence (ecodn,ecore(1,2))
!c _____________________________________________________________________
!c
         data etop/1.d-6/,thresh/1.d-7/

!c _____________________________________________________________________
!c /////////////////////////////////////////////////////////////////////
           
      if ( associated(r) ) nullify(r)
      if ( associated(rho) ) nullify(rho)
      allocate(r(1:nmx))
      allocate(rho(1:nmx,2))
      if ( associated(rv) ) deallocate(rv)
      allocate(rv(1:nmx,2))

      if ( allocated(vrin) )  deallocate(vrin)
      if ( allocated(vrout) ) deallocate(vrout)
      allocate( vrin(nmx,2,ndgand) )
      allocate( vrout(nmx,2,ndgand) )

           maxit_scf = 150
           mesh = nmx
           nsize=40
!c /////////////////////////////////////////////////////////////////////
!c      zed is the atomic number
!c
!c      xmom is the moment;   up spins minus down spins
!c
!c      numsp is one   for a paramagnetic calculation
!c
!c      maxit_scf is max number of SCF iterations
!c
!c      alfa is the mixing parameter ( i have been using .15 )
!c /////////////////////////////////////////////////////////////////////
!c
!c _____________________________________________________________________
!c
      if ( dbprint >= 2 ) then
       write(6,1050)
       write(6,1503)
      end if
!c
 1050  format(1x,73('=')/1x,'atomic code: local density (spherical)'    &
     &   ,3x,'written jan/feb 87 by f.j.pinski')
 1503 format(1x,73('.') )
!c
       zup=.5d0*(zed+xmom)
!c
!c /////////////////////////////////////////////////////////////////////
!c         xsetup contructs r grid
!c         thofer sets up an intial approximation to the potential
!c               and charge density using latters fit of the
!c               thomas-fermi atom
!c =====================================================================
           call xsetup(x,r,mesh,nsize,zed,sym,name)
!c =====================================================================
!c
!c =====================================================================
!           call getDfltPot(zed,mesh,r,rv(:,1),0)     ! table-potential
           call getDfltPot(zed,r(1:mesh),rv(:,1),1)   ! thomas-fermi atom
           if (numsp==2) rv(:,numsp) = rv(:,1)
           rhoold = 0.d0
!          call thofer(r,rv,rhoold,zed,mesh,numsp)
!c =====================================================================
!c
!c /////////////////////////////////////////////////////////////////////
              do 6 i=1,numsp
              zd=(i-1)*zed+(3-2*i)*zup
!c =====================================================================
              call config(zd,numcor(i),ncore(1,i),lcore(1,i),occ(1,i)   &
     &                          ,ecore(1,i),numsp)
!c =====================================================================
    6         continue
!c /////////////////////////////////////////////////////////////////////
!c
      if ( dbprint >= 1 ) then
           write(6,1005) zed,sym,zup,zed-zup
 1005  format(1x,73('=')/'  atomic number ',f6.2,8x,a2,2x,6('='),       &
     &      '====  atom:    up electrons=',f5.2/1x,39('='),             &
     &      '===========  down electrons=',f5.2/7x,a15,8x,a2,2x,        &
     &   /1x,73('=')  )
      end if
!c
!c /////////////////////////////////////////////////////////////////////
!c =====================================================================
!c --------------> main loop <--------------> main loop <---------------
!c --------------> main loop <--------------> main loop <---------------
!c --------------> main loop <--------------> main loop <---------------
!c --------------> main loop <--------------> main loop <---------------
!c --------------> main loop <--------------> main loop <---------------
!c =====================================================================
!c /////////////////////////////////////////////////////////////////////
       ok_to_exit = .false.
       DO iter=1,maxit_scf
        itermx=max(1,iter-2)
!c _____________________________________________________________________
        call savin(itermx,rv,numsp)
!c =====================================================================
      if ( dbprint >= 2 ) then
        write(6,1005) zed,sym,zup,zed-zup
        write(6,1777) iter,abs(alfa)
      end if
 1777  format(1x,73('=')/ 1x,' iteration number:',i4,19x,               &
     &  'mixing parameter: ',f6.3 / 1x,73('=') )
!c =====================================================================
        qsum=0.0d0
        do 35 ns=1,numsp
          elcnum(ns)=0.d0
          rho(:,ns) = 0.d0
!c =====================================================================
          do 30 n=1,numcor(ns)
             en=ecore(n,ns)
             lambda=lcore(n,ns)
             nfl=ncore(n,ns)
             em=etop
!c =====================================================================
!c   bound -- solves the schrodinger equation: finds bound states
!c =====================================================================
             abad = .false.
             call bound(abad,zed,r,rv(:,ns),mesh,en,thresh,             &
     &                        nfl,lambda,temp,kkk,em,q2,qi)
!c =====================================================================
             ecore(n,ns)=en
             tmo=(3-numsp)*occ(n,ns)
!c =====================================================================
             if( abad ) then
                  elcnum(ns) = elcnum(ns) + occ(n,ns)
                  qsum=qsum-(3-numsp)*occ(n,ns)*qi
                  rho(1:kkk,ns) = rho(1:kkk,ns) + occ(n,ns)*temp(1:kkk)
              if ( dbprint >= 2 )                                       &
     &         write(6,1788) ok,ns,n,nfl,lambda,en,tmo
             else
              if ( dbprint >= 2 )                                       &
     &         write(6,1788) trouble,ns,n,nfl,lambda,en,tmo
             endif
!c ---------------------------------------------------------------------
 1788        format(1x,a8,', spin=',i1,', state=',i2,', n=',i1,         &
     &         ', l=',i1,', energy=',f18.11,', oc=',f6.3)
!c ---------------------------------------------------------------------
   30     continue
!c ---------------------------------------------------------------------
   35   continue
!c ---------------------------------------------------------------------
!c        elcore=elcnum(1)+elcnum(numsp)
!c =====================================================================
        call potgen1(r,rvnew,rho,zed,mesh,numsp,excpo,excen)
!c =====================================================================
!c
!c                                       total energy section
!c                                       ====================
!c
        enxc=0.0d0
        ecorr=0.0d0
        do ns=1,numsp
!c ---------------------------------------------------------------------
            do i=1,mesh
              temp(i)=rho(i,ns)*(4.d0*excen(i)-3.d0*excpo(i,ns))
            enddo
            call intqud(temp,tmp,r,mesh)
            enxc=enxc+(3-numsp)*tmp(mesh)
!c
!c ----------------------------- correction terms to make --------------
!c ------------------------- the total energy variational --------------
!c
            temp(1)=0.0d0
            do i=2,mesh
             temp(i)=(rv(i,ns)-rvnew(i,ns))/r(i)
            enddo
            call derv5_ini(temp,tmp,r,mesh)
            do i=2,mesh
             temp(i)=r(i)*rho(i,ns)*tmp(i)
            enddo
            call intqud(temp,tmp,r,mesh)
            ecorr=ecorr+(3-numsp)*tmp(mesh)
        enddo
!c ---------------------------------------------------------------------
        call savout(itermx,rvnew,numsp)
!c ---------------------------------------------------------------------
        tote=qsum+enxc
!       write(6,1188) qsum,enxc,tote
! 1188   format(1x,73('/')/1x,' qsum=',g12.6,3x,' excor=',g12.6,5x,      &
!     &     '      energy=',f15.7)
!c
        tote=tote+ecorr
!        write(6,1189) ecorr,tote
! 1189   format(1x,' corr=',g12.6,3x,'       ',12x,                      &
!     &      5x,'total energy=',f15.7/1x,73('/')/1x,73('_') )
!c =====================================================================
!c
!c  ---------------      mix potentials     ------------------------
!c  ---------------      mix potentials     ------------------------
!c  ---------------      mix potentials     ------------------------
!c  ---------------      mix potentials     ------------------------
!c
        if ( alfa < 0.d0 ) then
          write(6,*) ' alfa=',alfa,' < 0 ???'
          stop 'mixing coef should be non-negative'
        end if
        rms=0.0d0
        vrms=0.0d0
        do ns=1,numsp
         do k=1,mesh
           rms=rms+(rhoold(k,ns)-rho(k,ns))**2
           vrms=vrms+(rvnew(k,ns)-rv(k,ns))**2
           rhoold(k,ns)=rho(k,ns)
         enddo
        enddo
        rms=sqrt(rms/(mesh*numsp))
        vrms=sqrt(vrms/(mesh*numsp))

        ok_to_exit = iter==maxit_scf .or. ( rms<1.d-7 .and. vrms<1.d-7 )
        if ( dbprint>=1 .or. (ok_to_exit.and.dbprint==0) ) then
            if ( ok_to_exit .or. nint(zed-elcnum(1)-elcnum(numsp))>1 )  &
     &      then
             write(6,1200) name,zed,elcnum(1),elcnum(numsp)
 1200        format(1x,73('=')/1x,a15,'at.num=',f5.1,5x,f7.3,           &
     &       ' up   and',f7.3,'  down   electrons')
            end if
             write(6,1787) iter,rms,vrms,tote
 1787        format(3x,' iter=',i3,4x,'rms difference in ch. density=', &
     &              g11.4,',  in r*potential=',g11.4                    &
     &             /3x,'non-rel total energy =',f18.8,' Ry'             &
     &     /)
        end if
        if ( ok_to_exit ) exit
!c =====================================================================
        ndim=min(ndgand,itermx)
        call dgand(rv,ndim,numsp,alfa)
!c =====================================================================
!c
        END DO  !  end of maxit_scf-cycle

        if ( present(iunot) ) then
          write(iunot,3321) numcup,occup
3321      format(i3,(4(1x,f18.15)))
          write(iunot,3323) ncoup,lcoup,ecoup
3323      format(i3,1x,i3,(4(1x,e22.15)))
          if(numsp.eq.2)write(iunot,3321) numcdn,occdn
          if(numsp.eq.2)write(iunot,3323) ncodn,lcodn,ecodn
          write(iunot,3330) tote
          write(iunot,3330) r(1:mesh)
          write(iunot,3330) rv(1:mesh,1)
          write(iunot,3330) rho(1:mesh,1)
          if ( numsp.gt.1) then
           write(iunot,3330) rv(1:mesh,1)
           write(iunot,3330) rho(1:mesh,1)
          end if
 3330     format(4e22.14)
        end if
!c =====================================================================
!c
!CDEBUG
            if ( nint(zed-elcnum(1)-elcnum(numsp))>1 ) then
!             open(41,file=trim(sym)//'-core-'//symmagn(numsp)//'.dat')
             write(6,*) ' Atomic potential for <<'//name//'>>'
             write(6,*) ' Z = ',nint(zed)

               spincoef = 1.d0
               if(numsp.eq.1) spincoef=2.d0
               do is=1,numsp
                 nc = 0
                 do i=1,20
                  if(occ(i,is).gt.0.d0) then
                   nc = nc+1
                   if(lcore(i,is).ne.0)  nc = nc+1
                  end if
                 end do
                 write(6,*) nc,'   Spin=',is,' MagnMom=',real(xmom)
                 do i=1,20
                  if(occ(i,is).gt.0.d0) then
                  if(lcore(i,is).eq.0) then
                   kc = -1
                   write(6,41)                                          &
     &  ncore(i,is),lcore(i,is),kc,ecore(i,is)                          &
     &                                     ,occ(i,is)*spincoef
                  else
                   write(6,41)                                          &
     &  ncore(i,is),lcore(i,is),lcore(i,is),ecore(i,is)                 &
     &                                     ,occ(i,is)*spincoef
                   write(6,41)                                          &
     &  ncore(i,is),lcore(i,is),-(lcore(i,is)+1),ecore(i,is)            &
     &                                     ,occ(i,is)*spincoef
                  end if
                  end if
                 end do
               end do
41     format(1x,i2,2x,i2,2x,i3,3x,f12.5,2x,f6.2)
!                close(41)
            end if
!CDEBUG
            
      rms = 0.d0
      do k=mesh-1,1,-1
        rms=rms+(rho(k+1,1)+rho(k,1))*(r(k+1)-r(k))
        if ( rms>1.d-9 ) exit
      enddo
      if ( k>2 .and. k<mesh ) mesh = k

      deallocate(vrin)
      deallocate(vrout)
      return

      end subroutine fjp_atom

      subroutine zID(iz,symb,name)
!c =====================================================================
        implicit none
        integer, intent(in) :: iz
        character(2), intent(out) :: symb
        character(15), intent(out) :: name
        character(2) :: sym(103)
        character(15) :: nam(103)
!c
!c =====================================================================
!c
       sym(  1)='H '
       nam(  1)='hydrogen       '
       sym(  2)='He'
       nam(  2)='helium         '
       sym(  3)='Li'
       nam(  3)='lithium        '
       sym(  4)='Be'
       nam(  4)='berylium       '
       sym(  5)='B '
       nam(  5)='boron          '
       sym(  6)='C '
       nam(  6)='carbon         '
       sym(  7)='N '
       nam(  7)='nitrogen       '
       sym(  8)='O '
       nam(  8)='oxygen         '
       sym(  9)='F '
       nam(  9)='fluorine       '
       sym( 10)='Ne'
       nam( 10)='neon           '
       sym( 11)='Na'
       nam( 11)='sodium         '
       sym( 12)='Mg'
       nam( 12)='mangnesium     '
       sym( 13)='Al'
       nam( 13)='aluminum       '
       sym( 14)='Si'
       nam( 14)='silicon        '
       sym( 15)='P '
       nam( 15)='phosphorus     '
       sym( 16)='S '
       nam( 16)='sulfur         '
       sym( 17)='Cl'
       nam( 17)='chlorine       '
       sym( 18)='Ar'
       nam( 18)='argon          '
       sym( 19)='K '
       nam( 19)='potassium      '
       sym( 20)='Ca'
       nam( 20)='calcium        '
       sym( 21)='Sc'
       nam( 21)='scandium       '
       sym( 22)='Ti'
       nam( 22)='titanium       '
       sym( 23)='V '
       nam( 23)='vanadium       '
       sym( 24)='Cr'
       nam( 24)='chromium       '
       sym( 25)='Mn'
       nam( 25)='manganese      '
       sym( 26)='Fe'
       nam( 26)='iron           '
       sym( 27)='Co'
       nam( 27)='cobalt         '
       sym( 28)='Ni'
       nam( 28)='nickel         '
       sym( 29)='Cu'
       nam( 29)='copper         '
       sym( 30)='Zn'
       nam( 30)='zinc           '
       sym( 31)='Ga'
       nam( 31)='gallium        '
       sym( 32)='Ge'
       nam( 32)='germanium      '
       sym( 33)='As'
       nam( 33)='arsenic        '
       sym( 34)='Se'
       nam( 34)='selenium       '
       sym( 35)='Br'
       nam( 35)='bromine        '
       sym( 36)='Kr'
       nam( 36)='krypton        '
       sym( 37)='Rb'
       nam( 37)='rubidium       '
       sym( 38)='Sr'
       nam( 38)='strontium      '
       sym( 39)='Y '
       nam( 39)='yttrium        '
       sym( 40)='Zr'
       nam( 40)='zirconium      '
       sym( 41)='Nb'
       nam( 41)='niobium        '
       sym( 42)='Mo'
       nam( 42)='molybdenum     '
       sym( 43)='Tc'
       nam( 43)='technetium     '
       sym( 44)='Ru'
       nam( 44)='ruthenium      '
       sym( 45)='Rh'
       nam( 45)='rhodium        '
       sym( 46)='Pd'
       nam( 46)='palladium      '
       sym( 47)='Ag'
       nam( 47)='silver         '
       sym( 48)='Cd'
       nam( 48)='cadmium        '
       sym( 49)='In'
       nam( 49)='indium         '
       sym( 50)='Sn'
       nam( 50)='tin            '
       sym( 51)='Sb'
       nam( 51)='antimony       '
       sym( 52)='Te'
       nam( 52)='tellurium      '
       sym( 53)='I '
       nam( 53)='iodine         '
       sym( 54)='Xe'
       nam( 54)='xenon          '
       sym( 55)='Cs'
       nam( 55)='cesium         '
       sym( 56)='Ba'
       nam( 56)='barium         '
       sym( 57)='La'
       nam( 57)='lanthanum      '
       sym( 58)='Ce'
       nam( 58)='cerium         '
       sym( 59)='Pr'
       nam( 59)='praseodymium   '
       sym( 60)='Nd'
       nam( 60)='neodymium      '
       sym( 61)='Pm'
       nam( 61)='promethium     '
       sym( 62)='Sm'
       nam( 62)='samarium       '
       sym( 63)='Eu'
       nam( 63)='europium       '
       sym( 64)='Gd'
       nam( 64)='gadolinium     '
       sym( 65)='Tb'
       nam( 65)='terbium        '
       sym( 66)='Dy'
       nam( 66)='dysprosium     '
       sym( 67)='Ho'
       nam( 67)='holmium        '
       sym( 68)='Er'
       nam( 68)='erbium         '
       sym( 69)='Tm'
       nam( 69)='thulium        '
       sym( 70)='Yb'
       nam( 70)='ytterbium      '
       sym( 71)='Lu'
       nam( 71)='lutetium       '
       sym( 72)='Hf'
       nam( 72)='hafnium        '
       sym( 73)='Ta'
       nam( 73)='tantalum       '
       sym( 74)='W '
       nam( 74)='tungsten       '
       sym( 75)='Re'
       nam( 75)='rhenium        '
       sym( 76)='Os'
       nam( 76)='osmium         '
       sym( 77)='Ir'
       nam( 77)='iridium        '
       sym( 78)='Pt'
       nam( 78)='platinum       '
       sym( 79)='Au'
       nam( 79)='gold           '
       sym( 80)='Hg'
       nam( 80)='mercury        '
       sym( 81)='Tl'
       nam( 81)='thallium       '
       sym( 82)='Pb'
       nam( 82)='lead           '
       sym( 83)='Bi'
       nam( 83)='bismuth        '
       sym( 84)='Po'
       nam( 84)='polonium       '
       sym( 85)='At'
       nam( 85)='astatine       '
       sym( 86)='Rn'
       nam( 86)='radon          '
       sym( 87)='Fr'
       nam( 87)='francium       '
       sym( 88)='Ra'
       nam( 88)='radium         '
       sym( 89)='Ac'
       nam( 89)='actinium       '
       sym( 90)='Th'
       nam( 90)='thorium        '
       sym( 91)='Pa'
       nam( 91)='protactinium   '
       sym( 92)='U '
       nam( 92)='uranium        '
       sym( 93)='Np'
       nam( 93)='neptunium      '
       sym( 94)='Pu'
       nam( 94)='plutonium      '
       sym( 95)='Am'
       nam( 95)='americium      '
       sym( 96)='Cm'
       nam( 96)='curium         '
       sym( 97)='Bk'
       nam( 97)='berkelium      '
       sym( 98)='Cf'
       nam( 98)='californium    '
       sym( 99)='Es'
       nam( 99)='einsteinium    '
       sym(100)='Fm'
       nam(100)='fermium        '
       sym(101)='Md'
       nam(101)='mendelevium    '
       sym(102)='No'
       nam(102)='nobelium       '
       sym(103)='Lr'
       nam(103)='lawrencium     '
       if ( iz>0 .and. iz<=size(sym) ) then
        symb = sym(iz)
        name  = nam(iz)
       else
        symb=''
        name=''
       end if
       return
       end subroutine zID
!c _____________________________________________________________________
!c /////////////////////////////////////////////////////////////////////
        subroutine xsetup(x,r,mesh,nsize,zed,symb,name)
!c =====================================================================
        implicit real(8)(a-h,o-z), integer(i-n)
        character*2 symb,sym(103)
        character*15 name,nam(103)
        dimension x(mesh),r(mesh)
        parameter(three=3.d0,third=1.d0/three)
!c
!c =====================================================================
!c
        ized = nint(zed)
        call zID(ized,symb,name)

          if ( dbprint >= 2 ) then 
        write(6,1705) zed,symb,name
        write(6,1706)
 1705   format(1x,73('=')/5x,'atomic number ',f4.0,                     &
     &     3x,'chemical symbol: ',a2,                                   &
     &     3x,'element: ',a15  )
 1706   format(1x,73('=')  )
          end if

        if(ized.le.0.or.ized.gt.103) then
         stop ' Wrong value of ZED '
        end if
!c
!c =====================================================================
 1002   format(11x,' mesh=',i5,8x,'nsize=',i4,8x,'nblock=',i4)
        nblock=mesh/nsize
          if ( dbprint >= 2 ) then 
        write(6,1002) mesh,nsize,nblock
          end if
        c=0.75d0
        dx=.000025d0
!CAB        dx=.00005
        i=1
        x(1)=0.d0
        r(1)=0.d0
        do 50 j=1,nblock
           dr=c*dx
           imk=(j-1)*nsize+1
           ximk=x(imk)
           do 40 k=1,nsize
                i=i+1
                x(i)=ximk+k*dx
                r(i)=c*x(i)
   40      continue
!c          write(6,*) 'n,r(n),dr=',i,r(i),dr
           if(dr.lt.0.2d0)dx=dx+dx
   50   continue
!c =====================================================================
        m3=mesh/150
        m3=150*m3
          if ( dbprint >= 2 ) then 
        write(6,1001) (k,r(k),k=1,m3,50)
          end if
 1001   format(3x,'r(',i4,')=',f13.8                                    &
     &        ,4x,'r(',i4,')=',f13.8                                    &
     &        ,4x,'r(',i4,')=',f13.8)
!c =====================================================================
        if(i.le.mesh) return
        write(6,*)' trouble in xsetup:  mesh problem'
        stop
      end subroutine xsetup
!c _____________________________________________________________________
!c /////////////////////////////////////////////////////////////////////
       subroutine config(zed,nstate,nprin,l,fill,e,numsp)
!c =====================================================================
        implicit real(8)(a-h,o-z), integer(i-n)
       character*1 nc(2,20)
       dimension nprnc(20),ll(20),nprin(20),l(20),fill(20),e(20)
       data nc/'1','s', '2','s', '2','p', '3','s',                      &
     &         '3','p', '4','s', '3','d', '4','p',                      &
     &         '5','s', '4','d', '5','p', '6','s',                      &
     &         '4','f', '5','d', '6','p', '7','s',                      &
     &         '5','f', '6','d', '7','p', '8','s'/
       data nprnc/1,2,2,3,3,4,3,4,5,4,5,6,4,5,6,7,5,6,7,8/
       data  ll  /0,0,1,0,1,0,2,1,0,2,1,0,3,2,1,0,3,2,1,0/
!c
       nm=0
       nstate=0
       do i=1,20
         l(i)=ll(i)
         nprin(i)=nprnc(i)
         e(i)=-3.d0*zed*zed/nprin(i)**6
         fill(i)=0.d0
       enddo
   20  nstate=nstate+1
!c =====================================================================
       if(nstate.gt.20) then
          write(6,*) ' stopping in subroutine config:  nstate problem'
          stop
       endif
!c =====================================================================
       ndeg=2*ll(nstate)+1
       nm=nm+ndeg
       fill(nstate)=ndeg
       if(zed-nm.gt.0.0d0) go to 20
       dif=nm-zed
       fill(nstate)=ndeg-dif
!c =====================================================================
           sum=0.0d0
           do 19 i=1,nstate
              sum=sum+fill(i)
   19      continue
!c =====================================================================
       zed=sum
          if ( dbprint >= 2 ) then 
       write(6,1000)   (3-numsp)*sum
       write(6,1001)(i,nc(1,i),nc(2,i),nprnc(i),ll(i),                  &
     &    e(i),(3-numsp)*fill(i),i=1,nstate)
       write(6,1002)
          end if
!c =====================================================================
!c
 1000 format(/1x,23('+'),' number of electrons:',f7.3,1x,21('+')        &
     &   /1x,73('=')/29x,'state configuration'/1x,73('-') )
 1001  format(1x,i2,'.',2a1,':n=',i1,',l=',i1,',e=',f9.3,',occ=',f5.2   &
     & ,      2x,i2,'.',2a1,':n=',i1,',l=',i1,',e=',f9.3,',occ=',f5.2)
 1002 format(1x,73('=') )
!c
      return
      end subroutine config
!c _____________________________________________________________________
!c /////////////////////////////////////////////////////////////////////
      subroutine intqud(y,yi,x,nbig)
!c =====================================================================
      implicit real(8)(a-h,o-z), integer(i-n)
      real(8), intent(in) :: x(*),y(*)
      real(8), intent(out) :: yi(*)
      integer, intent(in) :: nbig
!      nbig=nin*nb+1
      yi(1)=0.0d0
      do 10 i=2,nbig
          top=x(i)
          bot=x(i-1)
          j=max(i-2,1)
          j=min(j,nbig-3)
          y1=y(j)
          y2=y(j+1)
          y3=y(j+2)
          y4=y(j+3)
          x1=x(j)
          x2=x(j+1)
          x3=x(j+2)
          x4=x(j+3)
          a1=y1
          a2=(y2-y1)/(x2-x1)
          a3=(((y3-y1)/(x3-x1))-a2)/(x3-x2)
          a4=((((((y4-y1)/(x4-x1))-a2)/(x4-x2)))-a3)/(x4-x3)
          b1=a1-a2*x1+a3*x1*x2-a4*x1*x2*x3
          b2=a2-a3*(x1+x2)+a4*(x2*x3+x1*x2+x1*x3)
          b3=a3-a4*(x1+x2+x3)
          b4=a4
          xint=b1+(.5d0*b2+.25d0*b4*(top**2+bot**2))*(top+bot)          &
     &        +b3*(top**2+top*bot+bot**2)/3.d0
          yi(i)=yi(i-1)+xint*(top-bot)
   10  continue
!c
      return
      end subroutine intqud
!c _____________________________________________________________________
!c /////////////////////////////////////////////////////////////////////
      subroutine smpold(y,yi,r,nb,nin)
!c =====================================================================
      implicit real(8)(a-h,o-z), integer(i-n)
      dimension r(*),y(*),yi(*)
      bsum=0.d0
      asum=0.d0
      yi(1)=0.d0
      i=1
      kmax=nin/2
!c                             nin must be an even integer
      do j=1,nb
        dx=r(i+1)-r(i)
        h=dx/3.d0
        g=h/4.d0
        do k=1,kmax
          i=i+2
          bsum=bsum+h*(y(i-2)+4.d0*y(i-1)+y(i))
          yi(i)=asum+bsum
          yi(i-1)=yi(i-2)+g*(5.d0*y(i-2)+8.d0*y(i-1)-y(i))
        enddo
        asum=yi(i)
        bsum=0.d0
      enddo
      return
      end subroutine smpold
!c _____________________________________________________________________
!c /////////////////////////////////////////////////////////////////////
!BOP
!!ROUTINE: potgen1
!!INTERFACE:
       subroutine potgen1(r,rv,rhos,zed,mesh,numsp,excpo,excen)
!!DESCRIPTION:
! generates atomic potential
!
!!REVISION HISTORY:
! adapted - A.S. - 2013
!EOP
!
!BOC
!c =====================================================================
      implicit real(8)(a-h,o-z), integer(i-n)
       parameter (third=1.d0/3.d0)
       parameter (epsm=1.d-32,onemd=1.d0-1.d-14)
       real(8), intent(in) :: r(mesh),rhos(mesh,2)
       real(8), intent(out) :: rv(mesh,2)
       real(8), intent(out) :: excpo(mesh,2),excen(mesh)
       dimension dcoul(mesh),coul(mesh)
       dimension q(mesh),q1(mesh)
!       dimension dexcpo(mesh,2)
!       dimension drv(mesh,2)
       dimension rho(mesh),xm(mesh),temp(mesh)
!       common/excorr/excpo(1001,2),excen(1001)

       integer :: iexcor
       integer :: ns,i 
       real(8) :: rs,sp,dz
!c =====================================================================
!c
!c      iexcor=2
!c      ========       use vosko-wilks-nusair exchange  <===========
!c
       iexcor=1
!c      ========       use von-Barth-Hedin    exchange  <===========
!c
       rho(1) = 0.d0
       temp(1) = 0.d0
       xm(1) = 0.d0
       do 30 i=2,mesh
         rho(i)=rhos(i,1)+rhos(i,numsp)
         temp(i)=rho(i)/r(i)
         xm(i)=rhos(i,1)-rhos(i,numsp)
   30  continue
!c =====================================================================
!c
       call intqud(rho,q,r,mesh)
          if ( dbprint >= 2 ) then 
       write(6,*)' total number of electrons: ',q(mesh)
          end if
!c
       call intqud(temp,q1,r,mesh)
!c =====================================================================
       coul(1)=-2*zed
       dcoul(1)=0.d0
       excpo(1,1:numsp) = 0.d0
       excen(1) = 0.d0
       do 62 ns=1,numsp
         sp=3-2*ns
         rv(1,ns)=coul(1)
         do 60 i=2,mesh
            rs=((3.d0*r(i)**2)/max(rho(i),epsm))**third
            if(ns.eq.1) then
             coul(i)=-2*(zed-q(i)+r(i)*(q1(i)-q1(mesh)))
             dcoul(i)=-2*(q1(i)-q1(mesh))
             dz = 0.d0
            else
             dz=(numsp-1)*xm(i)/max(rho(i),epsm)
             if(dz.le.-1.) dz=-onemd
             if(dz.ge. 1.) dz= onemd
            endif
            excpo(i,ns)=alpha2_ini(rs,dz,sp,iexcor,edum)
            temp(i)=r(i)*excpo(i,ns)
            excen(i)=edum
            rv(i,ns)=coul(i)+temp(i)
   60    continue
!         call derv5_ini(temp,dexcpo(1,ns),r,mesh)
!         do 61 i=1,mesh
!            drv(i,ns)=dcoul(i)+dexcpo(i,ns)
!   61    continue
   62  continue
!c =====================================================================
!c
!        m2=mesh/200
!        m2=m2*200
!        do 77 ns=1,numsp
!        write(6,1001) (r(i),i,rv(i,ns),i=1,m2,100)
! 1001   format(1x,'r=',e12.6,1x,'v(',i4,')=',e12.6                      &
!     & ,       3x,'r=',e12.6,1x,'v(',i4,')=',e12.6)
!c       write(6,1002) (r(i),i,rhos(i,ns),i=1,m2,100)
!c1002   format(1x,'r=',e12.6,1x,'n(',i4,')=',e12.6
!c    > ,       3x,'r=',e12.6,1x,'n(',i4,')=',e12.6)
!   77    continue
!c =====================================================================
!c             if(numsp.eq.1) return
!c =====================================================================
!c     write(6,1008)(r(i),i,xm(i)/max(rho(i),epsm)
!c    >                 ,i=1,m2,100)
!c1008   format(1x,'r=',e12.6,1x,'p(',i4,')=',e12.6
!c    > ,       3x,'r=',e12.6,1x,'p(',i4,')=',e12.6)
!c
      return
      end subroutine potgen1
!c _____________________________________________________________________
!c /////////////////////////////////////////////////////////////////////
      function alpha2_ini (rs,dz,sp,iexch,exchg)
!c =====================================================================
!c
      implicit real(8)(a-h,o-z), integer(i-n)
      parameter(one=1.d0,two=2.d0,three=3.d0)
      parameter(thrd=1.d0/three,for3=4.d0/three)
      dimension vxx(2),vxcs(2),g(3),dg(3),tbq(3),tbxq(3)
      dimension bxx(3),a(3),b(3),c(3),q(3),x0(3),bb(3),cx(3)
      save
!c
!c     data for von barth-hedin
!c
      data ccp,rp,ccf,rf/0.045d0,21.d0,0.0225d0,52.916682d0/
!c
!c     data for vosko-wilk-nusair
!c
      data incof/0/
      data  a/-0.033774d0,0.0621814d0,0.0310907d0/
      data  b/1.13107d0,3.72744d0,7.06042d0/
      data  c/13.0045d0,12.9352d0,18.0578d0/
      data x0/-0.0047584d0,-0.10498d0,-0.3250d0/
      data cst,aip/1.92366105d0,0.91633059d0/
      data fnot,bip/1.70992095d0,0.25992105d0/
!c
!c  the following are constants needed to obtain potential
!c  which are in g.s. painters paper
!c  =====given here for check=====(generated below)
!c
!c     data q/.7123108918e+01,.6151990820e+01,.4730926910e+01/
!c     data bxx/-.4140337943e-03,-.3116760868e-01,-.1446006102/
!c     data tbq/.3175776232e+00,.1211783343e+01,.2984793524e+01/
!c     data tbxq/.3149055315e+00,.1143525764e+01,.2710005934e+01/
!c     data bb/.4526137444e+01,.1534828576e+02,.3194948257e+02/
!c
!c if rs > 250, then return zero for pot and exc
      alpha2_ini=0
      exchg=0
      if (rs.gt.250) return
      go to (10,20) iexch
!c
!c     von barth-hedin  exch-corr potential
!c     j. phys. c5,1629(1972)
!c
  10  continue
      fm=two**for3-two
      fdz=( (one+dz)**for3 + (one-dz)**for3 - two )/fm
      ex=-0.91633d0/rs
      exf=ex*two**thrd
      xp=rs/rp
      xf=rs/rf
      gp=(one+xp**3)*log(one+one/xp) - xp*xp +xp/two - thrd
      gf=(one+xf**3)*log(one+one/xf) - xf*xf +xf/two - thrd
      exc=ex-ccp*gp
      excf=exf-ccf*gf
      dedz=for3*(excf-exc)*((one+dz)**thrd - (one-dz)**thrd)/fm
      gpp=three*xp*xp*log(one+one/xp)-one/xp + 1.5d0 - three*xp
      gfp=three*xf*xf*log(one+one/xf)-one/xf + 1.5d0 - three*xf
      depd=-ex/rs-ccp/rp*gpp
      defd=-exf/rs-ccf/rf*gfp
      decd=depd+(defd-depd)*fdz
!c exchange-correlation energy
      exchg= exc + (excf-exc)*fdz
!c exchange-correlation potential
      alpha2_ini=exc+(excf-exc)*fdz - rs*decd*thrd + sp*(one-sp*dz)*dedz
      return
!c
!c
  20  continue
      if(incof.ne.0) go to 30
      incof=2
!c
!c   vosk0-wilk-nusair exch-corr potential
!c    taken from g.s. painter
!c   phys. rev. b24 4264,1981
!c
!c   generate constant coefficients for the parameterization (v-w-n)
!c
      do i=1,3
        cx(i)= x0(i)**2 + b(i)*x0(i) + c(i)
        bfc= 4*c(i) - b(i)**2
        q(i)= sqrt(bfc)
        bxx(i)= b(i)*x0(i)/cx(i)
        tbq(i)= 2*b(i)/q(i)
        tbxq(i)= tbq(i) + 4*x0(i)/q(i)
        bb(i)= 4*b(i)*( 1 - x0(i)*(b(i) + 2*x0(i))/cx(i) )
      enddo
!c
  30  continue
      zp1= 1 + dz
      zm1= 1 - dz
      xr=sqrt(rs)
      pex= -aip/rs
      xrsq= rs
!c
!c   generate g(i)=alpha,epsilon fct.s
!c     and their derivatives dg(i)
!c    1=alpha(spin stiffness)  2=ecp  3=ecf
!c
      do 2 i=1,3
         qi=q(i)
         txb= 2*xr + b(i)
         fx= xrsq + xr*b(i) + c(i)
         arct= atan2(qi,txb)
         dxs= (xr-x0(i))**2/fx
!c
         g(i)=a(i)*( log(xrsq/fx) + tbq(i)*arct-bxx(i)*                 &
     &                 (log(dxs) + tbxq(i)*arct) )
!c
         dg(i)=a(i)*( 2.d0/xr - txb/fx -                                &
     &      bxx(i)*(2.d0/(xr-x0(i))-txb/fx) - bb(i)/(qi**2 + txb**2) )
!c
   2  continue
!c
      ecp=g(2)
      zp3=zp1**thrd
      zm3=zm1**thrd
      zp3m3=zp3-zm3
!c part of last term in vx   eq(13)
      fx1=.5d0*for3*pex*zp3m3
      z4= dz**4
      fz= cst*(zp1**for3 + zm1**for3 - 2.d0)
      beta= fnot*( g(3)-g(2) )/g(1) - 1.d0
      ec= ecp + fz*g(1)*( 1 + z4*beta )/fnot
      ex= pex*( 1 + fz*bip )
      f3ex= for3*ex
!c echange-correlation energy
      exchg= ec + ex
!c exchange potential
      vxx(1)= f3ex + fx1*zm1
      vxx(2)= f3ex - fx1*zp1
!c correlation potential
      vcc= ec - xr*( (1 - z4*fz)*dg(2) + z4*fz*dg(3)                    &
     &                  +  (1 - z4)*fz*dg(1)/fnot  )/6
!c
      facc= 4*g(1)*( dz**3*fz*beta + (1 + beta*z4 )*zp3m3/(6*bip) )/fnot
!c
!c exch-corr. potential for each spin as called in newpot
!c
      vxcs(1)= vcc + zm1*facc + vxx(1)
      vxcs(2)= vcc - zp1*facc + vxx(2)
!c
      if( sp.ge.0 ) alpha2_ini= vxcs(1)
      if( sp.lt.0 ) alpha2_ini= vxcs(2)
      return
      end function alpha2_ini
!c _____________________________________________________________________
!c /////////////////////////////////////////////////////////////////////
         subroutine derv5_ini(y,dy,x,nn)
!c =====================================================================
      implicit real(8)(a-h,o-z), integer(i-n)
         dimension x(nn),y(nn),dy(nn)
!c _____________________________________________________________________
!c
!c                                      lagrangian fit of y=f(x)
!c                                      derivative is then calculated
!c _____________________________________________________________________
         do 10 i=2,nn
         i1=max0(i-2,2)
         i1=min0(i1,nn-4)
         a=y(i1)
         b=(y(i1+1)-a)/(x(i1+1)-x(i1))
         c=(y(i1+2)-a)/(x(i1+2)-x(i1))
         c=(c-b)/(x(i1+2)-x(i1+1))
         d=(y(i1+3)-a)/(x(i1+3)-x(i1))
         d=(d-b)/(x(i1+3)-x(i1+1))
         d=(d-c)/(x(i1+3)-x(i1+2))
         e=(y(i1+4)-a)/(x(i1+4)-x(i1))
         e=(e-b)/(x(i1+4)-x(i1+1))
         e=(e-c)/(x(i1+4)-x(i1+2))
         e=(e-d)/(x(i1+4)-x(i1+3))
         dy(i)=b+c*( (x(i)-x(i1)) + (x(i)-x(i1+1)) )                    &
     &        + d*(  (x(i)-x(i1))*(x(i)-x(i1+1))                        &
     &             + (x(i)-x(i1+1))*(x(i)-x(i1+2))                      &
     &             + (x(i)-x(i1+2))*(x(i)-x(i1))      )                 &
     &      +e*( (x(i)-x(i1))*(x(i)-x(i1+1))*(x(i)-x(i1+2))             &
     &      +  (x(i)-x(i1+3))*(x(i)-x(i1+1))*(x(i)-x(i1+2))             &
     &      +  (x(i)-x(i1))*(x(i)-x(i1+1))*(x(i)-x(i1+3))               &
     &      +  (x(i)-x(i1))*(x(i)-x(i1+2))*(x(i)-x(i1+3)) )
   10    continue
!c
         dy(1)=dy(2)
!c _____________________________________________________________________
      return
      end subroutine derv5_ini
!c _____________________________________________________________________
!c /////////////////////////////////////////////////////////////////////
        subroutine bound(abad,zed,r,v,mesh,en,thresh,                   &
     &                      nq,lq,chden,kkk,emn,qi2,qii)
!c =====================================================================
      implicit real(8)(a-h,o-z), integer(i-n)
        logical :: abad
        parameter (onepd=1.d0,eminup=-.0005d0)
        dimension r(mesh),v(mesh),chden(mesh),temp(mesh),qi2(mesh)
        dimension vme(mesh),y(mesh),dy(mesh),yin(mesh),dyin(mesh)
!c =====================================================================
       abad=.false.
       l=lq
       n=nq
       if(en.ge.0.0d0) en=-zed**2/n**4
       emin=min(emn,eminup)
       energy=en
       en=emin
!c      WRITE(6,*) ' in bound: emin=',emin
!       eold=99.99d0
       eold=-99.99d0
!c =====================================================================
!c zero out chden
        do i=1,mesh
          qi2(i)=0.d0
          chden(i)=0.d0
        enddo
!c =====================================================================
        vme(1)=0.d0
        r0 = 1.d-6
        vme(1)=(-2*zed+l*(l+1)/r0)/r0 - emin
        do j=2,mesh
         vme(j)=(v(j)+(l*l+l)/r(j))/r(j)   - emin
        enddo
        do 6 i=2,mesh
         j=i
         if(r(i)*sqrt(-emin) .gt. 150.d0) exit
   6    continue
        lll=j
!c =====================================================================
!c       WRITE(6,*)' in bound: limit=',lll
        call outsch(emin,l,lll,vme,v,r,y,dy)
!c =====================================================================
        num=0
        do 8 i=3,lll
           if(y(i).eq.0.d0) then
                num=num+1
                cycle
           else
                if(y(i-1).eq.0.d0) cycle
                is=nint(sign(onepd, y(i) ))
                js=nint(sign(onepd,y(i-1)))
                if(is.ne.js) num=num+1
           endif
   8    continue
!c =====================================================================
!c number of nodes  n-l-1
!c
!c      WRITE(6,*)' found ',num,' nodes'
       nnode=n-l-1
       if(nnode.ge.num) then
          if ( dbprint >= 1 ) then 
               write(6,1740) emin,nq,lq
 1740          format('  no bound state below energy=',f8.4,            &
     &         ', with quantum numbers: n=',i2,', l=',i2 )
          end if
        return
       endif
!c =====================================================================
!c
!c  initialize parameters for search
!c
       maxit=40
       maxjt=20
       iext=0
       iter=0
       jter=0
       ratold = 0.d0
       nold = -1000
!c
       v(1)=-2*zed
!c =====================================================================
!c ....................  main loop .....................................
!c =====================================================================
  10   continue
       iter=iter+1
       if(iter.gt.maxit) go to 800
       if(energy.gt.0) energy=eold/2.d0
       bigkr=150.d0
       if(abs(energy).gt.400.d0) bigkr=125.d0
!c
!c find classical turning point ........................................
!c
        vme(1)=0.d0
        r0 = 1.d-6
        vme(1)=(-2*zed+l*(l+1)/r0)/r0 - energy
        do j=2,mesh
          vme(j)=(v(j)+(l*l+l)/r(j))/r(j)   - energy
        enddo
!c =====================================================================
        do 42 k=3,mesh
        j=mesh+1-k
        if(vme(j).le.0.00d0) go to 45
   42   continue
!c =====================================================================
          if ( dbprint >= 2 ) then 
        write(6,*) ' cannot find classical turning point '
          end if
        if(iter.eq.1) then
               iter=0
               jter=jter+1
               if(jter.gt.maxjt) then
                if ( dbprint >= 1 ) then 
                    write(6,*) ' turning point trouble'
                end if
                return
               endif
               energy=.75d0*energy
               go to 10
         else
               iter=iter-1
               jter=jter+1
               if(jter.gt.maxjt) then
                if ( dbprint >= 1 ) then 
                    write(6,*) ' turning point trouble'
                end if
                    return
               endif
               energy=.5d0*(energy+eold)
          endif
!c =====================================================================
  45    match=j+1
!c =====================================================================
        match=min(match,mesh-40)
        en_sqrt = sqrt(-energy)
        do 60 i=match,mesh
        j=i
        if(r(i)*en_sqrt .gt. bigkr ) go to 61
  60    continue
  61    last=j
        if(iext.eq.0) lll=last
        if(iext.ne.0) lll=match+1
        mmmm=match-1
!c =====================================================================
!c    outward integration of the schrodinger equation
!c =====================================================================
        call outsch(energy,l,lll,vme,v,r,y,dy)
!c        WRITE(6,*)' got past outsch'
!c =====================================================================
        gamout=dy(match)/(en_sqrt*y(match))
!c                                                 count number of nodes
        num=0
        do 80 i=3,lll
           if(y(i).eq.0.d0) then
                num=num+1
                go to 80
           else
                if(y(i-1).eq.0.d0) go to 80
                is=nint(sign(onepd, y(i) ))
                js=nint(sign(onepd,y(i-1)))
                if(is.ne.js) num=num+1
           endif
  80    continue
!c =====================================================================
        if(iter.eq.1) then
             iext=0
             if(num.eq.nnode+1) then
                  enext=1.25d0*energy
                  jter=0
                  go to 87
             endif
             if(num.eq.nnode) then
                  enext=.75d0*energy
                  jter=0
                  go to 87
             endif
             jter=jter+1
             if(jter.gt.maxjt)  then
               if ( dbprint >= 1 ) then 
                  write(6,*) ' cannot find right number of nodes '
                  write(6,*) ' looking for ',nnode,' nodes'
                  write(6,*) '       found ',num,' nodes'
               end if    
                  return
             endif
             iter=0
             if(num.lt.nnode) then
                  energy=.5d0*energy
                  go to 10
             else
                  energy=1.5d0*energy
                  go to 10
             endif
         endif
!c =====================================================================
         if(iter.eq.2)  then
             if(num.eq.nold) then
                  iter=1
                  jter=jter+1
                  if(jter.gt.maxjt) then
                    if ( dbprint >= 1 ) then 
                       write(6,*) ' trouble in second iteration'
                    end if
                   return
                  endif
                  dir=+1.d0
                  if(num.eq.nnode) dir=-1.d0
                  enext=energy-.8d0*dir*abs(energy-eold)
                  if(enext.ge.0.d0) enext=.5d0*energy
                  energy=enext
                  go to 10
             endif
             if(iabs(num-nold).ne.1) then
                  iter=1
                  jter=jter+1
                  if(jter.gt.maxjt) then
                    if ( dbprint >= 1 ) then 
                       write(6,*) ' trouble in second iteration'
                    end if
                   return
                  endif
                  enext=.5d0*energy+.5d0*eold
                  energy=enext
                  go to 10
             endif
             go to 81
        endif
!c =====================================================================
         if(abs(eold-energy).lt.thresh) go to 200
!c =====================================================================
         if(num.ne.nnode) go to 83
   81    dir=+1.d0
         go to 85
   83    dir=-1.d0
   85    enext=energy+.5d0*dir*abs(energy-eold)
   87    jter=0
         nold=num
         if( abs(1.d0-eold/energy) .lt. 0.1d0  .or. iext.eq.1) then
!c =====================================================================
!c   inward integration of schrodinger equation
!c =====================================================================
             call inwsch(energy,mmmm,last,vme,r,yin,dyin)
!c =====================================================================
             gamin=dyin(match)/(en_sqrt*yin(match))
!             if ( abs(gamin-gamout) <                                   &
!     &            2*epsilon(gamin)*(abs(gamin)+abs(gamout)) ) go to 200
             rat=(1.d0+gamin)/(1.d0-gamin)
             rat=rat-((1.d0+gamout)/(1.d0-gamout))
!             if(abs(rat).le.tiny(1.d0)) go to 200
             if(rat.eq.0) go to 200
             if(iext.eq.1) then
                 enext=energy+(eold-energy)/( 1.d0 - ratold/rat)
             endif
             iext=1
             ratold=rat
         endif
!c =====================================================================
         eold=energy
         energy=enext
         go to 10
!c
  800    continue
!c =====================================================================
!c ..........  can not find state  .....................................
!c =====================================================================
             if ( dbprint >= 1 ) then 
         write(6,*) ' too many tries for n=',nq,' l=',l
             end if
         return
!c
  200    continue
!c =====================================================================
!c .................... found state ............................
!c =====================================================================
         call inwsch(energy,mmmm,last,vme,r,yin,dyin)
!c =====================================================================
         gamin=dyin(match)/(sqrt(-energy)*yin(match))
         abad = .true.
         kkk=last
         chden(1) = 0.d0
         do i=2,match
           chden(i)=y(i)**2
         enddo
         anorm=y(match)/yin(match)
         do i=match,last
          y(i)=anorm*yin(i)
          dy(i)=anorm*dyin(i)
          chden(i)=y(i)**2
         enddo
         y(last+1:) = 0.d0
         dy(last+1:) = 0.d0
         chden(last+1:) = 0.d0
         en=energy
!c =====================================================================
         call intqud(chden,temp,r,mesh)
         qi2(1)=0.d0
         do 227 i=2,mesh
           chden(i)=chden(i)/temp(mesh)
           qi2(i)=(dy(i)-(l+1)*y(i)/r(i))**2/temp(mesh)
  227    continue
         call intqud(qi2,temp,r,mesh)
         qii=temp(mesh)
      return
      end subroutine bound
!c _____________________________________________________________________
!c /////////////////////////////////////////////////////////////////////
        subroutine outsch(energy,l,last,vme,v,r,y,dy)
!c =====================================================================
      implicit real(8)(a-h,o-z), integer(i-n)
        dimension vme(last),v(last),r(last)
        dimension y(last),dy(last)
        dimension fac(5)
        data fac/1.d0,3.d0,15.d0,105.d0,945.d0/
!c =====================================================================
        if(l.lt.0 .or. l.gt.4) then
             write(6,*) ' angular momentum quatum number is ',l
             write(6,*) ' stopping in outsch: l out of bounds'
             stop
        endif
!c =====================================================================
        e=energy
!c
        b1=v(1)
        f1=(v(2)-v(1))/r(2)
        f2=(v(3)-v(1))/r(3)
        f3=(v(4)-v(1))/r(4)
        bx1=f1
        bx2=(f2-f1)/(r(3)-r(2))
        bx3=(f3-f1)/(r(4)-r(2))
        bx3=(bx3-bx2)/(r(4)-r(3))
        b4=bx3
        b3=bx2-bx3*(r(2)+r(3))
        b2=bx1-bx2*r(2)+bx3*r(2)*r(3)
        a=sqrt(-e)**(l+1) / fac(l+1)
        b=b1*a/(2*l+2)
        c=(a*(b2-e)+b*b1)/(4*l+6)
        d=(a*b3+b*(b2-e)+c*b1)/(6*l+12)
        f=(a*b4+b*b3+c*(b2-e)+d*b1)/(8*l+20)
!c
        do 10 i=2,5
            x=r(i)**l
            p1=a+r(i)*( b+ r(i)*( c+ r(i)*( d+ f*r(i) )))
            p2=b+r(i)*(2.d0*c+r(i)*(3.d0*d+4.d0*f*r(i) ))
            y(i)=x*r(i)*p1
            dy(i)=(l+1)*x*p1+x*r(i)*p2
   10   continue
!c =====================================================================
      delold=r(6)-r(5)
      ityp=0
      do 40 n=6,last
         del=r(n)-r(n-1)
!c =====================================================================
         if(abs(del-delold).gt.1.d-7)  then
                 ityp=1
                 delold=del
                 m4=n-7
                 m3=n-5
                 m2=n-3
                 m1=n-1
         else
                 if(ityp.eq.0) then
                     m4=n-4
                     m3=n-3
                     m2=n-2
                     m1=n-1
                 endif
                 if(ityp.eq.2) then
                     ityp=0
                     m4=n-5
                     m3=n-3
                     m2=n-2
                     m1=n-1
                 endif
                 if(ityp.eq.1) then
                     ityp=2
                     m4=n-6
                     m3=n-4
                     m2=n-2
                     m1=n-1
                 endif
         endif
!c =====================================================================
        h24=del/24.d0
!c
        dy(n)=dy(m1)+h24*(55.d0*vme(m1)*y(m1) -59.d0*vme(m2)*y(m2)      &
     &                  + 37.d0*vme(m3)*y(m3) - 9.d0*vme(m4)*y(m4))
        do 37 ijk=1,5
        y(n)=y(m1)+h24*( 9.d0*dy(n) +19.d0*dy(m1) -5.d0*dy(m2) + dy(m3))
        dy(n)=dy(m1)+h24*( 9.d0*vme(n)*y(n) +19.d0*vme(m1)*y(m1)        &
     &                   - 5.d0*vme(m2)*y(m2) +  vme(m3)*y(m3))
   37  continue
        y(n)=y(m1)+h24*( 9.d0*dy(n) +19.d0*dy(m1) -5.d0*dy(m2) + dy(m3))
!c =====================================================================
   40  continue
!c
      return
      end subroutine outsch
!c _____________________________________________________________________
!c /////////////////////////////////////////////////////////////////////
       subroutine inwsch(energy,match1,last1,vme,r,y,dy)
!c =====================================================================
      implicit real(8)(a-h,o-z), integer(i-n)
        dimension r(last1),vme(last1),y(last1),dy(last1)
!c =====================================================================
        match=match1
        last=last1
        if(mod(last-2,40).lt.4) last=min(last+6,last1)
!c
        n=last
        p=sqrt(-energy)
!c =====================================================================
        xpr=exp(-p*r(n))
        y(n)=xpr
        dy(n)=-p*xpr
        ym3=y(n)
        dym3=dy(n)
        vmem3=vme(n)
        n = n-1
        xpr=exp(-p*r(n))
        y(n)=xpr
        dy(n)=-p*xpr
        ym2=y(n)
        dym2=dy(n)
        vmem2=vme(n)
        n = n-1
        xpr=exp(-p*r(n))
        y(n)=xpr
        dy(n)=-p*xpr
        ym1=y(n)
        dym1=dy(n)
        vmem1=vme(n)
        n = n-1
        xpr=exp(-p*r(n))
        y(n)=xpr
        dy(n)=-p*xpr
        n = n-1

!        do 10 i=1,4
!        xpr=exp(-p*r(n))
!        y(n)=xpr
!        dy(n)=-p*xpr
!            if(i.eq.1) then
!                   ym3=y(n)
!                   dym3=dy(n)
!                   vmem3=vme(n)
!            endif
!            if(i.eq.2) then
!                   ym2=y(n)
!                   dym2=dy(n)
!                   vmem2=vme(n)
!            endif
!            if(i.eq.3) then
!                   ym1=y(n)
!                   dym1=dy(n)
!                   vmem1=vme(n)
!            endif
!        n=n-1
!  10    continue
!c =====================================================================
        nend=n
        n=n+1
        delold=r(n+1)-r(n)
        do 80 nn=match,nend
         n=n-1
         del=r(n+1)-r(n)
         m1=n+1
         m2=n+2
         m3=n+3
!c =====================================================================
         if(abs(del-delold).gt.1.d-7) then
             a1=dy(m1)
             a2=dy(m2)
             a3=dy(m3)
             call qrfit(delold,a1,a2,a3,f1,f2)
             dym1=a1
             dym2=f1
             dym3=a2
!c
             a1=y(m1)
             a2=y(m2)
             a3=y(m3)
             call qrfit(delold,a1,a2,a3,f1,f2)
             ym1=a1
             ym2=f1
             ym3=a2
             ym4=f2
!c
             a1=vme(m1)
             a2=vme(m2)
             a3=vme(m3)
             call qrfit(delold,a1,a2,a3,f1,f2)
             vmem1=a1
             vmem2=f1
             vmem3=a2
             vmem4=f2
!c
             delold=del
!c =====================================================================
        else
!c =====================================================================
             ym4=ym3
             ym3=ym2
             ym2=ym1
             ym1=y(n+1)
!c
             dym3=dym2
             dym2=dym1
             dym1=dy(n+1)
!c
             vmem4=vmem3
             vmem3=vmem2
             vmem2=vmem1
             vmem1=vme(n+1)
         endif
!c =====================================================================
!c
         h24=-del/24.d0
         dy(n)=dym1 + h24 * (55.d0*vmem1*ym1-59.d0*vmem2*ym2            &
     &                      +37.d0*vmem3*ym3- 9.d0*vmem4*ym4)
         do 70 ijk=1,5
         y(n)=ym1 + h24*(9.d0*dy(n)+19.d0*dym1-5.d0*dym2+dym3)
         dy(n)=dym1 + h24 * ( 9.d0*vme(n)*y(n)+19.d0*vmem1*ym1          &
     &                       -5.d0*vmem2*ym2+vmem3*ym3)
   70    continue
         y(n)=ym1 + h24*(9.d0*dy(n)+19.d0*dym1-5.d0*dym2+dym3)
!c =====================================================================
   80 continue
      return
      end subroutine inwsch
!c _____________________________________________________________________
!c /////////////////////////////////////////////////////////////////////
       subroutine qrfit(del,a1,a2,a3,f1,f2)
!c ======================================================================
      implicit real(8)(a-h,o-z), integer(i-n)
       real*8   del,del2,delh
!c=======================================================================
!c  given a function evaluated at three equally spaced points
!c     returns values at the two midpoints using
!c         quadratic interpolation
!c               needs     del,  the spacing
!c                       a1,a2,a3, the 3 function evaluations
!c              returns    f1,f2,  the interpolated values
!c        f(x)= a + bx + cx(x-d)
!c        f(0)=a1
!c        f(d)=a2
!c        f(2d)=a3
!c        f1=f(d/2)
!c        f2=f(3d/2)
!c =====================================================================
          del2=2*del
          delh=del*.5d0
          a=a1
          b=(a2-a1)/del
          c=(((a3-a1)/del2)-b)/del
          f1=a+delh*(b-c*delh)
          f2=a+3.d0*delh*(b+c*delh)
      return
      end subroutine qrfit
!c _____________________________________________________________________
!c /////////////////////////////////////////////////////////////////////
       subroutine savout(iter,vr,numsp)
!c =====================================================================
      implicit real(8)(a-h,o-z), integer(i-n)
       real(8) :: vr(:,:)
!       common/vout/vrout(1001,2,4)
!c
       n=mod(iter-1,4)+1
       do ns=1,numsp
        do i=1,size(vrout,1)
         vrout(i,ns,n)=vr(i,ns)
        enddo
       enddo
      return
      end subroutine savout
!c _____________________________________________________________________
!c /////////////////////////////////////////////////////////////////////
       subroutine savin(iter,vr,numsp)
!c =====================================================================
      implicit real(8)(a-h,o-z), integer(i-n)
       real(8) :: vr(:,:)
!       common/vin/vrin(1001,2,4)
!c
       n=mod(iter-1,4)+1
       do ns=1,numsp
        do i=1,size(vrin,1)
         vrin(i,ns,n)=vr(i,ns)
        enddo
       enddo
      return
      end subroutine savin
!c _____________________________________________________________________
!c /////////////////////////////////////////////////////////////////////
       function dpdif(i,j,numsp)
!c =====================================================================
      implicit real(8)(a-h,o-z), integer(i-n)
!       common/vin/vrin(1001,2,4)
!       common/vout/vrout(1001,2,4)
       real(8) :: sum
!c
       sum=0.0d0
       do ns=1,numsp
        do k=1,size(vrin,1)
           sum=sum+(vrin(k,ns,i)-vrout(k,ns,i))                         &
     &      *k*(vrin(k,ns,j)-vrout(k,ns,j))
       enddo
       enddo
       dpdif=sum
      return
      end function dpdif
!c _____________________________________________________________________
!c /////////////////////////////////////////////////////////////////////
       subroutine dgand(vrnew,ndim,numsp,alpa)
!c =====================================================================
      implicit real(8)(a-h,o-z), integer(i-n)
!       common/vin/vrin(1001,2,4)
!       common/vout/vrout(1001,2,4)
       dimension a(5,5),b(5),vrnew(:,:)
!c
       jdim=ndim+1
       do i=1,ndim
         a(i,jdim)=1.d0
         a(jdim,i)=1.d0
         b(i)=0.d0
       enddo
!c
       a(jdim,jdim)=0.d0
       b(jdim)=1.d0
       do j=1,ndim
         jj=j
         do i=1,j
           ii=i
           a(i,j)=dpdif(ii,jj,numsp)
           if(i.ne.j) a(j,i)=a(i,j)
         enddo
       enddo
!c
       call mxlneq(a,b,jdim)
!c
       do ns=1,numsp
        do k=1,size(vrin,1)
          sum=0.0d0
         do i=1,ndim
         sum=sum+b(i)*(vrin(k,ns,i)+alpa*(vrout(k,ns,i)-vrin(k,ns,i)))
         enddo
          vrnew(k,ns)=sum
        enddo
       enddo
      return
      end subroutine dgand
!c _____________________________________________________________________
!c /////////////////////////////////////////////////////////////////////
       subroutine mxlneq(a,y,nn)
!c =====================================================================
      implicit real(8)(a-h,o-z), integer(i-n)
       dimension a(5,5),y(5)
!c
       n=nn
       do i=2,n
            f1=-1.d0/a(i-1,i-1)
            do j=i,n
             f2=f1*a(j,i-1)
             do k=1,n
               a(j,k)=a(j,k)+f2*a(i-1,k)
             enddo
             y(j)=y(j)+f2*y(i-1)
            enddo
       enddo
      y(n)=y(n)/a(n,n)
      do ij=1,n-1
        i=n-ij
          do j=1,ij
           y(i)=y(i)-y(i+j)*a(i,i+j)
          enddo
        y(i)=y(i)/a(i,i)
      enddo
!c
!c
      return
      end subroutine mxlneq
!c _____________________________________________________________________
!c ////////////////////////////////////////end of program///////////////

!BOP
!!IROUTINE: dealloc_rho_hsk
!!INTERFACE:
           subroutine dealloc_rho_hsk
!EOP
!
!BOC
             if ( allocated(rho_hsk) ) deallocate(rho_hsk)
             if ( allocated(r_hsk) ) deallocate(r_hsk)
!EOC
           end subroutine dealloc_rho_hsk

!BOP
!!IROUTINE: alloc_rho_hsk
!!INTERFACE:
           subroutine alloc_rho_hsk(ndrpts,ndcomp,nsubl,nspin)
! Initial Version - A.S. - 2013
!EOP
!
!BOC
           integer, intent(in) :: ndrpts,ndcomp,nsubl,nspin
           call dealloc_rho_hsk()
           allocate( r_hsk(ndrpts,ndcomp,nsubl) ); r_hsk=0
           allocate( rho_hsk(ndrpts,nspin,ndcomp,nsubl) ); rho_hsk=0
!EOC
           end subroutine alloc_rho_hsk

!BOP
!!IROUTINE: g_rho_hsk
!!INTERFACE:
           subroutine  g_rho_hsk( r,rho )
!!DESCRIPTION:
! provides radial mesh and charge density (for atoms)
!
!!ARGUMENTS:
           real(8), pointer :: r(:,:,:)
           real(8), pointer :: rho(:,:,:,:)
!!REVISION HISTORY:
! Initial Version - A.S. - 2013
!EOP
!
!BOC
           if ( associated(r) ) then
               nullify(r)
           end if   
           if ( associated(rho) ) then
               nullify(rho)
           end if   
           if ( allocated(r_hsk) ) then
             r => r_hsk
           end if
           if ( allocated(rho_hsk) ) then
             rho => rho_hsk
           end if
           return
!EOC
           end subroutine g_rho_hsk


!      subroutine thofer(r,v,rho,zed,mesh,numsp)
!!c =====================================================================
!       implicit real(8)(a-h,o-z), integer(i-n)
!       parameter (pi=3.14159265358979d0,rf=4.d0/(3.d0*pi) )
!       parameter(three=3.d0,oneth=1.d0/three)
!       dimension r(mesh)
!       dimension v(mesh,2),rho(mesh,2)
!!c
!!c     v   is r*potential
!!c    rho  is 4pi r**2 * density
!!c =====================================================================
!!c
!       if(zed.le.0.d0) then
!        scale=zed**oneth/.88534138d0
!       else
!        scale = 1.d0
!       end if
!!c
!       v(1,1)=-2.d0*zed
!       rho(1,1)=0.d0
!       v(1,numsp)=-2.d0*zed
!       rho(1,numsp)=0.d0
!!c =====================================================================
!       do 10 i=2,mesh
!       rr=r(i)*scale
!       root= sqrt(rr)
!       rvozin=1.d0            + 0.02747d0 *root                         &
!     &      + 1.243d0  *rr    - 0.14860d0 *rr*root                      &
!     &      + .2302d0  *rr*rr + 0.007298d0*rr*rr*root                   &
!     &      + .006944d0*rr*rr*rr
!       v(i,1)=2.d0*zed/rvozin
!       rho(i,1)=rf*v(i,1)* sqrt(v(i,1)*r(i))
!       v(i,1)=-v(i,1)
!       if(numsp.le.1) go to 10
!          v(i,2)=v(i,1)
!          rho(i,2)=rho(i,1)
!   10  continue
!!c =====================================================================
!!c
!!c       write(6,1001) (r(i),i,v(i,1),i=1,mesh,100)
!!c1001   format(1x,'r=',e11.5,1x,'v(',i4,')=',e11.5
!!c    > ,       3x,'r=',e11.5,1x,'v(',i4,')=',e11.5)
!!c       write(6,1002) (r(i),i,rho(i,1),i=1,mesh,100)
!!c1002   format(1x,'r=',e11.5,1x,'n(',i4,')=',e11.5
!!c    > ,       3x,'r=',e11.5,1x,'n(',i4,')=',e11.5)
!      return
!      end subroutine thofer


      end module atom_hsk
