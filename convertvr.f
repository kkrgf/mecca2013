!BOP
!!ROUTINE: convertVr0
!!INTERFACE:
      subroutine convertVr0(n,ztot,rr,vector,iflag)
!!DESCRIPTION:
! {\bv
! iflag=0: vector = vector + <nuclear-pot> ,
! iflag=1: vector = vector - <nuclear_pot> ,
!   ztot >= 0
!  \ev }
!

!!DO_NOT_PRINT
      implicit none
!!DO_NOT_PRINT

!!ARGUMENTS:
      integer, intent(in) :: n
      real(8), intent(in) :: ztot
      real(8), intent(in) :: rr(:)
      real(8), intent(inout) :: vector(:)
      integer, intent(in) :: iflag
!!REVISION HISTORY:
! Initial version - A.S. - 2013
!EOP
!
!BOC
!
      real(8) :: zt2(n)
!
       if ( ztot==dble(0) ) return
!      zt2 = 2.d0*ztot
       call twoZ(ztot,rr(1:n),n,zt2)
       if ( iflag.eq.1 ) then
        vector(1:n) = vector(1:n) - zt2
       else if ( iflag.eq.0 ) then
        vector(1:n) = zt2 + vector(1:n)
       end if
      return
!EOC
      end subroutine convertVr0
!
!BOP
!!ROUTINE: convertVr1
!!INTERFACE:
      subroutine convertVr1(n,ztot,rr,vector,iflag)
!!DESCRIPTION:
! iflag=0: vector = -2*ztot + vector*rr \\
! iflag=1: vector =  (2*ztot + vector)/rr
!

!!DO_NOT_PRINT
      implicit none
!!DO_NOT_PRINT

!!ARGUMENTS:
      integer, intent(in) :: n
      real(8), intent(in) :: ztot
      real(8), intent(in) :: rr(:)
      real(8), intent(inout) :: vector(:)
      integer, intent(in) :: iflag
!!REVISION HISTORY:
! Initial version - A.S. - 2013
!EOP
!
!BOC
!
      real(8) :: zt2(n)
!
      if ( iflag.eq.1 ) then
        if ( ztot.ne.dble(0) ) then
!      zt2 = 2.d0*ztot
         call twoZ(ztot,rr(1:n),n,zt2)
         vector(1:n) = vector(1:n) - zt2
        end if
        vector(1:n) = vector(1:n) / rr(1:n)
!         vector(1:n) = (vector(1:n) + zt2) / rr(1:n)
      else if ( iflag.eq.0 ) then
        vector(1:n) = vector(1:n)*rr(1:n)
        if ( ztot.ne.dble(0) ) then
!      zt2 = 2.d0*ztot
         call twoZ(ztot,rr(1:n),n,zt2)
         vector(1:n) = vector(1:n) + zt2
        end if
!         vector(1:n) = -zt2 + vector(1:n)*rr(1:n)
      end if
      return
!EOC
      end subroutine convertVr1
!
!BOP
!!ROUTINE: convertVr2
!!INTERFACE:
      subroutine convertVr2(n,ztot,rr,vector,iflag)
!!DESCRIPTION:
! iflag=0: vector = -2*ztot + vector*rr**2 \\
! iflag=1: vector =  (2*ztot + vector)/rr**2
!

!!DO_NOT_PRINT
      implicit none
!!DO_NOT_PRINT

!!ARGUMENTS:
      integer, intent(in) :: n
      real(8), intent(in) :: ztot
      real(8), intent(in) :: rr(:)
      real(8), intent(inout) :: vector(:)
      integer, intent(in) :: iflag
!!REVISION HISTORY:
! Initial version - A.S. - 2016
!EOP
!
!BOC
!
      real(8) :: zt2(n)
!
!      zt2 = 2.d0*ztot
      if ( iflag.eq.1 ) then
        if ( ztot.ne.dble(0) ) then
!      zt2 = 2.d0*ztot
         call twoZ(ztot,rr(1:n),n,zt2)
         vector(1:n) = vector(1:n) - zt2
        end if
        vector(1:n) = vector(1:n) / rr(1:n)**2
!         vector(1:n) = (vector(1:n) + zt2) / rr(1:n)**2
      else if ( iflag.eq.0 ) then
        vector(1:n) = vector(1:n)*rr(1:n)**2
        if ( ztot.ne.dble(0) ) then
!      zt2 = 2.d0*ztot
         call twoZ(ztot,rr(1:n),n,zt2)
         vector(1:n) = vector(1:n) + zt2
        end if
!         vector(1:n) = -zt2 + vector(1:n)*rr(1:n)**2
      end if
      return
!EOC
      end subroutine convertVr2
!
!BOP
!!ROUTINE: convertVr
!!INTERFACE:
      subroutine convertVr(n1,n2,nsublat,nspin,ztot,r,vector,iflag)
!!DESCRIPTION:
! wrapper calling {\em convertVr1} for array, {\tt vector}, with 4 dimensions 
! \\ \\
! iflag=0: vector = -2*ztot + vector*rr \\
! iflag=1: vector =  (2*ztot + vector)/rr
!
!!USES:
      use mecca_interface

!!DO_NOT_PRINT
      implicit none
!!DO_NOT_PRINT

!!ARGUMENTS:
      integer, intent(in) :: nsublat,n1(nsublat),n2(nsublat),nspin
      real(8), intent(in) :: ztot(:,:)  ! (ndcomp,ndsublat)
      real(8), intent(in) :: r(:,:)     ! (ndrpts,ndsublat)
      real(8), intent(inout) :: vector(:,:,:,:)  ! (ndrpts,ndcomp,ndsublat,1:nspin)
      integer, intent(in) :: iflag
!!REVISION HISTORY:
! Initial version - A.S. - 2013
!EOP
!
!BOC
      integer i2,i3,i4
!
       do i4=1,nspin
        do i3=1,nsublat
         do i2=1,n2(i3)
          call convertVr1(n1(i3),ztot(i2,i3),r(1:n1(i3),i3),            &
     &                                 vector(1:n1(i3),i2,i3,i4),iflag)
         end do
        end do
       end do
!
      return
!EOC
      end subroutine convertVr
