!BOP
!!ROUTINE: nnlist
!!INTERFACE:
      subroutine nnlist(iatom0,natom,                                   &
     &                 aij,isub,                                        &
     &                 mapstr,ndimbas,                                  &
     &                 mapsprs,rslatt,                                  &
     &                 nnmax,jclust,Rclust,                             &
     &                 mapsnni,ndimnn,                                  &
     &                 rsij,                                            &
     &                 nnptr,nnclmn,                                    &
     &                 isave,istop)
!!DESCRIPTION:
! computes list of nearest neighbors for both equivalent and inequivalent sites
! {\bv
! output:
!    nnptr(0:1)
!    nnclmn(1:2,*)
!
!    nnptr(0) -- 1+number_of_iatom0s_Neighbours
!    nnptr(1) points to atom where NN have been calculated
!
!    nnclmn(1,*)=jatom0 - neighbours of iatom0
!                                       (including iatom0)
!    nnclmn(2,*)=jnn    - number of corresponding neighbour
!                               of nnptr(1,iatom0)
!               jatom = jclust(nnclmn(2,*),isub)
!
!  Then to calculate matrix A for whole system if you know
!  only inequivalent part of matrix B:
!
!       A = 0
!       do i=1,natom
!        i0 = nnptr(1,i)
!        do inn=1,nnptr(0,i)
!          j=nnclmn(1,inn,i)
!          j0 = nnclmn(1,(nnclmn(2,inn,i),i0)
!          A[i,j] = B[i0,j0]
!        end do
!       end do
! \ev}
!EOP
!
!BOC
      implicit none
!c
      integer natom,ndimbas
      real*8      aij(3,*)

      integer     mapstr(ndimbas,natom)

      integer mapsprs(natom,natom)
      real*8  rslatt(3,3)

      integer iatom            ! Shows the number of inequivalent atom
      integer ndimnn
      integer jclust(*)
      real*8  Rclust
      integer mapsnni(ndimnn*ndimnn)
      real*8  rsij(*)
      integer nnptr(0:1),nnclmn(2,*)

      character istop*10
!c     ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
      character    sname*10
      parameter    (sname='nnlist')
!c     ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
      integer iatom0,jatom0,isub,ia0,inn,nnmax,m
      integer j,njnn
      integer, allocatable  ::  jnniat(:)

      integer isave

!c      isave = 0

       allocate(jnniat(1:nnmax))

          iatom = jclust(1)
          nnptr(0) = nnmax
          nnptr(1) = iatom
          inn = 0

          do jatom0 = 1,natom
           if(mapsprs(iatom0,jatom0).eq.2) then

!c  i.e. iatom0,jatom0 are neighbours (distance[iatom0,jatom0]<Rcut)
!CDEBUG!!! mapsprs --      For Rcut=Rclust !!!!

             ia0 = mapstr(iatom0,jatom0)
!CDEBUG             jatom = indclstr(iatom0,jatom0,iatom,
!CDEBUG     *             rslatt(1,1),rslatt(1,2),rslatt(1,3),aij(1,ia0),
!CDEBUG     *             mapstr,ndimbas,
!CDEBUG     *             nnmax,jclust(1,isub),Rclust,
!CDEBUG     *             mapsnni(1,isub),ndimnn,rsij(1,isub),isave)
             call innclstr(iatom0,jatom0,iatom,                         &
     &             rslatt(1,1),rslatt(1,2),rslatt(1,3),aij(1,ia0),      &
     &             mapstr,ndimbas,                                      &
     &             nnmax,jclust,Rclust,                                 &
     &             mapsnni,ndimnn,rsij,isave,jnniat,njnn)

             if(njnn.gt.1) then
              isave = -1
             end if
             do j=1,njnn
               inn = inn+1
               if(inn > nnmax) then
                write(6,*) ' IATOM0=',iatom0
                write(6,*) ' IATOM= ',iatom
                write(6,*) ' JATOM0=',jatom0
                write(6,*) ' NJNN=  ',njnn
                write(6,*) ' ISUB=  ',isub
                write(6,*) ' INN=   ',inn
                write(6,*) ' NNMAX= ',nnmax
                write(6,*) ' JNNIAT:'
                write(6,*) (jnniat(m),m=1,njnn)
                call fstop(sname//' :: INN > NDIMCL ??? ')
               end if
               nnclmn(1,inn) = jatom0
               nnclmn(2,inn) = jnniat(j)
             end do
           end if
          end do
          if ( inn<nnmax) then
           nnptr(0) = inn
           nnclmn(1:2,inn+1:nnmax) = 0
          end if

      deallocate(jnniat)

!c     ==================================================================
      if (istop.eq.sname) call fstop(sname)
!c
      return
!EOC
      contains

!BOP
!!IROUTINE: innclstr
!!INTERFACE:
      subroutine innclstr(iatom,jatom,iatom0,rslat1,rslat2,rslat3,aij,  &
     &    mapstr,ndmap,numnbi,jclust,                                   &
     &    rcut,mapsnni,ndimnn,rsij,isave,jnniat,njnn)
!!DESCRIPTION:
! computes  indices array, {\tt jnniat}, for NN cluster
!
!!REMARKS:
! private procedure of subroutine nnlist
!EOP
!
!BOC
      implicit none
!c
!c  iatom,jatom  are neigbours
!c
!c  iatom,iatom0 -- equivalent sites; calculation has been done for
!c                  iatom0 -->( isub )--> jclust(1..numnbi)
!c
!c  aij() -- R(iatom)-R(jatom)
!c  rsij() -- Rnn(ij)=R(iatom0)-R(j), j -- jclust(1..numnbi)
!c

      integer, intent(in) :: iatom,jatom,iatom0
      real(8), intent(in) ::  rslat1(3),rslat2(3),rslat3(3)
      real(8), intent(in) ::  aij(3),rcut
      integer, intent(in) :: ndmap,mapstr(ndmap,*)
      integer, intent(in) :: numnbi,jclust(numnbi),ndimnn
      integer, intent(in) :: mapsnni(ndimnn,numnbi)
      real(8), intent(in) ::  rsij(3,*)
      integer, intent(inout) :: isave
      integer, intent(out) :: jnniat(numnbi),njnn

      character*10 sname
      parameter (sname='innclstr')
      real*8 zero,one,epsrs
      parameter (zero=0.d0,one=1.d0,epsrs=1.d-10)

      real*8 rx,ry,rz,b
      integer m1,m2,m3,L1,L2,L3
      integer ia,k,n1,n2,n3,jatom0,i

      real*8 rsl1,rsl2,rsl3
      save rsl1,rsl2,rsl3

      njnn = 0
      ia = mapstr(iatom,jatom)

!c   direct neighbours
!c
      do k=1,numnbi
       jatom0 = jclust(k)
       if(mapstr(iatom0,jatom0).eq.ia) then
        njnn = njnn+1
        jnniat(njnn) = k
        if(njnn.eq.numnbi) return
       end if
      end do

!c   "boundary" neighbours
!c
      if(isave.eq.0) then
       isave=1
       rsl1 = one/sqrt(dot_product(rslat1,rslat1))
       rsl2 = one/sqrt(dot_product(rslat2,rslat2))
       rsl3 = one/sqrt(dot_product(rslat3,rslat3))
      end if

      rx = -aij(1)
      ry = -aij(2)
      rz = -aij(3)

      b = rsl1*dot_product(rslat1,aij)
      m1 = nint(sign(one,b))
      if(abs(b).gt.rcut) then           ! this is not very strict condition
       rx = rx + sign(rslat1(1),b)
       ry = ry + sign(rslat1(2),b)
       rz = rz + sign(rslat1(3),b)
      end if
      b = rsl2*dot_product(rslat2,aij)
      m2 = nint(sign(one,b))
      if(abs(b).gt.rcut) then           ! this is not very strict condition
       rx = rx + sign(rslat2(1),b)
       ry = ry + sign(rslat2(2),b)
       rz = rz + sign(rslat2(3),b)
      end if
      b = rsl3*dot_product(rslat3,aij)
      m3 = nint(sign(one,b))
      if(abs(b).gt.rcut) then           ! this is not very strict condition
       rx = rx + sign(rslat3(1),b)
       ry = ry + sign(rslat3(2),b)
       rz = rz + sign(rslat3(3),b)
      end if

      do k=2,numnbi

       ia = mapsnni(1,k)
       if(                                                              &
     & max(abs(rsij(1,ia)-rx),abs(rsij(2,ia)-ry),abs(rsij(3,ia)-rz))    &
     & .lt.epsrs) then
        do i=1,njnn
         if(k.eq.jnniat(i)) go to 1
        end do
        njnn = njnn+1
        jnniat(njnn) = k
1       if(njnn.eq.numnbi) return
       end if
      end do

!CDEBUG      do n1=0,m1,m1
!CDEBUG      do n2=0,m2,m2
!CDEBUG      do n3=0,m3,m3
!CDEBUG

      L1 = rcut*rsl1
      L2 = rcut*rsl2
      L3 = rcut*rsl3


      L1 = 1+max(0,L1)
      L2 = 1+max(0,L2)
      L3 = 1+max(0,L3)
!c                         L1,L2,L3:
!c                                   1, if rcut/alat<2
!c                                   2, if 2<rcut/alat<3, etc

      do n1=-m1*L1,m1*L1,m1
      do n2=-m2*L2,m2*L2,m2
      do n3=-m3*L3,m3*L3,m3
        rx = -aij(1) + n1*rslat1(1)+n2*rslat2(1)+n3*rslat3(1)
        ry = -aij(2) + n1*rslat1(2)+n2*rslat2(2)+n3*rslat3(2)
        rz = -aij(3) + n1*rslat1(3)+n2*rslat2(3)+n3*rslat3(3)
        do k=2,numnbi
         ia = mapsnni(1,k)
         if(                                                            &
     & max(abs(rsij(1,ia)-rx),abs(rsij(2,ia)-ry),abs(rsij(3,ia)-rz))    &
     &      .lt.epsrs) then
          do i=1,njnn
           if(k.eq.jnniat(i)) then
            go to 2
           end if
          end do
          njnn = njnn+1
          jnniat(njnn) = k

2         if(njnn.eq.numnbi) return
         end if
        end do
      end do
      end do
      end do

      return
!EOC
      end subroutine innclstr

!BOP
!!IROUTINE: nnrow
!!INTERFACE:
      subroutine nnrow(bmt,ndbmt,kkrsz,nnmax,nnclmn,zrow,ndkkr)
!!DESCRIPTION:
! "row matrix formation"
!
!!REMARKS:
! private procedure of subroutine nnlist
!EOP
!
!BOC
      implicit none
!c
!c        bmt -- "cluster" matrix
!c        zrow -- kkrsz-row of the whole matrix
!c
      integer ndbmt
      complex*16 bmt(ndbmt,*)
      integer kkrsz,ndkkr,nnmax
      integer nnclmn(2,nnmax)
!c      integer nlen
      complex*16 zrow(ndkkr,*)

      integer inn,jat0,jnn,iclmn,lnb,j,i

      do inn=1,nnmax
       jat0 = nnclmn(1,inn)
       jnn  = nnclmn(2,inn)
       iclmn = (jat0-1)*kkrsz
       lnb = (jnn-1)*kkrsz

       do j=1,kkrsz
        do i=1,kkrsz
         zrow(j,iclmn+i) = bmt(j,lnb+i)
        end do
       end do

      end do

      return
!EOC
      end subroutine nnrow

!BOP
!!IROUTINE: nncln
!!INTERFACE:
      subroutine nncln(jat0,natom,bmt,ndbmt,kkrsz,nnptr,nnclmn,         &
     &                  zclmn,nlen1)
!!DESCRIPTION:
! "column matrix formation"
!
!!REMARKS:
! private procedure of subroutine nnlist
!EOP
!
!BOC
      implicit none
!c
!c   input:     bmt -- "cluster" matrix
!c   output:    zclmn -- kkrsz-column of the whole matrix
!c
      integer jat0,natom,ndbmt
      complex*16 bmt(ndbmt,*)
      integer kkrsz,nnptr(0:1,*)
      integer nnclmn(2,*)
      integer nlen1
      complex*16 zclmn(nlen1,kkrsz)

      integer iat,irow,inn,jat,jnn,lnb,j,i

      call zerooutC(zclmn,nlen1*kkrsz)
      do iat=1,natom
       irow =  (iat-1)*kkrsz
       do inn = 1,nnptr(0,iat)
        jat = nnclmn(1,inn)
        if(jat.eq.jat0) then
         jnn  = nnclmn(2,inn)
         lnb = (jnn-1)*kkrsz
         do j=1,kkrsz
          do i=1,kkrsz
           zclmn(irow+j,i) = bmt(j,lnb+i)
          end do
         end do
        end if
       end do
      end do

      return
!EOC
      end subroutine nncln

!BOP
!!IROUTINE: nnrowT
!!INTERFACE:
      subroutine nnrowT(bmt,ndbmt,kkrsz,nnmax,nnclmn,zrow,ndmat)
!!DESCRIPTION:
! "row transposed matrix formation"
!
!!REMARKS:
! private procedure of subroutine nnlist
!EOP
!
!BOC
      implicit none
!c
!c        bmt -- "cluster" matrix
!c        zrowT -- Transpose(kkrsz-row of the whole matrix)
!c
      integer ndbmt
      complex*16 bmt(ndbmt,*)
      integer kkrsz,ndmat,nnmax
      integer nnclmn(2,nnmax)
!c      integer nlen
      complex*16 zrow(ndmat,*)

      integer inn,jat0,jnn,iclmn,lnb,j,i

      do inn=1,nnmax
       jat0 = nnclmn(1,inn)
       jnn  = nnclmn(2,inn)
       iclmn = (jat0-1)*kkrsz
       lnb = (jnn-1)*kkrsz

       do j=1,kkrsz
        do i=1,kkrsz
         zrow(iclmn+i,j) = bmt(j,lnb+i)
        end do
       end do

      end do

      return
!EOC
      end subroutine nnrowT
!
      end subroutine nnlist
