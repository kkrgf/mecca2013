!BOP
!!MODULE: zplane
!!INTERFACE:
      module zplane
!!DESCRIPTION:
!  procedures related to KKR-CPA Green's function calculations in complex plane
!
!!USES:
      use mecca_constants
      use mecca_types
      use gettau_interface
      use gfncts_interface, only : g_komp,g_atcon,g_meshv,g_zvaltot
      use gfncts_interface, only : g_reltype
      use mpi

!!DO_NOT_PRINT
      implicit none
      private
!!DO_NOT_PRINT

!!PUBLIC MEMBER FUNCTIONS:
      public :: calcZkkr
      public :: get_recdlm
!!PRIVATE MEMBER FUNCTIONS:
! subroutine getTauE
! integer function doscomp_nunit
! subroutine createDOSfiles
! subroutine writeDOSfiles
! subroutine applyDLMsymm
! subroutine totupGF
!
!!REVISION HISTORY:
! Initial Version - A.S. - 2013
!EOP
!
!BOC
      character(10), private, parameter :: sname = 'zplane'

!      integer, parameter :: mecca_ames = 0
!      integer, parameter :: tau_ray  = 2
!      integer, parameter :: tau_scf  = 4

      type ( IniFile ), pointer :: inFile=>null()
      type ( RS_Str ), pointer :: rs_box=>null()
      type ( KS_Str ), pointer :: ks_box=>null()
      type ( EW_Str ), pointer :: ew_box=>null()
      type (VP_data), pointer :: vp_box=>null()
      type (GF_data), pointer :: gf_box=>null()
      type (GF_output), pointer :: gf_out=>null()
      type (DosBsf_data), pointer :: bsf_box=>null()
      type (FS_data),  pointer :: fs_box=>null()
      type (MPI_data),  pointer :: mpi_box=>null()

      integer, save :: nume = 0
      complex(8), allocatable :: egrd(:)
      complex(8), allocatable :: dele1(:)
!
!      complex(8), allocatable :: hplnm(:,:)
!      complex(8), allocatable :: conr(:)

      logical, save :: dos_scan=.false.
      logical, save :: enforce_dlm=.false.
      logical, save :: enforce_afm=.false.

      real(8) :: efermi = 0
      complex(8) :: edu, dostmp

      integer :: Lss_max = 0
      logical, private :: lSS,lMS    ! defined in calcZcore and used also in gettau_scf

      integer :: nspin = 1
      integer :: nbasis = 1
      real(8) :: natomcnt = 0
      integer :: iprint = -1000
      integer :: mxcomp = 1
!dos_scan
      logical :: opendos = .true.
      character(200) :: dfs

      ! MPI related variables
      integer :: ierr, myrank, nproc, num_jobs, average, imesh
      integer :: ik_begin, ik_end, last_ie
      integer :: nenergy_lowE, nspecialk_lowE
      integer :: nenergy_highE, nspecialk_highE
      integer :: nodes_per_E_lowE, nodes_per_E_highE
      integer :: firstjob_this_round, lastjob_this_round
      integer :: myjob_this_round, ijob, inode
      integer :: nodes_at_thisE, world_group
      integer :: nodes_thisE_group, nodes_thisE_comm
      integer, allocatable :: nodes_thisE_array(:)
      complex(8), allocatable :: mpi_dos_buffer(:,:,:,:,:)
      integer :: idm(5)
      complex(8), allocatable :: z5buffer(:,:,:,:,:)
      complex(8), allocatable :: z4buffer(:,:,:,:)
      complex(8), allocatable :: z3buffer(:,:,:)
      complex(8), allocatable :: z2buffer(:,:)
      real(8), allocatable    :: r3buffer(:,:,:)
!
      real(8), external :: fermiFun

!***********************************************************************
!EOC
      contains
!***********************************************************************

!BOP
!!IROUTINE: calcZkkr
!!INTERFACE:
      subroutine calcZkkr( iflag, mecca_state, ef, irun )
!!DESCRIPTION:
! this subroutine is main module procedure, which, for a set
! of complex E-points, calculates
! site-dependent radial KKR-CPA Green's function, density of states,
! integrated denstity of states, etc. for valence electrons
!

!!DO_NOT_PRINT
      implicit none
!!DO_NOT_PRINT

!!ARGUMENTS:
! control parameter to choose calculational method
      integer, intent(in) :: iflag
      type(RunState), target :: mecca_state
      real(8), intent(in) :: ef
      integer, intent(in), optional :: irun    !  2 - SS, 1 - MS, 0 - SS+MS
!!REVISION HISTORY:
! Separate calculations for SS and MS - A.S. - 2020
! Initial Version - A.S. - 2013
!EOP
!
!BOC

      type(Sublattice), dimension(mecca_state%intfile%nsubl) :: sublat
      integer, dimension(mecca_state%intfile%nsubl) :: numbsub

      integer, allocatable :: komp(:)
      real(8), allocatable :: atcon(:,:)
      real(8), allocatable :: rmt_true(:,:)
      real(8), allocatable :: r_circ(:,:)
      integer :: lmax,ndrpts
      character(10) istop
      character(80) :: char80
      integer :: nrelv
      integer :: ie,is,in,ic,nsub,nonafm,zcmx,NSP
      real(8) :: zval,rytodu
      integer, allocatable :: edu_indx(:)
      real(8) :: vshift(2),egrd_min,Tempr,zz
      complex(8) :: dostot(2),sumdos,dos_Ff
      logical :: lVP

      ! get rank and number of processes
      call mpi_comm_rank(mpi_comm_world,myrank,ierr)
      call mpi_comm_size(mpi_comm_world,nproc,ierr)

      nullify(inFile) ; if ( associated(mecca_state%intfile) )          &
     &  inFile => mecca_state%intfile
      if ( .not.associated(inFile) ) return
      nullify(rs_box) ; if ( associated(mecca_state%rs_box)  )          &
     &  rs_box => mecca_state%rs_box
      if ( .not.associated(rs_box) ) return
      nullify(ks_box) ; if ( associated(mecca_state%ks_box) )           &
     &  ks_box => mecca_state%ks_box
      nullify(ew_box) ; if ( associated(mecca_state%ew_box) )           &
     &  ew_box => mecca_state%ew_box
      nullify(vp_box) ; if ( associated(mecca_state%vp_box) )           &
     &  vp_box => mecca_state%vp_box
      nullify(gf_box) ; if ( associated(mecca_state%gf_box) )           &
     &  gf_box => mecca_state%gf_box
      if ( .not.associated(gf_box) ) return
      nullify(gf_out) ; if ( associated(mecca_state%gf_out_box) )       &
     &  gf_out => mecca_state%gf_out_box
      if ( .not.associated(gf_out) ) return
      nullify(bsf_box) ; if ( associated(mecca_state%bsf_box) )         &
     &  bsf_box => mecca_state%bsf_box
      nullify(fs_box) ; if ( associated(mecca_state%fs_box) )           &
     &  fs_box => mecca_state%fs_box
      nullify(mpi_box) ;
      if ( associated(mecca_state%mpi_box) )  then
       mpi_box => mecca_state%mpi_box
       mpi_box%ik_begin = 0
       mpi_box%ik_end   = -1
       mpi_box%thisE_comm = 0
      end if

      efermi = ef
      nspin = inFile%nspin
      NSP   = nspin
      call g_reltype(valence=nrelv)
      lmax  = inFile%lmax
      vshift = zero
      if ( nspin==2 ) then
        vshift(1 ) = -(inFile%v0(2)-inFile%v0(1))/2
!!TODO        vshift(1) = vshift(1) - inFile%magnField/2   ! for external magn.field
        vshift(2) = -vshift(1)
      end if
      lVP = .false.
      if ( associated(vp_box) ) then
       lVP = (vp_box%nF>0) .and. (inFile%mtasa>1)
      end if

      Tempr = inFile%Tempr
      iprint = inFile%iprint
      istop = inFile%istop
      call g_ndims(inFile,ndrpts,mxcomp,nbasis,nspin)

      if ( mecca_state%gf_out_box%ms_gf==0 ) then
       zcmx = 0
       do nsub=1,inFile%nsubl
        do ic=1,inFile%sublat(nsub)%ncomp
         zcmx = max(zcmx,inFile%sublat(nsub)%compon(ic)%zcor)
        end do
       end do
       zval = g_zvaltot(inFile,.false.)
       if ( zval <= qtol*zcmx .and. Tempr==zero) then
        if ( zcmx>0 ) then
         gf_out%nume = 0        ! no valence run
        else
         continue               ! zval=zcmx=0 - empty-lattice run
        end if
       end if
      end if

      call allocGFoutput(gf_out,ndrpts,mxcomp,nbasis,nspin)
      call allocGFdata(gf_box,lmax,ndrpts,mxcomp,nbasis,nspin)

      if ( gf_out%nume == 0 ) return

      lSS = .true.
      lMS = .true.
      if ( present(irun) ) then
       lMS = irun==1 .or. irun==0
       lSS = irun==2 .or. irun==0
      end if

!      alat = inFile%alat
      call g_komp(inFile,komp)
      sublat = inFile%sublat(1:nbasis)
      numbsub = sublat(1:nbasis)%ns
!
      enforce_dlm = all( sublat(1:nbasis)%isDLM .eqv. .true. )
!
      call applyAFMsymm( -1, nonafm )
      enforce_afm = nonafm==0
!
      if ( associated(bsf_box) ) then
       dos_scan = bsf_box%idosplot .ne. 0
      else
       dos_scan = .false.
      end if

!      nsites = sum(numbsub)
      call g_atcon(inFile,atcon)
      nume = gf_out%nume
      allocate(egrd(1:nume))
      allocate(dele1(1:nume))
      egrd = gf_out%egrd(1:nume)
      dele1 = gf_out%wgrd(1:nume)

      allocate(rmt_true(mxcomp,nbasis))
      allocate(r_circ(mxcomp,nbasis))
      do nsub=1,nbasis
!        rmt_true(1:komp(nsub),nsub) = rs_box%rmt(nsub)
        rmt_true(1:komp(nsub),nsub) =                                   &
     &                           sublat(nsub)%compon(1:komp(nsub))%rSph
        r_circ(1:komp(nsub),nsub) = rs_box%rcs(nsub)
      end do

!c     ================================================================
!c     solve cpa equations over the energy mesh........................
!c     ----------------------------------------------------------------

      natomcnt = 0
      do nsub = 1,nbasis
!c  to exclude empty spheres
       natomcnt=natomcnt+numbsub(nsub)*sum(atcon(1:komp(nsub),nsub),    &
     &                sublat(nsub)%compon(1:komp(nsub))%zID>0)
      end do
      if ( natomcnt <= 0 ) natomcnt=1

      if ( dos_scan ) then
!  if dos_scan -> create the neccesary files
       if (  myrank == 0 ) then
        call createDOSfiles (  )
       end if
       if( nproc > 1 ) then
        allocate( mpi_dos_buffer(0:nproc-1,0:lmax,mxcomp,nbasis,nspin) )
       endif
       egrd_min = inFile%ebot     ! egrd_min: DOS (E<egrd_min) = zero or Inf (i.e. pole)
       do nsub = 1,nbasis
        do ic=1,inFile%sublat(nsub)%ncomp
         if ( inFile%sublat(nsub)%compon(ic)%v_rho_box%numc(1)>0 )      &
     &   egrd_min = min(egrd_min,minval(inFile%sublat(nsub)%compon(ic)% &
     &                 v_rho_box%ec(1,1:nspin))-one)

        end do
       end do
       if ( egrd_min==inFile%ebot ) then
        egrd_min = min(inFile%ebot,-10.d0)
       end if
      else
       if ( nume > 1 ) then
        egrd_min = minval(dreal(egrd(1:nume-1)))
       else
        egrd_min = dreal(egrd(1))
       end if
      endif

      Lss_max = 1+int(2*sqrt(Tempr)*maxval(rs_box%rws(1:nbasis)))

      ! gather information needed for subdividing E,k into jobs
      if ( size(ks_box%nqpt)>2 ) then
       if ( nproc>1 ) then
        call fstop(sname//': MPI is not implemented for nmesh > 2')
       else
        if ( associated(mpi_box) ) then
          nullify(mpi_box)
        end if
       end if
      end if

      if ( nproc>1 ) then
       allocate( z5buffer(0:nproc-1,0:lmax,mxcomp,nbasis,nspin) )
      end if
      allocate(edu_indx(1:gf_out%nume))
      nenergy_highE = 0
      nenergy_lowE  = 0
      rytodu = rs_box%alat/(two*pi)
      if ( lMS ) then
       nspecialk_highE = ks_box%nqpt(1)
       nspecialk_lowE  = sum(ks_box%nqpt(:))-nspecialk_highE
       do ie = gf_out%nume,1,-1
        edu = gf_out%egrd(ie)*rytodu**2
        call pickmesh(edu,infile%enKlim,size(ks_box%nqpt),imesh)
        if(imesh == 1) then
          nenergy_highE = nenergy_highE + 1
          edu_indx(gf_out%nume-nenergy_highE+1) = ie
        else
          nenergy_lowE  = nenergy_lowE + 1
          edu_indx(nenergy_lowE) = ie
        endif
       enddo
       average = 1 + (nenergy_highE*nspecialk_highE +                   &
     &                 nenergy_lowE*nspecialk_lowE)/nproc

       nodes_per_E_highE = 1 + nspecialk_highE/average
       if ( nenergy_lowE>0 ) then
        nodes_per_E_lowE  = 1 +  nspecialk_lowE/average
       end if
       num_jobs = nenergy_highE*nodes_per_E_highE +                     &
     &            nenergy_lowE*nodes_per_E_lowE
      else if ( lSS ) then
!       forall (ie=1:gf_out%nume) edu_indx(gf_out%nume-ie+1)=ie
       forall (ie=1:gf_out%nume) edu_indx(ie)=ie
       nenergy_highE = gf_out%nume
       nspecialk_highE = gf_out%nume
       nspecialk_lowE  = 0
       average = max(1,(nenergy_highE*nspecialk_highE)/nproc)
       nodes_per_E_highE = max(1,nspecialk_highE/average)
       nodes_per_E_lowE = 0
       num_jobs = nenergy_highE*nodes_per_E_highE
      else
       nodes_per_E_highE = 0
       nodes_per_E_lowE = 0
       num_jobs = 0
      end if

      if(iprint.ge.0) then
       if( nproc > 1 ) then
        write(6,'(a)') ' MPI distribution of nodes'
        write(6,'(a,i5)') ' - number of jobs: ', num_jobs
        write(6,'(a,i5)') ' - nodes/energy at large E: ',               &
     &     nodes_per_E_highE
        write(6,'(a,i5)') ' - nodes/energy at small E: ',               &
     &     nodes_per_E_lowE
       endif
      endif

      ! write(6,*) 'begin z-plane integration'

      ! syncronize before beginning jobs
!MPIDEBUG      call log_mpi_sync(__FILE__,__LINE__)
      call mpi_barrier(mpi_comm_world,ierr)

      ! perform parallel integration over E and k
!     do ie=1,nume
      firstjob_this_round = 0
      do while( firstjob_this_round < num_jobs )
!..................................................................................
      IF ( lSS .or. lMS ) THEN
!..................................................................................

       ! what is the last job for which we have enough nodes to
       ! cover all the k-points at that energy
       lastjob_this_round = min(num_jobs, firstjob_this_round + nproc)
       call GetJobInfo(lastjob_this_round,edu_indx,ie,ik_begin,ik_end,  &
     &    nodes_thisE_array)
       lastjob_this_round = firstjob_this_round +                       &
     &    nodes_thisE_array(1) - 1

       ! what E and k-points does my job correspond to?
       myjob_this_round = firstjob_this_round + myrank
       call GetJobInfo(myjob_this_round,edu_indx,ie,ik_begin,ik_end,    &
     &    nodes_thisE_array)
       if ( associated(mpi_box) ) then
        mpi_box%ik_begin = ik_begin
        mpi_box%ik_end   = ik_end
       end if
       nodes_at_thisE = size(nodes_thisE_array)

       ! we need to buffer the partial DoS so the root node can print
       ! in correct order
       if( dos_scan .and. nproc > 1 ) mpi_dos_buffer = dcmplx(0.d0,0.d0)

       ! debugging: print job assignment
!       write(6,'(a,i3)') ' ie = ', ie
!       write(6,'(a,i3)') 'ik_begin = ', ik_begin
!       write(6,'(a,i3)') 'ik_end   = ', ik_end
!       write(6,'(a,i3)') 'my friends this energy = '
!       write(6,*) nodes_thisE_array(:)
!       flush(unit=6)
!
       ! only perform work those energies with total k-coverage
       if( myjob_this_round <= lastjob_this_round .and. ie>0) then
         ! create a separate group for nodes sharing this energy
         call mpi_comm_group(MPI_COMM_WORLD, world_group, ierr)
         call mpi_group_incl(world_group, nodes_at_thisE,               &
     &       nodes_thisE_array, nodes_thisE_group, ierr)
         call mpi_comm_create(MPI_COMM_WORLD, nodes_thisE_group,        &
     &     nodes_thisE_comm, ierr)
         if ( associated(mpi_box) )                                     &
     &     mpi_box%thisE_comm = nodes_thisE_comm
         if (allocated(nodes_thisE_array)) deallocate(nodes_thisE_array)

       else
         call mpi_comm_create(MPI_COMM_WORLD, MPI_GROUP_EMPTY,          &
     &     nodes_thisE_comm, ierr)
         if (allocated(nodes_thisE_array)) deallocate(nodes_thisE_array)
         goto 2000
       end if

       ! now perform the actual job
       if ( dreal(egrd(ie))<egrd_min ) then
         do in=1,nbasis
           gf_box%dos(0:lmax,1:komp(in),in,1:nspin) = 0
           gf_box%green(:,1:komp(in),in,1:nspin) = 0
           gf_out%doslast(1:komp(in),in,1:nspin) = 0
           gf_out%doscklast(1:komp(in),in,1:nspin) = 0
           gf_out%greenlast(:,1:komp(in),in,1:nspin) = 0
         enddo
         goto 2000
       else
         if ( enforce_afm .or. enforce_dlm ) NSP = 1
!
!...MAIN CYCLE.....................................................................
!
         do is=1,nspin
            gf_box%energy(is) = egrd(ie)-vshift(is)
            if ( is>NSP ) cycle
            call getTauE ( iflag, mecca_state, is, lmax, efermi )
         enddo   ! is-cycle
!..................................................................................
         if ( enforce_dlm ) then
          call applyDLMsymm( lmax )
         end if
!         else if ( enforce_afm ) then
         if ( enforce_afm ) then
          call applyAFMsymm( lmax, nonafm )
          if ( nonafm .ne. 0 ) then
           write(6,*) nonafm,' non-AFM sublattices are present'
           call fstop(sname//                                           &
     &                   ': input is not compatible with AFM ordering')
          end if
         end if
         call totupGF( ie, lmax )
          if(ie.eq.nume) then
             do in=1,nbasis
               do ic=1,komp(in)
                 gf_out%doslast(ic,in,1:nspin) =                        &
     &          sum(gf_box%dos(0:lmax,ic,in,1:nspin),1)/nodes_at_thisE
                 gf_out%doscklast(ic,in,1:nspin) =                      &
     &             gf_box%dosck(ic,in,1:nspin)/nodes_at_thisE
!CAA                 do ir=1,jws(ic,in)
!C             For VP Integration
                 gf_out%greenlast(:,ic,in,1:nspin) =                    &
     &             gf_box%green(:,ic,in,1:nspin)/nodes_at_thisE
               enddo
             enddo
          else
            gf_out%doslast = 0
            gf_out%doscklast = 0
            gf_out%greenlast = 0
          endif
       end if
2000   continue ! end of round processing
!..................................................................................
       END IF
!..................................................................................
       ! if performing DoS run
       if( dos_scan ) then
        if ( nproc==1 ) then
          dostot(1:nspin) = gf_out%dostspn(1:nspin,ie)
          call writeDOSfiles( nspin, egrd(ie), lmax, dostot,            &
     &                           gf_box%dos(0:lmax,:,:,1:nspin) )
        else if ( nproc > 1) then  ! for multiple processes

         ! exchange complete DoS info between processes
         if ( in_place_mpi ) then
          call mpi_allreduce(MPI_IN_PLACE,mpi_dos_buffer,               &
     &       size(mpi_dos_buffer),                                      &
     &       MPI_COMPLEX16,MPI_SUM,MPI_COMM_WORLD,ierr)
         else
          z5buffer = mpi_dos_buffer
          call mpi_allreduce(z5buffer,mpi_dos_buffer,                   &
     &       size(mpi_dos_buffer),                                      &
     &       MPI_COMPLEX16,MPI_SUM,MPI_COMM_WORLD,ierr)
         end if

         ! the root node needs to print to DoS file in order
         if(myrank == 0) then

           ! print DoS related to energy by going through each job
           last_ie = -1
           do ijob = firstjob_this_round, lastjob_this_round

             ! find out what energy this job corresponded to
             call GetJobInfo(ijob,edu_indx,ie,ik_begin,ik_end,          &
     &                       nodes_thisE_array)

             ! skip if we already printed this energy
             if(ie == last_ie) cycle

             ! print DoS for this energy
             inode = ijob - firstjob_this_round
             dostot = 0
             do is = 1, nspin
               do in=1,nbasis
               do ic=1,inFile%sublat(in)%ncomp
                 sumdos = sum(mpi_dos_buffer(inode,0:lmax,ic,in,is))
                 dostot(is) = dostot(is) +                              &
     &              sumdos*atcon(ic,in)*inFile%sublat(in)%ns
               enddo; enddo
             end do
             call writeDOSfiles( nspin, egrd(ie), lmax, dostot,         &
     &           mpi_dos_buffer(inode,0:lmax,:,:,1:nspin) )

             last_ie = ie

           enddo
         endif
        endif
       endif

       ! destroy the communicator associated with this energy point
       if( myjob_this_round <= lastjob_this_round ) then
         call mpi_comm_free(nodes_thisE_comm,ierr)
         call mpi_group_free(nodes_thisE_group,ierr)
       endif

       ! syncronize processes at the end of round
!MPIDEBUG      call log_mpi_sync(__FILE__,__LINE__)
       call mpi_barrier(MPI_COMM_WORLD,ierr)

       ! setup for the next round
       firstjob_this_round = lastjob_this_round + 1

      end do    ! ie-cycle
      call getTauE(-1,mecca_state,nspin,lmax)
      if ( allocated(z5buffer) ) deallocate(z5buffer)
!c============================================================================

      ! for parallel execution we want to exchange the information
      ! totaled up for the green's function in 'totupGF'
      if ( in_place_mpi ) then
       call mpi_allreduce(MPI_IN_PLACE,gf_out%xvalws,                   &
     &       size(gf_out%xvalws),                                       &
     &       MPI_REAL8,MPI_SUM,MPI_COMM_WORLD,ierr)
       call mpi_allreduce(MPI_IN_PLACE,gf_out%evalsum(1,1,1),           &
     &       size(gf_out%evalsum),                                      &
     &       MPI_REAL8,MPI_SUM,MPI_COMM_WORLD,ierr)
       call mpi_allreduce(MPI_IN_PLACE,gf_out%xvalmt(1,1,1),            &
     &       size(gf_out%xvalmt),                                       &
     &       MPI_REAL8,MPI_SUM,MPI_COMM_WORLD,ierr)
       call mpi_allreduce(MPI_IN_PLACE,gf_out%greenint(1,1,1,1),        &
     &       size(gf_out%greenint),                                     &
     &      MPI_COMPLEX16,MPI_SUM,MPI_COMM_WORLD,ierr)
       call mpi_allreduce(MPI_IN_PLACE,gf_out%dostspn(1,1),             &
     &       size(gf_out%dostspn),                                      &
     &      MPI_COMPLEX16,MPI_SUM,MPI_COMM_WORLD,ierr)

      ! all processes should get the last G and dos

       call mpi_allreduce(MPI_IN_PLACE,gf_out%doslast(1,1,1),           &
     &       size(gf_out%doslast),                                      &
     &       MPI_COMPLEX16,MPI_SUM,MPI_COMM_WORLD,ierr)
       call mpi_allreduce(MPI_IN_PLACE,gf_out%doscklast(1,1,1),         &
     &       size(gf_out%doscklast),                                    &
     &      MPI_COMPLEX16,MPI_SUM,MPI_COMM_WORLD,ierr)
       call mpi_allreduce(MPI_IN_PLACE,gf_out%greenlast(1,1,1,1),       &
     &       size(gf_out%greenlast),                                    &
     &      MPI_COMPLEX16,MPI_SUM,MPI_COMM_WORLD,ierr)
      else
       idm(1:3) = shape(gf_out%xvalws)
       allocate(r3buffer(idm(1),idm(2),idm(3)))
       r3buffer = gf_out%xvalws
       call mpi_allreduce(r3buffer,gf_out%xvalws,                       &
     &       size(gf_out%xvalws),                                       &
     &       MPI_REAL8,MPI_SUM,MPI_COMM_WORLD,ierr)
       r3buffer = gf_out%xvalmt
       call mpi_allreduce(r3buffer,gf_out%xvalmt(1,1,1),                &
     &       size(gf_out%xvalmt),                                       &
     &       MPI_REAL8,MPI_SUM,MPI_COMM_WORLD,ierr)
       r3buffer = gf_out%evalsum
       call mpi_allreduce(r3buffer,gf_out%evalsum(1,1,1),               &
     &       size(gf_out%evalsum),                                      &
     &       MPI_REAL8,MPI_SUM,MPI_COMM_WORLD,ierr)
       if ( allocated(r3buffer) ) deallocate(r3buffer)

       idm(1:4) = shape(gf_out%greenint)
       allocate(z4buffer(idm(1),idm(2),idm(3),idm(4)))
       z4buffer = gf_out%greenint
       call mpi_allreduce(z4buffer,gf_out%greenint(1,1,1,1),            &!
     &       size(gf_out%greenint),                                     &
     &      MPI_COMPLEX16,MPI_SUM,MPI_COMM_WORLD,ierr)
       z4buffer = gf_out%greenlast
       call mpi_allreduce(z4buffer,gf_out%greenlast(1,1,1,1),           &!
     &       size(gf_out%greenlast),                                    &
     &      MPI_COMPLEX16,MPI_SUM,MPI_COMM_WORLD,ierr)
       if ( allocated(z4buffer) ) deallocate(z4buffer)

       idm(1:3) = shape(gf_out%doslast)
       allocate(z3buffer(idm(1),idm(2),idm(3)))
       z3buffer = gf_out%doslast
       call mpi_allreduce(z3buffer,gf_out%doslast(1,1,1),               &!
     &       size(gf_out%doslast),                                      &
     &       MPI_COMPLEX16,MPI_SUM,MPI_COMM_WORLD,ierr)
       z3buffer = gf_out%doscklast
       call mpi_allreduce(z3buffer,gf_out%doscklast(1,1,1),             &!
     &       size(gf_out%doscklast),                                    &
     &      MPI_COMPLEX16,MPI_SUM,MPI_COMM_WORLD,ierr)
       if ( allocated(z3buffer) ) deallocate(z3buffer)

       idm(1:2) = shape(gf_out%dostspn)
       allocate(z2buffer(idm(1),idm(2)))
       z2buffer = gf_out%dostspn
       call mpi_allreduce(z2buffer,gf_out%dostspn(1,1),                 &!
     &       size(gf_out%dostspn),                                      &
     &      MPI_COMPLEX16,MPI_SUM,MPI_COMM_WORLD,ierr)
       if ( allocated(z2buffer) ) deallocate(z2buffer)
      end if

!      << all processes should converge by here >>

      if (nspin.eq.1) then
       gf_out%dostspn(2,1:nume) = gf_out%dostspn(1,1:nume)
! and spin-factor for all others
      end if

      if(iprint.ge.-1) then
!       if(iset_b_cont.eq.0)then
       if ( maxval(abs(gf_out%dostspn(1:nspin,1:nume))) >               &
     &           epsilon(one) ) then
        write(6,*)
        if(nspin.eq.2) then
         do ie=1,nume
          write(6,1001) egrd(ie),gf_out%dostspn(1,ie)/natomcnt,         &
     &                  '  Re+Im: E , dos_spin1 '
         end do
!DELETE1001       format(2f14.7,1x,2g16.8,1x,a24)
!DELETE1002       format(2f14.7,1x,2g16.8,1x,2g16.8,3x,a)
1001     format(g15.7,1x,f14.7,1x,g16.8,1x,g16.8,1x,a24)
1002     format(g15.7,1x,f14.7,2(1x,g16.8,1x,g16.8),3x,a)

         write(6,*)
         do ie=1,nume
          write(6,1001) egrd(ie),gf_out%dostspn(2,ie)/natomcnt,         &
     &                  '  Re+Im: E , dos_spin2 '
         end do
         write(6,*)
        end if
        if(Tempr > zero) then
        do ie=1,nume
         dostmp=sum(gf_out%dostspn(1:2,ie))/natomcnt
         zz = (dreal(egrd(ie))-inFile%etop)/Tempr
         zz = fermiFun(zz)
!         dos_Ff = aimag(dostmp)*fermiFun(zz)
         write(6,1002) egrd(ie),dostmp,                                 &
     &     aimag(dostmp)*zz,zz,                                         &
     &   'Re+Im: E,dos/atom,dos/atom*Fermifunc(Re(E),T))'
         write(char80,1001) dostmp
         if ( index(char80,'NaN')>0 ) then
          call fstop(sname//' ERROR: dos value is NaN')
         end if
        end do
        else
        do ie=1,nume
         write(6,1001) egrd(ie),sum(gf_out%dostspn(1:2,ie))/natomcnt,   &
     &                                             ' Re+Im: E,dos/atom'
         write(char80,1001) gf_out%dostspn(1:2,ie)
         if ( index(char80,'NaN')>0 ) then
          call fstop(sname//' ERROR: dos value is NaN')
         end if

        end do
        endif ! T>0
       end if
!       endif        ! End of iset_b_cont loop
      end if

!c     ================================================================

      call deallocGFdata(gf_box)
      if ( allocated(egrd) )     deallocate(egrd)
      if ( allocated(dele1) )    deallocate(dele1)
      if ( allocated(komp) )     deallocate(komp)
      if ( allocated(atcon) )    deallocate(atcon)
      if ( allocated(rmt_true) ) deallocate(rmt_true)
      if ( allocated(r_circ) )   deallocate(r_circ)
      if ( allocated(edu_indx) ) deallocate(edu_indx)
      if ( allocated(mpi_dos_buffer) ) deallocate(mpi_dos_buffer)
      if ( allocated(nodes_thisE_array) ) deallocate(nodes_thisE_array)
!      nullify(bsf_box)

      if(istop.eq.sname) then
        call fstop(sname)
      endif
!c
      return
!EOC
      end subroutine calcZkkr

      ! for parallel run, convert job ID to index into energy and k
      subroutine GetJobInfo(jobid,edu_indx,ie,ik_begin,ik_end,          &
     &                      nodes_thisE)

        ! warning: this routine uses a few backdoor parameters
        !  (e.g. firstjob_this_round)

        ! jobid = 0,1,2,...

        implicit none
        integer, intent(in)  :: jobid,edu_indx(:)
        integer, intent(out) :: ie, ik_begin, ik_end
        integer :: i, ik, dk
        integer, intent(out), allocatable :: nodes_thisE(:)

        if(allocated(nodes_thisE)) deallocate(nodes_thisE)

        ! are we in the low-E k-mesh?
        if( jobid < nenergy_lowE*nodes_per_E_lowE ) then
          allocate(nodes_thisE(nodes_per_E_lowE))
          ie = 1 + jobid/nodes_per_E_lowE
          ie = edu_indx(ie)
          ik = mod(jobid,nodes_per_E_lowE)
          dk = nspecialk_lowE/nodes_per_E_lowE
          ik_begin = 1 + ik*dk
          ik_end   = (ik+1)*dk
          if(ik == nodes_per_E_lowE-1) ik_end = nspecialk_lowE
        else
          allocate(nodes_thisE(nodes_per_E_highE))
          ie = 1 + nenergy_lowE +                                       &
     &      (jobid-nenergy_lowE*nodes_per_E_lowE)/nodes_per_E_highE
          if ( ie>0 .and. ie<=size(edu_indx) ) then
           ie = edu_indx(ie)
          else
           ie = 0
          end if
          ik = mod(jobid-nenergy_lowE*nodes_per_E_lowE,                 &
     &      nodes_per_E_highE)
          dk = nspecialk_highE/nodes_per_E_highE
          ik_begin = 1 + ik*dk
          ik_end   = (ik+1)*dk
          if(ik == nodes_per_E_highE-1) ik_end = nspecialk_highE
        endif

        ! what other processes are assigned this E point?
        do i = 1, size(nodes_thisE)
          nodes_thisE(i) = jobid-firstjob_this_round-ik+i-1
        end do

      end subroutine GetJobInfo
!
!c============================================================================
!BOP
!!IROUTINE: getTauE
!!INTERFACE:
      subroutine getTauE ( iflag, mecca_state, ispin, lmax, ef )
!!DESCRIPTION:
! single E-point calculation of KKR-CPA Green's function data:
! {\tt mecca\_state\%gf\_box (type GF\_data)}
!
!c============================================================================
!!REVISION HISTORY:
! Initial Version - A.S. - 2013
!  changed arguments - A.S. - 2016
!!REMARKS:
! private module data are used for input/output
!EOP
      use mecca_constants
      use mecca_types
      use gfncts_interface, only : g_reltype,g_atcon,g_komp

!
!BOC
      implicit none
      character(10), parameter :: sname = 'getTauE'
      integer, intent(in) :: iflag
      type(RunState), target :: mecca_state
      integer, intent(in) :: ispin
      integer, intent(in) :: lmax
      real(8), optional, intent(in) :: ef

      integer :: kkrsz, kkrsz2, kkr2l, ndimrhp,ndimlhp
      integer :: l,m,m3,ksint_sch,ivers_tau,idosplt,irecdlm
      integer :: st_env
      real(8) :: a,con
      complex(8) :: cirl,edu

      type ( IniFile ), pointer :: inFile=>null()
      type ( RS_Str ), pointer :: rs_box=>null()
      type ( KS_Str ), pointer :: ks_box=>null()
      type ( EW_Str ), pointer :: ew_box=>null()
      type (GF_data), pointer :: gf_box=>null()
      type (GF_output), pointer :: gf_out=>null()

      integer :: nrelv
      integer :: ndrpts,mxcomp,nbasis,nspin,nsub
      logical :: lVP
      real(8) :: efermi,rytodu
      real(8), allocatable :: rmt_true(:,:)
      real(8), allocatable :: r_circ(:,:)
      real(8), allocatable :: r_ref(:)

      type (VP_data), pointer :: vp_box=>null()
      type (DosBsf_data), pointer :: bsf_box=>null()
      type (FS_data),  pointer :: fs_box=>null()
!      type (MPI_data),  pointer :: mpi_box

      integer, allocatable :: komp(:)
      real(8), allocatable :: atcon(:,:)
      real(8) :: conk
      complex(8), allocatable, save :: conr(:)
      complex(8), allocatable, save :: hplnm(:,:)

      if ( iflag<0 ) then
          if ( allocated(hplnm) ) deallocate(hplnm)
          if ( allocated(conr) ) deallocate(conr)
          return
      end if
      nullify(inFile) ; if ( associated(mecca_state%intfile) )          &
     &  inFile => mecca_state%intfile
      nullify(rs_box) ; if ( associated(mecca_state%rs_box)  )          &
     &  rs_box => mecca_state%rs_box
      nullify(ks_box) ; if ( associated(mecca_state%ks_box) )           &
     &  ks_box => mecca_state%ks_box
      nullify(ew_box) ; if ( associated(mecca_state%ew_box) )           &
     &  ew_box => mecca_state%ew_box
      nullify(gf_box) ; if ( associated(mecca_state%gf_box) )           &
     &  gf_box => mecca_state%gf_box
      nullify(gf_out) ; if ( associated(mecca_state%gf_out_box) )       &
     &  gf_out => mecca_state%gf_out_box

      if ( .not. ( associated(inFile) .and.                             &
     &             associated(rs_box) .and.                             &
     &             associated(ks_box) .and.                             &
     &             associated(ew_box) .and.                             &
     &             associated(gf_box) .and.                             &
     &             associated(gf_out) ) ) then
       call fstop(sname//' :: ERROR :: pointers are not associated')
      end if

      nullify(vp_box) ; if ( associated(mecca_state%vp_box) )           &
     &  vp_box => mecca_state%vp_box
      nullify(bsf_box) ; if ( associated(mecca_state%bsf_box) )         &
     &  bsf_box => mecca_state%bsf_box
      nullify(fs_box) ; if ( associated(mecca_state%fs_box) )           &
     &  fs_box => mecca_state%fs_box

      idosplt = 0
!      if ( associated(bsf_box) ) then
!       idosplt = bsf_box%idosplot
!      end if
!      lVP = .false.
!      if ( associated(vp_box) ) then
!       lVP = (vp_box%nF>0) .and. (inFile%mtasa>1)
!      end if
!
      IF ( lMS ) THEN

       rytodu = rs_box%alat/(two*pi)
       edu = gf_box%energy(ispin)*rytodu**2
       call get_recdlm(edu,irecdlm)
       call gEnvVar('ENV_TAU_RECDLM',.false.,st_env)
       if ( st_env>=0 ) then
        irecdlm = min(st_env,1)
       end if
!
       ndimlhp=(2*lmax+1)*(2*lmax+1)
       if ( inFile%imethod>0 .and. irecdlm==0 ) then
!DEBUG      if ( inFile%imethod>0 ) then
          ndimrhp = 1
       else
          ndimrhp=size(ew_box%xrslatij(:,1))
       end if
       if ( allocated(hplnm) ) then
        if ( size(hplnm(1,:)).ne.ndimlhp ) deallocate(hplnm)
       end if
       if ( .not.allocated(hplnm) ) then
        allocate(hplnm(ndimrhp,ndimlhp))
!c     ----------------------------------------------------------------
!c               (r**l)*Y_lm(r)   [ M.ge.0]
!c     ----------------------------------------------------------------
        call rlylm(ew_box%xrslatij,ew_box%nrsij,ew_box%ndimrij,lmax,    &
     &            hplnm,ndimrhp)
       end if
!c
!c         set up r-,k-space coefficients
!c     ----------------------------------------------------------------
       kkrsz=(lmax+1)**2
       kkrsz2=kkrsz*kkrsz
       kkr2l=(2*lmax+1)**2
       if ( allocated(conr) ) then
        if ( size(conr).ne.kkr2l ) then
         deallocate(conr)
        end if
       end if
       if ( .not.allocated(conr) ) then
        allocate(conr(1:kkr2l))
!c  calculate r-space coefficents -4.0/sqrt( pi)*2**l (for Ewald)
!c
        a=-four/sqrt(pi)
!c
!c     calculate indexing for array dqint which is the integral in
!c     real-space based on Ewald formulation. This integral only
!c     depends on shell and energy (not on k space).
!c     Calculate dqint once per energy  (see gettau & ewald).
!c
!c      ndlj= (2*lmax+1)**2
!c      ndx(1:ndlj) -> l+1   ! not needed anymore
        m3=0
        do l=0,2*lmax
         con=a*2**l
         cirl=(-dcmplx(zero,one))**l
         do m=-l,l
           m3=m3+1
           conr(m3) = (con/2)*cirl     ! conr() -- is used in ewald-routine only
         enddo
        enddo
       end if
!c     ----------------------------------------------------------------
!c  calculate k-space coefficient in D.U. (for Ewald)
!c          conk=-4*pi/((2*pi/alat)^3*omega),
!c              here omega -- volume of unit cell (real space)
!c
       conk=-(four*pi)/(rs_box%volume*(two*pi/rs_box%alat)**3) ! -(four*pi)/(omegws*dutory**3)
      ELSE
       if ( .not.allocated(hplnm) ) then
        ndimrhp=1
        ndimlhp=1
        allocate(hplnm(ndimrhp,ndimlhp))
       end if
       if ( .not.allocated(conr) ) allocate(conr(1))
      END IF

      call g_ndims(inFile,ndrpts,mxcomp,nbasis,nspin)
      allocate(r_ref(nbasis))
!c
!      r_ref = minval(rs_box%rws(1:rs_box%nsubl))  ! reference system radius
      r_ref = rs_box%volume/sum(rs_box%numbsubl(1:nbasis))
      r_ref = (r_ref*three/(four*pi))**(one/three)
      if ( inFile%mtasa == 0 ) then
        r_ref = minval(rs_box%rmt(1:nbasis))/                           &
     &            minval(rs_box%rws(1:nbasis)) * r_ref
      end if

      ivers_tau = g_unpack(iflag,ksint_sch)   ! ksint_sch: 1 - monkhorst-pack,  2 - ray method

      select case ( ivers_tau )
      case ( tau_ray )
            if ( .not. ( associated(vp_box) .and.                       &
     &                   associated(bsf_box) .and.                      &
     &                   associated(fs_box) ) ) then
             call fstop(sname//' :: ERROR :: not all pointers'//        &
     &                                     ' are associated')
            end if

            allocate(rmt_true(mxcomp,nbasis))
            allocate(r_circ(mxcomp,nbasis))
            call g_atcon(inFile,atcon)
            call g_komp(inFile,komp)
            do nsub=1,nbasis
!             rmt_true(1:komp(nsub),nsub) = rs_box%rmt(nsub)
             rmt_true(1:komp(nsub),nsub) =                              &
     &               inFile%sublat(nsub)%compon(1:komp(nsub))%rSph
             r_circ(1:komp(nsub),nsub) = rs_box%rcs(nsub)
            end do
            if ( present(ef) ) then
             efermi = ef
            else
             efermi = inFile%etop
            end if

            call g_reltype(valence=nrelv)
            call gettau_ray(ispin,inFile%nspin,nrelv,                   &
!!     &                inFile%icryst,                                  &
     &                  lmax,kkrsz,rs_box%nsubl,rs_box%numbsubl,        &
     &                  rs_box%alat,rs_box%volume,rs_box%aij,           &
     &                  rs_box%itype,rs_box%natom,                      &
     &                  rs_box%if0,rs_box%mapstr,rs_box%mappnt,         &
     &                  size(rs_box%mappnt,1),rs_box%mapij,             &
     &                  komp,atcon,                                     &
     &                  ew_box%xknlat,ew_box%Rksp,                      &
     &                  ew_box%nksn,                                    &
     &                  ks_box%qmesh,ks_box%ndimq,size(ks_box%nqpt),    &
     &                  ks_box%nqpt,                                    &
     &                  ks_box%wghtq,ks_box%lrot,                       &
     &                  ks_box%ngrp,ks_box%kptgrp,ks_box%kptset,        &
     &                  size(ks_box%kptset(:,1)),                       &
     &                  ks_box%kptindx,                                 &
     &                  inFile%sublat(1:rs_box%nsubl),r_ref,            &
     &                  rs_box%rws(:),                                  &
     &                  rmt_true,r_circ,inFile%intgrsch,                &
     &                  vp_box%fcount,vp_box%weight,                    &
     &                  vp_box%rmag(0,:,:,:,:,:),vp_box%vj,lVP,         &
     &                  gf_box%energy(ispin),inFile%ncpa,               &
     &                  ew_box%xrslatij,ew_box%nrsij,ew_box%ndimrij,    &
     &                  ew_box%np2r,ew_box%ndimr,                       &
     &                  ew_box%numbrs,rs_box%ncount,                    &
     &                  ks_box%rot,ks_box%dop,ks_box%nrot,              &
     &                  hplnm,ndimrhp,ndimlhp,                          &
     &                  irecdlm,                                        &
     &                  gf_box%dos(0:,1:,1:,ispin),                     &
     &                  gf_box%dosck(1:,1:,ispin),                      &
     &                  gf_box%green(1:,1:,1:,ispin),                   &
     &                  inFile%eneta,inFile%enKlim,                     &
     &                  conk,conr,                                      &
     &                  rs_box%rslatt,ks_box%kslatt,rs_box%Rnncut,      &
     &                  rs_box%Rnncut,rs_box%mapsprs,                   & ! here Rsmall=Rnncut
     &                  inFile%mtasa,                                   &
     &                  ksint_sch,efermi,inFile%iprint,inFile%istop     &
     &                 ,idosplt,bsf_box,fs_box)

      case ( tau_scf )
             call gettau_scf(ispin,inFile,gf_box,                       &
     &                   rs_box,ks_box,ew_box,idosplt)

      case ( mecca_ames )
      case default
             call gettau_scf(ispin,inFile,gf_box,                       &
     &                   rs_box,ks_box,ew_box,idosplt)

      end select

      if ( allocated(atcon) )    deallocate(atcon)
      if ( allocated(komp) )     deallocate(komp)
      if ( allocated(rmt_true) ) deallocate(rmt_true)
      if ( allocated(r_circ) )   deallocate(r_circ)
      if ( allocated(r_ref) )   deallocate(r_ref)

      return
!EOC
      contains
!
!BOP
!!IROUTINE: gettau_scf
!!INTERFACE:
      subroutine gettau_scf(ispin,inFile,gf_box,                        &
     &                       rs_box,ks_box,ew_box,idos)
!!DESCRIPTION:
! this subroutine is a basic procedure of getTauE subroutine to calculate
! KKR-CPA Green's function data needed for scf cycle
!
!!USES:
!      use mecca_types, only : lj
      use mecca_types
      use sctrng, only : getSSgf,getMSgf,gf2dos,setSStype
      use sctrng, only : getRelSSgf
      use sctrng, only : IS_ss,ASA_ss,MIX_ss
!
!!REVISION HISTORY:
! Initial Version - A.S. - 2013
!EOP
!
!BOC
      implicit none
      integer, intent(in) :: ispin
      type ( IniFile ) :: inFile
      type (GF_data)   :: gf_box
      type ( RS_Str )  :: rs_box
      type ( KS_Str )  :: ks_box
      type ( EW_Str )  :: ew_box
      integer, intent(in) :: idos
      complex(8) :: energy,edu
      integer :: iprint,nbasis,ndcomp,lmax,L_limit,Lss_max,iv,i_ss,nrelv
      integer :: irecdlm,kkrsz,ncpa_max
      complex(8), allocatable :: greentmp(:,:,:)
      complex(8), pointer :: tab(:,:,:,:)=>null()
      integer, allocatable :: komp(:)
      real(8), allocatable :: atcon(:,:)
      real(8) :: rytodu,ptempr,t
      real(8), parameter :: clight=two*inv_fine_struct_const
      integer, parameter :: lx=size(lj)/2

      lmax = inFile%lmax
      kkrsz=(lmax+1)**2
      call g_atcon(inFile,atcon)
      call g_komp(inFile,komp)
      energy = gf_box%energy(ispin)
      iprint = inFile%iprint
      nbasis = rs_box%nsubl
      ndcomp = size(atcon,1)
!      Lss_max = 1+int(2*sqrt(inFile%Tempr)*maxval(rs_box%rws(1:nbasis)))
      t = zero
      if ( inFile%Tempr > zero ) then
        t = max(1.5d0*inFile%Tempr,inFile%V0(ispin))
      end if
      ptempr = sqrt(2*m_e*t*(one + t/(2*m_e*clight**2)))
      Lss_max = lminSS + nint(ptempr*maxval(rs_box%rws(1:nbasis)))

!      if ( Lss_max <= lmax ) then
!        L_limit = lmax
!      else if ( inFile%Tempr < 1/(2*ry2eV) ) then
!        L_limit = min(Lss_max,lmax+1)
!      else
!       L_limit = Lss_max
!      end if

      L_limit = lmax + Lss_max
      if ( t > zero ) L_limit = max(L_limit,lx)
      call gEnvVar('ENV_L_LIMIT',.false.,iv)
      if ( iv >= 0 ) then
       if ( iprint >=0 ) then
        write(6,'('' VARIABLE ENV_L_LIMIT =: '',i4)') iv
       end if
       L_limit = max(iv,lmax)
      end if

      call gEnvVar('ENV_SSTYPE',.false.,i_ss)
      if ( i_ss >= 0 ) then
       if ( iprint >=0 ) then
        write(6,'('' VARIABLE ENV_SSTYPE =: '',i4)') i_ss
       end if
      else
       if ( inFile%mtasa == 2 ) then
        i_ss = ASA_ss    ! ASA_ss = 1, IS_ss = 0
       else
        i_ss = ASA_ss
       end if
      end if
      rytodu = rs_box%alat/(two*pi)
      edu = energy*rytodu**2
      call get_recdlm(edu,irecdlm)
      call gEnvVar('ENV_TAU_RECDLM',.false.,iv)
      if ( iv>=0 ) then
        irecdlm = min(iv,1)
      end if


      call g_reltype(valence=nrelv)
      call setSStype(i_ss)
      ncpa_max = inFile%ncpa

      if ( nrelv==0 ) then
       IF ( lSS ) THEN
        allocate (tab(1+max(lmax,L_limit),2,ndcomp,nbasis))
        call getRelSSgf(energy,lmax,L_limit,ispin,nrelv,inFile%so_power,&
     &                   inFile%alat,inFile%sublat,iprint,              &
     &                   gf_box%green(1:,1:,1:,ispin),                  & ! calculations of Dirac SS density
     &                   tab)                                             ! Dirac-tab is not used for MS
        deallocate(tab)
       END IF
       IF ( lMS ) THEN                                                    ! back-scattering
        allocate (tab(kkrsz,kkrsz,ndcomp,nbasis))
        allocate (greentmp(size(gf_box%green,1),ndcomp,nbasis))
        call getSSgf(energy,lmax,lmax,ispin,nrelv,                      &
     &             ndcomp,nbasis,inFile%alat,inFile%sublat,iprint,      & ! calculation of scalar-relativistic SS-tab
     &             greentmp,tab,no_gf=1)
        if ( allocated(greentmp) ) deallocate(greentmp)
        call getMSgf(tab,ispin,nrelv,lmax,kkrsz,                        &
     &                  nbasis,rs_box%numbsubl,inFile%alat,rs_box%aij,  &
     &                  rs_box%itype,rs_box%natom,                      &
     &                  rs_box%if0,rs_box%mapstr,rs_box%mappnt,         &
     &                  size(rs_box%mappnt,1),rs_box%mapij,             &
     &                  komp,atcon,                                     &
     &                  ew_box%xknlat,ew_box%Rksp,                      &
     &                  ew_box%nksn,                                    &
     &                  ks_box%qmesh,ks_box%ndimq,size(ks_box%nqpt),    &
     &                  ks_box%nqpt,                                    &
     &                  ks_box%wghtq,ks_box%lrot,                       &
     &                  ks_box%ngrp,ks_box%kptgrp,ks_box%kptset,        &
     &                  size(ks_box%kptset,1),                          &
     &                  ks_box%kptindx,                                 &
     &                  inFile%sublat(1:rs_box%nsubl),r_ref,            &
     &                  rs_box%rws(:),                                  &
     &                  energy,ncpa_max,                                &
     &                  ew_box%xrslatij,ew_box%nrsij,ew_box%ndimrij,    &
     &                  ew_box%np2r,ew_box%ndimr,                       &
     &                  ew_box%numbrs,rs_box%ncount,                    &
     &                  ks_box%rot,ks_box%dop,ks_box%nrot,              &
     &                  hplnm,ndimrhp,ndimlhp,                          &
     &                  irecdlm,                                        &
     &                  gf_box%green(1:,1:,1:,ispin),                   &
     &                  inFile%eneta,inFile%enKlim,                     &
     &                  conk,conr,                                      &
     &                  rs_box%rslatt,ks_box%kslatt,rs_box%Rnncut,      &
     &                  rs_box%Rnncut,rs_box%mapsprs,                   & ! here Rsmall=Rnncut
     &                  idos,ksint_sch,iprint,inFile%istop)
       END IF
      else
        allocate (tab(kkrsz,kkrsz,ndcomp,nbasis))
        call getSSgf(energy,lmax,L_limit,ispin,nrelv,                   & ! older version scalar-relat. SS density and tab
     &             ndcomp,nbasis,inFile%alat,inFile%sublat,iprint,      &
     &             gf_box%green(:,:,:,ispin),tab)
        call getMSgf(tab,ispin,nrelv,lmax,kkrsz,                        &
     &                  nbasis,rs_box%numbsubl,inFile%alat,rs_box%aij,  &
     &                  rs_box%itype,rs_box%natom,                      &
     &                  rs_box%if0,rs_box%mapstr,rs_box%mappnt,         &
     &                  size(rs_box%mappnt,1),rs_box%mapij,             &
     &                  komp,atcon,                                     &
     &                  ew_box%xknlat,ew_box%Rksp,                      &
     &                  ew_box%nksn,                                    &
     &                  ks_box%qmesh,ks_box%ndimq,size(ks_box%nqpt),    &
     &                  ks_box%nqpt,                                    &
     &                  ks_box%wghtq,ks_box%lrot,                       &
     &                  ks_box%ngrp,ks_box%kptgrp,ks_box%kptset,        &
     &                  size(ks_box%kptset,1),                          &
     &                  ks_box%kptindx,                                 &
     &                  inFile%sublat(1:rs_box%nsubl),r_ref,            &
     &                  rs_box%rws(:),                                  &
     &                  energy,ncpa_max,                                &
     &                  ew_box%xrslatij,ew_box%nrsij,ew_box%ndimrij,    &
     &                  ew_box%np2r,ew_box%ndimr,                       &
     &                  ew_box%numbrs,rs_box%ncount,                    &
     &                  ks_box%rot,ks_box%dop,ks_box%nrot,              &
     &                  hplnm,ndimrhp,ndimlhp,                          &
     &                  irecdlm,                                        &
     &                  gf_box%green(1:,1:,1:,ispin),                   &
     &                  inFile%eneta,inFile%enKlim,                     &
     &                  conk,conr,                                      &
     &                  rs_box%rslatt,ks_box%kslatt,rs_box%Rnncut,      &
     &                  rs_box%Rnncut,rs_box%mapsprs,                   & ! here Rsmall=Rnncut
     &                  idos,ksint_sch,iprint,inFile%istop)
      end if
      if ( associated(tab) )     deallocate(tab)
      if ( allocated(atcon) )    deallocate(atcon)
      if ( allocated(komp) )     deallocate(komp)

      call gf2dos(ispin,gf_box%green(1:,1:ndcomp,1:nbasis,ispin))

!DEBUG
!       call fstop('DEBUG')
!DEBUG
      return
!EOC
      end subroutine gettau_scf
!
      end subroutine getTauE
!
!BOP
!!IROUTINE: doscomp_nunit
!!INTERFACE:
      function doscomp_nunit(n0,in,ic)
!!DESCRIPTION:
! provides fortran unit number for DOS calculations
!
!!REVISION HISTORY:
! Initial Version - A.S. - 2013
!EOP
!
!BOC
      integer doscomp_nunit
      integer, intent(in) :: n0,in,ic
      doscomp_nunit = n0+10*in+ic
      return
!EOC
      end function doscomp_nunit
!
!c============================================================================
!BOP
!!IROUTINE: createDOSfiles
!!INTERFACE:
      subroutine createDOSfiles (  )
!c============================================================================
!!REVISION HISTORY:
! Initial Version - A.S. - 2013
!EOP
!
!BOC
      implicit none
      integer :: i,in,ic,lmx
      real(8) :: efermi
      integer :: dfo
      logical :: ex
      character(250) :: stl

      if ( .not. opendos .or. .not.associated(bsf_box) ) return

        efermi = bsf_box%bsf_ef
        lmx = inFile%lmax
        dfo = inFile%io(nu_dos)%nunit
        inquire(unit=dfo,opened=opendos )
        if ( opendos ) close(dfo)
        open(dfo,file='dos_all.'//inFile%genName,status='unknown')
        write(dfo,'(a,f18.10,a,f12.8,a)') '# efermi = ',                &
     &       efermi, " + ",aimag(egrd(1)),' i'
        if(nspin == 2) then
          write(dfo,'(a)') '#      energy(ryd)        total/cell'       &
     &//'           up/cell           dn/cell'
        else
          write(dfo,'(a)') '#      energy(ryd)        total/cell'
        end if

        do in=1,nbasis
        do ic=1,inFile%sublat(in)%ncomp

         write(dfs,'(a,i2.2,a,i2.2)') 'dos_s',in,'c',ic
         dfs = trim(dfs)//'.'//inFile%genName
         dfo = doscomp_nunit(inFile%io(nu_dos)%nunit,in,ic)
         inquire(unit=dfo,exist=ex)
         if ( .not.ex ) then
             write(6,'('' WARNING: unit number '',i4,                   &
     &'' is out of range,'',                                            &
     & '' DOS for nsub='',i4,'', comp='',i2,'' is not reported'' )')    &
     &           dfo,in,ic
          cycle
         else
          open(dfo,file=trim(dfs),status='unknown')
          write(dfo,'(a,f18.10,a,f12.8,a)') '# efermi = ',              &
     &        efermi,' + ',aimag(egrd(1)),' i'
          stl = '#...energy(ryd).'
          i = len_trim(stl)
          if(nspin == 2) then
           if ( inFile%sublat(in)%compon(ic)%zID.ne.0 ) then
            write(stl(i+1:i+16),'(a)') '.....component..'
            i = len_trim(stl)
            if ( lmx>=0 ) write(stl(i+1:i+16),'(a)')                    &
     &                    '..tot_OR_s(up)..'
            i = len_trim(stl)
            if ( lmx>=1 ) write(stl(i+1:i+16),'(a)')                    &
     &                    '.......p(up)....'
            i = len_trim(stl)
            if ( lmx>=2 ) write(stl(i+1:i+16),'(a)')                    &
     &                    '.......d(up)....'
            i = len_trim(stl)
            if ( lmx>=3 ) write(stl(i+1:i+16),'(a)')                    &
     &                    '.......f(up)....'
            i = len_trim(stl)
            if ( lmx>=4 ) write(stl(i+1:i+16),'(a)')                    &
     &                    '.......g(up)....'
            i = len_trim(stl)
            if ( lmx>=5 ) write(stl(i+1:i+16),'(a)')                    &
     &                    '.......h(up)....'
            i = len_trim(stl)
            write(stl(i+1:i+16),'(a)') '.....component..'
            i = len_trim(stl)
            if ( lmx>=0 ) write(stl(i+1:i+16),'(a)')                    &
     &                    '..tot_OR_s(dn)..'
            i = len_trim(stl)
            if ( lmx>=1 ) write(stl(i+1:i+16),'(a)')                    &
     &                    '.......p(dn)....'
            i = len_trim(stl)
            if ( lmx>=2 ) write(stl(i+1:i+16),'(a)')                    &
     &                    '.......d(dn)....'
            i = len_trim(stl)
            if ( lmx>=3 ) write(stl(i+1:i+16),'(a)')                    &
     &                    '.......f(dn)....'
            i = len_trim(stl)
            if ( lmx>=4 ) write(stl(i+1:i+16),'(a)')                    &
     &                    '.......g(dn)....'
            i = len_trim(stl)
            if ( lmx>=5 ) write(stl(i+1:i+16),'(a)')                    &
     &                    '.......h(dn)....'
           else
            write(stl(i+1:i+16),'(a)') '.....tot(up)....'
            i = len_trim(stl)
            write(stl(i+1:i+16),'(a)') '.....tot(dn)....'
           end if
          else
           if ( inFile%sublat(in)%compon(ic)%zID.ne.0 ) then
            write(stl(i+1:i+16),'(a)') '.....component..'
            i = len_trim(stl)
            if ( lmx>=0 ) write(stl(i+1:i+16),'(a)')                    &
     &                    '....tot_OR_s....'
            i = len_trim(stl)
            if ( lmx>=1 ) write(stl(i+1:i+16),'(a)')                    &
     &                    '.......p........'
            i = len_trim(stl)
            if ( lmx>=2 ) write(stl(i+1:i+16),'(a)')                    &
     &                    '.......d........'
            i = len_trim(stl)
            if ( lmx>=3 ) write(stl(i+1:i+16),'(a)')                    &
     &                    '.......f........'
            i = len_trim(stl)
            if ( lmx>=4 ) write(stl(i+1:i+16),'(a)')                    &
     &                    '.......g........'
            i = len_trim(stl)
            if ( lmx>=5 ) write(stl(i+1:i+16),'(a)')                    &
     &                    '.......h........'
           else
            write(stl(i+1:i+16),'(a)') '....component...'
           end if
          end if
          write(dfo,'(a)') stl
         end if

        end do; end do

        opendos = .false.

      return
!EOC
      end subroutine createDOSfiles

!c============================================================================
!BOP
!!IROUTINE: writeDOSfiles
!!INTERFACE:
      subroutine writeDOSfiles( nsp, en, lmax, dostot, dos )
!!REVISION HISTORY:
! Initial Version - A.S. - 2013
!EOP
!
!BOC
      implicit none
      integer, intent(in) :: nsp   ! max spin
      integer, intent(in) :: lmax
      complex(8), intent(in) :: en, dostot(nsp)
      complex(8), intent(in) :: dos(0:,:,:,:)
      integer :: ic,in,dfo
      complex(8) :: dossc1=0,dossc=0
      logical ex

            dfo = inFile%io(nu_dos)%nunit
            if(nsp == 1) then
              write(dfo,'(2f18.10)') real(en-efermi),                   &
     &                               2.d0*aimag(dostot(nsp))
            else
              write(dfo,'(4f18.10)') real(en-efermi),                   &
     &                          sum(aimag(dostot(1:nsp))),              &
     &                          aimag(dostot(1)),aimag(dostot(nsp))
            end if

            if ( size(dos)<=1 ) return

            do in=1,nbasis
            do ic=1,inFile%sublat(in)%ncomp

              dfo = doscomp_nunit(inFile%io(nu_dos)%nunit,in,ic)
              inquire(unit=dfo,exist=ex)
              if ( .not.ex ) cycle
              if(nsp == 1) then
                 dossc1 = sum(dos(0:lmax,ic,in,1))
                 if ( dossc1==dos(0,ic,in,1) ) then  ! case when only total dos is calculated
                  write(dfo,'(2f16.8)') real(en-efermi),                &
     &                                  2.d0*aimag(dossc1)
                 else
                  if ( inFile%sublat(in)%compon(ic)%zID==0 ) then
                   write(dfo,'(10f16.8)') real(en-efermi),              &
     &                                  2.d0*aimag(dossc1)
                  else
                   write(dfo,'(10f16.8)') real(en-efermi),              &
     &             2.d0*aimag(dossc1), 2.d0*aimag(dos(0:lmax,ic,in,1))
                  end if
                 end if
              else
               dossc1 = sum(dos(0:lmax,ic,in,1))
               dossc  = sum(dos(0:lmax,ic,in,nsp))
               if ( inFile%sublat(in)%compon(ic)%zID==0 ) then
                 write(dfo,'(f16.8)',advance='no') real(en-efermi)
                 write(dfo,'(2f16.8)') aimag(dossc1),aimag(dossc)
               else
                 if ( lmax <=5 ) then
                  write(dfo,'(f16.8)',advance='no') real(en-efermi)
                  write(dfo,'(f16.8)',advance='no') aimag(dossc1)
                  write(dfo,'(6f16.8)',advance='no')                    &
     &             aimag(dos(0:lmax,ic,in,1))
                  write(dfo,'(f16.8)',advance='no') aimag(dossc)
                  write(dfo,'(6f16.8)')                                 &
     &             aimag(dos(0:lmax,ic,in,nsp))
                 else
                  write(dfo,'(12f16.8,(1x,12f16.8))') real(en-efermi)   &
     &             ,aimag(dossc1),aimag(dos(0:lmax,ic,in,1))            &
     &             ,aimag(dossc),aimag(dos(0:lmax,ic,in,nsp))
                 end if
               endif
              endif
            end do; end do

      return
!EOC
      end subroutine writeDOSfiles

!c============================================================================
!BOP
!!IROUTINE: applyDLMsymm
!!INTERFACE:
      subroutine applyDLMsymm( lmax )
!!REVISION HISTORY:
! Initial Version - A.S. - 2013
!EOP
!
!BOC
      implicit none
      integer, intent(in) :: lmax
      integer ic,in,in0,ik,nc

      if ( nspin < 2 ) return

       do in = 1,nbasis
        if( inFile%sublat(in)%isDLM ) then
         nc = inFile%sublat(in)%ncomp/2
         do ic = 1,nc
          ik = nc+ic
          if ( inFile%sublat(in)%compon(ic)%zID==0 ) then   ! ES are enforced to be non-magnetic
           gf_box%dos(0:lmax,ic,in,1) = (gf_box%dos(0:lmax,ic,in,1) +   &
     &                                   gf_box%dos(0:lmax,ik,in,1))/2
           gf_box%dos(0:lmax,ic,in,2) = gf_box%dos(0:lmax,ic,in,1)
           gf_box%dos(0:lmax,ik,in,1:2) = gf_box%dos(0:lmax,ic,in,1:2)
           gf_box%green(:,ic,in,1) = (gf_box%green(:,ic,in,1) +         &
     &                                   gf_box%green(:,ik,in,1))/2
           gf_box%green(:,ic,in,2) = gf_box%green(:,ic,in,1)
           gf_box%green(:,ik,in,1:2) = gf_box%green(:,ic,in,1:2)
           gf_box%dosck(ic,in,1:2) = (gf_box%dosck(ic,in,1) +           &
     &                                   gf_box%dosck(ik,in,1))/2
           gf_box%dosck(ik,in,1:2) = gf_box%dosck(ic,in,1:2)
          else
           gf_box%dos(0:lmax,ik,in,2) = gf_box%dos(0:lmax,ic,in,1)
           gf_box%green(:,ik,in,2) = gf_box%green(:,ic,in,1)
           gf_box%dosck(ik,in,2) = gf_box%dosck(ic,in,1)
           gf_box%dos(0:lmax,ic,in,2) = gf_box%dos(0:lmax,ik,in,1)
           gf_box%green(:,ic,in,2) = gf_box%green(:,ik,in,1)
           gf_box%dosck(ic,in,2) = gf_box%dosck(ik,in,1)
          end if
         enddo
        end if
       enddo



!cSUFF .. Perform DLM symmetrization if necessary
!c No more averaging. The 2nd component is meant to be a flipped 1st.
!       do in = 1,nbasis
!        if( inFile%sublat(in)%isDLM ) then
!         nc = inFile%sublat(in)%ncomp/2
!         gf_box%dos(0:lmax,nc+1:nc+nc,in,2) =                           &
!     &                                      gf_box%dos(0:lmax,1:nc,in,1)
!         gf_box%dos(0:lmax,nc+1:nc+nc,in,1) =                           &
!     &                                      gf_box%dos(0:lmax,1:nc,in,2)
!         gf_box%green(:,nc+1:nc+nc,in,2) = gf_box%green(:,1:nc,in,1)
!         gf_box%green(:,nc+1:nc+nc,in,1) = gf_box%green(:,1:nc,in,2)
!         gf_box%dosck(nc+1:nc+nc,in,2) = gf_box%dosck(1:nc,in,1)
!         gf_box%dosck(nc+1:nc+nc,in,1) = gf_box%dosck(1:nc,in,2)
!        end if
!       enddo

!c .. Enforce a magnetic pattern of flipped spins
!       do in = 1,nbasis
!        if( inFile%sublat(in)%isflipped ) then
!         in0 = inFile%sublat(in)%flippoint
!         if ( in0>0 ) then
!          do ic = 1,inFile%sublat(in)%ncomp
!!           write(6,'(4f15.10)') energysp(1), energysp(2)
!           gf_box%dos(0:lmax,ic,in,2) = gf_box%dos(0:lmax,ic,in0,1)
!           gf_box%dos(0:lmax,ic,in,1) = gf_box%dos(0:lmax,ic,in0,2)
!           gf_box%green(:,ic,in,2) = gf_box%green(:,ic,in0,1)
!           gf_box%green(:,ic,in,1) = gf_box%green(:,ic,in0,2)
!           gf_box%dosck(ic,in,2) = gf_box%dosck(ic,in0,1)
!           gf_box%dosck(ic,in,1) = gf_box%dosck(ic,in0,2)
!          enddo
!         end if
!        end if
!       enddo
!cSUFF
      return
!EOC
      end subroutine applyDLMsymm

!c============================================================================
!BOP
!!IROUTINE: applyiAFMsymm
!!INTERFACE:
      subroutine applyAFMsymm( lmax,nt )
!!REVISION HISTORY:
! Initial Version - A.S. - 2019
!EOP
!
!BOC
      implicit none
      integer, intent(in) :: lmax
      integer, intent(out) :: nt
      integer :: ns,ns1,ncomp

      nt = nbasis
      if ( nspin < 2 .or. nbasis==1 ) return
      nt = 1
      if ( lmax<0 ) then
        if ( all(inFile%sublat(1:nbasis)%flippoint > 0) ) then
          do ns = 1,nbasis
           ns1 = inFile%sublat(ns)%flippoint
           if ( ns1.ne.ns ) then
            if ( inFile%sublat(ns1)%isflipped .neqv.                    &
     &                    inFile%sublat(ns)%isflipped ) then
             nt = nt+1
            end if
           end if
          end do
        end if
      else
!  1st spin component of not-flipped sublat is meant
!  to be 2nd spin component for flipped sublat
       do ns = 1,nbasis
        ns1 = inFile%sublat(ns)%flippoint
        if ( ns1<=0 ) cycle
        if ( ns1 .ne. ns ) then
         if ( inFile%sublat(ns1)%isflipped .neqv.                       &
     &                    inFile%sublat(ns)%isflipped ) then
          ncomp = inFile%sublat(ns1)%ncomp
          gf_box%dos(0:lmax,1:ncomp,ns1,2) =                            &
     &                               gf_box%dos(0:lmax,1:ncomp,ns,1)
          gf_box%dos(0:lmax,1:ncomp,ns,2) =                             &
     &                               gf_box%dos(0:lmax,1:ncomp,ns1,1)
          gf_box%dosck(1:ncomp,ns1,2) = gf_box%dosck(1:ncomp,ns,1)
          gf_box%dosck(1:ncomp,ns,2) = gf_box%dosck(1:ncomp,ns1,1)
          gf_box%green(:,1:ncomp,ns1,2) = gf_box%green(:,1:ncomp,ns,1)
          gf_box%green(:,1:ncomp,ns,2) = gf_box%green(:,1:ncomp,ns1,1)
          nt = nt+1
         end if
        end if
       end do
      end if

      nt = nbasis - nt

      return
!EOC
      end subroutine applyAFMsymm

!c============================================================================
!BOP
!!IROUTINE: totupGF
!!INTERFACE:
      subroutine totupGF( ie, lmax )
!!DESCRIPTION:
! energy ({\tt ie-}), orbital- and spin-summation of various
! site-dependent values related to Green's function
! ( for {\tt mecca\_state\%gf\_out (type GF\_output)})
!
!!REVISION HISTORY:
! Initial Version - A.S. - 2013
!EOP
!
!BOC
      integer, intent(in) :: ie
      integer, intent(in) :: lmax
      integer ic,in,is,is0
      complex(8) :: dostot, sumdos
      complex(8) :: wght
      integer :: il
      wght = dele1(ie)
      if ( nspin==1 ) then
         wght = 2 * wght   ! spin_factor
                           ! integrals in gf_out (except dostspn) are with spin-factor,
                           ! but E-dependent values are not
      end if
      do is=1,nspin
        dostot = 0
        do in=1,nbasis
          do ic=1,inFile%sublat(in)%ncomp
            is0 = is
            sumdos = sum(gf_box%dos(0:lmax,ic,in,is))/nodes_at_thisE
            dostot = dostot + sumdos*                                   &
     &        inFile%sublat(in)%compon(ic)%concentr*inFile%sublat(in)%ns
            gf_out%xvalws(ic,in,is) =  gf_out%xvalws(ic,in,is) +        &
     &                                           aimag(wght*sumdos)
            gf_out%evalsum(ic,in,is) = gf_out%evalsum(ic,in,is) +       &
     &                        aimag(gf_box%energy(is0)*wght*sumdos)
            gf_out%xvalmt(ic,in,is) = gf_out%xvalmt(ic,in,is) +         &
     &            aimag(gf_box%dosck(ic,in,is)*wght)/nodes_at_thisE
            gf_out%greenint(:,ic,in,is) = gf_out%greenint(:,ic,in,is) + &
     &            gf_box%green(:,ic,in,is)*wght/nodes_at_thisE
          enddo
        enddo

        gf_out%dostspn(is,ie) = dostot   ! no spin_factor

!CALAM **********************************************************
        if ( dos_scan ) then
          if( nproc == 1 ) then
              continue
!             call writeDOSfiles( is, egrd(ie), lmax, dostot,            &
!     &                           gf_box%dos(0:lmax,:,:,1:nspin) )
          else
            ! if more than one process we need to buffer DoS
            ! so only the root node writes to file (and in order)
            ! the root node will write at the end of round processing
            do in = 1, nbasis
            do ic = 1, inFile%sublat(in)%ncomp
            do il = 0, lmax
              mpi_dos_buffer(myrank, il,ic,in,is) =                     &
     &          gf_box%dos(il,ic,in,is)
            enddo; enddo; enddo
          endif
        end if
!CALAM **********************************************************

      end do   ! is-cycle

      return
!EOC
      end subroutine totupGF
!
      subroutine get_recdlm( edu, irecdlm )
      use mecca_constants, only : eresw,eimsw
      implicit none
      complex(8), intent(in) :: edu   ! energy in D.U.
      integer, intent(out)   :: irecdlm
      real(8) :: p_imsw = sqrt(eimsw)
      real(8) :: ewld_re
!      ewld_re = min(eresw,minval(enlim(1,:)))

      if ( dreal(edu) >= zero ) then
       if ( dimag(edu) < eimsw ) then
        irecdlm = 0  ! Ewald  (r-space + k-space summation)
       else
        irecdlm = 1  ! Hankel (only r-space summation)
       end if
      else
       if ( dimag(sqrt(edu))**2 < eimsw ) then
        irecdlm = 0  ! Ewald  (r-space + k-space summation)
       else
        irecdlm = 1  ! Hankel (only r-space summation)
       end if
      end if

      return
      end subroutine get_recdlm
!
      end module zplane
