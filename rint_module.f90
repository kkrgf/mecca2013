module rint_module
  use universal_const, only: dp=>DBL_R
  implicit none

  public :: rint
  private:: rint_double,rint_double_complex
  interface rint
     module procedure rint_double
     module procedure rint_double_complex
  end interface rint


  public :: rint_get_weights
  private:: rint_get_weights_double
  interface rint_get_weights
     module procedure rint_get_weights_double
  end interface rint_get_weights

  integer, parameter:: wp = dp


  real (wp), parameter,private  :: c(105) = (/ &
       &        1._wp,                                                         & ! a
       &        2._wp,        1._wp,                                           & ! b
       &       23._wp,       28._wp,        9._wp,                             & ! c
       &       25._wp,       20._wp,       31._wp,        8._wp,               & ! d
       &     1413._wp,     1586._wp,     1104._wp,     1902._wp,      475._wp, & ! e
       &     1456._wp,     1333._wp,     1746._wp,      944._wp,     1982._wp, & ! f
       &      459._wp,                                                         & ! f
       &   119585._wp,   130936._wp,    89437._wp,   177984._wp,    54851._wp, & ! g
       &   176648._wp,    36799._wp,                                           & ! f
       &   122175._wp,   111080._wp,   156451._wp,    46912._wp,   220509._wp, & ! h
       &    29336._wp,   185153._wp,    35584._wp,                             & ! h
       &  7200319._wp,  7783754._wp,  5095890._wp, 12489922._wp, -1020160._wp, & ! i
       & 16263486._wp,   261166._wp, 11532470._wp,  2082753._wp,               & ! i
       &  7305728._wp,  6767167._wp,  9516362._wp,  1053138._wp, 18554050._wp, & ! j
       & -7084288._wp, 20306238._wp, -1471442._wp, 11965622._wp,  2034625._wp, & ! j
       &   952327935._wp,  1021256716._wp,   636547389._wp,  1942518504._wp,   & ! k
       & -1065220914._wp,  3897945600._wp, -2145575886._wp,  3373884696._wp,   & ! k
       &  -454944189._wp,  1637546484._wp,   262747265._wp,                    & ! k
       &   963053825._wp,   896771060._wp,  1299041091._wp,  -196805736._wp,   & ! l
       &  3609224754._wp, -3398609664._wp,  6231334350._wp, -3812282136._wp,   & ! l
       &  4207237821._wp,  -732728564._wp,  1693103359._wp,   257696640._wp,   & ! l
       &   5206230892907._wp,   5551687979302._wp,   3283609164916._wp,        & ! m
       &  12465244770050._wp, -13155015007785._wp,  39022895874876._wp,        & ! m
       & -41078125154304._wp,  53315213499588._wp, -32865015189975._wp,        & ! m
       &  28323664941310._wp,  -5605325192308._wp,   9535909891802._wp,        & ! m
       &   1382741929621._wp,                                                  & ! m
       &   5252701747968._wp,   4920175305323._wp,   7268021504806._wp,        & ! n
       &  -3009613761932._wp,  28198302087170._wp, -41474518178601._wp,        & ! n
       &  76782233435964._wp, -78837462715392._wp,  81634716670404._wp,        & ! n
       & -48598072507095._wp,  34616887868158._wp,  -7321658717812._wp,        & ! n
       &   9821965479386._wp,   1360737653653._wp                              & ! n
       & /)
  real (wp), parameter,private  :: d(14) = (/ &
       &   2._wp,2._wp,24._wp,24._wp,1440._wp,1440._wp,120960._wp,             & ! a
       &  120960._wp,7257600._wp,7257600._wp,958003200._wp,958003200._wp,      & ! b
       &  5230697472000._wp,5230697472000._wp                                  & ! c
       & /)


contains

  function rint_double (f,na,nb,nq,h) result(rint)
    implicit none
    !     ------------------------------------------------------------------
    !     this program calculates the integral of the function f
    !     from point na to point nb using a nq points quadrature.
    !     nq is any integer between 1 and 14.
    !     h is the grid size.
    !                                     written by c. c. j. roothaan
    !     ------------------------------------------------------------------
    !     method:
    !
    !       integral(0,n*h) dx f(x) =
    !           (h/2){ c(0)*f[0]+c(1)*f[1*h]+...c(nq-1)*f[(nq-1)*h]
    !                       + 2* Sum(i=nq,n-nq) f[i*h]
    !                  c(nq-1)*f[(n-(nq-1))*h]+... c(0)*f[n*h] }
    !
    !     the coefficients c(j) are obtained by requiring exactness for
    !     monomials f(x)=1,x,...x^(nq-1)
    !
    !     the coefficients c(j) for different orders nq are packed
    !     into a linear array
    !     ------------------------------------------------------------------
    real (dp)             :: rint
    integer,   intent(in) :: na
    integer,   intent(in) :: nb
    integer,   intent(in) :: nq
    real (dp), intent(in) :: h
    real (dp), intent(in) :: f(nb)

    real (dp) :: a
    integer   :: i,j,l,m,n

    if((nb-na+1).lt.2*nq) then
       write(*,'(a)') "ERROR in rint: the requested order is too high of the number of data points."
       stop
    endif

    a = 0.0_dp
    l = na
    m = nb
    i = nq*(nq+1)/2
    do j = 1,nq
       a = a + c(i)*( f(l) + f(m) )
       l = l + 1
       m = m - 1
       i = i - 1
    enddo ! j
    a = a/d(nq)
    do n = l,m
       a = a + f(n)
    enddo ! n
    rint = a*h

    return
  end function rint_double





  function rint_double_complex (f,na,nb,nq,h) result(rint)
    implicit none
    !     ------------------------------------------------------------------
    !     this program calculates the integral of the function f
    !     from point na to point nb using a nq points quadrature.
    !     nq is any integer between 1 and 14.
    !     h is the grid size.
    !                                     written by c. c. j. roothaan
    !     ------------------------------------------------------------------
    !     method:
    !
    !       integral(0,n*h) dx f(x) =
    !           (h/2){ c(0)*f[0]+c(1)*f[1*h]+...c(nq-1)*f[(nq-1)*h]
    !                       + 2* Sum(i=nq,n-nq) f[i*h]
    !                  c(nq-1)*f[(n-(nq-1))*h]+... c(0)*f[n*h] }
    !
    !     the coefficients c(j) are obtained by requiring exactness for
    !     monomials f(x)=1,x,...x^(nq-1)
    !
    !     the coefficients c(j) for different orders nq are packed
    !     into a linear array
    !     ------------------------------------------------------------------
    complex (dp)             :: rint
    integer,      intent(in) :: na
    integer,      intent(in) :: nb
    integer,      intent(in) :: nq
    real (dp),    intent(in) :: h
    complex (dp), intent(in) :: f(nb)

    complex (dp) :: a
    integer      :: i,j,l,m,n

    if((nb-na+1).lt.2*nq) then
       write(*,'(a)') "ERROR in rint: the requested order is too high of the number of data points."
       stop
    endif

    a = cmplx(0.0_dp,0.0_dp,dp)
    l = na
    m = nb
    i = nq*(nq+1)/2
    do j = 1,nq
       a = a + c(i)*( f(l) + f(m) )
       l = l + 1
       m = m - 1
       i = i - 1
    enddo ! j
    a = a/d(nq)
    do n = l,m
       a = a + f(n)
    enddo ! n
    rint = a*h

    return
  end function rint_double_complex




  subroutine rint_get_weights_double(np,nq,h,weights)
    implicit none
    integer,  intent(in)  :: np          !> number of points
    integer,  intent(in)  :: nq          !> requested order
    real(dp), intent(in)  :: h           !> stepsize (difference between x-points)
    real(dp), intent(out) :: weights(np) !> weigts for integration

    integer :: l,m,i,j

    if((np).lt.2*nq) then
       write(*,'(a)') "ERROR in rint_get_weigts: the requested order is too high of the number of data points."
       stop
    endif

    weights(1:np) = 1.0_dp

    l = 1
    m = np
    i = nq*(nq+1)/2

    do j = 1,nq
       weights(l)=c(i)/d(nq)
       weights(m)=weights(l)
       l = l + 1
       m = m - 1
       i = i - 1
    enddo ! j
    weights(1:np)=weights(1:np)*h

  end subroutine rint_get_weights_double



end module rint_module
