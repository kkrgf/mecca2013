!BOP
!!ROUTINE: ktollp1
!!INTERFACE:
      subroutine ktollp1(xk,xllp,ktop,kkrsz,ndimkkr,istop)
!!DESCRIPTION:
! takes a vector that depends on k (symm.type) and
! expands it into a matrix in L,L`(for fcc-,bcc-symmetry)
!

!!DO_NOT_PRINT
      implicit real*8 (a-h,o-z)
!!DO_NOT_PRINT

!!ARGUMENTS:
      complex*16  xk(*)
      integer ktop,kkrsz,ndimkkr
      complex*16  xllp(ndimkkr,kkrsz)
      character*10 istop
!EOP
!
!BOC
      character*10 sname
      parameter (sname='ktollp1')
!c
!c      sym-index
!c     -------------------------------------------------------------
!c          k                l         (  kdeg  )
!c          1                0         (    1   )
!c          2                1         (    3   )
!c          3                2         (    3   )
!c          4                2         (    2   )
!c          5                3         (    3   )
!c          6                3         (    3   )
!c          7                3         (    1   )
!c          8               1-3        (    3   )
!c     -------------------------------------------------------------
!c
      if(kkrsz.gt.ndimkkr) then
        write(6,'(/a10,'': trouble with kkrsz :='',i2)') sname,kkrsz
        call fstop(sname)
      endif
!c     -------------------------------------------------------------
      call zerooutC(xllp,ndimkkr*kkrsz)
!c     -------------------------------------------------------------
      lmax1 = nint(sqrt(real(kkrsz)))
      if(lmax1.ge.1) then                             ! L=0
       xllp(1,1) = xk(1)
       if(lmax1.ge.2) then                            ! L=1
        xllp(2,2) = xk(2)
        xllp(3,3) = xk(2)
        xllp(4,4) = xllp(2,2)
        if(lmax1.ge.3) then                           ! L=2
         xllp(5,5) = (xk(3)+xk(4))*0.5d0
         xllp(6,6) = xk(3)
         xllp(7,7) = xk(4)
         xllp(8,8) = xllp(6,6)
         xllp(9,9) = xllp(5,5)
         if(lmax1.ge.4) then                          ! L=3
          xllp(10,10) = (3.d0*xk(5)+5*xk(6))/8.d0
          xllp(11,11) = (xk(5)+xk(7))*0.5d0
          xllp(12,12) = (5.d0*xk(5)+3*xk(6))/8.d0
          xllp(13,13) = xk(6)
          xllp(14,14) = xllp(12,12)
          xllp(15,15) = xllp(11,11)
          xllp(16,16) = xllp(10,10)
          if(ktop.eq.8) then                          ! L=3, non-diagonal

           xllp(2,12)  = (xk(8)/sqrt(6.d0))  *3.d0
           xllp(3,13)  =-(xk(8)*(2.d0/3.d0)) *3.d0
           xllp(4,14)  = (xk(8)/sqrt(6.d0))  *3.d0

           xllp(12,2)  = xllp(2,12)
           xllp(13,3)  = xllp(3,13)
           xllp(14,4)  = xllp(4,14)
          end if
         else                                         ! L>3
          write(6,*) sname,'  LMAX=',lmax1-1
          call fstop(sname//': WARNING :: LMAX > 3')
         end if
        end if
       end if
      end if
!c
!c     =============================================================
      if (istop.eq.sname) then
         call fstop(sname)
      else
         return
      endif
!EOC
      end subroutine ktollp1
