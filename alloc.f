!BOP
!!ROUTINE: allocSphAtomDeps
!!INTERFACE:
      subroutine allocSphAtomDeps(v_rho_box,ndrpts,ndspin)
!!DESCRIPTION:
! allocates array components of {\tt v\_rho\_box}, 
! type {\em SphAtomDeps}
!
!!USES:
      use mecca_types, only : SphAtomDeps
!
!!REVISION HISTORY:
! Initial Version - A.S. - 2013
!EOP
!
!BOC
      implicit none
      type(SphAtomDeps) :: v_rho_box
      integer, intent(in) :: ndrpts,ndspin

      if ( ndspin<=0 .or. ndspin>size(v_rho_box%v0(:)) ) then
       write(*,*) ' allocSphAtomDeps: ndspin=',ndspin
       stop   ' ERROR: input ndspin-value is out of range'
      end if
      if ( ndrpts<=0 ) then
       write(*,*) ' allocSphAtomDeps: ndrpts=',ndrpts
       stop ' ERROR: non-positive input ndrpts'
      end if
      call deallocSphAtomDeps(v_rho_box)
      allocate( v_rho_box%vr(ndrpts,ndspin) )
      v_rho_box%ndrpts = ndrpts
      allocate( v_rho_box%chgtot(ndrpts,ndspin) )
      v_rho_box%ndchg = ndrpts
      allocate( v_rho_box%chgcor(ndrpts,ndspin) )
      v_rho_box%ndcor = ndrpts
      return
!EOC
      end subroutine allocSphAtomDeps
!      
!BOP
!!ROUTINE: deallocSphAtomDeps
!!INTERFACE:
      subroutine deallocSphAtomDeps(v_rho_box)
!!DESCRIPTION:
! deallocates array components of {\tt v\_rho\_box}, 
! type {\em SphAtomDeps}
!
!!USES:
      use mecca_types, only : SphAtomDeps
!
!!REVISION HISTORY:
! Initial Version - A.S. - 2013
!EOP
!
!BOC
      implicit none
      type(SphAtomDeps) :: v_rho_box
      if ( allocated(v_rho_box%vr) ) then
       deallocate( v_rho_box%vr )
      end if
      if ( allocated(v_rho_box%chgtot) ) then
       deallocate( v_rho_box%chgtot )
      end if
      if ( allocated(v_rho_box%chgcor) ) then
       deallocate( v_rho_box%chgcor )
      end if
      v_rho_box%ndrpts = 0
      v_rho_box%ndchg  = 0
      v_rho_box%ndcor  = 0
      v_rho_box%xst  = 0
      v_rho_box%xmt  = 0
      v_rho_box%jmt  = 0
      return
!EOC
      end subroutine deallocSphAtomDeps
!
!BOP
!!ROUTINE: deallocSphDeps
!!INTERFACE:
      subroutine deallocSphDeps(intfile)
!!DESCRIPTION:
! deallocates array components of all {\tt v\_rho\_box} components 
! of {\tt intfile}, type {\em IniFile} 
!
!!USES:
      use mecca_types, only : IniFile
!
!!REVISION HISTORY:
! Initial Version - A.S. - 2013
!EOP
!
!BOC
      implicit none
      type(IniFile) :: intfile
      integer :: nsub,ic
      if ( intfile%ntask==0 ) then
       do nsub=1,intfile%nsubl
        do ic=1,intfile%sublat(nsub)%ncomp
         call deallocSphAtomDeps(intfile%sublat(nsub)%compon(ic)%       &
     &                          v_rho_box)
        end do
       end do
      end if
      return
!EOC
      end subroutine deallocSphDeps
!
!BOP
!!ROUTINE: allocGFoutput
!!INTERFACE:
      subroutine allocGFoutput(gf_out,ndrpts,ndcomp,ndsubl,ndspin)
!!DESCRIPTION:
! allocates array components of {\tt gf\_out},
! type {\em GF\_output} 
!
!!USES:
      use mecca_types
!
!!REVISION HISTORY:
! Initial Version - A.S. - 2013
!EOP
!
!BOC
      implicit none
      type(GF_output) :: gf_out 
      integer, intent(in) :: ndrpts,ndcomp,ndsubl,ndspin   !,nume
!      call deallocGFoutput(gf_out,.false.)
!      if ( gf_out%nume > 0 ) then
!       if ( .not. allocated(gf_out%egrd) )                              &
!     &                       allocate(gf_out%egrd(1:gf_out%nume))
!       if ( .not. allocated(gf_out%wgrd) )                              &
!     &                       allocate(gf_out%wgrd(1:gf_out%nume))
!       if ( .not. allocated(gf_out%dostspn) )                           &
!     &                       allocate(gf_out%dostspn(1:2,1:gf_out%nume))
!      else
!       gf_out%nume = nume
!       allocate(gf_out%egrd(1:gf_out%nume))
!       allocate(gf_out%wgrd(1:gf_out%nume))
!       allocate(gf_out%dostspn(1:2,1:gf_out%nume))
!      end if
      if ( .not. allocated(gf_out%greenint) ) then
       allocate(gf_out%greenint(1:ndrpts,1:ndcomp,1:ndsubl,1:ndspin))
      end if
      if ( .not. allocated(gf_out%greenlast) ) then
       allocate(gf_out%greenlast(1:ndrpts,1:ndcomp,1:ndsubl,1:ndspin))
      end if
      if ( .not. allocated(gf_out%doslast) ) then
       allocate(gf_out%doslast(1:ndcomp,1:ndsubl,1:ndspin))
      end if
      if ( .not. allocated(gf_out%doscklast) ) then
       allocate(gf_out%doscklast(1:ndcomp,1:ndsubl,1:ndspin))
      end if
!DELETE      if ( .not. allocated(gf_out%Pterm) ) then
!DELETE       allocate(gf_out%Pterm(1:ndcomp,1:ndsubl,1:ndspin))
!DELETE      end if
!DEBUG      if ( .not. allocated(gf_out%dosint) ) then
!DEBUG       allocate(gf_out%dosint(1:ndcomp,1:ndsubl,1:ndspin))
!DEBUG       gf_out%dosint = 0
!DEBUG      end if
      if ( .not. allocated(gf_out%xvalmt) ) then
       allocate(gf_out%xvalmt(1:ndcomp,1:ndsubl,1:ndspin))
      end if
      if ( .not. allocated(gf_out%evalsum) ) then
       allocate(gf_out%evalsum(1:ndcomp,1:ndsubl,1:ndspin))
      end if
      if ( .not. allocated(gf_out%xvalws) ) then
       allocate(gf_out%xvalws(1:ndcomp,1:ndsubl,1:ndspin))
      end if
      gf_out%greenint = 0
      gf_out%greenlast = 0
      gf_out%doslast = 0
      gf_out%doscklast = 0
      gf_out%xvalmt = 0
      gf_out%evalsum = 0
      gf_out%xvalws = 0
      return
!EOC
      end subroutine allocGFoutput
!
!BOP
!!ROUTINE: deallocGFoutput
!!INTERFACE:
      subroutine deallocGFoutput(gf_out,all)
!!DESCRIPTION:
! deallocates array components of {\tt gf\_out},
! type {\em GF\_output} 
!
!!USES:
      use mecca_types
!
!!REVISION HISTORY:
! Initial Version - A.S. - 2013
!EOP
!
!BOC
      implicit none
      type(GF_output) :: gf_out
      logical, intent(in) :: all
      if ( all ) then
        if (allocated(gf_out%dostspn)) deallocate(gf_out%dostspn)
        if (allocated(gf_out%wgrd))   deallocate(gf_out%wgrd)
        if (allocated(gf_out%egrd))   deallocate(gf_out%egrd)
        gf_out%nume = 0
        gf_out%efermi = 0
      end if
      if (allocated(gf_out%xvalws))    then
        deallocate(gf_out%xvalws)
      end if
      if (allocated(gf_out%evalsum))   then
        deallocate(gf_out%evalsum)
      end if
      if (allocated(gf_out%xvalmt))  then
        deallocate(gf_out%xvalmt)
      end if
!DEBUG      if (allocated(gf_out%dosint))    then
!DEBUG        deallocate(gf_out%dosint)
!DEBUG      end if
!DELETE      if (allocated(gf_out%Pterm)) then
!DELETE        deallocate(gf_out%Pterm)
!DELETE      end if
      if (allocated(gf_out%doscklast)) then
        deallocate(gf_out%doscklast)
      end if
      if (allocated(gf_out%doslast))   then
          deallocate(gf_out%doslast)
      end if
      if (allocated(gf_out%greenint))  then
        deallocate(gf_out%greenint)
      end if
      if (allocated(gf_out%greenlast)) then
        deallocate(gf_out%greenlast)
      end if
      return
!EOC
      end subroutine deallocGFoutput
!      
!BOP
!!ROUTINE: allocGFdata
!!INTERFACE:
      subroutine allocGFdata(gf_box,lmax,ndrpts,ndcomp,ndsubl,ndspin)
!!DESCRIPTION:
! allocates array components of {\tt gf\_box},
! type {\em GF\_data} 
!
!!USES:
      use mecca_types
!
!!REVISION HISTORY:
! Initial Version - A.S. - 2013
!EOP
!
!BOC
      implicit none
      type(GF_data) :: gf_box 
      integer, intent(in) :: lmax,ndrpts,ndcomp,ndsubl,ndspin
      call deallocGFdata(gf_box)
      allocate(gf_box%green(1:ndrpts,1:ndcomp,1:ndsubl,1:ndspin))
      gf_box%green = 0
      allocate(gf_box%dos(0:lmax,1:ndcomp,1:ndsubl,1:ndspin))
      gf_box%dos = 0
      allocate(gf_box%dosck(1:ndcomp,1:ndsubl,1:ndspin))
      gf_box%dosck = 0
!DELETE      allocate(gf_box%pterm(1:ndcomp,1:ndsubl,1:ndspin))
!DELETE      gf_box%pterm = 0
      return
!EOC
      end subroutine allocGFdata
!
!BOP
!!ROUTINE: deallocGFdata
!!INTERFACE:
      subroutine deallocGFdata(gf_box)
!!DESCRIPTION:
! deallocates array components of {\tt gf\_box},
! type {\em GF\_data} 
!
!!USES:
      use mecca_types
!
!!REVISION HISTORY:
! Initial Version - A.S. - 2013
!EOP
!
!BOC
      implicit none
      type(GF_data) :: gf_box
!DELETE      if (allocated(gf_box%pterm)) deallocate(gf_box%pterm)
      if (allocated(gf_box%dosck)) deallocate(gf_box%dosck)
      if (allocated(gf_box%dos))   deallocate(gf_box%dos)
      if (allocated(gf_box%green)) deallocate(gf_box%green)
      gf_box%energy = 0
      return
!EOC
      end subroutine deallocGFdata
!
!BOP
!!ROUTINE: allocWorkdata
!!INTERFACE:
      subroutine allocWorkdata(work_box,ndrpts,ndcomp,ndsubl,ndspin)
!!DESCRIPTION:
! allocates array components of {\tt work\_box},
! type {\em Work\_data} 
!
! {\bv   
! if ndrpts>0, both vr and vrms/qrms are allocated;     
!         ==0, only vrms.qrms are allocated
!          <0, only vr is allocated
! \ev}
!!USES:
      use mecca_types
!
!!REVISION HISTORY:
! Initial Version - A.S. - 2013
!EOP
!
!BOC
      implicit none
      type(Work_data) :: work_box
      integer, intent(in) :: ndrpts,ndcomp,ndsubl,ndspin
      if ( ndcomp>0 .and. ndsubl>0 .and. ndspin>0 ) then 
        if ( ndrpts.ne.0 ) then
          if ( allocated(work_box%vr) ) deallocate(work_box%vr)
          allocate(work_box%vr(abs(ndrpts),ndcomp,ndsubl,ndspin))
          if (allocated(work_box%chgsemi)) deallocate(work_box%chgsemi)
          allocate(work_box%chgsemi(abs(ndrpts),ndcomp,ndsubl,ndspin))
          work_box%chgsemi = 0
          if (allocated(work_box%esemicorr)) then                       
                                      deallocate(work_box%esemicorr)
          end if
          allocate(work_box%esemicorr(ndcomp,ndsubl,ndspin))
          work_box%esemicorr = 0
          if (allocated(work_box%zsemi)) then                       
                                      deallocate(work_box%zsemi)
          end if
          allocate(work_box%zsemi(ndcomp,ndsubl,ndspin))
          work_box%zsemi = 0
        end if
        if ( ndrpts>=0 ) then
          if ( allocated(work_box%vrms) ) deallocate(work_box%vrms)
          allocate(work_box%vrms(ndcomp,ndsubl,ndspin))    
          work_box%vrms = 1
          if ( allocated(work_box%qrms) ) deallocate(work_box%qrms)
          allocate(work_box%qrms(ndcomp,ndsubl,ndspin))
          work_box%qrms = 1
          if ( allocated(work_box%dq_err) ) deallocate(work_box%dq_err)
          allocate(work_box%dq_err(ndcomp,ndsubl,ndspin))
          work_box%dq_err = 1
        end if
      end if
      return
!EOC
      end subroutine allocWorkdata
!      
!BOP
!!ROUTINE: deallocWorkdata
!!INTERFACE:
      subroutine deallocWorkdata(work_box)
!!DESCRIPTION:
! deallocates array components of {\tt work\_box},
! type {\em Work\_data} 
!
!!USES:
      use mecca_types
!
!!REVISION HISTORY:
! Initial Version - A.S. - 2013
!EOP
!
!BOC
      implicit none
      type(Work_data) :: work_box
      if (allocated(work_box%vr)) deallocate(work_box%vr)
      if (allocated(work_box%chgsemi)) deallocate(work_box%chgsemi)
      if (allocated(work_box%esemicorr)) deallocate(work_box%esemicorr)
      if (allocated(work_box%zsemi)) deallocate(work_box%zsemi)
      if (allocated(work_box%vrms)) deallocate(work_box%vrms)
      if (allocated(work_box%qrms)) deallocate(work_box%qrms)
      if (allocated(work_box%dq_err)) deallocate(work_box%dq_err)
      work_box%vmtz  = 0
      work_box%dV0   = 0
      work_box%etot  = 0
      work_box%press = 0
      return
!EOC
      end subroutine deallocWorkdata
!      
!BOP
!!ROUTINE: deallocRSstr
!!INTERFACE:
      subroutine deallocRSstr(work_box)
!!DESCRIPTION:
! deallocates array components of {\tt work\_box},
! type {\em RS\_str} 
!
!!USES:
      use mecca_types
!
!!REVISION HISTORY:
! Initial Version - A.S. - 2013
!EOP
!
!BOC
      implicit none
      type(RS_Str) :: work_box
      if (allocated(work_box%xyzr))    deallocate(work_box%xyzr)
      if (allocated(work_box%itype))   deallocate(work_box%itype)
      if (allocated(work_box%numbsubl)) deallocate(work_box%numbsubl)
      if (allocated(work_box%rmt))     deallocate(work_box%rmt)
      if (allocated(work_box%rws))     deallocate(work_box%rws)
      if (allocated(work_box%rcs))     deallocate(work_box%rcs)
      if (allocated(work_box%Rnn))     deallocate(work_box%Rnn)
      if (allocated(work_box%if0))     deallocate(work_box%if0)
      if (allocated(work_box%mapstr))  deallocate(work_box%mapstr)
      if (allocated(work_box%mappnt))  deallocate(work_box%mappnt)
      if (allocated(work_box%mapij))   deallocate(work_box%mapij)
      if (allocated(work_box%mapsprs)) deallocate(work_box%mapsprs)
      if (allocated(work_box%aij))     deallocate(work_box%aij)
      work_box%alat  = 0
      work_box%natom   = 0
      work_box%nsubl  = 0
      work_box%ncount  = 0
      return
!EOC
      end subroutine deallocRSstr
!      
!BOP
!!ROUTINE: deallocKSstr
!!INTERFACE:
      subroutine deallocKSstr(work_box)
!!DESCRIPTION:
! deallocates array components of {\tt work\_box},
! type {\em KS\_str} 
!
!!USES:
      use mecca_types
!
!!REVISION HISTORY:
! Initial Version - A.S. - 2013
!EOP
!
!BOC
      implicit none
      type(KS_Str) :: work_box
      if (allocated(work_box%dop))     deallocate(work_box%dop)
      if (allocated(work_box%nqpt))    deallocate(work_box%nqpt)
      if (allocated(work_box%qmesh))   deallocate(work_box%qmesh)
      if (allocated(work_box%wghtq))   deallocate(work_box%wghtq)
      if (allocated(work_box%twght))   deallocate(work_box%twght)
      if (allocated(work_box%lrot))    deallocate(work_box%lrot)
      if (allocated(work_box%ngrp))    deallocate(work_box%ngrp)
      if (allocated(work_box%kptgrp))  deallocate(work_box%kptgrp)
      if (allocated(work_box%kptset))  deallocate(work_box%kptset)
      if (allocated(work_box%kptindx)) deallocate(work_box%kptindx)
      work_box%nrot  = 0
      work_box%lmax  = -1
      work_box%ndimq  = 0
      return
!EOC
      end subroutine deallocKSstr
!      
!BOP
!!ROUTINE: deallocEWstr
!!INTERFACE:
      subroutine deallocEWstr(work_box)
!!DESCRIPTION:
! deallocates array components of {\tt work\_box},
! type {\em EW\_str} 
!
!!USES:
      use mecca_types
!
!!REVISION HISTORY:
! Initial Version - A.S. - 2013
!EOP
!
!BOC
      implicit none
      type(EW_Str) :: work_box
      if (allocated(work_box%nksn))     deallocate(work_box%nksn)
      if (allocated(work_box%xknlat))   deallocate(work_box%xknlat)
!      if (allocated(work_box%xrnlat))   deallocate(work_box%xrnlat)
      if (allocated(work_box%xrslatij)) deallocate(work_box%xrslatij)
      if (allocated(work_box%np2r))     deallocate(work_box%np2r)
      if (allocated(work_box%numbrs))   deallocate(work_box%numbrs)
      work_box%ndimk  = 0
      work_box%ndimr  = 0
      work_box%ndimrij = 0
      work_box%nrsn  =  0
      work_box%nrsij  = 0
      return
!EOC
      end subroutine deallocEWstr
!      
!BOP
!!ROUTINE: deallocJacbnVP
!!INTERFACE:
      subroutine deallocJacbnVP(work_box)
!!DESCRIPTION:
! deallocates array components of {\tt work\_box},
! type {\em Jacbn\_VP} 
!
!!USES:
      use mecca_types
!
!!REVISION HISTORY:
! Initial Version - A.S. - 2013
!EOP
!
!BOC
      implicit none
      type(VP_data) :: work_box
      if (allocated(work_box%fcount)) deallocate(work_box%fcount)
      if (allocated(work_box%weight)) deallocate(work_box%weight)
      if (allocated(work_box%rmag))   deallocate(work_box%rmag)
      if (allocated(work_box%vj))     deallocate(work_box%vj)
      if (allocated(work_box%rmt))    deallocate(work_box%rmt)
      if (allocated(work_box%alphpot))deallocate(work_box%alphpot)
      if (allocated(work_box%alphen)) deallocate(work_box%alphen)
      if (allocated(work_box%ylmPls)) deallocate(work_box%ylmPls)
      if (allocated(work_box%ylmMns)) deallocate(work_box%ylmMns)
      work_box%nF  = 0
      work_box%ngauss  = 0
      return
!EOC
      end subroutine deallocJacbnVP
!      
!BOP
!!ROUTINE: deallocDosBsf
!!INTERFACE:
      subroutine deallocDosBsf(work_box)
!!DESCRIPTION:
! deallocates array components of {\tt work\_box},
! type {\em DosBsf\_Data} 
!
!!USES:
      use mecca_types
!
!!REVISION HISTORY:
! Initial Version - A.S. - 2013
!EOP
!
!BOC
      implicit none
      type(DosBsf_Data) :: work_box
      if (allocated(work_box%bsf_kwaypts))                              &
     &                                  deallocate(work_box%bsf_kwaypts)
      if (allocated(work_box%bsf_kptlabel))                             &
     &                                 deallocate(work_box%bsf_kptlabel)
      work_box%idosplot  = 0
      work_box%ibsfplot  = 0
      work_box%dosbsf_kintsch = 1
      work_box%dosbsf_nepts  = 0
      work_box%bsf_ksamplerate  = 0
      work_box%bsf_nkwaypts  = 0
      return
!EOC
      end subroutine deallocDosBsf
!      
!BOP
!!ROUTINE: deallocFSdata
!!INTERFACE:
      subroutine deallocFSdata(work_box)
!!DESCRIPTION:
! deallocates array components of {\tt work\_box},
! type {\em FS\_Data} 
!
!!USES:
      use mecca_types
!
!!REVISION HISTORY:
! Initial Version - A.S. - 2013
!EOP
!
!BOC
      implicit none
      type(FS_Data) :: work_box
      if (allocated(work_box%fermi_kpts))                               &
     &                                  deallocate(work_box%fermi_kpts)
      if (allocated(work_box%fermi_kptlabel))                           &
     &                              deallocate(work_box%fermi_kptlabel)
      work_box%ifermiplot  = 0
      work_box%fermi_raysampling  = 0
      work_box%fermi_gridsampling = 0
      work_box%fermi_nkpts  = 0
      return
!EOC
      end subroutine deallocFSdata
