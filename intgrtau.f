!BOP
!!ROUTINE: intgrtau
!!INTERFACE:
      subroutine intgrtau(                                              &
     &                 lmax,kkrsz,nbasis,numbsub,atcon,mxcomp,komp,     &
     &                 rytodu,aij,itype,natom,iorig,itype1,             &
     &                 if0,mapstr,mappnt,ndimbas,mapij,                 &
     &                 powe,                                            &
     &                 edu,pdu,                                         &
     &                 irecdlm,                                         &
     &                 rsn,ndimrs,                                      &
     &                 np2r,ndimnp,numbrs,naij,                         &
     &                 dqint,ndimdqr,                                   &
     &                 hplnm,ndimr,ndimlm,                              &
     &                 tcpa,                                            &
     &                 tau00,                                           &
     &                 dop,nop,d00,eoeta,                               &
     &                 eta,                                             &
     &                 xknlat,ndimks,R2ksp,                             &
     &                 conr,nkns,                                       &
     &                 qmesh,nqpt,lwght,lrot,                           &
     &                 ngrp,kptgrp,kptset,kptindx,                      &
     &                 rslatt,Rnncut,Rsmall,mapsprs,                    &
     &                 r_ref,                                           &
     &                 iprint,istop)
!!DESCRIPTION:
! computes KKR tcpa and tau00 matrices
!
!!USES:
      use mecca_constants
      use mecca_interface, only : RStau00
      use mecca_run
      use mtrx_interface
      use freespaceG, only : gf0_rs
      use mpi
      use invparams, only : invpar_prlim,invpar_prSLU,invpar_prPRC
      use screening, only : calcTref,clstrRegEst,clstrsize
      use screening, only : g_scr_potpar,g_calcref
      use sparse, only : tauScr_ks

!!DO_NOT_PRINT
      implicit real*8 (a-h,o-z)
!!DO_NOT_PRINT

!!ARGUMENTS:
      integer, intent(in) :: lmax,kkrsz,nbasis
      integer, intent(in) :: numbsub(nbasis),komp(nbasis),mxcomp
      real(8), intent(in) :: atcon(mxcomp,nbasis),rytodu
      integer, intent(in) :: naij
      real(8), intent(in) :: aij(3,naij)
      integer, intent(in) :: natom,itype(natom)
      integer, intent(in) :: iorig(nbasis),itype1(natom)
      integer, intent(in) :: if0(48,natom)
      integer, intent(in) :: ndimbas
      integer, intent(in) :: mapstr(ndimbas,natom),mappnt(ndimbas,natom)
      integer, intent(in) :: mapij(2,*)
      complex(8), intent(in) :: powe(*)
      complex(8), intent(in) :: edu      ! energy in DU
      complex(8), intent(in) :: pdu
      integer, intent(in) :: irecdlm,ndimrs
      real(8), intent(in) :: rsn(ndimrs,4)
      integer, intent(in) :: ndimnp
      integer, intent(in) :: np2r(ndimnp,*),numbrs(*)
      integer, intent(in) :: ndimdqr
      complex(8), intent(in) :: dqint(ndimdqr,*)
      integer, intent(in) :: ndimr,ndimlm
      complex(8), intent(in) :: hplnm(ndimr,ndimlm,*)
      complex(8) :: tcpa(:,:,:)
      complex(8) :: tau00(:,:,:)
      integer, intent(in) :: nop
      complex(8), intent(in) :: dop(kkrsz,kkrsz,nop)  ! rotation matrices for Ylm
      complex(8), intent(in) :: d00,eoeta
      real(8), intent(in) :: eta
      integer, intent(in) :: ndimks,nkns
      real(8), intent(in) :: xknlat(ndimks,3),R2ksp
      complex(8), intent(in) :: conr(*)
      integer, intent(in) :: nqpt
      real(8), intent(in) :: qmesh(3,nqpt)
      integer, intent(in) :: lwght(nqpt),lrot(*)
      integer, intent(in) :: ngrp,kptgrp(ngrp+1)
      integer, intent(in) :: kptset(ngrp),kptindx(nqpt)
      real(8), intent(in) :: rslatt(3,3)           ! Alat*rslatt(1..3,*)/(2*pi) -- in <(at.un.>/(2*pi)
      real(8), intent(in) :: Rnncut,Rsmall         ! in at.unit.
      integer, intent(in) :: mapsprs(natom,natom)
      real(8), intent(in) :: r_ref(:)    ! rws(nbasis)  ;   rmt0 is a radius for ref.system (screening)
      integer, intent(in) :: iprint
      character(10), intent(in) :: istop
!!REVISION HISTORY:
! Adapted  - A.S. - 2013
!!TO DO:
! to remove common block /Alltime/ used for timing
!EOP
!
!BOC
!
      real*8 xknlat0(ndimks,3)
      real*8 dutory
      real(8) :: Rnn_2pi,Rsm_2pi
!c
      complex*16 cof(size(tcpa,1),size(tcpa,1),natom)

      complex*16 trefinv(size(tcpa,1),nbasis)
      complex*16 tref(size(tcpa,1),nbasis)
      complex*16 deltat(size(tcpa,1),size(tcpa,1),natom)
      complex*16 deltinv(size(tcpa,1),size(tcpa,1),natom)

      complex*16 tauir(size(tcpa,1),size(tcpa,1),natom)

      integer :: icalcref


!c      complex*16 taun1n2,tautmp
!c      complex*16 w2(ipkkr,ipkkr)

!c
!c     +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
      character(10), parameter :: sname='intgrtau'
!c     +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!c
!c     ***************************************************************
!c     called by:     gettau
!c     calls    :     caltau  zeroout  fstop
!c
!c     does brillouin zone integral: special k-point method
!c                                             DDJ and WAS, May 1996
!c     ***************************************************************
!c

      complex*16 trefl

!      complex*16 cone,czero
!      parameter (czero=(0.d0,0.d0),cone=(1.d0,0.d0))
      integer nlatt(3)
      integer erralloc
!calloc   complex*16 greenks(iprmat*iprmat)
      complex*16, allocatable  :: greenKS(:)
!      complex*16, allocatable  :: greenrs(:,:)

      complex*16 wsmall(size(tcpa,1)*size(tcpa,1)*2),wfac

      integer ::  isprs(3),isave
      integer, allocatable ::  numnbi(:),nsmall(:),nrsij(:)

      integer, allocatable  ::  ntau(:,:)
      integer, allocatable  ::  jclust(:,:)
      integer, allocatable  ::  ntypecl(:,:)
      integer, allocatable  ::  mappnni(:,:),mapnnij(:,:)  !  working arrays
      integer, allocatable  ::  mapsnni(:,:)
      real*8,  allocatable  ::  rsij(:,:)
      real*8,  allocatable  ::  rijcl(:)

      integer :: i,j,iatom,nsub
      integer :: ndimnn=0,maxclstr=0,ntmp=0,ndmtr3=0

      integer  nnptr(0:1,natom)

!c      logical  ialloc
!      save ntau,jclust,ntypecl,mappnni,mapsnni,rsij

      type(RunState), pointer :: M_S
      integer :: ierr, iii

      integer imethod,isparse,iscreen,irealsp
      integer :: invparam=0

      real MB
      parameter (MB=1024.*1024.)

      real(8) :: time1,time2
!CDEBUG
!DEBUG      real timeEn,timeIneq
!DEBUG      common /Alltime/timeEn,timeIneq
!CDEBUG

      call timel(time2)

!CDEBUG
!CDEBUG      nrotmax = 1
!CDEBUG      do igrp=1,ngrp
!CDEBUG       nrotmax = max(nrotmax,lwght(kptindx(kptgrp(igrp))))
!CDEBUG      enddo
!CDEBUG      if(nrotmax.gt.1) then
!CDEBUG       nch = 74
!CDEBUG      else
!CDEBUG       nch = 73
!CDEBUG      end if
!CDEBUG      do nsub=1,natom
!CDEBUG       write(nch,*) ' NSUB=',nsub,ngrp,nch,nrotmax
!CDEBUG
!CDEBUGCDEBUG        tcpa(5,9,nsub) = tcpa(5,9,nsub)+(0.02d0,0.01d0)
!CDEBUGCDEBUG        tcpa(9,5,nsub) = tcpa(5,9,nsub)
!CDEBUG
!CDEBUG      do i=1,kkrsz
!CDEBUG       do j=i,kkrsz
!CDEBUG        if(max(abs(tcpa(j,i,nsub)),abs(tcpa(i,j,nsub)))
!CDEBUG     *                                       .gt.1.d-14) then
!CDEBUG         if(j.ne.i.or.abs(tcpa(j,i,nsub)-tcpa(i,j,nsub))
!CDEBUG     *                                       .gt.1.d-14) then
!CDEBUG          write(nch,2222) j,i,tcpa(j,i,nsub)
!CDEBUG     *                       ,tcpa(i,j,nsub)
!CDEBUG         else
!CDEBUG          write(nch,2222) j,i,tcpa(j,i,nsub)
!CDEBUG         end if
!CDEBUG        end if
!CDEBUG       end do
!CDEBUG      end do
!CDEBUG      end do
!CDEBUG

      isprs(:) = 0
      iscreen = 0                               ! no screening
      irealsp = 0                               ! KS-inversion
!      icut    = 0
      M_S => getMS()

      call DefMethod(1,imethod,isparse)

      if(isparse.ne.0) then
       isprs(1) = 1            ! SPARSE-MATRIX TECHNIQUE
       if(aimag(pdu).gt.invpar_prSLU()) then
        isprs(2) = isparse      ! TFQMR+PRECONDITIONER: 1-JCB,2-ILU,3-CLS
                                ! isprs(3) = 1 -- sparse KS-inversion
        if(aimag(pdu).gt.invpar_prPRC()) then
          isprs(2) = -abs(isprs(2))   ! without precond.
        end if
       else
         isprs(2) = 0             ! SuperLU
!c         isprs(2) = 1             ! TFQMR
!c         isprs(2) = 2             ! MAR
       end if
      end if

!DEBUG ::  TO DELETE AFTER IMPLEMENTATION OF ITERATIVE METHODS
      if ( isprs(1).ne.0 ) then
       isprs(2)=0
      end if
!DEBUG ::  TO DELETE AFTER IMPLEMENTATION OF ITERATIVE METHODS

!DEBUGPRINT
!DELETE      write(6,*) ' DEBUG intgrtau ISPRS=',isprs
!DEBUGPRINT

      select case (imethod)
!       case(-1)   !  KS-screening + KS-inversion    DISABLED
!        iscreen=-1                               ! KS-screening
!        if(isprs(1).ne.0) then
!         write(6,*) ' IMETHOD=',imethod
!         write(6,*) ' ISPRS(1)=',isprs(1)
!         call fstop(sname//' :: Method has not been implemented')
!        end if
!
       case(0)    !  KS-inversion (classical method)
!
         iscreen=0                ! no screening
         isprs = 0
!
         call tauKS(                                                    &
     &                 lmax,kkrsz,nbasis,natom,rytodu,                  &
     &                 naij,aij,iorig,itype,itype1,if0,                 &
     &                 ndimbas,mapstr,mappnt,mapij,                     &
     &                 edu,pdu,powe,                                    &
     &                 irecdlm,ndimrs,rsn,ndimnp,np2r,numbrs,           &
     &                 ndimdqr,dqint,ndimr,ndimlm,hplnm,                &
     &                 tcpa,tau00,nop,dop,d00,eoeta,eta,                &
     &                 ndimks,nkns,xknlat,R2ksp,conr,                   &
     &                 nqpt,qmesh,lwght,lrot,ngrp,kptgrp,kptset,kptindx,&
     &                 M_S%mpi_box                                      &
     &                ,iprint)
         go to 1000
!
       case(1)    !  RS-screening + FFT + KS-inversion
!
         iscreen=1                                ! type of RS-screening
         irealsp = 0                              ! sparse KS-inversion
!
         call tauScrKS(                                                 &
     &                 lmax,size(tcpa,1),nbasis,                        &
     &                 rytodu,naij,aij,natom,                           &
     &                 iorig,itype,itype1,if0,                          &
     &                 ndimbas,mapstr,                                  &
     &                 edu,pdu,                                         &
     &                 ndimr,ndimlm,hplnm,                              &
     &                 tcpa,tau00,dop,                                  &
     &                 nqpt,qmesh,lwght,lrot,ngrp,kptgrp,kptset,kptindx,&
     &                 rslatt,Rnncut,Rsmall,                            &
     &                 r_ref,                                           &
     &                 mapsprs,                                         &
     &                 M_S%mpi_box,                                     &
     &                 iprint)
         go to 1000
!
       case(2)    !  RS-screening + ( FFT+KS- or RS-inversion )
!
         iscreen=1                                ! type of RS-screening
         if(aimag(pdu).gt.invpar_prlim()) then
!         isprs(1) = 1
          irealsp = 1                             ! sparse RS-inversion
          call RStau00(                                                 &
     &              lmax,size(tcpa,1),nbasis,                           &
     &              rytodu,aij,itype,natom,iorig,                       &
     &              mapstr,ndimbas,                                     &
     &              pdu,                                                &
     &              naij,                                               &
     &              tcpa,                                               &
     &              tau00,                                              &
     &              rslatt,Rnncut,Rsmall,                               &
     &              r_ref,                                              &
     &              isprs(1:1),                                         &
     &              iprint,istop)
         else
!         isprs(1) = 1
          irealsp = 0                             ! sparse KS-inversion
         call tauScrKS(                                                 &
     &                 lmax,size(tcpa,1),nbasis,                        &
     &                 rytodu,naij,aij,natom,                           &
     &                 iorig,itype,itype1,if0,                          &
     &                 ndimbas,mapstr,                                  &
     &                 edu,pdu,                                         &
     &                 ndimr,ndimlm,hplnm,                              &
     &                 tcpa,tau00,dop,                                  &
     &                 nqpt,qmesh,lwght,lrot,ngrp,kptgrp,kptset,kptindx,&
     &                 rslatt,Rnncut,Rsmall,                            &
     &                 r_ref,                                           &
     &                 mapsprs,                                         &
     &                 M_S%mpi_box,                                     &
     &                 iprint)
         end if
         go to 1000
!
       case(3)    !  RS-screening + sparse RS-inversion (screened LSMS)
!
         iscreen = 1
!        isprs(1) = 1
         irealsp = 1
         call RStau00(                                                  &
     &              lmax,size(tcpa,1),nbasis,                           &
     &              rytodu,aij,itype,natom,iorig,                       &
     &              mapstr,ndimbas,                                     &
     &              pdu,                                                &
     &              naij,                                               &
     &              tcpa,                                               &
     &              tau00,                                              &
     &              rslatt,Rnncut,Rsmall,                               &
     &              r_ref,                                              &
     &              isprs(1:1),                                         &
     &              iprint,istop)
         go to 1000
!
       case default
!
         write(6,*) ' IMETHOD=',imethod
         call fstop(sname//' :: Method is not implemented')
!
      end select

!c***********************************************************************

1000  continue

!DELETE      timeEn = timeEn+(time2-time1)
!DELETE      timeIneq = timeIneq+(time2-time1)
!DELETE      if(isprs(1).eq.0.or.iscreen.le.0) timeIneq = 0.
!      if ( allocated(greenrs) ) deallocate(greenrs)
      if ( allocated(rsij) )    deallocate(rsij)
      if ( allocated(mapsnni) ) deallocate(mapsnni)
      if ( allocated(mappnni) ) deallocate(mappnni)
      if ( allocated(mapnnij) ) deallocate(mapnnij)
      if ( allocated(ntau) )    deallocate(ntau)
      if ( allocated(jclust) )  deallocate(jclust)
      if ( allocated(ntypecl) ) deallocate(ntypecl)
      if ( allocated(rijcl) )   deallocate(rijcl)
      if ( allocated(numnbi) )  deallocate(numnbi)
      if ( allocated(nsmall) )  deallocate(nsmall)
      if ( allocated(nrsij) )   deallocate(nrsij)

      do iatom=1,natom
       do j=1,kkrsz
        do i=1,kkrsz
         tcpa(i,j,iatom) = tcpa(i,j,iatom)*rytodu
        end do
       end do
      end do

      time1 = time2
      call timel(time2)
      if(iprint.ge.0) then
       write(6,*) sname//' :: E-point time per CPA iter (sec) =',       &
     &                        time2-time1
      end if

!DEBUG
!      write(6,*) ' intgrtau DEBUG: tau00 is changed'
!       do nsub=1,nbasis
!!          write(6,*) sname//':: nsub=',nsub
!!        iatom = iorig(nsub)
!!        tau00(:,:,nsub) = tcpa(:,:,iatom)
!        call change_matrix(tau00(:,:,nsub),size(tau00,1),kkrsz,1.d-9)
!!!        call symmtrx(tau00(1,1,nsub),size(tau00,1),kkrsz,1.d-12)
!       enddo
!DEBUG

!c
!c     ==============================================================

       if( iprint .ge. 1 ) then
        write(6,1001) sname//':: TAU00 ::',edu,pdu
1001    format(2x,a22,2d18.10,1x,2d18.10)
         do nsub = 1,nbasis
          write(6,*) sname//':: nsub=',nsub
          call wrtmtx(tau00(:,:,nsub),kkrsz,istop)
         enddo
       endif

!c     ==============================================================
      if( istop .eq. sname ) then
        call fstop(sname)
      endif
!c
      return

!EOC
      contains

!DEBUG
      subroutine change_matrix(a,n1,n2,eps)
      implicit none
      integer n1,n2,i,j
      complex(8) a(n1,n2)
      real(8) eps
      do i=1,n2
       do j=i+1,n2
       if ( abs(a(i,j))<eps .or. abs(a(j,i))<eps ) then
           a(i,j) = 0 ; a(j,i) = 0
       end if
       end do
!       write(6,*) i,dreal(a(i,i)),aimag(a(i,i)),' ***'
      end do
      return
      end subroutine change_matrix
!DEBUG

!BOP
!!IROUTINE: sumrot2
!!INTERFACE:
      subroutine sumrot2(tauir,tau00,if0,                               &
     &                   nrot,lrot,dop,nbasis,kkrsz,ndkkr,              &
     &                   iorig)
!!DESCRIPTION:
! given integral of tau00 over irreducible part of Brillouin Zone
! calculates the full integral by summing over rotations
! {\bv
! Tau_rot = U * Tau * U^(-1), U -- rotation matrix for matrices,
!                   U = conjg[ D ], D -- rotation matrix for Ylm,
!                   and, BTW, D^(-1)_LM = conjg[ D_ML]
!
! Therefore
!    Tau_rot = conjg[ D ] * Tau * transposed [ D ]
! \ev}
!!REMARKS:
! private procedure of subroutine intgrtau
!EOP
!
!BOC
      implicit none
!c
      character    sname*10
!c
!c     ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
      integer      nbasis,ndkkr,kkrsz
      complex*16   tau00(ndkkr,ndkkr,nbasis)
      complex*16   tauir(ndkkr,ndkkr)
      integer      if0(48)
      integer      nrot,lrot(nrot),iorig(*)
      complex*16   dop(kkrsz,kkrsz,*)
      complex*16   taun1n2
      complex*16   tautmp,czero
      parameter    (czero=(0.d0,0.d0))

      integer      nzzz
      complex*16   zzztau(ndkkr,ndkkr)

!c     ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
      parameter    (sname='sumrot2')

      integer nsub,j,n1,n2,ir,irrot,jsite
      logical laddinv

!c     ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!c
!c
       nzzz = size(zzztau,1)*kkrsz
       laddinv = .false.
       do ir=1,nrot
        if(lrot(ir).lt.0) then
         laddinv = .true.
         exit
        end if
       end do

       do nsub=1,nbasis

        call zerooutC(zzztau,nzzz)

        do ir=1,nrot
         irrot = lrot(ir)
         jsite = if0(abs(irrot))

         if(iorig(nsub).eq.jsite) then

          if(irrot.gt.0) then

           do n2=1,kkrsz
            do n1=1,kkrsz
            taun1n2 = tauir(n1,n2)

             do j=1,kkrsz
              tautmp = conjg( dop(j,n1,irrot) ) * taun1n2

              call zaxpy(kkrsz,tautmp,dop(1,n2,irrot),1,                &
     &                                zzztau(j,1),ndkkr)

             enddo
            enddo
           enddo

          end if

         end if

        enddo

        call zgeadd(tau00(1,1,nsub),ndkkr,'N',                          &
     &              zzztau,ndkkr,'N',                                   &
     &              tau00(1,1,nsub),ndkkr,kkrsz,kkrsz)

        if(laddinv) then
         call zgeadd(tau00(1,1,nsub),ndkkr,'N',                         &
     &               zzztau,ndkkr,'T',                                  &
     &               tau00(1,1,nsub),ndkkr,kkrsz,kkrsz)
        end if
       enddo

!c
      return
!c
!EOC
      end subroutine sumrot2

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

      subroutine tauKS(                                                 &
     &                 lmax,kkrsz,nbasis,natom,rytodu,                  &
     &                 naij,aij,iorig,itype,itype1,if0,                 &
     &                 ndimbas,mapstr,mappnt,mapij,                     &
     &                 edu,pdu,powe,                                    &
     &                 irecdlm,ndimrs,rsn,ndimnp,np2r,numbrs,           &
     &                 ndimdqr,dqint,ndimr,ndimlm,hplnm,                &
     &                 tcpa,tau00,nop,dop,d00,eoeta,eta,                &
     &                 ndimks,nkns,xknlat,R2ksp,conr,                   &
     &                 nqpt,qmesh,lwght,lrot,ngrp,kptgrp,kptset,kptindx,&
     &                 mpi_box                                          &
     &                ,iprint)
      use mecca_constants
      use mecca_types, only : MPI_data
      use mpi
      implicit none

      integer, intent(in) :: lmax,kkrsz,nbasis
      real(8), intent(in) :: rytodu
      integer, intent(in) :: naij
      real(8), intent(in) :: aij(3,naij)
      integer, intent(in) :: natom,itype(natom)
      integer, intent(in) :: iorig(nbasis),itype1(natom)
      integer, intent(in) :: if0(48,natom)
      integer, intent(in) :: ndimbas
      integer, intent(in) :: mapstr(ndimbas,natom),mappnt(ndimbas,natom)
      integer, intent(in) :: mapij(2,*)
      complex(8), intent(in) :: powe(*)
      complex(8), intent(in) :: edu      ! energy in DU
      complex(8), intent(in) :: pdu
      integer, intent(in) :: irecdlm,ndimrs
      real(8), intent(in) :: rsn(ndimrs,4)
      integer, intent(in) :: ndimnp
      integer, intent(in) :: np2r(ndimnp,*),numbrs(*)
      integer, intent(in) :: ndimdqr
      complex(8), intent(in) :: dqint(ndimdqr,*)
      integer, intent(in) :: ndimr,ndimlm
      complex(8), intent(in) :: hplnm(ndimr,ndimlm,*)
      complex(8) :: tcpa(:,:,:)
      complex(8) :: tau00(:,:,:)
      integer, intent(in) :: nop
      complex(8), intent(in) :: dop(kkrsz,kkrsz,nop)  ! rotation matrices for Ylm
      complex(8), intent(in) :: d00,eoeta
      real(8), intent(in) :: eta
      integer, intent(in) :: ndimks,nkns
      real(8), intent(in) :: xknlat(ndimks,3),R2ksp
      complex(8), intent(in) :: conr(*)
      integer, intent(in) :: nqpt
      real(8), intent(in) :: qmesh(3,nqpt)
      integer, intent(in) :: lwght(nqpt),lrot(*)
      integer, intent(in) :: ngrp,kptgrp(ngrp+1)
      integer, intent(in) :: kptset(ngrp),kptindx(nqpt)
      type(MPI_data), intent(in), pointer :: mpi_box
      integer, intent(in) :: iprint
!
      character(*), parameter :: sname='tauKS'
      character(10), parameter :: istop='#nostop'
      integer :: iscreen = 0
      integer :: isprs(3) = 0
      integer :: erralloc,i,iatom,ikgrp,igrp,ineq,isite,iw
      integer :: ierr,j,nirot,nlrot,nrot,nn,nsub
      integer :: invparam=0
      integer :: ndmatr,ndmtr1,nknmin,nknmax,nkns0

      integer ::  numnbi(1),nsmall(1),nrsij(1)   ! redundant
      integer ::  nnclmn(1,1,1)                  ! redundant

      real(8) :: kx,ky,kz,xkn,ykn,zkn,xkn2
!      real(8) :: time1,time2,timeEn
      real(8) :: dutory

      real(8) :: xknlat0(ndimks,3)

      complex(8) :: deltat(size(tcpa,1),size(tcpa,1),natom)
      complex(8) :: deltinv(size(tcpa,1),size(tcpa,1),natom)
      complex(8) :: wsmall(size(tcpa,1)*size(tcpa,1)*2),wfac
      complex(8) :: tauir(size(tcpa,1),size(tcpa,1),natom)
      complex(8) :: cof(size(tcpa,1),size(tcpa,1),natom)
      complex(8), allocatable  :: greenKS(:)
      complex(8) :: greenrs(1)
!redundant      complex(8), allocatable  :: greenrs(:,:)

      complex(8) :: trefinv(size(tcpa,1),1)
      complex(8) :: tref(size(tcpa,1),1)

      integer :: ndimnn=0,maxclstr=0,ndmtr3=0 !redundant
      integer  ::  jclust(1,1)                !redundant
      integer  ::  mapsnni(1,1)               !redundant
      integer  ::  nnptr(0:1,1)               !redundant
      real(8)  ::  rsij(1,1)                  !redundant

!      call timel(time2)

      ineq = 0    ! all sites are considered inequivalent in matrix-inversion

!redundant      allocate(numnbi(nbasis),nsmall(nbasis),nrsij(nbasis))

      ndmatr = kkrsz*natom
      ndmtr1 = ndmatr*ndmatr

      call zerooutC(tau00,size(tau00,1)*size(tau00,2)*nbasis)
!c
!c     ===============================================================
!c     calculate tau00.....integral of tau(k) over 1 irr. wedge of BZ.
!c
      dutory= dble(1)/rytodu

      tcpa(1:kkrsz,1:kkrsz,1:natom)=tcpa(1:kkrsz,1:kkrsz,1:natom)*dutory
      deltat(1:kkrsz,1:kkrsz,1:natom)=tcpa(1:kkrsz,1:kkrsz,1:natom)

      do iatom=1,natom
       iw = 0
       do j=1,kkrsz
        do i=1,kkrsz
         iw = iw+1
         wsmall(iw) = deltat(i,j,iatom)
        end do
       end do
       call invmatr(wsmall(1:iw),                                       &
     &            kkrsz,1,                                              &
     &            1,1,                                                  &
     &            iprint,istop)
       iw = 0
        do j=1,kkrsz
         do i=1,kkrsz
          iw = iw+1
          deltinv(i,j,iatom) = wsmall(iw)
         end do
        end do
      end do

!c***********************************************************************
! K-space integration
!c***********************************************************************

!c       nskpt = nqpt
!c       twght = twght0
!c
!c     ---------------------------------------------------------------
!c     loop over all special k-pts
!c     ---------------------------------------------------------------
!c

       nknmin = nkns
       nknmax = 0
       allocate(greenKS(1:ndmtr1),                                      &
     &          stat=erralloc)
       if(erralloc.ne.0) then
         write(6,*) ' NDMATR=',ndmatr
         write(6,*) ' NDMTR1=',ndmtr1
         write(6,*) ' ERRALLOC=',erralloc
         call fstop(sname//' :: ALLOCATION MEMORY PROBLEM 4a')
       end if

       nlrot = 0
!c
!c     ---------------------------------------------------------------
!c
      do igrp=1,ngrp                                  ! begin group loop
       nirot = kptset(igrp)
       nrot = lwght(kptindx(kptgrp(igrp)))
       nlrot = nlrot+nrot*(kptgrp(igrp+1)-kptgrp(igrp))
       call zerooutC(tauir,size(tauir,1)*size(tauir,2)*natom)
!c
!c     ---------------------------------------------------------------
!c
       do  ikgrp=kptgrp(igrp),kptgrp(igrp+1)-1        ! begin group k-loop
        nn = kptindx(ikgrp)
!c
!c     ---------------------------------------------------------------
!c
        if ( associated(mpi_box) ) then
         if ( mpi_box%ik_end >= mpi_box%ik_begin ) then
        ! only perform those k-points within MPI job range
          if( nn < mpi_box%ik_begin .or. mpi_box%ik_end < nn ) cycle
         end if
        end if

       kx =   qmesh(1,nn)
       ky =   qmesh(2,nn)
       kz =   qmesh(3,nn)

!c        ============================================================
!c        calculate tau(k) [= cof(k)] for current k-pt
!c        ------------------------------------------------------------

          nkns0 = 0
          if(irecdlm.eq.0) then
           do i=1,nkns
            xkn = xknlat(i,1)
            ykn = xknlat(i,2)
            zkn = xknlat(i,3)
            xkn2=(xkn+kx)*(xkn+kx)+(ykn+ky)*(ykn+ky)+(zkn+kz)*(zkn+kz)
            if(xkn2.le.R2ksp) then
             nkns0 = nkns0+1
             xknlat0(nkns0,1) = xkn
             xknlat0(nkns0,2) = ykn
             xknlat0(nkns0,3) = zkn
            end if
           end do
           nknmin = min(nknmin,nkns0)
           nknmax = max(nknmax,nkns0)
          end if

          call taumix(lmax,natom,                                       &
     &              aij,itype,itype1,                                   &
     &              mapstr,mappnt,ndimbas,mapij,                        &
     &              powe,                                               &
     &              kx,ky,kz,                                           &
     &              edu,pdu,                                            &
     &              irecdlm,                                            &
     &              rsn,ndimrs,                                         &
     &              np2r,ndimnp,numbrs,naij,                            &
     &              dqint,ndimdqr,                                      &
     &              hplnm,ndimr,ndimlm,                                 &
     &              tcpa,                                               &
     &              d00,eoeta,                                          &
     &              eta,xknlat0,ndimks,                                 &
     &              conr,nkns0,                                         &
     &              iscreen,                                            &
     &              tref,trefinv,size(tcpa,1),deltat,deltinv,           &
     &              nsmall,jclust,                                      &
     &              mapsnni,ndimnn,                                     &
     &              nnptr,maxclstr,nnclmn,                              &
     &              rsij,                                               &
     &              1,invparam,isprs,                                   &
     &              greenrs,ndmtr3,greenKS,cof,                         &
     &              iprint,istop)

!c        ------------------------------------------------------------

        do isite=1,natom
           call zgeadd(tauir(1,1,isite),size(tauir,1),'N',              &
     &                 cof(1,1,isite),size(cof,1),'N',                  &
     &                 tauir(1,1,isite),size(tauir,1),kkrsz,kkrsz)
        enddo

       enddo                               ! end group k-pt loop

!c        ------------------------------------------------------------
!c        calculate tau00 in full zone on special k-pt mesh
!c               (unfolding)
!c        ------------------------------------------------------------

       if(ineq.ne.1) then
        do isite=1,natom
         call sumrot2(tauir(1,1,isite),tau00,if0(1,isite),              &
     &                 nrot,lrot(nirot),dop,nbasis,kkrsz,size(tau00,1), &
     &                 iorig)
        enddo
       else
        do nsub=1,nbasis
           call sumrot2(tauir(1,1,iorig(nsub)),tau00,if0(1,iorig(nsub)),&
     &                 nrot,lrot(nirot),dop,nbasis,kkrsz,size(tau00,1), &
     &                 iorig)
        enddo
       end if
!c
      enddo                               ! end group loop
!CDEBUG      enddo                               ! end k-pt loop

      ! sum contributions to tau00 from friend nodes
!MPIDEBUG      call log_mpi_sync(__FILE__,__LINE__)
       if ( in_place_mpi ) then    ! in_place_mpi is defined in mecca_constants
        if ( associated(mpi_box) ) then
         call mpi_allreduce(MPI_IN_PLACE,                               &
     &                    tau00(1:kkrsz,1:kkrsz,1:nbasis),              &
     &                    kkrsz*kkrsz*nbasis,                           &
     &       MPI_COMPLEX16,MPI_SUM,mpi_box%thisE_comm,ierr)
        end if
       else
        tauir(1:kkrsz,1:kkrsz,1:nbasis)=tau00(1:kkrsz,1:kkrsz,1:nbasis)
        if ( associated(mpi_box) ) then
         call mpi_allreduce(tauir(1:kkrsz,1:kkrsz,1:nbasis),            &
     &                    tau00(1:kkrsz,1:kkrsz,1:nbasis),              &
     &                    kkrsz*kkrsz*nbasis,                           &
     &       MPI_COMPLEX16,MPI_SUM,mpi_box%thisE_comm,ierr)
        end if
       end if

       if ( nlrot<=0 ) then
        write(*,*) sname//' : unexpected value of nlrot=',nlrot
        write(*,*) sname//' : kptgrp=',kptgrp(1:ngrp)
!        write(*,*) sname//' : kptindx=',kptindx(1:kptgrp(ngrp+1)-1)
        call fstop(sname//' :: WRONG NUMBER OF ROTATION OPERATIONS')
       end if

       wfac =  dcmplx(rytodu/nlrot,zero)
       do nsub=1,nbasis
        do i=1,kkrsz
         call zscal(kkrsz,wfac,tau00(1,i,nsub),1)
        enddo
       enddo

!       time1 = time2
!       call timel(time2)
!       timeEn = timeEn+(time2-time1)
!       if(iprint.ge.0) then
!        write(6,*) sname//' :: K-loop time=',real(time2-time1)          &
!     &            ,timeEn
!       end if

!redundant       if(allocated(nnclmn)) deallocate(nnclmn,stat=erralloc)
       deallocate(greenKS,stat=erralloc)
       if(erralloc.ne.0) then
        write(6,*) ' NDMTR1=',ndmtr1
!DELETE        write(6,*) ' NDMTR2=',ndmtr2
        write(6,*) ' ERRALLOC=',erralloc
        call fstop(sname//' :: DEALLOCATION MEMORY PROBLEM')
       end if

       if( iprint .ge. 0 ) then
        if(iscreen<=0 .and. irecdlm.eq.0)                               &
     &   write(6,*) '  NKNMIN=',nknmin,'  NKNMAX=',nknmax
       end if

!c***********************************************************************

      return
      end subroutine tauKS

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

      subroutine tauScrKS(                                              &
     &                 lmax,ndkkr,nstot,                                &
     &                 rytodu,naij,aij,natom,                           &
     &                 iorig,itype,itype1,if0,                          &
     &                 ndimbas,mapstr,                                  &
     &                 edu,pdu,                                         &
     &                 ndimr,ndimlm,hplnm,                              &
     &                 tcpa,tau00,dop,                                  &
     &                 nqpt,qmesh,lwght,lrot,ngrp,kptgrp,kptset,kptindx,&
     &                 rslatt,Rnncut,Rsmall,                            &
     &                 r_ref,                                           &
     &                 mapsprs,                                         &
     &                 mpi_box                                          &
     &                ,iprint)
!!DESCRIPTION:
! computes tau00 in reciprocal space \\
! RS-screening + KS-inversion ("screened KKR")
!
!!USES:
      use mecca_constants
      use mecca_types, only : MPI_data
      use mpi

      use mtrx_interface
      use screening, only : calcTref,clstrRegEst
      use screening, only : g_scr_potpar,g_calcref
      implicit none

      integer, intent(in) :: lmax,ndkkr,nstot
      real(8), intent(in) :: rytodu
      integer, intent(in) :: naij
      real(8), intent(in) :: aij(3,naij)
      integer, intent(in) :: natom,itype(natom)
      integer, intent(in) :: iorig(nstot),itype1(natom)
      integer, intent(in) :: if0(48,natom)
      integer, intent(in) :: ndimbas
      integer, intent(in) :: mapstr(ndimbas,natom)
      complex(8), intent(in) :: edu,pdu
      integer, intent(in) :: ndimr,ndimlm
      complex(8), intent(in) :: hplnm(ndimr,ndimlm,*)
      complex(8) :: tcpa(ndkkr,ndkkr,natom)
      complex(8), intent(out) :: tau00(ndkkr,ndkkr,nstot)
      complex(8), intent(in) :: dop(:,:,:)  ! (kkrsz,kkrsz,nop)  ! rotation matrices for Ylm
      integer, intent(in) :: nqpt
      real(8), intent(in) :: qmesh(3,nqpt)
      integer, intent(in) :: lwght(nqpt),lrot(*)
      integer, intent(in) :: ngrp,kptgrp(ngrp+1)
      integer, intent(in) :: kptset(ngrp),kptindx(nqpt)
      type(MPI_data), intent(in), pointer :: mpi_box
      integer, intent(in) :: mapsprs(natom,natom)
      integer, intent(in) :: iprint
      real(8), intent(in) :: rslatt(3,3)   ! Alat*rslatt(1..3,*)/(2*pi) -- in <at.un.>/(2*pi)
      real(8), intent(in) :: Rnncut,Rsmall ! in at.unit.
      real(8), intent(in) :: r_ref(:)
!
      character(*), parameter :: sname='tauScrKS'
      character(10), parameter :: istop='#nostop'
      integer :: iscreen = 1
      integer :: isparse = 1
      integer :: kkrsz
      integer :: erralloc,i,iatom,ikgrp,igrp,ineq,isite,iw
      integer :: invparam,ierr,j,nirot,nlrot,nrot,nn,nsub
      integer :: ndmatr,ndmtr1,nknmin,nknmax

      integer, allocatable ::  numnbi(:),nsmall(:),nrsij(:)
      integer, allocatable ::  nnclmn(:,:,:)

      real(8) :: kx,ky,kz
!      real(8) :: time1,time2,timeEn,timers1,time1a
      real(8) :: dutory

      complex(8) :: deltat(size(tcpa,1),size(tcpa,1),natom)
      complex(8) :: deltinv(size(tcpa,1),size(tcpa,1),natom)
      complex(8) :: wsmall(size(tcpa,1)*size(tcpa,1)*2),wfac
      complex(8) :: tauir(size(tcpa,1),size(tcpa,1),natom)
      complex(8) :: cof(size(tcpa,1),size(tcpa,1),natom)
      complex(8), allocatable  :: greenKS(:)
      complex(8), allocatable  :: greenrs(:,:)

      complex(8) :: trefinv(size(tcpa,1),nstot)
      complex(8) :: tref(size(tcpa,1),nstot)

      integer :: ndimnn=0,maxclstr=0,ndmtr3=0
      integer ::  nnptr(0:1,natom)
      integer, allocatable  ::  jclust(:,:)
      integer, allocatable  ::  mapsnni(:,:)
      real(8), allocatable  ::  rsij(:,:)

      integer :: icalcref,isave,iatom0,isub
      real(8) :: Rnn_2pi,Rsm_2pi

      real(8) :: param1
      integer :: inum
      integer :: nlatt(3)

      integer, allocatable  ::  mappnni(:,:),mapnnij(:,:)  !  working arrays
      integer, allocatable  ::  ntau(:,:)
      integer, allocatable  ::  ntypecl(:,:)
      real(8), allocatable  ::  rijcl(:)

      real(8), parameter :: MB=dble(1024*1024)

!----------------------------------------------------------------------
!   To invert the K-space matrix all sites should be considered as
!   inequivalent except the case when full zone integration is performed
!   or you need only diagonal elements of tau-matrix
!   (without CPA, for instance).
!-----------------------------------------------------------------------

!      call timel(time2)
!      time1 = time2
!      timeEn = 0

      ineq = 0    ! all sites are considered inequivalent in matrix-inversion
      iscreen=1                                ! RS-screening

      allocate(numnbi(nstot),nsmall(nstot),nrsij(nstot))

      kkrsz = (lmax+1)**2
      ndmatr = kkrsz*natom
      ndmtr1 = ndmatr*ndmatr
      call zerooutC(tau00,size(tau00,1)*size(tau00,2)*nstot)
!c
!c     ===============================================================
!c     calculate tau00.....integral of tau(k) over 1 irr. wedge of BZ.
!c
      dutory= one/rytodu
      Rnn_2pi = Rnncut*dutory
      Rsm_2pi = Rsmall*dutory

!      r3ws = 0
!      do nsub=1,nstot
!       do ic=1,komp(nsub)
!        r3ws = r3ws + atcon(ic,nsub)*numbsub(nsub)*rws(ic,nsub)**3
!       end do
!      end do
!      r3ws = r3ws/natom
!       rmt0 = r3ws**(one/three)

       icalcref = g_calcref()
       call calcTref(nstot,lmax,pdu,rytodu,r_ref,icalcref,              &
     &                                           tref,trefinv,iprint>=0)
!         trefinv[1:kkrsz] = tref^(-1) -- in D.U.

!c  Calculation of DeltaT = t - t_ref

      tcpa = dutory * tcpa
      deltat = tcpa
!c  Now  tcpa -- in DU
      do iatom=1,natom
       nsub = itype(iatom)
       do j=1,kkrsz
         deltat(j,j,iatom) = tcpa(j,j,iatom)-tref(j,nsub)
       end do
      end do

!c  Now DeltaT = t-tref (in DU)

      do iatom=1,natom

       iw = 0
       do j=1,kkrsz
        do i=1,kkrsz
         iw = iw+1
         wsmall(iw) = deltat(i,j,iatom)
        end do
       end do
       call invmatr(wsmall(1:iw),                                       &
     &            kkrsz,1,                                              &
     &            1,1,                                                  &
     &            iprint,istop)
       iw = 0
        do j=1,kkrsz
         do i=1,kkrsz
          iw = iw+1
          deltinv(i,j,iatom) = wsmall(iw)
         end do
        end do

      end do

!c=======================================================================
!c  Now deltat = DeltaT^(-1) = (t-tref)^(-1)
!c=======================================================================

!c***********************************************************************
!  Screening
!c***********************************************************************

!TO DO: VP-info could be used for ndimnn estimate

!DELETE       call clstrRegEst(rslatt,Rnn_2pi,ndimnn,nlatt)
!DELETE!c      write(6,*) ' nndim=',ndimnn
!DELETE       ndimnn = 1 + ndimnn*natom
       call clstrsize(nstot,natom,iorig,mapstr,                         &
     &                            aij,Rnn_2pi,rslatt,nlatt,numnbi)
       ndimnn = maxval(numnbi(1:nstot))
       maxclstr = ndimnn
       allocate(                                                        &
     &   rsij(1:ndimnn*ndimnn*3,1:nstot),                               &
     &   mapsnni(1:ndimnn*ndimnn,1:nstot),                              &
     &   mappnni(1:ndimnn*ndimnn,1:nstot),                              &
     &   mapnnij(1:ndimnn*ndimnn*2,1:nstot),                            &
     &   ntau(1:ndimnn,1:nstot),                                        &
     &   jclust(1:ndimnn,1:nstot),                                      &
     &   ntypecl(1:ndimnn,1:nstot),                                     &
     &   rijcl(1:3*ndimnn*ndimnn),                                      &
     &  stat=erralloc)

       if(erralloc.ne.0) then
        write(6,*) '   NBASIS=',nstot
        write(6,*) '   NDIMNN=',ndimnn
        write(6,*) '    NATOM=',natom
        write(6,*) '   RNNCUT=',rnncut
        write(6,*) ' ERRALLOC=',erralloc
        call fstop(sname//' :: ALLOCATION MEMORY PROBLEM 3')
       end if

       if ( dble(kkrsz*ndimnn)**2 > dble(huge(0)) ) then
        write(6,*) ' kkrsz=',kkrsz,' ndimnn=',ndimnn
        write(6,*) ' (kkrsz*ndimnn)**2=',(dble(kkrsz)*ndimnn)**2
        call fstop(sname//' :: MATRIX SIZE IS TOO LARGE')
       else
!DELETE        ndmtr3 = (kkrsz*ndimnn)*(kkrsz*ndimnn)
        ndmtr3 = (kkrsz*maxclstr)*(kkrsz*maxclstr)
       end if

       allocate(greenrs(ndmtr3,1:nstot),                                &
     &                   stat=erralloc)
       if(erralloc.ne.0) then
        write(6,*) ' KKRSZ=',kkrsz
        write(6,*) ' NDIMNN=',ndimnn
        write(6,*) ' NBASIS=',nstot
        write(6,*) ' NDMTR3=',ndmtr3
        write(6,*) ' ERRALLOC=',erralloc
        write(6,*)                                                      &
     &      ndmtr3*16./MB*nstot,                                        &
     &      'MB COULD NOT BE ALLOCATED'
        call fstop(sname//' :: ALLOCATION MEMORY PROBLEM 1')
       end if
       call zerooutC(greenrs,size(greenrs))

       if(iprint.ge.1) then
        write(6,*) '    NDIMNN=',ndimnn
        write(6,*)                                                      &
     &      ndmtr3*16./MB*nstot,                                        &
     &      'MB HAVE BEEN ALLOCATED'
       end if

!       maxclstr = 0

!       timers1 = zero
!       time1a = time2

       do nsub=1,nstot

        iatom = iorig(nsub)

!c=======================================================================
!c   Calculation of NN-cluster for site "iatom"
!c=======================================================================
        call nncluster(iatom,natom,mapstr,ndimbas,                      &
     &   aij,Rnn_2pi,Rsm_2pi,rslatt,itype,nlatt,                        &
     &   numnbi(nsub),nsmall(nsub),                                     &
     &   jclust(1,nsub),ntypecl(1,nsub),ntau(1,nsub),                   &
     &   rsij(1,nsub),nrsij(nsub),                                      &
     &   mapsnni(1,nsub),ndimnn,mappnni(1,nsub),mapnnij(1,nsub)         &
     &   ,rijcl                                                         &
     &    )

        if(max(nsmall(nsub),numnbi(nsub)).gt.ndimnn) then
         write(6,1005)                                                  &
     &    iatom,nsub,numnbi(nsub),Rnncut,                               &
     &    nsmall(nsub),Rsmall
         write(6,'('' NDIMNN='',i5)') ndimnn
         call fstop(sname//':: NDIMNN is too small')
        end if

        if(iprint.ge.0) then
         write(6,1005)                                                  &
     &    iatom,nsub,numnbi(nsub),Rnncut,                               &
     &    nsmall(nsub),Rsmall
1005    format(' IATOM=',i5,' NSUB=',i5,' NUMNBI=',i4,' Rnncut=',f5.2,  &
     &         ' NSMALL=',i4,' Rsmall=',f5.2)
        end if

!        maxclstr = max(maxclstr,nsmall(nsub))

        call gf0_rs(lmax,pdu,                                           &
     &          greenrs(1,nsub),                                        &
     &          ndimnn,nsmall(nsub),                                    &
     &          rsij(1,nsub),mapsnni(1,nsub),                           &
     &          1,                                                      &
     &          istop)

        call calgreen(                                                  &
     &              greenrs(1,nsub),                                    &
     &              kkrsz,nsmall(nsub),                                 &
     &              tref,trefinv,size(tref,1),1,ntypecl(1,nsub),        &
     &              1,invparam,                                         &
     &              iprint,istop)

!CDEBUG
!CDEBUG        write(80+nsub,1070)
!CDEBUG     *    (iii,greenrs(iii,nsub),iii=1,(kkrsz*maxclstr)**2)
!CDEBUG1070    format(1x,i4,1x,2f15.8)

!CDEBUG        if(iii.gt.1.and.irealsp.ne.1) then
!CDEBUG         greenrs(1:ndmtr3,nsub) = greenrs(1:ndmtr3,itype(iatom))
!CDEBUG        end if
!CDEBUG


!        time1 = time2
!        call timel(time2)
!        timers1 = timers1+(time2-time1)
       end do

!!       timers1 = natom*(timers1/nstot) -timers1
!       time1 = time2
!       call timel(time2)
!       timeEn = timeEn+(time2-time1a)
!!       timeIneq = timeEn + timers1
!
!       if(iprint.ge.0) then
!         write(6,'(a,3(1x,f7.2))') sname//' :: RS-time=',               &
!     &                            real(time2-time1a),timeEn
!       end if

! ===================================================================

        allocate(nnclmn(1:2,1:maxclstr,1:natom),                        &
     &                   stat=erralloc)
        if(erralloc.ne.0) then
          write(6,*) ' MAXCLSTR=',maxclstr
          write(6,*) ' NATOM=',natom
          write(6,*) ' ERRALLOC=',erralloc
          call fstop(sname//' :: ALLOCATION MEMORY PROBLEM ')
        end if

        isave = 0
        do iatom0=1,natom
         isub = itype(iatom0)
         call nnlist(iatom0,natom,                                      &
     &              aij,isub,                                           &
     &              mapstr,ndimbas,                                     &
     &              mapsprs,rslatt,                                     &
     &              nsmall(isub),jclust(1,isub),Rsmall,                 &
     &              mapsnni(1,isub),ndimnn,                             &
     &              rsij(1,isub),                                       &
     &              nnptr(0,iatom0),nnclmn(1,1,iatom0),                 &
     &              isave,istop)
         if(isave.lt.0) then  ! NN-cluster is bigger than the cell,
          isave = 2           ! matrix is not sparse: it could be used
!          do i=1,3           ! to switch to non-sparse KS-integration
!           isprs(i) = 0
!          end do
         end if
        end do

!c***********************************************************************

!        time1 = time2
!        call timel(time2)
!        timeEn = timeEn+(time2-time1)

!c***********************************************************************
! K-space integration
!c***********************************************************************

!DELETE       timeIneq = timeEn

!c       nskpt = nqpt
!c       twght = twght0
!c
!c     ---------------------------------------------------------------
!c     loop over all special k-pts
!c     ---------------------------------------------------------------
!c

       nknmin = 0
       nknmax = 0
       ndmtr1 = natom*(kkrsz*kkrsz)*maxclstr
!DELETE       if(isprs(2).eq.3.and.isprs(3).eq.0)                              &
!DELETE     &   ndmtr1 = ndmtr1+natom*(kkrsz*kkrsz)*maxclstr
!c
!c   NDMTR1 can be increased to use additional memory (in future)
!c

         allocate(greenKS(1:ndmtr1),                                    &
     &          stat=erralloc)
         if(erralloc.ne.0.or.ndmtr1.eq.0) then
          write(6,*) ' NDMATR=',ndmatr
          write(6,*) ' NDMTR1=',ndmtr1
          write(6,*) ' NATOM=',natom
          write(6,*) ' KKRSZ=',kkrsz
          write(6,*) ' MAXCLSTR=',maxclstr
          write(6,*) ' ERRALLOC=',erralloc
          call fstop(sname//' :: ALLOCATION MEMORY PROBLEM 4b')
         end if

       nlrot = 0
!c
!c     ---------------------------------------------------------------
!c
      do igrp=1,ngrp                                  ! begin group loop
       nirot = kptset(igrp)
       nrot = lwght(kptindx(kptgrp(igrp)))
       nlrot = nlrot+nrot*(kptgrp(igrp+1)-kptgrp(igrp))
       call zerooutC(tauir,size(tauir,1)*size(tauir,2)*natom)
!c
!c     ---------------------------------------------------------------
!c
       do  ikgrp=kptgrp(igrp),kptgrp(igrp+1)-1        ! begin group k-loop
        nn = kptindx(ikgrp)
!c
!c     ---------------------------------------------------------------
!c
        if ( associated(mpi_box) ) then
         if ( mpi_box%ik_end >= mpi_box%ik_begin ) then
        ! only perform those k-points within MPI job range
          if( nn < mpi_box%ik_begin .or.                                &
     &       mpi_box%ik_end < nn ) cycle
         end if
        end if

       kx =   qmesh(1,nn)
       ky =   qmesh(2,nn)
       kz =   qmesh(3,nn)

!c        ============================================================
!c        calculate tau(k) [= cof(k)] for current k-pt
!c        ------------------------------------------------------------

!CDEBUGPRINT
!        write(70+nsub,*)' kkrsz=',kkrsz,' ndimnn=',ndimnn,' nsub=',nsub
!        do iii=1,(kkrsz*ndimnn)**2
!         if ( abs(greenrs(iii,nsub))>1.d-10 ) then
!        write(70+nsub,1071)                                             &
!     &    iii,greenrs(iii,nsub)
!1071    format(1x,i4,1x,2f15.8)
!         end if
!        end do
!        close(70+nsub)
!CDEBUGPRINT
          call tauScr_ks(lmax,natom,                                    &
     &              aij,                                                &
     &              itype,nstot,                                        &
     &              mapstr,ndimbas,                                     &
     &              kx,ky,kz,                                           &
     &              tcpa,                                               &
     &              size(tref,1),deltat,deltinv,                        &
     &              numnbi,jclust,                                      &
     &              mapsnni,ndimnn,                                     &
     &              nnptr,maxclstr,nnclmn,                              &
!DELETE     &              rsij,param1,inum,                                   &
     &              rsij,                                               &
     &              greenrs,size(greenrs,1),cof,                        &
     &              istop)
!c        ------------------------------------------------------------

          do isite=1,natom
           call zgeadd(tauir(1,1,isite),size(tauir,1),'N',              &
     &                 cof(1,1,isite),size(cof,1),'N',                  &
     &                 tauir(1,1,isite),size(tauir,1),kkrsz,kkrsz)
          enddo

       enddo                               ! end group k-pt loop

!c        ------------------------------------------------------------
!c        calculate tau00 in full zone on special k-pt mesh
!c               (unfolding)
!c        ------------------------------------------------------------

!?       if(ineq.ne.1) then
        do isite=1,natom
         call sumrot2(tauir(1,1,isite),tau00,if0(1,isite),              &
     &                 nrot,lrot(nirot),dop,nstot,kkrsz,size(tau00,1),  &
     &                 iorig)
        enddo
!?       else
!?        do nsub=1,nstot
!?           call sumrot2(tauir(1,1,iorig(nsub)),tau00,if0(1,iorig(nsub)),&
!?     &                 nrot,lrot(nirot),dop,nstot,kkrsz,size(tau00,1),  &
!?     &                 iorig)
!?        enddo
!?       end if
!c
      enddo                               ! end group loop

!DEBUGPRINT
!       write(6,*) ' TAU00(k)'
!       do nsub=1,nstot
!        call wrtmtx(tau00(1:kkrsz,1:kkrsz,nsub),kkrsz,'##########')
!       end do
!       call fstop('DEBUG')
!DELETE       call debug_tau(rytodu,edu,kx,ky,kz,tcpa(1:kkrsz,1:kkrsz,1:natom),&
!DELETE     &           tauir(1:kkrsz,1:kkrsz,1:natom),kkrsz,natom,'TAUFZ')
!DEBUGPRINT

      ! sum contributions to tau00 from friend nodes
!MPIDEBUG      call log_mpi_sync(__FILE__,__LINE__)
       if ( in_place_mpi ) then    ! in_place_mpi is defined in mecca_constants
        if ( associated(mpi_box) ) then
         call mpi_allreduce(MPI_IN_PLACE,                               &
     &                    tau00(1:kkrsz,1:kkrsz,1:nstot),               &
     &                    kkrsz*kkrsz*nstot,                            &
     &       MPI_COMPLEX16,MPI_SUM,mpi_box%thisE_comm,ierr)
        end if
       else
        tauir(1:kkrsz,1:kkrsz,1:nstot)=tau00(1:kkrsz,1:kkrsz,1:nstot)
        if ( associated(mpi_box) ) then
         call mpi_allreduce(tauir(1:kkrsz,1:kkrsz,1:nstot),             &
     &                    tau00(1:kkrsz,1:kkrsz,1:nstot),               &
     &                    kkrsz*kkrsz*nstot,                            &
     &       MPI_COMPLEX16,MPI_SUM,mpi_box%thisE_comm,ierr)
        end if
       end if

       if ( nlrot<=0 ) then
        write(*,*) sname//' : unexpected value of nlrot=',nlrot
        write(*,*) sname//' : kptgrp=',kptgrp(1:ngrp)
!        write(*,*) sname//' : kptindx=',kptindx(1:kptgrp(ngrp+1)-1)
        call fstop(sname//' :: WRONG NUMBER OF ROTATION OPERATIONS')
       end if

       wfac =  dcmplx(rytodu/nlrot,zero)
       do nsub=1,nstot
        do i=1,kkrsz
         call zscal(kkrsz,wfac,tau00(1,i,nsub),1)
        enddo
       enddo

       tcpa = rytodu * tcpa

!       time1 = time2
!       call timel(time2)
!       timeEn = timeEn+(time2-time1)
!DELETE       if(isprs(1).eq.0.or.iscreen.le.0) timeIneq = 0.
!       if(iprint.ge.0) then
!        write(6,'(a,3(1x,f7.2))') sname//' :: K-loop time=',            &
!     &                            real(time2-time1),timeEn
!       end if

       if(allocated(nnclmn)) deallocate(nnclmn,stat=erralloc)
       deallocate(greenKS,stat=erralloc)
       if(erralloc.ne.0) then
        write(6,*) ' NDMTR1=',ndmtr1
        write(6,*) ' ERRALLOC=',erralloc
        call fstop(sname//' :: DEALLOCATION MEMORY PROBLEM')
       end if

      return
      end subroutine tauScrKS
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

      subroutine tauScrRS()
      implicit none
      return
      end subroutine tauScrRS

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

      end subroutine intgrtau
