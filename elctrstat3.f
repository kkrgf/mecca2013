!BOP
!!ROUTINE: elctrstat3
!!INTERFACE:
      subroutine elctrstat3(alat,rslatt,natom,basis,                    &
     &                     itype,nsublat,                               &
     &                     cm,dm,qm,hm3,hm4,                            &
     &                     energy,potential,force,                      &
     &                     iprint,iflag                                 &
     &                    )
!!DESCRIPTION:
! calculates electrostatic energy, potentials, forces
! for a lattice of point charge monopoles+dipoles+quadrupoles
!
!!USES:
      use mecca_interface, only : genvec

!!DO_NOT_PRINT
      implicit none
!!DO_NOT_PRINT

!!ARGUMENTS:
      real(8), intent(in) :: alat
      real(8)             :: rslatt(3,3)
      integer, intent(in) :: natom
      real(8), intent(in) :: basis(3,natom)
      integer, intent(in) :: itype(natom),nsublat

      real(8), intent(in) :: cm(nsublat)
      real(8), intent(in) :: dm(3,nsublat)
      real(8), intent(in) :: qm(3,3,nsublat)
      real(8), intent(in) :: hm3(3,3,3,nsublat)
      real(8), intent(in) :: hm4(3,3,3,3,nsublat)
!
      real(8), intent(out) :: energy
      real(8), intent(out) :: potential(nsublat)
      real(8), intent(out) :: force(3,nsublat)
!
      integer, intent(in) :: iprint,iflag
!!REVISION HISTORY:
! Initial Version - A.S. - 2003
!EOP
!
!BOC
      integer lmaxml
      real*8 kslatt(3,3)
      real*8 omegrs,omegks
      real*8 trsdks,rcut,qcut,eta,eksi,xk2
      integer ndrs,ndks,ndtmp,nrsn,nksn,nksn1
      real*8 zpoint(3)
      integer i,k1,nsub
      real*8 volume
      real*8 emad
      real*8 pot0
!c
      real*8 one,zero,half
      parameter (one=1.d0,zero=0.d0,half=0.5d0)
      real*8 third
      parameter (third=1.d0/3.d0)
      real*8 pi
      parameter (pi=3.14159265358979323844d0)
      real*8 pi4d3,twopi
      parameter (pi4d3=4.d0*pi/3.d0,twopi=pi+pi)
      real*8 oned4pi
      parameter (oned4pi=1.d0/(4.d0*pi))
!c
      real*8 eps
      parameter (eps=1.d-10)
!c
      real*8 timersks              ! optimizational RS/KS parameter
      parameter (timersks=5.0d0)   !       memory (~1) vs time (~5)
!c
      real*8,  allocatable :: xrnkn(:,:)
      real*8,  allocatable :: potmad(:)
      real*8,  allocatable :: fxyz(:,:)
      real(8), allocatable :: tmprs(:),tmpks(:)
!c
      character(*), parameter :: sname='elctrstat3'
!c
      real(8), external :: cellvolm
      integer, external :: indRmesh
!c

!CDEBUG      ztot = zero
!CDEBUG      do i=1,natom
!CDEBUG       nsub = itype(i)
!CDEBUG       ztot = ztot + cm(nsub)
!CDEBUG      end do
!CDEBUG      if(abs(ztot).gt.1.d-6) then
!CDEBUG       write(*,*) ' WARNING: The SYSTEM IS NOT CHARGE NEUTRAL'
!CDEBUG       energy = zero
!CDEBUG       potential(1:nsublat) = zero
!CDEBUG       force(1:3,1:nsublat) = zero
!CDEBUG       return
!CDEBUG      end if

      if(abs(iflag).le.1) then
       energy = zero
       potential(1:nsublat) = zero
       force(1:3,1:nsublat) = zero
       if(nsublat.le.1) then
         if(abs(cm(1)).lt.1.d-6) return
       end if
      end if

      if(iflag.ge.1) then
       rslatt = rslatt/twopi
      end if

!CDEBUG
!CDEBUG       WRITE(*,*) ' CM:',cm(1:nsublat)
!CDEBUG

      if(maxval(abs(qm(1:3,1:3,1:nsublat))).gt.2.d-5) then
       lmaxml = 2
      else if(maxval(abs(dm(1:3,1:nsublat))).gt.1.d-5) then
       lmaxml = 1
      else
       lmaxml = 0
      end if
      omegrs = cellvolm(rslatt,kslatt,1)
      omegks = one/omegrs
      trsdks = timersks*natom
      call mdlngprm(omegrs,trsdks,eta,rcut,qcut)

      if(iprint.ge.1) then
       write(6,*)
       write(6,'('' eta='',f7.3,'' rcut='',f9.3,'' qcut='',f9.3)')      &
     &                    eta,rcut,qcut
      end if
      ndrs = max(27,nint((pi4d3*rcut**3/omegrs)))
      ndks = max(27,nint((pi4d3*qcut**3/omegks)))
      rcut = (ndrs*omegrs/pi4d3)**third
      qcut = (ndks*omegks/pi4d3)**third
!c
!c   just to be safe  (might be not enough for some structures?)
!c
      ndks = ndks+max(150,nint(ndks*0.1d0))
      ndrs = ndrs+max(150,nint(ndrs*0.1d0))
      ndtmp = max(ndrs,ndks)
!c
      if(iprint.ge.1) then
      write(6,*)
      write(6,'('' RS-cell volume (D.U.)='',d15.7)') omegrs
      write(6,'('' KS-cell volume       ='',d15.7)') omegks
      write(6,*)
      write(6,'('' Eta_Mdlng='',f19.14)') eta
      write(6,'('' Rcut(Madelung)='',f7.3                               &
     &            ,4x,'' Nd(RS)='',i7)') rcut,ndrs
      write(6,'('' Kcut(Madelung)='',f7.3                               &
     &            ,4x,'' Nd(KS)='',i7)') qcut,ndks
      write(6,*)
      end if

      allocate(potmad(1:nsublat))
      allocate(fxyz(1:3,1:nsublat))

      zpoint(1:3) = zero

      call genvec(rslatt,rcut,zpoint,1,                                 &
     &              tmprs,ndrs,nrsn,iprint)
!     &              xrnkn(1,1),ndrs,nrsn,iprint)
!      call transdp(3,nrsn,ndrs,xrnkn(1,1))

      zpoint(1:3) = zero
      call genvec(kslatt,qcut,zpoint,1,                                 &
     &                  tmpks,ndks,nksn,iprint)
!     &                  xrnkn(1,ndrs+1),ndks,nksn,iprint)
!      call transdp(3,nksn,ndks,xrnkn(1,ndrs+1))

      allocate(xrnkn(1:3,1:nrsn+nksn))

!      xrnkn(1:3,1:nrsn) = transpose(reshape(tmprs(1:3*nrsn),[nrsn,3]))
      do i=1,nrsn
       xrnkn(1,i) = tmprs(i)
       xrnkn(2,i) = tmprs(i+ndrs)
       xrnkn(3,i) = tmprs(i+2*ndrs)
      end do
      ndrs = nrsn
      if ( allocated(tmprs) ) deallocate(tmprs)

!      xrnkn(1:3,nrsn+1:nrsn+nksn) =                                     &
!     &                      transpose(reshape(tmpks(1:3*nksn),[nksn,3]))
      do i=nrsn+1,nrsn+nksn
       k1 = i-nrsn
       xrnkn(1,i) = tmpks(k1)
       xrnkn(2,i) = tmpks(k1+ndks)
       xrnkn(3,i) = tmpks(k1+2*ndks)
      end do
      ndks = nksn
      if ( allocated(tmpks) ) deallocate(tmpks)
!c
!c
!c   to eliminate k=0
!c
      k1 = 1
      zpoint(1:3) = xrnkn(1:3,ndrs+1)
      do i=1,nksn
      xk2 = maxval(abs(xrnkn(1:3,ndrs+i)))
      if(abs(xk2).lt.eps) then
       xrnkn(1:3,ndrs+1) = zero
       xrnkn(1:3,ndrs+i) = zpoint(1:3)
       k1 = 2
       exit
      end if
      end do
      nksn1 = nksn+1-k1
!c
      if(iprint.ge.1) then
      write(6,'('' Nmdlng(RS)='',i5,                                    &
     &            '' Nmdlng(KS)='',i5)') nrsn,nksn
      write(6,*)
      end if

      eksi = half*sqrt(eta)
      volume = omegrs * alat**3
      rcut = rcut*alat
      rslatt = rslatt*alat
!c      basis(1:3,1:natom) = basis(1:3,1:natom)*alat
      xrnkn(1:3,1:nrsn) = xrnkn(1:3,1:nrsn)*alat         !  RS-vectors
      xrnkn(1:3,ndrs+k1:ndrs+nksn) =                                    &
     &        xrnkn(1:3,ndrs+k1:ndrs+nksn)*(twopi/alat)  !  KS-vectors

!c==============================================================
!c
!c   [energy] = Ry, [distance] = bohr (0.529177 Angstroem)
!c
      call KSmultpew(lmaxml,natom,nsublat,itype,                        &
     &                  cm,dm,qm,basis,eksi,alat,volume,                &
     &             nksn1,xrnkn(1,ndrs+k1),emad,potmad,fxyz)
!CDEBUG
!CDEBUG      do nsub=1,nsublat
!CDEBUG         write(70+nsub,'(i5,1x,f7.4,1x,f15.9)')
!CDEBUG     *           0,0.d0,potmad(nsub)
!CDEBUG      end do
!CDEBUG

      if(iprint.ge.1) then
       write(6,*)
       write(6,'('' KS-emad='',f15.9,'' KS-emad*alat='',f15.9)')        &
     &                                               emad,emad*alat
       write(6,*)
       do nsub=1,nsublat
      write(6,'('' iat='',i5,2x,1x,f15.9,'' KS-pot.'')')                &
     &                                          nsub,potmad(nsub)
       end do
       write(6,*)
       do nsub=1,nsublat
      write(6,'('' iat='',i5,2x,3(1x,f15.9),'' KS-force'')')            &
     &                                          nsub,fxyz(1:3,nsub)
       end do
      end if

      energy = energy + emad
      potential(1:nsublat) = potential(1:nsublat) +                     &
     &                       potmad(1:nsublat)
      force(1:3,1:nsublat) = force(1:3,1:nsublat) +                     &
     &                       fxyz(1:3,1:nsublat)

      call RSmultpew(lmaxml,natom,nsublat,itype,                        &
     &                  cm,dm,qm,basis,eksi,alat,rcut,                  &
     &                nrsn,xrnkn(1,1),emad,potmad,fxyz)

!CDEBUG
!CDEBUG      do nsub=1,nsublat
!CDEBUG         write(80+nsub,'(i5,1x,f7.4,1x,f15.9)')
!CDEBUG     *           0,0.d0,potmad(nsub)
!CDEBUG      end do
!CDEBUG
      if(iprint.ge.1) then
       write(6,*)
       write(6,'('' RS-emad='',f15.9,'' RS-emad*alat='',f15.9)')        &
     &                                          emad,emad*alat
       write(6,*)
       do nsub=1,nsublat
      write(6,'('' nsub='',i5,2x,1x,f15.9,10x,''RS-pot.'')')            &
     &                                          nsub,potmad(nsub)
       end do
       write(6,*)
       do nsub=1,nsublat
      write(6,'('' nsub='',i5,2x,3(1x,f15.9),1x,''RS-force'')')         &
     &                                          nsub,fxyz(1:3,nsub)
       end do
      end if
!c
      energy = energy + emad
      potential(1:nsublat) = potential(1:nsublat) +                     &
     &                       potmad(1:nsublat)

      force(1:3,1:nsublat) = force(1:3,1:nsublat) +                     &
     &                       fxyz(1:3,1:nsublat)
!c
!c
!c==============================================================


!c%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
      ndrs = 512                              ! i.e. rcut ~ 8*Rws
      rcut = (ndrs*omegrs/pi4d3)**third
!      ndrs = ndrs+max(150,nint(ndrs*0.1d0))
!      if(ndrs.gt.ndtmp) then
!       deallocate(xrnkn)
!       allocate(xrnkn(1:3,ndrs))
!      end if
      zpoint(1:3) = zero
      rslatt = rslatt/alat

      call genvec(rslatt,rcut,zpoint,1,                                 &
     &              tmprs,ndrs,nrsn,iprint)
      if ( allocated(xrnkn) ) deallocate(xrnkn)
      if ( .not.allocated(xrnkn) ) then
        allocate(xrnkn(3,nrsn))
      end if    
!      xrnkn(1:3,1:nrsn) = transpose(reshape(tmprs(1:3*nrsn),[nrsn,3]))
      do i=1,nrsn
       xrnkn(1,i) = tmprs(i)
       xrnkn(2,i) = tmprs(i+ndrs)
       xrnkn(3,i) = tmprs(i+2*ndrs)
      end do
      if ( allocated(tmprs) ) deallocate(tmprs)
      ndrs = nrsn
!      call transdp(3,nrsn,ndrs,xrnkn(1,1))

      rcut = rcut*alat
      rslatt = rslatt*alat
      xrnkn(1:3,1:nrsn) = xrnkn(1:3,1:nrsn)*alat         !  RS-vectors

      call addRSmltpl(natom,nsublat,itype,                              &
     &                cm,dm,hm3,hm4,basis,alat,rcut,                    &
     &                nrsn,xrnkn,emad,potmad,fxyz)

      if(iprint.ge.1) then
       write(6,*)
       write(6,'('' add-RS='',f15.9,'' add-RS*alat='',f15.9)')          &
     &                                          emad,emad*alat
       write(6,*)
       do nsub=1,nsublat
      write(6,'('' nsub='',i5,2x,1x,f15.9,10x,''add-RS-pot.'')')        &
     &                                          nsub,potmad(nsub)
       end do
       write(6,*)
!c       do nsub=1,nsublat
!c        write(6,'('' nsub='',i5,2x,3(1x,f15.9),1x,''RS-force'')')
!c     *                                          nsub,fxyz(1:3,nsub)
!c       end do
      end if
!c
      energy = energy + emad
      potential(1:nsublat) = potential(1:nsublat) +                     &
     &                       potmad(1:nsublat)

      force(1:3,1:nsublat) = force(1:3,1:nsublat) +                     &
     &                       fxyz(1:3,1:nsublat)

!c%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

      deallocate(potmad,fxyz,xrnkn)
      if(iprint.ge.0) then
       if(iprint.ge.1) then
         write(6,*)
         write(6,'('' Emad='',f15.9,''    Emad*alat='',f15.9)')         &
     &                                    energy,energy*alat/natom
       end if
!c       write(6,*)
!c       do nsub=1,nsublat
!c        write(6,'('' nsub='',i5,2x,1x,f15.9,10x,''ES-Potential'')')
!c     *                                          nsub,potential(nsub)
!c       end do
       write(6,*)
       do nsub=1,nsublat
       if ( maxval(abs(force(1:3,nsub))) > eps ) then
      write(6,'('' nsub='',i5,2x,3(1x,f15.9),5x,''ES-Force'')')         &
     &                                          nsub,force(1:3,nsub)
       end if
       end do
      end if

      if(iflag.eq.1) then
       rslatt = rslatt*twopi
      end if

      rslatt = rslatt/alat

      return

!EOC
      contains
!c
!BOP
!!IROUTINE: mdlngprm
!!INTERFACE:
      subroutine mdlngprm(omegrs,trsdks,eta,rcut,qcut)
!!REMARKS:
! private procedure of subroutine elctrstat3
!EOP
!
!BOC
      implicit none
      real*8 omegrs,trsdks,eta,rcut,qcut
      real*8 third
      parameter (third=1.d0/3.d0)
      real(8) :: r00,eksi

!c  rsmax,ksmax -- accuracy parameters
!c
      real*8 rsmax               !         erfc(rsmax)/erfc(1)   = 0.d0
      parameter (rsmax=5.8d0)
      real*8 ksmax               ! exp(-ksmax^2)/exp(-1)/ksmax^2 = 0.d0
      parameter (ksmax=5.5d0)

      r00 = omegrs**third

      eksi = sqrt(ksmax/rsmax)*r00/trsdks**(1.d0/6.d0)

      rcut = eksi*rsmax
      qcut = ksmax/(2.d0*eksi)
      eta = 1.d0/(4.d0*eksi**2)

      return
!EOC
      end subroutine mdlngprm
!c
!BOP
!!IROUTINE: transdp
!!INTERFACE:
      subroutine transdp(m,n,ndim,A)
!!REMARKS:
! private procedure of subroutine elctrstat3
!EOP
!
!BOC
      implicit none
      integer m,n,ndim
      real(8), intent(inout) :: A(ndim,*)
      A(1:m,1:n) = transpose( A(1:n,1:m) )
      return
!EOC
      end subroutine transdp
!c
!cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
!c
!BOP
!!IROUTINE: KSmultpew
!!INTERFACE:
      subroutine KSmultpew(lmaxml,natom,nsublat,itype,                  &
     &                  cm,dm,qm,xyz,eksi,alat,volume,                  &
     &                  nksn,xkn,emad,potmad,fxyz)
!!REMARKS:
! private procedure of subroutine elctrstat3
!EOP
!c*********************************************************************
!c     evaluation of fourier component of ewald sum in a point
!c       lmaxml: multipole lmax,
!c             0 -- monopoles only
!c             1 -- monopole+dipole
!c             2 -- monopole+dipole+quadrupole
!c       natom -- number of sites in the cell
!c       nsublat -- number of inequivalent sites (sublattices)
!c       itype(1:natom) -- type of atom (sublattice number)
!c       cm(1:natom) -- monopole, i.e. Integral_over_sphere{Charge_Density}
!c       dm(3,1:natom) -- dipole,   Integral_over_sphere{Charge_Density*r_mu}
!c                                                              r_mu = (x,y,z)
!c       qm(9,1:natom) -- quadrupole, Integral_over_sphere{Charge_Density*r_mu*r_nu}
!c       xyz[1:natom]  -- basis vectors
!c       eksi          -- Ewald's parameter  (4*eksi^2 = eta (in MADSUM))
!c       alat          -- lattice constant
!c       xkn[1:3,1:nrsn] -- vectors for k-space summation; xkn*xyz == 2*pi*k*r
!c                            (xkn .NE. 0)
!c       emad -- Madelung energy
!c       potmad[1:nsublat] -- potential
!c       fxyz[1:3,1:nsublat] -- forces (x,y,z)
!c
!c  See, W. Smith, CCP5 Newsletter, No.46, p.18-46
!c
!c*********************************************************************
!
!BOC
      implicit none

      integer lmaxml,natom
      integer itype(natom)
      integer nsublat
      real*8  cm(nsublat)
      real*8  dm(3,nsublat)
      real*8  qm(9,nsublat)
      real*8  xyz(:,:)  ! (3,natom)
      real*8  eksi,alphad
      real*8  alat
      integer nksn
      real*8  xkn(3,nksn)
!c
      real*8 emad,potmad(nsublat),fxyz(3,nsublat)

      real*8 pi,twopi
      parameter (pi=3.14159265358979323844d0,twopi=pi+pi)
      real*8 zero,one,two,four
      parameter (zero=0.d0,one=1.d0,two=2.d0,four=4.d0)
      complex*16 czero
      parameter (czero=(0.d0,0.d0))

      integer k,iat,nsub
      real*8 volume,ralpha,sialpha
      real*8 tmpv,xksq,ak,absfsum,absfsum2
      real*8 de0,phis,trqm,ddqm,phiqd1,phiqd2,pots
      complex*16 akfijexp
      complex*16 fsum,qr

      integer, allocatable :: numbsbl(:)
      real*8, allocatable :: dk(:),qkk(:)
      complex*16, allocatable :: fi(:),riexp(:)

      real(8), external :: cellvolm,ddproduct

      emad = zero
      potmad(1:nsublat) = zero
      fxyz(1:3,1:nsublat) = zero
      alphad = eksi*twopi/alat

      if(alphad.le.zero.or.lmaxml.lt.0.or.lmaxml.gt.2) return

      ralpha = -one/(four*alphad**2)
      sialpha = alphad/(4.d0*pi*dsqrt(pi))

      allocate(dk(1:nsublat),qkk(1:nsublat),                            &
     &         fi(1:natom),riexp(1:natom),numbsbl(1:nsublat))

      dk(1:nsublat) = zero
      qkk(1:nsublat) = zero

      do k=1,nksn

       if(lmaxml.ge.1) then
      do nsub=1,nsublat
       dk(nsub) = dot_product(dm(1:3,nsub),xkn(1:3,k))
      end do
      if(lmaxml.ge.2) then
       do nsub=1,nsublat
        qkk(nsub) = ddproduct(qm(1:9,nsub),xkn(1:3,k),xkn(1:3,k))
       end do
      end if
       end if

       fsum = czero
       do iat=1,natom
      nsub = itype(iat)
      fi(iat) =                                                         &
     &        dcmplx(cm(nsub)-qkk(nsub),-dk(nsub))
      qr = dcmplx(zero,-dot_product(xkn(1:3,k),xyz(1:3,iat)))
      riexp(iat) = exp(qr)
      fsum = fsum + fi(iat)*riexp(iat)
       end do

       xksq = dot_product(xkn(1:3,k),xkn(1:3,k))

       ak = exp(xksq*ralpha)/xksq           ! ralpha = -one/(four*alphad**2)

       absfsum2 = dreal(fsum*conjg(fsum))

       emad = emad + ak*absfsum2

       do iat = 1,natom
      nsub = itype(iat)
      akfijexp = (0.d0,1.d0)*conjg(fi(iat)*riexp(iat))*fsum
      fxyz(1:3,nsub)=fxyz(1:3,nsub)+xkn(1:3,k)*ak*dreal(akfijexp)
      potmad(nsub) = potmad(nsub) + ak*dreal(conjg(riexp(iat))*fsum)
       end do

      end do                            ! k=1,nksn

      emad = 0.5d0*emad/volume
      fxyz(1:3,1:nsublat) = -fxyz(1:3,1:nsublat)/volume
      potmad(1:nsublat) = potmad(1:nsublat)/volume

      de0 = zero
      do iat=1,natom
       nsub = itype(iat)

       phis = cm(nsub)**2
       pots = cm(nsub)

       if(lmaxml.ge.1) then
       phis = phis + (two*alphad**2)/3.d0*                              &
     &          dot_product(dm(1:3,nsub),dm(1:3,nsub))
!c     *          (dm(1,nsub)**2+dm(2,nsub)**2+dm(3,nsub)**2)

      if(lmaxml.ge.2) then
       trqm = qm(1,nsub)+qm(5,nsub)+qm(9,nsub)
       phiqd1 = cm(nsub)*trqm
       ddqm = dot_product(qm(1:9,nsub),qm(1:9,nsub))
       phiqd2 = two*ddqm+trqm**2
       phis = phis + (two*alphad**2)*                                   &
     &                 (two*phiqd1 +                                    &
     &                         (two*alphad**2)/5.d0*phiqd2)
       pots = pots - (two*alphad**2)/3.d0*trqm
      end if
       end if

       de0 = de0 - sialpha*phis
!CDEBUG
!CDEBUG        write(70+nsub,*) '# pots=',potmad(nsub),-(2.d0*sialpha)*pots
!CDEBUG
       potmad(nsub) = potmad(nsub)-(2.d0*sialpha)*pots
      end do
!c
!c  de0 -  self-interaction term
!c

      emad = emad + de0

      emad = emad*(four*pi)        ! e.m.u.
      emad = emad*two              ! Ry

      potmad(1:nsublat) = potmad(1:nsublat)*((four*pi)*two)
      fxyz(1:3,1:nsublat) = fxyz(1:3,1:nsublat)*((four*pi)*two)

!ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
!c
!c   to be compatible with previous versions, it is necessary to add
!c   -8pi^2/volr/eta (eta=4pi*eksi^2) to Madelung matrix
!c
      de0 = zero
      do iat = 1,natom
       nsub = itype(iat)
       de0 = de0+cm(nsub)*cm(nsub)
       do k=iat+1,natom
      de0 = de0 + two*cm(nsub)*cm(itype(k))
       end do
      end do
      emad = emad - de0*alat*alat/(four*pi*volume*eksi*eksi)

      de0 = zero
      do iat = 1,natom
       de0 = de0+cm(itype(iat))
      end do
      de0 = two*de0
      do iat = 1,natom
       nsub = itype(iat)
       potmad(nsub) = potmad(nsub) -                                    &
     &              de0*alat*alat/(four*pi*volume*eksi*eksi)
      end do
!c
!c
!ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc

      if(nsublat.ne.natom) then
       numbsbl(1:nsublat) = 0
       do iat=1,natom
      nsub = itype(iat)
      numbsbl(nsub) = numbsbl(nsub) + 1
       end do
       do nsub = 1,nsublat
      potmad(nsub) = potmad(nsub)/numbsbl(nsub)
      fxyz(1:3,nsub) = fxyz(1:3,nsub)/numbsbl(nsub)
       end do
      end if

      deallocate(dk,qkk,fi,riexp,numbsbl)

      return
!EOC
      end subroutine KSmultpew
!c
!cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
!c
!BOP
!!IROUTINE: RSmultpew
!!INTERFACE:
      subroutine RSmultpew(lmaxml,natom,nsublat,itype,                  &
     &                  cm,dm,qm,xyz,eksi,alat,rcut,                    &
     &                  nrsn,xrn,emad,potmad,fxyz)
!!REMARKS:
! private procedure of subroutine elctrstat3
!EOP
!c*********************************************************************
!c     evaluation of real-space component of ewald sum in a point
!c       lmaxml: multipole lmax,
!c             0 -- monopoles only
!c             1 -- monopole+dipole
!c             2 -- monopole+dipole+quadrupole
!c       natom -- number of sites in the cell
!c       nsublat -- number of inequivalent sites (sublattices)
!c       itype(1:natom) -- type of atom (sublattice number)
!c       cm(1:natom) -- monopole, i.e. Integral_over_sphere{Charge_Density}
!c       dm(3,1:natom) -- dipole,   Integral_over_sphere{Charge_Density*r_mu}
!c                                                              r_mu = (x,y,z)
!c       qm(9,1:natom) -- quadrupole, Integral_over_sphere{Charge_Density*r_mu*r_nu}
!c       xyz[1:natom]  -- basis vectors
!c       eksi          -- Ewald's parameter
!c       alat          -- lattice constant
!c       xrn[1:3,1:nrsn] -- vectors for r-space summation;
!c       emad -- Madelung energy
!c       potmad[1:nsublat] -- potential
!c       fxyz[1:3,1:nsublat] -- forces (x,y,z)   !!! for each SITE (not only for inequivalent)
!c
!c  See, W. Smith, CCP5 Newsletter, No.46, p.18-46
!c
!c*********************************************************************
!
!BOC
      implicit none

      integer lmaxml,natom
      integer itype(natom)
      integer nsublat
      real*8  cm(nsublat)
      real*8  dm(3,nsublat)
      real*8  qm(9,nsublat)
      real*8  xyz(:,:)  ! (3,natom)
      real*8  eksi,alphad
      real*8  alat,rcut
      integer nrsn
      real*8  xrn(3,nrsn)
!c
      real*8 emad,potmad(nsublat),fxyz(3,nsublat)

      real*8 pi,oned4pi
      parameter (pi=3.14159265358979323844d0,oned4pi=0.25d0/pi)
      real*8 erfarg
      parameter (erfarg=6.d0)
      real*8 zero,one,two,four
      parameter (zero=0.d0,one=1.d0,two=2.d0,four=4.d0)

      integer k,iat,jat,nsi,nsj
      integer klim,l

      real*8 rcut2
      real*8 ralpi,alsq2,alphar,exp2a
      real*8 tmpv(9),aij(3),xji(3),rji,r2,r2i,fxyzi(3)
      real*8 de0,phis,trqmi,trqmj,ddqm,phiqd1,phiqd2
      real*8 dxi,dxj,qxxi,qxxj
      real*8 g0,g1,g2,g3,g4
      real*8 g1ji,g2ji,g3ji,g4ji
      real*8 gf0(1:3),gf1(3),gf2(3),gf3(3),gf4(3)
      real*8 gf1ji(3),gf2ji(3),gf3ji(3),gf4ji(3)
      real*8 qmqm(3,3)

      integer, allocatable :: numbsbl(:)
      real*8, allocatable :: bn(:)
      real*8, allocatable :: dk(:),qkk(:)
      real(8), external :: cellvolm,ddproduct
      real(8), external :: erfc

      emad = zero
      potmad(1:nsublat) = zero
      fxyz(1:3,1:nsublat) = zero
      gf0(1:3) = zero
      if(eksi.le.zero.or.lmaxml.lt.0.or.lmaxml.gt.2) return

      alphad = eksi*(two*pi)/alat
      ralpi = one/(alphad*sqrt(pi))
      alsq2 = 2.d0*alphad**2
      rcut2 = min(rcut**2,(erfarg/alphad)**2)

      klim = 2*lmaxml+1

      allocate(                                                         &
     &         dk(1:nsublat),qkk(1:nsublat)                             &
     &        ,bn(0:klim),numbsbl(1:nsublat)                            &
     &         )

      do iat=1,natom
       nsi = itype(iat)
       fxyzi(1:3) = zero
       do jat=1,natom
       nsj = itype(jat)
       g0 = cm(nsi)*cm(nsj)

       if(lmaxml.ge.1) then
        g1ji = dot_product(dm(1:3,nsi),dm(1:3,nsj))
        gf1ji(1:3)= cm(nsi)*dm(1:3,nsj) - cm(nsj)*dm(1:3,nsi)

        if(lmaxml.ge.2) then
         trqmi= qm(1,nsi)+qm(5,nsi)+qm(9,nsi)
         trqmj = qm(1,nsj)+qm(5,nsj)+qm(9,nsj)
         g1ji = g1ji - ( cm(nsj)*trqmi + cm(nsi)*trqmj )
!c
!c   g2ji = 2 Q:Q + (Q:I)(Q:I)
!c
         g2ji = 2.d0*dot_product(qm(1:9,nsi),qm(1:9,nsj)) +             &
     &                  trqmi*trqmj
         call dgemv('N',3,3,two,qm(1,nsj),3,dm(1,nsi),1,                &
     &                                            zero,gf2ji,1)
         call dgemv('N',3,3,-two,qm(1,nsi),3,dm(1,nsj),1,               &
     &                                             one,gf2ji,1)
         gf2ji(1:3) = gf2ji(1:3) + trqmj*dm(1:3,nsi)-trqmi*dm(1:3,nsj)
         call dgemm('N','N',3,3,3,four,qm(1,nsi),3,qm(1,nsj),3,         &
     &                                             zero,qmqm,3)
         call dgemm('N','N',3,3,3,four,qm(1,nsj),3,qm(1,nsi),3,         &
     &                                             one,qmqm,3)
        end if
       end if

       aij(1:3) = xyz(1:3,jat) - xyz(1:3,iat)

       do k=1,nrsn
        xji(1:3) = aij(1:3) + xrn(1:3,k)      ! Rj-Ri
        r2 = dot_product(xji(1:3),xji(1:3))
        if(r2.gt.rcut2) then
         CYCLE
        else if(r2.lt.(1.d-10*alat*alat)) then
         if(iat.ne.jat) then
          write(*,'(a,i6,3g20.12)') ' iat=',iat,xyz(1:3,iat)
          write(*,'(a,i6,3g20.12)') ' jat=',jat,xyz(1:3,jat)
          write(*,'(a,i6,3g20.12)') '   k=', k,xrn(1:3,k)
          call fstop(' WRONG STRUCTURE ?')
         end if
         CYCLE
        end if
        rji = sqrt(r2)
        r2i = one/r2
        alphar = alphad*rji
        bn(0) = erfc(alphar)/rji
        exp2a = ralpi*exp(-alphar*alphar)

        do l=1,klim
         exp2a = alsq2*exp2a
         bn(l) = r2i*((l+l-1)*bn(l-1)+exp2a)
        end do

        if(lmaxml.ge.1) then
!c
!c     gf1ji= cm(nsi)*dm(*,nsj) - cm(nsj)*dm(*,nsi)
!c
         g1 = g1ji - dot_product(gf1ji(1:3),xji(1:3))
         gf1(1:3) = gf1ji(1:3)

         dxi =  dot_product(dm(1:3,nsi),xji(1:3))
         dxj =  dot_product(dm(1:3,nsj),xji(1:3))
         g2 = -dxi*dxj
         gf2(1:3) = dxj*dm(1:3,nsi) + dxi*dm(1:3,nsj)
!c...................................................................
         if(lmaxml.ge.2) then
          tmpv(1:9) = cm(nsj)*qm(1:9,nsi) + cm(nsi)*qm(1:9,nsj)
          qxxi = ddproduct(qm(1,nsi),xji,xji)
          qxxj = ddproduct(qm(1,nsj),xji,xji)

          g2 = g2 + g2ji + cm(nsj)*qxxi + cm(nsi)*qxxj
          g2 = g2 + two*(ddproduct(qm(1,nsi),dm(1,nsj),xji) -           &
     &                     ddproduct(qm(1,nsj),dm(1,nsi),xji))
          g2 = g2 + dxj*trqmi - dxi*trqmj

          gf2(1:3) = gf2(1:3) + gf2ji(1:3)
          call dgemv('N',3,3,-two,tmpv(1),3,xji,1,one,gf2,1)
!c
!c   gf2 = gf2 - 2*(Cj*Qi+Ci*Qj)*xji
!c

          g3 = (dxi-trqmi)*qxxj-(dxj+trqmj)*qxxi
          tmpv(1) = dot_product(qm(1:3,nsj),xji(1:3))
          tmpv(2) = dot_product(qm(4:6,nsj),xji(1:3))
          tmpv(3) = dot_product(qm(7:9,nsj),xji(1:3))
!c
!c   tmpv(1:3) = xji_alpha*qmj_{alpha,beta}
!c
          gf3(1:3) = two*(-dxi+trqmi)*tmpv(1:3)
          gf4(1:3) =-two*qxxi*tmpv(1:3)

          tmpv(1) = dot_product(qm(1:3,nsi),xji(1:3))
          tmpv(2) = dot_product(qm(4:6,nsi),xji(1:3))
          tmpv(3) = dot_product(qm(7:9,nsi),xji(1:3))
!c
!c   tmpv(1:3) = xji_alpha*qmi_{alpha,beta}
!c
          gf3(1:3) = gf3(1:3) + two*(+dxj+trqmj)*tmpv(1:3)
          gf4(1:3) = gf4(1:3) - two*qxxj*tmpv(1:3)

          tmpv(4) = dot_product(tmpv(1:3),qm(1:3,nsj))
          tmpv(5) = dot_product(tmpv(1:3),qm(4:6,nsj))
          tmpv(6) = dot_product(tmpv(1:3),qm(7:9,nsj))
!c
!c   tmpv(4:6) = xji_alpha*qmi_{alpha,beta}*qmj_{beta,gamma}
!c
          g3 = g3 - 4.d0*dot_product(tmpv(4:6),xji(1:3))

          call dgemv('N',3,3,one,qmqm,3,xji,1,one,gf3,1)
          gf3(1:3) = gf3(1:3)  + qxxi*dm(1:3,nsj)-qxxj*dm(1:3,nsi)

          g4 = qxxi*qxxj

         end if                               ! lmaxml > 1
!c...................................................................
        end if                                ! lmaxml > 0

        emad = emad + g0*bn(0)
        fxyzi(1:3) = fxyzi(1:3) + g0*bn(1)*xji(1:3)
!c          fxyzi(1:3) = fxyzi(1:3) + bn(0)*gf0(1:3)       !  gf0 = zero
        potmad(nsi) = potmad(nsi) +                                     &
     &                        cm(nsj)*bn(0)

        if(lmaxml.eq.1) then
         emad = emad +g1*bn(1)+g2*bn(2)
         fxyzi(1:3) = fxyzi(1:3) + (g1*bn(2)+g2*bn(3))*xji(1:3)
         fxyzi(1:3) = fxyzi(1:3) + bn(1)*gf1(1:3) + bn(2)*gf2(1:3)
         potmad(nsi) = potmad(nsi) -                                    &
     &                        dxj*bn(1)
        else if(lmaxml.eq.2) then
         emad = emad +g1*bn(1)+g2*bn(2)+g3*bn(3)+g4*bn(4)
         fxyzi(1:3) = fxyzi(1:3) + (g1*bn(2)+g2*bn(3) +                 &
     &                              g3*bn(4)+g4*bn(5)) * xji(1:3)
         fxyzi(1:3) = fxyzi(1:3) + bn(1)*gf1(1:3)+bn(2)*gf2(1:3)+       &
     &                             bn(3)*gf3(1:3)+bn(4)*gf4(1:3)
         potmad(nsi) = potmad(nsi) -                                    &
     &                        (dxj+trqmj)*bn(1)                         &
     &                      + qxxj*bn(2)
        end if

       end do                   ! k=1,nrsn

       end do                   ! jat=1,natom
       fxyz(1:3,nsi) = fxyz(1:3,nsi)-oned4pi*fxyzi(1:3)
      end do                   ! iat=1,natom

      emad = 0.5d0*oned4pi*emad
      potmad(1:nsublat) = oned4pi*potmad(1:nsublat)

      emad = emad*(four*pi)        ! e.m.u.
      emad = emad*two              ! Ry

      potmad(1:nsublat) = potmad(1:nsublat)*((four*pi)*two)
      fxyz(1:3,1:nsublat) = fxyz(1:3,1:nsublat)*((four*pi)*two)



      if(nsublat.ne.natom) then
       numbsbl(1:nsublat) = 0
       do iat=1,natom
      nsi = itype(iat)
      numbsbl(nsi) = numbsbl(nsi) + 1
       end do
       do nsi = 1,nsublat
      potmad(nsi) = potmad(nsi)/numbsbl(nsi)
      fxyz(1:3,nsi) = fxyz(1:3,nsi)/numbsbl(nsi)
       end do
      end if

      deallocate(dk,qkk,bn,numbsbl)

      return
!EOC
      end subroutine RSmultpew
!
!BOP
!!IROUTINE: addRSmltpl
!!INTERFACE:
      subroutine addRSmltpl(natom,nsublat,itype,                        &
     &                  cm,dm,hm3,hm4,xyz,alat,rcut,                    &
     &                  nrsn,xrn,emad,potmad,fxyz)
!!REMARKS:
! private procedure of subroutine elctrstat3
!EOP
!c*********************************************************************
!c     real-space evaluation of multipoles contributions
!c     (energy: only m-hm3, m-hm4, d-hm3, potential:only m-hm3, m-hm4)
!c       cm -- monopoles
!c       dm -- dipoles
!c       hm3 -- octopoles
!c       hm4 -- hexadecapoles
!c       natom -- number of sites in the cell
!c       nsublat -- number of inequivalent sites (sublattices)
!c       itype(1:natom) -- type of atom (sublattice number)
!c       cm(1:natom) -- monopole, i.e. Integral_over_VP{Charge_Density}
!c       dm(3,1:natom) -- dipole,   Integral_over_VP{Charge_Density*r_a}
!c                                                              r_a = (x,y,z)
!c       hm3(3,3,3,1:natom) -- octopoles,
!c   Integral_over_VP{Charge_Density*0.5*(r_a*r_b*r_c-
!c                r^2*(r_a*delta_bc+r_a*delta_bc+r_a*delta_bc)}
!c       hm4(3,3,3,3,1:natom) -- hexadecapole,
!c   Integral_over_sphere{Charge_Density*0.125*(35*r_a*r_b*r_c*r_d-
!c            5*r^2(r_b*r_c*delta_ad+r_a*r_c*delta_bd+r_a*r_b*delta_cd+
!c                  r_a*r_d*delta_bc+r_b*r_d*delta_ac+r_c*r_d*delta_ab+
!c         +r^4*(delta_ad*delta_bc+delta_ac*delta_bd+delta_ab*delta_cd))}
!c
!c       xyz[1:natom]  -- basis vectors
!c       alat          -- lattice constant
!c       xrn[1:3,1:nrsn] -- vectors for r-space summation;
!c       emad -- energy
!c       potmad[1:nsublat] -- potential
!c       fxyz[1:3,1:nsublat] -- forces (x,y,z)   !!! for each SITE (not only for inequivalent)
!c
!c*********************************************************************
!
!BOC
      implicit none

      integer natom
      integer itype(natom)
      integer nsublat
      real*8  cm(nsublat)
      real*8  dm(3,nsublat)
      real*8  hm3(3,3,3,nsublat)
      real*8  hm4(3,3,3,3,nsublat)
      real*8  xyz(:,:) !! (3,natom)
      real*8  alat,rcut
      integer nrsn
      real*8  xrn(3,nrsn)
!c
      real*8 emad,potmad(nsublat),fxyz(3,nsublat)

      real*8 pi,oned4pi
      parameter (pi=3.14159265358979323844d0,oned4pi=0.25d0/pi)
      real*8 zero,one,two,four
      parameter (zero=0.d0,one=1.d0,two=2.d0,four=4.d0)

      integer k,iat,jat,nsi,nsj
      integer ia,ib,ic,id

      real*8 rcut2
      real*8 aij(3),xji(3),rji,r2,r2i,r7i,r9i
      real*8 pti,ptj,dui,duj,dmci,dmcj,dpl2ij,dpl2ji,dpl1i,dpl1j
      real*8 ra,rb,rc,rd

      integer numbsbl(nsublat)
!c      integer, allocatable :: numbsbl(:)

      emad = zero
      potmad(1:nsublat) = zero
      fxyz(1:3,1:nsublat) = zero

      rcut2 = rcut**2

      do iat=1,natom
       nsi = itype(iat)
!c       fxyzi(1:3) = zero
       do jat=1,natom
       nsj = itype(jat)
       aij(1:3) = xyz(1:3,jat) - xyz(1:3,iat)

       do k=1,nrsn
        xji(1:3) = aij(1:3) + xrn(1:3,k)      ! Rj-Ri
        r2 = dot_product(xji(1:3),xji(1:3))
        if(r2.gt.rcut2) then
         CYCLE
        else if(r2.lt.(1.d-10*alat*alat)) then
         if(iat.ne.jat) then
          write(*,'(a,i6,3g20.12)') ' iat=',iat,xyz(1:3,iat)
          write(*,'(a,i6,3g20.12)') ' jat=',jat,xyz(1:3,jat)
          write(*,'(a,i6,3g20.12)') '   k=', k,xrn(1:3,k)
          call fstop(' WRONG STRUCTURE?')
         end if
         CYCLE
        end if
        rji = sqrt(r2)
        r2i = one/r2
        r7i = r2i**3/rji
        r9i = r2i*r7i

        if(maxval(abs(hm3(1:3,1:3,1:3,nsj))).gt.1.d-6) then

         pti = zero
         ptj = zero
         dpl2ij = zero
         dpl2ji = zero
         do ic=1,3
          rc = xji(ic)
          dmci = 3.d0*dm(ic,nsi)
          dmcj = 3.d0*dm(ic,nsj)
          do ib=1,3
           rb = xji(ib)
           do ia=1,3
            ra = xji(ia)
            duj = hm3(ia,ib,ic,nsj)*ra*(rb*(rc*r7i))
            dui = hm3(ia,ib,ic,nsi)*ra*(rb*(rc*r7i))
            pti = pti + dui
            ptj = ptj + duj
            dpl2ij = dpl2ij + hm3(ia,ib,ic,nsi)*ra*(rb*(dmcj*r7i))
            dpl2ji = dpl2ji + hm3(ia,ib,ic,nsj)*ra*(rb*(dmci*r7i))
           end do
          end do
         end do

         dpl1i = 7.d0*r2i*dot_product(xji(1:3),dm(1:3,nsi))
         dpl1j = 7.d0*r2i*dot_product(xji(1:3),dm(1:3,nsj))

         emad = emad + cm(nsj)*pti - cm(nsi)*ptj              ! m-hm3
!c
         emad = emad - (dpl1i*ptj + dpl1j*pti)                ! d-hm3
         emad = emad + (dpl2ij + dpl2ji)                      ! d-hm3

         potmad(nsi) = potmad(nsi) - ptj

        end if

        if(maxval(abs(hm4(1:3,1:3,1:3,1:3,nsj))).gt.1.d-6) then

         pti = zero
         ptj = zero
         do id=1,3
          rd = xji(id)
          do ic=1,3
           rc = xji(ic)
           do ib=1,3
            rb = xji(ib)
            do ia=1,3
             ra = xji(ia)
             duj = hm4(ia,ib,ic,id,nsj)*ra*(rb*(rc*(rd*r9i)))
             dui = hm4(ia,ib,ic,id,nsj)*ra*(rb*(rc*(rd*r9i)))
             pti = pti + dui
             ptj = ptj + duj
            end do
           end do
          end do
         end do

         emad = emad + cm(nsj)*pti + cm(nsi)*ptj         ! m-hm4
         potmad(nsi) = potmad(nsi) + ptj

        end if

       end do                   ! k=1,nrsn

       end do                   ! jat=1,natom
!c       fxyz(1:3,nsi) = fxyz(1:3,nsi)-oned4pi*fxyzi(1:3)
      end do                   ! iat=1,natom

      emad = 0.5d0*oned4pi*emad
      potmad(1:nsublat) = oned4pi*potmad(1:nsublat)

      emad = emad*(four*pi)        ! e.m.u.
      emad = emad*two              ! Ry

      potmad(1:nsublat) = potmad(1:nsublat)*((four*pi)*two)

!c      fxyz(1:3,1:nsublat) = fxyz(1:3,1:nsublat)*((four*pi)*two)

      if(nsublat.ne.natom) then
!c       allocate(numbsbl(1:nsublat))
       numbsbl(1:nsublat) = 0
       do iat=1,natom
      nsi = itype(iat)
      numbsbl(nsi) = numbsbl(nsi) + 1
       end do
       do nsi = 1,nsublat
      potmad(nsi) = potmad(nsi)/numbsbl(nsi)
      fxyz(1:3,nsi) = fxyz(1:3,nsi)/numbsbl(nsi)
       end do
!c       deallocate(numbsbl)
      end if

      return
!EOC
      end subroutine addRSmltpl

!
      end subroutine elctrstat3
