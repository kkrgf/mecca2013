!BOP
!!ROUTINE: gaussq
!!INTERFACE:
      subroutine gaussq(kind, n, kpts, endpts, b, t, w)
!!DESCRIPTION:
! {\bv
! computes the nodes t(j) and weights  w(j) for gaussian-type 
! quadrature rules with pre-assigned nodes. these are used 
! when one wishes to approximate
!
!          integral (from a to b)  f(x) w(x) dx
!
!                       n
! by                   sum w  f(t )
!                      j=1  j    j
!
! (note w(x) and w(j) have no connection with each other.)
! here w(x) is one of four possible non-negative weight
! functions (listed below), and f(x) is the
! function to be integrated.  gaussian quadrature is particularly
! useful on infinite intervals (with appropriate weight
! functions), since then other techniques often fail.
!
!    associated with each weight function w(x) is a set of
! orthogonal polynomials.  the nodes t(j) are just the zeroes
! of the proper n-th degree polynomial.
!
! input parameters (all real numbers are in double precision)
!
!  kind     an integer between 1 and 6 giving the type of
!           quadrature rule:
!
!  kind = 1:  legendre quadrature, w(x) = 1 on (-1, 1)
!  kind = 2:  chebyshev quadrature of the first kind
!             w(x) = 1/sqrt(1 - x*x) on (-1, +1)
!  kind = 3:  chebyshev quadrature of the second kind
!              w(x) = sqrt(1 - x*x) on (-1, 1)
!  kind = 4:  hermite quadrature, w(x) = exp(-x*x) on
!             (-infinity, +infinity)
!  n        the number of points used for the quadrature rule
!  kpts     (integer) normally 0, unless the left or right end-
!          point (or both) of the interval is required to be a
!          node (this is called gauss-radau or gauss-lobatto
!          quadrature).  then kpts is the number of fixed
!          endpoints (1 or 2).
!  endpts   real array of length 2.  contains the values of
!          any fixed endpoints, if kpts = 1 or 2.
!  b        real scratch array of length n
!
! output parameters (both double precision arrays of length n)
!
!  t        will contain the desired nodes.
!  w        will contain the desired weights w(j).
!
! underflow may sometimes occur, but is harmless.
!
! references
!    1.  golub, g. h., and welsch, j. h., "calculation of gaussian
!        quadrature rules," mathematics of computation 23 (april,
!        1969), pp. 221-230.
!    2.  golub, g. h., "some modified matrix eigenvalue problems,"
!        siam review 15 (april, 1973), pp. 318-334 (section 7).
!    3.  stroud and secrest, gaussian quadrature formulas, prentice-
!        hall, englewood cliffs, n.j., 1966.
!
! \ev}

!!ARGUMENTS:
      integer kind, n, kpts
      real*8 endpts(2), b(n), t(n), w(n)
!!REVISION HISTORY:
!    original version 20 jan 1975 from stanford
!    modified 21 dec 1983 by eric grosse
!      imtql2 => gausq2
!      compute pi using atan
!      removed accuracy claims, description of method
!      added single precision version
!    modified 15 Sept. 1991 by Yang Wang:
!      use generic name for functions rather than the data type
!      specified name.
!      for simplicity, delete the routines for kind = 5 and 6,
!      which give the roots and weights for Laguerre polynormials
!      and Jacobi polynormials.
! Adapted - A.S. - 2013
!EOP
!
!BOC
      real*8 muzero, t1, gam 
!      real*8 solve
!c
      call pclass (kind, n, b, t, muzero)
!c
!c           the matrix of coefficients is assumed to be symmetric.
!c           the array t contains the diagonal elements, the array
!c           b the off-diagonal elements.
!c           make appropriate changes in the lower right 2 by 2
!c           submatrix.
!c
      if (kpts.eq.0)  go to 100
      if (kpts.eq.2)  go to  50
!c
!c           if kpts=1, only t(n) must be changed
!c
      t(n) = solve(endpts(1), n, t, b)*b(n-1)**2 + endpts(1)
      go to 100
!c
!c           if kpts=2, t(n) and b(n-1) must be recomputed
!c
   50 gam = solve(endpts(1), n, t, b)
      t1 = ((endpts(1) - endpts(2))/(solve(endpts(2), n, t, b) - gam))
      b(n-1) = sqrt(t1)
      t(n) = endpts(1) + gam*t1
!c
!c           note that the indices of the elements of b run from 1 to n-1
!c           and thus the value of b(n) is arbitrary.
!c           now compute the eigenvalues of the symmetric tridiagonal
!c           matrix, which has been modified as necessary.
!c           the method used is a ql-type method with origin shifting
!c
  100 w(1) = 1.0d0
      do i = 2, n
         w(i) = 0.0d0
      enddo
!c
      call gausq2 (n, t, b, w, ierr)
      if(ierr.ne.0) stop                                                &
     &' Error in gaussq. Choose a proper machine parameter.'
      do 110 i = 1, n
         w(i) = muzero * w(i) * w(i)
  110 continue
!c
      return

!EOC
      contains
!c
!c
!c
!c     cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
!BOP
!!IROUTINE: solve
!!INTERFACE:
      double precision function solve(shift, n, a, b)
!!REMARKS:
! private procedure of subroutine gaussq
!EOP
!
!c     ================================================================
!c
!c       this procedure performs elimination to solve for the
!c       n-th component of the solution delta to the equation
!c
!c             (jn - shift*identity) * delta  = en,
!c
!c       where en is the vector of all zeroes except for 1 in
!c       the n-th position.
!c
!c       the matrix jn is symmetric tridiagonal, with diagonal
!c       elements a(i), off-diagonal elements b(i).  this equation
!c       must be solved to obtain the appropriate changes in the lower
!c       2 by 2 submatrix of coefficients for orthogonal polynomials.
!c
!c
!BOC
      real*8 shift, a(n), b(n), alpha
!c
      alpha = a(1) - shift
      nm1 = n - 1
      do i = 2, nm1
         alpha = a(i) - shift - b(i-1)**2/alpha
      enddo
      solve = 1.0d0/alpha
      return
!EOC
      end function solve
!c
!c
!c     cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
!BOP
!!IROUTINE: pclass
!!INTERFACE:
      subroutine pclass(kind, n, b, a, muzero)
!!REMARKS:
! private procedure of subroutine gaussq
!EOP
!c     ================================================================
!c
!c           this procedure supplies the coefficients a(j), b(j) of the
!c        recurrence relation
!c
!c             b p (x) = (x - a ) p   (x) - b   p   (x)
!c              j j            j   j-1       j-1 j-2
!c
!c        for the various classical (normalized) orthogonal polynomials,
!c        and the zero-th moment
!c
!c             muzero = integral w(x) dx
!c
!c        of the given polynomial's weight function w(x).  since the
!c        polynomials are orthonormalized, the tridiagonal matrix is
!c        guaranteed to be symmetric.
!c
!c
!
!BOC
      real*8 a(n), b(n), muzero
      real*8 abi, pi
!c
      pi = 4.0d0 * atan(1.0d0)
      nm1 = n - 1
      go to (10, 20, 30, 40), kind
!c
!c              kind = 1:  legendre polynomials p(x)
!c              on (-1, +1), w(x) = 1.
!c
   10 muzero = 2.0d0
      do i = 1, nm1
         a(i) = 0.0d0
         abi = i
         b(i) = abi/sqrt(4*abi*abi - 1.0d0)
      enddo
      a(n) = 0.0d0
      return
!c
!c              kind = 2:  chebyshev polynomials of the first kind t(x)
!c              on (-1, +1), w(x) = 1 / sqrt(1 - x*x)
!c
   20 muzero = pi
      do i = 1, nm1
         a(i) = 0.0d0
         b(i) = 0.5d0
      enddo
      b(1) = sqrt(0.5d0)
      a(n) = 0.0d0
      return
!c
!c              kind = 3:  chebyshev polynomials of the second kind u(x)
!c              on (-1, +1), w(x) = sqrt(1 - x*x)
!c
   30 muzero = pi/2.0d0
      do i = 1, nm1
         a(i) = 0.0d0
         b(i) = 0.5d0
      enddo
      a(n) = 0.0d0
      return
!c
!c              kind = 4:  hermite polynomials h(x) on (-infinity,
!c              +infinity), w(x) = exp(-x**2)
!c
   40 muzero = sqrt(pi)
      do i = 1, nm1
         a(i) = 0.0d0
         b(i) = sqrt(i/2.0d0)
      enddo
      a(n) = 0.0d0
      return
!EOC
      end subroutine pclass
!c
!c
!BOP
!!IROUTINE: gausq2
!!INTERFACE:
      subroutine gausq2(n, d, e, z, ierr)
!!REMARKS:
! private procedure of subroutine gaussq
!EOP
!c
!c     this subroutine is a translation of an algol procedure,
!c     num. math. 12, 377-383(1968) by martin and wilkinson,
!c     as modified in num. math. 15, 450(1970) by dubrulle.
!c     handbook for auto. comp., vol.ii-linear algebra, 241-248(1971).
!c     this is a modified version of the 'eispack' routine imtql2.
!c
!c     this subroutine finds the eigenvalues and first components of the
!c     eigenvectors of a symmetric tridiagonal matrix by the implicit ql
!c     method.
!c
!c     on input:
!c
!c        n is the order of the matrix;
!c
!c        d contains the diagonal elements of the input matrix;
!c
!c        e contains the subdiagonal elements of the input matrix
!c          in its first n-1 positions.  e(n) is arbitrary;
!c
!c        z contains the first row of the identity matrix.
!c
!c      on output:
!c
!c        d contains the eigenvalues in ascending order.  if an
!c          error exit is made, the eigenvalues are correct but
!c          unordered for indices 1, 2, ..., ierr-1;
!c
!c        e has been destroyed;
!c
!c        z contains the first components of the orthonormal eigenvectors
!c          of the symmetric tridiagonal matrix.  if an error exit is
!c          made, z contains the eigenvectors associated with the stored
!c          eigenvalues;
!c
!c        ierr is set to
!c          zero       for normal return,
!c          j          if the j-th eigenvalue has not been
!c                     determined after 30 iterations.
!c
!c     ------------------------------------------------------------------
!c
!
!BOC
      integer i, j, k, l, m, n, ii, mml, ierr
      real*8 d(n), e(n), z(n), b, c, f, g, p, r, s
      real(8), parameter :: machep=epsilon(machep)
!c
!c     ..................................................................
!c     Causion: unproper assigned machine parameter could cause
!c              unconvergent problem.
!c     ..................................................................
!c
!      machep=2.0d0**(-127)
!c
      ierr = 0
      if (n .eq. 1) go to 1001
!c
      e(n) = 0.0d0
      do 240 l = 1, n
         j = 0
!c     :::::::::: look for small sub-diagonal element ::::::::::
  105    do 110 m = l, n
            if (m .eq. n) go to 120
            if (abs(e(m)) .le. machep * (abs(d(m)) + abs(d(m+1))))      &
     &         go to 120
  110    continue
!c
  120    p = d(l)
         if (m .eq. l) go to 240
         if (j .eq. 30) go to 1000
         j = j + 1
!c     :::::::::: form shift ::::::::::
         g = (d(l+1) - p) / (2.0d0 * e(l))
         r = sqrt(g*g+1.0d0)
         g = d(m) - p + e(l) / (g + sign(r, g))
         s = 1.0d0
         c = 1.0d0
         p = 0.0d0
         mml = m - l
!c
!c     :::::::::: for i=m-1 step -1 until l do -- ::::::::::
         do ii = 1, mml
            i = m - ii
            f = s * e(i)
            b = c * e(i)
            if (abs(f) .lt. abs(g)) go to 150
            c = g / f
            r = sqrt(c*c+1.0d0)
            e(i+1) = f * r
            s = 1.0d0 / r
            c = c * s
            go to 160
  150       s = f / g
            r = sqrt(s*s+1.0d0)
            e(i+1) = g * r
            c = 1.0d0 / r
            s = s * c
  160       g = d(i+1) - p
            r = (d(i) - g) * s + 2.0d0 * c * b
            p = s * r
            d(i+1) = g + p
            g = c * r - b
!c     :::::::::: form first component of vector ::::::::::
            f = z(i+1)
            z(i+1) = s * z(i) + c * f
            z(i) = c * z(i) - s * f
         enddo
!c
         d(l) = d(l) - p
         e(l) = g
         e(m) = 0.0d0
         go to 105
  240 continue
!c
!c     :::::::::: order eigenvalues and eigenvectors ::::::::::
      do 300 ii = 2, n
         i = ii - 1
         k = i
         p = d(i)
!c
         do 260 j = ii, n
            if (d(j) .ge. p) go to 260
            k = j
            p = d(j)
  260    continue
!c
         if (k .eq. i) go to 300
         d(k) = d(i)
         d(i) = p
         p = z(i)
         z(i) = z(k)
         z(k) = p
  300 continue
!c
      go to 1001
!c     :::::::::: set error -- no convergence to an
!c                eigenvalue after 30 iterations ::::::::::
 1000 ierr = l
 1001 return
!c     :::::::::: last card of gausq2 ::::::::::

!EOC
      end subroutine gausq2

      end subroutine gaussq
