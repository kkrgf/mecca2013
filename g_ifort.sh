#!/bin/csh -f 

if ( -f ./mecca_idb ) then 
rm ./mecca_idb
endif

./make.sh idb all

if ( -f ./mecca_idb ) then 
mv mecca_idb mecca_ifort
endif

# make SYS=Darwin_idb MECCAHOME=${MECCAHOME} ODIR=obj_ifort FMDIR=finclude_ifort LIBTMP=lib_ifort PROGRAM=mecca_ifort realclean
# make SYS=Darwin_idb MECCAHOME=${MECCAHOME} ODIR=obj_ifort FMDIR=finclude_ifort LIBTMP=lib_ifort PROGRAM=mecca_ifort -j 4 all
#ifort -module ./finclude_ifort -I ./include readpot1.f lib_ifort/libMECCA.a -o readpot1.exe -framework vecLib
