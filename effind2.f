!
!BOP
!!ROUTINE: effind2
!!INTERFACE:
      subroutine effind2(numef,ebot,etop,                               &
     &               zvaltot,deltZ,cdosEf,curr_error,                   &
     &               defmax,                                            &
     &               efnew)
!!DESCRIPTION:
! estimates the Fermi energy for next iteration, {\tt efnew}
!
!!USES:
      use mecca_constants

!!DO_NOT_PRINT
      implicit none
!!DO_NOT_PRINT

!!ARGUMENTS:
      integer, intent(in) :: numef
      real(8) ::     ebot,etop
      real(8), intent(in) :: zvaltot
      real(8), intent(in) :: deltZ
      complex(8), intent(in) :: cdosEf
      real(8), intent(in) :: curr_error ! estimate of current SCF error if positive 
      real(8), intent(in) ::     defmax
      real(8) ::     efnew
!!PARAMETERS:
! eftol,zvaltol,etol are defined in module mecca_constants 
!!REVISION HISTORY:
! Adapted - A.S. - 2013
!TO DO:
! use Aitken-Stephenson method to imporve convergence?      
!EOP
!
!BOC
!c
      real(8) ::     delta,delta_fe
      real(8) ::     xtws,dosen,dE
      integer ::     dlt_sgn

!      complex(8) ::   czero
!      parameter (czero=(0.d0,0.d0))
!
!
!c     ==================================================================
!c     find the fermi energy....DEPENDENT UPON E GRID...................
!c     ==================================================================

      dosen = aimag(cdosEf)
      if(dosen.lt.zero) then
        write(6,*)
        write(6,*) ' cdosEf=',cdosEf
        write(6,*) ' DOS(En)=',dosen,' < ZERO, accuracy is lost!'
        dosen = zero 
      end if  
      delta = defmax
      if ( dosen == zero ) then
        if ( etop<ebot ) then
         delta = defmax*min(one,5.d-1*abs(deltZ)/zvaltot)   ! deltZ/abs(cdosEf)
         delta = max(delta,1.d-2)
        end if 
      end if
      call efappr(etop,deltZ,dosen,delta,efnew,numef)
!
      return
!EOC
      CONTAINS
!c
!BOP
!!IROUTINE: efappr
!!INTERFACE:
      subroutine efappr(efold,deltZ,dosen,defmax,efnew,numef)
!!REMARKS:
! private procedure of subroutine effind2
!EOP
!
!BOC
      implicit none
      real(8), intent(in) :: efold,deltZ,dosen,defmax
      real(8), intent(out) :: efnew
      integer, intent(in) :: numef

      real(8), save :: e1,delt1,dos1
      real(8), save :: e2,delt2,dos2
      real(8) :: delta
      real(8), parameter :: eps=2*epsilon(1.d0)
      integer, parameter :: nmx=13
!!!      integer, parameter :: nmx=2
      real(8) :: e0

      delta = defmax
      if ( dosen>eps ) then
        delta = min(delta,abs(deltZ/dosen))
      end if

      if(numef.eq.0.or.abs(deltZ-eps/2)<eps/2) then
       delt1 = deltZ
       e1    = efold
       dos1  = dosen
       delt2 = deltZ
       e2    = efold
       dos2  = dosen
       if ( abs(deltZ-eps/2) < eps/2 ) then
         efnew = efold  
         return
       else  
        if ( abs(deltZ)<1 ) then
         delta = min(delta,0.15d0)   
        end if
        efnew = efold + sign(delta,deltZ)
       end if
      else
       if ( delt1*delt2 > 0 ) then
          if ( efold < e1 ) then
           e2 = e1
           delt2 = delt1
           dos2 = dos1
           e1 = efold
           delt1 = deltZ
           dos1 = dosen
          else if ( efold > e2 ) then
           e1 = e2
           delt1 = delt2
           dos1 = dos2
           e2 = efold
           delt2 = deltZ
           dos2 = dosen
          else
           if ( delt1 < deltZ ) then
             e2 = efold
             delt2 = deltZ
             dos2 = dosen
           else
             e1 = efold
             delt1 = deltZ
             dos1 = dosen
           end if
          end if
       else   
          if ( delt1*deltZ < 0 ) then
            if ( efold > e1 ) then
              e2 = efold
              delt2 = deltZ
              dos2 = dosen
            else  
              e2 = e1
              delt2 = delt1
              dos2 = dos1
              e1 = efold
              delt1 = deltZ
              dos1 = dosen
            end if
          else 
            if ( efold < e2 ) then
              e1 = efold
              delt1 = deltZ
              dos1 = dosen
            else  
              e1 = e2
              delt1 = delt2
              dos1 = dos2
              e2 = efold
              delt2 = deltZ
              dos2 = dosen
            end if
          end if
       end if    
       if ( delt1*delt2 < 0 ) then
        if ( abs(deltZ)>one .or. numef>nmx ) then
          efnew = (e1+e2)/2
        else  
          efnew = e1 + (e2-e1)*abs(delt1)/(abs(delt1)+abs(delt2))
        end if
       else 
        efnew = efold+sign(delta,deltZ)
       end if
      end if
      return
!EOC
      end subroutine efappr
!c
      end subroutine effind2

!
!BOP
!!ROUTINE: efadjust2
!!INTERFACE:
      subroutine efadjust2(efnew,etop,                                  &
     &               xvalws,xvalmt,doslast,doscklast,evalsum,           &
     &               zvaltot,dQ,                                        &
     &               nbasis,nspin,komp,atcon,numbsub,                   &
!CMULTI     &               XYZmltpl,ndmltp,XYZforce,                          &
     &               iprint,istop)
!valmt     &               xvalws,xvalmt,iprint,istop)
!!DESCRIPTION:
! adjusts integrated DOS, energy and doslast according to the position of
! fermi level
!
!!USES:
      use mecca_constants

!!DO_NOT_PRINT
      implicit none
!!DO_NOT_PRINT

!!ARGUMENTS:
      real(8) ::    efnew
      real(8) ::    etop
      real(8) ::    xvalws(:,:,:)  ! (ipcomp,ipsublat,ipspin)
      real(8) ::    xvalmt(:,:,:)  ! (ipcomp,ipsublat,ipspin)
      complex(8) :: doslast(:,:,:)  ! (ipcomp,ipsublat,ipspin)
      complex(8) :: doscklast(:,:,:)  ! (ipcomp,ipsublat,ipspin)
      real(8), intent(out) :: evalsum(:,:,:)  ! (ipcomp,ipsublat,ipspin)
      real(8), intent(in)  :: zvaltot
      real(8), intent(in)  :: dQ    ! it can be used for a correction, e.g. non-spherical contribution
      integer ::    nbasis,nspin
      integer ::    komp(nbasis)
      real(8) ::    atcon(:,:)  ! (ipcomp,nbasis)
      integer ::    numbsub(nbasis)
      integer :: iprint
      character(10) :: istop
!!REVISION HISTORY:
! Adapted - A.S. - 2013
!EOP
!
!BOC
!
!c      real(8) ::      dqcorr(ipcomp,ipsublat,ipspin)

!CMULTI
!CMULTI      integer     ndmltp
!CMULTI      real(8) ::      XYZmltpl(:,:,:)  ! (ndmltp,nbasis,2)
!CMULTI      real(8) ::      XYZforce(:,:,:)  ! (3,nbasis,2)
!CMULTI

!valmt      real(8), intent(out) ::      xvalmt(:,:,:)  ! (ipcomp,ipsublat,ipspin)

!c
!c
      integer :: is,in,ic
      real(8) ::      xvaltws
      real(8) ::      xvaltmt
      real(8) ::      eigensum
!c
      real(8) ::      dlteff,sp_f
!c parameter
      character sname*10
      parameter (sname='efadjust')
!c
!c     ================================================================== !=

      dlteff = efnew - etop

!c     ==================================================================
!c
      if(iprint.ge.0) then
       write(6,'(''    Change in Ef'',t40,''='',f18.11)') dlteff
      end if
      write(6,*)
!c
!c     ==================================================================
!c     compute integrated densities of states up to fermi energy........
!c      (estimation)
!c
      if ( nspin==1 ) then   
       sp_f = two        ! spin-factor for DOS only (not for E-integral values!)
      else
       sp_f = one
      end if
      xvaltws=zero
      xvaltmt=zero
      do is=1,nspin
       do in=1,nbasis
        do ic=1,komp(in)
         xvalws(ic,in,is)=xvalws(ic,in,is) +                            &
     &                        sp_f*aimag( doslast(ic,in,is)*dlteff )
!c
         xvalmt(ic,in,is)=xvalmt(ic,in,is) +                            &
     &                        sp_f*aimag( doscklast(ic,in,is)*dlteff )
         if(iprint.ge.0) then
         write(6,'(a14,'': dosint '',2i4,2d17.8)') sname,               &
     &                         in,ic,xvalws(ic,in,is)                   &
     &                              ,xvalmt(ic,in,is)
         end if
!c
!c     +                        +dqnonasa(ic,in,is)
!c     >   aimag(dosint(ic,in,is))
!c
!valmt         xvalmt(ic,in,is)=xvalmt(ic,in,is) +                            &
!valmt     &                         dckint
!c     +                         +dqcorr(ic,in,is)
!c     >   aimag(dosckint(ic,in,is))
!c
         xvaltws=xvaltws + atcon(ic,in)*xvalws(ic,in,is)*numbsub(in)
         xvaltmt=xvaltmt + atcon(ic,in)*xvalmt(ic,in,is)*numbsub(in)
!c
        enddo
       enddo
      enddo

!      if ( abs(dQ) > zero ) then
!        write(*,*) ' DEBUG xvaltws,xvaltmt=',sngl(xvaltws),sngl(xvaltmt)
!        write(6,'(''    dQ(free electr.)'',t40,''='',f16.11)')          &
!     &              dQ+xvaltws-xvaltmt
!      end if  

      xvaltws = xvaltws + dQ

!CMULTI
!c     ==================================================================
!c     Calculate dipole and force with correct Fermi energy
!c     ==================================================================
!c

!CMULTI      XYZmltpl(1:ndmltp,1:nbasis,2) = XYZmltpl(1:ndmltp,1:nbasis,1) +   &
!CMULTI     &                   XYZmltpl(1:ndmltp,1:nbasis,2)*dlteff
!c
!CMULTI      XYZforce(1:3,1:nbasis,2) = XYZforce(1:3,1:nbasis,1) +             &
!CMULTI     &                   XYZforce(1:3,1:nbasis,2)*dlteff
!c     ==================================================================
!CMULTI

!c
!c     ==================================================================
!c
      if(iprint.ge.-1) then
!c
!c   Major printout....... Integrated n(e) at New Fermi Energy.........
!c
       write(6,'(''  +++++++++++++++++++++++++++++++++++'',             &
     &           ''++++++++++++++++++++++++++++++++++'')')
       write(6,'(''  Total     WS(VP) Int{n(e)}:'',                     &
     &                                 t40,''='',2f16.11)') xvaltws
       do is=1,nspin
        do in=1,nbasis
         if(nbasis*nspin.ne.1.or.komp(in).ne.1) then
          do ic=1,komp(in)
               write(6,'(''       Spin ='',i2,                          &
     &                   '' Sub-lat ='',i2,'' Comp ='',i2,              &
     &                   t40,''=   '',f16.11)')                         &
     &         is,in,ic,xvalws(ic,in,is)
          enddo
         end if
        enddo
       enddo
      endif
!c
!c     ==================================================================
!c
!c     calculate contibution to eigenvalue sum from e(nume-1) to efermi..

      eigensum=zero
      do is=1,nspin

!c
         do in=1,nbasis
           do ic=1,komp(in)
           evalsum(ic,in,is)=evalsum(ic,in,is) +                        &
     &                    sp_f*aimag(doslast(ic,in,is))*dlteff*etop
           eigensum=eigensum +                                          &
     &                atcon(ic,in)*evalsum(ic,in,is)*numbsub(in)
           enddo
         enddo
      enddo
!c
!c     ==================================================================
!c
      if(iprint.ge.-1) then
!c
!c   Major printout.......Band Energy............................
!c
       write(6,'(/)')
       write(6,'(''  +++++++++++++++++++++++++++++++++++'',             &
     &           ''++++++++++++++++++++++++++++++++++'')')
       write(6,'(''  Band Energy: Int{e*n(e)}, Int{(e-Ef)*n(e)}'',      &
     &          t46,''='',f19.11,1x,f19.11)') eigensum                  &
     &                                   ,eigensum-zvaltot*efnew
       do is=1,nspin
         do in=1,nbasis
           if(nbasis*nspin.ne.1.or.komp(in).ne.1) then
            do ic=1,komp(in)
               write(6,'(''       Spin ='',i2,                          &
     &                   '' Sub-lat ='',i2,'' Comp ='',i2,              &
     &                   t40,''=   '',f16.11)')                         &
     &                    is,in,ic,evalsum(ic,in,is)
!c
!CDDJ           write(6,'('' eval-xvalws*vdif,eval+xvalws*vdif '',
!CDDJ >                      2f16.11)')
!CDDJ >         evalsum(ic,in,is)-xvalws(ic,in,is)*vdif,
!CDDJ >         evalsum(ic,in,is)+xvalws(ic,in,is)*vdif
!c
            enddo
           end if
         enddo
       enddo
       write(6,'(''  +++++++++++++++++++++++++++++++++++'',             &
     &           ''++++++++++++++++++++++++++++++++++'')')
      end if
!c
!c    *************************************************************
      if(istop.eq.sname) call p_fstop(sname)
!c    *************************************************************

      return
!EOC
      end subroutine efadjust2
