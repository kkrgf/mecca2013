
!!MODULE: zcore
!!INTERFACE:
      module zcore
!!DESCRIPTION:
! calculation of single-site core-electrons energy levels and charge density
!
!!USES:
      use mecca_constants
      use mecca_types
      use mecca_interface
      use gfncts_interface
      use coresolver, only : deepst,deepst_sr
      use sctrng, only : solve1AS
      use iso_fortran_env, only : output_unit
!

!!DO_NOT_PRINT
      implicit real*8 (a-h,o-z)
      private
!!DO_NOT_PRINT

!!PUBLIC MEMBER FUNCTIONS:
      public calcCore
!!PRIVATE MEMBER FUNCTIONS:
!      subroutine check_zcor
!      subroutine core_lvls
!      subroutine core_dnsty
!      subroutine readCore
!      subroutine betterEbot
!      function empirBandW
!      function gValBottom
!      integer function jScat
!      integer function useDirac
!!REVISION HISTORY:
! Initial Version - A.S. - 2013
! Scal-Rel. solver - A.S. - 07/2015
!EOP
!
!BOC
      integer, public, parameter :: noSR=0,SR=1,noFR=2,vSR=3
      real(8), public            :: semicore_level=zero   ! all eigenvalues >= semicore_level are considered as semicore
      real(8), public            :: max_core_level=zero   ! max eigenvalue

!DELETE      real(8), parameter :: tolch=(one/ten)**7
      real(8), parameter :: tol=1.d-10
      integer, parameter :: nitmax=70
      character(*), parameter :: frozen_core='FROZEN_CORE'

      real(8), external :: fermiFun

      interface
      subroutine gMltplScatGF( mecca_state, etop_in )
      use mecca_types, only : RunState
      type(RunState), intent(inout), target :: mecca_state
      real(8), intent(in) :: etop_in
      end subroutine gMltplScatGF

      subroutine coreOrder(numc,enc,nc,lc,kc,n_hcl)
       implicit none
       integer :: numc
       real(8) :: enc(numc)
       integer :: nc(numc),lc(numc),kc(numc)
       integer, optional :: n_hcl
      end subroutine
      end interface
!EOC
      CONTAINS

!BOP
!!IROUTINE: calcCore
!!INTERFACE:
      subroutine calcCore( mecca_state,ichZcor,tempr_in,efermi )
!!DESCRIPTION:
! this subroutine is main module procedure, which calculates eigenvalues and
! eigenfunctions for relativistic radial Schroedinger equation and respective
!  core-electrons charge densities

!!DO_NOT_PRINT
      implicit none
!!DO_NOT_PRINT

!!ARGUMENTS:
      type(RunState), target :: mecca_state
      integer, intent(out) :: ichZcor
      real(8), optional, intent(in) :: tempr_in
      real(8), optional, intent(inout), target :: efermi
!!PARAMETERS:
!!REVISION HISTORY:
! Initial Version - A.S. - 2013
!!REMARKS:
!EOP
!
!BOC
!
      type(IniFile),     pointer :: inFile
      type(RS_Str),      pointer :: rs_box
      type(Sublattice),  pointer :: sublat
      type(SphAtomDeps), pointer :: v_rho_box
!
      real(8) :: Rscat,Rasa
      real(8) :: qscoutav(2)
      real(8) :: ecorv
      real(8) :: qcorout,corout_max
      real(8) :: zcornew, zcortmp(2),zsemcnew,qsemi
      real(8), pointer :: zsemi => null()
      real(8), save :: ebot0=zero
      real(8) :: ebottom
      real(8) :: ebot,etop
!      real(8) :: delec
      logical :: fileexist=.false.
      integer   jmt,jws
      integer :: nbasis,nzcor
      integer, allocatable :: komp(:)
      real(8), allocatable :: atcon(:,:)
      real(8) :: tempr
      real(8), pointer :: Ef
      real(8) :: h
      real(8), allocatable :: xr(:), rr(:), rhotmp(:)
      real(8) :: qcormt,qcortot
      integer :: nspin,nrelc,iprint,iprt,mtasa,iv,itscf,numc_max,stype
      integer :: nsub,ic,is,ndchg,ndcor,i,ibot,zc(2),indx,j,check
      integer :: ncpa_save,sch_save,msgf_save,numc,n_hocl,sp
      real(8) :: ecmin,ecmax,ecmntmp,ec_hocl
      real(8) :: totchg,corchg,qcorkkr,fac1,renorm,esemitmp,tF
!      real(8) :: t_q,t_z
      real(8), pointer :: w_ec(:,:)
      real(8), allocatable, target :: t_wec(:,:,:,:)
      real(8), pointer :: semiden(:),esemicorr
      character(4) :: n_lj
      integer, external :: indRmesh
      integer, external :: gScfItN
!
      real(8), parameter :: qcorout_limit = 0.11d0
      real(8), parameter :: ectmp = -100000.d0
      character(*), parameter :: sname='calcCore'

      rs_box => mecca_state%rs_box
      inFile => mecca_state%intfile

      tempr = zero
      if ( present(tempr_in) ) then
        tempr = tempr_in
      end if

      inquire(file=frozen_core,EXIST=fileexist)   ! TODO: env.variable to control?
      if (tempr==zero .and. (inFile%ebot.ne.zero) .and. fileexist) then  ! tempr.ne.zero is not implemented
       inFile%freeze_core = 1
       inquire(file=frozen_core,SIZE=is)
                      ! read eigenvalues and charge density
       if ( is>0 ) then
        call readCore(inFile%ebot,frozen_core)
       else
        call readCore(inFile%ebot)
       end if
       return
      else
       inFile%freeze_core = 0
      end if

      Ef => inFile%etop
      if ( present(efermi) ) then
        Ef => efermi
      end if

      itscf = gScfItN(inFile)      ! reads file from disk
      if ( itscf==0 ) then
       ebot0=zero
      end if
      nspin = inFile%nspin
      call g_reltype(core=nrelc,semicore=stype)
      iprint = inFile%iprint
      nbasis = inFile%nsubl
      mtasa = inFile%mtasa

      call g_komp(inFile,komp)
      call g_atcon(inFile,atcon)

!      c_du = (inFile%alat/(2*pi))**2

!      if(inFile%iset_ebot.eq.0) then
!       ibot = 1
!       ebot0 = inFile%ebot
!      else
      ibot = inFile%iset_ebot
      i = 0
      do nsub=1,nbasis
        if ( maxval(inFile%sublat(nsub)%compon(1:komp(nsub))%zcor) > 0 )&
     &    then
         i = i+1
        end if
      end do
      if ( i==0 .or. ebot0==zero ) then
        ibot=-1
      end if
!
      qscoutav = zero
      numc_max = 0
      semicore_level = zero
      max_core_level = zero
      allocate(t_wec(ipeval,nspin,maxval(komp),nbasis))
      t_wec = zero
! core levels
      do nsub=1,nbasis
       sublat => inFile%sublat(nsub)
       Rasa = rs_box%rws(nsub)
       Rscat = rs_box%rmt(nsub)
       do ic=1,komp(nsub)
         v_rho_box => sublat%compon(ic)%v_rho_box
         w_ec => t_wec(:,:,ic,nsub)
         numc_max = max(numc_max,v_rho_box%numc(1))
!DEBUG         if ( sublat%compon(ic)%zID == 0 ) then
!DEBUG           v_rho_box%ndcor = 0
!DEBUG           v_rho_box%zcor(1:nspin)   = zero
!DEBUG           v_rho_box%zcor_T(1:nspin) = zero
!DEBUG           v_rho_box%numc(1:nspin) = 0
!DEBUG           nullify(v_rho_box)
!DEBUG           cycle
!DEBUG         end if
         if ( allocated(xr) ) then
          if ( size(xr,1)<v_rho_box%ndrpts ) deallocate(xr,rr)
         end if
         if ( .not.allocated(xr) ) then
          allocate(xr(1:v_rho_box%ndrpts),rr(1:v_rho_box%ndrpts))
         end if
         call g_meshv(v_rho_box,h,xr,rr)
         jws = min(1+indRmesh(Rasa,size(rr),rr),size(rr))
         !? vr = ???  (vdif?)
         do is=1,nspin
!          if(iprint.gt.-10) then
!           write(6,'(//,''     Sub-lattice ='',i3,                      &
!     &                      ''  Component ='',i3,''  Spin ='',i3,       &
!     &                      ''  mtasa ='',i3)')  nsub,ic,is,mtasa
!          end if
!c     ---------------------------------------------------------------
          if ( tempr == zero ) then
           numc = v_rho_box%numc(is)
          else
           numc = size(v_rho_box%nc,1)
          end if
          i = numc
!DELETE          v_rho_box%ec(:,is) = zero
          jmt = jScat(mtasa,Rscat,jws,rr,v_rho_box%vr(1:,is))
          call core_lvls(dble(sublat%compon(ic)%zID),                   &
     &                   xr(1:v_rho_box%ndchg),rr(1:v_rho_box%ndchg),   &
     &                   v_rho_box%vr(:,is),h,mtasa,jmt,jws,nrelc,      &
     &                   i,                                             &
     &                   v_rho_box%nc(:,is),v_rho_box%lc(:,is),         &
     &                   v_rho_box%kc(:,is),v_rho_box%ec(:,is),         &
     &                   iprint,width=w_ec(:,is),nsp=nspin)
          v_rho_box%numc(is) = numc
          v_rho_box%ec(i+1:,is) = zero
          if ( max_core_level == zero ) then
           if ( numc>0 ) max_core_level = v_rho_box%ec(i,is)
          else
           max_core_level = max(v_rho_box%ec(i,is),max_core_level)
          end if
!DELETE          do while ( numc>0 )
!DELETE           if ( v_rho_box%ec(numc,is)>=zero ) exit
!DELETE           numc = numc - 1
!DELETE          end do
!DELETE          if ( max_core_level == zero ) then
!DELETE           max_core_level = maxval( v_rho_box%ec(1:numc,is),            &
!DELETE     &                              v_rho_box%ec(1:numc,is)<zero )
!DELETE          else
!DELETE           max_core_level = max( max_core_level,                        &
!DELETE     &                           maxval( v_rho_box%ec(1:numc,is),       &
!DELETE     &                            v_rho_box%ec(1:numc,is)<zero ))
!DELETE          end if
         enddo
         is = nspin
         if ( maxval(abs(w_ec(1:v_rho_box%numc(is),is)))>zero ) then
          do i=1,v_rho_box%numc(is)
           if ( w_ec(i,is)>zero ) then
            w_ec(i,is) = min(w_ec(i,is),abs(v_rho_box%ec(i,is)))
            semicore_level = min(semicore_level,v_rho_box%ec(i,is) -    &
     &                      max(min(w_ec(i,is),abs(v_rho_box%ec(i,is))),&
     &                                  empirBandW(v_rho_box%ec(i,is))))   !DEBUG
            if ( nspin>1 ) then
              semicore_level = min(semicore_level,v_rho_box%ec(i,1) -   &
     &                  max(min(w_ec(i,1),abs(v_rho_box%ec(i,1))),      &
     &                                empirBandW(v_rho_box%ec(i,1))))   !DEBUG
!   semicore_level ! it is assumed that there is no any core levels between scor(1) and scor(2);
!                  ! other assumptions that any pair of up-/down-levels is either above
!                  ! semicore_level or below it
!                  ! TODO: better way to choose semicore_level is required
            end if
           end if
          end do
         end if
!
        if ( inFile%iset_ebot == 0 ) then
         ebottom = inFile%ebot
         zc(1) = sublat%compon(ic)%zcor
         call check_zcor(ebottom,Ef,nspin,sublat%compon(ic)%zID,        &
     &                        v_rho_box,sublat%compon(ic)%zcor,nzcor)
         if ( sublat%compon(ic)%zcor.ne.nzcor ) then
          sublat%compon(ic)%zcor = nzcor
          if ( ebottom.ne.0 ) then
           ebot0 = zero
           ebottom = zero
          end if
          if ( sublat%compon(ic)%zcor<sublat%compon(ic)%zID ) then
           if ( mod(sublat%compon(ic)%zcor,2)==1 ) then
            sublat%compon(ic)%zcor = zc(1)
            inFile%iset_ebot = -1
            ibot = -1
            inFile%ebot = zero
            ebot0 = zero
            ebottom = zero
           end if
          end if
         end if
        end if
        nullify(v_rho_box)
       enddo
      enddo
!
      if ( semicore_level < zero ) then
          write(6,'(/''     Semicore states are above '',g14.5)')       &
     &                                                   semicore_level
      end if
!
      if ( stype < vSR ) then
       semicore_level = zero
      end if
      if ( inFile%iset_ebot == 0 ) then
        ebottom = inFile%ebot
        if ( ebottom > zero ) then
         write(6,'(/''WARNING: E-contour bottom > 0, input ebot='',     &
     &                                              f5.3)') ebottom
        end if
      else
        if ( ibot == -1 ) then
         ebottom = gValBottom(nspin,nbasis,inFile%sublat(1:nbasis))    ! T=0
         if ( ebot0<zero ) then
!DEBUG
          if ( abs(ebot0-ebottom)<0.05d0 ) then
           ebottom = ebot0*(one-0.5d0) + ebottom*0.5d0
          end if
!DEBUG
         end if
        else
         ebottom = zero
        end if
      end if

      do nsub=1,nbasis
       sublat => inFile%sublat(nsub)
       do ic=1,komp(nsub)
         v_rho_box => sublat%compon(ic)%v_rho_box
         call findHighestOccupiedCoreLevel(sublat%compon(ic)%zcor,      &
     &                                                 v_rho_box)
!
        if( iprint >= -1 ) then
         do is=1,nspin
           if ( v_rho_box%numc(is) < 1 ) cycle
           write(6,'(/,''     Sub-lattice ='',i3,                       &
     &                      ''  Component ='',i3,''  Spin ='',i3)')     &
     &                       nsub,ic,is
!c
!c     Major printout: core eigenvalues...........................
!c
          write(6,'(''     ///////////////////////////////////'',       &
     &          ''//////////////////////////////////'')')
          write(6,'(''      Eigenvalues:'',t20,''n'',t25,''l'',t30,     &
     &    ''k'',t42,''energy'',6x,''core'',4x,''occ.'')')
          n_hocl=nint(v_rho_box%highest_occupied_core_level(0,is))
          sp = is
          ec_hocl=v_rho_box%highest_occupied_core_level(2,is)
          do j=1,v_rho_box%numc(is)
           if(v_rho_box%ec(j,is).eq.zero) then
            if(iprint <= 1 ) cycle
           end if

           if ( v_rho_box%ec(j,is)<ec_hocl ) then
            zc(is)= (3-nspin)*abs(v_rho_box%kc(j,is))
           else if ( v_rho_box%ec(j,is)>ec_hocl) then
            zc(is) = 0
           else
            zc(is)= (3-nspin)*abs(v_rho_box%kc(j,is))
            if ( n_hocl==j ) then
             zc(is)=min(zc(is),                                         &
     &                nint(v_rho_box%highest_occupied_core_level(1,is)))
            end if
           end if

           indx= v_rho_box%lc(j,is) + abs(v_rho_box%kc(j,is))
           if (indx.gt.size(lj)) then
            n_lj = '****'
           else
            n_lj = lj(indx)
           end if
           if ( tempr==zero ) then
             write(6,'(t18,i3,t23,i3,t28,i3,t35,f16.8,3x,i1,a,i5)')     &
     &       v_rho_box%nc(j,is),v_rho_box%lc(j,is),v_rho_box%kc(j,is),  &
     &       v_rho_box%ec(j,is),v_rho_box%nc(j,is),n_lj,zc(is)
            else
             tF = fermiFun((v_rho_box%ec(j,is)-Ef)/tempr)
             write(6,'(t18,i3,t23,i3,t28,i3,t35,f16.8,3x,i1,a,i5,f9.3)')&
     &       v_rho_box%nc(j,is),v_rho_box%lc(j,is),v_rho_box%kc(j,is),  &
     &       v_rho_box%ec(j,is),v_rho_box%nc(j,is),n_lj,zc(is)          &
     &                                                 ,zc(is)*tF
            end if
          enddo
          write(6,'(''     ///////////////////////////////////'',       &
     &          ''//////////////////////////////////'')')
!c
         end do
        end if
        nullify(v_rho_box)
       enddo
      enddo
!


! charge density
!DEBUG
      do check=1,1+numc_max
       iprt = iprint
       corout_max = zero
!DEBUG
      ebot0 = ebottom
      ecmin = zero
      ecmax = ectmp
      ichZcor = 0
      do nsub=1,nbasis
       sublat => inFile%sublat(nsub)
       Rasa = rs_box%rws(nsub)
       Rscat = rs_box%rmt(nsub)
       do ic=1,komp(nsub)
!DEBUG        Rscat = sublat%compon(ic)%rSph
        v_rho_box => sublat%compon(ic)%v_rho_box
        if ( sublat%compon(ic)%zID == 0 ) then
          v_rho_box%ndcor = 0
          v_rho_box%zcor(1:nspin)   = zero
          v_rho_box%zcor_T(1:nspin) = zero
          v_rho_box%numc(1:nspin) = 0
          nullify(v_rho_box)
          cycle
        end if
        if ( inFile%iset_ebot .ne. 0 .and. ibot == -1 ) then
         call check_zcor(ebottom,Ef,nspin,sublat%compon(ic)%zID,        &
     &                        v_rho_box,sublat%compon(ic)%zcor,nzcor)
         sublat%compon(ic)%zcor = nzcor
        end if
!
        call g_meshv(v_rho_box,h,xr,rr)
        jws = min(1+indRmesh(Rasa,size(rr),rr),size(rr))
         !? vr = ???  (vdif?)
        jmt = jScat(mtasa,Rscat,jws,rr,v_rho_box%vr(1:,1))
        v_rho_box%ndcor = 0
        zcornew = zero
        zsemcnew = zero
        w_ec => t_wec(:,:,ic,nsub)
        do is=1,nspin
!c     ---------------------------------------------------------------
!          jmt = jScat(mtasa,Rscat,jws,rr,v_rho_box%vr(1:,is))
          call core_dnsty(mecca_state,nsub,ic,is,jmt,jws,Rasa,h,        &
     &                   rr(1:v_rho_box%ndchg),xr(1:v_rho_box%ndchg),   &
     &                   ebottom,ecmntmp,zcortmp,                       &
     &                   iprt,tempr,Ef,w_in=w_ec(:,is))
         do i=1,v_rho_box%numc(is)
          if(v_rho_box%ec(i,is).lt.ecmntmp) then
           ecmax = max(v_rho_box%ec(i,is),ecmax)
          endif
         end do
         ecmin = min(ecmin,ecmntmp)
         v_rho_box%zcor(is) = zero
         v_rho_box%zcor_T(is) = zero
         if ( sublat%compon(ic)%zcor > 0 ) then
          ndcor = v_rho_box%ndcor
!          call s_rho(v_rho_box,is,v_rho_box%chgcor(1:ndcor,is))
          call asa_integral(mecca_state,ic,nsub,                        &
     &         v_rho_box%chgcor(1:ndcor,is),v_rho_box%zcor_T(is),qcormt)
!
          if ( iprt>=-1 ) then
!  total core charges: MT and WS
       write(6,'(''           mt,tot charge:'',t35,''='',               &
     &                           2f16.8)') qcormt,v_rho_box%zcor_T(is)
       write(6,'(''     -----------------------------------'',          &
     &          ''----------------------------------'')')
          end if
!      ============================================================
!
          if ( v_rho_box%zcor_T(is) > zero ) then
           if ( tempr .gt. zero ) then
           v_rho_box%zcor(is)=zcortmp(1)/zcortmp(2)*v_rho_box%zcor_T(is)
           else
           v_rho_box%zcor(is)=v_rho_box%zcor_T(is)
           end if
          end if
          zcornew = zcornew + zcortmp(1)   ! T=0
          zsemcnew = zsemcnew + (zcortmp(1) - v_rho_box%zcor(is))
          if ( semicore_level < zero ) then
           qcorout = zero
          else
           qcorout = zcortmp(1) - v_rho_box%zcor(is)
          end if
          if(iprint.gt.-10 .and. qcorout.ne.zero ) then
           write(6,'(''      Charge     core:   interst.'',t35,''='',   &
     &                 f16.8)') qcorout
          end if
          corout_max = max(corout_max,abs(qcorout))
          qscoutav(is) = qscoutav(is) +                                 &
     &                            atcon(ic,nsub)*qcorout*sublat%ns
         else
          qscoutav(is) = zero
          qcormt  = zero
         end if
!c
        enddo
        if ( tempr==zero ) then
         if( zcornew > sublat%compon(ic)%zID )                          &
     &      call fstop(sname//' :: ERROR :: zcor > ztot')
         if ( abs(sublat%compon(ic)%zcor-zcornew) > 0.1d0 ) then
         write(*,'('' FAILED INTERNAL CHECK: inconsistent core charge ''&
     &      ,g14.4,'' vs '',g14.4)')  sublat%compon(ic)%zcor,zcornew
          ichZcor = ichZcor + 1
         end if
        end if
        nullify(v_rho_box)
       enddo
      enddo
      w_ec => null()
!  ecmax -- max core eigenvalue for given number of core electrons
!  ecmin -- min eigenvalue above core eigenvalues
!DEBUG
        if ( inFile%iset_ebot .ne. 0 .and. ibot == -1) then
         if ( corout_max > qcorout_limit .and. ebottom > ecmax ) then
          iprt = -1000
          ebottom = ecmax-2*empirBandW(ecmax)
         else
          exit
         end if
        else
         exit
        end if
      enddo    ! end of check-cycle
!DEBUG
      deallocate(xr,rr)
!----- ebot, etop, ef ---------------------------------------------------
!
       if ( ibot == -1 ) then
        ebot = ebot0
       else
        if (ecmin.lt.ecmax) then
          write(*,*) ' ecmin=',ecmin,'< ecmax=',ecmax
          write(*,*)  ' eBottom = ',ebot0
          call fstop(sname//                                            &
     &     ' :: ERROR :: number of valence electrons is'//              &
     &      ' incompatible with core levels')
        end if

        if ( ebot0 == zero ) then
           ebot0 = 0.5d0*(ecmin+ecmax)
        end if

        if ( ebot0.ge.ecmax+empirBandW(ecmax)                           &
     &      .and. ebot0.le.ecmin-empirBandW(ecmin) ) then
         ebot = ebot0
        else
         if ( ebot0.gt.ecmin-empirBandW(ecmin) ) then
          ebot = max(ecmin-2*empirBandW(ecmin),0.5d0*(ecmax+ecmin))
         else if  (ebot0.lt.ecmax+empirBandW(ecmax)) then
          ebot = min(ecmax+2*empirBandW(ecmax),0.5d0*(ecmax+ecmin))
         else
          call fstop(sname//                                            &
     &     ' :: ERROR :: unexpected ecmin/ecmax?')
         end if
        end if
       end if

       if ( ecmax <= ectmp ) then
        if ( inFile%Tempr == zero ) then
         ebot = ecmin-2*empirBandW(zero)
        end if
       else if ( ebot<ecmax .or. ebot>ecmin ) then
        ebot=0.5d0*(ecmin+ecmax)
       else
        ebottom = ebot
        call betterEbot(itscf,ebot)
        if ( ebot < ecmax ) then
         ebot = min(ebottom,0.5d0*(ecmin+ecmax))
        end if
       end if
!DEBUG
!       if ( inFile%Tempr > zero ) then
!        ebot = min(0.8d0*max_core_level,ebot)
!       end if
!DEBUG
       call gEnvVar('ENV_FIXED_EBOT',.true.,iv)
       if ( iv > 0 ) then
        ebot = inFile%ebot
        do nsub=1,nbasis
         sublat => inFile%sublat(nsub)
         do ic=1,komp(nsub)
          if ( sublat%compon(ic)%zID == 0 ) cycle
          v_rho_box => sublat%compon(ic)%v_rho_box
          call check_zcor(ebot,Ef,nspin,sublat%compon(ic)%zID,          &
     &                        v_rho_box,sublat%compon(ic)%zcor,nzcor)
          sublat%compon(ic)%zcor = nzcor
         end do
        end do
       end if
!DEBUG
!DELETE       if ( inFile%Tempr > zero .and. (.not. iv>0) ) then
!DELETE        if ( sum(inFile%V0)/nspin > zero ) then
!DELETE         ebot = min(ebot,-sum(inFile%V0)/nspin)
!DELETE        end if
!DELETE       end if
!DEBUG

       if ( inFile%iset_ebot == 0 .and. ecmax > ectmp .and.             &  ! "fixed" ebot
     &            inFile%ebot >= ecmax .and. inFile%ebot<= ecmin) then
         if ( iprint >= 0 ) then
          write(6,'(''      contour bottom: recommended = '',f13.4,     &
     &                          '', used from input = '',f13.4)')       &
     &        ebot, inFile%ebot
         end if
       else                                                                ! automatic mode
        inFile%ebot = ebot
        if( ebot.ne.ebot0 .and. iprint>=0 ) then
        write(6,'(''      core ecmin = '',f13.6,''  ecmax='',f14.6)')   &
     &        ecmin, ecmax
        write(6,'(''      contour bottom: requested = '',f13.4,         &
     &                                    '' actual = '',f13.4)')       &
     &        ebot0, ebot
        end if
       end if
!
       if ( inFile%ebot > Ef .and. inFile%Tempr==0 ) then
        etop = inFile%ebot + 0.5d0
        if ( iprint>=0 ) then
        write(6,'(''      ebot ='',f7.4, '' > etop ='',f7.4,            &
     &            '' new etop ='',f7.4)') inFile%ebot,Ef,etop
        end if
        Ef = etop
       end if
!
!DEBUG
       if ( stype >= vSR .and. semicore_level < ebot ) then
        etop = semicore_level
        do is=1,nspin
         do nsub=1,nbasis
          sublat => inFile%sublat(nsub)
          do ic=1,komp(nsub)
           if ( sublat%compon(ic)%v_rho_box%ndcor==0 ) cycle
           do i=1,sublat%compon(ic)%v_rho_box%numc(is)
            if ( sublat%compon(ic)%v_rho_box%ec(i,is)<=semicore_level ) &
     &       cycle
            if ( sublat%compon(ic)%v_rho_box%ec(i,is)>=ebot ) cycle
            etop = max(etop,sublat%compon(ic)%v_rho_box%ec(i,is))
           end do
          end do
         end do
        end do
        if ( etop<=semicore_level ) then
          semicore_level = zero
          call zero_work_core(mecca_state)
        else
         if ( stype == vSR ) then
          ebot = inFile%ebot
          etop = min(etop + empirBandW(etop),ebot)
          ncpa_save = inFile%ncpa
          sch_save = inFile%intgrsch
          msgf_save = mecca_state%gf_out_box%ms_gf
          inFile%intgrsch = 1
          inFile%ncpa = 1
          mecca_state%gf_out_box%ms_gf = 1
          inFile%ebot = semicore_level
!      M_S%intfile%npts = 40
!      M_S%intfile%eitop = 1.e-8

          call gMltplScatGF(mecca_state,etop)

          do nsub=1,nbasis
           sublat => inFile%sublat(nsub)
           do ic=1,komp(nsub)
            v_rho_box => sublat%compon(ic)%v_rho_box
            ndcor = v_rho_box%ndcor
            if ( ndcor==0 ) cycle
            ndchg = v_rho_box%ndchg
            if ( allocated(rhotmp) ) then
             if ( size(rhotmp,1)<v_rho_box%ndrpts ) deallocate(rhotmp)
            end if
            if ( .not. allocated(rhotmp) ) then
             allocate(rhotmp(1:v_rho_box%ndrpts))
            end if
            do is=1,nspin
             semiden => mecca_state%work_box%chgsemi(:,ic,nsub,is)
             call asa_integral(mecca_state,ic,nsub,                     &
     &                      semiden(1:ndcor),qsemi,qcormt)                !  qsemi by atomic-solver
             qcorkkr = v_rho_box%ztot_T(is)                               !  KKR-solver
             zsemi => mecca_state%work_box%zsemi(ic,nsub,is)              !  semicore Z
!DEBUGPRINT
!!            call g_totchg(v_rho_box,.true.,is,totchg,corchg)
!!            write(6,'(a,(6g14.5))') ' DEBUG Q-CHGCOR=',corchg,totchg
!!            write(6,*) ' DEBUG1ZCOR_T=',v_rho_box%zcor_T(is)
!            write(6,*) ' DEBUG     qcorkkr=',v_rho_box%ztot_T(is)
!            write(6,*) ' DEBUG qsemi(core)=',qsemi,' zsemi=',zsemi
!DEBUGPRINT
             if ( iprt>=-1 ) then
!  total (kkr) semicore energy and charges
       write(6,'(/,''  sublatt. ='',i3,'' compon. ='',i2,'' spin ='',   &
     &          i2,''   KKR semicore energy = '',f12.8,                 &
     &             '' atomic semicore energy = '',f12.8,                &
     &             ''  KKR semicor charge = '',f12.8/)') nsub,ic,is,    &
     &          mecca_state%gf_out_box%evalsum(ic,nsub,is),             &
     &          mecca_state%work_box%esemicorr(ic,nsub,is),qcorkkr-zsemi
             end if
             rhotmp(1:ndcor)=(zsemi/qcorkkr) *                          &
     &                                     v_rho_box%chgtot(1:ndcor,is)
             semiden(1:ndcor) = v_rho_box%chgtot(1:ndcor,is) -          &
     &                                                  rhotmp(1:ndcor)
             v_rho_box%chgcor(1:ndcor,is)=v_rho_box%chgcor(1:ndcor,is) +&
     &                                                  rhotmp(1:ndcor)
             v_rho_box%zcor_T = v_rho_box%zcor_T + zsemi
             renorm = zsemi/qcorkkr
             mecca_state%work_box%esemicorr(ic,nsub,is) =               &
     &                       mecca_state%gf_out_box%evalsum(ic,nsub,is) &
     &                   - mecca_state%work_box%esemicorr(ic,nsub,is)
             if ( zsemi>zero ) then
              mecca_state%work_box%esemicorr(ic,nsub,is) =              &
     &        mecca_state%work_box%esemicorr(ic,nsub,is) -              &
     &      (one-qsemi/zsemi)*mecca_state%work_box%esemicorr(ic,nsub,is)  ! band energy for semicore
                                                                          ! outside ASA
             end if

!ATOM            mecca_state%work_box%esemicorr(ic,nsub,is) =                &
!ATOM     &          (one-renorm)*mecca_state%gf_out_box%evalsum(ic,nsub,is) & !
!ATOM     &        - (zsemi-qsemi)/qcorkkr *                                 & ! band energy for semicore
!ATOM     &                       mecca_state%gf_out_box%evalsum(ic,nsub,is)   ! outside ASA

             mecca_state%gf_out_box%evalsum(ic,nsub,is) = zero
             v_rho_box%chgtot(:,is) = zero
             v_rho_box%ztot_T(is) = zero
             mecca_state%gf_out_box%xvalws(ic,nsub,is) = zero
             mecca_state%gf_out_box%xvalmt(ic,nsub,is) = zero
            end do
           end do
          end do
          mecca_state%gf_out_box%ms_gf = msgf_save
          inFile%intgrsch = sch_save
          inFile%ncpa = ncpa_save
          inFile%ebot = ebot
         else
          call p_fstop('UNKNOWN stype in ZCORE')
         end if

         if ( iprt>=-1 ) then
           write(6,'(''     -----------------------------------'',      &
     &          ''----------------------------------'')')
         end if

        end if
       else
        semicore_level = zero
        call zero_work_core(mecca_state)
       end if
!DEBUG
!?       if ( inFile%Tempr > zero ) then
!?        if ( inFile%ebot > inFile%etop - 2.5d0*inFile%Tempr             &
!?     &                                     .and. numc_max>0 ) then
!?         i = 4
!?        else
!?         i = -1
!?        end if
!?        call setNefmax(i)
!?       end if
!DEBUG
!
!-----------------------------------------------------------------------
!
      if(iprint.ge.-1 .and. maxval(abs(qscoutav(1:nspin)))>1.d-7) then
       write(6,'(''     -----------------------------------'',          &
     &          ''----------------------------------'',//)')
       write(6,'(''      Charge     core: inter. avg'',t35,''='',       &
     &                2f16.8)') qscoutav(1),qscoutav(nspin)
       write(6,'(''     -----------------------------------'',          &
     &          ''----------------------------------'',//)')
      end if
!c
!c     ===============================================================
!c
      if ( allocated(rhotmp) ) deallocate(rhotmp)
      if ( allocated(komp) ) deallocate(komp)
      if ( allocated(atcon) ) deallocate(atcon)
      if ( allocated(t_wec) ) deallocate(t_wec)
      return

      contains

      subroutine zero_work_core(mecca_state)
      implicit none
      type(RunState), target :: mecca_state
      integer :: is,nsub,ic
      do is=1,mecca_state%intfile%nspin
       do nsub=1,mecca_state%intfile%nsubl
        do ic=1,mecca_state%intfile%sublat(nsub)%ncomp
         mecca_state%intfile%sublat(nsub)%compon(ic)%                   &
     &                       v_rho_box%chgtot(:,is) = zero
         mecca_state%work_box%chgsemi(:,ic,nsub,is) = zero
         mecca_state%work_box%esemicorr(ic,nsub,is) = zero
        end do
       end do
      end do
      return
      end subroutine zero_work_core
!
      subroutine check_zcor(ebot,ef,nspin,ztot,v_rho_box,zcor,nzcor)
!
      implicit none
      real(8), intent(in) :: ebot,ef
      integer, intent(in) :: nspin,ztot
      type(SphAtomDeps), pointer, intent(in) :: v_rho_box
      integer, intent(in) :: zcor
      integer, intent(out) :: nzcor
!     real(8) :: tempr   -----> calcCore variable
      integer :: i,is,is1,is2,j,k,n_ec,spf
      integer :: zc(1:nspin),zc_t
      real(8) :: tF,ej
      integer, allocatable :: nj(:)
      real(8), allocatable :: ec(:),aindx(:)

      nzcor = 0
      if ( maxval(v_rho_box%numc(1:nspin)) <= 0 ) return

      if ( ebot > zero .and. tempr==zero ) then
        nzcor = ztot
        return
      end if
!c
!c----------------------------------------------------------------------- !-
!c NOTE: For ASA, eigenvalues are sensitive for the normalization of
!c       wave-fct. Default normalization is for whole space (and core
!c       density outside sphere may be replaced by homogeneous charge density
!        added to valence part); ASA normalization can be considered also.
!c----------------------------------------------------------------------- !-
      zc = 0
      zc_t = 0

      n_ec = sum(v_rho_box%numc(1:nspin))
      allocate(ec(1:n_ec),aindx(1:n_ec),nj(1:n_ec))
      j = 0
      do is = 1,nspin
       do i = 1,v_rho_box%numc(is)
        j = j + 1
        ec(j) = v_rho_box%ec(i,is)
        aindx(j) = j
       end do
      end do

      call dsort(ec,aindx,n_ec,2)
      nj = nint(aindx)

      do j=1,n_ec
       ej = ec(j)
       if ( ej>ebot ) cycle
       if ( tempr == zero ) then
         tF = one
       else
         tF = fermiFun( (ej-ef)/tempr )
       end if

       if ( nj(j)<= v_rho_box%numc(1) ) then
         i = nj(j)
         is = 1
       else
         i = nj(j) - v_rho_box%numc(1)
         is = 2
       end if

       zc(is) = zc(is) + (3-nspin)*abs(v_rho_box%kc(i,is))
       zc_t = zc_t + (3-nspin)*abs(v_rho_box%kc(i,is))*tF

       if ( zc_t > ztot ) then
         k = (3-nspin)*min(abs(v_rho_box%kc(i,is)),zc_t-ztot)
         zc(is) = zc(is) + k
         exit
       end if
!
      end do


      nzcor = sum(zc(1:nspin))
      if ( nzcor>ztot ) then
       nzcor = min(nzcor,ztot)
       zc(nspin) = zc(nspin)-min(ztot-nzcor,k)
       if ( j>(ztot-nzcor) ) then
        zc(1) = zc(1)-(k-(ztot-nzcor))
       end if
       if ( nzcor .ne. sum(zc(1:nspin)) ) then
        write(6,*) 'nzcor=',nzcor,' sum(zc)=',sum(zc),'zc=',zc
        call fstop('ERROR: nzcor.ne.sum(zc)')
       end if
      end if
      if ( zcor .ne. nzcor ) then
        write(6,'(/'' input zcor = '',i3,'',  ebot ='',f9.3,a)')        &
     &                                zcor,ebot                         &
     &         ,' : values of ebot and input zcore are incompatible'
        write(6,'(''   new zcor = '',i3)') nzcor
      end if
      if ( allocated(ec) ) deallocate(ec)
      if ( allocated(nj) ) deallocate(nj)
      if ( allocated(aindx) ) deallocate(aindx)
      return
      end subroutine check_zcor
!
!EOC
      end subroutine calcCore
!
!BOP
!!IROUTINE: betterEbot
!!INTERFACE:
      subroutine betterEbot(it,ebot)
!!REVISION HISTORY:
! Initial Version - A.S. - 2013
!EOP
!
!BOC
      implicit none
      integer, intent(in) :: it
      real(8), intent(inout) :: ebot
      integer, parameter :: nf=4
      real(8), save :: f(nf)
      integer ns,m
      if ( it <= 1 ) f = zero
      if ( it<=0 ) then
        return
      end if
      if ( it>nf ) then
       f(1:nf-1) = f(2:nf)
       f(nf) = ebot
      else
       f(it) = ebot
      end if
      m = min(it,nf)
      ebot = minval(f(1:m))
      return
!EOC
      end subroutine betterEbot
!
!BOP
!!IROUTINE: gValBottom
!!INTERFACE:
      function gValBottom(nspin,ns,sublat)
!!DESCRIPTION:
! to estimate position of valence band bottom
!
!!USES:
      use universal_const
      use mecca_types
!
!!ARGUMENTS:
!!PARAMETERS:
!!REVISION HISTORY:
! Initial Version - A.S. - 2013
!!REMARKS:
!EOP
!
!BOC
      implicit none
      real(8) :: gValBottom
      integer :: nspin,ns
      type(Sublattice), target :: sublat(1:ns)
!      type(Sublattice), target :: sublat(:)
      integer :: numc
      integer :: i,j,k,nsub,is,zc,zc_j
      real(8), allocatable :: aindx(:),ec(:),de(:)
      integer, allocatable :: lc(:)
      real(8) :: delt1,delt2,dd,ebot_max,ec1
      type(SphAtomDeps), pointer :: v_rho_box

!      ns = size(sublat)
      numc = 0
      do nsub=1,ns
       do k=1,sublat(nsub)%ncomp
       numc = numc + sum(sublat(nsub)%compon(k)%v_rho_box%numc(1:nspin))
       end do
      end do
      if ( numc == 0 ) then
       gValBottom = -two*empirBandW(zero)
       return
      end if
      allocate(aindx(numc),ec(numc),de(numc),lc(-1:numc))
      de(1:numc) = zero
      lc(-1:numc) = -1
      i = 0
      ebot_max = zero
      do nsub=1,ns
       do k=1,sublat(nsub)%ncomp
        v_rho_box => sublat(nsub)%compon(k)%v_rho_box
        zc = 0
        do is=1,nspin
         do j=1,v_rho_box%numc(is)
          if ( v_rho_box%ec(j,is)<zero ) then
           zc = zc + (3-nspin)*abs(v_rho_box%kc(j,is))
           if ( zc >= sublat(nsub)%compon(k)%zID .and.                  &
     &                               ebot_max>v_rho_box%ec(j,is) ) then
            ebot_max = v_rho_box%ec(j,is)
           end if
           i = i+1
           ec(i) = v_rho_box%ec(j,is)
           lc(i) = v_rho_box%lc(j,is)
           aindx(i) = i
          end if
         end do
        end do
        nullify(v_rho_box)
       end do
      end do
      ebot_max = ebot_max-epsilon(ebot_max)
      if ( i<numc ) then
        numc = i+1
        ec(numc) = zero
        aindx(numc) = -1
      end if
      call dsort(ec,aindx,numc,2)
      delt1 = empirBandW(zero)
      k = 1
      do i=1,numc-1
       k = i+1
       if ( ec(k)>-2*epsilon(ebot_max) ) then
        de(i) = 0.5d0*delt1-ec(i)
       else
        de(i) = ec(k)-ec(i)
       end if
       if ( ec(k) < min(-delt1,ebot_max) ) then
!        de(i) = ec(k)-ec(i)
       else
        exit
       end if
      end do

!      if ( k<numc ) then
!       de(i) = ec(k)-ec(k-1)
!      end if
      i = k-1

      if ( numc == 1 ) then
       gValBottom = min(-empirBandW(zero),                              &
     &                   ec(1)-empirBandW(ec(1),lc(nint(aindx(1)))))
      else
       gValBottom = zero
      end if

      do k=i,1,-1
       gValBottom = 0.5d0*(ec(k+1)+ec(k))
       j = nint(aindx(k+1))
       delt1 = empirBandW(ec(k+1),lc(j))
!       if ( k==i .and. k+2<=numc ) then
!         delt2 = empirBandW(ec(k+2))
!         dd = ec(k+2)-ec(k+1) - (delt1+delt2)
!         write(6,*) ' DEBUG:',delt1,delt2,dd
!         if ( dd > zero ) then
!          gValBottom = ec(k+1)+delt1+0.5d0*dd
!          write(*,*) ' DEBUG1:: k,gValBottom=',k,gValBottom
!          exit
!         end if
!       end if
       j = nint(aindx(k))
       delt2 = empirBandW(ec(k),lc(j))
       if ( delt1+delt2 < de(k) ) then
         if ( gValBottom<-1.d0 ) then   !DEBUG
          gValBottom = max(gValBottom,ec(k+1)-delt1)
         end if
         exit
       else
         if ( k==1 ) gValBottom = ec(1)                                 &
     &              - empirBandW(ec(1),lc(nint(aindx(1))))
       end if
      end do

      gValBottom = min(gValBottom,ebot_max)
      deallocate(aindx,ec,de,lc)
      do nsub=1,ns
       do k=1,sublat(nsub)%ncomp
        v_rho_box => sublat(nsub)%compon(k)%v_rho_box
        zc = 0
        ec1 = v_rho_box%ec(1,1)
        do is=1,nspin
         do j=1,v_rho_box%numc(is)
          if ( v_rho_box%ec(j,is)<gValBottom ) then
           zc_j = (3-nspin)*abs(v_rho_box%kc(j,is))
           if ( (zc + zc_j) > sublat(nsub)%compon(k)%zID ) then
            gValBottom = 0.5d0*(ec1+v_rho_box%ec(j,is))
            go to 1
           end if
           zc = zc + zc_j
          end if
          ec1 = max(ec1,v_rho_box%ec(j,is))
         end do
        end do
1       continue
        sublat(nsub)%compon(k)%zcor = zc
        nullify(v_rho_box)
       end do
      end do
      return
!EOC
      end function gValBottom
!
!BOP
!!IROUTINE: empirBandW
!!INTERFACE:
      function empirBandW(en,lc)
!!DESCRIPTION:
! to generate empirical parameters related to width of core-levels
!
!!REVISION HISTORY:
! Initial Version - A.S. - 2013
!EOP
!
!BOC
      implicit none
      real(8) :: empirBandW
      real(8), intent(in) :: en
      integer, optional, intent(in) :: lc
      real(8), parameter :: d0=0.4d0,a0=1.0d0,one=1.d0,zero=0.d0
      real(8), parameter :: wmin=d0/5
      real(8), parameter :: corr(-1:4) =                                &
     &                  [ 0.d0, 0.15d0, 0.075d0, 0.05d0, 0.1d0, 0.05d0 ]
      if ( en>=zero ) then
          empirBandW = d0
      else
          empirBandW = max(wmin,d0/(a0*en**2 + one))
      end if
      if ( present(lc) ) then
       if (lc>=0 .and. lc<=size(corr)-1) empirBandW=empirBandW+corr(lc)
      end if
      return
!EOC
      end function empirBandW
!
!c
!c     ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
!c
!BOP
!!IROUTINE: core_lvls
!!INTERFACE:
      subroutine core_lvls(z,x,r,rv,h,mtasa,jmt,jws,nrelc,              &
     &                     numc,nc,lc,kc,ecore,iprint,width,nsp)
!!DESCRIPTION:
! to calculate core electrons eigenvalues
!
!!REVISION HISTORY:
! Initial Version - A.S. - 2013
!EOP
!
!BOC
      implicit none
!c
      real(8), intent(in) :: z
      real(8), intent(in) :: x(:)
      real(8), intent(in) :: r(size(x)),rv(size(x))
      real(8), intent(in) :: h
      integer, intent(in) :: mtasa,jmt,jws,nrelc
      integer, intent(inout) :: numc
      integer, intent(out) :: nc(:),lc(:),kc(:)
      real(8), intent(inout) :: ecore(:)
      integer, intent(in) :: iprint
      real(8), intent(out), optional :: width(:)
      integer, optional :: nsp
!c
      real*8 g(size(x))
      real*8 f(size(x))
      real(8) :: dy(5),drhodr,fac1,xmt,xws,wdth
      real(8), allocatable :: wrk(:)
      real(8) :: emax,c,ec0
      integer :: last,nelt,nspin,i,j,iter,ifail,nP=100
      integer :: n_hcl
!c parameters
      real(8), parameter :: e0=-0.8d0
!c
!c     //////////////////////////////////////////////////////////////////
!c     This routine drives the radial eq. solution for the core states.
!c
!c                 modified for ASA case....... ddj & fjp  july 1991
!c           ......modified for SPIN-POLARIZED REL. case...ddj sept 1995
!c     //////////////////////////////////////////////////////////////////
!c
!c     *****************************************************************
!c     calls:  deepst(outws,inws)
!c     *****************************************************************
!c     r= radial grid,
!c     rv=potential times r
!c     ecore= core levels guesses: to be updated
!c     nc= principal q. nos.,
!c     lc= orbital q. nos.,
!c     kc= kappa q.nos.
!c     numc= no. of core states,
!c     z= atomic no.,
!c     jmt=muffin-tin index
!c     jws=top r index
!c     **************************************************************
!c
!c     ks is the plus and minus relativistic solutions.
!c     -------------------------------------------------------------
!c           n =   1      2            3                  4
!c           l =   0   0  1  1   0  1  1  2  2   0  1  1  2  2  3  3
!c     core kc =  -1  -1  1 -2  -1  1 -2  2 -3  -1  1 -2  2 -3  3 -4
!c     core ks =  -1  -1  1 -1  -1  1 -1  1 -1  -1  1 -1  1 -1  1 -1
!c                        \ /       \ /   \ /       \ /   \ /   \ /
!c                         |         |     |         |     |     |
!c     averaged:   s   s   p     s   p     d     s   p     d     f
!c     -------------------------------------------------------------
!c     NOTE: valence KC is may be different by sign for l.ne.0
!c     val. kc =  -1  -1 -1  2  -1 -1  2 -2  3  -1 -1  2 -2  3 -3  4
!c     -------------------------------------------------------------
!c

      nspin = 1
      last = size(x)
!
      if ( present(width) ) then
       if ( present(nsp) ) then
        nspin = nsp
       end if
       allocate(wrk(last))
       width = zero
      end if
!
      if (numc.le.0) then
       ecore = zero
       return
      end if
!
      emax=-ten**6
      nelt=0
      c = two*inv_fine_struct_const
      c=c*10.0d0**max(0,nrelc)
      nP = useDirac(nint(z))
      do i=1,numc
        ec0 = ecore(i)
        if (ec0.ge.e0) ec0 = ec0-one
        if ( nc(i) < nP ) then
         call deepst(nc(i),lc(i),kc(i),ec0,                             &
     &            rv,r,x,g,f,h,z,c,nitmax,tol,jmt,jws,                  &
     &            last,iter,ifail)
        else
         nP = -1
         call deepst_sr(nc(i),lc(i),kc(i),ec0,                          &
     &            rv,r,x,g,f,h,z,c,nitmax,tol,jmt,jws,                  &
     &            last,iter,ifail)

         if ( present(width) .and. ec0<zero ) then
          wrk(1:jws) = g(1:jws)*g(1:jws)
          call qexpup(1,wrk,jws,x,f)
          xmt = f(jmt)
          xws = f(jws)
          j = 0
          call derv5(wrk(jws-2:jws+j),dy(1:j+2+1),r(jws-2:jws+j),j+2+1)
          drhodr = dy(3)
          wdth = -drhodr*r(jws)**2/xws
          fac1 = (2/nsp)*abs(2*kc(i)+1)
!          if (fac1*(xws-xmt)>qtol .and. wdth*fac1*(xws-xmt)>etol) then
          if ((one-xws)>1.d-04.and.wdth*fac1*(xws-xmt)>1.d-03) then
           width(i)=wdth*(0.25d0+log(lc(i)+one))
        write(6,'('' CORE_LVLS: '',3i3,'' EC='',g18.10,'' WDTH='',g13.5,&
     &            '' Qintr,dE='',3g14.5)') nc(i),lc(i),kc(i),ec0,       &
     &                            width(i),xws-xmt,wdth*fac1*(xws-xmt)
          end if
         end if
        end if
        if ( iprint.ge.0 .and. ifail .ne. 0 .and. ec0.lt.e0) then
          write(6,*) ' WARNING after deepst, IFAIL=',ifail              &
     &,    ' nc=',nc(i),' lc=',lc(i),' kc=',kc(i)                       &
     &,    ' iter=',iter,' ec0=',ec0
        end if
        ecore(i) = ec0
      enddo
      if ( allocated(wrk) ) deallocate(wrk)
      call coreOrder(numc,ecore,nc,lc,kc,n_hcl)
      numc = n_hcl
      return
!EOC
      end subroutine core_lvls
!
!BOP
!!IROUTINE: core_dnsty
!!INTERFACE:
      subroutine core_dnsty(mecca_state,nsub,ic,is,                     &
     &                  jmt,jws,Rasa,h,r,x,                             &
     &                  eBottom,ecmin,qnelt,                            &
     &                  iprint,tempr,efermi,w_in)
!cab     &                  jvp,norder,rmoments,                            &
!!DESCRIPTION:
!  to calculate core-electrons charge density and charges
!
!!REVISION HISTORY:
! Initial Version - A.S. - 2013
!EOP
!
!BOC
      implicit none
!c
      type(RunState), target :: mecca_state
      integer, intent(in) :: ic,nsub,is
      integer, intent(in) :: jmt,jws
      real(8), intent(in) :: Rasa,h
      real(8), intent(in) :: x(:)
      real(8), intent(in) :: r(size(x))
!cab      integer jvp,norder
!cab      real(8) :: rmoments(0:norder),coeff(0:norder)
!c
      real(8), intent(in) :: eBottom
      real(8), intent(out) :: ecmin
      real(8), intent(out) :: qnelt(2)
      integer, intent(in) :: iprint
!
      real(8), intent(in), optional :: tempr,efermi
      real(8), intent(in), optional, target :: w_in(:)
!
! in/out from mecca_state
!
      real(8), pointer :: rSph
      real(8), pointer :: rv(:)
      integer, pointer :: zcor
      integer, pointer :: numc
      integer, pointer :: nc(:)
      integer, pointer :: lc(:)
      integer, pointer :: kc(:)
      real(8), pointer :: ecore(:)
      real(8), pointer :: corden(:)
      real(8), pointer :: semiden(:),esemicorr,zsemi
      real(8), pointer :: w_ec(:)
      integer :: nspin,nrelc,mtasa
      real(8) :: wc,z
!
      type(SphAtomDeps), pointer :: v_rho_box
      integer, pointer :: ndcor
      real(8) :: ecorv
      real(8) :: tempr0=0, ef=0
      real(8) :: g(size(x))
      real(8) :: f(size(x))
      real(8) :: wrk(size(x)),tmpr(size(x))
      complex(8) :: gf(size(x))
      real(8) :: xtot,xmt,ff,enterm
      real(8) :: pv3_tu_semc,en_tu_semc
      real(8) :: nelt_T,nelt_r,c,anorm,fac1,ec0,dz,occ_inc
      integer :: i,last,last2,nelt,indx,inc,itmp,spl
      integer :: iter,ifail,j,nP=100

!c parameters
!DELETE      real(8), parameter ::     tol=1.d-10
      character(10), parameter ::  sname='core_dnsty'
!c     +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!      data lj/'s1/2','p1/2','p3/2','d3/2','d5/2','f5/2','f7/2'/

!      complex(8), external :: TemprFun
      integer :: st_env
!c
!c     //////////////////////////////////////////////////////////////////
!c     This routine drives the radial eq. solution for the core states.
!c     It must be called for each component (sublattice) separately.
!c
!c           ......added INWHNK for inward integration of dirac eq.
!c                 and modified for ASA case....... ddj & fjp  july 1991
!c           ......modified for SPIN-POLARIZED REL. case...ddj sept 1995
!c     //////////////////////////////////////////////////////////////////
!c
!c     *****************************************************************
!c     calls:  deepst(outws,inws)
!c
!c     *****************************************************************
!c     r= radial grid,
!c     rv=potential times r
!c     ecore= core levels guesses: to be updated
!c     g,f upper and lower components times r
!c     corden= core charge density
!c     nc= principal q. nos.,
!c     lc= orbital q. nos.,

!c     kc= kappa q.nos.
!c     numc= no. of core states,
!c     z= atomic no.,
!c     jmt=muffin-tin index
!c     jws=top r index
!c     **************************************************************
!c
!c     ks is the plus and minus relativistic solutions.
!c     -------------------------------------------------------------
!c           n =   1      2            3                  4
!c           l =   0   0  1  1   0  1  1  2  2   0  1  1  2  2  3  3
!c     core kc =  -1  -1  1 -2  -1  1 -2  2 -3  -1  1 -2  2 -3  3 -4
!c     core ks =  -1  -1  1 -1  -1  1 -1  1 -1  -1  1 -1  1 -1  1 -1
!c                        \ /       \ /   \ /       \ /   \ /   \ /
!c                         |         |     |         |     |     |
!c     averaged:   s   s   p     s   p     d     s   p     d     f
!c     -------------------------------------------------------------
!c     NOTE: valence KC is may be different by sign for l.ne.0
!c     val. kc =  -1  -1 -1  2  -1 -1  2 -2  3  -1 -1  2 -2  3 -3  4
!c     -------------------------------------------------------------
!c
!c
!c----------------------------------------------------------------------- !-
!c NOTE: For ASA, normalization and energy update of eigenvalue guess are
!c       sensitive to loss of points which describe decaying tail of
!c       wave-fct., so in DEEP and, especially, SEMCST the normalization
!c       and energy update require different calls to integrate wave-fcts !.
!c       r-grids must go to IPRPTS and ASA integrations must go to LAST2.
!c----------------------------------------------------------------------- !-
!c
      if ( present(tempr) .and. present(efermi) ) then
        tempr0 = tempr
        ef = efermi
      else
        tempr0 = zero
      end if
      zcor => mecca_state%intfile%sublat(nsub)%compon(ic)%zcor
      v_rho_box => mecca_state%intfile%sublat(nsub)%compon(ic)%v_rho_box
      rSph => mecca_state%intfile%sublat(nsub)%compon(ic)%rSph
      rv => v_rho_box%vr(:,is)
      numc => v_rho_box%numc(is)
      nc => v_rho_box%nc(:,is)
      lc => v_rho_box%lc(:,is)
      kc => v_rho_box%kc(:,is)
      ecore => v_rho_box%ec(:,is)
      corden => v_rho_box%chgcor(:,is)
      semiden => null()
      if ( allocated(mecca_state%work_box%chgsemi) ) then
       semiden => mecca_state%work_box%chgsemi(:,ic,nsub,is)
       semiden = zero
      end if
      esemicorr => null()
      if ( allocated(mecca_state%work_box%esemicorr) ) then
       esemicorr => mecca_state%work_box%esemicorr(ic,nsub,is)
       esemicorr = zero
      end if
      if ( allocated(mecca_state%work_box%zsemi) ) then
       zsemi => mecca_state%work_box%zsemi(ic,nsub,is)
       zsemi = zero
      end if
      nspin = mecca_state%intfile%nspin
      call g_reltype(core=nrelc)
      mtasa = mecca_state%intfile%mtasa
      z = dble(mecca_state%intfile%sublat(nsub)%compon(ic)%zID)
      ndcor => v_rho_box%ndcor
!c fix the last point.....
      last=size(x)
      select case (mtasa)
       case (0)
!c potential is defined up to jmt
        last2=last
       case (1)
!c potential is defined up to jws
        last2=jws
       case default
        last2=last
      end select
      last2 = min(last2,v_rho_box%ndchg)
!
      corden(1:last) = zero
      ecorv=zero
      qnelt = zero
!
      if (numc.le.0) then
        ndcor = 0
        return
      end if
      if ( present(w_in) ) then
        w_ec => w_in
      else
        allocate(w_ec(numc))
        w_ec = zero
      end if
!
      c = two*inv_fine_struct_const
      c=c*10.0d0**max(0,nrelc)
!DELETE      ec0 = eBottom
!DELETE      inc = 0
!DELETE      nelt=0
!DELETE      itmp = 0
!DELETE      do i=1,numc
!DELETE        if ( ecore(i) < zero ) then
!DELETE         itmp = nelt+abs(kc(i))
!DELETE         inc = i
!DELETE         if ( nspin == 1 ) then
!DELETE          if ( itmp*2 < zcor ) then
!DELETE              nelt = itmp
!DELETE          else
!DELETE           if ( nelt+itmp == zcor .or. itmp+itmp == zcor ) then
!DELETE             nelt = zcor
!DELETE           else
!DELETE             nelt = nelt*2
!DELETE           end if
!DELETE           if ( ecore(i) > eBottom ) then
!DELETE             ec0 = ecore(i)
!DELETE           end if
!DELETE           exit
!DELETE          end if
!DELETE         else
!DELETE          nelt = itmp
!DELETE          if ( itmp >= zcor/2 ) then
!DELETE           if ( ecore(i) > eBottom ) then
!DELETE             ec0 = ecore(i)
!DELETE           end if
!DELETE           exit
!DELETE          end if
!DELETE         end if
!DELETE        end if
!DELETE      end do
!c
!c     ============================================================
!c
      pv3_tu_semc = zero
      en_tu_semc  = zero

      inc = nint(v_rho_box%highest_occupied_core_level(0,is))
      ecmin = ecore(min(numc,inc+1))

      nelt_r=zero
      nelt_T=zero
      nP = useDirac(nint(z))
!
      occ_inc = v_rho_box%highest_occupied_core_level(1,is)

      do i=1,inc
        if ( nspin==1 ) then
         fac1=2*abs(kc(i))
        else
         fac1=abs(kc(i))
        end if
        if ( i==inc ) then
         fac1 = occ_inc
        end if
        if ( nc(i) < nP ) then
          call deepst(nc(i),lc(i),kc(i),ecore(i),                       &
     &            rv,r,x,g,f,h,z,c,1,tol,jmt,jws,                       &
     &            last,iter,ifail)
        else
          nP = -1
          call deepst_sr(nc(i),lc(i),kc(i),ecore(i),                    &
     &            rv,r,x,g,f,h,z,c,nitmax,tol,jmt,jws,                  &
     &            last,iter,ifail)
        end if
!
        if ( nP>=0 ) then
         wrk(1:last) = f(1:last)*f(1:last) + g(1:last)*g(1:last)
        else
         wrk(1:last) = f(1:last)*f(1:last) + g(1:last)*g(1:last)
         call asa_integral(mecca_state,ic,nsub,wrk(1:last),xtot,xmt)
         if ( one-xmt < 5.d-3 ) then
         else
!DEBUGPRINT
            write(6,*)
            write(6,'('' CORE_DNST: '',3i3,'' EC='',g18.10,             &
     &       '' Xmt,Xtot,wf^2='',3g14.5)') nc(i),lc(i),kc(i),ecore(i),  &
     &                  xmt,xtot,wrk(jws)
!DEBUGPRINT
            wc = max(1.d-3,w_ec(i)/2)
            call solve1AS(nrelc,c,nint(z),lc(i),ecore(i),wc,            &
     &                  gf,                                             &
     &                  rSph,r,rv)
            wrk(1:last) = aimag(gf(1:last))
!DEBUGPRINT
            call qexpup(1,wrk,last,x,tmpr)
            write(6,'(6(a,g13.4))')                                     &
     &                           ' DEBUG WS=',r(jws),' xws=',tmpr(jws), &
     &                           '  CS=',r(last),' xcs=',tmpr(last)
          write(6,'(a,3g13.5)')  ' DEBUG delt=',1-xmt,1-tmpr(jws),      &
     &                             1-tmpr(last)
!DEBUGPRINT
         end if
        end if

! NORMALIZATION IS IMPOSED IN SOLVER
!

!        call gEnvVar('ENV_RENORMCORE',.false.,st_env)
!        if ( st_env==1 ) then
!         wrk(1:last) = wrk(1:last)/xtot
!         xmt = xmt/xtot
!         xtot = one
!        else if ( st_env==2 ) then
!         wrk(1:last)=wrk(1:last)+(one-xtot)*three/Rasa**3*r(1:last)**2
!         call asa_integral(mecca_state,ic,nsub,wrk(1:last),             &
!     &            xtot,xmt)
!        end if

        if ( zcor - nelt_r >= fac1 ) then
         dz = fac1
         nelt_r = nelt_r   + fac1
        else
         dz = max(zero,zcor-nelt_r)
         nelt_r = zcor
        end if
        if ( tempr0>zero ) then
!         ff = dreal(TemprFun(dcmplx((ecore(i)-ef)/tempr0,zero),0))
         ff = fermiFun( (ecore(i)-ef)/tempr )
        else
         if ( nelt_r > zcor ) then
          ff = zero
         else
         ff = one
        end if
        end if
        nelt_T = nelt_T + ff*dz
        ecorv = ecorv + ff*dz*ecore(i) ! ecorv is only for printing
        corden(1:last) = corden(1:last) + ff*dz*wrk(1:last)
        end do   !  end of 1:ink cycle
!
      ecore(inc+1:) = zero
      if ( is==1 ) then
       if ( inc > 0 ) then
        ndcor = last2
       else
        ndcor = 0
       end if
      end if
      if ( present(w_in) ) then
      else
          if (associated(w_ec)) deallocate(w_ec)
      end if
      w_ec => null()
!
!!!c
!!!c integrate averaged single-site density
!!!c
!!!c deep core: MT and WS
!!!      if ( inc > 0 ) then
!!!        call asa_integral(mecca_state,ic,nsub,corden(1:last),           &
!!!     &            qcortot,qcormt)
!!!cab      if(jmt.lt.jws.and.jws.lt.jvp) then
!!!cab       ndata = jvp - jmt + 1
!!!cab       g(jmt:jvp) = corden(jmt:jvp)/(r(jmt:jvp)**2)
!!!cab       qcortot = qcortot + vpmtInt(ndata,g(jmt),r(jmt),Rasa,Rasa,       &
!!!cab     &                            norder,rmoments,coeff)
!!!cab      end if
!!!      end if
      qnelt(1) = nelt_r
      qnelt(2) = nelt_T
!c
!c     ============================================================
      if(iprint.ge.-1 .and. ecorv.ne.zero) then
       write(6,'(/,''     Sub-lattice ='',i3,                           &
     &              ''  Component ='',i3,''  Spin ='',i3)')  nsub,ic,is
       write(6,'(''     -----------------------------------'',          &
     &          ''----------------------------------'')')
       write(6,'(''      Core        energy:'',t35,''='',f16.8)')       &
     &                                          ecorv
!!!c
!!!c total core charges: MT and WS
!!       write(6,'(''           mt,tot charge:'',t35,''='',               &
!!     &                           2f16.8)') qcormt,qcortot
!!       write(6,'(''     -----------------------------------'',          &
!!     &          ''----------------------------------'')')
      end if
!c     ============================================================
!c
      return
!EOC
      end subroutine core_dnsty
!
!%BOP
!!IROUTINE: coreTUterms
!!INTERFACE:
!%EOP
!
!BOC
      subroutine coreTUterms(xr,rr,rv,z,jsct,ecore,rhoc,en_c,pv3_c)
      implicit none
      real(8), intent(in) :: xr(:),rr(:),rv(:),z
      integer, intent(in) :: jsct
      real(8), intent(in) :: ecore,rhoc(:)
      real(8), intent(out) :: en_c,pv3_c

      real(8) :: bndint(jsct),tmpint(jsct),rhoEcore(jsct)
      real(8) :: tmpec(jsct),tmppv(jsct)
      real(8) :: dvrdr(jsct)

!c     ==================================================================
!c     calculate d(vr)/dr
!c
       call derv5(rv,dvrdr,rr,jsct)

!c     ==================================================================
!c     calculate:  -int 4pi rhoc*d(rv)/dr r2 dr .........................
!c
       tmpint(1:jsct) = rhoc(1:jsct)*dvrdr(1:jsct)
       call qexpup(1,tmpint,jsct,xr,tmpec)
       en_c = ecore - tmpec(jsct)                                       &
     &              -tmpint(1)*rr(1)/three
!c
!c     ==================================================================
!c     pressure: calculate -int 4pi corden d(r2v)/dr r dr ...............
!c
!c     1/r*d(r^2*v)/dr = 1/r*d(r*rv)/dr = rv/r + d(rv)/dr
!c
!DEBUG       call qexpup(0,rhoc,jsct,xr,tmppv)
!DEBUG       pv3_c = en_c + ecore + (two*z)*tmppv(jsct)
!DEBUG       tmpint(1:jsct) = rhoc(1:jsct)*((two*z)+rv(1:jsct))/rr(1:jsct)
!DEBUG       call qexpup(1,tmpint,jsct,xr,tmppv)
!DEBUG       pv3_c = pv3_c - tmppv(jsct)                                     &
!DEBUG     &               -tmpint(1)*rr(1)/two
       tmpint(1:jsct) = rhoc(1:jsct)*rv(1:jsct)
       call qexpup(0,tmpint,jsct,xr,tmppv)
       pv3_c = en_c + ecore - tmppv(jsct) - tmpint(1)/two

      return
!EOC
      end subroutine coreTUterms
!
!BOP
!!IROUTINE: jScat
!!INTERFACE:
      integer function jScat(mtasa,rscat,jvp,rr,vr)
!EOP
!BOC
      implicit none
      integer mtasa
      real*8 rscat
      integer jvp
      real*8 rr(:)
      real*8 vr(:)
      integer indRmesh
      external indRmesh

      if ( jvp > size(rr) ) call fstop('jScat: unexpected error')
      jScat = indRmesh(rscat,jvp,rr)
      if ( mtasa.eq.5 ) then
       jScat = maxloc( vr(jScat:jvp)/rr(jScat:jvp), 1 )
      else
       if(rr(jScat).lt.rscat) jScat = jScat+1
      end if

      return
!EOC
      end function jScat
!c
!BOP
!!IROUTINE: readCore
!!INTERFACE:
      subroutine readCore(ebot,filenm)
!!DESCRIPTION:
! reads core-related data from potential file,
! eigenvalues above {\tt ebot} are ignored
!
!!USES:
      use mecca_run
      use scf_io, only : Potfile, readpot,cp_from_potfile

!!DO_NOT_PRINT
      implicit none
!!DO_NOT_PRINT

!!ARGUMENTS:
      real(8), intent(in) :: ebot
      character(*), intent(in), optional :: filenm
!!REVISION HISTORY:
! Initial Version - A.S. - 2013
!EOP
!
!BOC
      type(RunState), pointer :: M_S
      type(IniFile),     pointer :: inFile
      type(Potfile) :: siteptn
      type(Sublattice),  pointer :: sublat
      type(SphAtomDeps), pointer :: v_rho_box
      integer, allocatable :: komp(:)
      real(8), allocatable :: atcon(:,:)
      integer :: nspin,nbasis,nsub,ic,is,i,nelt,indx
      logical :: endflg=.false.
      integer STT
      character(200) :: MSG
      integer :: nfch=1

      M_S => getMS()
      inFile => M_S%intfile
      if(inFile%iprint.ge.-1) then
        write(6,'(/a/)') '  "FROZEN-CORE RUN"  '
      end if

      if ( present(filenm) ) then
       nfch = 1
       open(unit=nfch,file=trim(filenm),                                &
     &                 status='old',IOSTAT=STT,IOMSG=MSG)
      else
       nfch=inFile%io(nu_inpot)%nunit
       open(unit=nfch,file=trim(inFile%io(nu_inpot)%name),              &
     &                 status='old',IOSTAT=STT,IOMSG=MSG)
      end if
      if ( STT .ne. 0 ) then
       write(6,*) ' IO-error in readCore: ',MSG
       call fstop(' ERROR :: file does not exist or corrupted')
      end if

      nspin = inFile%nspin
      nbasis = inFile%nsubl
      call g_komp(inFile,komp)
      call g_atcon(inFile,atcon)

      do is=1,nspin
       do nsub=1,nbasis
        sublat => inFile%sublat(nsub)
        do ic=1,komp(nsub)
          v_rho_box => sublat%compon(ic)%v_rho_box
          if ( endflg ) then
              write(*,*)
              write(*,*) ' is=',is,' ic=',ic,' nsub=',nsub
              call fstop(' ERROR:: corrupted frozen-core file')
          end if
          call readpot(nfch,siteptn,endflg)
          if ( siteptn%ztot .ne. sublat%compon(ic)%zID ) then
           call fstop(' ERROR: frozen-core ztot .ne. Z')
          end if
          if ( sublat%compon(ic)%zID == 0 ) then
           v_rho_box%ndcor = 0
           v_rho_box%zcor(is)   = zero
           v_rho_box%zcor_T(is) = zero
           v_rho_box%numc(is) = 0
           v_rho_box%chgcor(:,is) = zero
           cycle
          end if
!
          call cp_from_potfile(siteptn,v_rho_box,is,'chgcor')
!
          v_rho_box%numc(is)=siteptn%numc
          v_rho_box%nc(1:siteptn%numc,is) = siteptn%nc(1:siteptn%numc)
          v_rho_box%lc(1:siteptn%numc,is) = siteptn%lc(1:siteptn%numc)
          v_rho_box%kc(1:siteptn%numc,is) = siteptn%kc(1:siteptn%numc)
          do i=1,siteptn%numc
           if ( siteptn%ec(i) < 0 )  then
             v_rho_box%ec(i,is) = siteptn%ec(i) - v_rho_box%v0(is)
           end if
           if ( v_rho_box%ec(i,is) > ebot ) v_rho_box%ec(i,is) = zero
          end do
          v_rho_box%nc(siteptn%numc+1:,is) = 0
          v_rho_box%lc(siteptn%numc+1:,is) = 0
          v_rho_box%kc(siteptn%numc+1:,is) = 0
          v_rho_box%ec(siteptn%numc+1:,is) = 0
          v_rho_box%zcor(is) = siteptn%ztot-siteptn%zval
          v_rho_box%zcor_T(is) = v_rho_box%zcor(is)    ! T=0 case
!c     *************************************************************
      if(inFile%iprint.ge.-1) then
!c
!c     Major printout: core eigenvalues...........................
!c
       write(6,'(''     ///////////////////////////////////'',          &
     &          ''//////////////////////////////////'')')
       write(6,'(''      Eigenvalues:'',t20,''n'',t25,''l'',t30,        &
     &  ''k'',t42,''energy'',6x,''core'',4x,''occ.'')')
       do i=1,v_rho_box%numc(is)
        if(v_rho_box%ec(i,is).eq.zero) cycle
        nelt= (3-nspin)*abs(v_rho_box%kc(i,is))
        indx= v_rho_box%lc(i,is) + abs(v_rho_box%kc(i,is))
        write(6,'(t18,i3,t23,i3,t28,i3,t35,f16.8,3x,i1,a4,i5)')         &
     &    v_rho_box%nc(i,is),v_rho_box%lc(i,is),v_rho_box%kc(i,is),     &
     &             v_rho_box%ec(i,is),v_rho_box%nc(i,is),lj(indx),nelt
       enddo
       write(6,'(''     ///////////////////////////////////'',          &
     &          ''//////////////////////////////////'')')
!c
      end if
!c     *************************************************************
         if ( is==1 ) sublat%compon(ic)%zcor = v_rho_box%zcor(1)
         nullify(v_rho_box)
        end do
       end do
      end do
!
      do nsub=1,nbasis
       sublat => inFile%sublat(nsub)
       do ic=1,komp(nsub)
        v_rho_box => sublat%compon(ic)%v_rho_box
        call findHighestOccupiedCoreLevel(sublat%compon(ic)%zID,        &
     &                                                 v_rho_box)
       end do
      end do
!
      close(nfch)
      if ( allocated(komp) ) deallocate(komp)
      if ( allocated(atcon) ) deallocate(atcon)
      return
!EOC
      end subroutine readCore
!c
      integer function useDirac(z)
      integer, intent(in) :: z
      integer, external :: nPeriod

      logical :: always = .true.
      integer :: stype

      call g_reltype(semicore=stype)
      if ( stype > noSR ) then
       useDirac = nPeriod(z)-1
       if ( stype == noFR ) useDirac = -1
       return
      else if ( stype == noSR ) then
       useDirac = 200
       return
      end if
      if ( always ) then
       useDirac = 200
      else
       useDirac = nPeriod(z)-1
      end if
      return
      end function useDirac

!!BOP
!!!ROUTINE: coreOrder
!!!INTERFACE:
!      subroutine coreOrder(numc,enc,nc,lc,kc)
!!!DESCRIPTION:
!! orders core levels by its eigenvalues {\tt ec}
!!
!
!!!DO_NOT_PRINT
!      implicit none
!!!DO_NOT_PRINT
!
!!!ARGUMENTS:
!      integer, intent(in) :: numc            ! number of core levels
!      real(8) :: enc(numc)                   ! eigenvalues
!      integer :: nc(numc),lc(numc),kc(numc)  ! quantum numbers
!!!REVISION HISTORY:
!! Initial Version - A.S. - 2005
!!EOP
!!
!!BOC
!      real(8) :: aindx(0:numc),ec(0:numc)
!      integer :: itmp(0:numc)
!      integer :: i, nl, i0
!      if(numc.gt.1) then
!       do i=1,numc
!        aindx(i) = dble(i)
!        ec(i) = enc(i)*(dble(1)-abs(epsilon(dble(1))*kc(i)))
!       end do
!       call dsort(ec(1),aindx(1),numc,2)
!       itmp(1:numc) = int(aindx(1:numc))
!       aindx(1:numc) = dble(nc(itmp(1:numc)))
!       nc(1:numc) = nint(aindx(1:numc))
!       aindx(1:numc) = dble(lc(itmp(1:numc)))
!       lc(1:numc) = nint(aindx(1:numc))
!       aindx(1:numc) = dble(kc(itmp(1:numc)))
!       kc(1:numc) = nint(aindx(1:numc))
!       aindx(1:numc) = enc(itmp(1:numc))
!       enc(1:numc) = aindx(1:numc)
!      end if
!      return
!!EOC
!      end subroutine coreOrder

      subroutine findHighestOccupiedCoreLevel(zcor,v_rho_box)
      implicit none
      integer, intent(in) :: zcor
      type(SphAtomDeps), pointer :: v_rho_box

      integer :: nd_sp,nd_lvl,n_hcl
      integer :: n_hocl(2),occ_hocl(2)
      real(8) :: e_hocl(2)
!
      real(8), allocatable :: ec(:,:)
      integer, allocatable :: nc(:,:)
      integer, allocatable :: lc(:,:)
      integer, allocatable :: kc(:,:)
      integer, allocatable :: numc(:)
      integer :: i,nn,zc,nl1,nl2,spl,occ,zc1,zc2

      v_rho_box%highest_occupied_core_level(:,:) = 0
!DEBUG
!      if ( zcor>=0 ) return
!DEBUG
      if ( v_rho_box%numc(1)<=0 ) return
      nd_sp=size(v_rho_box%numc)
      if ( nd_sp==2 .and. v_rho_box%numc(nd_sp)==0 ) nd_sp=1
      nd_lvl=sum(v_rho_box%numc)
      allocate(ec(nd_lvl,nd_sp)) ; ec=zero
      allocate(numc(nd_sp))      ; numc=0
      allocate(nc(nd_lvl,nd_sp)) ; nc=0
      allocate(lc(nd_lvl,nd_sp)) ; lc=0
      allocate(kc(nd_lvl,nd_sp)) ; kc=0

      numc = v_rho_box%numc
!DELETE      ec(1:nd_lvl,1:nd_sp)   = v_rho_box%ec(1:nd_lvl,1:nd_sp)
!DELETE      nc(1:nd_lvl,1:nd_sp)   = v_rho_box%nc(1:nd_lvl,1:nd_sp)
!DELETE      lc(1:nd_lvl,1:nd_sp)   = v_rho_box%lc(1:nd_lvl,1:nd_sp)
!DELETE      kc(1:nd_lvl,1:nd_sp)   = v_rho_box%kc(1:nd_lvl,1:nd_sp)
      n_hocl = 0
      occ_hocl = 0

      if ( nd_sp == 1 ) then
       if ( v_rho_box%ec(1,1)<zero ) then
        ec(1:numc(1),1)   = v_rho_box%ec(1:numc(1),1)
        nc(1:numc(1),1)   = v_rho_box%nc(1:numc(1),1)
        lc(1:numc(1),1)   = v_rho_box%lc(1:numc(1),1)
        kc(1:numc(1),1)   = v_rho_box%kc(1:numc(1),1)

        call coreOrder(numc(1),ec(1:numc(1),1),nc(1:numc(1),1),         &
     &                        lc(1:numc(1),1),kc(1:numc(1),1),n_hcl)
        zc = 0
        n_hocl = n_hcl
        if ( mod(zcor,2)==0 ) then
         occ_hocl = 2*abs(kc(n_hcl,1))
        else
         occ_hocl = abs(kc(n_hcl,1))
        end if

        do i=1,n_hcl
         if ( zc+2*abs(kc(i,1))>=zcor ) then
          n_hocl = i
          occ_hocl = zcor-zc
          exit
         else
          zc = zc+2*abs(kc(i,1))
         end if
        end do

        v_rho_box%highest_occupied_core_level(0,:) = dble(n_hocl)
        v_rho_box%highest_occupied_core_level(1,:) = occ_hocl
        v_rho_box%highest_occupied_core_level(2,:) = ec(n_hocl,1)

       end if
      end if


      if ( nd_sp == 2 ) then
       if ( v_rho_box%ec(1,nd_sp)<zero ) then
        ec(1:numc(1),1:nd_sp)    = v_rho_box%ec(1:numc(1),1:nd_sp)
        nc(1:numc(1),1:nd_sp)    = v_rho_box%nc(1:numc(1),1:nd_sp)
        lc(1:numc(1),1:nd_sp)    = 1
        kc(1:numc(1),1:nd_sp)    = v_rho_box%kc(1:numc(1),1:nd_sp)
        ec(numc(1)+1:nd_lvl,1)   = v_rho_box%ec(1:nd_lvl-numc(1),nd_sp)
        nc(numc(1)+1:nd_lvl,1)   = v_rho_box%nc(1:nd_lvl-numc(1),nd_sp)
        lc(numc(1)+1:nd_lvl,1)   = 2
        kc(numc(1)+1:nd_lvl,1)   = v_rho_box%kc(1:nd_lvl-numc(1),nd_sp)
        ec(numc(1)+1:nd_lvl,1) = ec(numc(1)+1:nd_lvl,1)*0.999999999999d0
!DELETE        allocate(indx(nd_lvl))

        call coreOrder(nd_lvl,ec(1:nd_lvl,1),nc(1:nd_lvl,1),            &
     &                        lc(1:nd_lvl,1),kc(1:nd_lvl,1),n_hcl)
!
        zc = 0
        occ_hocl = abs(kc(n_hcl,1))
        n_hocl = n_hcl
        e_hocl = ec(n_hcl,1)

        nl1 = 0; nl2 = 0
        zc1 = 0; zc2 = 0
        nn = n_hcl
        do i=1,nn
         occ = abs(kc(i,1))
         spl = lc(i,1)
         if ( spl==1 ) then
          nl1=nl1+1
          n_hocl(1) = nl1
          occ_hocl(1) = occ
          e_hocl(1) = ec(i,1)
         else
          nl2=nl2+1
          n_hocl(2) = nl2
          occ_hocl(2) = occ
          e_hocl(2) = ec(i,1)
         end if
         zc  = zc+occ
!         e_hocl = ec(i,1)
         if ( zc==zcor ) then
          occ_hocl(spl) = occ
          exit
         else
          if ( zc>zcor ) then
           occ = zcor-(zc-occ)
           occ_hocl(spl) = occ
           zc = zcor
           exit
          end if
         end if
        end do

!        occ_hocl = occ

        v_rho_box%highest_occupied_core_level(0,:) = dble(n_hocl)
        v_rho_box%highest_occupied_core_level(1,:) = occ_hocl
        v_rho_box%highest_occupied_core_level(2,:) = e_hocl

!DELETE        deallocate(indx)
       end if
      end if
!
!!      write(6,'('' setup HOCL = '',i3)')                                &
!!     &                  nint(v_rho_box%highest_occupied_core_level(0))
!!      write(6,'('' setup OCC  = '',i3)')                                &
!!     &                  nint(v_rho_box%highest_occupied_core_level(1))
!!      write(6,'('' setup ENC  = '',f14.5)')                             &
!!     &                       v_rho_box%highest_occupied_core_level(2)
!
      deallocate(numc,ec,nc,lc,kc)

      return
      end subroutine findHighestOccupiedCoreLevel


      end module zcore

