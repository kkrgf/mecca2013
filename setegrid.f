!BOP
!!ROUTINE: setEgrid
!!INTERFACE:
      subroutine setEgrid( mecca_state, etop_in, Tempr_in )
!!DESCRIPTION:
! generates points in energy complex plane for SCF- or DOS-calculations
!
!!USES:
      use mecca_constants
      use mecca_types
      use mecca_interface
      use egrid_interface, only : conbox

!!DO_NOT_PRINT
      implicit none
!!DO_NOT_PRINT

!!ARGUMENTS:
      type(RunState), intent(inout), target :: mecca_state
      real(8), intent(in), target :: etop_in
      real(8), intent(in), target:: Tempr_in
!!REVISION HISTORY:
! Initial Version - A.S. - 2013
!!REMARKS:
! number of energy points is limited by 10000
!EOP
!
!BOC

      type ( IniFile ), pointer :: inFile
      type (GF_output), pointer :: gf_out
      type (DosBsf_Data), pointer :: dos_box=>null()

      integer :: i,ngrd(2),npts1,npts2,igrid
      integer :: nptsGaussLaguerre
      integer, parameter :: ig0 = 0
      integer, parameter :: ipepts = 10000
      character(*), parameter :: sname='setEgrid'
      integer, pointer :: npts,npar,iprint
      real(8), pointer :: ebot,etop,eitop,eibot
      real(8) :: Tempr
      real(8), target :: ebot0,emin,emax
      real(8) e0,de1,de2,eStartGaussLaguerre
      logical :: rctngl_only
      type(energy_points_t) :: epoints
      real(8), external :: gMaxCoreLevel

      interface
       subroutine conTempr(Tempr,ebot,etop,egrd,wgrd,npts1,npts2,       &
     &                                         iprint,rctngl_only,istop)
       real(8), intent(in) :: Tempr,ebot,etop
       complex(8), intent(out) :: egrd(*)
       complex(8), intent(out) :: wgrd(*)
       integer,  intent(inout) :: npts1,npts2
       integer, intent(in) :: iprint
       logical, intent(in) :: rctngl_only
       character(10), intent(in) :: istop
       end subroutine
      end interface

      ebot0 = zero
      rctngl_only = .false.
      inFile => mecca_state%intfile
      gf_out => mecca_state%gf_out_box
      igrid = inFile%igrid
      npts => inFile%npts
      ebot => inFile%ebot
      etop => etop_in
      eitop => inFile%eitop
      eibot => inFile%eibot
      Tempr = Tempr_in
      npar => inFile%npar
      iprint => inFile%iprint

      emin=zero
      emax=zero
      e0=zero

      if ( associated( mecca_state%bsf_box ) ) then
        dos_box => mecca_state%bsf_box
        if ( inFile%nscf == 0 ) then
         if ( dos_box%bsf_ef == zero ) dos_box%bsf_ef = etop
        end if
        if ( dos_box%idosplot > 0 ) then
         igrid = ig0
         npts => dos_box%dosbsf_nepts
         if ( dos_box%bsf_ef == zero ) then
          e0 = etop
         else
          e0 = dos_box%bsf_ef
         end if
         emin = e0 + dos_box%dosbsf_emin
         emax = e0 + dos_box%dosbsf_emax
         ebot => emin
         etop => emax
         if ( dos_box%dosbsf_esmear > zero ) then
           eitop => dos_box%dosbsf_esmear
           eibot => dos_box%dosbsf_esmear
         end if
        end if
      end if

!c  set up energy mesh..............................................
      call epoints%resize(ipepts)

      select case (igrid)
       case (ig0)
        if ( emin == emax .and. eitop==eibot ) then
         npts=1
         epoints%nume=1
         epoints%egrd(1) = dcmplx(emin,eibot)
         epoints%wgrd(1) = czero
        else
         de1 = (emax-emin)/npts
         de2 = (eitop-eibot)/npts
         call epoints%resize(npts+1)
         do i=1,npts+1
          epoints%egrd(i) = dcmplx(emin+(i-1)*de1,eibot+(i-1)*de2)
          epoints%wgrd(i) = czero
         end do
         epoints%nume = 1+npts
        end if
       case(1)
        npts1 = mod(npts,1000)
        call conbox(ebot,etop,eitop,eibot,zero,epoints%egrd(:),         &
     &              epoints%nume,npar,                                  & ! intel compiler fails with -warn all (without -free),
     &              epoints%wgrd(:),npts1,iprint,inFile%istop)            ! it is similar to issue DPD200236944 in 2013 Compilers Fixes List
        if ( epoints%nume >=1 ) then
         if ( minval(abs(epoints%wgrd(1:epoints%nume))) == zero ) then
          inFile%igrid = 0
          inFile%iprint = max(-1,inFile%iprint)
         end if
        else
         inFile%igrid = 0
         gf_out%nume = 0
        end if
       case(2)
        if ( gf_out%ms_gf == 2 ) then
!TODO         ebot0 = max(0.8d0*gMaxCoreLevel(),inFile%ebot)   !  *** TODO ***
!TODO         ebot => ebot0
!TODO         rctngl_only = .true.
        end if
        gf_out%nume = 0
        call eGridN(mecca_state,ngrd)
        npts1 = ngrd(1)
        npts2 = ngrd(2)
        call epoints%resize(sum(ngrd)+1)
        if ( abs(etop-ebot) >= pi*Tempr ) rctngl_only = .true.
        call conTempr(Tempr,ebot,etop,epoints%egrd,epoints%wgrd,        &
     &                npts1,npts2,iprint,                               & ! intel compiler fails with -warn all (without -free)
     &                rctngl_only,inFile%istop)                                       ! it is similar to issue DPD200236944 in 2013 Compilers Fixes List
        call epoints%resize(npts1+npts2+1)
!!!        if ( gf_out%ms_gf == 2 ) then
        if ( Tempr .ne. zero ) then
!DELETE         if ( epoints%wgrd(epoints%nume-1) == czero ) then
!DELETE          do i=epoints%nume-2,1,-1
!DELETE           if (abs(epoints%wgrd(i)) .ne. czero) then
!DELETE            e0 = epoints%egrd(i+1)
!DELETE            call epoints%resize(i+1)
!DELETE            call conTempr(Tempr,ebot,e0,epoints%egrd,epoints%wgrd,      &
!DELETE     &                npts1,npts2,iprint,                               & ! intel compiler fails with -warn all (without -free)
!DELETE     &                rctngl_only,inFile%istop)                                       ! it is similar to issue DPD200236944 in 2013 Compilers Fixes List
!DELETE            exit
!DELETE           end if
!DELETE          end do
!DELETE         end if
         epoints%egrd(epoints%nume) = dcmplx(etop,                      &
     &                              aimag(epoints%egrd(epoints%nume-1)))          ! last point
        else
         epoints%egrd(epoints%nume) = dcmplx(etop,eitop)          ! last point
        end if
        epoints%wgrd(epoints%nume) = czero
       case(3)
        if ( Tempr == zero ) then
         Tempr = -abs(eitop)
        end if
        gf_out%nume = 0
        npts1 = mod(npts,1000)
        call conboxTempr(Tempr,npts1,ebot,etop,inFile%etop,             &
     &                   epoints%egrd(:),epoints%wgrd(:),               &
     &                   epoints%nume,iprint)
!c
        case(4)
        gf_out%nume         = 0
        npts1               = max(1 ,mod(inFile%npts,1000))                !!! density of points per 0.1Ry
        nptsGaussLaguerre   = max(32,inFile%npts/1000)                     !!! TO DO properly later !!!
        eStartGaussLaguerre = min(max(one,                              &  !!! TO DO
     &                                10*Tempr+etop),                   &
     &                            dble(10000))
        call conUpRightTempr(epoints,Tempr,ebot,eStartGaussLaguerre,    &  !!!
     &                       npts1,nptsGaussLaguerre,                   &
     &                       etop,                                      &  !!! mu
     &                       iprint,6)
!c
       case default
         write(6,*) ' ERROR: igrid = ',igrid
         mecca_state%fail = .true.
         call fstop(sname//':: chosen igrid-option is not implemented')
         return
      end select

      if ( allocated(gf_out%egrd) ) then
        if ( size(gf_out%egrd) < epoints%nume ) deallocate(gf_out%egrd)
      end if
      if ( .not.allocated(gf_out%egrd) )                                &
     &               allocate( gf_out%egrd(1:epoints%nume) )
      if ( allocated(gf_out%wgrd) ) then
        if ( size(gf_out%wgrd) < epoints%nume ) deallocate(gf_out%wgrd)
      end if
      if ( .not.allocated(gf_out%wgrd) )                                &
     &               allocate( gf_out%wgrd(1:epoints%nume) )
      if ( allocated(gf_out%dostspn) ) then
        if ( size(gf_out%dostspn,2) < epoints%nume )                    &
     &               deallocate(gf_out%dostspn)
      end if

      if( epoints%nume < 0) then
        write(6,*) '  NUME=',epoints%nume
        mecca_state%fail = .true.
        call p_fstop(sname//':: nume < 0')
        return
      end if

      if ( .not. allocated(gf_out%dostspn) ) then
        allocate(gf_out%dostspn(1:2,1:epoints%nume))
      end if
      nullify(dos_box)
!
      gf_out%nume = epoints%nume
      gf_out%egrd(1:epoints%nume) = epoints%egrd(1:epoints%nume)
      gf_out%wgrd(1:epoints%nume) = epoints%wgrd(1:epoints%nume)
      gf_out%dostspn(1:2,1:epoints%nume) = 0
!
      call epoints%clear
      return
!EOC
      end subroutine setEgrid

!BOP
!!ROUTINE: eGridN
!!INTERFACE:
      subroutine eGridN(mecca_state,ngrd)
!!DESCRIPTION:
! provides number of grid-points {\tt ngrd(*)} depending
! on internal parameters of {\tt mecca\_state} (type RunState)
!
!!USES:
      use mecca_types
!
!!REVISION HISTORY:
! Initial Version - A.S. - 2013
!EOP
!
!BOC
      implicit none
      type(RunState), intent(in) :: mecca_state
      integer, intent(out) :: ngrd(*)  ! dim[ngrd]>=2
      integer :: igrid,npts,kp
!
      ngrd(1:2) = 0
      if ( .not. associated(mecca_state%intfile) ) return
      igrid = mecca_state%intfile%igrid
      npts = mecca_state%intfile%npts
      if ( npts<=0 .and. igrid==2 ) then
        write(6,*) ' IGRID = ',igrid
        write(6,*) ' ERROR: NPTS = ',npts
        call fstop('egridN :: incorrect NPTS value')
      end if
      select case (igrid)
       case (2)
        if(mecca_state%intfile%Tempr.le.0) then
         ngrd(1) = npts
         ngrd(2) = 0
        else
         ngrd(1) = mod(npts,1000)
         ngrd(2) = npts/1000
         if ( associated(mecca_state%gf_out_box) ) then
          if ( mecca_state%gf_out_box%nume >= ngrd(1) ) then
           kp = (mecca_state%gf_out_box%nume - 1) - ngrd(1)
           if ( kp>ngrd(2) ) then
            ngrd(2) = kp
           end if
          end if
         end if
        end if
       case (4)
         ngrd(1) = 0
         ngrd(2) = 0
       case default
         ngrd(1) = 0
         ngrd(2) = 0
      end select
!
      return
!EOC
      end subroutine eGridN

