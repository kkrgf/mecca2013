!BOP
!!ROUTINE: gettau_ray
!!INTERFACE:
      subroutine gettau_ray(ispin,nspin,nrelv,                          &
!!     &                  icryst,                                       &
     &                  lmax,kkrsz,nbasis,numbsub,                      &
     &                  alat,volume,aij,itype,natom,                    &
     &                  if0,mapstr,mappnt,ndimbas,mapij,                &
     &                  komp,atcon,                                     &
     &                  xknlat,Rksp0,nkns,                              &
     &                  qmesh,ndimq,nmesh,nqpt,                         &
     &                  lwght,lrot,                                     &
     &                  ngrp,kptgrp,kptset,ndrot,kptindx,               &
     &                  sublat,r_ref,                                   &
     &                  rws,                                            &
     &                  rmt_true,r_circ,ivar_mtz,                       &
     &                  fcount,weight,rmag,vj,lVP,                      &
     &                  energy,                                         &
     &                  itmax,rns,nrns,ndimrs,                          &
     &                  np2r,ndimnp,numbrs,naij,                        &
     &                      rot,dop,nop,                                &
     &                  hplnm,ndimrhp,ndimlhp,                          &
     &                  irecdlm,                                        &
     &                  dos,dosck,green,                                &
     &                  eneta,enlim,                                    &
     &                  conk,conr,                                      &
     &                  rslatt,kslatt,Rnncut,Rsmall,mapsprs,            &
     &                  mtasa,ksintsch,efermi,                          &
     &                  iprint,istop                                    &
     &                 ,idosplot,bsf_box,fs_box)
!!DESCRIPTION:
! this subroutine is a basic procedure of getTauE subroutine to calculate
! both KKR-CPA Green's function data needed for scf cycle and DOS/BSF/FS;
!
! it solves the cpa equations using the brillouin-zone integration method
! for calculating tau(0,0)
! ....gms   july 1984
!
!!USES:
      use mecca_constants
      use mecca_types, only : Sublattice,SphAtomDeps
      use mecca_types, only : DosBsf_data,FS_data
      use raymethod
      use sctrng, only : etamin
      use gfncts_interface
      use mecca_interface, only : get0ata,grint,intgrtau,mdos,mgreen
      use mtrx_interface
      use mpi, only : mpi_comm_rank,mpi_comm_world
!
!!REVISION HISTORY:
! Adapated - A.S. - 2013
!!REMARKS:
! this subroutine is used in current MECCA version predominantly when
! ray method integration over the brillouin-zone is required; in SCF
! calculations it is replaced by gettau_scf procedure.
!EOP
!
!BOC
      implicit none
      interface
       subroutine mcpait1(p,tau00,tcpa,tab,                             &
     &                  kkrsz,                                          &
     &                  nbasis,                                         &
     &                  atcon,                                          &
     &                  komp,numbsub,                                   &
     &                  iconverge,nitcpa,                               &
     &                  iprint,istop)
       use mecca_constants
       use mtrx_interface
       implicit none
       complex(8), intent(in) :: p
       complex(8), intent(in) :: tau00(:,:,:)
       complex(8), intent(inout) ::  tcpa(:,:,:)
       complex(8), intent(in) ::  tab(:,:,:,:)
       integer, intent(in) :: kkrsz,nbasis
       real(8), intent(in) ::  atcon(:,:)
       integer, intent(in) :: komp(:),numbsub(:)
       integer, intent(out) :: iconverge
       integer, intent(in) :: nitcpa,iprint
       character(10), intent(in) :: istop
       end subroutine mcpait1
      end interface
      integer, intent(in) :: ispin,nspin,nrelv
!!      integer, intent(in) :: icryst
      integer, intent(in) :: lmax,kkrsz,nbasis,numbsub(nbasis)
      real(8), intent(in) :: alat,volume
      integer, intent(in) :: naij
      real(8), intent(in) :: aij(3,naij)
      integer, intent(in) :: natom,itype(natom)
      integer, intent(in) :: if0(48,natom)
      integer, intent(in) :: ndimbas
      integer, intent(in) :: mapstr(ndimbas,natom),mappnt(ndimbas,natom)
      integer, intent(in) :: mapij(2,naij)
      integer, intent(in) :: komp(nbasis)
      real(8), intent(in) :: atcon(:,:)       ! (ipcomp,nbasis)
      real(8), intent(in) :: xknlat(*),Rksp0
      integer, intent(in) :: nmesh,nkns(nmesh)
      integer, intent(in) :: ndimq
      real(8), intent(in) :: qmesh(3,ndimq,nmesh)
      integer, intent(in) :: nqpt(nmesh)
      integer, intent(in) :: lwght(ndimq,nmesh),lrot(ndimq,nmesh)
      integer, intent(in) :: ngrp(nmesh),kptindx(ndimq,nmesh)
      integer, intent(in) :: ndrot,kptgrp(ndrot+1,nmesh)
      integer, intent(in) :: kptset(ndrot,nmesh)
      type(Sublattice), intent(in), target :: sublat(nbasis)
      real(8), intent(in) :: r_ref(:)  ! reference (screening) radius
      real(8), intent(in) :: rws(nbasis)
      real(8), intent(in) :: rmt_true(:,:)    !  (ipcomp,nbasis)
      real(8), intent(in) :: r_circ(:,:)      !  (ipcomp,nbasis)
      integer, intent(in) :: ivar_mtz
      integer, intent(in) :: fcount(:)          !  (nbasis)
      real(8), intent(in) :: weight(:)        ! (MNqp)
      real(8), intent(in) :: rmag(:,:,:,:,:)  ! (MNqp,MNqp,MNqp,MNF,nbasis)
      real(8), intent(in) :: vj(:,:,:,:,:)    ! (MNqp,MNqp,MNqp,MNF,nbasis)
      logical, intent(in) :: lVP  ! if .true., VP is applied
      complex(8), intent(in) :: energy
      integer, intent(in) :: itmax           ! ATA/CPA control parameter
      integer, intent(in) :: ndimrs,nrns
      real(8), intent(in) :: rns(ndimrs,4)
      integer, intent(in) :: ndimnp
      integer, intent(in) :: np2r(ndimnp,naij),numbrs(naij)
      real(8), intent(in) :: rot(49,3,3)
      integer, intent(in) :: nop
      complex(8), intent(in) :: dop(kkrsz,kkrsz,nop)
      integer, intent(in) :: ndimrhp,ndimlhp
      complex(8), intent(in) :: hplnm(ndimrhp,ndimlhp,*)
      integer, intent(in) :: irecdlm
!
      complex(8) :: dos(0:,:,:)  ! (0:iplmax,ipcomp,nbasis)
      complex(8) :: dosck(:,:)   ! (ipcomp,nbasis)
      complex(8) :: green(:,:,:) ! (iprpts,ipcomp,nbasis)
!
      real(8), intent(in) :: eneta, enlim(2,nmesh-1)
      real(8), intent(in) :: conk
      complex(8), intent(in) :: conr((2*lmax+1)**2)
      real(8), intent(in) :: rslatt(3,3)           ! Alat*rslatt(1..3,*)/(2*pi) -- latt.vect.
      real(8), intent(in) :: kslatt(3,3)           ! Alat*rslatt(1..3,*)/(2*pi) -- latt.vect.
      real(8), intent(in) :: Rnncut,Rsmall         ! at.units
      integer, intent(in) :: mapsprs(:,:)
!
      integer, intent(in) :: idosplot
      type (DosBsf_data), pointer, intent(in) :: bsf_box
      type (FS_data),  pointer, intent(in) :: fs_box

      integer, intent(in) :: mtasa
      integer, intent(in) :: ksintsch ! k-space integration scheme
                                      ! 1 = monkhorst-pack
                                      ! 2 = ray method
      real(8), intent(in) :: efermi
      integer, intent(in) :: iprint
      character(10), intent(in) :: istop
!c     ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
      character(10), parameter :: sname='gettau_ray'
!c     ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!c     FOR STRUCTURE CONSTANTS: use Ewald and/or real-space methods
!c              real and imaginery energy switch, see below
!c     ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!c     switch to use EWALD within "eresw" D.U. of Real E axis and
!c       real-space method beyond "eresw" D.U. of Real E axis.
!c     ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!      real(8), parameter :: eresw=-0.5d0,eimsw=0.5d0   ! see mecca_constants
!c     ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!c
!c
!      integer      ivpoint(ipsublat)
!      integer      ikpoint(ipcomp,ipsublat)
      integer      nitcpa
!c
!      type(SphAtomDeps), pointer :: v_rho_box
!      real*8       vr(iprpts,ipcomp,ipsublat)
!      real*8       xr(iprpts,ipcomp,ipsublat)
!      real*8       rr(iprpts,ipcomp,ipsublat)
!!CALAM      real*8       xstart(ipcomp,ipsublat)
!      real*8       h(ipcomp,ipsublat)
!      real*8       rmt(ipcomp,ipsublat)
!      integer      jmt(ipcomp,ipsublat)
!      integer      jws(ipcomp,ipsublat)
!c      real*8       rns2(iprsn)
!c      real*8       rnstmp(iprsn,3)
!c      real*8       rtmp(3)
      real*8       r2tmp
!c      real*8       scale
!c
      complex*16   powe((2*lmax+1)**2)

      complex*16   dqint1(nrns,2*lmax+1)
      complex*16   dqtmp(0:2*lmax)
      complex*16   tab(kkrsz,kkrsz,size(atcon,1),nbasis)
      complex*16   almat(lmax+1,size(atcon,1),nbasis)

      complex*16   tcpatmp(size(tab,1),size(tab,1),natom)
      integer      itype1(natom),iorig(nbasis)

      complex*16   cotdl(lmax+1,nbasis,size(atcon,1))
!      complex*16   tcpa(size(tab,1),size(tab,1),nbasis)
!      complex(8) :: pzz(size(tab,1),size(tab,1),size(atcon,1),nbasis)
!      complex(8) :: pzz(size(tcpa,1),size(tcpa,1),size(atcon,1),nbasis)
!      complex(8), target ::                                             &
!     &             pzj(size(tab,1),size(tab,1),size(atcon,1),nbasis)
!      complex*16   pzzbl(size(tab,1),size(tab,1),size(atcon,1),         &
!     &                                           size(atcon,1),nbasis)
!      complex*16   tau00(size(tab,1),size(tab,1),nbasis)
!      complex*16   green00(size(tab,1),size(tab,1),nbasis)
      complex(8) :: tcpa(kkrsz,kkrsz,nbasis)
      complex(8) :: pzz(kkrsz,kkrsz,size(atcon,1),nbasis)
      complex(8), target :: pzj(kkrsz,kkrsz,size(atcon,1),nbasis)
      complex(8) ::pzzbl(kkrsz,kkrsz,size(atcon,1),size(atcon,1),nbasis)
      complex(8) :: tau00(kkrsz,kkrsz,nbasis)
      complex(8) :: green00(kkrsz,kkrsz,nbasis)
!cab      complex*16   prel
      complex*16   pmom
      complex*16   edu
      complex*16   pdu
      complex*16   xpr
      complex*16   d00
      complex*16   eoeta
      complex*16   lloydms

      real(8), parameter :: cphot=two*inv_fine_struct_const ! 274.072d0
      real(8) :: clight
!DELETE      real(8) :: V0
      integer imethod,isparse

      ! plotting fermi surface
      integer :: ifermiplot
      real(8), pointer :: fermi_korig(:)
      integer :: fermi_nkpts
      real(8), pointer :: fermi_kpts(:,:)             !(3,ipbsfkpts)
      character(32), pointer :: fermi_kptlabel(:)  !
      character(:), pointer :: fermi_orilabel
      integer :: fermi_gridsampling,fermi_raysampling
!
      real*8 kpnt(3),startk(3),endk(3)
      real*8 displ(3),edge1(3),edge2(3),norm(3)
      real*8 e1len,e2len,dllen,dlang,sint,xyang,th
      real*8 x,y,u,v,du,dv
      integer e1ndiv,e2ndiv
      complex*16 rayp
      real*8 roots(10)
      integer nroots
      integer fermi_fileno


      ! bloch spectral function
      integer :: ibsfplot,bsf_ksamplerate,bsf_nkwaypts
      real(8), pointer :: bsf_kwaypts(:,:)   ! (3,ipbsfkpts)
      character(32), pointer :: bsf_kptlabel(:)  ! (ipbsfkpts)
!
      real*8 :: bsfNL(0:lmax,nbasis)
!      real*8 :: bsfcubic(kkrsz,nbasis)

      real(8) :: bsf,bsfdis,del,ds,dutory,rytodu,dx,raysamplerate,rsp
      real(8) :: time1,time2,xmin,xmax,ymin,ymax
      integer :: i,j,iatom,iconverge,ict,iL,iec,imesh,iprint1,iprint2
      integer :: ic,ir,iswzj,l1,m,n,ndkkr,nraydiv,nsub,nthdiv

!      complex*16 zlab(size(green,1),lmax+1,size(atcon,1))
      complex(8), allocatable, target :: zlab(:,:,:,:)

!      complex*16 zzab(iprpts),izzab(iprpts),pzzab
      complex*16 bjl(lmax+2),bnl(lmax+2)
      complex*16 djl(lmax+2),dnl(lmax+2)
      complex(8), external :: ylag_cmplx


      logical, save :: firstcall = .true.
      logical :: isdis
      logical :: do_ray_int
      logical :: fermi_calc
      logical :: dos_calc      ! dos only
      logical :: bsf_calc      ! bsf only
      logical :: fer_area_calc,fer_rays_calc,fermiS_calc

      logical opnd

      integer, save :: n_ake=0      ! number of E-point (BSF)
      integer, save :: n_akebz=0    ! number of E-point (FS)
      character(10) :: c_n
      character(3)  :: sfx
      character(256):: datfile
      character(80), allocatable :: akefile(:)
      integer :: sz,nrecs
      ! post-processing akeBZlo.dat for fermi output
      integer flb,fub
      integer, parameter :: maxraysamples = 10000
      integer :: ips
      character(len=80) :: akestring
      real*8 :: akeray(3,maxraysamples), halfmax
      logical :: ispeak
      integer :: myrank,ierr
      !!!

      real*8 R2ksp
!      real*8 :: etamin = 0.1d0

      real*8       eta0
      integer      n1,n2
      integer :: st_env

      real*8 lloydform
!      real*8, save :: lastph(12)=0.d0

      real(8), external :: gLattConst

      ndkkr = size(tab,1)

      if ( kkrsz > ndkkr ) then
        write(6,'(a,i3,a,i3,a,a)') ' ERROR :: kkrsz=',kkrsz             &
     &   ,' cannot be larger than ndkkr=',ndkkr,'; to change ndkkr,'    &
     &   ,' code re-compilation with a new lmax-limit is required.'
        call fstop(sname// ' :: kkrsz is too big')
      end if
      call timel(time2)

      ! is the system disordered?
      isdis = .false.
      do n = 1, nbasis
        if(komp(n)>1) isdis = .true.
      end do
!
      if ( associated(bsf_box) ) then
       ibsfplot = bsf_box%ibsfplot
       bsf_ksamplerate = bsf_box%bsf_ksamplerate
       bsf_nkwaypts = bsf_box%bsf_nkwaypts
       bsf_kwaypts  =>  bsf_box%bsf_kwaypts
       bsf_kptlabel =>  bsf_box%bsf_kptlabel
       if ( .not.allocated(zlab) .and. ibsfplot>0 ) then
        if ( ibsfplot==2 .and. .not.isdis ) then
         continue
        else
         allocate(zlab(size(green,1),lmax+1,size(atcon,1),nbasis))
        end if
       end if
      else
       ibsfplot = 0
       bsf_ksamplerate = 0
       bsf_nkwaypts = 0
      end if

      if ( associated(fs_box) ) then
       ifermiplot = fs_box%ifermiplot
       fermi_nkpts = fs_box%fermi_nkpts
       fermi_gridsampling = fs_box%fermi_gridsampling
       fermi_raysampling = fs_box%fermi_raysampling
       fermi_korig => fs_box%fermi_korig
       fermi_kpts => fs_box%fermi_kpts
       fermi_kptlabel => fs_box%fermi_kptlabel
       fermi_orilabel => fs_box%fermi_orilabel
       if ( .not.allocated(zlab) .and. ifermiplot>0 ) then
        allocate(zlab(size(green,1),lmax+1,size(atcon,1),nbasis))
       end if
      else
       ifermiplot = 0
       fermi_gridsampling = 0
       fermi_raysampling = 0
       fermi_nkpts = 0
      end if

      if ( .not.allocated(zlab) ) then
       allocate(zlab(size(green,1),lmax+1,size(atcon,1),1))
      end if

      do_ray_int = ksintsch.ne.ksint_scf_sch
      fermi_calc = ifermiplot>0
      dos_calc =  idosplot.ne.0
      bsf_calc =  ibsfplot.ne.0

      fer_area_calc = ifermiplot.eq.1
      fer_rays_calc = ifermiplot.eq.2    ! fer_rays and fermiS cannot be combined
      fermiS_calc = ifermiplot>2         !

      clight = cphot
      if ( nrelv>1 ) then
       clight = cphot*ten**nrelv
      end if

!c     ================================================================
!c     Note:
!c             In relativistic case p=sqrt[e(1+e/cphot**2)]
!
!c     ================================================================
!c     calculate things that depend only on energy
!      pnrel = cdsqrt(energy)
!c
!c                Im(pnrel) must be .GE. zero
!c
!      pnrel = pnrel*sign(1.d0,aimag(pnrel))

      pmom = cdsqrt(2*m_e*energy*(one + energy/(2*m_e*clight**2)))
      pmom = pmom*sign(1.d0,aimag(pmom))

!c     energy and momentum in dimensionless units (DU).

      rytodu = gLattConst(1)/(two*pi)
      dutory = one / rytodu

      edu =energy*rytodu**2
      pdu = pmom*rytodu
!c
!c     switch for getting irregular part of G(E) at complex E, i.e. ZJ
      iswzj=1
!c     --------------------------------------------------------------
      if(iprint.ge.1) then
       write(6,'(a10,'':  edu,pdu='',1x,''('',f9.5,'','',f9.5,'' ),'',  &
     &                              1x,''('',f9.5,'','',f9.5,'' )'')')  &
     &                      sname,edu,pdu
      endif
!c     --------------------------------------------------------------

      call pickmesh(edu,enlim,nmesh,imesh)
      call DefMethod(1,imethod,isparse)
!DELETE      if ( imethod == 0 ) then
!DELETE        V0 = zero
!DELETE      else
!DELETE        V0 = U0
!DELETE      end if
!DELETE      if(imethod.le.zero.or.imethod.eq.4) then    ! For K-space calculations
      if(imethod.ne.3) then    ! For K-space calculations

       eta0 = max(etamin,abs(edu)/eneta)
!c                                    to be sure in accuracy of D00
       if( irecdlm.eq.0 ) then
         R2ksp = max(zero,eta0*Rksp0*Rksp0+dreal(edu))
         ! rooeml --> sqrt(e^-l)
         call rooeml(pdu,powe,lmax,iprint)
         call d003(edu,eta0,d00)
         eoeta=exp( edu/eta0 ) * conk
!c
!c ==================================================================
!c  calculate the real-space integral used in Ewald technique.
!c  This is energy dependent, but k-space independent.
!c  DO once for each energy and store results in upper triangular
!c  form. INTFAC calculated the integral and returns in DQINT.
!c ==================================================================
!c
       else
!         removed for debugging
!         if(aimag(edu).lt.0.d0)
!     *   call fstop(sname//': Im(E)<0')
!c
!c ==================================================================
!c  calculate the hankel fct used in real-space calculation of
!c   structure constants out in complex plane.
!c  This is energy dependent, but k-space independent.
!c  DO once for each energy and store results in upper triangular
!c  form. HANKEL calculated the fct and returns in DQINT.
!c ==================================================================
!c
!c  do not start a hankel with zero vector length (ir=1)...blows up!
!c  only true for diagonal block, of course.
!c
       endif

!c ==================================================================
          rsp = -two
          do i=1,nrns
!c          rtmp(1)= rns(i,1)
!c          rtmp(2)= rns(i,2)
!c          rtmp(3)= rns(i,3)
           r2tmp=   rns(i,4)
           if( abs(r2tmp-rsp) .ge. 1.0d-6 ) then
             rsp = r2tmp
             if(irecdlm.eq.0) then
                call intfac(rsp,edu,eta0,2*lmax,dqtmp)
             else
                if(r2tmp.le.1.0d-6) then
                 do l1=0,2*lmax
                  dqtmp(l1) = dcmplx(zero,zero)
                 enddo
                else
                 xpr=pdu*sqrt(rsp)
                 call hankel(xpr,dqtmp,2*lmax)
                end if
             end if
             do l1=0,2*lmax
                dqint1(i,l1+1)=dqtmp(l1)
             enddo
           else
             if ( i==1 ) then
              call fstop(sname//': MEMORY BUG???')
             end if
             do l1= 1,2*lmax+1
              dqint1(i,l1)=dqint1(i-1,l1)
             enddo
           endif
          enddo

      else
         if(iprint.ge.0) then
          write(6,1002) Rsmall,Rnncut,imesh,nqpt(imesh)
1002      format(' REAL SPACE *****    Rsmall=',f6.3,                   &
     &          '  Rnncut=',f6.3,' NQPT(',i1,')=',i7)
         endif
      endif

!c ==================================================================
!c

       do nsub=1,nbasis
        call gettab(nsub)  ! output: zlab,tab,pzj,pzz,cotdl,almat
       end do

!c        write(6,*) 't-matrix at en = ', energy
!c        do i=1,kkrsz
!c          write(6,*) tab(i,i,1,1)
!c        end do
!c
!c     =================================================================
!c     construct the average t-matrix..................................
!c
      call get0ata(atcon,tab,tcpa,kkrsz,komp,nbasis,iprint,istop)
!c     -----------------------------------------------------------------

      do nsub=1,nbasis
        do iatom=1,natom
         if(itype(iatom).eq.nsub) go to 10
        end do
        call fstop(sname// ' :: You have wrong ITYPE')
10      iorig(nsub) = iatom

!c!!!
!c  it is supposed below  that "iorig(nsub)" -- number of 1st site of
!c  type "nsub" for isite=1,natom
!c!!!
      end do

      do iatom=1,natom
       itype1(iatom) = iatom
      end do

!C!----------------------------------------------------------------------
!C!  Do not change the array itype1(*) below this line; this array is used
!C!  to treat equivalent sites as inequivalent (when it is needed)
!C!----------------------------------------------------------------------

!c     =================================================================

      iconverge = 0
      nitcpa = 0
      if(itmax.eq.-1) then          !  ATA SCF solution

       tau00 = tcpa

      else                          !  CPA SCF

       do ict = 1, max(1,abs(itmax))
         nitcpa=ict
         if(ict.gt.1) then
          iprint1 = iprint-2
          iprint2 = 1000
         else
          iprint1 = iprint
          iprint2 = iprint
         end if

         if(ict.ne.1) then
          if(ngrp(1).gt.1                                               &
     &       .or.lwght(kptindx(kptgrp(1,1),1),1).gt.1                   &
     &      ) then
!c
!c  Symmetry of tcpa-matrix may be destroyed due to numerical errors,
!c  to maintain the proper symmetry for each CPA-iteration
!c
           call symmrot(ngrp(imesh),kptgrp(1,imesh),                    &
     &                  kptset(1,imesh),kptindx(1,imesh),               &
     &                  lwght(1,imesh),lrot(1,imesh),                   &
     &                  tcpa,size(tcpa,1),kkrsz,                        &
     &                  if0,dop,nbasis,                                 &
     &                  iorig)
          end if
         end if
!c
!c  Tcpa-matrices for equivalent sites can differ !!!
!c
         call tcpa2all(natom,nbasis,iorig,itype,numbsub,if0,nop,        &
     &                   kkrsz,ndkkr,dop,tcpa,tcpatmp,iprint2)

         ! do not perform integral for ordered system BSF calc.
!         if( (istop /= 'AKEBZ_CALC' .and. istop /= 'FERMI_CALC' .and.   &
!     &         istop /= 'BSF_CALC') .or. isdis ) then
          if( ksintsch == ksint_scf_sch ) then
!          if( ksintsch == 1 .or.                                        &
!     &       (ksintsch == 3.and.lastdos > hybrid_cutoff) ) then

           ! BZ integration using special k-pt method
!           write(6,'(a,f7.3,a,f7.3,a)') '     Performing k-space '
!     >       //'integral @ en = (', real(energy),',',aimag(energy),')'
           if(iprint.ge.0.and.ict==1) then
             if( irecdlm.eq.0 ) then
              if( R2ksp>0 ) then
               write(6,1000) eta0,sqrt(R2ksp),imesh,nqpt(imesh)
1000           format(' USING EWALD *****    ETA=',f8.5,                &
     &          '  Rksp=',f12.5,' NQPT(',i1,')=',i7)
              end if
             else
              write(6,1001) imesh,nqpt(imesh)
1001          format(' USING HANKEL *****   NQPT(',i1,')=',i7)
             end if
           end if

           n2 = 3*sum(nkns(1:imesh))
           n1 = n2 - 3*nkns(imesh)+1

           call intgrtau(                                               &
     &               lmax,kkrsz,nbasis,numbsub,atcon,size(atcon,1),komp,&
     &                  rytodu,aij,itype,natom,iorig,itype1,            &
     &                  if0,mapstr,mappnt,ndimbas,mapij,                &
     &                  powe,                                           &
     &                  edu,pdu,                                        &
     &                  irecdlm,                                        &
     &                  rns,ndimrs,                                     &
     &                  np2r,ndimnp,numbrs,naij,                        &
     &                  dqint1,size(dqint1,1),                          &
     &                  hplnm,ndimrhp,ndimlhp,                          &
     &                  tcpatmp,                                        &
     &                  tau00,                                          &
     &                  dop,nop,d00,eoeta,                              &
     &                  eta0,                                           &
     &                  reshape(xknlat(n1:n2),[nkns(imesh),3]),         &
     &                  nkns(imesh),R2ksp,conr(:),nkns(imesh),          &
     &                  qmesh(1,1,imesh),nqpt(imesh),                   &
     &               lwght(1,imesh),lrot(1,imesh),                      &
     &   ngrp(imesh),kptgrp(1,imesh),kptset(1,imesh),kptindx(1,imesh),  &
     &                  rslatt,Rnncut,Rsmall,mapsprs,                   &
     &                  r_ref,                                          &
     &                  iprint1,istop)
           do_ray_int = .false.

!            write(6,*) 'itcpa = ', ict,'   ispin = ', ispin
!            write(6,*) 'tau00 = ', tau00(1:kkrsz,1:kkrsz,1)

          else

           ! BZ integration using radial wedges in spherical coordinates
!          write(6,'(a,f7.3,a,f7.3,a)') '     Performing k-space '
!     >       //'integral @ en = (', real(energy),',',aimag(energy),')'

           tcpatmp = dutory * tcpatmp
             ! remember that tcpa has units of 1/sqrt(energy)
             ! so we are *really* changing to DU here

!           write(6,*) 'd00=', d00, eoeta


           ! find diagonal blocks of tau = [t^-1 - G0]^-1
           imesh = 1
           n2 = 3*sum(nkns(1:imesh))
           n1 = n2 - 3*nkns(imesh)+1

           call ksrayint( lmax,kkrsz,natom,aij,mapstr(1:natom,1:natom), &
     &                               mappnt(1:natom,1:natom),           &
     &         nbasis,mapij,powe,edu,                                   &
     &         pdu,rns,ndimrs,np2r,ndimnp,numbrs,naij,dqint1,           &
     &  size(dqint1,1),hplnm,ndimrhp,ndimlhp,irecdlm,d00,eoeta,eta0,    &
     &         reshape(xknlat(n1:n2),[nkns(imesh),3]),nkns(imesh),conr, &
     &                 tcpatmp,iorig,itype,kslatt,nop,rot,dop,if0,      &
     &         tau00,lloydms,green00 )

           tau00 = rytodu * tau00
           green00 = rytodu * green00
           tcpatmp = rytodu * tcpatmp
           do_ray_int = .true.

!           write(6,*) 'tau00 = ', tau00(1:kkrsz,1:kkrsz,1)

          end if

         ! if a random alloy then iterate cpa
         if(abs(itmax)>1) then

           if(ict.eq.itmax) then
            iprint1 = max(0,iprint)
           else
            iprint1 = iprint
           end if

           call mcpait1(pdu,tau00,tcpa,tab,                             &
     &                  kkrsz,                                          &
     &                  nbasis,                                         &
     &                  atcon,                                          &
     &                  komp,numbsub,                                   &
     &                  iconverge,nitcpa,                               &
     &                  iprint1,istop)

!c           -----------------------------------------------------------


         else ! no CPA case

            iconverge=0

         endif

         if( iconverge<=0 ) go to 100

       end do

      end if

100   continue

      if( iconverge >0  .and.                                           &
     &   .not.(bsf_calc.or.dos_calc.or.fermi_calc)                      &
     &   ) then
! EBOT_CALC can be done with itmax = 1
!     &  istop/='DOS_CALC' .and.                                         &
!     &  istop/='BSF_CALC' .and.                                         &
!     &  istop/='DOSBSF_CAL' .and.                                       &
!     &  istop/='EBOT_CALC' ) then

!c        CPA failed to converge.......................................

        write(6,*)
        write(6,*) ' ICONVERGE=',iconverge
!        write(6,'('' gettau:: CPA failed to converge in '',i3,          &
!     &          '' iterations'')') itmax
        if(float(iconverge)/float(nbasis).gt.0.5.and.itmax.gt.0) then
         write(6,'(a,'':: CPA failed to converge in '',i3,              &
     &          '' iterations, energy='',2es15.6)') sname,itmax,energy
!         write(*,1003) sname,energy
!1003     format(/a,':: ERROR  no CPA convergence, energy=',2d20.12/)
          write(*,'(                                                    &
     &'' Use of negative CPA control parameter for max.number of''/     &
     &'' iterations allows to continue with calculations,''/            &
     &'' but if you see this message after last SCF iteration, then''   &
     &'' results are inaccurate or wrong.''/                            &
     &   )')
!     &'  You have a problem with CPA convergence:'/                     &
!     &'  It is very likely that you have numerical instability'/        &
!     &'  (inaccuracy in symmetry+CPA --> lack of convergence).'/        &
!     &'  For such cases we have never had problems with full zone',     &
!     &   ' integration.'/                                               &
!     &'  We do not know a good recipe to find CPA solution in',         &
!     &   ' general case.'/                                              &
         call fstop(sname//' :: change CPA-parameter in input file')
        else if ( iconverge<0 ) then
         write(6,'(a,'' WARNING :: CPA is unable to converge'')')
        else if(itmax.gt.0) then
         write(6,*)                                                     &
     &  (' You have no convergence for less then 50% of sublattices')
        end if
        write(6,*)
      else if( iconverge.ne.0 .and. itmax > 1 ) then
       if ( iconverge<0 ) then
         if ( nitcpa>4 .or. iprint>0 ) then
         write(6,'(a,'' WARNING :: CPA is unable to converge with'',    &
     &     '' requested tolerance, cpa iteration#'',i3,'', energy='',   &
     &     2es15.6)') sname,nitcpa,energy
         end if
       else
         write(6,'(a,i3,a)') '     Using best coherent potential in ',  &
     &          nitcpa,' iterations.'
       end if
      end if

!c     ===========================================================
!c     cpa iteration converged or the case of an ordered system...
!c     write(6,'('' gettau:: cpa converged : calling getdos'')')
!c     ===========================================================
!c     calculate the density of states n(e).......................
!c     -----------------------------------------------------------

!      return

!      if( (istop=='DOS_CALC'.or.istop=='BSF_CALC'                       &
!     & .or.istop=='DOSBSF_CALC')) then
      if ( do_ray_int ) then
!       if ( dos_calc .or. bsf_calc .or. dosbsf_calc ) then
       if ( dos_calc ) then

        call addlloydcorr(natom,nbasis,iorig,itype,komp,numbsub,atcon,  &
     &   ispin,nspin,energy,efermi,kkrsz,lmax,nrelv,cotdl,almat,tab,    &
     &   tcpatmp,tau00,                                                 &
     &   lloydms,green00,rytodu,volume,lloydform )
!        write(6,*) 'lloydms = ', lloydms, 'lloydform = ', lloydform

       end if
      end if

      if(itmax > 1 .and. iconverge.eq.0) then
!         write(6,'(a,i3,a)') '     Found coherent potential in ',
!     >     nitcpa,' iterations.'
      end if

!      if( (istop /= 'AKEBZ_CALC' .and. istop /= 'FERMI_CALC' .and.      &
!     &         istop /= 'BSF_CALC') .or. isdis ) then

      call getdos(pmom)

!TODO: Everything related to bsf_calc.or.dosbsf_calc.or.akebz_calc.or.fermi_calc
!      could be in a separate file(s)

      if ( bsf_calc.or.fermi_calc ) then
       call mpi_comm_rank(mpi_comm_world,myrank,ierr)
        if ( nspin==1 ) then
         sfx = '   '
        else if ( ispin==1 ) then
         sfx = '_up'
        else
         sfx = '_dn'
        end if

       if ( ibsfplot==2 .and. .not.isdis ) then
        continue
       else
         ! suffian
         ! Need mixed component entries of pzz for a calculation
         ! of the Bloch Spectral Fn A(k,E)
        call calc_pzzbl(pmom,pzzbl)
       end if
!          write(6,*) 'is = ', ispin
!          write(6,*) 'pzzbl(1,1) =', ispin
!          write(6,*) pzzbl(1:kkrsz,1:kkrsz,1,1,2)
!          write(6,*) 'pzzbl(1,2) =', ispin
!          write(6,*) pzzbl(1:kkrsz,1:kkrsz,1,2,2)
!          write(6,*) 'pzzbl(2,1) =', ispin
!          write(6,*) pzzbl(1:kkrsz,1:kkrsz,2,1,2)
!          write(6,*) 'pzzbl(2,2) =', ispin
!          write(6,*) pzzbl(1:kkrsz,1:kkrsz,2,2,2)
!

       write(datfile,'(i10)') myrank
       c_n = trim(adjustl(datfile))
! 'BSF_CALC'
       if ( bsf_calc ) then
        n_ake = n_ake+1
        imesh = 1
        n2 = 3*sum(nkns(1:imesh))
        n1 = n2 - 3*nkns(imesh)+1

        if ( n_ake==1 ) then
         datfile = ''
         if ( nspin==1 ) then
          write(datfile,'(a)') c_ake_band//trim(c_n)//'.dat'          ! be careful with parallelization
          open(nu_bsf,file=trim(datfile),status='replace',iostat=iec)
          close(nu_bsf)
         else
          write(datfile,'(a)') c_ake_band//trim(c_n)//'_up'//'.dat'   ! be careful with parallelization
          open(nu_bsf,file=trim(datfile),status='replace',iostat=iec)
          close(nu_bsf)
          write(datfile,'(a)') c_ake_band//trim(c_n)//'_dn'//'.dat'   ! be careful with parallelization
          open(nu_bsf,file=trim(datfile),status='replace',iostat=iec)
          close(nu_bsf)
         end if
        end if
        datfile = ''
        write(datfile,'(a)') c_ake_band//trim(c_n)//trim(sfx)//'.dat'   ! be careful with parallelization
!DELETE         if ( n_ake==1 ) then
!DELETE         open(nu_bsf,file=trim(datfile),status='replace',               &
!DELETE     &     iostat=iec)
!DELETE         else
!DELETE         end if
!DELETE        if ( energy .ne. en_ake(ispin) ) then
!DELETE          en_ake(ispin) = energy
!DELETE          open(nu_bsf,file=trim(datfile),status='old',                   &
!DELETE     &     position='append',iostat=iec)
!DELETE          write(nu_bsf,'(a,2g14.4)') '# E = ',energy
!DELETE          close(nu_bsf)
!DELETE        end if

        do i = 2, bsf_nkwaypts
          ds = ( bsf_kwaypts(1,i) - bsf_kwaypts(1,i-1) ) *              &
     &         ( bsf_kwaypts(1,i) - bsf_kwaypts(1,i-1) ) +              &
     &         ( bsf_kwaypts(2,i) - bsf_kwaypts(2,i-1) ) *              &
     &         ( bsf_kwaypts(2,i) - bsf_kwaypts(2,i-1) ) +              &
     &         ( bsf_kwaypts(3,i) - bsf_kwaypts(3,i-1) ) *              &
     &         ( bsf_kwaypts(3,i) - bsf_kwaypts(3,i-1) )
          ds = dsqrt(ds)
          n = ceiling(ds*bsf_ksamplerate)
          dx = 1.d0/n

          x = dx
          if( i == 2 ) then
            n = n + 1; x = 0.d0; y = -dx*ds
          end if

          open(nu_bsf,file=trim(datfile),status='old',                  &
     &     position='append',iostat=iec)
          write(nu_bsf,'(a,3f7.3,1x,a,1x,a,2g14.4)')                    &
     &                 '# from ', bsf_kwaypts(:,i-1)                    &
     &                 ,bsf_kptlabel(i-1),' at E = ',energy
          write(nu_bsf,'(a,3f7.3,1x,a)') '#  to  ', bsf_kwaypts(:,i)    &
     &                          ,bsf_kptlabel(i)
          close(nu_bsf)

          if ( ibsfplot==1 ) then

            allocate(akefile(n))
            do j = 1, n

              kpnt(:) = (1.d0-x)*bsf_kwaypts(:,i-1)                     &
     &                  + x*bsf_kwaypts(:,i)
              x = x + dx
              y = y + dx*ds

              rayp = x
              call getblochspecfn(                                      &
     &          natom,nbasis,iorig,itype,komp,numbsub,atcon,            &
     &          energy,kkrsz,tab,tcpatmp,tau00,pzzbl,pzj,               &
     &          bsf_kwaypts(:,i-1),bsf_kwaypts(:,i),rayp,kpnt,bsf,bsfNL,&
!     &          bsfcubic,bsfdis,alat,                                   &
     &                   bsfdis,rytodu,                                 &
     &         lmax,aij,mapstr(1:natom,1:natom),mappnt(1:natom,1:natom),&
     &                                 mapij,powe,                      &
     &          edu,pdu,rns,ndimrs,np2r,ndimnp,numbrs,naij,             &
     &          dqint1,size(dqint1,1),hplnm,ndimrhp,ndimlhp,            &
     &          irecdlm,d00,eoeta,                                      &
     &          eta0,reshape(xknlat(n1:n2),[nkns(imesh),3]),nkns(imesh),&
     &          conr,nop,rot,dop,if0)
              write(akefile(j),'(3f20.10)')                             &
     &               y, dreal(energy)-efermi, bsf

!  uncomment to show cubic projection of A(k,E)
!   ... be sure to add advance='no' to above write
!              do in=1,nbasis
!              do il=1,kkrsz
!              if(in/=nbasis.or.il/=kkrsz) then
!                write(nu_bsf,'(1f15.7)',advance='no') bsfcubic(il,in)
!              end if; end do; end do
!              write(nu_bsf,'(1f15.7)') bsfcubic(kkrsz,nbasis)

!              do in=1,nbasis
!              do il=0,lmax
!              if(in/=nbasis.or.il/=lmax) then
!                write(nu_bsf,'(1f15.7)',advance='no') bsfNL(il,in)
!              end if; end do; end do
!              write(nu_bsf,'(1f15.7)') bsfNL(lmax,nbasis)


            end do
            y=0;x=0;bsf=0
            open(nu_bsf,file=trim(datfile),status='old',                &
     &                  position='append',iostat=iec)
            do j=1,n
             read(akefile(j),'(3f20.10)') y,x,bsf
             write(nu_bsf,'(3f20.10)') y,x,bsf
            end do
            close(nu_bsf)
            deallocate(akefile)
          else if ( ibsfplot==2 .and. .not.isdis ) then

            ! for ordered crystal, just find ||tau(k,E)^-1|| = 0
            call findblochstate(                                        &
     &        natom,nbasis,iorig,itype,komp,numbsub,atcon,              &
     &        energy,kkrsz,tab,tcpatmp,tau00,pzj,                       &
     &       bsf_kwaypts(:,i-1),bsf_kwaypts(:,i),roots(1),nroots,rytodu,&
     &        lmax,aij,mapstr(1:natom,1:natom),mappnt(1:natom,1:natom), &
     &                                 mapij,powe,                      &
     &        edu,pdu,rns,ndimrs,np2r,ndimnp,numbrs,naij,               &
     &        dqint1,size(dqint1,1),hplnm,ndimrhp,ndimlhp,              &
     &        irecdlm,d00,eoeta,                                        &
     &        eta0,reshape(xknlat(n1:n2),[nkns(imesh),3]),nkns(imesh),  &
     &        conr,nop,rot,dop,if0)

            open(nu_bsf,file=trim(datfile),status='old',                &
     &                  position='append',iostat=iec)
            do j = 1, nroots
              x = roots(j)
              write(nu_bsf,'(2f20.10)') y+x*ds, real(energy)-efermi
            end do
            close(nu_bsf)
            y = y + ds

          end if

        end do
        write(6,'(a,f8.4,a)') '     Completed A(k,E)'//trim(sfx)//      &
     &          ' scan at '//'@ Re(E) = (',real(energy),')'
!        write(nu_bsf,*) ''
!
!        close(nu_bsf)

       end if   ! bsf_calc

       if ( fermi_calc ) then
        if ( myrank.ne.0 ) then
       call fstop('Multiprocessing is not supported in FS calculations')
        end if
       end if
! 'AKEBZ_CALC'
       if ( fer_area_calc ) then  ! akeBZarea.dat file is generated

        n_akebz = n_akebz+1

        imesh = 1
        n2 = 3*sum(nkns(1:imesh))
        n1 = n2 - 3*nkns(imesh)+1
        datfile = ''
        write(datfile,'(a)') c_ake_bz_area//trim(sfx)//'.dat'

        if ( n_akebz==1 ) then
         open(nu_fs1,file=trim(datfile),status='replace',iostat=iec)
         write(nu_fs1,'(a,2g17.7)') '# E = ',energy
         open(unit=11,file=c_ake_bz_area//trim(sfx)//'.gnu',            &
     &        status='replace',iostat=iec)
        else
         open(nu_fs1,file=trim(datfile),status='old',                   &
     &     position='append',iostat=iec)
        open(unit=11,file=c_ake_bz_area//trim(sfx)//'.gnu',             &
     &        status='old',position='append',iostat=iec)

        end if

        ! first make a gnuplot script for easy viewing
        write(11,'(a)') 'unset xtics; unset ytics'
        write(11,'(a)') 'unset colorbox; unset key; unset border'
        write(11,'(a)') 'set size square'
        write(11,'(a)') 'set pm3d map'
        write(11,'(a)') 'set palette define (0 "white", 100 "black"'    &
     &    //', 1000 "black")'
        write(11,'(a)') 'set title "A(k,Efermi) plot" offset 0.0,1.0'
        write(11,'(a)') 'set zrange [0:1000]'

         xyang = 0.d0
         do i = 2, fermi_nkpts

            write(nu_fs1,'(a,3f7.3)') '# from ', fermi_kpts(:,i-1)
            write(nu_fs1,'(a,3f7.3)') '#  to  ', fermi_kpts(:,i)

            edge1(:) = -fermi_korig(:) + fermi_kpts(:,i-1)
            edge2(:) = -fermi_korig(:) + fermi_kpts(:,i)

            e1len = dsqrt(dot_product(edge1,edge1))
            e2len = dsqrt(dot_product(edge2,edge2))
            if ( e1len*e2len>zero ) then
             call vectprd(edge1,edge2,norm)
             sint = dsqrt(dot_product(norm,norm))/(e1len*e2len)
            else
             write(nu_fs1,'(a)') '# equivalent k-points detected'
             cycle
            end if
            th = dasin(sint)

            ! for gnuplot script

            if( i == 2 ) then

              write(11,'(a,a,a,f7.3,a,f7.3,a)') 'set label "',          &
     &          trim(fermi_orilabel),'" at ',                           &
     &          0.d0,',',0.d0,                                          &
     &          ' front center offset 0.0,-1.0'

              write(11,'(a,a,a,f7.3,a,f7.3,a)') 'set label "',          &
     &          trim(fermi_kptlabel(1)),'" at ',                        &
     &          e1len,',',0.d0,                                         &
     &          ' front center offset 0.0,-1.0'

              write(11,'(a,f7.3,a,f7.3,a)')                             &
     &          'set arrow from 0.0,0.0 to ',                           &
     &          e1len,',',0.d0,                                         &
     &          ' front nohead'

            end if

            write(11,'(a,a,a,f7.3,a,f7.3,a,f7.3,a,f7.3)')               &
     &        'set label "',                                            &
     &        trim(fermi_kptlabel(i)),'" at ',                          &
     &        e2len*dcos(xyang+th),',',e2len*dsin(xyang+th),            &
     &        ' front center offset ',                                  &
     &        dcos(xyang+th),',',dsin(xyang+th)

            write(11,'(a,f7.3,a,f7.3,a)')                               &
     &        'set arrow from 0.0,0.0 to ',                             &
     &        e2len*dcos(xyang+th),',',e2len*dsin(xyang+th),            &
     &        ' front nohead'

            write(11,'(a,f7.3,a,f7.3,a,f7.3,a,f7.3,a)')                 &
     &        'set arrow from ',                                        &
     &        e1len*dcos(xyang),',',e1len*dsin(xyang),' to ',           &
     &        e2len*dcos(xyang+th),',',e2len*dsin(xyang+th),            &
     &        ' front nohead'

            if( fermi_gridsampling < 10 ) then
              write(6,'(a,i5,a)') 'warning: ake samples/(2pi/a) = ',    &
     &          fermi_gridsampling, ' is low'
            end if

            e1ndiv = ceiling( e1len*fermi_gridsampling*dsqrt(sint) )
            if ( e1ndiv > 0 ) then
             du = 1.d0/e1ndiv
            else
             du = one
            end if

            u = 0.d0
            do n = 1, e1ndiv
              u = u + du

              e2ndiv = ceiling( e2len*(1.d0-u)*                         &
     &          fermi_gridsampling*dsqrt(sint) )
              if ( e2ndiv > 0 ) then
               if ( n < e1ndiv ) then  ! u(e1ndiv) = 1
                dv = (1.d0-u)/e2ndiv
               else
                dv = 0.d0
               end if
              else
               cycle
              end if

              startk = fermi_korig + u*edge1
              endk   = fermi_korig + u*edge1 + (1.d0-u)*edge2

              v = 0.d0
              do m = 1, e2ndiv
                v = v + dv
                if( u + v > 1.0d0 ) exit
                displ = u*edge1 + v*edge2
                dllen = dsqrt(dot_product(displ,displ))
                if ( e1len*dllen>zero ) then
                 x = dot_product(displ/dllen,edge1/e1len)
                 dlang = dacos(x)
                else
                 cycle
                end if
                x = dllen * dcos(xyang + dlang)
                y = dllen * dsin(xyang + dlang)
                rayp = v/(1.d0-u)

                kpnt  = fermi_korig + displ
                call getblochspecfn(                                    &
     &           natom,nbasis,iorig,itype,komp,numbsub,atcon,           &
     &           energy,kkrsz,tab,tcpatmp,tau00,pzzbl,pzj,              &
!     &           startk,endk,rayp,kpnt,bsf,bsfNL,bsfcubic,bsfdis,alat,  &
     &           startk,endk,rayp,kpnt,bsf,bsfNL,         bsfdis,rytodu,&
     &        lmax,aij,mapstr(1:natom,1:natom),mappnt(1:natom,1:natom), &
     &                                 mapij,powe,                      &
     &                edu,pdu,rns,ndimrs,np2r,ndimnp,numbrs,naij,       &
     &           dqint1,size(dqint1,1),hplnm,ndimrhp,ndimlhp,           &
     &           irecdlm,d00,eoeta,                                     &
     &          eta0,reshape(xknlat(n1:n2),[nkns(imesh),3]),nkns(imesh),&
     &           conr,nop,rot,dop,if0)
                write(nu_fs1,*) x, y, bsf

              end do
              write(nu_fs1,*) ''

            end do

            xyang = xyang + th

         end do

          write(11,'(a)') 'splot "./'//trim(datfile)//'"'

         close(unit=11)
         close(nu_fs1)

! 'FERMI_CALC'
       else
        if ( fer_rays_calc ) then  ! akeBZray.dat file is generated

        imesh = 1
        n2 = 3*sum(nkns(1:imesh))
        n1 = n2 - 3*nkns(imesh)+1

        datfile = ''
        write(datfile,'(a)') c_ake_bz_rays//trim(sfx)//'.dat'

        open(nu_fs2,file=trim(datfile),status='replace',                &
     &     iostat=iec)
        nrecs = 0

        ! temporary debugging switch
!        isdis = .true.

        ! first make a gnuplot script for easy viewing

          flb = 11
          open(unit=flb,file=c_ake_bz_rays//trim(sfx)//'.gnu',          &
     &          status='replace',iostat=iec)

        xmin = 0.d10; xmax = 0.d10
        ymin = 0.d10; ymax = 0.d10

          n = flb
          write(n,'(a)') 'unset xtics; unset ytics; unset ztics'
          write(n,'(a)') 'unset colorbox; unset key; unset border'
          write(n,'(a)') 'set size square'
          write(n,'(a)') 'set title "Fermi surface" offset 0.0,1.0'

         xyang = 0.d0
         do i = 2, fermi_nkpts

            write(nu_fs2,'(a,3f7.3)') '# from ', fermi_kpts(:,i-1)
            write(nu_fs2,'(a,3f7.3)') '#  to  ', fermi_kpts(:,i)

            edge1(:) = -fermi_korig(:) + fermi_kpts(:,i-1)
            edge2(:) = -fermi_korig(:) + fermi_kpts(:,i)

            e1len = dsqrt(dot_product(edge1,edge1))
            e2len = dsqrt(dot_product(edge2,edge2))

            call vectprd(edge1,edge2,norm)
            sint = dsqrt(dabs(dot_product(norm,norm)))/(e1len*e2len)
            if( dot_product(edge1,edge2) > 0.d0 ) then
              th = dasin(sint)
            else
              th = pi - dasin(sint)
            end if

            ! for gnuplot script

            if( i == 2 ) then

                n = flb

                write(n,'(a,a,a,f7.3,a,f7.3,a)') 'set label "',         &
     &            trim(fermi_orilabel),'" at ',                         &
     &            0.d0,',',0.d0,                                        &
     &            ' front center offset 0.0,-1.0'

                write(n,'(a,a,a,f7.3,a,f7.3,a)') 'set label "',         &
     &            trim(fermi_kptlabel(1)),'" at ',                      &
     &            e1len,',',0.d0,                                       &
     &            ' front center offset 0.0,-1.0'

                write(n,'(a,f7.3,a,f7.3,a)')                            &
     &            'set arrow from 0.0,0.0 to ',                         &
     &            e1len,',',0.d0,                                       &
     &            ' front nohead'

              if( e1len > xmax ) xmax = e1len

            end if

            n = flb
             write(n,'(a,a,a,f7.3,a,f7.3,a,f7.3,a,f7.3)')               &
     &        'set label "',                                            &
     &        trim(fermi_kptlabel(i)),'" at ',                          &
     &        e2len*dcos(xyang+th),',',e2len*dsin(xyang+th),            &
     &        ' front center offset ',                                  &
     &        dcos(xyang+th),',',dsin(xyang+th)
             write(n,'(a,f7.3,a,f7.3,a)')                               &
     &        'set arrow from 0.0,0.0 to ',                             &
     &        e2len*dcos(xyang+th),',',e2len*dsin(xyang+th),            &
     &        ' front nohead'
             write(n,'(a,f7.3,a,f7.3,a,f7.3,a,f7.3,a)')                 &
     &        'set arrow from ',                                        &
     &        e1len*dcos(xyang),',',e1len*dsin(xyang),' to ',           &
     &        e2len*dcos(xyang+th),',',e2len*dsin(xyang+th),            &
     &        ' front nohead'

            ! recompute bounding box
            if(e2len*dcos(xyang+th) > xmax) xmax = e2len*dcos(xyang+th)
            if(e2len*dcos(xyang+th) < xmin) xmin = e2len*dcos(xyang+th)
            if(e2len*dsin(xyang+th) > ymax) ymax = e2len*dsin(xyang+th)
            if(e2len*dsin(xyang+th) < ymin) ymin = e2len*dsin(xyang+th)

            nthdiv = ceiling( fermi_raysampling*th )
            raysamplerate = fermi_gridsampling
            du = 1.d0/nthdiv

            if( i == 2 ) then
              u = -du; nthdiv = nthdiv + 1
            else
              u = 0.d0
            end if
            do n = 1, nthdiv
              u = u + du

              displ = (1.d0-u)*edge1 + u*edge2
              dllen = dsqrt(dot_product(displ,displ))
              nraydiv = ceiling(raysamplerate * dllen)
              dv = 1.d0/nraydiv

              startk = fermi_korig
              endk   = fermi_korig + displ


                v = 0.d0
                do m = 1, nraydiv
                  v = v + dv

                  displ = v*( (1.d0-u)*edge1 + u*edge2 )
                  dllen = dsqrt(dot_product(displ,displ))
                  dlang = dacos(max(min(dot_product(displ,edge1)/       &
     &                                       (dllen*e1len),1.d0),-1.d0))
                  x = dllen * dcos(xyang + dlang)
                  y = dllen * dsin(xyang + dlang)
                  rayp = v

                  kpnt  = fermi_korig + displ
                  call getblochspecfn(                                  &
     &             natom,nbasis,iorig,itype,komp,numbsub,atcon,         &
     &             energy,kkrsz,tab,tcpatmp,tau00,pzzbl,pzj,            &
!     &             startk,endk,rayp,kpnt,bsf,bsfNL,bsfcubic,bsfdis,alat,&
     &             startk,endk,rayp,kpnt,bsf,bsfNL,      bsfdis,rytodu, &
     &        lmax,aij,mapstr(1:natom,1:natom),mappnt(1:natom,1:natom), &
     &                                 mapij,powe,                      &
     &                  edu,pdu,rns,ndimrs,np2r,ndimnp,numbrs,naij,     &
     &             dqint1,size(dqint1,1),hplnm,ndimrhp,ndimlhp,         &
     &             irecdlm,d00,eoeta,                                   &
     &          eta0,reshape(xknlat(n1:n2),[nkns(imesh),3]),nkns(imesh),&
     &             conr,nop,rot,dop,if0)
                  write(nu_fs2,'(4f15.7)')                              &
     &                   x, y, bsf
                  nrecs = nrecs+1

!  uncomment to show cubic projection of A(k,E)
!   ... be sure to add advance='no' to above write
!                  do in=1,nbasis
!                  do il=1,kkrsz
!                  if(in/=nbasis.or.il/=kkrsz) then
!                    write(nu_fs2,'(1f15.7)',advance='no') bsfcubic(il,in)
!                  end if; end do; end do
!                  write(nu_fs2,'(1f15.7)') bsfcubic(kkrsz,nbasis)
!
                end do

              write(nu_fs2,*) ''

            end do

            xyang = xyang + th

         end do

         del = 0.05
           n = flb
           write(n,'(a,f7.3,a,f7.3,a)')                                 &
     &       'set xrange [',xmin-del,':',xmax+del,']'
           write(n,'(a,f7.3,a,f7.3,a)')                                 &
     &       'set yrange [',ymin-del,':',ymax+del,']'

           fub = 12
           if ( nrecs>1 ) then
            rewind(flb)
            open(unit=fub,file=c_fermi//trim(sfx)//'.gnu',              &
     &        status='replace',iostat=iec)
            do
             read(flb,'(a)',end=1901) akestring
             write(fub,'(a)') trim(akestring)
            end do
1901        continue
           end if
           write(flb,'(a)') 'splot "./'//trim(datfile)//'" w l'

         close(unit=flb)
         close(unit=nu_fs2)
         if ( nrecs>0 ) then
! Find peaks of A(k,E) to get the Fermi Surface
! find approximate fermi surface for disordered structure
          open(nu_fs2,file=trim(datfile),status='old',iostat=iec)
          datfile = ''
          write(datfile,'(a)') c_fermi//trim(sfx)//'.dat'
          open(nu_fs1,file=trim(datfile),status='replace',iostat=iec)

          del = 0.05
           write(fub,'(a,f7.3,a,f7.3,a)')                               &
     &       'set xrange [',xmin-del,':',xmax+del,']'
           write(fub,'(a,f7.3,a,f7.3,a)')                               &
     &       'set yrange [',ymin-del,':',ymax+del,']'

          do ! <-- infinite loop terminates on eof

           ! ignore comments and white-space to next ray
           n = 0
           do while( n == 0 )
             read(nu_fs2,'(a70)',end=1921) akestring
             n = verify(akestring,' ')
             if( n/=0 ) then
               if( akestring(n:n) == '#' ) n=0
             end if
           end do

           ! read AkE ray until next comment or white-space
           n = 0; m = 1
           do while( m /= 0 )
             n = n + 1
             read(akestring,*) akeray(1:3,n)
             read(nu_fs2,'(a70)',end=1911) akestring
             m = verify(akestring,' ')
             if( m/=0 ) then
               if( akestring(m:m) == '#' ) m=0
             end if
           end do

           ! pick out local maxima and corresponding FWHM
1911       ips = 4
           do m = ips+1, n-ips

             ! must be max over this span to qualify as peak
             ispeak = .true.
             do i = 1, ips
               if( akeray(3,m-i) > akeray(3,m-i+1) .or.                 &
     &             akeray(3,m+i) > akeray(3,m+i-1) ) then
                  ispeak = .false.; exit
               end if
             end do
!             if( akeray(3,m) < 1.0d0 ) ispeak = .false.

             if( ispeak ) then
               write(nu_fs1,*) akeray(1:2,m)

!              .. change for cu3au paper
               halfmax = akeray(3,m)/2.d0
!              halfmax = 10.d0

               do iL = m, 1, -1
                 if( akeray(3,iL) < halfmax ) exit
               end do
               do iR = m, n
                 if( akeray(3,iR) < halfmax ) exit
               end do
               if ( IL>0 .and. IR<=n ) then
               write(fub,'(a,f7.3,a,f7.3,a,f7.3,a,f7.3,a)')             &
     &           'set arrow from ',akeray(1,iL),',',                    &
     &           akeray(2,iL),' to ',akeray(1,iR),',',akeray(2,iR),     &
     &           ' heads size 0.005,90'
               end if
               !!!
             end if

           end do

          end do

1921      continue
          write(fub,'(a)') 'plot "'//c_fermi//trim(sfx)//'.dat" w p'

         ! close fermi.dat, fermi.gnu, akeray.dat
          close(nu_fs1)
          close(fub)
          close(nu_fs2)
         else
          write(6,'(a)') 'WARNING: no data records for FS in '//datfile
         end if
!
         ! Find peaks of A(k,E) to get the Fermi Surface
         ! find approximate fermi surface for disordered structure
!!??        else if ( fermiS_calc .and. .not.isdis ) then    ! fermiS.dat file is generated
!!??
!!??         imesh = 1
!!??         n2 = 3*sum(nkns(1:imesh))
!!??         n1 = n2 - 3*nkns(imesh)+1
!!??
!!??!        fermi_fileno = 4
!!??        datfile = ''
!!??        write(datfile,'(a)') c_fermi//trim(sfx)//'.dat'
!!??
!!??        open(nu_fs2,file=trim(datfile),status='replace',                &
!!??     &     iostat=iec)
!!??        nrecs = 0
!!??
!!??
!!??          flb = 11
!!??
!!??        open(unit=flb,file=c_fermi//trim(sfx)//'.gnu',status='replace', &
!!??     &     iostat=iec)
!!??
!!??        xmin = 0.d10; xmax = 0.d10
!!??        ymin = 0.d10; ymax = 0.d10
!!??
!!??          write(flb,'(a)') 'unset xtics; unset ytics; unset ztics'
!!??          write(flb,'(a)') 'unset colorbox; unset key; unset border'
!!??          write(flb,'(a)') 'set size square'
!!??          write(flb,'(a)') 'set title "Fermi surface" offset 0.0,1.0'
!!??
!!??         xyang = 0.d0
!!??         do i = 2, fermi_nkpts
!!??
!!??            write(nu_fs2,'(a,3f7.3)') '# from ', fermi_kpts(:,i-1)
!!??            write(nu_fs2,'(a,3f7.3)') '#  to  ', fermi_kpts(:,i)
!!??
!!??            edge1(:) = -fermi_korig(:) + fermi_kpts(:,i-1)
!!??            edge2(:) = -fermi_korig(:) + fermi_kpts(:,i)
!!??
!!??            e1len = dsqrt(dot_product(edge1,edge1))
!!??            e2len = dsqrt(dot_product(edge2,edge2))
!!??
!!??            call vectprd(edge1,edge2,norm)
!!??            sint = dsqrt(dabs(dot_product(norm,norm)))/(e1len*e2len)
!!??            if( dot_product(edge1,edge2) > 0.d0 ) then
!!??              th = dasin(sint)
!!??            else
!!??              th = pi - dasin(sint)
!!??            end if
!!??
!!??            ! for gnuplot script
!!??
!!??            if( i == 2 ) then
!!??
!!??
!!??                write(flb,'(a,a,a,f7.3,a,f7.3,a)') 'set label "',       &
!!??     &            trim(fermi_orilabel),'" at ',                         &
!!??     &            0.d0,',',0.d0,                                        &
!!??     &            ' front center offset 0.0,-1.0'
!!??
!!??                write(flb,'(a,a,a,f7.3,a,f7.3,a)') 'set label "',       &
!!??     &            trim(fermi_kptlabel(1)),'" at ',                      &
!!??     &            e1len,',',0.d0,                                       &
!!??     &            ' front center offset 0.0,-1.0'
!!??
!!??                write(flb,'(a,f7.3,a,f7.3,a)')                          &
!!??     &            'set arrow from 0.0,0.0 to ',                         &
!!??     &            e1len,',',0.d0,                                       &
!!??     &            ' front nohead'
!!??
!!??              if( e1len > xmax ) xmax = e1len
!!??
!!??            end if
!!??
!!??             write(flb,'(a,a,a,f7.3,a,f7.3,a,f7.3,a,f7.3)')             &
!!??     &        'set label "',                                            &
!!??     &        trim(fermi_kptlabel(i)),'" at ',                          &
!!??     &        e2len*dcos(xyang+th),',',e2len*dsin(xyang+th),            &
!!??     &        ' front center offset ',                                  &
!!??     &        dcos(xyang+th),',',dsin(xyang+th)
!!??             write(flb,'(a,f7.3,a,f7.3,a)')                             &
!!??     &        'set arrow from 0.0,0.0 to ',                             &
!!??     &        e2len*dcos(xyang+th),',',e2len*dsin(xyang+th),            &
!!??     &        ' front nohead'
!!??             write(flb,'(a,f7.3,a,f7.3,a,f7.3,a,f7.3,a)')               &
!!??     &        'set arrow from ',                                        &
!!??     &        e1len*dcos(xyang),',',e1len*dsin(xyang),' to ',           &
!!??     &        e2len*dcos(xyang+th),',',e2len*dsin(xyang+th),            &
!!??     &        ' front nohead'
!!??
!!??            ! recompute bounding box
!!??            if(e2len*dcos(xyang+th) > xmax) xmax = e2len*dcos(xyang+th)
!!??            if(e2len*dcos(xyang+th) < xmin) xmin = e2len*dcos(xyang+th)
!!??            if(e2len*dsin(xyang+th) > ymax) ymax = e2len*dsin(xyang+th)
!!??            if(e2len*dsin(xyang+th) < ymin) ymin = e2len*dsin(xyang+th)
!!??
!!??            nthdiv = ceiling( fermi_raysampling*th )
!!??            raysamplerate = fermi_gridsampling
!!??            du = 1.d0/nthdiv
!!??
!!??            if( i == 2 ) then
!!??              u = -du; nthdiv = nthdiv + 1
!!??            else
!!??              u = 0.d0
!!??            end if
!!??            do n = 1, nthdiv
!!??              u = u + du
!!??
!!??              displ = (1.d0-u)*edge1 + u*edge2
!!??              dllen = dsqrt(dot_product(displ,displ))
!!??              nraydiv = ceiling(raysamplerate * dllen)
!!??              dv = 1.d0/nraydiv
!!??
!!??              startk = fermi_korig
!!??              endk   = fermi_korig + displ
!!??
!!??                ! for ordered crystal, just find ||tau(k,E)^-1|| = 0
!!??                call findblochstate(                                    &
!!??     &             natom,nbasis,iorig,itype,komp,numbsub,atcon,         &
!!??     &             energy,kkrsz,tab,tcpatmp,tau00,pzzbl,pzj,            &
!!??     &             startk,endk,roots(1),nroots,alat,                    &
!!??     &        lmax,aij,mapstr(1:natom,1:natom),mappnt(1:natom,1:natom), &
!!??     &                                 mapij,powe,                      &
!!??     &                  edu,pdu,rns,ndimrs,np2r,ndimnp,numbrs,naij,     &
!!??     &             dqint1,size(dqint1,1),hplnm,ndimrhp,ndimlhp,         &
!!??     &             irecdlm,d00,eoeta,                                   &
!!??     &          eta0,reshape(xknlat(n1:n2),[nkns(imesh),3]),nkns(imesh),&
!!??     &             conr,nop,rot,dop,if0)
!!??
!!??                do j = 1, nroots
!!??                  v = roots(j)
!!??                  displ = v*( (1.d0-u)*edge1 + u*edge2 )
!!??                  dllen = dsqrt(dot_product(displ,displ))
!!??                  dlang = dacos(dot_product(displ,edge1)/(dllen*e1len))
!!??                  x = dllen * dcos(xyang + dlang)
!!??                  y = dllen * dsin(xyang + dlang)
!!??                  write(nu_fs2,*) x, y
!!??                  nrecs = nrecs + 1
!!??                end do
!!??
!!??              write(nu_fs2,*) ''
!!??
!!??            end do
!!??
!!??            xyang = xyang + th
!!??
!!??         end do
!!??
!!??         del = 0.05
!!??           write(flb,'(a,f7.3,a,f7.3,a)')                               &
!!??     &       'set xrange [',xmin-del,':',xmax+del,']'
!!??           write(flb,'(a,f7.3,a,f7.3,a)')                               &
!!??     &       'set yrange [',ymin-del,':',ymax+del,']'
!!??
!!??
!!??         ! Find peaks of A(k,E) to get the Fermi Surface
!!??         ! find approximate fermi surface for disordered structure
!!??
!!??
!!??
!!??           rewind(nu_fs2)
!!??
!!??         if ( nrecs>0 ) then
!!??!????          open(unit=fermi_fileno,file=c_ake_bz_rays//trim(sfx)//'.dat', &
!!??!????     &                 status='old',iostat=iec)
!!??
!!??          do ! <-- infinite loop terminates on eof
!!??
!!??           ! ignore comments and white-space to next ray
!!??           n = 0
!!??           do while( n == 0 )
!!??             read(nu_fs2,'(a70)',end=2001) akestring
!!??             n = verify(akestring,' ')
!!??             if( n/=0 ) then
!!??               if( akestring(n:n) == '#' ) n=0
!!??             end if
!!??           end do
!!??
!!??           ! read AkE ray until next comment or white-space
!!??           n = 0; m = 1
!!??           do while( m /= 0 )
!!??             n = n + 1
!!??             read(akestring,*) akeray(1:3,n)
!!??             read(nu_fs2,'(a70)',end=2000) akestring
!!??             m = verify(akestring,' ')
!!??             if( m/=0 ) then
!!??               if( akestring(m:m) == '#' ) m=0
!!??             end if
!!??           end do
!!??
!!??           ! pick out local maxima and corresponding FWHM
!!??2000       ips = 4
!!??           do m = ips+1, n-ips
!!??
!!??             ! must be max over this span to qualify as peak
!!??             ispeak = .true.
!!??             do i = 1, ips
!!??               if( akeray(3,m-i) > akeray(3,m-i+1) .or.                 &
!!??     &             akeray(3,m+i) > akeray(3,m+i-1) ) then
!!??                  ispeak = .false.; exit
!!??               end if
!!??             end do
!!??!             if( akeray(3,m) < 1.0d0 ) ispeak = .false.
!!??
!!??             if( ispeak ) then
!!??               write(nu_fs2,*) akeray(1:2,m)
!!??
!!??!              .. change for cu3au paper
!!??               halfmax = akeray(3,m)/2.d0
!!??!              halfmax = 10.d0
!!??
!!??               do iL = m, 1, -1
!!??                 if( akeray(3,iL) < halfmax ) exit
!!??               end do
!!??               do iR = m, n
!!??                 if( akeray(3,iR) < halfmax ) exit
!!??               end do
!!??               write(flb,'(a,f7.3,a,f7.3,a,f7.3,a,f7.3,a)')             &
!!??     &           'set arrow from ',akeray(1,iL),',',                    &
!!??     &           akeray(2,iL),' to ',akeray(1,iR),',',akeray(2,iR),     &
!!??     &           ' heads size 0.005,90'
!!??
!!??               ! tmp -- for cu3au paper
!!??!               write(95,*) akeray(1:2,iL)
!!??!               write(96,*) akeray(1:2,iR)
!!??               !!!
!!??             end if
!!??
!!??           end do
!!??
!!??          end do
!!??
!!??2001      continue
!!??          write(flb,'(a)') 'plot "'//c_fermi//trim(sfx)//'.dat" w p'
!!??         else
!!??          write(6,'(a)') 'WARNING: no data records in '//c_fermi//      &
!!??     &                                     trim(sfx)//'.dat'//' file'
!!??         end if
!!??
!!??         ! close fermi.dat, fermi.gnu
!!??         close(unit=nu_fs2)
!!??         close(unit=flb)
!!??
        end if    ! fermi_calc
       end if
      end if

      if ( allocated(zlab) ) deallocate(zlab)

      time1 = time2
      call timel(time2)

      if(iprint.ge.0.and.nitcpa.gt.1) then
       write(6,*) sname//' time  ='                                     &
     &           ,real(time2-time1),' NITCPA=',nitcpa
      else if(nitcpa.gt.20.and.iprint.ge.-1.and.itmax>0) then
       write(6,*) ' Number of CPA iterations is ',nitcpa
      end if

!c           -----------------------------------------------------------
            if(istop.eq.sname) then
               call fstop(sname)
            endif
!c

      firstcall = .false.
      return

      contains

!      function catanf(gam)
!      complex*16 catanf
!      real(8), intent(in) :: gam
!      complex*16, parameter :: sqrtm1 = dcmplx(0.d0,1.d0)
!      catanf(gam)=0.5d0*sqrtm1*log((sqrtm1+gam)/(sqrtm1-gam))
!      return
!      end function catanf
!c
      subroutine gettab(nsub)
      implicit none
      integer :: nsub
      character(*), parameter :: sname='gettab'
!c     ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!c
      real(8) :: h,rmt,rScat
      real(8) :: xr(size(green,1))
      real(8) :: rr(size(green,1))
      real(8) :: vr(size(green,1))
      integer :: jmt,jws

      ! .. now need to store wave fn. to get cross integrals zlzl
      ! between different components at the same site. these terms
      ! are included in the expression for the bloch spectral fn
      ! for alloys given by phys. rev. b 21, 3222

!      logical :: isordered

      integer :: nk,ns_zl
      integer, external :: indRmesh
!c
!c     ****************************************************************
!c     set up t-matrix, zz, zj for each component and  sub-lattice
!c     ****************************************************************
!c
      if(iprint.ge.1) then
         write(6,'('' gettab:  nrelv,lmax='',2i5)') nrelv,lmax
!!         write(6,'('' gettab: icryst,alat='',i5,d12.4)') icryst,alat
         write(6,'('' gettab: energy,pmom='',4d12.4)') energy,pmom
         write(6,'('' gettab: size(tab)='',4i5)') (size(tab,ic),ic=1,4)
      endif

!c
         ns_zl = min(nsub,size(zlab,4))
         do nk=1,komp(nsub)
!            if(ikpoint(nk,nsub).eq.0 .or. ivpoint(nsub).eq.0) then
!               v_rho_box => sublat(nsub)%compon(nk)%v_rho_box
               call g_meshv(sublat(nsub)%compon(nk)%v_rho_box,          &
     &                                                 h,xr,rr,ispin,vr)
               jws = 1+indRmesh(rws(nsub),size(rr),rr)
!               rScat = rr(sublat(nsub)%compon(nk)%v_rho_box%jmt)
               rScat = gRasa(nsub,nk)
               jmt = sublat(nsub)%compon(nk)%v_rho_box%jmt
!c              -------------------------------------------------------
               call grint(nrelv,clight,                                 &
     &                    lmax,kkrsz,                                   &
!!     &                    icryst,alat,                                  &
     &                    energy,pmom,                                  &
     &                    iswzj,                                        &
     &                    vr,xr,rr,h,                                   &
     &                    jmt,jws,                                      &
     &                    rScat,rws(nsub),                              &
     &                    rmt_true(nk,nsub),r_circ(nk,nsub),ivar_mtz,   &
     &                    fcount(nsub),weight,rmag(:,:,:,:,nsub),       &
     &                    vj(:,:,:,:,nsub),lVP,                         &
     &                    zlab(:,:,nk,ns_zl),                           &
     &                    tab(1:kkrsz,1:kkrsz,nk,nsub),                 &
     &                    pzz(1:kkrsz,1:kkrsz,nk,nsub),                 &
     &                    pzj(1:kkrsz,1:kkrsz,nk,nsub),                 &
     &                    cotdl(:,nsub,nk),almat(:,nk,nsub),mtasa,      &
     &                    iprint,istop)
!c              -------------------------------------------------------
!CDEBUG
!CDEBUG              call symmtrx(tab(1,1,nk,nsub),ipkkr,kkrsz,1.d-12)
!CDEBUG              call symmtrx(pzz(1,1,nk,nsub),ipkkr,kkrsz,1.d-12)
!CDEBUG              call symmtrx(pzj(1,1,nk,nsub),ipkkr,kkrsz,1.d-12)
!CDEBUG
!            else
!               nkd=ikpoint(nk,nsub)
!               nsubd=ivpoint(nsub)
!               do j=1,kkrsz
!                  do i=1,kkrsz
!                     tab(i,j,nk,nsub)=tab(i,j,nkd,nsubd)
!                     pzz(i,j,nk,nsub)=pzz(i,j,nkd,nsubd)
!                     pzj(i,j,nk,nsub)=pzj(i,j,nkd,nsubd)
!                     pzzck(i,j,nk,nsub)=pzzck(i,j,nkd,nsubd)
!                     pzjck(i,j,nk,nsub)=pzjck(i,j,nkd,nsubd)
!                  enddo
!               enddo
!            endif
         enddo

      if( iprint .ge. 1 ) then
          do ic=1,komp(nsub)
            call wrtmtx(tab(:,:,ic,nsub),kkrsz)
          enddo
      endif
!c
!c     ================================================================
      if(istop.eq.sname) then
        call fstop(sname)
      endif
!c
      return
      end subroutine gettab
!c
!c     ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      subroutine calc_pzzbl(pmom,pzzbl)
      implicit none
      complex(8), intent(in)  :: pmom
      complex(8), intent(out) :: pzzbl(:,:,:,:,:)
      integer :: nka,nkb,nk1,nk2,i,l,ll,lmin,m,iR1
      integer :: jmt1,jmt2,jws2,iex
      complex(8) :: pr
      real(8), allocatable :: xr1(:),xr2(:)
      real(8), allocatable :: rr1(:),rr2(:)
      real(8) :: rmt1,h
      complex(8) :: zzab(size(zlab,1)),izzab(size(zlab,1)),pzzab
      integer, external :: indRmesh
      integer :: ndr,nsub_
!
      pzzbl(:,:,:,:,:) = czero
      do nsub_=1,nbasis
       if ( (bsf_calc.or.fermi_calc) .and. komp(nsub_)>1 ) then
           ndr =                                                        &
     &      maxval(sublat(nsub_)%compon(1:komp(nsub_))%v_rho_box%ndrpts)
           allocate(xr1(ndr),rr1(ndr))
           allocate(xr2(ndr),rr2(ndr))

           do nka = 1, komp(nsub_)
           do nkb = 1, komp(nsub_)

             ! which grid is finer?
!             if( rws(nka,nsub) <= rws(nkb,nsub) ) then
             if ( (sublat(nsub_)%compon(nka)%v_rho_box%xmt) <=          &
     &            (sublat(nsub_)%compon(nkb)%v_rho_box%xmt) ) then
               nk1 = nka; nk2 = nkb
             else
               nk1 = nkb; nk2 = nka
             end if
             rmt1 = rmt_true(nk1,nsub_)

             call g_meshv(sublat(nsub_)%compon(nk1)%v_rho_box,h,xr1,rr1)
             call g_meshv(sublat(nsub_)%compon(nk2)%v_rho_box,h,xr2,rr2)
             jws2 = 1+indRmesh(rws(nsub_),size(rr2),rr2)
             jmt2 = sublat(nsub_)%compon(nk2)%v_rho_box%jmt
             jmt1 = sublat(nsub_)%compon(nk1)%v_rho_box%jmt

             ! momentum needed to extend finer wave fn beyond MT,
             ! pmom needs to be identical to choice in scalar.f

             ! for all l
             do l = 1, lmax+1


               ! integrate from 0 --> R1
               do i = 1, jmt1 ! (nk1,nsub_)
                 zzab(i) = zlab(i,l,nk1,nsub_) *                        &
     &           ylag_cmplx(rr1(i),rr2,zlab(1,l,nk2,nsub_),0,3,jws2,iex)
               end do
               call cqxpup(1,zzab,jmt1,xr1(:),izzab)
               pzzab = izzab(jmt1)

               ! integrate R1 --> R1', where R1' > R1 and R1' on coarse grid
               ! recall 'zlab' is actually r*Z_l(r).
               iR1 = 1+indRmesh(rmt1,size(rr2),rr2)
               if( iR1 < jmt2 ) then
                 pzzab = pzzab + zzab(jmt1)*(rr2(iR1) - rmt1)
               end if

               ! integrate from R1' --> R2
               if( iR1 < jmt2 ) then

                 do i = 1, jmt2-iR1+1

                   lmin = max(2,l)
                   ! extend zl1 beyond its MT sphere
                   pr = pmom*rr2(iR1+i-1)
!                   call ricbes(lmin,pr,bjl(1:lmin),bnl(1:lmin),         &
!     &                                        djl(1:lmin),dnl(1:lmin))
                   call ricbes(lmin,pr,bjl,bnl,djl,dnl)
                   zzab(i) = bjl(l)*(dcmplx(1.d0,0.d0)/                 &
     &               tab(l**2,l**2,nk1,nsub_)/pmom-dcmplx(0.d0,1.d0))   &
     &               + bnl(l)

                   zzab(i) = zzab(i)*zlab(iR1+i-1,l,nk2,nsub_)

                 end do

                 call cqxpup(1,zzab,jmt2-iR1+1,                         &
     &                  xr2(iR1),izzab)
                 pzzab = pzzab + izzab(jmt2-iR1+1)

               end if

               ! include prefactor of -1/pi
               pzzab = -pzzab/pi

               ! distribute integral(zal*zbl') across corresponding entries
               ! of matrix 'pzzbl' matrix

               do m = 1, 2*(l-1) + 1

                 ll = (l-1)**2 + m
                 pzzbl(ll,ll,nka,nkb,nsub_) = pzzab

               end do

             end do

           end do; end do
           deallocate(xr1,rr1,xr2,rr2)
       else
        pzzbl(:,:,1,1,nsub_) = pzz(:,:,1,nsub_)
       end if
      enddo
      return
      end subroutine calc_pzzbl
!
!c     ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      subroutine getdos(prel)
!c
!c     *****************************************************************
!c      input:  w/ Z regular and J irrregular sols. to Schrodinger Eq.
!c             tau00   (tauc)
!c             tcpa    (tc)
!c             tab     (t-matrix for atoms a & b)
!c             pzz     (ZZ integrals MT)
!c             pzj     (ZJ integrals MT)
!c             pzzck   (ZZ integrals WS)
!c             pzjck   (ZJ integrals WS)
!c             kkrsz   (size of KKR-matrix)
!c             komp    (number of components on sublattice)
!c             istop   (index of subroutine prog. stops in)
!c       output:
!c             dos     (wigner-seitz cell density of states)
!c             dosck   (wigner-seitz or muffin-tin density of states)
!c     ****************************************************************
!c
      implicit none
      character sname*10
!c parameter
      real(8), parameter :: onem=-one
!c
      complex(8) :: w1(kkrsz,kkrsz)
      complex(8), target :: w2(kkrsz,kkrsz)
      complex(8) :: w3(kkrsz,kkrsz)
      complex(8) :: w4(kkrsz,kkrsz)
!      complex(8), pointer :: p_pzj(:,:) => null()
      complex(8), intent(in) :: prel

      integer :: i,ic,il,nsub
      real(8) :: h
      real(8) :: xr(size(green,1))
      real(8) :: rr(size(green,1))
      real(8) :: vr(size(green,1))
      complex(8) :: tmpgreen(size(green,1))
      complex(8) :: tmpdos(0:size(dos,1)-1)
      integer :: jmt,jws
      integer, external :: indRmesh
!c parameter
      character(2), parameter :: updn(2) = ['up','dn']
!      complex(8), parameter :: cone=(1.d0,0.d0)
!c
!c     ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
      parameter (sname='getdos')
!c     ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!c
      if(iprint.ge.4) then
         write(6,'('' getdos:: nbasis,kkrsz'',3i4)')                    &
     &                         nbasis,kkrsz
         do nsub=1,nbasis
               write(6,'('' GETDOS: tau00 '')')
               call wrtmtx(tau00(:,:,nsub),kkrsz)
            do ic=1,komp(nsub)
               write(6,'('' getdos:: pzz ::'')')
               call wrtmtx(pzz(:,:,ic,nsub),kkrsz)
               write(6,'('' getdos:: pzj ::'')')
               call wrtmtx(pzj(:,:,ic,nsub),kkrsz)
            enddo
         enddo
      endif
      dos = czero
      dosck = czero
      do nsub=1,nbasis
         if(iprint.ge.4) then
            write(6,'('' getdos::  nsub ::'',i3)') nsub
         endif
!c         errcpa=zero
!c        =============================================================
!c        tcpa => t_{c}(old) = t_{c}...................................
!c        w4   => t_{c}^{-1} = m_c.....................................
!c        -------------------------------------------------------------
         call matinv(tcpa(1:kkrsz,1:kkrsz,nsub),w4(1:kkrsz,1:kkrsz),w3, &
     &                                             kkrsz, iprint,istop)
!c        -------------------------------------------------------------
         if(iprint.ge.4) then
            write(6,'('' getdos:: t_c(old) ::'')')
            call wrtmtx(tcpa(:,:,nsub),kkrsz)
            write(6,'('' getdos:: m_c(old) ::'')')
            call wrtmtx(w4,kkrsz)
         endif
         do ic=1,komp(nsub)
!c           ==========================================================
!c           w2 => t_a(b)^{-1} = m_a(b)................................
!c           ----------------------------------------------------------
            call matinv(tab(1:kkrsz,1:kkrsz,ic,nsub),                   &
     &                      w2(1:kkrsz,1:kkrsz),w3,kkrsz, iprint,istop)
            if(iprint.ge.4) then
               write(6,'('' getdos::  ic ::'',i3)') ic
               write(6,'('' getdos:: t_a(b) ::'')')
               call wrtmtx(tab(:,:,ic,nsub),kkrsz)
               write(6,'('' getdos:: m_a(b) ::'')')
               call wrtmtx(w2,kkrsz)
            endif
!c           ==========================================================
!c           w1 => m_c - m_a(b)........................................
!c           ----------------------------------------------------------
            call madd(w4,onem,w2,w1,kkrsz,iprint)
!c           ==========================================================
!c           w2 => tau_c*[m_c - m_a(b)]^{-1}...........................
!c           ----------------------------------------------------------
            call mmul(tau00(:,:,nsub),w1,w2,kkrsz)
            if(iprint.ge.4) then
               write(6,'('' getdos::m_c -  m_a(b) ::'')')
               call wrtmtx(w1,kkrsz)
               write(6,'('' getdos::tau_c*[m_c -  m_a(b)] ::'')')
               call wrtmtx(w2,kkrsz)
            endif
!c           ==========================================================
!c           w1 => [1-tau_c*[m_c - m_a(b)]]^{-1}] = D^{-1}.............
!c           ----------------------------------------------------------
            w3 = czero
            do i=1,kkrsz
               w3(i,i)=cone
            enddo
            call madd(w3,onem,w2,w1,kkrsz,iprint)
            if(iprint.ge.4) then
               write(6,'('' getdos:: D^{-1} ::'')')
               call wrtmtx(w1,kkrsz)
            endif
!c           ==========================================================
!c           w2 => D ..................................................
!c           ----------------------------------------------------------
            call matinv(w1,w2,w3,kkrsz,iprint,istop)
            if(iprint.ge.4) then
               write(6,'('' getdos:: D ::'')')
               call wrtmtx(w2,kkrsz)
            endif
!c           ==========================================================
!c           w1 => D*tau_c = tau_a(b).................................
!c           ----------------------------------------------------------
            call mmul(w2,tau00(:,:,nsub),w1,kkrsz)
!
            call g_meshv(sublat(nsub)%compon(ic)%v_rho_box,             &
     &                                                 h,xr,rr,ispin,vr)
            jws = 1+indRmesh(rws(nsub),size(rr),rr)
            jmt = sublat(nsub)%compon(ic)%v_rho_box%jmt


            if(iprint.ge.4) then
               write(6,'('' getdos:: tau_a(b) ::'')')
               call wrtmtx(w1,kkrsz)
            endif

            if ( dreal(energy) >= zero ) then
!c           ==========================================================
!c           dos =>  ZZ*(tau_a(b)-t_a(b)) + ZZ*t_a(b)- ZJ_a(b)..........
!c           ----------------------------------------------------------
             call mdos(tmpdos(0:),                                      &
     &                 pzz(:,:,ic,nsub),                                &
     &                 pzj(:,:,ic,nsub),                                &
     &                 tab(:,:,ic,nsub),                                &
     &                 kkrsz,0,istop)
             call mgreen(tmpgreen,tab(:,:,ic,nsub),kkrsz,               &
     &                    nrelv,clight,                                 &
     &                    lmax,                                         &
     &                    energy,prel,                                  &
     &                    h,jmt,jws,rr,vr,                              &
     &                    1,                                            &
     &                    iprint,istop)
             w3 = czero
             call madd(w1,onem,tab(:,:,ic,nsub),w2,kkrsz,iprint)   ! tau-tab
             call mdos(dos(0:,ic,nsub),                                 &
     &                 pzz(:,:,ic,nsub),                                &
     &                 w3,                                              &
     &                 w2,                                              &
     &                 kkrsz,0,istop)
              call mgreen(green(:,ic,nsub),w2,kkrsz,                    &
     &                    nrelv,clight,                                 &
     &                    lmax,                                         &
     &                    energy,prel,                                  &
     &                    h,jmt,jws,rr,vr,                              &
     &                    0,                                            &
     &                    iprint,istop)
             dos(0:lmax,ic,nsub) = dos(0:lmax,ic,nsub) + tmpdos(0:lmax)
             green(:,ic,nsub) = green(:,ic,nsub) + tmpgreen(:)

            else
!c           ==========================================================
!c           dos =>  ZZ*tau_a(b) - ZJ_a(b).............................
!c           ----------------------------------------------------------
             call mdos(dos(0:,ic,nsub),                                 &
     &                 pzz(:,:,ic,nsub),                                &
     &                 pzj(:,:,ic,nsub),                                &
     &                 w1,                                              &
     &                 kkrsz,0,istop)
             call mgreen(green(:,ic,nsub),w1,kkrsz,                     &
     &                    nrelv,clight,                                 &
     &                    lmax,                                         &
     &                    energy,prel,                                  &
     &                    h,jmt,jws,rr,vr,                              &
     &                    1,                                            &
     &                    iprint,istop)
            end if
!
            if(iprint.ge.0) then
             write(6,1001) energy,sum(dos(0:lmax,ic,nsub)),updn(ispin), &
     &                    nsub,ic
 1001        format(2g15.7,1x,2g16.8,' E',a2,',dos(E) nsub=',i4,1x,i2)
            end if
!
            dosck(ic,nsub) = dcmplx(0.d0,0.d0)
            do il = 0,lmax
              dosck(ic,nsub) = dosck(ic,nsub) + dos(il,ic,nsub)
            end do

         enddo
      enddo
!c     =================================================================
      if(istop.eq.sname) then
        call fstop(sname)
      endif
!c     =================================================================
      return
!
      end subroutine getdos
!
      end subroutine gettau_ray
!EOC
