       SUBROUTINE ZUILLIM (NROW,L,IL,JL,X,Z1,Z2)
!C
!C     Purpose:
!C     Computes X = L^{-1} * X,  where L is the lower  triangular factor
!C     of the ILUT preconditioner,  with 1s on  the  diagonal.  Only the
!C     lower triangle is actually stored,  in row major order, hence the
!C     elimination is formulated as dot products of rows of L with upper
!C     parts of X.
!C
!C     Parameters:
!C     NROW = the number of rows in the matrix (input).
!C     L    = the matrix elements (input).
!C     IL   = the array of row pointers (input).
!C     JL   = the array of column indices (input).
!C     X    = the vector to apply L^{-1} to (input/output).
!C
!C**********************************************************************
!C
      INTEGER JL(*), NROW, IL(NROW+1)
      DOUBLE COMPLEX L(*), X(NROW)
      INTEGER I0,NK
      DOUBLE COMPLEX Z1(*),Z2(*)
      DOUBLE COMPLEX ZDOTU,ZDOTUI
      EXTERNAL ZDOTU,ZDOTUI
!C
!C     Miscellaneous parameters.
!C
      DOUBLE COMPLEX ZZERO
      PARAMETER (ZZERO = (0.0D0,0.0D0))
!C
!C     Local variables.
!C
!C      INTEGER I, J, K
      INTEGER I
!C      DOUBLE COMPLEX ZTMP
!C      DOUBLE COMPLEX ZTMP1
!C
!C     Loop over the rows of L.
!C
      DO 20 I = 1, NROW
!C
!C     Loop over the columns, up to the diagonal.
!C
       I0 = IL(I)
       NK = IL(I+1)-I0
       X(I) = X(I) - ZDOTUI(NK,L(I0),JL(I0),X)

20    CONTINUE

      RETURN
      END

      SUBROUTINE ZUILUIM (NROW,U,IU,JU,DUINV,X,Z1,Z2)
!C
!C     Purpose:
!C     Computes X = U^{-1} * X,  where U is the upper  triangular factor
!C     of the ILUT preconditioner. It is stored as the diagonal elements
!C     followed by  the upper  triangle  in row  major order,  hence the
!C     elimination is formulated as dot products of rows of L with lower
!C     parts of X.
!C
!C     Parameters:
!C     NROW  = the number of rows in the matrix (input).
!C     U     = the matrix elements for U (input).
!C     IU    = the array of row pointers for U (input).
!C     JU    = the array of column indices for U (input).
!C     DUINV = the array of reciprocals of the diagonal of U (input).
!C     X     = the vector to apply the preconditioner to
!C             (input/output).
!C
!C**********************************************************************
!C
      INTEGER JU(*), NROW, IU(NROW+1)
      DOUBLE COMPLEX DUINV(NROW), U(*), X(NROW)
      DOUBLE COMPLEX Z1(*),Z2(*)
!C
!C     Miscellaneous parameters.
!C
      DOUBLE COMPLEX ZZERO
      PARAMETER (ZZERO = (0.0D0,0.0D0))
      DOUBLE COMPLEX ZDOTU,ZDOTUI
      EXTERNAL ZDOTU,ZDOTUI
!C
!C     Local variables.
!C
      INTEGER I0,NK
      INTEGER I
!c      INTEGER I, J, K
!C      DOUBLE COMPLEX ZTMP
!C      DOUBLE COMPLEX ZTMP1
!C
!C     Loop over the rows of U.
!C
      DO 20 I = NROW, 1, -1

       I0 = IU(I)
       NK = IU(I+1)-I0
       X(I) = (X(I) - ZDOTUI(NK,U(I0),JU(I0),X)) * DUINV(I)

20    CONTINUE

      RETURN
      END

      SUBROUTINE ZUIL1IM(NROW,L,IL,JL,U,IU,JU,DUINV,PRECON,X,Z1,Z2)
!C
!C     Purpose:
!C     Computes X = M_1^{-1} * X.
!C
!C     Parameters:
!C     NROW   = the number of rows in the matrix (input).
!C     L      = the matrix elements for L (input).
!C     IL     = the array of row pointers for L (input).
!C     JL     = the array of column indices for L (input).
!C     U      = the matrix elements for U (input).
!C     IU     = the array of row pointers for U (input).
!C     JU     = the array of column indices for U (input).
!C     DUINV  = the array of reciprocals of the diagonal of U (input).
!C     PRECON = the preconditioner type flag (input).
!C     X      = the vector to apply the preconditioner to
!C              (input/output).
!C
!C     External routines used:
!C     subroutine zuilli(x)
!C        Computes L^{-1} * x.
!C     subroutine zuilui(x)
!C        Computes U^{-1} * x.
!C
!C     Noel M. Nachtigal
!C     November 23, 1992
!C
!C**********************************************************************
!C
      EXTERNAL ZUILLIM, ZUILUI
!C
      INTEGER IL(*), IU(*), JL(*), JU(*), NROW, PRECON
      DOUBLE COMPLEX DUINV(*), L(*), U(*), X(*)
      DOUBLE COMPLEX Z1(*),Z2(*)
!C
      IF (PRECON.EQ.1) THEN
       CALL ZUILLIM (NROW,L,IL,JL,X,Z1,Z2)
!C         CALL ZUILLI (NROW,L,IL,JL,X)
       CALL ZUILUIM (NROW,U,IU,JU,DUINV,X,Z1,Z2)
!C         CALL ZUILUI (NROW,U,IU,JU,DUINV,X)
      ELSE IF (PRECON.EQ.3) THEN
       CALL ZUILLIM (NROW,L,IL,JL,X,Z1,Z2)
!C         CALL ZUILLI (NROW,L,IL,JL,X)
      END IF
!C
      RETURN
      END

      SUBROUTINE ZUIL2IM (NROW,L,IL,JL,U,IU,JU,DUINV,PRECON,X,Z1,Z2)
!C
!C     Purpose:
!C     Computes X = M_2^{-I} * X.
!C
!C     Parameters:
!C     NROW   = the number of rows in the matrix (input).
!C     L      = the matrix elements for L (input).
!C     IL     = the array of row pointers for L (input).
!C     JL     = the array of column indices for L (input).
!C     U      = the matrix elements for U (input).
!C     IU     = the array of row pointers for U (input).
!C     JU     = the array of column indices for U (input).
!C     DUINV  = the array of reciprocals of the diagonal of U (input).
!C     PRECON = the preconditioner type flag (input).
!C     X      = the vector to apply the preconditioner to
!C              (input/output).
!C
!C     External routines used:
!C     subroutine zuilli(x)
!C        Computes L^{-1} * x.
!C     subroutine zuilui(x)
!C        Computes U^{-1} * x.
!C
!C     Noel M. Nachtigal
!C     November 23, 1992
!C
!C**********************************************************************
!C
      EXTERNAL ZUILLIM, ZUILUIM
!C
      INTEGER IL(*), IU(*), JL(*), JU(*), NROW, PRECON
      DOUBLE COMPLEX DUINV(*), L(*), U(*), X(*)
      DOUBLE COMPLEX Z1(*), Z2(*)
!C
      IF (PRECON.EQ.2) THEN
       CALL ZUILLIM (NROW,L,IL,JL,X,Z1,Z2)
!C         CALL ZUILLI (NROW,L,IL,JL,X)
       CALL ZUILUIM (NROW,U,IU,JU,DUINV,X,Z1,Z2)
!C         CALL ZUILUI (NROW,U,IU,JU,DUINV,X)
      ELSE IF (PRECON.EQ.3) THEN
       CALL ZUILUIM (NROW,U,IU,JU,DUINV,X,Z1,Z2)
!C         CALL ZUILUI (NROW,U,IU,JU,DUINV,X)
      END IF
!C
      RETURN
      END
!C
!C**********************************************************************
!C
      SUBROUTINE ZUILSTM(NROW,A,IA,JA,L,IL,JL,U,IU,JU,JT,JI,DN,DR,DS,   &
     &                   DUINV,NZLMAX,NZUMAX,FILL,TOL,INFO)
!C
!C     Purpose:
!C     This subroutine sets up the ILUT preconditioner.  It assumes that
!C     the matrix is given in CSR format in A, IA, and JA.
!C     The code checks  for small diagonals,  where small is relative to
!C     the 1-norm of the row and the column.
!C
!C     Parameters:
!C
!C     NROW   = the number of rows in the matrix (input).
!C     A      = the matrix elements (input).
!C     IA     = the array of row pointers (input).
!C     JA     = the array of column indices (input).
!C     L      = the matrix elements (output).
!C     IL     = the array of row pointers (output).
!C     JL     = the array of column indices (output).
!C     U      = the matrix elements for U (output).
!C     IU     = the array of row pointers for U (output).
!C     JU     = the array of column indices for U (output).
!C     JT     = logical work array of size NROW (output).
!C     JI     = work array of size NROW (output).
!C     DN     = work array of size NROW (output).
!C     DR     = work array of size NROW (output).
!C     DS     = work array of size NROW (output).
!C     DUINV  = the array of reciprocals of the diagonal of U (output).
!C     NZLMAX = the maximum size of L and JL (input).
!C     NZUMAX = the maximum size of U and JU (input).
!C     INFO   = error checking flag, non-zero if not enough memory was
!C              allocated for L or U (input).
!C
!C     External routines used:
!C     double precision dznrm1(n,dx,incx)
!C        Computes the 1-norm of dx.
!C     subroutine zspsrt (n,arow,jrow)
!C        Sorts the column indices of arow in increasing order.
!C     subroutine zuilsr (n,maxsrt,tol,idx,x)
!C        Extracts the largest  min(n,maxsrt) elements  from x  that are
!C        larger than tol.
!C
!C     Noel M. Nachtigal
!C     November 23, 1992
!C
!C**********************************************************************
!C
      INTRINSIC CDABS, DABS, DCMPLX, MIN0
      EXTERNAL DZNRM1, ZSPSRT, ZUILSR
      DOUBLE PRECISION DZNRM1
!C
      INTEGER INFO, JA(*), NROW, NZLMAX, NZUMAX, IA(NROW+1), IL(NROW+1)
      INTEGER IU(NROW+1), JI(NROW), JL(NZLMAX), JU(NZUMAX)
      LOGICAL JT(NROW)
      DOUBLE COMPLEX A(*), DR(NROW), DUINV(NROW), L(NZLMAX), U(NZUMAX)
      DOUBLE PRECISION DN(NROW), DS(NROW)
!C
!C     Miscellaneous parameters.
!C
      DOUBLE COMPLEX ZONE, ZZERO
      PARAMETER (ZONE = (1.0D0,0.0D0), ZZERO = (0.0D0,0.0D0))
!C
!C     Local variables.
!C
      INTEGER FILL, I, IREPL, J, JJ, JPTR, K, KK
      INTEGER LENL, LENU
      DOUBLE COMPLEX DIAG, ZTMP
      DOUBLE PRECISION AVG, DTMP, TOL
!C
!C     Compute the 1-norms of the rows in DN.
!C
      DO 10 I = 1, NROW
         K      = IA(I)
         J      = IA(I+1) - K
         DN(I)  = DZNRM1(J,A(K),1)
 10   CONTINUE
!C
!C     Get the fill-in and tolerance from the user.
!C
!CAB      WRITE (6,'(A17,$)') 'Enter fill-in  : '
!CAB      READ (5,'(I10)') FILL
      FILL = MIN0(NROW,FILL)
!CAB      WRITE (6,'(A17,$)') 'Enter tolerance: '
!CAB      READ (5,*) TOL
      TOL = DMAX1(TOL,0.0D0)
!C
!C     Computation of the ILUT starts here.
!C
!CAB      WRITE (6,'(A15,I10)') 'Elements in A: ', IA(NROW+1)-1
!C
!C     Initialize the counters and the data.
!C
      INFO  = 0
      IL(1) = 1
      IU(1) = 1
      IREPL = 0
      DO 20 J = 1, NROW
         DR(J) = ZZERO
         JT(J) = .FALSE.
 20   CONTINUE
!C
!C     Iterate over all rows.  All further references to A actually apply
!C     to the matrix as it is modified by the elimination,  with possible
!C     fill-in, and not to the original matrix.
!C
!c      IPRINT = 1
      DO 110 I = 1, NROW
!C
!C     Trace execution every 4% of the rows.
!C

!CAB         J = IPRINT * 4 * NROW / 100
!CAB         IF (I.EQ.J) THEN
!CAB            IPRINT = IPRINT + 1
!CAB            WRITE (6,'(I10,$)') I
!CAB         END IF

!C
!C     Zero out DR.
!C
         DO 30 J = 1, NROW
            IF (JT(J)) THEN
               DR(J) = ZZERO
               JT(J) = .FALSE.
            END IF
 30      CONTINUE
!C
!C     Spread the Ith row of A into DR, count the  number of  elements in
!C     the lower and upper triangle.
!C
         LENL = 0
         LENU = 0
         DO 40 K = IA(I), IA(I+1)-1
            J = JA(K)
            IF (J.LT.I) THEN
               LENL = LENL + 1
            ELSEIF (J.GT.I) THEN
               LENU = LENU + 1
            END IF
            DR(J) = A(K)
            JT(J) = .TRUE.
 40      CONTINUE
!C
!C     Compute the maximum length of the rows of L and U.
!C
         LENL = MIN0(MAX0(0,LENL+FILL),NROW)
         LENU = MIN0(MAX0(0,LENU+FILL),NROW)
!C
!C     Compute the  full row I  of L and U,  subtracting multiples of all
!C     rows up to I-1.  For each  J=1,...,I-1, eliminate  element  A(I,J)
!C     by subtracting  L(I,J) times row J of U  from row I of A.
!C     L(I,J) = A(I,J) / U(J,J)
!C
         DO 60 J = 1, I-1
            IF (JT(J)) THEN
!C
!C     Store L_{i,j} = DR(J) / U_{j,j} in DR(J),  which  is in  the lower
!C     triangle of DR.
!C
!CC               ZTMP  = DR(J) * DUINV(J)
!CC               DR(J) = ZTMP
!C
!C     Now subtract the multiple of row J of U from DR, adding the column
!C     indices to JT.  The subtraction affects only elements to the right
!C     of column J.
!C     The code assumes here that those elements of DR which are supposed
!C     to be zero have in fact been zeroed out.
!C
!CC               DO 50 KK = IU(J), IU(J+1)-1
!CC                  JJ     = JU(KK)
!CC                  JT(JJ) = .TRUE.
!CC                  DR(JJ) = DR(JJ) - ZTMP * U(KK)
!CC 50            CONTINUE

             ZTMP  = -DR(J) * DUINV(J)
             DR(J) = -ZTMP
             I0 = IU(J)
             NK = IU(J+1) - I0
             CALL ZAXPYI(NK,ZTMP,U(I0),JU(I0),DR)
             DO KK = I0,I0-1+NK
             JT(JU(KK)) = .TRUE.
             END DO

          END IF
 60      CONTINUE
!C
!C     Elimination of the Ith row of A is complete.
!C     Set up the sort in DS and compute the average scaled value.  Since
!C     these are the multipliers L(I,J), scale each by  the norm of row J
!C     of U.
!C
         K   = 0
         AVG = 0.0D0
         DO 70 J = 1, I-1
            IF (JT(J)) THEN
               K     = K + 1
               JI(K) = J
               DTMP  = CDABS(DR(J))
               AVG   = AVG + DTMP * DN(J)
               DS(J) = DTMP
            END IF
 70      CONTINUE
!C
!C     ZUILSR extracts the largest MIN(K,LENL) elements from DS, but none
!C     smaller than AVG.  The corresponding  indices are  returned as the
!C     last K indices in JI, with K updated as necessary.  These elements
!C     will form the Ith row of L.
!C
         JPTR = K
         IF (K.NE.0) AVG = TOL * AVG  / DBLE(K)
         CALL ZUILSR (K,LENL,AVG,JI,DS)
         KK = IL(I)
         IF (KK+K-1.GT.NZLMAX) INFO = 1
         IF (INFO.EQ.0) THEN
            DO 80 JJ = JPTR, JPTR-K+1, -1
               J      = JI(JJ)
               JL(KK) = J
               L(KK)  = DR(J)
               KK     = KK + 1
 80         CONTINUE
         END IF
         IL(I+1) = KK
!C
!C     Sort the new row in order of increasing indices.
!C
         K = IL(I)
         CALL ZSPSRT (IL(I+1)-K,L(K),JL(K))
!C
!C     Set up the sort in DS  and compute the average value.  Since these
!C     are the elements U(I,J), they are left unscaled.
!C
         K    = 0
         AVG  = 0.0D0
         DO 90 J = I+1, NROW
            IF (JT(J)) THEN
               K     = K + 1
               JI(K) = J
               DTMP  = CDABS(DR(J))
               AVG   = AVG + DTMP
               DS(J) = DTMP
            END IF
 90      CONTINUE
!C
!C     ZUILSR extracts the largest MIN(K,LENU) elements from DS, but none
!C     smaller than AVG.  The corresponding  indices are  returned as the
!C     last K indices in JI, with K updated as necessary.  These elements
!C     will form the Ith row of U.
!C
         JPTR = K
         IF (K.NE.0) AVG = TOL * AVG / DBLE(K)
         CALL ZUILSR (K,LENU,AVG,JI,DS)
         KK  = IU(I)
         AVG = 0.0D0
         IF (KK+K-1.GT.NZUMAX) INFO = 1
         IF (INFO.EQ.0) THEN
            DO 100 JJ = JPTR, JPTR-K+1, -1
               J      = JI(JJ)
               U(KK)  = DR(J)
               JU(KK) = J
               KK     = KK + 1
               AVG    = AVG + DS(J)
 100        CONTINUE
         END IF
         IU(I+1) = KK
         IF (K.NE.0) AVG = AVG / DBLE(K)
!C
!C     Sort the new row in order of increasing indices.
!C
         K = IU(I)
         CALL ZSPSRT (IU(I+1)-K,U(K),JU(K))
!C
!C     Extract the diagonal element.
!C     If it is small relative to the average, set it to the average. If
!C     the average is still small, set the diagonal element to 1.0.
!C
         J    = 0
         DIAG = DR(I)
         DTMP = AVG + CDABS(DIAG)
         IF (DTMP.EQ.AVG) THEN
            DR(I) = DCMPLX(AVG,0.0D0)
            J = 1
         END IF
         DTMP = 1.0D0 + CDABS(DR(I))
         IF (DTMP.EQ.1.0D0) THEN
            DR(I) = ZONE
            J = 1
         END IF
         DUINV(I) = ZONE / DR(I)
         IREPL    = IREPL + J
!C
 110  CONTINUE
!C
!C     Done.
!C
!CAB      WRITE (6,'()')
      IF (IREPL.NE.0) WRITE (6, '(A22,I8)') 'Small diagonals found:',   &
     &IREPL
!C
      WRITE (6,'(A15,I10)') 'Elements in L: ', IL(NROW+1)+NROW-1
      WRITE (6,'(A15,I10)') 'Elements in U: ', IU(NROW+1)+NROW-1
      IF (INFO.NE.0) THEN
         WRITE (6,'(A40)') 'WARNING: Insufficient storage allocated.'
      END IF
!C
      RETURN
      END
