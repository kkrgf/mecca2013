      subroutine gsvJCBsp(a,ndmat,                                      &
     &                   nnptr,nnclmn,ndimnn,                           &
     &                   cof,ndcof,                                     &
     &                   itype,kkrsz,natom,precond,                     &
     &                   niter,tolinv,iprint)
!c     ================================================================
!c
      implicit real*8(a-h,o-z)
!c
!c      To find solution of equation:  A*X=1  (A=1-G*t)
!c      using iterative Transpose-Free-Quasi-Residual Algorithm
!c
!c      Jacobi-like preconditioner is used
!c
!c      and then output:  cof = X  (tau = t*cof)
!c
!c
!c      niter -- maximum number of iterations
!c
!c     ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!c
      integer ndmat,natom,kkrsz
      complex*16 a(*)
!c  a(ndmat,1:natom), ndmat .ge. maxclstr*kkrsz**2

      integer nnptr(0:1,natom),ndimnn,nnclmn(2,ndimnn,natom)
!CALAM      complex*16 bmt(ndbmt*ndbmt,*)
!c      complex*16 vecs(kkrsz*natom,*)
      integer ndcof
!CALAM      complex*16 t(ndcof,ndcof,*)
      complex*16 cof(ndcof,ndcof,natom)
      integer    itype(natom)
      logical precond
!CALAM      integer nblck,niter,iprint
      real*8 tolinv

!c      complex*16 sum,sum0
      complex*16 czero,cone
      integer cx,cb,revcom
      parameter (czero=(0.d0,0.d0),cone=(1.d0,0.d0))

      integer erralloc
      complex*16,  allocatable :: value(:)
      complex*16,  allocatable :: vecs(:,:)
      complex*16,  allocatable :: vecstmp(:,:)
      integer nd2vecs
      parameter (nd2vecs=9)

!c      complex*16,  allocatable :: tmpmtr(:,:)
!c      complex*16,  allocatable :: tmpmtr1(:,:)
!c      complex*16,  allocatable :: tmpmtr2(:,:)

      integer,     allocatable :: iptrow(:)
      integer,     allocatable :: indcol(:)
      integer INFO(4)
      real*8  TOL

      character sname*10
      parameter (sname='gsvJCBsp')

      complex*16 aij,aii
      integer isave/0/,j,ii,i,ierr
!c      integer isave/0/,ntry,j,jj,ii,i,l1mx,l1mn,ierr
      integer nrmat,ne,nlim,nlen,jn,jns,ndim
      integer ns1,nsub1,nsub2,l1
      real*8 time1,time2
      save isave
!CDEBUG
      real timeEn,timeIneq
      common /Alltime/timeEn,timeIneq
!CDEBUG

      if(isave.eq.0) then
       write(6,*)
       write(6,*)                                                       &
     & ' ROUTINE  '//sname//' BASED ON SPARSE'                          &
     &            ,' ITERATIVE <<TFQMR>> ALGORITHM'
       write(6,*)                                                       &
     &     '          IS USED TO INVERT MATRIX'
       write(6,*) '  MAXIMUM NUMBER OF ITERATIONS = ',niter,            &
     &     ' AND TOLINV=',real(tolinv)
       write(6,*)
       isave=1
      end if

!CDEBUG
       time2 = 0.d0
       call timel(time2)
!CDEBUG
      nrmat = kkrsz*natom

      allocate(vecs(1:nrmat,nd2vecs),vecstmp(1:nrmat,2),                &
     &         iptrow(1:nrmat+1),                                       &
     &         stat=erralloc)
      if(erralloc.ne.0) then
      write(6,*) ' NRMAT=',nrmat
      write(6,*) ' ND2VECS+1=',nd2vecs+1
      write(6,*) ' ERRALLOC=',erralloc
      call fstop(sname//' :: ALLOCATION MEMORY PROBLEM')
      end if

      if(precond) then
       i=0
       do iatom=1,natom
      indI = (iatom-1)*ndmat
      do inn=1,nnptr(0,iatom)
       if(nnclmn(2,inn,iatom).eq.1) go to 5
      end do
      write(6,*) '  IATOM=',iatom
      call fstop(sname//' nnclmn(2,*,iatom).ne.1 ???')
5       continue
      indNN = (inn-1)*(kkrsz*kkrsz)
      do lm=1,kkrsz
!c
!c   ndmat should be equal to (kkrsz*kkrsz)*maxclstr
!c
!c                         aii = conjg(a((lm-1)*kkrsz+lm+indNN,iatom))
       aii = conjg(a(indI+(lm-1)*kkrsz+lm+indNN))

!c   aii = conjg(A[i,i])
!c         i = (iatom-1)*kkrsz+lmi
       i = i+1
       vecstmp(i,1) = aii
      end do
       end do
      else
       do i=1,nrmat
      vecstmp(i,1) = cone
       end do
      end if

      ne = 0
      i=0
      do iatom=1,natom
      indI = (iatom-1)*ndmat
      do lmi=1,kkrsz
!c         i = (iatom-1)*kkrsz+lmi
       i = i + 1
       aii = vecstmp(i,1)
       indLI = indI+lmi-kkrsz
       do jnn=1,nnptr(0,iatom)
        indIJ = indLI+(jnn-1)*(kkrsz*kkrsz)
        do lmj=1,kkrsz
         if(a(indIJ+lmj*kkrsz).ne.czero) then
          a(indIJ+lmj*kkrsz) = a(indIJ+lmj*kkrsz)*aii
          ne = ne+1
           end if
        end do
       end do
      end do
      end do

      if(iprint.ge.0) then
       write(6,*) ' NE=',ne,' NRMAT*NRMAT=',nrmat*nrmat
      end if

      allocate(value(1:ne),                                             &
     &         indcol(1:ne),                                            &
     &         stat=erralloc)
      if(erralloc.ne.0) then
      write(6,*) ' NE=',ne
      write(6,*) ' NRMAT=',nrmat
      write(6,*) ' NDMAT=',ndmat
      write(6,*) ' ALLOCATION (C16) =',ne+nrmat+(nrmat+1+ne)/4+1
      write(6,*) ' ERRALLOC=',erralloc
      call fstop(sname//' :: ALLOCATION MEMORY PROBLEM')
      end if

      kelem = 0
      iptrow(1) = 1

      i = 0
      do iatom=1,natom
       indI = (iatom-1)*ndmat
       do lmi=1,kkrsz
!c        i = (iatom-1)*kkrsz+lmi
      i = i + 1
      do inn=1,nnptr(0,iatom)
       jatom = nnclmn(1,inn,iatom)
       indJ = (lmi-kkrsz) + (inn-1)*(kkrsz*kkrsz)
       do lmj=1,kkrsz
        j = (jatom-1)*kkrsz+lmj
        lmij = lmj*kkrsz +indJ
!c                          lmij = (lmj-1)*kkrsz + lmi
!c                          aij  = a(lmij,iatom)
        aij = a(indI+lmij)
        if(aij.ne.czero) then
         kelem = kelem+1
         value(kelem) = aij
         indcol(kelem) = j
        end if
       end do
      end do
      iptrow(i+1) = kelem+1
       end do
      end do

!c      call ge2crs(a,ndmat,nrmat,tolinv,value,iptrow,indcol,ne,2)

      if(iprint.ge.0) then
       itmax=0
       itmin=niter
       anlim = 0.d0
      end if

!CDEBUG
       time1 = time2
       call timel(time2)
       timePreit = time2-time1
!CDEBUG

      ntime = 0
!c      nsub0 = 0
      do nsub1=1,natom

       ns1 = itype(nsub1)
       Jns = (nsub1-1)*kkrsz

       do nsub2=1,nsub1-1
      if(ns1.eq.itype(nsub2)) then            ! copy
       do l1 = 1,kkrsz
        do i=1,kkrsz
         cof(i,l1,nsub1) = cof(i,l1,nsub2)
        end do
       end do
       go to 10
      end if
       end do

       ntime = ntime+1

       do l1=1,kkrsz
      j = Jns+l1

      call zerooutC(vecs(1,2),natom*kkrsz)

      vecs(j,2) = cone*vecstmp(j,1)
!c
!c                                        VECS(*,2) = 1*Precond[A]
!c

      NDIM = nrmat
      NLEN = nrmat
!c     NROW = nrmat
      NLIM = niter
      TOL = tolinv
      INFO(1) = 0
      INFO(2) = 0
      INFO(3) = 0
      INFO(4) = 0

80      CALL ZUTFXm (NDIM,NLEN,NLIM,VECS,TOL,INFO,jns+1,kkrsz)

!C
!C     Perform matrix-vector multiplications when needed.
!C
      IERR   = INFO(1)
      REVCOM = INFO(2)
      CX     = INFO(3)
      CB     = INFO(4)

!C
!C     Multiply VECS(1,CX) with the preconditioned matrix.
!C
      IF (REVCOM.EQ.1) THEN

!CDEBUG        call ZSPAXB (nlen,value,iptrow,indcol,' U ',vecs(1,cx),vec !s(1,cb))
      call ZSPMMV (nlen,value,iptrow,indcol,vecs(1,cx),                 &
     &               vecs(1,cb))
       GO TO 80
!C
!C     Multiply VECS(1,CX) with the preconditioned transpose.
!C
      ELSE IF (REVCOM.EQ.2) THEN

      call ZSPATX (NLEN,value,iptrow,indcol,' U ',vecs(1,cx),vecs(1,cb))
       GO TO 80
      END IF
!C
!C     Check why the solver stopped (this could be more compact).
!C
      IF (IERR.EQ.0) THEN
!c         WRITE (6,'(A32)') 'The residual norm has converged.'
         GO TO 90
      ELSE IF (IERR.EQ.1) THEN
       WRITE (6,*) sname//': IERR=1 '
       WRITE (6,*) 'Invalid reverse communication call.'
       call fstop(sname//'Invalid reverse communication call.')
      ELSE IF (IERR.EQ.2) THEN
       WRITE (6,*) sname//': IERR=2 '
       WRITE (6,*) 'Invalid inputs encountered.'
       call fstop(sname//'Invalid inputs encountered.')
      ELSE IF (IERR.EQ.4) THEN
       WRITE (6,*) sname//': IERR=4 '
       WRITE (6,*) ' The algorithm did not converge.'
       call fstop(sname//'The algorithm did not converge.')
      ELSE IF (IERR.EQ.8) THEN
          WRITE (6,*) sname//': IERR=8 '
          WRITE (6,*) 'The algorithm broke down.'
          call fstop(sname//'The iterative algorithm broke down.')
      ELSE
       WRITE (6,*) ' Unknown INFO code:  IERR=', IERR
!c        call fstop(sname//' Unknown IERR code ')
      END IF
      write(6,*) ' NE=',ne
      write(6,*) ' NRMAT=',nrmat
      write(6,*) ' NDMAT=',ndmat

      WRITE (6,*) 'WARNING:: The algorithm did not converge after ',    &
     &                niter,' iterations'
      niter = -1

      do i=1,nrmat
       aii = cone/vecstmp(i,1)
       do j=1,nrmat
        a((j-1)*ndmat+i) = a((j-1)*ndmat+i)*aii
       end do
      end do
      go to 20

90      continue

      if(iprint.ge.0) then
       itmax = max(itmax,nlim)
       itmin = min(itmin,nlim)
       anlim = anlim + NLIM
      end if

!c                                          we need only diagonal blocks

!c        Jns = (nsub1-1)*kkrsz

      do ii=1,kkrsz
!CDEBUG         sum = czero
!CDEBUG         do jj=1,kkrsz
!CDEBUGc          Jn = (nsub1-1)*kkrsz+jj
!CDEBUG          Jn = Jns +jj
!CDEBUG          sum = sum + t(ii,jj,ns1)*vecs(Jn,1)
!CDEBUG         end do
!CDEBUG         cof(ii,l1,nsub1) = sum

!c          Jn = (nsub1-1)*kkrsz+ii
        Jn = Jns +ii
        cof(ii,l1,nsub1) = vecs(Jn,1)
!CDEBUG
      end do
       end do

10     continue
      end do

20     continue

!CDEBUG
      time1 = time2
      call timel(time2)
      tsolve = (time2-time1)/ntime
      if(iprint.ge.0) then
       write(*,*) sname//' :: SPARSEINV-time=',real(tsolve)             &
     &            ,real(tsolve*natom),real(tsolve*natom+timePreit)
      end if
      timeIneq = timeIneq + tsolve*natom+timePreit
!CDEBUG

      deallocate(value,vecs,vecstmp,iptrow,indcol,stat=erralloc)

      if(iprint.ge.0) then
       if(precond) then
      write(6,1001) real(anlim/nrmat),itmin,itmax
1001   format(f10.6,2i5,                                                &
     &           ' ITERAT/NRMAT,ITMAX,ITMIN (PTFX-SPRS-JCB)')
       else
      write(6,1002) real(anlim/nrmat),itmin,itmax
1002   format(f10.6,2i5,                                                &
     &           ' ITERAT/NRMAT,ITMAX,ITMIN (TFX-SPRS)')
       end if
      end if

      if(erralloc.ne.0) then
      write(6,*) ' NE=',ne
      write(6,*) ' ERRALLOC=',erralloc
      call fstop(sname//' :: DEALLOCATION MEMORY PROBLEM')
      end if

      return
      end
