!BOP
!!ROUTINE: calcCoulombShC
!!INTERFACE:
      subroutine calcCoulombShC(nface,mpoint,faceVrtxs,aunit,nmom,      &
     &                                            rmomm1,energy,ylmRmom)
!!DESCRIPTION:
! calculates dimensionless r-moments, {\tt rmom}, and Coulomb energy, {\tt energy},
! for Voronoi polyhedron defined by {\tt nface, mpoint, faceVrtxs}; {\tt aunit} is 
! a cubic root of VP volume and used for normalization 
!

!!DO_NOT_PRINT
      implicit none
!!DO_NOT_PRINT

!!ARGUMENTS:
      integer, intent(in)  :: nface
      integer, intent(in)  :: mpoint(nface)
      real(8), intent(in)  :: faceVrtxs(nface,3,*)
      real(8), intent(in)  :: aunit                          !  aunit**3 = VP-volume
      integer, intent(in)  :: nmom
      real(8), intent(out), optional :: rmomm1               ! normalized moment r**(-1)
      real(8), intent(out), optional :: energy               ! normalized Coulomb energy
      complex(8), intent(out), optional :: ylmRmom(0:,1:)    ! normalized multipoles, r**n*r**l*Ylm
!!REVISION HISTORY:
! Initial Version - A.S. - 2013
!EOP
!
!BOC
      integer, external :: g_Lmltpl
      real(8), parameter :: one=1.d0,zero=0.d0
      integer, parameter :: nShC = 16
      real(8), parameter :: epsShC = 2.d-8
      real(8), parameter :: pi=3.1415926535897932384d0
      real(8), parameter :: r2a=(4.d0*pi/3)**(dble(1)/3)
      integer, allocatable :: listvrt(:,:)
      integer, allocatable ::  nwF(:)
      real(8), allocatable :: faceV(:,:,:)
      real(8) :: alphen1,alphen2,alphen3,en1,en2,rws 
      real(8) :: tmprmom(-1:nmom)
      integer nf,nf0,i,j,N1,N2,N3,Lmax
      real(8), parameter :: R0(3)=0
      nf = 0
      do i=1,nface
       nf = nf + (mpoint(i)-2)
      end do

      allocate(listvrt(3,maxval(mpoint)))
      allocate(nwF(nf),faceV(3,3,nf))

      rws = aunit/r2a   ! 4*pi/3*rws**3 = VP-volume
      nwF(1:nf) = 1
      nf0 = 0
      do i=1,nface
       call facetriang(mpoint(i),listvrt)
       do j=1,mpoint(i)-2
        nf0 = nf0+1
        faceV(1:3,1,nf0) = faceVrtxs(i,1:3,listvrt(1,j))/aunit
        faceV(1:3,2,nf0) = faceVrtxs(i,1:3,listvrt(2,j))/aunit
        faceV(1:3,3,nf0) = faceVrtxs(i,1:3,listvrt(3,j))/aunit
       end do
      end do

      if ( present(energy) ) then
       alphen1 = zero
       alphen2 = zero
       alphen3 = zero
       N1 = nShC
       N2 = 2*N1
       N3 = 2*N2
       call energyShC(N1,nf,nwF,faceV,alphen1)
       call energyShC(N2,nf,nwF,faceV,alphen2)
!       energy = (N2**2*alphen2-N1**2*alphen1)/(N2**2-N1**2)
       call energyShC(N3,nf,nwF,faceV,alphen3)

       en1 = alphen2 + (alphen2-alphen1)/3
       en2 = alphen3 + (alphen3-alphen2)/3
       energy = en2 +  (en2-en1)/15
      end if

      if ( nmom>=-1 ) then
       if ( present(ylmRmom) ) then
        Lmax = max(g_Lmltpl(1),g_Lmltpl(-1))
        call ylmShC(R0,nf,nwF,faceV,Lmax,nmom,ylmRmom)
       end if
       if ( present(rmomm1) ) then
        call rmomShC(nf,nwF,faceV,nmom,tmprmom)
        rmomm1 = tmprmom(-1)
       end if
!       call rmomShC(nf,nwF,faceV,nmom,tmprmom)
!       rmomm1 = tmprmom(-1)
      end if

      deallocate(faceV,nwF,listvrt)
      return

!EOC
      contains

!BOP
!!IROUTINE: facetriang
!!INTERFACE:
       subroutine facetriang(Nf,listvrt)
!!REMARKS:
! private procedure of subroutine calcCoulombShC
!EOP
!
!BOC
       implicit none
!c
!c   simple triangulization of polygon
!c
!c   Nf -- number of face vertexes
!c
       integer Nf
       integer listvrt(3,*)
       real*8 aindex(NF)
       integer i

       do i=1,Nf
        aindex(i) = i
       end do

       do i=Nf,3,-1
        listvrt(1,Nf-i+1) = nint(aindex(1)) 
        listvrt(2,Nf-i+1) = nint(aindex(i)) 
        listvrt(3,Nf-i+1) = nint(aindex(i-1)) 
       end do

       return
!EOC
       end subroutine facetriang
!
!BOP
!!IROUTINE: energyShC
!!INTERFACE:
      subroutine energyShC(N,nf,nwF,faceV,energy)
!!REMARKS:
! private procedure of subroutine calcCoulombShC
!EOP
!
!BOC
      implicit none
! 
!      electrostatic energy calculation for polyhedron with Volume=1;
!      polyhedron is defined by triangulated faces (faceV-array),       
!      
      integer N
      integer nf
      integer nwF(nf)
      real(8) :: faceV(3,3,nf)    ! 1st dim - {x,y,z}, 2nd dim - triangle vertices index, 3rd dim - face index
      real(8) :: energy           ! normalized Coulomb energy
!c                            energy = 1.d0 for sphere of Volume=1 and rho=1
!c                            energy = 0.973077977 for cube of Volume=1 and rho=1
!c
!      real(8) :: time1,time2,timeEn
      real(8) :: SphRad,SphEn,dV,en0
      real(8) :: tetrsum
      real(8), parameter :: R0(3)=0
      integer j,i

      real(8), parameter :: VolmPol=1.d0,zero=0.d0
      real(8), parameter :: pi=3.1415926535897932384d0
      real(8), parameter :: pi4d3inv=3.d0/(4.d0*pi)

      SphRad = (VolmPol*pi4d3inv)**(1.d0/3.d0)
      SphEn = 0.6d0*VolmPol**2/SphRad

      dV = VolmPol/dble(N)**3

      energy = zero
!      timeEn = zero
!      time1 = zero
!      call timel(time2)
      do j=1,nf
      if(nwF(j).gt.0) then
       en0 = zero
       do i=1,nf
        call  tetrint(R0,                                               &
     &                    faceV(1:3,1,j),                               &
     &                    faceV(1:3,2,j),                               &
     &                    faceV(1:3,3,j),                               &
     &                    faceV(1:3,1,i),                               &
     &                    faceV(1:3,2,i),                               &
     &                    faceV(1:3,3,i),                               &
     &                    dV,                                           &
     &                    tetrsum)
        en0    = en0+tetrsum
       end do
       energy = energy + en0*nwF(j)
      end if
      end do
      energy = (0.5d0*energy)/SphEn

      return
!EOC
      end subroutine energyShC
!
!BOP
!!IROUTINE: tetrint
!!INTERFACE:
      subroutine tetrint(R0,Fj1,Fj2,Fj3,                                &
     &                        Fi1,Fi2,Fi3,                              &
     &                     dV,                                          &
     &                     tetrsum)
!!REMARKS:
! private procedure of subroutine calcCoulombShC
!EOP
!
!BOC
      implicit none
!c
!c    R0 - top of a "tetrahedron"
!c    {Fj1,Fj2,Fj3} - base of a tetrahedron to calculate j-i-"energy" term
!c     hn           -- perpendicular to {Fj}, |hn|=1
!c    {Fi1,Fi2,Fi3} - base of a tetrahedron to calculate "potential"
!c                    in each point from i-tetrahedron
!c
!c     N - number of integration layers
!c
!c    tetrsum == Integral[ 1/|R-R'|, {R' is from (R0,Fi)}
!c                                   {R  is from (R0,Fj)} ]
!c
      real(8), intent(in) :: R0(3)
      real(8), intent(in) :: Fj1(3),Fj2(3),Fj3(3)
      real(8), intent(in) :: Fi1(3),Fi2(3),Fi3(3)
      real(8), intent(in) :: dV
      real(8), intent(out) :: tetrsum

      integer N
      real(8) :: VA1(3),VA2(3)
      real(8) :: hn(3)
      real(8) :: h0

      real(8) :: H,a1,a2,csa,AreaN,vi,h1,h2,cosds(1)
      real(8) :: q1(3),q2(3),q3(3),Pm(3)

      integer i,j,k,M,Mp

      real(8), allocatable :: Ri(:),Zi(:)

      tetrsum = 0.d0
      if ( dV <= 0.d0 ) return
      VA1 = Fj2-Fj1
      a1 = sqrt(dot_product(VA1,VA1))
      VA1 = VA1/a1
      VA2 = Fj3-Fj1
      a2 = sqrt(dot_product(VA2,VA2))
      VA2 = VA2/a2
      csa = dot_product(VA1,VA2)
      AreaN = 0.5d0*a1*a2*sqrt(1.d0-csa*csa)

      call vectprd(VA1,VA2,hn)
      hn = hn / sqrt(dot_product(hn,hn))
      H = abs(dot_product(Fj1-R0,hn))
      hn = (Fj1 - R0)/H

!c
!c       mesh generation
!c
      h0 = dv**(1.d0/3.d0)
      N = int(H/h0)+1
      h0 = H/N

      allocate(Ri(0:N),Zi(1:N))

      Ri(0) = 0.d0
      do i=1,N
       Ri(i) = h0*i
      end do

      Zi(1) = 0.75d0*Ri(1)
      do i=2,N
       Zi(i) = 0.75d0*(Ri(i)**4-Ri(i-1)**4)/(Ri(i)**3-Ri(i-1)**3)
      end do

      do i = 1,N
!c
!c  layer: R(i-1) < z < R(i) ;   Zi -- centroid of the 3-pyramid  (z-axis == hn)
!c
       vi = (AreaN/(3.d0*H**2))*                                        &
     &             (Ri(i)**3-Ri(i-1)*Ri(i-1)*Ri(i-1))

       M = int(sqrt(vi/dV))+1         ! M**2 -- number of prisms in the layer

       Mp = M**2

       vi = vi/Mp                          ! volume of each prism

       h1 = (a1/H*Zi(i))/M
       h2 = (a2/H*Zi(i))/M

       do j=0,M-1

        do k=0,(M-1)-j
         q1 =     j*h1*VA1 +     k*h2*VA2       !
         q2 =     j*h1*VA1 + (k+1)*h2*VA2       !
         q3 = (j+1)*h1*VA1 +     k*h2*VA2       !

!C           Pm = Fj1+Zi(i)*hn + (q1+q2+q3)/3.d0
         Pm = R0 + Zi(i)*hn + (q1+q2+q3)/3.d0

         call momntint(Pm,Fi1,Fi2,Fi3,-1,cosds)

         tetrsum = tetrsum + cosds(1)*vi

        end do

        do k=(M-1)-j,1,-1
         q1 = (j+1)*h1*VA1 +     k*h2*VA2        !
         q2 = (j+1)*h1*VA1 + (k-1)*h2*VA2        !
         q3 =     j*h1*VA1 +     k*h2*VA2        !

         Pm = R0 + Zi(i)*hn + (q1+q2+q3)/3.d0

         call momntint(Pm,Fi1,Fi2,Fi3,-1,cosds)

         tetrsum = tetrsum + cosds(1)*vi

        end do
       end do
      end do
      deallocate(Ri,Zi)

      return
!EOC
      end subroutine tetrint
!
!BOP
!!IROUTINE: rmomShC
!!INTERFACE:
      subroutine rmomShC(nf,nwF,faceV,nmom,rmom)
!!REMARKS:
! private procedure of subroutine calcCoulombShC
!EOP
!
!BOC
      implicit none
!
!   rmom(-1) -- potential for r=0
!   rmom(0)  -- volume, should be equal to 1 (if normalized)
!   rmom(n)  -- n-radial_moment; integral{ over V.P. | r**n }
!           output rmom is sphere-normalized      
!      polyhedron is defined by triangulated faces (faceV-array),       
!
      integer nf
      integer nwF(nf)             ! number of equivalent faces
      real(8) :: faceV(3,3,nf)    ! 1st dim - {x,y,z}, 2nd dim - triangle vertices index, 3rd dim - face index
      integer, intent(in) :: nmom
      real(8), intent(out) :: rmom(-1:nmom)
      real(8) :: SphRad,SphMom
      real(8) :: rmomI(-1:nmom)
      real(8), parameter :: R0(3)=0
      integer j
      real(8), parameter :: one=1.d0,zero=0.d0
      real(8), parameter :: pi=3.1415926535897932384d0
      real(8), parameter :: pi4d3inv=3.d0/(4.d0*pi)
      rmom = zero
      do j=1,nf
       if(nwF(j).gt.0) then
        call momntint(R0,                                               &
     &                    faceV(1:3,1,j),                               &
     &                    faceV(1:3,2,j),                               &
     &                    faceV(1:3,3,j),                               &
     &                    nmom,rmomI(-1:nmom))  
        rmom(-1:nmom) = rmom(-1:nmom) + rmomI(-1:nmom)*nwF(j) 
       end if
      end do
      SphRad = ((one)*pi4d3inv)**(1.d0/3.d0)
!      SphMom = 1.5d0*(one)/SphRad          ! for VolmPol=1.d0
      rmom(-1) = rmom(-1)/(1.5d0*(one)/SphRad)
      if ( nmom>=0 ) then
       SphMom = one
       do j = 1,nmom
!        SphMom = SphMom*SphRad*(one-one/(j+3.d0))
        SphMom = (4*pi)/(j+3)*SphRad**(j+3)
        rmom(j) = rmom(j)/SphMom
       end do
      end if
      return
!EOC
      end subroutine rmomShC
!
!BOP
!!IROUTINE: ylmShC
!!INTERFACE:
      subroutine ylmShC(R0,nf,nwF,faceV,Lmax,nmom,ylmRmom)
!!REMARKS:
! private procedure of subroutine calcCoulombShC
!EOP
!
!BOC
      implicit none
!
!  ylmRmom(n) -- n-radial_moment;
!                integral{ over V.P. | r**n *r**L*Y_LM } -
!                 integral{ over WS sphere | r**n *r**L*Y_LM }
!      polyhedron is defined by triangulated faces (faceV-array),       
!      R0 -- origin for Y_LM (i.e. in the integral Y_LM = Y_LM(r+R0)
!
      integer, intent(in) :: nf
      integer, intent(in) :: nwF(nf)             ! number of equivalent faces
      real(8), intent(in) :: faceV(3,3,nf)    ! 1st dim - {x,y,z}, 2nd dim - triangle vertices index, 3rd dim - face index
      integer, intent(in) :: Lmax
      integer, intent(in) :: nmom
      complex(8), intent(out) :: ylmRmom(0:,1:)
      real(8), intent(in) :: R0(3)
      complex(8) :: ylmRe(0:nmom),ylmIm(0:nmom)
      real(8) :: chebc(0:nmom)
      real(16) :: buff(0:nmom)
      real(8) :: sphRad,sphMom,volmPol
      real(16) :: qp_chebc(0:nmom),qp_ylm(0:nmom)
      integer :: L,M,LM,lm1,lm2,j,k
      complex(8), parameter :: sqrtm1=(0.d0,1.d0)
      real(8), parameter :: zero=0.d0,one=1.d0
      real(8), parameter :: pi=3.1415926535897932384d0
      real(8), parameter :: third=1.d0/3.d0, pi4d3inv=3.d0/(4.d0*pi)
      real(8), parameter :: Y0 = one/sqrt(4.d0*pi)
      if ( nmom<0 ) return
      if ( Lmax<0 ) return
      ylmRmom = 0
      lm1 = 1
      lm2 = 1
      do L=0,Lmax,2   ! odd moments are assumed to be zero
       do M=0,L
        LM = L*(L+1)/2+M+1
        do j=1,nf
         if(nwF(j).gt.0) then
          call ylmtint(R0,                                              &
     &                    faceV(1:3,1,j),                               &
     &                    faceV(1:3,2,j),                               &
     &                    faceV(1:3,3,j),                               &
     &                    nmom,lm1,lm2,                                 &
     &                    L,M,epsShC,                                   &
     &                    ylmRe,ylmIm)
          ylmRmom(0:nmom,LM) = ylmRmom(0:nmom,LM) +                     &
     &                   ylmRe(0:nmom) + sqrtm1*ylmIm(0:nmom)
         end if
        end do
!c

       end do
      end do
      VolmPol = dreal(ylmRmom(0,1)) / Y0**2  ! it is expected to be equal to 1;
                                             ! ylmtint: Integral(Y00*Ylm)
!DEBUGPRINT      
      if ( abs(abs(VolmPol)-one) > epsShC ) then
       write(6,'(a,e11.3)') ' WARNING: rel.error in VP-volume is about '&
     &                      ,abs(VolmPol)-one
      end if
!DEBUGPRINT      
      sphRad = (one*pi4d3inv)**third  ! one==VolmPol
      do L=0,Lmax
       do M=0,L
        LM = L*(L+1)/2+M+1
        do j= 0,nmom
         ylmRmom(j,LM) = ylmRmom(j,LM)/sphRad**(j+L+3)
         if ( abs(ylmRmom(j,LM))<epsShC/10 ) then
          ylmRmom(j,LM)=0
!         else
!          ylmRmom(j,LM) = ylmRmom(j,LM)/Y0   ! Integral(Ylm)
         end if
        end do
       end do
      end do

!      do LM=1,(Lmax+1)*(Lmax+2)/2 
!       if ( abs(ylmRmom(0,LM))==zero ) cycle
!       do j= 0,nmom
!        call chebcoef(j,chebc)
!        qp_chebc(0:j) = real(chebc(0:j),16)
!        qp_ylm(0:j)   = real(ylmRmom(0:j,LM))
!        buff(j) = sum(qp_chebc(0:j)*qp_ylm(0:j))
!!        buff(j) = 0
!!        do k=0,j
!!         buff(j) = buff(j) + chebc(k)*ylmRmom(k,LM)
!!        end do
!        if ( abs(buff(j))<epsShC/10 ) buff(j)=0
!       end do
!!DEBUGPRINT
!        write(6,'(i3,a,(1x,16g14.5))') LM,' T-ylm=',buff(0:nmom)
!!DEBUGPRINT
!       ylmRmom(0:nmom,LM) = buff(0:nmom)    ! Chebyshev R-moments 
!      end do
!      ylmRmom(0:nmom,1) = ylmRmom(0:nmom,1)-one

!?norm      do j= 0,nmom
!?norm       ylmRmom(j,1) = ylmRmom(j,1)-one
!?norm       if ( abs(ylmRmom(j,1))<epsShC/10 ) ylmRmom(j,1)=0
!?norm      end do

      return
!EOC
      end subroutine ylmShC
!
      end subroutine calcCoulombShC

!BOP
!!ROUTINE: momntint
!!INTERFACE:
      subroutine momntint(R0,P1,P2,P3,nmom,rmoments)
!!DESCRIPTION:
! calculates tetrahedron r-moments
! {\bv
!     R0 -- origin
!     (P1,P2,P3) -- triangle in 3d
!
!      rmoments == integral[ |R-R0|**n dx dy dz over tetrahedron {R0,(R1,R2,R3)} ]
!       if R is a point from (R1,R2,R3) then
!       Theta is an angle between (R-R0) and a unit vector
!       perpendicular to the triangle
! \ev}

!!DO_NOT_PRINT
      implicit none
!!DO_NOT_PRINT

!!ARGUMENTS:
      real(8), intent(in) :: R0(3)
      real(8), intent(in) :: P1(3),P2(3),P3(3)
      integer, intent(in) :: nmom
      real(8), intent(out) :: rmoments(-1:nmom)
!!REVISION HISTORY:
! Initial Version - A.S. - 2007
!EOP
!
!BOC
      real(8) :: VA1(3),VA2(3),VA3(3),P0(3)
      real(8) :: a1,a2,a3
      real(8) :: csa,csb,tga,tgb,a2x,h,p0x,p0y

      integer, parameter :: ndfun=10
      real(8) :: funpar(ndfun)

      real(8), parameter :: err0=1.d-8

      integer ierr,i
      real(8) :: err,x0,x1,dsum

!      real*8 tridylog
!      external tridylog

      if(nmom.lt.-1) return

      rmoments = 0.d0

      P0  = P1-R0
      VA1 = P3-P1
      VA2 = P2-P1
      VA3 = VA2-VA1
      a1 = sqrt(dot_product(VA1,VA1))
      a2 = sqrt(dot_product(VA2,VA2))
      a3 = sqrt(dot_product(VA3,VA3))

      if(a2.gt.max(a1,a3)) then
       VA1 = VA2
       a1 = a2
       VA2 = P3-P1
       a2 = sqrt(dot_product(VA2,VA2))
       VA3 = VA2-VA1
       a3 = sqrt(dot_product(VA3,VA3))
      else if(a3.gt.max(a1,a2)) then
       P0 = P3-R0
       VA1 = VA3                      ! P2-P3
       a1 = a3
       VA2 = P1-P3
       a2 = sqrt(dot_product(VA2,VA2))
       VA3 = VA2-VA1
       a3 = sqrt(dot_product(VA3,VA3))
      end if

      if(abs(a1-a2-a3).le.1.d-7*(a1+a2+a3)) return

      csa = (dot_product(VA1,VA2)/(a1*a2))
      tga = sqrt(1.d0-csa*csa)/csa
      csb = -(dot_product(VA1,VA3)/(a1*a3))
      tgb = sqrt(1.d0-csb*csb)/csb
      a2x = a2*csa

      VA1 = VA1/a1
      VA2 = VA2 - dot_product(VA2,VA1)*VA1
      VA2 = VA2/sqrt(dot_product(VA2,VA2))
      call vectprd(VA1,VA2,VA3)
      VA3 = VA3/sqrt(dot_product(VA3,VA3))

      h = abs(dot_product(P0,VA3))
      p0x = dot_product(P0,VA1)
      p0y = dot_product(P0,VA2)

       err = err0
       x0 = 0.d0
       x1 = a2x
       funpar(1) = p0y
       funpar(2) = tga
       funpar(3) = p0x
       funpar(4) = h*h
       funpar(5) = p0y

       do i=-1,nmom
        funpar(6) = i
        call dgaus8m (tridylog,x0,x1,funpar,ERR,dsum,ierr)

        if(abs(ierr).gt.1) then
         write(*,*)
         write(*,*) ' ierr=',ierr
         write(*,*) ' x0,x1:',x0,x1
         write(*,*) ' p0x,p0y:',p0x,p0y
         write(*,*) ' tga,tgb:',tga,tgb
         write(*,*) ' a2x,a1,h=',a2x,a1,h
         write(*,*) ' R0:',r0
         write(*,*) ' P1:',p1
         write(*,*) ' P2:',p2
         write(*,*) ' P3:',p3
         write(*,*) ' I=',i
         write(*,*)
         write(*,*) ' ERR=',ERR
         write(*,*) ' dsum=',dsum
         write(*,*)

         stop ' momntint: integration error 1'
        end if

        rmoments(i) = rmoments(i) + dsum
       end do

       err = err0
       x0 = a2x
       x1 = a1
       funpar(1) = p0y+a1*tgb
       funpar(2) = -tgb
       funpar(3) = p0x
       funpar(4) = h*h
       funpar(5) = p0y

       do i=-1,nmom
        funpar(6) = i
        call dgaus8m (tridylog,x0,x1,funpar,ERR,dsum,ierr)
        if(abs(ierr).gt.1) then
         write(*,*)
         write(*,*) ' ierr=',ierr
         write(*,*) ' x0,x1:',x0,x1
         write(*,*) ' p0x,p0y:',p0x,p0y
         write(*,*) ' tga,tgb:',tga,tgb
         write(*,*) ' a2x,a1,h=',a2x,a1,h
         write(*,*) ' R0:',r0
         write(*,*) ' P1:',p1
         write(*,*) ' P2:',p2
         write(*,*) ' P3:',p3
         write(*,*) ' I=',i
         write(*,*)

         stop ' momntint: integration error 2'
        end if

        rmoments(i) = rmoments(i) + dsum

       end do

       do i=-1,nmom
        rmoments(i) = rmoments(i)/(i+3)*h
       end do

      return

!EOC
      contains

!BOP
!!IROUTINE: tridylog
!!INTERFACE:
      function tridylog(x,param)
!!REMARKS:
! private procedure of subroutine momntint
!EOP
!
!BOC
      implicit none
      real(8) :: tridylog
      real(8), intent(in) :: x
      real(8), intent(in) :: param(6)
      real(8) :: x2,ax
      real(8) :: y1,y2
      real(8) :: cy1,cy2
      real(8) :: acy1,acy2

      integer n,n1,i,m

      y1 = param(5)
      y2 = param(1) + param(2)*x

      n = nint(param(6))

      if(mod(n,2).eq.0) then
!c
       tridylog = y2 - y1
!c
       if(n.le.0) return
!c
       x2 = (param(3)+x)**2
       ax = param(4) + x2                  ! ="x*x+h*h"
       cy2 = ax+(y2*y2)
       cy1 = ax+(y1*y1)
       acy2 = y2
       acy1 = y1
       n1 = 0
       m = n/2
!c
      else
!c
       x2 = (param(3)+x)**2
       ax = param(4) + x2                  ! ="x*x+h*h"
       cy2 = ax+y2*y2
       cy1 = ax+y1*y1
       acy2 = sqrt(cy2)
       acy1 = sqrt(cy1)
       tridylog = log((y2+acy2)/(y1+acy1))
!c
       if(n.le.0) return
!c
       acy2 = y2/acy2
       acy1 = y1/acy1
       n1 = -1
       m = (n+1)/2
      end if

      do i=1,m
       n1 = n1+2
       acy2 = acy2*cy2
       acy1 = acy1*cy1
       tridylog = (acy2 - acy1 + n1*ax*tridylog)/(n1+1)
      end do
      return
!EOC
      end function tridylog
!      
      end subroutine momntint
!
      subroutine ylmtint(R0,P1,P2,P3,nmom,lm1,lm2,                      &
     &                     L,M,epsShC,                                  &
     &                     yyre,yyim)
!c
!c        R0 -- origin of the tetrahedron
!c        (P1,P2,P3) -- triangle in 3d (base of the tetrahedron)
!c
!c        yyre + sqrt(-1)*yyim --
!c         [ integral{over tetrahedron| Ylm1*conjg(Ylm2)*Y_LM} ]
!c
      implicit none

      real(8), intent(in) :: R0(3)
      real(8), intent(in) :: P1(3),P2(3),P3(3)

      integer, intent(in) :: nmom
      integer, intent(in) :: lm1,lm2,L,M
      real(8), intent(in) :: epsShC
      complex(8), intent(out) :: yyre(0:nmom)                !  YY*Re(Y)
      complex(8), intent(out) :: yyim(0:nmom)                !  YY*Im(Y)

      real*8 ylm1ylm2(0:nmom)
      real*8 VA1(3),VA2(3),VA3(3),P0(3)
      real*8 a1,a2,a3
      real*8 csa,csb,tga,tgb,a2x,h,p0x,p0y

      integer ndfun
      parameter (ndfun=21)
      real*8 funpar(ndfun)

      real*8 err0
      parameter (err0=1.d-8)

      integer ic,im,icim
      integer ierr,i,l1,l2,LM
      real*8 err,x0,x1,sum

!c        real*8 cosds

!      real*8 ylmintdy
!      external ylmintdy

      real*8 zero
      parameter (zero=0.d0)

      integer nerr/10/
      save nerr

      if(nmom.lt.0) return

      VA1 = P3-P1
      VA2 = P2-P1
      VA3 = VA2-VA1                   ! P2-P3
      a1 = sqrt(dot_product(VA1,VA1))
      a2 = sqrt(dot_product(VA2,VA2))
      a3 = sqrt(dot_product(VA3,VA3))
      if(abs(a1-a2-a3).le.1.d-7*(a1+a2+a3)) return

      P0 = P1 - R0

      if(a2.gt.max(a1,a3)) then
       VA1 = -VA2                     ! P1-P2
       a1 = a2
       VA2 = -VA3                     ! P3-P2
       a2 = a3
       P0 = P2 - R0
       VA3 = VA2-VA1
       a3 = sqrt(dot_product(VA3,VA3))
      else if(a3.gt.max(a1,a2)) then
       VA2 = -VA1                     ! P1-P3
       a2 = a1
       VA1 = VA3                      ! P2-P3
       a1 = a3
       P0 = P3 - R0
       VA3 = VA2-VA1
       a3 = sqrt(dot_product(VA3,VA3))
      end if

      csa = (dot_product(VA1,VA2)/(a1*a2))
      tga = sqrt(1.d0-csa*csa)/csa
      csb = -(dot_product(VA1,VA3)/(a1*a3))
      tgb = sqrt(1.d0-csb*csb)/csb
      a2x = a2*csa                    ! projection a2-->a1

      VA1 = VA1/a1
      VA2 = VA2 - dot_product(VA2,VA1)*VA1
      VA2 = VA2/sqrt(dot_product(VA2,VA2))
      call vectprd(VA1,VA2,VA3)
      VA3 = VA3/sqrt(dot_product(VA3,VA3))

      h = dot_product(P0,VA3)

        h = abs(h)

      p0x = dot_product(P0,VA1)
      p0y = dot_product(P0,VA2)

       err = err0
       x0 = 0.d0
       x1 = a2x

       funpar(5:7) = P0(1:3)
       funpar(8:10) = VA1(1:3)
       funpar(11:13) = VA2(1:3)

       funpar(14) = lm1
       funpar(15) = lm2
       l1 = sqrt(dfloat(lm1))
       if(l1*l1.eq.lm1) l1 = l1-1
       funpar(16) = l1
       l2 = sqrt(dfloat(lm2))
       if(l2*l2.eq.lm2) l2 = l2-1
       funpar(17) = l2
       funpar(18) = max(l1,l2,L)

       yyre(0:nmom) = dcmplx(zero,zero)
       yyim(0:nmom) = dcmplx(zero,zero)

       do icim=0,3
!c
!c  im=0,ic=0 -- real(YY*)*real(Y) (0)
!c  im=0,ic=1 -- imag*real         (1)
!c  im=2,ic=0 -- real*imag         (2)
!c  im=2,ic=1 -- imag*imag         (3)

        funpar(19) = icim      ! to calculate real/imag part of <YYY>
        ic = mod(icim,2)
        im = icim-ic

        if(im.eq.0) then
         LM = L*(L+1) +M +1
!CDEBUG
         err = min(err0,epsShC)
!CDEBUG
        else
         if(M.eq.0) cycle
         LM = L*(L+1) -M +1
         err = epsShC
        end if
        funpar(20) = LM
!c
        x0 = 0.d0
        x1 = a2x
        funpar(1) = zero
        funpar(2) = zero
        funpar(3) = tga
        ylm1ylm2(0:nmom)  = zero
        do i=0,nmom
         funpar(4) = i+L
         call dgaus8m (ylmintdy,x0,x1,funpar,err,sum,ierr)
         if(abs(ierr).gt.1.and.nerr.gt.0.and.abs(sum).gt.err)           &
     &      then
          write(*,*) ' WARNING: I=',i                                   &
     &                 ,' (L,M)=',LM,' icim =',icim
          write(*,*) ' ierr=',ierr
          write(*,*) ' err=',err
          write(*,*) ' x0,x1:',x0,x1
          write(*,*) ' p0x,p0y:',p0x,p0y
          write(*,*) ' tga,tgb:',tga,tgb
          write(*,*) ' a2x,a1,h=',a2x,a1,h
          write(*,*) ' R0:',r0
          write(*,*) ' P1:',p1
          write(*,*) ' P2:',p2
          write(*,*) ' P3:',p3
          write(*,*) ' I=',i
          nerr = nerr - 1
          write(*,*)
!CDEBUG      stop ' momntint: integration error 1'
         end if

         ylm1ylm2(i) = ylm1ylm2(i) + sum

       end do

        err = err0
        x0 = a2x
        x1 = a1

        funpar(1) = zero
        funpar(2) = a1*tgb
        funpar(3) = -tgb

        do i=0,nmom
         funpar(4) = i+L
         call dgaus8m (ylmintdy,x0,x1,funpar,err,sum,ierr)
         if(abs(ierr).gt.1.and.nerr.gt.0.and.abs(sum).gt.err)           &
     &      then
          write(*,*) ' WARNING: I=',i                                   &
     &                 ,' (L,M)=',LM,' icim =',icim
          write(*,*) ' ierr=',ierr
          write(*,*) ' sum=',sum,' err=',err
          write(*,*) ' x0,x1:',x0,x1
          write(*,*) ' p0x,p0y:',p0x,p0y
          write(*,*) ' tga,tgb:',tga,tgb
          write(*,*) ' a2x,a1,h=',a2x,a1,h
          write(*,*) ' R0:',r0
          write(*,*) ' P1:',p1
          write(*,*) ' P2:',p2
          write(*,*) ' P3:',p3
          write(*,*) ' I=',i
          nerr = nerr-1
          write(*,*)

!CDEBUG      stop ' momntint: integration error 2'
         end if

         ylm1ylm2(i) = ylm1ylm2(i) + sum

        end do

        if(icim.eq.0) then                                 ! real*real
         yyre(0:nmom) = yyre(0:nmom) +                                  &
     &                         dcmplx(ylm1ylm2(0:nmom),zero)
        else if(icim.eq.1) then                            ! imag*real
         yyre(0:nmom) = yyre(0:nmom) +                                  &
     &                         dcmplx(zero,ylm1ylm2(0:nmom))
        else if(icim.eq.2) then                            ! real*imag
         yyim(0:nmom) = yyim(0:nmom) +                                  &
     &                         dcmplx(zero,ylm1ylm2(0:nmom))
        else if(icim.eq.3) then                            ! imag*imag
         yyim(0:nmom) = yyim(0:nmom) -                                  &
     &                         dcmplx(ylm1ylm2(0:nmom),zero)
        end if

       end do                 ! icim-do

       do i=L,L+nmom
        yyre(i-L) = yyre(i-L)*(h/(i+3))
        yyim(i-L) = yyim(i-L)*(h/(i+3))
       end do

      return

      contains

      function ylmintdy(x,param)
      use universal_const
      implicit none
!      include 'lmax.h'
      real*8 x,ylmintdy
      real*8 param(ndfun)

      integer ngauss
      parameter (ngauss=6)
!c
!c ngauss=6
!c
      real(8), parameter :: xg(ngauss) = [                              &
     &     -0.932469514203d0                                            &
     &,    -0.661209386466d0                                            &
     &,    -0.238619186083d0                                            &
     &,     0.238619186083d0                                            &
     &,     0.661209386466d0                                            &
     &,     0.932469514203d0                                            &
     &       ]
!c
      real(8), parameter :: wg(ngauss) = [                              &
     &      0.171324492379d0                                            &
     &,     0.360761573048d0                                            &
     &,     0.467913934573d0                                            &
     &,     0.467913934573d0                                            &
     &,     0.360761573048d0                                            &
     &,     0.171324492379d0                                            &
     &       ]
!c
!C      parameter (ngauss=8)
!C
!C      real*8 xg(ngauss),wg(ngauss)
!Cc
!Cc ngauss=8
!Cc
!C      data xg/
!C     ,    -0.960289856498d0
!C     ,,   -0.796666477414d0
!C     ,,   -0.525532409916d0
!C     ,,   -0.183434642496d0
!C     ,,    0.183434642496d0
!C     ,,    0.525532409916d0
!C     ,,    0.796666477414d0
!C     ,,    0.960289856498d0
!C     *       /
!C      data wg/
!C     ,     0.10122853629d0
!C     ,,    0.22238103445d0
!C     ,,    0.31370664588d0
!C     ,,    0.36268378338d0
!C     ,,    0.36268378338d0
!C     ,,    0.31370664588d0
!C     ,,    0.22238103445d0
!C     ,,    0.10122853629d0
!C     *       /

      integer n,ig
      real*8 R(3),Rx(3)
      real*8 c,d,y1,y2,y,func
!      real*8 zero,half,one
!      parameter (zero=0.d0,half=0.5d0,one=1.d0)
!      real*8 pi
!      parameter (pi=3.1415926535897932384d0)
      real(8), parameter :: half = one/two
      real(8), parameter :: fourpi=four*pi

      integer lmax,l1,l2,lm1,lm2
      integer LM,icim
      real*8 r2(1)
      complex(8), allocatable :: ylm(:)  ! (ipkkr)

      y1 = param(1)
      y2 = param(2) + param(3)*x

      n = nint(param(4))

      c= half*(y2-y1)
      d= half*(y2+y1)

      Rx(1:3) = param(5:7) + param(8:10)*x
!c
      lm1  = nint(param(14))
      lm2  = nint(param(15))
      l1   = nint(param(16))
      l2   = nint(param(17))
      lmax = nint(param(18))
      icim = nint(param(19))
      LM = nint(param(20))

      allocate(ylm(max(1,LM,(lmax+1)**2)))
      ylm = 0
!c         lmax = max(l1,l2)
!c         l1 = sqrt(dfloat(lm1))
!c         if(l1*l1.eq.lm1) l1 = l1-1
!c         l2 = sqrt(dfloat(lm2))
!c         if(l2*l2.eq.lm2) l2 = l2-1
!c
      ylmintdy = zero
!c
!c        ngauss-point gaussian integration
!c
      if(LM.eq.1) then
       if(mod(icim,2).eq.0) then             ! real part
      do ig=1,ngauss
        y=c*xg(ig)+d
        R(1:3) = Rx(1:3) + param(11:13)*y
        call hrmplnm(zero,zero,zero,                                    &
     &                 R, 1, r2, ylm, 1,                                &
     &                      -10, 1, lmax , -1)
        func = dreal(ylm(lm1)*conjg(ylm(lm2)))                          &
     &            *sqrt(dot_product(R,R))**n
        ylmintdy = ylmintdy + wg(ig)*func
      enddo
       else                                  ! imag part
      do ig=1,ngauss
        y=c*xg(ig)+d
        R(1:3) = Rx(1:3) + param(11:13)*y
        call hrmplnm(zero,zero,zero,                                    &
     &                 R, 1, r2, ylm, 1,                                &
     &                      -10, 1, lmax , -1)

         func = aimag(ylm(lm1)*conjg(ylm(lm2)))                         &
     &           *sqrt(dot_product(R,R))**n

        ylmintdy = ylmintdy + wg(ig)*func
      enddo
       end if
      else
       if(icim.eq.0) then             ! real*real part
      do ig=1,ngauss
        y=c*xg(ig)+d
        R(1:3) = Rx(1:3) + param(11:13)*y
        call hrmplnm(zero,zero,zero,                                    &
     &                 R, 1, r2, ylm, 1,                                &
     &                      -10, 1, lmax , -1)
        func = dreal(ylm(lm1)*conjg(ylm(lm2)))*dreal(ylm(LM))           &
     &            *sqrt(dot_product(R,R))**n
        ylmintdy = ylmintdy + wg(ig)*func
      enddo
       else if(icim.eq.1) then ! imaginary*real part
      do ig=1,ngauss
        y=c*xg(ig)+d
        R(1:3) = Rx(1:3) + param(11:13)*y
        call hrmplnm(zero,zero,zero,                                    &
     &                 R, 1, r2, ylm, 1,                                &
     &                      -10, 1, lmax , -1)
        func = aimag(ylm(lm1)*conjg(ylm(lm2)))*dreal(ylm(LM))           &
     &           *sqrt(dot_product(R,R))**n
        ylmintdy = ylmintdy + wg(ig)*func
      enddo
       else if(icim.eq.2) then             ! real*imag part
      do ig=1,ngauss
        y=c*xg(ig)+d
        R(1:3) = Rx(1:3) + param(11:13)*y
        call hrmplnm(zero,zero,zero,                                    &
     &                 R, 1, r2, ylm, 1,                                &
     &                      -10, 1, lmax , -1)
         func = dreal(ylm(lm1)*conjg(ylm(lm2)))*aimag(ylm(LM))          &
     &            *sqrt(dot_product(R,R))**n
        ylmintdy = ylmintdy + wg(ig)*func
      enddo
       else if(icim.eq.3) then !           imag*imag part
      do ig=1,ngauss
        y=c*xg(ig)+d
        R(1:3) = Rx(1:3) + param(11:13)*y
        call hrmplnm(zero,zero,zero,                                    &
     &                 R, 1, r2, ylm, 1,                                &
     &                      -10, 1, lmax , -1)
        func = aimag(ylm(lm1)*conjg(ylm(lm2)))*aimag(ylm(LM))           &
     &           *sqrt(dot_product(R,R))**n
        ylmintdy = ylmintdy + wg(ig)*func
      enddo
       end if
      end if
!c
      ylmintdy = ylmintdy*c
      deallocate(ylm)

      return
      end function ylmintdy

      end subroutine ylmtint

!BOP
!!ROUTINE: calcCoulombShC1
!!INTERFACE:
      subroutine calcCoulombShC1(nmom,nsub,aunit,nnb,xyznb,itypnb,vp_j)
!!DESCRIPTION:
! calculates dimensionless ylm R-moments, {\tt vp\_j\%ylmPls},
! for Voronoi polyhedron defined by {\tt nface, mpoint, faceVrtxs} 
!
!!USES:
      use mecca_types, only : VP_data

!!DO_NOT_PRINT
      implicit none
!!DO_NOT_PRINT

!!ARGUMENTS:
      integer, intent(in)  :: nmom
      integer, intent(in)  :: nsub
      real(8), intent(in)  :: aunit
      integer, intent(in)  :: nnb
      real(8), intent(in)  :: xyznb(3,nnb)
      integer, intent(in)  :: itypnb(nnb)
      type (VP_data), pointer :: vp_j
!!REVISION HISTORY:
! Initial Version - A.S. - 2015
!EOP
!
!BOC
      integer, external :: g_Lmltpl
!      real(8), parameter :: one=1.d0,zero=0.d0
      real(8), parameter :: epsShC = 2.d-8
      real(8), parameter :: pi=3.1415926535897932384d0
      real(8), parameter :: r2a=(4.d0*pi/3)**(dble(1)/3)
      integer :: iflag,i,k,l,m,lm,lmax,lmaxm,lmx,nfc,np
      real(8) :: R0(3),ymom2du,rws
      real(8), allocatable :: vp_ylm(:,:)
!DELETE      real(8), allocatable :: rkYlm(:,:)
      real(8), allocatable :: rl(:),xyz(:,:)
      real(8), pointer :: subYl(:,:,:)

      integer n,j,n1  !DELETE
      real(8) :: rm0,rm1,rm1i !DELETE

      iflag = -1
      nfc = vp_j%fcount(nsub)
      lmaxm = g_Lmltpl(iflag)
      lmx = (lmaxm+1)*(lmaxm+2)/2
      np  = size(vp_j%rmag(0:0,:,:,:,1:nfc,1:1))
!      rws = aunit   
      rws = aunit/r2a   
      allocate(vp_ylm(lmx,np))
!DELETE      allocate(rkYlm(lmx,np))

      if ( allocated(vp_j%ylmMns(nsub)%array) )                         &
     &  deallocate(vp_j%ylmMns(nsub)%array)
      if ( allocated(vp_j%ylmMns(nsub)%indx) )                          &
     &  deallocate(vp_j%ylmMns(nsub)%indx)
!      if ( allocated(vp_j%ylmMns(nsub)%r0nn) )                          &
!     &  deallocate(vp_j%ylmMns(nsub)%r0nn)
      allocate(vp_j%ylmMns(nsub)%array(0:nmom,1:lmaxm+1,1:nnb)) 
      allocate(vp_j%ylmMns(nsub)%indx(1:nnb)) 
!      allocate(vp_j%ylmMns(nsub)%r0nn(1:nnb)) 
      vp_j%ylmMns(nsub)%indx = itypnb
      subYl => vp_j%ylmMns(nsub)%array

      allocate(rl(1:np),xyz(1:3,1:np))
      if ( nmom>=0 ) then
       rl(1:np) = reshape(vp_j%rmag(0,:,:,:,1:nfc,nsub),[np])/aunit
      else
       rl = 0
      end if
      xyz(1:3,1:np) = reshape(vp_j%rmag(1:3,:,:,:,1:nfc,nsub),[3,np])

!DEBUGPRINT
!      n1 = 0
!      rm0=0
!      rm1=0
!      rm1i=0
!      do n=1,nfc
!!       write(101,*) ' FACE ',n,' # r x y z vj'
!       do i=1,size(vp_j%vj,3)
!        do j=1,size(vp_j%vj,2)
!         do k=1,size(vp_j%vj,1)
!       n1 = n1+1
!!          write(101,'(i6,1x,5g18.7)')                                   &
!!     &         n1,vp_j%rmag(0:3,k,j,i,n,nsub),vp_j%vj(k,j,i,n,nsub)
!          rm0 = rm0+vp_j%vj(k,j,i,n,nsub)
!          rm1i = rm1i+vp_j%vj(k,j,i,n,nsub)/vp_j%rmag(0,k,j,i,n,nsub)
!          rm1 = rm1+vp_j%vj(k,j,i,n,nsub)*vp_j%rmag(0,k,j,i,n,nsub)
!         end do
!        end do
!       end do
!      end do
!!      write(6,*) ' DEBUG RM0=',rm0,                                     &
!!     &             aunit**3-4.d0*pi/3.d0*vp_j%rmt(nsub)**3
!!      write(6,*) ' DEBUG RM1=',rm1,                                     &
!!     &      pi*(rws**4-vp_j%rmt(nsub)**4)
!      write(6,*) ' DEBUG DU RM1=',                                      &
!     &      (rm1 + pi*vp_j%rmt(nsub)**4) / (pi*rws**4)
!!      write(6,*) ' DEBUG RM1i=',rm1i,                                   &
!!     &      2*pi*(rws**2-vp_j%rmt(nsub)**2)
!      write(6,*) ' DEBUG DU RM1i=',                                     &
!     &      (rm1i+2*pi*vp_j%rmt(nsub)**2)/(2*pi*rws**2)
!DEBUGPRINT

!DEBUGPRINT
       write(6,*) ' nsub=',nsub,' Nnb=',nnb,' lmaxm=',lmaxm
!DEBUGPRINT

      do i=1,nnb
       R0 = xyznb(1:3,i)
!       vp_j%ylmMns(nsub)%r0nn(i) = sqrt(dot_product(R0,R0))
       call getVPylm(R0,lmaxm,iflag,xyz,                                &
     &               reshape(vp_j%vj(:,:,:,1:nfc,nsub),[np]),           &
     &               vp_ylm)     ! vp_ylm is weighted Jacobian * real(Ylm) / |r+R|**(l+1)
       ymom2du = aunit**(3-1)
       vp_ylm(1:lmx,1:np) = vp_ylm(1:lmx,1:np) / ymom2du   ! in D.U.
       subYl(0:,1:,i) = 0
       do k=0,nmom
        do l=0,lmaxm
         ymom2du = aunit**(-l)    ! normalization is for VP-volume=1
         do m=-l,l
          lm = 1+l*(l+1)/2+abs(m)
          subYl(k,l+1,i) = subYl(k,l+1,i) + sum(vp_ylm(lm,1:np))
         end do
         subYl(k,l+1,i) = subYl(k,l+1,i) / ymom2du    ! in D.U.
         if ( abs(subYl(k,l+1,i)) < epsShC/10 ) subYl(k,l+1,i)=0
!DEBUGPRINT
!         if ( l==0 .and. k==0 ) then
!          write(6,*) ' R0=',sngl(R0)
!         end if
!         if ( l==0 ) then
!          rm1i = sqrt(4*pi)*subYl(k,1,i)*aunit**(k+2)
!          if ( i==1 ) rm1i = rm1i                                       &
!     &             + 4*pi*vp_j%rmt(nsub)**(k+2)/(k+2)
!          rm1i = rm1i / (4*pi*rws**(k+2)/(k+2))
!          write(6,*) ' subYl  RM1i=',rm1i 
!         else
!          if ( subYl(k,l+1,i) .ne. 0 ) then
!           rm1i = subYl(k,l+1,i)*aunit**(k+2)
!           rm1i = rm1i / (4*pi*rws**(k+2)/(k+2))
!           write(6,'(a,i2,a,g14.5)') ' subYl-RM(l=',l,')i=',rm1i 
!          end if
!         end if
!DEBUGPRINT
        end do
        if ( k==0.and.maxval(abs(subYl(0,1:lmaxm+1,i)))<epsShC/10 ) exit
        if ( k<nmom) then
         forall (lm=1:lmx) vp_ylm(lm,1:np) = vp_ylm(lm,1:np)*rl(1:np)
        end if
       end do
      end do
      deallocate(vp_ylm)
      deallocate(rl,xyz)
      return

!EOC
      contains

      subroutine getVPylm(R0,lmax,iflag,rxyz,vj,vpYlm)
      implicit none
      real(8), intent(in) :: R0(3)
      integer, intent(in) :: lmax,iflag
      real(8), intent(in) :: rxyz(:,:)
      real(8), intent(in) :: vj(:)
      real(8), intent(out) :: vpYlm(:,:)

      complex(8), allocatable :: z_vpYlm(:,:)
      real(8), allocatable :: r2(:)
      integer :: m,n,nlm,l,lm,lm1
      
      n = size(rxyz,2)
      allocate(r2(n))
      allocate(z_vpYlm(1:n,1:(lmax+1)**2))
      call hrmplnm(R0(1),R0(2),R0(3),                                   &
     &           rxyz(1:3,1:n),3,r2,z_vpYlm(:,:),n,                     &
     &                      -10, n, lmax , -1)
      do l=0,lmax
       do m=0,l
        lm1 = l*(l+1)/2+1 + m
        lm = l*(l+1)+1 + m
        vpYlm(lm1,1:n) = dreal(z_vpYlm(1:n,lm)) 
       end do
      end do
      deallocate(z_vpYlm)
      nlm = (lmax+1)*(lmax+2)/2
      forall (lm=1:nlm) vpYlm(lm,1:n) = vpYlm(lm,1:n)*vj(1:n) 
      if ( iflag < 0 ) then  ! |R0+rxyz|**(-l-1)*Ylm
       if ( maxval(abs(R0)) > epsilon(dble(0)) ) then
!DEBUG        r2 = 1/sqrt(r2)
           r2 = (1/sqrt(r2) - 1/sqrt(dot_product(R0,R0)))
       else
        do l=1,n  
         if ( r2(l) == dble(0) ) then
           r2(l) = huge(dble(0)) 
!DEBUGPRINT           
           write(6,*) ' DEBUG r(i)=0, i=',l
           if ( abs(vj(l)) > epsilon(dble(0)) ) then
            write(6,*) ' i=',i,' r(i) = 0, vj=',vj(l)
           end if
!DEBUGPRINT           
         else
          r2(l) = 1/sqrt(r2(l))
         end if
        end do
       end if
       forall (lm=1:nlm) vpYlm(lm,1:n) = vpYlm(lm,1:n)*r2(1:n) 
       do l=1,lmax
        lm = l*(l+1)/2+1
        forall (lm1=lm:nlm) vpYlm(lm1,1:n) = vpYlm(lm1,1:n)*r2(1:n) 
       end do
      else                   ! |R0+rxyz|**l*Ylm
       r2 = sqrt(r2)
       do l=1,lmax
        lm = l*(l+1)/2+1
        forall (lm1=lm:nlm) vpYlm(lm1,1:n) = vpYlm(lm1,1:n)*r2(1:n) 
       end do
      end if
      deallocate(r2)
      return
      end subroutine getVPylm

      end subroutine calcCoulombShC1
