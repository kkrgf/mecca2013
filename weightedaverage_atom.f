      real(8) function  weightedaverage_atom(M_S)
      use atom, only : get_etot
      use mecca_types, only : RunState
      use gfncts_interface, only : g_atcon
      implicit none
!      integer, intent(in) :: iZ(:)
!      real(8), intent(out) :: answ
      type(RunState) :: M_S
      real(8) :: answ
      integer, allocatable :: iZ(:)
      real(8), allocatable :: values(:)
      real(8), allocatable :: wght(:)
      real(8), allocatable :: atcon(:,:)
      integer :: i,k,nsub,n_z

      answ = 0
      n_z = M_S%intfile%nelements
      allocate(iZ(n_z),values(n_z),wght(n_z))
      do i=1,n_z
       iZ(i) =  M_S%intfile%elements(i)%ztot
      end do
      call get_etot(iZ,values)
      if ( abs(sum(values))>epsilon(0.d0)/dble(10) ) then
       if ( .not.allocated(atcon) ) allocate(atcon(1,1))
       call g_atcon(M_S%intfile,atcon)
       wght = 0
       do i=1,size(iZ)
        do nsub=1,size(atcon,2)
         do k=1,M_S%intfile%sublat(nsub)%ncomp
          if ( M_S%intfile%sublat(nsub)%compon(k)%zID==iZ(i) ) then
           wght(i) = wght(i) + atcon(k,nsub)*M_S%intfile%sublat(nsub)%ns
          end if
         end do
        end do
       end do
       answ = dot_product(wght,values)/sum(wght)
      end if

      weightedaverage_atom = answ

      deallocate(iZ,values,wght)
      return
      end function weightedaverage_atom
