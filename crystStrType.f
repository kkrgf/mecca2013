!BOP
!!ROUTINE: crystStrType
!!INTERFACE:
      subroutine crystStrType(icryst, typcr)
!!DESCRIPTION:
! provides type of structure, {\tt typcr},
! input {\tt icryst} is structure integer id
!
!!REVISION HISTORY:
! Initial version - A.S. - 2013
!!REMARKS:
! default name "xyz" is used for icryst<1 or icryst>11 
!EOP
!
!BOC
      implicit none
      integer icryst
      character*(*) typcr

      select case (icryst)
      case (1)
       typcr = 'fcc'
      case (2)
       typcr = 'bcc'
      case (3)
       typcr = 'sc'
      case (4)
       typcr = 'b2'        ! CsCl
      case (5)
       typcr = 'l12'       ! Cu3Au
      case (6)
       typcr = 'p1-1-3'    ! BaTiO3, perovskite
      case (7)
       typcr = 'b1'        ! NaCl
      case (8)
       typcr = 'do3'       ! Fe3Al
      case (9)
       typcr = 'r3m'       ! trigonal
      case (10)
       typcr = 'do22'      ! TiAl3
      case (11)
       typcr = 'a4'        ! Si4
      case default
       typcr = 'xyz'
      end select

      return
!EOC
      end subroutine crystStrType
