!BOP
!!ROUTINE: matrix
!!INTERFACE:
      subroutine matrix(y,x,i1,i2,nrmat,kkrsz,expdot)
!!DESCRIPTION:
! {\tt x}["i1","i2"] = {\tt expdot} * {\tt y} 
!

!!DO_NOT_PRINT
      implicit real*8 (a-h,o-z)
!!DO_NOT_PRINT

!!ARGUMENTS:
      integer nrmat
      complex*16   x(nrmat,*)
      integer kkrsz
      complex*16   y(kkrsz,kkrsz)
      integer i1,i2
      complex*16   expdot
!EOP
!
!BOC
      iist=(i2-1)*kkrsz
      jjst=(i1-1)*kkrsz
         do i=1,kkrsz
            do j=1,kkrsz
               x(jjst+j,iist+i)=y(j,i)*expdot
            enddo
         enddo
      return
!EOC
      end subroutine matrix
!
!BOP
!!ROUTINE: matrix1
!!INTERFACE:
      subroutine matrix1(y,x,i1,i2,nrmat,kkrsz,expdot)
!!DESCRIPTION:
! calculates cmplx({\tt expdot}) * cmplx({\tt y}) and pput it in matrix {\tt x}:\\ 
!    x(1:nrmat,1:nrmat) -- Re( cmplx({\tt expdot}) * cmplx({\tt y}) )   \\ 
!    x(1:nrmat,nrmat+1:nrmat*2) -- Im( cmplx({\tt expdot}) * cmplx({\tt y}) )  
!

!!DO_NOT_PRINT
      implicit real*8 (a-h,o-z)
!!DO_NOT_PRINT

!!ARGUMENTS:
      integer nrmat
      real*8       x(nrmat,*)
      integer kkrsz
      real*8       y(0:1,kkrsz,kkrsz)
      integer i1,i2
      real*8       expdot(0:1)
!EOP
!
!BOC
      iist=(i2-1)*kkrsz
      jjst=(i1-1)*kkrsz
      iistim=iist+nrmat
         do i=1,kkrsz
            do j=1,kkrsz

!CAB               x(jjst+j,iist+i)=y(j,i)*expdot
!c       real part      -- x(jjst+j,iist+i)
!c       imaginary part -- x(jjst+j,iistim+i)

               yre = y(0,j,i)
               yim = y(1,j,i)
               expre = expdot(0)
               expim = expdot(1)
               x(jjst+j,iist+i) = yre*expre-yim*expim
               x(jjst+j,iistim+i) = yre*expim+yim*expre
            enddo
         enddo
      return
!EOC
      end subroutine matrix1
