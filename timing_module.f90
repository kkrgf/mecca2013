module timing

  implicit none
  real,    parameter :: aborttime  = 1.d10 ! don't stop

  integer, parameter :: numtimers  = 30
  real :: timers(2,numtimers) = 0.0, totaltime = 0.0
  character(len=200) :: timerdesc(numtimers) = ""
  integer :: opentimers(numtimers) = -1
  integer :: nopentimers = 0

  contains

!BOP
!!IROUTINE: begintiming
!!INTERFACE:
  subroutine begintiming(message)
!!DESCRIPTION:
! starts one of the module timers
!
!!ARGUMENTS:
    character(len=*), intent(in) :: message
!EOP
!
!BOC
    real(8) :: xx
    integer :: n

    do n = 1, numtimers
      if( Trim(timerdesc(n)) == Trim(message) ) exit
      if( Trim(timerdesc(n)) == "" ) then
        timerdesc(n) = message; exit
      end if
    end do

    if( n > numtimers ) then
      write(0,*) "Insufficient timers for debugging."
      stop
    end if

    nopentimers = nopentimers + 1
    opentimers(nopentimers) = n

    call cpu_time(xx)
    timers(2,opentimers(nopentimers)) = xx

!EOC
  end subroutine begintiming

!BOP
!!IROUTINE: endtiming
!!INTERFACE:
  subroutine endtiming() 
!!DESCRIPTION:
! stops opened module timer
!
!EOP
!
!BOC
    real(8) :: xx
    integer :: OpenTimer

    OpenTimer = opentimers(nopentimers)
    call cpu_time(xx)
    timers(1,OpenTimer) = timers(1,OpenTimer) + xx-timers(2,OpenTimer)
    if(nopentimers == 1) totaltime = totaltime + xx-timers(2,OpenTimer)
    nopentimers = nopentimers - 1 
!EOC
  end subroutine endtiming

!BOP
!!IROUTINE: dumptimings
!!INTERFACE:
  subroutine dumptimings()
!!REMARKS:
! fortran unit 7 is used for writing
!EOP
!
!BOC
    integer :: i
     
    open(unit=7,file='timing.out',status='replace',action='write',iostat=i)
    write(7,'(a)') 'Timings'
    write(7,*) ''
    do i = 1, numtimers
    if( trim(timerdesc(i)) /= "" ) then

      write(7,'(A,F10.4,A,F5.1,A,A)') &
        "time:", timers(1,i), ", perc:",100.0*timers(1,i)/totaltime, &
        ", desc: ", trim(timerdesc(i))

    end if; end do
    close(unit=7)

!EOC
  end subroutine dumptimings

!BOP
!!IROUTINE: checkToAbort
!!INTERFACE:
  subroutine checkToAbort()
!!DESCRIPTION:
! aborts if run time is more than a time limit 
!
!!REMARKS:
! time limit is a predefined module private parameter
!EOP
    if( totaltime > aborttime ) then
!
!BOC
      call dumptimings()
      stop
    end if
!EOC
  end subroutine checkToAbort

end module timing
