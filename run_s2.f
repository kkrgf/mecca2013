!BOP
!!ROUTINE: mecca
!!INTERFACE:
      program S2run
!!DESCRIPTION:
! main program to perform S2 calculations
!

!!USES:
      use mecca_constants
      use mecca_types
      use mecca_interface, only : buildDate
      use mecca_scf_interface
      use input
      use mpi
!
!!ARGUMENTS:
!  it is a program, which gets input information from command line
!
!!REVISION HISTORY:
! Initial Version - A.S. - 2016
!EOP
!
!BOC
      implicit none
      integer :: narg,istop
      character(2048) :: buffer
      character(:), allocatable :: meccainp
      character(:), allocatable :: s2inp
      character(3) :: ver

      integer, external :: idToZ

      ! MPI related variables
      integer myrank, nproc, ierr
      logical mecca_mpi

      ! initialize MPI
      call mpi_init(ierr)
      call mpi_comm_size(mpi_comm_world, nproc, ierr)
      call mpi_comm_rank(mpi_comm_world, myrank, ierr)
      if( myrank > 0 ) then
        open(unit=6,file='/dev/null')
        ! write(logfile,'(a,i2,a)') 'rank', myrank, '.log'
        ! open(unit=6, file = trim(logfile) )
      endif
      call mpi_initialized(mecca_mpi,ierr)

      istop = 0
      narg = iargc()
      if ( narg == 2 ) then
       call getarg(1,buffer)
       if ( scan(buffer,'-') .eq. 1 ) then
        write(6,*)                                                      &
     &           ' ERROR: Incorrect command-line arguments list;'
        call getarg(0,buffer)
        if ( myrank == 0 ) call printHelp(buffer)
        istop = 2
       else
        if ( mecca_mpi ) then
        ! inform user about number of processes
         write(6,*) 'MPI initialized with ', nproc, ' processes.'
        end if
        meccainp = trim(buffer)
        call getarg(2,buffer)
        s2inp = trim(buffer)
!......................................................................
        call run_s2mecca(meccainp,s2inp)
!......................................................................
       end if
      else if (narg==1 ) then
       call getarg(1,buffer)
       if ( scan(buffer,'-') .eq. 1 ) then
        istop = 1
        read(buffer,*) ver
        if ( trim(ver)=='-v' ) then
          write(6,'(1x,a)') version//buildDate()
          if ( mecca_mpi ) then
           write(6,*) 'MPI initialized with ', nproc, ' processes.'
          end if
          istop = 0
          go to 10
        end if
       end if
       call getarg(0,buffer)
       if ( myrank == 0 ) call printHelp(buffer)
      else
       if (narg > 0) write(*,*)                                         &
     &           ' ERROR: Incorrect command-line arguments list;'
       call getarg(0,buffer)
       if ( myrank == 0 ) call printHelp(buffer)
       istop = narg
      end if

10    continue
!DELETE      call mpi_initialized(mecca_mpi,ierr)
!DELETE      write(0,*) myrank,' MPI DEBUG initial ierr=',ierr
!DELETE      if ( mecca_mpi ) then
!DELETE       call mpi_barrier(mpi_comm_world,ierr)
!DELETE      write(0,*) myrank,' MPI DEBUG barrier ierr=',ierr
!DELETE       call mpi_finalize(ierr)
!DELETE      end if

      call mpi_finalize(ierr)

      select case (istop)
      case (1)
        stop 1
      case (2)
        stop 2
      case (3:)
        stop 99
      case default
        stop
      end select
!EOC
      CONTAINS
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!BOP
!!IROUTINE: printHelp
!!INTERFACE:
      subroutine printHelp(exename)
!!ARGUMENTS:
      character*(*) exename
!!REVISION HISTORY:
! Initial Version - A.S. - 2016
!EOP
!
!BOC
      write(6,*)
      write(6,*) ' Proper usage:'
      write(6,*)
      write(6,*) trim(exename),' mecca_input_file S2_input_file'
      write(6,*)
      return
!EOC
      end subroutine printHelp

      end program S2run
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

!BOP
!!ROUTINE: run_s2mecca
!!INTERFACE:
       subroutine run_s2mecca (meccainp,s2inp)
!!DESCRIPTION:
! this subroutine runs {\sc S2} calculation
!
!!USES:
       use mecca_types
       use  input
       use mecca_scf_interface
       use struct
       use taucalc, only : runTauCalc

!!DO_NOT_PRINT
       implicit none
!!DO_NOT_PRINT

!!ARGUMENTS:
       character*(*) meccainp,s2inp
!!REVISION HISTORY:
! Initial Version - A.S. - 2016
!EOP
!
!BOC

      interface
       subroutine defineTauSet( mecca_state, s2inp )
       use mecca_constants
       use mecca_types
       implicit none
       type(RunState), intent(inout), target :: mecca_state
       character(*), intent(in) :: s2inp
       end subroutine defineTauSet
      end interface

       type(RunState), target :: mecca_state
       type(IniFile), target :: internalFile
       type(RS_Str),     target, save :: rs0box
       type(KS_Str),     target, save :: ks0box
       type(EW_Str),     target, save :: ew0box
!      type(GF_output),  target, save :: gf_out0box
       character(*), parameter :: sname='run_s2mecca'
       character(120) :: outinfo
       integer :: ierr

       mecca_state%intfile => internalFile
       mecca_state%rs_box => rs0box
       mecca_state%ks_box => ks0box
       mecca_state%ew_box => ew0box
!       mecca_state%gf_out_box => gf_out0box

       call read2intFile(meccainp,mecca_state%intfile,ierr)
       internalFile%ntask = 1    ! to calculate tau
       internalFile%nscf = 1
       internalFile%igrid = 1
       internalFile%npts = 0    ! 99 -> (etop,eitop), 100 -> from file EGRD.INPUT
       if ( ierr == 0 ) then
         call mecca_scf(mecca_state,outinfo)

         if ( mecca_state%fail ) write(6,*) sname//                     &
     &   ' :: ERROR, unable to complete SCF-setup'

         call defineTauSet( mecca_state, s2inp )

         call runTauCalc(mecca_state)

         if ( mecca_state%fail ) write(6,*) sname//                     &
     &   ' :: ERROR, unable to complete Tau-related calculations'
!
!  memory deallocation
!
         internalFile%ntask = 0
         call deallocSphDeps(mecca_state%intfile)
!?          call deallocRunState(mecca_state)
!
!         call s2calculator(s2inp,mecca_state%allTau)
!
!
       else
         if ( ierr == -1 ) then
         write(*,*)                                                     &
     &   ' ERROR :: no access to file <'//trim(meccainp)//'>'
         call p_fstop(sname)
         end if
       end if

       call deallocEWstr(ew0box)
       nullify(mecca_state%ew_box)
       call deallocKSstr(ks0box)
       nullify(mecca_state%ks_box)
       call deallocRSstr(rs0box)
       nullify(mecca_state%rs_box)

       call fstop('return')
       return
!EOC
       end subroutine run_s2mecca


      subroutine defineTauSet( mecca_state, s2inp )
      use mecca_constants
      use mecca_types
!DELETE      use interf_void, only : encd_tau,type_tau_set
      implicit none
      type(RunState), intent(inout), target :: mecca_state
      character(*), intent(in) :: s2inp
      type(Tau_set), pointer :: allTau,pTau(:)
      type(IniFile), pointer :: inFile
      type (KS_Str), pointer :: ks_box
!DELETE      type(GF_output), pointer :: gf_out

      integer :: i,is,nsub,nspin,nume,nbasis
      logical :: fileexist
      integer, allocatable :: nK(:)
      complex(8) :: ene

      if ( mecca_state%intfile%nscf .ne. 1 ) return

      if ( associated(mecca_state%intfile) ) then
       inFile => mecca_state%intfile
      else
       call fstop(' NO INTFILE ')
      end if
      if ( associated(mecca_state%ks_box) ) then
       ks_box => mecca_state%ks_box
      else
       call fstop(' NO KS_BOX ')
      end if
!DELETE      if ( associated(mecca_state%gf_out_box) ) then
!DELETE       gf_out => mecca_state%gf_out_box
!DELETE       nume = gf_out%nume
!DELETE      else
!DELETE       call fstop(' NO GF_OUT ')
!DELETE      end if

!DELETE      nK   = 0
!DELETE      nK(1:nume) = 1

      nume = 0
      write(6,*) ' S2INP file=',trim(s2inp)
      inquire(file=trim(s2inp),EXIST=fileexist)

      open(11,file=trim(s2inp),status='OLD')

      if ( fileexist ) then
       do
        read(11,*,err=10,end=10) ene
        nume = nume+1
       end do
      end if
10    continue

      if ( nume==0 ) then
       call fstop(' UNDEFINED S2-INPUT ')
      else
       rewind(11)
       allocate(nK(1:nume))
       nK(1:nume) = 2                   ! it is just an example; DO_NOT_USE!
      end if

      allocate(pTau(1))
      allTau => pTau(1)

      nspin = 1
      if ( inFile%nspin>1 ) nspin = 2

      allocate(allTau%set(1:nume,1:nspin))
      allTau%nset = nume

      allocate(allTau%set(1,1)%kpoints(1:3,1:nK(1)))

!DEBUGPRINT
      write(6,*) ' DEFINETAU: nume=',nume,' nspin=',nspin
      write(6,*) ' ***: size(allTau%set)=',size(allTau%set,1),          &
     &              size(allTau%set,2)
      write(6,*) ' ENERGY:'
!DEBUGPRINT

      do i=1,nume
       read(11,*) ene
       allTau%set(i,1:nspin)%energy = ene
!DEBUGPRINT
       write(6,'(i3,2(1x,2es17.8))') i,allTau%set(i,1:nspin)%energy
!DEBUGPRINT
      end do
      close(11)

!DELETE      do is=1,nspin
!DELETE!       allTau%set(1,is)%energy = gf_out%egrd(gf_out%nume)
!DELETE       allTau%set(i,is)%energy = gf_out%egrd(i)
!DELETE      end do

      nK = min(nK,size(ks_box%qmesh,2))
      allTau%set(1,1)%kpoints(1:3,1:nK(1)) = ks_box%qmesh(1:3,1:nK(1),1)
      if ( nspin>1 ) then
       allTau%set(1,nspin)%kpoints => allTau%set(1,1)%kpoints
      end if

      nbasis = inFile%nsubl
      allocate(allTau%set(1,1)%ncomp(nbasis))
      do nsub=1,nbasis
       allTau%set(1,1)%ncomp(nsub) = inFile%sublat(nsub)%ncomp
      end do
      if ( nspin>1 ) then
       allTau%set(1,nspin)%ncomp => allTau%set(1,1)%ncomp
      end if

      do is=1,nspin
       do i=2,nume
        allTau%set(i,is)%kpoints => allTau%set(1,is)%kpoints
        allTau%set(i,is)%ncomp => allTau%set(1,is)%ncomp
       end do
      end do

!!!      allocate(allTau%set(1)%kpoints(3,ks_box%nqpt(1)))
!!!      allTau%set(1)%kpoints(:,:) = ks_box%qmesh(1:3,1:ks_box%nqpt(1),1)

!?      do i=2,nume
!?        allTau%set(i)%kpoints => allTau%set(1)%kpoints
!?      end do

!      call printTauset(allTau,99)

      mecca_state%allTau => allTau

      return

      contains

      subroutine printTauset(pTau,io)
      implicit none
      type(Tau_set), intent(in) :: pTau
      integer, intent(in) :: io

      integer :: i,ndim,nume

      write(io,'(1x,i5,a)') pTau%nset,' # NSET'
      if ( pTau%nset<=0 ) return
      if ( .not.associated(pTau%set) ) then
        write(io,*) ' Tau-set is not allocated'
       return
      else
       nume = pTau%nset
       ndim = size(pTau%set,1)
       nspin= size(pTau%set,2)
       write(io,*) ' size(Tau-set): ',ndim,nspin
      end if

      write(io,'(1x,i5,a)') size(pTau%set(1,1)%kpoints,2),' # No.K-PNTS'
      do i=1,size(pTau%set(1,1)%kpoints,2)
       write(io,'(1x,i5,3e14.5)') i,pTau%set(1,1)%kpoints(1:3,i)
      end do

      do i=1,nume
       write(io,'(1x,2e15.6,a)') pTau%set(i,1)%energy,' # ENERGY'
       if ( associated(pTau%set(i,1)%tab) ) then
       end if
       if ( associated(pTau%set(i,1)%tcpa) ) then
       end if
       if ( associated(pTau%set(i,1)%tau_k) ) then
       end if
      end do

      return
      end subroutine printTauset

      end subroutine defineTauSet

