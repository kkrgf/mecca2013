!BOP
!!ROUTINE: eos1fit
!!INTERFACE:
      program eos1fit
!!DESCRIPTION:
! computes parameters of equation-of-states fitting ("MURNAGHAN","VINET") \\
! \\
! input (from console): volume of unit cell (dimensionless) and  
! not more than 100 lines of equation-of-state points calculated by {\sc mecca},
! 6 values per line:  alat,Etot,volume,pressure,any-number,temperature
!
!!USES:
      USE eos_data, only : EOS, eosid
      USE ISO_FORTRAN_ENV, only : INPUT_UNIT
!
!!REVISION HISTORY:
! Initial version - A.S. - 2013
!EOP
!
!BOC
      implicit none
      integer ndim
      parameter (ndim=100)
      real*8 a(ndim),Etot(ndim),volume(ndim),P(ndim)
      real*8 T,cellV
      integer i, Neos
      character*10 line10

       real*8 RyUnit
       parameter ( RyUnit = 13.60569301d0 )
       real*8 Angstroem
       parameter (Angstroem =  0.529177211d0 )
!       character*9 eosid(2) /'MURNAGHAN','VINET'/

       real*8 Vol0,E0,B0,B0Prim,a0,grueneis_par,err

       Neos = 0
       read(INPUT_UNIT,*) cellV
       write(*,*) ' cellV=',cellV
       do i=1,ndim
        read(INPUT_UNIT,*,end=10,err=10)                                &
     &          a(i), Etot(i),volume(i),P(i),line10,T
        write(*,'(1x,es14.7,1x,es21.13,2(1x,es15.7),2x,a,1x,es12.5)')   &
     &                       a(i),Etot(i),volume(i),P(i),line10,T
        volume(i) = cellV*a(i)**3
        Neos = i
       end do
10     continue
       close(INPUT_UNIT)

       if ( Neos .gt. 2) then
        do i=1,3
         if ( Neos==3 .and. i.ne.1 ) cycle
         if ( Neos==4 .and. i==3 ) cycle
         call EOS(i,Angstroem,RyUnit,Neos,volume,Etot                   &
     &,                    Vol0,E0,B0,B0Prim,err)
         a0 = (Vol0/cellV)**(1.d0/3.d0)
         if ( i==1 ) then
           write(6,'(2x,''a0= '',f6.3,'' Bohr'',2x,                     &
     &               ''E0= '',f13.5,'' Ry'', 2x,                        &
     &               ''B0= '',g13.6,'' Mbar'',2x,                       &
     &               ''B0''''= '','' ---'',11x,                         &
     &               ''V0= '',f10.5,1x,a                                &
     &           )')                                                    &
     &     a0,E0,B0,Vol0,':'//trim(eosid(i))
         else if ( i==2 ) then
           write(6,'(2x,''a0= '',f6.3,'' Bohr'',2x,                     &
     &               ''E0= '',f13.5,'' Ry'', 2x,                        &
     &               ''B0= '',g13.6,'' Mbar'',2x,                       &
     &               ''B0''''= '',g13.6,2x,                             &
     &               ''V0= '',f10.5,1x,a                                &
     &           )')                                                    &
     &     a0,E0,B0,B0Prim,Vol0,':'//trim(eosid(i))
         else if ( i==3 ) then
          if  ( Neos>5 ) then
           grueneis_par = B0Prim/dble(2) - dble(5)/dble(6)
           write(6,'(2x,''a0= '',f6.3,'' Bohr'',2x,                     &
     &               ''E0= '',f13.5,'' Ry'', 2x,                        &
     &               ''B0= '',g13.6,'' Mbar'',2x,                       &
     &               ''B0''''= '',g13.6,2x,                             &
     &               ''V0= '',f10.5,1x,a                                &
     &           ,2x,''grune= '',g13.6                                  &
     &           )')                                                    &
     &     a0,E0,B0,B0Prim,Vol0,':'//trim(eosid(i)),grueneis_par
          else
           write(6,'(2x,''a0= '',f6.3,'' Bohr'',2x,                     &
     &               ''E0= '',f13.5,'' Ry'', 2x,                        &
     &               ''B0= '',g13.6,'' Mbar'',2x,                       &
     &               ''B0''''= '',g13.6,2x,                             &
     &               ''V0= '',f10.5,1x,a                                &
     &           )')                                                    &
     &     a0,E0,B0,B0Prim,Vol0,':'//trim(eosid(i))
!            if ( err>1.d-4 ) write(6,'(6x,'' Warning: B0prim''         &
!     & ,'' may be inaccurate, fitting mse = '',e9.2,'' mRy'')') err*1.d3
          end if
         end if
        end do
       end if

       write(6,'(/)')
       stop
       end program
