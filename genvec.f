!BOP
!!ROUTINE: genvec
!!INTERFACE:
      subroutine genvec(bas,Rvsn,point,npoint,                          &
     &                  vsnlat,ndimv,nvsn,iprint) 
!!DESCRIPTION:
! generates vectors {\tt vsnlat} for all lattice sites within
! specific distance {\tt Rvsn} from {\tt points}; 
! basis vectors and sites are defined by {\tt bas} and 
! {\tt points}, respectively
!

!!DO_NOT_PRINT
      implicit none
!!DO_NOT_PRINT

!!ARGUMENTS:
      integer, intent(in) :: npoint
      real(8), intent(in) :: bas(3,3),Rvsn,point(3,*)
      integer :: ndimv
      real(8), allocatable :: vsnlat(:)  ! vsnlat(ndimv,3)
      integer, intent(out) :: nvsn
      integer, intent(in) :: iprint
      integer, allocatable ::  iorder(:)
      real(8), allocatable ::  tmpvsn(:)
!!REVISION HISTORY:
! Adapted - A.S. - 2013
!EOP
!
!      integer, optional :: memsize
!BOC
!c
      real*8  rsn(3),r2vsn,rsq
      real(8), allocatable :: tmpvec(:,:)
      integer, allocatable :: itmpvec(:)

      real*8  bmr1,bmr2,bmr3
      integer nmax1,nmax2,nmax3,ndimv1
      real(8) :: rtmp(3)

      integer n1,n2,n3,i,ip
!      integer ic
!      character*10 :: sname = 'genvec'

!      if ( present(memsize) ) memsize = 0
!c     generate  vectors...............................
      nvsn = 0
      if ( Rvsn<0 ) return
      r2vsn = Rvsn**2

      bmr1 = sqrt(bas(1,1)**2+bas(2,1)**2+bas(3,1)**2)
      bmr2 = sqrt(bas(1,2)**2+bas(2,2)**2+bas(3,2)**2)
      bmr3 = sqrt(bas(1,3)**2+bas(2,3)**2+bas(3,3)**2)

      nmax1 = max(2,nint(Rvsn/bmr1)+1)
      nmax2 = max(2,nint(Rvsn/bmr2)+1)
      nmax3 = max(2,nint(Rvsn/bmr3)+1)

      if ( allocated(vsnlat) ) deallocate(vsnlat)
      ndimv = 10*npoint+100  
      allocate(vsnlat(1:3*ndimv)) ; vsnlat = 0
      allocate(iorder(ndimv),tmpvsn(ndimv))

      do n1=-nmax1,nmax1
         do n2=-nmax2,nmax2
            do n3=-nmax3,nmax3
              rsn(1:3) = n1*bas(1:3,1)+n2*bas(1:3,2)+n3*bas(1:3,3)
              rsq = dot_product(rsn,rsn)
              do ip=1,npoint
                rtmp(1:3) = rsn(1:3)+point(1:3,ip)
                if(dot_product(rtmp,rtmp).le.r2vsn) then
!                 if ( present(memsize) ) then
!                     memsize = memsize+1
!                 else
                  if(nvsn+1.gt.ndimv) then
                     allocate(tmpvec(ndimv,3))
                     tmpvec(1:ndimv,1) = vsnlat(1:ndimv)
                     tmpvec(1:ndimv,2) = vsnlat(ndimv+1:ndimv+ndimv)
                     tmpvec(1:ndimv,3) = vsnlat(2*ndimv+1:2*ndimv+ndimv)
                     deallocate(vsnlat)
                     ndimv1 = ndimv + max(ndimv/5,10*npoint)
                     allocate(vsnlat(3*ndimv1))
                     vsnlat(1:ndimv) = tmpvec(1:ndimv,1) 
                     vsnlat(ndimv+1:ndimv1) = 0
                     vsnlat(ndimv1+1:ndimv1+ndimv) = tmpvec(1:ndimv,2) 
                     vsnlat(ndimv1+ndimv+1:2*ndimv1) = 0
                     vsnlat(2*ndimv1+1:2*ndimv1+ndimv) =                &
     &                                                 tmpvec(1:ndimv,3)
                     vsnlat(2*ndimv1+ndimv+1:3*ndimv1) = 0
                     tmpvec(1:ndimv,1) = tmpvsn(1:ndimv)
                     deallocate(tmpvsn)
                     allocate(tmpvsn(ndimv1))
                     tmpvsn(1:ndimv) = tmpvec(1:ndimv,1)
                     deallocate(tmpvec)
                     allocate(itmpvec(ndimv))
                     itmpvec = iorder
                     deallocate(iorder)
                     allocate(iorder(ndimv1))
                     iorder(1:ndimv) = itmpvec(1:ndimv)
                     deallocate(itmpvec)
                     ndimv = ndimv1
                  endif
!c                 --------------------------------------------------
                  call ord3v(vsnlat,tmpvsn,nvsn,rsn,rsq,iorder)
                  exit
!c                 --------------------------------------------------
!                 end if
                endif
              enddo
            enddo
         enddo
      enddo

!c     ===============================================================
!      if ( present(memsize) ) then
!        if(iprint.ge.2)  write(6,'('' vsnlat size ='',i11)') memsize
!      else    
        if(iprint.ge.7)  write(6,'('' ns,vsnlat '',i5,4f10.5)')         &
     &    (i,vsnlat(i:3*ndimv:ndimv),tmpvsn(i),i=1,nvsn)
!     &    (i,(vsnlat(i,ip),ip=1,3),tmpvsn(i),i=1,nvsn)
!      end if
!DEBUG      write(6,'(''DEBUG: genvec input npoint,ndimv,Rvsn'',2i5,f10.5)')  &
!DEBUG     &                                               npoint,ndimv,Rvsn 
!DEBUG      write(6,'(''debug bas: '',i3,3f10.5)')                            &
!DEBUG     &                                      (i,(bas(i,ip),ip=1,3),i=1,3)
!DEBUG      write(6,'(''debug pnt: '',i3,3f10.5)')                            &
!DEBUG     &                            (i,(point(ip,i),ip=1,3),i=1,npoint)
!DEBUG      write(6,'(''DEBUG: genvec output nvsn,r2vsn'',i5,f10.5,i5)')      &
!DEBUG     &               nvsn,r2vsn,ndimv
!DEBUG        write(6,'(''debug vsnlat,tmpvsn: '',i5,3f10.5,2x,f10.5,i5)')    &
!DEBUG     &    (i,vsnlat(i:3*ndimv:ndimv),tmpvsn(i),iorder(i),i=1,nvsn)

      deallocate(iorder,tmpvsn)
!c
!c     ===============================================================
      return

!EOC
      contains

!BOP
!!IROUTINE: ord3v
!!INTERFACE:
      subroutine ord3v(v3out,vsqout,nv3,v3in,vsqin,iorder)
!!DESCRIPTION:
! adds vector {\tt v3in} in ordered array {\tt v3out}
! (ordering is by means of vsqout/vsqin)

!!DO_NOT_PRINT
      implicit none
!!DO_NOT_PRINT

!!ARGUMENTS:
      real*8  v3out(*)     ! (:,:)   ! [1:ndimv,1:3]
      real*8  vsqout(:)    ! [1:ndimv]
      integer :: nv3
      real(8), intent(in) ::  v3in(3)
      real(8), intent(in) ::  vsqin
      integer iorder(:)    ! [1:ndimv]
!!REMARKS:
! private procedure of subroutine genvec
!EOP
!
!BOC
      integer :: nv,nvm,n

      n = ndimv*3
      if(nv3.eq.0) then
         vsqout(1)=vsqin
         iorder(1)=1
         v3out(1:n:ndimv) = v3in(1:3)
!         v3out(1,1:3) = v3in(1:3)
         nv3=1
      else
         do nv=1,nv3,+1
            if(vsqout(nv).ge.vsqin) then
               do nvm=nv3,nv,-1
                  vsqout(nvm+1)=vsqout(nvm)
                  iorder(nvm+1)=iorder(nvm)
                  v3out(nvm+1:n:ndimv) = v3out(nvm:n:ndimv)
!                  v3out(nvm+1,1:3) = v3out(nvm,1:3)
               end do
               vsqout(nv)=vsqin
               v3out(nv:n:ndimv) = v3in(1:3)
!               v3out(nv,1:3) = v3in(1:3)
               nv3=nv3+1
               iorder(nv)=nv3
               return
            endif
         end do
         vsqout(nv3+1)=vsqin
         v3out(nv3+1:n:ndimv) = v3in(1:3)
!         v3out(nv3+1,1:3) = v3in(1:3)
         nv3=nv3+1
         iorder(nv3)=nv3
      endif
!c
      return
!c
!EOC
      end subroutine ord3v

      end subroutine genvec
