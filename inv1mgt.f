!c     cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
!BOP
!!ROUTINE: inv1mgt
!!INTERFACE:
      subroutine inv1mgt(amt,ndmat,kkrsz,nbasis,tcpa,ndtcpa,            &
     &                  idiag,                                          &
     &                  itype,                                          &
     &                  invswitch,invparam,                             &
     &                  iprint,istop)
!!DESCRIPTION:
! inverts matrix [1-amt*tcpa],\\
!        amt  -- matrix, order KKRSZ*NBASIS \\
!        tcpa -- diagonal of matrix, order KKRSZ
!
!!USES:
      use mtrx_interface

!!DO_NOT_PRINT
      implicit real*8 (a-h,o-z)
!!DO_NOT_PRINT

!!ARGUMENTS:
      integer    kkrsz,nbasis,ndmat,iprint
      complex*16 amt(ndmat,kkrsz*nbasis)
      complex*16 tcpa(*)
      integer    itype(nbasis)
      integer    ndtcpa,idiag,invswitch,invparam
      character  istop*10
!!REVISION HISTORY:
! Adapted - A.S. - 2013
!EOP
!
!BOC
!
!c parameter
      character  sname*10
      parameter (sname='inv1mgt')
      complex*16 czero
      parameter (czero=(0.d0,0.d0))
      complex*16 cone
      parameter (cone=(1.d0,0.d0))
!
!c     **************************************************************
!c     input tau**(-1) and calculates tau............................
!c     full matrix storage version...........bg & gms [Nov 1991].....
!c     **************************************************************
!c

!c     nrmat = kkrsz*nbasis

!C
      call g21mgt(amt,tcpa,ndtcpa,itype,                                &
     &                   nbasis,kkrsz,idiag)
      call invmatr(amt,                                                 &
     &             kkrsz,nbasis,                                        &
     &             invswitch,invparam,                                  &
     &             iprint,istop)

      if( istop .eq. sname ) then
        call fstop(sname)
      endif
!c
      return
!EOC
      end subroutine inv1mgt
