!BOP
!!ROUTINE: clstrKS
!!INTERFACE:
      subroutine clstrKS(kkrsz,natom,                                   &
     &                 aij,itype,                                       &
     &                 mapstr,ndimbas,                                  &
     &                 kx,ky,kz,                                        &
     &                 numnbi,jclust,                                   &
     &                 mapsnni,ndimnn,                                  &
     &                 nnptr,maxclstr,nnclmn,                           &
     &                 rsij,                                            &
     &                 gclstr,ndclstr,gclstKS,                          &
     &                 istop)
!!DESCRIPTION:
! computes Fourier transformation of real-space screened Greens function
! (or diagonal part of Tau, tau00 in RS cluster approximation)
!
!!REVISION HISTORY:
! Adapted - A.S. - 2013
!EOP
!
!BOC
      implicit none
!c
      integer kkrsz,natom,ndimbas
!c
      real*8      aij(3,*)
      real*8      kx
      real*8      ky
      real*8      kz
!c
      integer      ndclstr
      complex*16   gclstr(kkrsz*kkrsz*ndclstr,*)
      integer      maxclstr
      complex*16   gclstKS(kkrsz*kkrsz*maxclstr,*)


      integer      itype(natom)
      integer      mapstr(ndimbas,natom)

      integer numnbi(*),ndimnn,jclust(ndimnn,*)
!CALAM      integer ntypecl(ndimnn,*)
      integer mapsnni(ndimnn*ndimnn,*)
      real*8  rsij(ndimnn*ndimnn*3,*)

!c      integer   iprint
      character istop*10
!c     ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
      character    sname*10
      parameter    (sname='clstrKS')
!c     ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
      integer iatom,iatom0,jatom,jatom0,isub,ia0
      integer ndmat
!c      integer indgKSi,indgKSj

      integer ndKS

      integer inn,inn0
      integer nnptr(0:1,natom),nnclmn(2,maxclstr,natom)

        ndmat = kkrsz
        ndKS = 0
        do iatom = 1,natom
         iatom0 = nnptr(1,iatom)
         isub = itype(iatom0)
!c         indgKS = (iatom-1)*(ndmat*ndmat)*maxclstr
!c        indgKS = ndKS
         call zerooutC(gclstKS(1,iatom),kkrsz*kkrsz*maxclstr)
         do inn=1,nnptr(0,iatom)
           inn0 = nnclmn(2,inn,iatom)
           jatom = nnclmn(1,inn,iatom)
           jatom0 = jclust(inn0,isub)    ! origin of "jatom"
           ia0 = mapstr(iatom,jatom)
           call ksmatr1(1,inn,jatom0,                                   &
     &                 kx,ky,kz,aij(1,ia0),gclstr(1,isub),              &
     &                 numnbi(isub),kkrsz,rsij(1,isub),                 &
     &                 mapsnni(1,isub),ndimnn,                          &
     &                 jclust(1,isub),                                  &
     &                 gclstKS(1,iatom),ndmat,                          &
     &                 istop)
         end do
         ndKS = ndKS+(ndmat*ndmat)*maxclstr


        end do

      if (istop.eq.sname) call fstop(sname)
!c

      return
!EOC
      contains

!BOP
!!IROUTINE: ksmatr1
!!INTERFACE:
      subroutine ksmatr1(iatom0,jatom0,jcell,                           &
     &    kx,ky,kz,aij,greens,                                          &
     &    numnbi,kkrsz,rsij,                                            &
     &    mapsnni,ndimnn,                                               &
     &    jclust,                                                       &
     &    aksp,ndmat,                                                   &
     &    istop)
!!DESCRIPTION:
! calculates block of k-space matrix {\tt aksp}, which is
! $ Fourier[greens] $ (I=iatom0,J=jatom0); $ {\bf k}=(kx,ky,kz) $

! {\bv
! A_IJ(k) = exp(-i*k*aij)*sum(exp(i*k*rij)*A(I|rij));
!             rij = rsij for J-type NN of atom I only;
!
! output: aksp_IJ(1:ndmat,*), is defined only for neighbours of atom I
! \ev}
!

!c
!!USES:
      use universal_const

!!DO_NOT_PRINT
      implicit none
!!DO_NOT_PRINT

!!ARGUMENTS:
      integer    iatom0,jatom0,jcell
      real*8     kx,ky,kz,aij(3)
      integer    kkrsz,numnbi
      complex*16 greens(kkrsz,kkrsz,numnbi)
      real*8     rsij(3,*)
      integer    ndimnn
      integer    mapsnni(ndimnn,numnbi)
      integer    jclust(numnbi)
      character  istop*10
!
!! k-space matrix (output);
!
      integer ndmat
      complex*16 aksp(ndmat,*)
!!REMARKS:
! private procedure of subroutine clstrKS;
! to fill matrix aksp the routine should be used for each NN pair (?)
!EOP
!
!BOC
!CALAM      integer invswitch
      character    sname*10
      parameter    (sname='ksmatr1')

!      real*8 zero
!      parameter (zero=0.d0)

      real*8  rx,ry,rz
!c      real*8  aijx,aijy,aijz
!c      integer ia,i1,i2,i0,j0,iaij,jtp,nnj,i,j,k0,l1,l2,m
!c      integer n0,ndksp
!c      integer kkrsq2,kkrszi0,kkrszj0,kkrszk0,iaik
      integer ij,i,j
!      integer, parameter :: lclkkr=ipkkr
      complex*16 expdot0,expdot,tautmp(kkrsz,kkrsz)

      integer k2sub


!      if(kkrsz.gt.lclkkr) call fstop(sname//':  please increase LCLKKR')

      expdot0 = exp(-dcmplx(zero,kx*aij(1)+ky*aij(2)+kz*aij(3)))
!CAB                ??? sign

      tautmp = 0

      do k2sub=1,numnbi
       if(jcell.eq.jclust(k2sub)) then
        ij = mapsnni(1,k2sub)
        rx = rsij(1,ij)
        ry = rsij(2,ij)
        rz = rsij(3,ij)
        expdot = expdot0*exp(+dcmplx(zero,kx*rx+ky*ry+kz*rz))
!CAB                          ??? sign

!c
!c  tautmp = tautmp+greens[1,k2sub]*expdot

        do i=1,kkrsz
         do j=1,kkrsz
          tautmp(j,i)=tautmp(j,i)+greens(j,i,k2sub)*expdot
         enddo
        enddo

       end if
      end do

!c
!c  aksp = tautmp
!c
      call cpblk2(tautmp,size(tautmp,1),1,1,                            &
     &            aksp,ndmat,iatom0,jatom0,kkrsz)

!c
!c  AKSP -- Fourier[A]
!c
!c  --------------------------------------------------------------------
!c
      if (istop.eq.sname) call fstop(sname//' DEBUG')
!c
      return
!EOC
      end subroutine ksmatr1
 
      end subroutine clstrKS
