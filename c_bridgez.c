/*
 * -- SuperLU routine (version 1.1) --
 * Univ. of California Berkeley, Xerox Palo Alto Research Center,
 * and Lawrence Berkeley National Lab.
 * November 15, 1997
 *
 */
#include <stdlib.h>
#include <stdio.h>

#include "SuperLU/SRC/zsp_defs.h"
#include "SuperLU/SRC/util.h"
#include "SuperLU/SRC/Cnames.h"

#ifdef SCORE
c_bridgez_(int *n, int *nnz, int *nrhs, doublecomplex *values, int *rowptr,
	   int *colind, doublecomplex *b, int *ldb, int *info,
	   int *work, int *lwork, int *iflag)
#endif

#ifndef SCORE
c_bridgez(int *n, int *nnz, int *nrhs, doublecomplex *values, int *rowptr,
	  int *colind, doublecomplex *b, int *ldb, int *info,
	  int *work, int *lwork, int *iflag)
#endif


{

    static SuperMatrix  L, U;
    static SuperMatrix A, B;
    static SCformat *Lstore;
    static NCformat *Ustore;
    static int *perm_r; /* row permutations from partial pivoting */
    static int *perm_c; /* column permutation vector */
    int      panel_size, permc_spec, i;
    char     trans[1];
    SuperLUStat_t *stat;
    mem_usage_t  mem_usage;

    /* Adjust to 0-based indexing */
/*    for (i = 0; i < *nnz; ++i) --colind[i];
 *  for (i = 0; i <= *n; ++i) --rowptr[i];
 */

/*
 *  dCreate_CompRow_Matrix(&A, *n, *n, *nnz, values,
 *                          colind, rowptr, NR, _D, GE);
 *  dCreate_Dense_Matrix(&B, *n, *nrhs, b, *ldb, DN, _D, GE);
 */

/*
 *  if ( !(perm_r = intMalloc(*n)) ) ABORT("Malloc fails for perm_r[].");
 *  if ( !(perm_c = intMalloc(*n)) ) ABORT("Malloc fails for perm_c[].");
 */

    /*    printf("point=1\n"); */
    /*
     * Get column permutation vector perm_c[], according to permc_spec:
     *   permc_spec = 0: use the natural ordering
     *   permc_spec = 1: use minimum degree ordering on structure of A'*A
     *   permc_spec = 2: use minimum degree ordering on structure of A'+A
     */

    if(*iflag == 1) {

/*      printf("iflag=1\n");   */

/*  Adjust to 0-based indexing */

    for (i = 0; i < *nnz; ++i) --colind[i];
    for (i = 0; i <= *n; ++i) --rowptr[i];


    if ( !(perm_r = intMalloc(*n)) ) ABORT("Malloc fails for perm_r[].");
    if ( !(perm_c = intMalloc(*n)) ) ABORT("Malloc fails for perm_c[].");


    zCreate_CompRow_Matrix(&A, *n, *n, *nnz, values,
			    colind, rowptr, SLU_NR, SLU_Z, SLU_GE);

    zCreate_Dense_Matrix(&B, *n, *nrhs, b, *ldb, SLU_DN, SLU_Z, SLU_GE);


/*    permc_spec = 1;   */
/*    permc_spec = 2;   */
    permc_spec = 0;
    get_perm_c(permc_spec, &A, perm_c);

    panel_size = sp_ienv(1);

    *info = 1 ;

    zgssv1(&A, perm_c, perm_r, &L, &U, &B, info, work, *lwork);

/*
 *      *info  =  0;
 *      *trans = 'T';
 *
 *      dgstrs (trans, &L, &U, perm_r, perm_c, &B, info);
 */

    if ( *info == 0 ) {

	Lstore = (SCformat *) L.Store;
	Ustore = (NCformat *) U.Store;
    	printf("No of nonzeros in factor L = %d\n", Lstore->nnz);
    	printf("No of nonzeros in factor U = %d\n", Ustore->nnz);
    	printf("No of nonzeros in L+U = %d\n", Lstore->nnz + Ustore->nnz);

	zQuerySpace(&L, &U, &mem_usage);
	printf("LU:: need memory expansions\n");
//	printf("LU:: MB %.3f\ttotal MB needed %.3f\texpansions %d\n",
//	       mem_usage.for_lu/1e6, mem_usage.total_needed/1e6,
//	       mem_usage.expansions);

    } else {
	printf("zgssv1() error returns INFO= %d\n", *info);
	if ( info <= n ) { /* factorization completes */
	    zQuerySpace(&L, &U, &mem_usage);
	    printf("LU:: need memory expansions\n");
//	    printf("LU: MB %.3f\ttotal MB needed %.3f\texpansions %d\n",
//		   mem_usage.for_lu/1e6, mem_usage.total_needed/1e6,
//		   mem_usage.expansions);
	}
    }
    Destroy_SuperMatrix_Store(&B);
    Destroy_SuperMatrix_Store(&A);

    /* Restore to 1-based indexing */

    for (i = 0; i < *nnz; ++i) ++colind[i];
    for (i = 0; i <= *n; ++i) ++rowptr[i];

/*      printf("iflag=1\n"); */

/*  *iflag == 1 */
    }

    if(*iflag == 2) {

/*      printf("iflag=2\n"); */

/*    dCreate_CompRow_Matrix(&A, *n, *n, *nnz, values,
 *                          colind, rowptr, NR, _D, GE);
 */

    zCreate_Dense_Matrix(&B, *n, *nrhs, b, *ldb, SLU_DN, SLU_Z, SLU_GE);

/*      printf("iflag=2\n"); */

        *info  =  0;
        *trans = 'T';

	zgstrs (trans, &L, &U, perm_r, perm_c, &B, &stat, info);

/*      printf("iflag=2\n"); */

    Destroy_SuperMatrix_Store(&B);

/*      printf("iflag=2\n"); */

/*  *iflag == 2 */
    }

    if(*iflag == 3) {

    SUPERLU_FREE (perm_r);
    SUPERLU_FREE (perm_c);

/*    Destroy_SuperMatrix_Store(&A);  */

/*    Destroy_SuperMatrix_Store(&B);  */

    Destroy_SuperNode_Matrix(&L);
    Destroy_CompCol_Matrix(&U);

/*  *iflag == 3  */
    }

    /* Restore to 1-based indexing */
/*    for (i = 0; i < *nnz; ++i) ++colind[i];
 *    for (i = 0; i <= *n; ++i) ++rowptr[i];
 */

}

