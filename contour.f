!BOP
!!ROUTINE: conTempr
!!INTERFACE:
      subroutine conTempr(Tempr,ebot,etop,egrd,dele1,                   &
     &                 npts1,npts2,                                     &
     &                 iprint,rctngl_only,istop)
!!DESCRIPTION:
! {\bv
!  constructs energy contour for Temperature integration
!       (typical picture for LOW temperatures)
!        ! Im(e)             .   .
!        !               .           .       x
!        !            .                 .
!        !          .                     ._______ eTempr=pi/2*Tempr
!        !        .
!        !       .
!       -!-----------------------------------------------
!        !     ebot        Real(e)           etop
!
!    Tempr     : temperature
!  ebot      : bottom of the contour on the real axis.............
!  etop      :
!   if etop>ebot
!     etop is top of the circular contour on the real axis,
!     npts1     : number of points with Re(En)<etop (circular contur)
!     npts2     : number of points with Re(En)>etop (linear contour)
!   if etop<ebot  (e.g. if Tempr is high)
!     npts1     : to determine number of points with ebot<Re(En)<0 (rectangular contour)
!     npts2     : number of points with Re(En)>0 (linear contour)
!  iprint    : print level control (>0 for print of energies)..
!
!  egrd[1:npts1+npts2]   : En           (output)
!  dele1[1:npts1+npts2]  : weight       (output)
! \ev}

!!DO_NOT_PRINT
      implicit   none
!!DO_NOT_PRINT

!!ARGUMENTS:
      real(8), intent(in) :: Tempr,ebot,etop
      complex(8), intent(out) :: egrd(*)
      complex(8), intent(out) :: dele1(*)
      integer,  intent(inout) :: npts1,npts2
      integer, intent(in) :: iprint
      logical, intent(in) :: rctngl_only
      character(10), intent(in) :: istop
!!REVISION HISTORY:
! Initial Version - A.S. -2001
! Adapted - A.S. - 2013
!EOP
!
!BOC
      logical    no_circ_cont
      integer    i,ie,jj,nume,nde
      integer    np0,np1,np2
      real(8) :: eband,ewR,ewT,step,alat,c_du
      real(8) :: pitempr,erad,alpha,beta,e1
      real(8) :: eTempr,eT1,eT2,etmp,sf
      complex(8) :: e0,ztmp
      real(8), allocatable :: enrg(:),wghts(:)
!DELETE      real(8), allocatable :: egrd2(:),dele2(:)
      integer :: np1a,np1b
      real(8) :: p1b,stminY
      real(8) :: dl0,dl1,w0,w1
      real(8), external :: gLattConst
      complex(8), external :: TemprFun
!
!c parameters
      character  sname*10
      parameter  (sname='conTempr')
      real*8     zero,half,one
      parameter  (zero=0.d0,half=0.5d0,one=1.d0)
!c      complex*16 sqrtm1
!c      parameter  (sqrtm1=(0.d0,1.d0))
      real(8), parameter :: pi=4*atan(one)
      real(8), parameter :: eup_max=2.d0     ! 0.55d0
!      real(8), parameter :: eup_max=1.d0     ! 0.55d0
      real(8), parameter :: deX = 0.04d0     ! "step per E-point", X-dir
      real(8), parameter :: deY = 0.04d0     ! "step per E-point", Y-dir
!      real(8), parameter :: deY = 0.03d0     ! "step per E-point", Y-dir
      real(8), parameter :: w = 2.d0        ! "width" coeff. for Tempr-distribution
      real(8), parameter :: zt    = -log(0.2d0), eps = 1.d-12
      real(8), parameter :: very_small_temp = 1.d-6 ! ~0.2K; no temperature calculations below very_small_temp
!c
!c  ==================================================================

      alat = gLattConst(1)
      if ( alat ==0 ) then
       c_du = one
      else
       c_du = (alat/(2*pi))**2
      end if
      nde = npts1+npts2
!
      no_circ_cont = .false.
      nume = 0
      if(Tempr >= very_small_temp) then
       pitempr = max(zero,pi*Tempr)         !  first Matsubara pole
       eTempr = pitempr/2
       eT1    = etop - zt*Tempr
       eT2    = etop + zt*Tempr
       etmp = c_du*eTempr
       if ( etmp > 3.d0*eup_max ) then
        eTempr = min(eTempr,eup_max/c_du)
       end if
       if ( rctngl_only ) then
        no_circ_cont = .true.
       else if ( etop < ebot ) then
        no_circ_cont = .true.
       else
        if ( etop-ebot >= eTempr*2 ) then
         no_circ_cont = .true.
!DEBUG
        else
         no_circ_cont = .true.
!DEBUG
        end if
       end if

       IF ( no_circ_cont ) THEN
!DELETE?        if ( eT1<ebot .and. eT2>ebot ) then
!DELETE?         eT2 = eT2 + (ebot-eT1)
!DELETE?         eT1 = ebot
!DELETE?        end if
!DELETE?        if ( eT2<ebot ) then
!DELETE?         eT2 = ebot
!DELETE?        end if
        stminY = deY/c_du
!        stminX = deX/c_du
!        eT1 = max(eT1,ebot)
!        eT2 = max(eT2,ebot)

        w0 = one/deY
        w1 = one/deX

        dl0 = max(eTempr,stminY)
        dl1 = eT2-ebot
        if ( eT1 < ebot ) then
         dl1 = eT2-eT1
         eT1 = ebot
        end if

!      "leg" between (ebot,0) and (ebot,eTempr)

!DELETE        np0 = max(2,nint(eTempr/(2*(deY/c_du))))
!DELETE?        if ( eT2>zero ) then
!DELETE?         dl1 = (eT2-ebot)
!DELETE         np1 = max(2,nint((eT2-ebot)/(2*(deX/c_du))))
!DELETE         if (np0+np1<npts1) then
!DELETE          np0 = 1+int(eTempr/(eTempr+(eT2-ebot))*npts1)
!DELETE         else if (np0+np1>npts1) then
!DELETE          np0 = max(np0,int(eTempr/(eTempr+(eT2-ebot))*npts1))
!DELETE         end if
!DELETE?        else
!DELETE?         dl1 = (zero-ebot)
!DELETE         np1 = max(2,nint((zero-ebot)/(2*(deX/c_du))))
!DELETE         if (np0+np1<npts1) then
!DELETE          np0 = 1+int(eTempr/(eTempr+(zero-ebot))*npts1)
!DELETE         else if (np0+np1>npts1) then
!DELETE          np0 = max(np0,int(eTempr/(eTempr+(zero-ebot))*npts1))
!DELETE         end if
!DELETE?        end if

        step = ( dl0*w0 + dl1*w1 )/(w0+w1) / npts1
        np0 = max(1,nint(dl0/step))
        step = eTempr/np0

        e0 = dcmplx(ebot,half*step)
        do ie=1,np0
         dele1(nume+ie) = dcmplx(zero,step)
         egrd(nume+ie) = e0 + (ie-1)*dcmplx(zero,step)
        end do
        nume = nume+np0

!      "leg" between (eT2,eTempr) and (Inf,eTempr)
        if ( eT2<=ebot .and. (npts1 - np0)>0 ) then
         npts2 = npts2 + (npts1 - np0)
         npts1 = np0
         eT2 = ebot
        end if

        np2 = npts2
        allocate(enrg(np2),wghts(np2))
        call conlagr1(Tempr,eT2,etop,np2,enrg,wghts)
        egrd(npts1+1:npts1+np2)  = dcmplx(enrg(1:np2),eTempr)
        dele1(npts1+1:npts1+np2) = dcmplx(wghts(1:np2),zero)
        deallocate(enrg,wghts)

        do jj=npts1+np2,npts1+1,-1
         ie = jj
         e0 = (egrd(ie)-etop)/Tempr
         ztmp = TemprFun(e0,0)
         ztmp = dele1(ie)*ztmp
         if ( abs(ztmp)>eps ) then
          exit
         end if
        end do

!!!         jj = ie-npts1;  dele1(npst1+np2-jj+1:npst1+np2)
!!!                         dele1(npst1+1:npst1+jj)   i+np2-jj
!!!         dele1(npst1+np2-jj)+i))  dele1(npts1+i)

        jj = ie-npts1
        do i=npts1+jj,npts1+1,-1
         dele1((np2-jj)+i) = dele1(i)
         egrd((np2-jj)+i)  = egrd(i)
        end do
        np2 = jj
        npts1 = npts1+npts2-np2

!DELETE        if ( eT2 <= ebot ) then
!DELETE         step = eTempr/npts1
!DELETE         e0 = dcmplx(ebot,half*step)
!DELETE         nume = nume-np0
!DELETE         do ie=1,npts1
!DELETE          dele1(nume+ie) = dcmplx(zero,step)
!DELETE          egrd(nume+ie) = e0 + (ie-1)*dcmplx(zero,step)
!DELETE         end do
!DELETE         nume = nume+np0
!DELETE        end if
        npts2 = np2

        if ( eT2>ebot ) then
         beta = zero
         alpha = zero
         np1 = npts1-np0

!DELETE         if ( eT1>ebot ) then
!DELETE!      "leg" between (ebot,eTempr) and (eT1,eTempr)
!DELETE          p1b = (eT2-eT1)/(eT2-ebot)
!DELETE
!DELETE          np1b = min(np1,nint(p1b*np1) + 2)
!DELETE          np1a = np1-np1b
!DELETE          if ( np1a<1 ) then
!DELETE           np1a = 0 ; np1b = np1
!DELETE           eT1 = ebot
!DELETE          else
!DELETE           eT2 = eT2 + (ebot-eT1)
!DELETE           eT1 = ebot
!DELETE          end if
!DELETE
!DELETE          if ( eT1>ebot ) then
!DELETE           step = (eT1-ebot)/np1a
!DELETE           erad = (eT1-ebot)/2
!DELETE           e0 = dcmplx(ebot+erad,eTempr)
!DELETE           call conlegndr(e0,erad,alpha,beta,np1a,                       &
!DELETE     &                                       egrd(np0+1),dele1(np0+1))
!DELETE           nume = nume + np1a
!DELETE          end if
!DELETE
!DELETE!      "leg" between (eT1,eTempr) and (eT2,eTempr)
!DELETE          step = (eT2-eT1)/np1b
!DELETE          erad = (eT2-eT1)/2
!DELETE          e0 = dcmplx(eT1+erad,eTempr)
!DELETE          call conlegndr(e0,erad,alpha,beta,np1b,                       &
!DELETE     &                                       egrd(nume+1),dele1(nume+1))
!DELETE          nume = nume + np1b
!DELETE
!DELETE         else
!      "leg" between (ebot,eTempr) and (eT2,eTempr), eT1<ebot
          step = (eT2-ebot)/np1
          erad = (eT2-ebot)/2
          e0 = dcmplx(ebot+erad,eTempr)
          call conlegndr(e0,erad,alpha,beta,np1,                        &
     &                                       egrd(nume+1),dele1(nume+1))
          nume = nume + np1
!DELETE         end if
!DELETE        else
!DELETE!      "leg" between (ebot,eTempr) and (zero,eTempr), eT2<ebot
!DELETE         step = (zero-ebot)/np1
!DELETE         erad = (zero-ebot)/2
!DELETE         e0 = dcmplx(ebot+erad,eTempr)
!DELETE         call conlegndr(e0,erad,alpha,beta,np1,                         &
!DELETE     &                                       egrd(nume+1),dele1(nume+1))
!DELETE         nume = nume + np1
        else
         np1 = 0
         npts1 = np0
        end if
        nume = nume + npts2
        if ( nume>nde ) call fstop('memory error1 in '//sname)
       ELSE
!
       if ( w*Tempr<(deX/c_du) ) then
        ewR = (deX/c_du)
        np1 = min(2,(npts2+1)/2)
        np2 = npts2-np1
        ewT = ewR + (deX/c_du)
        np0 = npts1-np1
        np1 = np1+np1
        eband = etop-(deX/c_du)-ebot
       else
        eband = etop-ebot
        ewR = w*Tempr
        np2 = npts2/2
        np1 = npts2-np2
!        if ( w*Tempr >= 0*(eband-(de/c_du)) ) then
!DEBUG        if ( w*Tempr >= eband-(de/c_du) ) then
        if ( ewR >= eband-max(eTempr,(deX/c_du))                        &
     &        .or. ( eTempr*abs(ebot)/sqrt(eband**2+eTempr**2)          &
     &              < 2*max(deX,deY)/c_du )                             & ! too close to e=0
!     &       .or. ( eband/2 > 2*eTempr )
     &     ) then
          no_circ_cont = .true.
          ewT = ewR + eband
          np0 = nint(eTempr/(2*(deY/c_du)))
          if ( np0<2 ) np0=np0+1
          np1 = np1+npts1-np0
          eband = zero
        else
          np0 = min(npts1,1+int((one-w*Tempr/(etop-ebot))*npts1))
          eband = etop-w*Tempr-ebot
          ewT = ewR + w*Tempr
          np1 = np1+(npts1-np0)
        end if
       end if
       if(eband < zero) then
        write(6,*) ' ebot=',ebot
        write(6,*) ' etop=',etop
        write(6,*) ' w*Tempr=',w*Tempr
        write(6,*) ' eband=',eband
        call p_fstop(sname//': semicircular radius < 0???')
       end if
!np0
       e0 = dcmplx(ebot,zero)
       nume = 0
       if ( np0 > 0 ) then
        if ( no_circ_cont ) then
         step = eTempr/np0
         e0 = dcmplx(ebot,half*step)
         do ie=1,np0
          dele1(nume+ie) = dcmplx(zero,step)
          egrd(nume+ie) = e0 + (ie-1)*dcmplx(zero,step)
         end do
         e0 = dcmplx(ebot,eTempr)
        else
         erad = half*sqrt(eband**2+eTempr**2)
         beta =  atan(eTempr/eband)                ! angle between circle diameter and Re-axis
!         if ( eTempr < eup_max ) then
         if ( beta > pi/6.d0 ) then
           alpha = beta
         else
           alpha = beta+pi                         ! "left" angle
         end if
         e0 = dcmplx(ebot+half*eband,half*eTempr)  ! center of the circle
         call conlegndr(e0,erad,alpha,beta,np0,                         &
     &                                       egrd(nume+1),dele1(nume+1))
         e0 = dcmplx(ebot+eband,eTempr)
        end if
        nume = nume + np0
       end if
!np1
       if ( np1 > 0 ) then
        erad = half*ewT
        beta = zero
        alpha = zero
        e0 = e0 + erad
        call conlegndr(e0,erad,alpha,beta,np1,                          &
     &                                       egrd(nume+1),dele1(nume+1))
        nume = nume + np1
        e0 = e0 + erad
       end if
!np2
        if ( np2 > 0 ) then
!        e0 = dcmplx(etop+ewR,eTempr)
!DELETE        do i=np2,2*np2
!DELETE         call conlagr(Tempr,e0,i,egrd(nume+1),dele1(nume+1))
!DELETE         do ie=nume+1,nume+i
!DELETE           if ( dreal(egrd(ie))>eTplus ) then
!DELETE             exit
!DELETE           end if
!DELETE         end do
!DELETE         if ( ie-nume-1 >= np2 ) exit
!DELETE        end do
!DELETE        nume = nume + np2

         allocate(enrg(np2),wghts(np2))
         etmp = dreal(e0)
         call conlagr1(Tempr,etmp,etop,np2,enrg,wghts)
         egrd(nume+1:nume+np2)  = dcmplx(enrg(1:np2),eTempr)
         dele1(nume+1:nume+np2) = dcmplx(wghts(1:np2),zero)
         deallocate(enrg,wghts)
         do ie=nume+np2,nume+1,-1
          e0 = (egrd(ie)-etop)/Tempr
          ztmp = TemprFun(e0,0)
          ztmp = dele1(ie)*ztmp
          if ( abs(ztmp)>epsilon(one) ) then
           exit
          end if
         end do
         do i=ie,nume+1,-1
          dele1(i+(np2-ie)) = dele1(i)
          egrd(i+(np2-ie)) = egrd(i)
         end do
         np2 = ie-nume
         nume = nume+npts2-np2
         npts2 = np2
!
         nume = nume + np2
        end if
!
       END IF
!c
!c  takes into account Fermi function
!c
       do ie=1,nume
        e0 = (egrd(ie)-etop)/Tempr
        ztmp = TemprFun(e0,0)
        dele1(ie) = dele1(ie) * ztmp
       end do

!DELETE       do ie=nume,npts1+1,-1
!DELETE        if ( abs(dele1(ie))>epsilon(one) ) then
!DELETE         exit
!DELETE        end if
!DELETE       end do
!DELETE       npts2 = ie-npts1
!DELETE       nume = npts1 + npts2
!
      else                              ! T = 0
       eband = etop-ebot
       if(eband <= zero) then
        write(6,*) ' ebot=',ebot
        write(6,*) ' etop=',etop
        call p_fstop(sname//': etop.LE.ebot???')
       end if
       eTempr = zero
       if ( npts2>0 ) then
!!TODO
        call fstop('contour: RECTANGLE CONTOUR IS NOT IMPLEMENTED')
         no_circ_cont = .true.
         etmp = eup_max/c_du
!         epar = zero
!         npar = 0
!         step = 0  ! ???
!         call conbox(ebot,etop,etmp,etmp,epar,egrd,npts2,npar,      &
!     &                  step,npts,iprint,istop)
!        npts1 = ???
!        beta =  zero                              ! angle between circle diameter
!        alpha = zero                              ! "left" angle
!        e0 = dcmplx(etop-half*eband,etmp)         ! center of the segment
!        call conlegndr(e0,erad,zero,zero,npts1,egrd(?),dele1(?))
!!TODO
       else
        npts2 = 0
        no_circ_cont = .false.
        erad = half*eband
        beta =  zero                              ! angle between circle diameter
        alpha = pi                                ! "left" angle
        e0 = dcmplx(etop-half*eband,zero)         ! center of the circle
        call conlegndr(e0,erad,alpha,beta,npts1,egrd(1),dele1(1))
        nume = npts1
        np0 = npts1
        np1 = 0
        np2 = 0
       end if
      end if

!c
!c  ===================================================================
!c  write out grid and return.......................................
!c
      if(iprint.gt.-10) then
       if ( no_circ_cont ) then
       write(6,'(/''    Vert.contour at'',t40,''='',                    &
     &                                         f12.5)') ebot
       write(6,'(''     Top of vert-contour'',t40,''='',                &
     &                                         f12.5)') eTempr
       write(6,'(''     Number of energies: vert-part'',t40,            &
     &          ''='',i4)') np0
       else
       write(6,'(/''     Bottom of circ-contour'',t40,''='',            &
     &                                         f10.5)') ebot
       write(6,'(''     Top of circ-contour'',t40,''='',                &
     &                                    f10.5)') ebot+eband
       write(6,'(''     Number of energies: circ-part'',t40,            &
     &          ''='',i4)') np0
       end if
       if ( np0 < 0 ) then
        write(6,*) ' np0 < 0 ?'
        call fstop(sname//': unexpected error')
       end if
       write(6,'(''     Min. Imag(Egrd) :'',t40,''='',                  &
     &                       f14.5)') minval(aimag(egrd(1:npts1)))
       if ( np1 > 0 ) then
        write(6,'(''     Lin.contour at '',t40,''='',                   &
     &                                 f14.5)') aimag(egrd(np0+1))
!DELETE        if ( np2>0 ) then
!DELETE         etmp = dreal(e0)
!DELETE        else
!DELETE         etmp = dreal(egrd(np0+np1))
!DELETE        end if
        etmp = dreal(egrd(npts1))
        write(6,'(''     Top of Legendre lin-contour'',t40,''='',       &
     &                              f14.5)') etmp
        write(6,'(''     and number of energies: lin-part'',t40,        &
     &          ''='',i4)') np1
       end if
       if(np2 > 0) then
        etmp = dreal(egrd(npts1+npts2))
        write(6,'(''     Max of Legendre lin-contour'',t40,''='',       &
     &                              f14.5)') etmp
        write(6,'(''                : Laguerr lin-part'',t40,           &
     &                                                 ''='',i4)') np2
       end if

       if(istop.eq.sname) then
        write(6,'('' n='',i5,'' e='',2d13.4,'' de='',2d13.4)')          &
     &   (ie,egrd(ie),dele1(ie),ie=1,nume)
       endif
       write(6,'(/)')
      end if
!c
!c  ===================================================================
!c
      npts2 = nume - npts1
      if(istop.eq.sname) then
         call fstop(sname)
      else
         return
      endif
!EOC
      end subroutine conTempr
!
!BOP
!!ROUTINE: conboxTempr
!!INTERFACE:
      subroutine conboxTempr(Tempr,npts,ebot,etop,ef,                   &
     &                                          egrd,dele1,nume,iprint)
!!DESCRIPTION:
! {\bv
!  constructs rectangular energy contour for Temperature integration
!
!  Tempr     : temperature
!  npts      : number of points on a leg parallel to X axis
!  ebot      : bottom of the contour on the real axis.............
!  etop      : top
!  ef        : Fermi level
!  egrd[1:nume]   : En           (output)
!  dele1[1:nume]  : weight       (output)
!  nume      : total number of points (output)
!  iprint    : print level control (>0 for print of energies)..
! \ev}


!!DO_NOT_PRINT
      implicit   none
!!DO_NOT_PRINT

!!ARGUMENTS:
      real(8), intent(in) :: Tempr,ebot,etop,ef
      integer, intent(in) :: npts
      complex(8), intent(out) :: egrd(*)
      complex(8), intent(out) :: dele1(*)
      integer,  intent(out) :: nume
      integer, intent(in) :: iprint
!!REVISION HISTORY:
! Initial Version - A.S. - 2013
!EOP
!
!BOC
!c      logical    no_circ_cont
      integer    ie
      integer    np0,np1                                       !,np2
      real(8) :: step,alat,c_du
      real(8) :: pitempr,erad,alpha,beta
      real(8) :: eTempr
      complex(8) :: e0,ztmp
      real(8), external :: gLattConst
      complex(8), external :: TemprFun
!c parameters
!c      complex(8), parameter :: sqrtm1=(0.d0,1.d0)
      real(8), parameter :: zero=0.d0,half=0.5d0,one=1.d0
      real(8), parameter :: pi=4*atan(one)
      real(8), parameter :: deY = 0.04d0     ! "step per E-point", Y-dir
      real(8), parameter :: eup_max=5*deY
!c      real(8), parameter :: w = 2.d0        ! "width" coeff. for Tempr-distribution
!c      real(8), parameter :: ztmpm = -log(1.d-9)  ! cutoff "threshold"
      real(8), parameter :: very_small_temp = 1.d-6 ! ~0.2K; no temperature calculations below very_small_temp
!c
!c  ==================================================================

      alat = gLattConst(0)
      if ( alat ==0 ) then
       c_du = one
      else
       c_du = (alat/(2*pi))**2
      end if
!
      nume = 0

      if ( Tempr>=very_small_temp ) then
       pitempr = pi*Tempr                !  first Matsubara pole
       eTempr = min(pitempr/2,eup_max/c_du)
      else
       pitempr = zero
       eTempr = abs(Tempr)
      end if

!      "leg" between (ebot,0) and (ebot,eTempr) and "leg" between (etop,eTempr) and (etop,zero)
        np0 = max(1,nint(eTempr/(2*(deY/c_du))))
!      "leg" between (ebot,eTempr) and (etop,eTempr)
        np1 = npts

        step = eTempr/np0
        e0 = dcmplx(ebot,half*step)
        do ie=1,np0
         dele1(nume+ie) = dcmplx(zero,step)
         egrd(nume+ie) = e0 + (ie-1)*dcmplx(zero,step)
        end do
        nume = nume+np0

        erad = (etop-ebot)/2
        beta = zero
        alpha = zero
        e0 = dcmplx(ebot+erad,eTempr)
        call conlegndr(e0,erad,alpha,beta,np1,                          &
     &                                       egrd(nume+1),dele1(nume+1))
        nume = nume + np1

        step = eTempr/np0
        e0 = dcmplx(etop,eTempr-half*step)
        do ie=1,np0
         dele1(nume+ie) = dcmplx(zero,-step)
         egrd(nume+ie) = e0 - (ie-1)*dcmplx(zero,step)
        end do
        nume = nume+np0
!
!c
!c  takes into account Fermi function
!c
      if ( Tempr>=very_small_temp ) then
       do ie=1,nume
        ztmp = (egrd(ie)-ef)/Tempr
        dele1(ie) = dele1(ie)*TemprFun(ztmp,0)
       end do
      end if
!c
!c  ===================================================================
!c  write out grid and return.......................................
!c
      if(iprint.ge.-10) then
       write(6,'(/''    Vert.contour at'',t40,''='',                    &
     &                              f12.5,'' and '',f12.5)') ebot,etop
       write(6,'(''     Top of vert-contour'',t40,''='',                &
     &                                         f12.5)') eTempr
       write(6,'(''     Number of energies: vert-part'',t40,            &
     &          ''='',i4)') np0
       write(6,'(''     Lin.contour at '',t40,''='',                    &
     &                                 f12.5)') aimag(egrd(np0+1))
       write(6,'(''     Number of energies: lin-part'',t40,             &
     &          ''='',i4)') np1
       write(6,'(/)')
      end if
      return
!c
!EOC
      end subroutine conboxTempr
!c
!c     ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
!BOP
!!ROUTINE: conlegndr
!!INTERFACE:
      subroutine conlegndr(e0,erad,alpha,beta,nume,egrd,dele1)
!!DESCRIPTION:
! {\bv
!  constructs semi-circle contour based on segment with arbitrary direction
!
!                            . ! .
!                        .     !     .
!                    .         !         .
!                  .           !
!                .             !
!                              !
!       -----------------------!-------------------------
!                             e0
!
!
!  e0        : center of the circle
!  erad      : radius of the circle
!  alpha     : "bottom" angle
!  beta      : "top" angle
!            :  for semi-circle -- alpha=pi, beta=zero
!  nume      : number of points for Gauss-Legendre quadrature
!
!  output    : (egrd(ie),dele1(ie),ie=1,nume)
! \ev}

!!DO_NOT_PRINT
      implicit   none
!!DO_NOT_PRINT

!!ARGUMENTS:
      complex(8), intent(in) :: e0
      real(8),    intent(in) :: erad,alpha,beta
      integer,    intent(in) :: nume
      complex(8) ::  egrd(*)
      complex(8) ::  dele1(*)
!!REVISION HISTORY:
! Update A.S. - 2020: restriction nume<=200 removed
! Initial Version - A.S. - 1998
!EOP
!
!BOC
      integer    ie
!c
      real(8) :: xgs(nume)
      real(8) :: wgs(nume)
      real*8  phi,phi0,dphi
      complex*16 zzero
!c
!c parameters
!c
!c      real*8, parameter :: zero = 0.d0
      real*8, parameter :: one = 1.d0, half = 0.5d0
      complex*16 sqrtm1
      parameter  (sqrtm1=(0.d0,1.d0))
      character  sname*10
      parameter  (sname='conlegndr')
!c      real(8), parameter :: pi=4*atan(one)
!c
!c     ----------------------------------------------------------------
      call gauleg(-one,one,xgs,wgs,nume)
!c     ----------------------------------------------------------------
!c
!c     loop over energy grid for gaussian integration.................

      phi0 = (beta+alpha) * half
      dphi = (beta-alpha) * half

      if(abs(dphi).lt.1.d-10) then
       zzero =  erad*exp( sqrtm1*phi0 )
       do ie=1,nume
        egrd(ie) = e0 + zzero*xgs(ie)
        dele1(ie)= zzero*wgs(ie)
       enddo
      else
       do ie=1,nume
        phi =  phi0 + xgs(ie)*dphi
        zzero =  erad*exp( sqrtm1*phi )
        egrd(ie) = e0 + zzero
        dele1(ie)=wgs(ie)*zzero*(sqrtm1*dphi)
       enddo
      end if
!EOC
      return
!c
      end subroutine conlegndr

!c     ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
!BOP
!!ROUTINE: conlagr
!!INTERFACE:
      subroutine conlagr(Tempr,e0,ngauss,egrd,dele1)
!!DESCRIPTION:
! {\bv
!  constructs Gauss-Laguerr linear energy contour
!
!  e0        : starting point
!  ngauss    : total number of desired points and weights for...
!              Gauss-Laguerre quadrature.......................
!  output    : (egrd(ie),dele1(ie),ie=1,ngauss)
! \ev}

!!DO_NOT_PRINT
      implicit   none
!!DO_NOT_PRINT

!!ARGUMENTS:
      real(8), intent(in)     :: Tempr
      complex(8), intent(in)  :: e0
      integer, intent(in)     :: ngauss
      complex(8), intent(out) :: egrd(ngauss)
      complex(8), intent(out) :: dele1(ngauss)
!!REVISION HISTORY:
! Update A.S. - 2020: restriction ngauss<=100 removed
! Initial Version - A.S. - 1998
!EOP
!
!BOC

!?REMARKS:
! ngauss should be .le. 100
!!!      integer, parameter :: ipts=100
      integer :: ie
      real(8) :: xgs(ngauss)
      real(8) :: wgs(ngauss)
!c
!c     +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
      character  sname*10
      parameter  (sname='conlagr')
!c     +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

!!!      if(ngauss.gt.ipts) then
!!!       write(6,'(a10,'':: ngauss.gt.ipts'',2i5)')                       &
!!!     &       sname,ngauss,ipts
!!!       call p_fstop(sname)
!!!      endif

      if ( ngauss==1 ) then        ! do not use ngauss=1 unless for debug
         egrd(1) = e0 + Tempr
         dele1(1)= dcmplx(abs(Tempr)*exp(1.0d0),0.d0)
      else
       call lagzo(ngauss,xgs,wgs)

       if(Tempr.ge.0.d0) then
        do ie=1,ngauss
         egrd(ie) = e0 + Tempr*xgs(ie)
         dele1(ie)= dcmplx(abs(Tempr)*exp(xgs(ie))*wgs(ie),0.d0)
        enddo
       else
        do ie=1,ngauss
        egrd(ngauss+1-ie) = e0 + Tempr*xgs(ie)
        dele1(ngauss+1-ie)= dcmplx(abs(Tempr)*exp(xgs(ie))*wgs(ie),0.d0)
        enddo
      end if

      end if

      return
!EOC
      end subroutine conlagr
!BOP
!!ROUTINE: conlagr1
!!INTERFACE:
      subroutine conlagr1(Tempr,e0,ef,ngauss,egrd,wghts)
!!DESCRIPTION:
! {\bv
!  constructs Gauss-Laguerr linear energy contour
!
!  Tempr     : temperature
!  e0        : E = e0 + e = e0 + T*zi, e0 is lower integration limit)
!  ef        : chemical potential
!  ngauss    : total number of desired points and weights for...
!              Gauss-Laguerre quadrature.......................
!  output    : (egrd(ie),dele1(ie),ie=1,ngauss), weights do not include Fermi factor.
! \ev}

!!DO_NOT_PRINT
      implicit   none
!!DO_NOT_PRINT

!!ARGUMENTS:
      real(8), intent(in)   :: Tempr
      real(8), intent(in)   :: e0
      real(8), intent(in)   :: ef
      integer, intent(in)   :: ngauss
      real(8), intent(out)  :: egrd(ngauss)
      real(8), intent(out)  :: wghts(ngauss)
!!REVISION HISTORY:
! Revised - A.S. - 2020
! Initial Version - A.S. - 1998
!EOP
!
!BOC
      integer :: ie
      real(8) :: x0
      real(8) :: xgs(ngauss)
      real(8) :: wgs(ngauss)
      real(8) :: ctmp(ngauss)
      real(8), external :: fermiFun
!c
!c     +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
      character  sname*10
      parameter  (sname='conlagr1')
!c     +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

      if ( Tempr==0.d0) then
       egrd = e0 ; wghts = 0.d0
       return
      end if
      if ( ngauss==1 ) then        ! do not use ngauss=1 unless for debug
         egrd = e0 + Tempr
         wghts= dabs(Tempr)*dexp(1.0d0)
      else
       x0 = (e0-ef)/Tempr
       call lagzo(ngauss,xgs,wgs)
       wghts = wgs*dexp(xgs)    !  exp(x): f(x) * exp(x) * (exp(-x))
       wghts = dabs(Tempr)*wghts*fermiFun(x0+xgs)/fermiFun(xgs) ! due to x0/e0-shift

       egrd  = e0 + abs(Tempr)*xgs
       if ( Tempr < 0.d0) then  ! NOT TESTED
        do ie=1,ngauss
         ctmp(ngauss+1-ie) = egrd(ie)
        end do
        egrd = ctmp
        do ie=1,ngauss
         ctmp(ngauss+1-ie) = wghts(ie)
        end do
        wghts = ctmp
       end if

      end if

!DEBUGPRINT
!      write(6,'(a,i3,3d13.4)') ' DEBUG NGAUSS2 = ',ngauss,Tempr,e0,ef
!        write(6,'('' n='',i5,'' e='',d13.4,'' de='',d13.4)')            &
!     &   (ie,egrd(ie),wghts(ie),ie=1,ngauss)
!DEBUG PRINT
      return
!EOC
      end subroutine conlagr1
!
!BOP
!!ROUTINE: LAGZO
!!INTERFACE:
        SUBROUTINE LAGZO(N,X,W)
!!DESCRIPTION:
! {\bv
!   Purpose : Compute the zeros of Laguerre polynomial Ln(x)
!             in the interval [0,Inf], and the corresponding
!             weighting coefficients for Gauss-Laguerre
!             integration
!   Input :   n    --- Order of the Laguerre polynomial
!             X(n) --- Zeros of the Laguerre polynomial
!             W(n) --- Corresponding weighting coefficients
!
!    This routine is copyrighted by Shanjie Zhang and Jianming Jin.  However,
!    they give permission to incorporate this routine into a user program
!    provided that the copyright is acknowledged.
!
!  Reference:
!
!    Shanjie Zhang, Jianming Jin,
!    Computation of Special Functions,
!    Wiley, 1996,
!    ISBN: 0-471-11963-6,
!    LC: QA351.C45.
!
! \ev}

!EOP
!
!BOC
        IMPLICIT DOUBLE PRECISION (A-H,O-Z)
        DIMENSION X(N),W(N)
        IF ( N<=1 ) THEN
         STOP 'LAGZO ERROR: PARAMETER N <= 1'
        END IF
        HN=1.0D0/N
        DO 35 NR=1,N
           IF (NR.EQ.1) Z=HN
           IF (NR.GT.1) Z=X(NR-1)+HN*NR**1.27D0
           IT=0
10         IT=IT+1
           Z0=Z
           P=1.0D0
           DO 15 I=1,NR-1
15            P=P*(Z-X(I))
           F0=1.0D0
           F1=1.0D0-Z
           DO 20 K=2,N
              PF=((2.0D0*K-1.0D0-Z)*F1-(K-1.0D0)*F0)/K
              PD=K/Z*(PF-F1)
              F0=F1
20            F1=PF
           FD=PF/P
           Q=0.0D0
           DO 30 I=1,NR-1
              WP=1.0D0
              DO 25 J=1,NR-1
                 IF (J.EQ.I) GO TO 25
                 WP=WP*(Z-X(J))
25            CONTINUE
              Q=Q+WP
30         CONTINUE
           GD=(PD-Q*FD)/P
           Z=Z-FD/GD
           IF (IT.LE.40.AND.DABS((Z-Z0)/Z).GT.1.0D-15) GO TO 10
           X(NR)=Z
           W(NR)=1.0D0/(Z*PD*PD)
35      CONTINUE
        RETURN
!EOC
        END SUBROUTINE LAGZO
