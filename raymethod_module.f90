!BOP
!!MODULE: raymethod
!!INTERFACE:
module raymethod
!!DESCRIPTION:

! Module contains routines that can
! {\bv
! (1) Find Brillouin Zone cell vertices & face normals
! (2) Perform Integral[ tau(k) dk, k spans BZ ]
! (2) Perform Integral[ green(k) dk, k spans BZ ]
! (3) Plot Bloch Spectral Fn A(k,E) (see below for description)
! (4) Find integrated density of states N(E) modulo 1.0
! (5) Find band structure E(k)
! \ev}
!
!!USES:
  use universal_const, only : pi,DBL_R
  use mtrx_interface, only  : invmatr,wrtmtx
  use mecca_types, only : DosBsf_Data,FS_Data
  use mecca_run,   only : gGenName
  use freespaceG,  only : gf0_ks
  use timing

!!DO_NOT_PRINT
  implicit none
  private
!!DO_NOT_PRINT

!!PUBLIC MEMBER FUNCTIONS:
  public :: addlloydcorr
  public :: findblochstate
  public :: getblochspecfn
  public :: ksrayint
  public :: sRayParams
  public :: sDosInpParams
  public :: sBsfInpParams
  public :: sFsInpParams
  public :: gFsInput
  public :: gDosBsfInput
!!PRIVATE MEMBER FUNCTIONS:
! subroutine genhexrays
! subroutine genboxrays
! subroutine clipray
! subroutine clipbz
! subroutine getwedgevol
! subroutine dumprays
! subroutine evalwedgeint
! recursive subroutine evalwedgeintrec
! subroutine evalrayint
! subroutine evaltau
! subroutine makenearest
! subroutine makeprimbas
! subroutine getcubicfit
! subroutine getquadfit
! subroutine getlinefit
! subroutine getquadroots
! subroutine getcubicroots
! subroutine dolongdiv
! subroutine getcellsymm
! subroutine triangulatecell
! subroutine getwscell
! subroutine clipface
! subroutine checkToAbort
! subroutine dumpcellgeom
!
! function center
! function midpoint
! function cross
! function dot
!
!!REVISION HISTORY:
! Adapted  - A.S. - 2013
!AUTHORS:
! Suffian Khan 05/24/2013
!EOP
!
!  public :: dot
!  public :: cross
!
!BOC

  integer, parameter :: dp = DBL_R  ! kind(1.d0)
  integer, parameter :: cd = DBL_R  ! kind((1.d0,1.d0))

  type :: face
    real(kind=dp) :: normal(3)
    real(kind=dp), pointer :: vert(:,:)
    integer :: nvert
  end type

  type :: i1ptr
    integer, pointer :: p(:) => null()
  end type

  type(DosBsf_Data), target :: dosbsf_input
  type(FS_Data), target     :: fermis_input

  ! fixed integration parameters
  integer,       parameter :: maxlvl = 5

  ! .. moved these to input file
  logical, save :: adaptive  = .false.
  logical, save :: fixedrays = .true.    ! from input file
  integer :: nraysubint=0   ! n = 1,2,3,...
  integer :: nrecursion=0   ! can't exceed 'maxlvl' below

  ! static set of rays

  integer, save :: nx=0, ny=0, nz=0

  logical, save :: hexbox_m = .false.

  ! Debugging Variables
  ! to do: reorganize debugging code, hide output

  logical, parameter :: debugflag  = .false.

  logical, save :: pr_rayintegrand = .false.
  logical, save :: pr_bzgeometry   = .true.
  logical, save :: pr_rays         = .false.

  integer :: iprint=-100
  character(10), parameter :: istop='null'

!EOC
contains

!BOP
!!IROUTINE: sDosInpParams
!!INTERFACE:
  subroutine sDosInpParams(iplot,kintsch,nx,ny,nz,nepts,emin,emax, &
                     esmear,nrec_ray,nrad_ray,fixed_ray,hexbox_ray)
!!DESCRIPTION:
! initializes parameters for DOS-related calculations

!!DO_NOT_PRINT
  implicit none
!!DO_NOT_PRINT

!!ARGUMENTS:
  integer, intent(in), optional :: iplot
  integer, intent(in), optional :: kintsch
  integer, intent(in), optional :: nx,ny,nz
  integer, intent(in), optional :: nepts
  real(dp), intent(in), optional :: emin,emax,esmear
  integer, intent(in), optional :: nrec_ray,nrad_ray
  logical, intent(in), optional :: fixed_ray,hexbox_ray
!!REVISION HISTORY:
! Initial Version - A.S. - 2013
!EOP
!
!BOC

  if ( present(iplot) ) dosbsf_input%idosplot = iplot
  if ( present(kintsch) ) dosbsf_input%dosbsf_kintsch = kintsch
  if ( present(nx) ) dosbsf_input%nx = nx
  if ( present(ny) ) dosbsf_input%ny = ny
  if ( present(nz) ) dosbsf_input%nz = nz
  if ( present(nepts) ) dosbsf_input%dosbsf_nepts = nepts
  if ( present(emin) ) dosbsf_input%dosbsf_emin = emin
  if ( present(emax) ) dosbsf_input%dosbsf_emax = emax
  if ( present(esmear) ) dosbsf_input%dosbsf_esmear = esmear

  if ( present(fixed_ray) ) dosbsf_input%ray_fixed = fixed_ray
  if ( present(nrec_ray) ) then
    nrecursion = min(nrec_ray,maxlvl)
    adaptive = nrec_ray<0
    dosbsf_input%ray_nrec = min(nrec_ray,maxlvl)
  end if
  if ( present(nrad_ray) ) then
    nraysubint = nrad_ray
    dosbsf_input%ray_nrad = nrad_ray
  end if
  if ( present(hexbox_ray) ) then
      hexbox_m = hexbox_ray
  end if

  return
!EOC
  end subroutine sDosInpParams

!BOP
!!IROUTINE: sBsfInpParams
!!INTERFACE:
  subroutine sBsfInpParams(iplot,ksamplerate,kwaypts,kptlabel)
!!DESCRIPTION:
! initializes parameters for BSF-related calculations

!!DO_NOT_PRINT
  implicit none
!!DO_NOT_PRINT

!!ARGUMENTS:
  integer, intent(in), optional :: iplot
  integer, intent(in), optional :: ksamplerate
  real(dp), intent(in), optional :: kwaypts(:,:)
  character(*), intent(in), optional :: kptlabel(:)
!!REVISION HISTORY:
! Initial Version - A.S. - 2013
!EOP
!
!BOC
  integer :: ndpts

  if ( present(iplot) ) dosbsf_input%ibsfplot = iplot
  if ( present(ksamplerate) ) dosbsf_input%bsf_ksamplerate = ksamplerate
!  if ( present(klen) ) dosbsf_input%klen = klen
!  if ( present(ef) ) dosbsf_input%bsf_ef = ef

  if ( present(kwaypts) .and. present(kptlabel) ) then
    ndpts = min(size(kwaypts,2),size(kptlabel,1))
    if ( allocated(dosbsf_input%bsf_kwaypts) ) then
       if ( size(dosbsf_input%bsf_kwaypts,2)<ndpts ) then
            deallocate(dosbsf_input%bsf_kwaypts)
        end if
    end if
    if ( .not. allocated(dosbsf_input%bsf_kwaypts) ) &
      allocate(dosbsf_input%bsf_kwaypts(3,ndpts))
    if ( allocated(dosbsf_input%bsf_kptlabel) ) then
        if ( size(dosbsf_input%bsf_kptlabel,1)<ndpts ) then
            deallocate(dosbsf_input%bsf_kptlabel)
        end if
    end if
    if ( .not. allocated(dosbsf_input%bsf_kptlabel) ) &
      allocate(dosbsf_input%bsf_kptlabel(ndpts))
    dosbsf_input%bsf_kwaypts(1:3,1:ndpts) = &
                kwaypts(1:3,1:ndpts)
    dosbsf_input%bsf_kptlabel(1:ndpts) = &
                kptlabel(1:ndpts)
    dosbsf_input%bsf_nkwaypts = ndpts
  end if

  return
!EOC
  end subroutine sBsfInpParams

!BOP
!!IROUTINE: sFsInpParams
!!INTERFACE:
  subroutine sFsInpParams(iplot,en,korig,gridsampling,raysampling, &
                                      orilabel,kpts,kptlabel)
!!DESCRIPTION:
! initializes parameters for FS-related calculations

!!DO_NOT_PRINT
  implicit none
!!DO_NOT_PRINT

!!ARGUMENTS:
  integer, intent(in), optional :: iplot
  real(dp), intent(in), optional :: en
  real(dp), intent(in), optional :: korig(3)
  integer, intent(in), optional :: gridsampling
  integer, intent(in), optional :: raysampling
  character(*), intent(in), optional :: orilabel
  real(dp), intent(in), optional :: kpts(:,:)
  character(*), intent(in), optional :: kptlabel(:)
!!REVISION HISTORY:
! Initial Version - A.S. - 2013
!EOP
!
!BOC
  integer :: ndpts

  if ( present(iplot) ) fermis_input%ifermiplot = iplot
  if ( present(en) ) fermis_input%fermi_en = en
  if ( present(korig) ) fermis_input%fermi_korig = korig
  if ( present(orilabel) ) fermis_input%fermi_orilabel = orilabel
  if ( present(gridsampling) ) fermis_input%fermi_gridsampling = gridsampling
  if ( present(raysampling) ) fermis_input%fermi_raysampling = raysampling

  if ( present(kpts) .and. present(kptlabel) ) then
    ndpts = min(size(kpts,2),size(kptlabel,1))
    if ( allocated(fermis_input%fermi_kpts) ) then
        if ( size(fermis_input%fermi_kpts,2)<ndpts ) then
            deallocate(fermis_input%fermi_kpts)
        end if
    end if
    if ( .not. allocated(fermis_input%fermi_kpts) ) &
      allocate(fermis_input%fermi_kpts(3,ndpts))
    if ( allocated(fermis_input%fermi_kptlabel) ) then
        if ( size(fermis_input%fermi_kptlabel,1)<ndpts ) then
            deallocate(fermis_input%fermi_kptlabel)
        end if
    end if
    if ( .not. allocated(fermis_input%fermi_kptlabel) ) &
      allocate(fermis_input%fermi_kptlabel(ndpts))
    fermis_input%fermi_kpts(1:3,1:ndpts) = kpts(1:3,1:ndpts)
    fermis_input%fermi_kptlabel(1:ndpts) = kptlabel(1:ndpts)
    fermis_input%fermi_nkpts = ndpts
  end if

  return
!EOC
  end subroutine sFsInpParams

!BOP
!!IROUTINE: gDosBsfInput
!!INTERFACE:
  subroutine gDosBsfInput(dosbsf)
!!DESCRIPTION:
! gets {\tt dosbsf} pointer (type DosBsf\_Data)

!!ARGUMENTS:
  type(DosBsf_Data), intent(out), pointer :: dosbsf
!!REVISION HISTORY:
! Initial Version - A.S. - 2013
!EOP
!
!BOC
  dosbsf => dosbsf_input
  return
!EOC
  end subroutine gDosBsfInput

!BOP
!!IROUTINE: gFsInput
!!INTERFACE:
  subroutine gFsInput(fermis)
!!DESCRIPTION:
! gets {\tt fermis} pointer (type FS\_Data)
!
!!ARGUMENTS:
  type(FS_Data), intent(out), pointer :: fermis
!!REVISION HISTORY:
! Initial Version - A.S. - 2013
!EOP
!
!BOC
  fermis => fermis_input
  return
!EOC
  end subroutine gFsInput

!BOP
!!IROUTINE: sRayParams
!!INTERFACE:
  subroutine sRayParams(nrec_ray,nrad_ray,nx_ray,ny_ray,nz_ray,hexbox_ray)
!!DESCRIPTION:
! initializes parameters for ray-method calculations
!
!!ARGUMENTS:
  integer, intent(in), optional :: nrec_ray
  integer, intent(in), optional :: nrad_ray
  integer, intent(in), optional :: nx_ray,ny_ray,nz_ray
  logical, intent(in), optional :: hexbox_ray
!!REVISION HISTORY:
! Initial Version - A.S. - 2013
!EOP
!
!BOC
  if ( present(nrec_ray) ) then
    nrecursion = min(nrec_ray,maxlvl)
    adaptive = nrec_ray<0
  end if
  if ( present(nrad_ray) ) then
    nraysubint = nrad_ray
  end if
  if ( present(nx_ray) ) then
    nx = nx_ray
  end if
  if ( present(ny_ray) ) then
    ny = ny_ray
  end if
  if ( present(nz_ray) ) then
    nz = nz_ray
  end if
  if ( present(hexbox_ray) ) then
      hexbox_m = hexbox_ray
  end if
  return
!EOC
  end subroutine sRayParams

!DEBUG???  subroutine gRayParams(iray_nrec,iray_nrad)
!DEBUG???  integer, intent(out) :: iray_nrec,iray_nrad
!DEBUG???  iray_nrec = ray_nrec
!DEBUG???  iray_nrad = ray_nrad
!DEBUG???  return
!DEBUG???  end subroutine gRayParams

!BOP
!!IROUTINE: ksrayint
!!INTERFACE:
  subroutine ksrayint( lmax,kkrsz,natom,aij,mapstr,mappnt,nsublat,mapij, &
      powe,edu,pdu,rsnij,ndimrij,np2r,ndimnp, &
      numbrs,naij,dqint,ndimdqr,hplnm,ndimrhp,ndimlhp,irecdlm,d00,eoeta, &
      eta,ksvecs,nksvecs,conr,tcpa,iorig,itype, &
      avec,nop,rot,dop,if0,tau00,lloydms,green00 )
!!DESCRIPTION:
! returns {\tt tau00} = $int[tau(k,z)dk]$ using ray method

!EOP
!
!BOC

!dummy arguments: itype

    implicit none
    ! structure constant parameters

    integer lmax,natom,nsublat,ndimrij,kkrsz
    integer ndimnp,ndimdqr,ndimrhp,ndimlhp
    integer mapij(2,*),naij,np2r(ndimnp,*),iorig(natom)
    integer mapstr(natom,natom),mappnt(natom,natom),numbrs(*)
    integer nksvecs,itype(natom),irecdlm

    real*8 aij(3,*),rsnij(ndimrij,4)
    real*8 eta
    real*8 ksvecs(nksvecs,3)

    complex*16 conr(*),powe(*),dqint(ndimdqr,*)
    complex*16 lloydms
!    complex(8) greenks(kkrsz*natom,kkrsz*natom)
    complex(8) :: tcpa(:,:,:)    ! (ipkkr,ipkkr,natom)
    complex(8) :: tau00(:,:,:)   ! (ipkkr,ipkkr,nsublat)
    complex(8) :: green00(:,:,:)  ! (ipkkr,ipkkr,nsublat)
    complex*16 edu,pdu,d00,eoeta
    complex*16 hplnm(ndimrhp,ndimlhp), wi(kkrsz,kkrsz,natom)
    complex*16 wgi(kkrsz,kkrsz,natom)

    ! symmetry related parameters

    integer nop
    real*8 rot(49,3,3)
    complex*16 dop(kkrsz,kkrsz,nop)
    integer if0(48,natom)
!    integer ihg,ihc

    ! subroutine specific parameters

    complex(kind=cd) :: li
    logical :: evalgamma, unexpected
    logical, save :: bzcellexists = .false.

    integer, save :: nfaces, nwedges, norbits
    type(face), pointer :: bzfaces(:) => null()
    real(kind=dp), pointer, save :: bzwedgevert(:,:,:) => null()
    real(kind=dp), pointer, save :: bzwedgerays(:,:) => null()
    integer, pointer, save :: orbitlens(:) => null()
    type(i1ptr), pointer, save :: wedgeorbits(:) => null()
    type(i1ptr), pointer, save :: symmsubsets(:) => null()

    integer, save :: nrays, nrayorbits
    real(kind=dp), pointer, save :: raylist(:,:) => null()
    real(kind=dp), pointer, save :: rayweights(:) => null()
    integer, pointer, save :: rayorbitlens(:) => null()
    type(i1ptr), pointer, save :: rayorbits(:) => null()
    type(i1ptr), pointer, save :: raysymmsubsets(:) => null()

    real(kind=dp), save :: cellavec(3,3) = 0.d0

!DEBUG    real(kind=dp) :: v1(3), v2(3), v3(3)
    real(kind=dp) :: avec(3,3)
    real(kind=dp), save :: bzvol
    integer :: i, j, k, n, m
    logical :: hexbox

!DEBUG    integer :: iray_nrec, iray_nrad

    complex*16 A(kkrsz,kkrsz), B(kkrsz,kkrsz)

    ! executable statements

!DEBUG    nrecursion = iray_nrec
!DEBUG    nraysubint = iray_nrad

    ! on first call, determine brillouin zone geometry

    unexpected = .false.
    do i = 1, 3; do j = 1, 3
      if( avec(i,j) /= cellavec(i,j) ) bzcellexists = .false.
    end do; end do

    if( .not. bzcellexists ) then

      ! get (some) readin variables through backend
!DEBUG      call sendrcvray(0,nx,ny,nz,fixedrays)
!DEBUG      call sendrcvsymm(0,ihg,ihc)

!DEBUG      hexbox = .false.
!DEBUG      select case(ihg)
!DEBUG        case(3:5)
!DEBUG          hexbox = .false.
!DEBUG        case(7)
!DEBUG          hexbox = .true.
!DEBUG      end select

      if ( nop<12 .or. nop>24 ) then
        hexbox = .false.
      else
        hexbox = hexbox_m
      end if
      ! deconstruct previous cell
      if( associated(wedgeorbits) ) then
        do i = 1, norbits
         if ( associated(wedgeorbits(i)%p) ) then
          deallocate(wedgeorbits(i)%p)
          wedgeorbits(i)%p => null()
         end if
        end do
        wedgeorbits => null()
      end if
      if( associated(symmsubsets) ) then
        do i = 1, norbits
         if ( associated(symmsubsets(i)%p) ) then
          deallocate(symmsubsets(i)%p)
          symmsubsets(i)%p => null()
         end if
        end do
        symmsubsets => null()
      end if
      if( associated(orbitlens) ) then
        deallocate(orbitlens)
        orbitlens => null()
      end if
      if( associated(bzwedgevert) ) then
        deallocate(bzwedgevert)
        bzwedgevert => null()
      end if
      if( associated(bzwedgerays) ) then
        deallocate(bzwedgerays)
        bzwedgerays => null()
      end if
      if( associated(bzfaces) ) then
        do i = 1, nfaces
         if ( associated(bzfaces(i)%vert) ) then
          deallocate( bzfaces(i)%vert )
          bzfaces(i)%vert => null()
         end if
        end do
        deallocate( bzfaces )
        bzfaces => null()
      end if
      if ( associated(raylist) ) then
        deallocate(raylist)
        raylist => null()
      end if
      if ( associated(rayweights) ) then
        deallocate(rayweights)
        rayweights => null()
      end if

      write(6,*) ''
      write(6,'(a,i3)') ' radial pts/du = ', nraysubint
      if( fixedrays ) then
        if( .not. hexbox ) then
          write(6,'(a)') '    ray choice = enclosing box'
        else
          write(6,'(a)') '    ray choice = enclosed hexagon'
        end if
      else
        write(6,'(a)') '    ray choise = recursive div'
      end if
      write(6,*) ''

      ! construct bz using clipping method
      call getwscell(avec,nfaces,bzfaces)
      call triangulatecell(nfaces,bzfaces,nwedges,bzwedgevert,bzwedgerays)
      call getcellsymm(nwedges,bzwedgerays,nop,rot,norbits,orbitlens, &
             wedgeorbits, symmsubsets)
      if( .not. fixedrays ) then
        write(6,'(a,i3)') ' num. of wedges = ', nwedges
        write(6,'(a,i3)') ' num. of wedge orbits = ', norbits
        if ( nop==1 ) then
         if ( minval(orbitlens)==maxval(orbitlens) ) then
          write(6,'(a)') ' orbit sizes = { 1 } for all orbits'
         else
          unexpected = .true.
         end if
        end if
        if ( nop>1 .or. unexpected ) then
         write(6,'(a)',advance='no') ' orbit sizes = { '
         do i = 1, norbits
          write(6,'(i3)',advance='no') orbitlens(i)
          if( mod(i,40) == 0 ) write(6,*) ''
         end do
         write(6,'(a)') ' }'
         unexpected = .false.
        end if
      end if

      bzvol = dabs(dot(cross(avec(:,1),avec(:,2)),avec(:,3)))
      cellavec = avec

      bzcellexists = .true.

      ! dump bz geometry to file
      if( pr_bzgeometry ) then
        call dumpcellgeom(avec,nfaces,bzfaces)
      end if

      ! static ray list of rays from bounding box
      if( fixedrays ) then
        if( .not. hexbox ) then
          call genboxrays(nrays,raylist,rayweights,nx,ny,nz,nfaces,bzfaces)
        else
          call genhexrays(nrays,raylist,rayweights,nx,ny,nz,nfaces,bzfaces)
        end if
        if( pr_rays ) then
          call dumprays(nrays,raylist,rayweights)
        end if
        call getcellsymm(nrays,raylist,nop,rot,nrayorbits,rayorbitlens, &
             rayorbits, raysymmsubsets)
        write(6,'(a,i6)') ' num. of ray orbits = ', nrayorbits
        if ( nop==1 ) then
         if ( minval(orbitlens)==maxval(orbitlens) ) then
          write(6,'(a)') ' orbit sizes = { 1 } for all orbits'
         else
          unexpected = .true.
         end if
        end if
        if ( nop>1 .or. unexpected ) then
         write(6,'(a)') ' orbit sizes = { '
         do i = 1, nrayorbits
          write(6,'(i3)',advance='no') rayorbitlens(i)
          if( mod(i,40) == 0 ) write(6,*) ''
         end do
         write(6,'(a)') ' }'
         unexpected = .false.
        end if
        write(6,*) ''
      end if

    end if

    ! perform brillouin zone integral

    tau00 = dcmplx(0.0d0,0.0d0)
    lloydms = dcmplx(0.0d0,0.0d0)
    green00 = dcmplx(0.0d0,0.0d0)

    ! rays spread over wedges
    if( .not. fixedrays ) then

      do i = 1, norbits

        if(i == 1) evalgamma = .true.
        if(i /= 1) evalgamma = .false.

        ! integrate tau(k) within reduced zone wedge
        n = wedgeorbits(i)%p(1)
        call evalwedgeint( &
          bzwedgevert(:,1,n), bzwedgevert(:,2,n), bzwedgevert(:,3,n), wi,li,wgi,&
          lmax,kkrsz,natom,aij,mapstr,mappnt,nsublat,mapij, &
          powe,edu,pdu,rsnij,ndimrij,np2r,ndimnp, &
          numbrs,naij,dqint,ndimdqr,hplnm,ndimrhp,ndimlhp,irecdlm,d00,eoeta, &
          eta,ksvecs,nksvecs,conr,tcpa,iorig,evalgamma )

        ! unfold reduced tau to full zone
        if(debugflag) call begintiming("Unfold reduced T,G to full zone")
        lloydms = lloydms + orbitlens(i)*li
        do j = 1, orbitlens(i)
          n = symmsubsets(i)%p(j)
          m = kkrsz

          do k = 1, nsublat
            A = dconjg(dop(:,:,n))
            call zgemm('N','N',m,m,m,dcmplx(1.d0,0.d0),A,m,&
              wi(1,1,if0(n,iorig(k))),m,dcmplx(0.d0,0.d0),B,m)
            call zgemm('N','T',m,m,m,dcmplx(1.d0,0.d0),B,m,&
              dop(1,1,n),m,dcmplx(0.d0,0.d0),A,m)
            tau00(1:kkrsz,1:kkrsz,k) = tau00(1:kkrsz,1:kkrsz,k) + A(:,:)
          end do

          do k = 1, nsublat
            A = dconjg(dop(:,:,n))
            call zgemm('N','N',m,m,m,dcmplx(1.d0,0.d0),A,m,&
              wgi(1,1,if0(n,iorig(k))),m,dcmplx(0.d0,0.d0),B,m)
            call zgemm('N','T',m,m,m,dcmplx(1.d0,0.d0),B,m,&
              dop(1,1,n),m,dcmplx(0.d0,0.d0),A,m)
            green00(1:kkrsz,1:kkrsz,k) = green00(1:kkrsz,1:kkrsz,k) + A(:,:)
          end do

        end do
        if(debugflag) call endtiming()

      end do

    else  ! fixed set of rays from enclosing shape

      do i = 1, nrayorbits

        if(i == 1) evalgamma = .true.
        if(i /= 1) evalgamma = .false.

        ! integrate tau(k) over line corresponding to wedge
        n = rayorbits(i)%p(1)
        call evalrayint( raylist(:,n), nraysubint, wi, li, wgi, &
          lmax,kkrsz,natom,aij,mapstr,mappnt,nsublat,mapij, &
          powe,edu,pdu,rsnij,ndimrij,np2r,ndimnp, &
          numbrs,naij,dqint,ndimdqr,hplnm,ndimrhp,ndimlhp,irecdlm,d00,eoeta, &
          eta,ksvecs,nksvecs,conr,tcpa,evalgamma )
        wi  = wi*rayweights(n)
        wgi = wgi*rayweights(n)
        li  = li*rayweights(n)

        ! unfold reduced tau to full zone
        if(debugflag) call begintiming("Unfold reduced T,G to full zone")
        lloydms = lloydms + rayorbitlens(i)*li
        do j = 1, rayorbitlens(i)
          n = raysymmsubsets(i)%p(j)
          m = kkrsz

          do k = 1, nsublat
            A = dconjg(dop(:,:,n))
            call zgemm('N','N',m,m,m,dcmplx(1.d0,0.d0),A,m,&
              wi(1,1,if0(n,iorig(k))),m,dcmplx(0.d0,0.d0),B,m)
            call zgemm('N','T',m,m,m,dcmplx(1.d0,0.d0),B,m,&
              dop(1,1,n),m,dcmplx(0.d0,0.d0),A,m)
            tau00(1:kkrsz,1:kkrsz,k) = tau00(1:kkrsz,1:kkrsz,k) + A(:,:)
          end do

          do k = 1, nsublat
            A = dconjg(dop(:,:,n))
            call zgemm('N','N',m,m,m,dcmplx(1.d0,0.d0),A,m,&
              wgi(1,1,if0(n,iorig(k))),m,dcmplx(0.d0,0.d0),B,m)
            call zgemm('N','T',m,m,m,dcmplx(1.d0,0.d0),B,m,&
              dop(1,1,n),m,dcmplx(0.d0,0.d0),A,m)
            green00(1:kkrsz,1:kkrsz,k) = green00(1:kkrsz,1:kkrsz,k) + A(:,:)
          end do

        end do
        if(debugflag) call endtiming()

      end do

    end if

    tau00 = tau00/bzvol
    lloydms = lloydms/bzvol
    green00 = green00/bzvol

    if(debugflag) call dumptimings()

!EOC
  end subroutine ksrayint

!BOP
!!IROUTINE: genhexrays
!!INTERFACE:
  subroutine genhexrays(nrays,raylist,rayweights,nx,ny,nz,nfaces,bzfaces)
!!DESCRIPTION:
! generates rays for hexagonal symmetry
!

!!DO_NOT_PRINT
    implicit none
!!DO_NOT_PRINT

!!ARGUMENTS:
    integer, intent(out) :: nrays
    real(kind=dp), pointer, intent(out) :: raylist(:,:)
    real(kind=dp), pointer, intent(out) :: rayweights(:)
    integer, intent(in) :: nx,ny,nz
    integer, intent(in) :: nfaces
    type(face), pointer, intent(in) :: bzfaces(:)
!!PARAMETERS:
    integer, parameter :: maxvert = 30
!EOP
!
!BOC

    integer :: nwfaces, nsides
    type(face), pointer :: wfaces(:)
    real(kind=dp) :: sides(3,4), corn(3,4)

    integer :: s, i, j, n, ni, nj, k, m, t, q
    real(kind=dp) :: hexRad, hexZrad, hexAlt, da, area, x, d
    real(kind=dp) :: topl(3), di(3), dj(3), wvol, totvol
    real(kind=dp) :: hexdir(3), v(3), th

!    real(kind=dp), parameter :: pi = 3.141592653589793238d0

    ! find circumscribing hexagonal box
    hexRad = 0.d0; hexZrad = 0.d0
    do i = 1, nfaces
    do j = 1, bzfaces(i)%nvert
      di = bzfaces(i)%vert(:,j); di(3) = 0.d0
      x = dsqrt(dot(di,di))
      if( x > hexRad ) then
        hexRad = x
        hexdir(:) = di(:)/x
      end if
      x = abs(bzfaces(i)%vert(3,j))
      if( x > hexZrad ) hexZrad = x
    end do; end do
    hexAlt = hexRad * dsqrt(3.d0)/2.d0

    ! grow the box in case any points are outside
    do i = 1, nfaces
    do j = 1, bzfaces(i)%nvert
      do s = 1, 6
        th = (s-1)*pi/3.d0 + pi/6.d0
        v(1) = dcos(th)*hexdir(1) - dsin(th)*hexdir(2)
        v(2) = dsin(th)*hexdir(1) + dcos(th)*hexdir(2)
        d = dot(v,bzfaces(i)%vert(:,j))
        if( d > hexAlt ) then
          hexRad = (d/hexAlt)*hexRad
          hexAlt = hexRad * dsqrt(3.d0)/2.d0
        end if
      end do
    end do; end do

    write(6,'(a,i3,i3,i3)') ' hex divisions = ', nx, ny, nz
    write(6,'(a,3f20.15)') ' hex radii = ', hexRad, hexZrad

    ! how many rays are needed?

    nrays = 6*nx*nz
    da = 0.5d0*hexAlt*hexRad/(ny*ny)*(2.d0*ny-1.d0)*(1.d0/nx)
    do i = 1, ny
      area = 0.5d0*hexAlt*hexRad/(ny*ny)*(2.d0*i-1.d0)
      n = nint(area/da)
      if(n == 0) n = 1
      nrays = nrays + 12*n
    end do

    allocate(raylist(3,nrays))
    allocate(rayweights(nrays))

    ! wfaces = wedge faces
    allocate(wfaces(nfaces))
    do i = 1, nfaces
      allocate(wfaces(i)%vert(3,maxvert))
      wfaces(i)%nvert = bzfaces(i)%nvert
      wfaces(i)%normal = bzfaces(i)%normal
    end do
    nwfaces = nfaces

    totvol = 0.d0

    ! lateral faces
    n = 0
    do s = 1, 6

      dj(:) = 0; dj(3) = 2.d0*hexZrad/nz

      di = 0.d0; th = (s-1)*pi/3.d0 + pi/6.d0 + pi/2.d0
      di(1) = dcos(th)*hexdir(1) - dsin(th)*hexdir(2)
      di(2) = dsin(th)*hexdir(1) + dcos(th)*hexdir(2)
      di = di*hexRad/nx

      topl = 0.d0; th = (s-1)*pi/3.d0 + pi/6.d0
      topl(1) = dcos(th)*hexdir(1) - dsin(th)*hexdir(2)
      topl(2) = dsin(th)*hexdir(1) + dcos(th)*hexdir(2)
      topl = topl*hexAlt

      ni = nx; nj = nz
      topl(:) = topl(:) + (-ni/2.d0+0.5d0)*di(:) + (-nj/2.d0+0.5d0)*dj(:)

      do i = 1, ni
      do j = 1, nj

        n = n+1
        raylist(:,n) = topl(:) + di(:)*(i-1) + dj(:)*(j-1)
        corn(:,1) = topl(:) + di(:)*(i-0.5d0) + dj(:)*(j-0.5d0)
        corn(:,2) = topl(:) + di(:)*(i-1.5d0) + dj(:)*(j-0.5d0)
        corn(:,3) = topl(:) + di(:)*(i-1.5d0) + dj(:)*(j-1.5d0)
        corn(:,4) = topl(:) + di(:)*(i-0.5d0) + dj(:)*(j-1.5d0)
        call clipray(raylist(:,n),nfaces,bzfaces)

        ! copy the BZ
        do k = 1, nfaces
          wfaces(k)%normal = bzfaces(k)%normal
          wfaces(k)%nvert  = bzfaces(k)%nvert
          do m = 1, wfaces(k)%nvert
            wfaces(k)%vert(:,m) = bzfaces(k)%vert(:,m)
          end do
        end do

        ! create sides of wedge
        nsides = 4
        sides(:,1) = cross(corn(:,2),corn(:,1))
        x = dsqrt(dot(sides(:,1),sides(:,1)))
        sides(:,1) = (1.0/x)*sides(:,1)

        sides(:,2) = cross(corn(:,3),corn(:,2))
        x = dsqrt(dot(sides(:,2),sides(:,2)))
        sides(:,2) = (1.0/x)*sides(:,2)

        sides(:,3) = cross(corn(:,4),corn(:,3))
        x = dsqrt(dot(sides(:,3),sides(:,3)))
        sides(:,3) = (1.0/x)*sides(:,3)

        sides(:,4) = cross(corn(:,1),corn(:,4))
        x = dsqrt(dot(sides(:,4),sides(:,4)))
        sides(:,4) = (1.0/x)*sides(:,4)

        call clipbz(nsides, sides, nwfaces, wfaces)
        call getwedgevol(nwfaces, wfaces, wvol)
        rayweights(n) = wvol
        totvol = totvol + wvol

      end do; end do

    end do

    ! top and bottom faces
    do t = 0, 1
    do s = 1, 6

      do i = 1, ny

        ! this code snippet should match above
        area = 0.5d0*hexAlt*hexRad/(ny*ny)*(2.d0*i-1.d0)
        q = nint(area/da)
        if(q == 0) q = 1

        di = 0.d0; th = (s-1)*pi/3.d0
        di(1) = dcos(th)*hexdir(1) - dsin(th)*hexdir(2)
        di(2) = dsin(th)*hexdir(1) + dcos(th)*hexdir(2)
        di = di*hexRad*i/ny

        dj = 0.d0; th = pi/3.d0
        dj(1) = dcos(th)*di(1) - dsin(th)*di(2)
        dj(2) = dsin(th)*di(1) + dcos(th)*di(2)

        do j = 1, q

          x = (j-0.5d0)/q
          v(:) = (1.d0-x)*di(:) + x*dj(:)
          v = (1.d0-0.5d0/i)*v
          v(3) = hexZrad
          if( t == 0 ) v(3) = -v(3)

          n = n+1
          raylist(:,n) = v
          call clipray(raylist(:,n),nfaces,bzfaces)

          if( t == 1 ) then
            corn(:,1) = di(:)*(1.d0-x+0.5d0/q) + dj(:)*(x-0.5d0/q)
            corn(:,2) = di(:)*(1.d0-x-0.5d0/q) + dj(:)*(x+0.5d0/q)
            corn(:,3) = corn(:,2) * (1.d0-1.d0/i)
            corn(:,4) = corn(:,1) * (1.d0-1.d0/i)
            corn(3,:) = hexZrad
          else
            corn(:,2) = di(:)*(1.d0-x+0.5d0/q) + dj(:)*(x-0.5d0/q)
            corn(:,1) = di(:)*(1.d0-x-0.5d0/q) + dj(:)*(x+0.5d0/q)
            corn(:,3) = corn(:,2) * (1.d0-1.d0/i)
            corn(:,4) = corn(:,1) * (1.d0-1.d0/i)
            corn(3,:) = -hexZrad
          end if

          ! copy the BZ
          do k = 1, nfaces
            wfaces(k)%normal = bzfaces(k)%normal
            wfaces(k)%nvert  = bzfaces(k)%nvert
            do m = 1, wfaces(k)%nvert
              wfaces(k)%vert(:,m) = bzfaces(k)%vert(:,m)
            end do
          end do

          ! create sides of wedge
          nsides = 4
          sides(:,1) = cross(corn(:,2),corn(:,1))
          x = dsqrt(dot(sides(:,1),sides(:,1)))
          sides(:,1) = (1.0/x)*sides(:,1)

          sides(:,2) = cross(corn(:,3),corn(:,2))
          x = dsqrt(dot(sides(:,2),sides(:,2)))
          sides(:,2) = (1.0/x)*sides(:,2)

          sides(:,3) = cross(corn(:,4),corn(:,3))
          x = dsqrt(dot(sides(:,3),sides(:,3)))
          sides(:,3) = (1.0/x)*sides(:,3)

          sides(:,4) = cross(corn(:,1),corn(:,4))
          x = dsqrt(dot(sides(:,4),sides(:,4)))
          sides(:,4) = (1.0/x)*sides(:,4)

          call clipbz(nsides, sides, nwfaces, wfaces)
          call getwedgevol(nwfaces, wfaces, wvol)
          rayweights(n) = wvol
          totvol = totvol + wvol

        end do

      end do

    end do; end do

    if( nrays /= n ) then
      write(6,*) 'Did not generate all expected rays?'
      write(6,'(a,i6,a,i6)') '  n = ', n, ', nrays = ', nrays
      stop
    end if

    write(6,'(a,1f20.15)') ' sum of weights = ', totvol
    write(6,'(a,i5)') ' num. of rays = ', nrays

    do i = 1, nwfaces
      deallocate(wfaces(i)%vert)
    end do
    deallocate(wfaces)

!EOC
  end subroutine genhexrays

!BOP
!!IROUTINE: genboxrays
!!INTERFACE:
  subroutine genboxrays(nrays,raylist,rayweights,nx,ny,nz,nfaces,bzfaces)
!!DESCRIPTION:
! generates rays for non-hexagonal cases

!!DO_NOT_PRINT
    implicit none
!!DO_NOT_PRINT

!!ARGUMENTS:
    integer, intent(out) :: nrays
    real(kind=dp), pointer, intent(out) :: raylist(:,:)
    real(kind=dp), pointer, intent(out) :: rayweights(:)
    integer, intent(in) :: nx,ny,nz
    integer, intent(in) :: nfaces
    type(face), pointer, intent(in) :: bzfaces(:)
!!PARAMETERS:
    integer, parameter :: maxvert = 30
!EOP
!
!BOC
    integer :: nwfaces, nsides
    type(face), pointer :: wfaces(:)
    real(kind=dp) :: sides(3,4), corn(3,4)

    integer :: s, i, j, n, ni, nj, k, m
    real(kind=dp) :: boxRx, boxRy, boxRz, x
    real(kind=dp) :: topl(3), di(3), dj(3), wvol, totvol

    ! Find circumscribing box
    boxRx = 0.d0; boxRy = 0.d0; boxRz = 0.d0
    do i = 1, nfaces
    do j = 1, bzfaces(i)%nvert
      if( abs(bzfaces(i)%vert(1,j)) > boxRx ) boxRx = abs(bzfaces(i)%vert(1,j))
      if( abs(bzfaces(i)%vert(2,j)) > boxRy ) boxRy = abs(bzfaces(i)%vert(2,j))
      if( abs(bzfaces(i)%vert(3,j)) > boxRz ) boxRz = abs(bzfaces(i)%vert(3,j))
    end do; end do

    write(6,'(a,i3,i3,i3)') ' box divisions = ', nx, ny, nz
    write(6,'(a,3f20.15)') ' box radii = ', boxRx, boxRy, boxRz

    ! wfaces = wedge faces
    allocate(wfaces(nfaces))
    do i = 1, nfaces
      allocate(wfaces(i)%vert(3,maxvert))
      wfaces(i)%nvert = bzfaces(i)%nvert
      wfaces(i)%normal = bzfaces(i)%normal
    end do
    nwfaces = nfaces

    totvol = 0.d0

    ! grid the box and generate rays
    nrays = 2*nx*ny+2*ny*nz+2*nx*nz
    allocate(raylist(3,nrays))
    allocate(rayweights(nrays))

    n = 0
    do s = 1, 6

      topl(:) = 0; di(:) = 0; dj(:) = 0
      select case(s)
        case(1)
          di(2) = 2.d0*boxRy/ny; dj(3) = 2.d0*boxRz/nz
          topl(:) = (-ny/2.d0+0.5d0)*di(:) + (-nz/2.d0+0.5d0)*dj(:)
          topl(1) = boxRx; ni = ny; nj = nz
        case(2)
          di(2) = -2.d0*boxRy/ny; dj(3) = 2.d0*boxRz/nz
          topl(:) = (-ny/2.d0+0.5d0)*di(:) + (-nz/2.d0+0.5d0)*dj(:)
          topl(1) = -boxRx; ni = ny; nj = nz
        case(3)
          di(1) = -2.d0*boxRx/nx; dj(3) = 2.d0*boxRz/nz
          topl(:) = (-nx/2.d0+0.5d0)*di(:) + (-nz/2.d0+0.5d0)*dj(:)
          topl(2) = boxRy; ni = nx; nj = nz
        case(4)
          di(1) = 2.d0*boxRx/nx; dj(3) = 2.d0*boxRz/nz
          topl(:) = (-nx/2.d0+0.5d0)*di(:) + (-nz/2.d0+0.5d0)*dj(:)
          topl(2) = -boxRy; ni = nx; nj = nz
        case(5)
          di(1) = 2.d0*boxRx/nx; dj(2) = 2.d0*boxRy/ny
          topl(:) = (-nx/2.d0+0.5d0)*di(:) + (-ny/2.d0+0.5d0)*dj(:)
          topl(3) = boxRz; ni = nx; nj = ny
        case(6)
          di(1) = 2.d0*boxRx/nx; dj(2) = -2.d0*boxRy/ny
          topl(:) = (-nx/2.d0+0.5d0)*di(:) + (-ny/2.d0+0.5d0)*dj(:)
          topl(3) = -boxRz; ni = nx; nj = ny
      end select

      do i = 1, ni
      do j = 1, nj

        n = n+1
        raylist(:,n) = topl(:) + di(:)*(i-1) + dj(:)*(j-1)
        corn(:,1) = topl(:) + di(:)*(i-0.5d0) + dj(:)*(j-0.5d0)
        corn(:,2) = topl(:) + di(:)*(i-1.5d0) + dj(:)*(j-0.5d0)
        corn(:,3) = topl(:) + di(:)*(i-1.5d0) + dj(:)*(j-1.5d0)
        corn(:,4) = topl(:) + di(:)*(i-0.5d0) + dj(:)*(j-1.5d0)
        call clipray(raylist(:,n),nfaces,bzfaces)

        ! copy the BZ
        do k = 1, nfaces
          wfaces(k)%normal = bzfaces(k)%normal
          wfaces(k)%nvert  = bzfaces(k)%nvert
          do m = 1, wfaces(k)%nvert
            wfaces(k)%vert(:,m) = bzfaces(k)%vert(:,m)
          end do
        end do

        ! create sides of wedge
        nsides = 4
        sides(:,1) = cross(corn(:,2),corn(:,1))
        x = sqrt(dot(sides(:,1),sides(:,1)))
        sides(:,1) = (1.0/x)*sides(:,1)

        sides(:,2) = cross(corn(:,3),corn(:,2))
        x = sqrt(dot(sides(:,2),sides(:,2)))
        sides(:,2) = (1.0/x)*sides(:,2)

        sides(:,3) = cross(corn(:,4),corn(:,3))
        x = sqrt(dot(sides(:,3),sides(:,3)))
        sides(:,3) = (1.0/x)*sides(:,3)

        sides(:,4) = cross(corn(:,1),corn(:,4))
        x = sqrt(dot(sides(:,4),sides(:,4)))
        sides(:,4) = (1.0/x)*sides(:,4)

        call clipbz(nsides, sides, nwfaces, wfaces)
        call getwedgevol(nwfaces, wfaces, wvol)
        rayweights(n) = wvol
        totvol = totvol + wvol

      end do; end do

    end do


    if( nrays /= n ) then
      write(6,*) 'Did not generate all expected rays?'
      stop
    end if

    write(6,'(a,1f20.15)') ' sum of weights = ', totvol
    write(6,'(a,i5)') ' num. of rays = ', nrays

    do i = 1, nwfaces
      deallocate(wfaces(i)%vert)
    end do
    deallocate(wfaces)

!EOC
  end subroutine genboxrays

!BOP
!!IROUTINE: clipray
!!INTERFACE:
  subroutine clipray(ray, nfaces, bzfaces)

!!DO_NOT_PRINT
    implicit none
!!DO_NOT_PRINT

!!ARGUMENTS:
    real(kind=dp), intent(inout) :: ray(3)
    integer, intent(in) :: nfaces
    type(face), pointer, intent(in) :: bzfaces(:)
!EOP
!
!BOC
    integer :: i
    real(kind=dp) :: out(3)
    real(kind=dp) :: dp1, n2
    do i = 1, nfaces
      n2 = dot(bzfaces(i)%normal,bzfaces(i)%normal)
      dp1 = dot(ray,bzfaces(i)%normal)
      if( dp1 > n2 ) then
        out(:) = (n2/dp1)*ray(:)
        ray(:) = out(:)
      end if
    end do
!EOC
  end subroutine clipray

!BOP
!!IROUTINE: clipbz
!!INTERFACE:
  subroutine clipbz(nsides, sides, nfaces, bzfaces)

!!DO_NOT_PRINT
    implicit none
!!DO_NOT_PRINT

!!ARGUMENTS:
    integer, intent(in) :: nsides
    real(kind=dp), intent(in) :: sides(3,nsides)
    integer :: nfaces
    type(face), pointer :: bzfaces(:)
!EOP
!
!BOC
    real(kind=dp) :: d
    integer :: i, j

    d = 0.0d0
a:  do i = 1, nfaces

      ! clip face by sides
      ! if face ceases to exist, skip ahead
      do j = 1, nsides
        call clipface(bzfaces(i)%nvert, bzfaces(i)%vert, sides(:,j), d)
        if( bzfaces(i)%nvert == 0 ) cycle a
      end do

    end do a

!EOC
  end subroutine clipbz

!BOP
!!IROUTINE: getwedgevol
!!INTERFACE:
  subroutine getwedgevol(nfaces, faces, wvol)

!!DO_NOT_PRINT
    implicit none
!!DO_NOT_PRINT

!!ARGUMENTS:
    integer :: nfaces
    type(face), pointer :: faces(:)
    real(kind=dp) :: wvol
!EOP
!
!BOC
    integer :: i, j
    real(kind=dp) :: base, u(3), v(3), f(3), area

    wvol = 0.d0
    do i = 1, nfaces
      if( faces(i)%nvert == 0 ) cycle
      base = sqrt(dot(faces(i)%normal,faces(i)%normal))

      area = 0.d0
      do j = 2, faces(i)%nvert-1
        u(:) = faces(i)%vert(:,j)-faces(i)%vert(:,1)
        v(:) = faces(i)%vert(:,j+1)-faces(i)%vert(:,1)
        f = cross(u,v)
        area = area + sqrt(dot(f,f))/2.d0
      end do

      wvol = wvol + area*base/3.d0
    end do

!EOC
  end subroutine getwedgevol

!BOP
!!IROUTINE: dumprays
!!INTERFACE:
  subroutine dumprays(nrays, raylist, rayweights)

!!DO_NOT_PRINT
    implicit none
!!DO_NOT_PRINT

!!ARGUMENTS:
    integer, intent(in) :: nrays
    real(kind=dp), intent(in) :: raylist(3,nrays)
    real(kind=dp), intent(in) :: rayweights(nrays)
!!REMARKS:
! fortran unit 199 is used for writing
!EOP
!
!BOC
    integer :: i

    open(unit=199,file='raylist.out.'//gGenName(),status='replace',action='write')
    do i = 1, nrays
      write(199,'(4f22.15)') raylist(1:3,i), rayweights(i)
    end do
    close(unit=199)

!EOC
  end subroutine dumprays

! Bloch Spectral Properties A(k,E) & Lloyd's integrated D.o.S. N(E)
! -----------------------------------------------------------------------

!BOP
!!IROUTINE: addlloydcorr
!!INTERFACE:
  subroutine addlloydcorr( &
      natom,nsublat,iorig,itype,komp,numbsub,atcon, &
      ispin,nspin,energy,efSCF,kkrsz,lmax,nrelv,cotdl,almat,&
      tab,tcpa,tau00,lloydms,&
      green00, rytodu, volume, lloydform )
!!DESCRIPTION:
! returns the lloyd's formula for integrated density of states but
! w/o the free electron density contribution. the input {\tt lloydms}
! is 1/bzvol*Integral\{$log(1 - G_0*t_{cpa}),$ {\tt over b.z.}\}

!!DO_NOT_PRINT
    implicit none
!!DO_NOT_PRINT

!!ARGUMENTS:
    integer, intent(in) :: natom,nsublat
    integer, intent(in) :: iorig(nsublat)
    integer, intent(in) :: itype(:)
    integer, intent(in) :: komp(nsublat)
    integer, intent(in) :: numbsub(nsublat)
    integer, intent(in) :: ispin,nspin
    integer, intent(in) :: kkrsz
    integer, intent(in) :: lmax, nrelv
    real(kind=dp), intent(in) :: atcon(:,:)  ! (ipcomp,nsublat)
    real(kind=dp), intent(in) :: efSCF
    complex(kind=cd), intent(in) :: energy
    complex(kind=cd), intent(in) :: cotdl(:,:,:)    ! (iplmax+1,ipsublat,ipcomp)
    complex(kind=cd), intent(in) :: almat(:,:,:)    ! (iplmax+1,ipcomp,ipsublat)
    complex(kind=cd), intent(in) :: tab(:,:,:,:)   ! (ipkkr,ipkkr,ipcomp,nsublat)
    complex(kind=cd), intent(in) :: tcpa(:,:,:)  ! (ipkkr,ipkkr,natom)
    complex(kind=cd), intent(in) :: tau00(:,:,:)  ! (ipkkr,ipkkr,nsublat)
    complex(kind=cd), intent(in) :: lloydms
    complex(kind=cd), intent(in) :: green00(:,:,:)  ! (ipkkr,ipkkr,nsublat)
    real(kind=dp), intent(in) :: rytodu, volume
    real(kind=dp), intent(out) :: lloydform
!!REMARKS:
! fortran units from 110 to 113 are used for writing
!EOP
!
!BOC

    complex(kind=cd) :: dinv(size(green00,1),size(green00,1))
    integer :: piv(size(dinv,1)), info, i, j, k, l, n, m, nkomp

    real(kind=dp) :: dE
    real(kind=dp), save :: lasten(2),lastllform(2)
    complex(kind=cd), save :: lastlltau(2),lastllcorr(2)
    complex(kind=cd), save :: lastllfree(2),lastllsing(2)
    complex(kind=cd), save, allocatable :: lastllsingabl(:,:,:,:)   ! (0:lmax,ipcomp,ipsublat,2)
    complex(kind=cd) :: clloydtau, clloydcorr, clloydfree, clloydsing
!    complex(kind=cd), save :: lastllcorrab(ipcomp,ipsublat,2)
!    complex(kind=cd) :: clloydcorrab(ipcomp,ipsublat)
!    complex(kind=cd) :: clloydsingabl(0:lmax,ipcomp,ipsublat)
    complex(kind=cd), save, allocatable :: lastllcorrab(:,:,:)
    complex(kind=cd), save, allocatable :: clloydcorrab(:,:)     ! no need in "save"
    complex(kind=cd), save, allocatable :: clloydsingabl(:,:,:)  ! no need in "save"
    complex(kind=cd) :: z, dinvdet
    complex(kind=cd) :: logdet
    complex(kind=cd), parameter :: im = dcmplx(0.d0,1.d0)

!    integer, parameter :: iTOl(16) = (/0,1,1,1,2,2,2,2,2,3,3,3,3,3,3,3/)
!    integer, parameter :: lTOi(0:3) = (/1,2,5,10/)

    logical, parameter :: printderv  = .false.
    logical, parameter :: phasetrack = .true.
    real(kind=dp) :: period, x
    real(kind=dp), parameter :: tol = 10d-06

    integer,save :: llFi = 110, llFi_up, llFi_dn
    integer,save :: doFi = 110, doFi_up, doFi_dn

!    complex*16 catanf, gam
!    catanf(gam)=.5d+00*im*cdlog((im+gam)/(im-gam))

    logical :: isordered
    logical, save :: firstcall = .true.

    nkomp = maxval(komp(1:nsublat))
    if ( firstcall ) then
     lasten   = 0; lastllform = 0
     lastlltau = 0; lastllcorr = 0
     lastllfree = 0; lastllsing = 0
     if ( allocated(lastllcorrab) ) deallocate(lastllcorrab)
     allocate(lastllcorrab(nkomp,nsublat,2))
     if ( allocated(clloydcorrab) ) deallocate(clloydcorrab)
     allocate(clloydcorrab(nkomp,nsublat))
     if ( allocated(clloydsingabl) ) deallocate(clloydsingabl)
     allocate(clloydsingabl(0:lmax,nkomp,nsublat))
     lastllcorrab = 0 ;
     if ( allocated(lastllsingabl) ) deallocate(lastllsingabl)
     allocate(lastllsingabl(0:lmax,1:nkomp,1:nsublat,2))
     lastllsingabl = 0
    else
      if ( nsublat == 0 ) then
        if ( allocated(lastllsingabl) ) deallocate(lastllsingabl)
        if ( allocated(lastllcorrab) ) deallocate(lastllcorrab)
        if ( allocated(clloydcorrab) ) deallocate(clloydcorrab)
        if ( allocated(clloydsingabl) ) deallocate(clloydsingabl)
        firstcall = .true.
        return
      end if
      if (   size(lastllsingabl,1) .gt. lmax+1                         &
     &  .or. size(lastllsingabl,2) .gt. nkomp                          &
     &  .or. size(lastllsingabl,3) .gt. nsublat                        &
     &  .or. size(lastllsingabl,4) .gt. 2 ) then
         stop ' addlloydcorr ERROR :: improper memory allocation'
      end if
     if ( size(lastllcorrab,2)<nsublat .or.                            &
     &    size(lastllcorrab,1)<nkomp ) then
       if ( size(lastllcorrab,2)<nsublat ) then
        write(6,*) ' nsublat=',nsublat,' > size(lastllcorrab,2)=',     &
     &                                     size(lastllcorrab,2)
       end if
       if ( size(lastllcorrab,1)<nkomp ) then
        write(6,*) ' ncomp=',nkomp,                                    &
     &                 ' > size(lastllcorrab,1)=',size(lastllcorrab,1)
       end if
       stop ' ERROR :: size of lastllcorrab is too small'
     end if
    end if

    ! What is the distance between adjacent branches?

    period = 0.d0
a:  do n = 1, 1000
      do i = 1, nsublat
      do j = 1, komp(i)
        x = mod(atcon(j,i)*n,1.d0)
        if( x > 0.5d0 ) x = x - 1.d0
        if( x < -tol .or. tol < x ) then
          cycle a
        end if
      end do; end do
      period = 2.d0/n
      exit a
    end do a

    ! take lloyd back-scattering term modulo two electrons
    ! this term is integral( log|| 1 - t g || over BZ )

    clloydtau = -lloydms/pi
    if(phasetrack) call makenearest(clloydtau,lastlltau(ispin),2.d0)

    ! compute CPA correction: sum{ c_i log || I + (tab-tcpa)G00 || }
    ! see Akai & Dederichs in PRB 47, 8739

    isordered = .true.
    do n = 1, nsublat
      if( komp(n) > 1 ) then
          isordered = .false.
          exit
      end if
    end do

    clloydcorr = dcmplx(0.d0,0.d0)
    if( .not. isordered ) then

      do n = 1, nsublat
      do m = 1, komp(n)

        dinv(:,:) = dcmplx(0.d0,0.d0)
        do j = 1, kkrsz
          z = tab(j,j,m,n) - tcpa(j,j,iorig(n))
          dinv(j,:) = z * green00(j,:,n)
          dinv(j,j) = -dinv(j,j) + dcmplx(1.d0,0.d0)
        end do

       call zgetrf(kkrsz,kkrsz,dinv(1,1),size(dinv,1),piv,info)
       if( info /= 0 ) call fstop("getlloyd :: failed to factor D^-1")

!TO DO: DELETE after testing
!       dinvdet = dcmplx(1.d0,0.d0)
!       do i = 1, kkrsz
!         dinvdet = dinv(i,i)*dinvdet
!         if( piv(i) /= i ) dinvdet = -dinvdet
!       end do
!       clloydcorrab(m,n) = -cdlog(dinvdet)/pi

       logdet = dcmplx(0.d0,0.d0)
       do i = 1, kkrsz
         logdet = logdet + cdlog(dinv(i,i))
         if( piv(i) /= i ) logdet = logdet + dcmplx(0.d0,pi)
       end do
       clloydcorrab(m,n) = -logdet/pi

       if(phasetrack) call makenearest(clloydcorrab(m,n),lastllcorrab(m,n,ispin),2.d0)
       lastllcorrab(m,n,ispin) = clloydcorrab(m,n)

       clloydcorr = clloydcorr + atcon(m,n)*numbsub(n)*clloydcorrab(m,n)

      end do; end do

    end if

    ! free electron term

    clloydfree = im*volume*energy**1.5d0/(6.d0*pi**2)

    ! single-scattering term
    ! using alpha matrix and NOT single-site t-matrix
    ! this avoids phases from sqrt(E) and sin(dl)
    ! see Zeller, Journ. of Physics CM V. 16 p6453

    clloydsing = dcmplx(0.d0,0.d0)
    do j = 1, nsublat
    do k = 1, komp(j)
    do l = 0, lmax

      clloydsingabl(l,k,j) = cdlog(almat(l+1,k,j))/pi

      if(phasetrack) call makenearest(clloydsingabl(l,k,j),lastllsingabl(l,k,j,ispin),2.d0)
      lastllsingabl(l,k,j,ispin) = clloydsingabl(l,k,j)

      clloydsing = clloydsing + numbsub(j)*atcon(k,j)* &
                          (2.d0*l+1.d0)*clloydsingabl(l,k,j)

    end do; end do; end do

    ! sum all terms together
    lloydform = dimag(clloydtau  + clloydcorr +  clloydfree + clloydsing)

    ! print each term to file


    if( firstcall ) then

      if( nspin == 1 ) then
        llfi = 110
        open(unit=110,file='intdos.dat.'//gGenName(),status='replace',action='write')
        write(110,'(a,f8.5)') "# Lloyd's formula for N(E) %",2.d0*period
      else
        llfi_up = 110
        open(unit=110,file='intdos_up.dat.'//gGenName(),status='replace',action='write')
        write(110,'(a,f8.5)') "# Lloyd's formula for UP N(E) %",period
        llfi_dn = 112
        open(unit=112,file='intdos_dn.dat.'//gGenName(),status='replace',action='write')
        write(112,'(a,f8.5)') "# Lloyd's formula for DN N(E) %",period
      end if
      do i = 1, nspin
        if( i == 2 ) llfi = llfi_dn
        write(llfi,'(a)') "#  (i.e. only remainder is physically significant)"
        write(llfi,'(a)') '#'
        write(llfi,'(a)') '# Warning: You must be careful to '
        write(llfi,'(a)') '# (1) converge w/ respect to no. of rays'
        write(llfi,'(a)') '# (2) converge w/ respect to pts along ray'
        write(llfi,'(a)') '# (3) extrapolate back to real axis'
        write(llfi,'(a)') '#'
        write(llfi,'(a,f10.7)') '# Off real-axis by Im E = ', dimag(energy)
        if( .not. fixedrays ) then
          if( .not. adaptive ) then
            write(llfi,'(a,i1)') '# Using ray recursion level of ', nrecursion
          else
            write(llfi,'(a)') '# Using adaptive recursion'
          end if
        else
          write(llfi,'(a,3i5)') '# Using box grid div ', nx, ny, nz
        end if
        write(llfi,'(a,i4,a)') '# Using ',nraysubint,' points/du along ray'
        write(llfi,'(a)') '#'
        write(llfi,'(6a)') '#      E-Ef     ','     N(E)      ',        &
       &                    '    direct     ','        back   ',        &
       &                    '    disorder   ','        free   '
        write(dofi,'(a,i4,a)') '# du = 2pi/a '
      end do
      firstcall = .false.

      if( printderv ) then

        if( nspin == 1 ) then
          dofi = 111
          open(unit=111,file='altdos.dat.'//gGenName(),status='replace',action='write')
        else
          dofi_up = 111
          open(unit=111,file='altdos_up.dat.'//gGenName(),status='replace',action='write')
          dofi_dn = 113
          open(unit=113,file='altdos_dn.dat.'//gGenName(),status='replace',action='write')
        end if

        do i = 1, nspin
          if(i == 2) dofi = dofi_dn
          write(dofi,'(a)') "# derivative of Lloyd's formula for N(E)"
          write(dofi,'(a,f10.7)') '# Off real-axis by Im E = ', dimag(energy)
          if( .not. fixedrays ) then
            if( .not. adaptive ) then
              write(dofi,'(a,i1)') '# Using ray recursion level of ', nrecursion
            else
              write(dofi,'(a)') '# Using adaptive recursion'
            end if
          else
            write(dofi,'(a,3i5)') '# Using box grid div ', nx, ny, nz
          end if
          write(dofi,'(a,i4,a)') '# Using ',nraysubint,' points/du along ray'
          write(dofi,'(a,i4,a)') '# du = 2pi/a '
          write(dofi,'(a)') '#'
          write(dofi,'(6a)') '#      E-Ef     ','     N(E)      ',      &
       &                      '    direct     ','        back   ',      &
       &                      '    disorder   ','        free   '
        end do

      end if

    end if

    if( nspin == 2 ) then
      if(ispin == 1) then
        llfi = llfi_up
        dofi = dofi_up
      else
        llfi = llfi_dn
        dofi = dofi_dn
      end if
    end if

    dE = real(energy - lasten(ispin),dp)
    if(nspin == 1) then
      write(llfi,'(6g15.5)') real(energy,dp)-efSCF, 2.d0*lloydform, &
        2.d0*dimag(clloydsing),  2.d0*dimag(clloydtau), 2.d0*dimag(clloydcorr), 2.d0*dimag(clloydfree)
!       write(llfi,'(3f10.5)') real(energy)-efSCF, 2.d0*(clloydtau  + clloydcorr +  clloydfree + clloydsing)
    else
      write(llfi,'(6g15.5)') real(energy,dp)-efSCF, lloydform, &
        dimag(clloydsing),  dimag(clloydtau), dimag(clloydcorr), dimag(clloydfree)
    end if

    if( printderv .and. dE > 0.d0 ) then
      if(nspin == 1) then
        write(dofi,'(6g15.5)') real(energy,dp)-efSCF, 2.d0*(lloydform-lastllform(ispin))/dE, &
          2.d0*dimag(clloydsing-lastllsing(ispin))/dE,  &
          2.d0*dimag(clloydtau-lastlltau(ispin))/dE, &
          2.d0*dimag(clloydcorr-lastllcorr(ispin))/dE, &
          2.d0*dimag(clloydfree-lastllfree(ispin))/dE
      else
        write(dofi,'(6g15.5)') real(energy,dp)-efSCF, (lloydform-lastllform(ispin))/dE, &
          dimag(clloydsing-lastllsing(ispin))/dE,  &
          dimag(clloydtau-lastlltau(ispin))/dE, &
          dimag(clloydcorr-lastllcorr(ispin))/dE, &
          dimag(clloydfree-lastllfree(ispin))/dE
      end if
    end if

    lastlltau(ispin)  = clloydtau
    lastllcorr(ispin) = clloydcorr
    lastllfree(ispin) = clloydfree
    lastllsing(ispin) = clloydsing
    lastllform(ispin) = lloydform
    lasten(ispin) = real(energy,dp)

!EOC
  end subroutine addlloydcorr

!BOP
!!IROUTINE: getblochspecfn
!!INTERFACE:
  subroutine getblochspecfn( &
      natom,nsublat,iorig,itype,komp,numbsub,atcon, &
      energy,kkrsz,tab,tcpa,tau00,pzzbl,pzj,  &
      startk, endk, rayp, kpoint, bsf, bsfNL, akedis, rytodu, &
      lmax,aij,mapstr,mappnt,mapij, &
      powe,edu,pdu,rsnij,ndimrij,np2r,ndimnp, &
      numbrs,naij,dqint,ndimdqr,hplnm,ndimrhp,ndimlhp,irecdlm,d00,eoeta, &
      eta,ksvecs,nksvecs,conr,nop,rot,dop,if0)
!!DESCRIPTION:
! Bloch Spectral Density Fn A(k,E)
!
! See PRB {\bf 21}, 3222 by Faulkner \& Stocks

!!DO_NOT_PRINT
    implicit none
!!DO_NOT_PRINT

!!ARGUMENTS:
! NOT full list
    integer, intent(in) :: natom,nsublat
    integer, intent(in) :: iorig(nsublat)
    integer, intent(in) :: komp(nsublat)
    integer, intent(in) :: numbsub(nsublat)
    real(kind=dp), intent(in) :: atcon(:,:)  ! (ipcomp,nsublat)

    integer, intent(in) :: kkrsz,lmax
    complex(kind=cd), intent(in) :: energy
    complex(kind=cd), intent(in) :: tab(:,:,:,:) ! ipkkr,ipkkr,ipcomp,nsublat)
    complex(kind=cd)             :: tcpa(:,:,:)  ! (ipkkr,ipkkr,natom)
    complex(kind=cd), intent(in) :: tau00(:,:,:)  ! (ipkkr,ipkkr,nsublat)
    complex(kind=cd), intent(in) :: pzzbl(:,:,:,:,:)  ! (ipkkr,ipkkr,ipcomp,ipcomp,nsublat)
    complex(kind=cd), intent(in) :: pzj(:,:,:,:)      ! (ipkkr,ipkkr,ipcomp,nsublat)

    real(8), intent(in) :: startk(3), endk(3)  ! not used
    complex(8), intent(in) :: rayp
    real(8), intent(in) :: kpoint(3)
    real(8), intent(out) :: bsf
    real(8), intent(out) :: bsfNL(0:,:)  ! not used ;  (0:iplmax,nsublat)
    real(kind=dp), intent(out) :: akedis

    real(kind=dp), intent(in) :: rytodu
    complex(8), intent(in) :: edu,pdu

!! structure constant parameters

    integer, intent(in) ::  ndimrij
    integer, intent(in) ::  ndimnp,ndimdqr,ndimrhp,ndimlhp
    integer, intent(in) ::  mapij(2,*),naij,np2r(ndimnp,*)
    integer, intent(in) :: mapstr(natom,natom),mappnt(natom,natom)
    integer, intent(in) :: numbrs(*),itype(natom),irecdlm
    real(8), intent(in) :: aij(3,*),rsnij(ndimrij,4)
    real(8), intent(in) :: eta
    complex(8), intent(in) :: conr(*),powe(*),dqint(ndimdqr,*)
    complex(8), intent(in) :: hplnm(ndimrhp,ndimlhp)
    complex(8), intent(in) :: d00,eoeta

    integer, intent(in) :: nksvecs
    real*8 ksvecs(nksvecs,3)

!! symmetry related input

!! rotation operations are sent in case we wish to make
!! symmetry reductions in the plotting. but this routine is
!! simpler than that and does not use them.

    integer nop
    real*8 rot(49,3,3)
    complex*16 dop(kkrsz,kkrsz,nop)
    integer if0(48,natom)

!EOP

    ! Plot A(k,E) -- the no. of states at energy E after projecting
    ! onto plane waves k + Kn where Kn is a reciprocal lattice vec.

!BOC

    complex(kind=cd), parameter :: cone=(1_dp,0_dp),czero=(0_dp,0_dp)

!    real*8 bsfcubic(kkrsz,nsublat)
!    complex(8) greenks(kkrsz*natom,kkrsz*natom)

    complex(kind=cd) :: dab(size(tab,1),size(tab,2),size(tab,3),size(tab,4))
    complex(kind=cd) :: fc(size(tau00,1),size(tau00,2),size(tau00,3))
    complex(kind=cd) :: fcc(size(tau00,1),size(tau00,2),size(tau00,3))
    complex(kind=cd) :: tauk(size(tcpa,1),size(tcpa,2),size(tcpa,3))
    complex(kind=cd) :: greenk(size(tcpa,1),size(tcpa,2),size(tcpa,3))
    complex(kind=cd) :: divfac
    real(kind=dp) :: ake, del(0:lmax,nsublat), thisk(3)

    integer :: npoles = 0
    complex(kind=cd) :: freepoles(nksvecs)
    real(kind=dp) :: dfsign

!DEBUG    complex(kind=cd) :: fpr(2)

    complex(kind=cd) :: z, w, v
    complex(kind=cd) :: wmat(size(tcpa,1),size(tcpa,2))
    complex(kind=cd) :: wmat2(size(tcpa,1),size(tcpa,2))
    complex(kind=cd) :: work(kkrsz)
    integer :: i, j, k, l, n, m
    integer :: piv(size(dab,1)), info

!DEBUG    integer, save :: nok = 0
!DEBUG    real*8 kslatt(3,3), bzvol2

    logical :: isordered
    real*8 totdel

    integer :: nblock=0
    integer :: st_env
    integer, save :: idebug=-1

!DEBUG    complex(kind=cd) :: u(2)
!    real(kind=dp), parameter :: irt2 = 1.d0/sqrt(2.d0)
!    real(kind=dp), parameter :: fepthr = 1.0d-02
!    integer, parameter :: kkr_cubic=16
!    complex(kind=cd), save :: U_cubic(kkr_cubic,kkr_cubic)
!    logical, save :: firstcall = .true.
!    if( firstcall ) then
!
!    ! fill out unitary matrix to transform from spherical to cubic harmonics
!    ! valid only up to lmax = 3
!
!
!      if( lmax > 3 ) call fstop("U_cubic must be enlarged for lmax > 3")
!      ! basis ordering: s, px, py, pz, dxy, dxz, dyz, dx2-y2, dz2,
!      !   fz3, fxz2, fyz2, fxyz, fz(x2-y2), fx(x2-3y2), fy(3x2-y2)
!
!      U_cubic = dcmplx(0.d0,0.d0)
!      U_cubic(1,1) = dcmplx(1.d0,0.d0)
!
!      U_cubic(2,2) = dcmplx(-irt2,0.d0)
!      U_cubic(4,2) = dcmplx(irt2,0.d0)
!      U_cubic(2,3) = dcmplx(0.d0,-irt2)
!      U_cubic(4,3) = dcmplx(0.d0,-irt2)
!      U_cubic(3,4) = dcmplx(1.d0,0.d0)
!
!      U_cubic(5,5) = dcmplx(0.d0,irt2)
!      U_cubic(9,5) = dcmplx(0.d0,irt2)
!      U_cubic(6,6) = dcmplx(irt2,0.d0)
!      U_cubic(8,6) = dcmplx(irt2,0.d0)
!      U_cubic(6,7) = dcmplx(0.d0,-irt2)
!      U_cubic(8,7) = dcmplx(0.d0,irt2)
!      U_cubic(5,8) = dcmplx(-irt2,0.d0)
!      U_cubic(9,8) = dcmplx(irt2,0.d0)
!      U_cubic(7,9) = dcmplx(1.d0,0.d0)
!
!      U_cubic(13,10) = dcmplx(1.d0,0.d0)
!      U_cubic(12,11) = dcmplx(-irt2,0.d0)
!      U_cubic(14,11) = dcmplx(irt2,0.d0)
!      U_cubic(12,12) = dcmplx(0.d0,-irt2)
!      U_cubic(14,12) = dcmplx(0.d0,-irt2)
!      U_cubic(11,13) = dcmplx(0.d0,irt2)
!      U_cubic(15,13) = dcmplx(0.d0,-irt2)
!      U_cubic(11,14) = dcmplx(irt2,0.d0)
!      U_cubic(15,14) = dcmplx(irt2,0.d0)
!      U_cubic(10,15) = dcmplx(-irt2,0.d0)
!      U_cubic(16,15) = dcmplx(irt2,0.d0)
!      U_cubic(10,16) = dcmplx(0.d0,-irt2)
!      U_cubic(16,16) = dcmplx(0.d0,-irt2)
!
!
!      firstcall = .false.
!
!      do n = 1, natom
!        do i = 1, kkrsz
!          write(6,*) tcpa(i,i,n)
!        end do
!        write(6,*) '^^^',n,'^^^'
!      end do
!      stop
!
!      do n = 1, nsublat
!      do i = 1, nop
!        write(6,'(i2,a,i2)') iorig(n),' --> ',if0(i,iorig(n))
!        write(6,'(3f6.2)') rot(i,1,:)
!        write(6,'(3f6.2)') rot(i,2,:)
!        write(6,'(3f6.2)') rot(i,3,:)
!        write(6,*) ''
!      end do; end do
!      stop
!
!      ! for testing purposes
!      ! where are the free electron poles?
!      npoles = 0
!      do i = 1, nksvecs
!
!        avec(:) = endk(:)-startk(:)
!        bvec(:) = startk(:)+ksvecs(i,:)
!
!        call getquadroots( dcmplx(1.d0,0.d0)*dot(avec,avec), &
!          dcmplx(2.d0,0.d0)*dot(avec,bvec), &
!          dcmplx(1.d0,0.d0)*(dot(bvec,bvec)-edu), &
!          u(2), u(1) )
!
!        do j = 1, 2
!        if( (-fepthr < dreal(u(j)) .and. dreal(u(j)) < 1.d0+fepthr) .and. &
!            (-fepthr < dimag(u(j)) .and. dimag(u(j)) < fepthr) ) then
!
!            npoles = npoles + 1
!            freepoles(npoles) = u(j)
!            write(6,*) 'fep = ', u(j)*dsqrt(dot(endk-startk,endk-startk))
!            write(6,*) 'i = , ', i, ' j = ', j
!            write(6,*) 'dk = ', ksvecs(i,:)
!
!        end if; end do
!
!      !end do
!
!    end if

!DEBUG    bzvol2 = dabs(dot(cross(kslatt(:,1),kslatt(:,2)),kslatt(:,3)))

    ! Ignore extra calculations for an ordered crystal

    isordered = .true.
    do n = 1, nsublat
      if( komp(n) > 1 ) then
        isordered = .false.
        exit
      end if
    end do

    ! Construct all D = [I + tau00(mab-mcpa)]^(-1)

    if( .not. isordered ) then

     if ( idebug<0 ) then
      call gEnvVar('ENV_EVALTAU',.true.,st_env)
      if ( st_env>=0 ) then
       idebug = min(st_env,1)
      else
!       idebug = 1
       idebug = 0
      end if
     end if

      do n = 1, nsublat
       dab(:,:,:,n) = czero
       if ( komp(n) > 1 ) then
        do m = 1, komp(n)

         wmat(1:kkrsz,1:kkrsz) = tab(1:kkrsz,1:kkrsz,m,n)
         call invmatr(wmat,kkrsz,1,1,nblock,iprint,istop)
         wmat2(1:kkrsz,1:kkrsz) = tcpa(1:kkrsz,1:kkrsz,iorig(n))
         call invmatr(wmat2,kkrsz,1,1,nblock,iprint,istop)
         wmat(1:kkrsz,1:kkrsz)=wmat(1:kkrsz,1:kkrsz)-wmat2(1:kkrsz,1:kkrsz)

        call zgemm('N','N',kkrsz,kkrsz,kkrsz,cone,tau00(1,1,n)          &
     &,                       kkrsz,wmat(1,1),kkrsz,czero,              &
     &                        dab(1,1,m,n),kkrsz)
        do j = 1, kkrsz
          dab(j,j,m,n) = dab(j,j,m,n) + cone
        end do

!!      dab(:,:,m,n) = dcmplx(0.d0,0.d0)
!!      do j = 1, kkrsz
!!        z = 1.d0/tab(j,j,m,n) - 1.d0/tcpa(j,j,iorig(n))
!!        dab(:,j,m,n) = z * tau00(:,j,n)
!!        dab(j,j,m,n) = dab(j,j,m,n) + dcmplx(1.d0,0.d0)
!!      end do
        if ( idebug .ne. 0 ) then
         call zgetrf(kkrsz,kkrsz,dab(1,1,m,n),size(dab,1),piv,info)
         if( info /= 0 ) call fstop("plotblochspecfn :: failed to factor D^-1")
         call zgetri(kkrsz,dab(1,1,m,n),size(dab,1),piv,work,kkrsz,info)
         if( info /= 0 ) call fstop("plotblochspecfn :: failed to invert D^-1")
        else
!       nblock = 0     ! i.e. the block size is defined in invmatr
         call invmatr(dab(1,1,m,n),                                     &
     &              kkrsz,1,                                            &
     &              1,nblock,                                           &
     &              iprint,istop)
        end if

        end do
       end if
      end do

!DELETE    end if

    ! Construct Fc  = -1/pi sum[ ci int[zi zi] Di, all comp. i ]
    ! Construct Fcc = -1/pi sum[ ci cj Dj* int[zi zj] Di, all comp. i,j ]

!DELETE    if( .not. isordered ) then

      fc = dcmplx(0.d0,0.d0)
      fcc = dcmplx(0.d0,0.d0)
      do n = 1, nsublat
       if ( komp(n)>1 ) then
        do m = 1, komp(n)

         z = dcmplx(atcon(m,n)); v = dcmplx(1.d0,0.d0)
        call zgemm('N','N',kkrsz,kkrsz,kkrsz,z,pzzbl(1,1,m,m,n),size(pzzbl,1), &
              dab(1,1,m,n),size(dab,1),v,fc(1,1,n),size(fc,1))

         do k = 1, komp(n)

          w = dcmplx(atcon(k,n)); v = dcmplx(0.d0,0.d0)
          call zgemm('N','N',kkrsz,kkrsz,kkrsz,z,pzzbl(1,1,k,m,n),size(pzzbl,1), &
                 dab(1,1,m,n),size(dab,1),czero,wmat(1,1),size(wmat,1))
          v = dcmplx(1.d0,0.d0)
          call zgemm('T','N',kkrsz,kkrsz,kkrsz,w,dab(1,1,k,n),size(dab,1), &
                 wmat(1,1),size(wmat,1),cone,fcc(1,1,n),size(fcc,1))

         end do

        end do
       else
        fcc(:,:,n) = pzzbl(:,:,1,1,n)
       end if
      end do

    else
      do n = 1, nsublat
        fcc(:,:,n) = pzzbl(:,:,1,1,n)
      end do
    end if

    ! Construct Delta = Im{ tr[(Fc-Fcc)tau00] + 1/pi sum[ci int[zi ji]] }

    akedis = 0.d0
    do n = 1, nsublat

!      if( .not. isordered ) then
      if( komp(n)>1 ) then

        wmat = fc(:,:,n) - fcc(:,:,n)

        v = dcmplx(1.d0,0.d0); w = dcmplx(0.d0,0.d0)
        call zgemm('N','N',kkrsz,kkrsz,kkrsz,v, &
               wmat(1,1),size(wmat,1),tau00(1,1,n),size(tau00,1), &
               czero,wmat2(1,1),size(wmat2,1))

      else
        wmat2 = dcmplx(0.d0,0.d0)
      end if
      do i = 1, kkrsz
        akedis = akedis + numbsub(n)*dimag(wmat2(i,i))
      end do
      do m = 1, komp(n)
        wmat2 = wmat2 + atcon(m,n)*pzj(:,:,m,n)
      end do

      del(:,n) = 0.d0
      do l = 0, lmax
      do m = 0, 2*l
        del(l,n) = del(l,n) + dimag(wmat2(l*l+m+1,l*l+m+1))
      end do; end do
      del(:,n) = numbsub(n) * del(:,n)

      ! partial contribution by cubic harmonics
!     w = dcmplx(real(numbsub(n)),0.d0)
!     z = dcmplx(0.d0,0.d0)
!     call zgemm('N','N',kkrsz,kkrsz,kkrsz,w,wmat2(1,1),ipkkr, &
!            U_cubic(1,1),size(U_cubic,1),z,wmat(1,1),ipkkr)
!     w = dcmplx(1.d0,0.d0)
!     call zgemm('C','N',kkrsz,kkrsz,kkrsz,w,U_cubic(1,1),size(U_cubic,1), &
!            wmat(1,1),ipkkr,z,wmat2(1,1),ipkkr)
!     do j = 1, kkrsz
!       bsfcubic(j,n) = dimag(wmat2(j,j))
!     end do
!
    end do

    npoles = 0

    ! calculate tau(k,E)
    ! make sure to set imag(E) = 10^-6 or so. Otherwise you'll get garbage.

    tcpa = tcpa / rytodu
    thisk(:) = kpoint(:)
    call evaltau( lmax,kkrsz,natom,aij,mapstr,mappnt,nsublat,mapij, &
      powe,edu,pdu,rsnij,ndimrij,np2r,ndimnp, &
      numbrs,naij,dqint,ndimdqr,hplnm,ndimrhp,ndimlhp,irecdlm,d00,eoeta, &
      eta,ksvecs,nksvecs,conr,tcpa,rayp,freepoles,npoles, &
      thisk, tauk, divfac, dfsign, greenk )
    tauk = tauk/divfac
    tauk = rytodu * tauk
    tcpa = rytodu * tcpa

    totdel = 0.d0
    do n = 1, nsublat
    do l = 0, lmax
        totdel = totdel + del(l,n)
    end do; end do

    ! calculate A(k,E) and return
    ake = 0.d0
    do n = 1, nsublat

      ! W = Fcc x Tau
!!      w = cone; z = czero
      wmat2 = czero
      do m = 1, natom
      if( itype(m) == n ) then

       call zgemm('N','N',kkrsz,kkrsz,kkrsz,cone,fcc(1,1,n),size(fcc,1),&
                 tauk(1,1,m),size(tauk,1),cone,wmat2(1,1),size(wmat2,1))
!!       wmat2 = wmat2 + wmat

      end if; end do

      ! A(k,E) total
       do i = 1, kkrsz
         ake = ake + dimag(wmat2(i,i))
       end do
!
! A(k,E) by contribution  ! CHECK IF PLAN TO USE (wmat? wmat2?)
     bsfNL(:,n) = 0.d0
!     do l = 0, lmax
!     do m = 0, 2*l
!        bsfNL(l,n) = bsfNL(l,n) + dimag(wmat(l*l+m+1,l*l+m+1))
!     end do; end do
!     bsfNL(l,n) = bsfNL(l,n) + del(l,n)

!     ! final A(k,E) contribution by cubic harmonics
!     w = dcmplx(1.d0,0.d0)
!     z = dcmplx(0.d0,0.d0)
!     call zgemm('N','N',kkrsz,kkrsz,kkrsz,w,wmat(1,1),ipkkr, &
!            U_cubic(1,1),size(U_cubic,1),z,wmat2(1,1),ipkkr)
!     call zgemm('C','N',kkrsz,kkrsz,kkrsz,w,U_cubic(1,1),size(U_cubic,1), &
!            wmat2(1,1),ipkkr,z,wmat(1,1),ipkkr)
!     do j = 1, kkrsz
!       bsfcubic(j,n) = bsfcubic(j,n) + dimag(wmat(j,j))
!     end do

    end do
    ake = ake + totdel
!DEBUG
    bsf = ake

!EOC
  end subroutine getblochspecfn

!BOP
!!IROUTINE: findblochstate
!!INTERFACE:
  subroutine findblochstate( &
      natom,nsublat,iorig,itype,komp,numbsub,atcon, &
      energy,kkrsz,tab,tcpa,tau00,pzj,  &
      startk, endk, roots, nroots, rytodu, &
      lmax,aij,mapstr,mappnt,mapij, &
      powe,edu,pdu,rsnij,ndimrij,np2r,ndimnp, &
      numbrs,naij,dqint,ndimdqr,hplnm,ndimrhp,ndimlhp,irecdlm,d00,eoeta, &
      eta,ksvecs,nksvecs,conr,nop,rot,dop,if0 )
!!DESCRIPTION:
! prepares data for BSF plotting
!
!EOP
!
!BOC

    implicit none

    ! Plot band structure

    ! rotation operations are sent in case we wish to make
    ! symmetry reductions in the plotting. but this routine is
    ! simpler than that and does not use them.

    ! constant parameters

    real(kind=dp), parameter :: zerotl = 1.0d-04
    real(kind=dp), parameter :: fepthr = 1.0d-03
    integer, parameter :: maxit = 30

    ! structure constant parameters

    integer lmax,ndimrij
    integer ndimnp,ndimdqr,ndimrhp,ndimlhp
    integer mapij(2,*),naij,np2r(ndimnp,*)
    integer mapstr(natom,natom),mappnt(natom,natom),numbrs(*)
    integer nksvecs,itype(natom),irecdlm

    real*8 aij(3,*),rsnij(ndimrij,4)
    real*8 eta,rytodu
    real*8 ksvecs(nksvecs,3)
    real*8 startk(3), endk(3), avec(3), bvec(3)

    complex*16 conr(*),powe(*),dqint(ndimdqr,*)
    complex*16 edu,pdu,d00,eoeta
    complex*16 hplnm(ndimrhp,ndimlhp)
    complex*16 rayp
!    complex(8) greenks(kkrsz*natom,kkrsz*natom)


    ! symmetry related parameters

    integer nop
    real*8 rot(49,3,3)
    complex*16 dop(kkrsz,kkrsz,nop)
    integer if0(48,natom)

    integer nsub,nsubi

    ! Bloch Spectral Density Fn A(k,E)
    ! See PRB Vo. 21 No. 8 by Faulkner & Stocks

    integer, intent(in) :: natom,nsublat
    integer, intent(in) :: komp(nsublat)
    integer, intent(in) :: numbsub(nsublat)
    integer, intent(in) :: iorig(nsublat)
    real(kind=dp), intent(in) :: atcon(:,:)  ! (ipcomp,nsublat)

    integer, intent(in) :: kkrsz
    complex(kind=cd), intent(in) :: energy
    complex(kind=cd), intent(in) :: tab(:,:,:,:)   ! (ipkkr,ipkkr,ipcomp,nsublat)
    complex(kind=cd)             :: tcpa(:,:,:)  ! (ipkkr,ipkkr,natom)
    complex(kind=cd), intent(in) :: tau00(:,:,:)  ! (ipkkr,ipkkr,nsublat)
    complex(kind=cd), intent(in) :: pzj(:,:,:,:)  ! (ipkkr,ipkkr,ipcomp,nsublat)

    integer :: npoles
    complex(kind=cd) :: freepoles(nksvecs)
    complex(kind=cd) :: u(3)

    complex(kind=cd) :: tauk(size(tcpa,1),size(tcpa,2),size(tcpa,3))
    complex(kind=cd) :: greenk(size(tcpa,1),size(tcpa,2),size(tcpa,3))
    complex(kind=cd) :: divfac
    real(kind=dp) :: thisk(3), roots(10), dfsign
    integer :: nroots

    integer :: i, j, ndiv
    real(kind=dp) :: x, dx, klen, p, xb, xe
    complex(kind=cd) :: last4tau(4),cfit(0:3)
    real(kind=dp) :: last4xv(4),lb

    integer :: istack(maxit), sp
    real*8  :: bstack(3,maxit)

!DEBUG    real*8 :: curr

!DEBUG    logical :: isordered

    logical, parameter :: curvefitting = .true.

    ! debugging, which ray are we on?
    integer, save :: counter = 0, next_print
    counter = counter + 1
    if ( counter == 1 ) next_print=1

    ! stop program if we try this routine on a disordered crystal

!    isordered = .true.
!    do n = 1, nsublat
!      if( komp(n) > 1 ) isordered = .false.
!    end do
!    if( .not. isordered ) then
!      write(0,*) 'findblochstate :: only works for ordered crystal'
!      stop
!    end if
!
    ! k-space ray metrics and tcpa units conversion
    if ( counter == next_print) then
      write(6,*) ' --- entered findbloch --- ',counter
      next_print = (nint(float(counter)**(1./3.))+1)**3
    end if
!    if ( counter==1000) then
!      write(6,'('' --- skipping other similar messages...''/)')
!    end if
    tcpa = tcpa / rytodu
    thisk(:) = endk(:)-startk(:)
    klen = dsqrt(dot(thisk,thisk))

    ! where are the free electron poles?

    npoles = 0
    do i = 1, nksvecs

      avec(:) = endk(:)-startk(:)
      bvec(:) = startk(:)+ksvecs(i,:)

      call getquadroots( dcmplx(1.d0,0.d0)*dot(avec,avec), &
        dcmplx(2.d0,0.d0)*dot(avec,bvec), &
        dcmplx(1.d0,0.d0)*(dot(bvec,bvec)-edu), &
        u(2), u(1) )

      do j = 1, 2
      if( (-fepthr < dreal(u(j)) .and. dreal(u(j)) < 1.d0+fepthr) .and. &
          (-fepthr < dimag(u(j)) .and. dimag(u(j)) < fepthr) ) then

          npoles = npoles + 1
          if ( npoles>size(freepoles) ) call fstop('ERROR: too many freepoles')
          freepoles(npoles) = u(j)

      end if; end do

    end do

    if( .not. curvefitting ) then

      ! find roots by adaptive recursion with 5 subintervals
      nroots = 0; nsub = 3; nsubi = 25
      xb = 0.d0; xe = 1.d0; sp = 0; i = 0
      ndiv = max(nsub,ceiling(2.d0*nsubi*klen))
      do while( sp > 0 .or. i < ndiv )

        dx = (xe-xb)/ndiv
        x  = (1.d0-dble(i)/ndiv)*xb + dble(i)/ndiv*xe; rayp = x
        thisk(:) = (1.d0-x)*startk(:) + x*endk(:)
        call evaltau( lmax,kkrsz,natom,aij,mapstr,mappnt,nsublat,mapij, &
          powe,edu,pdu,rsnij,ndimrij,np2r,ndimnp, &
          numbrs,naij,dqint,ndimdqr,hplnm,ndimrhp,ndimlhp,irecdlm,d00,eoeta, &
          eta,ksvecs,nksvecs,conr,tcpa,rayp,freepoles,npoles, &
          thisk, tauk, divfac, dfsign, greenk )
        tauk = tauk/divfac
        tauk = rytodu * tauk

        if( i == 0 .and. sp == 0 ) then
          p = dfsign ! sign(1.d0,dimag(divfac))
          continue
        end if

        if( dabs(p - dfsign) > 0.1d0 ) then
          if( dx < zerotl .or. sp == maxit ) then
            nroots = nroots + 1;
            if ( nroots > size(roots) ) &
             call fstop('ERROR:: too small size(roots)')
            roots(nroots) = x; p = -p
          else
            sp = sp + 1
            istack(sp) = i
            bstack(1,sp) = xb
            bstack(2,sp) = xe
            bstack(3,sp) = dfsign ! sign(1.d0,dimag(divfac))

            xb = (1.d0-dble(i-1)/ndiv)*xb + dble(i-1)/ndiv*xe
            xe = x
            i = 0; ndiv = nsub
          end if
        end if

        do while( i == ndiv .and. sp > 0 )
          i  = istack(sp)
          xb = bstack(1,sp)
          xe = bstack(2,sp)
          p  = bstack(3,sp)
          sp = sp - 1

          if( sp == 0 ) ndiv = max(nsub,ceiling(2.d0*nsubi*klen))
        end do

        i = i + 1

      end do

    else

      ! find roots by fitting cubics to every four points
      nroots = 0; nsubi = 250
      ndiv = ceiling(2.d0*nsubi*klen)
      do i = 0, ndiv

        ! shift last 4 points
        last4tau(1:3) = last4tau(2:4)
        last4xv(1:3)  = last4xv(2:4)

        ! get next point
        x  = dble(i)/ndiv; rayp = x
        thisk(:) = (1.d0-x)*startk(:) + x*endk(:)
        call evaltau( lmax,kkrsz,natom,aij,mapstr,mappnt,nsublat,mapij, &
          powe,edu,pdu,rsnij,ndimrij,np2r,ndimnp, &
          numbrs,naij,dqint,ndimdqr,hplnm,ndimrhp,ndimlhp,irecdlm,d00,eoeta, &
          eta,ksvecs,nksvecs,conr,tcpa,rayp,freepoles,npoles, &
          thisk, tauk, divfac, dfsign, greenk )
        tauk = tauk/divfac
        tauk = rytodu * tauk
        last4tau(4) = divfac
        last4xv(4) = x
        if(i < 3) cycle

        ! use cubic fit to look for roots

        call getcubicfit(last4xv,last4tau,cfit)
        call getcubicroots(cfit(3),cfit(2),cfit(1),cfit(0),u(1),u(2),u(3))

        do j = 1, 3

          if( i /= 3 ) then
            lb = last4xv(3)
          else
            lb = last4xv(1)
          end if

          if( lb < real(u(j),dp) .and. &
              real(u(j),dp) < last4xv(4) .and. &
              -zerotl < dimag(u(j)) .and. dimag(u(j)) < zerotl ) then
            nroots = nroots + 1
            if ( nroots > size(roots) ) &
             call fstop('ERROR:: too small size(roots)')
            roots(nroots) = real(u(j),dp)
          end if

        end do

      end do

    end if

    ! debugging, print found poles and roots

!   write(6,*) 'np = ', npoles
!   do j = 1, npoles
!     write(6,*) freepoles(j)*klen
!   end do
!   write(6,*) 'nr = ', nroots
!   do j = 1, nroots
!     write(6,*) roots(j)*klen
!   end do

    ! remove roots too close to free electron poles?
    i = 1
    do while( i <= nroots )
      do j = 1, npoles
      if( dabs(roots(i)-real(freepoles(j),dp)) < fepthr ) then
        if( i < nroots ) roots(i:nroots-1) = roots(i+1:nroots)
        nroots = nroots - 1; i = i-1; exit
      end if; end do
      i = i + 1
    end do

!   write(6,*) 'nr* = ', nroots
!   do j = 1, nroots
!     write(6,*) roots(j)*klen
!   end do

     ! debugging, print out nth ray
!    if( counter == -1 ) then

!     last = 0.d0
!     curr = 0.d0
!
!       ndiv = 50000
!       do j = 1, ndiv
!         x  = dble(j)/ndiv; rayp = x
!         if( x*klen < 0.12d0 .or. x*klen > 0.16d0 ) cycle
!
!         thisk(:) = (1.d0-x)*startk(:) + x*endk(:)
!         call evaltau( lmax,kkrsz,natom,aij,mapstr,mappnt,nsublat,mapij, &
!           powe,edu,pdu,rsnij,ndimrij,np2r,ndimnp, &
!           numbrs,naij,dqint,ndimdqr,hplnm,ndimrhp,ndimlhp,irecdlm,d00,eoeta, &
!           eta,ksvecs,nksvecs,conr,greenks,tcpa,rayp,freepoles,npoles, &
!           thisk(1), tauk(1,1,1), divfac, dfsign, greenk(1,1,1) )
!         tauk = tauk/divfac
!         tauk = alat/(2.d0*pi) * tauk
!
!         curr = -dimag(cdlog(divfac))/pi
!         do while( curr - last > +1.0d0 ); curr = curr - 2.d0; end do
!         do while( curr - last < -1.0d0 ); curr = curr + 2.d0; end do
!         last = curr
!
! !        write(6,*) divfac
!
!         if( npoles > 0 ) then
!           write(100+counter,'(3f30.20)',advance='no') x*klen, curr, dfsign
!           do i = 1, npoles-1
!             write(100+counter,'(1f30.20)',advance='no') -dimag(cdlog(x-freepoles(i)))/pi
!           end do
!           write(100+counter,'(1f30.20)',advance='yes') -dimag(cdlog(x-freepoles(npoles)))/pi
!         else
!           write(100+counter,'(3f30.20)') x*klen, curr, dfsign
!         end if
!
!       end do
!       write(100+counter,*) ''
!
! !    end if

    tcpa = rytodu * tcpa

!EOC
  end subroutine findblochstate

  ! Subintegration Routines
  ! -----------------------------------------------------------------------

!BOP
!!IROUTINE: evalwedgeint
!!INTERFACE:
  subroutine evalwedgeint( v1, v2, v3, integ, loginteg, integG, &
      lmax,kkrsz,natom,aij,mapstr,mappnt,nsublat,mapij, &
      powe,edu,pdu,rsnij,ndimrij,np2r,ndimnp, &
      numbrs,naij,dqint,ndimdqr,hplnm,ndimrhp,ndimlhp,irecdlm,d00,eoeta, &
      eta,ksvecs,nksvecs,conr,tcpa,iorig,evalgamma )
!!DESCRIPTION:
! evaluate integral over polyhedra defined by face
!

!EOP
!
!BOC
    implicit none
!    include 'lmax.h'

    ! structure constant parameters

    integer lmax,natom,nsublat,ndimrij,kkrsz
    integer ndimnp,ndimdqr,ndimrhp,ndimlhp
    integer mapij(2,*),naij,np2r(ndimnp,*),iorig(natom)
    integer mapstr(natom,natom),mappnt(natom,natom),numbrs(*)
    integer nksvecs,irecdlm


    real*8 aij(3,*),rsnij(ndimrij,4)
    real*8 eta
    real*8 ksvecs(nksvecs,3)

    complex*16 conr(*),powe(*),dqint(ndimdqr,*)
!    complex*16 greenks(*)
    complex*16 loginteg
    complex(kind=cd), intent(in) :: tcpa(:,:,:)  ! (ipkkr,ipkkr,natom)
    complex*16 integ(kkrsz,kkrsz,natom)
    complex*16 integG(kkrsz,kkrsz,natom)
    complex*16 edu,pdu,d00,eoeta
    complex*16 hplnm(ndimrhp,ndimlhp)

    real(kind=dp), intent(in) :: v1(3), v2(3), v3(3)
    real(kind=dp) :: vol

    complex(kind=cd) :: cri(kkrsz,kkrsz,natom)
    complex(kind=cd) :: crgi(kkrsz,kkrsz,natom)
    complex(kind=cd) :: crli
    integer :: lvl
    logical :: evalgamma

    call evalrayint( center(v1,v2,v3), nraysubint, cri, crli, crgi, &
      lmax,kkrsz,natom,aij,mapstr,mappnt,nsublat,mapij, &
      powe,edu,pdu,rsnij,ndimrij,np2r,ndimnp, &
      numbrs,naij,dqint,ndimdqr,hplnm,ndimrhp,ndimlhp,irecdlm,d00,eoeta, &
      eta,ksvecs,nksvecs,conr,tcpa,evalgamma )

    vol = dabs(dot(v3,cross(v1-v3,v2-v3))/6.d0); lvl = 0

    if( nrecursion > 0 ) then
      call evalwedgeintrec( v1, v2, v3, vol, cri, integ, crli, loginteg, &
        crgi, integG, lvl, &
        lmax,kkrsz,natom,aij,mapstr,mappnt,nsublat,mapij, &
        powe,edu,pdu,rsnij,ndimrij,np2r,ndimnp, &
        numbrs,naij,dqint,ndimdqr,hplnm,ndimrhp,ndimlhp,irecdlm,d00,eoeta, &
        eta,ksvecs,nksvecs,conr,tcpa,iorig )
    else
      integ = cri*vol;
      loginteg = crli*vol;
      integG = crgi*vol;
    end if

!EOC
  end subroutine evalwedgeint

!BOP
!!IROUTINE: evalwedgeintrec
!!INTERFACE:
  recursive subroutine evalwedgeintrec( v1, v2, v3, vol, &
      cri, integ, crli, loginteg, crgi, integG, lvl, &
      lmax,kkrsz,natom,aij,mapstr,mappnt,nsublat,mapij, &
      powe,edu,pdu,rsnij,ndimrij,np2r,ndimnp, &
      numbrs,naij,dqint,ndimdqr,hplnm,ndimrhp,ndimlhp,irecdlm,d00,eoeta, &
      eta,ksvecs,nksvecs,conr,tcpa,iorig )
!!DESCRIPTION:
! recursive part of evaluation of integral over polyhedra defined by face
!
!EOP
!BOC
    implicit none

    ! structure constant parameters

    integer lmax,natom,nsublat,ndimrij,kkrsz
    integer ndimnp,ndimdqr,ndimrhp,ndimlhp
    integer mapij(2,*),naij,np2r(ndimnp,*),iorig(natom)
    integer mapstr(natom,natom),mappnt(natom,natom),numbrs(*)
    integer nksvecs,irecdlm

    real*8 aij(3,*),rsnij(ndimrij,4)
    real*8 eta
    real*8 ksvecs(nksvecs,3)

    complex*16 conr(*),powe(*),dqint(ndimdqr,*)
!    complex*16 greenks(*)
    complex(8), intent(in) :: tcpa(:,:,:)  ! (ipkkr,ipkkr,natom)
    complex*16 integ(kkrsz,kkrsz,natom)
    complex*16 integG(kkrsz,kkrsz,natom)
    complex*16 edu,pdu,d00,eoeta
    complex*16 loginteg
    complex*16 hplnm(ndimrhp,ndimlhp)

    ! evaluate integral (recursively) over wedge defined by v1, v2, v3

    real(kind=dp), parameter :: peracc = .05d0

    real(kind=dp), intent(in) :: v1(3), v2(3), v3(3), vol
    real(kind=dp) :: m1(3), m2(3), m3(3), del, svol, sri, error

    complex(kind=cd) :: cri(kkrsz,kkrsz,natom)
    complex(kind=cd) :: ri1(kkrsz,kkrsz,natom)
    complex(kind=cd) :: ri2(kkrsz,kkrsz,natom)
    complex(kind=cd) :: ri3(kkrsz,kkrsz,natom)

    complex(kind=cd) :: crgi(kkrsz,kkrsz,natom)
    complex(kind=cd) :: rgi1(kkrsz,kkrsz,natom)
    complex(kind=cd) :: rgi2(kkrsz,kkrsz,natom)
    complex(kind=cd) :: rgi3(kkrsz,kkrsz,natom)

    complex(kind=cd) :: di(kkrsz,kkrsz,natom)
    complex(kind=cd) :: dgi(kkrsz,kkrsz,natom)
    complex(kind=cd) :: crli, rli1, rli2, rli3
    complex(kind=cd) :: dli
    integer :: lvl, i, n
    logical :: subdivide, evalgamma = .false.

    lvl = lvl + 1

    svol = vol/4.d0
    m1 = midpoint(v2,v3)
    m2 = midpoint(v1,v3)
    m3 = midpoint(v1,v2)

    ! Debug code: Draw triangle on face

!    write(90,'(a,f10.5,a,f10.5,a,f10.5,a,f10.5,a,f10.5,a,f10.5,a)') &
!      'set arrow from ',v1(1),',',v1(2),',',v1(3),' to ', &
!      m2(1),',',m2(2),',',m2(3),' nohead lt 3'
!    write(90,'(a,f10.5,a,f10.5,a,f10.5,a,f10.5,a,f10.5,a,f10.5,a)') &
!      'set arrow from ',m2(1),',',m2(2),',',m2(3),' to ', &
!      m3(1),',',m3(2),',',m3(3),' nohead lt 3'
!    write(90,'(a,f10.5,a,f10.5,a,f10.5,a,f10.5,a,f10.5,a,f10.5,a)') &
!      'set arrow from ',m3(1),',',m3(2),',',m3(3),' to ', &
!      v1(1),',',v1(2),',',v1(3),' nohead lt 3'
!
!    write(90,'(a,f10.5,a,f10.5,a,f10.5,a,f10.5,a,f10.5,a,f10.5,a)') &
!      'set arrow from ',m1(1),',',m1(2),',',m1(3),' to ', &
!      v2(1),',',v2(2),',',v2(3),' nohead lt 3'
!    write(90,'(a,f10.5,a,f10.5,a,f10.5,a,f10.5,a,f10.5,a,f10.5,a)') &
!      'set arrow from ',v2(1),',',v2(2),',',v2(3),' to ', &
!      m3(1),',',m3(2),',',m3(3),' nohead lt 3'
!    write(90,'(a,f10.5,a,f10.5,a,f10.5,a,f10.5,a,f10.5,a,f10.5,a)') &
!      'set arrow from ',m3(1),',',m3(2),',',m3(3),' to ', &
!      m1(1),',',m1(2),',',m1(3),' nohead lt 3'
!
!    write(90,'(a,f10.5,a,f10.5,a,f10.5,a,f10.5,a,f10.5,a,f10.5,a)') &
!      'set arrow from ',m1(1),',',m1(2),',',m1(3),' to ', &
!      m2(1),',',m2(2),',',m2(3),' nohead lt 3'
!    write(90,'(a,f10.5,a,f10.5,a,f10.5,a,f10.5,a,f10.5,a,f10.5,a)') &
!      'set arrow from ',m2(1),',',m2(2),',',m2(3),' to ', &
!      v3(1),',',v3(2),',',v3(3),' nohead lt 3'
!    write(90,'(a,f10.5,a,f10.5,a,f10.5,a,f10.5,a,f10.5,a,f10.5,a)') &
!      'set arrow from ',v3(1),',',v3(2),',',v3(3),' to ', &
!      m1(1),',',m1(2),',',m1(3),' nohead lt 3'


    call evalrayint( center(v1,m2,m3), nraysubint, ri1, rli1, rgi1, &
      lmax,kkrsz,natom,aij,mapstr,mappnt,nsublat,mapij, &
      powe,edu,pdu,rsnij,ndimrij,np2r,ndimnp, &
      numbrs,naij,dqint,ndimdqr,hplnm,ndimrhp,ndimlhp,irecdlm,d00,eoeta, &
      eta,ksvecs,nksvecs,conr,tcpa,evalgamma )

    call evalrayint( center(m1,v2,m3), nraysubint, ri2, rli2, rgi2, &
      lmax,kkrsz,natom,aij,mapstr,mappnt,nsublat,mapij, &
      powe,edu,pdu,rsnij,ndimrij,np2r,ndimnp, &
      numbrs,naij,dqint,ndimdqr,hplnm,ndimrhp,ndimlhp,irecdlm,d00,eoeta, &
      eta,ksvecs,nksvecs,conr,tcpa,evalgamma )

    call evalrayint( center(m1,m2,v3), nraysubint, ri3, rli3, rgi3, &
      lmax,kkrsz,natom,aij,mapstr,mappnt,nsublat,mapij, &
      powe,edu,pdu,rsnij,ndimrij,np2r,ndimnp, &
      numbrs,naij,dqint,ndimdqr,hplnm,ndimrhp,ndimlhp,irecdlm,d00,eoeta, &
      eta,ksvecs,nksvecs,conr,tcpa,evalgamma )

    ! determine (and print) error in current estimate

    error = 0.d0
    do i = 1, kkrsz
    do n = 1, natom
      sri = cdabs(ri1(i,i,n)+ri2(i,i,n)+ri3(i,i,n)+cri(i,i,n))
      del = cdabs(ri1(i,i,n)+ri2(i,i,n)+ri3(i,i,n)-3.d0*cri(i,i,n))
      error = error + del/sri
    end do; end do

!    write(6,'(a)',advance='no') '     '
!    do i = 1, lvl
!      write(6,'(a)',advance='no') ' *'
!    end do
!
!    write(6,'(a,f5.2,a)',advance='no') ' : err = ', &
!      100.d0*error/(kkrsz*nsublat), '%'

    ! if error not below desired accuracy then split
    subdivide = .false.
    if( lvl < maxlvl ) then
      if( adaptive ) then
        if( error/(kkrsz*natom) > peracc ) subdivide = .true.
      else if( lvl < nrecursion ) then
        subdivide = .true.
      end if
!      write(6,*) ''
    else
!      write(6,'(a)') ', (max depth)'
    end if

    if( subdivide ) then

      integ = 0.d0; loginteg = 0.d0; integG = 0.d0
      call evalwedgeintrec( v1, m2, m3, svol, ri1,di,rli1,dli,rgi1,dgi,lvl,&
        lmax,kkrsz,natom,aij,mapstr,mappnt,nsublat,mapij, &
        powe,edu,pdu,rsnij,ndimrij,np2r,ndimnp, &
        numbrs,naij,dqint,ndimdqr,hplnm,ndimrhp,ndimlhp,irecdlm,d00,eoeta, &
        eta,ksvecs,nksvecs,conr,tcpa,iorig )
      integ = integ + di
      loginteg = loginteg + dli
      integG = integG + dgi

      call evalwedgeintrec( m1, v2, m3, svol, ri2,di,rli2,dli,rgi2,dgi,lvl,&
        lmax,kkrsz,natom,aij,mapstr,mappnt,nsublat,mapij, &
        powe,edu,pdu,rsnij,ndimrij,np2r,ndimnp, &
        numbrs,naij,dqint,ndimdqr,hplnm,ndimrhp,ndimlhp,irecdlm,d00,eoeta, &
        eta,ksvecs,nksvecs,conr,tcpa,iorig )
      integ = integ + di
      loginteg = loginteg + dli
      integG = integG + dgi

      call evalwedgeintrec( m1, m2, v3, svol, ri3,di,rli3,dli,rgi3,dgi,lvl,&
        lmax,kkrsz,natom,aij,mapstr,mappnt,nsublat,mapij, &
        powe,edu,pdu,rsnij,ndimrij,np2r,ndimnp, &
        numbrs,naij,dqint,ndimdqr,hplnm,ndimrhp,ndimlhp,irecdlm,d00,eoeta, &
        eta,ksvecs,nksvecs,conr,tcpa,iorig )
      integ = integ + di
      loginteg = loginteg + dli
      integG = integG + dgi

      call evalwedgeintrec( m1, m2, m3, svol, cri,di,crli,dli,crgi,dgi,lvl,&
        lmax,kkrsz,natom,aij,mapstr,mappnt,nsublat,mapij, &
        powe,edu,pdu,rsnij,ndimrij,np2r,ndimnp, &
        numbrs,naij,dqint,ndimdqr,hplnm,ndimrhp,ndimlhp,irecdlm,d00,eoeta, &
        eta,ksvecs,nksvecs,conr,tcpa,iorig )
      integ = integ + di
      loginteg = loginteg + dli
      integG = integG + dgi

    else

      integ = svol*(cri + ri1 + ri2 + ri3)
      loginteg = svol*(crli + rli1 + rli2 + rli3)
      integG = svol*(crgi + rgi1 + rgi2 + rgi3)

    end if

    lvl = lvl - 1
!EOC
  end subroutine evalwedgeintrec

!BOP
!!IROUTINE: evalrayint
!!INTERFACE:
  subroutine evalrayint(raytip, nsubint, integ, loginteg, integG, &
      lmax,kkrsz,natom,aij,mapstr,mappnt,nsublat,mapij, &
      powe,edu,pdu,rsnij,ndimrij,np2r,ndimnp, &
      numbrs,naij,dqint,ndimdqr,hplnm,ndimrhp,ndimlhp,irecdlm,d00,eoeta,   &
      eta,ksvecs,nksvecs,conr,tcpa,evalgamma )
!!DESCRIPTION:
! integration over ray:
! integral{$f(r)/g(r) 3 r^2/R^3 dr$} using nsample points
! (integral x volume) gives total contribution of wedge
!
!EOP
!
!BOC
    implicit none

    ! structure constant parameters

    integer lmax,natom,nsublat,ndimrij,kkrsz
    integer ndimnp,ndimdqr,ndimrhp,ndimlhp
    integer mapij(2,*),naij,np2r(ndimnp,*)
    integer mapstr(natom,natom),mappnt(natom,natom),numbrs(*)
    integer nksvecs,irecdlm

    real*8 aij(3,*),rsnij(ndimrij,4)
    real*8 eta
    real*8 ksvecs(nksvecs,3)

    complex*16 conr(*),powe(*),dqint(ndimdqr,*)
!    complex*16 greenks(*)
    complex(kind=cd), intent(in) :: tcpa(:,:,:)  ! (ipkkr,ipkkr,natom)
    complex*16 integ(kkrsz,kkrsz,natom)
    complex*16 integG(kkrsz,kkrsz,natom)
    complex*16 edu,pdu,d00,eoeta
    complex*16 hplnm(ndimrhp,ndimlhp)
    complex*16 loginteg


    real(kind=dp), parameter :: divthr = 1.0d-03
    real(kind=dp), parameter :: fepthr = 1.0d-02
    complex(kind=cd), parameter :: im = dcmplx(0_dp,1_dp)
!    real(kind=dp), parameter :: pi = 3.141592653589793
    integer, save :: rayindex = 0
    integer, save :: imsg_save = 0
    complex(8) :: edu_save = -im

!   real(kind=dp), intent(in) :: raytip(3)
    real(kind=dp) :: raytip(3), detsign
    integer, intent(in) :: nsubint

    complex(kind=cd), save, pointer :: gamptf(:,:,:)=>null(), gampth(:,:,:)=>null()
    complex(kind=cd), save :: gamptg = 0
    logical, save :: isFirstRun = .true.
    logical :: evalgamma

    complex(kind=cd) :: f(kkrsz,kkrsz,natom,4)
    complex(kind=cd) :: h(kkrsz,kkrsz,natom,4)
    complex(kind=cd) :: a(kkrsz,kkrsz,natom,0:5)
    complex(kind=cd) :: d(kkrsz,kkrsz,natom,0:5)
    complex(kind=cd) :: g(4), b(0:3), c(0:3)
    complex(kind=cd) :: freepoles(nksvecs), rayp
    integer :: npoles

    real(kind=dp) :: raylen, normrt(3)
    real(kind=dp) :: dr,r(4),v(3,4)
    real(kind=dp) :: x,y,k,dk,rLn,rRn
    real(kind=dp) :: phL,phR,phb,dph,dph1,dph2,dph3
    complex(kind=cd) :: dI(kkrsz,kkrsz,natom)
    complex(kind=cd) :: dJ(kkrsz,kkrsz,natom)
    complex(kind=cd) :: al(kkrsz,kkrsz,natom)
    complex(kind=cd) :: be,ga,fp,z(3),dLI,ddLI
    integer :: i,j,m,n,gfit, nsubintsc, le, re

    complex(kind=cd) :: LIfree

    character(len=32) :: filename
!DEBUG    logical :: ex

    raylen = dsqrt(dot(raytip,raytip))
    normrt = raytip/raylen
    if ( edu .ne. edu_save ) then
      edu_save = edu
      imsg_save = 1
    end if

    ! determine which free electron poles to remove for this integration

    npoles = 0
    do i = 1, nksvecs

      call getquadroots( dcmplx(1.d0,0.d0), &
        dcmplx(2.d0,0.d0)*dot(normrt,ksvecs(i,:)), &
        dcmplx(1.d0,0.d0)*(dot(ksvecs(i,:),ksvecs(i,:))-edu), &
        z(2), z(1) )

      do j = 1, 2
      if( (-fepthr < dreal(z(j)) .and. dreal(z(j)) < raylen+fepthr) .and. &
          (-fepthr < dimag(z(j)) .and. dimag(z(j)) < fepthr) ) then

          npoles = npoles + 1
          if ( npoles>size(freepoles) ) call fstop('ERROR:: too many freepoles')
          freepoles(npoles) = z(j)

      end if; end do

    end do

    if( debugflag .and. pr_rayintegrand ) then
      write(6,*) ''
      write(6,'(a,3f20.15)') 'ray=', raytip(:)
      write(6,*) ''
      do i = 1, npoles
        write(6,'(''pole @ k = '',2f20.15)') freepoles(i)
      end do
      if( npoles > 0 ) write(6,*) ''
    end if

    ! for lloyd's formula: subtract integral{ log(freefactor) r^2 dr }

    loginteg = dcmplx(0.d0,0.d0)

    dLI = dcmplx(0.d0,0.d0)
    do i = 1, npoles

        fp = freepoles(i)
        be = raylen - fp
        ddLI = dcmplx(0.d0,0.d0)
        ddLI = ddLI + fp*fp*be
        ddLI = ddLI + fp*be*be
        ddLI = ddLI + be*be*be/3.d0
        dLI = dLI + ddLI * cdlog(be)
        dLI = dLI - fp*fp*be
        dLI = dLI - fp*be*be/2.d0
        dLI = dLI - be*be*be/9.d0

        be = -fp
        ddLI = dcmplx(0.d0,0.d0)
        ddLI = ddLI - fp*fp*be
        ddLI = ddLI - fp*be*be
        ddLI = ddLI - be*be*be/3.d0
        dLI = dLI + ddLI * cdlog(be)
        dLI = dLI + fp*fp*be
        dLI = dLI + fp*be*be/2.d0
        dLI = dLI + be*be*be/9.d0

    end do
    LIfree = -dLI
    loginteg = loginteg - dLI

    ! evaluate and save shared point at origin of BZ
    ! note: can't include free poles since they are ray dependent
    ! note: if many BZ integrals are done at different energies,
    !  then need to add guard code to ensure phase at gamma doesn't jump

    if(debugflag) call begintiming("Evaluate T(0),G(0),f(0)")
    r(1) = 0.0d0
    v(:,1) = 0.0d0
    if( isFirstRun ) then
      allocate( gamptf(kkrsz,kkrsz,natom) )
      allocate( gampth(kkrsz,kkrsz,natom) )
      isFirstRun = .false.
    end if
    if( evalgamma ) then
      n = 0
      rayp = dsqrt(dot(v(:,1),v(:,1)))
      call evaltau( lmax,kkrsz,natom,aij,mapstr,mappnt,nsublat,mapij, &
        powe,edu,pdu,rsnij,ndimrij,np2r,ndimnp, &
        numbrs,naij,dqint,ndimdqr,hplnm,ndimrhp,ndimlhp,irecdlm,d00,eoeta, &
        eta,ksvecs,nksvecs,conr,tcpa,rayp,freepoles,n,&
        v(:,1), gamptf, gamptg, detsign, gampth )
      rayindex = 1
      ! write(6,'(a,i5)') ' ray# = ', rayindex
    else
      rayindex = rayindex + 1
      ! write(6,'(a,i5)') ' ray# = ', rayindex
    end if

    ! include free electron poles in gamma pt
    ! note: must add free phases to gamma phase one at a time
    g(1) = gamptg; phL = dimag(cdlog(gamptg))
    ga = dcmplx(1.d0,0.d0)
    do i = 1, npoles
      ga = -freepoles(i)*ga
      phL = phL + dimag(cdlog(-freepoles(i)))
    end do
    f(:,:,:,1) = ga * gamptf; g(1) = ga * gamptg
    h(:,:,:,1) = ga * gampth

    if(debugflag) call endtiming()

    ! debugging: open file for writing integrand along ray
    if( debugflag .and. pr_rayintegrand ) then
      write(filename,'(''ray_g(k).'',i3.3)') rayindex
      open(unit=200+rayindex,file=trim(filename)//'.'//gGenName(),     &
            status='replace',action='write', position='append')
      write(200+rayindex,'(4e30.15)') &
        r(1), phL, real(g(1),dp), dimag(g(1))
    end if

    nsubintsc = nint(nsubint*raylen)

    integ = (0.d0,0.d0); integG = (0.d0,0.d0)
    k = 0.d0; dk = 1.d0/dble(nsubintsc)
    do i = 1, nsubintsc

      dr = raylen/dble(nsubintsc)

      ! make cubic fit over subinterval
      ! f --> a3 r^3 + a2 r^2 + a1 r + a0

      if(debugflag) call begintiming("Evaluate T(k),G(k),f(k)")

      if( i == 1 ) then

        r(2) = 1.d0*dk*raylen
        v(:,2) = r(2)*normrt; rayp = dsqrt(dot(v(:,2),v(:,2)))
        call evaltau( lmax,kkrsz,natom,aij,mapstr,mappnt,nsublat,mapij, &
        powe,edu,pdu,rsnij,ndimrij,np2r,ndimnp, &
        numbrs,naij,dqint,ndimdqr,hplnm,ndimrhp,ndimlhp,irecdlm,d00,eoeta,   &
        eta,ksvecs,nksvecs,conr,tcpa,rayp,freepoles,npoles,&
        v(:,2), f(:,:,:,2), g(2), detsign, h(:,:,:,2) )

        r(3) = 2.d0*dk*raylen
        v(:,3) = r(3)*normrt; rayp = dsqrt(dot(v(:,3),v(:,3)))
        call evaltau(lmax,kkrsz,natom,aij,mapstr,mappnt,nsublat,mapij, &
        powe,edu,pdu,rsnij,ndimrij,np2r,ndimnp, &
        numbrs,naij,dqint,ndimdqr,hplnm,ndimrhp,ndimlhp,irecdlm,d00,eoeta,   &
        eta,ksvecs,nksvecs,conr,tcpa,rayp,freepoles,npoles,&
        v(:,3), f(:,:,:,3), g(3), detsign, h(:,:,:,3) )

        r(4) = 3.d0*dk*raylen
        v(:,4) = r(4)*normrt; rayp = dsqrt(dot(v(:,4),v(:,4)))
        call evaltau( lmax,kkrsz,natom,aij,mapstr,mappnt,nsublat,mapij, &
        powe,edu,pdu,rsnij,ndimrij,np2r,ndimnp, &
        numbrs,naij,dqint,ndimdqr,hplnm,ndimrhp,ndimlhp,irecdlm,d00,eoeta,   &
        eta,ksvecs,nksvecs,conr,tcpa,rayp,freepoles,npoles,&
        v(:,4), f(:,:,:,4), g(4), detsign, h(:,:,:,4) )

        le = 1; re = 2

      else if ( i == 2 ) then

        le = 2; re = 3

      else if ( 3 <= i .and. i <= nsubintsc-1 ) then

        ! shift sampling points to the left by one
        ! is this expensive copy operation?
        ! to do: using pointers will result in fewer move operations

        if(debugflag) call begintiming("Shift endpoints")
        r(1) = r(2); v(:,1) = v(:,2); g(1) = g(2)
        f(:,:,:,1) = f(:,:,:,2); h(:,:,:,1) = h(:,:,:,2)
        r(2) = r(3); v(:,2) = v(:,3); g(2) = g(3)
        f(:,:,:,2) = f(:,:,:,3); h(:,:,:,2) = h(:,:,:,3)
        r(3) = r(4); v(:,3) = v(:,4); g(3) = g(4)
        f(:,:,:,3) = f(:,:,:,4); h(:,:,:,3) = h(:,:,:,4)
        if(debugflag) call endtiming()

        ! evaluate next point

        r(4) = (i+1)*dk*raylen
        v(:,4) = r(4)*normrt; rayp = dsqrt(dot(v(:,4),v(:,4)))
        call evaltau( lmax,kkrsz,natom,aij,mapstr,mappnt,nsublat,mapij, &
        powe,edu,pdu,rsnij,ndimrij,np2r,ndimnp, &
        numbrs,naij,dqint,ndimdqr,hplnm,ndimrhp,ndimlhp,irecdlm,d00,eoeta,   &
        eta,ksvecs,nksvecs,conr,tcpa,rayp,freepoles,npoles,&
        v(:,4), f(:,:,:,4), g(4), detsign, h(:,:,:,4) )

        le = 2; re = 3

      else if ( i == nsubintsc ) then

        le = 3; re = 4

      end if

      if(debugflag) call endtiming()

      if(debugflag) call begintiming("Find cubic fit of T(k),G(k),f(k)")
      gfit = 3
      a(:,:,:,0:1) = dcmplx(0.d0,0.d0)
      d(:,:,:,0:1) = dcmplx(0.d0,0.d0)
      do j = 1, natom
      do m = 1, kkrsz
      do n = 1, kkrsz
        call getcubicfit(r, f(m,n,j,:), a(m,n,j,2:5))
        call getcubicfit(r, h(m,n,j,:), d(m,n,j,2:5))
      end do; end do; end do
      call getcubicfit(r, g, b)
      if(debugflag) call endtiming()

      ! guard against overfitting the denominator
      ! it leads to significiant roundoff error on long division

      do

!?        x = tiny(x)
!?        do j = 0, gfit-1
!?          x = max(x, dabs(real(b(j),dp)), dabs(dimag(b(j))) )
!?        end do

        y = max( dabs(real(b(gfit),dp)), dabs(dimag(b(gfit))) )
!       select case( gfit )
!         case(3); y = y * dr*dr*dr
!         case(2); y = y * dr*dr
!         case(1); y = y * dr
!       end select
!       if( y/x > divthr ) exit
        if( y > divthr ) exit

        gfit = gfit-1
        select case (gfit)
          case(2); call getquadfit(r(1:3),g(1:3),b(0:2))
                   call getquadfit(r(2:4),g(2:4),c(0:2))
                   b = (b + c)/2.d0
          case(1); call getlinefit(r(1:2),g(1:3),b(0:1))
                   call getlinefit(r(2:4),g(2:4),c(0:1))
                   b = (b + c)/2.d0
          case(0); b(0) = (g(1)+g(2)+g(3)+g(4))/4.d0
          case default
            if ( abs(b(0)) == 0.d0 )    &
            call fstop('ERROR :: det==0 in evalrayint?')
            gfit = 0
            if ( imsg_save== 1 ) then
             write(6,'(a,2g18.8,a,2g18.8,a)')       &
        ' evalrayint: at edu=',edu,' det=',b(0),' is very close to zero'
             imsg_save = 0
             edu_save = edu
            end if
            exit
        end select

      end do

      ! on long division,
      ! sum{ a(n) r^n, n=0..5 } / sum{ b(n) r^n, n=0..3 } =
      !   sum{ q(n) r^n, n=0..2 } + remainder
      ! where remainder = sum{ c(n) r^n, n=0..2 } / sum{ b(n) r^n }

      if(debugflag) call begintiming("Long divide fit T(k)/f(k) and G(k)/f(k)")
      do j = 1, natom
      do m = 1, kkrsz
      do n = 1, kkrsz
        call dolongdiv(a(m,n,j,:),5,b,gfit)
        call dolongdiv(d(m,n,j,:),5,b,gfit)
      end do; end do; end do
      if(debugflag) call endtiming()

      ! approximate true integral by the exact integral corresponding
      ! to our fits in this subinterval

      if(debugflag) call begintiming("Evaluate subintegral of fit")

      dI = dcmplx(0.d0,0.d0)
      dJ = dcmplx(0.d0,0.d0)

      rRn = 1.d0; rLn = 1.d0; x = 1.0d0
      do j = gfit, 5
        rRn = rRn*r(re)
        rLn = rLn*r(le)
        dI = dI + a(:,:,:,j)*(rRn - rLn)/x
        dJ = dJ + d(:,:,:,j)*(rRn - rLn)/x
        x = x + 1.d0
      end do

      select case ( gfit )
        case(3)
          call getcubicroots(b(3),b(2),b(1),b(0),z(3),z(2),z(1))

          ! for tau
          al = a(:,:,:,2)*z(3)*z(3) + a(:,:,:,1)*z(3) + a(:,:,:,0)
          be = b(3)*(z(3)-z(1))*(z(3)-z(2))
          dI = dI + al/be*cdlog( (r(re)-z(3))/(r(le)-z(3)) )
          al = a(:,:,:,2)*z(2)*z(2) + a(:,:,:,1)*z(2) + a(:,:,:,0)
          be = b(3)*(z(2)-z(1))*(z(2)-z(3))
          dI = dI + al/be*cdlog( (r(re)-z(2))/(r(le)-z(2)) )
          al = a(:,:,:,2)*z(1)*z(1) + a(:,:,:,1)*z(1) + a(:,:,:,0)
          be = b(3)*(z(1)-z(2))*(z(1)-z(3))
          dI = dI + al/be*cdlog( (r(re)-z(1))/(r(le)-z(1)) )

          ! for green
          al = d(:,:,:,2)*z(3)*z(3) + d(:,:,:,1)*z(3) + d(:,:,:,0)
          be = b(3)*(z(3)-z(1))*(z(3)-z(2))
          dJ = dJ + al/be*cdlog( (r(re)-z(3))/(r(le)-z(3)) )
          al = d(:,:,:,2)*z(2)*z(2) + d(:,:,:,1)*z(2) + d(:,:,:,0)
          be = b(3)*(z(2)-z(1))*(z(2)-z(3))
          dJ = dJ + al/be*cdlog( (r(re)-z(2))/(r(le)-z(2)) )
          al = d(:,:,:,2)*z(1)*z(1) + d(:,:,:,1)*z(1) + d(:,:,:,0)
          be = b(3)*(z(1)-z(2))*(z(1)-z(3))
          dJ = dJ + al/be*cdlog( (r(re)-z(1))/(r(le)-z(1)) )

        case(2)

          call getquadroots(b(2),b(1),b(0),z(2),z(1))

          al = a(:,:,:,1)*z(2) + a(:,:,:,0)
          be = b(2)*(z(2)-z(1))
          dI = dI + al/be*cdlog( (r(re)-z(2))/(r(le)-z(2)) )
          al = a(:,:,:,1)*z(1) + a(:,:,:,0)
          be = b(2)*(z(1)-z(2))
          dI = dI + al/be*cdlog( (r(re)-z(1))/(r(le)-z(1)) )

          al = d(:,:,:,1)*z(2) + d(:,:,:,0)
          be = b(2)*(z(2)-z(1))
          dJ = dJ + al/be*cdlog( (r(re)-z(2))/(r(le)-z(2)) )
          al = d(:,:,:,1)*z(1) + d(:,:,:,0)
          be = b(2)*(z(1)-z(2))
          dJ = dJ + al/be*cdlog( (r(re)-z(1))/(r(le)-z(1)) )

        case(1)
          z(1) = -b(0)/b(1)
          dI = dI + a(:,:,:,0)/b(1)*cdlog( (r(re)-z(1))/(r(le)-z(1)) )
          dJ = dJ + d(:,:,:,0)/b(1)*cdlog( (r(re)-z(1))/(r(le)-z(1)) )

      end select

      integ = integ + dI
      integG = integG + dJ

      if(debugflag) call endtiming()

      ! debugging: print denominator fit for subinterval

      if( debugflag .and. pr_rayintegrand ) then

        write(6,'(''ray= '',i3.3,'', subi= '',i3.3,'', gfit='',i1)') &
          rayindex, i, gfit
        do j = 1, 4
          write(6,'(a,i1,a,f20.15)') ' r',j,'= ',r(j)
        end do
        do j = 1, gfit
          write(6,'('' z'',i1,''='',2f20.15)') j, z(j)
        end do

      end if

      ! for lloyd's formula: evaluate integral{ log(g) r^2 dr }

      if(debugflag) call begintiming("Do r^2 log(f) integration")

      dLI = dcmplx(0.d0,0.d0)
      dph = 0.d0; phb = phL
      do j = 1, gfit

        be = r(re) - z(j)
        ga = cdlog(be)
        ddLI = dcmplx(0.d0,0.d0)
        ddLI = ddLI + z(j)*z(j)*be
        ddLI = ddLI + z(j)*be*be
        ddLI = ddLI + be*be*be/3.d0
        dLI = dLI + ddLI * ga
        dLI = dLI - z(j)*z(j)*be
        dLI = dLI - z(j)*be*be/2.d0
        dLI = dLI - be*be*be/9.d0
        dph = dph + dimag(ga)

        be = r(le) - z(j)
        ga = cdlog(be)
        ddLI = dcmplx(0.d0,0.d0)
        ddLI = ddLI - z(j)*z(j)*be
        ddLI = ddLI - z(j)*be*be
        ddLI = ddLI - be*be*be/3.d0
        dLI = dLI  + ddLI * ga
        dLI = dLI + z(j)*z(j)*be
        dLI = dLI + z(j)*be*be/2.d0
        dLI = dLI + be*be*be/9.d0
        dph = dph - dimag(ga)
        phb = phb - dimag(ga)

      end do
      phR = phL + dph

      if( debugflag .and. pr_rayintegrand ) then

        x = phL
        do j = re, re

          dph = 0.d0
          do n = 1, gfit
            dph = dph + dimag(cdlog(r(j) - z(n)))
            dph = dph - dimag(cdlog(r(j-1)-z(n)))
          end do

          dph1 = 0.d0; dph2 = 0.d0; dph3 = 0.d0
          dph1 = dimag(cdlog(r(j) - z(1))) - dimag(cdlog(r(j-1)-z(1)))
          dph2 = dimag(cdlog(r(j) - z(2))) - dimag(cdlog(r(j-1)-z(2)))
          dph3 = dimag(cdlog(r(j) - z(3))) - dimag(cdlog(r(j-1)-z(3)))
          y = x + dph1 + dph2 + dph3
          x = y

          write(6,*) 'dph1 = ', dph1, 'for', j
          write(6,*) 'dph2 = ', dph2, 'for', j
          write(6,*) 'dph3 = ', dph3, 'for', j
          write(200+rayindex,'(10e30.15)') &
            r(j), y, real(g(j),dp), dimag(g(j)), z(1), z(2), z(3)

        end do

      end if

      ! add the effect of an overall phase constant

      dLI = dLI + im*(r(re)*r(re)*r(re)-r(le)*r(le)*r(le))*phb/3.d0
      loginteg = loginteg + dLI

      if( debugflag .and. pr_rayintegrand ) then
        write(6,'(a,2f20.15)') 'dLI = ', dLI
        write(6,'(a,2f20.15)') 'LI = ', loginteg
        write(6,*) ''
      end if

      if(debugflag) call endtiming()

      phL = phR

    end do

    ! debugging: close ray info file
    if( debugflag .and. pr_rayintegrand ) then

      ! print free pole locations
      write(200+rayindex,*) ''
      do i = 1, npoles
        write(200+rayindex,'(2e30.15)') freepoles(i)
      end do
      close(unit=200+rayindex)

    end if

    ! an additional factor to simplify ray weights, see fn preface
    integ = 3.d0*integ/(raylen*raylen*raylen)
    integG = 3.d0*integG/(raylen*raylen*raylen)
    loginteg = 3.d0*loginteg/(raylen*raylen*raylen)
    if(debugflag.and.pr_rayintegrand) write(6,'('' LI='',2f20.15)') loginteg
    if(debugflag) call checkToAbort()

!EOC
  end subroutine evalrayint

!BOP
!!IROUTINE: evaltau
!!INTERFACE:
  subroutine evaltau( lmax,kkrsz,natom,aij,mapstr,mappnt,nsublat,mapij, &
      powe,edu,pdu,rsnij,ndimrij,np2r,ndimnp, &
      numbrs,naij,dqint,ndimdqr,hplnm,ndimrhp,ndimlhp,irecdlm,d00,eoeta,   &
      eta,ksvecs,nksvecs,conr,tcpa,rayp,freepoles,npoles, &
      k,taunum,tauden,tdsign,greenfn )
!!DESCRIPTION:
! module engine to calculate tau-related data
!
!!REMARKS:
! matrix inversion is done here explicitly, i.e. other
! mecca inversion-related subroutines are not involved
!EOP
!
!BOC
    implicit none

!    logical, parameter :: usealternate = .true.

    ! structure constant parameters

    integer, intent(in) :: lmax,natom,nsublat,ndimrij,kkrsz
    integer, intent(in) :: ndimnp,ndimdqr,ndimrhp,ndimlhp
    integer, intent(in) :: mapij(2,*),naij,np2r(ndimnp,*)
    integer, intent(in) :: mapstr(natom,natom),mappnt(natom,natom),numbrs(*)
    integer, intent(in) :: nksvecs,irecdlm

    real(8), intent(in) :: aij(3,*),rsnij(ndimrij,4)
    real(8), intent(in) :: eta
    real(8), intent(in) :: ksvecs(nksvecs,3)
    real(8), intent(out) :: tdsign

    complex(8), intent(in) :: conr(*),powe(*),dqint(ndimdqr,*)
    complex(8), intent(out) :: tauden
    complex(kind=cd), intent(in) :: tcpa(:,:,:)  ! (ipkkr,ipkkr,natom)
    complex(8), intent(in) :: edu,pdu,d00,eoeta
    complex(8), intent(in) :: hplnm(ndimrhp,ndimlhp)
    complex(8), intent(out) :: taunum(:,:,:)
    complex(8), intent(out) :: greenfn(:,:,:)

    real(kind=dp), intent(in) :: k(3)
    integer, intent(in) :: npoles
    complex(kind=cd), intent(in) :: freepoles(npoles)
    complex(kind=cd), intent(in) :: rayp

    complex(8), allocatable :: greenks(:,:)
    complex(8), allocatable :: g0ks(:,:)
    complex(8), allocatable :: work(:,:)

    ! internal variables

!    real(kind=dp), parameter :: pi = 3.141592653589793_dp

    character*10 :: istop = 'null'

!DEBUG    real*8, allocatable :: xknlat0(:,:)
!DEBUG    complex*16, allocatable :: work(:,:)

    integer :: i, n


!DEBUG    real(kind=dp) :: nn(3)
!DEBUG    real(kind=dp) :: c = 32.d0*datan(1.0d0)
    complex(kind=cd) :: freefactor
!DEBUG    real(kind=dp) :: scalefac


    integer :: nblock
    integer :: st_env
    integer, save :: idebug=-1
    complex(kind=cd) :: logdet

    ! debug warning
!    tcpa = dcmplx(1.d-15,0.d0)

    ! fill in structure constants
    if(debugflag) call begintiming("evaltau // compute structure const")

    allocate(greenks(kkrsz*natom,kkrsz*natom))

    n = (2*lmax+1)**2
    call gf0_ks(lmax,natom,aij,mapstr,mappnt,natom,mapij, &
      powe,k(1),k(2),k(3),edu,pdu,irecdlm, &
      rsnij,ndimrij,np2r,ndimnp,numbrs,naij,dqint,ndimdqr,&
      hplnm,ndimrhp,ndimlhp,d00,eoeta,eta,ksvecs,nksvecs,conr,nksvecs, &
      greenks,iprint,istop)
    if(debugflag) call endtiming()


    ! remove free electron poles for this ray
!    klen = dsqrt(dot(k,k))
    freefactor = dcmplx(1.d0,0.d0)
    do i = 1, npoles
      freefactor = ( rayp - freepoles(i) )*freefactor
    end do

      ! MAJOR DEBUG WARNING
      ! debugging: dump t_cpa matrix for read in mathematica
!     write(101,*) ''
!     write(101,*) '(* at k = ', dsqrt(dot(k,k)), ' *)'
!     write(101,*) 't = SetPrecision['
!     write(101,*) '{'
!     do i = 1, kkrsz
!
!       write(101,*) '{'
!       do j = 1, kkrsz
!         write(101,'(F30.20,A,F30.20)', advance='no') &
!           dble(tcpa(i,j,1)), ' + I * ', dimag(tcpa(i,j,1))
!         if( j /= kkrsz ) then
!           write(101,*) ','
!          else
!           write(101,*) ''
!         end if
!       end do
!
!       if( i /= kkrsz ) then
!         write(101,*) '},'
!       else
!         write(101,*) '}'
!       end if
!
!     end do
!     write(101,*) '},16];'

!     ! debugging: dump G0 matrix for read in mathematica
!     write(101,*) 'Gfree = SetPrecision['
!     write(101,*) '{'
!     do i = 1, kkrsz
!
!       write(101,*) '{'
!       do j = 1, kkrsz
!         write(101,'(F30.20,A,F30.20)', advance='no') &
!           dble(greenks(i,j)), ' + I * ', dimag(greenks(i,j))
!         if( j /= kkrsz ) then
!           write(101,*) ','
!          else
!           write(101,*) ''
!         end if
!       end do
!
!       if( i /= kkrsz ) then
!         write(101,*) '},'
!       else
!         write(101,*) '}'
!       end if
!
!     end do
!     write(101,*) '},16];'

    if(debugflag) call begintiming("evaltau // save g0(k)")

    allocate(g0ks(kkrsz*natom,kkrsz*natom))

    g0ks = greenks
    if(debugflag) call endtiming()

    ! change to ( 1 - t G0 ), more stable

    n = natom*kkrsz
      if(debugflag) call begintiming("evaltau // premultiply t")
      allocate( work(kkrsz,n) )
      do i = 1, natom
        call zgemm('N','N',kkrsz,n,kkrsz,dcmplx(1.d0,0.d0),&
          tcpa(1,1,i),size(tcpa,1),greenks(1+(i-1)*kkrsz,1),n,&
          dcmplx(0.d0,0.d0),work(1,1),kkrsz)
        greenks(1+(i-1)*kkrsz:i*kkrsz,1:n) = -work(1:kkrsz,1:n)
      end do
      deallocate(work)
      do i = 1, kkrsz*natom
        greenks(i,i) = greenks(i,i) + dcmplx(1.d0,0.d0)
      end do

      if(debugflag) call endtiming()

      ! debugging: dump M matrix for read in mathematica
!     write(101,*) 'M = SetPrecision['
!     write(101,*) '{'
!     do i = 1, kkrsz
!
!       write(101,*) '{'
!       do j = 1, kkrsz
!         write(101,'(F30.20,A,F30.20)', advance='no') &
!           dble(greenks(i,j)), ' + I * ', dimag(greenks(i,j))
!         if( j /= kkrsz ) then
!           write(101,*) ','
!          else
!           write(101,*) ''
!         end if
!       end do
!
!       if( i /= kkrsz ) then
!         write(101,*) '},'
!       else
!         write(101,*) '}'
!       end if
!
!     end do
!     write(101,*) '},16];'

    tauden = dcmplx(1.d0,0.d0); tdsign = 0.d0

    if(debugflag) call begintiming("evaltau // lapack inversion")

      nblock = 0     ! i.e. the block size is defined in invmatr
      call invmatr(greenks,                                             &
     &             kkrsz,natom,                                         &
     &             1,nblock,                                            &
     &             iprint,istop,logdet)
      tauden = exp(logdet)
      tdsign = aimag(logdet)

    if(debugflag) call endtiming()

    tauden = freefactor * tauden

      ! final tau = ( 1 - t G0 )^-1 t
      n = kkrsz*natom
      if(debugflag) call begintiming("evaltau // postmultiply t")
      do i = 1, natom
        call zgemm('N','N',kkrsz,kkrsz,kkrsz,dcmplx(1.d0,0.d0), &
          greenks(1+(i-1)*kkrsz:i*kkrsz,1+(i-1)*kkrsz:i*kkrsz),kkrsz,tcpa(1,1,i),size(tcpa,1),&
          dcmplx(0.d0,0.d0),taunum(1,1,i),size(taunum,1))
      end do
      if(debugflag) call endtiming()

      ! also compute: final G = G0 ( 1 - t G0 )^-1
      n = natom*kkrsz
      if(debugflag) call begintiming("evaltau // G = G0(1-tG0)^-1")
      do i = 1, natom
        call zgemm('N','N',kkrsz,kkrsz,n,dcmplx(1.d0,0.d0),&
          g0ks(1+(i-1)*kkrsz,1),n,greenks(1,1+(i-1)*kkrsz),n,&
          dcmplx(0.d0,0.d0),greenfn(1,1,i),size(greenfn,1))
      end do
      deallocate(g0ks)
      deallocate(greenks)
      if(debugflag) call endtiming()

    ! split tau into numerator and denominator parts
    taunum  = tauden * taunum
    greenfn = tauden * greenfn

    return
!EOC
  end subroutine evaltau

  ! Simple Math Helper Routines
  ! -----------------------------------------------------------------------

!BOP
!!IROUTINE: makenearest
!!INTERFACE:
  subroutine makenearest(z1,z2,x)
!!DESCRIPTION:
! brings Imag z1 close to Imag z2 in modulo x arithmetic
!
!EOP
!
!BOC
    complex(kind=cd), intent(inout) :: z1
    complex(kind=cd), intent(in) :: z2
    real(kind=dp), intent(in) :: x
    complex(kind=cd), parameter :: im = dcmplx(0.d0,1.d0)
    do while( dimag(z1-z2) > x/2.d0 )
      z1 = z1 - x*im
    end do
    do while( dimag(z1-z2) < -x/2.d0 )
      z1 = z1 + x*im
    end do
!EOC
  end subroutine makenearest

!BOP
!!IROUTINE: makeprimbas
!!INTERFACE:
  subroutine makeprimbas(avec)
!!REMARKS:
! not implemented
!EOP
!
!BOC
    implicit none
    real(kind=dp), intent(inout) :: avec(3,3)
    ! stub
!EOC
  end subroutine makeprimbas

!BOP
!!IROUTINE: center
!!INTERFACE:
  function center(v1, v2, v3)
!!DESCRIPTION:
! calculates center of triangle in 3d
!
!EOP
!
!BOC
    real(kind=dp) :: center(3)
    real(kind=dp), intent(in) :: v1(3), v2(3), v3(3)

    center = &
      (/ (v1(1)+v2(1)+v3(1))/3.d0, &
         (v1(2)+v2(2)+v3(2))/3.d0, &
         (v1(3)+v2(3)+v3(3))/3.d0  /)

!EOC
  end function center

!BOP
!!IROUTINE: midpoint
!!INTERFACE:
  function midpoint(v1, v2)
!!DESCRIPTION:
! calculates midpoint of segment in 3d
!
!EOP
!
!BOC
    real(kind=dp) :: midpoint(3)
    real(kind=dp) :: v1(3), v2(3)

    midpoint = &
       (/ (v1(1)+v2(1))/2.0d0, &
          (v1(2)+v2(2))/2.0d0, &
          (v1(3)+v2(3))/2.0d0  /)
!EOC
  end function midpoint

!BOP
!!IROUTINE: cross
!!INTERFACE:
  function cross(a, b)
!!DESCRIPTION:
! vector product
!
!EOP
!
!BOC
    real(kind=dp) :: cross(3)
    real(kind=dp), intent(in) :: a(3), b(3)
    cross(1) = a(2)*b(3)-a(3)*b(2)
    cross(2) = a(3)*b(1)-a(1)*b(3)
    cross(3) = a(1)*b(2)-a(2)*b(1)
!EOC
  end function cross

!BOP
!!IROUTINE: dot
!!INTERFACE:
  function dot(a, b)
!!DESCRIPTION:
! alias for dot\_product function,
! arguments {\tt a, b} are 3d vectors
!
!EOP
!
!BOC
    real(kind=dp) :: dot
    real(kind=dp), intent(in) :: a(3), b(3)
    dot = dot_product(a,b)
!    dot = a(1)*b(1) + a(2)*b(2) + a(3)*b(3)
!EOC
    end function dot

!BOP
!!IROUTINE: getcubicfit
!!INTERFACE:
  subroutine getcubicfit(x, f, a)
!!DESCRIPTION:
! returns lagrangian fit $g(x) = \sum( a(n) x^n, n = 0..3 )$
! so that {\it g(xi) = f(xi) for i = 1..4}

!EOP
!
!BOC
    implicit none

    real(kind=dp), intent(in) :: x(4)
    complex(kind=cd), intent(in)  :: f(4)
    complex(kind=cd), intent(out) :: a(0:3)
    complex(kind=cd) :: al,be,ga,de,ep

    al = f(1)/( (x(1)-x(2))*(x(1)-x(3))*(x(1)-x(4)) )
    be = f(2)/( (x(2)-x(1))*(x(2)-x(3))*(x(2)-x(4)) )
    ga = f(3)/( (x(3)-x(1))*(x(3)-x(2))*(x(3)-x(4)) )
    de = f(4)/( (x(4)-x(1))*(x(4)-x(2))*(x(4)-x(3)) )

    a(3) = al + be + ga + de

    ep = x(1)+x(2)+x(3)+x(4)
    a(2) = al*( x(1) - ep ) + &
           be*( x(2) - ep ) + &
           ga*( x(3) - ep ) + &
           de*( x(4) - ep )

    a(1) = al*(x(2)*x(3)+x(2)*x(4)+x(3)*x(4)) + &
           be*(x(1)*x(3)+x(1)*x(4)+x(3)*x(4)) + &
           ga*(x(1)*x(2)+x(1)*x(4)+x(2)*x(4)) + &
           de*(x(1)*x(2)+x(1)*x(3)+x(2)*x(3))

    a(0) = -al*( x(2)*x(3)*x(4) ) &
           -be*( x(1)*x(3)*x(4) ) &
           -ga*( x(1)*x(2)*x(4) ) &
           -de*( x(1)*x(2)*x(3) )

!EOC
  end subroutine getcubicfit

!BOP
!!IROUTINE: getquadfit
!!INTERFACE:
  subroutine getquadfit(x, f, a)
!!DESCRIPTION:
! returns lagrangian fit $g(x) = \sum( a(n) x^n, n = 0..2 )$,
! so that {\it g(xi) = f(xi) for i = 1..3}
!
!EOP
!
!BOC
    implicit none


    real(kind=dp), intent(in)  :: x(3)
    complex(kind=cd), intent(in)  :: f(3)
    complex(kind=cd), intent(out) :: a(0:2)
    complex(kind=cd) :: al,be,ga

    al = f(1)/( (x(1)-x(2))*(x(1)-x(3)) )
    be = f(2)/( (x(2)-x(1))*(x(2)-x(3)) )
    ga = f(3)/( (x(3)-x(1))*(x(3)-x(2)) )

    a(2) = al + be + ga
    a(1) = -al*(x(2)+x(3))-be*(x(1)+x(3))-ga*(x(1)+x(2))
    a(0) = al*x(2)*x(3) + be*x(1)*x(3) + ga*x(1)*x(2)

!EOC
  end subroutine getquadfit

!BOP
!!IROUTINE: getlinefit
!!INTERFACE:
  subroutine getlinefit(x, f, a)
!EOP
!
!BOC
    implicit none

    real(kind=dp), intent(in)  :: x(2)
    complex(kind=cd), intent(in)  :: f(2)
    complex(kind=cd), intent(out) :: a(0:1)

    a(1) = (f(2)-f(1))/(x(2)-x(1))
    a(0) = f(1) - x(1)*a(1)

!EOC
  end subroutine getlinefit

!BOP
!!IROUTINE: getquadroots
!!INTERFACE:
  subroutine getquadroots(a,b,c,z1,z2)
!EOP
!
!BOC
    implicit none

    complex(kind=cd), intent(in) :: a,b,c
    complex(kind=cd), intent(out) :: z1, z2
    complex(kind=cd) :: t

    t = cdsqrt(b*b-4.d0*a*c)
    t = -0.5d0*(b+t*sign(1.d0,dreal(b)*dreal(t) + dimag(b)*dimag(t)))
    z1 = t/a; z2 = c/t

!EOC
  end subroutine getquadroots

!BOP
!!IROUTINE: getcubicroots
!!INTERFACE:
  subroutine getcubicroots(c3,c2,c1,c0,z1,z2,z3)
!!DESCRIPTION:
! find roots of $x^3 + a x^2 + b x + c$ (i.e. monic form)
! drawn from Numerical Recipes (2nd ed., pg. 179)
!
!EOP
!
!BOC
    implicit none


    complex(kind=cd), intent(in) :: c3,c2,c1,c0
    complex(kind=cd), intent(out) :: z1,z2,z3
    complex(kind=cd) :: a,b,c,t,Al,Be,Q,R
    integer :: i, it = 3

    a = c2/c3; b = c1/c3; c = c0/c3

    Q = (a*a - 3.d0*b)/9.d0
    R = (2.d0*a*a*a - 9.d0*a*b + 27.d0*c)/54.d0

    t = cdsqrt(R*R-Q*Q*Q)
    t = sign(1.d0,dreal(R)*dreal(t)+dimag(R)*dimag(t))*t
    Al = -(R+t)**(1.d0/3.d0)
    Be = dcmplx(0.d0,0.d0)
    if(Al /= Be) Be = Q/Al

    Q = -(Al + Be)/2.d0 - a/3.d0
    R = (Al - Be)*dcmplx(0.d0,dsqrt(3.d0)/2.d0)
    z1 = Al + Be - a/3.d0
    z2 = Q + R
    z3 = Q - R

    ! brush up with Newton's method
    do i = 1, it
      Q = c0 + z1*(c1 + z1*(c2 + z1*c3))
      R = c1 + z1*(2.d0*c2 + z1*3.d0*c3)
      z1 = z1 - Q/R

      Q = c0 + z2*(c1 + z2*(c2 + z2*c3))
      R = c1 + z2*(2.d0*c2 + z2*3.d0*c3)
      z2 = z2 - Q/R

      Q = c0 + z3*(c1 + z3*(c2 + z3*c3))
      R = c1 + z3*(2.d0*c2 + z3*3.d0*c3)
      z3 = z3 - Q/R
    end do

!EOC
  end subroutine getcubicroots

!BOP
!!IROUTINE: dolongdiv
!!INTERFACE:
  subroutine dolongdiv(a,n,b,m)
!!DESCRIPTION:
! polinomial long division
!
! $\( a(n) x^n + ... + a(0) \) / \( b(m) x^m + ... + b(0) \)
!     = q(n-m) x^{n-m} + ... + q(0) + \\
!     \( r(m-1) x^{m-1} + ... + r(0) \) / \( b(m) x^m + ... + b(0) \)$
!
!   results are returned as
! {\bv
!     a(n .. m)   <-- q(n-m .. 0),
!     a(m-1 .. 0) <-- r(m-1 .. 0)
! \ev}
!EOP
!
!BOC
    implicit none

    integer, intent(in) :: n, m
    complex(kind=cd), intent(inout) :: a(0:n)
    complex(kind=cd), intent(in) :: b(0:m)
    integer :: i, j

    do i = n, m, -1
      a(i) = a(i)/b(m)
      do j = i-1, i-m, -1
        a(j) = a(j) - a(i)*b(j-i+m)
      end do
    end do

!EOC
    end subroutine dolongdiv

  ! Determine Brillouin Zone Geometry & Symmetry Routines
  ! -----------------------------------------------------------------------

!BOP
!!IROUTINE: getcellsymm
!!INTERFACE:
  subroutine getcellsymm(nwedges, wedgerays, nsymmops, symmops, &
    norbits, orbitlens, wedgeorbits, symmsubsets)
!!DESCRIPTION:
! finds wedge-specific cell symmetries
!

!!DO_NOT_PRINT
    implicit none
!!DO_NOT_PRINT

!!ARGUMENTS:
    integer, intent(in) :: nwedges
    real(kind=dp), intent(in) :: wedgerays(3,nwedges)

    integer, intent(in) :: nsymmops
    real(kind=dp), intent(in) :: symmops(49,3,3)

    integer, intent(out) :: norbits
    integer, pointer :: orbitlens(:)
    type(i1ptr), pointer :: wedgeorbits(:)
    type(i1ptr), pointer :: symmsubsets(:)
!!PARAMETERS:
    real(kind=dp), parameter :: symmtol = 1.d-10
!EOP
!
!BOC

    ! the wedges referred to here are defined as follows:
    ! (a) triangulate each face of cell about face center
    ! (b) a wedge is built from one such triangle plus cell center

    ! the symmetry operations define an action on the wedge normals
    ! this partitions the wedge normals into distinct wedge orbits:

    ! orbit1 --> norm11, norm12, ..., norm1N_1
    ! orbit2 --> norm21, norm22, ..., norm2N_2
    ! ...
    ! orbitM --> normM1, normM2, ..., normMN_M

    ! orbitlens = { N_1, N_2, ..., N_M }
    ! norbits = M

    logical :: flag(nwedges)
    integer :: torbitlens(nwedges)

    real(kind=dp) :: v(3),vw(3),s2,s2m
    integer :: i, j, k, m, n

    norbits = 0
    torbitlens = 0

    ! find how much room we need to store wedge ray orbits
    n = 0; flag = .false.
a:  do i = 1, nwedges

      if( flag(i) ) cycle a
      n = n + 1

      m = 0
b:    do j = 1, nsymmops
        v = matmul( symmops(j,:,:), wedgerays(:,i) )
        s2m = 1
c:      do k = 1, nwedges
          vw = v-wedgerays(:,k)
          s2 = dot(vw,vw)
          if( s2 < symmtol ) then
            if( .not. flag(k) ) then
              flag(k) = .true.
              m = m + 1
            end if
            cycle b
          end if
          s2m = min(s2m,s2)
          if( k == nwedges ) then
            write(6,*) ''
            write(6,*) ' rays are incompatible with symmetry operation j=',j
            write(6,'(3f10.5)') symmops(j,1,:)
            write(6,'(3f10.5)') symmops(j,2,:)
            write(6,'(3f10.5)') symmops(j,3,:)
            write(6,*) "DEBUG :: getcellsymm :: failed to reduce wedge normals or rays"
            write(6,'(a,g13.5)') ' min (v-wedgerays)^2 =',s2m
            call fstop("getcellsymm :: failed to reduce wedge normals or rays")
          end if
        end do c
      end do b
      torbitlens(n) = m

    end do a
    norbits = n

    if ( norbits>0 ) then

    ! make room for wedge ray orbits
     allocate( orbitlens(norbits), wedgeorbits(norbits), symmsubsets(norbits) )
     orbitlens(1:norbits) = torbitlens(1:norbits)
     do i = 1, norbits
      allocate( wedgeorbits(i)%p(orbitlens(i)) )
      allocate( symmsubsets(i)%p(orbitlens(i)) )
     end do

    ! produce and store wedge ray orbits
     n = 0; flag = .false.
d:   do i = 1, nwedges

      if( flag(i) ) cycle d
      n = n + 1

      m = 0
e:    do j = 1, nsymmops
        v = matmul( symmops(j,:,:), wedgerays(:,i) )
f:      do k = 1, nwedges
          vw = v-wedgerays(:,k)
          if( dot(vw,vw) < symmtol ) then
            if( .not. flag(k) ) then
              flag(k) = .true.
              m = m + 1
              wedgeorbits(n)%p(m) = k
              symmsubsets(n)%p(m) = j
            end if
            cycle e
          end if
!DEBUG          if( k == nwedges ) &
!DEBUG            call fstop("getcellsymm :: failed to reduce wedge normals or rays")
        end do f
      end do e

     end do d

    else
     orbitlens   => null()
     wedgeorbits => null()
     symmsubsets => null()
    end if  ! end of norbit-if

    ! to do: do orbits really partition wedges ?

!EOC
  end subroutine getcellsymm

!BOP
!!IROUTINE: triangulatecell
!!INTERFACE:
  subroutine triangulatecell(nfaces,faces,nwedges,wedgevert,wedgerays)

!!DO_NOT_PRINT
    implicit none
!!DO_NOT_PRINT

!!ARGUMENTS:
    integer, intent(in) :: nfaces
    type(face), pointer, intent(in) :: faces(:)
    integer, intent(out) :: nwedges
    real(kind=dp), pointer, intent(out) :: wedgevert(:,:,:)
    real(kind=dp), pointer, intent(out) :: wedgerays(:,:)
!!REMARKS:
! it is user's responsibility to deallocate wedges
!EOP
!
!BOC

    integer i,j,n
    real(kind=dp) :: v1(3),v2(3),v3(3)

    ! executable statements

    ! make room for wedge info
    nwedges = 0
    do i = 1, nfaces
      nwedges = nwedges + 2*faces(i)%nvert
    end do
    allocate( wedgevert(3,3,nwedges) )
    allocate( wedgerays(3,nwedges) )

    ! save wedge info
    n = 0
    do i = 1, nfaces

      ! find center of face for triangulation
      v3 = 0.0d0
      do j = 1, faces(i)%nvert
        v3 = v3 + faces(i)%vert(:,j)
      end do
      v3 = v3/dble(faces(i)%nvert)

      ! store vertices to each triangular wedge separately
      v1 = faces(i)%vert(:, faces(i)%nvert)
      do j = 1, 2*faces(i)%nvert

        if( mod(j,2) == 1 ) then
          if( j /= 1 ) then
            v2 = ( faces(i)%vert(:, floor(j/2.d0) ) + &
                   faces(i)%vert(:, ceiling(j/2.d0) ) )/2.d0
          else
            v2 = ( faces(i)%vert(:, faces(i)%nvert) + &
                   faces(i)%vert(:, 1) )/2.d0
          end if
        else
          v2 = faces(i)%vert(:, j/2)
        end if

        n = n + 1
        wedgevert(:,1,n) = v1
        wedgevert(:,2,n) = v2
        wedgevert(:,3,n) = v3
        wedgerays(:,n) = (v1+v2+v3)/3.d0

        v1 = v2

      end do

    end do

!EOC
  end subroutine triangulatecell

!BOP
!!IROUTINE: getwscell
!!INTERFACE:
  subroutine getwscell( avec, nfaces, faces )

!!DO_NOT_PRINT
    implicit none
!!DO_NOT_PRINT

!!ARGUMENTS:
    real(kind=dp), intent(in) :: avec(3,3)
    integer, intent(out) :: nfaces
    type(face), pointer, intent(out) :: faces(:)
!!PARAMETERS:
    integer, parameter :: maxvert = 15
    integer, parameter :: nwfaces = 26
!!REMARKS:
! it is user's responsibility to deallocate face vertices
!EOP
!
!BOC
    real(kind=dp) :: wvert(3,maxvert)
    real(kind=dp) :: wvec(3,3)
    real(kind=dp) :: L, u(3), v(3)
    real(kind=dp) :: planedir(3), planedis
    integer :: i, j, nvert

    ! algorithm begins with predefined working faces
    type(face), target, save :: wfaces(nwfaces)

    do i = 1, nwfaces
      nullify( wfaces(i)%vert )
    end do

    ! 1st order neighbors -- direct latt units
    wfaces( 1)%normal = (/ 1.0d0,  0.0d0,  0.d0/)
    wfaces( 2)%normal = (/ 0.0d0,  1.0d0,  0.d0/)
    wfaces( 3)%normal = (/ 0.0d0,  0.0d0,  1.d0/)
    wfaces( 4)%normal = (/-1.0d0,  0.0d0,  0.d0/)
    wfaces( 5)%normal = (/ 0.0d0, -1.0d0,  0.d0/)
    wfaces( 6)%normal = (/ 0.0d0,  0.0d0, -1.d0/)

    ! 2nd order neighbors
    wfaces( 7)%normal = (/ 1.0d0,  1.0d0,  0.d0/)
    wfaces( 8)%normal = (/ 0.0d0,  1.0d0,  1.d0/)
    wfaces( 9)%normal = (/ 1.0d0,  0.0d0,  1.d0/)
    wfaces(10)%normal = (/-1.0d0,  1.0d0,  0.d0/)
    wfaces(11)%normal = (/ 0.0d0, -1.0d0,  1.d0/)
    wfaces(12)%normal = (/-1.0d0,  0.0d0,  1.d0/)
    wfaces(13)%normal = (/ 1.0d0, -1.0d0,  0.d0/)
    wfaces(14)%normal = (/ 0.0d0,  1.0d0, -1.d0/)
    wfaces(15)%normal = (/ 1.0d0,  0.0d0, -1.d0/)
    wfaces(16)%normal = (/-1.0d0, -1.0d0,  0.d0/)
    wfaces(17)%normal = (/ 0.0d0, -1.0d0, -1.d0/)
    wfaces(18)%normal = (/-1.0d0,  0.0d0, -1.d0/)

    ! 3rd order neighbors
    wfaces(19)%normal = (/ 1.0d0,  1.0d0,  1.d0/)
    wfaces(20)%normal = (/ 1.0d0,  1.0d0, -1.d0/)
    wfaces(21)%normal = (/ 1.0d0, -1.0d0,  1.d0/)
    wfaces(22)%normal = (/ 1.0d0, -1.0d0, -1.d0/)
    wfaces(23)%normal = (/-1.0d0,  1.0d0,  1.d0/)
    wfaces(24)%normal = (/-1.0d0,  1.0d0, -1.d0/)
    wfaces(25)%normal = (/-1.0d0, -1.0d0,  1.d0/)
    wfaces(26)%normal = (/-1.0d0, -1.0d0, -1.d0/)

    ! ensure primitive basis <-- does nothing .. stub
    wvec = avec
    call makeprimbas(wvec)

    ! set initial size of square planes
    L = 0.0d0
    L = max(L, dot(wvec(:,1),wvec(:,1)))
    L = max(L, dot(wvec(:,2),wvec(:,3)))
    L = max(L, dot(wvec(:,2),wvec(:,3)))
    L = 1.1d0*dsqrt(6.0d0*L)

    ! for every working face ( clipping plane )
    do i = 1, nwfaces

      ! redefine normal in cartesian coords
      wfaces(i)%normal(:) = &
         wfaces(i)%normal(1)*wvec(:,1) + &
         wfaces(i)%normal(2)*wvec(:,2) + &
         wfaces(i)%normal(3)*wvec(:,3)

     ! set plane distance to half vertex distance
      wfaces(i)%normal = wfaces(i)%normal/2.0d0

    end do

    ! for every candidate face ( which begins as plane )
    nfaces = 0
a:  do i = 1, nwfaces

      ! choose vector not perpendicular to face
      if( i /= 1 .and. i /= 4 ) then
        u = wfaces(1)%normal
      else
        u = wfaces(2)%normal
      end if

      ! form orthogonal basis u,v,n where n = normal
      v = wfaces(i)%normal
      u = u - v*dot(u,v)/dot(v,v)
      u = u/dsqrt(dot(u,u))
      v = cross(u,v)
      v = v/dsqrt(dot(v,v))

      ! set initial face as large square
      nvert = 4
      wvert(:,1) = wfaces(i)%normal(:) + L*u(:)
      wvert(:,2) = wfaces(i)%normal(:) + L*v(:)
      wvert(:,3) = wfaces(i)%normal(:) - L*u(:)
      wvert(:,4) = wfaces(i)%normal(:) - L*v(:)

      ! clip face by all other planes
      ! if face ceases to exist, skip ahead
      do j = 1, nwfaces
        if( i == j ) cycle
        planedis = sqrt(dot(wfaces(j)%normal,wfaces(j)%normal))
        planedir = (1.0/planedis)*wfaces(j)%normal(:)
        call clipface(nvert, wvert, planedir, planedis)
        if( nvert == 0 ) cycle a
      end do

      ! if face survives, store face information
      nfaces = nfaces + 1
      allocate( wfaces(i)%vert(3,nvert) )
      wfaces(i)%nvert = nvert
      wfaces(i)%vert(1:3,1:nvert)  = wvert(1:3,1:nvert)

    end do a

    ! return face information
    allocate( faces(nfaces) )
    i = 0
    do j = 1, nwfaces
    if( associated(wfaces(j)%vert) ) then

      i = i + 1
      faces(i)%normal = wfaces(j)%normal
      faces(i)%nvert  = wfaces(j)%nvert
      faces(i)%vert  => wfaces(j)%vert

    end if; end do

!EOC
  end subroutine getwscell

!BOP
!!IROUTINE: clipface
!!INTERFACE:
  subroutine clipface(nvert, vert, planedir, planedist)

!!DO_NOT_PRINT
    implicit none
!!DO_NOT_PRINT

!!ARGUMENTS:
    integer, intent(inout) :: nvert
    real(kind=dp), intent(inout) :: vert(3,*)
    real(kind=dp), intent(in) :: planedir(3)
    real(kind=dp), intent(in) :: planedist
!!PARAMETERS:
! proximity tolerance for borderline cases
    real(kind=dp), parameter :: prxtol = 1.0d-08
    real(kind=dp), parameter :: divtol = 1.0d-06
!EOP
!
!BOC
    real(kind=dp) :: x, y
    real(kind=dp) :: v(3), w(3), plane(3)
    integer :: i, n, m

    plane(:) = planedist*planedir(:)
    if(nvert < 3) then
      write(6,*) "nvert < 3"
      nvert = 0; return
    end if

    ! find begin = v_m and end = v_n vertices inside half-plane
    ! if all inside or all outside, n = m = 0
    ! push borderline vertices outside
    n = 0; m = 0
    y = dot( vert(:,nvert)-plane, planedir )
    if( dabs(y) < prxtol ) y = 1.0d0
    do i = 1, nvert
      x = dot( vert(:,i)-plane, planedir )
      if( dabs(x) < prxtol ) x = 1.0d0
      if( x*y < 0.0d0 ) then
        if( x < 0.0d0 ) then
          m = i
          if( n /= 0 ) exit
        else
          n = i - 1
          if( n == 0 ) n = nvert
          if( m /= 0 ) exit
        end if
      end if
      y = x
    end do

    ! return on all inside or all outside cases
    if( n == 0 ) then
      if( x < 0.0d0 ) return
      if( x > 0.0d0 ) nvert = 0; return
    end if

    ! guard against shallow cut of one vertex
    if( m-n == 2 .or. n-m == nvert-2  ) then
      i = n+1; if( i > nvert ) i = 1
      x = dot(planedir,vert(:,i)-plane)
      if( dabs(x) < prxtol ) return
    end if

    ! cut line v_m-1 --> v_m
    i = m - 1
    if( i < 1 ) i = nvert
    x = dot( planedir,     plane - vert(:,m) )
    y = dot( planedir, vert(:,i) - vert(:,m) )
    x = x / y
    v = x*vert(:,i) + (1.0d0-x)*vert(:,m)
    if(dabs(y)<divtol) write(*,*) "clipface :: unstable division occured"


    ! cut line v_n --> v_n+1
    i = n + 1
    if( i > nvert ) i = 1
    x = dot( planedir,     plane - vert(:,i) )
    y = dot( planedir, vert(:,n) - vert(:,i) )
    x = x / y
    w = x*vert(:,n) + (1.0d0-x)*vert(:,i)
    if(dabs(y)<divtol) write(*,*) "clipface :: unstable division occured"

    ! compact vertices (must preserve cyclic order)
    if( m <= n ) then
      do i = 1, n-m+1
        vert(:,i) = vert(:,m+i-1)
      end do
      vert(:,n-m+2) = w
      vert(:,n-m+3) = v
      nvert = n-m+3
    else
      vert(:,n+1) = w
      if( m /= n+2 ) then
        vert(:,n+2) = v
        do i = n+3, n+3+nvert-m
          vert(:,i) = vert(:,m+i-n-3)
        end do
      else
        do i = nvert+1, m+1, -1
          vert(:,i) = vert(:,i-1)
        end do
        vert(:,n+2) = v
      end if
      nvert = n+3+nvert-m
    end if

!EOC
  end subroutine clipface


  ! Output and Debugging Dump Routines
  ! -----------------------------------------------------------------------

!BOP
!!IROUTINE: dumpcellgeom
!!INTERFACE:
  subroutine dumpcellgeom(avec, nfaces, faces)

!!ARGUMENTS:
    real(kind=dp), intent(in) :: avec(3,3)
    integer, intent(in) :: nfaces
    type(face), pointer, intent(in) :: faces(:)
!!REMARKS:
! fortran units 11, 12 are used for writing
!EOP
!
!BOC
    integer :: i,j

    open(unit=11,file='bzgeom.out.'//trim(gGenName()),status='replace',action='write',iostat=i)
    open(unit=12,file='bzsurf.dat.'//trim(gGenName()),status='replace',action='write',iostat=i)
    write(12,'(''# set size square'')')
    write(12,'(''# splot "./bzsurf.dat.'',a,''" w lp''/)') trim(gGenName())

    write(11,*) ''
    write(11,*) " ... Reciprocal Basis Vectors ... "
    write(11,*) ''
    write(11,'(a,f8.5,a,f8.5,a,f8.5,a)') &
      '  a1 = (',avec(1,1),',',avec(2,1),',',avec(3,1),')'
    write(11,'(a,f8.5,a,f8.5,a,f8.5,a)') &
      '  a2 = (',avec(1,2),',',avec(2,2),',',avec(3,2),')'
    write(11,'(a,f8.5,a,f8.5,a,f8.5,a)') &
      '  a3 = (',avec(1,3),',',avec(2,3),',',avec(3,3),')'

    write(11,*) ''
    write(11,*) " ... Brillouin Zone Cell ... "
    write(11,*) ''

    write(11,*) ''
    write(11,'(a,i2)')   '  num faces = ', nfaces
    if ( associated(faces) ) then
     do i = 1, nfaces

      write(11,*) ''
      write(11,'(a,f8.5,a,f8.5,a,f8.5,a)') '  normal = (', &
        faces(i)%normal(1),',', &
        faces(i)%normal(2),',', &
        faces(i)%normal(3),')'

      do j = 1, faces(i)%nvert
        write(11,'(a,i1,a,f8.5,a,f8.5,a,f8.5,a)') &
          '  v', j, ' = (', &
          faces(i)%vert(1,j),',', &
          faces(i)%vert(2,j),',', &
          faces(i)%vert(3,j),')'
         write(12,'(f20.15,f20.15,f20.15)') &
           faces(i)%vert(1,j), &
           faces(i)%vert(2,j), &
           faces(i)%vert(3,j)
      end do
      write(12,'(//)')

     end do
    end if

    call flush(11); call flush(12)
    close(unit=11); close(unit=12)

!    open(unit=4,file='bzsurf.gnu.'//gGenName(),status='replace',action='write',iostat=i)
!    write(4,*) 'set size square'
!    write(4,*) 'splot "./bzsurf.dat" w lp'
!    call flush(4); close(unit=4)

!EOC
  end subroutine dumpcellgeom

end module raymethod
