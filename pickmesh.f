!BOP
!!ROUTINE: pickmesh
!!INTERFACE:
      subroutine pickmesh(edu,enlim,nmesh,imesh)
!!DESCRIPTION:
! provides mesh number {\tt imesh} for E-contour integration
!

!!DO_NOT_PRINT
      implicit none
!!DO_NOT_PRINT

!!ARGUMENTS:
      integer nmesh,imesh
      complex*16 edu
      real*8 enlim(2,nmesh-1)
!EOP
!
!BOC
      integer i
      imesh = 1
      do i=1,nmesh-1
       if(dreal(edu).lt.enlim(1,i).or.                                  &
     &    abs(imag(edu)).gt.enlim(2,i)) then
        imesh = i+1
       end if
      end do
      return
!EOC
      end subroutine pickmesh
