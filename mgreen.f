!BOP
!!ROUTINE: mgreen
!!INTERFACE:
      subroutine mgreen(green,tau,kkrsz,                                &
     &                  nrelv,clight,                                   &
     &                  lmax,                                           &
     &                  energy,prel,                                    &
     &                  h,jmt,jws,rr,vr,                                &
     &                  iswzj,                                          &
     &                  iprint,istop)
!!DESCRIPTION:
! computes spherical part of Green's function, {\tt green}, defined by
! $\tau$({\tt energy})-matrix {\tt tau} and respective regular and irregular
! solutions of radial Schroedinger equation
!
!!USES:
!!!      use mecca_interface, only : semrelzj
!!!      use mtrx_interface, only  : wrtmtx

!!DO_NOT_PRINT
      implicit none
!!DO_NOT_PRINT

!!ARGUMENTS:
      complex(8), intent(out) :: green(:)  ! (iprpts)
      complex(8), intent(in)  :: tau(:,:)   ! (ipkkr,ipkkr)
      integer, intent(in) ::    kkrsz
      integer, intent(in) ::    nrelv
      real(8), intent(in) ::     clight
      integer, intent(in) ::    lmax
      complex(8), intent(in) :: energy
      complex(8), intent(in) :: prel
      real(8), intent(in) ::     h
      integer, intent(in) ::    jmt
      integer, intent(in) ::    jws
      real(8), intent(in) ::     vr(:)  ! (iprpts)
      real(8), intent(in) ::     rr(:)  ! (iprpts)
      integer, intent(in) ::    iswzj
      integer, intent(in) ::    iprint
      character(10), intent(in) ::  istop
!!REVISION HISTORY:
! Adapted - A.S. - 2013
!!REMARKS:
! it is an old subroutine; module "sctrng" shoud be considered for new development;
! DIRAC GF is not available: "small" component (f) is ignored.
!EOP
!
!BOC
!
      interface
        subroutine semrelzj(nrelv,clight,                               &
     &                  lmax,                                           &
     &                  energy,pnrel,                                   &
     &                  zlr,jlr,                                        &
     &                  h,jmt,jws,r,rv,                                 &
     &                  iswzj,                                          &
     &                  iprint,istop)
        implicit none
        integer, intent(in) :: nrelv,lmax
        real(8), intent(in) :: clight
        complex(8), intent(in) :: energy,pnrel
        complex(8) :: zlr(:,0:)  ! (iprpts,0:iplmax)
        complex(8) :: jlr(:,0:)  ! (iprpts,0:iplmax)
        real(8), intent(in) :: h
        integer, intent(in) :: jmt,jws,iswzj,iprint
        real(8), intent(in) :: r(:)
        real(8), intent(in) :: rv(:)
        character(10), intent(in) :: istop
        end subroutine semrelzj
      end interface
!
      interface
        subroutine wrtmtx(x,n,istop)
        implicit none
        integer, intent(in) :: n
        complex(8), intent(in) :: x(:,:)
        character(10), intent(in), optional :: istop
        end subroutine wrtmtx
      end interface
!
      character  sname*10
      parameter (sname='mgreen')
!c
      integer    ir
      integer    i,nrel
      integer    lcur
!c
      complex*16 zlr(size(green),0:lmax)  ! (iprpts,0:iplmax)
      complex*16 jlr(size(green),0:lmax)  ! (iprpts,0:iplmax)
      integer    lindex(kkrsz)
!c           lindex/0,
!c                  1,1,1,
!c                  2,2,2,2,2,
!c                  3,3,3,3,3,3,3,
!c                  ...
      if(iprint.ge.2) then
         write(6,'('' mgreen::      lmax='',i5)') lmax
         write(6,'('' mgreen::     iswzj='',i5)') iswzj
         write(6,'('' mgreen::    energy='',2d12.4)') energy
         write(6,'('' mgreen::      prel='',2d12.4)')   prel
         write(6,'('' mgreen::      tau:'')')
!c        -----------------------------------------------------------
         call wrtmtx(tau,kkrsz,istop)
!c        -----------------------------------------------------------
      endif
!c     ==============================================================
!c     get regular and irregular radial soln's.......................
!c     write(6,'('' mgreen:: calling semrelzj'')')
!c     --------------------------------------------------------------
      if ( nrelv<=1 ) then
       nrel=0
      else
       nrel=nrelv
      end if
      call semrelzj(nrel,clight,                                        &
     &            lmax,                                                 &
     &            energy,prel,                                          &
     &            zlr,jlr,                                              &
     &            h,jmt,jws,rr,vr,                                      &
     &            iswzj,                                                &
     &            iprint,istop)
!c     --------------------------------------------------------------
!c
!DEBUGPRINT
      if(iprint==2) then
!DEBUGPRINT
         do lcur=0,lmax
           write(6,'('' mgreen:: l='',i3)') lcur
           if ( iswzj .ne. 0 ) then
            write(6,'('' mgreen::ir,z*z,z*j:'',i4,4d12.4)')             &
     &               (ir,zlr(ir,lcur)*zlr(ir,lcur),                     &
     &                   zlr(ir,lcur)*jlr(ir,lcur),ir=1,jws,50)
           else
            write(6,'('' mgreen::ir,z*z:'',i4,2d12.4)')                 &
     &               (ir,zlr(ir,lcur)*zlr(ir,lcur),ir=1,jws,50)
           end if
         enddo
      endif
!c     ==============================================================
!c     calculate the green function for this (energy,species,sub-lat,
!c     spin) :: Spherical part only in this code.....................
!c     --------------------------------------------------------------
       i = 0
       do lcur=0,lmax
        lindex(i+1:i+1+2*lcur) = lcur
        i = i+1+2*lcur
       end do
!DEBUG
       if ( i.ne.kkrsz ) then
        write(6,*) ' DEBUG::  i= ',i,' lmax=',lmax,' kkrsz=',kkrsz
        call fstop(sname//' :: ERROR in lindex')
       end if

      green(:) = 0
      if(iswzj.eq.1) then
         do i=1,kkrsz
!C      For VP Integration
!DEBUG????          green(:) = green(:) + zlr(:,lindex(i))*jlr(:,lindex(i))
          green(:) = green(:) + zlr(:,lindex(i)) *                      &
     &                   ( tau(i,i)*zlr(:,lindex(i))-jlr(:,lindex(i)) )
         enddo
      else
       do i=1,kkrsz
!C      For VP Integration
        green(:) = green(:) + zlr(:,lindex(i))*zlr(:,lindex(i))*tau(i,i)
       enddo
      endif
!c     ==============================================================
!c     print if needed...............................................
!DEBUGPRINT
      if(iprint==2) then
!DEBUGPRINT
         write(6,'(//)')
         write(6,'('' mgreen:: ir,rr(ir),green(ir)'',i4,3d12.4)')       &
     &            (ir,rr(ir),green(ir),ir=1,size(green),50)
      endif
!c     ==============================================================
      if(istop.eq.sname) then
         call fstop(sname)
      endif
!c     ==============================================================
      return
      end subroutine mgreen
!
!
!
!c     ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
!BOP
!!ROUTINE: semrelzj
!!INTERFACE:
      subroutine semrelzj(nrelv,clight,                                 &
     &                  lmax,                                           &
     &                  energy,pnrel,                                   &
     &                  zlr,jlr,                                        &
     &                  h,jmt,jws,r,rv,                                 &
     &                  iswzj,                                          &
     &                  iprint,istop)
!!DESCRIPTION:
!   this subroutine solves scalar relativistic equations for complex
!   energies using Calogero's phase method and gets regular {\tt zlr} and,
!   optionally, irregular {\tt jlr} radial solutions
!   (single-scattering problem)  \\
!....based on ddj \& fjp version --- july 1991
!
!!USES:
      use mecca_constants
      use sctrng, only : scalar
!      use mecca_interface, only : scalar

!!DO_NOT_PRINT
      implicit real*8 (a-h,o-z)
!!DO_NOT_PRINT

!!ARGUMENTS:
      integer, intent(in) :: nrelv,lmax
      real(8), intent(in) :: clight
      complex(8), intent(in) :: energy,pnrel
      complex(8) :: zlr(:,0:)  ! (iprpts,0:iplmax)
      complex(8) :: jlr(:,0:)  ! (iprpts,0:iplmax)
      real(8), intent(in) :: h
      integer, intent(in) :: jmt,jws,iswzj,iprint
      real(8), intent(in) :: r(:)
      real(8), intent(in) :: rv(:)
      character(10), intent(in) :: istop
!!REVISION HISTORY:
! Adapted - A.S. - 2013
!!REMARKS:
! old non-module version,
! but empty spheres are always treated as non-relativistic
!EOP
!
!BOC
      character(:), allocatable :: coerr
!c
      real(8) :: r2(size(r))
!c
      complex(8) :: bjl(lmax+2,size(r))
      complex(8) :: bnl(lmax+2,size(r))
      complex(8) :: g(size(r))
      complex(8) :: f(size(r))
      complex(8) :: gpl(size(r))
      complex(8) :: fpl(size(r))
      complex(8) :: prel
      complex(8) :: pr
      complex(8) :: anorm
      complex(8) :: tandel
      complex(8) :: cotdel
      complex(8) :: alpha
!c parameters
!      complex*16 cone
!c     +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!      parameter (cone=(1.d0,0.d0))
      character(10), parameter :: sname='semrelzj'
      integer, save :: limitprint = 3
!c     +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!c
!c
!c     Gets radial integrals, t-matrix.
!c     If iswzj =1, then also gets the z*j interals............
!c
!c     In this version rv is r * v
!c
!c     called by: grint
!c     calls:     bessel,cqexp,cqxpup,csimp,zeroout
!c
!c      requires:
!c            lmax        lmax
!c            energy      energy
!c            iswzj       switch: if iswzj=1 gets z*j
!c            h,r       grid info (step for log-grid, r-grid)
!c
!c      returns:
!c            zlr         zl(r) properly normalized..............
!c            jlr         jl(r) properly normalized..............
!c
!c     set c for doing semi-relativistic or non-relativistic calc.
!c     .....c = speed of light in proper units..........
!c     Ryd units: m=1/2  hbar=1 c=2*inverse of fine structure const.
!c
!c     Solving a relativistic (nrelv=0) or non-relativistic (nrelv>0)
!c     problem.   If non-relativistic, then set e=e-elmass equal to e
!c     (where elmass = electron mass) and 1/c is set to ~zero to make
!c     relativistic terms zero.
!c
!c     Using Ricatti Bessel fcts. Need only l=0 and l=1 to get wave-fcts.
!c
!c     To get physical phase-shifts and normalization, need get Rl in
!c     terms of spherical bessel for all l's at the boundary of sphere.
!c     This is properly done in SCALAR, see clmt and slmt.
!c
!c     With M(r)= [1 + (e-v(r))*c2inv]
!c     Obtain r*(wave-fct.):   regular     g=r*R and   f=r*R'/M
!c                           irregular   gpl=r*R and fpl=r*R'/M
!c
!c     NOTE: ====>   need to get gderiv for mom. matrix elements
!c
!c     *****************************************************************
!c

!CAB      prel=sqrt( energy*(cone+energy/clight**2) )
!CAB??
!CAB??       What about sqrt(1+energy/clight^2) , when energy is complex  !????
!CAB??

!      zed = -(rv(1) + (rv(1)-rv(2))*r(1)/(r(2)-r(1)))
!      ized = nint(0.5d0*zed)
      ized = g_zed(rv,r)
!c     =================================================================
!DELETE      if(ized.lt.1 .and. nrelv.le.0) then
!DELETE!        if(iprint.ge.2.and.limitprint.gt.0) then
!DELETE!         limitprint=limitprint-1
!DELETE!         write(6,*) sname//':: ZED=',zed
!DELETE!         write(6,*) ' for this case (ZED<1) relativistic approach ',    &
!DELETE!     &              ' is not implemented'
!DELETE!        end if
!DELETE        prel = pnrel
!DELETE        nrel = 1
!DELETE      else
!c
!c Phaseshifts and wavefcts are calculated with relativsitic mom. -- prel
!c Whereas, because t_inv= i*Kappa + Kappa*cotdel, i*Kappa term must
!c cancel i*Kappa piece of Non-Rel g(k). Therefore, Kappa=sqrt(e) -- pnre !l.
!c
        prel = pnrel*sqrt(cone+energy/clight**2)
        nrel = nrelv
!DELETE      endif

      if(iprint.ge.2) then
         write(6,'('' semrelzj:         lmax='',i5)')lmax
         write(6,'('' semrelzj: nrel,clight='',i5,d12.4)')nrel,clight
         write(6,'('' semrelzj: energy,pnrel='',4d12.4)') energy,pnrel
         write(6,'('' semrelzj:  energy,prel='',4d12.4)') energy,prel
         write(6,'('' semrelzj:      jmt,jws='',2i5)') jmt,jws
      endif
!c     =================================================================
!c     for muffin-tins incrs and jmt should be the same

!CAB      iend=jmt+11                    ! this line is used in DDJ code

      if(jmt.gt.jws) then
       write(6,'(a10,'': jmt,jws='',2i6)') sname,jmt,jws
       call fstop(sname//': JMT > JWS ???')
      end if

      iend=size(r)
      if( iend .lt. jws ) then
        coerr = ' iend less than jws'
        write(6,'(2(2x,a))')coerr,sname
        write(6,'('' semrelzj: jmt,jws,iend='',3i5)') jmt,jws,iend
        call fstop(sname)
      endif
!      if( iend .gt. size(r) ) then
!        write(6,'('' semrel: iend,size(r)='',3i5)') iend,size(r)
!        coerr=' iend greater than array size'
!        write(6,'(2(2x,a))')coerr,sname
!        call fstop(sname)
!      endif
      do j=1,iend
          r2(j)=r(j)*r(j)
          pr=prel*r(j)
          bjl(1,j)= sin(pr)
          bnl(1,j)=-cos(pr)
          bjl(2,j)= bnl(1,j) + bjl(1,j)/pr
          bnl(2,j)=-bjl(1,j) + bnl(1,j)/pr
      enddo
!c     =================================================================
!c     solve scalar-relativistic or non-relativistic equation.
      do l=0,lmax
!c        ------------------------------------------------------------
!DELETE         call scalar(nrel,clight,                                       &
!DELETE     &               l,                                                 &
!DELETE     &               bjl,bnl,g,f,gpl,fpl,                               &
!DELETE     &               tandel,cotdel,anorm,alpha,                         &
!DELETE     &               energy,prel,                                       &
!DELETE     &               rv,r,r2,h,jmt,iend,                                &
!DELETE     &               iswzj,                                             &
!DELETE     &               iprint,istop)
         anorm = cone
         call scalar(nrel,clight,                                       &
     &               ized,                                              &
     &               l,                                                 &
     &               g,f,gpl,fpl,                                       &
     &               tandel,cotdel,alpha,                               &
     &               energy,prel,                                       &
     &               rv,r,r(jmt),iend,                                  &
     &               iswzj,.true.,                                      &
     &               iprint)
!c        ------------------------------------------------------------
!c
!c        -------------------------------------------------------------
!c        get zz ......................................................
         do j=1,iend
            zlr(j,l)= g(j)*anorm/r(j)
         enddo
!c        -------------------------------------------------------------
!c        get zj if needed.............................................
         if(iswzj.eq.1) then
            do j=1,iend
               jlr(j,l)= gpl(j)/r(j)
            enddo
         endif
      enddo
!c
!c     ================================================================
      if (istop.eq.sname) then
         call fstop(sname)
      else
         return
      endif
!c
      end subroutine semrelzj
