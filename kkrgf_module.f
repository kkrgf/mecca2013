!BOP
!!MODULE: kkrgf
!!INTERFACE:
      module kkrgf
!!DESCRIPTION:
! calculation of KKR Green's function related data,
! Fermi level, charge densities, potentials, {\em etc.}
!

!!USES:
      use mecca_constants
      use mecca_types
      use density, only : calcDensity,calcFreeElctrnCorr,dealloc_fe
      use potential, only : calcPotential
      use zcore, only : calcCore,max_core_level
      use zplane, only : calcZkkr
      use scf_io, only : saveSCF, sCurrParams
      use egrid_interface
      use update_interface
      use mecca_interface
      use gfncts_interface
      use mpi
!
!!REVISION HISTORY:
! Initial Version - A.S. - 2013
!EOP
!
!BOC
      implicit none
      integer, save :: ivers_tau = tau_scf
      integer, save :: ivers_pot = mecca_ames
!      integer, parameter :: ksint_scf_sch = 1
!      integer, parameter :: ksint_ray_sch = 2
      integer, parameter :: no_prev_data = -100

      external delete_file
! files prefixes
      character(8), parameter :: r_prefx='#mixrho.'
      character(8), parameter :: br1prefx='#br1rho.'
      character(8), parameter :: br2prefx='#br2rho.'
      character(8), parameter :: bp1prefx='#br1ptn.'
      character(8), parameter :: bp2prefx='#br2ptn.'

      complex(8), external :: TemprFun

      integer, parameter :: iSS=-1   ! ncpa=-1 (single-scattering flag)
      real(8), parameter :: mc2 = m_e/(mu0*eps0)   ! mc2 = m_e*clight**2
!EOC
      contains
!
!BOP
!!IROUTINE: scf_iteration
!!INTERFACE:
      subroutine scf_iteration( mecca_state, gf_only, efermi )
!!DESCRIPTION:
!   main module subroutine for single SCF iteration
!   (with or without Fermi level adjusting), also it can be used
!   to calculate Green's function related data only.
!

!!ARGUMENTS:
      type(RunState), intent(inout), target :: mecca_state
      logical, intent(in)           :: gf_only  ! if .true. then only GF is calculated
      real(8), optional, intent(inout) :: efermi
!      real(8), optional, intent(in) :: Tempr
!!REVISION HISTORY:
! Initial Version - A.S. - 2013
!EOP
!
!BOC

      type ( IniFile ), pointer :: inFile => null()
!      type ( RS_Str ),    pointer :: rs_box
!      type ( KS_Str ),    pointer :: ks_box
!      type ( EW_Str ),    pointer :: ew_box
!      type (VP_data),    pointer :: vp_box
!      type (GF_data),     pointer :: gf_box
      type(GF_output),  pointer :: gf_out => null()
      type(GF_output),  target :: gf2_out
      type(Work_data), save, target :: work0box

      integer :: nspin,itscf
!      integer :: nscf
      integer :: ndrpts,ndcomp,ndsubl
      integer :: ichZcor
      real(8) :: etop
!      logical :: fileexist=.false.
!      real(8) :: etopIn
      real(8) :: Tempr
      integer numef,iv,ntmp
      logical OK_to_stop
      integer :: ksint_sch,imix_rho
      logical :: forcedmix,do_etot,mecca_mpi

      ! MPI related variables
      integer :: ierr, myrank, nproc
      type(MPI_data), target :: mpi0box

      itscf = 0
      ksint_sch = ksint_scf_sch
      inFile => mecca_state%intfile
!      rs_box => mecca_state%rs_box
!      ks_box => mecca_state%ks_box
!      ew_box => mecca_state%ew_box
!      vp_box => mecca_state%vp_box
!      gf_box => mecca_state%gf_box
      gf_out => mecca_state%gf_out_box
!      bsf_box => mecca_state%bsf_box
!      fs_box => mecca_state%fs_box

      call mpi_initialized(mecca_mpi,ierr)
      if ( mecca_mpi ) then
       if ( .not.associated(mecca_state%mpi_box) ) then
        mecca_state%mpi_box => mpi0box
       end if
      end if

      call g_ndims(inFile,ndrpts,ndcomp,ndsubl,nspin)

      if ( associated(mecca_state%work_box) ) then
!        call deallocWorkdata(mecca_state%work_box)
      else
       mecca_state%work_box => work0box
       call allocWorkdata(mecca_state%work_box,                         &
     &                                    ndrpts,ndcomp,ndsubl,nspin)
      end if

      if ( present(efermi) ) then
        inFile%etop = efermi
      end if
      etop = inFile%etop
!DEBUG      nspin = inFile%nspin

!      ibot = 0      ! ebot is taken from inFile
      Tempr = inFile%Tempr
      if ( gf_only ) then
        ksint_sch = gKSintScheme(mecca_state)
        if ( ksint_sch ==  ksint_scf_sch ) then
         ivers_tau = tau_scf
        else
         ivers_tau = tau_ray
        end if
      else
        ivers_tau = tau_scf
        call calcCore( mecca_state,ichZcor,Tempr,etop )   !  ichZcor -- output: number of sublattice with Zcor_out < Zcor_in
        if ( inFile%iprint >= 1 ) call calcCanonicalBands(mecca_state)
      end if

      call gEnvVar('ENV_TAU',.true.,iv)
      if ( iv==0 ) then
        ivers_tau = tau_ray
      else if ( iv>0 ) then
        ivers_tau = tau_scf
      end if

!c        ==============================================================
!c        solve CPA over energy mesh: ..................................
!c        --------------------------------------------------------------

!      etopIn = etop
      numef = 0
      OK_to_stop = .false.

      if ( inFile%ebot>zero ) then
       OK_to_stop = .true.   ! Atomic calculations
       call allocGFoutput(gf_out,ndrpts,ndcomp,ndsubl,nspin)
       if ( Tempr==zero ) then
        gf_out%efermi = max_core_level
       else
        continue
       end if
      end if

      do while ( .not. OK_to_stop )  ! <140   continue>

       if ( Tempr == zero ) then
        call setEgrid( mecca_state, etop, zero )
        call calcZkkr(ivers_tau+divisor*ksint_sch, mecca_state, etop)    ! V_in --> val rho_out and DOS
       else
        mecca_state%gf_out_box%ms_gf = 0
        call setEgrid( mecca_state, etop, Tempr )
        call calcZkkr(ivers_tau+divisor*ksint_sch,mecca_state,etop,0)    ! (SS+(MS-SS))
!
!!        ntmp = inFile%ncpa
!!       inFile%ncpa = iSS
!!!
!!        mecca_state%gf_out_box => gf2_out
!!        mecca_state%gf_out_box%ms_gf = 2
!!        call setEgrid( mecca_state, etop, Tempr )
!!        call calcZkkr(ivers_tau+divisor*ksint_sch,mecca_state,etop,2)    ! (SS)
!!!
!!        inFile%ncpa = ntmp
!!        if ( inFile%ncpa .ne. iSS ) then
!!         mecca_state%gf_out_box => gf_out
!!         gf_out%ms_gf = 1
!!         call setEgrid( mecca_state, etop, Tempr )
!!         call calcZkkr(ivers_tau+divisor*ksint_sch,mecca_state,etop,1)   ! (MS-SS)
!!         call addGFout(gf_out,gf2_out)
!!         call deallocGFoutput(gf2_out,.true.)
!!        end if
!!        mecca_state%gf_out_box%ms_gf = 0
       end if
!

       if ( gf_only ) then
           return
       end if

       call calcFreeElctrnCorr(mecca_state,etop)
       call calcEfermi( mecca_state, etop, numef, OK_to_stop ) ! effind6() + efadjust
!?                                               to remove in future?
       if ( inFile%igrid == 0 ) then
!       it may happen if DOS calculations are enforced by e-grid parameters in input file
          return
       end if
!
       call mpi_bcast(OK_to_stop, 1, MPI_INTEGER, 0,                    &
     &       mpi_comm_world, ierr)

      end do  ! while

      if ( present(efermi) ) then
          efermi = gf_out%efermi
      end if

      call calcEntropy( mecca_state )

      call calcFreeElctrnCorr(mecca_state,gf_out%efermi)

      call calcDensity( mecca_state )  ! Rho_out, Q and check internal consistency

      call dealloc_fe

      if ( inFile%mtasa > 1 ) then
          ivers_pot = mecca_2013
      else
          ivers_pot = mecca_ames
      end if
!      call gEnvVar('ENV_M2013',.true.,iv)
!      if ( iv==0 ) then
!        ivers_pot = mecca_ames
!      else if ( iv>0 ) then
!        ivers_pot = mecca_2013
!      end if
      do_etot = .true.
      call calcPotential( ivers_pot, mecca_state, do_etot )    ! Rho, V[Rho] --> Etot
!
      call updateDensity( mecca_state, itscf, ichZcor.ne.0, imix_rho )   ! if rho-mixing

      if ( imix_rho >= 0 ) then
       do_etot = .false.
       call calcPotential( ivers_pot, mecca_state, do_etot )
      end if
                                                                         ! then Rho_in/Rho_out ==> Rho_next
      forcedmix = (ichZcor.ne.0) .or. (imix_rho==no_prev_data)
      call updatePotential( mecca_state, itscf, forcedmix )              ! if pot-mixing
                                                                         ! then V_in/V[Rho_next] ==> V_next
      if ( abs(mecca_state%work_box%dV0(1))>0.1d0 ) then
       gf_out%efermi = gf_out%efermi - mecca_state%work_box%dV0(1)       ! to decrease number of Ef-calculations
      end if
      mecca_state%intfile%V0(1:nspin)=mecca_state%work_box%vmtz(1:nspin)

      call saveSCF( mecca_state )

      if ( associated(mecca_state%work_box,work0box) ) then
        call deallocWorkdata(work0box)
        nullify(mecca_state%work_box)
      endif
      if ( associated(mecca_state%mpi_box,mpi0box) ) then
        nullify(mecca_state%mpi_box)
      endif
      nullify(inFile)

      return
!EOC
      end subroutine scf_iteration

!BOP
!!IROUTINE: updateDensity
!!INTERFACE:
      subroutine updateDensity( mecca_state, itscf, simplemix, imix  )
!
!!ARGUMENTS:
      type(RunState), intent(inout), target :: mecca_state
      integer, intent(out) :: itscf
      logical, intent(in)  :: simplemix
      integer, intent(out) :: imix
!!REVISION HISTORY:
! Initial Version - A.S. - 2013
!EOP
!
!BOC

      real(8), allocatable :: xvalold(:,:,:)  ! (ipcomp,ipsublat,ipspin)
      real(8), allocatable :: qtotmt(:,:,:)  ! (ipcomp,ipsublat,ipspin)
      real(8), allocatable :: rho(:,:,:,:)   ! (iprpts,ipcomp,ipsublat,ipspin)
      real(8), allocatable :: rhoold(:,:,:,:)   ! (iprpts,ipcomp,ipsublat,ipspin)
      real(8), allocatable :: vrold(:,:,:,:)   ! (iprpts,ipcomp,ipsublat,ipspin)
      real(8), allocatable :: rr(:,:,:)  !  (iprpts,ipcomp,nsublat)
      real(8), allocatable :: xr(:,:,:)  !  (iprpts,ipcomp,nsublat)
      real(8), allocatable :: rtmp(:),wrk1(:),vr_nucl(:)
      real(8) :: rScat
      type(SphAtomDeps), pointer :: v_rho_box => null()
      integer :: jmt(mecca_state%intfile%nsubl)
      integer :: jend(mecca_state%intfile%nsubl)
      integer    nsublat
      real(8), allocatable :: atcon(:,:)
      integer, allocatable :: komp(:)
!      integer    nbasis
      integer    nspin
      integer :: mtasa
      integer :: ic,is,nsub,ndcomp,ndrpts
      integer :: iprint
      character(*), parameter :: sname='updateDensity'
      character(120) :: brfile1,brfile2,rhofile
      logical :: fileexist=.false.

      integer, external :: indRmesh

      type ( IniFile ),   pointer :: inFile => null()
      type (GF_output),   pointer :: gf_out => null()
      type ( RS_Str ),    pointer :: rs_box => null()
      type (Work_data),   pointer :: work_box => null()    ! output (potential,energy)

      integer j,n1,n2,n3,n4
      real(8) :: v0old(2),zt,xvalt_new,xvalt_old
      real(8) :: alpha0,beta,h,qt,qc

      ! MPI related variables
      integer myrank, ierr

      ! distinguish root process
      call mpi_comm_rank(mpi_comm_world, myrank, ierr)

      itscf = 0
      inFile => mecca_state%intfile
      gf_out => mecca_state%gf_out_box
      rs_box => mecca_state%rs_box
      work_box => mecca_state%work_box

      if ( inFile%imixtype == 0 ) then ! rho-mixing
        imix = inFile%mixscheme
        alpha0 = inFile%alphmix
        beta   = inFile%betamix
      else
       imix = -1
       alpha0 = one
       beta   = one
      end if
      rhofile = r_prefx//inFile%genName
      brfile1 = br1prefx//inFile%genName
      brfile2 = br2prefx//inFile%genName
      inquire(file=rhofile,EXIST=fileexist)

      if( myrank == 0 ) then
        if( simplemix .or. .not.fileexist ) then
           call delete_file(brfile1)
           call delete_file(brfile2)
        end if
      endif
!MPIDEBUG      call log_mpi_sync(__FILE__,__LINE__)
      call mpi_barrier(mpi_comm_world, ierr)

      call g_ndims(inFile,ndrpts,ndcomp,nsublat,nspin)
      iprint = inFile%iprint

      call g_komp(inFile,komp)
      call g_atcon(inFile,atcon)

      mtasa = inFile%mtasa
      do nsub=1,nsublat
       jend(nsub) = maxval(inFile%sublat(nsub)%compon(1:komp(nsub))     &
     &                                        %v_rho_box%ndrpts)
      end do
      allocate(xr(1:ndrpts,1:ndcomp,1:nsublat))
      allocate(rr(1:ndrpts,1:ndcomp,1:nsublat))
      allocate(rho(1:ndrpts,1:ndcomp,1:nsublat,1:nspin))
      rho = zero
      allocate(vrold(1:ndrpts,1:ndcomp,1:nsublat,1:nspin))
      vrold = zero
      allocate(qtotmt(1:ndcomp,1:nsublat,1:nspin))
      qtotmt = zero

      do nsub=1,nsublat
       do ic=1,komp(nsub)
        v_rho_box => inFile%sublat(nsub)%compon(ic)%v_rho_box
        call g_meshv(v_rho_box,h,xr(:,ic,nsub),rr(:,ic,nsub))
        rScat = exp(v_rho_box%xmt)
        j = 1+indRmesh(rScat,ndrpts,rr(:,ic,nsub))
        if ( ic>1 .and. j.ne.jmt(nsub) ) then
          write(*,*) ' ic=',ic,' nsub=',nsub,' jmt=',jmt(nsub),j
          call fstop(sname//': ERROR :: r-meshes are not the same')
        else
         jmt(nsub) = j
        end if
        do is=1,nspin
          call g_vr(v_rho_box,is,vrold(:,ic,nsub,is),v0old(is))
          call g_rho(v_rho_box,is,rho(:,ic,nsub,is))
          call g_totchg(v_rho_box,.true.,is,qtotmt(ic,nsub,is),qc)
        end do
        nullify(v_rho_box)
       end do
      end do

      ! slaves will skip read operation
      if( myrank > 0 ) goto 100

      inquire(file=rhofile,EXIST=fileexist)
      if ( fileexist ) then
       open(11,file=rhofile,status='old',form='unformatted')
       read(11,err=100) itscf
       read(11,err=100) n1,n2,n3
       if(n1.ne.size(gf_out%xvalws,1).or.n2.ne.size(gf_out%xvalws,2).or.&
     &           n3.ne.size(gf_out%xvalws,3)) go to 100
       allocate(xvalold(1:size(gf_out%xvalws,1),1:size(gf_out%xvalws,2),&
     &                                         1:size(gf_out%xvalws,3)))
       read(11,err=100) xvalold
       read(11,err=100) n1,n2,n3,n4
       if ( n1.ne.size(rho,1) .or. n2.ne.size(rho,2) .or.               &
     &           n3.ne.size(rho,3) .or. n4.ne.size(rho,4) ) go to 100
       allocate(rhoold(1:size(rho,1),1:size(rho,2),                     &
     &                                     1:size(rho,3),1:size(rho,4)))
       do is=1,n4
        do nsub=1,n3
         do ic=1,n2
          if ( ic>komp(nsub) .or. nsub>nsublat .or. is>nspin ) then
           read(11,end=100)
          else
           read(11,end=100) rhoold(1:size(rhoold,1),ic,nsub,is)
          end if
         end do
        end do
       end do
       close(11)

       xvalt_new = zero
       xvalt_old = zero
       do nsub=1,nsublat
        do ic=1,komp(nsub)
         xvalt_new = xvalt_new + sum(gf_out%xvalws(ic,nsub,1:nspin))    &
     &                   * atcon(ic,nsub)*rs_box%numbsubl(nsub)
         xvalt_old = xvalt_old + sum(xvalold(ic,nsub,1:nspin))          &
     &                   * atcon(ic,nsub)*rs_box%numbsubl(nsub)
        end do
       end do
       if ( abs(xvalt_new-xvalt_old) > 0.5d0 ) then
        call delete_file(brfile1)
        call delete_file(brfile2)
       end if

       n1 = size(rho,1)
       n2 = size(rho,2)
       n3 = size(rho,3)
       if ( .not.allocated(work_box%qrms) ) then
        allocate(work_box%qrms(1:n2,1:n3,1:nspin))
       end if
       if ( .not.allocated(work_box%vrms) ) then
        allocate(work_box%vrms(1:n2,1:n3,1:nspin))
       end if
!       call maxdiff(n1,n2,n3,jmt,komp,nsublat,nspin,                    &
!     &                   rhoold,rho,work_box%qrms)

       work_box%vrms(:,nsublat+1:n3,1:nspin) = zero
       work_box%qrms(:,nsublat+1:n3,1:nspin) = zero
       allocate(rtmp(1:n1),wrk1(1:n1),vr_nucl(1:n1))
       do is=1,nspin
        do nsub=1,nsublat
         work_box%vrms(komp(nsub)+1:n2,nsub,is) = zero
         work_box%qrms(komp(nsub)+1:n2,nsub,is) = zero
         do ic=1,komp(nsub)
          zt = inFile%sublat(nsub)%compon(ic)%zID
!          vr_nucl(1:jmt(nsub) = -two*zt
          call twoZ(zt,rr(1:jmt(nsub),ic,nsub),jmt(nsub),vr_nucl)
          rtmp(1:jmt(nsub)) = rho(1:jmt(nsub),ic,nsub,is)*              &
     &     (work_box%vr(1:jmt(nsub),ic,nsub,is)-vr_nucl(1:jmt(nsub)))
          wrk1(1:jmt(nsub)) = rhoold(1:jmt(nsub),ic,nsub,is)*           &
     &           (vrold(1:jmt(nsub),ic,nsub,is)-vr_nucl(1:jmt(nsub)))
          rtmp(1:jmt(nsub)) = abs(rtmp(1:jmt(nsub))-wrk1(1:jmt(nsub)))
          call qexpup(0,rtmp(1:jmt(nsub)),jmt(nsub),xr(1:jmt(nsub),     &
     &                                                   ic,nsub),wrk1)
          work_box%vrms(ic,nsub,is) = wrk1(jmt(nsub))
!
!   "vrms" = integral { from 0 to R(jmt) | 4*pi*r^2*abs(delta(ChargeDens*V*r))/r }
!
          rtmp(1:jmt(nsub))=abs(rho(1:jmt(nsub),ic,nsub,is)-            &
     &                                  rhoold(1:jmt(nsub),ic,nsub,is))
          call qexpup(0,rtmp(1:jmt(nsub)),jmt(nsub),xr(1:jmt(nsub),     &
     &                                                   ic,nsub),wrk1)
          work_box%qrms(ic,nsub,is) = elctrn**2*wrk1(jmt(nsub))   ! elctrn=sqrt(2) is for Ry units
!
!   "qrms" = integral { from 0 to R(jmt) | 4*pi*r^2*abs(delta(ChargeDens))*2/r }
!
         end do
        end do
       end do
       deallocate(vrold)
       deallocate(rtmp,wrk1,vr_nucl)

       if ( imix >= 0 ) then
         call update_rho(rhoold,xvalold,rho,gf_out%xvalws,              &
     &                     qtotmt,xr,                                   &
     &                     imix,alpha0,beta,                            &
     &                     komp,jmt,jend,nsublat,                       &
     &                     atcon,rs_box%numbsubl(1:nsublat),nspin,      &
     &                     mtasa,iprint,brfile1,brfile2)
       end if
       if ( allocated(rhoold) ) deallocate(rhoold)
       if ( allocated(xvalold) ) deallocate(xvalold)
      else
        imix=no_prev_data
      end if  ! fileexist

100   continue ! all processes merge here

      ! exchange charge density across processes
      call mpi_bcast(rho(1,1,1,1), size(rho), MPI_DOUBLE_PRECISION, 0,  &
     &      mpi_comm_world, ierr)
      call mpi_bcast(qtotmt(1,1,1),size(qtotmt),MPI_DOUBLE_PRECISION,0, &
     &      mpi_comm_world, ierr)
      call mpi_bcast(work_box%qrms(1,1,1), size(work_box%qrms),         &
     &      MPI_DOUBLE_PRECISION, 0, mpi_comm_world, ierr)
      call mpi_bcast(work_box%vrms(1,1,1), size(work_box%vrms),         &
     &      MPI_DOUBLE_PRECISION, 0, mpi_comm_world, ierr)
      call mpi_bcast(itscf, 1,                                          &
     &      MPI_INTEGER, 0, mpi_comm_world, ierr)


      do nsub=1,nsublat
       do ic=1,komp(nsub)
        v_rho_box => inFile%sublat(nsub)%compon(ic)%v_rho_box
        do is=1,nspin
          call s_rho(v_rho_box,is,rho(:,ic,nsub,is))
          call s_totchg(v_rho_box,is,qtotmt(ic,nsub,is))
!DEBUG0912
          if ( mtasa .ne. 0 ) then
           call g_totchg(v_rho_box,.true.,is,qt,qc)
           gf_out%xvalws(ic,nsub,is) = qt-qc
          end if
!DEBUG0912
        end do
        nullify(v_rho_box)
       end do
      end do

      itscf = itscf+1

      ! only root node should write
      if( myrank == 0 ) then
        open(11,file=rhofile,status='unknown',form='unformatted')
        write(11) itscf
        write(11) size(gf_out%xvalws,1),size(gf_out%xvalws,2),          &
     &                                             size(gf_out%xvalws,3)
        write(11) gf_out%xvalws
        write(11) size(rho,1),size(rho,2),size(rho,3),size(rho,4)
        do is=1,size(rho,4)
         do nsub=1,size(rho,3)
          do ic=1,size(rho,2)
           if ( ic>komp(nsub) .or. nsub>nsublat .or. is>nspin ) then
            write(11) 0
           else
            write(11) rho(1:size(rho,1),ic,nsub,is)
           end if
          end do
         end do
        end do
        close(11)
      endif

      if( imix>3.and.mod(itscf-1,max(1,imix))==0 ) then
        if( myrank == 0 ) then
          call delete_file(brfile1)
          call delete_file(brfile2)
        end if
      endif

      deallocate(xr)
      deallocate(rr)
      deallocate(rho)
      deallocate(qtotmt)
      deallocate(komp)
      deallocate(atcon)
      nullify(work_box)

      ! don't leave until root finished processing
!MPIDEBUG      call log_mpi_sync(__FILE__,__LINE__)
      call mpi_barrier(mpi_comm_world, ierr)

      return
!EOC
      end subroutine updateDensity

!BOP
!!IROUTINE: updatePotential
!!INTERFACE:
      subroutine updatePotential( mecca_state, itscf, force_mix )
!
!!ARGUMENTS:
      type(RunState), intent(inout), target :: mecca_state
      integer, intent(in) :: itscf     ! iteration number used to control mixing procedure
      logical, intent(in) :: force_mix ! to enforce more conservative mixing procedure
!!REVISION HISTORY:
! Initial Version - A.S. - 2013
!EOP
!
!BOC

      real(8), allocatable :: ztot(:,:)  ! (ipcomp,ipsublat)
      real(8), allocatable :: vrold(:,:,:,:)   ! (iprpts,ipcomp,ipsublat,ipspin)
      real(8), allocatable :: rr(:,:,:)  !  (iprpts,ipcomp,nsublat)
      real(8), allocatable :: xr(:,:,:)  !  (iprpts,ipcomp,nsublat)
      real(8), allocatable :: rho(:,:,:,:)   ! (iprpts,ipcomp,ipsublat,ipspin)
      real(8), allocatable :: rtmp(:),wrk1(:)
      type(SphAtomDeps), pointer :: v_rho_box => null()
      integer :: jend(mecca_state%intfile%nsubl)
      integer :: jmt(mecca_state%intfile%nsubl)
      integer    nsublat
      real(8), allocatable :: atcon(:,:)
      integer, allocatable :: komp(:)
      integer    nspin
      integer :: ic,is,nsub,ndcomp,ndrpts
      integer :: iprint
!      character(:), allocatable :: sname='updatePotential'
      character(120) :: brfile1,brfile2
      character(120) :: bpfile1m,bpfile2m
      integer, parameter :: ifrc_max=3
      integer, save :: iforce

      type ( IniFile ),   pointer :: inFile => null()
      type (GF_output),   pointer :: gf_out => null()
      type (Work_data),   pointer :: work_box  => null()   ! output (potential,energy)

      integer, external :: indRmesh
!      real(8), external :: gMaxErr

      logical :: fileexist=.false.
      integer imix
      integer n1,n2,n3
      real(8) :: alpha0,beta,h,rglz
      real(8) :: dv0
      real(8) :: v0old(2),v0new(2)
      real(8), allocatable :: vCoul(:,:,:,:)
      real(8), allocatable :: vXC(:,:,:,:)

      ! MPI related variables
      integer myrank, ierr

      ! distinguish root process
      call mpi_comm_rank(mpi_comm_world, myrank, ierr)

      inFile => mecca_state%intfile
      gf_out => mecca_state%gf_out_box
      work_box => mecca_state%work_box

      if ( itscf > 1 ) then
       if ( force_mix ) iforce=iforce+1
       if ( iforce == ifrc_max ) then
        inFile%imixtype = 1
        inFile%alphmix = min(0.25d0,inFile%alphmix)
       end if
      else
       iforce = 0
      end if
      alpha0 = inFile%alphmix
      beta   = inFile%betamix
      if ( inFile%imixtype .ne. 0 ) then ! V-mixing
        if ( inFile%imixtype == 1 ) then
         imix = inFile%mixscheme
        else
         imix = 0
       end if
      else
       if ( force_mix ) then
        imix = 0
        alpha0 = 0.5d0*inFile%alphmix
        beta   = 0.5d0*inFile%betamix
       else
        imix = -1
       end if
      end if

      call g_ndims(inFile,ndrpts,ndcomp,nsublat,nspin)
      iprint = inFile%iprint

      call g_komp(inFile,komp)
      call g_atcon(inFile,atcon)

      do nsub=1,nsublat
       jend(nsub) = maxval(inFile%sublat(nsub)%compon(1:komp(nsub))     &
     &                                        %v_rho_box%ndrpts)
      end do

      allocate(ztot(1:ndcomp,1:nsublat))
      allocate(wrk1(1:ndrpts))
      allocate(xr(1:ndrpts,1:ndcomp,1:nsublat))
      allocate(rr(1:ndrpts,1:ndcomp,1:nsublat))
      allocate(vrold(1:ndrpts,1:ndcomp,1:nsublat,1:nspin))
      allocate(rho(1:ndrpts,1:ndcomp,1:nsublat,1:nspin))
      vrold = zero

      do nsub=1,nsublat
       do ic=1,komp(nsub)
        ztot(ic,nsub) = inFile%sublat(nsub)%compon(ic)%zID
        v_rho_box => inFile%sublat(nsub)%compon(ic)%v_rho_box
        call g_meshv(v_rho_box,h,xr(:,ic,nsub),rr(:,ic,nsub))
        jmt(nsub) = 1+indRmesh(exp(v_rho_box%xmt),ndrpts,rr(:,ic,nsub))
        do is=1,nspin
          call g_vr(v_rho_box,is,vrold(:,ic,nsub,is),v0old(is))
          call g_rho(v_rho_box,is,rho(:,ic,nsub,is))
        end do
        nullify(v_rho_box)
       end do
      end do
      v0old(1:nspin) = inFile%V0(1:nspin)

!      rhofile = r_prefx//inFile%genName
      brfile1 = bp1prefx//inFile%genName
      brfile2 = bp2prefx//inFile%genName
      if ( inFile%mixscheme == 3 ) then
       bpfile1m = bp1prefx//'M'//inFile%genName
       bpfile2m = bp2prefx//'M'//inFile%genName
      end if

      if( myrank == 0 ) then
        if ( imix==0 ) then
          call delete_file(brfile1)
          call delete_file(brfile2)
          if ( inFile%mixscheme == 3 ) then
           call delete_file(bpfile1m)
           call delete_file(bpfile2m)
          end if
        end if
      endif

!MPIDEBUG      call log_mpi_sync(__FILE__,__LINE__)
      call mpi_barrier(mpi_comm_world, ierr)

      work_box%dV0(1:nspin) = work_box%vmtz(1:nspin)-v0old(1:nspin)
      rglz = (0.4d0/max(0.4d0,sum(abs(work_box%dV0(1:nspin)))/nspin))
      if ( force_mix ) then
        alpha0 = alpha0 * rglz
      end if

      if ( itscf==1 .and. inFile%mtasa>1 ) then
       if ( inFile%io(nu_inpot)%status == 'n' ) then
        v0old(1:nspin) = work_box%vmtz(1:nspin)
       end if
      else
!
      end if

!      n1 = size(work_box%vr,1)
!      n2 = size(work_box%vr,2)
!      n3 = size(work_box%vr,3)
!      if ( .not.allocated(work_box%vrms) ) then
!       allocate(work_box%vrms(1:n2,1:n3,1:nspin))
!      end if
!!      call maxdiff(n1,n2,n3,jend,komp,nsublat,nspin,                    &
!!     &                   vrold,work_box%vr,work_box%vrms)
!
!       allocate(rtmp(1:n1),wrk1(1:n1))
!       do is=1,nspin
!        do nsub=1,nsublat
!         do ic=1,komp(n3)
!          rtmp(1:jmt(nsub))=abs(work_box%vr(1:jmt(nsub),ic,nsub,is)-    &
!     &                                 vrold(1:jmt(nsub),ic,nsub,is)) * &
!     &                      rho(1:jmt(nsub),ic,nsub,is)
!          call qexpup(0,rtmp(1:jmt(nsub)),jmt(nsub),xr(1:jmt(nsub),     &
!     &                                                   ic,nsub),wrk1)
!          work_box%vrms(ic,nsub,is) = wrk1(jmt(nsub))
!!
!!   "vrms" = integral { from 0 to R(jmt) | 4*pi*r^2*ChargeDens*abs(delta(V*r))/r }
!!
!         end do
!        end do
!       end do
!       deallocate(rtmp,wrk1)


      ! only root performs mixing
      if( myrank == 0 ) then

        if ( imix >= 0 .or. force_mix) then
          if ( inFile%nscf>1 ) then
           if ( inFile%ncpa == 0 ) then
            if ( itscf==1 ) alpha0=min(alpha0,0.03d0)
           else                             !  cpa
            if ( itscf==1 ) alpha0=min(alpha0,0.002d0)
           end if
          end if
          if ( inFile%mixscheme == 3 ) then  ! Coulomb + XC
           if ( nspin==1 .and. inFile%betamix== zero ) beta = alpha0

           if ( itscf==1 ) then
            call update_ptn(vrold,                                      &
     &                  work_box%vr,                                    &
     &                  ztot,                                           &
     &                     work_box%vmtz,v0old,rr,                      &
     &                     1,alpha0,beta,                               &
     &                     komp,jend,nsublat,                           &
     &                     nspin,brfile1,brfile2                        &
     &                     )
            call delete_file(brfile1)
            call delete_file(brfile2)
           else
!xc
            allocate(vXC(1:ndrpts,1:ndcomp,1:nsublat,4))
            do nsub=1,nsublat
             do ic=1,komp(nsub)
              if ( nspin==1 ) then
               vXC = zero
              else
!
               vXC(1:jend(nsub),ic,nsub,1) =                            &  ! old r*Vh
     &          ( vrold(1:jend(nsub),ic,nsub,1) -                       &
     &              vrold(1:jend(nsub),ic,nsub,nspin) ) * 0.5d0
               vXC(1:jend(nsub),ic,nsub,2) =                            &  ! old r*Vh
     &         -( vrold(1:jend(nsub),ic,nsub,1) -                       &
     &              vrold(1:jend(nsub),ic,nsub,nspin) ) * 0.5d0
!
               vXC(1:jend(nsub),ic,nsub,3) =                            &  ! new r*Vh
     &          (work_box%vr(1:jend(nsub),ic,nsub,1) -                  &
     &              work_box%vr(1:jend(nsub),ic,nsub,nspin)) * 0.5d0
               vXC(1:jend(nsub),ic,nsub,4) =                            &  ! new r*Vh
     &         -(work_box%vr(1:jend(nsub),ic,nsub,1) -                  &
     &              work_box%vr(1:jend(nsub),ic,nsub,nspin)) * 0.5d0
!
               vXC(jend(nsub)+1:,ic,nsub,:) = zero
              end if
             end do
            end do
            v0new = zero
!coul
            allocate(vCoul(1:ndrpts,1:ndcomp,1:nsublat,2))
            do nsub=1,nsublat
             do ic=1,komp(nsub)
              if ( nspin==1 ) then
!
               vCoul(1:jend(nsub),ic,nsub,1) =                           &  ! old r*Vh
     &          vrold(1:jend(nsub),ic,nsub,1)
!
                   vCoul(1:jend(nsub),ic,nsub,2) =                       &  ! new r*Vh
     &          work_box%vr(1:jend(nsub),ic,nsub,1)
!
                   else
!
               vCoul(1:jend(nsub),ic,nsub,1) =                           &  ! old r*Vh
     &          ( vrold(1:jend(nsub),ic,nsub,1) +                        &
     &              vrold(1:jend(nsub),ic,nsub,nspin) ) * 0.5d0
!
               vCoul(1:jend(nsub),ic,nsub,2) =                           &  ! new r*Vh
     &          (work_box%vr(1:jend(nsub),ic,nsub,1) +                   &
     &              work_box%vr(1:jend(nsub),ic,nsub,nspin)) * 0.5d0
!
              end if
              vCoul(jend(nsub)+1:,ic,nsub,:) = zero
             end do
            end do
!!!
            if ( nspin>1 ) then
            call update_ptn(vXC(:,:,:,1:nspin),vXC(:,:,:,3:),           &
     &                  ztot*zero,                                      &
     &                     v0new,v0new,rr,                              &
     &                     2,beta,beta,                                 &
     &                     komp,jend,nsublat,                           &
     &                     nspin,bpfile1m,bpfile2m                      &
     &                     )
            end if
            call update_ptn(vCoul(:,:,:,1:1),vCoul(:,:,:,2:2),          &
     &                  ztot,                                           &
     &                     work_box%vmtz,v0old,rr,                      &
     &                     1,alpha0,beta,                               &
     &                     komp,jend,nsublat,                           &
     &                     1,brfile1,brfile2                            &
     &                     )
!!!
            do nsub=1,nsublat
             do ic=1,komp(nsub)
!
              work_box%vr(1:jend(nsub),ic,nsub,1) =                     &
     &        ( vCoul(1:jend(nsub),ic,nsub,2) +                         &
     &          vXC(1:jend(nsub),ic,nsub,3) )
              if ( nspin>1 ) then
               work_box%vr(1:jend(nsub),ic,nsub,2) =                    &
     &         ( vCoul(1:jend(nsub),ic,nsub,2) +                        &
     &           vXC(1:jend(nsub),ic,nsub,4) )
              end if
!
             end do
            end do
            if ( allocated(vCoul) ) deallocate(vCoul)
            if ( allocated(vXC) ) deallocate(vXC)
           end if
           call sCurrParams(alpha=alpha0,beta=beta)
          else
            call update_ptn(vrold,                                      &
     &                  work_box%vr,                                    &
     &                  ztot,                                           &
     &                     work_box%vmtz,v0old,rr,                      &
     &                     imix,alpha0,beta,                            &
     &                     komp,jend,nsublat,                           &
     &                     nspin,brfile1,brfile2                        &
     &                     )
          end if
        else   ! imix<0 .and. .not.force_mix
          continue  ! no mixing (contribution from older potentials is 0)
        end if
      endif

      ! exchange potential across nodes
      call mpi_bcast(work_box%vr(1,1,1,1), size(work_box%vr),           &
     &  MPI_DOUBLE_PRECISION, 0, mpi_comm_world, ierr)
      call mpi_bcast(work_box%vmtz(1), size(work_box%vmtz),             &
     &  MPI_DOUBLE_PRECISION, 0, mpi_comm_world, ierr)

      do nsub=1,nsublat
       do ic=1,komp(nsub)
        v_rho_box => inFile%sublat(nsub)%compon(ic)%v_rho_box
        do is=1,nspin
            call s_vr(v_rho_box,is,work_box%vr(:,ic,nsub,is),           &
     &                                                work_box%vmtz(is))
        end do
        nullify(v_rho_box)
       end do
      end do
      work_box%dV0(1:nspin) = work_box%vmtz(1:nspin)-v0old(1:nspin)      ! update after mixing

      if( imix>=4 .and. mod(itscf,max(1,imix)).eq.0 ) then
        if( myrank == 0 ) then
          call delete_file(brfile1)
          call delete_file(brfile2)
        end if
      endif

      deallocate(wrk1)
      deallocate(ztot)
      deallocate(xr)
      deallocate(rr)
      deallocate(komp)
      deallocate(atcon)
      nullify(work_box)

      ! syncronize before exiting

!MPIDEBUG      call log_mpi_sync(__FILE__,__LINE__)
      call mpi_barrier(mpi_comm_world, ierr)
      return
!EOC
      end subroutine updatePotential
!
!BOP
!!IROUTINE: calcEntropy
!!INTERFACE:
      subroutine calcEntropy( mecca_state )
!!REVISION HISTORY:
! Initial Version - A.S. - 2013
!EOP
!
!BOC
      implicit none
      type(RunState), intent(inout), target :: mecca_state
      type ( IniFile ),   pointer :: inFile => null()
      type (GF_output),   pointer :: gf_out => null()
      type (Work_data),   pointer :: work_box  => null()   ! output
      type(SphAtomDeps),  pointer :: v_rho_box => null()
      real(8) :: avnatom
      real(8) :: entrp,e0,ef,T,ec,bt,ffz
      integer :: nsub,nspin,ic,is,ie
      complex(8) :: z

      if ( mecca_state%intfile%Tempr <= zero ) return
!      real(8), external :: g_numNonESsites
      inFile   => mecca_state%intfile
      gf_out   => mecca_state%gf_out_box
      work_box => mecca_state%work_box

      avnatom = g_numNonESsites( inFile )

      call entrpterm(inFile%Tempr,gf_out%efermi,                        &
     &                     gf_out%egrd(1:gf_out%nume),                  &
     &                     gf_out%wgrd(1:gf_out%nume),                  &
     &                     gf_out%nume,gf_out%dostspn(:,:),             &
     &                     gf_out%dostspn(:,gf_out%nume),               &
     &                     inFile%nspin,avnatom,work_box%entropy,       &
     &                     inFile%iprint,inFile%istop)

       nspin = inFile%nspin
       entrp = zero
       e0 = min(zero,inFile%ebot)
       ef = gf_out%efermi
       T = inFile%Tempr
       do nsub=1,inFile%nsubl
        do ic=1,inFile%sublat(nsub)%ncomp
         v_rho_box => inFile%sublat(nsub)%compon(ic)%v_rho_box
          do is=1,nspin
           do ie=v_rho_box%numc(is),1,-1
            ec = v_rho_box%ec(ie,is)
            if ( ec < e0 ) then
             if ( T>zero ) then
              bt=(ec-ef)/T
              z = dcmplx(bt,zero)
              bt = dreal(TemprFun(z,0))
              ffz = dreal(TemprFun(z,1))
              if ( bt>epsilon(one) .and. abs(ffz)>epsilon(one) ) then
               entrp = entrp + abs(v_rho_box%kc(ie,is))*ffz
              end if
             end if
            end if
           end do
          end do
          work_box%entropy = work_box%entropy + (3-nspin)*entrp
        end do
       end do
       if(inFile%iprint.ge.-1) then
        write(6,'(''     Entropy per atom:          '',                 &
     &          t40,''='',g18.11)') work_box%entropy/avnatom
        write(6,'(''    -Tempr*Entropy per atom (Ry):'',                &
     &          t40,''='',g18.11)') -T*work_box%entropy/avnatom
        write(6,'(''     +++++++++++++++++++++++++++++++++++'',         &
     &           ''++++++++++++++++++++++++++++++++++'')')
       endif

      nullify(work_box)
      nullify(gf_out)
      nullify(inFile)
      nullify(v_rho_box)
      return
!EOC
      end subroutine calcEntropy
!
!BOP
!!IROUTINE: calcEfermi
!!INTERFACE:
      subroutine calcEfermi( mecca_state,etop,numef,OK_to_stop ) ! effind6() + efadjust
!!DESCRIPTION:
! estimate of Fermi level position ( {\tt etop} )
!
!!USES:
      use density, only : qCell_fe,qws_fe
!
!!REVISION HISTORY:
! Initial Version - A.S. - 2013
!EOP
!
!BOC
      type(RunState), intent(inout), target :: mecca_state
      real(8), intent(inout) :: etop
      integer             :: numef
      logical             :: OK_to_stop

      type ( IniFile ),   pointer :: inFile => null()
      type (RS_str),      pointer :: rs_box => null()
      type (GF_output),   pointer :: gf_out => null()
      type (Work_data),   pointer :: work_box  => null()   ! output (potential,energy)

      integer nspin,ndcomp,nsublat
      integer nsub,is,ic,ne,zcmx
      integer, allocatable :: komp(:)
      real(8), allocatable :: atcon(:,:)
      real(8) :: efermi,efnew
      real(8) :: xvaltot,xtws,delt_xval,deltQ
      real(8) :: dQ,dq_fe,rho0fe
      real(8) :: estim_err,dE,dx
      complex(8) :: cdost_ef
      logical :: non_zero_T
!      logical :: not_good_enough_rho
!      real(8), external :: gMaxErr
!      real(8), external :: g_zvaltot

      integer :: nefmax       ! max number of Ef calcs per SCF iter
      real(8), parameter :: defmax=0.15d0   ! to limit change in Ef for lousy guesses
      real(8) :: d_efmx,demin,x1,x2,f1,f2,cg,fF
      real(8), external :: fermiFun
      real(8), allocatable, save :: efwork(:),dZwork(:)
      real(8), allocatable :: p_real(:)
      integer, allocatable :: t_work(:),p_int(:)

      inFile => mecca_state%intfile
      rs_box => mecca_state%rs_box

      gf_out => mecca_state%gf_out_box
      work_box => mecca_state%work_box

      non_zero_T = .true.
      xvaltot = g_zvaltot(inFile,non_zero_T) !DEBUG  - g_zsemckkr(mecca_state)

      zcmx = zero
      do nsub=1,inFile%nsubl
       do ic=1,inFile%sublat(nsub)%ncomp
        zcmx = max(zcmx,inFile%sublat(nsub)%compon(ic)%zcor)
       end do
      end do

      if ( abs(xvaltot)<qtol*zcmx) then
       OK_to_stop = .true.
       etop = zero
       gf_out%efermi = zero
       return       ! no valence charge
      end if

      nefmax = gNefmax()
      if ( allocated(efwork) ) then
       if ( size(efwork)<nefmax ) then
         allocate(p_real(nefmax))
         p_real(1:size(efwork)) = efwork
         call move_alloc(p_real,efwork)
         allocate(p_real(nefmax))
         p_real(1:size(dZwork)) = dZwork
         call move_alloc(p_real,dZwork)
       end if
      else
       allocate(efwork(nefmax),dZwork(nefmax))
      end if
      if ( allocated(t_work) ) then
       if ( size(t_work)<nefmax ) then
         allocate(p_int(0:nefmax))
         p_int(0:size(t_work)-1)=t_work
         call move_alloc(p_int,t_work)
       end if
      else
       allocate(t_work(0:nefmax))
      end if

      if ( numef==0 ) then
       efwork(:) = zero
       dZwork(:) = zero
       t_work(:) = -1
       t_work(0) = 1
      end if

      nspin = inFile%nspin
      call g_komp(inFile,komp)
      call g_atcon(inFile,atcon)
      ndcomp = size(atcon,1)
      nsublat = size(atcon,2)
!
      if(inFile%iprint.ge.0) then
       do is=1,nspin
        do nsub=1,nsublat
         do ic=1,komp(nsub)
          write(6,'(''      Efermi: dosint '',2i4,2d17.8)')             &
     &                        nsub,ic,gf_out%xvalws(ic,nsub,is)
         enddo
        enddo
       enddo
      end if
!
      xtws=zero
      do is=1,nspin
       do nsub=1,nsublat
        do ic=1,komp(nsub)
         xtws = xtws + gf_out%xvalws(ic,nsub,is) *                      &
     &                  (rs_box%numbsubl(nsub)*atcon(ic,nsub))
        enddo
       enddo
      enddo

      dq_fe = zero
      if ( qCell_fe>zero ) then        ! free-electron higher L contribution
       if ( associated(qws_fe) ) then
        rho0fe = (four*pi/three)*qCell_fe/rs_box%volume
        do nsub=1,nsublat
         do ic=1,komp(nsub)
!          gf_out%xvalws(ic,nsub,1:nspin)=gf_out%xvalws(ic,nsub,1:nspin)+&
!     &            (rho0fe*rs_box%rws(nsub)**3 - qws_fe(ic,nsub))/nspin
          dq_fe = dq_fe + (rs_box%numbsubl(nsub)*atcon(ic,nsub)) *      &
     &             (rho0fe*rs_box%rws(nsub)**3 - qws_fe(ic,nsub))
         enddo
        enddo
        xvaltot = xvaltot - dq_fe
       end if
      end if

      dQ = zero    ! non-spherical contribution  (e.g. VP\ASA)

      xtws = xtws + dQ

      delt_xval = xvaltot - xtws

      estim_err=gMaxErr(0)
!TODO: interpolation for each species?      cdost_ef = cIntrplDos(mecca_state,inFile%igrid,etop,              &
!TODO     &                                               dcmplx(etop,zero))
      cdost_ef = cIntrplDos(mecca_state,0,etop,dcmplx(etop,zero))
!
      if(inFile%iprint.ge.-1) then
       write(6,*)
       write(6,'(''    Zvaltot'',t40,''='',f18.11)') xvaltot+dq_fe
       if ( qCell_fe == zero ) then
        write(6,'(''    Total N(etop)'',t40,''='',f18.11)') xtws
       else
        write(6,'(''    Total N(etop)'',t40,''='',f18.11,3x,            &
     &      ''including '',f12.7,'' free-electrons'')') xtws+dq_fe,dq_fe
       end if
       write(6,'(''    Zvaltot - N(etop)'',t40,''='',g20.11)') delt_xval
       write(6,'(''    Tot_DOS(~Ef) per cell'',t40,''='',2d19.10)')     &
     &                                                        cdost_ef
      end if
!
      if ( xtws < -1.d-6*xvaltot ) then
       write(6,'(a)')                                                   &
     &   'calcEfermi: WARNING : accuracy is lost, Int{n(e)} < ZERO ????'
      else if ( xtws>100*max(one,xvaltot) ) then
!DELETE        if ( inFile%Tempr > 0 ) then
!DELETE          if ( etop > inFile%ebot .and. inFile%Tempr<etop-inFile%ebot ) &
!DELETE     &    then
!DELETE       write(6,'(''calcEfermi: WARNING : Int{n(e)} is too big ????'')')
!DELETE          end if
!DELETE        else
       write(6,'(''calcEfermi: WARNING : Int{n(e)} is too big ????'')')
!DELETE         call fstop('calcEfermi: ERROR : Int{n(e)} is too big ????')
!DELETE        end if
      end if
!
      OK_to_stop = .false.
      deltQ = delt_xval
      call efTemprFind(numef,mecca_state,etop,                          &  ! does nothing for zero and small T
     &               xvaltot,deltQ,cdost_ef,                            &
     &               efnew)
      t_work(numef) = 0
      OK_to_stop = deltQ==zero
!DEBUG
      if ( inFile%Tempr .ne. zero ) then
       fF = fermiFun((etop-efnew)/inFile%Tempr)
       if ( fF < epsilon(etop) ) then
        if ( efnew > zero ) then
         write(6,*) '(etop-efnew)*aimag(cdost_ef)=',                    &
     &                          (etop-efnew)*aimag(cdost_ef)
         write(6,*) ' deltQ,numef=',deltQ,numef
         OK_to_stop = (etop-efnew)*aimag(cdost_ef) < abs(deltQ)*1.d-2
         numef = -numef
        end if
       end if
      end if
!DEBUG
      if ( .not. OK_to_stop ) then
!       efermi = etop
       efermi = efnew
       deltQ = delt_xval
!       d_efmx = max(defmax,0.2d0*abs(etop),inFile%Tempr)
       d_efmx = max(defmax+inFile%Tempr,0.2d0*abs(etop))
       call effind2(t_work(numef),inFile%ebot,efermi,                   &
     &               xvaltot,deltQ,cdost_ef,estim_err,                  &
     &               d_efmx,                                            &
     &               efnew)
       t_work(numef) = 1
       if ( inFile%Tempr > zero ) then
!DELETE        if ( efnew < -m_e/(mu0*eps0) ) then
!DELETE         efnew = -m_e/(mu0*eps0)       ! m_e*clight**2
!DELETE         numef = -1
!DELETE         if(inFile%iprint.ge.-1) then
!DELETE          write(6,'(a)')                                                 &
!DELETE     &      'WARNING: efnew is not allowed to be smaller m_e*clight**2'
!DELETE         end if
!DELETE        end if
        if ( abs(efermi-efnew) < estim_err/100 ) numef=-1
!??? .and. abs(delt_xval) > 100*qtol*xvaltot
       end if
      end if
      efermi = efnew
      if(numef.ge.0. .and. numef.lt.nefmax) then
       numef = numef +1
       efwork(numef) = etop
       dZwork(numef) = delt_xval

       if ( numef>1 ) then
        x2 = huge(x2)
        x1 = -x2
        do ic=1,numef
         if ( dZwork(ic)>=zero ) then
          x1 = max(x1,efwork(ic))
          f1 = dZwork(ic)
         else
          x2 = min(x2,efwork(ic))
          f2 = dZwork(ic)
         end if
        end do
        if(x1>= minval(efwork(1:numef)).and.x2<=maxval(efwork(1:numef)))&
     &     then
          if ( inFile%Tempr==zero ) then
           if ( mod(numef,3) == 1 .or. numef>15 ) then
            efermi = (x1+x2)/2
           else
            if ( x1<zero .and. abs(f1)>abs(f2) ) then
             cg = one + log(abs(f1)/max(qtol,abs(f2)))
            else
             cg = one
            end if
            dx = cg*f2-f1
            if ( abs(dx)<1.d-7 ) dx=sign(1.d-7,dx)
            efermi = (cg*f2*x1-f1*x2)/dx
!            efermi = (cg*f2*x1-f1*x2)/max(cg*f2-f1)
           end if
          else
           cg = one
           dx = cg*f2-f1
           if ( abs(dx)<1.d-7 ) dx=sign(1.d-7,dx)
           efermi = (cg*f2*x1-f1*x2)/dx
!           efermi = (cg*f2*x1-f1*x2)/(cg*f2-f1)
          end if
        end if
       end if

       if ( numef==1 ) estim_err = etol
       call check_conv(etop,efermi,xvaltot,delt_xval,estim_err,numef)
       if(numef.eq.nefmax) numef = 0
      else
       numef = -nefmax*100-1
      end if

      if ( inFile%Tempr==zero ) then
       OK_to_stop = numef<=0 .or. abs(efermi-inFile%ebot)<gEfTol()/100
       if ( efermi < inFile%ebot ) then
        efermi = (etop+inFile%ebot)/2
       end if
      else
       OK_to_stop = numef<=0
      end if
!
      if ( OK_to_stop ) then
        gf_out%efermi = efermi
        write(6,'(/13x,''    Fermi energy'',t40,''='',f18.11/)') efnew
        if ( inFile%Tempr==zero ) then
         call  efadjust2(efermi,etop,                                   &
     &               gf_out%xvalws,gf_out%xvalmt,                       &
     &               gf_out%doslast,gf_out%doscklast,                   &
     &               gf_out%evalsum,xvaltot,dQ,                         &
     &               nsublat,nspin,komp,atcon,                          &
     &               rs_box%numbsubl(1:nsublat),                        &
!CMULTI     &               XYZmltpl,ndmltp,XYZforce,                          &
     &               inFile%iprint,inFile%istop)
        end if
!
      else
         etop = efermi
      end if
      if ( qCell_fe>zero ) then        ! free-electron higher L contribution
       if ( associated(qws_fe) ) then
        rho0fe = (four*pi/three)*qCell_fe/rs_box%volume
        do nsub=1,nsublat
         do ic=1,komp(nsub)
          gf_out%xvalws(ic,nsub,1:nspin)=gf_out%xvalws(ic,nsub,1:nspin)+&
     &            (rho0fe*rs_box%rws(nsub)**3 - qws_fe(ic,nsub))/nspin
         enddo
        enddo
       end if
      end if
      deallocate(komp)
      deallocate(atcon)
      nullify(work_box)
      return

!EOC
      end subroutine calcEfermi
!
      subroutine check_conv(efold,efnew,zvaltot,deltZ,curr_error,numef)
      implicit none
      real(8), intent(in)    :: efold
      real(8), intent(inout) :: efnew
      real(8), intent(in)    :: zvaltot,deltZ,curr_error
      integer, intent(inout) :: numef
      real(8) :: delta,dE,eps,eps1

       eps1 = min(qtol,1.d-8)
       eps  = min(1.d-7,qtol*zvaltot)
       if ( deltZ>-eps1 .and. deltZ<eps ) then
         efnew  = efold
         numef=0
         go to 100
       end if

!DELETE       delta = efnew - efold
!DELETE!       if ( delta*deltZ < zero )  then
!DELETE!          numef=0   ! case when error in deltZ calculation is bad
!DELETE!          go to 100
!DELETE!       end if

!DELETE       dE = abs(delta*deltZ)

!DELETE       if( dE < 0.5d0*etol ) then
!DELETE        eps = min(1.d-7,qtol*zvaltot)
!DELETE!        if ( abs(deltZ)<1.d-5 ) then
!DELETE        if ( deltZ<eps .and. deltZ>-eps1 ) then
!DELETE         efnew  = efold
!DELETE         numef=0
!DELETE         go to 100
!DELETE        end if
!DELETE        eps = min(1.d-7,qtol*zvaltot)
!DELETE        if ( abs(delta)<gEfTol() .and. deltZ<eps.and.deltZ>-eps1 ) then
!DELETE!         efnew  = efold
!DELETE         numef=0
!DELETE         go to 100
!DELETE        end if
!DELETE       end if

!DELETE       eps = 1.d-4
!DELETE       if ( deltZ<eps.and.deltZ>-eps1 ) then
!DELETE        if ( curr_error>1.d3*etol ) then
!DELETE!        if ( curr_error>1.d2*etol ) then
!DELETE!         efnew  = efold
!DELETE         numef=0
!DELETE         go to 100
!DELETE        end if
!DELETE       end if

       eps = 1.d-3
       if ( deltZ<eps.and.deltZ>-eps1 ) then
        if ( curr_error>1.d4*etol ) then
         numef=0
         go to 100
        end if
       end if

100   continue
      write(6,'(/)')
      return
      end subroutine check_conv
!
!BOP
!!IROUTINE: gKSintScheme
!!INTERFACE:
      function gKSintScheme( M_S )
      integer gKSintScheme
!!DESCRIPTION:
!  to get k-space integration scheme id
!
!!REVISION HISTORY:
! Initial Version - A.S. - 2013
!EOP
!
!BOC
      type(RunState) :: M_S
      gKSintScheme = ksint_scf_sch
      if ( associated(M_S%bsf_box) ) then
       if ( M_S%bsf_box%dosbsf_kintsch == ksint_ray_sch ) then
         gKSintScheme = ksint_ray_sch
       end if
      end if
      return
!EOC
      end function gKSintScheme
!
!c============================================================================
!BOP
!!IROUTINE: cIntrplDos
!!INTERFACE:
      function cIntrplDos(mecca_state,igrid,etop,E)
!!DESCRIPTION:
! polynomial interpolation of density of states in complex e-plane, DOS(e=E)
!
!!USES:
      use polyintrpl, only : cpolyval
!
!!ARGUMENTS:
!!PARAMETERS:
!!REVISION HISTORY:
! Initial Version - A.S. - 2013
!!REMARKS:
!  interpolation is implemented only for igrid=2
!EOP
!
!BOC
      implicit none
      complex(8) :: cIntrplDos
      type(RunState), intent(in), target :: mecca_state
      integer, intent(in) :: igrid
      real(8), intent(in) :: etop
      complex(8), intent(in) :: E
      integer :: ne,is,nsub,ic,nspin,ne1
      cIntrplDos = 0
      if ( igrid == 2 ) then
       if ( mecca_state%intfile%Tempr > zero ) then
        do is=1,mecca_state%gf_out_box%nume
         if ( dreal(mecca_state%gf_out_box%egrd(is)) > etop ) then
          ne = is-1
          exit
         end if
        end do
        if ( dreal(E)<=etop ) then
         do is=1,2
          cIntrplDos = cIntrplDos +                                     &
     &          cpolyval(E,mecca_state%gf_out_box%egrd(1:ne),           &
     &                   mecca_state%gf_out_box%dostspn(is,1:ne))
         end do
        else
         ne1 = mecca_state%gf_out_box%nume-1
         do is=1,2
          cIntrplDos = cIntrplDos +                                     &
     &          cpolyval(E,mecca_state%gf_out_box%egrd(ne:ne1),         &
     &                   mecca_state%gf_out_box%dostspn(is,ne:ne1))
         end do
        end if
       else
        ne1 = mecca_state%gf_out_box%nume-1
        do is=1,2
         cIntrplDos = cIntrplDos +                                      &
     &          cpolyval(E,mecca_state%gf_out_box%egrd(1:ne1),          &
     &                   mecca_state%gf_out_box%dostspn(is,1:ne1))
        end do
       end if
!       if ( (aimag(cIntrplDos)<zero .and. abs(dreal(E)-etop).lt.1.d-3) )&
!     &    then
!        nspin = mecca_state%intfile%nspin
!        do nsub=1,mecca_state%intfile%nsubl
!         do ic=1,mecca_state%intfile%sublat(nsub)%ncomp
!          cIntrplDos = cIntrplDos +                                     &
!     &         sum(mecca_state%gf_out_box%doslast(ic,nsub,1:nspin)) *   &
!     &         ( mecca_state%intfile%sublat(nsub)%compon(ic)%concentr * &
!     &                             mecca_state%intfile%sublat(nsub)%ns )
!         enddo
!        enddo
!        if ( nspin == 1 ) cIntrplDos = cIntrplDos + cIntrplDos
!       end if
      else
!      interpolation is not implemented; value of E is ignored and
!      estimate for DOS(Ef) is provided
       nspin = mecca_state%intfile%nspin
       do nsub=1,mecca_state%intfile%nsubl
        do ic=1,mecca_state%intfile%sublat(nsub)%ncomp
!DEBUGPRINT
!        do is=1,nspin
!        write(6,'('' DEBUG: is,ic,nsub='',3i5,'' dos='',2e15.6)')       &
!     &            is,ic,nsub,mecca_state%gf_out_box%doslast(ic,nsub,is)
!        end do
!DEBUGPRINT
          cIntrplDos = cIntrplDos +                                     &
     &         sum(mecca_state%gf_out_box%doslast(ic,nsub,1:nspin)) *   &
     &         ( mecca_state%intfile%sublat(nsub)%compon(ic)%concentr * &
     &                             mecca_state%intfile%sublat(nsub)%ns )
        enddo
       enddo
       if ( nspin == 1 ) cIntrplDos = cIntrplDos + cIntrplDos
      end if
      return
!EOC
      end function cIntrplDos
!
!c============================================================================
!BOP
!!IROUTINE: efTemprFind
!!INTERFACE:
      subroutine efTemprFind(numef,mecca_state,efold,                   &
     &               zvaltot,deltZ,cdosEf,                              &
     &               efnew)
!!DESCRIPTION:
!  to estimate the Fermi energy for some non-zero temperatures
!
!!DO_NOT_PRINT
      implicit none
!!DO_NOT_PRINT

!
!!ARGUMENTS:
      integer, intent(in) :: numef
      type(RunState), intent(in), target :: mecca_state
      real(8), intent(in) :: efold
      real(8), intent(in) :: zvaltot
      real(8), intent(inout) :: deltZ
      complex(8), intent(in) :: cdosEf
      real(8), intent(out):: efnew
!!REVISION HISTORY:
! Initial Version - A.S. - 2015
!EOP
!
!BOC
      type ( IniFile ),   pointer :: inFile => null()
      type (GF_output),   pointer :: gf_out => null()
!c
      logical :: OK
      real(8) :: e0,e1,etmp,F0,F1,emin,emax,e0lc,step,eps1
      real(8) :: xtws,dosen,dq,ebot,Tempr
      complex(8) :: ec,tf2
      complex(8), allocatable :: cwght(:),dost(:),tf1(:)
      integer :: icount,ie,lc,nume
          integer, parameter :: nlg = 2
          integer :: il,nl
          real(8) :: elg(0:nlg+1),flg(0:nlg+1)
      complex(8), external :: TemprFun

      efnew = efold

      inFile => mecca_state%intfile
      Tempr = inFile%Tempr
      gf_out => mecca_state%gf_out_box
      nume = gf_out%nume

      if ( Tempr<aimag(gf_out%egrd(nume)) ) then  ! it seems that at low T the algorithm
       return                                     ! below may fail due to inaccuracy for E close to zero
      end if
      if ( efold+Tempr<inFile%ebot ) then   ! the algorithm below doesn't take into account changes due to core electrons
       return
      end if

      eps1 = 1.d-9

      if ( allocated(cwght) ) deallocate(cwght)
      if ( allocated(dost) ) deallocate(dost)
      if ( allocated(tf1) ) deallocate(tf1)
      allocate(cwght(1:nume),dost(1:nume),tf1(1:nume))
      cwght = gf_out%wgrd(1:nume)

      dosen = aimag(cdosEf)
      dost  = gf_out%dostspn(1,1:nume)+gf_out%dostspn(2,1:nume)
      emin = min(efold,minval(dreal(gf_out%egrd(1:nume))))
      emax = max(efold,maxval(dreal(gf_out%egrd(1:nume))))

      xtws = aimag(sum(cwght(1:nume) * dost(1:nume)))
      dq = zvaltot - deltZ - xtws

      e0lc = min(efnew,emin-100*max(Tempr,efnew-inFile%ebot))
      do ie = 1,nume
       ec =  gf_out%egrd(ie)
       tf1(ie) = TemprFun((ec-efnew)/Tempr,0)
!DELETE       if ( abs(tf1(ie)*cwght(ie))<eps1 ) then
!DELETE        tf1(ie) = czero
!DELETE        dost(ie) = czero
!DELETE       end if
      end do
      lc = 0
!!! it's assumed that nume-1 is an end-point of linear contour
      do ie=nume-1,1,-1
       if ( aimag(gf_out%egrd(ie))>aimag(gf_out%egrd(nume)) ) then
        lc = ie+1
        exit
       end if
      end do
      if ( lc>0 ) then
       if ( nume<=2 ) then
        e0lc = dreal(gf_out%egrd(1))   ! first point of linear contour
       else
        e0lc = dreal(gf_out%egrd(lc))   ! first point of linear contour
!
        step = dreal(gf_out%egrd(nume-1))-dreal(gf_out%egrd(nume-2))
        do ie=nume-3,lc,-1
         if (dreal(gf_out%egrd(ie+1))-dreal(gf_out%egrd(ie))>=step) then
          exit
         else
          step = dreal(gf_out%egrd(ie+1))-dreal(gf_out%egrd(ie))
         end if
        end do
        lc = ie+1
        e0lc = max(e0lc,min(efnew,dreal(gf_out%egrd(lc))))
       end if
!
      end if

      e0 = efnew
      F0 = zvaltot-dq-xtws

!      step = (three*Tempr+abs(efold-ebot))/two
      if ( abs(F0)<0.01d0 .or. numef>0 ) then
       step = gEfTol()
      else
       step = (three*Tempr+abs(efnew-inFile%ebot))/pi
      end if
      step = sign(one,F0) * step       ! F0/abs(F0)*step
!      e1 = e0 + step
!      e1 = min(e1,emax)
      icount = 0
      OK = .false.
      do
       icount = icount+1
       if ( icount>40 ) then
        write(6,'(/''UNEXPECTED infinite Ef loop? icount='',i4/)')icount
        if ( icount>40 ) then
         call fstop(                                                    &
     &  'ERROR in efTemprFin: check if there are states below e-bottom')
        end if
       end if
       e1 = e0+step
       if ( e1 > emax ) then
        efnew = emax + abs(step)
!        deltZ = F0 + (F0-deltZ)*(efnew-e0)/(e0-efold)
        exit
       else if ( e1 < emin ) then
        efnew = max(-mc2,emin - abs(step))
        exit
       else if ( lc>0 .and. step<0 ) then
        if ( e1<=e0lc ) then
         if ( e0<=e0lc ) then
          if ( e0==e0lc ) then
           efnew = e0
!           deltZ = F0
          else
           efnew = efold
          end if
          exit
         else
          e1 = e0lc
         end if
        end if
       else if (abs(step)<epsilon(step)*abs(e0) ) then
        exit
       end if
       do ie = 1,nume
        if ( tf1(ie).ne.czero ) then
         ec =  gf_out%egrd(ie)
         tf2 = TemprFun((ec-e1)/Tempr,0)
         cwght(ie) = gf_out%wgrd(ie)*(tf2/tf1(ie))
        else
         cwght(ie) = czero
        end if
       end do
       F1 = zvaltot-dq-aimag(sum(cwght(1:nume) * dost(1:nume)))
       if ( sign(one,F0) .ne. sign(one,F1) ) then
        OK = .true.
        if ( F0<=zero ) then
         etmp = e0
         e0 = e1
         e1 = etmp
         etmp = F0
         F0 = F1
         F1 = etmp
        end if
        exit
       end if
       e0 = e1
       F0 = F1
       step = sign(min(2000.d0,abs(step+step)),step)
      end do

      if ( .not. OK ) then
        continue
      else
       OK = .false.
       icount = 0
       do while ( .not. OK )
        if ( icount>40 ) then
        write(6,'(/''UNEXPECTED infinite Ef loop? icount='',i4/)')icount
        if ( icount>41 ) then
         write(6,'(4(a,e13.5))') 'e0=',e0,' F0=',F0,' e1=',e1,' F1=',F1
         call fstop('ERROR in efTemprFind')
        end if
       end if

        if ( abs(e1-e0)>max(eps1,gEfTol()/10) .or. abs(F1)>qtol ) then
         nl = nlg
        else
         nl = 1
        end if
        etmp = (e1-e0)/two**nl
        do il=1,nl
         elg(il) = e0 + etmp
         etmp = etmp+etmp
        end do
        elg(0) = e0
        flg(0) = F0
        do il=1,nl
         etmp = elg(il)
         do ie = 1,nume
          ec =  gf_out%egrd(ie)
          tf2 = TemprFun((ec-etmp)/Tempr,0)
          if ( tf1(ie).ne.czero ) then
           cwght(ie) = gf_out%wgrd(ie)*(tf2/tf1(ie))
          else
           cwght(ie) = czero
          end if
         end do
         flg(il) = zvaltot-dq-aimag(sum(cwght(1:nume) * dost(1:nume)))
         if ( flg(il)<=zero ) then
          efnew = elg(il)
          F1 = flg(il)
          exit
         end if
         if ( il==nl ) then
          flg(il+1) = F1
          elg(il+1) = e1
          efnew = elg(il)
          F1 = flg(il)
         end if
        end do
        if ( il>nl ) then
         nl = nl+1
        else
         nl = il
        end if

        OK = abs(F1)<eps1 .or. abs(e1-e0)<epsilon(e0)
!DEBUG?        OK = abs(F1-eps1)<max(eps1,epsilon(eps1))
        OK=OK .or. (F1>zero .and. abs(elg(nl)-elg(nl-1))<epsilon(e0))
        if ( OK ) then
         deltZ = zero
         exit
        end if
!        if ( sign(one,F0) .ne. sign(one,F1) ) then
!         nl = nl-1
!        end if
        e0 = elg(nl-1)
        F0 = flg(nl-1)
        e1 = elg(nl)
        F1 = flg(nl)
       end do
      end if

      deallocate(cwght,dost,tf1)

      return
!EOC
      end subroutine efTemprFind
!!!
!!!c============================================================================
!!!BOP
!!!!IROUTINE: efIntrplFind
!!!!INTERFACE:
!!      subroutine efIntrplFind(mecca_state,ebot,etop,                    &
!!     &               zvaltot,deltZ,cdosEf,                              &
!!     &               defmax,                                            &
!!     &               efnew,OK)
!!!!DESCRIPTION:
!!!  to estimate the Fermi energy: polynomial interpolation
!!!
!!!!USES:
!!      use polyintrpl, only : cpolyval,cpolyintegral
!!
!!!!DO_NOT_PRINT
!!      implicit none
!!!!DO_NOT_PRINT
!!
!!!
!!!!ARGUMENTS:
!!      type(RunState), intent(in), target :: mecca_state
!!      real(8), intent(in) :: ebot,etop
!!      real(8), intent(in) :: zvaltot
!!      real(8), intent(in) :: deltZ
!!      complex(8), intent(in) :: cdosEf
!!      real(8), intent(in) ::     defmax
!!      real(8), intent(out)::  efnew
!!      logical, intent(out) :: OK
!!!!REVISION HISTORY:
!!! Initial Version - A.S. - 2014
!!!EOP
!!!
!!!BOC
!!      type (GF_output),   pointer :: gf_out => null()
!!!c
!!      real(8) :: e0,e1,F0,F1,eps1
!!      real(8) :: xtws,dosen,dE,Tempr
!!      complex(8), allocatable :: cdos(:)
!!      integer :: ngrd(2),nL,nR
!!
!!      gf_out => mecca_state%gf_out_box
!!      dosen = aimag(cdosEf)
!!      efnew = etop + deltZ / dosen
!!      call eGridN(mecca_state,ngrd)  <---- it should be revised
!!      nL = ngrd(1)                   <----
!!      nR = ngrd(2)                   <----
!!      allocate(cdos(1:nL+nR))
!!      cdos(1:nL+nR) = gf_out%dostspn(1,1:nL+nR) +                       &
!!     &                gf_out%dostspn(2,1:nL+nR)
!!      Tempr = mecca_state%intfile%Tempr
!!      xtws = aimag(                                                     &
!!     &           cpolyintegral(Tempr,ebot,efnew,nL,nR,                  &
!!     &                             gf_out%egrd(1:nL+nR),cdos(1:nL+nR)) )
!!
!!      if ( (zvaltot-xtws)*deltZ <= zero ) then
!!        if ( etop<efnew ) then
!!          e0 = etop
!!          F0 = deltZ
!!          e1 = efnew
!!          F1 = zvaltot-xtws
!!        else
!!          e0 = efnew
!!          F0 = zvaltot-xtws
!!          e1 = etop
!!          F1 = deltZ
!!       end if
!!     else
!!       if (efnew > etop ) then
!!        e0 = efnew
!!        F0 = zvaltot-xtws
!!        e1 = e0
!!        F1 = F0
!!       else
!!        e1 = efnew
!!        F1 = zvaltot-xtws
!!        e0 = min(efnew-defmax,(ebot+etop)/2)
!!        xtws = aimag(                                                   &
!!     &           cpolyintegral(Tempr,ebot,e0,nL,nR,                     &
!!     &                             gf_out%egrd(1:nL+nR),cdos(1:nL+nR)) )
!!        F0 = zvaltot-xtws
!!       end if
!!      end if
!!
!!!DEBUG
!!      write(6,*) ' DEBUG: deltZ=',deltZ
!!      write(6,*) ' DEBUG: cdosEf=',cdosEf
!!!DEBUG
!!      eps1 = 1.d-6*max(zvaltot,one)
!!      if ( sign(one,F0) .ne. sign(one,F1) ) then
!!       OK = (abs((e1-e0)*etop)<0.5d0*etol) .and. (abs(F1-F0)<eps1)
!!       do while ( .not. OK )
!!        efnew = (e0+e1)/2
!!        xtws = aimag(                                                   &
!!     &           cpolyintegral(Tempr,ebot,efnew,nL,nR,                  &
!!     &                             gf_out%egrd(1:nL+nR),cdos(1:nL+nR)) )
!!        if ( xtws <= zvaltot ) then
!!         e0 = efnew
!!         F0 = zvaltot-xtws
!!        else
!!         e1 = efnew
!!         F1 = zvaltot-xtws
!!        end if
!!        OK = (abs((e1-e0)*efnew)<0.5d0*etol) .and.                      &
!!     &                                 (min(abs(F1),abs(F0))<eps1)
!!       end do
!!       if ( OK ) then
!!        efnew = (e0*abs(F1)+e1*abs(F0))/(abs(F0)+abs(F1))
!!       end if
!!      else
!!       OK = .false.
!!       if ( F0<zero ) then
!!        efnew = e0
!!        if ( abs(F0)*efnew < 0.5d0*etol .and. abs(F0)<eps1 ) OK=.true.
!!       else
!!        efnew = e1
!!        if ( abs(F1)*efnew < 0.5d0*etol .and. abs(F1)<eps1 ) OK=.true.
!!       end if
!!      end if
!!
!!      deallocate(cdos)
!!!DEBUG
!!      write(6,*) ' DEBUG: OK=',OK
!!      write(6,*) ' DEBUG: e0,e1=',e0,e1
!!      write(6,*) ' DEBUG: F0,F1=',F0,F1
!!      write(6,*) ' DEBUG: efnew=',efnew
!!!DEBUG
!!
!!      return
!!!EOC
!!      end subroutine efIntrplFind

!
!BOP
!!IROUTINE: gLocalizedRho
!!INTERFACE:
      subroutine gLocalizedRho( mecca_state, etop_in )
!!DESCRIPTION:
!   calculates charge density for electrons with energies below {\tt etop\_in}
!

!!ARGUMENTS:
      type(RunState), intent(inout), target :: mecca_state
      real(8), intent(in) :: etop_in
!!REVISION HISTORY:
! Initial Version - A.S. - 2015
!EOP
!
!BOC
      type ( IniFile ),   pointer :: inFile => null()
      type(Work_data), save, target :: work0box

      integer :: nspin
      integer :: ndrpts,ndcomp,ndsubl
      integer :: ichZcor
      real(8) :: Tempr,etop
!      logical OK_to_stop
      integer :: ksint_sch,imix_rho
!      logical :: forcedmix,do_etot

      ! MPI related variables
!      integer :: ierr, myrank, nproc
!      type(MPI_data), target :: mpi_box

      ksint_sch = ksint_scf_sch
      inFile => mecca_state%intfile

!      ! give mecca_state%mpi_box a target
!      mecca_state%mpi_box => mpi_box

      call g_ndims(inFile,ndrpts,ndcomp,ndsubl,nspin)

      if ( .not. associated(mecca_state%work_box) ) then
       mecca_state%work_box => work0box
       call allocWorkdata(mecca_state%work_box,                         &
     &                                    ndrpts,ndcomp,ndsubl,nspin)
      end if

      etop = etop_in
      Tempr = inFile%Tempr
      ivers_tau = tau_scf
      call calcCore( mecca_state,ichZcor,Tempr,etop )   !  ichZcor -- output: number of sublattice with Zcor_out < Zcor_in
      ivers_tau = tau_scf

!      OK_to_stop = .false.

      call setEgrid( mecca_state, etop, Tempr )

      call calcZkkr(ivers_tau+divisor*ksint_sch, mecca_state, etop)    ! V_in --> val rho_out and DOS

      call calcFreeElctrnCorr(mecca_state,etop)

!       call mpi_bcast(OK_to_stop, 1, MPI_INTEGER, 0,                    &
!     &       mpi_comm_world, ierr)

      call calcDensity( mecca_state )  ! Rho_out, Q and check internal consistency

      call dealloc_fe

      if ( associated(mecca_state%work_box,work0box) ) then
        call deallocWorkdata(work0box)
        nullify(mecca_state%work_box)
      endif
      nullify(inFile)

      return
!EOC
      end subroutine gLocalizedRho
!
      subroutine addGFout(gf1out,gf2out)
      implicit none
      type(GF_output), intent(inout) :: gf1out
      type(GF_output), intent(in) :: gf2out
!       Green's Function and DOS (zplane module) output
!integer :: nume=0                       !   number of E-points
!complex(8), allocatable :: egrd(:)     !   E-point
!complex(8), allocatable :: wgrd(:)     !   intgr.weight of E-point

!TODO: in general E-grids are not the same!!!

!complex(8), allocatable :: dostspn(:,:)  ! (2,nume)
      if ( allocated(gf1out%dostspn) .and. allocated(gf2out%dostspn) )  &
     &  gf1out%dostspn(:,:) = gf1out%dostspn(:,:) + gf2out%dostspn(:,:)

!complex(8), allocatable :: greenint(:,:,:,:)  ! (iprpts,ipcomp,ipsublat,ipspin)
      if ( allocated(gf1out%greenint) .and. allocated(gf2out%greenint) )&
     &  gf1out%greenint(:,:,:,:) = gf1out%greenint(:,:,:,:) +           &
     &                                    gf2out%greenint(:,:,:,:)

!complex(8), allocatable :: greenlast(:,:,:,:)  ! (iprpts,ipcomp,ipsublat,ipspin)
     !!         greenlast is calculated in the point egrd(ie) with ie: wgrd(ie)=0
      if ( allocated(gf1out%greenlast).and.allocated(gf2out%greenlast) )&
     & gf1out%greenlast(:,:,:,:) = gf1out%greenlast(:,:,:,:) +          &
     &                                    gf2out%greenlast(:,:,:,:)

!complex(8), allocatable :: doslast(:,:,:)  ! (ipcomp,ipsublat,ipspin)
       if ( allocated(gf1out%doslast).and.allocated(gf2out%doslast) )   &
     &  gf1out%doslast(:,:,:) = gf1out%doslast(:,:,:) +                 &
     &                                    gf2out%doslast(:,:,:)

!complex(8), allocatable :: doscklast(:,:,:)  ! (ipcomp,ipsublat,ipspin)
       if ( allocated(gf1out%doscklast).and.allocated(gf2out%doscklast))&
     &  gf1out%doscklast(:,:,:) = gf1out%doscklast(:,:,:) +             &
     &                                    gf2out%doscklast(:,:,:)

!real(8), allocatable :: evalsum(:,:,:)  ! (ipcomp,ipsublat,ipspin)
       if ( allocated(gf1out%evalsum).and.allocated(gf2out%evalsum) )   &
     &  gf1out%evalsum(:,:,:) = gf1out%evalsum(:,:,:) +                 &
     &                                    gf2out%evalsum(:,:,:)

!real(8), allocatable :: xvalws(:,:,:)  ! (ipcomp,ipsublat,ipspin)
       if ( allocated(gf1out%xvalws).and.allocated(gf2out%xvalws) )     &
     &  gf1out%xvalws(:,:,:) = gf1out%xvalws(:,:,:) +                   &
     &                                    gf2out%xvalws(:,:,:)

!real(8), allocatable :: xvalmt(:,:,:)  ! (ipcomp,ipsublat,ipspin)
       if ( allocated(gf1out%xvalmt).and.allocated(gf2out%xvalmt) )     &
     &  gf1out%xvalmt(:,:,:) = gf1out%xvalmt(:,:,:) +                   &
     &                                    gf2out%xvalmt(:,:,:)

!real(8) :: efermi    !  Ef-estimate based on calculated dos

!integer :: ms_gf
       gf1out%ms_gf = 0                ! if ms_gf=0, green = total

      return
      end subroutine addGFout
!
      end module kkrgf
!
!BOP
!!ROUTINE: scf_delete_files
!!INTERFACE:
      subroutine scf_delete_files( genname )
!!DESCRIPTION:
! deletes scf temporary files
!
!!USES:
      use kkrgf, only : r_prefx,br1prefx,br2prefx,bp1prefx,bp2prefx
      use mpi
!
!!REVISION HISTORY:
! Initial Version - A.S. - 2013
!EOP
!
!BOC
      character*(*) genname
      integer :: myrank,ierr
      call mpi_comm_rank(mpi_comm_world, myrank, ierr)
      call mpi_barrier(mpi_comm_world, ierr)
      if(myrank == 0) then
       call delete_file( r_prefx//genname )
       call delete_file( br1prefx//genname )
       call delete_file( br2prefx//genname )
       call delete_file( bp1prefx//genname )
       call delete_file( bp2prefx//genname )
       call delete_file( bp1prefx//'M'//genname )
       call delete_file( bp2prefx//'M'//genname )
      endif
!MPIDEBUG      call log_mpi_sync(__FILE__,__LINE__)
      call mpi_barrier(mpi_comm_world, ierr)
      return
!EOC
      end subroutine scf_delete_files
!
!BOP
!!IROUTINE: gMltplScatGF
!!INTERFACE:
      subroutine gMltplScatGF( mecca_state, etop_in )
!!DESCRIPTION:
!   calculates charge density for electrons with energies below {\tt etop\_in};
!   output data are in {\tt work\_box} structure
!

!!USES:
      use mecca_constants
      use mecca_types
      use density, only : calcDensity,calcFreeElctrnCorr,dealloc_fe
      use zplane, only : calcZkkr
      use egrid_interface
!
!!ARGUMENTS:
      type(RunState), intent(inout), target :: mecca_state
      real(8), intent(in) :: etop_in
!!REVISION HISTORY:
! Initial Version - A.S. - 2015
!EOP
!
!BOC
      type ( IniFile ),   pointer :: inFile => null()
      type(Work_data), save, target :: work0box

      integer :: nspin
      integer :: ndrpts,ndcomp,ndsubl
      integer :: ichZcor
      real(8) :: Tempr,etop
!      logical OK_to_stop
      integer :: ksint_sch,imix_rho
!      logical :: forcedmix,do_etot

      ! MPI related variables
!      integer :: ierr, myrank, nproc
!      type(MPI_data), target :: mpi_box

      ksint_sch = ksint_scf_sch
      inFile => mecca_state%intfile

!      ! give mecca_state%mpi_box a target
!      mecca_state%mpi_box => mpi_box

      call g_ndims(inFile,ndrpts,ndcomp,ndsubl,nspin)

      if ( .not. associated(mecca_state%work_box) ) then
       mecca_state%work_box => work0box
       call allocWorkdata(mecca_state%work_box,                         &
     &                                    ndrpts,ndcomp,ndsubl,nspin)
      end if

      etop = etop_in
!      ivers_tau = tau_scf
!      call calcCore( mecca_state,ichZcor,Tempr,etop )   !  ichZcor -- output: number of sublattice with Zcor_out < Zcor_in
      ivers_tau = tau_scf

!      OK_to_stop = .false.

      if ( mecca_state%gf_out_box%ms_gf==1 ) then
       Tempr = zero   ! it should be used only for E-grid definition
      else
       Tempr = inFile%Tempr
      end if
      call setEgrid( mecca_state, etop, Tempr )

      call calcZkkr(ivers_tau+divisor*ksint_sch, mecca_state, etop)    ! V_in --> val rho_out and DOS

      call calcFreeElctrnCorr(mecca_state,etop)

!       call mpi_bcast(OK_to_stop, 1, MPI_INTEGER, 0,                    &
!     &       mpi_comm_world, ierr)

      call calcDensity( mecca_state )  ! Rho_out, Q and check internal consistency

      call dealloc_fe

!      if ( associated(mecca_state%work_box,work0box) ) then
!        call deallocWorkdata(work0box)
!        nullify(mecca_state%work_box)
!      endif
      nullify(inFile)

      return
!EOC
      end subroutine gMltplScatGF
