!BOP
!!ROUTINE: timel
!!INTERFACE:
      subroutine timel(timex)
!!DESCRIPTION:
! estimates processor time {\tt timex} (in sec) \\
!
!!ARGUMENTS:
      real(8), intent(out) :: timex
!EOP
!
      call cpu_time(timex)

!cf90        integer icount,irate,ihuge,isave,ncount
!cf90
!cf90        save isave
!cf90
!cf90        call system_clock(icount,irate,ihuge)
!cf90
!cf90        if(time.le.0.d0) then
!cf90         isave = icount
!cf90         timex = nearest(0.d0,1.d0)
!cf90        else
!cf90         ncount = icount-isave
!cf90         isave = icount
!cf90         if(ncount.lt.0) ncount = ncount+ihuge+1
!cf90         timex = dfloat(ncount)/dfloat(irate)
!cf90        end if

      return
      end subroutine timel
