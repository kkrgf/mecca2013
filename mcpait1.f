!c     ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
!BOP
!!ROUTINE: mcpait1
!!INTERFACE:
      subroutine mcpait1(p,tau00,tcpa,tab,                              &
     &                  kkrsz,                                          &
     &                  nbasis,                                         &
     &                  atcon,                                          &
     &                  komp,numbsub,                                   &
     &                  iconverge,nitcpa,                               &
     &                  iprint,istop)
!!DESCRIPTION:
! computes one iteration of cpa equation solver
! {\bv
! input:
!        p
!        tau00   (tauc)
!        tcpa    (tc) ------->   tc = -1/p * S_l(Phi) * exp(i*Phi)
!                                                       exp(i*Phi) = cos(Phi)+i*sin(Phi)
!        tab     (t-matrix for atoms a & b)
!        conc    (concentration of species a)
!        kkrsz   (size of KKR-matrix)
!        komp    (number of components on sublattice)
!        istop   (index of subroutine prog. stops in)
!  output:
!        tcpa    (next iterate for tc)
!        iconverge (iconverge .ne. 0 if errcpa > tolcpa)
!        ! errcpa  (error in cpa solution (tcold-tcnew) )
!
!  method:
!    iteration scheme uses
! mc(n+1) = mc(n) - [ xc(mc(n))-1 + tauc(mc(n)) ]-1
!    where
! xc = sum over i [ c(i) * ( (mc-m(i))-1 - tauc(mc) ) ]-1
!
! mc(n+1) is obtained by assuming that it can obtain the same
! xc that was obtained by using m(i). it is like an "equivalent
! ata." this solution must always be started with an ata to
! ensure convergence, as shown by bob mills and co-workers.
! \ev}
!!USES:
      use mecca_constants
      use mtrx_interface

!!DO_NOT_PRINT
      implicit none
!!DO_NOT_PRINT

!!ARGUMENTS:
      complex(8), intent(in) :: p
      complex(8), intent(in) :: tau00(:,:,:)
      complex(8), intent(inout) ::  tcpa(:,:,:)
      complex(8), intent(in) ::  tab(:,:,:,:)
      integer, intent(in) :: kkrsz,nbasis
      real(8), intent(in) ::  atcon(:,:)
      integer, intent(in) :: komp(:),numbsub(:)
      integer, intent(out) :: iconverge
      integer, intent(in) :: nitcpa,iprint
      character(10), intent(in) :: istop
!!REVISION HISTORY:
! Adapted - A.S. - 2013
!EOP
!
!BOC
      character(10), parameter :: sname='mcpait1'
!c
!c parameter
!c    +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
      integer, parameter :: ipit=4               ! for tc accelerator
!c    +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!c
      real*8 errcpa,errcpand,errmax
!     real(8) :: relerr
!c
      complex(8), save, allocatable :: tcout(:,:,:,:)
      complex(8), save, allocatable ::  tcin(:,:,:,:)
      complex*16 w1(size(tau00,1),size(tau00,2))
      complex*16 w2(size(tau00,1),size(tau00,2))
      complex*16 w3(size(tau00,1),size(tau00,2))
      complex*16 w4(size(tau00,1),size(tau00,2))
      complex*16 xab(size(tau00,1),size(tau00,2),size(tab,3))
      real*8 diftab(nbasis)

      real*8 tc00,tcij

      integer :: i,ic,icur,j,ndit,ndkkr,nsub
      logical, save :: reset    ! .true. to restart Anders.scheme
!c
!c    +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!c parameter
      real*8 onem,coef
      real(8) :: tolstcpa
      real(8) :: tlrnc_cpa
!CAB      parameter (tctol=(one/ten)**5)
!CAB      parameter (tctol=1.d-6)

      parameter (onem=-one)
      parameter (coef=100.d0)

      real(8), external :: g_pi
      integer ncpasub
      real*8 alphmix,alpha0,alpha1,andrstol
      real(8) :: envval
      parameter (alpha0=0.5d0,alpha1=0.2d0,andrstol=0.15d0)
!c                                           alpha0 -- for simple mixing
!c                                           alpha1 -- for Anders.scheme
!c    +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!c


      tlrnc_cpa = gTcTol()
      if(nitcpa.eq.1) then
       alphmix = alpha0
       reset = .true.
       call gEnvVar1('ENV_CPA_A0',.true.,envval)
       if ( envval>0 ) then
        alphmix = envval
       end if
      else
       alphmix = alpha1
       call gEnvVar1('ENV_CPA_A1',.true.,envval)
       if ( envval>0 ) then
        alphmix = envval
       end if
      end if

      ndit = ipit
      ndkkr = size(tau00,1)
      iconverge=0
      icur=mod(nitcpa-1,ndit)+1

      if(mod(nitcpa,20).eq.0) then
       reset = .true.
      end if

      if ( reset ) then
       icur=1
       ndit=1
      end if

      ndit = min(ndit,nitcpa)

      if ( nitcpa <= 1 ) then
        if ( allocated(tcin) ) deallocate(tcin)
        if ( allocated(tcout) ) deallocate(tcout)
        if ( nitcpa<1 ) return
        allocate(tcin(size(tcpa,1),size(tcpa,2),nbasis,ipit))
        allocate(tcout(size(tcpa,1),size(tcpa,2),nbasis,ipit))
      end if
      call zcopy(ndkkr*ndkkr*nbasis,tcpa(1,1,1),1,tcin(1,1,1,icur),1)

      errmax = zero
      ncpasub = 0
      do nsub=1,nbasis

       diftab(nsub) = zero
       if(komp(nsub).gt.1) then
        do i=1,kkrsz
         do j=1,kkrsz
          do ic=1,komp(nsub)
           diftab(nsub) = max(diftab(nsub),abs(tcpa(j,i,nsub) -         &
     &                                   tab(j,i,ic,nsub)))
          end do
         end do
        end do
       end if

!c   if diftab.eq.zero ==>  NO CPA for such a sublattice
!c                 (one component or DLM, but magn.mom.=0)

       if( diftab(nsub) .gt. tlrnc_cpa ) then
        ncpasub = ncpasub+1
        errcpa = zero
        errcpand = zero
        if(iprint.ge.3) then
            write(6,'('' mcpait::  nsub ::'',i3)') nsub
        endif

!c        =============================================================
!c        tcpa => t_{c}(old) \equiv t_{c}..............................
!c        w4   => t_{c}^{-1}...........................................
!c        -------------------------------------------------------------
        call matinv(tcpa(1:kkrsz,1:kkrsz,nsub),w4(1:kkrsz,1:kkrsz),w3,  &
     &                                              kkrsz,iprint)
!c        -------------------------------------------------------------
        if(iprint.ge.1) then
          write(6,'('' mcpait:: t_c(old) :: nsub'',i3)') nsub
            call wrtmtx(tcpa(:,:,nsub),kkrsz)
         if(iprint.ge.3) then
            write(6,'('' mcpait:: m_c(old) ::'')')
            call wrtmtx(w4,kkrsz)
         endif
         write(6,'('' mcpait:: tau00(old) :: nsub'',i3)') nsub
         call wrtmtx(tau00(:,:,nsub),kkrsz)
        endif
        do ic=1,komp(nsub)
!c           ==========================================================
!c           w2 => t_{\alpha}^{-1}......................................
!c           ----------------------------------------------------------
          call matinv(tab(1:kkrsz,1:kkrsz,ic,nsub),w2(1:kkrsz,1:kkrsz), &
     &                                            w3,kkrsz,iprint)
!c           ----------------------------------------------------------
          if(iprint.ge.3) then
               write(6,'('' mcpait::  ic ::'',i3)') ic
               write(6,'('' mcpait:: t_a(b) ::'')')
!c              -------------------------------------------------------
               call wrtmtx(tab(:,:,ic,nsub),kkrsz)
!c              -------------------------------------------------------
               write(6,'('' mcpait:: m_a(b) ::'')')
!c              -------------------------------------------------------
               call wrtmtx(w2,kkrsz)
!c              -------------------------------------------------------
          endif
!c           ==========================================================
!c           w1 => t_c^{-1} - t_{\alpha}^{-1}..........................
!c           ----------------------------------------------------------
          call madd(w4(1:kkrsz,1:kkrsz),onem,w2(1:kkrsz,1:kkrsz),       &
     &                          w1(1:kkrsz,1:kkrsz),kkrsz,iprint)
!c           ----------------------------------------------------------
!c           ==========================================================
!c           w2 => [t_c^{-1} - t_{\alpha}^{-1}]^{-1}...................
!c           ----------------------------------------------------------
          call matinv(w1(1:kkrsz,1:kkrsz),w2(1:kkrsz,1:kkrsz),w3,kkrsz, &
     &                                                     iprint)
!c           ----------------------------------------------------------
          if(iprint.ge.3) then
               write(6,'('' mcpait::m_c -  m_a(b) ::'')')
!c              -------------------------------------------------------
               call wrtmtx(w1,kkrsz)
!c              -------------------------------------------------------
               write(6,'('' mcpait::[m_c -  m_a(b)]^-1 ::'')')
!c              -------------------------------------------------------
               call wrtmtx(w2,kkrsz)
!c              -------------------------------------------------------
          endif
!c           ==========================================================
!c           w1 => [[t_c^{-1} - t_{\alpha}^{-1}]^{-1} - \tau_c]........
!c           ----------------------------------------------------------
          call madd(w2(1:kkrsz,1:kkrsz),onem,                           &
     &                tau00(1:kkrsz,1:kkrsz,nsub),w1(1:kkrsz,1:kkrsz),  &
     &                                               kkrsz,iprint)
!c           ----------------------------------------------------------
!c           ==========================================================
!c           w1 => [[t_c^{-1} - t_{\alpha}^{-1}]^{-1} - \tau_c]^{-1}...
!c           w1 =>  X_\alpha...........................................
!c           ----------------------------------------------------------
          call matinv(w1(1:kkrsz,1:kkrsz),xab(1:kkrsz,1:kkrsz,ic),w3,   &
     &                                               kkrsz,iprint)
!c           ----------------------------------------------------------
          if(iprint.ge.3) then
               write(6,'('' mcpait::[m_c -  m_a(b)]^-1 - tau_c::'')')
!c              -------------------------------------------------------
               call wrtmtx(w1,kkrsz)
!c              -------------------------------------------------------
               write(6,'('' mcpait:: X_a(b)::'')')
!c              -------------------------------------------------------
               call wrtmtx(xab(:,:,ic),kkrsz)
!c              -------------------------------------------------------
          endif
        enddo  ! ic-cycle
!c        =============================================================
!c        w1 => X_c = \Sum_\alpha{C_\alpha X_\alpha}...................
!c        -------------------------------------------------------------
        call mcav(atcon(1:komp(nsub),nsub),xab,w1,kkrsz,komp(nsub),     &
     &             iprint)
!c        -------------------------------------------------------------
        if(iprint.ge.3) then
            write(6,'('' mcpait:: X_c ::'')')
!c           ----------------------------------------------------------
            call wrtmtx(w1,kkrsz)
!c           ----------------------------------------------------------
       endif
!c        =============================================================
!c        w2 => X_{c}^{-1}.............................................
!c        -------------------------------------------------------------
        call matinv(w1(1:kkrsz,1:kkrsz),w2(1:kkrsz,1:kkrsz),w3,kkrsz,   &
     &                                                     iprint)
!c        -------------------------------------------------------------
!c        =============================================================
!c        w1 => X_{c}^{-1} + \tau_c.....................................
!c        -------------------------------------------------------------
        call madd(w2(1:kkrsz,1:kkrsz),one,tau00(1:kkrsz,1:kkrsz,nsub),  &
     &                           w1(1:kkrsz,1:kkrsz),kkrsz,iprint)
!c        -------------------------------------------------------------
!c        =============================================================
!c        w2 => [X_{c}^{-1} + \tau_c]^{-1}.............................
!c        -------------------------------------------------------------
        call matinv(w1(1:kkrsz,1:kkrsz),w2(1:kkrsz,1:kkrsz),w3,kkrsz,   &
     &                                                     iprint)
!c        -------------------------------------------------------------
!c        =============================================================
!c        w1 =>  t_{c}^{-1} - [X_{c}^{-1} + \tau_c]^{-1}...............
!c        w1 =>  t_{c}^{-1}(new).......................................
!c        -------------------------------------------------------------
        call madd(w4(1:kkrsz,1:kkrsz),onem,w2(1:kkrsz,1:kkrsz),         &
     &                           w1(1:kkrsz,1:kkrsz),kkrsz,iprint)
!c        -------------------------------------------------------------
!c        =============================================================
!c        w2 =>  t_{c}(new).............................................
!c        -------------------------------------------------------------
        call matinv(w1(1:kkrsz,1:kkrsz),w2(1:kkrsz,1:kkrsz),w3,kkrsz,   &
     &                                                     iprint)
!c        -------------------------------------------------------------
         if(iprint.ge.3) then
            write(6,'('' mcpait:: t_c(new) ::'')')
            call wrtmtx(w2,kkrsz)
         endif
!c        =============================================================
!c        w1 => \delta t_{c} = t_{c}(new) - t_{c}(old).................
!c        -------------------------------------------------------------
        call madd(w2(1:kkrsz,1:kkrsz),onem,tcpa(1:kkrsz,1:kkrsz,nsub),  &
     &                           w1(1:kkrsz,1:kkrsz),kkrsz,iprint)
!c        -------------------------------------------------------------
!c        =============================================================
!c        check for convergence........................................
        do i=1,kkrsz
          errcpa=max(errcpa,abs(w1(i,i)))
          do j=1,kkrsz
           if(j.ne.i) then
            errcpand=max(errcpand,abs(w1(i,j)))
           end if
           tcpa(i,j,nsub)=w2(i,j)
            enddo
        enddo
        errmax = max(errmax,errcpa,errcpand)

!        relerr = errcpa/abs(w2(1,1))

!CDEBUG
        if(errcpa.gt.tlrnc_cpa) then
         if(errcpand.lt.1.d-6*tlrnc_cpa) then
          if(errcpa.lt.100.d0*tlrnc_cpa) then
           iconverge=iconverge+1
          else if(errcpa.lt.1.d+4*tlrnc_cpa) then
           iconverge=iconverge+(100*nbasis)
          else
           iconverge=iconverge+(1000*nbasis)
          end if
         else
          if(errcpa.gt.100.d0*tlrnc_cpa) then
           iconverge=iconverge+(100*nbasis)
          else
           iconverge=iconverge+(10*nbasis)
          end if
         end if
        endif
!CDEBUG

        if(iprint>=1.or.(mod(nitcpa,20).eq.0.and.iprint>=0)) then
!       if(iprint.ge.1.or.mod(nitcpa,5).eq.0) then
         write(6,1001) sname,errcpa,errcpand,nsub,nitcpa
1001       format(a10,':: ERRCPA=',d12.5,' NOND_ERR=',d12.5,            &
     &            ' NSUB=',i5,' NITCPA=',i3)
        end if

       end if

      enddo    ! nsub-cycle

      if(ncpasub.le.0) return

      if(iprint.ge.1.and.nitcpa.gt.1) then
       do nsub=1,nbasis
         write(6,'('' mcpait:: t_c(new) :: nsub'',i3)') nsub
         call wrtmtx(tcpa(:,:,nsub),kkrsz)
       enddo
      end if

      call zcopy(ndkkr*ndkkr*nbasis,tcpa(1,1,1),1,tcout(1,1,1,icur),1)

!      tolstcpa = tlrnc_cpa/10
      tolstcpa = min(1.d-8,tlrnc_cpa)

      if ( nitcpa==1 .or. errmax>andrstol .or. reset ) then
!c
!c  simple mixing -- 1st iteration and if (In-Out) is too large
!c
       do nsub=1,nbasis
!        if( diftab(nsub) .gt. tlrnc_cpa ) then
         do i=1,kkrsz
          do j=1,kkrsz
           tcpa(i,j,nsub)=alphmix*tcpa(i,j,nsub)+                       &
     &                    (one-alphmix)*tcin(i,j,nsub,icur)
          enddo
         enddo
!        end if
       enddo

       reset = .false.

      else

!      errmax = errmax*abs(p)
!      tcin = tcin*abs(p)
!      tcpa = tcpa*abs(p)
!      tcout = tcout*abs(p)

       call acctc1(tcin,tcout,alphmix,                                  &
     &             ndkkr,kkrsz,                                         &
     &             nbasis,nbasis,                                       &
     &             ndit,nitcpa,icur,                                    &
     &             komp,numbsub,                                        &
     &             tolstcpa,                                            &
     &             tcpa)

!      tcpa = tcpa/abs(p)

       if ( tolstcpa<zero ) then
        iconverge = -1000000-iconverge
        if ( iprint>=-1 ) then
         if ( nitcpa>4 ) then
          write(*,1002) sname,errcpa,nitcpa
1002       format(a10,':: ERRCPA=',d12.5,' NITCPA=',i3)
         else
          if ( iprint>=0 ) then
           write(*,1002) sname,errcpa,nitcpa
          end if
         end if
        end if
       end if

      end if
!c
!c     ***************************************************************
      if(istop.eq.sname) then
        call fstop(sname)
      endif
!c
      return

!EOC
      contains

      subroutine debug_print(info,ns,nitcpa,isz,snglval,err1,err2)
      implicit none
      integer, intent(in) :: info,ns,nitcpa,isz
      real(8), intent(in) :: snglval(isz),err1,err2
      integer :: i
      if ( info==0 ) then
       write(*,'(a,i3,a,i3,a,2es11.3,a,(25es13.4))')                    &
     &            'DEBUG SVD :: ',ns,'NITCPA=',nitcpa,                  &
     &            ' err1,err2=',err1,err2,                              &
     &            ' sngl vals:',(snglval(i),i=1,kkrsz)

      else
       write(*,*) 'DEBUG_PRINT: INFO=',info,' NITCPA=',nitcpa
      end if
      return
      end subroutine debug_print
!
!BOP
!!IROUTINE: mcav
!!INTERFACE:
      subroutine mcav(atcon,xab,xav,kkrsz,komp,                         &
     &                iprint)
!!DESCRIPTION:
! calculates concentration average of {\tt xab} and puts in {\tt xav},
! it is used in iteration of CPA equation
!

!!DO_NOT_PRINT
      implicit none
!!DO_NOT_PRINT

!!ARGUMENTS:
      real(8), intent(in) :: atcon(:)
      complex(8), intent(in) :: xab(:,:,:)
      complex(8), intent(out) :: xav(:,:)
      integer, intent(in) :: komp,kkrsz,iprint
!!REMARKS:
! private procedure of subroutine mcpait1
!EOP
!
!BOC
      complex*16 zatcon
      integer :: ic,i
!c
!c
!c     ==============================================================
!c     zeroout array that average is stored in.......................
!c     --------------------------------------------------------------
!      call zerooutC(xav,ipkkr*kkrsz)
      xav(:,1:kkrsz) = 0
!c     --------------------------------------------------------------
!c     ==============================================================
!c     average over components.......................................
      do ic=1,komp
       zatcon = dcmplx(atcon(ic),0.d0)
       do i=1,kkrsz
!cab        call zaxpy(kkrsz,zatcon,xab(1,i,ic),1,xav(1,i),1)
            xav(1:kkrsz,i) = xav(1:kkrsz,i) + zatcon*xab(1:kkrsz,i,ic)
       enddo
      enddo
!c     ==============================================================
!c     print if needed...............................................
      if (iprint.ge.3) then
         write(6,'('' mcav:: kkrsz,komp :'',2i5)') kkrsz,komp
         call wrtmtx(xav,kkrsz)
      endif
!c
!c     ===============================================================
!c
      return
!EOC
      end subroutine mcav
!
!c     ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
!BOP
!!IROUTINE: acctc1
!!INTERFACE:
      subroutine acctc1(tcin,tcout,alphmix,                             &
     &                   ndkkr,kkrsz,                                   &
     &                   ndsubl,nbasis,                                 &
     &                   ndit,nt,icur,                                  &
     &                   komp,numbsub,                                  &
     &                   tolstcpa,                                      &
     &                   tc)
!!DESCRIPTION:
! mixing, {\tt tc} = {\tt tcin} + ({\tt tcout-tcin}) * mix.coef(...)
!
!!REMARKS:
! private procedure of subroutine mcpait1
!EOP
!
!BOC
      implicit none
!c
!c     ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!c
      integer    ndkkr,kkrsz,ndit,nt,n,ndsubl,nbasis
      integer    nit,icur
      integer    i,i1,i2,nsub
      integer    j
      integer    k
!c
      integer komp(ndsubl),numbsub(ndsubl)
      real*8 a(ndit+1,ndit+1)
      real*8 b(ndit+1)
      integer ipiv(ndit+1),info

!c
      complex*16 tcin(ndkkr,ndkkr,ndsubl,ndit)
      complex*16 tcout(ndkkr,ndkkr,ndsubl,ndit)
      complex*16 tc(ndkkr,ndkkr,nbasis)
      complex*16 tsumx(ndkkr,ndkkr,nbasis)

      real*8 alphmix

      integer natcpa

      real*8 zero,one
      parameter (one=1.d0,zero=0.d0)
      complex*16 sumz,sumx
      real*8 sum0,sum1,sumtmp,coef,asum
      real(8), save :: asum1=one
      real(8) :: tolstcpa
!      real(8) :: tolstcpa=1.d-8
      real*8 err,err0,alfa,coefmix
      parameter (err0=0.05d0,alfa=0.9d0)
!c     ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!c
      nit = min(ndit,nt)
      n = nit+1
      do j = 1,nit
       do i=1,j
        sum0=zero
        do nsub=1,nbasis
         if( komp(nsub) .gt. 1 ) then
          sum1=zero
          do i1=1,kkrsz
           do i2=1,kkrsz
!C
!C To improve stability of CPA
!C
            if(abs(tcin(i2,i1,nsub,i)).gt.tolstcpa.and.                 &
     &        abs(tcin(i2,i1,nsub,j)).gt.tolstcpa) then
             sumtmp =                                                   &
     &        (dreal(tcin(i2,i1,nsub,i))-dreal(tcout(i2,i1,nsub,i)))*   &
     &        (dreal(tcin(i2,i1,nsub,j))-dreal(tcout(i2,i1,nsub,j)))+   &
     &        (aimag(tcin(i2,i1,nsub,i))-aimag(tcout(i2,i1,nsub,i)))*   &
     &        (aimag(tcin(i2,i1,nsub,j))-aimag(tcout(i2,i1,nsub,j)))
            else
             sumtmp = zero
            end if
            if(i1.eq.i2) sumtmp=sumtmp+sumtmp
            sum1 = sum1 + sumtmp
           end do   ! i2-cycle
          end do   ! i1-cycle
          sum0 = sum0+sum1*numbsub(nsub)
         end if
        end do   ! nsub-cycle
        a(i,j)=sum0
        if( i .ne. j ) then
         a(j,i) = a(i,j)
        endif
       enddo    ! i-cycle
       b(j) = zero
       a(j,n) = one
       a(n,j) = one
      enddo    ! j-cycle

      a(n,n) = zero
      b(n) = one
!c
      asum = maxval(abs(a(1:nit,1:nit)))
      if(asum>tolstcpa .or. nt<=2) then
       call dgesv( n, 1, a, ndit+1, ipiv, b, n, info )
       b(1:nit) = b(1:nit)/sum(b(1:nit))
       asum1 = asum
      else
       b(1:nit) = zero
       b(icur) = one
       if ( asum>=asum1 .and. nt>2 ) then
        tolstcpa=-asum
!
!        write(*,*) ' DEBUG :: tolstcpa=',tolstcpa,' NIT=',nt
!
       else
        asum1 = asum
       end if
      end if
!c
      coefmix = alfa
      do nsub=1,nbasis
       err = zero
       do i1=1,kkrsz
       do i2=1,kkrsz
        sumx = (0.d0,0.d0)
        sumz = (0.d0,0.d0)
        do i = 1,nit
         sumz = sumz + b(i)*tcout(i2,i1,nsub,i)
         sumx = sumx + b(i)*tcin(i2,i1,nsub,i)
        enddo
        tc(i2,i1,nsub) = sumz-sumx
        tsumx(i2,i1,nsub) = sumx
        err = max(err,abs(sumz-sumx))
       enddo
       enddo
       if(err.gt.err0) then
        coefmix = min(coefmix,max(alphmix,err0/err))
       end if
      end do
!c
!c  tc = (1-alph)*t_in + alph*t_out = t_in + alph*(t_out-t_in)
!c
      tc(1:kkrsz,1:kkrsz,1:nbasis) = tsumx(1:kkrsz,1:kkrsz,1:nbasis) +  &
     &                            coefmix*tc(1:kkrsz,1:kkrsz,1:nbasis)
!c

      return
!EOC
      end subroutine acctc1
!
      end subroutine mcpait1

!DEBUG
      subroutine gEnvVar1(envvar1,print_val,envval)
      implicit none
      character(*), intent(in) :: envvar1
      logical, intent(in)  :: print_val
      real(8), intent(out) :: envval

      integer :: st_env

      call gEnvVar(envvar1,print_val,st_env)
      if ( st_env>=0 .and. st_env<=100 ) then
        envval = dble(st_env)/dble(100)
      else
        envval = 0
      end if
      end subroutine gEnvVar1
!DEBUG
