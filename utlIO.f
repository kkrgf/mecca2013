!BOP
!!ROUTINE: iostr_head
!!INTERFACE:
      subroutine iostr_head(io,iflag,natom,alat,ba,ca)
!!DESCRIPTION:
! {\bv
! reads header of structural description file if {\tt io}>0,
! writes header if {\tt io}<0 and optional arguments {\tt natom,alat,ba}
! are provided;
! argument {\tt iflag} is used to choose header format
! \ev}

!!DO_NOT_PRINT
       implicit none
!!DO_NOT_PRINT

!!ARGUMENTS:
       integer, intent(in) :: io
       integer, intent(in) :: iflag
       integer, intent(out), optional :: natom
       real(8), intent(out), optional :: alat
       real(8), intent(out), optional :: ba
       real(8), intent(out), optional :: ca
!!REVISION HISTORY:
! Initial Version - A.S. - 2013
!EOP
!
!BOC
       integer :: natom_
       real(8) :: alat_,ba_,ca_
       integer :: nch,i
       integer, parameter :: i0=99999999
       character(120) :: tmpstr
       character(40) :: tmpstr1
       nch = abs(io)
       select case (iflag)
       case(1)
        if ( io > 0 ) then
         read(nch,'(a)',end=101) tmpstr
         read(tmpstr,*,end=101) natom_,ba_,ca_
         if ( present(natom) ) natom=natom_
         if ( present(ba) ) ba=ba_
         if ( present(ca) ) ca=ca_
        else if ( io < 0 ) then
         if ( present(natom) .and. present(ba) .and. present(ca) ) then
         write(nch,'(1x,i7,2(1x,f12.8),a)')                             &
     &                                   natom,ba,ca,'  STRUCTURE DATA'
         else
         endif
        endif
       case(2)
        if ( io > 0 ) then
         read(nch,'(a)',end=101)
         read(nch,'(a)',end=101) tmpstr
         natom_ = 0
         read(nch,'(a)',end=101) tmpstr1
         i=i0
         read(tmpstr1,*,err=10,end=10) natom_,i
10       if ( present(natom) ) then
          if ( i .ne. i0 ) then
           natom=0
          else
           natom = natom_
          end if
          if ( natom == 0 ) return
         else
          call fstop(' IOSTR_HEAD: argument natom must be present')
         end if
         read(tmpstr,*) alat_,ba_,ca_
         if ( present(alat) ) alat=alat_
         if ( present(ba) )   ba=ba_
         if ( present(ca) )   ca=ca_
        else if ( io < 0 ) then
         if ( present(alat) .and. present(ba) .and. present(ca)         &
     &          .and. present(natom) ) then
          write(nch,'(a)',err=99) '  STRUCTURE DATA'
          write(nch,'(3(1x,f12.8))',err=99) alat,ba,ca
          write(nch,'(1x,i7)',err=99) natom
         else
         endif
        endif
       case default
       end select
       return
 99    write(*,*) ' IOSTR_HEAD :: unexpected write-EOF/ERR'
       call fstop(' IOSTR_HEAD: write-eof/error')
101    write(*,*) ' IOSTR_HEAD :: unexpected read-EOF/ERR'
       call fstop(' IOSTR_HEAD: read-eof/error')
!EOC
      end subroutine iostr_head

!BOP
!!ROUTINE: iostrf
!!INTERFACE:
       subroutine iostrf(io,natom,itype,rslatt,basis,ba,ca,unit,invadd)
!!DESCRIPTION:
! {\bv
!   reads/writes structure data
!
!   rslatt(*,i) -- i-th translation vector, i=1..3
!    basis(*,i) -- position of i-th site in the cell, i=1..natom
!    ba = B over A
!    ca = C over A
!    unit -- to change units of rslatt, basis
!    invadd:  =0 -- not to add inversion to symmetry operations in K-space
!             =1 --  to add inversion rotation (time-reversal symmetry)...
! \ev}

!!REVISION HISTORY:
! Initial version - A.S. - 2013
!EOP
!
!BOC
       implicit none
       integer, intent(in) :: io
       integer :: natom
       integer, allocatable :: itype(:)  ! (ndim)
       real(8) :: rslatt(3,3)
       real(8), allocatable :: basis(:,:)  ! (3,natom)
       real(8) :: ba,ca
       real(8) :: unit
       integer, intent(out) :: invadd
       interface
        subroutine iostr_head(io,iflag,natom,alat,ba,ca)
        integer, intent(in) :: io
        integer, intent(in) :: iflag
        integer, intent(out), optional :: natom
        real(8), intent(out), optional :: alat
        real(8), intent(out), optional :: ba
        real(8), intent(out), optional :: ca
        end subroutine iostr_head
       end interface
       real(8), parameter :: zero=0.d0,one=1.d0
       character(1), parameter :: direct(6)=['d','i','r','e','c','t']
       character(80) :: ln1
       character(1) :: chartmp(size(direct))

       integer nch,i,j
       real(8) :: drct(3),rtmp5(1),volume,a_,ba_,ca_
       logical :: do_direct=.false.
       real(8), external :: cellvolm
       real(8), parameter :: pi2=atan(one)*8
       character(1), external :: toLower

       if(io.eq.0) return

       nch = abs(io)
       if(io.gt.0) then
      write(6,*) '  Reading in structure data ...'
!      read(nch,*,end=101) natom,ba,ca
      call iostr_head(io,2,alat=a_,ba=ba_,ca=ca_,natom=natom)
      if ( natom == 0 ) then
        rewind(nch)
        call iostr_head(io,1,ba=ba_,ca=ca_,natom=natom)
      end if
!c  Read lattice vectors .........................................
      if(natom.lt.1) then
       write(*,*) ' NATOM=',natom
       stop ' IOSTR: wrong number of atoms'
      end if

      if ( ba==zero ) ba = ba_
      if ( ca==zero ) ca = ca_

      do i=1,3
       read(nch,*,err=100,end=101) rslatt(1,i),rslatt(2,i),rslatt(3,i)
       rslatt(2,i) = rslatt(2,i)*ba
       rslatt(3,i) = rslatt(3,i)*ca
      end do

      volume = cellvolm(rslatt,rtmp5,0)

      if(volume.lt.zero) then
       write(6,*)
       write(6,*) '  YOU HAVE TO USE RIGHT-HAND BASIS '
       write(6,*) '    rslatt(*,3) <--> rslatt(*,2) '
       write(6,*)
       do i=1,3
        rtmp5(1) = rslatt(i,2)
        rslatt(i,2) = rslatt(i,3)
        rslatt(i,3) = rtmp5(1)
       end do
      end if

      read(nch,*,err=100,end=101) ln1
      ln1 = adjustl(ln1)
      if ( len(ln1)>=size(direct) ) then
       do_direct = .true.
       read(ln1,'(6a1)') chartmp(1:6)
       do i=1,size(direct)
        if ( toLower(chartmp(i)).ne.direct(i) ) then
            do_direct = .false.
         exit
        end if
       end do
      else
       do_direct = .false.
      end if
!c  Read coordinates of atoms (basis) and decoration (itype)......
      if ( allocated(basis) ) deallocate(basis)
      if ( allocated(itype) ) deallocate(itype)
      allocate(basis(3,natom),itype(natom))

      if ( do_direct ) then   ! direct
       do i=1,natom
        read(nch,*,err=100,end=101) itype(i),drct(1:3)
        basis(1:3,i) = drct(1)*rslatt(1:3,1) + drct(2)*rslatt(1:3,2) +  &
     &                 drct(3)*rslatt(1:3,3)
!DELETE        basis(2,i) = basis(2,i)*ba
!DELETE        basis(3,i) = basis(3,i)*ca
       end do
      else    ! cartesian
       do i=1,natom
        read(nch,*,err=100,end=101) itype(i),(basis(j,i),j=1,3)
        basis(2,i) = basis(2,i)*ba
        basis(3,i) = basis(3,i)*ca
       end do
      end if
      call close2zero(natom,rslatt,basis)
      unit = one
      invadd=0
      read(nch,*,err=1999,end=1999) unit,invadd
1999    if(unit.le.zero) unit=one
!DELETE      if(invadd>0) then
!DELETE        invadd=1
!DELETE      else if(invadd<0) then
!DELETE       call fstop(' ERROR: incorrect input value of invadd')
!DELETE       invadd=-1
!DELETE      end if
!!
       elseif(io.lt.0) then
!!
      write(6,*) '  Writing down structure data ...'
!       write(nch,*,err=99) natom,ba,ca,'  STRUCTURE DATA'
        call iostr_head(io,1,ba=ba,ca=ca,natom=natom)
      do i=1,3
       write(nch,*,err=99)                                              &
     &          rslatt(1,i)/pi2,rslatt(2,i)/pi2/ba,rslatt(3,i)/pi2/ca
      end do
      write(nch,*,err=99) ' --------------------- '
!c  Read coordinates of atoms (basis) and decoration (itype)......
      do i=1,natom
       write(nch,1,err=99) itype(i),                                    &
     &                       basis(1,i),basis(2,i)/ba,basis(3,i)/ca
1        format(1x,i5,3d20.10)
      end do
!      write(nch,*,err=99) unit,invadd
!!
       end if
!!
       return
99     write(*,*)  ' WRITE '
100    write(*,*) ' ERROR '
101    write(*,*) ' IOSTR:: Channel=',nch
       write(*,*) ' I=',i
       write(*,*) ' EOF/ERROR '
       call fstop(' IOSTR: eof/error')
!EOC
       end subroutine iostrf

!BOP
!!ROUTINE: iostrf2
!!INTERFACE:
       subroutine iostrf2(io,natom,itype,rslatt,basis,alat,boa,coa,     &
     &                   scale,ndim,invadd,nsublat0,isflipped,flippoint)
!!DESCRIPTION:
! {\bv
!   read/write structure data (Ames format)
!
!    rslatt(*,i) -- i-th translation vector, i=1..3
!    basis(*,i) -- position of i-th site in the cell, i=1..natom
!    alat = Lattice constant
!    boa = B over A
!    coa = C over A
!    scale -- to change units of rslatt, basis
!    invadd:  =0 -- not to add inversion to symmetry operations in K-space
!                   for appropriate cases
!             =1 --  to add inversion ...
!
!    this subroutine also takes care of additional sublattices for flipped potentials
! \ev}
!
!!REVISION HISTORY:
! Initial Version - A.S. - 2013
!!REMARKS:
! it is not used and tested yet
!EOP
!
!BOC
       implicit none
       interface
        subroutine iostr_head(io,iflag,natom,alat,ba,ca)
        integer, intent(in) :: io
        integer, intent(in) :: iflag
        integer, intent(out), optional :: natom
        real(8), intent(out), optional :: alat
        real(8), intent(out), optional :: ba
        real(8), intent(out), optional :: ca
        end subroutine iostr_head
       end interface
       integer io,natom,ndim,nch,i,j,invadd
       integer itype(ndim)
       real*8  rslatt(3,3),basis(3,ndim),alat,boa,coa,scale

       real*8  rtmp5,volume
!       character  namk*32, text*80

       logical, allocatable :: isflipped(:)
       integer, allocatable :: flippoint(:)
       integer :: cnt,nsublat0

       if(io.eq.0) return

       nch = abs(io)
       if(io.gt.0) then
!        write(6,*) '  Reading in structure data ...'
!        read(nch,'(a32)',end=101) namk
!        read(nch,*,end=101) alat,boa,coa
!        read(nch,*,end=101) natom
        call iostr_head(io,2,alat=alat,ba=boa,ca=coa,natom=natom)
!c  Read lattice vectors .........................................
        if(natom.gt.ndim.or.natom.lt.1) then
         write(*,*) ' NATOM=',natom,' NDIM=',ndim
         stop ' IOSTR: wrong number of atoms'
        end if
        do i=1,3
         read(nch,*,err=100,end=101) rslatt(1,i),rslatt(2,i),rslatt(3,i)
        end do

        volume = ( rslatt(2,1)*rslatt(3,2) - rslatt(3,1)*rslatt(2,2) )  &
     &         * rslatt(1,3) +                                          &
     &           ( rslatt(3,1)*rslatt(1,2) - rslatt(1,1)*rslatt(3,2) )  &
     &         * rslatt(2,3) +                                          &
     &           ( rslatt(1,1)*rslatt(2,2) - rslatt(2,1)*rslatt(1,2) )  &
     &         * rslatt(3,3)

        if(volume.lt.0.d0) then
         write(6,*)
         write(6,*) '  YOU HAVE TO USE RIGHT-HAND BASIS '
         write(6,*) '    rslatt(*,3) <--> rslatt(*,2) '
         write(6,*)
         do i=1,3
          rtmp5 = rslatt(i,2)
          rslatt(i,2) = rslatt(i,3)
          rslatt(i,3) = rtmp5
         end do
        end if

        nsublat0 = 0
        read(nch,*,err=100,end=101)
!c  Read coordinates of atoms (basis) and decoration (itype)......
        do i=1,natom
         read(nch,*,err=100,end=101) itype(i),(basis(j,i),j=1,3)
         nsublat0 = max(nsublat0,itype(i))
        end do
        call close2zero(natom,rslatt,basis)
!        read(nch,'(a80)') text
!        if(trim(adjustl(text))/='end of file!') then
!          call fstop("did not see 'end of file!' marker in *.xyz file")
!        end if

!cSUFF Add additional sublattices for flipped potentials
        if ( allocated(isflipped) ) deallocate(isflipped)
        allocate(isflipped(natom))
        if ( allocated(flippoint) ) deallocate(flippoint)
        allocate(flippoint(natom))
        isflipped = .false.
        cnt = 0; flippoint = 0
        do i=1,natom
        if( itype(i) < 0 ) then
          cnt = cnt + 1
          flippoint(nsublat0 + cnt) = -itype(i)
          itype(i) = nsublat0 + cnt
          isflipped(itype(i)) = .true.
        end if; end do
        nsublat0 = nsublat0 + cnt

!c       read(nch,*,err=1999,end=1999) scale,invadd

        scale = 1.d0   ! Don't change the units of rslatt, basis
        invadd = 0     ! default
!        invadd = 1

1999    if(scale.le.0.d0) scale=1.d0
!      if(invadd>0) then
!        invadd=1
!      else if(invadd<0) then
!       invadd=-1
!      end if

       elseif(io.lt.0) then
        write(6,*) '  Writing down structure data ...'
!        write(nch,'(a32)',err=99) namk
!        write(nch,*,err=99) alat,boa,coa
!        write(nch,*,err=99) natom,'  STRUCTURE DATA'
        call iostr_head(io,-2,alat=alat,ba=boa,ca=coa,natom=natom)
        do i=1,3
         write(nch,*,err=99) rslatt(1,i),rslatt(2,i),rslatt(3,i)
        end do
        write(nch,*,err=99) ' --------------------- '
!c Write coordinates of atoms (basis) and decoration (itype)......
        do i=1,natom
         if(.not. isflipped(itype(i))) then
          write(nch,1,err=99)  itype(i),(basis(j,i),j=1,3)
         else
          write(nch,1,err=99) -flippoint(itype(i)), (basis(j,i),j=1,3)
         end if
1        format(1x,i5,3d20.10)
        end do
!        write(nch,*,err=99) scale,invadd
       end if

       return
99    write(*,*)  ' WRITE '
100    write(*,*) ' ERROR '
101    write(*,*) ' IOSTR:: Channel=',nch
       write(*,*) ' I=',i
       write(*,*) ' EOF/ERROR '
       call fstop(' IOSTR: eof/error')
!EOC
       end subroutine iostrf2

!BOP
!!ROUTINE: iostr_subl
!!INTERFACE:
      subroutine iostr_subl(nch,iflag,nsubl)
!!DESCRIPTION:
! {\bv
! reads sublattice information {\tt nsubl(:)} from structural description file;
! {\tt nsubl(0)} is number of sublattices, other elements of the array are number of
! equivalent sites in each sublattice;
! argument {\tt nch} is unit number, argument {\tt iflag} is used to choose header file format
! \ev}

!!DO_NOT_PRINT
       implicit none
!!DO_NOT_PRINT

!!ARGUMENTS:
       integer, intent(in) :: nch
       integer, intent(in) :: iflag
       integer, allocatable, intent(out) :: nsubl(:)
!!REVISION HISTORY:
! Initial Version - A.S. - 2015
!EOP
!
!BOC
       integer :: natom_
       integer :: i,j,n,ierr
       integer, allocatable :: itype(:)
       character(120) :: tmpstr
       if ( nch <=0 ) return
       select case (iflag)
       case(1)
         ierr = 1
         read(nch,'(a)',end=101) tmpstr
         ierr = 2
         read(tmpstr,*,err=101,end=101) natom_
       case(2)
         ierr = 3
         read(nch,'(a)',end=101)
         ierr = 4
         read(nch,'(a)',end=101)
         ierr = 5
         read(nch,*,err=101,end=101) natom_
       case default
         return
       end select
       ierr = 0
       do i=1,4
        read(nch,*)
       end do
       allocate(itype(natom_))
       n = 0
       do i=1,natom_
        read(nch,*) itype(i)
        n = n+1
        do j=1,i-1
         if ( itype(i)==itype(j) ) then
          n=n-1
          exit
         end if
        end do
       end do
       if ( n<=0 ) then
        write(*,*) ' NATOM_=',natom_,' N=',n
        call p_fstop(' UNEXPECTED ERROR in STR.FILE READING')
       end if
       if ( allocated(nsubl) ) deallocate(nsubl)
       allocate(nsubl(0:n))
       nsubl(0)   = n
       nsubl(1:n) = 0
       do i=1,nsubl(0)
        do j=1,natom_
         if ( itype(j) == i ) then
          nsubl(i) = nsubl(i)+1
         end if
        end do
       end do
       if ( sum(nsubl(1:nsubl(0))) .ne. natom_ ) then
        call p_fstop(' UNEXPECTED ERROR in STR.FILE READING')
       end if
       if ( allocated(itype) ) deallocate(itype)
       return
101    write(*,*) ' ierr=',ierr,' IOSTR_SUBL :: unexpected read-EOF/ERR'
       call fstop(' IOSTR_SUBL: read-eof/error')
!EOC
      end subroutine iostr_subl

!BOP
!!ROUTINE: close2zero
!!INTERFACE:
       subroutine close2zero(natom,rslatt,basis)
!!DESCRIPTION:
! moves sites closer to {0,0,0} (taking into account
! periodic boundary conditions)
!
!!REVISION HISTORY:
! Initial Version - A.S. - 1997
!EOP
!
!BOC
       implicit none
       integer natom
       real*8 rslatt(3,3)
       real*8 basis(3,natom)

       real*8 r,r2,rsm(3),xyznew(3),r2new
       real*8 zero
       parameter (zero=0.d0)
       integer n(3)
       integer i,i1,i2,i3,imove,inew

       if(natom.eq.1) then
       basis(1:3,1) = zero
       else
      do i=1,3
       rsm(i) = sqrt(rslatt(1,i)**2+rslatt(2,i)**2+rslatt(3,i)**2)
      end do

      inew = 0
      do i=1,natom
       imove = 0
       r2 = dot_product(basis(1:3,i),basis(1:3,i))
       r = sqrt(r2)
       n(1:3) = nint(r/rsm(1:3))+1
       if(maxval(n).gt.0) then
        do i1=-n(1),n(1)
         do i2=-n(2),n(2)
          do i3=-n(3),n(3)
           xyznew(1:3) = basis(1:3,i)                                   &
     &                    +i1*rslatt(1:3,1)                             &
     &                    +i2*rslatt(1:3,2)                             &
     &                    +i3*rslatt(1:3,3)
           r2new = dot_product(xyznew,xyznew)
           if(r2new.lt.r2) then
            imove = 1
            basis(1:3,i) = xyznew(1:3)
            r2 = r2new
           end if
          end do
         end do
        end do
       end if
       if(imove.ne.0) inew = inew+1
      end do
      if(inew.ne.0) then
       write(6,*)
       write(6,'('' I MOVED '',i6,                                      &
     &     '' ATOM(S) TO NEW POSITIONS CLOSER TO (0,0,0)'')') inew
       write(6,*)
      end if
!c
!c         xyznew(1:3) = basis(1:3,1)
!c         do i=2,natom
!c          xyznew(1:3) = xyznew(1:3) + basis(1:3,i)
!c         end do
!c         xyznew(1:3) = xyznew(1:3)/natom
!c
!c         if(max(abs(xyznew(1)),abs(xyznew(1)),abs(xyznew(1))).gt.1.d-10 !)
!c     *    then
!c          do i=1,natom
!c           basis(1:3,i) = basis(1:3,i) - xyznew(1:3)
!c          end do
!c          write(6,'('' SHIFT OF THE SYSTEM IS '',3f10.5)') xyznew
!c          write(6,*)
!c        end if
!c
       end if

       return
!EOC
       end subroutine close2zero
!
!BOP
!!ROUTINE: iokpnt
!!INTERFACE:
       subroutine iokpnt(io,pointk,wghtq,lrot,twght,nkpt)
!!DESCRIPTION:
!  reads/writes k-points
!
!!REVISION HISTORY:
! Initial Version - A.S. - 1997
!EOP
!
!BOC
       implicit none
       integer nch,nkpt,i,io
       real*8 pointk(3,*)
       integer wghtq(*)
       integer lrot(*)
       real*8  twght
       integer j,nwghtq

       if(io.eq.0) return

       nch = abs(io)
       if(io.gt.0) then
      write(6,*) '  Reading in k-points mesh ...'
      read(nch,*,end=100,err=100) nkpt
      read(nch,*,end=100,err=100) twght
      write(6,1) nch,nkpt
1       format('   Channel ',i3,' number of special k-pts = ',i5)
      nwghtq = 0
      do i=1,nkpt
       read(nch,*,end=100,err=100)                                      &
     &                             pointk(1:3,i),wghtq(i)
!     &                   wghtq(i),pointk(1,i),pointk(2,i),pointk(3,i)
       read(nch,*,end=100,err=100) (lrot(nwghtq+j),j=1,wghtq(i))
       nwghtq = nwghtq+wghtq(i)

      end do
       elseif(io.lt.0) then
      write(6,*) '  Writing down k-points mesh ...'
      write(nch,*) nkpt,'  K-POINTS MESH'
      write(nch,*) twght
      write(6,1) nch,nkpt
      write(6,'(13x,''twght '',d14.6)') twght
      nwghtq = 0
      do i=1,nkpt
       write(nch,2,err=100) pointk(1:3,i),wghtq(i)
2      format(1x,3(d22.15,1x),i10)
!       write(nch,3,end=100,err=100)                                     &
!     &                   wghtq(i),pointk(1,i),pointk(2,i),pointk(3,i)
!3        format(1x,i10,1x,3(1x,d22.15))
       write(nch,5,err=100) (lrot(nwghtq+j),j=1,wghtq(i))
5        format(1x,16i4)
       nwghtq = nwghtq+wghtq(i)
      end do
       end if

       return
100    write(*,*) ' IOKPNT:: Channel=',nch
       write(*,*) ' NKPT=',nkpt,' I=',i
       write(*,*) ' EOF/ERROR '
       call p_fstop(' IOKPNT: eof/error')
!EOC
       end subroutine iokpnt

!BOP
!!ROUTINE: iodop
!!INTERFACE:
       subroutine iodop(io,nop,kkrsz,dop)
!!DESCRIPTION:
!   reads/writes dop-matrices
!
!!REVISION HISTORY:
! Initial Version - A.S. - 1997
!EOP
!
!BOC
       implicit none
       integer io,nop,kkrsz,i,kk1,kk2,nop0,kkrsz0,nch
       complex*16   dop(*)
       real*8 r1,r2

       if(io.eq.0) return

       nch = abs(io)
       if(io.gt.0) then
      write(6,*) '  Reading in DOP-matrices ... NOP=',                  &
     &                                     nop,' KKRSZ=',kkrsz
      read(nch,*,err=100,end=100) nop0,kkrsz0
      if(nop0.ne.nop.or.kkrsz.ne.kkrsz0) then
       write(*,*) ' NOP=',nop,' NOP(in file)=',nop0
       write(*,*) ' KKRSZ=',kkrsz,' KKRSZ(in file)=',kkrsz0
       call p_fstop(' IODOP: wrong info in input file')
      end if
      do i = 1,nop
       do kk2 = 1,kkrsz
         read(nch,*,err=100,end=100)                                    &
     &             (dop(kk1+((kk2-1)+(i-1)*kkrsz)*kkrsz),               &
     &                  kk1 = 1,kkrsz)
       enddo
       read(nch,*,err=100,end=1)
1        continue
      enddo
       elseif(io.lt.0) then
      write(6,*) '  Writing down DOP-matrices ...'
      write(nch,*) nop,kkrsz,' DOP-matrices'
      do i = 1,nop
!CDEBUG
       do kk2 = 1,kkrsz
        do kk1=1,kkrsz
         r1 = dreal(dop(kk1+((kk2-1)+(i-1)*kkrsz)*kkrsz))
         r2 = aimag(dop(kk1+((kk2-1)+(i-1)*kkrsz)*kkrsz))
         if(abs(r1).lt.1.d-16) r1 = 0.d0
         if(abs(r2).lt.1.d-16) r2 = 0.d0
         dop(kk1+((kk2-1)+(i-1)*kkrsz)*kkrsz) = dcmplx(r1,r2)
        enddo
       enddo
!CDEBUG
       do kk2 = 1,kkrsz
         write(nch,10,err=100)                                          &
     &     (dop(kk1+((kk2-1)+(i-1)*kkrsz)*kkrsz),kk1 = 1,kkrsz)
10         format(1x,'(',es23.16,',',es23.16,')')
       enddo
       write(nch,*)
      enddo
       end if

       return
100    write(*,*) ' IODOP:: Channel=',nch
       write(*,*) ' KK2=',kk2,' I=',i
       write(*,*) ' EOF/ERROR '
       call p_fstop(' IODOP: eof/error')
!EOC
       end subroutine iodop

!       subroutine iomdlng(io,nbasis,madmat,ndimb)
!!c
!!c   read/write madelung constants
!!c
!       implicit none
!       integer io,nbasis,ndimb,nbasis0,i,j,nch
!       real*8  madmat(ndimb,nbasis)
!
!       if(io.eq.0) return
!
!       nch = abs(io)
!       if(io.gt.0) then
!      write(6,*) '  Reading in Madelung constants ...'
!      read(nch,*,err=100,end=100) nbasis0
!      if(nbasis.ne.nbasis0) then
!       write(*,*) ' NBASIS=',nbasis,' NBASIS(in file)=',nbasis0
!       call p_fstop(' IOMDLNG: wrong info in input file')
!      end if
!      do i=1,nbasis
!       read(nch,*,err=100,end=100) (madmat(i,j),j=1,nbasis)
!      enddo
!       elseif(io.lt.0) then
!      write(6,*) '  Writing down Madelung constants ...'
!      write(nch,*,err=100) nbasis,' MADELUNG CONSTANTS'
!      do i=1,nbasis
!       write(nch,*) (madmat(i,j),j=1,nbasis)
!      enddo
!       end if
!
!       return
!100    write(*,*) ' IOMDLNG:: Channel=',nch
!       write(*,*) ' J=',j,' I=',i
!       write(*,*) ' EOF/ERROR '
!       call p_fstop(' IOMDLNG: eof/error')
!       end subroutine iomdlng

!BOP
!!ROUTINE: char2status
!!INTERFACE:
      function char2status( stat ) result ( cstatus )
!!DESCRIPTION:
! converts symbol {\tt stat} (char*1) to name of fortran
! IO status {\tt cstatus} (char*7)
!
!!REVISION HISTORY:
! Initial Version - A.S. - 2013
!EOP
!
!BOC
        implicit none
        character(1), intent(in) :: stat
        character(7) :: cstatus
        character(7), parameter :: nw='NEW'
        character(7), parameter :: old='OLD'
        character(7), parameter :: unk='UNKNOWN'
        character(7), parameter :: rpl='REPLACE'
        character(7), parameter :: dlt='DELETE'
        character(7), parameter :: scr='SCRATCH'
        if ( stat == 'n' ) then
          cstatus = nw
        else if ( stat == 'u' ) then
          cstatus = unk
        else if ( stat == 'r' ) then
          cstatus = rpl
        else if ( stat == 'o' ) then
          cstatus = old
        else if ( stat == 'd' ) then
          cstatus = dlt
        else
          cstatus = scr
        end if
        return
!EOC
      end function char2status

!BOP
!!ROUTINE: char2form
!!INTERFACE:
      function char2form( frm ) result ( cform )
!!DESCRIPTION:
! converts symbol {\tt frm} (char*1) to name of fortran
! IO format {\tt cform} (char*11)
!
!!REVISION HISTORY:
! Initial Version - A.S. - 2013
!EOP
!
!BOC
        implicit none
        character(1), intent(in) :: frm
        character(11) :: cform
        character(11), parameter :: frmt='FORMATTED'
        character(11), parameter :: ufrm='UNFORMATTED'
        if ( frm == 'f' ) then
            cform = frmt
        else
            cform = ufrm
        end if
        return
!EOC
      end function char2form

!BOP
!!ROUTINE: delete_file
!!INTERFACE:
      subroutine delete_file( fname )
!!DESCRIPTION:
! deletes file {\tt fname}
!
!!REVISION HISTORY:
! Initial Version - A.S. - 2013
!!REMARKS:
! fortran unit 11 is used
!EOP
!
!BOC
      implicit none
      character*(*) fname
      integer stt
      character(200) msg
      logical :: fileexist,opnd
      inquire(file=trim(fname),EXIST=fileexist)
      if ( fileexist ) then
        inquire(11,opened=opnd)
        if ( opnd ) close(11)
        open(11,file=trim(fname),status='old',iomsg=msg,iostat=stt)
!        if (stt.ne.0) write(*,*) ' FILE ',trim(fname)//' :: '//msg
        close(11,status='delete',iomsg=msg,iostat=stt)
        if (stt.ne.0) write(*,'(a,a)') ' FILE ',trim(fname)//' :: '//msg
      end if
      return
!EOC
      end subroutine delete_file

!BOP
!!ROUTINE: cptxtfile
!!INTERFACE:
      subroutine cptxtfile( from_file, to_file )
!!DESCRIPTION:
! copies text file {\em from\_file} to file {\em to\_file}
!
!!REVISION HISTORY:
! Initial Version - A.S. - 2013
!!REMARKS:
! fortran units 11 and 12 are used
!EOP
!
!BOC
      implicit none
      character*(*) from_file,to_file
      integer stt
      character(1024) msg
      logical fileexist
      inquire(file=trim(from_file),EXIST=fileexist)
      if ( fileexist ) then
        open(11,file=trim(from_file),status='OLD',iomsg=msg,iostat=stt)
        if ( stt.ne.0) then
         write(*,*) ' FILE ',trim(from_file)//' :: '//msg
        else
         open(12,file=trim(to_file),status='UNKNOWN',iomsg=msg,          &
     &                                                       iostat=stt)
         if ( stt.ne.0) then
           write(*,*) ' FILE ',trim(to_file)//' :: '//msg
         else
          do while ( 2 > 1 )
           read(11,fmt='(a)',advance='NO',end=100,eor=10,err=10) msg
10         write(12,fmt='(a)') trim(msg)
          end do
100       close(11)
          close(12)
         end if
        end if
      end if
      return
!EOC
      end subroutine cptxtfile

!      subroutine checkIOfile( io_file )
!      return
!      end subroutine checkIOfile

      subroutine log_mpi_sync(filen, linen)
      use mpi
      character(*) :: filen
      integer :: linen, myrank, ierr
!#ifdef DEBUG
      call mpi_comm_rank(mpi_comm_world,myrank,ierr)
      write(0,'(a,i3,a,a,a,i4)') 'MPI sync point ::',myrank,':"',       &
     &  trim(filen),'":',linen
      flush(unit=0)
!#endif
      end subroutine log_mpi_sync
