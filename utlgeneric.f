!BOP
!!ROUTINE: erfc
!!INTERFACE:
      real*8 function erfc(xx)
!!DESCRIPTION:
! {\bv
!  erfc(x) computes 2.0/sqrt(pi) times the integral from x to
!  infinity of exp(-x**2). this is done using rational approx-
!  imations.  eleven correct significant figures are provided.
!
!     erfc is documented completely in sc-m-70-275.
!
!     sandia mathematical program library
!     applied mathematics division 2613
!     sandia laboratories
!     albuquerque, new mexico  87185
!     control data 6600/7600  version 7.2  may 1978
!
!     Revision 1.1  89/03/11  19:09:54  sverre
!
! * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
! *                 issued by sandia laboratories                     *
! *                   a prime contractor to the                       *
! *                united states department of energy                 *
! * * * * * * * * * * * * * * * notice  * * * * * * * * * * * * * * * *
! * this report was prepared as an account of work sponsored by the   *
! * united states government.  neither the united states nor the      *
! * united states department of energy nor any of their employees,    *
! * nor any of their contractors, subcontractors, or their employees  *
! * makes any warranty, express or implied, or assumes any legal      *
! * liability or responsibility for the accuracy, completeness or     *
! * usefulness of any information, apparatus, product or process      *
! * disclosed, or represents that its use would not infringe          *
! * owned rights.                                                     *
! * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
! * the primary document for the library of which this routine is     *
! * part is sand77-1441.                                              *
! \ev}


!!DO_NOT_PRINT
      implicit real*8 (a-h,o-z)
!!DO_NOT_PRINT

!!ARGUMENTS:
      real*8, intent(in) :: xx
!AUTHOR:
! j.e. vogel
!!REMARKS:
! Warning: the function can only be called via an explicit interface or
! if declared EXTERNAL; otherwise intrinsic fortran 'erfc' function is used.
!EOP
!BOC
      real(8), save :: p1(4),q1(4),p2(6),q2(6),p3(4),q3(4)
!c
!c
      data (p1(i),i=1,4)/                                               &
     &     242.6679552305318D0,                                         &
     &     21.97926161829415D0,                                         &
     &     6.996383488619136D0,                                         &
     &     -3.560984370181539D-2/                                       &
     &     (q1(i),i=1,4)/                                               &
     &     215.0588758698612D0,                                         &
     &     91.16490540451490D0,                                         &
     &     15.08279763040779D0,                                         &
     &     1.D0/
      data (p2(i),i=1,6)/                                               &
     &     22.898992851659D0,                                           &
     &     26.094746956075D0,                                           &
     &     14.571898596926D0,                                           &
     &     4.2677201070898D0,                                           &
     &     .56437160686381D0,                                           &
     &     -6.0858151959688D-6/
      data (q2(i),i=1,6)/                                               &
     &     22.898985749891D0,                                           &
     &     51.933570687552D0,                                           &
     &     50.273202863803D0,                                           &
     &     26.288795758761D0,                                           &
     &     7.5688482293618D0,                                           &
     &     1.D0/
      data (p3(i),i=1,4)/                                               &
     &     -1.21308276389978D-2,                                        &
     &     -.1199039552681460D0,                                        &
     &     -.243911029488626D0,                                         &
     &     -3.24319519277746D-2/                                        &
     &     (q3(i),i=1,4)/                                               &
     &     4.30026643452770D-2,                                         &
     &     .489552441961437D0,                                          &
     &     1.43771227937118D0,                                          &
     &     1.D0/
      real(8), parameter :: sqpi=0.564189583547756D0
      real(8) :: x,x2,xi2,r,a
!c
      x=abs(xx)
      x2=x*x
!c
      if (xx .lt. -6.D0) then
         erfc = 2.D0
         return
      else if (xx .gt. 25.8D0) then
         erfc = 0.D0
         return
      else if (x .gt. 4.D0) then
         xi2 = 1.D0/x2
         r = xi2*(p3(1)+xi2*(p3(2)+xi2*(p3(3)+xi2*p3(4))))/             &
     &        (q3(1)+xi2*(q3(2)+xi2*(q3(3)+xi2*q3(4))))
         a = exp(-x2)*(sqpi+r)/x
         if (xx .lt. 0.D0) then
            erfc = 2.D0-a
         else
            erfc = a
         end if
         return
      else if (x .gt. 0.46875D0) then
         a = exp(-x2)
         a = a*(p2(1)+x*(p2(2)+x*(p2(3)+x*(p2(4)+x*(p2(5)+x*p2(6))))))
         a = a/(q2(1)+x*(q2(2)+x*(q2(3)+x*(q2(4)+x*(q2(5)+x*q2(6))))))
         if (xx .lt. 0.D0) then
            erfc = 2.D0-a
         else
            erfc = a
         end if
         return
      else
         a= x*(p1(1)+x2*(p1(2)+x2*(p1(3)+x2*p1(4))))
         a = a/(q1(1)+x2*(q1(2)+x2*(q1(3)+x2*q1(4))))
         if (xx .lt. 0.D0) then
            erfc = 1.D0+a
         else
            erfc = 1.D0-a
         end if
         return
      end if
!EOC
      end function erfc
!c
!c     ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
!BOP
!!ROUTINE: simpun
!!INTERFACE:
      subroutine simpun(xx,fx,nx,i,ax)
!!DESCRIPTION:
! {\bv
! Simpson's integration, uniform mesh
! \ev}

!!DO_NOT_PRINT
      implicit real*8 (a-h,o-z)

!!ARGUMENTS:
      integer i,nx
      real(8), dimension(nx+1) ::  xx,fx,ax
!AUTHOR:
! J. Barish (1970),
! Computing Technology Center, Union Carbide Corp., Nuclear Div.,
! Oak Ridge, TN.
!
!EOP
!c
      integer ix,ic
      real(8) :: d1,d2,d3,a2,a3
!c
      if (i.gt.0) then
      ax(1)=0.0d+00
      do ix=2,nx,2
         d1=xx(ix)-xx(ix-1)
         ax(ix)=ax(ix-1)+d1/2.0d+00*(fx(ix)+fx(ix-1))
         if (nx.eq.ix) then
            return
         endif
         d2=xx(ix+1)-xx(ix-1)
         d3=d2/d1
         a2=d3/6.0d+00*d2**2/(xx(ix+1)-xx(ix))
         a3=d2/2.0d+00-a2/d3
         ax(ix+1)=ax(ix-1)+(d2-a2-a3)*fx(ix-1)+a2*fx(ix)+a3*fx(ix+1)
      enddo
      else
      ax(nx)=0.0d+00
      do ix=2,nx,2
         ic=nx+1-ix
         d1=xx(ic+1)-xx(ic)
         ax(ic)=ax(ic+1)+d1/2.0d+00*(fx(ic+1)+fx(ic))
         if (nx.eq.ix) then
            return
         endif
         d2=xx(ic+1)-xx(ic-1)
         d3=d2/(xx(ic)-xx(ic-1))
         a2=d3/6.0d+00*d2**2/d1
         a3=d2/2.0d+00-a2/d3
         ax(ic-1)=ax(ic+1)+(d2-a2-a3)*fx(ic-1)+a2*fx(ic)+ a3*fx(ic+1)
      enddo
      endif
!c
      return
      end subroutine simpun
!
!BOP
!!ROUTINE: g_pi
!!INTERFACE:
      function g_pi()
!!DESCRIPTION:
! $\pi$ value
!
!!USES:
      use universal_const
!
!EOP
!
!BOC
      real(8) :: g_pi
      g_pi = pi
      return
!EOC
      end function g_pi
!
!BOP
!!ROUTINE: g_ry2H
!!INTERFACE:
      function g_ry2H()
!!DESCRIPTION:
! Ry / Hartree ratio
!
!!USES:
      use universal_const
!
!EOP
!
!BOC
      real(8) :: g_ry2H
      g_ry2H = ry2H
      return
!EOC
      end function g_ry2H
!
!BOP
!!ROUTINE: g_ry2eV
!!INTERFACE:
      function g_ry2eV()
!!DESCRIPTION:
! Ry / eV ratio
!
!!USES:
      use universal_const
!
!EOP
!
!BOC
      real(8) :: g_ry2eV
      g_ry2eV = ry2eV
      return
!EOC
      end function g_ry2eV
!
!BOP
!!ROUTINE: g_ry2kelvin
!!INTERFACE:
      function g_ry2kelvin()
!!DESCRIPTION:
! Ry / Kelvin ratio (in a.u.)
!
!!USES:
      use universal_const
!
!EOP
!
!BOC
      real(8) :: g_ry2kelvin
      g_ry2kelvin = ry2kelvin
      return
!EOC
      end function g_ry2kelvin
!
!BOP
!!ROUTINE: g_bohr2A
!!INTERFACE:
      function g_bohr2A()
!!DESCRIPTION:
! Bohr / Angstroem ratio
!
!!USES:
      use universal_const
!
!EOP
!
!BOC
      real(8) :: g_bohr2A
      g_bohr2A = bohr2A
      return
!EOC
      end function g_bohr2A
!
!BOP
!!ROUTINE: g_ry2Mbar
!!INTERFACE:
      function g_ry2Mbar()
!!DESCRIPTION:
! Ry / Mbar ratio (in a.u.)
!
!!USES:
      use universal_const
!
!EOP
!
!BOC
      real(8) :: g_ry2Mbar
      g_ry2Mbar = pfact
      return
!EOC
      end function g_ry2Mbar

!BOP
!!ROUTINE: char2int
!!INTERFACE:
      subroutine char2int(word,intword,n)
!!DESCRIPTION:
! character word --> integer word
!
!EOP
!
!BOC
      implicit none
      integer :: n
      character(*) :: word
      integer :: intword(n)
      character(1) :: s(n)
      integer :: i,l

      read(word,'(1000a1)',iostat=l) s
      if ( l.ne.0 ) then
       intword = ichar('%')
       return
      end if
      do i=1,size(s)
       intword(i) = ichar(s(i))
      end do
      return
!EOC
      end subroutine char2int

!BOP
!!ROUTINE: int2char
!!INTERFACE:
      subroutine int2char(intword,word,n)
!!DESCRIPTION:
! integer word --> character word
!
!EOP
!
!BOC
      implicit none
      integer :: n
      integer :: intword(n)
      character*(*) word
      character(1) :: s(n)
      integer :: i
      do i=1,n
       s(i) = char(intword(i))
      end do
      write(word,'(1000a1)') s
      return
!EOC
      end subroutine int2char

!BOP
!!ROUTINE: findRmax
!!INTERFACE:
      subroutine findRmax(jend,func,rr,imx,rmx)
!!DESCRIPTION:
! to find position of first maximum, {\tt rmx} of table function {\tt func(rr(1:jend))};
! {\tt imx} is an index pointing to the interval $rr(imx)<rmx<rr(imx+1)$
!
!!DO_NOT_PRINT
      implicit none

!!ARGUMENTS:
      integer, intent(in) :: jend
      real(8), intent(in) :: func(jend),rr(jend)
      integer, intent(out) :: imx
      real(8), intent(out) :: rmx
!!REVISION HISTORY:
! Initial version - A.S. - 2015
!!REMARKS
! if there are several maximums, only the closest one
! to rr(jend) can be found;
! maximum cannot be found if it is above rr(jend-1);
! if imx=jend and rmx=0, then maximum is not found
!EOP
!
!BOC
      real(8) :: dy(jend)
      real(8) :: x1,x2,fmx
      integer :: ir,i1,i2,iex
      real(8), external :: ylag

!      imx = 0
!      rmx = 0
!      return

!1     read(89,*,end=2) rmx
!      do ir=jend,4,-1
!       if ( rr(ir)<rmx ) then
!        imx = ir
!        exit
!       end if
!      end do
!      return
!2     rewind(89)
!      if ( jend>1 ) go to 1

      imx = jend
      call derv5_VP(func(:),dy(:),rr(:),1,jend)
      rmx = 0
      fmx = 0
      do ir=jend-2,1,-1
       if ( dy(ir) > 0 ) then
        if ( dy(ir+1) <= 0 ) then
         x1 = rr(ir)
         x2 = rr(ir+1)
         i2 = ir+2
         i1 = max(1,ir-3)
         do
          rmx = 0.5d0*(x1+x2)
          fmx=-ylag(rmx,rr(i1:i2),dy(i1:i2),0,3,i2-i1+1,iex)
          if ( abs(fmx)<1.d-8 .or. abs(x2-x1)<1.d-14 ) then
           rmx = 0.5d0*(x1+x2)
           imx = ir
           exit
          end if
          if ( fmx<0 ) then
           x1 = rmx
          else
           x2 = rmx
          end if
         end do
        end if
        exit
       end if
      end do
      return
!EOC
      end subroutine findRmax

!BOP
!!ROUTINE: isLetter
!!INTERFACE:
       logical function isLetter(symb)
!!DESCRIPTION:
! returns result if {\tt symb} is a letter
!
!!ARGUMENTS:
       character*1 symb
!!REVISION HISTORY:
! Initial Version - A.S. - 2007
!EOP
!
!BOC
       integer i
       i = iachar(symb)
       if (                                                             &
     &      ( i.gt.64 .and. i.lt.91 )                                   &
     &   .or.                                                           &
     &      ( i.gt.96 .and. i.lt.123 )                                  &
     &     ) then
         isLetter = .TRUE.
       else
         isLetter = .FALSE.
       end if
       return
!EOC
       end function isLetter
!
!BOP
!!ROUTINE: isDigit
!!INTERFACE:
       logical function isDigit(symb)
!!DESCRIPTION:
! returns result if {\tt symb} is a digit
!
!!ARGUMENTS:
       character*1 symb
!!REVISION HISTORY:
! Initial Version - A.S. - 2007
!EOP
!
!BOC
       integer i
       i = iachar(symb)
       if ( i.ge.48 .and. i.le.57 ) then
         isDigit = .TRUE.
       else
         isDigit = .FALSE.
       end if
       return
!EOC
       end function isDigit
!
!BOP
!!ROUTINE: toLower
!!INTERFACE:
       character*1 function toLower(symb)
!!DESCRIPTION:
! returns {\tt symb} in a lower case

!!DO_NOT_PRINT
       implicit none
!!DO_NOT_PRINT

!!ARGUMENTS:
       character*1 symb
!!REVISION HISTORY:
! Initial Version - A.S. - 2007
!EOP
!
!BOC
       integer i
       integer, parameter :: A = iachar('A')
       integer, parameter :: Z = iachar('Z')
       integer, parameter :: dd = iachar('z') - iachar('Z')
       i = iachar(symb)
       if ( i>=A .and. i<=Z ) then
           toLower = achar(i+dd)
       else
           toLower = symb
       end if
!       if ( i.gt.64 .and. i.lt.91 ) then
!         toLower = achar(i+32)
!       else
!         toLower = symb
!       end if
       return
!EOC
       end function toLower
!
!BOP
!!ROUTINE: idToZ
!!INTERFACE:
       integer function idToZ(id)
!!DESCRIPTION:
! translates {\tt id} to atomic number

!!DO_NOT_PRINT
       implicit none
!!DO_NOT_PRINT

!!ARGUMENTS:
       character*1 id(2)
!!REVISION HISTORY:
! Initial Version - A.S. - 2007
!EOP
!
!BOC
       integer i
       character*1 symb(2)
       character*1 idout(2)
       character*2 idtmp
       equivalence (idtmp,idout)
       character*15 sName
       character*1 toLower
       external toLower
       logical isLetter
       external isLetter
       idToZ = -1
       if (id(1) .eq. ' ' .or. isLetter(id(1))) then

        if (id(1) .eq. ' ') then
          symb(1) = toLower(id(2))
          symb(2) = ' '
        else
         symb(1) = toLower(id(1))
         symb(2) = ' '
         if ( isLetter(id(2)) ) then
          if( id(1).eq.'E' .and. id(2).eq.'S' ) then
            symb(2) =  id(2)
            symb(1) =  id(1)
          else
            symb(2) = toLower(id(2))
          end if
         end if
        end if

        do i=0,103
         idout(1:2) = ' '
         call atomID(dble(i),idtmp,sName)
         if (idout(1) .eq. ' ') then
           idout(1) = idout(2)
           idout(2) = ' '
         end if
         if ( idout(1) .eq. symb(1) .and. idout(2) .eq. symb(2) ) then
          idToZ = i
          exit
         end if
        end do

       end if
       return
!EOC
       end function idToZ

!BOP
!!ROUTINE: DGAUS8m
!!INTERFACE:
      SUBROUTINE DGAUS8m (FUN, A, B, FUNPAR, ERR, ANS, IERR)
!!DESCRIPTION:
! {\bv
! Modified DGAUS8 from SLATEC-library
!
!***PURPOSE  Integrate a real function of one variable over a finite
!            interval using an adaptive 8-point Legendre-Gauss
!            algorithm.  Intended primarily for high accuracy
!            integration or integration of smooth functions.
!***DESCRIPTION
!
!     Abstract  *** a DOUBLE PRECISION routine ***
!        DGAUS8 integrates real functions of one variable over finite
!        intervals using an adaptive 8-point Legendre-Gauss algorithm.
!        DGAUS8 is intended primarily for high accuracy integration
!        or integration of smooth functions.
!
!        The maximum number of significant digits obtainable in ANS
!        is the smaller of 18 and the number of digits carried in
!        double precision arithmetic.
!
!     Description of Arguments
!
!        Input--* FUN, A, B, ERR are DOUBLE PRECISION *
!        FUN - name of external function to be integrated.  This name
!              must be in an EXTERNAL statement in the calling program.
!              FUN must be a DOUBLE PRECISION function of one DOUBLE
!              PRECISION argument.  The value of the argument to FUN
!              is the variable of integration which ranges from A to B.
!        A   - lower limit of integration
!        B   - upper limit of integration (may be less than A)
!        ERR - is a requested pseudorelative error tolerance.  Normally
!              pick a value of ABS(ERR) so that DTOL .LT. ABS(ERR) .LE.
!              1.0D-3 where DTOL is the larger of 1.0D-18 and the
!              double precision unit roundoff D1MACH(4).  ANS will
!              normally have no more error than ABS(ERR) times the
!              integral of the absolute value of FUN(X).  Usually,
!              smaller values of ERR yield more accuracy and require
!              more function evaluations.
!
!              A negative value for ERR causes an estimate of the
!              absolute error in ANS to be returned in ERR.  Note that
!              ERR must be a variable (not a constant) in this case.
!              Note also that the user must reset the value of ERR
!              before making any more calls that use the variable ERR.
!
!        Output--* ERR,ANS are double precision *
!        ERR - will be an estimate of the absolute error in ANS if the
!              input value of ERR was negative.  (ERR is unchanged if
!              the input value of ERR was non-negative.)  The estimated
!              error is solely for information to the user and should
!              not be used as a correction to the computed integral.
!        ANS - computed value of integral
!        IERR- a status code
!            --Normal codes
!               1 ANS most likely meets requested error tolerance,
!                 or A=B.
!              -1 A and B are too nearly equal to allow normal
!                 integration.  ANS is set to zero.
!            --Abnormal code
!               2 ANS probably does not meet requested error tolerance.
! \ev}

!!REVISION HISTORY:
! Adapted - A.S. - 2013
!AUTHOR:
! Jones, R. E., (SNLA)
!EOP
!
!BOC
      IMPLICIT NONE

      INTEGER IERR, K, KML, KMX, L, LMN, LMX, LR, MXL, NBITS,           &
     & NIB, NLMN, NLMX
      INTEGER I1MACH
      DOUBLE PRECISION A,AA,AE,ANIB,ANS,AREA,B,C,CE,EE,EF,              &
     & EPS, ERR, EST, GL, GLR, GR, HH, SQ2, TOL, VL, VR, W1, W2, W3,    &
     & W4, X1, X2, X3, X4, X, H
      DOUBLE PRECISION D1MACH
      DOUBLE PRECISION G8, FUN
      EXTERNAL FUN

      DOUBLE PRECISION FUNPAR(*)

      DIMENSION AA(60), HH(60), LR(60), VL(60), GR(60)
      SAVE X1, X2, X3, X4, W1, W2, W3, W4, SQ2,                         &
     & NLMN, KMX, KML
      DATA X1, X2, X3, X4/                                              &
     &     1.83434642495649805D-01,     5.25532409916328986D-01,        &
     &     7.96666477413626740D-01,     9.60289856497536232D-01/
      DATA W1, W2, W3, W4/                                              &
     &     3.62683783378361983D-01,     3.13706645877887287D-01,        &
     &     2.22381034453374471D-01,     1.01228536290376259D-01/
      DATA SQ2/1.414213562373095D0/
      DATA NLMN/1/,KMX/5000/,KML/6/
      G8(X,H)=H*(                                                       &
     &  (W1*(FUN(X-X1*H,FUNPAR) + FUN(X+X1*H,FUNPAR))                   &
     &  +W2*(FUN(X-X2*H,FUNPAR) + FUN(X+X2*H,FUNPAR)))                  &
     & +(W3*(FUN(X-X3*H,FUNPAR) + FUN(X+X3*H,FUNPAR))                   &
     &  +W4*(FUN(X-X4*H,FUNPAR) + FUN(X+X4*H,FUNPAR))))
!C***FIRST EXECUTABLE STATEMENT  DGAUS8
!C
!C     Initialize
!C

      K = I1MACH(14)
!cab      K = 53

      ANIB = D1MACH(5)*K/log10(2.d0)
!cab      ANIB = LOG10(2.d0)*K/0.30102000D0

      NBITS = ANIB
      NLMX = MIN(60,(NBITS*5)/8)
      ANS = 0.0D0
      IERR = 1
      CE = 0.0D0
      IF (A .EQ. B) GO TO 140
      LMX = NLMX
      LMN = NLMN
      IF (B .EQ. 0.0D0) GO TO 10
      IF (SIGN(1.0D0,B)*A .LE. 0.0D0) GO TO 10
      C = ABS(1.0D0-A/B)
      IF (C .GT. 0.1D0) GO TO 10
      IF (C .LE. 0.0D0) GO TO 140
      ANIB = 0.5D0 - LOG(C)/log(2.d0)
      NIB = ANIB
      LMX = MIN(NLMX,NBITS-NIB-7)
      IF (LMX .LT. 1) GO TO 130
      LMN = MIN(LMN,LMX)
   10 TOL = MAX(ABS(ERR),2.0D0**(5-NBITS))/2.0D0

      IF (ERR .EQ. 0.0D0) TOL = SQRT(D1MACH(4))
!cab      IF (ERR .EQ. 0.0D0) TOL = SQRT(1.d-15)

      EPS = TOL
      HH(1) = (B-A)/4.0D0
      AA(1) = A
      LR(1) = 1
      L = 1
      EST = G8(AA(L)+2.0D0*HH(L),2.0D0*HH(L))
!CAB
      if(abs(EST).lt.1.d-16) then
       ANS = 0.d0
       CE =  EST
       go to 140
      end if
!CAB
      K = 8
      AREA = ABS(EST)
      EF = 0.5D0
      MXL = 0
!C
!C     Compute refined estimates, estimate the error, etc.
!C
   20 GL = G8(AA(L)+HH(L),HH(L))
      GR(L) = G8(AA(L)+3.0D0*HH(L),HH(L))
      K = K + 16
      AREA = AREA + (ABS(GL)+ABS(GR(L))-ABS(EST))
!C     IF (L .LT .LMN) GO TO 11
      GLR = GL + GR(L)
      EE = ABS(EST-GLR)*EF
      AE = MAX(EPS*AREA,TOL*ABS(GLR))
      IF (EE-AE) 40, 40, 50
   30 MXL = 1
   40 CE = CE + (EST-GLR)
      IF (LR(L)) 60, 60, 80
!C
!C     Consider the left half of this level
!C
   50 IF (K .GT. KMX) LMX = KML
      IF (L .GE. LMX) GO TO 30
      L = L + 1
      EPS = EPS*0.5D0
      EF = EF/SQ2
      HH(L) = HH(L-1)*0.5D0
      LR(L) = -1
      AA(L) = AA(L-1)
      EST = GL
      GO TO 20
!C
!C     Proceed to right half at this level
!C
   60 VL(L) = GLR
   70 EST = GR(L-1)
      LR(L) = 1
      AA(L) = AA(L) + 4.0D0*HH(L)
      GO TO 20
!C
!C     Return one level
!C
   80 VR = GLR
   90 IF (L .LE. 1) GO TO 120
      L = L - 1
      EPS = EPS*2.0D0
      EF = EF*SQ2
      IF (LR(L)) 100, 100, 110
  100 VL(L) = VL(L+1) + VR
      GO TO 70
  110 VR = VL(L+1) + VR
      GO TO 90
!C
!C     Exit
!C
  120 ANS = VR
      IF ((MXL.EQ.0) .OR. (ABS(CE).LE.2.0D0*TOL*AREA)) GO TO 140
      IERR = 2
!CAB      CALL XERMSG ('SLATEC', 'DGAUS8',
!CAB     +   'ANS is probably insufficiently accurate.', 3, 1)
      GO TO 140
  130 IERR = -1
!CAB      CALL XERMSG ('SLATEC', 'DGAUS8',
!CAB     +   'A and B are too nearly equal to allow normal integration. $ !$'
!CAB     +   // 'ANS is set to zero and IERR to -1.', 1, -1)
  140 IF (ERR .LT. 0.0D0) ERR = CE
      RETURN
      END SUBROUTINE DGAUS8m

!BOP
!!ROUTINE: DSORT
!!INTERFACE:
      SUBROUTINE DSORT (DX, DY, N, KFLAG)
!!DESCRIPTION:
! {\bv
!***PURPOSE  Sort an array and optionally make the same interchanges in
!            an auxiliary array.  The array may be sorted in increasing
!            or decreasing order.  A slightly modified QUICKSORT
!            algorithm is used.
!***LIBRARY   SLATEC
!
!   DSORT sorts array DX and optionally makes the same interchanges in
!   array DY.  The array DX may be sorted in increasing order or
!   decreasing order.  A slightly modified quicksort algorithm is used.
!
!   Description of Parameters
!      DX - array of values to be sorted   (usually abscissas)
!      DY - array to be (optionally) carried along
!      N  - number of values in array DX to be sorted
!      KFLAG - control parameter
!            =  2  means sort DX in increasing order and carry DY along.
!            =  1  means sort DX in increasing order (ignoring DY)
!            = -1  means sort DX in decreasing order (ignoring DY)
!            = -2  means sort DX in decreasing order and carry DY along.
!
!***REFERENCES  R. C. Singleton, Algorithm 347, An efficient algorithm
!                 for sorting with minimal storage, Communications of
!                 the ACM, 12, 3 (1969), pp. 185-187.
!
! \ev}
!AUTHORS:
! Jones, R. E., (SNLA)
! Wisniewski, J. A., (SNLA)
!EOP
!
!BOC
      INTEGER KFLAG, N
!C     .. Array Arguments ..
      DOUBLE PRECISION DX(*), DY(*)
!C     .. Local Scalars ..
      DOUBLE PRECISION R, T, TT, TTY, TY
      INTEGER I, IJ, J, K, KK, L, M, NN
!C     .. Local Arrays ..
      INTEGER IL(21), IU(21)
!C     .. External Subroutines ..
!c      EXTERNAL XERMSG
!C     .. Intrinsic Functions ..
      INTRINSIC ABS, INT
      character*10 sname/'dsort'/
!C***FIRST EXECUTABLE STATEMENT  DSORT
      NN = N
      IF (NN .LT. 1) THEN
        write(6,'('' The number of values to be sorted                  &
     &     is not positive '')')
!c        CALL XERMSG ('SLATEC', 'DSORT',
!c    +      'The number of values to be sorted is not positive.', 1, 1)
       call p_fstop(sname)
!c        RETURN
      ENDIF
!C
      KK = ABS(KFLAG)
      IF (KK.NE.1 .AND. KK.NE.2) THEN
!c        CALL XERMSG ('SLATEC', 'DSORT',
!c    +      'The sort control parameter, K, is not 2, 1, -1, or -2.', 2,
!c    +      1)
       write(6,'('' The sort control parameter, K,                      &
     & is not 2, 1, -1, or -2 '')')
       call p_fstop(sname)
!c        RETURN
      ENDIF
!C
!C     Alter array DX to get decreasing order if needed
!C
      IF (KFLAG .LE. -1) THEN
         DO 10 I=1,NN
            DX(I) = -DX(I)
   10    CONTINUE
      ENDIF
!C
      IF (KK .EQ. 2) GO TO 100
!C
!C     Sort DX only
!C
      M = 1
      I = 1
      J = NN
      R = 0.375D0
!C
   20 IF (I .EQ. J) GO TO 60
      IF (R .LE. 0.5898437D0) THEN
         R = R+3.90625D-2
      ELSE
         R = R-0.21875D0
      ENDIF
!C
   30 K = I
!C
!C     Select a central element of the array and save it in location T
!C
      IJ = I + INT((J-I)*R)
      T = DX(IJ)
!C
!C     If first element of array is greater than T, interchange with T
!C
      IF (DX(I) .GT. T) THEN
         DX(IJ) = DX(I)
         DX(I) = T
         T = DX(IJ)
      ENDIF
      L = J
!C
!C     If last element of array is less than than T, interchange with T
!C
      IF (DX(J) .LT. T) THEN
         DX(IJ) = DX(J)
         DX(J) = T
         T = DX(IJ)
!C
!C        If first element of array is greater than T, interchange with T
!C
         IF (DX(I) .GT. T) THEN
            DX(IJ) = DX(I)
            DX(I) = T
            T = DX(IJ)
         ENDIF
      ENDIF
!C
!C     Find an element in the second half of the array which is smaller
!C     than T
!C
   40 L = L-1
      IF (DX(L) .GT. T) GO TO 40
!C
!C     Find an element in the first half of the array which is greater
!C     than T
!C
   50 K = K+1
      IF (DX(K) .LT. T) GO TO 50
!C
!C     Interchange these elements
!C
      IF (K .LE. L) THEN
         TT = DX(L)
         DX(L) = DX(K)
         DX(K) = TT
         GO TO 40
      ENDIF
!C
!C     Save upper and lower subscripts of the array yet to be sorted
!C
      IF (L-I .GT. J-K) THEN
         IL(M) = I
         IU(M) = L
         I = K
         M = M+1
      ELSE
         IL(M) = K
         IU(M) = J
         J = L
         M = M+1
      ENDIF
      GO TO 70
!C
!C     Begin again on another portion of the unsorted array
!C
   60 M = M-1
      IF (M .EQ. 0) GO TO 190
      I = IL(M)
      J = IU(M)
!C
   70 IF (J-I .GE. 1) GO TO 30
      IF (I .EQ. 1) GO TO 20
      I = I-1
!C
   80 I = I+1
      IF (I .EQ. J) GO TO 60
      T = DX(I+1)
      IF (DX(I) .LE. T) GO TO 80
      K = I
!C
   90 DX(K+1) = DX(K)
      K = K-1
      IF (T .LT. DX(K)) GO TO 90
      DX(K+1) = T
      GO TO 80
!C
!C     Sort DX and carry DY along
!C
  100 M = 1
      I = 1
      J = NN
      R = 0.375D0
!C
  110 IF (I .EQ. J) GO TO 150
      IF (R .LE. 0.5898437D0) THEN
         R = R+3.90625D-2
      ELSE
         R = R-0.21875D0
      ENDIF
!C
  120 K = I
!C
!C     Select a central element of the array and save it in location T
!C
      IJ = I + INT((J-I)*R)
      T = DX(IJ)
      TY = DY(IJ)
!C
!C     If first element of array is greater than T, interchange with T
!C
      IF (DX(I) .GT. T) THEN
         DX(IJ) = DX(I)
         DX(I) = T
         T = DX(IJ)
         DY(IJ) = DY(I)
         DY(I) = TY
         TY = DY(IJ)
      ENDIF
      L = J
!C
!C     If last element of array is less than T, interchange with T
!C
      IF (DX(J) .LT. T) THEN
         DX(IJ) = DX(J)
         DX(J) = T
         T = DX(IJ)
         DY(IJ) = DY(J)
         DY(J) = TY
         TY = DY(IJ)
!C
!C        If first element of array is greater than T, interchange with T
!C
         IF (DX(I) .GT. T) THEN
            DX(IJ) = DX(I)
            DX(I) = T
            T = DX(IJ)
            DY(IJ) = DY(I)
            DY(I) = TY
            TY = DY(IJ)
         ENDIF
      ENDIF
!C
!C     Find an element in the second half of the array which is smaller
!C     than T
!C
  130 L = L-1
      IF (DX(L) .GT. T) GO TO 130
!C
!C     Find an element in the first half of the array which is greater
!C     than T
!C
  140 K = K+1
      IF (DX(K) .LT. T) GO TO 140
!C
!C     Interchange these elements
!C
      IF (K .LE. L) THEN
         TT = DX(L)
         DX(L) = DX(K)
         DX(K) = TT
         TTY = DY(L)
         DY(L) = DY(K)
         DY(K) = TTY
         GO TO 130
      ENDIF
!C
!C     Save upper and lower subscripts of the array yet to be sorted
!C
      IF (L-I .GT. J-K) THEN
         IL(M) = I
         IU(M) = L
         I = K
         M = M+1
      ELSE
         IL(M) = K
         IU(M) = J
         J = L
         M = M+1
      ENDIF
      GO TO 160
!C
!C     Begin again on another portion of the unsorted array
!C
  150 M = M-1
      IF (M .EQ. 0) GO TO 190
      I = IL(M)
      J = IU(M)
!C
  160 IF (J-I .GE. 1) GO TO 120
      IF (I .EQ. 1) GO TO 110
      I = I-1
!C
  170 I = I+1
      IF (I .EQ. J) GO TO 150
      T = DX(I+1)
      TY = DY(I+1)
      IF (DX(I) .LE. T) GO TO 170
      K = I
!C
  180 DX(K+1) = DX(K)
      DY(K+1) = DY(K)
      K = K-1
      IF (T .LT. DX(K)) GO TO 180
      DX(K+1) = T
      DY(K+1) = TY
      GO TO 170
!C
!C     Clean up
!C
  190 IF (KFLAG .LE. -1) THEN
         DO 200 I=1,NN
            DX(I) = -DX(I)
  200    CONTINUE
      ENDIF
      RETURN
!EOC
      END SUBROUTINE DSORT

!BOP
!!ROUTINE: ISORT
!!INTERFACE:
      SUBROUTINE ISORT (IX, IY, N, KFLAG)
!!DESCRIPTION:
! {\bv
!***PURPOSE  Sort an array and optionally make the same interchanges in
!            an auxiliary array.  The array may be sorted in increasing
!            or decreasing order.  A slightly modified QUICKSORT
!            algorithm is used.
!***LIBRARY   SLATEC
!***DESCRIPTION
!
!   ISORT sorts array IX and optionally makes the same interchanges in
!   array IY.  The array IX may be sorted in increasing order or
!   decreasing order.  A slightly modified quicksort algorithm is used.
!
!   Description of Parameters
!      IX - integer array of values to be sorted
!      IY - integer array to be (optionally) carried along
!      N  - number of values in integer array IX to be sorted
!      KFLAG - control parameter
!            =  2  means sort IX in increasing order and carry IY along.
!            =  1  means sort IX in increasing order (ignoring IY)
!            = -1  means sort IX in decreasing order (ignoring IY)
!            = -2  means sort IX in decreasing order and carry IY along.
!
!***REFERENCES  R. C. Singleton, Algorithm 347, An efficient algorithm
!                 for sorting with minimal storage, Communications of
!                 the ACM, 12, 3 (1969), pp. 185-187.
!
! \ev}
!AUTHORS:
!  Jones, R. E., (SNLA)
!  Kahaner, D. K., (NBS)
!  Wisniewski, J. A., (SNLA)
!EOP
!
!BOC
      INTEGER KFLAG, N
!C     .. Array Arguments ..
      INTEGER IX(*), IY(*)
!C     .. Local Scalars ..
      REAL R
      INTEGER I, IJ, J, K, KK, L, M, NN, T, TT, TTY, TY
!C     .. Local Arrays ..
      INTEGER IL(21), IU(21)
!C     .. External Subroutines ..
!c      EXTERNAL XERMSG
!C     .. Intrinsic Functions ..
      INTRINSIC ABS, INT
!C***FIRST EXECUTABLE STATEMENT  ISORT
      NN = N
      IF (NN .LT. 1) THEN
!c         CALL XERMSG ('SLATEC', 'ISORT',
!c     +      'The number of values to be sorted is not positive.', 1, 1)
        call p_fstop(' ISORT : N .LE. 0')
         RETURN
      ENDIF
!C
      KK = ABS(KFLAG)
      IF (KK.NE.1 .AND. KK.NE.2) THEN
!c         CALL XERMSG ('SLATEC', 'ISORT',
!c     +      'The sort control parameter, K, is not 2, 1, -1, or -2.', 2 !,
!c     +      1)
        call p_fstop(' ISORT: K .NE. {-2,-1, 1, 2}')
         RETURN
      ENDIF
!C
!C     Alter array IX to get decreasing order if needed
!C
      IF (KFLAG .LE. -1) THEN
         DO 10 I=1,NN
            IX(I) = -IX(I)
   10    CONTINUE
      ENDIF
!C
      IF (KK .EQ. 2) GO TO 100
!C
!C     Sort IX only
!C
      M = 1
      I = 1
      J = NN
      R = 0.375E0
!C
   20 IF (I .EQ. J) GO TO 60
      IF (R .LE. 0.5898437E0) THEN
         R = R+3.90625E-2
      ELSE
         R = R-0.21875E0
      ENDIF
!C
   30 K = I
!C
!C     Select a central element of the array and save it in location T
!C
      IJ = I + INT((J-I)*R)
      T = IX(IJ)
!C
!C     If first element of array is greater than T, interchange with T
!C
      IF (IX(I) .GT. T) THEN
         IX(IJ) = IX(I)
         IX(I) = T
         T = IX(IJ)
      ENDIF
      L = J
!C
!C     If last element of array is less than than T, interchange with T
!C
      IF (IX(J) .LT. T) THEN
         IX(IJ) = IX(J)
         IX(J) = T
         T = IX(IJ)
!C
!C        If first element of array is greater than T, interchange with T
!C
         IF (IX(I) .GT. T) THEN
            IX(IJ) = IX(I)
            IX(I) = T
            T = IX(IJ)
         ENDIF
      ENDIF
!C
!C     Find an element in the second half of the array which is smaller
!C     than T
!C
   40 L = L-1
      IF (IX(L) .GT. T) GO TO 40
!C
!C     Find an element in the first half of the array which is greater
!C     than T
!C
   50 K = K+1
      IF (IX(K) .LT. T) GO TO 50
!C
!C     Interchange these elements
!C
      IF (K .LE. L) THEN
         TT = IX(L)
         IX(L) = IX(K)
         IX(K) = TT
         GO TO 40
      ENDIF
!C
!C     Save upper and lower subscripts of the array yet to be sorted
!C
      IF (L-I .GT. J-K) THEN
         IL(M) = I
         IU(M) = L
         I = K
         M = M+1
      ELSE
         IL(M) = K
         IU(M) = J
         J = L
         M = M+1
      ENDIF
      GO TO 70
!C
!C     Begin again on another portion of the unsorted array
!C
   60 M = M-1
      IF (M .EQ. 0) GO TO 190
      I = IL(M)
      J = IU(M)
!C
   70 IF (J-I .GE. 1) GO TO 30
      IF (I .EQ. 1) GO TO 20
      I = I-1
!C
   80 I = I+1
      IF (I .EQ. J) GO TO 60
      T = IX(I+1)
      IF (IX(I) .LE. T) GO TO 80
      K = I
!C
   90 IX(K+1) = IX(K)
      K = K-1
      IF (T .LT. IX(K)) GO TO 90
      IX(K+1) = T
      GO TO 80
!C
!C     Sort IX and carry IY along
!C
  100 M = 1
      I = 1
      J = NN
      R = 0.375E0
!C
  110 IF (I .EQ. J) GO TO 150
      IF (R .LE. 0.5898437E0) THEN
         R = R+3.90625E-2
      ELSE
         R = R-0.21875E0
      ENDIF
!C
  120 K = I
!C
!C     Select a central element of the array and save it in location T
!C
      IJ = I + INT((J-I)*R)
      T = IX(IJ)
      TY = IY(IJ)
!C
!C     If first element of array is greater than T, interchange with T
!C
      IF (IX(I) .GT. T) THEN
         IX(IJ) = IX(I)
         IX(I) = T
         T = IX(IJ)
         IY(IJ) = IY(I)
         IY(I) = TY
         TY = IY(IJ)
      ENDIF
      L = J
!C
!C     If last element of array is less than T, interchange with T
!C
      IF (IX(J) .LT. T) THEN
         IX(IJ) = IX(J)
         IX(J) = T
         T = IX(IJ)
         IY(IJ) = IY(J)
         IY(J) = TY
         TY = IY(IJ)
!C
!C        If first element of array is greater than T, interchange with T
!C
         IF (IX(I) .GT. T) THEN
            IX(IJ) = IX(I)
            IX(I) = T
            T = IX(IJ)
            IY(IJ) = IY(I)
            IY(I) = TY
            TY = IY(IJ)
         ENDIF
      ENDIF
!C
!C     Find an element in the second half of the array which is smaller
!C     than T
!C
  130 L = L-1
      IF (IX(L) .GT. T) GO TO 130
!C
!C     Find an element in the first half of the array which is greater
!C     than T
!C
  140 K = K+1
      IF (IX(K) .LT. T) GO TO 140
!C
!C     Interchange these elements
!C
      IF (K .LE. L) THEN
         TT = IX(L)
         IX(L) = IX(K)
         IX(K) = TT
         TTY = IY(L)
         IY(L) = IY(K)
         IY(K) = TTY
         GO TO 130
      ENDIF
!C
!C     Save upper and lower subscripts of the array yet to be sorted
!C
      IF (L-I .GT. J-K) THEN
         IL(M) = I
         IU(M) = L
         I = K
         M = M+1
      ELSE
         IL(M) = K
         IU(M) = J
         J = L
         M = M+1
      ENDIF
      GO TO 160
!C
!C     Begin again on another portion of the unsorted array
!C
  150 M = M-1
      IF (M .EQ. 0) GO TO 190
      I = IL(M)
      J = IU(M)
!C
  160 IF (J-I .GE. 1) GO TO 120
      IF (I .EQ. 1) GO TO 110
      I = I-1
!C
  170 I = I+1
      IF (I .EQ. J) GO TO 150
      T = IX(I+1)
      TY = IY(I+1)
      IF (IX(I) .LE. T) GO TO 170
      K = I
!C
  180 IX(K+1) = IX(K)
      IY(K+1) = IY(K)
      K = K-1
      IF (T .LT. IX(K)) GO TO 180
      IX(K+1) = T
      IY(K+1) = TY
      GO TO 170
!C
!C     Clean up
!C
  190 IF (KFLAG .LE. -1) THEN
         DO 200 I=1,NN
            IX(I) = -IX(I)
  200    CONTINUE
      ENDIF
      RETURN
      END SUBROUTINE ISORT

!BOP
!!ROUTINE: DPSORT
!!INTERFACE:
      SUBROUTINE DPSORT (DX, N, IPERM, KFLAG, IER)
!!DESCRIPTION:
! {\bv
!***PURPOSE  Return the permutation vector generated by sorting a given
!            array and, optionally, rearrange the elements of the array.
!            The array may be sorted in increasing or decreasing order.
!            A slightly modified quicksort algorithm is used.
!***LIBRARY   SLATEC
!
!   DPSORT returns the permutation vector IPERM generated by sorting
!   the array DX and, optionally, rearranges the values in DX.  DX may
!   be sorted in increasing or decreasing order.  A slightly modified
!   quicksort algorithm is used.
!
!   IPERM is such that DX(IPERM(I)) is the Ith value in the
!   rearrangement of DX.  IPERM may be applied to another array by
!   calling IPPERM, SPPERM, DPPERM or HPPERM.
!
!   The main difference between DPSORT and its active sorting equivalent
!   DSORT is that the data are referenced indirectly rather than
!   directly.  Therefore, DPSORT should require approximately twice as
!   long to execute as DSORT.  However, DPSORT is more general.
!
!   Description of Parameters
!      DX - input/output -- double precision array of values to be
!           sorted.  If ABS(KFLAG) = 2, then the values in DX will be
!           rearranged on output; otherwise, they are unchanged.
!      N  - input -- number of values in array DX to be sorted.
!      IPERM - output -- permutation array such that IPERM(I) is the
!              index of the value in the original order of the
!              DX array that is in the Ith location in the sorted
!              order.
!      KFLAG - input -- control parameter:
!            =  2  means return the permutation vector resulting from
!                  sorting DX in increasing order and sort DX also.
!            =  1  means return the permutation vector resulting from
!                  sorting DX in increasing order and do not sort DX.
!            = -1  means return the permutation vector resulting from
!                  sorting DX in decreasing order and do not sort DX.
!            = -2  means return the permutation vector resulting from
!                  sorting DX in decreasing order and sort DX also.
!      IER - output -- error indicator:
!          =  0  if no error,
!          =  1  if N is zero or negative,
!          =  2  if KFLAG is not 2, 1, -1, or -2.
!***REFERENCES  R. C. Singleton, Algorithm 347, An efficient algorithm
!                 for sorting with minimal storage, Communications of
!                 the ACM, 12, 3 (1969), pp. 185-187.
!
! \ev}
!AUTHORS:
! Jones, R. E., (SNLA)
! Rhoads, G. S., (NBS)
! Wisniewski, J. A., (SNLA)
!EOP
!
!BOC
      INTEGER IER, KFLAG, N
!C     .. Array Arguments ..
      DOUBLE PRECISION DX(*)
      INTEGER IPERM(*)
!C     .. Local Scalars ..
      DOUBLE PRECISION R, TEMP
      INTEGER I, IJ, INDX, INDX0, ISTRT, J, K, KK, L, LM, LMT, M, NN
!C     .. Local Arrays ..
      INTEGER IL(21), IU(21)
!C     .. External Subroutines ..
!c      EXTERNAL XERMSG
!C     .. Intrinsic Functions ..
      INTRINSIC ABS, INT
!C***FIRST EXECUTABLE STATEMENT  DPSORT
      IER = 0
      NN = N
      IF (NN .LT. 1) THEN
         IER = 1
!c         CALL XERMSG ('SLATEC', 'DPSORT',
!c     +    'The number of values to be sorted, N, is not positive.',
!c     +    IER, 1)
         RETURN
      ENDIF
!C
      KK = ABS(KFLAG)
      IF (KK.NE.1 .AND. KK.NE.2) THEN
         IER = 2
!c         CALL XERMSG ('SLATEC', 'DPSORT',
!c     +    'The sort control parameter, KFLAG, is not 2, 1, -1, or -2.',
!c     +    IER, 1)
         RETURN
      ENDIF
!C
!C     Initialize permutation vector
!C
      DO 10 I=1,NN
         IPERM(I) = I
   10 CONTINUE
!C
!C     Return if only one value is to be sorted
!C
      IF (NN .EQ. 1) RETURN
!C
!C     Alter array DX to get decreasing order if needed
!C
      IF (KFLAG .LE. -1) THEN
         DO 20 I=1,NN
            DX(I) = -DX(I)
   20    CONTINUE
      ENDIF
!C
!C     Sort DX only
!C
      M = 1
      I = 1
      J = NN
      R = .375D0
!C
   30 IF (I .EQ. J) GO TO 80
      IF (R .LE. 0.5898437D0) THEN
         R = R+3.90625D-2
      ELSE
         R = R-0.21875D0
      ENDIF
!C
   40 K = I
!C
!C     Select a central element of the array and save it in location L
!C
      IJ = I + INT((J-I)*R)
      LM = IPERM(IJ)
!C
!C     If first element of array is greater than LM, interchange with LM
!C
      IF (DX(IPERM(I)) .GT. DX(LM)) THEN
         IPERM(IJ) = IPERM(I)
         IPERM(I) = LM
         LM = IPERM(IJ)
      ENDIF
      L = J
!C
!C     If last element of array is less than LM, interchange with LM
!C
      IF (DX(IPERM(J)) .LT. DX(LM)) THEN
         IPERM(IJ) = IPERM(J)
         IPERM(J) = LM
         LM = IPERM(IJ)
!C
!C        If first element of array is greater than LM, interchange
!C        with LM
!C
         IF (DX(IPERM(I)) .GT. DX(LM)) THEN
            IPERM(IJ) = IPERM(I)
            IPERM(I) = LM
            LM = IPERM(IJ)
         ENDIF
      ENDIF
      GO TO 60
   50 LMT = IPERM(L)
      IPERM(L) = IPERM(K)
      IPERM(K) = LMT
!C
!C     Find an element in the second half of the array which is smaller
!C     than LM
!C
   60 L = L-1
      IF (DX(IPERM(L)) .GT. DX(LM)) GO TO 60
!C
!C     Find an element in the first half of the array which is greater
!C     than LM
!C
   70 K = K+1
      IF (DX(IPERM(K)) .LT. DX(LM)) GO TO 70
!C
!C     Interchange these elements
!C
      IF (K .LE. L) GO TO 50
!C
!C     Save upper and lower subscripts of the array yet to be sorted
!C
      IF (L-I .GT. J-K) THEN
         IL(M) = I
         IU(M) = L
         I = K
         M = M+1
      ELSE
         IL(M) = K
         IU(M) = J
         J = L
         M = M+1
      ENDIF
      GO TO 90
!C
!C     Begin again on another portion of the unsorted array
!C
   80 M = M-1
      IF (M .EQ. 0) GO TO 120
      I = IL(M)
      J = IU(M)
!C
   90 IF (J-I .GE. 1) GO TO 40
      IF (I .EQ. 1) GO TO 30
      I = I-1
!C
  100 I = I+1
      IF (I .EQ. J) GO TO 80
      LM = IPERM(I+1)
      IF (DX(IPERM(I)) .LE. DX(LM)) GO TO 100
      K = I
!C
  110 IPERM(K+1) = IPERM(K)
      K = K-1
      IF (DX(LM) .LT. DX(IPERM(K))) GO TO 110
      IPERM(K+1) = LM
      GO TO 100
!C
!C     Clean up
!C
  120 IF (KFLAG .LE. -1) THEN
         DO 130 I=1,NN
            DX(I) = -DX(I)
  130    CONTINUE
      ENDIF
!C
!C     Rearrange the values of DX if desired
!C
      IF (KK .EQ. 2) THEN
!C
!C        Use the IPERM vector as a flag.
!C        If IPERM(I) < 0, then the I-th value is in correct location
!C
         DO 150 ISTRT=1,NN
            IF (IPERM(ISTRT) .GE. 0) THEN
               INDX = ISTRT
               INDX0 = INDX
               TEMP = DX(ISTRT)
  140          IF (IPERM(INDX) .GT. 0) THEN
                  DX(INDX) = DX(IPERM(INDX))
                  INDX0 = INDX
                  IPERM(INDX) = -IPERM(INDX)
                  INDX = ABS(IPERM(INDX))
                  GO TO 140
               ENDIF
               DX(INDX0) = TEMP
            ENDIF
  150    CONTINUE
!C
!C        Revert the signs of the IPERM values
!C
         DO 160 I=1,NN
            IPERM(I) = -IPERM(I)
  160    CONTINUE
!C
      ENDIF
!C
      RETURN
      END SUBROUTINE DPSORT
!C
!BOP
!!ROUTINE: D1MACH
!!INTERFACE:
      real*8 FUNCTION D1MACH (I)
!!DESCRIPTION:
! {\bv
! D1MACH can be used to obtain machine-dependent parameters for the
! local machine environment.  It is a function subprogram with one
! (input) argument, and can be referenced as follows:
!
!      D = D1MACH(I)
!
! where I=1,...,5.  The (output) value of D above is determined by
! the (input) value of I.  The results for various values of I are
! discussed below.
!
! D1MACH( 1) = B**(EMIN-1), the smallest positive magnitude.
! D1MACH( 2) = B**EMAX*(1 - B**(-T)), the largest magnitude.
! D1MACH( 3) = B**(-T), the smallest relative spacing.
! D1MACH( 4) = B**(1-T), the largest relative spacing.
! D1MACH( 5) = LOG10(B)
!
! Assume double precision numbers are represented in the T-digit,
! base-B form
!
!            sign (B**E)*( (X(1)/B) + ... + (X(T)/B**T) )
!
! where 0 .LE. X(I) .LT. B for I=1,...,T, 0 .LT. X(1), and
! EMIN .LE. E .LE. EMAX.
!
! The values of B, T, EMIN and EMAX are provided in I1MACH as
! follows:
!
! I1MACH(10) = B, the base.
! I1MACH(14) = T, the number of base-B digits.
! I1MACH(15) = EMIN, the smallest exponent E.
! I1MACH(16) = EMAX, the largest exponent E.
! \ev}
!!ARGUMENTS:
      integer :: I
!!REVISION HISTORY:
! Adapted - A.S. - 2013
!!REMARKS:
! slatec original code is replaced by internal fortran functions
!EOP
!
!BOC
      integer, save :: isave=0
      real(8), save :: DMACH(5)
      real(8), parameter :: x=1.d0
      IF (I .LT. 1 .OR. I .GT. 5) STOP ' D1MACH ERROR'
!C
      if(isave.eq.0) then
       dmach(1) = tiny(x)
       dmach(2) = huge(x)
       dmach(3) = epsilon(x)/radix(x)
       dmach(4) = epsilon(x)
       dmach(5) = log10(dble(radix(x)))
       isave = 1
      end if

      D1MACH = DMACH(I)
      RETURN
!EOC
      END FUNCTION D1MACH
!
!BOP
!!ROUTINE: I1MACH
!!INTERFACE:
      INTEGER FUNCTION I1MACH (I)
!!DESCRIPTION:
! {\bv
! I1MACH can be used to obtain machine-dependent parameters for the
! local machine environment.  It is a function subprogram with one
! (input) argument and can be referenced as follows:
!
!      K = I1MACH(I)
!
! where I=1,...,16.  The (output) value of K above is determined by
! the (input) value of I.  The results for various values of I are
! discussed below.
!
! I/O unit numbers:
!
!   I1MACH( 1) = the standard input unit.
!   I1MACH( 2) = the standard output unit.
!   I1MACH( 3) = the standard punch unit.
!   I1MACH( 4) = the standard error message unit.
!
! Words:
!
!   I1MACH( 5) = the number of bits per integer storage unit.
!   I1MACH( 6) = the number of characters per integer storage unit.
!
! Integers:
!
!   assume integers are represented in the S-digit, base-A form
!
!    sign ( X(S-1)*A**(S-1) + ... + X(1)*A + X(0) ) ,
!              where 0 .LE. X(I) .LT. A for I=0,...,S-1.
!
!   I1MACH( 7) = A, the base.
!   I1MACH( 8) = S, the number of base-A digits.
!   I1MACH( 9) = A**S - 1, the largest magnitude.
!
! Floating-Point Numbers:
!   Assume floating-point numbers are represented in the T-digit,
!   base-B form
!
!              sign (B**E)*( (X(1)/B) + ... + (X(T)/B**T) )
!
!              where 0 .LE. X(I) .LT. B for I=1,...,T,
!              0 .LT. X(1), and EMIN .LE. E .LE. EMAX.
!
!   I1MACH(10) = B, the base.
!
! Single-Precision:
!
!   I1MACH(11) = T, the number of base-B digits.
!   I1MACH(12) = EMIN, the smallest exponent E.
!   I1MACH(13) = EMAX, the largest exponent E.
!
! Double-Precision:
!
!   I1MACH(14) = T, the number of base-B digits.
!   I1MACH(15) = EMIN, the smallest exponent E.
!   I1MACH(16) = EMAX, the largest exponent E.
!
! The values of I1MACH(1) - I1MACH(6) should be
! checked for consistency with the local operating system.
! \ev}
!!ARGUMENTS:
      integer :: I
!!REVISION HISTORY:
! Adapted - A.S. - 2013
!!REMARKS:
! slatec original code is replaced by internal fortran functions
!EOP
!
!BOC
      integer, save :: isave=0
      integer, save :: IMACH(16)
      INTEGER OUTPUT
      real(4) :: xr4
      real(8) :: xr8
      EQUIVALENCE (IMACH(4),OUTPUT)

      IF (I .LT. 1  .OR.  I .GT. 16) GO TO 10
!C
      if(isave.eq.0) then
       IMACH( 1) =  5
       IMACH( 2) =  6
       IMACH( 3) =  6
       IMACH( 4) =  6
       IMACH( 5) = 32
       IMACH( 6) =  4
!c
       imach(7) = radix(isave)
       imach(9) = huge(isave)
       imach(8) = nint(log(dble(imach(9)))/log(2.d0))
       imach(9) = 1 + 2*(imach(7)**(imach(8)-1)-1)
       imach(10) = radix(xr8)
!c
       imach(11) = -(nint(log(epsilon(xr4))/log(2.)) - 1)
       imach(12) = minexponent(xr4)
       imach(13) = maxexponent(xr4)
       imach(14) = -(nint(log(epsilon(xr8))/log(2.)) - 1)
       imach(15) = minexponent(xr8)
       imach(16) = maxexponent(xr8)
       isave = 1
      end if
      I1MACH = IMACH(I)
      RETURN
!C
   10 CONTINUE
      WRITE (UNIT = OUTPUT, FMT = 9000)
 9000 FORMAT ('1ERROR    1 IN I1MACH - I OUT OF BOUNDS')
      STOP
!EOC
      END FUNCTION I1MACH

!BOP
!!ROUTINE: R1MACH
!!INTERFACE:
      real FUNCTION R1MACH (I)
!!DESCRIPTION:
! {\bv
!
! R1MACH can be used to obtain machine-dependent parameters for the
! local machine environment.  It is a function subprogram with one
! (input) argument, and can be referenced as follows:
!
!      A = R1MACH(I)
!
! where I=1,...,5.  The (output) value of A above is determined by
! the (input) value of I.  The results for various values of I are
! discussed below.
!
! R1MACH(1) = B**(EMIN-1), the smallest positive magnitude.
! R1MACH(2) = B**EMAX*(1 - B**(-T)), the largest magnitude.
! R1MACH(3) = B**(-T), the smallest relative spacing.
! R1MACH(4) = B**(1-T), the largest relative spacing.
! R1MACH(5) = LOG10(B)
!
! Assume single precision numbers are represented in the T-digit,
! base-B form
!
!            sign (B**E)*( (X(1)/B) + ... + (X(T)/B**T) )
!
! where 0 .LE. X(I) .LT. B for I=1,...,T, 0 .LT. X(1), and
! EMIN .LE. E .LE. EMAX.
!
! The values of B, T, EMIN and EMAX are provided in I1MACH as
! follows:
!
! I1MACH(10) = B, the base.
! I1MACH(11) = T, the number of base-B digits.
! I1MACH(12) = EMIN, the smallest exponent E.
! I1MACH(13) = EMAX, the largest exponent E.
! \ev}
!!ARGUMENTS:
      integer :: I
!!REVISION HISTORY:
! Adapted - A.S. - 2013
!!REMARKS:
! slatec original code is replaced by internal fortran functions
!EOP
!
!BOC
      integer, save :: isave=0
      real(4), save :: RMACH(5)
      real(4), parameter :: x=1.
      IF (I .LT. 1 .OR. I .GT. 5) STOP ' R1MACH ERROR'
!C
      if(isave.eq.0) then
       rmach(1) = tiny(x)
       rmach(2) = huge(x)
       rmach(3) = epsilon(x)/radix(x)
       rmach(4) = epsilon(x)
       rmach(5) = log10(float(radix(x)))
       isave = 1
      end if
      R1MACH = RMACH(I)
      RETURN
!EOC
      END FUNCTION R1MACH

!BOP
!!ROUTINE: vsrtrd
!!INTERFACE:
      subroutine vsrtrd (A,LA,IR)
!!DESCRIPTION:
!  vsrtrd is just a wrapper for DPSORT

!!DO_NOT_PRINT
      implicit none
!!DO_NOT_PRINT

!!ARGUMENTS:
      integer            LA,IR(LA)
      real*8             A(LA)
!!REVISION HISTORY:
! Initial Version - A.S. - 2013
!EOP
!
!BOC
      character    sname*10
      parameter    (sname='vsrtrd')
      integer, allocatable :: iperm(:),itmp(:)
      integer ier,i
      if(la.le.0) return
      allocate(iperm(1:la),itmp(1:la))
      call dpsort(A,LA,IPERM,2,ier)
      if(ier.ne.0) then
       write(*,*) sname//': incorrect input argument in DPSORT, IER='   &
     &            ,ier
       ir(1:la) = 0
      else
       itmp = ir
       do i=1,la
        ir(i) = itmp(iperm(i))
       end do
      end if
      deallocate(iperm,itmp,stat=ier)
      return
!EOC
      end subroutine vsrtrd
!
!BOP
!!ROUTINE: ZSPL3
!!INTERFACE:
        subroutine ZSPL3(n,x,y,z)
!!DESCRIPTION:
!  ZSPL3 calculates coefficients {\tt z} for spline interpolation
!
!!REVISION HISTORY:
! Initial Version - mecca_ames - 2010
!AUTHOR:
!  Based on NUMERICAL MATHEMATICS AND COMPUTING, CHENEY/KINCAID, 1985, pp.277-278
!!REMARKS:
! Natural cubic spline
!EOP
!
!BOC
        implicit none
        integer :: n
        real(8) :: x(n),y(n),z(n)
        real(8) :: h(n),b(n),u(n),v(n)
        integer :: i

        if ( n<2 ) then
          if ( n==1 ) z(1) = 0.d0
          return
        end if
        do 2 i=1,n-1
          h(i)=x(i+1)-x(i)
          b(i)=(y(i+1)-y(i))/h(i)
 2      continue
          u(2)=2.d0*(h(1)+h(2))
          v(2)=6.d0*(b(2)-b(1))
        do 3 i=3,n-1
           u(i)=2.d0*(h(i)+h(i-1))-(h(i-1)*h(i-1))/u(i-1)
           v(i)=6.d0*(b(i)-b(i-1))-(h(i-1)*v(i-1))/u(i-1)
 3      continue
        z(n)=0.d0
        do 4 i=n-1,2,-1
            z(i)=(v(i)-h(i)*z(i+1))/u(i)
 4      continue
        z(1)=0.d0
!*=======================================================================
        return
!EOC
        end subroutine ZSPL3
!*=======================================================================
!BOP
!!ROUTINE: SPL3
!!INTERFACE:
        function SPL3(n,x,y,z,xv)
        implicit none
!!DESCRIPTION:
!  SPL3 is cubic spline interpolation for $y(x)$ at x=xv
!
!!ARGUMENTS:
        double precision :: SPL3
        integer :: n
        real(8) :: x(n),y(n),z(n),xv
!!REVISION HISTORY:
! Initial Version - mecca_ames - 2010
!AUTHOR:
!  Based on NUMERICAL MATHEMATICS AND COMPUTING, CHENEY/KINCAID, 1985, pp.277-278
!EOP
!
!BOC
        real(8) :: diff,h,b,p
        integer :: i
         do 2 i=n-1,2,-1
           diff=xv-x(i)
           if(diff.ge.0.0d0) go to 3
 2       continue
         i=1
         diff=xv-x(1)
 3       h=x(i+1)-x(i)
         b=((y(i+1)-y(i))/h)-(h*(z(i+1)+2.0d0*z(i)))/6.0d0
         p=0.5d0*z(i)+(diff*(z(i+1)-z(i)))/(6.0d0*h)
         p=b+diff*p
         SPL3=y(i)+diff*p
!*=====================================================
         return
!EOC
         end function SPL3
!*=====================================================
!
!BOP
!!ROUTINE: fltrng
!!INTERFACE:
      subroutine fltrng(n,f,nmx)
      implicit none
!!DESCRIPTION:
!  filtering with {\tt nmx} points; nmx = 5 or 7
!
!!ARGUMENTS:
      integer, intent(in) :: n
      real(8), intent(inout) :: f(n)
      integer, intent(in) :: nmx
!EOP
!
!BOC
      real(8), parameter :: zero=0
      integer :: i,nw
      real(8), parameter :: w5(5) = [ -3,12,17,12,-3 ]
      real(8), parameter :: w7(7) = [ -2,3,6,7,6,3,-2 ]
      real(8) :: ff(-2:n+3),w(20)

      w = 0
      if ( nmx==5 ) then
       w(1:nmx) = w5
      else if ( nmx==7 ) then
       w(1:nmx) = w7
      else
       return
      end if

      nw = nmx/2
      w(1:nmx) = w(1:nmx)/sum(w(1:nmx))

      ff(1:n) = f(1:n)
      ff(:0) = f(1)
      ff(n+1:) = f(n)
      do i=1,n
       f(i) = dot_product(w(1:2*nw+1),ff(i-nw:i+nw))
      end do
      return
!EOC
      end subroutine fltrng

      subroutine steff_interpolation(nobs, x, y, kint, xint, yint)
      implicit none

! STEFFEN 1-D Steffen interpolation
!
!    STEFFEN(NOBS,X,Y,kint,XINT,YINT) interpolates to find YINT,
!    Input is NOBS, X, Y, kint, and XINT.
!
!    X and Y are observed data of length NOBS from which
!          a polynomial spline is formulated. X must be increasing
!          in the array. The code will stop with a warning if X(i) >= X(i+1).
!
!    XINT is an array of values with length kint to compute the
!         inteprolated value YINT. XINT does not need to be increasing
!         in the array, but XINT cannot < X(1) or > X(kint). In other words,
!         no extrapolation is allowed, and a missing value (MISS) is generated
!         with a warning message.
!
!    Steffen's interpolation method is based on a third-order polynomial.
!    The slope at each grid point is calculated in a way to guarantee
!    a monotonic behavior of the interpolating function. The curve
!    is smooth up to the first derivative.

!    Steffen's method guarantees the monotonicity of the interpolating
!    function between the given data points. Steffen's formulation is the
!    same as Akima (1970) for Eqs. 1-7. See:
!
!    https://quantmacro.wordpress.com/2015/09/01/akima-spline-interpolation-in-excel/
!
!    and Eq. 8, which computes the slope based on three points of known
!    y value and known slope value --- a technique known as the osculatory
!    interpolation method (see discussion on pg 592 in Akima (1970), his Eq. 10).
!
!    But, Steffen provides different constraints on the polynomial than Akima.
!    Akima's constraint provided mostly smooth curves, but occassional wiggles
!    emerged, especially for adjacent data points of the same value. Steffen's
!    constrain imposes that minima and maxima can only occur exactly at
!    the data points, and there can never be spurious oscillations between
!    data points. The interpolated function is piecewise cubic in each
!    interval. The resulting curve and its first derivative are guaranteed
!    to be continuous, but the second derivative may be discontinuous.

!    This code is based on Joe Henning's original MATLAB program, written
!    in Summer 2014.

!    Yee Lau (lau@gri.msstate.edu) converted his code to FORTRAN
!    in January 2018.
!
!    Pat Fitzpatrick (fitz@gri.msstate.edu) added a driver routine and
!    provided extensive comments in June 2018.
!
! References:
!
! Steffen, M., 1990: A simple method for monotonic interpolation
! in one dimension. Astron. Astrophys. 239, 443-450.
!
! Akima, H., 1970: A new method of interpolation and smooth curve fitting
! based on local procedure. J. Ass. Computing Machinery, 17, 589-602.
!
      integer, intent(in) :: nobs, kint
      real(8) :: x(nobs), y(nobs), yp(nobs)
      real(8) :: xint(kint), yint(kint)
      real(8) :: hi, him1, si, sim1, pi
      real(8) :: h, h1, h2, s, s1, s2, p1, hnm1, hnm2, snm1, snm2
      real(8) :: t, t2, t3, a, b, c, d, pn
      integer :: iargc, i, k, isiny, klo, khi

      real(8), parameter :: miss=-10000

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! input x array must be monotonic increasing
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
!      do i = 1, nobs-1
!         if (x(i) >= x(i+1)) then
!             print *, 'Error! Input x is not monotonic increasing. ' //
!     &                'Program stop.'
!             return
!         endif
!      enddo
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! The next section calculate slopes for array yp for internal points
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

      do i = 2, nobs-1

! Eq. 6 in Steffen (1990)

         hi = x(i+1) - x(i)
         him1 = x(i) - x(i-1)

! Eq. 7 in Steffen (1990)

         si = (y(i+1) - y(i))/hi
         sim1 = (y(i) - y(i-1))/him1

! Eq. 8 in Steffen (1990).
!
! This computes the slope at an internal point
! based on the slope of the secant through points
! x(i-1), y(i-1); x(i), y(i); and x(i+1), y(i+1)
!
! This is known as osculatory interpolation, an old
! but still effective interpolation technique
! as discussed in Akima (1970).
!
! Steffen states it can be derived from Bessel
! intepolation, but the text lacks details.
!

         pi = (sim1*hi + si*him1)/(him1 + hi)

! Eq. 11 in Steffen (1990) applies the constraint
! to the slope, stating "the slope at i must not be
! greater than twice the minimum of the absolute values
! of the slopes given by the left- and right-handed
! finite differences. If these have different signs,
! (the slope) must be zero."
!
! Effectively, minima and maxima can only occur exactly
! at the data points, and there can never be spurious
! oscillations between data points.

         if (sim1*si <= 0) then
            yp(i) = 0
         elseif ((abs(pi) > 2.d0*abs(sim1)) .or.
     &          (abs(pi) > 2.d0*abs(si))) then
            if (sign(sim1,si) /= sim1) then
                print *, 'Error: Sign si not equal to sim1'
                stop
            endif
            if (sim1 > 0) then
                a = 1
            elseif (sim1 == 0) then
                a = 0
            else
                a = -1
            endif
!DEBUG            yp(i) = 2.d0*a*min1(abs(sim1),abs(si))
            yp(i) = 2.d0*a*min(abs(sim1),abs(si))
         else
            yp(i) = pi
         endif
      enddo

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! The next section calculate slopes for array yp at boundaries
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

! Apply boundary condition at the first point

      h1 = x(2) - x(1)
      h2 = x(3) - x(2)
      s1 = (y(2) - y(1))/h1
      s2 = (y(3) - y(2))/h2

! Eq. 24 in Steffen (1990)

      p1 = s1*(1.d0 + h1/(h1 + h2)) - s2*h1/(h1 + h2)

! Eq. 26 in Steffen (1990)

      if (p1*s1 <= 0) then
         yp(1) = 0
      elseif (abs(p1) > 2*abs(s1)) then
         yp(1) = 2.d0*s1
      else
         yp(1) = p1
      endif

! Apply boundary condition at the last point

      hnm1 = x(nobs) - x(nobs-1)
      hnm2 = x(nobs-1) - x(nobs-2)
      snm1 = (y(nobs) - y(nobs-1))/hnm1
      snm2 = (y(nobs-1) - y(nobs-2))/hnm2

! Eq. 25 in Steffen (1990)

      pn = snm1*(1.d0+ hnm1/(hnm1 + hnm2)) - snm2*hnm1/(hnm1 + hnm2)

! Eq. 27 in Steffen (1990)

      if (pn*snm1 <= 0) then
         yp(nobs) = 0
      elseif (abs(pn) > 2.d0*abs(snm1)) then
         yp(nobs) = 2*snm1
      else
         yp(nobs) = pn
      endif

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! Perform interpolation at array xint
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

      do i = 1, kint

! Check the current xint value is within range
!
!         if (xint(i) < x(1) .or. xint(i) > x(nobs)) then
!             print *, 'Warning! ' //
!     &          'Input xint =',xint(i), ' is out of range. ' //
!     &          'Set interpolated value to missing.'
!             yint(i) = miss        ! go to next i in do loop
!             cycle
!         endif
!
! Find the interpolation interval using bisection

         klo = 1
         khi = nobs
         do while (khi-klo > 1)
            k = floor((khi+klo)/2.d0)
            if (x(k) > xint(i)) then
               khi = k
            else
               klo = k
            endif
         enddo

! Eq. 6 in Steffen (1990)

         h = x(khi) - x(klo)

         if (h == 0.0d0) then
             print *, 'Bad x input  ==> x values must be distinct. ' //
     &                'Set interpolated value to missing.'
             yint(i) = miss
             cycle                 ! go to next i in do loop
         endif

! If xint=x, then set yint=y. Also set isiny=1.
! Then go to next i in do loop

         isiny = 0
         do k = 1, nobs
           if (xint(i) == x(k)) then
              yint(i) = y(k)
              isiny = 1
              exit
           endif
         enddo

         if (isiny == 1) then
            cycle
         endif

! Perform interpolation

! Eq. 7 in Steffen (1990)

         s = (y(khi) - y(klo))/h

! Eqs. 2-5 in Steffen (1990)

         a = (yp(klo) + yp(khi) - 2.d0*s)/h/h
         b = (3.d0*s - 2.d0*yp(klo) - yp(khi))/h
         c = yp(klo)
         d = y(klo)

! Eq. 1 in Steffen (1990). This is the interpolation computation.

         t = xint(i) - x(klo)
         t2 = t*t
         t3 = t2*t
         yint(i) = a*t3 + b*t2 + c*t + d

      enddo

      return
      end subroutine steff_interpolation
