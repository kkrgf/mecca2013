!c     cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
!BOP
!!ROUTINE: scf_atom_pot
!!INTERFACE:
      subroutine scf_atom_pot(ndrpts,jmt,jws,h,rr,xr,ztot,vr,v0,totrho, &
     &                  corden,nspin,numc,nc,lc,kc,ec,mtasa,zcor,       &
     &                  zval,iprints,istop)
!!DESCRIPTION:
!  atomic SCF solver (potential and charge density)
!

!!USES:
      use mecca_constants
      use coresolver, only : deepst

!!DO_NOT_PRINT
      implicit none
!!DO_NOT_PRINT

!!ARGUMENTS:
      integer ndrpts,jmt,jws,nspin,numc,mtasa,iprints
      real*8  h,rr(ndrpts),xr(ndrpts)
      real*8  ztot
      real*8  vr(ndrpts),v0
      real*8  totrho(ndrpts)
      real*8  corden(ndrpts)
      integer nc(:),lc(:),kc(:)
      real*8  ec(:)
      real*8  zcor,zval
      character*(*) istop
!!REVISION HISTORY:
! Initial Version - A.S. - 2013
!EOP
!
!BOC
      real*8  ecorv,esemv,qcormt,qcortot,qsemmt,qsemtot,esave
      real*8  vrnew(ndrpts)
      integer nrel
      integer n,nn,l,ll
!c
      character sname*12
      parameter (sname='scf_atom_pot')
      integer nlvl,i,nZ,nl
      real*8  fac,ro3,emax
      real*8  vx(ndrpts),dummy
      real(8), external :: alpha2
      real*8  vhart(ndrpts)
      integer ir,ilvlch
      real*8  semcor(ndrpts)
      real*8  rho1int(ndrpts),rho2int(ndrpts)
      integer, parameter :: ztot_Ac=89

!CALAM
      integer iprint

      ilvlch=0
!c ASSUME nrel=0
      nrel=0

      iprint =iprints
      if (iprint.le.-1) iprint=iprint-2 !cut print statements from corslv

!Cdb  generate atomic electron levels
      nlvl=0
      nZ=0
      do l=0,3
         do n=l+1,l+2
            nn=n
            do ll=l,0, -1
                  if (nZ.lt.ztot) then !we need another level!
                     nlvl=nlvl+1
                     nc( nlvl ) = nn
                     lc( nlvl ) = ll
                     kc( nlvl ) = ll
                     nl= 2*abs(kc (nlvl) )
                     if(nZ+nl.gt.ztot) nl=nint(ztot-nZ)
                     nZ=nZ+nl
                  endif
                  if (nZ.lt.ztot) then !we need another level!
                     if (ll.eq.0) nlvl=nlvl-1
                     nlvl=nlvl+1
                     nc( nlvl ) = nn
                     lc( nlvl ) = ll
                     kc( nlvl ) =-ll-1
                     nl= 2*abs(kc (nlvl) )
                     if(nZ+nl.gt.ztot) nl=nint(ztot-nZ)
                     nZ=nZ+nl
                  endif
                     nn=nn+1
            enddo
         enddo
      enddo

!c potential in Rydberg units
!c
      if(nint(ztot).le.0) then
       zcor = 0.0d0
       zval = ztot
       numc = 0
       return ! Exits from subroutine
      end if

      ec(1:nlvl)= -ztot**2/nc(1:nlvl)**2  ! start scf iterations

      write(6,*)
      write(6,'(a,i4)') 'Finding scf atomic potential for Z='           &
     &                   ,nint(ztot)

      if (iprint.le.-1) then
         write(6,'(5a12)') 'Core energy:','old','new,','%diff/100'
      end if
      esave=0
!C----------------------------------------------------------------------- !----
!c  start iterations on overlapping potential
      do n=1,100

         call zeroout(corden,ndrpts)
         call zeroout(semcor,ndrpts)
         call zeroout(totrho,ndrpts)
!c     ---------------------------------------------------------------
      call corslv_initpot(jmt,jws,h, rr,xr,vr,ztot,nlvl,nc,lc,kc,ec,    &
     &                 corden,semcor,qcormt,qcortot,qsemmt,qsemtot,     &
     &                        ecorv,esemv,                              &
     &                        1    ,nrel   ,mtasa,iprint,istop)
!c    >                        nspin,nrelc,mtasa,iprint,istop)
!c     ---------------------------------------------------------------

      if(n.lt.30) then
       call sortev(ec,lc,kc,nc,nlvl)
      else
       if(ecorv+esemv.gt.esave) then
!c    since corslv_initpot generates the charge density depending on the order
!c    of the eigenvalues,  we need to recalculate the density to take
!c    into account the new order of the eignevalues
        call sortev(ec,lc,kc,nc,nlvl)
        call zeroout(corden,ndrpts)
        call zeroout(semcor,ndrpts)
!c     ---------------------------------------------------------------
      call corslv_initpot(jmt,jws,h, rr,xr,vr,ztot,nlvl,nc,lc,kc,ec,    &
     &                corden,semcor,qcormt,qcortot,qsemmt,qsemtot,      &
     &                        ecorv,esemv,                              &
     &                        1    ,nrel   ,mtasa,-1000,istop)
!c     >                        nspin,nrelc,mtasa,iprint,istop)
!c     ---------------------------------------------------------------
         !    some times the lowest energy level is not full,  e.g. Cr
       endif
      endif

         totrho(1:ndrpts)=corden(1:ndrpts)+semcor(1:ndrpts)
         call qexpup(1,totrho,ndrpts,xr,rho2int)
!?cab041213            fac=rho2int(jws)
!?cab041213            rho2int(jws+1:ndrpts)=fac       ! this fixes an error

         call qexpup(0,totrho,ndrpts,xr,rho1int)
!?cab041213            fac=rho1int(jws)
!?cab041213            rho1int(jws+1:ndrpts)=fac

!c
!c Hartree piece
         vhart(1:jmt) = 2.0d0*( (-ztot+rho2int(1:jmt))/rr(1:jmt) +      &
     &                   (rho1int(jmt)-rho1int(1:jmt)) )
         vhart(jmt+1:ndrpts) = (vhart(jmt)*rr(jmt))/rr(jmt+1:ndrpts)
         do ir=1,ndrpts  !calculate new potential
!c
!c exchange-correlation potential
            if(totrho(ir).le. 0.0d0) totrho(ir)=totrho(ir-1)
            ro3=(3.0d0*(rr(ir)**2)/totrho(ir))**(1.0d0/3.0d0)
!c
            vx(ir)=alpha2(ro3,zero, 1d0,10   ,dummy) !spin is real*8

!c  totrho->0 ro3-> large, some times this causes an error in alpha2
!c  in this case the vx=0
!            if(vx(ir).eq.0)  then
!               vx(ir)=vx(ir-1)
!               write(*,*) 'bug ,vx(ir)=' ,vx(ir)
!            end if

         enddo
!c
!c new potential
         vrnew(1:ndrpts)=( vhart(1:ndrpts) + vx(1:ndrpts) )*rr(1:ndrpts)

         emax=ec(1)
         do i=1,nlvl
            emax=max(emax,ec(i))
         enddo

         fac= 0.5d0                   !large mixing for first few iterations
         if (emax .gt. -1.00d0) fac=.20d0  !slow the mixing down!
         if (emax .gt. -0.50d0) fac=.10d0
         if (emax .gt. -0.05d0) fac=.05d0
         if (emax .gt. -0.02d0) fac=.01d0
         if (n    .gt.  25    ) fac=min(.1d0,fac)

         vr(1:ndrpts)=(1d0-fac)*vr(1:ndrpts)+fac*vrnew(1:ndrpts)

!Cdb  special case: Ac does not have standard electron configuration
         if ( nint(ztot)==ztot_Ac ) then
          if (emax.gt.-.002d0.and.ilvlch.eq.0) then
            ilvlch=1 !level has been changed, only do this once
            write(*,'(a,3i3)')                                          &
     &            'Level      n l k=',nc(nlvl),lc(nlvl),kc(nlvl)
            nc(nlvl)=nc(nlvl)+1
            lc(nlvl)=lc(nlvl)-1
            if (kc(nlvl).gt.0) then
              kc(nlvl)=lc(nlvl)
            else
              kc(nlvl)=-lc(nlvl)-1
            endif
            write(*,'(a,3i3)')                                          &
     &            'Changed to n l k=',nc(nlvl),lc(nlvl),kc(nlvl)
          endif
         endif

           if(mod(n,3).eq.0) write(6,'(9x,i3,2f12.4,3e12.3)')           &
     &                 n,esave,ecorv+esemv,(esave/(ecorv+esemv)-1)


         if (emax.gt.-.00001d0) exit  ! positive eigen values are a problem in core solve
         if(abs(esave/(ecorv+esemv)-1).lt.1d-7 ) exit ! potential has converged
         esave=ecorv+esemv
      enddo

!c establish a zero at WS radius
      v0 = vr(jws)/rr(jws)
      vr(1:jws) = vr(1:jws) - v0*rr(1:jws)
      vr(jws+1:ndrpts) = 0d0

!c Check Zcor if correct
      numc=0
      nZ=0
      do n=1,nlvl
        if(zcor.ge.2)then
         nl=2*abs( kc(n) )
         nZ=nZ+nl
         numc=numc+1
         if (nZ.eq.zcor) exit
         if (nZ.gt.zcor) call fstop(sname//': wrong ZCOR')
        endif
      enddo
      zval=ztot-zcor

      return

!EOC
      contains
!
!BOP
!!IROUTINE: sortev
!!INTERFACE:
      subroutine sortev(ec,lc,kc,nc,nlvl)
!!DESCRIPTION:
! sorts eigenvaues
!
!!REMARKS:
! private procedure of subroutine scf_atom_pot
!EOP
!
!BOC
      implicit none
      integer nlvl
      real*8 ec(nlvl),tmp
      integer tmp2
      integer lc(nlvl),kc(nlvl),nc(nlvl),jmin,j,jj
!c----------------------------------------------------------------------- !-----

      do j=1,nlvl
         jmin=j
         do jj=j,nlvl
            if (ec(jj).lt.ec(jmin)) jmin = jj
         enddo
         if (j.ne.jmin) then
            tmp=ec(jmin)
            ec(jmin)=ec(j)
            ec(j)=tmp

            tmp2=lc(jmin)
            lc(jmin)=lc(j)
            lc(j)=tmp2

            tmp2=kc(jmin)
            kc(jmin)=kc(j)
            kc(j)=tmp2

            tmp2=nc(jmin)
            nc(jmin)=nc(j)
            nc(j)=tmp2
         endif
      enddo

!EOC
      end subroutine sortev

!
!BOP
!!IROUTINE: corslv_initpot
!!INTERFACE:
      subroutine corslv_initpot(jmt,jws,h,r,x,                          &
     &                  rv,                                             &
     &                  z,numc,nc,lc,kc,ecore,                          &
     &                  corden,semden,                                  &
     &                  qcormt,qcortot,qsemmt,qsemtot,                  &
     &                  ecorv,esemv,                                    &
     &                  nspin,nrelc,mtasa,iprint,istop)
!!DESCRIPTION:
! solves radial Schroediner equation for atom
!
!!REMARKS:
! private procedure of subroutine scf_atom_pot
!EOP
!
!BOC
      implicit none
!c
      integer, intent(in) :: jmt,jws
      real(8), intent(in) :: h
      real(8), intent(in) :: r(:)  ! (iprpts)
      real(8), intent(in) :: x(:)  ! (iprpts)
      real(8), intent(in) :: rv(:)  ! (iprpts)
      real(8), intent(in) :: z
      integer :: numc
      integer :: nc(:)  ! (ipeval)
      integer :: lc(:)  ! (ipeval)
      integer :: kc(:)  ! (ipeval)
      real(8) :: ecore(:)  ! (ipeval)
      real(8) :: corden(:)  ! (iprpts)
      real(8) :: semden(:)  ! (iprpts)
      real(8) :: qcormt
      real(8) :: qcortot
      real(8) :: qsemmt
      real(8) :: qsemtot
      real(8) :: ecorv,esemv
      integer, intent(in) :: nspin,nrelc,mtasa,iprint
      character*(*) istop
!c
!c
!c      real*8 ecorein(ipeval)
      real(8) :: g(size(r))  ! (iprpts)
      real(8) :: f(size(r))  ! (iprpts)
      real(8) :: wrk(size(r))
      real(8) :: qmt
      real(8) :: qtot
!Cdb
!CDEBUG
!      integer, save :: iverybad=0
!CDEBUG

!c parameters
      real*8     tol,tolchg
!c
!c     +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
      parameter (tol=one/ten**10)
      parameter (tolchg=one/ten**10)
      character(10), parameter :: sname='corslv_int'
!c     +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!      character lj(7)*4
!      data lj/'s1/2','p1/2','p3/2','d3/2','d5/2','f5/2','f7/2'/
!c
!c     //////////////////////////////////////////////////////////////////
!c     This routine drives the radial eq. solution for the core states.
!c     It must be called for each component (sublattice) separately.
!c
!c           ......added INWHNK for inward integration of dirac eq.
!c                 and modified for ASA case....... ddj & fjp  july 1991
!c           ......modified for SPIN-POLARIZED REL. case...ddj sept 1995
!c     //////////////////////////////////////////////////////////////////
!c
!c     *****************************************************************
!c     calls:  deepst(outws,inws)
!c             semcor(outws,hank)
!c
!c     *****************************************************************
!c     r= radial grid,
!c     rv=potential times r
!c     ecore= core levels guesses: to be updated
!c     g,f upper and lower components times r
!c     corden= core charge density
!c     nc= principal q. nos.,
!c     lc= orbital q. nos.,
!c     kc= kappa q.nos.
!c     numc= no. of core states,
!c     z= atomic no.,
!c     jmt=muffin-tin index
!c     jws=top r index
!c     **************************************************************
!c
!c     ks is the plus and minus relativistic solutions.
!c     -------------------------------------------------------------
!c           n =   1      2            3                  4
!c           l =   0   0  1  1   0  1  1  2  2   0  1  1  2  2  3  3
!c     core kc =  -1  -1  1 -2  -1  1 -2  2 -3  -1  1 -2  2 -3  3 -4
!c     core ks =  -1  -1  1 -1  -1  1 -1  1 -1  -1  1 -1  1 -1  1 -1
!c                        \ /       \ /   \ /       \ /   \ /   \ /
!c                         |         |     |         |     |     |
!c     averaged:   s   s   p     s   p     d     s   p     d     f
!c     -------------------------------------------------------------
!c     NOTE: valence KC is may be different by sign for l.ne.0
!c     val. kc =  -1  -1 -1  2  -1 -1  2 -2  3  -1 -1  2 -2  3 -3  4
!c     -------------------------------------------------------------
!c
!c----------------------------------------------------------------------- !-
!c NOTE: For ASA, normalization and energy update of eigenvalue guess are
!c       sensitive to loss of points which describe decaying tail of
!c       wave-fct., so in DEEP and, especially, SEMCST the normalization
!c       and energy update require different calls to integrate wave-fcts !.
!c       r-grids must go to IPRPTS and ASA integrations must go to LAST2.
!c----------------------------------------------------------------------- !-
!c
!c fix the last point.....(cannot be larger than iprpts)........
      integer :: last,last2,ifail,nitmax,nelt,i,j,indx,iter,nel
      real(8) :: c,fnstrc,fac1
!
      last=size(r)
!c
       if(mtasa.le.0) then
!c normalization can be in interstitial, outside MT spheres
         last2=size(r)
       else
!c normalization must be inside ASA spheres
         last2=jws
       endif
!c
      if(iprint.ge.2) write(6,'('' numc '',i5)') numc
      if (numc.le.0) return
!c
      ifail=0
!CALAM      emax=-ten**6
      nitmax=100
      nelt=0
      c = two*inv_fine_struct_const
      c=c*10.0d0**nrelc
!c     ================================================================
!c     call deepst for each core state.................................
      esemv=zero
      ecorv=zero
      do i=1,numc
        fac1=(3-nspin)*iabs(kc(i))
!Cdb  ... not all levels are full
        if(fac1+nelt.gt.nint(z)) fac1= max (0,nint(z)-nelt)
!Cdb
        nelt=nelt+nint(fac1)
!Cdb      ecorein(i)=ecore(i)
        if(iprint.ge.2) then
         write(6,'('' i,nc,lc,kc,ecore  '',4i5,d12.5)')                 &
     &            i,nc(i),lc(i),kc(i),ecore(i)
         call flush(6)
!cab         call flush_(6)
        end if
!Cdb
!c       ----------------------------------------------------------
        call deepst(nc(i),lc(i),kc(i),ecore(i),                         &
     &            rv,r,x,g,f,h,z,c,nitmax,tol,                          &
     &            jws,jws,last,iter,ifail)
!     &            jws,last,iter,ifail)
!c       ----------------------------------------------------------
!c       ==========================================================
!DEBUG!c       when core state energy >-10 then treat as semi-core.......
!DEBUG!Cdb  starting potential has probelms in semcst, so do not use
!DEBUG        if( ecore(i) .ge. semicore_level ) then
!DEBUG!c          ------------------------------------------------------
!DEBUG           call semcst(nc(i),lc(i),kc(i),ecore(i),                      &
!DEBUG     &                 rv,r,x,g,f,h,z,c,nitmax,tol,                     &
!DEBUG     &                 jmt,jmt,last2,iter,ifail)
!DEBUG!     &                 jmt,last2,iter,ifail)
!DEBUG!c          ------------------------------------------------------
!DEBUG!CDDJ check if this should be:
!DEBUG!CDDJ       esemv=esemv + ecore(i)*fac1 + (nspn-1)*vdif*fac1
!DEBUG           esemv=esemv + ecore(i)*fac1
!DEBUG!c          degeneracy of the level
!DEBUG!c          accumulate the core chg density
!DEBUG           do j=1,last
!DEBUG              semden(j)=semden(j) + fac1*( g(j)*g(j)+f(j)*f(j) )
!DEBUG           enddo
!DEBUG        else
!c       ----------------------------------------------------------
!c        call deepst(nc(i),lc(i),kc(i),ecore(i),
!c     >            rv,r,x,g,f,h,z,c,nitmax,tol,jmt,jws,
!c     >            last,iter,ifail)
!c       ----------------------------------------------------------
           do j=1,last
              corden(j)= corden(j) + fac1*( g(j)*g(j)+f(j)*f(j) )
           enddo
           ecorv=ecorv+ecore(i)*fac1
!DEBUG        endif
      enddo

      if(iprint.ge.-1) then
!c
!c     Major printout: core eigenvalues...........................
!c
       write(6,'(''     ///////////////////////////////////'',          &
     &          ''//////////////////////////////////'')')
       write(6,'(''      Eigenvalues:'',t20,''n'',t25,''l'',t30,        &
     &  ''k'',t42,''energy'',6x,''core'',4x,''occ.'',1x,''iter'')')
       do i=1,numc
        nel= (3-nspin)*iabs(kc(i))
        indx= lc(i) + iabs(kc(i))
!c       write(6,'(t18,i3,t23,i3,t28,i3,t35,d20.13,3x,i1,a4,i5)')
        write(6,'(t18,i3,t23,i3,t28,i3,t35,f16.8,3x,i1,a4,2i5)')        &
     &    nc(i),lc(i),kc(i),ecore(i),nc(i),lj(indx),nel,iter
       enddo
       write(6,'(''     ///////////////////////////////////'',          &
     &          ''//////////////////////////////////'')')
!c
      end if
!c
!c integrate averaged single-site density
!c
!c deep core: MT and WS
      call qexpup(1,corden,last2,x,wrk)
      qcormt=wrk(jmt)
      qmt=qcormt
      qcortot=wrk(last2)
      qtot=qcortot
      if(iprint.ge.-2) then
       write(6,'(''     -----------------------------------'',          &
     &          ''----------------------------------'')')
       write(6,'(''      Charge     core: mt/asa,tot'',t35,''='',       &
     &                           2f16.8)') wrk(jmt),wrk(last2)
      end if
!c
!c semi-core core: MT and WS
      call qexpup(1,semden,last2,x,wrk)
      qsemmt=wrk(jmt)
      qmt=qmt+qsemmt
      qsemtot=wrk(last2)
      qtot=qtot+qsemtot
      if(iprint.ge.-2) then
       write(6,'(''      Charge semicore: mt/asa,tot'',t35,''='',       &
     &                           2f16.8)') wrk(jmt),wrk(last2)
!c
!c total core charges: MT and WS
       write(6,'(''     -----------------------------------'',          &
     &          ''----------------------------------'')')
       write(6,'(''      Charge    total: mt/asa,tot'',t35,''='',       &
     &                           2f16.8)') qmt,qtot
       write(6,'(''     -----------------------------------'',          &
     &          ''----------------------------------'')')
      end if
      if(istop.eq.sname) then
      call fstop(sname)
      endif
      return
!EOC
      end subroutine corslv_initpot
!c----------------------------------------------------------------

      end subroutine scf_atom_pot


