!BOP
!!MODULE: density
!!INTERFACE:
      module density
!!DESCRIPTION:
! charge density calculation and related issues

!!USES:
      use mecca_constants
      use mecca_types
      use mecca_interface
      use gfncts_interface

!!DO_NOT_PRINT
      implicit none
      private
!!DO_NOT_PRINT

!!PUBLIC MEMBER FUNCTIONS:
      public :: calcDensity
      public :: calcFreeElctrnCorr,dealloc_fe
!!PRIVATE MEMBER FUNCTIONS:
!  subroutine correctNegRho
!  subroutine getWSfreeElctrn
!  subroutine radialJJint

!!PUBLIC DATA MEMBERS:
      public :: qCell_fe,qws_fe
!!REVISION HISTORY:
! Initial Version - A.S. - 2013
!EOP
!
!BOC
      real(8), parameter :: piinv=one/pi,pi4=pi*four
      real(8), parameter :: eps=ten*epsilon(one)

      real(8), save, pointer :: chgfe(:,:,:) => null()  ! free-electrons density, radial dependence 
      real(8), save, pointer :: qws_fe(:,:)  => null()  ! integral of chgfe(r) over VP or ASA
      real(8), save :: qCell_fe=zero                    ! number of free-electrons in the cell,  
                                                        ! FE_charge_denstity = qCell_fe/cell_volume
!EOC
      contains

!BOP
!!IROUTINE: calcDensity
!!INTERFACE:
      subroutine calcDensity( mecca_state )  
!!DESCRIPTION:
!  generates site charge densities and calculates charges
!  (pointer i{\tt gf\_out} is used as main input source, 
!   and pointers {\tt sublat, work\_box} are for output) 
!
!!ARGUMENTS:
      type(RunState), intent(inout), target :: mecca_state
!!REVISION HISTORY:
! Initial Version - A.S. - 2013
!EOP
!
!BOC

      type ( IniFile ),   pointer :: inFile
      type (GF_output),   pointer :: gf_out
      type (Work_data),   pointer :: work_box    ! output (potential,energy,errors)
      type(Sublattice),  pointer :: sublat      
      type(SphAtomDeps), pointer :: v_rho_box

      real(8), allocatable :: xr(:),rr(:),rhoval(:,:),rhotmp(:)
      integer nsub,ic,is,ndrpts,ndchg,nspin,nsublat,iprint
      real(8) :: h,qval(2),qval_mt,rhoneg,rhneg,xval_in
!      real(8) :: delef
      real(8) :: totchg,corchg,rws,sp_f,qval_asa,qval_ws
      real(8) :: qval_lst,rho0fe,rho0semi,q0semi,q0c,q0v
      real(8) :: cell_calc,cell_charge,semichg
      integer :: ndcomp,st_env,zcmx
!      real(8), parameter :: qtol=ten**(-6)
      logical, parameter :: non_zero_T=.true.

      inFile => mecca_state%intfile
      gf_out => mecca_state%gf_out_box
      work_box => mecca_state%work_box
      call g_ndims(inFile,ndrpts,ndcomp,nsublat,nspin)
      iprint = inFile%iprint
      if ( nspin==1 ) then
       sp_f = two     ! spin-factor is needed for E-dependent functions
      else
       sp_f = one
      end if

      if ( .not.allocated(work_box%dq_err) ) then
        allocate(work_box%dq_err(1:ndcomp,1:nsublat,1:nspin))
      end if
      work_box%dq_err = 0

      zcmx = zero
      do nsub=1,inFile%nsubl
       sublat => inFile%sublat(nsub)
       do ic=1,sublat%ncomp
        zcmx = max(zcmx,sublat%compon(ic)%zcor)
       end do
      end do
      if ( allocated(xr) ) then
       if (  size(xr,1).le.ndrpts ) deallocate(xr,rr,rhoval,rhotmp)
      end if
      if ( .not.allocated(xr) ) then     
        allocate(xr(1:ndrpts))
        allocate(rr(1:ndrpts))
        allocate(rhoval(1:ndrpts,nspin))
        allocate(rhotmp(1:ndrpts))
      end if  
      rho0semi=pi4*g_zsemcout(inFile,.false.)/mecca_state%rs_box%volume
      semichg = zero
      xval_in = zero
      qval_lst  = zero
      qval_asa  = zero
      qval_ws  = zero
      cell_charge = zero
      cell_calc = zero
      do nsub=1,inFile%nsubl
       sublat => inFile%sublat(nsub)
       do ic=1,sublat%ncomp
        v_rho_box => sublat%compon(ic)%v_rho_box
        call g_meshv(v_rho_box,h,xr(:),rr(:))
        ndchg = v_rho_box%ndchg
        rhoval = zero   
        do is=1,nspin
          v_rho_box%chgtot(:,is) = zero
          xval_in = xval_in + sublat%ns*sublat%compon(ic)%concentr*     &
     &                        gf_out%xvalws(ic,nsub,is)
!          
          rhoval(1:ndchg,is)=rhoval(1:ndchg,is)-(piinv*rr(1:ndchg)**2)* &
     &                    aimag(gf_out%greenint(1:ndchg,ic,nsub,is))
          if ( gf_out%ms_gf==0 ) then 
           call g_rho(v_rho_box,is,rhotmp)
           if (rhotmp(1) .ne. zero) rhoval(:,is) = rhoval(:,is)+rhotmp
           if ( qCell_fe>zero ) then   !DEBUG
            if ( associated(chgfe) ) then
             rho0fe = pi4*qCell_fe/mecca_state%rs_box%volume
             rhotmp(1:ndchg) =                                          &
     &              (rho0fe*rr(1:ndchg)**2-chgfe(1:ndchg,ic,nsub))/nspin
             rhoval(1:ndchg,is) = rhoval(1:ndchg,is) + rhotmp(1:ndchg) 
             if ( sublat%compon(ic)%zID > 0 ) then
              call correctNegRho(v_rho_box%jmt,rr(1:ndchg),             &
     &                  rhoval(1:ndchg,is),rhoneg)
              if(rhoneg < zero) then
               write(6,'(/a,d12.5,a,3i5)')                              &
     & ' WARNING: Accuracy is lost: negative valence charge density ',  &
     &            rhoneg,' is observed for is,ic,nsub=',is,ic,nsub
              end if
             end if
            end if
           end if
          end if
        end do
!
        if ( sublat%compon(ic)%zID==0 ) then
         if ( gf_out%ms_gf==0 ) then 
          rhoneg = zero
          do is=1,nspin
           call correctNegRho(v_rho_box%jmt,rr(1:ndchg),                &
     &                                   rhoval(1:ndchg,is),rhneg)
           if ( rhneg<zero ) then
            rhoneg = min(rhoneg,rhneg)
            write(6,'(/a,d12.5,a,3i5)')                                 &
     &      ' WARNING: Accuracy is lost: negative ES charge density ',  &
     &               rhneg,' is observed for is,ic,nsub=',is,ic,nsub
           end if
          end do 
!          if ( rhoneg<zero ) then
!           call correctESrho(v_rho_box%jmt,rr(1:ndchg),                 &
!     &                                      rhoval(1:ndchg,1:nspin))
!          end if
         end if
        end if

        do is=1,nspin

          call asa_integral(mecca_state,ic,nsub,rhoval(1:ndchg,is),     &
     &                                              qval(is),qval_mt)
          work_box%dq_err(ic,nsub,is)=gf_out%xvalws(ic,nsub,is)-qval(is)

          if ( gf_out%ms_gf==0 .and. rho0semi>zero ) then
           call g_totchg(v_rho_box,.true.,is,totchg,corchg)
           rws = mecca_state%rs_box%rws(nsub)
           q0semi = rho0semi/nspin/three*rws**3
           q0c = dble(sublat%compon(ic)%zcor)/nspin-corchg
           q0v = q0semi-q0c
           rhotmp(1:ndchg)=rho0semi/nspin*rr(1:ndchg)**2
           rhoval(1:ndchg,is) = rhoval(1:ndchg,is)                      & 
     &                               + (q0v/q0semi)*rhotmp(1:ndchg)
           v_rho_box%chgcor(1:ndchg,is) = v_rho_box%chgcor(1:ndchg,is)  &
     &                               + (q0c/q0semi)*rhotmp(1:ndchg)
           v_rho_box%zcor_T(is) = v_rho_box%zcor_T(is) + q0c 
           qval(is) = qval(is) + q0v
          else
           q0semi = zero
          end if
          qval_asa = qval_asa + sublat%ns*sublat%compon(ic)%concentr*   &
     &                       qval(is)

          totchg = qval(is)

          call s_totchg(v_rho_box,is,totchg)       ! ASA-charge  
          call s_rho(v_rho_box,is,rhoval(1:ndchg,is))

          if ( gf_out%ms_gf==0 ) then 
           call g_rhocor(v_rho_box,is,rhoval(:,is))
!DEBUGPRINT
!           call asa_integral(mecca_state,ic,nsub,rhoval(:,is),                &
!     &                                            totchg,qval_mt)
!           write(6,*) ' DEBUG cor totchg=',totchg,qval_mt
!DEBUGPRINT
           call g_rhosemicore(ic,nsub,is,rhotmp,mecca_state)
!DEBUGPRINT
!            call asa_integral(mecca_state,ic,nsub,rhotmp,               &
!     &                                            totchg,qval_mt)
!            write(6,*) ' DEBUG semi totchg=',totchg,qval_mt
!DEBUGPRINT
           if ( abs(rhotmp(1)) .gt. tiny(zero) ) then
            rhoval(:,is) = rhoval(:,is)+rhotmp
            call asa_integral(mecca_state,ic,nsub,rhoval(:,is),         &
     &                                            totchg,qval_mt)
            semichg = semichg + (totchg-corchg) *                       &
     &                             sublat%ns*sublat%compon(ic)%concentr 
!DEBUGPRINT
!            write(6,*) ' DEBUG core+semi totchg=',totchg,semichg
!DEBUGPRINT
           end if
           call s_rho(v_rho_box,is,rhoval(:,is),add=1)
           call g_rho(v_rho_box,is,rhotmp)
           call asa_integral(mecca_state,ic,nsub,rhotmp,                &
     &                                            totchg,qval_mt)
           call s_totchg(v_rho_box,is,totchg)       ! ASA-charge  
!DEBUGPRINT           write(6,*) ' DEBUG TOTCHG=',totchg,corchg
           cell_calc = cell_calc + totchg *                             &
     &                             sublat%ns*sublat%compon(ic)%concentr 
          end if
        end do
        cell_charge = cell_charge + sublat%compon(ic)%zID *             &
     &                             sublat%ns*sublat%compon(ic)%concentr
        nullify(v_rho_box)
       end do
       nullify(sublat)
      end do

      if ( gf_out%ms_gf==0 ) then
!DELETE       if ( inFile%Tempr==zero ) then
!        compensation for numerical inaccuracies ( T = 0 )
        rho0fe = -pi4*semichg/mecca_state%rs_box%volume
        cell_calc = cell_calc - semichg
        if ( iprint>=-1 .and.                                           &
     &       abs(cell_calc-cell_charge)>abs(cell_charge)*qtol) then
         write(6,'('' WARNING:'',1x,a,2g19.10)')                        &
     &  'inaccurate cell charge, CALC vs INPUT ',cell_calc,cell_charge
        end if
        if ( abs(cell_calc-cell_charge)<one .or. rho0fe>zero ) then
         rho0fe = pi4*(cell_charge-cell_calc)/mecca_state%rs_box%volume &
     &        + rho0fe
         if ( abs(rho0fe)>1.d-8 ) then
          if ( iprint>=-1 ) then
           if ( abs(cell_calc-cell_charge)<one ) write(6,'(a,g14.5)')   &
     &     ' homogeneous charge density correction is applied, rho0fe=',&
     &         rho0fe/pi4
          end if
         end if
!
!TODO:        compensation for numerical inaccuracies ( T.ne.0 - T=0 )
!
         do nsub=1,inFile%nsubl
          sublat => inFile%sublat(nsub)
          do ic=1,sublat%ncomp
           v_rho_box => sublat%compon(ic)%v_rho_box
           ndchg = v_rho_box%ndchg
           call g_meshv(v_rho_box,h,xr(:),rr(:))
           do is=1,nspin
            call g_rho(v_rho_box,is,rhotmp)   ! actually rhotmp is rhotot = rhocore + rhoval
!           call asa_integral(mecca_state,ic,nsub,rhotmp(1:ndchg),         &
!     &                                             totchg,qval_mt)
!      write(6,*) ' DEBUG integral2 totchg=',totchg
!      write(6,*) ' DEBUG g_totchg totchg=',totchg,                       &
!     &                                     rho0fe/nspin*three*rws**3
            rhotmp(1:ndchg) = rhotmp(1:ndchg) +                         &
     &                             (rho0fe/nspin)*rr(1:ndchg)**2
            call asa_integral(mecca_state,ic,nsub,rhotmp(1:ndchg),      &
     &                                             totchg,qval_mt)
!            rws = mecca_state%rs_box%rws(nsub)
!            call g_totchg(v_rho_box,.true.,is,totchg,corchg)
!            totchg = totchg + rho0fe/nspin*rws**3/three
            call s_totchg(v_rho_box,is,totchg)
            call s_rho(v_rho_box,is,rhotmp(1:ndchg))
           end do
          end do
         end do
!DELETE        end if
       end if

      end if   ! gf_out%ms_gf==0
!
!
!      if ( inFile%mtasa==0 ) then
!       continue
!      else 
!
!       if ( rho_fe .eq. zero ) then
!        qval_t = g_zvaltot( inFile, .true. )  
!        renorm = qval_t/xval_in       
!        do nsub=1,inFile%nsubl
!         do ic=1,inFile%sublat(nsub)%ncomp
!          v_rho_box => inFile%sublat(nsub)%compon(ic)%v_rho_box
!          ndcor = v_rho_box%ndcor
!          do is=1,nspin
!           v_rho_box%chgtot(1:ndcor,is) = v_rho_box%chgcor(1:ndcor,is) +&
!     &                                                         renorm * &
!     &      (v_rho_box%chgtot(1:ndcor,is)-v_rho_box%chgcor(1:ndcor,is))
!           call g_totchg(v_rho_box,.true.,is,totchg,corchg)
!           gf_out%xvalws(ic,nsub,is) = (totchg-corchg)*renorm
!           call s_totchg(v_rho_box,is,v_rho_box%zcor_T(is) +            &
!     &                                       gf_out%xvalws(ic,nsub,is))
!          end do
!          nullify(v_rho_box)
!         end do
!        end do
!       end if 

!       if ( inFile%mtasa>1  .and. inFile%intgrsch>1                     &
!     &     .and. associated(mecca_state%jcbn_box) ) then
!        if (mecca_state%jcbn_box%nF>0 ) then
!         if ( allocated(tmprho) ) deallocate(tmprho) 
!         allocate(tmprho(1:ndrpts))
!!         do nsub=1,inFile%nsubl
!!          sublat => inFile%sublat(nsub)
!!          do ic=1,sublat%ncomp
!!           v_rho_box => sublat%compon(ic)%v_rho_box
!!           do is=1,nspin
!!            call g_rho(v_rho_box,is,rhoval)
!!            call g_rhocor(v_rho_box,is,rhotmp)
!!            rhoval = rhoval-rhotmp
!!            call ws_integral(mecca_state,ic,nsub,rhoval,                &
!!     &                                              qval(is),qval_mt)
!!!!          qval_ws = qval_ws + sublat%ns*sublat%compon(ic)%concentr*     &
!!     &                       qval(is)
!!            call g_totchg(v_rho_box,.true.,is,totchg,corchg)
!!!         write(6,*) 'DEBUG nsub:',nsub,' q_vp=',sngl(qval(is)-qval_mt), &
!!!     &                  ' q_ws=',sngl(totchg-corchg-qval_mt)
!!!            write(6,*) 'DEBUG vp-asa ',sngl(qval(is)-(totchg-corchg))
!!!!DEBUGPRINT            
!!!            totchg = corchg+qval(is)
!!!            call s_totchg(v_rho_box,is,totchg)       ! VP-charge, i.e. it is not equal to Int(n(e))  
!!           end do 
!!          end do 
!!        end do 
!         deallocate(tmprho)
!        end if
!       end if
!
!      end if 
!
      deallocate(xr,rr,rhoval,rhotmp)
      return

!EOC
      end subroutine calcDensity  

!BOP
!!IROUTINE: correctNegRho
!!INTERFACE:
      subroutine correctNegRho(jsph,xr,rho,rhoneg)
!!DESCRIPTION:
! check if {\tt rho(xr)} is always positive 
!
! Initial Version - A.S. - 2013
!!REMARKS:
! correction for negative rho is not activated
!EOP
!
!BOC
      integer, intent(in) :: jsph
      real(8), intent(in) :: xr(:)
      real(8), intent(inout) :: rho(:)
      real(8), intent(out) :: rhoneg
      real(8), parameter :: eps_neg = 0

      real(8) :: d1
      real(8) :: q, wi(size(xr))
      integer i,n
      integer :: st_env
      n = size(xr)
      rhoneg = minval(rho(1:jsph))
      
      call gEnvVar('ENV_ENABLE_NEG_RHO_CORR',.false.,st_env)
      if ( st_env <= 0 ) return

      if ( rhoneg < eps_neg ) then
        call qexpup(1,rho,jsph,log(xr),wi)
        q = wi(jsph)
        do i=jsph-1,1,-1
         if ( q-wi(i)<eps_neg ) then
          d1 = (q-wi(i+1))/xr(i+1)**2
          rho(1:i) = d1*xr(1:i)**2
          return
         end if
        end do
!
      end if

      return
!EOC
      end subroutine correctNegRho

!BOP
!!IROUTINE: calcFreeElctrnCorr
!!INTERFACE:
      subroutine calcFreeElctrnCorr ( mecca_state, ef )
!!DESCRIPTION:
! calculates free electron correction
!
!!REVISION HISTORY:
! Initial Version - A.S. - Mar 2014
!!REMARKS:
!EOP
!
!BOC
      implicit none
      type(RunState) :: mecca_state
      real(8), intent(in)  :: ef
      integer :: ndrpts,ndcomp,ndsubl,nspin,ic,nsub
      real(8) :: q_mt
      type ( IniFile ),   pointer :: inFile => null()

      inFile => mecca_state%intfile
      call g_ndims(inFile,ndrpts,ndcomp,ndsubl,nspin)
      if ( associated(chgfe) ) then
       if ( size(chgfe,1)<ndrpts                                        &
     &  .or.size(chgfe,2)<ndcomp                                        &
     &  .or.size(chgfe,3)<ndsubl ) then
        call dealloc_fe
       end if
      end if
      if ( associated(qws_fe) ) then
       if ( size(chgfe,1)<ndcomp                                        &
     &  .or.size(chgfe,2)<ndsubl ) then
        call dealloc_fe
       end if
      end if
      if ( .not.associated(chgfe) ) then
        allocate(chgfe(ndrpts,ndcomp,ndsubl))  
      end if
      if ( .not.associated(qws_fe) ) then
        allocate(qws_fe(ndcomp,ndsubl))  
      end if
      qws_fe = zero

      call getWSfreeElctrn(mecca_state,ef,chgfe,qCell_fe)

      if ( qCell_fe > zero ) then
       do nsub=1,ndsubl
        do ic=1,inFile%sublat(nsub)%ncomp
         call ws_integral(mecca_state,ic,nsub,                          &
     & chgfe(1:inFile%sublat(nsub)%compon(ic)%v_rho_box%ndrpts,ic,nsub),&
     &                   qws_fe(ic,nsub),q_mt)
        end do
       end do
      end if

!EOC
      end subroutine calcFreeElctrnCorr

!BOP
!!IROUTINE: dealloc_fe
!!INTERFACE:
      subroutine dealloc_fe
!EOP
!
!BOC
      if ( associated(chgfe) ) then
         deallocate(chgfe)
         nullify(chgfe)
      end if 
      if ( associated(qws_fe) ) then
         deallocate(qws_fe)
         nullify(qws_fe)
         qCell_fe = zero
      end if 
!EOC
      end subroutine dealloc_fe 
!      
!c============================================================================
!BOP
!!IROUTINE: getWSfreeElctrn
!!INTERFACE:
      subroutine getWSfreeElctrn ( mecca_state, ef, rho_fe, qcell_fe )
!!DESCRIPTION:
! Free-electron charge density correction (Tempr = 0!)
!
! {\bv
!  rho_fe -- free-electron charge density within ASA/VP
!    rho_fe(r)= -1/pi*Integral{0:eFermi|dE sqrt(E) Sum(l=0:lmax| (2*l+1)*j_l(sqrt(E)*r)^2)}
!      
!  qcell_fe -- number of free electrons in the cell = 1/(6*pi^2)*Efermi^(3/2)      
! \ev}

!!USES:
      use gfncts_interface, only : addFEforEf

!!DO_NOT_PRINT      
      implicit none
!!DO_NOT_PRINT      

!!ARGUMENTS:
      type(RunState) :: mecca_state
      real(8), intent(in)  :: ef
      real(8), intent(out) :: rho_fe(:,:,:)        ! free-electron charge density inside ASA/VP
      real(8), intent(out) :: qcell_fe
!!REVISION HISTORY:
! Initial Version - A.S. - Mar 2014
!EOP
!
!BOC
      type(SphAtomDeps), pointer :: v_rho_box
      integer :: lmx,nbasis,ndrpts,nsub,ic,k,kx,ndint
      real(8) :: h
      real(8), allocatable :: xr(:)
      real(8), allocatable :: rr(:),rint(:)
      real(8), allocatable :: feJJint(:)
      real(8) :: clight,pf,xmax,rho_ws,c_rho
      real(8), external :: ylag

      integer, parameter :: n_lgn0=8, n_lag0=8
      integer :: npt
      real(8), parameter :: relerr = 1.d-5, abserr = 1.d-6
      real(8) :: xgs1(100),wgs1(100)
      real(8) :: xgs2(100),wgs2(100)
      real(8) :: rho1,rho2,rho0fe,e0,w,z,em
      real(8), external :: fermiFun

      qcell_fe = zero
      rho_fe = zero
      if ( .not. addFEforEf() ) return
      if ( mecca_state%intfile%Tempr == zero .and. ef <= zero ) return
      clight = two*inv_fine_struct_const*ten**mecca_state%intfile%nrel
      if ( mecca_state%intfile%Tempr>zero ) then    !TODO:  non-zero temperature case
          RETURN     ! it is not clear how to calculate efficiently radially-dependent Sum(l=0:lmax), i.e. rho_fe
        rho_ws = sqrt(ef**3)/(6*pi**2) !???? what if ef<0?
       rho_ws = rho_ws*2    ! spin-factor
       em = ef/mecca_state%intfile%Tempr 
       rho0fe = zero
       if ( ef > zero ) then
        rho1 = zero   
        rho2 = zero   
        e0 = one/two
        w =  one/two
        do npt = n_lgn0,size(xgs1),max(1,n_lgn0/2)
         call gauleg(-one,one,xgs1,wgs1,npt)
         xgs1(1:npt) = e0 + w*xgs1(1:npt)
         wgs1(1:npt) =      w*wgs1(1:npt)
         rho1 = zero
         do k=1,npt
          z = em*xgs1(k)
          rho1 = rho1 + wgs1(k) * fermiFun(z) *                         &
     &                        (sqrt(one+xgs1(k))-sqrt(one-xgs1(k)))
         end do     
         if ( abs(rho1-rho2)<abs(rho1)*relerr ) exit
         if ( abs(rho1)<abserr ) exit
         rho2 = rho1
        end do     
        rho0fe = rho0fe + rho1
       end if
       em = abs(ef)/mecca_state%intfile%Tempr 
       do npt = n_lag0,size(xgs2),max(1,n_lag0/2)
        call lagzo(npt,xgs2,wgs2)
        rho1 = zero
        do k=1,npt
          z = em*(one+xgs2(k))
          rho1 = rho1 + wgs2(k) * fermiFun(z) * sqrt(two+xgs2(k))
        end do
        if ( abs(rho1-rho2)<max(abs(rho1),abs(rho0fe))*relerr ) exit
        if ( abs(rho1)<abserr ) exit
        rho2 = rho1
       end do
       rho0fe = rho0fe + rho1
       rho_ws = rho_ws*(one+rho0fe)  
      else
       rho_ws = sqrt(ef**3)/(6*pi**2)
       rho_ws = rho_ws*2    ! spin-factor
      end if
      qcell_fe = rho_ws*mecca_state%rs_box%volume
      c_rho = rho_ws*(6*pi)       ! sqrt(ef**3)*(2/pi)
      pf = sqrt(ef+(ef/clight)**2)
!      ndrpts = size(mecca_state%gf_box%green,1)
      ndrpts = size(rho_fe,1)
      allocate(xr(ndrpts),rr(ndrpts))
      nbasis = mecca_state%intfile%nsubl
      lmx = mecca_state%intfile%lmax + lminSS    ! lminSS is a global const (mecca_constants.f)
      xmax = pf*max(maxval(mecca_state%rs_box%rws(1:nbasis)),           &
     &              maxval(mecca_state%rs_box%rcs(1:nbasis)))
      ndint = 4*ndrpts
      allocate(rint(ndint),feJJint(ndint))
      call radialJJint(lmx,ndint,xmax,feJJint)     !? it can be saved for re-use 
      forall ( k=1:ndint ) rint(k) = k*(xmax/ndint)
      do nsub=1,nbasis
       do ic=1,mecca_state%intfile%sublat(nsub)%ncomp
        v_rho_box=>mecca_state%intfile%sublat(nsub)%compon(ic)%v_rho_box
        call g_meshv(v_rho_box,h,xr,rr)
        do k=1,v_rho_box%ndrpts
         rho_fe(k,ic,nsub)= c_rho*ylag(rr(k),rint,feJJint,0,3,ndint,kx) &
     &                    * rr(k)**2
!         if ( mecca_state%intfile%Tempr>zero ) then    !TODO:  non-zero temperature case
!          CYCLE
!         end if  
        end do
       end do
      end do
      deallocate(xr,rr,rint,feJJint)
      return
!EOC
      end subroutine getWSfreeElctrn
!
!BOP
!!IROUTINE: radialJJint
!!INTERFACE:
      subroutine radialJJint(lmax,nd,xmax,feJJintegral)
!!DESCRIPTION:
! to calculate free electron JJ-integral:  
! {\bv
!     Sum(l=0:lmax { (2*l+1)/X^3*Integral(0:Xmax| (x*j_l(x))^2) } )
!  \ev}

!!DO_NOT_PRINT      
      implicit none
!!DO_NOT_PRINT      

!!ARGUMENTS:
      integer, intent(in) :: nd,lmax
      real(8), intent(in) :: xmax
      real(8), intent(out) :: feJJintegral(1:nd)
!!REVISION HISTORY:
! Initial Version - A.S. - Mar 2014
!EOP
!
!BOC
      integer :: l,k
      real(8) :: h,hh,rk
      real(8) :: f0,f1,f2
      complex(8) :: xk,bjl(0:lmax),bnl(0:lmax)

      if ( nd<=0 .or. lmax<0 ) return
      h = xmax/nd
      hh = h/three
      xk = h
      call ricbes0(lmax+1,xk,bjl,bnl)
      f1 = zero
      do l=0,lmax
       f1 = f1+(2*l+1)*dreal(bjl(l))**2
      end do
      f0 = zero
      feJJintegral(1) = h/2*(f0+f1)
      do k=2,nd
        xk = dcmplx(k*h,zero)
        call ricbes0(lmax+1,xk,bjl,bnl)
        f2 = zero
        do l=0,lmax
         f2 = f2+(2*l+1)*dreal(bjl(l))**2
        end do
        feJJintegral(k) = feJJintegral(k-1) + hh*(f0+four*f1+f2)
        f0 = f1
        f1 = f2
      end do
      do k=1,nd
        rk = k*h
        feJJintegral(k) = feJJintegral(k)/rk**3
      end do
      return
!EOC
      end subroutine radialJJint
!
      end module density      
