#!/bin/csh 
#
# arguments should have absolute paths
#  examples:  ../../libxc.sh `pwd`/../.. `which ifort` `which fpp`
#             ../../libxc.sh `pwd`/../.. `which gfortran` `which cpp`

set LIBXC=$1
set FC=$2
set FCCPP=$3

rm -f ${LIBXC}/include/libxc_funcs_m.mod
rm -f ${LIBXC}/include/xc_f90_lib_m.mod
rm -f ${LIBXC}/include/xc_f90_types_m.mod
rm -f ${LIBXC}/lib/libxc.a 
rm -f ${LIBXC}/lib/libxc.la 
rm -f ${LIBXC}/lib/libxcf90.a
rm -f ${LIBXC}/lib/libxcf90.la
rm -f ${LIBXC}/lib/libxcf03.a
rm -f ${LIBXC}/lib/libxcf03.la
#rm -f ${LIBXC}/lib/pkgconfig/libxc.pc 

#set MAKE_XC=`echo ./configure --prefix=${LIBXC} FC=${FC} FCCPP=${FCCPP} FCFLAGS="-g"`
set MAKE_XC=`echo ./configure --prefix=${LIBXC} FC=${FC} FCCPP=${FCCPP} FCFLAGS="-O2" CFLAGS="-O2"`

##if ( $1 == 'ifort' ) set MAKE_XC=`echo ${MAKE_XC} FC=$1 FCCPP=$2`

`echo ${MAKE_XC}`
make clean >& /dev/null
make && make install && make distclean

#mkdir -p $LIBXC/finclude
#mv $LIBXC/include/*.mod $LIBXC/finclude
