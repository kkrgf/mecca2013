!c     For REAL x
!c     ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      subroutine zeroout( x, nx )
!c     =================================================================
      implicit none
      integer nx
      real(8), intent(out) :: x(nx)
      x = 0.d0
      return
      end

!c     =================================================================
!c     For COMPLEX x
!c     ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      subroutine zerooutC( x, nx )
!c     =================================================================
      implicit none
      integer nx
      complex(8), intent(out) :: x(nx)
      x = (0.d0,0.d0)
      return
      end
