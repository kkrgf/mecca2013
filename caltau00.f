!BOP
!!ROUTINE: caltau00
!!INTERFACE:
      subroutine caltau00(g,nrmat,t,ndimt,itype,                        &
     &                    natom,kkrsz,tau00,iswitch)
!!DESCRIPTION:
! wrapper for subroutine gr2tauz
! {\bv
! input: g=G ,  order KKRSZ*NATOM
!
! output: tau00=t+t*G*t -- tau-matrix, if G=G0*[1-t*G0]^(-1)
! \ev}

!!DO_NOT_PRINT
      implicit none
!!DO_NOT_PRINT

!!ARGUMENTS:
      integer, intent(in) :: ndimt
      complex(8), intent(in) :: g(*),t(*)
      complex(8), intent(out) :: tau00(ndimt,ndimt,*)
      integer , intent(in) :: nrmat,itype(*),natom,kkrsz,iswitch
!!REVISION HISTORY:
! Adapted - A.S. - 2013
!!REMARKS:
! iswitch=2 is not implemented
!EOP
!
!BOC
!c      integer    idiag
      character*10 :: sname = 'caltau00'

!C      nrmat = kkrsz*natom
      if(iswitch.ne.2) then
        call gr2tauz(g,nrmat,t,ndimt,itype,                             &
     &                   natom,kkrsz,tau00)
      else
       call fstop(sname//' :: iswitch=2 is not implemented')
      end if
      return
!EOC
      end subroutine caltau00

!BOP
!!ROUTINE: gr2tauz
!!INTERFACE:
      subroutine  gr2tauz(g,nrmat,t,ndimt,itype,                        &
     &                           natom,kkrsz,tau)
!!DESCRIPTION:
! {\bv
! input: g=G
!
! output: tau=t*G*t+t
! \ev}

!!USES:
      use mecca_constants

!!DO_NOT_PRINT
      implicit none
!!DO_NOT_PRINT

!!ARGUMENTS:
      integer, intent(in) :: nrmat,ndimt,natom,kkrsz
      complex(8), intent(in) :: g(nrmat*nrmat)
      complex(8), intent(in) :: t(ndimt,ndimt,*)
      complex(8), intent(out) :: tau(ndimt,ndimt,*)
      integer, intent(in) :: itype(natom)
!!REVISION HISTORY:
! Adapted - A.S. - 2013
!EOP
!
!BOC
      integer nsub,ns,i,i1

      complex(8) :: a(kkrsz,kkrsz)  ! complex*16 a(lkkrmax,lkkrmax)
      complex(8) :: b(kkrsz,kkrsz)  ! complex*16 b(lkkrmax,lkkrmax)
!      complex*16 czero,cone
!      parameter (czero=(0.d0,0.d0))
!      parameter (cone=(1.d0,0.d0))
!c     ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

!c
!c  diagonal (I,I)-block
!c
      do nsub = 1,natom

       ns = itype(nsub)
       i1 = (nsub-1)*kkrsz
!C       i2i1 = (i1-1)*nrmat+i1

!c
       call cpblkz(i1,i1,g,nrmat,kkrsz,kkrsz,a,size(a,1))
!c
!c                                     A = transposed-G = G'

!       do i=1,kkrsz
!         do j=1,kkrsz
!           sum = czero
!           do k=1,kkrsz
!            sum = sum+a(k,i)*t(k,j,ns)
!           end do
!           b(i,j) = sum                  ! = G*t
!         end do
!         b(i,i) = b(i,i) + cone          ! = G*t+1
!       end do

       call zgemm('T','N',kkrsz,kkrsz,kkrsz,                            &
     &            cone,a,size(a,1),t(1,1,ns),ndimt,                     &
     &            czero,b,size(b,1))
!c                                                     b = G*t
       do i=1,kkrsz
        b(i,i) = b(i,i) + cone            ! = G*t+1
       end do

!       do i=1,kkrsz
!         do j=1,kkrsz
!           sum = czero
!           do k=1,kkrsz
!            sum = sum+t(j,k,ns)*b(k,i)
!           end do
!           tau(j,i,nsub) = sum             ! = t*(G*t+1)
!         end do
!       end do

       call zgemm('N','N',kkrsz,kkrsz,kkrsz,                            &
     &            cone,t(1,1,ns),ndimt,b,size(b,1),                     &
     &            czero,tau(1,1,nsub),ndimt)

      end do

      return
!EOC
      end subroutine  gr2tauz

!BOP
!!ROUTINE: gr2gtp1
!!INTERFACE:
      subroutine gr2gtp1(g,nrmat,t,ndimt,itype,                         &
     &                   natom,kkrsz,tau                                &
     &                   )
!!DESCRIPTION:
! {\bv
! input: g=G
!
! output: tau=G*t+1
! \ev}
!
!!USES:
      use mecca_constants

!!DO_NOT_PRINT
      implicit none
!!DO_NOT_PRINT

!!ARGUMENTS:
      integer nrmat,ndimt,natom,kkrsz
      complex*16 g(nrmat*nrmat)
      complex*16 t(ndimt,ndimt,*)
      complex*16 tau(kkrsz,kkrsz,natom)
      integer    itype(natom)
!!REVISION HISTORY:
! Adapted - A.S. - 2013
!EOP
!
!BOC
      integer nsub,ns,i1,i

      complex(8) :: a(kkrsz,kkrsz)  ! complex*16 a(lkkrmax,lkkrmax)
!c      complex*16 b(lkkrmax,lkkrmax)
!      complex*16 czero,cone
!      parameter (czero=(0.d0,0.d0))
!      parameter (cone=(1.d0,0.d0))
!c     ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

!      if(lkkrmax.lt.kkrsz) stop ' lkkrmax.lt.kkrsz '

!c
!c  diagonal (I,I)-block
!c
      do nsub=1,natom

       ns = itype(nsub)
       i1 = (nsub-1)*kkrsz
!C       i2i1 = (i1-1)*nrmat+i1

!c
       call cpblkz(i1,i1,g,nrmat,kkrsz,kkrsz,a,size(a,1))
!c
!c                                     A = transposed-G = G'

       call zgemm('T','N',kkrsz,kkrsz,kkrsz,                            &
     &           cone,a,size(a,1),t(1,1,ns),ndimt,                      &
     &            czero,tau(1,1,nsub),kkrsz)
!c                                               tau = G*t
       if(nsub.eq.1) then
        do i=1,kkrsz
         tau(i,i,1) = tau(i,i,1) + cone         ! = G*t+1
        end do
       end if

      end do


      return
!EOC
      end subroutine gr2gtp1
