!BOP
!!MODULE: mecca_types
!!INTERFACE:
      module mecca_types
!!DESCRIPTION:
! The module contains  {\sc mecca} user-derived types definitions
!
!!USES:
      use iso_c_binding, only: c_ptr, c_loc, c_f_pointer
!      use mecca_constants
      use mecca_constants, only : ipspin, ipeval, ipcomp, ipmesh, ipunit
      use mecca_constants, only : dflt_spltscale, lj
      use mecca_constants, only : nu_xyz,nu_mix1,nu_mix2,nu_mdlng,      &
     &    nu_print,nu_info,nu_inpot,nu_outpot,nu_spkpt,nu_dop,nu_yymom, &
     &    nu_dos,nu_bsf,nu_fs1,nu_fs2
      use universal_const, only: dp=>DBL_R
      implicit none
!
!!PUBLIC TYPES:
!      type, public :: RunState            ! run-description container, a set of pointers
!                                          ! (all available info about a run state)
!      type, public :: IniFile             ! input container to initiate/continue a run
!      type, public :: RS_Str              ! R-SPACE container
!      type, public :: KS_Str              ! K-SPACE container
!      type, public :: EW_Str              ! Ewald container
!      type, public :: VP_data             ! VP container (space related)
!      type, public :: IntrstlQ            ! interstitial region container (charge related)
!      type, public :: GF_output           ! Green\'s function output container
!      type, public :: GF_data             ! E-point Green\'s function data container
!      type, public :: Tau_data            ! (E,{k})-point Tau matrix container
!      type, public :: Work_data           ! SCF working data container
!      type, public :: DosBsf_Data         ! data container for density-of-states and
!                                          ! bloch-spectral-function calculations
!      type, public :: FS_Data             ! data container for Fermi surface calculations
!      type, public :: SphAtomDeps         ! species potential-density container
!      type, public :: IOFile              ! input-output file description
!      type, public :: Element             ! chemical element description
!      type, public :: Species             ! species description
!      type, public :: Sublattice          ! sublattice (a set of species) description
!!REVISION HISTORY:
! Initial Version - A.S. - 2013
!!REMARKS:
!EOP
!
!BOC
!c*******************************************************************
      type, public :: F_void
!c*******************************************************************
       type(c_ptr) :: data
       integer :: id=-1
      end type F_void
!c*******************************************************************
      type, public :: R_params !
!c*******************************************************************
       integer :: id=0
       real(8), allocatable :: params(:)
      end type R_params
!
!c*******************************************************************
      type, public :: IOFile
!c*******************************************************************
       character(80) :: name=''
       character(1) :: status=''
       character(1) :: form=''
       integer nunit
      end type IOFile

!c*******************************************************************
      type, public :: Element
!c*******************************************************************
       integer :: ztot=0
       integer :: zval=0
       character(15) :: name=''
       character(2)  :: symb=''
      end type Element
!c*******************************************************************
      type, public :: SphAtomDeps
!c*******************************************************************
       real(8) :: xst=0    ! xst,xmt,jmt defines a mesh xr (rr=exp(xr))
       real(8) :: xmt=0    ! log(rmt) (or log(rws) for ASA)
       integer :: jmt=0    ! h = (xmt-xst)/(jmt-1)
       real(8) :: v0(ipspin)=0
       integer :: ndrpts=0 !  last valid point for vr
       real(8), allocatable :: vr(:,:)      ! (iprpts,ipspin)
       integer :: ndchg=0   ! last valid point for chgtot
       real(8), allocatable :: chgtot(:,:)  ! (iprpts,ipspin)
       integer :: ndcor=0   ! last valid point for chgcor
       real(8), allocatable :: chgcor(:,:)  ! (iprpts,ipspin)
!       real(8) :: ztot(ipspin)=0   ! real-space chgtot-integrals
       real(8) :: zcor(ipspin)=0   ! real-space chgcor-integrals
       real(8) :: ztot_T(ipspin)=0   ! real-space temperature chgtot-integrals
       real(8) :: zcor_T(ipspin)=0   ! real-space temperature chgcor-integrals
!       real(8) :: zval=0
       integer :: numc(ipspin)=0
       integer :: nc(ipeval,ipspin)
       integer :: lc(ipeval,ipspin)
       integer :: kc(ipeval,ipspin)
       real(8) :: ec(ipeval,ipspin)
       real(8) :: highest_occupied_core_level(0:2,1:2)=0  ! 0->num,1->occupation,2->ec
!      character(len=5) :: lj(:)
      end type SphAtomDeps
!c*******************************************************************
      type, public :: Species
!c*******************************************************************
       character(16) :: name=''
       real(8) :: concentr=0
       real(8) :: ex_ini_splt=dflt_spltscale      ! relative exchange initial splitting
       real(8) :: rSph=0
       integer :: zID=0
       integer :: zcor=0
       type(SphAtomDeps) :: v_rho_box
      end type Species
!c*******************************************************************
      type, public :: Sublattice
!c*******************************************************************
       type ( Species ) :: compon(1:ipcomp)
       integer :: ncomp=0   ! komp, number of components
       real(8) :: rIS=0     ! geom. rIS (e.g. from VP) in alat units
       integer :: ns=0      ! number of sublattice sites
       integer :: indx=0    ! sublattice index (nsub)
       logical :: isDLM=.false.
       logical :: isflipped=.false.
       integer :: flippoint=0
      end type Sublattice
!c*******************************************************************
      type, public :: IniFile
!c*******************************************************************
      character(80) :: name=''
      character(64) :: genName=''
      type ( IOFile ) :: io(1:ipunit)
      integer ::  numIOfiles=0
      character(10) :: istop=''
      character(32) :: task=''   ! MP_NAME (executable name, only for parallel jobs)
      integer :: nproc=1
      integer :: ntask=0
      integer :: icryst=0
      integer :: imethod=0    ! k-space (see DefMethod for other options))
      integer :: intgrsch=0   ! integration scheme flag (ASA:1, VP:2), interpretation depends on mtasa
      integer :: sprstech=0
      integer :: iprint=0
      integer :: lmax=0
      integer :: iXC=0        ! exchange-correlation type
      type(R_params) :: x_pot_corr   ! X potential correction (such as vLB potential)
      integer :: mtasa=11     ! ASA (0 - MT)
      integer :: nspin=0      ! non-magn/1, SP/2, DLM/3 ( => nMM=1, i.e. only in input file)
      integer :: nMM=0        ! magn.model: 1 - DLM averaged potential, -1 - with opposite spins
      integer :: nrel=0       ! scalar-relat. (>14 for non-relat)
      real(8) :: Rrsp=0    !  the RS translation vectors maximum
      real(8) :: Rksp=0   !  the KS translation vectors maximum
      real(8) :: Rclstr=0  !  the RS cluster radius
      real(8) :: enmax=0  !  Ewald parameter, best value is about (efermi+delta)
      real(8) :: eneta=0  !  Ewald parameter, (between 0 and 2.8 -- limitation from d003-routine)
      integer :: nmesh=0
      integer :: nKxyz(1:3,1:ipmesh)=0
      real(8) ::  enKlim(1:2,1:ipmesh-1)=0
!      character(32) :: systemID=''
      real(8) :: alat=0
      real(8) :: ba=0
      real(8) :: ca=0
      integer :: nelements=0
      type (Element), allocatable :: elements(:)
      integer :: ncpa=1
      integer :: nsubl=0
      type ( Sublattice ), allocatable :: sublat(:)
      integer :: iset_ebot=-1  ! (internal)
      integer :: igrid=0
      real(8) :: ebot=0
      real(8) :: etop=0
      real(8) :: eibot=0
      integer :: npts=0
      real(8) :: eitop=0
      integer :: npar=0
      integer :: nscf=0
      real(8) :: alphmix=0
      real(8) :: betamix=0
      integer :: imixtype=1              ! (0 -charge, 1 - potential)
      integer :: mixscheme=1             ! Broyden mixing (0 - simple, >1 -- Broyden+simple)
      real(8) :: so_power=1    ! spin-orbit power, so_power=0 -> scalar-relativistic, so_power=1 -> Dirac
!      real(8) :: so_power=0    ! spin-orbit power, so_power=0 -> scalar-relativistic, so_power=1 -> Dirac
      real(8) :: Tempr=0
      real(8) :: magnField=0
      integer :: freeze_core=0           ! if freeze_core.ne.0, then core charge density is fixed
      real(8) :: V0(2)=0       ! potential at R=Inf (internal)
      real(8) :: rho0          ! PBC interst. charge density (internal)
      end type IniFile

!c*******************************************************************
      type, public :: IntrstlQ  !
!c*******************************************************************
!
!     InterstRegion == VP\SphR or ASA\SphR
!
      real(8) :: SphR=0   !
      real(8) :: qsph=0   !   Integral[ d3R Over SphR | Z-ChargeDen ]
      real(8) :: q0=0     !   Integral[ d3R Over InterstRegion | ChargeDen ]
      real(8) :: qmag=0   !   Integral[ d3R Over InterstRegion | SpinDen ]
      real(8) :: qm1(2)=0 !   Integral[ d3R Over InterstRegion | ChargeDen/R ]
!                         !   ASA\SphR: qm1(1), VP\SphR: qm1(2)
!
!      real(8) :: qvxc(2)=0!   Integral[ d3R Over InterstRegion | Vxc*Den ]
!      real(8) :: qexc(2)=0!   Integral[ d3R Over InterstRegion | Exc*Den ]
!
!      real(8) :: Uc=0     !   Integral[ d3R Over InterstRegion
!                         !    Integral[ d3R' InterstRegion |
!                         !     ChDen(R)*ChDen(R')/|R-R'| ] ]
!                         !    *2 -- to be in Ry
!      real(8) :: Uc0=0    !   Integral[ d3R Over InterstRegion
!                         !    Integral[ d3R' InterstRegion |
!                         !              ChDen(R')/|R-R'| ] ]
!                         !    *2 -- to be in Ry
      complex(8), allocatable :: YlmVPmltpl(:)  ! (lmltp+1)*(lmltp+2)/2)
                          ! Integral[ d3R Over VP | Ylm*Den ]
                          !  YlmVPmltpl(1),   l=0, m=0
                          !  YlmVPmltpl(2:3), l=1, m=0:1
                          !  YlmVPmltpl(4:6), l=2, m=0:2, etc
      end type IntrstlQ


!!c*******************************************************************
!      type, public :: POTFILE
!!c*******************************************************************
!       character(len=80) :: comment=''
!       real*8  ztot
!       real*8  alat
!       real*8  zval
!       real*8  efpot
!       real*8  xst
!       real*8  xmt
!       integer jmt
!       real*8  vr(iprpts)
!       real*8  v0
!       integer ntchg
!       real*8  xvalsws
!       real*8  chgtot(iprpts)
!       integer numc
!       integer ntcor
!       integer nc(ipeval)
!       integer lc(ipeval)
!       integer kc(ipeval)
!       real*8  ec(ipeval)
!       character(len=5) lj(ipeval)
!       real*8  chgcor(iprpts)
!      end type POTFILE
!
!!c*******************************************************************
!      type, public :: VP      ! Voronoi Polyhedron
!!c*******************************************************************
!       real*8 facevtx(3,ipfvtx,ipnnmax) ! coordinates of VPface-vertices
!       real*8 vertex(3,ipvtx)           ! coordinates of VP-vertices
!       real*8 enn(3,0:ipnnmax)   ! normal vectors to the faces, |enn|=1
!       real*8 Rij(0:ipnnmax)     ! VP-NN distance; xyznn = Rij*enn
!       real*8 Dij(1:ipnnmax)     ! distance between VP-centre and a Face
!       real*8 Rcs        ! CircumsribedSphere-radius, i.e. max(|vertex|)
!       real*8 Ris        ! InsribedSphere-radius, i.e. min(Rij(2:nnvp+1)/2)
!!c
!!c    xyznn(0) -- coordinates of the VP-center, i.e. (x,y,z) for Iat
!!c
!c  ALL COORDINATES except xyznn(1:3,1) ARE GIVEN RELATIVE TO THE POSITION
!!c  OF THE VP-Centre (= Rij(0)*enn(1:3,0))
!c
!c    xyznn(j) = xyz(jat)-xyz(iat), j=1,nnvp -- NN-coordinates
!!c      (iat,jat are not provided here)
!c
!      integer nnvp      ! number of faces, i.e. number of VP-NN
!!c                       ! (i.e. directions to VP-NN)
!      integer nntype(0:ipnnmax) ! type (sublattice) of the VP-NN
!      integer npnts(0:ipnnmax)  ! can be used to identify number of elements
!!c                               ! related to each NN (face)
!       integer nvtx             ! number of VP-vertices,        .GE.4
!!c
!       integer nfvtx(ipnnmax)  ! number of vertices belonging to each face,
!!c                             !  .GE.3
!
!      end type VP
!
!c*******************************************************************
      type, public :: RS_Str              ! R-SPACE section
!c*******************************************************************
!c
!   lattice
      real(8) :: alat=0,volume=0     ! for fcc, volume=alat^3/4
      real(8) :: rslatt(3,3)=0       ! lattice vectors=rslatt(:,*)/(2*Pi)
!   lattice sites
      integer :: natom=0
      real(8),  allocatable :: xyzr(:,:)   ! xyz(1:3,1:natom) --> coordinates
!                                         ! ??? xyz(4:6,:)  --> Ris,Rasa,Rcs
      integer, allocatable :: itype(:)    ! atoms decoration (1:natom)
!   sublattice
      integer :: nsubl=0
      integer, allocatable :: numbsubl(:) ! equiv.atoms in the sublattice (1:nsubl)
      real(8), allocatable  :: rmt(:)  ! rmt - from VP
      real(8), allocatable  :: rws(:)
      real(8), allocatable  :: rcs(:)  ! rcs - from VP
!c                                 4*pi/3*rws**3 = VP/WS volume
      real(8), allocatable :: Rnn(:) !  NN-distance
      integer, allocatable :: if0(:,:)
      integer, allocatable :: mapstr(:,:),mappnt(:,:),mapij(:,:)
      real(8) :: Rnncut=0
      integer, allocatable :: mapsprs(:,:)
      integer :: ncount=0          ! number of "aij"
      real(8), allocatable :: aij(:,:)  ! basis_j-basis_i
!      real*8, allocatable :: omegamt(:)
!      real*8, allocatable :: surfamt(:)
!
      end type RS_Str
!c*******************************************************************
      type, public :: KS_Str                ! K-SPACE section
!c*******************************************************************
!c
      real(8) :: kslatt(3,3)=0
      real(8) :: rot(49,3,3)=0
      integer :: ib(48)=0
      integer :: nrot=0
      integer :: lmax=0,ndimq=0
      complex(8), allocatable :: dop(:,:)   !  dop((lmax+1)*(lmax+1),(lmax+1)*(lmax+1),*)
      integer, allocatable :: nqpt(:)             !  nqpt(nmesh)
      real(8), allocatable :: qmesh(:,:,:)   !  qmesh(3,ndimq,nmesh)
      integer, allocatable :: wghtq(:,:)    !  wghtq(ndimq,nmesh)
      real(8), allocatable :: twght(:)       !  twght(nmesh)
!      integer, allocatable :: lwght(:,:)    !  lwght(ipkpts*ipmesh)
!c      integer lrot(48,ipkpts,ipmesh)
      integer, allocatable :: lrot(:,:)     !  lrot(48,:)
      integer, allocatable :: ngrp(:),kptgrp(:,:)
      integer, allocatable :: kptset(:,:),kptindx(:,:)
!
      end type KS_Str
!c*******************************************************************
      type, public :: EW_Str                ! Ewald section
!c*******************************************************************
!c
      real(8) :: Rksp=0,Rrsp=0               ! Rksp -- in 1/alat-units; Rrsp -- in alat-units
      integer :: ndimk=0,ndimr=0,ndimrij=0
      integer :: nrsn=0, nrsij=0
      integer, allocatable :: nksn(:)        ! nksn(nmesh)
!  ndimk = sum(nksn(1:nmesh))
      real(8), allocatable :: xknlat(:)  ! xknlat(ndimk*3*nmesh)     ! in 2*pi/alat-units
!  for imesh=1, xknlat(imesh=1)=reshape(xknlat(1:3*nkns(1)),[nkns(1),3])
!  for imesh=2, xklnat(imesh=2)=reshape(xknlat(3*nkns(1)+1:3*nkns(2)),[nkns(2),3]), etc.

!      real(8), allocatable :: xrnlat(:)      ! xrnlat(ndimr*3)           ! in alat-units
      real(8), allocatable :: xrslatij(:,:)  ! xrslatij(ndimrij,4)
      integer, allocatable :: np2r(:,:)      ! np2r(ndimr,*)
      integer, allocatable :: numbrs(:)      ! numbrs(*)
!
      end type EW_Str
!c*******************************************************************
      type, public :: vect3d
!c*******************************************************************
!
      real(8), allocatable :: array(:,:,:)
      integer, allocatable :: indx(:)
!      real(8), allocatable :: r0nn(:)
      end type vect3d
!c*******************************************************************
      type, public :: VP_data                ! VP-integration
!c*******************************************************************
!c
      integer :: nF=0                         ! e.g. nF = MNF
      integer :: ngauss=0                     ! ngauss = MNqp
      integer, allocatable :: fcount(:)       ! fcount(nsubl)
      real(8), allocatable :: weight(:)       ! weight(MNqp), isoparam.-integration
      real(8), allocatable :: rmag(:,:,:,:,:,:) ! rmag(0:3,MNqp,MNqp,MNqp,MNF,nsubl),
                                                ! rmag(0:3,...) = {r,x,y,z}
      real(8), allocatable :: vj(:,:,:,:,:)   ! vj(MNqp,MNqp,MNqp,MNF,nsubl), Jacobian
      real(8), allocatable :: rmt(:)          ! rmt(nsubl)
      real(8), allocatable :: alphpot(:) ! potential shape coeff (Integral{1/|R| d3R})
!                                        ! alphpot = Pot_vp / Pot_ws, i.e. alphpot=1 for WS sphere
      real(8), allocatable :: alphen(:)  ! energy shape coeff (Integral{1/|R-R'| d3R d3R'})
!                                        ! alphen = En_vp / En_ws, i.e. alphen=1 for WS sphere
      complex(8), allocatable :: ylmPls(:,:,:)   ! L>=0, r-moments shape coeff (Integral{r**n*r**L*Y_LM d3r})
                                                 ! ylmPls(0:<rmom>,1:<lmPls>,1:<nsub>); DU normalization for r is Rws
      type(vect3d), allocatable :: ylmMns(:) ! (-L-1)<0,  r-moments shape coeff
                                             ! (Integral{r**n*[|r+R_NN|**(-L-1)-R_NN**(-L-1)]*Y_LM(r+R_NN) d3r})
                                             ! ylmMns(1:<nsub>)%array(0:<rmom>,1:<lmMns>,1:<NN>),
                                             ! DU normalization for r is aunit, aunit**3=VP-volume;
                                             ! ylmMns(1:<nsub>)%indx(ksub(1):ksub(<NN>)) - sublat-type
!
      end type VP_data
!c*******************************************************************
      type, public :: RunState               ! info about Run
!c*******************************************************************
!c
      type(IniFile),  pointer :: intfile =>null()
      type(RS_Str),   pointer :: rs_box => null()
      type(KS_Str),   pointer :: ks_box => null()
      type(EW_Str),   pointer :: ew_box => null()
      type(VP_data), pointer :: vp_box => null()
      type(GF_data),  pointer :: gf_box => null()
      type(GF_output),  pointer :: gf_out_box => null()
      type(Work_data), pointer :: work_box => null()
      type(DosBsf_data), pointer :: bsf_box => null()
      type(FS_data),  pointer :: fs_box => null()
      type(MPI_data), pointer :: mpi_box => null()
      type(Tau_set),  pointer :: allTau => null()
!      type(F_void) :: void => null()
      logical :: fail=.false.
      integer :: info=-1
!
      end type RunState
!c*******************************************************************

!c**************************************************************
!      module MECCADEF
!
!      include 'mkkrcpa.h'
!
!c*******************************************************************
!      type, public :: IntrstlQ  !
!c*******************************************************************
!c
!c  InterstRegion == VP\SphR or ASA\SphR
!c
!       real*8  SphR   !
!       real*8  qsph   !   Integral[ d3R Over SphR | Z-ChargeDen ]
!       real*8  q0     !   Integral[ d3R Over InterstRegion | ChargeDen ]
!       real*8  qmag   !   Integral[ d3R Over InterstRegion | SpinDen ]
!       real*8  qm1    !   Integral[ d3R Over InterstRegion | ChargeDen/R ]
!       real*8  qvxc(2)!   Integral[ d3R Over InterstRegion | Vxc*Den ]
!       real*8  qexc(2)!   Integral[ d3R Over InterstRegion | Exc*Den ]
!c
!       real*8  Uc     !   Integral[ d3R Over InterstRegion
!c                     !    Integral[ d3R' InterstRegion |
!c                     !     ChDen(R)*ChDen(R')/|R-R'| ] ]
!c                     !    *2 -- to be in Ry
!       real*8  Uc0    !   Integral[ d3R Over InterstRegion
!c                     !    Integral[ d3R' InterstRegion |
!c                     !              ChDen(R')/|R-R'| ] ]
!c                     !    *2 -- to be in Ry
!      end type IntrstlQ
!!c*******************************************************************
!      type, public :: POTFILE
!!c*******************************************************************
!
!       character(len=80) comment
!
!       real*8  ztot
!       real*8  alat
!       real*8  zval
!       real*8  efpot
!
!       real*8  xst
!       real*8  xmt
!       integer jmt
!
!       real*8  vr(iprpts)
!
!       real*8  v0
!
!       integer ntchg
!       real*8  xvalsws
!       real*8  chgtot(iprpts)
!       integer numc
!       integer ntcor
!       integer nc(ipeval)
!       integer lc(ipeval)
!       integer kc(ipeval)
!       real*8  ec(ipeval)
!       character(len=5) lj(ipeval)
!       real*8  chgcor(iprpts)
!      end type POTFILE
!!c*******************************************************************
!
!      integer ipnnmax
!      parameter ( ipnnmax = 60)   ! max number of VP-NN
!      integer ipvtx
!      parameter (ipvtx  = 98)       ! max number of VP-vertices
!      integer ipfvtx
!      parameter (ipfvtx = 24)       ! max number of face-vertices
!
!!c*******************************************************************
!      type, public :: VP      ! Voronoi Polyhedron
!!c*******************************************************************
!
!       real*8 facevtx(3,ipfvtx,ipnnmax) ! coordinates of VPface-vertices
!       real*8 vertex(3,ipvtx)           ! coordinates of VP-vertices
!       real*8 enn(3,0:ipnnmax)   ! normal vectors to the faces, |enn|=1
!       real*8 Rij(0:ipnnmax)     ! VP-NN distance; xyznn = Rij*enn
!       real*8 Dij(1:ipnnmax)     ! distance between VP-centre and a Face
!       real*8 Rcs        ! CircumsribedSphere-radius, i.e. max(|vertex|)
!       real*8 Ris        ! InsribedSphere-radius, i.e. min(Rij(2:nnvp+1)/2)
!
!!c
!!c    xyznn(0) -- coordinates of the VP-center, i.e. (x,y,z) for Iat
!!c
!!c  ALL COORDINATES except xyznn(1:3,1) ARE GIVEN RELATIVE TO THE POSITIO !N
!!c  OF THE VP-Centre (= Rij(0)*enn(1:3,0))
!!c
!!c    xyznn(j) = xyz(jat)-xyz(iat), j=1,nnvp -- NN-coordinates
!!c      (iat,jat are not provided here)
!!c
!       integer nnvp      ! number of faces, i.e. number of VP-NN
!!c                                ! (i.e. directions to VP-NN)
!       integer nntype(0:ipnnmax) ! type (sublattice) of the VP-NN
!
!       integer npnts(0:ipnnmax)  ! can be used to identify number of elements
!!c                                ! related to each NN (face)
!       integer nvtx             ! number of VP-vertices,        .GE.4
!!c
!       integer nfvtx(ipnnmax)  ! number of vertices belonging to each face,
!!c                             !  .GE.3
!
!      end type VP
!!c*******************************************************************
!
!      parameter (Nsphmx=500)
!!c      parameter (Nsphmx=240)
!!c      parameter (Nsphmx=400)
!!c      parameter (Nsphmx=500)
!!c      parameter (Nsphmx=1000)
!!c      parameter (Nsphmx=1500)
!!c      parameter (Nsphmx=2000)
!!c     parameter (Nsphmx=3000)
!!c      parameter (Nsphmx=4000)
!!c      parameter (Nsphmx=6000)
!!c      parameter (Nsphmx=8000)
!!c      parameter (Nsphmx=12000)
!!c      parameter (Nsphmx=18000)
!!c      parameter (Nsphmx=30000)
!      integer ippntmx
!      parameter (ippntmx =80*Nsphmx)     ! ippntmx ~ Nsphmx^(3/2) ???
!      integer ipsurfmx
!      parameter (ipsurfmx =10*Nsphmx)     ! ipsurfmx ~ Nsphmx     ???
!
!!c*******************************************************************
!      type, public :: ChDens3d      !
!!c*******************************************************************
!
!       real*8  Qk(ippntmx,ipcomp)      ! Qk(k) is a charge at Rk(1:3,k), k.le.Nk
!       real*8  QMk(ippntmx,ipcomp)     ! QMk(k) is a magn.charge at Rk(1:3,k)
!       real*8  Rk(1:3,ippntmx)  ! Rk(1:3,*) -- {x,y,z}-point,
!       real*8  R0k(ippntmx)     ! sqrt(Rk*Rk), center of the layer
!       real*8  dRk(ippntmx)     ! h/2, half-width of the sph.layer
!!c                               ! i.e. a layer is between R0k-dRk and
!!c                               !                         R0k+dRk
!       real*8  Vk(ippntmx)      ! volume of element
!       integer Nk               ! actual number of charges
!
!      end type ChDens3d
!!c*******************************************************************
!
!!c*******************************************************************
!      type, public :: SurfIntgr      !
!!c*******************************************************************
!
!       real*8  Rk(1:3,ipsurfmx)  ! Rk(1:3,*) -- "center" of surface element
!       real*8  R0k(ipsurfmx)     ! sqrt(Rk*Rk)
!       real*8  Sk(ipsurfmx)      ! area of surface element
!       real*8  ank(1:3,ipsurfmx) ! direction of the surface normal
!!c       real*8  CosTk(ipsurfmx)   ! cos(angle between Rk and the surface normal)
!       integer Nk                ! actual number of elements
!
!      end type SurfIntgr
!!c*******************************************************************
!      end module MECCADEF
!!c*******************************************************************

!c*******************************************************************
      type, public :: GF_data
!c*******************************************************************
!       Green's Function and DOS (zplane module)
      complex(8) :: energy(1:2)=0                   ! current energy
      complex(8), allocatable :: dos(:,:,:,:)  !  (0:iplmax,ipcomp,ipsublat,ipspin)
      complex(8), allocatable :: dosck(:,:,:)    !  ipcomp,ipsublat,nspin
      complex(8), allocatable :: green(:,:,:,:)    !  iprpts,ipcomp,ipsublat,spin
!      complex(8), allocatable :: pterm(:,:,:)    !  ipcomp,ipsublat,spin
!
      end type GF_data

!c*******************************************************************
      type, public :: GF_output
!c*******************************************************************
!       Green's Function and DOS (zplane module) output
      integer :: nume=0                       !   number of E-points
      complex(8), allocatable :: egrd(:)     !   E-point
      complex(8), allocatable :: wgrd(:)     !   intgr.weight of E-point
      complex(8), allocatable :: dostspn(:,:)  ! (2,nume)
      complex(8), allocatable :: greenint(:,:,:,:)  ! (iprpts,ipcomp,ipsublat,ipspin)
      complex(8), allocatable :: greenlast(:,:,:,:)  ! (iprpts,ipcomp,ipsublat,ipspin)
!         greenlast is calculated in the point egrd(ie) with ie: wgrd(ie)=0
      complex(8), allocatable :: doslast(:,:,:)  ! (ipcomp,ipsublat,ipspin)
      complex(8), allocatable :: doscklast(:,:,:)  ! (ipcomp,ipsublat,ipspin)
!      real(8), allocatable :: Pterm(:,:,:)  ! (ipcomp,ipsublat,ipspin)
!      real(8), allocatable :: dosint(:,:,:)  ! (ipcomp,ipsublat,ipspin)
!      real(8), allocatable :: dosckint(:,:,:)  ! (ipcomp,ipsublat,ipspin)
      real(8), allocatable :: evalsum(:,:,:)  ! (ipcomp,ipsublat,ipspin)
      real(8), allocatable :: xvalws(:,:,:)  ! (ipcomp,ipsublat,ipspin)
      real(8), allocatable :: xvalmt(:,:,:)  ! (ipcomp,ipsublat,ipspin)
      real(8) :: efermi    !  Ef-estimate based on calculated dos
      integer :: ms_gf=0                   ! if ms_gf=0, green = SS+MS
                                           ! if ms_gf=1, green = MS
!
      end type GF_output

!c*******************************************************************
      type, public :: Work_data
!c*******************************************************************
!  scf work data
      real(8), allocatable :: vr(:,:,:,:) ! (iprpts,ipcomp,ipsublat,ipspin)
      real(8), allocatable :: chgsemi(:,:,:,:) ! (iprpts,ipcomp,ipsublat,ipspin)
      real(8), allocatable :: esemicorr(:,:,:) ! (ipcomp,ipsublat,ipspin)
      real(8), allocatable :: zsemi(:,:,:) ! (ipcomp,ipsublat,ipspin)
      real(8), allocatable :: vrms(:,:,:) ! (ipcomp,ipsublat,ipspin); in fact, it is max, not rms
      real(8), allocatable :: qrms(:,:,:) ! (ipcomp,ipsublat,ipspin);
      real(8), allocatable :: dq_err(:,:,:) ! xval-qcal error
      real(8) :: vmtz(2)=0 ! potential "zero"
      real(8) :: dV0(2)=0    ! abs(z[n=1]-z[n])
      real(8) :: etot=0   ! total energy
      real(8) :: press=0  ! pressure
      real(8) :: entropy=0  ! entropy
!      real(8) :: Emf_cc=0  ! MF-CC energy
!      real(8) :: emad=0    ! Madelung (ES lattice) energy
!      real(8) :: emadp=0   ! Madelung pressure contribution
!      real(8) :: emtc=0    ! MT-correction for ASA
!      real(8) :: exc(2)=0  ! XC-energy
!      real(8) :: vxc(2)=0  ! XC-pot
!
      end type Work_data

!c*******************************************************************
      type, public :: DosBsf_Data
!c*******************************************************************
!       density of states and bloch spectral function related data

      integer :: idosplot=0
      integer :: ibsfplot=0
      integer :: dosbsf_kintsch=1     ! 1 = monkhorst-pack;  2 = ray method
      integer :: nx=0,ny=0,nz=0       ! k-space mesh parameters
      integer :: ray_nrad=0,ray_nrec=0    ! ray-method parameters
      logical :: ray_fixed
      integer :: dosbsf_nepts=0
      integer :: bsf_ksamplerate=0
      integer :: bsf_nkwaypts=0
!      real(8) :: klen=0
      real(8) :: bsf_ef
      real(8) :: dosbsf_emin=0
      real(8) :: dosbsf_emax=0
      real(8) :: dosbsf_esmear=0
      real(8), allocatable :: bsf_kwaypts(:,:)      ! (3,ipbsfkpts)
      character(32), allocatable :: bsf_kptlabel(:) ! (ipbsfkpts)
!      character(80) :: bsf_file
!      integer :: bsf_fileno=nu_bsf
!
      end type DosBsf_Data

!c*******************************************************************
      type, public :: FS_Data
!c*******************************************************************
!       Fermi surface related data

      real(8) :: fermi_korig(3)=0
      real(8) :: fermi_en=0
      real(8), allocatable :: fermi_kpts(:,:)   ! (3,ipbsfkpts)
      integer :: fermi_nkpts=0
      integer :: fermi_gridsampling=0
      integer :: fermi_raysampling=0
      integer :: ifermiplot=0
!      character(80) :: fermi_file(2)
!      integer :: fermi_fileno(2)=[ nu_fs1, nu_fs2 ]
      character(32), allocatable :: fermi_kptlabel(:)  ! (ipbsfkpts)
      character(80) :: fermi_orilabel
!
      end type FS_Data

!c*******************************************************************
      type, public :: MPI_data
!c*******************************************************************
        integer :: ik_begin, ik_end, thisE_comm
      end type MPI_data

!c*******************************************************************
      type, public :: Tau_data
!c*******************************************************************
       complex(8)          :: energy                   ! energy points
       complex(8), pointer :: tab(:,:,:,:)=>null()     ! (kkrsz,kkrsz,1:maxcomp,nbasis), spin-dependent
       complex(8), pointer :: tcpa(:,:,:)=>null()      ! (kkrsz,kkrsz,nbasis), spin-dependent
       complex(8), pointer :: tau_k(:,:,:,:)=>null()   ! diagonal blocks of Tau-matrix, spin-dependent
                                                       ! (kkrsz,kkrsz,ns,nK)
       complex(8), pointer :: logdet(:)=>null()        ! log(det(tau_k)), spin-dependent
                                                       ! (nK)
       real(8), pointer    :: kpoints(:,:)=>null()     ! set of k-points (1:3,1:nK)
       integer             :: ns                       ! number of sublattices  (nbasis or natoms)
                                                       ! if ns==0, then ns = size(tcpa,3)
       integer, pointer    :: ncomp(:)=>null()         ! number of components for each sublattice
      end type Tau_data

!c*******************************************************************
      type, public :: Tau_set
!c*******************************************************************
       type(Tau_data), pointer :: set(:,:)=>null()
       integer :: nset=0
      end type Tau_set

!c*******************************************************************
      type, public :: energy_points_t
!c*******************************************************************
       integer       :: nume = 0   ! size of used space
       complex(dp), allocatable :: egrd(:)
       complex(dp), allocatable :: wgrd(:)
       contains
        procedure, pass, public  :: clear  => energy_points_t_clear
        procedure, pass, public  :: resize => energy_points_t_resize
      end type energy_points_t

!c*******************************************************************
      type, public :: DOS_output
!c*******************************************************************
!       DOS (zplane module) output
        type(energy_points_t), pointer :: epoints
        complex(dp), allocatable :: dataspn(:,:)  ! (2,nume)
      end type

      private :: energy_points_t_clear
      private :: energy_points_t_resize

      contains
!c      this routine deallocates internal arrays
        subroutine energy_points_t_clear(this)
          implicit none
          class(energy_points_t), intent(inout) :: this

          if(allocated(this%egrd))  deallocate(this%egrd)
          if(allocated(this%wgrd))  deallocate(this%wgrd)
          this%nume=0
        end subroutine energy_points_t_clear

!c      this routine reallocates internal arrays to newsize
!c      existing elements up to newsize are keept
        subroutine energy_points_t_resize(this,newsize)
          implicit none
          class(energy_points_t), intent(inout) :: this
          integer,                intent(in)    :: newsize

          integer                  :: nume_tmp
          complex(dp), allocatable :: egrd_tmp(:)
          complex(dp), allocatable :: wgrd_tmp(:)


!c        early return for negative argument
          if(newsize<0) then
!c             write(*,'(a)')"negative argument in energy_points_t_resize"
             return
          endif

!c        early return if size is already sufficient
          if(allocated(this%egrd).and.allocated(this%wgrd)) then
           if( (size(this%egrd).gt.newsize) .and.                        &
     &         (size(this%wgrd).gt.newsize) ) then
!c              write(*,'(a)') "size already sufficient for resize"
             this%nume = newsize
             return
           endif
           nume_tmp = size(this%egrd)
           allocate(egrd_tmp(nume_tmp))
           egrd_tmp=this%egrd
           nume_tmp = size(this%wgrd)
           allocate(wgrd_tmp(nume_tmp))
           wgrd_tmp=this%wgrd
           nume_tmp = this%nume
           call this%clear
           allocate(this%egrd(newsize))
           allocate(this%wgrd(newsize))
           this%nume=newsize
!c        copying data back
           this%egrd(1:nume_tmp)=egrd_tmp(1:nume_tmp)
           this%wgrd(1:nume_tmp)=wgrd_tmp(1:nume_tmp)
           this%egrd(nume_tmp+1:)=cmplx(0.0_dp,0.0_dp,dp)
           this%wgrd(nume_tmp+1:)=cmplx(0.0_dp,0.0_dp,dp)
          else
           call this%clear
           this%nume=newsize
           allocate(this%egrd(newsize))
           allocate(this%wgrd(newsize))
           this%egrd(1:newsize)=cmplx(0.0_dp,0.0_dp,dp)
           this%wgrd(1:newsize)=cmplx(0.0_dp,0.0_dp,dp)
          endif
!
          if(allocated(egrd_tmp)) deallocate(egrd_tmp)
          if(allocated(wgrd_tmp)) deallocate(wgrd_tmp)
          return
        end subroutine energy_points_t_resize
!EOC
      end module mecca_types
