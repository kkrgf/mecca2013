!BOP
!!ROUTINE: tote1
!!INTERFACE:
      subroutine tote1(vrnew,rho,corden,xr,rr,                          &
     &                jmt,nbasis,nspin,iexch,                           &
     &                numbsub,komp,atcon,ztotss,omegws,                 &
     &                excort,qintex,emtc,emad,emadp,                    &
     &                evalsum,ecorv,esemv,                              &
     &                etot,press,Eccsum,mtasa,iprint,istop)
!!DESCRIPTION:
! total energy for multi-sublattice and multi-component KKR-CPA
!
!!USES:
      use mecca_constants
      use mecca_types, only : RunState
      use mecca_interface, only  : asa_integral,ws_integral
      use gfncts_interface, only  : g_numNonESsites,g_zsemcout
      use mecca_run
      use polyintrpl, only : derv_expmesh 
      use scf_io, only : readPotential
      use xc_mecca, only : xc_alpha,xc_mecca_pointer,gXCmecca,xc_is_gga
      use xc_mecca, only : defXCforES,undefXCforES

!!DO_NOT_PRINT
      implicit none
!!DO_NOT_PRINT

!!ARGUMENTS:
      real(8), intent(in), target :: vrnew(:,:,:,:)  ! (iprpts,ipcomp,ipsublat,ipspin)
      real(8), intent(in) :: rho(:,:,:,:)  ! (iprpts,ipcomp,ipsublat,ipspin)
      real(8), intent(in) :: corden(:,:,:,:)  ! (iprpts,ipcomp,ipsublat,ipspin)
      real(8), intent(in) :: xr(:,:,:)  ! (iprpts,ipcomp,nbasis)
      real(8), intent(in) :: rr(:,:,:)  ! (iprpts,ipcomp,nbasis)
      integer, intent(in) :: nbasis
      integer, intent(in) :: jmt(:,:)  ! (ipcomp,nbasis)
      integer, intent(in) :: nspin,iexch
      integer, intent(in) :: komp(nbasis),numbsub(nbasis)
      real(8), intent(in) :: atcon(:,:)  ! (ipcomp,nbasis)
      real(8), intent(in) :: ztotss(:,:)  ! (ipcomp,nbasis)
      real(8), intent(in) :: omegws
      real(8), intent(in) :: excort(nspin)
      real(8), intent(in) :: qintex(nspin)
      real(8), intent(in) :: emtc  !  MT: ES energy due to rho_inter; ASA: rho_inter (MT) correction
      real(8), intent(in) :: emad
      real(8), intent(in) :: emadp
      real(8), intent(in) :: evalsum(:,:,:)  ! (ipcomp,ipsublat,ipspin)
      real(8), intent(in) :: ecorv(:,:,:)  ! (ipcomp,ipsublat,ipspin)
      real(8), intent(in) :: esemv(:,:,:)  ! (ipcomp,ipsublat,ipspin)
      real(8), intent(out) :: etot,press
      real(8), intent(in) :: Eccsum
      integer, intent(in) :: mtasa,iprint
      character(10), intent(in) ::  istop
!!PARAMETERS:
!!REVISION HISTORY:
! based on DDJ & WAS version (1993)
! Adapted - A.S. - 2013
!EOP
!
!BOC
!c
!CAB          nbasis -- number of inequivalent sublattices !
!c
      character  sname*10
      parameter (sname='tote1')
      real(8), parameter :: third=one/three
!c     convert pressure from Ry to Mbars
!      parameter (pfact=147.1d00)
!c
!      real(8), intent(in) :: v0imtz(:)
      real(8), pointer :: vrold_t(:,:,:,:) 
      real(8), pointer ::                                               &
     &                 vrold_core(:,:,:,:)                              &
     &               , vrold(:,:,:,:)  ! size(vrnew,1),size(atcon,1),nbasis,nspin)
!      logical :: no_vrold=.true.
      real*8 coren(size(atcon,1))
      real*8 valen(size(atcon,1))
      real*8 correc(size(atcon,1))
      real*8 cor3pv(size(atcon,1))
      real*8 pterm1(size(atcon,1))
      real*8 exchen(size(atcon,1))
      real*8 ezpt(size(atcon,1))
      real*8 tpzpt(size(atcon,1))
      real*8 excormt(size(atcon,1))
      real*8 e_per_atom
      real*8 three_pv
      real*8 effvol

      real(8) :: etotns(1:nbasis),pv3ns(1:nbasis),xcShCtot
      real(8) :: vx(size(vrnew,1),size(atcon,1),nspin)
      real(8) :: enxc(size(vrnew,1),size(atcon,1),nspin)

      logical :: read_ok=.false.
      integer, external :: gFreezeCore

      type(RunState), pointer :: M_S
      type(xc_mecca_pointer) :: libxc_p  
      integer :: ik,i0,nsub,jend,nsph,nintr,ikeep
      integer :: iperm(1:nbasis)
      real(8) :: vsort(1:nbasis)
      real(8) :: natom,vdif,etot0,pv3,fetot,nsites
      logical :: addMTC
      integer :: st_env
      character(11) :: mtc_txt=''
!      character(512), external :: descrXC
!      real(8), external :: alpha2
!c     ==================================================================

      M_S => getMS()
!......................................      
      vrold_t => null()
      vrold_core => vrnew
      vrold => vrnew
!......................................      
      if ( gFreezeCore().ne.0 ) then
        allocate(vrold_t(size(vrnew,1),size(atcon,1),nbasis,nspin))
        call readPotential(vrold_t,read_ok)
        if ( read_ok ) then
         vrold_core => vrold_t
        end if
      end if
!......................................      
!       
       natom = g_numNonESsites( M_S%intfile )
       if ( natom <= zero ) natom=one
       coren = zero
       valen = zero
       correc = zero
       cor3pv = zero
       pterm1 = zero
       exchen = zero
       ezpt = zero
       tpzpt = zero
       excormt = zero
       vdif = zero
!       if ( nspin > 1 ) then
!        vdif = v0imtz(nspin)-v0imtz(1)
!       end if
      call gXCmecca(iexch,libxc_p)   ! to get xc_mecca_pointer  
      do nsub = 1,nbasis
       etotns(nsub) = zero
       pv3ns(nsub) = zero
!       do is = 1,nspin
!        sp = 3 - 2*is
       do ik = 1,komp(nsub)
         jend = M_S%intfile%sublat(nsub)%compon(ik)%v_rho_box%ndchg
         nsph = jend
         i0 = nsph+1
         ikeep = 0
         nintr = 0
         if ( nint(ztotss(ik,nsub)) == 0 ) then
          ikeep = libxc_p%lda_only
          libxc_p%lda_only = 1
!DEBUG           call defXCforES
!
         end if 
         call xc_alpha(nspin,nsph,rr(1:nsph,ik,nsub)                    &
     &     ,rho(1:nsph,ik,nsub,1),rho(1:nsph,ik,nsub,nspin)             &
     &     ,enxc(1:nsph,ik,1),vx(1:nsph,ik,1),vx(1:nsph,ik,nspin)       &
     &     ,libxc_p)
         enxc(jend+1:,ik,1) = zero
         vx(jend+1:,ik,1:nspin) = zero
         if ( nspin>1 ) enxc(:,ik,2) = enxc(:,ik,1)
         if ( nint(ztotss(ik,nsub)) == 0 ) then
          libxc_p%lda_only = ikeep
!DEBUG           call undefXCforES
         end if 
       end do 
       jend = maxval(M_S%intfile%sublat(nsub)%compon(1:komp(nsub))%     &
     &               v_rho_box%ndrpts)

!       call totcore(nsub,vdif,vrold,vrnew,enxc,vx,                      &
!  
!  core electrons are calculated in "old" potential, i.e. 
!  mecca does not recalculate core-electrons in "vrnew"       
!  
       call totcore(nsub,vdif,vrold_core,vrold_core,enxc,vx,corden,     &
     &                xr(1:jend,1:komp(nsub),nsub),                     &
     &                rr(1:jend,1:komp(nsub),nsub),                     &
     &                jmt(1:komp(nsub),nsub),nspin,                     &
     &                komp(nsub),atcon(1:komp(nsub),nsub),              &
     &                ecorv,esemv,                                      &
     &                etot0,pv3,iprint,istop)
       etotns(nsub) = etotns(nsub) + etot0
       pv3ns(nsub)  = pv3ns(nsub)  + pv3
       call totvale(nsub,vdif,vrold,vrnew,enxc,vx,rho,corden,           &
     &                xr(1:jend,1:komp(nsub),nsub),                     &
     &                rr(1:jend,1:komp(nsub),nsub),                     &
     &                jmt(1:komp(nsub),nsub),nspin,                     &
     &                komp(nsub),atcon(1:komp(nsub),nsub),              &
     &                evalsum,                                          &
     &                etot0,pv3,iprint,istop)
       etotns(nsub) = etotns(nsub) + etot0
       pv3ns(nsub)  = pv3ns(nsub)  + pv3
!          call varEnCorr(komp(nsub),jmt(:,nsub),xr(:,1:komp(nsub),nsub),&
!                         vrold(:,1:komp(nsub),nsub,is),                 &
!     &                   vrnew(:,1:komp(nsub),nsub,is),                 &
!     &                   rhov(:,1:komp(nsub)),                          &
!     &                   corden(:,1:komp(nsub),nsub,is),                &
!     &                   correc(ik),cor3pv(ik))
!!c     ==================================================================
!!          if( mtasa .le. 0 .and. komp(nsub) .gt. 1 ) then
!!            write(6,'('' NEED INTERSTITAL MT COULOMB FIX a la DDJ '')')
!! TODO??: correction for interstitial rho of each species for MT-case                         
!!          endif
!!c     ==================================================================
!!c     calculate the zeropoint energy....................................
!!c     ==================================================================
       effvol=omegws/natom
       if ( ipzpt==1 ) then
          call zeropt(ezpt,tpzpt,effvol,ztotss(1:komp(nsub),nsub),      &
     &                                       komp(nsub))
       end if
       if(iprint.ge.-1) then
         write(6,'(''     ***********************************'',        &
     &               ''**********************************'')')
         if(maxval(abs(ezpt(1:komp(nsub)))).lt.1.d-5) then
          write(6,'(''     No zero-point corrections'')')
         else
          do ik=1,komp(nsub)
           write(6,'(''       ik='',i3,'' ezpt ='',f13.5,               &
     &                               '' tpzpt ='',f13.5)')              &
     &                                ik,ezpt(ik),                      &
     &                                  tpzpt(ik)
          enddo
         end if
       end if
       do ik=1,komp(nsub)
         etotns(nsub) = etotns(nsub) + atcon(ik,nsub)*ezpt(ik)
         pv3ns(nsub)  = pv3ns(nsub)  + atcon(ik,nsub)*tpzpt(ik)
       end do
      end do 

      if( iprint .gt.-10 ) then
       write(6,'(''     ***********************************'',          &
     &           ''**********************************''/)')
       do nsub = 1,nbasis
        write(6,'(''     Sub-lat'',i3,                                  &
     &              '' total energy:'',f14.6 )')                        &
     &                    nsub,etotns(nsub)
        write(6,*)
       end do
      end if

      etot=zero
      three_pv=zero

      etot = etot + sum(qintex(1:nspin))
      three_pv = three_pv + sum(excort(1:nspin))
      etot = etot + Eccsum
      etot = etot + emad
      three_pv = three_pv + emadp
      addMTC = mtasa==0 .or. emtc.ne.zero
      call gEnvVar('ENV_MTC',.false.,st_env)
      if ( st_env==1 ) addMTC=.true.
      if ( st_env==0 ) addMTC=.false.
      if ( addMTC ) then
        etot = etot + emtc
        three_pv = three_pv + dble(5)/dble(3)*emtc
        if ( st_env==1 .or. (mtasa==1 .and. emtc.ne.zero) )             &
     &             mtc_txt=' (with MTC)'
      end if

      nsites = sum(numbsub(1:nbasis))
      etot = etot/nsites

      vsort(1:nbasis)=abs(etotns(1:nbasis))*(numbsub(1:nbasis)/nsites)
      call dpsort(vsort,nbasis,iperm,1,ik)
      if ( ik.ne.0 ) then
       write(*,*) '  DPSORT ERROR=',ik
       call fstop(sname//' UNEXPECTED ERROR IN DPSORT')
      end if

      do nsub=1,nbasis
       etot = etot + etotns(iperm(nsub))*(numbsub(iperm(nsub))/nsites)
       three_pv = three_pv + pv3ns(iperm(nsub))*numbsub(iperm(nsub))
      end do
      etot = etot * nsites
      press = three_pv*third*pfact/omegws
      e_per_atom = etot/natom
!
!c     ******************************************************************
      if( iprint .gt.-10 ) then
!c     Major print out...................................................
       write(6,'(''     ***********************************'',          &
     &          ''**********************************'')')
       write(6,'(''     Total Energy = '',f17.8,                        &
     &          ''     3PV (Ry) = '',f17.8,a)') etot,three_pv           &
     &                                         ,trim(mtc_txt)
       write(6,'(''     ***********************************'',          &
     &          ''**********************************'')')
       write(6,'(''     Energy/atom  = '',f17.8,                        &
     &          ''     P (Mbars)= '',f17.8,a)')e_per_atom,press         &
     &                                         ,trim(mtc_txt)
       write(6,'(''     ***********************************'',          &
     &          ''**********************************'')')
       if ( M_S%intfile%Tempr > zero ) then
        fetot = etot - M_S%intfile%Tempr*M_S%work_box%entropy
        if ( M_S%intfile%Tempr < 1000.d0/ry2kelvin ) then
         etot0 = etot - M_S%intfile%Tempr*M_S%work_box%entropy/2   
         write(6,'('' T=0 Total Energy = '',f17.8,                      &
     &          '' Etot/atom(Ry)= '',f17.8)') etot0,etot0/natom
         write(6,'(''     ***********************************'',        &
     &          ''**********************************'')')
        end if 
        write(6,'(''     Free Energy  = '',f17.8,                       &
     &          '' F.E./atom(Ry)= '',f17.8)') fetot,fetot/natom
        write(6,'(''     ***********************************'',         &
     &          ''**********************************'')')
       end if   
       if(abs(emad).gt.1.d-6.or.nbasis.gt.1.or.mtasa.eq.0) then
        write(6,'(5x,''PBC     contribution         (Ry) ='',3f11.6)')  &
     &      emad,emad/natom,emadp
       end if
       if(iprint.ge.0.and.maxval(komp(1:nbasis)).gt.1) then
        write(6,'(5x,''Eccsum                       (Ry) ='',2f11.6)')  &
     &      Eccsum,Eccsum/natom
       endif
       if(abs(emtc).gt.1.d-6) then
        write(6,'(5x,''Interstitial Coulomb         (Ry) ='',2f11.6)')  &
     &       emtc,emtc/natom
!     &       UcShCtot,UcShCtot/natom
       end if
       xcShCtot = sum(qintex(1:nspin))
       if (abs(xcShCtot)>1.d-6) then
        write(6,'(5x,''Interstitial Exch-Correl.    (Ry) ='',2f11.6)')  &
     &      xcShCtot,xcShCtot/natom
       end if
      endif
      nullify(vrold)
      if ( associated(vrold_t) ) then
       deallocate(vrold_t)
       nullify(vrold_t)
      end if
      call gXCmecca(-1,libxc_p)   ! to release xc_mecca_pointer  
!c     ******************************************************************
      if( istop .eq. sname ) then
       call fstop(sname)
      endif
!c     ******************************************************************
      return

!EOC
      contains

!BOP
!!IROUTINE: totvale
!!INTERFACE:
      subroutine totvale(nsub,vdif,vrold,vrnew,enxc,vx,                 &
     &                rho,corden,xr,rr,                                 &
     &                imt,nspin,                                        &
     &                komp,atcon,                                       &
     &                evalsum,                                          &
     &                etot,pv3,iprint,istop)
!!DESCRIPTION:
! calculate cotribution from valence electrons
!
!!REMARKS:
! private procedure of tote1 subroutine
!EOP
!
!BOC
      implicit none
!c
!c     ==================================================================
!c   total valence energy for multi-sublattice and multi-component KKR-CPA
!c   within ASA and MT, for both non- and  spin-polarized cases.
!c   written for ASA and spin-polarization     by WAS & DDJ Sept 1993
!c   modified                                        by CAB  Jan 2003
!c     ==================================================================
!c
!c     routine calls:  derv5    5 point derivative routine
!c                     qexpup   an integration algorithm
!c                     fstop    to stop in this routine
!c
!c     ==================================================================
!c
!c ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!c
      integer, intent(in) :: nsub
      real(8), intent(in) :: vdif
      real(8), intent(in) :: vrold(:,:,:,:)  ! (iprpts,ipcomp,ipsublat,nspin)
      real(8), intent(in) :: vrnew(:,:,:,:)  ! (iprpts,ipcomp,ipsublat,nspin)
      real(8), intent(in) :: enxc(:,:,:)     ! (iprpts,ipcomp,nspin)
      real(8), intent(in) :: vx(:,:,:)       ! (iprpts,ipcomp,nspin)
      real(8), intent(in) :: rho(:,:,:,:)    ! (iprpts,ipcomp,ipsublat,nspin)
      real(8), intent(in) :: corden(:,:,:,:) ! (iprpts,ipcomp,ipsublat,nspin)
      real(8), intent(in) :: xr(:,:)         ! (iprpt,ipcomp)
      real(8), intent(in) :: rr(:,:)
      integer, intent(in) :: imt(komp)
      integer, intent(in) :: nspin
      integer, intent(in) :: komp
      real(8), intent(in) :: atcon(komp)
      real(8), intent(in) :: evalsum(:,:,:)  ! (ipcomp,ipsublat,nspin)
      real(8), intent(out) :: etot,pv3
      integer, intent(in) :: iprint
      character(10), intent(in) :: istop
!c
      real*8 rhov(size(vrnew,1))
      real*8 dvrdr(size(vrnew,1))
      real*8 bndint(size(vrnew,1))
      real*8 bnd(size(vrnew,1))
      real*8 delta_v(size(vrnew,1))
      real*8 valen(komp)
      real*8 correc(komp)
      real*8 cor3pv(komp)
      real*8 pterm1(komp)
      real*8 exchen(komp)
      real*8 pxchen(komp)
      real*8 excormt(komp)
      real*8 qint,qsemcout
      real*8 etoti,pv3i

      integer is,ik,jmt

       integer jend
       real(8) :: tot_v,tot_a,sph_s,tot_sc
!c
!c parameter
      real(8), parameter :: third=one/three
      character(10), parameter :: sname='totvale'
!c     ==================================================================

       etot = zero
       pv3   = zero
       correc = zero
       cor3pv = zero

      do is = 1,nspin
       etoti = zero
       pv3i   = zero
!c
      do ik = 1,komp
       jmt = imt(ik)
       if ( M_S%intfile%intgrsch>1 ) then
         jend = M_S%intfile%sublat(nsub)%compon(ik)%v_rho_box%ndchg
       else 
         jend = jmt
       end if
       rhov(1:jend) =                                                   &
     &     rho(1:jend,ik,nsub,is) - corden(1:jend,ik,nsub,is)

!       if(jws.gt.jmt.and.nspin.eq.2) then
!        call qexpup(1,rhov,jws,xr(:,ik),bnd)
!        qint = bnd(jws) - bnd(jmt)
!       else
        qint = zero
!       end if

!c     ==================================================================
!c     calculate:  -int 4pi rhov*d(rv)/dr r2 dr .........................
!c
       call derv5(vrnew(:,ik,nsub,is),dvrdr,rr(:,ik),jend)
!DEBUG       call derv_expmesh(vrnew(:,ik,nsub,is),dvrdr,rr(:,ik),jend)

       bndint(1:jmt)=rhov(1:jmt)*dvrdr(1:jmt)

       call qexpup(1,bndint,jmt,xr(:,ik),bnd)
       valen(ik) =  -bnd(jmt)                                           &
     &               -bndint(1)*rr(1,ik)*third

         if(nspin.eq.2)                                                 &
     &     valen(ik) = valen(ik) - qint*vdif

!c
!c     ==================================================================
!c     pressure: calculate -int 4pi rhovalen d(r2v)/dr r dr ............. !..
!c
       bndint(1:jmt) = bndint(1:jmt) +                                  &
     &                         rhov(1:jmt)*                             &
     &                 vrnew(1:jmt,ik,nsub,is)/rr(1:jmt,ik)
       call qexpup(1,bndint,jmt,xr(:,ik),bnd)
       pterm1(ik) = -bnd(jmt)-bndint(1)*rr(1,ik)/two

!c     ==================================================================
!c     calculate terms that make the energy variational..................
!c                     and corrections to the pressure...................
!c
       delta_v(1:jmt)=vrold(1:jmt,ik,nsub,is)-vrnew(1:jmt,ik,nsub,is)
       if ( maxval(abs(delta_v(1:jmt))) > 0.01d0*etol ) then
         bndint(1:jmt) = rhov(1:jmt)*delta_v(1:jmt)/rr(1:jmt,ik)
         call qexpup(1,bndint,jmt,xr(:,ik),bnd)
         correc(ik)=-bnd(jmt) - bndint(1)*rr(1,ik)*third
       end if
!C***********************************************************************
!C  Exchange-Correlation Contribution
!C***********************************************************************

!c
!c   Surface contribution (due to discontinuity on the boundary)
!c
       pxchen(ik)=-rr(jmt,ik)*rhov(jmt)*                                &
     &                (enxc(jmt,ik,is)-vx(jmt,ik,is))
       call derv5(vx(:,ik,is),dvrdr,rr(:,ik),jend)    ! if jend>jmt, derivative may have better accuracy at jmt
!DEBUG       call derv_expmesh(vx(:,ik,is),dvrdr,rr(:,ik),jend)
       bndint(1:jmt) = rhov(1:jmt)*                                     &
     &            ( enxc(1:jmt,ik,is) + rr(1:jmt,ik)*dvrdr(1:jmt) )
       call asa_integral(M_S,ik,nsub,bndint(1:jmt),exchen(ik),sph_s)

       if ( M_S%intfile%mtasa>1 ) then       
!           
! XC correction should be consistent with normalization of core electrons,
! if core electrons are ASA-normalized, then no VP-contribution from core.           
!           
         bndint(1:jend) = rho(1:jend,ik,nsub,is)*enxc(1:jend,ik,is)
!         bndint(1:jend) = rhov(1:jend)*enxc(1:jend,ik,is)
         call ws_integral(M_S,ik,nsub,bndint(1:jend),tot_v,sph_s)
         call asa_integral(M_S,ik,nsub,bndint(1:jend),tot_a,sph_s)
         excormt(ik)= tot_v-tot_a                     ! VP-ASA correction
!DEBUG         
!DEBUG         call ws_integral(M_S,ik,nsub,rho(1:jend,ik,nsub,is),           &
!DEBUG     &                                                    tot_v,sph_s)
!DEBUG         call asa_integral(M_S,ik,nsub,rho(1:jend,ik,nsub,is),          &
!DEBUG     &                                                    tot_a,sph_s)
!DEBUG         correc(ik) = correc(ik)+(tot_a-tot_v)*M_S%work_box%vmtz(is)
!DEBUG         cor3pv(ik) = cor3pv(ik)+two*(tot_a-tot_v)*M_S%work_box%vmtz(is)  !?
       else  
         excormt(ik) = zero
       end if
!? contribution due to semicore outside sphere       
!       if ( gNonMT() ) then 
!        qsemcout = g_zsemcout(M_S%intfile,.false.)
!        correc(ik) = correc(ik) - qsub*qsemcout/rr(jmt,ik)
!       end if
!       
       cor3pv(ik) = two*correc(ik)
      enddo ! ik-cycle

      do ik=1,komp
       etoti = etoti +                                                  &
     &          atcon(ik)*(                                             &
     &                      valen(ik)                                   &
     &                    + evalsum(ik,nsub,is)                         &
     &                    + exchen(ik)                                  &
     &                    + excormt(ik)                                 &
     &                    + correc(ik)                                  &
     &                     )
!c     ==================================================================
       pv3i = pv3i +                                                    &
     &         atcon(ik)*(                                              &
     &                     pterm1(ik)                                   &
     &                   + two*evalsum(ik,nsub,is)                      &
     &                   + pxchen(ik)                                   &
     &                   + cor3pv(ik)                                   &
     &                          )
      enddo
!c
!c     ******************************************************************
      if( iprint.ge.-1 ) then
       write(6,'(''     ***********************************'',          &
     &               ''**********************************'')')
       write(6,'(''     Spin'',i3,'' Sub-lat '',i3,                     &
!     &              '' TotValenEn='',f14.6,'' Valen-3PV='',f13.5        &
     &              '' TotValenEn='',f14.6,''  3PV='',f13.5             &
     &            )')                                                   &
     &                 is,nsub,etoti,pv3i
       if( iprint.ge.0 ) then
        write(6,'(''     ==================================='',         &
     &               ''=================================='')')
        do ik=1,komp
         write(6,'(''       Component'',t40,i3)') ik
         write(6,'(''         valen ='',f13.5,'' exchen ='',f13.5,      &
     &               '' evals ='',f13.5)') valen(ik),                   &
     &                            exchen(ik),evalsum(ik,nsub,is)
         write(6,'(''        correc ='',f13.5,                          &
     &                     '' exc-corr ='',f13.5)')                     &
     &                                   correc(ik),excormt(ik)
         write(6,'(''        ptrm1v ='',f13.5,'' pxchen ='',f13.5,      &
     &               ''  c3pv ='',f13.5)') pterm1(ik),pxchen(ik),       &
     &                                   cor3pv(ik)
          enddo
       end if
      end if
!c
!c     ******************************************************************
      etot = etot + etoti
      pv3  = pv3  + pv3i
       enddo                             ! end do loop over spin
!c     ******************************************************************
!c
      if( istop .eq. sname ) then
       call p_fstop(sname)
      endif

      return
!EOC
      end subroutine totvale
!
!BOP
!!IROUTINE: totcore
!!INTERFACE:
      subroutine totcore(nsub,vdif,vrold,vrnew,enxc,vx,                 &
     &                corden,xr,rr,                                     &
     &                imt,nspin,                                        &
     &                komp,atcon,                                       &
     &                ecorv,esemv,                                      &
     &                etot,pv3,iprint,istop)
!!DESCRIPTION:
! calculates contribution from core electrons
!
!!REMARKS:
! private procedure of tote1 subroutine
!EOP
!
!BOC
      implicit none
!c
!c     ==================================================================
!c     total core energy for multi-sublattice and multi-component KKR-CPA
!c     within ASA and MT, for both non- and  spin-polarized cases.
!c     written for ASA and spin-polarization     by WAS & DDJ Sept 1993
!c     modified                                        by CAB  Jan 2003
!c     ==================================================================
!c
!c     routine calls:  derv5    5 point derivative routine
!c                     qexpup   an integration algorithm
!c                     fstop    to stop in this routine
!c
!c     ==================================================================
!c
!c
      integer, intent(in) :: nsub
      real(8), intent(in) :: vdif
      real(8), intent(in) :: vrold(:,:,:,:)  ! (iprpts,ipcomp,ipsublat,nspin)
      real(8), intent(in) :: vrnew(:,:,:,:)  ! (iprpts,ipcomp,ipsublat,nspin)
      real(8), intent(in) :: enxc(:,:,:)     ! (iprpts,ipcomp,nspin)
      real(8), intent(in) :: vx(:,:,:)       ! (iprpts,ipcomp,nspin)
      real(8), intent(in) :: corden(:,:,:,:) ! (iprpts,ipcomp,ipsublat,nspin)
      real(8), intent(in) :: xr(:,:)         ! (iprpt,ipcomp)
      real(8), intent(in) :: rr(:,:)
      integer, intent(in) :: imt(komp)
      integer, intent(in) :: nspin
      integer, intent(in) :: komp
      real(8), intent(in) :: atcon(komp)
      real(8), intent(in) :: ecorv(:,:,:)  ! (ipcomp,ipsublat,nspin)
      real(8), intent(in) :: esemv(:,:,:)  ! (ipcomp,ipsublat,nspin)
      real(8), intent(out) :: etot,pv3
      integer, intent(in) :: iprint
      character(10), intent(in) :: istop
!c
      real*8  qint,qsemcout
      real*8 rhoc(size(vrnew,1))
      real*8 delta_v(size(vrnew,1))
      real*8 dvrdr(size(vrnew,1))
      real*8 bndint(size(vrnew,1))
      real*8 bnd(size(vrnew,1))
      real*8 coren(komp)
      real*8 correc(komp)
      real*8 cor3pv(komp)
      real*8 pterm1(komp)
      real*8 exchen(komp)
      real*8 pxchen(komp)
      real*8 excormt(komp)
      real*8 etoti,pv3i,sp
      integer is,ik,jmt

      logical, external :: gNonMT
!c
!c parameter
      real(8), parameter :: third=one/three
      character(10), parameter  :: sname='totcore'
!c     ==================================================================

      etot = zero
      pv3   = zero
      if( maxval(abs(ecorv(1:komp,nsub,1:nspin) +                       &
     &           esemv(1:komp,nsub,1:nspin)) ) < epsilon(one) )  RETURN

      sp = 1
      if ( nspin == 1 ) sp = 2
      
      pterm1(1:komp) = zero
      correc(1:komp) = zero
      cor3pv(1:komp) = zero
      do is = 1,nspin
       etoti = zero
       pv3i   = zero
!c
      do ik = 1,komp
       jmt = imt(ik)
!c     ==================================================================
!c     calculate d(vr)/dr per for each component
!c
       call derv5(vrnew(:,ik,nsub,is),dvrdr,rr(:,ik),jmt)

!C***********************************************************************
!C  Core electrons: core charge density is assumed to be zero outside ASA sphere
!C***********************************************************************
       rhoc(1:jmt) =                                                    &
     &     corden(1:jmt,ik,nsub,is)

!       if(jws.gt.jmt.and.nspin.eq.2) then
!        call qexpup(1,rhoc,jws,xr(:,ik),bnd)
!        qint = bnd(jws) - bnd(jmt)
!       else
        qint = zero
!       end if

!c     ==================================================================
!c     calculate:  -int 4pi rhoc*d(rv)/dr r2 dr .........................
!c
       bndint(1:jmt)=rhoc(1:jmt)*dvrdr(1:jmt)
       call qexpup(1,bndint,jmt,xr(:,ik),bnd)
       coren(ik) =    sp*(ecorv(ik,nsub,is)+esemv(ik,nsub,is)) -        &
     &                bnd(jmt)                                          &
     &               -bndint(1)*rr(1,ik)*third
!c
!c     ==================================================================
!c     pressure: calculate -int 4pi corden d(r2v)/dr r dr ...............
!c          (it seems to be incorrect in s.-relativistic case)       
!c
!      1/r*d(r^2*v)/dr = 1/r*d(r*rv)/dr = rv/r + d(rv)/dr
!       
! It does not work for core electrons
!
!       bndint(1:jmt) = bndint(1:jmt) +                                  &
!     &                 rhoc(1:jmt)*vrnew(1:jmt,ik,nsub,is)/rr(1:jmt,ik)
!       call qexpup(1,bndint,jmt,xr(:,ik),bnd)
!       pterm1(ik) = -bnd(jmt)-bndint(1)*rr(1,ik)/two

       if(nspin.eq.2)                                                   &
     &     coren(ik) =  coren(ik) - qint*vdif

!c     ==================================================================
!c     calculate terms that make the energy variational..................
!c                     and corrections to the pressure...................
!c
       delta_v(1:jmt)=vrold(1:jmt,ik,nsub,is)-vrnew(1:jmt,ik,nsub,is)
       if ( maxval(abs(delta_v(1:jmt))) > 0.01d0*etol ) then
         bndint(1:jmt) = rhoc(1:jmt)*delta_v(1:jmt)/rr(1:jmt,ik)
         call qexpup(1,bndint,jmt,xr(:,ik),bnd)
         correc(ik)=-bnd(jmt) - bndint(1)*rr(1,ik)*third
       end if
!C***********************************************************************
!C  Exchange-Correlation Contribution
!C***********************************************************************
!       rhoc(1:jmt) =                                                    &
!     &   + corden(1:jmt,ik,nsub,is)
       call derv5(vx(:,ik,is),dvrdr,rr(:,ik),jmt)
       bndint(1:jmt) = rhoc(1:jmt)*                                     &
     &            ( enxc(1:jmt,ik,is) + rr(1:jmt,ik)*dvrdr(1:jmt) )
       call qexpup(1,bndint,jmt,xr(:,ik),bnd)
       exchen(ik) = bnd(jmt) + bndint(1)*rr(1,ik)*third
!c
!c   Surface contribution (due to discontinuity on the boundary)
!c
       if ( gNonMT() ) then 
        excormt(ik)=zero
       else
        excormt(ik)=-rr(jmt,ik)*rhoc(jmt)*                              &
     &                (enxc(jmt,ik,is)-vx(jmt,ik,is))
       end if 
! contribution due to semicore outside sphere       
!       if ( gNonMT() ) then 
!        qsemcout = g_zsemcout(M_S%intfile,.false.)
!        excormt(ik)=excormt(ik)+qsemcout*(enxc(jmt,ik,is)-vx(jmt,ik,is))
!        correc(ik) = correc(ik) - qsemcout*qsemcout/rr(jmt,ik)
!       end if
!
       cor3pv(ik) = two*correc(ik)
       pxchen(ik) = excormt(ik)

!DEBUGPRINT       
!       bndint(1:jmt) = 3*rhoc(1:jmt)*                                     &
!     &            ( enxc(1:jmt,ik,is)-vx(:,ik,is) )
!       call qexpup(1,bndint,jmt,xr(:,ik),bnd)
!!       do i=1,jmt
!!        write(90+nsub,'(i5,4e18.7)') i,rr(i,ik),rhoc(i),bndint(i),bnd(i)
!!       end do
!       write(6,'('' DEBUG is,ik='',2i2,''  exc-vxc='',3e17.7)')         &
!     &           is,ik,-bnd(jmt)
!       bndint(1:jmt) = rhoc(1:jmt)*                                     &
!     &            ( rr(1:jmt,ik)*dvrdr(1:jmt) )
!       call qexpup(1,bndint,jmt,xr(:,ik),bnd)
!!       do i=1,jmt
!!        write(80+nsub,'(i5,4e18.7)') i,rr(i,ik),rhoc(i),bndint(i),bnd(i)
!!       end do
!       write(6,'('' DEBUG is,ik='',2i2,'' r*d(vxc)/dr='',3e17.7)')      &
!     &           is,ik,-bnd(jmt)
!DEBUGPRINT       

      enddo

      do ik=1,komp
       etoti = etoti +                                                  &
     &          atcon(ik)*(                                             &
     &                      coren(ik)                                   &
     &                    + exchen(ik)                                  &
     &                    + excormt(ik)                                 &
     &                    + correc(ik)                                  &
     &                     )
!c     ==================================================================
       pv3i = pv3i +                                                    &
     &         atcon(ik)*(                                              &
     &                     pterm1(ik)                                   &
     &                   + pxchen(ik)                                   &
     &                   + cor3pv(ik)                                   &
     &                          )
      enddo
!c
!c     ******************************************************************
      if( iprint.ge.-1 ) then
       write(6,'(''     ***********************************'',          &
     &               ''**********************************'')')
!       write(6,'(''     Spin'',i3,'' Sub-lat '',i3,                     &
!     &              '' TotCoreEn='',f14.6,'' Core-3PV='',f13.5          &
!     &            )')                                                   &
!     &                 is,nsub,etoti,pv3i
       write(6,'(''     Spin'',i3,'' Sub-lat '',i3,                     &
     &              '' TotCoreEn='',f14.6                               &
     &            )')                                                   &
     &                 is,nsub,etoti
      end if
      if( iprint.ge.0 ) then
        write(6,'(''     ==================================='',         &
     &               ''=================================='')')
        do ik=1,komp
         write(6,'(''       Component'',t40,i3)') ik
         write(6,'(''         ecorv ='',f13.5,'' esemv ='',f13.5,       &
     &               '' ecsum ='',f13.5)') ecorv(ik,nsub,is)*sp         &
     &                                    ,esemv(ik,nsub,is)*sp         &
     &                     ,(ecorv(ik,nsub,is)+esemv(ik,nsub,is))*sp
         write(6,'(''        ecsum-rhodvrdr='',f13.5,13x,'' exchen ='', &
     &                            f13.5)') coren(ik),exchen(ik)
         write(6,'(''        correc ='',f13.5,                          &
     &                     '' exc-mt ='',f13.5)')                       &
     &                                   correc(ik),excormt(ik)
         write(6,'(''        ptrm1c ='',f13.5,'' pxchen  ='',f13.5,     &
     &               '' c3pv ='',f13.5)') pterm1(ik),pxchen(ik),        &
     &                                   cor3pv(ik)
        enddo
      end if
!c
!c     ******************************************************************
      etot = etot + etoti
      pv3  = pv3  + pv3i
       enddo                             ! end do loop over spin
!c     ******************************************************************

!       if ( minval(esemv(1:komp,nsub,1:nspin)) < zero ) then
!        if ( iprint>=0 ) then
!         write(6,'(/''     WARNING: pressure contribution from core elec&
!     &trons is ignored''/)') 
!        end if
!       end if

!        pv3 = zero

! zero-pressure is assumed for core states, i.e. integration ASA sphere should 
! be big enough; if it is not the case and there are electrons outside ASA sphere, 
! then they are ! treated as const background (in the whole space) and produce additional 
! pressure (if it is not corrected in valence part).
! 
!c
      if( istop .eq. sname ) then
       call p_fstop(sname)
      endif
!c     ******************************************************************
      return
!EOC
      end subroutine totcore
!c
      end subroutine tote1
!c     cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc

