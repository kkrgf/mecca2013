!BOP
!!ROUTINE: grpkpnt
!!INTERFACE:
       subroutine grpkpnt(nkpt,wghtq,lrot,                              &
     &                    ngrp,kptgrp,kptset,kptindx)
!!DESCRIPTION:
! find groups of k-points with the same set of group operations
! {\bv
! input:
!       nkpt -- number of k-points in the set
!       wghtq(i) -- weight of i-th k-point
!       lrot(*)  -- list of group operations for each k-point
!
! output:
!        ngrp -- number of k-points groups with the same set of
!                group operations    [always .le.48]
!        kptgrp(i+1)-kptgrp(i) -- number of k-points in i-th group
!                                 [kptgrp(1) = 1], i=1,ngrp+1
!        kptset(i) -- pointer to lrot for i-th group
!        kptindx(*) -- pointer to k-point list:
!          number_of_k-point = kptindx(j), j=kptgrp(i)..kptgrp(i+1)-1
! \ev}

!!DO_NOT_PRINT
       implicit none
!!DO_NOT_PRINT

!!ARGUMENTS:
       integer nkpt
       integer wghtq(nkpt)
       integer lrot(*)
       integer ngrp,kptgrp(*),kptset(*)
       integer kptindx(nkpt)
!EOP
!
!BOC
       integer, allocatable :: indxchg(:)

       integer i,j,nrot,nirot,njrot,ikgrp
       logical compset

       character*10 sname
       parameter (sname='grpkpnt')

       if(nkpt.le.0) return

       allocate(indxchg(1:nkpt))

       indxchg = 0

       nirot = 0
       ngrp = 0
       ikgrp = 0
       kptgrp(ngrp+1) = ikgrp + 1

       do i=1,nkpt

        nrot = wghtq(i)

        if( indxchg(i).eq.0 ) then

         ngrp = ngrp+1
         kptset(ngrp) = nirot+1
         ikgrp = ikgrp+1

         kptindx(ikgrp) = i
         indxchg(i) = ngrp

         njrot = nirot + nrot

         do j=i+1,nkpt

          if(indxchg(j).eq.0.and.wghtq(j).eq.nrot) then

           if( compset(nrot,lrot(nirot+1),lrot(njrot+1)) ) then
            ikgrp = ikgrp+1
            kptindx(ikgrp) = j
            indxchg(j) = ngrp
           end if

          end if

          njrot = njrot + wghtq(j)

         end do

         kptgrp(ngrp+1) = ikgrp + 1

        end if

        nirot = nirot + nrot

       end do

       deallocate(indxchg)

       if(ikgrp.ne.nkpt) then
        write(6,*) ' IKGRP=',ikgrp
        write(6,*) ' NKPT=',nkpt
        call fstop(sname//':: IKGRP.NE.NKPT ???')
       end if

       return
       end subroutine grpkpnt

!BOP
!!ROUTINE: compset
!!INTERFACE:
       logical function compset(nrot,lrot1,lrot2)
!!DESCRIPTION:
! returns result of comparison of integer arrays {\tt lrot1} and {\tt lrot2}
! (.true. only if all elements are the same)
!
!EOP
       implicit none
       integer, intent(in) :: nrot
       integer, intent(in) :: lrot1(nrot),lrot2(nrot)
       integer i

       compset = .TRUE.
       do i=1,nrot
        if(lrot1(i).ne.lrot2(i)) then
         compset = .FALSE.
         return
        end if
       end do

       return
       end function compset
