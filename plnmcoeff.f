!BOP
!!ROUTINE: plnmcoeff
!!INTERFACE:
      subroutine plnmcoeff(ndata,x,y,iflag,norder,coeff)
!!DESCRIPTION:
! {\bv
! generates coefficients to interpolate y(x) between x(1) and x(ndata) 
! with a function  (if iflag .NE. 0 )
!   rho(r) = sum{n=0,norder| coeff(n)*r**n }, r is between x(1) and x(ndata)
!            OR    (if iflag .EQ. 0 )
!   rho(r) = sum{n=0,norder| coeff(n)*T_n(z) }, z is between x(1) and x(ndata)
!   here T_n(x) is Chebyshev's Poly of order n; x = (z-x0)/w (see code for x0, w)
!     (use CHEBEV function to calculate rho(r))
! \ev}

!!DO_NOT_PRINT
      implicit none
!!DO_NOT_PRINT

!!ARGUMENTS:
      integer, intent(in) :: ndata
      real(8), intent(in) :: x(ndata),y(ndata)
      integer, intent(in) :: iflag
      integer, intent(in) :: norder
      real(8), intent(out) ::  coeff(norder+1)
!!REVISION HISTORY:
! Initial Version - A.S. - 2003
!EOP
!
!BOC

      real(8) :: bch(norder+1)    !  working array
      real(8) :: x0,w,hik,rik,rhoik,summ
      integer i,ik,iex,Nplnm
      real(8), parameter :: pi=3.1415926535897932384d0

      real(8), external :: ylag
      integer, parameter :: nlag=3

      x0 = 0.5d0*(x(ndata)+x(1))
      w  = 0.5d0*(x(ndata)-x(1))
      Nplnm = min(norder,ndata-1)

      do i=1,Nplnm+1
       summ = 0
       do ik=1,Nplnm+1
        hik = pi*(ik-0.5d0)/(Nplnm+1)
        rik = x0 + w*cos(hik)
        rhoik = ylag(rik,x(1),y(1),0,nlag,ndata,iex)
        summ = summ + rhoik*cos(hik*(i-1))
       end do
       bch(i) = summ*2.d0/(Nplnm+1)
      end do
!c
!c      rho(r) = -bch(1)/2+sum{1,norder|bch(i)*T_{i-1}(x)},
!c         here r = x0 + w*x
!c
!c      b0,b1,b2,... and A0,A1,A2,... -- polinom.coeff., i.e.
!c
!c        rho(r) = ( b0*T0 + b1*T1 + b2*T2 + ...) =   ! T: ChebyshevPlnm
!c               = ( A0 + A1*r + A2*r*r + ...)
!c
!DEBUGPRINT      
!           write(6,'(a,(20g14.5))') ' Bch=',bch(1:norder+1)
      if ( iflag .ne. 0 ) then     
       call chebpc(bch,Nplnm+1,x(1),x(ndata),coeff)
!DEBUGPRINT      
!           write(6,'(a,(20g14.5))') ' Coeff=',coeff(1:norder+1)
!DEBUGPRINT      
      else
       coeff = bch
      end if 
      coeff(Nplnm+2:norder+1) = 0
!CDEBUG
!CDEBUG      w = 0.d0
!CDEBUG      summ = 0.d0
!CDEBUG      do i=1,ndata
!CDEBUG       x0 = x(i)
!CDEBUG       rhoik = 0.d0
!CDEBUG       do ik=1,norder+1
!CDEBUG        rhoik = rhoik + coeff(ik)*x0**(ik-1)
!CDEBUG       end do
!CDEBUG       w = (rhoik-y(i))**2 +w
!CDEBUG       summ = summ+max(1.d-32,y(i)**2)
!CDEBUG      enddo
!CDEBUG      write(*,*) ' error=',sqrt(w/ndata),sqrt(w/summ)
!CDEBUG      write(*,*) ' Coeff=',coeff(1:norder+1)
!CDEBUG
      return
!EOC
      end subroutine plnmcoeff

!BOP
!!ROUTINE: chebpc
!!INTERFACE:
      subroutine chebpc(rc,n,a,b,rd)
!!DESCRIPTION:
! {\bv
! transforms Chebyshev coefficients into actual
! polynomial coefficients (!!! x from [-1,1] !!!);
! if input a .ne. b, then polynomial coefficients 
! are shifted, i.e. coefficients are for a < x < b
! based on Numerical Recipes 2nd Edition, Section 5.10
! \ev}

!!DO_NOT_PRINT
      implicit none
!!DO_NOT_PRINT

!!ARGUMENTS:
      integer n
      real(8), intent(in) :: rc(n)
      real(8), intent(in) :: a,b
      real(8), intent(out) :: rd(n)
!!REVISION HISTORY:
! Initial Version - A.S. - 2003
!!REMARKS:
!EOP
!
!BOC
      real(16) :: dd(n), d(n),c(n)
      real(16) :: sv
      real(16) :: fac
      real(8)  :: const
      integer j,k

      real(16), parameter :: epszero=1.e-8
      integer, save :: nerr=0,mx_w=1

      c = rc

      if(n.gt.8) then
!       if (nerr<=mx_w) then
!        write(*,*)
!        write(*,*) '  WARNING ::  CHEBPC:  n=',n,' > 8'
!        if ( nerr==0 ) then
!         write(*,*) '  You may have a numerical error,'
!         write(*,*) '  see Numerical Recipes for details'
!         write(*,*)
!        end if
!        if (nerr==mx_w ) then
!         write(*,*) ' CHEBPC warning is turned off'
!        end if
!        nerr = nerr+1
!       end if
       do j=8,n
        if ( abs(c(j-1))<epszero .and. abs(c(j))>abs(c(j-1)) ) then
         c(j:) = 0
         exit
        end if
       end do
      end if


      dd=0
      d(1)=c(n)
      d(2:n)=0

      do j=n-1,2,-1
       do k=n-j+1,2,-1
        sv=d(k)
        d(k)=d(k-1)+d(k-1)-dd(k)
        dd(k)=sv
       enddo
       sv=d(1)
       d(1)=-dd(1)+c(j)
       dd(1)=sv
      enddo

      do j=n,2,-1
       d(j)=d(j-1)-dd(j)
      enddo
      d(1)=-dd(1)+c(1)/2

      if ( a .ne. b ) then
       const=2/(b-a)
       fac=const
       do j=2,n
        d(j)=d(j)*fac
        fac=fac*const
       enddo
       const=(a+b)/2
       do j=1,n-1
        do k=n-1,j,-1
         d(k)=d(k)-const*d(k+1)
        enddo
       enddo
      end if

      rd = d

      return
!EOC
      end subroutine chebpc

!BOP
!!ROUTINE: chebcoef
!!INTERFACE:
      subroutine chebcoef(n,c)
!!DESCRIPTION:
! {\bv
! provides coefficients for Chebyshev (1st kind) polynomials, 
!  Tn = sum C(k) * x^{k-1}, k=1:n+1
! \ev}

!!DO_NOT_PRINT
      implicit none
!!DO_NOT_PRINT

!!ARGUMENTS:
      integer, intent(in) :: n
      real(8), intent(out) :: c(n+1)
!!REVISION HISTORY:
! Initial Version - A.S. - 2015
!!REMARKS:
!EOP
!
!BOC
      integer :: i,j,m
      real(8) :: a
      c = 0
      do j=n/2,0,-1
       m = 1+n-2*j
       if ( mod(n,2)==0 .and. j==n/2  ) then
        c(m) = dble((-1)**j)
       else
       if ( j==0 ) then
        a = dble(1)
       else
        a = dble(n)
        do i=1,j-1
         a = a*dble(n-j-i)/dble(j-i+1)
        end do
       end if
        c(m) = a*dble((-1)**j)*dble(2)**(n-(2*j+1))
       end if
      end do
      return
!EOC
      end subroutine chebcoef

!BOP
!!ROUTINE: pcshft
!!INTERFACE:
      subroutine pcshft(a,b,d,n)
!!DESCRIPTION:
! {\bv
! Polynomial coefficient shift based on
! Numerical Recipes 2nd Edition, Section 5.10
!  input:  coefficients are for -1 < x < 1
!  output: coefficients are for  a < x < b
! \ev}
!

!!DO_NOT_PRINT
      implicit none
!!DO_NOT_PRINT

!!ARGUMENTS:
      integer n
      real*8 a,b,d(n)
!!REVISION HISTORY:
! Initial Version - A.S. - 2003
!EOP
!
!BOC
      integer j,k
      real*8 const,fac

      if(n.le.1) return

      const=2.d0/(b-a)
      fac=const
      do j=2,n
       d(j)=d(j)*fac
       fac=fac*const
      enddo
      const=0.5d0*(a+b)
      do j=1,n-1
       do k=n-1,j,-1
      d(k)=d(k)-const*d(k+1)
       enddo
      enddo
      return
!EOC
      end subroutine pcshft

!BOP
!!ROUTINE: poly
!!INTERFACE:
      function poly(x,m,c)
!!DESCRIPTION:
! calculates value of polynomial function {\tt poly(x)}
! defined by coefficients {\tt c(0:m)}
!
!!REVISION HISTORY:
! Initial Version - A.S. - 2003
!EOP
!
!BOC
      implicit none
      real*8 poly
      real*8 x
      integer m
      real*8 c(0:m)
      integer j
      poly=c(m)
      do j=m-1,0,-1
       poly=poly*x+c(j)
      enddo
      return
!EOC
      end function poly
!c
!BOP
!!ROUTINE: chebev
!!INTERFACE:
      function chebev(a,b,c,m,x)
!!DESCRIPTION:
! {\bv
! function chebev evaluates f(x) from Chebyshev coefficients.
! (based on Numerical Recipes 2nd Edition, Section 5.8)
!   NO checks for x being not in the proper range
! \ev}

!!DO_NOT_PRINT
      implicit none
!!DO_NOT_PRINT

!!ARGUMENTS:
      integer, intent(in) :: m
      real(8), intent(in) :: a, b, x, c(m)
!!REVISION HISTORY:
! Initial Version - A.S. - 2003
!!REMARKS:
!EOP
!
!BOC
      real(8) :: chebev
      integer j
      real*8 d, dd, sv, y, y2
      d=0.d0
      dd=0.d0
      y = (2.d0*x-a-b)/(b-a)
      y2=y+y
      do j=m,2,-1
        sv=d
        d=y2*d-dd+c(j)
        dd=sv
      end do
      chebev=y*d-dd+0.5d0*c(1)
      return
!EOC
      end function chebev

