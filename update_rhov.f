      subroutine update_rhov(fold,xvalws,fnew,xvalwsnw,ztot,            &
     &                     v0imtz,v0iold,rr,                            &
     &                     imix,alpha0,beta,                            &
     &                     komp,jend,nbasis,                            &
     &                     mspn,brfile1,brfile2                         &
     &                     )
!c    ===============================================================
      use universal_const
      use mecca_interface
      use scf_io, only : sCurrParams,pot_B_mix,pot_S_mix
!c    ===============================================================
      implicit none
!c
!c    ==============================================================
!c     potential mixing
!c    ===============================================================
!c     imixrho:mix with Broyden or not (only for charges!)
!c     fnew:   new rho*pot
!c     fold:   old rho*pot
!c     xval:   valence charge (old and new)
!c     vdif:   new dif. of up & down potentials for spin-polarized
!c     vdold:  old dif. of up & down potentials for spin-polarized
!c    ===============================================================
!      integer, intent(in) ::  itscf
      integer, intent(in) ::  nbasis,komp(nbasis),mspn
      integer, intent(in) ::  jend(nbasis)
      integer, intent(in) ::  imix
!c
      real(8) ::   fold(:,:,:,:)  ! (iprpts,ipcomp,ipsublat,ipspin)
      real(8), intent(in) ::   xvalws(:,:,:)  ! (ipcomp,ipsublat,ipspin)
      real(8) ::   fnew(:,:,:,:)  ! (iprpts,ipcomp,ipsublat,ipspin)
      real(8) ::   xvalwsnw(:,:,:)! (ipcomp,ipsublat,ipspin)
      real(8), intent(in) ::   ztot(:,:)  ! (ipcomp,ipsublat)
      real(8), intent(in) ::   alpha0,beta
!c
      real(8), intent(inout) ::   v0imtz(:)
      real(8), intent(in)    ::   v0iold(:)
      real(8), intent(in) :: rr(:,:,:)  ! (iprpts,ipcomp,ipsublat)
      character(*), intent(in) :: brfile1,brfile2

!      real(8) :: xnjtop(size(xvalws,1),size(xvalws,2),size(xvalws,3))
!      real(8) :: xojtop(size(xvalws,1),size(xvalws,2),size(xvalws,3))
      integer :: ib,ik,is,jtop,ndrpts
      integer :: mxcomp,lastit
      real(8) :: alpha,amix

      ndrpts = size(fnew,1)
      alpha = alpha0
      jtop = min(maxval(jend(1:nbasis)),ndrpts)

       amix = alpha
!c===================================================================
       if(imix.eq.0) then
        call simplmx(fnew,fold,                                         &
     &               komp,nbasis,mspn,alpha,beta)
        call sCurrParams(imix=pot_S_mix,alpha=alpha,beta=beta)   
       else
!c
!c Broyden for potential
!c
        mxcomp = maxval(komp(1:nbasis))
!c====================================================================
        call broyden1(fnew(1:jtop,1:mxcomp,1:nbasis,1:mspn),            &
     &                fold(1:jtop,1:mxcomp,1:nbasis,1:mspn),            &
     &                xvalwsnw(1:mxcomp,1:nbasis,1:mspn),               &
     &                   alpha,beta,komp,nbasis,                        &
     &                       mspn,jtop,brfile1,brfile2)
        open(11,file=brfile1,status='unknown',form='unformatted')
        lastit = 1
        read(11,err=10,end=10)amix,lastit
        close(11)
10      if ( lastit == 1 ) then
          call sCurrParams(imix=pot_S_mix,alpha=amix,beta=beta)
        else
          call sCurrParams(imix=pot_B_mix,alpha=alpha,beta=beta)
        end if        
!c====================================================================

!c
       end if
       call simplmx1(v0imtz,v0iold,mspn,amix,beta)
!c===================================================================
!c
!c       write(6,'(//)')

       return

       contains

!c =====================================================================
        subroutine simplmx(frn,fro,komp,nbasis,nspin,alla,beta)
!c =====================================================================
        implicit none
!c
        real(8),  parameter :: half=one/two
!c+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!c
!c
        real(8), intent(inout) :: frn(:,:,:,:)  ! (iprpts,ipcomp,ipsublat,ipspin)
        real(8), intent(in)    :: fro(:,:,:,:)  ! (iprpts,ipcomp,ipsublat,ipspin)
        integer, intent(in)    :: nbasis,komp(nbasis),nspin
        real(8), intent(in)    :: alla,beta
        integer  ib,ik,k
        real(8) :: fa(size(fro,1))
        real(8) :: fb(size(fro,1))
        real(8) :: fc(size(fro,1))
        real(8) :: fd(size(fro,1))
!c
!c =====================================================================
!c subroutine to simple mix new and old potentials or charges ........
!c     nbasis    number of sublattices
!c     komp      number of compents/basis
!c     mspn      number of electron spins (1 or 2)
!c     alla      mixing parameter of average potential
!c     beta      mixing parameter of difference in potentials
!c     frn:      new      function(r)
!c     fro:      previous function(r)
!c =====================================================================
!c
        k = size(fro,1)
        do ib=1,nbasis
          do ik=1,komp(ib)
            if ( nspin==1 ) then
             fa = frn(1:k,ik,ib,1)
             fb = fro(1:k,ik,ib,1)
             frn(1:k,ik,ib,1) = (one-alla)*fb+alla*fa
            else
             fa = half*(frn(1:k,ik,ib,1)+frn(1:k,ik,ib,2))
             fb = half*(fro(1:k,ik,ib,1)+fro(1:k,ik,ib,2))
             fc = half*(frn(1:k,ik,ib,1)-frn(1:k,ik,ib,2))
             fd = half*(fro(1:k,ik,ib,1)-fro(1:k,ik,ib,2))
             frn(1:k,ik,ib,1) = (one-alla)*fb+alla*fa
             frn(1:k,ik,ib,2) = frn(1:k,ik,ib,1) -                      &
     &                                         ((one-beta)*fd + beta*fc)
             frn(1:k,ik,ib,1) = frn(1:k,ik,ib,1) +                      &
     &                                         ((one-beta)*fd + beta*fc)
            end if
          end do
        end do
!c
          return
          end subroutine simplmx
!c
          subroutine simplmx1(vnew,vold,nsp,alf_in,bet)
          implicit none
          integer :: nsp
          real(8) :: vold(nsp),vnew(nsp)
          real(8) :: alf_in,alf,bet
          real(8) :: vpls,vdif
          real(8), parameter :: half=one/two,dump=0.01d0
          if ( vnew(1)>0 .and. vnew(1)>vold(1) ) then
           alf = dump*alf_in
          else
           alf = alf_in
          end if
          if ( nsp==1 ) then
           vnew(1)= (one-alf)*vold(1) + alf*vnew(1)
          else
           vdif  = (one-bet)*(vold(2)-vold(1)) + bet*(vnew(2)-vnew(1))
           vpls  = (one-alf)*(vold(2)+vold(1)) + alf*(vnew(2)+vnew(1))
           vnew(1) = half*(vpls - vdif)
           vnew(2) = vnew(1) + vdif
          end if
          end subroutine simplmx1
!c
!          
       end subroutine update_rhov

