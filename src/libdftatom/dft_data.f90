module dft_data

! Contains the 'dft_data_t' type used in the DFT routines.
! This data type stores mesh, potential, atomic configuration, orbitals
! and other parameters of the DFT problem.

use types, only: dp
implicit none
private
public dft_data_t
public print_atom

type dft_data_t
    real(dp), dimension(:), pointer :: R, Rp, V_coulomb, V_h, V_xc, &
        e_xc, V_tot, rho
    real(dp), dimension(:, :), pointer :: orbitals
    real(dp) :: reigen_eps, alpha, c
    integer :: Z, scf_iter, reigen_max_iter
    logical :: dirac=.true.        ! If .true., we are solving the Dirac equation
    logical :: xc_relat=.false.
    logical :: ext_xc=.false.
    ! Triples of (n, l, f), where "n", "l" are quantum numbers and "f" is the
    ! occupation number:
    integer, dimension(:), pointer :: no, lo, so
    real(dp), dimension(:), pointer :: fo
    real(dp), pointer :: ks_energies(:), Emax_init(:), Emin_init(:)
    ! Total energy (and its parts):
    real(dp) :: Ekin, Ecoul, Exc, Eenuc, Etot
    logical :: perturb ! use perturbation acceleration in the eigenproblem
end type

contains

subroutine print_atom(d,nr)
type(dft_data_t), intent(in) :: d
character(*), optional :: nr
integer :: i
character(30) :: cfn
character(3) :: cZ

write(cZ,'(i0.3)') d%Z
if ( present(nr) ) then
 cfn = 'dftatom'//trim(cZ)//trim(nr)//'_xc.txt'
else
 cfn = 'dftatom'//trim(cZ)//'_xc.txt'
end if
open(11,file=trim(cfn))
write(11,'(''# Z = '',i3,a)') d%Z,' [Ry a.u.]'
write(11,'(''# '',(4x,10f14.6))') d%ks_energies * 2
write(11,'(''# '',2(a,f14.6))') 'E_tot=',d%Etot*2,' E_xc=',d%Exc*2
write(11,'(''# i,r,rho,e_xc,v_xc'')')
 do i=1,size(d%R)
  write(11,'(i5,4(2x,g18.8))') i,d%R(i),d%rho(i),d%e_xc(i)*2,d%V_xc(i)*2
!  if ( d%V_tot(i)>0 ) exit
 end do
 write(11,*)
close(11)

end subroutine print_atom

end module
