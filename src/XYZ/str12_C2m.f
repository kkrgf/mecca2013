       program str12
       implicit none
       real(8) :: a1(3),a2(3),a3(3)
       real(8) :: e1(3),e2(3),e3(3)
       real(8) :: scl(3)
       real(8) :: a,b,c,beta,x,y,z,xyz(3)
       integer :: ntype,nat
       character(2) :: id
       character(2), parameter :: w4i='4i',w8j='8j'
       character(2), parameter :: w2a='2a',w4h='4h'
!       real(8), parameter :: a0=0.529177d0  ! Angstr --> Bohr

       e1 = 0.d0
       e2 = 0.d0
       e3 = 0.d0
       e1(1) = 1.d0
       e2(2) = 1.d0
       e3(3) = 1.d0
       read(*,*) a,b,c,beta
!       a = a*a0
!       b = b*a0
!       c = c*a0
       scl(1) = a
       scl(2) = b
       scl(3) = c
       write(6,'(3g15.6,a,f8.3,a)') 0,b/a,c/a,' beta=',beta             &
     &                                    ,' ! REPLACE BY LAST LINE'  
       beta = beta/180.d0*4.d0*atan(1.d0)
       a1 = 0.5d0*a*e1 - 0.5d0*b*e2
       a2 = 0.5d0*a*e1 + 0.5d0*b*e2
       a3 = c*cos(beta)*e1 + c*sin(beta)*e3
       write(6,'(3g18.8,a)') a1/scl
       write(6,'(3g18.8,a)') a2/scl
       write(6,'(3g18.8,a)') a3/scl
       write(6,*) '--- none -- Prototype: none -- C2/m, #12 --'
       ntype = 0
       nat = 0
       do
       read(*,*,end=1004,err=1004) id,x,y,z
        if ( id == w4i ) then
         ntype = ntype+1
         xyz = x*(a1+a2) + z*a3
         xyz = xyz/scl
         write(6,'(i3,3g18.8,a)') ntype,xyz
         write(6,'(i3,3g18.8,a)') ntype,-xyz
         nat = nat+2
        else if ( id == w8j ) then
         ntype = ntype+1
         xyz = (x-y)*a1+(x+y)*a2+z*a3
         xyz = xyz/scl
         write(6,'(i3,3g18.8,a)') ntype,xyz
         write(6,'(i3,3g18.8,a)') ntype,-xyz
         xyz = (x+y)*a1+(x-y)*a2+z*a3
         xyz = xyz/scl
         write(6,'(i3,3g18.8,a)') ntype,xyz
         write(6,'(i3,3g18.8,a)') ntype,-xyz
         nat = nat+4
        else if ( id == w4h ) then
         ntype = ntype+1
         xyz = -y*a1 + y*a2 + 0.5d0*a3
         xyz = xyz/scl
         write(6,'(i3,3g18.8,a)') ntype,xyz
         xyz = y*a1 - y*a2 + 0.5d0*a3
         xyz = xyz/scl
         write(6,'(i3,3g18.8,a)') ntype,xyz
         nat = nat+2
        else if ( id == w2a ) then
         ntype = ntype+1
         write(6,'(i3,3g18.8,a)') ntype,0.d0,0.d0,0.d0
         nat = nat+1
        else
         exit
        end if
       end do
1004   continue
       write(6,*) 1.d0
       write(6,'(i3,2g15.6,a)') nat,b/a,c/a,' ! N,b/a,c/a'  

       end program str12
