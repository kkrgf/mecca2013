      subroutine zgssv_SLU(n,nnz,values,rowind,colptr,b,ldb,nrhs,       &
     &                     factors,info,iflag)
      implicit none
!  to solve equation A * z = b
!  A is a sparse matrix n by n defined by values, rowind and colptr;
!  values are non-zero elements of A;
!  b is right-hand-side on input,
!  output b is a solution z = A^(-1)*b
      integer, intent(in) :: n   ! matrix rank
      integer, intent(in) :: nnz ! number of non-zeros
      complex(8), intent(in) :: values(nnz)     ! 1:nnz
      integer, intent(in) :: rowind(nnz)        ! 1:nnz
      integer, intent(in) :: colptr(n+1)        ! 1:n+1
      complex(8), intent(inout) :: b(*)       ! n * nrhs
      integer, intent(in) :: ldb,nrhs
      integer(8), intent(inout) :: factors    ! saved variable on upper level 
      integer, intent(out) :: info
      integer, intent(in), optional :: iflag

      integer :: iopt,iperm

      info = 0
      iperm = 10
      iopt = 0
      if ( present(iflag) ) then
          if ( iflag>=1 .and. iflag <=3 ) then
           iopt = iflag
          else if ( iflag>=10 .and. iflag<=13 ) then
           iopt = iflag
          else
           if ( iflag.ne.0 ) then
               write(*,*) ' ERROR: value of IFLAG=',iflag,' is invalid'
            info = -1000
            return
           end if
          end if
      end if

      if ( iopt>0 ) then
! No change of b and *factors* between calls with opt=1 and opt=2 are assumed
        call c_fortran_zgssv( iopt,n,nnz,nrhs,values,rowind,colptr,     &
     &                       b(1),ldb,factors,info )
      else if ( iopt==0 ) then
       call slu_matrinv(iperm,n,nnz,values,rowind,colptr,b,ldb,info)
       if ( info.ne.0 ) stop 'ERROR in slu_matrinv'
      end if
      return
      end subroutine zgssv_SLU

      subroutine slu_matrinv(iperm,nrank,nnz,values,rowind,colptr,      &
     &                       b,ldb,info)
      implicit none
      integer, intent(in) :: iperm
      integer, intent(in) :: nrank, nnz
      integer, intent(in) :: rowind(nnz), colptr(nrank+1)
      complex(8), intent(in)  :: values(nnz)
      complex(8), intent(out) :: b(ldb,nrank)
      integer, intent(in)  :: ldb
      integer :: info

      integer(8) :: factors
      integer :: iopt,l1,nrhs

      if ( iperm>=10 .and. iperm<=13 ) then
       iopt = iperm
      else
       iopt = 1
      end if
      nrhs = nrank
      b = (0.d0,0.d0)
      do l1 = 1,nrank
       b(l1,l1) = (1.d0,0.d0)  ! in general, b(*,i) = 1*Precond[A]
      end do
      call c_fortran_zgssv(iopt,nrank,nnz,nrhs,values,rowind,colptr,    &
     &                      b, ldb, factors, info )
      if (info .eq. 0) then
!         write (*,*) 'Factorization succeeded'
      else
         write(*,*) 'Factorization failed, INFO = ', info
         return
      endif
      iopt = 2
      call c_fortran_zgssv(iopt,nrank,nnz,nrhs,values,rowind,colptr,    &
     &                      b, ldb, factors, info )
      if (info .eq. 0) then
!         write (*,*) 'Solve succeeded'
      else
         write(*,*) 'Triangular solve failed, info = ', info
         return
      endif

      iopt = 3
      call c_fortran_zgssv(iopt,nrank,nnz,nrhs,values,rowind,colptr,    &
     &                      b, ldb, factors, info )
      if (info .ne. 0) then
         write (*,*) 'Memory release failed, info = ',info
         return
      end if

      return
      end subroutine slu_matrinv
