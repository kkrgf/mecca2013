      program z_test_main
      integer maxn, maxnz
      parameter ( maxn = 10000, maxnz = 100000 )
      integer rowind(maxnz), colptr(maxn)
      complex*16  values(maxnz), b(maxn)
      integer n, nnz, nrhs, ldb, info, iopt
      integer*8 factors
!
      call zhbcode1(n, n, nnz, values, rowind, colptr)
!
      nrhs = 1
      ldb = n
      do i = 1, n
         b(i) = (1,2) + i*(3,4)
      enddo
!

! First, factorize the matrix. The factors are stored in *factors* handle.
      iopt = 1
      call zgssv_SLU(nnz,values,rowind,colptr,b,ldb,nrhs,factors,       &
     &               info,iopt)
!
      if (info .eq. 0) then
         write (*,*) 'Factorization succeeded'
      else
         write(*,*) 'INFO from factorization = ', info
      endif
!
! Second, solve the system using the existing factors.
      iopt = 2
      call zgssv_SLU(nnz,values,rowind,colptr,b,ldb,nrhs,factors,       &
     &               info,iopt)
!      call c_fortran_zgssv( iopt, n, nnz, nrhs, values, rowind, colptr, 
!     $                      b, ldb, factors, info )
!
      if (info .eq. 0) then
         write (*,*) 'Solve succeeded'
         write (*,*) (b(i), i=1, 10)
      else
         write(*,*) 'INFO from triangular solve = ', info
      endif

! Last, free the storage allocated inside SuperLU
      iopt = 3
!      call c_fortran_zgssv( iopt, n, nnz, nrhs, values, rowind, colptr, 
!     $                      b, ldb, factors, info )
      call zgssv_SLU(nnz,values,rowind,colptr,b,ldb,nrhs,factors,       &
     &               info,iopt)
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
      do i = 1, n
         b(i) = (1,2) + i*(3,4)
      enddo
      factors = huge(factors)
      iopt = 0
      call zgssv_SLU(nnz,values,rowind,colptr,b,ldb,nrhs,factors,       &
     &               info,iopt)
      if (info .eq. 0) then
         write (*,*) 'Solve succeeded'
         write (*,*) (b(i), i=1, 10)
      else
         write(*,*) 'bad news INFO = ', info
      endif
      
      stop

      contains

      subroutine zhbcode1(nrow, ncol, nnzero, values, rowind, colptr)

C     ================================================================
C     ... SAMPLE CODE FOR READING A SPARSE MATRIX IN STANDARD FORMAT
C     ================================================================

      CHARACTER      TITLE*72 , KEY*8    , MXTYPE*3 ,
     1               PTRFMT*16, INDFMT*16, VALFMT*20, RHSFMT*20

      INTEGER        TOTCRD, PTRCRD, INDCRD, VALCRD, RHSCRD,
     1               NROW  , NCOL  , NNZERO, NELTVL

      INTEGER        COLPTR (*), ROWIND (*)

      COMPLEX*16     VALUES (*)

C    ------------------------
C     ... READ IN HEADER BLOCK
C     ------------------------

      READ ( *, 1000 ) TITLE , KEY   ,
     1                     TOTCRD, PTRCRD, INDCRD, VALCRD, RHSCRD,
     2                     MXTYPE, NROW  , NCOL  , NNZERO, NELTVL,
     3                     PTRFMT, INDFMT, VALFMT, RHSFMT
 1000 FORMAT ( A72, A8 / 5I14 / A3, 11X, 4I14 / 2A16, 2A20 )

C     -------------------------
C     ... READ MATRIX STRUCTURE
C     -------------------------

      READ ( *, PTRFMT ) ( COLPTR (I), I = 1, NCOL+1 )

      READ ( *, INDFMT ) ( ROWIND (I), I = 1, NNZERO )

      IF  ( VALCRD .GT. 0 )  THEN

C         ----------------------
C         ... READ MATRIX VALUES
C         ----------------------

          READ ( *, VALFMT ) ( VALUES (I), I = 1, NNZERO )

      ENDIF

      return
      end subroutine zhbcode1

      end program 


