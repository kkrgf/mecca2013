!BOP
!!ROUTINE: tausprs
!!%INTERFACE:
!!REMARKS:
! Sparse version of mecca is not supported
!EOP
      subroutine tausprs(                                               &
     &              ndimbas,ndkkr,                                      &
!CALAM     *              tref,trefinv,ndkkr,deltat,deltinv,
!CALAM     *              numnbi,jclust,ntypecl,ntau,
     &              ndimnn,maxclstr,ndgreen)
!CALAM     *              nnptr,maxclstr,nnclmn,
!CALAM     *              rsij,param1,inum,
!CALAM     *              invswitch,invparam,isprs,ineq,tolinv,bigmem,
!CALAM     *              greenrs,ndgreen,greenKS,tau00,
!CALAM     *              iprint,istop)
!c     ==================================================================
!c
!c  To calculate diagonal block of Tau (tau00) in k-space
!c
!c     ==================================================================

!c
!
!BOC
      implicit none
!c
      integer natom,ndimbas

!c
!CALAM      real*8      aij(3,*)
!CALAM      real*8      kx
!CALAM      real*8      ky
!CALAM      real*8      kz
!c
      integer      ndgreen
!CALAM      complex*16   greenrs(ndgreen,*)
!CALAM      complex*16   greenKS(*)
!c                (1:nbasis*(kkrsz*kkrsz)*maxclstr+...)

      integer      ndkkr
!CALAM      complex*16   tcpa(ndkkr,ndkkr,*)
!CALAM      complex*16   tref(ndkkr,*)
!CALAM      complex*16   trefinv(ndkkr,*)
!CALAM      complex*16   deltat(ndkkr,ndkkr,*)
!CALAM      complex*16   deltinv(ndkkr,ndkkr,*)


!CALAM      complex*16   tau00(ndkkr,ndkkr,natom)

!CALAM      integer      itype(natom),itype1(natom)
!CALAM      integer      mapstr(ndimbas,natom)

!CALAM      integer iscreen

      integer ndimnn
!CALAM      integer numnbi(*),ndimnn,jclust(ndimnn,*)
!CALAM      integer ntypecl(ndimnn,*),ntau(ndimnn,*)
!CALAM      integer mapsnni(ndimnn*ndimnn,*)
!CALAM      real*8  rsij(ndimnn*ndimnn*3,*)
!CALAM      real*8  param1
!CALAM      integer inum

!CALAM      integer invswitch,invparam,iprint
!CALAM      character istop*10
!c     ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
      character    sname*10
      parameter    (sname='tausprs')
!c     ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!c      integer kkrsz
!c      integer iatom,iatom0,jatom,jatom0,isub,jsub,nsub,ia0,i,m,l
!c      integer ndmat,jnn,indgKS,indgKSi,indgKSj

!c      integer iswitch,isparse,isprs(3),ineq
!CALAM      integer isprs(3),ineq
!CALAM      real*8  tolinv,bigmem
      integer indclstr

!c      real*8 time1,time2

      external indclstr

!c      integer erralloc,ndbmt,ndKS

!c      integer nni,inn,inn0,nnmax
       integer maxclstr
!CALAM      integer nnptr(0:1,natom),nnclmn(2,maxclstr,natom)

!C
      write(*,*) ' SPARSE VERSION OF MECCA IS NOT SUPPORTED',           &
     &           ' IN THIS DISTRIBUTION......'
      write(*,*) '  to use sparse matrix techique file <tausprs.f>'
      write(*,*) '  should be replaced by <tausprs_orig.f>'
      write(*,*) '  Then additional subroutines (in particular, from'
      write(*,*) '  QMRPACK and SuperLU packages) are neseccary'
      call fstop(sname//': sparse version is not supported')
!C----------------------------------------------------------------------- !-

      return
      end

