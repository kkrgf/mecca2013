      SUBROUTINE TIMEL(TIME)
      REAL*8 TIME

      real*4 elapsed
      type tb_type
       sequence
       real*4 usrtime
       real*4 systime
      end type
      type(tb_type)etime_struct

      elapsed = etime_(etime_struct)
      time = (elapsed)

      return
      END
