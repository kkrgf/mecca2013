!BOP
!!ROUTINE: print_strctr
!!INTERFACE:
      subroutine print_strctr(vlatt,site_xyz,site_id                    &
     &                                         ,outfile,iflag,site_r)
!!DESCRIPTION:
! {\bv
!   prints structural file,
!   its format is defined by input parameter IFLAG
!
!   vlatt(1:3,i)    -- i-th lattice vector (in Bohr), i=1..3
!   site_xyz(1:3,i) -- cartesian site coordinates (in Bohr), i=1..nsites
!   site_id(i)      -- site_id (i.e. atomic chemical symbol), i=1..nsites
!
!   outfile (optional) -- name of output file
!   iflag (optional)  -- defines format of output file
!   site_r(i) (optional) -- radius of i-th site (in Bohr), i=1..nsites
! \ev}

!!REVISION HISTORY:
! Initial version - ..A.S.. - 2018
!EOP
!
!BOC
       implicit none

       real(8), intent(in) :: vlatt(3,3)
       real(8), intent(in) :: site_xyz(:,:)   !  (3,nsites)
       character(*), intent(in) :: site_id(*)
       character(*), intent(in), optional :: outfile
       integer, intent(in), optional :: iflag
       real(8), intent(in), optional :: site_r(:)

       integer :: nsites
       integer :: nch,ifrmt

       nch = 6
       if ( present(outfile) ) then
        nch = 11
        open(nch,file=trim(outfile))
       end if

       ifrmt = 1
       if ( present(iflag) ) then
           ifrmt = iflag
       end if 

       nsites = size(site_xyz,2)  ! number of sites

       select case (ifrmt)
       case(1)
!..............................
       case default
       end select

       if ( nch.ne.6 .and. nch.ne.0 )  close(nch)
!!
       return
!EOC
       end subroutine print_strctr

