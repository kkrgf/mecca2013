!   ******************************************************************
!   *                                                                *
!   *   Parameter file for Multi-sublattice/component KKR-CPA.       *
!   *                                                                *
!   *    The array dimensions correspond to the maximum number of    *
!   *    the variables specified below.                              *
!   *                                                                *
!   *    ipcomp  : No. of components.                                *
!   *    ipbase  : No. of atoms in basis.                            *
!   *    ipsublat: No. of inequiv.sublattices                        *
!   *    ipspin  : No. of spin variables, 1 or 2.                    *
!   *    ipepts  : No. of energy points.                             *
!   *    iprpts  : No. of radial mesh points. (must be odd)          *
!   *              1051 accurately gets energy of 3d-5d elements     *
!   *                                                                *
!   *    ipcrys  : No. of lattices implemented.                      *
!   *    ippole  : No. of Free electron poles.                       *
!   *    ipeval  : No. of allowed core states, including semicore    *
!   *    iprsn   : No. of real-space vectors for Ewald method        *
!   *    ipxkns  : No. of k-space vectors for Ewald method           *
!   *                                                                *
!DEBUG   *    ipkpts  : Max No. of k-points                          *
!DEBUG   *    iprot   : No. of rotation operations.                  *
!   ******************************************************************
! INTEGERS
      integer ipepts,iprpts,ipcomp,ipspin,ipbase,ipsublat
      integer ipcrys,ipeval,iprsn,ipxkns,ipbsfkpts

!      integer ippole
!DEBUG      integer ipkpts
!DEBUG      integer iprot
!
      parameter (ipepts=300)          ! max. pts. on E contour
      parameter (ipbsfkpts=50)        ! max. no. k-space waypts for A(k,E)
!      parameter (NDOS=401)            ! max. no. of E-pts for DOS calc.
      parameter (iprpts=1051)         ! max. pts. on r grid (odd only)
      parameter (ipcomp=4)            ! max. species for alloy
      parameter (ipspin=2)            ! max. spin manifolds  (1=NM or 2=Mag)
!      parameter (ipspin=1)           ! max. spin manifolds
      parameter (ipbase=700)          ! max. basis atoms
!      parameter (ipsublat=ipbase)    ! max. sublattices
      parameter (ipsublat=24)         ! max. sublattices
      parameter (ipcrys=11)           ! max. crystal structures
!
      parameter (ipeval=30)           ! max. core eigenvalues
!DEBUG      parameter (ipkpts=110592) ! max. k-pts.
!DEBUG      parameter (iprot=48)      ! max. rotatioin operations
!ALAM      parameter (ipxkns=400*25)
      parameter (ipxkns=800*25)
!                        max. k-vectors in Ewald sum (per k-cell)
!AB                 ??? ipxkns ~ ipbase**(1/3) ???
      parameter (iprsn=1100)
!                        max. r-vectors in Ewald sum (per r-cell)

!DEBUG      integer   ipbase2
!DEBUG      parameter (ipbase2 = ipbase*ipbase)
!DEBUGc
!DEBUGc        ipbase2 -- upper limit for the number of different Aij-vectors
!DEBUGc        1<=ipbase2<=ipbase*ipbase-ipbase+1
!
      integer   iprij
      parameter (iprij = iprsn*250)
!
!        iprij -- upper limit for the number of all r-vectors in Ewald sum
!        iprsn <= iprij <= iprsn*ipbase2
!
      integer   iprhp
      parameter (iprhp = iprij)
!
!        iprhp -- upper limit for the number of all harm.polinomials
!                 stored in memory (real space part)
!        1 <= iprhp <= iprij
!


! REALS
!=======================================================================
!     ANSI-77 standard is to recognize real parameter by declaration in
!     parameter statement, then convert to declaration in REAL statement.
!     This is not adhered to by many machines, but is by IBM.
!     In other words in ANSI standard, with [REAL*8 one] declared and
!     with [PARAMETER (one=1.0)], one is treated as 1.0e+16 and then
!     buffered out to 1.0d+16. Also, this proceedure can get errors in
!     some last double-precision decimal place because of inaccuracies
!     associated with binary to decimal conversion from the buffering.
!         We declare only the required REALS here.
!         Rest of program uses these to get everything else!
!=======================================================================
      real*8    zero,one,two,three,four,five,six,seven,eight,ten
      real*8    pi
!
      parameter (zero=0.d0,one=1.0d0,two=2.0d0,three=3.0d0,four=4.0d0)
      parameter (five=5.0d0,six=6.0d0,seven=7.0d0,eight=8.0d0)
      parameter (ten=10.0d0)
!
! PI to 19 decimals so that it is not machine dependent!!
! This is used throughout code to define 2pi and 4pi and get proper G.
      parameter (pi=3.1415926535897932385d0)

! coefficient Ry --> Kelvin; 1 Ry = ry2kelvin
      real*8 ry2kelvin
      parameter (ry2kelvin=1.579061d+05)
! coefficient to convert mu_B*MagnField to Ry; mu_B = amuBoT * (Ry/Tesla)
      real*8 amuBoT
      parameter (amuBoT = 4.25436d-06)
!
!=======================================================================
! Two REAL parameters have been FIXED in GETTAU to do only EWALD !!!
!                               ---------------
! FOR STRUCTURE CONSTANTS (see notes in GETTAU) 2 parameters might
! possibly change so that both Ewald and real-space methods are used
! to get Structure Constants. These are the switches in real and Im E
! (eresw & eimsw) to use EWALD around entire E contour or
! to use EWALD within 1/3 mRy of Real E axis and real-space method
! beyond 1/3 mRy of Real E axis.
! !Currently, EWALD is faster and more accurate than real-space method!!
!=======================================================================
!
!  CHARACTERS
!=======================================================================
      character  stats(4)*7
      data stats/'old    ','unknown','new    ','replace'/
      character  fmt(2)*11
      data  fmt/'formatted  ','unformatted'/
!  NO COMPLEXES
!=======================================================================

!  ipunit -- maximum number of input/output files
      integer ipunit
      parameter (ipunit=30)
!
!  Tolerence factors for convergence
      real*8 etol,rmstol,eftol
      parameter ( eftol=1.0d-4)   ! Fermi level tolerance (to find Ef)
      parameter (  etol=1.0d-6)   ! Etot tolerance (SCF cycle)
      parameter (rmstol=1.0d-6)   ! charge dens. tolerance (SCF cycle)

!       ipmesh -- upper limit for the number of different k-meshes
      integer ipmesh
      parameter (ipmesh=3)
!
!  ikpsym --Type of special k-point mesh to use
      integer ikpsym
      parameter (ikpsym=0)
!      parameter (ikpsym=-2)
!
!              ikpsym = 0   -- minimum number of special k-points
!              ikpsym =-2   -- full number of k-points
!              ikpsym = 1   -- minimum symmetrized number of k-points
!              ikpsym =-1   -- full symmetrized number of k-points
!
!  maxcpa -- upper limit of CPA iterations
      integer maxcpa
      parameter (maxcpa=50)
!
!  ipmdlng -- Number of real- and k-space vectors for Madelung summation
      integer ipmdlng
      parameter (ipmdlng = 2000)
!
      real*8 etamdl
      parameter (etamdl = 0.7d0)
!
!  etamdl -- "eta" for Madelung summation
!
      integer iptmpi1
      parameter (iptmpi1 = ipxkns+iprij+ipmdlng)
      integer iptmpr1
      parameter (iptmpr1 = iprij+ipmdlng+ipbase*3+ipxkns+ipxkns*3+ipxkns*2)
!
!
!  CONTROL SECTION:
!
!   CPA mean-field charge-correlation parameter -- see genpot.f
!    c_mf_cc = 0.0       standard CPA  (NO charge correlation)
!    c_mf_cc = 1.0/2=0.5 scr-cpa   Johnson & Pinsk, PRB48, 11553 (1993)
!    c_mf_cc = 1.3147/2  FCC analytic  Magri et al. PRB42, 11388 (1990)
!    c_mf_cc = 1.3831/2  BCC analytic
!    c_mf_cc = 2.0/2=1   SIM       Korzhavyi et al. PRB51,  5773 (1995)
!      real*8 c_mf_cc/0.6916d0/    ! BCC analytic
!      real*8 c_mf_cc/0.6574d0/    ! FCC analytic
!      real*8 c_mf_cc/0.50d0/      ! scr-cpa  (all close-packed lattices)
       real*8 :: c_mf_cc = 0.00d0       ! standard CPA

!  Is zero-pt energy included via gruneison parameter?
!                  (exp.data are used) (no problem if system has all Z<50)
      integer ipzpt
      parameter (ipzpt=0)   ! Do not include zero-pt energy (zeropt.f)
!     parameter (ipzpt=1)   ! Include zero-pt energy (zeropt.f) if it is known

!  Which screened-KKR version?
      integer ipmethod
      parameter (ipmethod=1)     ! Screened KKR, Mixed representation 1
!                  Real-Space for Gref, K-space for G  -- for any energy

!      parameter (ipmethod=2)     ! Screened KKR, Mixed representation 2
!                  Real-Space for Gref, K-space for G -- near the Real axis
!                  Real-Space for both Gref and G     -- otherwise

!      parameter (ipmethod=3)     ! Screened KKR, Cluster Approach  (LSMS)
!                  Real-Space for both Gref and G  -- for any energy

!      parameter (ipmethod=4)     ! Screened KKR, Mixed representation 2
!                  Real-Space for Gref, K-space for G -- near the Real axis
!                  sparse standard K-space -- otherwise

!      parameter (ipmethod= 0)   ! standard K-space KKR
!      parameter (ipmethod=-1)   ! K-space screened KKR
!      parameter (ipmethod=-2)   ! K-space screened KKR + sparse LU inversion
!      parameter (ipmethod=-3)   ! K-space screened KKR + sparse MAR+TFQMR inversion

      real*8 :: U0 = 4.d0
!            potential parameter for screened method  (ipmethod.ne.0)
