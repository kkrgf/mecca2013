!c
!c ======================================================================
      subroutine invals(rg,rf,r,slp,gam,v,z,tol,drg,drf,en,c,dk)
!c ======================================================================
!c
!c
!c initial values for outward integration................................
!c modified desclaux code.................bg.....june 1990...............
!c
!c **********************************************************************
!c variables explanation: ...............................................
!c rg big component times r rf: small component times r; ................
!c r; radial log. grid; slp: slope at the origine; gam 1st term power;...
!c v potential at the first point; z atomic number;......................
!c tol check for convergence; drg,drf enrivatives times r;...............
!c c: speed of light in rydbergs; en; energy in rydbergs;................
!c dk; spin angular quantum number;......................................
!c **********************************************************************
!      implicit real*8(a-h,o-z)
      use mecca_constants
      implicit none
!c ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!      include 'mkkrcpa.h'
!c ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
      integer, parameter :: ipdeq=5,ipdeq2=10
!c ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!c
      real(8) :: rg(:),rf(:),r(:)
      real(8) :: slp,gam,v,z,tol
      real(8) :: drg(:),drf(:)
      real(8) :: en,c,dk
      character(80) ::  coerr
      character(6) ::  sname='invals'

      integer j,m,k
      real(8) :: dm,pow1,pow2,ratfg,rfr,rgr,sm,term1,term2,zalfa
!c
      zalfa=two*z/c
      term1=-zalfa
      term2=zalfa/r(1)+(v-en)/c
!c
      rg(1:ipdeq2) = zero
      rf(1:ipdeq2) = zero
!c
      if( dk .le. zero ) then
        ratfg=(dk-gam)/zalfa
      else
        ratfg=zalfa/(dk+gam)
      endif
!c
      rf(ipdeq2)=slp
      rg(ipdeq2)=ratfg*slp
!c
      do j=1,ipdeq
        rg(j)=rg(ipdeq2)
        rf(j)=rf(ipdeq2)
        drg(j)=rg(j)*gam
        drf(j)=rf(j)*gam
      end do
!c
      do m=1,20
       dm=m+gam
       sm=dm*dm-dk*dk+term1*term1
       rfr=(c-term2)*rf(m+ipdeq2-1)
       rgr=term2*rg(m+ipdeq2-1)
       pow1=((dm-dk)*rfr-term1*rgr)/sm
       pow2=((dm+dk)*rgr+term1*rfr)/sm
       k=-1
!c
       do j=1,ipdeq
        rgr=r(j)**m
        rfr=pow2*rgr
        rgr=pow1*rgr
        if ( m.ne.1 ) then
          if ( abs(rgr/rg(j)) .le. tol .and.                            &
     &         abs(rfr/rf(j)) .le. tol ) k=1
        end if
        rg(j)=rg(j)+rgr
        rf(j)=rf(j)+rfr
        drg(j)=drg(j)+rgr*dm
        drf(j)=drf(j)+rfr*dm
       end do
!c
       if( k .eq. 1 ) return
!c
       rg(m+ipdeq2)=pow1
       rf(m+ipdeq2)=pow2
      end do
!c
      coerr=' no convergence in small r expansion'
      call fstop(sname//':'//coerr)
!c
      return
      end
