!c     ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      subroutine openf
!c     ---------------------------------------------------------------
!c
      character comment*72,st*1,fm*1
      character stats(2)*7,fmt(2)*11
      character fname(25)*50
      character opfile*50
!c      character opfile*50,ipfile*50
      character blnk*50,obroy1*50,obroy2*50
!c
      integer nunit(25)
!c
      common/broydn/obroy1,obroy2
!c
      data stats/'old    ','unknown'/
      data fmt/'formatted  ','unformatted'/
      data blnk/'                              '/
!c
!c     ================================================================
!c     this subroutine opens files for input and output
!c          these statements are very machine dependent ]
!c     ================================================================
!c
!c                explanation of read/write files
!c     read files --------------------------------------------------
!c
!c     write files --------------------------------------------------
!c
!c      *************************************************************
!c      initialise the drop file for cray-xmp at nmfecc..
!c      call dropfile(0)
!c
!c      *************************************************************
!c
!c      *************************************************************
!c      input file specifications for f77-type compiler..........
!c
!c      read in names of  input files.................
!c      open(unit=5,file='licoo2_i',status='old')
!*      write(6,'('' enter name of input file'')')
!*      read(5,'(a50)')  ipfile
!*      write(6,'(''  name of input  file =  '',a50)') ipfile
!c      initialise unit 5 .................................
!*      open(unit=5,file=ipfile,form='formatted')
!c
!c      read in names of  output files.................
       read(5,'(a50)')  opfile
       write(6,'(''  name of output file =  '',a50)') opfile
!c      initialise unit 6 .................................
       if(opfile.ne.blnk)open(unit=6,file=opfile,form='formatted')
!c
       write(6,'(1x,78(''=''))')
       write(6,'(//''  file management: list of files opened '',        &
     & ''by this program''/)')
       nu=1
       iu=6
       write(6,'(2x,i2,''.  unit number =  '',i3,                       &
     &     '',  file name is '',a50)') nu,iu,opfile
!c
!c ...................................................................
!c       initialize remaining file names and unit numbers
!c       the names and unit numbers are in the run file.
!c ...................................................................
        read(5,'(a72)') comment
        iopen=0
  10    iopen=iopen+1
        read(5,'(i3,1x,2a1,1x,a50)') nunit(iopen),st,fm,fname(iopen)
        if(nunit(iopen).le.0) go to 11
!c file status and format
        is=2
        if(st .eq. 'o' ) is=1
        ifm=1
        if(fm .eq. 'u' ) ifm=2
!c establish logical names of Broyden files for common block
        if(nunit(iopen).eq.61) obroy1=fname(iopen)
        if(nunit(iopen).eq.62) obroy2=fname(iopen)
        if(nunit(iopen).eq.61 .or. nunit(iopen).eq.62)go to 10
!c
        open(unit=nunit(iopen),file=fname(iopen),                       &
     &                 form=fmt(ifm),status=stats(is))
        go to 10
  11    iopen=iopen-1
        do i=1,iopen
         nu=i+1
         write(6,'(2x,i2,''.  unit number =  '',i3,                     &
     &     '',  file name is '',a50)') nu,nunit(i),fname(i)
        enddo
       write(6,'(/,1x,78(''=''))')
        return
!c
!c      -----------------------------------------------------------
!c      initialise file used for print when using flow trace...
!c      call link("unit10=(scfflow,create,text),print10//")
!c      -----------------------------------------------------------
!c
      end
