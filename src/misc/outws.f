!c
!c ======================================================================
      subroutine outws(ipdeq,invp,rg,rf,rv,r,en,c,drg,drf,elim,         &
     &                 z,gam,slp,tol,imm,lll,dk,dm,nodes,nws)
!c ======================================================================
!c
!c outward solution
!c adams 5 points  diff. eq. solver for dirac equations
!c
!c **********************************************************************
      use mecca_constants
!      implicit real*8(a-h,o-z)
      implicit none
!c ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!      include 'mkkrcpa.h'
!c ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!c
!c ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
      real(8), parameter :: twntsvn=three**3
      real(8), parameter :: f02=five*ten*ten + two
      real(8), parameter :: f75=f02 - twntsvn
      real(8), parameter :: coef2=twntsvn/f02,coef1=f75/f02
!c     coef1=475./502., coef2=27./502.
!c     parameter (coef1=.9462151394422310,coef2=.0537848605577689)
!c ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!c
!c     drg,drf derivatives of dp and dq times r;  c speed of light
!c     rv potential times r; r: radial grid; en energy guess
!c *********************************************************************
!c
      integer ipdeq,invp
      real(8) :: r(:),rv(:),rg(:),rf(:)
      real(8) :: en,c
      real(8) :: drf(:),drg(:)
      real(8) :: elim,z,gam,slp,tol
      integer :: imm,lll
      real(8) :: dk,dm
      integer :: nodes,nws
      character(80) :: coerr
      character(5), parameter ::sname='outws'

      integer :: nd,j,k,l
      real(8) :: dp,dpr,dq,dqr,er,emvoc,vor,rpow
!c
      nd=0
!CAB_TODO      vor = (rv(1)+2.d0*z)/r(1)
      vor=rv(1)/r(1)
!c
!c     find classical inversion point
!c *********************************************************************
!c
 10   if( imm .eq. 1 ) go to 40
!c
      invp = invPoint(ipdeq,lll,nws,r,rv,en)
!c
 30   if( invp .gt. ipdeq ) go to 40
!c
      en=en*0.5d+00
!c
      if( en .lt. -tol .and. nd .le. nodes ) go to 10
!c
        coerr=' ERROR :: screwed potential'
      call p_fstop(sname//':'//coerr)
!c **********************************************************************
!c initial values for outward integration
!c **********************************************************************
 40   call invals(rg,rf,r,slp,gam,vor,z,tol,drg,drf,en,c,dk)
!c **********************************************************************
      nd=1
      do 60 j=1,ipdeq
        rpow=r(j)**gam
!c
        if( j .eq. 1 ) go to 50
        if( rg(j-1) .eq. zero ) go to 50
        if( (rg(j)/rg(j-1)) .gt. zero ) go to 50
!c
        nd=nd+1
 50     rg(j)=rg(j)*rpow
        rf(j)=rf(j)*rpow
        drg(j)=drg(j)*rpow
        drf(j)=drf(j)*rpow
 60   continue
!c
!c     check consistence of signs of big and small component
!c **********************************************************************
      k=-1+2*(nodes-2*(nodes/2))
!c
      if( (rg(1)*k) .gt. zero ) go to 80
!c
 70   coerr=' ERROR :: errors in small r expansion'
      call p_fstop(sname//':'//coerr)
!c
 80   if( (k*dk*rf(1)) .lt. zero ) go to 70
!c
!c **********************************************************************
!c
!c     solve dirac eqs. now
!c **********************************************************************
      do 100 j=ipdeq+1,invp
!c
!c     5 points predictor
!c
        dpr= rg(j-1) + dm*(2.51d+02*drg(1)-1.274d+03*drg(2)+            &
     &       2.616d+03*drg(3)-2.774d+03*drg(4)+1.901d+03*drg(5) )
        dqr= rf(j-1) + dm*(2.51d+02*drf(1)-1.274d+03*drf(2)+            &
     &       2.616d+03*drf(3)-2.774d+03*drf(4)+1.901d+03*drf(5) )
!c
!c     shift derivatives
!c
        do 90 l=2,ipdeq
          drg(l-1)=drg(l)
          drf(l-1)=drf(l)
 90     continue
!c
!c     dirac equations (log. mesh)
!c
        er=en*r(j)
        emvoc=(er-rv(j))/c
        drg(ipdeq)=-dk*dpr+(c*r(j)+emvoc)*dqr
        drf(ipdeq)=dk*dqr-emvoc*dpr
!c
!c     5 points corrector
!c
        dp= rg(j-1) + dm*(-1.9d+01*drg(1)+1.06d+02*drg(2)               &
     &              -2.64d+02*drg(3)+6.46d+02*drg(4)+2.51d+02*drg(5) )
        dq= rf(j-1) + dm*(-1.9d+01*drf(1)+1.06d+02*drf(2)               &
     &              -2.64d+02*drf(3)+6.46d+02*drf(4)+2.51d+02*drf(5) )
!c
!c     mixing
!c
        dp=coef1*dp+coef2*dpr
        dq=coef1*dq+coef2*dqr
        rg(j)=dp
        rf(j)=dq
!c
!c     update derivative
!c
        drg(ipdeq)=-dk*dp+(c*r(j)+emvoc)*dq
        drf(ipdeq)=dk*dq-emvoc*dp
!c
!c     check number of nodes
!c **********************************************************************
!c
        if(  rg(j-1) .eq. zero ) go to 100
        if( (rg(j)/rg(j-1)) .gt. zero ) go to 100
!c
        nd=nd+1
!c
        if( nd .gt. nodes ) go to 110
!c
 100  continue
!c
!c     if no. of nodes is too small increase the energy and start again
!c **********************************************************************
!c
      if( nd .eq. nodes ) go to 130
!c
      en=0.8d+00*en
!c
      if( en .lt. -tol ) go to 10

      en = zero
      return
!c
!c
!c     this energy guess has become too high
!c **********************************************************************
!c     if no. of nodes is too big decrease the energy and start again
!c **********************************************************************
 110  en=1.2d+00*en
!c
      if( en .gt. elim ) go to 10
!c
!c     this energy guess has become too low
!c **********************************************************************
      coerr=' ERROR :: too many nodes'
      call p_fstop(sname//':'//coerr)
!c **********************************************************************
 130  return

      CONTAINS
!c
      integer function invPoint(ipdeq,lll,nr,r,rv,en)
      implicit none
      integer ipdeq, lll
      integer nr
      real*8 r(nr), rv(nr)
      real*8 en
      real*8 rj
      real*8 al
      integer j
      al = -lll
      do j = ipdeq+2, nr+1-ipdeq
       invPoint = nr+1-j
       rj = r(invPoint)
       if( (rj*(rv(invPoint)-en*rj)).le.al) return
      end do
      return
      end function invPoint
!c
      end subroutine outws
!c
