!C   *    iplmax  : No. of l-quantum numbers - lmax                   *
!C   *    ipkkr   : No. of {l,m} quantum numbers.  (lmax+1)^2         *
!C
      integer iplmax

!c      parameter (iplmax=2)                 ! max. ang. momentum
!c      parameter (iplmax=3)                 ! max. ang. momentum
      parameter (iplmax=4)                 ! max. ang. momentum
!c      parameter (iplmax=8)                 ! max. ang. momentum

      integer ipkkr
      parameter (ipkkr=(iplmax+1)**2)

      integer    ipdlj
      parameter (ipdlj=(2*iplmax+1)**2)     ! dlm(ewald)

      integer lcllmax
      parameter (lcllmax=iplmax)      ! "local" lmax (internal sum?)

      integer    ipmltp
      parameter (ipmltp=0)  ! multipoles: 0 - no multipoles, 1 - dipoles, ..
!c       parameter (ipmltp=1)  ! multipoles: 0 - no multipoles, 1 - dipoles, ..
!c       parameter (ipmltp=2)  ! multipoles: 0 - no multipoles, 1 - dipoles, ..

!c      parameter (ipmltp=4)  ! multipoles: 0 - no multipoles, 1 - dipoles, ..
