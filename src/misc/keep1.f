!c     cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      subroutine keep1(alat,boa,coa,atcon,iz,                           &
     &                nbasis,komp,nspin,                                &
     &                fetot,Temp,press,npts,nq,                         &
     &                rms,efermi,vmtz,vdif,                             &
     &                head,xc_descr,                                    &
     &                xvalws,zvalav,                                    &
     &                imix,alpha,beta,                                  &
     &                nkeep,                                            &
     &                iprint,istop)
!c     ==================================================================
      use universal_const
      use scf_io, only : pot_S_mix,pot_B_mix,rho_S_mix ! ,rho_B_mix
      implicit none
!c     ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
      integer, parameter :: ipline=206
!c     ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
      character(120) :: lines(ipline)
      character mix*4
      character sname*10
!c     ***************************************************************
      real(8), intent(in) :: alat,boa,coa
      integer, intent(in) :: nbasis
      real(8), intent(in) :: atcon(:,:)   ! (ipcomp,ipsublat)
      integer, intent(in) :: iz(:,:)      ! (ipcomp,ipsublat) : zID
      integer, intent(in) :: komp(nbasis) ! (ipsublat)
      integer, intent(in) :: nspin
      real(8), intent(in) :: fetot,Temp,press
      integer, intent(in) :: npts,nq(:,:)  ! (3,ipmesh)
      real(8), intent(in) :: rms,efermi,vmtz,vdif
      character(32), intent(in) :: head
      character(*),  intent(in) :: xc_descr 
      real(8), intent(in) :: xvalws(:,:,:)  ! (ipcomp,ipsublat,ipspin)
      real(8), intent(in) :: zvalav
      integer, intent(in) :: imix
      real(8), intent(in) :: alpha,beta
      integer, intent(in) :: nkeep,iprint
      character(10), intent(in) :: istop
!      
      integer nrd,num,i,ii,kk,nrec
      real*8 ek,pk
!c parameters
      parameter (sname='keep')
      real(8), parameter :: small=(one/ten)**2
!c     ***************************************************************
      character(80), parameter :: keyline =                             &
     &          '  i      Free E (Ry)    P(Mbars)    maxerr'//          &
     &          '      ef      vmtz      a      b    m'
!c     ***************************************************************
!c
!c
      if( imix==rho_S_mix ) then
       mix='   s'                    ! simple mix
      else if( imix==pot_S_mix ) then
       mix='   p'                    ! potential simple mix
      else if( imix <=pot_B_mix ) then
       mix ='   P'
      else
         mix='   b'                    ! Broyden mix
!         if(imix.gt.rho_B_mix) mix='   B'      ! Broyden mix with file refreshing
      endif
!c     ***************************************************************
      nrd = 0
      num = 0
      nrec = 0
      pk = 99999.d0
      ek = 0
!c
      rewind nkeep
      do i = 1,ipline
       read(nkeep,'(a)',err=2,end=2) lines(i)
       nrd = i
       if(lines(i).eq.keyline) then
        read(nkeep,'(i3,f17.9,f12.6)',err=2,end=2) num,ek,pk
        exit
       end if
      end do
      if(nrd.gt.ipline) then
       nrd = 0
      endif
      rewind nkeep
      do i=1,nrd
       read(nkeep,*,err=2,end=2)
      end do
      do i=1,ipline
       read(nkeep,'(a)',err=2,end=2) lines(i)
       nrec = i
      end do
   2  continue
      rewind nkeep
!c     ***************************************************************
      if(iprint.ge.0) then
       write(6,'(''    Iterations read from scflog file'',t40,          &
     &           ''='',i3)') nrec
      end if
!c     ***************************************************************
!c
!CCC      num=mod(num,1000)+1
      num=min(num+1,999)

      write(nkeep,'('' System'',t20,'': '',a)') trim(head)
      write(nkeep,'('' Temperature (K)'',t20,'':'',f11.1)') Temp
      write(nkeep,'('' Lattice spacing'',t20,'':'',f10.5,               &
     &            5x,''B/A='',f7.4,3x,''C/A='',f7.4)') alat,boa,coa
      write(nkeep,'('' No. of E-pts'',t20,'':'',i4)') npts
      write(nkeep,'('' No. of BZ K-pts'',t20,'':'',3i4)')               &
     &             (nq(ii,1),ii=1,3)
      do i=2,size(nq,2)
        write(nkeep,'('' '',t20,'' '',3i4)')                            &
     &             (nq(ii,i),ii=1,3)
      end do
      write(nkeep,'('' No. of Sub-lats.'',t20,'':'',i3)') nbasis
      write(nkeep,'('' No. of Components'',t20,'':'',10i3)')            &
     &             (komp(ii),ii=1,nbasis)
      write(nkeep,'('' Atomic Numbers'',t20,'':'',10i3)')               &
     &             ((iz(kk,ii),kk=1,komp(ii)),ii=1,nbasis)
      write(nkeep,'('' Concentrations'',t20,'':'',10f10.5)')            &
     &             ((atcon(kk,ii),kk=1,komp(ii)),ii=1,nbasis)
      write(nkeep,'('' zvalav & vdif'',t20,'':'',2f10.5)')              &
     &                  zvalav,vdif
      if ( nspin == 1 ) then
        write(nkeep,'('' WS-charges       '',t20,'':'',10f10.5)')       &
     &             ((xvalws(kk,ii,1),kk=1,komp(ii)),ii=1,nbasis)
      else
        write(nkeep,'('' WS-charges (maj.)'',t20,'':'',10f10.5)')       &
     &             ((xvalws(kk,ii,1),kk=1,komp(ii)),ii=1,nbasis)
        write(nkeep,'('' WS-charges (min.)'',t20,'':'',10f10.5)')       &
     &             ((xvalws(kk,ii,nspin),kk=1,komp(ii)),ii=1,nbasis)
        write(nkeep,'('' WS-moments'',t20,'':'',10f10.5)')              &
     &             ((xvalws(kk,ii,1)-xvalws(kk,ii,nspin),               &
     &                                   kk=1,komp(ii)),ii=1,nbasis)
      end if
      write(nkeep,'(a)') '# '//trim(xc_descr)
      write(nkeep,'(a80)') keyline
!c
      write(nkeep,'(i3,f17.7,f12.6,e11.3,f9.5,f9.4,2(1x,f6.4),a4)')     &
     &      num,fetot,press,rms,efermi,vmtz,alpha,beta,mix
!!c  Can be commented - muffin-tin corrected energy
!       write(nkeep,'(10x,'' MTC '',f17.7,5x,f8.6)') ek+emtc,emtc

      write(nkeep,'(a)') (lines(i),i=1,nrec)

!c     ===============================================================
      if(istop.eq.sname) then
         call fstop(sname)
      endif
!c     ===============================================================
      return
      end
