!c
!c =====================================================================
      subroutine semcst(nqn,lqn,kqn,en,rv,r,x,rg,rf,h,z,c,              &
     &                  nitmax,tol,nmt,nlast,iter,ifail)
!c =====================================================================
!c
!c semi-core states solver ..........................bg...june 1990.....
!c .................................      corrected by DDJ January 1991.
!c .................................      ASA added by DDJ January 1991.
!c   Most important correction was matching condition and
!c normalization at the sphere boundary.
!c Energy eigenvalues are found by newton-raphson method matching at
!c the classical inversion point the f/g ratio.
!c The energy derivative of f/g is evaluated analytically:
!c     see rose's book, chapter 4, pages 163-169,
!c     and also jp desclaux code...................................
!c
!c *********************************************************************
!c calls: outws(invals) and inws
!c *********************************************************************
!c variables explanation:
!c nqn principal quantum number; lqn orbital quantum number.............
!c kqn kappa quantum number; en energy; rv potential in rydbergs times r
!c r radial log. grid; rg big component: rf small component.............
!c h: exp. step; z atomic number; nitmax number of iterations...........
!c tol: energy tolerance; nmt muffin-tin radius index...................
!c nws: last tabulation point; c speed of light in rydbergs.............
!c drg,drf: wavefunctions derivatives times r...........................
!c gam: first power for small r expansion; slp:slope at the origin......
!c dm: h/720; ..........................................................
!c
!c Necessary to handle ASA as well as MT case.        DDJ January 1991
!c nmt: muffin-tin (ASA) radius index....................................
!c nws: ASA radius index.................................................
!c nlast: last tabulation point for normalization (MUST be jws for ASA)
!c **********************************************************************
      implicit real*8  (a-h,o-z)
!c ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
      include 'mkkrcpa.h'
!c ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!c
      character sname*10,coerr*80
!c parameter
      integer ipdeq,ipdeq2
!c
      real*8    half,thrqt
!c ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
      parameter  (sname='semcst')
      parameter (ipdeq=5,ipdeq2=10)
      parameter (half=one/two,thrqt=three/four)
!c ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!c
      dimension rg(iprpts),rf(iprpts),rv(iprpts),r(iprpts)
      dimension x(iprpts),tmp(iprpts),qmp(iprpts)
      dimension drg(ipdeq2),drf(ipdeq2)
!c **********************************************************************
!c
!c
!c     initialize quantities
!c **********************************************************************
      iter=0
      dk=kqn
      dm=h/720.0d+00
!c
!c     for semicore states you want to fix the inversion point at
!c     the muffin-in, or ASA, radius. Also for this, set imm=1.
      invp=nmt
      imm=1
!c
!c     for MT case nmax=iprpts, but for ASA case nmax=jws.
!c     this sets the charge within the proper regions.
      nmax=nlast
!c
!c     no. of nodes for the current states big component
!c **********************************************************************
      nodes=nqn-lqn
!c
!c     first power of small r expansion
!c **********************************************************************
      gam=sqrt(dk*dk - four*z*z/(c*c))
!c
!c     slope of the wavefcns at the origine
!c **********************************************************************
      if(nodes-2*(nodes/2).ne.0) then
         sign= one
      else
         sign=-one
      endif
      slp=sign*kqn/iabs(kqn)
!c
!c     find the lowest energy limit
!c **********************************************************************
      lll=(lqn*(lqn+1))/2
      if (lll.ne.0) then
         elim=-z*z/(thrqt*nqn*nqn)
      else
         elim=(rv(1)+lll/r(1))/r(1)
         do 1 j=2,iprpts
         elim=dmin1((rv(j)+lll/r(j))/r(j),elim)
  1      continue
      endif
!c
!c     check potential
!c **********************************************************************
      if(elim.ge.0) then
         coerr=' v+l*(l+1)/r**2 always positive'
         call fstop(sname//':'//coerr)
      endif
!c **********************************************************************
      if(en.le.elim) en=elim*half
!c
!c     routine outws performs the outward integration
!c **********************************************************************
!c
  2   call outws(ipdeq,invp,rg,rf,rv,r,en,c,drg,drf,elim,z,             &
     &                     gam,slp,tol,imm,lll,dk,dm,nodes,nlast)
!c **********************************************************************
!c
!c     store the inversion point values
!c **********************************************************************
      rfm=rf(invp)
      rgm=rg(invp)
!c
!c **********************************************************************
!c     routine inwhnk performs the inward integration and
!c     sets free solution directly to Riccati-Hankel for r*g and r*f
!c **********************************************************************
!cALAM
      call inwhnk(invp,rg,rf,r,en,c,drg,drf,                            &
     &                 dk)
!c     call inws (ipdeq,invp,iprpts,rg,rf,rv,r,en,c,drg,drf,
!c    >                 dk,dm,nlast,imm)
!c
!c components match
!c **********************************************************************
      fnrm=rgm/rg(invp)
!c
!c check first the sign of the big component
!c **********************************************************************
      if (fnrm.lt.zero) then
         coerr='wrong big component change'
         call fstop(sname//':'//coerr)
      endif
!c **********************************************************************
      do 4 j=invp,iprpts
         rg(j)=rg(j)*fnrm
         rf(j)=rf(j)*fnrm
  4   continue
!c
!c energy derivative of the wvfcns "log. derivative"
!c **********************************************************************
      do 5 j=1,iprpts
         tmp(j)=rg(j)**2+rf(j)**2
  5   continue
!c
!c normalization needed to end of grid to account for tails of wave-fct.
!c this get energy update correctly.
      call qexpup(1,tmp,iprpts,x,qmp)
!c
      rose=qmp(iprpts)+h*r(invp)*(rfm*rfm-rf(invp)*rf(invp))/three      &
     &                +r(1)*(rg(1)**2+rf(1)**2)/(gam+gam+one)
!c
!c energy update
!c **********************************************************************
      de=rg(invp)*(rfm-rf(invp))*c/rose
!c     imm=0
      val=abs(de/en)
      if (val.le.tol) go to 8
  6   enew=en+de
      if(enew.lt.zero) go to 7
      de=de*half
      val=val*half
      if (val.gt.tol) go to 6
!c
!c just in case the energy becomes zero
!c **********************************************************************
!c     ifail=ifail+1
!c     if( ifail .lt. 4 ) return
      coerr='zero energy'
      call fstop(sname//':'//coerr)
!c **********************************************************************
!c
!c     not yet convergence: try again
!c **********************************************************************
  7   en=enew
!c     if (val.le.0.2d+00) imm=1
      iter=iter+1
      if(iter.le.nitmax) go to 2
!c
!c     not converged, too small tolerance or too small nitmax
!c **********************************************************************
      coerr='warning: too many energy iterations'
      write(6,*) ' nitmax=',nitmax
      write(6,*) ' tol=',tol,' val=',val
      write(6,*) sname, coerr
!c **********************************************************************
!c
!c     got convergence: exit
!c     normalize the wavefunctions
!c **********************************************************************
 8    call qexpup(1,tmp,nmax,x,qmp)
!c normalization needed to jws (WS radii)  so as to account for all
!c charge consistently throughout program, whether core or valence.
      rose=qmp(nmax)+h*r(invp)*(rfm*rfm-rf(invp)*rf(invp))/three        &
     &                +r(1)*(rg(1)**2+rf(1)**2)/(gam+gam+one)
!c
      gnrm=sqrt(rose)
      do 9 j=1,iprpts
         rg(j)=rg(j)/gnrm
         rf(j)=rf(j)/gnrm
  9   continue
!c
      ifail=0
!c     end of job: waiting for next state
!c **********************************************************************
      return
      end
