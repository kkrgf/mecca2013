!c
!c =====================================================================
      subroutine deepst(nqn,lqn,kqn,en,rv,r,x,rg,rf,h,z,c,              &
     &                  nitmax,tol,nws,nlast,iter,ifail)
!c =====================================================================
!c
!c core states solver ...............................bg...june 1990.....
!c energy eigenvalues are found by newton-raphson method matching at
!c the classical inversion point the f/g ratio. the energy derivative
!c of f/g is evaluated analitically: see rose's book, chapter 4, pages
!c 163-169, and also jp desclaux code...................................
!c
!c *********************************************************************
!c calls: outws(invals) and inws
!c *********************************************************************
!c variables explanation:
!c nqn principal quantum number; lqn orbital quantum number.............
!c kqn kappa quantum number; en energy; rv potential in rydbergs times r
!c r radial log. grid; rg big component: rf small component.............
!c h: exp. step; z atomic number; nitmax number of iterations...........
!c tol: energy tolerance; nmt muffin-tin radius index...................
!c nws: last tabulation point; c speed of light in rydbergs.............
!c drg,drf: wavefunctions derivatives times r...........................
!c gam: first power for small r expansion; slp:slope at the origin......
!c dm: h/720; ..........................................................
!c **********************************************************************
      implicit real*8  (a-h,o-z)
!c ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
      include 'mkkrcpa.h'
!c ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!c
      character sname*10,coerr*80
      integer    ipdeq,ipdeq2
!c ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
      parameter (ipdeq=5,ipdeq2=10)
      parameter  (sname='deepst')
!c ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!c
      dimension rg(iprpts),rf(iprpts),rv(iprpts),r(iprpts)
      dimension x(iprpts),tmp(iprpts),qmp(iprpts)
      dimension drg(ipdeq2),drf(ipdeq2)
!c **********************************************************************
!c
!c
!c     initialize quantities
!c **********************************************************************
      imm=0
      iter=0
      dk=kqn
      dm=h/720.0d+00
!c
!c     no. of nodes for the current states big component
!c **********************************************************************
      nodes=nqn-lqn
!c
!c     first power of small r expansion
!c **********************************************************************
      gam=sqrt(dk*dk-4.0d+00*z*z/(c*c))
!c
!c     slope of the wavefcns at the origine
!c **********************************************************************
      if(nodes-2*(nodes/2).ne.0) then
         sign= 1.0d+00
      else
         sign=-1.0d+00
      endif
      slp=sign*kqn/abs(kqn)
!c
!c     find the lowest energy limit
!c **********************************************************************
      lll=(lqn*(lqn+1))/2
      if (lll.ne.0) then
         elim=-z*z/(0.75d+00*nqn*nqn)
      else
         elim=(rv(1)+lll/r(1))/r(1)
         do 1 j=2,nlast
         elim=dmin1((rv(j)+lll/r(j))/r(j),elim)
  1      continue
      endif
!c
!c     check potential
!c **********************************************************************
      if(elim.ge.0) then
         coerr='   v+l*(l+1)/r**2 always positive'
         call fstop(sname//':'//coerr)
      endif
!c **********************************************************************
      if(en.le.elim) en=elim*0.5d+00
!c
!c     routine outws performs the outward integration
!c **********************************************************************
!c
  2   call outws(ipdeq,invp,rg,rf,rv,r,en,c,drg,drf,elim,z,             &
     &                     gam,slp,tol,imm,lll,dk,dm,nodes,nlast)
!c **********************************************************************
!c
!c     store the inversion point values
!c **********************************************************************
      rfm=rf(invp)
      rgm=rg(invp)
!c
!c     routine inws performs the inward integration
!c **********************************************************************
      call inws (ipdeq,invp,nmax,rg,rf,rv,r,en,c,drg,drf,               &
     &                 dk,dm,nlast,imm)
!c
!c components match
!c **********************************************************************
      fnrm=rgm/rg(invp)
!c
!c check first the sign of the big component
!c **********************************************************************
      if (fnrm.lt.0.0d+00) then
         coerr='wrong big component change'
         call fstop(sname//':'//coerr)
      endif
!c **********************************************************************
      do 3 j=invp,nmax
         rg(j)=rg(j)*fnrm
         rf(j)=rf(j)*fnrm
  3   continue
!c
!c energy derivative of the wvfcns "log. derivative"
!c **********************************************************************
      do 4 j=1,nmax
         tmp(j)=rg(j)**2+rf(j)**2
  4   continue
      call qexpup(1,tmp,nmax,x,qmp)
      rose=qmp(nmax)+h*r(invp)*(rfm*rfm-rf(invp)*rf(invp))/3.0d+00      &
     &                +r(1)*(rg(1)**2+rf(1)**2)/(gam+gam+1.0d+00)
!c
!c energy update
!c **********************************************************************
      de=rg(invp)*(rfm-rf(invp))*c/rose
      imm=0
      val=abs(de/en)
      if (val.le.tol) go to 7
  5   enew=en+de
      if(enew.lt.0.0d+00) go to 6
      de=de*0.5d+00
      val=val*0.5d+00
      if (val.gt.tol) go to 5
!c
!c just in case the energy becomes zero
!c **********************************************************************
!c     ifail=ifail+1
!c     if( ifail .lt. 5 ) return
      coerr=' zero energy'
      call fstop(sname//':'//coerr)
!c **********************************************************************
!c
!c     not yet convergence: try again
!c **********************************************************************
  6   en=enew
      if (val.le.0.2d+00) imm=1
      iter=iter+1
      if(iter.le.nitmax) go to 2
!c
!c     not converged, too small tolerance or too small nitmax
!c **********************************************************************
      coerr='WARNING: too many energy iterations, val='
      write(6,*) sname, coerr,real(val)
!c **********************************************************************
!c
!c     got convergence: exit
!c     normalize the wavefunctions
!c **********************************************************************
  7   nmax2=nmax
!c
!c wave-fct. normalization is required to be within sphere for ASA case
      if(nmax.gt.nws) nmax2=nws
!c
      call qexpup(1,tmp,nmax2,x,qmp)
      rose=qmp(nmax2)+h*r(invp)*(rfm*rfm-rf(invp)*rf(invp))/3.0d+00
!c
      gnrm=sqrt(rose)
      do 8 j=1,nmax
         rg(j)=rg(j)/gnrm
         rf(j)=rf(j)/gnrm
  8   continue
      if(nmax.lt.nlast) then
         do 9 j=nmax+1,nlast
            rg(j)=0.0d+00
            rf(j)=0.0d+00
  9      continue
      endif
!c
      ifail=0
!c     end of job: waiting for next state
!c **********************************************************************
      return
      end
