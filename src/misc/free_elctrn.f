      function gQfe(ef)
      use mecca_constants
      use mecca_types, only : RunState,IniFile
      use mecca_run, only : getMS
      implicit none
!      
!    to calculate number/energy of free electrons below Ef in the cell for L>lmax
!    i.e.  N(Ef) = Int{ f(e)*n(e) | e=0:Ef},  
!    n(e) = 1/(4*pi**2)*sqrt(e)*Integral{ over cell | L-sum_above_lmax[(2l+1)*j_l^2(sqrt(e)*r)] }
!    f(e) - temperature factor
!      
      real(8) :: gQfe
      real(8), intent(in) :: ef
      logical, external :: addFEforEf
      type(RunState), pointer :: M_S=>null()
      type(IniFile),  pointer :: inFile
!      real(8), parameter :: c0=(one/(six*pi*pi))
      real(8), parameter :: c1=(one/(four*pi*pi))
      real(8), parameter :: err0 = 1.d-10
      real(8), parameter :: cutoff = 20.d0
      real(8), external :: rFEint
      integer, parameter :: L_big=100
      logical, parameter :: zeroT = .false.
      real(8) :: x0,x1,err,dq
      real(8) :: rparam(5)
      real(8) :: Tempr,Rmax,pmax,Xmax,c_norm
      integer :: nsub,l,lmx1,l_inf
      integer :: ierr
      gQfe = zero
      if ( ef <= zero ) return  
      if ( .not. addFEforEf() ) return  
      M_S => getMS()
      inFile => M_S%intfile
      pmax = sqrt(ef)

      do nsub=1,inFile%nsubl

      Rmax = M_S%rs_box%rws(nsub)
      Xmax = Rmax*pmax

      c_norm = c1
      c_norm = c_norm*inFile%sublat(nsub)%ns * 2 ! due to spin

      lmx1   = inFile%lmax+1
      l_inf = max(8,min(L_big,int(ef*Rmax**2)+1))
      rparam(1) = dble(lmx1)
      rparam(2) = dble(l_inf)
      rparam(3) = Rmax

      Tempr = inFile%Tempr

        ! 0 < E < Ef
        do l=l_inf,3,-1   !  DEBUG !!! ---> lmx1,-1
        rparam(1) = dble(l)
        rparam(2) = dble(l)
        rparam(4) = ef
        rparam(5) = zero
        x0 = zero
        x1 = ef
        err = err0
        call dgaus8m(rFEint,x0,x1,rparam,err,dq,ierr)
        if(abs(ierr).gt.1) then
         write(*,*)
         write(*,*) ' ierr=',ierr
         write(*,*) ' ef,Tempr:',rparam(4),rparam(5)
         write(*,*) ' ERR=',err
         write(*,*) ' dq=',dq
         write(*,*)
         stop ' gQfe: integration error1'
        end if

       write(*,*) 'DEBUG gQfe l= ',l,c_norm*dq

        gQfe = gQfe + c_norm*dq

        end do
      if ( Tempr > zero ) then    ! non-zero temperature
!
        ! 0 < E < 2*Ef
        rparam(5) = Tempr/(2*ef)
        x0 = zero
        x1 = 2*ef
        l_inf = max(8,min(L_big,int(2*ef*Rmax**2)+1))
        rparam(2) = dble(l_inf)
        rparam(3) = Rmax
        rparam(4) = ef
        err = err0
        call dgaus8m(rFEint,x0,x1,rparam,err,dq,ierr)
        if(abs(ierr).gt.1) then
         write(*,*)
         write(*,*) ' ierr=',ierr
         write(*,*) ' ef,Tempr:',rparam(4),rparam(5)
         write(*,*) ' ERR=',err
         write(*,*) ' dq=',dq
         write(*,*)
         stop ' gQfe: integration error2'
        end if
        gQfe = gQfe + c_norm*dq
!
        ! 2*Ef < E < Inf 
        rparam(5) = Tempr
        x0 = two*ef
        x1 = ef+cutoff*Tempr
        rparam(3) = Rmax
        rparam(4) = ef
        if ( x1>x0 ) then
         err = err0
         call dgaus8m(rFEint,x0,x1,rparam,err,dq,ierr)
         if(abs(ierr).gt.1) then
          write(*,*)
          write(*,*) ' ierr=',ierr
          write(*,*) ' ef,Tempr:',rparam(4),rparam(5)
          write(*,*) ' ERR=',err
          write(*,*) ' dq=',dq
          write(*,*)
          stop ' gQfe: integration error3'
         end if
         gQfe = gQfe + c_norm*dq
        end if 
!DEBUGPRINT      
        write(*,*) ' DEBUG: N(ef)/cell=',sngl(gQfe),'***'
!DEBUGPRINT      
!
      end if 

      end do

      return
      end function gQfe

      function gFreeElctrnQ(ef)
      use mecca_constants
      use mecca_types, only : RunState,IniFile
      use mecca_run, only : getMS
      implicit none
!      
!    to calculate density of free electrons N below Ef, i.e.
!       N(Ef) = Int{ f(e)*n(e) | e=0:Ef}, n(e) = 1/(4*pi**2)*sqrt(e),
!                f(e) - temperature factor
!      
      real(8) :: gFreeElctrnQ
      real(8), intent(in) :: ef
      logical, external :: addFEforEf
      type(RunState), pointer :: M_S=>null()
      type(IniFile),  pointer :: inFile
      real(8), parameter :: c0=(one/(six*pi*pi))
      real(8), parameter :: c1=(one/(four*pi*pi))
      real(8), parameter :: err0 = 1.d-10
      real(8), parameter :: cutoff = 20.d0
      real(8), external :: fe_tempr_spectrum
      logical, parameter :: zeroT = .false.
      real(8) :: funpar(2),x0,x1,err,dq
      integer :: ierr
      gFreeElctrnQ = zero
      if ( ef <= zero ) return  
      if ( .not. addFEforEf() ) return  
      M_S => getMS()
      inFile => M_S%intfile
      gFreeElctrnQ = c0*sqrt(ef)**3
!      if ( inFile%Tempr == zero ) then
!        funpar(1) = -one         ! 0 < E < 2*Ef
!        funpar(2) = ef
!        funpar(3) = zero
!        x0 = zero
!        x1 = ef
!        err = err0
!        call dgaus8m(fe_tempr_spectrum,x0,x1,funpar,err,dq,ierr)
!        gFreeElctrnQ = c1*dq
!      end if
      if ( inFile%Tempr > zero ) then
!
        ! 0 < E < 2*Ef
        funpar(1) = ef
        funpar(2) = inFile%Tempr
        x0 = zero
        x1 = two*ef
        err = err0
        call dgaus8m(fe_tempr_spectrum,x0,x1,funpar,err,dq,ierr)
        if(abs(ierr).gt.1) then
         write(*,*)
         write(*,*) ' ierr=',ierr
         write(*,*) ' ef,Tempr:',funpar(1),funpar(2)
         write(*,*) ' ERR=',err
         write(*,*) ' dq=',dq
         write(*,*)
         stop ' gFreeElctrnQ: integration error 1'
        end if
        gFreeElctrnQ = gFreeElctrnQ + c1*dq
!
        ! 2*Ef < E < Inf 
        funpar(1) = ef
        funpar(2) = inFile%Tempr
        x0 = two*ef
        x1 = ef+cutoff*inFile%Tempr
        if ( x1>x0 ) then
         err = err0
         call dgaus8m(fe_tempr_spectrum,x0,x1,funpar,err,dq,ierr)
         if(abs(ierr).gt.1) then
          write(*,*)
          write(*,*) ' ierr=',ierr
          write(*,*) ' ef,Tempr:',funpar(1),funpar(2)
          write(*,*) ' ERR=',err
          write(*,*) ' dq=',dq
          write(*,*)
          stop ' gFreeElctrnQ: integration error 2'
         end if
         gFreeElctrnQ = gFreeElctrnQ + c1*dq
        end if 
!DEBUGPRINT      
!        write(*,*) ' DEBUG: N(ef)/cell=',sngl(gFreeElctrnQ),'***'
!DEBUGPRINT      
!
      end if 

      return
      end function gFreeElctrnQ

       function fe_tempr_spectrum(e,param)
       use mecca_constants, only : zero
       implicit none
       real(8) :: fe_tempr_spectrum
       real(8), intent(in) :: e
       real(8), intent(in) :: param(2)
       real(8) :: z,Fz,ef,T
       real(8), external :: fermiFun
       ef = param(1)
       T  = param(2)
       if ( ef<=zero .or. e<=zero ) then
           fe_tempr_spectrum = zero
           return
       end if    
       if ( T <= zero ) then    ! spectrum for T=0
        if ( e<= ef ) then
          fe_tempr_spectrum = sqrt(e)
        else
          fe_tempr_spectrum = zero
        end if   
       else                           ! spectrum(T) - spectrum(T=0)
        z = (e-ef)/T
        Fz = fermiFun(z)
        if ( z < 0.d0 ) then
         fe_tempr_spectrum = (Fz-1.d0)*sqrt(e)
        else
         fe_tempr_spectrum = Fz*sqrt(e)   
        end if
       end if
       return
       end function fe_tempr_spectrum

       function rFEint(e,param)
       use mecca_constants, only : zero,two,pi
       implicit none
       real(8) :: rFEint
       real(8), intent(in) :: e
       real(8), intent(in) :: param(5)
       real(8), parameter :: log2=log(2.d0)
       real(8), external :: fe_tempr_spectrum
       integer :: lmx1,l_inf,l
       real(8) :: Xmax
       complex(8) :: rho_max
       complex(8), allocatable, save :: bj(:),bn(:)

       lmx1  = param(1)
       l_inf = param(2)
       Xmax  = param(3)
       if ( allocated(bj) ) then
         if ( size(bj)<l_inf+1 ) then
          deallocate(bj,bn)
          allocate(bj(0:l_inf))
          allocate(bn(0:l_inf))
         end if 
       else  
         allocate(bj(0:l_inf))
         allocate(bn(0:l_inf))
       end if  

       rho_max = Xmax*sqrt(e)

       rFEint = zero
       call ricbes0(l_inf+1,rho_max,bj,bn)
       do l=l_inf,lmx1,-1
        rFEint = rFEint + dble(2*l+1)*Xmax**3/3*real(bj(l))**2 !DEBUG
       end do
       rFEint = rFEint*fe_tempr_spectrum(e,param(4:5))
       return
       end function rFEint
