!c
!c     ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      subroutine ord3v(v3out,vsqout,nv3,v3in,vsqin,iorder)
!c     =============================================================
!c
      implicit real*8 (a-h,o-z)
!c
!c +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!c
      integer iorder(:)    ! [1:ndimv]
!c
      real*8  v3out(:,:)   ! [1:ndimv,1:3]
      real*8  vsqout(:)    ! [1:ndimv]
      real*8  v3in(3)
      real*8  vsqin
!c
      if(nv3.eq.0) then
         vsqout(1)=vsqin
         iorder(1)=1
         do 1 ic=1,3
            v3out(1,ic) = v3in(ic)
 1       continue
         nv3=1
         go to 10
      else
         do 2 nv=1,nv3,+1
            if(vsqout(nv).lt.vsqin) then
               go to 2
            else
               do 3 nvm=nv3,nv,-1
                  vsqout(nvm+1)=vsqout(nvm)
                  iorder(nvm+1)=iorder(nvm)
                  do 4 ic=1,3
                     v3out(nvm+1,ic) = v3out(nvm,ic)
 4                continue
 3             continue
               vsqout(nv)=vsqin
               do 5 ic=1,3
                  v3out(nv,ic) = v3in(ic)
 5             continue
               nv3=nv3+1
               iorder(nv)=nv3
               go to 10
             endif
 2        continue
          vsqout(nv3+1)=vsqin
          do 6 ic=1,3
             v3out(nv3+1,ic) = v3in(ic)
 6        continue
          nv3=nv3+1
          iorder(nv3)=nv3
      endif
!c
 10   continue
!c
      return
!c
      end
