!c**************************************************************
      module pot_types
      use mecca_constants

      type, public :: POTFILE

       character(len=80) comment

       real*8  ztot
       real*8  alat
       real*8  zval
       real*8  efpot

       real*8  xst
       real*8  xmt
       integer jmt

       real*8  vr(iprpts)

       real*8  v0

       integer ntchg
       real*8  xvalsws
       real*8  chgtot(iprpts)
       integer numc
       integer ntcor
       integer nc(ipeval)
       integer lc(ipeval)
       integer kc(ipeval)
       real*8  ec(ipeval)
       character(len=5) lj(ipeval)
       real*8  chgcor(iprpts)
      end type POTFILE

      end module pot_types
!c**************************************************************

      program main

      use pot_types
      implicit real*8 (a-h,o-z)

      character*32 filename
      character*10 spin
      integer nfch
      parameter (nini=100)
      type(POTFILE) siteptn(nini,2)

      logical endflg

      nfch = 1

      write(*,*) ' filename?'
      read(*,*)  filename
      open(nfch,file=filename,status='old')

      write(*,*) ' total number of potentials in this file?'
      write(*,*) '    ("grep comp <filename> | wc")'
      read(*,*) nptn


      read(nfch,*)
      read(nfch,*) nspin,vdif

      if(nptn.ne.nptn/nspin*nspin) stop ' wrong NPTN or NSPIN'

      nptn = nptn/nspin

      if(nptn.gt.nini) stop ' error:  nptn.gt.nini'

      ef = 0.d0
      do i=1,nspin
       do k=1,nptn

       call readpot(nfch,siteptn(k,i),endflg)

       write(*,*) siteptn(k,i)%comment
       write(*,*) siteptn(k,i)%efpot
       write(*,*) siteptn(k,i)%ntcor,                                   &
     &            siteptn(k,i)%chgcor(siteptn(k,i)%ntcor)

       ef = ef + siteptn(k,i)%efpot

       end do
      end do

      close(nfch)

      ef = ef/(nspin*nptn)

      spin = ' '
      write(*,*) ' Averaged Ef=',ef
      write(*,*) ' There are ',nptn,' potentials:'
      do i=1,nspin
       if(nspin.gt.1.and.i.eq.1) spin = ' Spin Up '
       if(nspin.gt.1.and.i.eq.2) spin = ' Spin Dn '
       do k=1,nptn

       write(*,*) k,spin,nint(siteptn(k,i)%ztot),real(siteptn(k,i)%alat)

       end do
      end do

      write(*,*) ' New filename?'
      read(*,*) filename
      write(*,*) ' how many potentials/spin ',                          &
     &           '  do you want in the new file?'
      read(*,*) nptn

      open(nfch,file=filename)

      write(nfch,*) filename
      write(nfch,*) nspin,vdif
      write(*,*) ' itype,ispin for each component?'

      do i=1,nspin
       do k=1,nptn
      if(nspin.eq.1) then
       read(*,*) itype
       ispin = 1
      else
       read(*,*) itype,ispin
      end if
!c        write(nfch,*) k,i,siteptn(itype,ispin)%ztot
      siteptn(itype,ispin)%efpot = ef
      call writepot(nfch,siteptn(itype,ispin))
       end do
      end do
      end

!**************************************************************

      subroutine readpot(nfch,siteptn,endflg)

      use pot_types

      implicit none

      integer nfch
      type(POTFILE) :: siteptn
      logical endflg

      integer ndrpts,jmt0,j

      character*10 sname
      parameter (sname='readpot')

!c ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

!c     ==========================================================

      read(nfch,'(a)',end=100) siteptn%comment

      endflg = .false.

      read(nfch,'(f5.0,17x,f12.5,f5.0,e20.13)')                         &
     &  siteptn%ztot,                                                   &
     &  siteptn%alat,                                                   &
     &  siteptn%zval,                                                   &
     &  siteptn%efpot

!c     ==========================================================
!c     read in potential deck....................................

      read(nfch,*)                                                      &
     &  siteptn%xst,                                                    &
     &  siteptn%xmt,                                                    &
     &  siteptn%jmt

      jmt0  = siteptn%jmt
      ndrpts = size(siteptn%vr)

      read(nfch,'(4e20.13)')    (siteptn%vr(j),j=1,min(jmt0,ndrpts))
      if(jmt0.lt.ndrpts) then
       siteptn%vr(jmt0+1:ndrpts) = 0.d0
      else
       siteptn%jmt = ndrpts
      end if

      read(nfch,'(35x,e20.13)') siteptn%v0

!c     ==========================================================
!c     read in the charge density ...............................

      read(nfch,'(i5,e20.13)')                                          &
     & siteptn%ntchg,                                                   &
     & siteptn%xvalsws

      if(siteptn%ntchg.gt.0) then
       ndrpts = size(siteptn%chgtot)
       siteptn%ntchg = min(ndrpts,siteptn%ntchg)
       read(nfch,'(4e20.13)')                                           &
     &  (siteptn%chgtot(j),j=1,siteptn%ntchg)
       if(siteptn%ntchg.lt.ndrpts)                                      &
     &   siteptn%chgtot(siteptn%ntchg+1:ndrpts) = 0.d0
      endif

!c     ==========================================================
!c     read in core state definition ............................

      read(nfch,'(3i5)')                                                &
     & siteptn%numc,                                                    &
     & siteptn%ntcor

      if(siteptn%numc.gt.0) then
       siteptn%numc = min(size(siteptn%nc),siteptn%numc)
       do j=1,siteptn%numc
       read(nfch,'(3i5,f12.5,9x,a5)')                                   &
     &    siteptn%nc(j),                                                &
     &    siteptn%lc(j),                                                &
     &    siteptn%kc(j),                                                &
     &    siteptn%ec(j),                                                &
     &    siteptn%lj(j)
       enddo
      endif

!c     ==========================================================
!c     read in the core density cccccccccccccccccccccccc

      if(siteptn%ntcor.gt.0) then
       ndrpts = size(siteptn%chgcor)
       siteptn%ntcor = min(siteptn%ntcor,ndrpts)
       read(nfch,'(4e20.13)') (siteptn%chgcor(j),j=1,siteptn%ntcor)
       if(siteptn%ntcor.lt.ndrpts)                                      &
     &   siteptn%chgcor(siteptn%ntcor+1:ndrpts) = 0.d0
      endif

!c     ==========================================================

      return

100   continue
      endflg = .true.

      return
      end subroutine readpot

      subroutine writepot(nfch,siteptn)

      use pot_types

      implicit none

      integer nfch
      type(POTFILE) siteptn
      logical endflg

!c      integer ndrpts,jmt0,j
      integer j

      character*10 sname
      parameter (sname='writepot')

!c ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

!c     ==========================================================

      write(nfch,'(a)') siteptn%comment

      endflg = .false.

      write(nfch,'(f5.0,17x,f12.5,f5.0,e20.13)')                        &
     &  siteptn%ztot,                                                   &
     &  siteptn%alat,                                                   &
     &  siteptn%zval,                                                   &
     &  siteptn%efpot

!c     ==========================================================
!c     write in potential deck....................................

      write(nfch,'(17x,d24.16,d24.16,i5)')                              &
     &  siteptn%xst,                                                    &
     &  siteptn%xmt,                                                    &
     &  siteptn%jmt

      write(nfch,'(4e20.13)')    (siteptn%vr(j),j=1,siteptn%jmt)

      write(nfch,'(35x,e20.13)') siteptn%v0

!c     ==========================================================
!c     write in the charge density ...............................

      write(nfch,'(i5,e20.13)')                                         &
     & siteptn%ntchg,                                                   &
     & siteptn%xvalsws

      if(siteptn%ntchg.gt.0) then
       write(nfch,'(4e20.13)')                                          &
     &  (siteptn%chgtot(j),j=1,siteptn%ntchg)
      endif

!c     ==========================================================
!c     write in core state definition ............................

      write(nfch,'(3i5)')                                               &
     & siteptn%numc,                                                    &
     & siteptn%ntcor

      if(siteptn%numc.gt.0) then
       do j=1,siteptn%numc
       write(nfch,'(3i5,f12.5,9x,a5)')                                  &
     &    siteptn%nc(j),                                                &
     &    siteptn%lc(j),                                                &
     &    siteptn%kc(j),                                                &
     &    siteptn%ec(j),                                                &
     &    siteptn%lj(j)
       enddo
      endif

!c     ==========================================================
!c     write in the core density cccccccccccccccccccccccc

      if(siteptn%ntcor.gt.0) then
       write(nfch,'(4e20.13)') (siteptn%chgcor(j),j=1,siteptn%ntcor)
      endif

!c     ==========================================================

      return

      return
      end subroutine writepot
