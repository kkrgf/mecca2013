!BOP
!!ROUTINE: green0rs
!!INTERFACE:
      subroutine green0rs(lmax,kkrsz,                                   &
     &                  ij3,nj3,cgaunt,cfac,                            &
     &                  pdu,                                            &
     &                  green,                                          &
     &                  ndimnn,numnbi,                                  &
     &                  rsij,mapsnni,                                   &
     &                  invsw,                                          &
     &                  istop)
!!DESCRIPTION:
! calculates free electron Greens function, {\tt green},
! in real space in cluster approximation
! {\bv
!  Iatom -- central atom
!  numnbi -- 1 + number of NN for Iatom
!  green -- output G-matrix
!  rsij  -- array for inequivalent vectors (Rj-Rj`); rsij(*,1) = (0,0,0 !)
!
!  invsw.ne.2 :   green -- (*,*)-complex matrix of (Re,Im)
!       .eq.2 :   green -- (0:1,*,*) - real matrix
!                   green(0,*) -- Re{matrix}
!                   green(1,*) -- Im{matrix}
! \ev}
!!USES:
      use universal_const

!!DO_NOT_PRINT
      implicit none
!!DO_NOT_PRINT

!!ARGUMENTS:
      integer kkrsz
      integer     ij3(kkrsz*kkrsz,*)
      integer     nj3(*)
      real*8      cgaunt(kkrsz*kkrsz,*)
      complex*16   cfac(*)
      complex*16 pdu
      complex*16 green(*) ! output matrix (1:numnbi*kkrsz,1:numnbi*kkrsz)
      integer ndimnn,numnbi
      real*8   rsij(3,*)
      integer  mapsnni(ndimnn,numnbi)
      integer invsw
      character istop*10
!!REVISION HISTORY:
! Adapted - A.S. - 2013
!EOP
!
!BOC
!c
      real*8 rsij2(1)

!c     ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
      character    sname*10
      parameter    (sname='green0rs')
!c     ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

!      integer lclkkr,lcl2
!      parameter (lclkkr=(iplmax+1)*(iplmax+1))
!      parameter (lcl2=(2*iplmax+1))

!c  complex*16   ylmtmp((2*iplmax+1)**2)    -- working array
      complex*16   ylmtmp((2*lmax+1)*(2*lmax+1))

!c  complex*16   hnkltmp(2*iplmax+1)    -- working array
      complex*16   hnkltmp(0:(2*lmax+1)-1)

!c  working arrays
      complex*16 g((lmax+1)**2*(lmax+1)**2)
      complex*16 dlm((2*lmax+1)*(2*lmax+1))

!c      complex*16   xpr
      complex*16   gi

      integer i,k,i0,j0,l,l0,m,ia,lmax,kkrsz2

      real(8), parameter :: pi4 = four*pi
!c      real*8 time1,time2
!      real*8 zero
!      parameter (zero=0.d0)
!      complex*16 cone,sqrtm1
!      parameter (cone=(1.d0,0.d0))
      complex(8), parameter :: sqrtm1=(0.d0,1.d0)

!c     =================================================================
!c     for each E-point and each Iatom set up in Real-Space
!c           free-electron Greens function - green = g0
!c     =================================================================

!      if(kkrsz.gt.lclkkr) then
!       write(6,*) ' KKRSZ=',kkrsz,' LCLKKR=',lclkkr
!       call fstop(sname//' kkrsz > lclkkr; increase lclkkr...')
!      end if
      if((lmax+1)**2.ne.kkrsz) then
       write(6,*) ' LMAX=',lmax,' KKRSZ=',kkrsz
       call fstop(sname//' (lmax+1)**2 .NE. kkrsz ???')
      end if

      kkrsz2 = kkrsz*kkrsz
!c
      call zerooutC(green,(kkrsz*numnbi)**2)

!c  Diagonal blocks: G0(L1,i1|L2,i1) = 0 for any L1,L2

!c  --------------------------------------------------------------------

!c        For site off-diagonal part    ................................. !.

      do i0=1,numnbi
       do j0=1,numnbi
        if(i0.ne.j0) then
         ia = mapsnni(i0,j0)

!c  dlm real space calculation...
!c  --------------------------------------------------------------------

         call hrmplnm(zero,zero,zero,rsij(:,ia),1,rsij2,ylmtmp,1,       &
     &              -10,1,2*lmax,1)

!c
!c ylmtmp = Ylm
!c
         call hankel(pdu*sqrt(rsij2(1)),hnkltmp(0),2*lmax)

!c        lm = 0
         do l=0,2*lmax
          l0 = l*( l + 1 ) + 1
!c  M=0
          dlm(l0)=hnkltmp(l)*ylmtmp(l0)
!c  M<>0
          do m=1,l
           dlm(l0+m) = hnkltmp(l)*(ylmtmp(l0+m))
           dlm(l0-m) = hnkltmp(l)*(ylmtmp(l0-m))
          enddo
         enddo
!c
!c  calculate g(l,lp)  ...................................
!c
         do i=1,kkrsz2
          gi = (0.d0,0.d0)
          do k=1,nj3(i)
           gi=gi+cgaunt(i,k)*dlm(ij3(i,k))
          enddo
          g(i)=pdu*pi4*cfac(i)*gi
         enddo

         if(invsw.eq.2) then
          call matrix1(g,green,i0,j0,kkrsz*numnbi,kkrsz,cone)
         else
          call matrix(g,green,i0,j0,kkrsz*numnbi,kkrsz,cone)
         end if
        end if
       enddo
      enddo
!c
      if (istop.eq.sname) call fstop(sname//' DEBUG')
!c
      return
!EOC
      end subroutine green0rs

