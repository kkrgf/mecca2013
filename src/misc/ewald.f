!BOP
!!ROUTINE: ewald
!!INTERFACE:
      subroutine ewald(x,y,z,e,eoeta,powe,lmax,aij,                     &
     &                 dlm,eta,                                         &
     &                 hplnm,ndimr,ndimlm,                              &
     &                 xknlat,ndimks,conr,                              &
     &                 rns,nrns,ndimrs,dqint,ndimdqr,ndx,               &
     &                 nkns,d00,icount)
!!DESCRIPTION:
! calculates for point {\bf k}={\tt (x,y,z)} various data required 
! for Ewald summation method \\

! (part of free-electon Green's function calculations)
!
!!USES:
      use universal_const
!!REVISION HISTORY:
! Adapted - A.S. - 2013
!EOP
!
!BOC
!
!DEBUG      use raymethod
!CAB      implicit none
      implicit real*8 (a-h,o-z)
      character sname*10
      parameter (sname='ewald')
!c     ==================================================================
      integer l
      integer icount
!cab                                   icount -- instead of (i1,i2)
      integer lmax
!c      integer lmaxt2
      integer nkns
      integer nrns
      integer ndx((2*lmax+1)**2)
!c     ==================================================================
      real*8 aij(3)
      complex*16 conr((2*lmax+1)**2)
      real*8 rns(ndimrs,*)
      real*8 xknlat(ndimks,*)
      real*8 eta
!c      real*8 rspv(3)
      real*8 x
      real*8 y
      real*8 z
!c     ==================================================================
!CAB      complex*16 ylmkpkn(ipxkns,ipdlj)
      complex*16, allocatable  ::  ylmkpkn(:,:)
      integer erralloc

      complex*16 dlmr((2*lmax+1)**2)
      complex*16 dlmk((2*lmax+1)**2)

      complex*16 dlm((2*lmax+1)**2)
      complex*16 powe((2*lmax+1)**2)
      complex*16 dqint(ndimdqr,*)
      complex*16 d00
      complex*16 e
      complex*16 eoeta

      complex*16 dlmknd

      complex*16 hplnm(ndimr,ndimlm)

      complex*16 expkrn(nrns),dlmrnp,dlmrnm,ylmrlp
      complex*16 dqexp(nrns)

      real*8 xkpkn2(nkns)
      complex*16 tmparr(nkns)
!c     ==================================================================
!c     Calculate the r-space part of dlm.
!c     Modified to reduce time of ewald method......ddj & was April 1994
!c     (see GETTAU)
!c     ==================================================================

!c     ================================================================== !=
!c     Calculate  exp(i*k*rs) for real-space translation vectors.
!c                    k=(x,y,z), rs==rns()-aij
!c     ================================================================== !=

!DEBUG      call begintiming("g0 // make real dlm")
      expkrn(1:nrns) = exp(dcmplx(zero,                                 &
     &               x*rns(1:nrns,1)+y*rns(1:nrns,2)+z*rns(1:nrns,3)))

      lm = 0
      do l=0,2*lmax
       l0 = l*( l + 1 ) + 1
!c  M=0
       lm = lm+1
       dqexp(1:nrns) = dqint(1:nrns,l+1)*expkrn(1:nrns)
       dlmr(l0)=sum(dqexp(1:nrns)*hplnm(1:nrns,lm))*conr(l0)
!c  M<>0
       do m=1,l
        lm = lm+1
        dlmr(l0+m)=sum(dqexp(1:nrns)*hplnm(1:nrns,lm))*conr(l0)
!        dlmr(l0-m)=sum(dqexp(1:nrns)*conjg(hplnm(1:nrns,lm)))*conr(l0)*(-1)**m
        dlmr(l0-m)=dot_product(hplnm(1:nrns,lm),dqexp(1:nrns))*conr(l0)*&
     &             (-1)**mod(m,2)
       enddo
      enddo
!DEBUG      call endtiming()

!c     ==================================================================
!c     Calculate the k-space part of dlm.
!c     ==================================================================


!DEBUG      call begintiming("g0 // allocate k-space Ylm")
      allocate(                                                         &
     &         ylmkpkn(1:nkns,1:(2*lmax+1)**2),                         &
     &  stat=erralloc)

      if(erralloc.ne.0) then
        write(6,*) ' NKNS=',nkns
        write(6,*) ' (2*LMAX+1)**2=',(2*lmax+1)**2
        write(6,*) ' ERRALLOC=',erralloc
        call fstop(sname//' :: ALLOCATION MEMORY PROBLEM ')
      end if
!DEBUG      call endtiming()

!DEBUG      call begintiming("g0 // fill-in k-space Ylm")
      call hrmplnm( x, y, z, xknlat, ndimks, xkpkn2, ylmkpkn, nkns,     &
     &               1, nkns, 2*lmax , 1)
!c     ==================================================================
!DEBUG      call endtiming()

!DEBUG      call begintiming("g0 // make k-space dlm")
      etainv = one/eta
      do nk=1,nkns
        dot1 = dot_product(xknlat(nk,1:3),aij(1:3))
        tmparr(nk) = eoeta*                                             &
     &     exp(dcmplx(-xkpkn2(nk)*etainv,dot1))/(xkpkn2(nk)-e)
      enddo

!c     ==================================================================
!c     calculate dlmk
!c     ==================================================================
      do nd=1,(2*lmax+1)**2
       dlmk(nd) = sum(tmparr(1:nkns)*ylmkpkn(1:nkns,nd))
      enddo
!DEBUG      call endtiming()
!c     ==================================================================
!c     If  the calculation of dlm is on a diagonal sublattice then
!c     add in d00
!c     ==================================================================

!DEBUG      call begintiming("g0 // get total dlm")
      if( icount.eq.1 ) then
        dlmk(1)=dlmk(1) + d00
      endif
!c     ==================================================================
!c     Calculate total dlm by appropiately summing over
!c     k and r space parts
!c     ==================================================================
      do l=1,(2*lmax+1)**2
        dlm(l)=powe(l)*( dlmk(l)+dlmr(l) )
      enddo
!DEBUG      call endtiming()

      deallocate(ylmkpkn,stat=erralloc)

        return
!EOC
        end subroutine ewald


!BOP
!!ROUTINE: rlylm
!!INTERFACE:
      subroutine rlylm(rsn,nrsn,ndimr,lmax,hplnm,ndimrhp)
!!DESCRIPTION:
! calculates real-space harmonical polinomials $ R^l Y_{lm}(R), m \leq 0 $
! {\bv
!    inputs: rsn,nrsn,lmax
!            (ndimrhp,ndimr -- dimensions)
!    output: hplnm
! \ev}

!!USES:
      use universal_const
!EOP
!
!BOC
      implicit none

      integer nrsn,ndimr,lmax,ndimrhp
      real*8     rsn(ndimr,3)
      complex*16 hplnm(ndimrhp,*)

      complex*16 ylmrl((2*lmax+1)*(2*lmax+1))
      real*8 rns(3)
!      real*8 zero
!      parameter (zero=0.d0)
      real*8     rns2(1)
      integer i,lm,l,m,l0

      call hrmplnm(zero,zero,zero,rns,1,rns2,ylmrl,1,                   &
     &                   0, 0, 2*lmax , 1)

      do i = 1,min(nrsn,ndimrhp)
        rns(1:3) = rsn(i,1:3)
        call hrmplnm(zero,zero,zero,rns,1,rns2,ylmrl,1,                 &
     &                   1, 1, 2*lmax , 1)
        lm = 0
        do l=0,2*lmax
            l0 = l*( l + 1 ) + 1
!c
!c            store only for non-negative M
!c
            do m=0,l
             lm = lm+1
             hplnm(i,lm) = ylmrl(l0+m)
            end do
        end do
      end do

      return
      end subroutine rlylm

!BOP
!!ROUTINE: hrmplnm
!!INTERFACE:
      subroutine hrmplnm( x, y, z, rpts, ndimr, r2pts, ylm, ndimy,      &
     &                     iset, n_vecs, lmax , istrg)
!!DESCRIPTION:
! calculates {\tt ylm}, i.e. either spherical harmonics $ Y_{lm} $
! (if iset=-10) or harmonical polynomials $ r^l Y_{lm} $;
! storage format for {\tt rpts} is defined by {\tt istrg} (it is 
! rpts(3,n\_vecs) if istrg<0, otherwise it is rpts(ndimr,3) )
!
!!USES:
      use universal_const
!EOP
!
!BOC
      implicit none
!c
      integer    lmax
!c      integer    icount
      integer    iset
      integer    n_vecs
      integer    i,i1,i2,istep,iix,iiy,iiz
      integer    m
      integer    l
      integer    lm
!c      integer    k
      integer    ndimr, ndimy, istrg
!c
      real*8     rpts(*)
      real*8     r2pts(n_vecs)
      real*8     p(0:2*lmax+1,0:2*lmax+1)
      real*8     x
      real*8     y
      real*8     z
      real*8     xx
      real*8     yy
      real*8     zz
      real*8     vrho
      real*8     rmod,rl
      real*8     fpi
!c      real*8     rf
      real*8     cth
      real*8     sth
      real*8     fac
      real*8     a
      real*8     t
      real*8     cd
      real*8     sgm
      real*8     tol
      parameter(tol=1.0d-08)
!c
      real*8 cosphase(0:2*lmax)
      real*8 sinphase(0:2*lmax)
      complex*16 ylm(ndimy,*)
!      complex*16 cone
!      parameter(cone=(1.0d0,0.0d0))
!      real*8 zero,one
!      parameter (zero=0.d0,one=1.d0)

      parameter (fpi = 4.d0*pi)

      real*8 cosylm,sinylm,cosph0,sinph0
      real(8), allocatable, save :: al(:),cdl(:,:)

      integer, save :: l0 = 0

      if(l0<2*lmax+1) then
       if ( allocated(al) ) deallocate(al)
       if ( allocated(cdl) ) deallocate(cdl)
      end if    

      if( .not.allocated(al) ) then
       allocate(al(0:2*lmax))
       allocate(cdl(2*lmax,0:2*lmax))
       do l = 0,2*lmax
        al(l)  = sqrt( ( 2*l + 1 )/fpi )
       end do
       do l = 0,2*lmax
        a = al(l)
        cd = one
        do m = 1,l
          t = ( (l + 1) - m )*( l + m )
          cd = cd/t
          cdl(m,l) = sqrt(cd)*a
        end do
       end do
       l0 = 2*lmax+1
      end if
!c
!c     Calculate Complex Spher. Harms for "n_vecs" vectors [r(n_vecs,i)]  !:.
!c
        if(istrg.ge.0) then    ! rpts[1:ndimr,1:3]
         i1 = 1
         i2 = 0
         istep = ndimr
        else                   ! rpts[1:3,1:n_vecs]
         i1 = 3
         i2 = -2
         istep = 1
        end if

      do i = 1,n_vecs
!c
!c     Calculate sin(theta); cos(theta) : ..........................
!c
        iix = i1*i+i2
        iiy = iix+istep
        iiz = iiy+istep
        xx = x + rpts(iix)
        yy = y + rpts(iiy)
        zz = z + rpts(iiz)

        vrho = xx*xx + yy*yy
        rmod = vrho + zz*zz
        r2pts(i) = rmod
        rmod = sqrt( rmod )
        vrho = sqrt( vrho )
!c
        if( rmod .le. tol ) then
          cth = one
          sth = zero
        else
          cth = zz/rmod
          sth = sqrt(max(zero,one - cth*cth))
        endif
!c
!c                                iset=-10 --> calculation of Ylm only
        if(iset.eq.-10) rmod = one
!c
        if( vrho .lt. tol ) then
          cosphase(0:lmax) = one
          sinphase(0:lmax) = zero
        else

!c
         vrho = one/vrho
         xx = xx*vrho
         yy = yy*vrho

          cosph0 = one
          sinph0 = zero

          do m = 1,lmax
!cab            phase(m)=phase(m-1)*dcmplx( xx, yy )
           cosphase(m) = cosph0*xx-sinph0*yy
           sinphase(m) = sinph0*xx+cosph0*yy
           cosph0 = cosphase(m)
           sinph0 = sinphase(m)
          enddo
!c
        endif
!c
!c     Generate associated legendre functions for m > 0 :...........
!c
        fac = one
!c
        do m = 0,lmax
          fac = -( m + m - 1 )*fac
          p(m,m) = fac
          p(m+1,m) = ( (m + m -1) + 2 )*cth*fac
!c
!c     Recurse upward in l :.....................................
!c
          do l = m+2,lmax
            p(l,m) = ( ( l + (l - 1) )*cth*p(l-1,m) - ( m + (l - 1) )   &
     &             *p(l-2,m) )/( l - m )
          enddo                                 ! end do loop over l
!c
          fac = fac*sth
!c
        enddo                                   ! end do loop over m
!c
!c     Calculate the complex spherical harmonics :..................
!c
        rl = one
        do l = 0,lmax
!c
!c          al(l)  = sqrt( ( 2*l + 1 )/fpi )
!c
          lm = l*( l + 1 ) + 1
          ylm(i,lm) = al(l)*p(l,0)*rl
          sgm = -one
!c
          do m = 1,l

            sinylm = sinphase(m)*(rl*cdl(m,l)*p(l,m))
            cosylm = cosphase(m)*(rl*cdl(m,l)*p(l,m))

!cab            ylm(i,lm+m) = (rl*cdl(m,l)*p(l,m))*phase(m)
            ylm(i,lm+m) = dcmplx(cosylm,sinylm)
            ylm(i,lm-m) = sgm*conjg(ylm(i,lm+m))
!c***            ylm(i,lm-m) = dcmplx(cosylm*sgm,-sinylm*sgm)

            sgm = -sgm
!c
          enddo                            ! end do loop over m
!c

          rl = rmod*rl

        enddo                              ! end do loop over l
!c
      enddo                                ! end do loop over vectors

!c
      return
!EOC
      end subroutine hrmplnm


!CAB      subroutine rlylma(aij,rsn,nrsn,lmax,ndimr,ndimlm,hplnm,ylmrl)
!CABc
!CABc        inputs: aij,rsn,nrsn,lmax
!CABc                (ndimr -- dimensions)
!CABc        output: hplnm
!CABc
!CABc
!CABc     to calculate real-space harmonical polinomials
!CABc                  R**l*Y_lm(R), m.ge.0
!CABc              (to be used in Ewald method)
!CABc
!CAB      implicit none
!CAB
!CABc     =============================================================== !====
!CAB
!CAB      integer nrsn,ndimr,ndimlm,lmax
!CAB
!CAB      real*8     aij(3)
!CAB      real*8     rsn(ndimr,3)
!CAB      complex*16 hplnm(ndimr,ndimlm)
!CAB
!CAB      complex*16 ylmrl(*)
!CAB      real*8 zero
!CAB      parameter (zero=0.d0)
!CAB      real*8     rns(3),rns2
!CAB      integer i,icount,l,m,lm,l0
!CAB
!CAB
!CAB          do i = 1,nrsn
!CAB           rns(1)  = rsn(i,1) - aij(1)
!CAB           rns(2)  = rsn(i,2) - aij(2)
!CAB           rns(3)  = rsn(i,3) - aij(3)
!CAB
!CAB           call harmplnm(zero,zero,zero,rns,rns2,ylmrl,
!CAB     *                   1, 1, 2*lmax ,1, 1)
!CAB
!CAB           lm = 0
!CAB           do l=0,2*lmax
!CAB            l0 = l*( l + 1 ) + 1
!CABc
!CABc            store only for non-negative M
!CABc
!CAB            do m=0,l
!CAB             lm = lm+1
!CAB             hplnm(i,lm) = ylmrl(l0+m)
!CAB            end do
!CAB           end do
!CAB          end do
!CAB
!CAB        return
!CAB        end
!CAB
!CAB
!CAB

!BOP
!!ROUTINE: rlylmb
!!INTERFACE:
      subroutine rlylmb(ia,hplnm,ndimrhp,                               &
     &                  nrsn,np2r,                                      &
     &                  aij,rsnij,ndimrij,rsn,ndimr1,                   &
     &                  edu,pdu,eta0,irecdlm,                           &
     &                  dqint,dqint1,ndimdqr,                           &
     &                  lmax,hplnm2,ndimr2,lmhp)
!!DESCRIPTION:
! calculates real-space harmonical polinomials $ R^l Y_{lm}(R), m \leq 0 $
! for multi-site cell and specific energy 
! {\bv
!    inputs: ia,hplnm, ..., lmax
!            (ndim...  -- dimensions)
!    output: hplnm2
! \ev}
! (part of free-electon Green's function calculations)
!
!!USES:
      use universal_const
!!REVISION HISTORY:
! Adapted - A.S. - 2013
!EOP
!
!BOC
      implicit none

!c     ================================================================== !=

      integer ia,ndimrhp,ndimrij,ndimr1,ndimr2,lmax,lmhp,ndimdqr

      complex*16 hplnm(ndimrhp,*)
      integer    np2r(*),nrsn
      real*8     aij(3)
      real*8     rsnij(ndimrij,3)
      real*8     rsn(ndimr1,3)
      complex*16   edu,pdu
      real*8       eta0
      integer      irecdlm
      complex*16 dqint(ndimdqr,*)
      complex*16 dqint1(ndimr2,*)
      complex*16 hplnm2(ndimr2,lmhp)

      complex*16 ylmrl((lmax+1)*(lmax+1))
      real*8 rns(3),rns2(1),rsq2
      integer i,lm,l,m,l0,ir,lmx2p1

      complex*16   dqtmp(0:2*lmax),xpr
      real*8 rsp

!CAB      integer isave/0/
!CAB      save isave

       if(ia.le.ndimrhp) then
!       if input rsnij = Rn + aij, then just collect
!         vectors + sphericals harm. into a single array for this atom
        do i=1,nrsn
         ir = np2r(i)
         rsn(i,1:3) = rsnij(ir,1:3)
         hplnm2(i,1:lmhp) = hplnm(ir,1:lmhp)
        end do
       else
!        if input rsnij = Rn, then we need to construct Rn + aij
!           into a list and build spherical harm.
        do i = 1,nrsn
         ir = np2r(i)
         rsn(i,1:3) = rsnij(ir,1:3) - aij(1:3)
         call hrmplnm(zero,zero,zero,rsn(i,1:3),1,rns2,ylmrl,1,         &
     &                   1, 1, 2*lmax , 1)
         lm = 0
         do l=0,2*lmax
            l0 = l*( l + 1 ) + 1
            do m=0,l
             lm = lm+1
             hplnm2(i,lm) = ylmrl(l0+m)
            end do
         end do
        end do
       end if

!CAB       if(isave.lt.ia) then
!CAB        write(90,*) ' RLYLMB::'
!CAB        write(90,*) ia,nrsn,ndimrij,ndimr1,ndimr2
!CAB        write(90,*)
!CAB        write(91,*) ' RLYLMB::'
!CAB        write(91,*) ia,ndimrhp,lmax,lmhp
!CAB        write(91,*)
!CAB        do l=1,nrsn
!CAB         rns2 = rsn(l,1)**2+rsn(l,2)**2+rsn(l,3)**2
!CAB         write(90,1000) (rsn(l,i)/(2*3.1415927),i=1,3),
!CAB     *                   rns2/(2*3.1415927)**2
!CAB         write(91,1000) (hplnm2(l,lm),lm=1,4)
!CAB1000    format(4f12.5)
!CAB        end do
!CAB        isave = max(ia,isave)
!CAB       end if

       ! similar to above with regard to input vecs, only this code
       ! deals with the integration fac. for ewald sum or
       ! hankel fn for real space eval.
       if(ia.le.ndimdqr) then
        do l=1,2*lmax+1
         dqint1(1:nrsn,l)=dqint(np2r(1:nrsn),l)
        end do
       else
        lmx2p1 = 2*lmax+1
        rsp = -2.d0
        do i = 1,nrsn
         ir = np2r(i)
         rns(1:3) = rsnij(ir,1:3) - aij(1:3)
         rsq2 =  dot_product(rns,rns)
         if( abs(rsq2-rsp) .ge. 1.0d-6 ) then
          rsp = rsq2
          if(irecdlm.eq.0) then
           call intfac(rsq2,edu,eta0,2*lmax,dqint1(i,1:lmx2p1))
          else
           xpr=pdu*sqrt(rsp)
           call hankel(xpr,dqint1(i,1:lmx2p1),2*lmax)
          end if
         else
          dqint1(i,1:lmx2p1)=dqint1(i-1,1:lmx2p1)
         endif
        end do
       end if

      return
!EOC
      end subroutine rlylmb
