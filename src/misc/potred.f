!c
!c     cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      subroutine potred(jmt,rmt,jws,rws,xr,rr,h,xstart,                 &
     &                   vr,                                            &
     &                   rhotot,corden,                                 &
     &                   xvalsws,ztot,zcor,zvals,zcors,                 &
     &                   numc,nc,lc,kc,ec,                              &
     &                   alat,efpot,                                    &
     &                   initialize,ebot,                               &
     &                   ibot_cont,iset_b_cont,mtasa,iprint,istop)
!c     ==================================================================
!c
      implicit real*8 (a-h,o-z)
!c
!c ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
      include 'mkkrcpa.h'
!c ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!c
      character jttl2*80
      character lj(ipeval)*5
      character istop*10
      character sname*10
      character coerr*80
!c
      integer   nc(ipeval)
      integer   lc(ipeval)
      integer   kc(ipeval)
      integer   iset_b_cont
!c
      real*8    ec(ipeval)
      real*8    xr(iprpts)
      real*8    rr(iprpts)
      real*8    xrold(2*iprpts)
      real*8    rrold(2*iprpts)
      real*8    vr(iprpts)
      real*8    vrold(2*iprpts)
      real*8    rhotot(iprpts)
      real*8    corden(iprpts)
      real*8    chgold(2*iprpts)
      real*8    corold(2*iprpts)
      real*8    a(iprpts)
      real*8    b(iprpts)
      real*8    v0
      real*8    qmtnew
      real*8    qmtold
      real*8    tol
      real*8    ebot,ebot_read
!c
!c ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
      parameter (tol=(one/ten)**5)
      parameter (sname='potred')
!c ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!c set number of points up to sphere boundary, i.e. m.t. or a.s.a
!c NOTE: For Total Energy calculation (incrs-1) must be divisible by 4
!c ---------------------------------------------------------------------- !--
!c set log-grid starting point
        xstart=-11.13096740d+00
!c
      if(initialize.eq.0) then
           if(mtasa.le.0) then
            jmt=iprpts-50            ! additional points used in "mixing"
           else
            jmt=iprpts-38            ! additional points used in "mixing"
            jws=jmt
           endif
!c
        xlast=log(rmt)
        h=(xlast-xstart)/(jmt-1)
        do j=1,iprpts
           xr(j)=xstart+(j-1)*h
           rr(j)=exp(xr(j))
!c
!c set number of pts. to w.s. radius
           if(mtasa.le.0 .and. rr(j).lt.rws) jws= j + 1
        enddo

        if(jws.ge.iprpts-1) then
          coerr=' jws greater than iprpts-1'
          write(6,'('' jws='',i3,2x,''iprpts='',i3)') jws,iprpts
          call fstop(sname//':'//coerr)
       endif
      endif
      if(iprint.ge.1) then
        write(6,'(''  potred: jmt,h,rr(1),rmt'',i7,3e14.6)')            &
     &                          jmt,h,rr(1),rmt
      endif
!c
!c initialize changed so not to redo grid
      initialize=1
!c     ----------------------------------------------------------
      call zeroout(vrold,2*iprpts)
!c     ----------------------------------------------------------
      call zeroout(chgold,2*iprpts)
!c     ----------------------------------------------------------
      call zeroout(corold,2*iprpts)
!c     ----------------------------------------------------------
!c     ==========================================================
!c     read in potential deck....................................
      read(22,'(a80)',end=100) jttl2
      read(22,'(f5.0,13x,f12.5,f5.0,f12.5,e20.13)',err=100,end=100)     &
     &          azed,aa,zcd,ebot_read,efpot
      read(22,*) xst,xmt,jmt0
      read(22,'(4e20.13)') (vrold(j),j=1,jmt0)
      read(22,'(35x,e20.13)') v0
      if(iprint.ge.1) then
        write(6,'('' potred: jttl2'',a80)') jttl2
        write(6,'('' potred: azed,aa,zcd,efpot'',4e12.4)')              &
     &                       azed,aa,zcd,efpot
        write(6,'('' potred: xst,xmt,jmt0'',9x,2e20.13,i5)')            &
     &                       xst,xmt,jmt0
        write(6,'('' potred: v0'',e12.4)') v0
      endif
!c
      if(abs(ztot-azed).gt.tol) then
         write(6,'('' potred: ztot.ne.azed:'',2d14.6)')                 &
     &                         ztot,azed
         call fstop(sname)
      endif
      if(abs(zcor-zcd).gt.tol) then
         write(6,'('' WARNING: potred: zcor.ne.zcd; Make it equal'',    &
     &              2d14.6)')                                           &
     &                                 zcor,zcd
        zcor=zcd
        zcors=zcor
        zvals=ztot-zcor
!CDEBUG         call fstop(sname)
      endif
!c     ==========================================================
!c     generate grid for potential read-in ......................
!c     set the energy zero correctly for maj./min. spins.........
      hh=(xmt-xst)/(jmt0-1)
      do j=1,2*iprpts
         xrold(j)=xst+(j-1)*hh
         rrold(j)=exp(xrold(j))
      enddo
      do jj=jmt0+1,2*iprpts
        if(rrold(jj).gt.rmt) then
!c
         a0 = vrold(jmt0)
         a1 = (vrold(jmt0-2)-4.0d0*vrold(jmt0-1)+3.0d0*vrold(jmt0))/    &
     &                             (2*hh)
         a2 = (vrold(jmt0-2)-2.0d0*vrold(jmt0-1)+vrold(jmt0))/          &
     &                             (2*hh*hh)
!c
         a2 =0.5d0*a2
         do j=jmt0+1,jj                  ! "Taylor expansion"
          deltax = xrold(j)-xrold(jmt0)
          vrold(j) = a0 + a1*deltax + a2*deltax*deltax
         end do
         exit
        end if
      end do
      do j=1,2*iprpts
         vrold(j)=vrold(j) - v0*rrold(j)
      enddo
!c
!c     ==========================================================
!c     read in the charge density ...............................
      read(22,'(i5,e20.13)') ntchg,xvalsws
      if(iprint.ge.1) then
         write(6,'(i5,e20.13)') ntchg,xvalsws
      endif
      if(ntchg.gt.0) then
         read(22,'(4e20.13)') (chgold(j),j=1,ntchg)
      endif
!c     read in core state definition ............................
      read(22,'(3i5)') numc,ntcor
      if(iprint.ge.2) then
         write(6,'('' potred: numc,ntcor'',3i5)') numc,ntcor
      endif
      if(numc.gt.0) then
        do j=1,numc
         read(22,'(3i5,f12.5,9x,a5)') nc(j),lc(j),kc(j),ec(j),lj(j)
        enddo
        if(iprint.ge.2) then
          do j=1,numc
          write(6,'('' potred: nc,lc,kc,ec,lj'',3i3,e12.4,2x,a5)')      &
     &        nc(j),lc(j),kc(j),ec(j),lj(j)
          enddo
        endif
      endif
!c     ==========================================================
!c     read in the core density cccccccccccccccccccccccc
      if(ntcor.gt.0) then
         read(22,'(4e20.13)') (corold(j),j=1,ntcor)
      endif
!c     ==========================================================
!c     interpolate chg density,cor density onto rsw mesh
      if(iprint.ge.2) then
         iup=max(jmt0,ntchg)
         iup=max(iup,ntcor)
         write(6,'(''  potred: prior interp:'',2i5)')
         write(6,'(''  potred: number of points iup='',i5)') iup
         write(6,'(''  potred: j,r,v(r),chgold(r),corold(r)'')')
         write(6,'(i4,4e12.4)')                                         &
     &   (j,rrold(j),vrold(j),chgold(j),corold(j),j=1,iup,01)
      endif

      do j=1,jmt
         vr(j)=ylag(xr(j),xrold,vrold,0,3,jmt0,iex)
      enddo
      do j=jmt+1,iprpts
         vr(j)= -v0*rr(j)
      end do
      if(ntchg.gt.0) then
         do j=1,jws
            rhotot(j)=ylag(xr(j),xrold,chgold,0,3,ntchg,iex)
         enddo
      endif
      if(ntcor.gt.0) then
         do j=1,jws
            corden(j)=ylag(xr(j),xrold,corold,0,3,ntcor,iex)
         enddo
      endif

!CAB      if ( abs(alat-aa).gt.1.d-5
      if ((abs(alat-aa).gt.1.d-5.or.mtasa.eq.1)                         &
     &          .and.  ntchg.gt.0)  then

!c
         do j=1,ntchg
           a(j)=(chgold(j)-corold(j))*rrold(j)
         enddo
!c
!c    ===========================================
         call simpun(xrold,a,ntchg,1,b)
!c    ===========================================
         qmtold=b(jmt0)
!c
         do j=1,jws
           a(j)=(rhotot(j)-corden(j))*rr(j)
         enddo
!c    ===========================================
         call simpun(xr,a,jws,1,b)
!c    ===========================================
         qmtnew=b(jmt)
!c
         if(abs(qmtnew).gt.1.d-14) then
          fac=qmtold/qmtnew
         else
          fac = 1.d0
         end if

         deltq = xvalsws*(fac-1.d0)
         xvalsws = xvalsws+deltq
         do j=1,jws
           rhotot(j)=fac*rhotot(j)+(1.0d0-fac)*corden(j)
           x = rr(j)/rws
           vr(j) = vr(j) + deltq*x*(3.d0-x*x)
!c
!c   3/2*(Q^{new}-Q^{old})/R*{1-r^2/(3 R^2)}  -- electrostatic potential
!c   correction due to changes in charges
!c
         enddo
         efpot = efpot + 2.d0*deltq/rws
!c
      endif


!c SUFFIAN, move to input file
        if(ibot_cont.ne.0)then
!c           if ( abs(alat-aa).gt.1.d-5)then
              iset_b_cont = 1
!c           else
!c              iset_b_cont = 0
!c              ebot = ebot_read
!c           endif
        else
              iset_b_cont = 0
!C              ebot = ebot_read
        endif

!c
      if(iprint.ge.1) then
        write(6,'(''  potred: after interp:'',2i5)')
        write(6,'(''  potred: j,r,v(r),vrold(r) '')')
        write(6,'(i4,3e16.7)')(j,rr(j),vr(j),vrold(j),j=1,jws,60)
      endif
!c
!c     ============================================================
      if (istop.eq.sname) then
         call fstop(sname)
      else
         return
      endif
!c
100   coerr =  ' EOF of POT-FILE????'

      return
      end
!c ====================END POTRED ==================
