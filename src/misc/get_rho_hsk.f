!BOP
!!MODULE: common_rho_hsk
!!INTERFACE:
      module common_rho_hsk
!!DESCRIPTION:
! data container for HSK (atomic) Schroedinger equation solver
! 
!!PUBLIC MEMBER FUNCTIONS:
! subroutine dealloc_rho_hsk
! subroutine alloc_rho_hsk
! subroutine g_rho_hsk
!
!!REVISION HISTORY:
! Initial Version - A.S. - 2013
!EOP
!
!BOC
           implicit none
           integer, parameter :: nmx = 1001
           integer, parameter :: ndgand = 1
           real(8) :: vrout(nmx,2,ndgand)
           real(8) :: vrin(nmx,2,ndgand)
           real(8) :: drv(nmx,2)
           real(8) :: excpo(nmx,2)
           real(8) :: excen(nmx)
           real(8), allocatable, target, save :: r_hsk(:,:,:)
           real(8), allocatable, target, save :: rho_hsk(:,:,:,:)

!DEBUGPRINT           integer :: dprint=-1   ! debug print
           integer :: dprint= 0   ! debug print

!EOC
           contains

!BOP
!!IROUTINE: dealloc_rho_hsk
!!INTERFACE:
           subroutine dealloc_rho_hsk
!EOP
!
!BOC
             if ( allocated(rho_hsk) ) deallocate(rho_hsk)
             if ( allocated(r_hsk) ) deallocate(r_hsk)
!EOC
           end subroutine dealloc_rho_hsk

!BOP
!!IROUTINE: alloc_rho_hsk
!!INTERFACE:
           subroutine alloc_rho_hsk(ndrpts,ndcomp,nsubl,nspin)
! Initial Version - A.S. - 2013
!EOP
!
!BOC
           integer, intent(in) :: ndrpts,ndcomp,nsubl,nspin
           call dealloc_rho_hsk()
           allocate( r_hsk(ndrpts,ndcomp,nsubl) ); r_hsk=0
           allocate( rho_hsk(ndrpts,nspin,ndcomp,nsubl) ); rho_hsk=0
!EOC
           end subroutine alloc_rho_hsk

!BOP
!!IROUTINE: g_rho_hsk
!!INTERFACE:
           subroutine  g_rho_hsk( r,rho )
!!DESCRIPTION:
! provides radial mesh and charge density (for atoms)
!
!!ARGUMENTS:
           real(8), pointer :: r(:,:,:)
           real(8), pointer :: rho(:,:,:,:)
!!REVISION HISTORY:
! Initial Version - A.S. - 2013
!EOP
!
!BOC
           if ( associated(r) ) then
               nullify(r)
           end if   
           if ( associated(rho) ) then
               nullify(rho)
           end if   
           if ( allocated(r_hsk) ) then
             r => r_hsk
           end if
           if ( allocated(rho_hsk) ) then
             rho => rho_hsk
           end if
           return
!EOC
           end subroutine g_rho_hsk

      end module common_rho_hsk

!C======================================================================
!BOP
!!ROUTINE: get_rho_hsk
!!INTERFACE:
      subroutine get_rho_hsk(icon,zed,xmom,numsp,maxit,alfa,r,rchg,ichg,&
     &                      factor,mesh,nsize,ichgm,rho)
!!DESCRIPTION:
!   fjp version of the herman-skillman (hsk) program    jan/feb 1987
!
!!USES:
      use common_rho_hsk, only : excpo,excen,nmx,dprint,ndgand

!!DO_NOT_PRINT
           implicit real*8   (a-h,o-z)
           integer, parameter :: icdim=50
!!DO_NOT_PRINT

!!PRIVATE MEMBER FUNCTIONS:
! subroutine sortev2
! subroutine xsetup
! subroutine config
!!ARGUMENTS:
           integer, intent(in) :: icon
           real(8), intent(in) :: zed,xmom
           integer, intent(in) :: numsp,maxit
           real(8), intent(in) :: alfa
           real(8), intent(out) :: r(nmx)
           real(8), intent(out) :: rchg(0:icdim)
           integer, intent(out) :: ichg(0:icdim)
           real(8), intent(in) :: factor
           integer, intent(in) :: mesh,nsize,ichgm
           real(8), intent(out) :: rho(nmx,2)
!!REVISION HISTORY:
! Modified By A. Alam    Oct/Nov 2007
! Revised - A.S. - 2013
!EOP
!
!BOC
           parameter (one=1.0d0,two=2.0d0,three=3.0d0)
           parameter (third=one/three,twoth=two/three)

           character*7 abad,fndt,tble

           dimension x(nmx),rv(nmx,2),temp(nmx)
           dimension q2(nmx),tmp(nmx)
           dimension occ(20,2),ncore(20,2),lcore(20,2),numcor(2),       &
     &                    elcnum(2),ecore(20,2),snlo(nmx)
           dimension rvnew(nmx,2),rhoold(nmx,2)
           dimension occup(20),ncoup(20),lcoup(20),ecoup(20)
           dimension occdn(20),ncodn(20),lcodn(20),ecodn(20)
           dimension toten(50)

           character  sname*11
           parameter (sname='get_rho_hsk')

           integer i,j,k,n,ns,iter
           real(8) :: al_rho
           real(8) :: vrtol=0.008d0,rhotol=0.002d0
          character(2) symb
          character(15) elname

!           real*8      vrout(1001,2,4)
!           real*8      vrin(1001,2,4)
!           common/e1/fndt,tble
!           real*8      drv
!           common/drvdr/drv(1001,2)
!           real*8      excpo,excen
!           common/excorr/excpo(1001,2),excen(1001)
!!!           common/vout/vrout(1001,2,4)
!!!       common/vin/vrin(1001,2,4)
!c _____________________________________________________________________
               equivalence (occup,occ(1,1))
               equivalence (occdn,occ(1,2))
               equivalence (ncoup,ncore(1,1))
               equivalence (ncodn,ncore(1,2))
               equivalence (lcoup,lcore(1,1))
               equivalence (lcodn,lcore(1,2))
               equivalence (numcup,numcor(1))
               equivalence (numcdn,numcor(2))
               equivalence (ecoup,ecore(1,1))
               equivalence (ecodn,ecore(1,2))
!c _____________________________________________________________________
         data etop/1.d-6/,thresh/1.d-7/
         fndt='foundit'
         tble='trouble'
!c /////////////////////////////////////////////////////////////////////
!CALAM           mesh=1001
!CALAM           nsize=40
!CALAM           nblock=mesh/nsize
!c /////////////////////////////////////////////////////////////////////
!c      icon  is zero  for starting from thomas-fermi atom
!cNOT IMPLEMENTED      icon  is one   for continuing for potential stored in oldfl
!cNOT IMPLEMENTED      icon  is two   for changing the electron configuration
!c      zed is the atomic number
!cNOT IMPLEMENTED      xmom is the moment;   up spins minus down spins
!c      numsp is one   for a non-magnetic calculation
!c      maxit is the number of iterations (negative for atom)
!c      alfa is the mixing parameter
!c /////////////////////////////////////////////////////////////////////
!c _____________________________________________________________________
!c      call redin(icon,zed,xmom,numsp,maxit,alfa,iunin,iunot)
!c _____________________________________________________________________
!c
!C       zup=.5*(zed+xmom)
!c
!c /////////////////////////////////////////////////////////////////////
!c         xsetup contructs r grid
!c         thofer sets up an intial approximation to the potential
!c               and charge density using latter's fit of the
!c               thomas-fermi atom
!c =====================================================================
           zup=0.5d0*(zed+xmom)
           call xsetup(r,rchg,ichg,factor,mesh,nsize,ichgm,zed)
!c =====================================================================
!DELETE!           if(icon.eq.0) then
!DELETE           if(icon.ne.0) then
!DELETE            call thofer(r,rv,rhoold,zed,mesh,numsp)
!DELETE           else
!               
            call getDfltPot(zed,nmx,r,rv(:,1),icon)
            rv(1:nmx,2) = rv(1:nmx,1)
!               
!DELETE            write(*,*) ' get_rho_hsk: icon=',icon,'???'
!DELETE            stop 'NOT IMPLEMENTED'
!DELETE           end if 
!c =====================================================================
              do 6 i=1,numsp
              zd=(i-1)*zed+(3-2*i)*zup
!c =====================================================================
              call config(zd,numcor(i),ncore(1,i),lcore(1,i),occ(1,i)   &
     &                          ,ecore(1,i))
!c =====================================================================
    6         continue

        iter=0
   20   continue
        iter=iter+1
        if ( alfa > 0 ) then      
          al_rho = min(1.5*alfa,0.5d0)
        else
          al_rho = 0.9d0
        end if    
!              if(icon.eq.0) then
        itermx=max(1,iter-2)
!              else
!                    itermx=iter
!              endif
!c _____________________________________________________________________
        call savin(itermx,rv,numsp)

        qsum=0.d0
        do 35 ns=1,numsp
        elcnum(ns)=0.d0
             do 21 k=1,nmx
   21        rho(k,ns)=0.d0
!c =====================================================================
             do 30 n=1,numcor(ns)
             en=ecore(n,ns)
             lambda=lcore(n,ns)
             nfl=ncore(n,ns)
             em=etop
!c =====================================================================
!c   bound -- solves the schrodinger equation: finds bound states
!c =====================================================================
             call bound(abad,fndt,tble,zed,r,rv(1,ns),mesh,nsize,ichgm, &
     &                  en,thresh,nfl,lambda,temp,kkk,em,q2,qi)
!c =====================================================================
             ecore(n,ns)=en
!c =====================================================================
             if(abad.eq.fndt) then
                  elcnum(ns) = elcnum(ns) + occ(n,ns)
                  qsum=qsum-(3-numsp)*occ(n,ns)*qi
                  do 22 k=1,kkk
   22             snlo(k)=max(0.d0,temp(k))
                  if(kkk.eq.mesh) go to 24
                  kk1=kkk+1
                  do 23 k=kk1,mesh
   23             snlo(k)=0.d0
   24             continue
                  xxx=occ(n,ns)
                  do 25 k=1,kkk
   25             rho(k,ns)=rho(k,ns)+xxx*snlo(k)
             endif
!c ---------------------------------------------------------------------
   30        continue
!c ---------------------------------------------------------------------
!0717        rho(1,ns) = min(rho(1,ns),max(0.d0,                             &
!0717     &       (rho(2,ns)*(r(3)-r(1))-rho(3,ns)*(r(2)-r(1)))/(r(3)-r(2))))
   35   continue
!c ---------------------------------------------------------------------
        elcore=elcnum(1)+elcnum(numsp)
!DEBUG
        rms = zed
        if ( itermx>1 ) then
          do i=2,mesh
            if ( r(i) > 0.8d0 ) then
          rms=maxval(abs(rho(i:mesh,1)-rhoold(i:mesh,1)))
              exit
            end if
          end do
!0717          rho(1:mesh,1:numsp) = rhoold(1:mesh,1:numsp)*(1-al_rho) +     &
!0717     &                           rho(1:mesh,1:numsp)*al_rho
        end if    
!DEBUG        
!c =====================================================================
        call potgen(r,rvnew,rho,zed,mesh,nsize,numsp)
!c =====================================================================
        enxc=0.d0
        ecorr=0.d0
        do 79 ns=1,numsp
!c ---------------------------------------------------------------------
            do 32 i=1,mesh
   32       temp(i)=rho(i,ns)*(4.d0*excen(i)-3.d0*excpo(i,ns))
            call intqud(temp,tmp,r,nblock,nsize)
            enxc=enxc+(3-numsp)*tmp(mesh)
!c
!c ----------------------------- correction terms to make --------------
!c ------------------------- the total energy variational --------------
!c
            temp(1)=0.d0
            do 56 i=2,mesh
   56       temp(i)=(rv(i,ns)-rvnew(i,ns))/r(i)
            call derv5_ini(temp,tmp,r,mesh)
            do 57 i=2,mesh
   57       temp(i)=r(i)*rho(i,ns)*tmp(i)
            call intqud(temp,tmp,r,nblock,nsize)
            ecorr=ecorr+(3-numsp)*tmp(mesh)
   79   continue
!c ---------------------------------------------------------------------
        call savout(itermx,rvnew,numsp)
!c ---------------------------------------------------------------------
        tote=qsum+enxc
        tote=tote+ecorr
        toten(iter)=tote
!c  ---------------      mix potentials     ------------------------
            if ( iter<=2 ) then
             alf=abs(alfa)/5
            else 
             alf=abs(alfa)
            end if 
!            if ( iter<=1 ) rms=0.d0
!1707            rms = 0   !0717
            vrms=0.d0
            do 80 ns=1,numsp
            do 80 k=1,mesh
!               if ( iter<=1 ) then   ! DEBUG
!               rms=rms+(rhoold(k,ns)-rho(k,ns))**2
!               end if
               vrms=vrms+((rho(k,ns)*rvnew(k,ns)-                       &
     &                                  rhoold(k,ns)*rv(k,ns))/zed)**2
               rhoold(k,ns)=rho(k,ns)
   80       continue
!            rms=sqrt(rms/(mesh*numsp))
            vrms=sqrt(vrms/(mesh*numsp))
            if (dprint.ge.1 ) then
              write(6,'('' iter,max_rhoerr,v_rms='',i3,2d14.7)')        &
     &                    iter,rms,vrms
            end if 
!c =====================================================================
            ndim=min(ndgand,itermx)
            call dgand(rv,ndim,numsp,alf)
!c =====================================================================

            if ( itermx<maxit ) then
              if ( iter<=1 ) go to 20
              if ( vrms>vrtol ) go to 20
              if ( rms>rhotol ) go to 20
            end if

        if (dprint.ge.0 ) then
          call atomIDc(zed,symb,elname)
          write(6,'(/3x,a,'': iter,max_rhoerr,v_rms='',i3,2d14.7)')     &
     &                    symb,iter,rms,vrms
        end if 
!CALAM===================================================================
        do i=1,numsp
       call sortev2(ecore(1,i),lcore(1,i),occ(1,i),ncore(1,i),numcor(i))
        enddo
        if (dprint.ge.1 ) then
         do i=1,numsp
          write(6,'(''     ///////////////////////////////////'')')
          write(6,'(''      Eigenvalues:'',t20,''n'',t25,''l'',t42,     &
     &     ''energy'',8x,''occ.'')')
          do j=1,numcor(i)
!c         nel= (3-numsp)*iabs(kc(j))
!c         indx= lc(j) + iabs(kc(j))
!c       write(6,'(t18,i3,t23,i3,t28,i3,t35,d20.13,3x,i1,a4,i5)')
           write(6,'(t18,i3,t23,i3,t35,f16.8,3x,f5.1)')                 &
     &     ncore(j,i),lcore(j,i),ecore(j,i),2.0*occ(j,i)
          enddo
         enddo
!         close(99)
        end if 
!CALAM===================================================================
      if ( dprint.ge.0 ) then  
       write(6,*) 
      end if
      return

!EOC
      CONTAINS 

!c /////////////////////////////////////////////////////////////////////
!DELETE
!DELETE       subroutine thofer(r,v,rho,zed,mesh,numsp)
!DELETE       implicit none
!DELETE       real*8, intent(in) :: r(nmx)
!DELETE       real*8, intent(out) :: v(nmx,2),rho(nmx,2)
!DELETE       real*8, intent(in) :: zed
!DELETE       integer, intent(in) :: mesh
!DELETE       integer, intent(in) :: numsp
!DELETE
!DELETE       real*8, parameter :: one=1.0d0,two=2.0d0,three=3.0d0,four=4.0d0
!DELETE       real*8, parameter :: pi=3.14159265358979d0,fact=1.0d+3
!DELETE       real*8, parameter :: rf=four/(three*pi),oneth=one/three
!DELETE       integer i
!DELETE       real*8 fac,rr,root,rvozin,scl
!DELETE!c
!DELETE!c     v   is r*potential
!DELETE!c    rho  is 4pi r**2 * density
!DELETE!c =====================================================================
!DELETE       if(zed.le.10.0d0)then
!DELETE          fac=fact
!DELETE       else
!DELETE          fac=1.0d0
!DELETE       endif
!DELETE
!DELETE         scl=fac*(zed**oneth)/0.88534138d0
!DELETE!c
!DELETE       v(1,1)=-two*zed
!DELETE       rho(1,1)=0.0d0
!DELETE       v(1,numsp)=-two*zed
!DELETE       rho(1,numsp)=0.0d0
!DELETE!c =====================================================================
!DELETE       do 10 i=2,mesh
!DELETE       rr=r(i)*scl
!DELETE       root= sqrt(rr)
!DELETE       rvozin=one            + 0.02747 *root                            &
!DELETE     &      + 1.243  *rr    - 0.14860 *rr*root                          &
!DELETE     &      + .2302  *rr*rr + 0.007298*rr*rr*root                       &
!DELETE     &      + .006944*rr*rr*rr
!DELETE       v(i,1)=two*zed/rvozin
!DELETE       rho(i,1)=rf*v(i,1)* sqrt(v(i,1)*r(i))
!DELETE       v(i,1)=-v(i,1)
!DELETE       if(numsp.le.1) go to 10
!DELETE          v(i,2)=v(i,1)
!DELETE          rho(i,2)=rho(i,1)
!DELETE   10  continue
!DELETE!c =====================================================================
!DELETE      return
!DELETE      end subroutine thofer
!DELETE
!
!BOP
!!IROUTINE: sortev2
!!INTERFACE:
      subroutine sortev2(ec,lc,kc,nc,nlvl)
!EOP
!
!BOC
      implicit none
      integer nlvl
      real*8 ec(nlvl),kc(nlvl),tmp
      integer tmp2
      integer lc(nlvl),nc(nlvl),jmin,j,jj
!c----------------------------------------------------------------------- !-----

      do j=1,nlvl
         jmin=j
         do jj=j,nlvl
            if (ec(jj).lt.ec(jmin)) jmin = jj
         enddo
         if (j.ne.jmin) then
            tmp=ec(jmin)
            ec(jmin)=ec(j)
            ec(j)=tmp

            tmp2=lc(jmin)
            lc(jmin)=lc(j)
            lc(j)=tmp2

            tmp=kc(jmin)
            kc(jmin)=kc(j)
            kc(j)=tmp

            tmp2=nc(jmin)
            nc(jmin)=nc(j)
            nc(j)=tmp2
         endif
      enddo

!EOC
      end subroutine sortev2

!c _____________________________________________________________________
!BOP
!!IROUTINE: xsetup
!!INTERFACE:
        subroutine xsetup(r,rchg,ichg,factor,mesh,nsize,ichgm,zed)
!EOP
!
!BOC
        implicit none
        integer, parameter :: icdim=50
        real*8 r(nmx),rchg(0:icdim)
        integer ichg(0:icdim)
        real*8 factor
        integer mesh,nsize,ichgm
        real*8 zed

!        dimension x(1051)

        real*8, parameter :: three=3.0d0,third=1.0d0/three
        integer i,ir,idoub
        real*8 h0,rstart
!c =====================================================================

        nblock=mesh/nsize


!cSUFFIAN COMMENT OUT TO PERFORM CHECK
!c       h0=factor/((zed)**(1.0/3.0))
        h0=factor
        r(1)=0.0d0
        rstart=r(1)
        rchg(0)=rstart
        ir=1
        ichg(0)=1
        do idoub=1,ichgm
          do i=1,nsize
             ir=ir+1
             r(ir)=i*h0+rstart
          enddo
          ichg(idoub)=ir
          rstart=r(ir)
          rchg(idoub)=rstart
          h0=2.d0*h0
        enddo
!C=====    Previous radial mesh=======================================
!c        write(99,*) mesh,nsize,nblock
!C        c=0.075
!C        dx=.000005
!C        i=1
!C        x(1)=0.
!C        r(1)=0.
!C        do 50 j=1,nblock
!C           dr=c*dx
!C           imk=(j-1)*nsize+1
!C           ximk=x(imk)
!C           do 40 k=1,nsize
!C                i=i+1
!C                x(i)=ximk+k*dx
!C                r(i)=c*x(i)
!C   40      continue
!C           if(dr.lt.0.2)dx=dx+dx
!C   50   continue
!Cc =====================================================================
!C        if(i.le.mesh) return
!C        write(6,*)' trouble in setup:  mesh problem'
      return
!EOC
      end subroutine xsetup
!c =====================================================================
!BOP
!!IROUTINE: config
!!INTERFACE:
       subroutine config(zed,nstate,nprin,l,fill,e)
!EOP
!
!BOC
       implicit real*8   (a-h,o-z)
       real*8 zed
       integer nstate
       dimension nprnc(20),ll(20),nprin(20),l(20),fill(20),e(20)
       character*1 nc(2,20)
       data nc/'1','s', '2','s', '2','p', '3','s',                      &
     &         '3','p', '4','s', '3','d', '4','p',                      &
     &         '5','s', '4','d', '5','p', '6','s',                      &
     &         '4','f', '5','d', '6','p', '7','s',                      &
     &         '5','f', '6','d', '7','p', '8','s'/
       data nprnc/1,2,2,3,3,4,3,4,5,4,5,6,4,5,6,7,5,6,7,8/
       data  ll  /0,0,1,0,1,0,2,1,0,2,1,0,3,2,1,0,3,2,1,0/
       integer i,nm,ndeg
       real*8  dif
!c
       nm=0
       nstate=0
       do 10 i=1,20
       l(i)=ll(i)
       nprin(i)=nprnc(i)
       e(i)=-3.d0*zed*zed/nprin(i)**6
   10  fill(i)=0.d0
   20  nstate=nstate+1
!c =====================================================================
       if(nstate.gt.20) then
          write(6,*) ' stopping in subroutine config:  nstate problem'
          stop
       endif
!c =====================================================================
       ndeg=2*ll(nstate)+1
       nm=nm+ndeg
       fill(nstate)=ndeg
       if(zed-nm.gt.0.d0) go to 20
       dif=nm-zed
       fill(nstate)=ndeg-dif
!c =====================================================================
       zed=sum(fill(1:nstate))
      return
!EOC
      end subroutine config
!c----------------------------------------------------------------------- !--
      end subroutine get_rho_hsk

!c _____________________________________________________________________
!c /////////////////////////////////////////////////////////////////////
!BOP
!!ROUTINE: intqud
!!INTERFACE:
      subroutine intqud(y,yi,x,nb,nin)
!!DESCRIPTION:
! 4-points integration

!!DO_NOT_PRINT
      implicit real*8   (a-h,o-z)
!!DO_NOT_PRINT

!!ARGUMENTS:
      integer nb,nin       ! intend(in)
      real*8 y(nin*nb+1)   ! intend(in)
      real*8 yi(nin*nb+1)  ! intend(out)
      real*8 x(nin*nb+1)   ! intend(in)
!!REVISION HISTORY:
! Adapted - A.S. - 2013
!EOP
!
!BOC
      integer i,j
      nbig=nin*nb+1
      yi(1)=0.d0
      do 10 i=2,nbig
          top=x(i)
          bot=x(i-1)
          j=max(i-2,1)
          j=min(j,nbig-3)
          y1=y(j)
          y2=y(j+1)
          y3=y(j+2)
          y4=y(j+3)
          x1=x(j)
          x2=x(j+1)
          x3=x(j+2)
          x4=x(j+3)
          a1=y1
          a2=(y2-y1)/(x2-x1)
          a3=(((y3-y1)/(x3-x1))-a2)/(x3-x2)
          a4=((((((y4-y1)/(x4-x1))-a2)/(x4-x2)))-a3)/(x4-x3)
          b1=a1-a2*x1+a3*x1*x2-a4*x1*x2*x3
          b2=a2-a3*(x1+x2)+a4*(x2*x3+x1*x2+x1*x3)
          b3=a3-a4*(x1+x2+x3)
          b4=a4
          xint =  b1  + (0.5d0*b2+0.25d0*b4*(top**2+bot**2))*(top+bot)
          xint = xint + b3*(top**2+top*bot+bot**2)/3.d0
          yi(i)=yi(i-1)+xint*(top-bot)
   10  continue
!c
      return
      end subroutine intqud
!c _____________________________________________________________________
!c /////////////////////////////////////////////////////////////////////
!BOP
!!ROUTINE: smpold
!!INTERFACE:
      subroutine smpold(y,yi,r,nb,nin)
!!DESCRIPTION:
! (Simpson's integration)
!c =====================================================================
!!ARGUMENTS:
!!PARAMETERS:
!!REVISION HISTORY:
! Initial Version - unknown
!!REMARKS: 
! not used
!EOP
!
!BOC
      implicit real*8   (a-h,o-z)
!      dimension r(1001),y(1001),yi(1001)
      integer nb,nin
      real*8 :: y(:),yi(:),r(:)
      integer i,j,kmax
      real*8 asum,bsum,dx,h,g
      bsum=0.d0
      asum=0.d0
      yi(1)=0.d0
      i=1
      kmax=nin/2
!c                             nin must be an even integer
      do 50 j=1,nb
      dx=r(i+1)-r(i)
      h=dx/3.d0
      g=h/4.d0
      do 40 k=1,kmax
      i=i+2
      bsum=bsum+h*(y(i-2)+4.d0*y(i-1)+y(i))
      yi(i)=asum+bsum
  40  yi(i-1)=yi(i-2)+g*(5.d0*y(i-2)+8.d0*y(i-1)-y(i))
      asum=yi(i)
      bsum=0.d0
  50  continue
      return
      end subroutine smpold
!c _____________________________________________________________________
!c /////////////////////////////////////////////////////////////////////
!BOP
!!ROUTINE: potgen
!!INTERFACE:
       subroutine potgen(r,rv,rhos,zed,mesh,nsize,numsp)
!!DESCRIPTION:
! generates atomic potential
!
!c =====================================================================
!!USES:
        use common_rho_hsk, only : drv,excpo,excen,nmx
!
!!REVISION HISTORY:
! adapted - A.S. - 2013
!EOP
!
!BOC
       implicit real*8   (a-h,o-z)
       parameter (one=1.0d0,three=3.0d0,third=one/three)
       parameter (epsm=1.d-32,onemd=.99999999d0)

       integer i,ns
       dimension r(nmx),rhos(nmx,2),rv(nmx,2)
       dimension dcoul(nmx),dexcpo(nmx,2)
       dimension q(nmx),q1(nmx),coul(nmx)
       dimension rho(nmx),xm(nmx),temp(nmx)

!       dimension drv(1001,2)
!       common/drvdr/drv
!       dimension excpo(1001,2)
!       common/excorr/excpo,excen(1001)
!c =====================================================================
!c      iexcor=2
!c      ========       use vosko-wilks-nusair exchange  <===========
       iexcor=1
!c      ========       use von-Barth-Hedin    exchange  <===========
       nblock=mesh/nsize
       do 30 i=2,mesh
         rho(i)=rhos(i,1)+rhos(i,numsp)
         temp(i)=rho(i)/r(i)
         xm(i)=rhos(i,1)-rhos(i,numsp)
   30  continue
!c =====================================================================
!c
       q(1)=0.
       call intqud(rho,q,r,nblock,nsize)
!c
       q1(1)=0.
       call intqud(temp,q1,r,nblock,nsize)
!c =====================================================================
       coul(1)=-2*zed
       dcoul(1)=0.d0
       temp(1)=0.d0
       do 62 ns=1,numsp
         sp=3-2*ns
         rv(1,ns)=coul(1)
         do 60 i=2,mesh
            rs=((3.d0*r(i)**2)/max(rho(min(i,mesh-20)),epsm))           &
     &           **third
            dz=(numsp-1)*xm(i)/max(rho(i),epsm)
            if(dz.le.-1.d0) dz=-onemd
            if(dz.ge. 1.d0) dz= onemd
            if(ns.eq.1) then
                        coul(i)=-2*(zed-q(i)+r(i)*(q1(i)-q1(mesh)))
                        dcoul(i)=-2*(q1(i)-q1(mesh))
            endif
            excpo(i,ns)=alpha2_ini(rs,dz,sp,iexcor,edum)
            temp(i)=r(i)*excpo(i,ns)
            excen(i)=edum
            rv(i,ns)=coul(i)+temp(i)
   60    continue
         call derv5_ini(temp,dexcpo(1,ns),r,mesh)
         do 61 i=1,mesh
            drv(i,ns)=dcoul(i)+dexcpo(i,ns)
   61    continue
   62  continue
      return
      end subroutine potgen
!c _____________________________________________________________________
!c /////////////////////////////////////////////////////////////////////
!BOP
!!ROUTINE: alpha2_ini
!!INTERFACE:
      function alpha2_ini (rs,dz,sp,iexch,exchg)
!!DESCRIPTION:
! LDA exchange-correlation functional 
!
!!REVISION HISTORY:
! Adapted - A.S. - 2013
!EOP
!
!BOC
      implicit real*8   (a-h,o-z)
      parameter(one=1.0d0,two=2.0d0,three=3.0d0,four=4.0d0)
      parameter(thrd=one/three,for3=four/three)

      dimension vxx(2),vxcs(2),g(3),dg(3),tbq(3),tbxq(3)
      dimension bxx(3),a(3),b(3),c(3),q(3),x0(3),bb(3),cx(3)
      integer i
      save
!c
!c     data for von barth-hedin
!c
      data ccp,rp,ccf,rf/0.045d0,21.d0,0.0225d0,52.916682d0/
!c
!c     data for vosko-wilk-nusair
!c
      data incof/0/
      data  a/-0.033774d0,0.0621814d0,0.0310907d0/
      data  b/1.13107d0,3.72744d0,7.06042d0/
      data  c/13.0045d0,12.9352d0,18.0578d0/
      data x0/-0.0047584d0,-0.10498d0,-0.3250d0/
      data cst,aip/1.92366105d0,0.91633059d0/
      data fnot,bip/1.70992095d0,0.25992105d0/
!c
!c  the following are constants needed to obtain potential
!c  which are in g.s. painter's paper
!c  =====given here for check=====(generated below)
!c
!c     data q/.7123108918e+01,.6151990820e+01,.4730926910e+01/
!c     data bxx/-.4140337943e-03,-.3116760868e-01,-.1446006102/
!c     data tbq/.3175776232e+00,.1211783343e+01,.2984793524e+01/
!c     data tbxq/.3149055315e+00,.1143525764e+01,.2710005934e+01/
!c     data bb/.4526137444e+01,.1534828576e+02,.3194948257e+02/
!c
!c if rs > 250, then return zero for pot and exc
      alpha2_ini=0.d0
      exchg=0.d0
      if (rs.gt.250) return
      go to (10,20) iexch
!c
!c     von barth-hedin  exch-corr potential
!c     j. phys. c5,1629(1972)
!c
  10  continue
      fm=two**for3-two
      fdz=( (one+dz)**for3 + (one-dz)**for3 - two )/fm
      ex=-0.91633d0/rs
      exf=ex*two**thrd
      xp=rs/rp
      xf=rs/rf
      gp=(one+xp**3)*log(one+one/xp) - xp*xp +xp/two - thrd
      gf=(one+xf**3)*log(one+one/xf) - xf*xf +xf/two - thrd
      exc=ex-ccp*gp
      excf=exf-ccf*gf
      dedz=for3*(excf-exc)*((one+dz)**thrd - (one-dz)**thrd)/fm
      gpp=three*xp*xp*log(one+one/xp)-one/xp + 1.5d0 - three*xp
      gfp=three*xf*xf*log(one+one/xf)-one/xf + 1.5d0 - three*xf
      depd=-ex/rs-ccp/rp*gpp
      defd=-exf/rs-ccf/rf*gfp
      decd=depd+(defd-depd)*fdz
!c exchange-correlation energy
      exchg= exc + (excf-exc)*fdz
!c exchange-correlation potential
      alpha2_ini=exc+(excf-exc)*fdz-rs*decd*thrd+sp*(one-sp*dz)*dedz
      return
!c
!c
  20  continue
      if(incof.ne.0) go to 30
      incof=2
!c
!c   vosk0-wilk-nusair exch-corr potential
!c    taken from g.s. painter
!c   phys. rev. b24 4264,1981
!c
!c   generate constant coefficients for the parameterization (v-w-n)
!c
      do 1 i=1,3
      cx(i)= x0(i)**2 + b(i)*x0(i) + c(i)
      bfc= 4*c(i) - b(i)**2
      q(i)= sqrt(bfc)
      bxx(i)= b(i)*x0(i)/cx(i)
      tbq(i)= 2*b(i)/q(i)
      tbxq(i)= tbq(i) + 4*x0(i)/q(i)
   1  bb(i)= 4*b(i)*( 1 - x0(i)*(b(i) + 2*x0(i))/cx(i) )
!c
  30  continue
      zp1= 1 + dz
      zm1= 1 - dz
      xr=sqrt(rs)
      pex= -aip/rs
      xrsq= rs
!c
!c   generate g(i)=alpha,epsilon fct.s
!c     and their derivatives dg(i)
!c    1=alpha(spin stiffness)  2=ecp  3=ecf
!c
      do 2 i=1,3
         qi=q(i)
         txb= 2*xr + b(i)
         fx= xrsq + xr*b(i) + c(i)
         arct= atan2(qi,txb)
         dxs= (xr-x0(i))**2/fx
!c
         g(i)=a(i)*( log(xrsq/fx) + tbq(i)*arct-bxx(i)*                 &
     &                 (log(dxs) + tbxq(i)*arct) )
!c
         dg(i)=a(i)*( 2.d0/xr - txb/fx -                                &
     &      bxx(i)*(2.d0/(xr-x0(i))-txb/fx) - bb(i)/(qi**2 + txb**2) )
!c
   2  continue
!c
      ecp=g(2)
      zp3=zp1**thrd
      zm3=zm1**thrd
      zp3m3=zp3-zm3
!c part of last term in vx   eq(13)
      fx1=0.5d0*for3*pex*zp3m3
      z4= dz**4
      fz= cst*(zp1**for3 + zm1**for3 - 2.d0)
      beta= fnot*( g(3)-g(2) )/g(1) - 1.d0
      ec= ecp + fz*g(1)*( 1 + z4*beta )/fnot
      ex= pex*( 1 + fz*bip )
      f3ex= for3*ex
!c echange-correlation energy
      exchg= ec + ex
!c exchange potential
      vxx(1)= f3ex + fx1*zm1
      vxx(2)= f3ex - fx1*zp1
!c correlation potential
      vcc= ec - xr*( (1 - z4*fz)*dg(2) + z4*fz*dg(3)                    &
     &                  +  (1 - z4)*fz*dg(1)/fnot  )/6
!c
      facc= 4*g(1)*( dz**3*fz*beta + (1 + beta*z4 )*zp3m3/(6*bip) )/fnot
!c
!c exch-corr. potential for each spin as called in newpot
!c
      vxcs(1)= vcc + zm1*facc + vxx(1)
      vxcs(2)= vcc - zp1*facc + vxx(2)
!c
      if( sp.ge.0 ) alpha2_ini= vxcs(1)
      if( sp.lt.0 ) alpha2_ini= vxcs(2)
      return
      end function alpha2_ini
!c /////////////////////////////////////////////////////////////////////
!BOP
!!ROUTINE: derv5_ini
!!INTERFACE:
         subroutine derv5_ini(y,dy,x,nn)
!!DESCRIPTION:
! calculates derivative with 5-points lagrangian fit
!

!!DO_NOT_PRINT
         implicit real*8   (a-h,o-z)
!!DO_NOT_PRINT

!!ARGUMENTS:
         integer nn
         real*8, intent(in) :: y(nn),x(nn)
         real*8, intent(out) :: dy(nn)
!!REVISION HISTORY:
! Adapted - A.S. - 2013
!!REMARKS:
!EOP
!
!BOC
!         parameter (ipmesh=1001)
!         dimension x(1051),y(ipmesh),dy(ipmesh)
         integer i
!c _____________________________________________________________________
!c                                      lagrangian fit of y=f(x)
!c                                      derivative is then calculated
!c _____________________________________________________________________
         do 10 i=2,nn
         i1=max(i-2,2)
         i1=min(i1,nn-4)
         a=y(i1)
         b=(y(i1+1)-a)/(x(i1+1)-x(i1))
         c=(y(i1+2)-a)/(x(i1+2)-x(i1))
         c=(c-b)/(x(i1+2)-x(i1+1))
         d=(y(i1+3)-a)/(x(i1+3)-x(i1))
         d=(d-b)/(x(i1+3)-x(i1+1))
         d=(d-c)/(x(i1+3)-x(i1+2))
         e=(y(i1+4)-a)/(x(i1+4)-x(i1))
         e=(e-b)/(x(i1+4)-x(i1+1))
         e=(e-c)/(x(i1+4)-x(i1+2))
         e=(e-d)/(x(i1+4)-x(i1+3))
         dy(i)=b+c*( (x(i)-x(i1)) + (x(i)-x(i1+1)) )                    &
     &        + d*(  (x(i)-x(i1))*(x(i)-x(i1+1))                        &
     &             + (x(i)-x(i1+1))*(x(i)-x(i1+2))                      &
     &             + (x(i)-x(i1+2))*(x(i)-x(i1))      )                 &
     &      +e*( (x(i)-x(i1))*(x(i)-x(i1+1))*(x(i)-x(i1+2))             &
     &      +  (x(i)-x(i1+3))*(x(i)-x(i1+1))*(x(i)-x(i1+2))             &
     &      +  (x(i)-x(i1))*(x(i)-x(i1+1))*(x(i)-x(i1+3))               &
     &      +  (x(i)-x(i1))*(x(i)-x(i1+2))*(x(i)-x(i1+3)) )
   10    continue
!c
         dy(1)=dy(2)
!c _____________________________________________________________________
      return
      end subroutine derv5_ini
!c _____________________________________________________________________
!c _____________________________________________________________________
!c /////////////////////////////////////////////////////////////////////
!BOP
!!ROUTINE: bound
!!INTERFACE:
        subroutine bound(abad,fndt,tble,zed,r,v,mesh1,nsize,nblock,     &
     &                   en,thresh,nq,lq,chden,kkk,emn,qi2,qii)
!c =====================================================================
!!DESCRIPTION:
! finds atomic eigenvalues, eigenstates and charge density
!
!!USES:
        use mecca_constants, only : Rnucl
        use common_rho_hsk, only : nmx
!
!!REVISION HISTORY:
! Modified - A.S. - 2013
!EOP
!
!BOC
        implicit real*8   (a-h,o-z)
        real*8 zed
        integer mesh1,nsize,nblock
        character*7 abad,fndt,tble
        real*8 en,thresh
        parameter (onepd=1.00001d0,eminup=-.0005d0)

!        character*7 fndt,tble
!        common/e1/fndt,tble

        real*8 r(nmx),v(nmx),chden(nmx),temp(nmx),qi2(nmx)
        real*8 vme(nmx),y(nmx),dy(nmx),yin(nmx),dyin(nmx)
        integer i,j,k,n,iter
        integer l,mesh,num,nnode
        integer maxit,maxjt,jter
        real*8 emin,energy,Rnz
!c =====================================================================
       Rnz = Rnucl(zed)
!DEBUG       Rnz = 5.d-5
       abad=tble
       l=lq
       n=nq
       mesh=mesh1
       if(en.ge.0.d0) en=-zed**2/n**4
       emin=min(emn,eminup)
       energy=en
       en=emin
!c      WRITE(6,*) ' in bound: emin=',emin
       eold=99.99d0
!c =====================================================================
!c zero out chden
        do 3 i=1,mesh1
        qi2(i)=0.0d0
    3   chden(i)=0.0d0
!c =====================================================================
        vme(1) = (v(1)+(l*(l+1))/Rnz)/Rnz - emin
        do 4 j=2,mesh
    4   vme(j)=(v(j)+(l*l+l)/r(j))/r(j)   - emin
        do 6 i=2,mesh
        j=i
        if(r(i)*sqrt(-emin) .gt. 150.d0)go to 7
   6    continue
   7    lll=j
!c =====================================================================
!c       WRITE(6,*)' in bound: limit=',lll
        call outsch(zed,emin,l,lll,vme,v,r,y,dy)
!c =====================================================================
        num=0
        do 8 i=3,lll
           if(y(i).eq.0.d0) then
                num=num+1
                go to 8
           else
                if(y(i-1).eq.0) go to 8
                is=sign(onepd, y(i) )
                js=sign(onepd,y(i-1))
                if(is.ne.js) num=num+1
           endif
   8    continue
!c =====================================================================
!c number of nodes  n-l-1
!c
       nnode=n-l-1
       if(nnode.ge.num) then
               return
       endif
!c =====================================================================
!c  initialize parameters for search
       maxit=40
       maxjt=20
       iext=0
       iter=0
       jter=0
!c
       v(1)=-2*zed
!c =====================================================================
!c ....................  main loop .....................................
!c =====================================================================
  10   continue
       iter=iter+1
       if(iter.gt.maxit) go to 800
!DEBUG       
       if ( Rnz*energy<2*v(1) ) go to 800
!DEBUG       
       if(energy.gt.0) energy=eold/2.d0
       bigkr=150.d0
       if(abs(energy).gt.400.d0) bigkr=125.d0
!c
!c find classical turning point ........................................
!c
        vme(1) = (v(1)+(l*(l+1))/Rnz)/Rnz - energy
         do 40 j=2,mesh
  40     vme(j)=(v(j)+(l*l+l)/r(j))/r(j)   - energy
!c =====================================================================
         do 42 k=3,mesh
         j=mesh+1-k
         if(vme(j).le.0.d0) go to 45
   42    continue
!c =====================================================================
!C        write(6,*) ' cannot find classical turning point '
        if(iter.eq.1) then
               iter=0
               jter=jter+1
               if(jter.gt.maxjt) then
                    return
               endif
               energy=0.75d0*energy
               go to 10
         else
               iter=iter-1
               jter=jter+1
               if(jter.gt.maxjt) then
!C                    write(6,*) ' turning point trouble'
                    return
               endif
              energy=max(minval(vme(1:mesh))+energy,0.5d0*(energy+eold))
!DEBUG               energy=0.5d0*(energy+eold)
          endif
!c =====================================================================
  45    match=j+1
!c =====================================================================
        match=min(match,mesh-40)
        do 60 i=match,mesh
        j=i
        if(r(i)*sqrt(-energy) .gt. bigkr ) go to 61
  60    continue
  61    last=j
        if(iext.eq.0) lll=last
        if(iext.ne.0) lll=match+1
        mmmm=match-1
!c =====================================================================
!c    outward integration of the schrodinger equation
!c =====================================================================
        call outsch(zed,energy,l,lll,vme,v,r,y,dy)
!c =====================================================================
        gamout=dy(match)/(sqrt(-energy)*y(match))
!c                                                 count number of nodes
        num=0
        do 80 i=3,lll
           if(y(i).eq.0.d0) then
                num=num+1
                go to 80
           else
                if(y(i-1).eq.0) go to 80
                is=sign(onepd, y(i) )
                js=sign(onepd,y(i-1))
                if(is.ne.js) num=num+1
           endif
  80    continue
!c =====================================================================
        if(iter.eq.1) then
             iext=0
             if(num.eq.nnode+1) then
                  enext=1.25d0*energy
                  jter=0
                  go to 87
             endif
             if(num.eq.nnode) then
                  enext=0.75d0*energy
                  jter=0
                  go to 87
             endif
             jter=jter+1
             if(jter.gt.maxjt)  then
                  return
             endif
             iter=0
             if(num.lt.nnode) then
                  energy=0.5d0*energy
                  go to 10
             else
                  energy=1.5d0*energy
                  go to 10
             endif
         endif
!c =====================================================================
         if(iter.eq.2)  then
             if(num.eq.nold) then
                  iter=1
                  jter=jter+1
                  if(jter.gt.maxjt) then
!C                       write(6,*) ' trouble in second iteration'
                       return
                  endif
                  dir=+1.
                  if(num.eq.nnode) dir=-1.d0
                  enext=energy-0.8d0*dir*abs(energy-eold)
                  if(enext.ge.0.d0) enext=0.5d0*energy
                  energy=enext
                  go to 10
             endif
             if(iabs(num-nold).ne.1) then
                  iter=1
                  jter=jter+1
                  if(jter.gt.maxjt) then
!C                       write(6,*) ' trouble in second iteration'
                       return
                  endif
                  enext=0.5d0*(energy+eold)
                  energy=enext
                  go to 10
             endif
             go to 81
        endif
!c =====================================================================
         if(abs(eold-energy).lt.thresh) go to 200
!DEBUG         
         if(abs(energy).lt.thresh) go to 800
         IEEEE = 0
         if(energy>eold) IEEEE=2
         if(eold>energy) IEEEE=1
         if ( IEEEE==0 ) then
                 write(*,*) ' v(1)/Rnz=',v(1)/Rnz,vme(1)
                 write(*,*) ' eold=',eold,' energy=',energy
                 stop 'ERROR'
         end if
!DEBUG         
!c =====================================================================
         if(num.ne.nnode) go to 83
   81    dir=+1
         go to 85
   83    dir=-1.d0
   85    enext=energy+0.5d0*dir*abs(energy-eold)
   87    jter=0
         nold=num
         if( abs(1.d0-eold/energy) .lt. 0.1d0  .or. iext.eq.1) then
!c =====================================================================
!c   inward integration of schrodinger equation
!c =====================================================================
             call inwsch(zed,energy,mmmm,last,l,vme,r,v,yin,dyin)
!c =====================================================================
             gamin=dyin(match)/(sqrt(-energy)*yin(match))
             rat=(1.d0+gamin)/(1.d0-gamin)
             rat=rat-((1.d0+gamout)/(1.d0-gamout))
             if(rat.eq.0) go to 200
             if(iext.eq.1) then
                 enext=energy+(eold-energy)/( 1.d0 - ratold/rat)
             endif
             iext=1
             ratold=rat
         endif
!c =====================================================================
         eold=energy
         energy=enext
         go to 10
!c
!DEBUG         
         if ( IEEEE.ne.0 ) write(*,*) ' DEBUG '
!DEBUG         
  800    continue
!c =====================================================================
!c ..........  can not find state  .....................................
!c =====================================================================
         return
!c
  200    continue
!c =====================================================================
!c .................... found state ............................
!c =====================================================================
         call inwsch(zed,energy,mmmm,last,l,vme,r,v,yin,dyin)
!c =====================================================================
         gamin=dyin(match)/(sqrt(-energy)*yin(match))
         abad=fndt
         kkk=last
         do 225 i=1,match
  225    chden(i)=y(i)**2
         anorm=y(match)/yin(match)
         do 226 i=match,last
         y(i)=anorm*yin(i)
         dy(i)=anorm*dyin(i)
  226    chden(i)=(anorm*yin(i))**2
         en=energy
!c =====================================================================
         call intqud(chden,temp,r,nblock,nsize)
         qi2(1)=0.d0
         do 227 i=2,mesh1
         if(i.lt.last) then
                chden(i)=chden(i)/temp(mesh1)
                qi2(i)=(dy(i)-(l+1)*y(i)/r(i))**2/temp(mesh1)
         else
                chden(i)=0.d0
                qi2(i)=0.d0
         endif
  227    continue
         call intqud(qi2,temp,r,nblock,nsize)
         qii=temp(mesh1)
      return
      end subroutine bound
!c _____________________________________________________________________
!c /////////////////////////////////////////////////////////////////////
!BOP
!!ROUTINE: outsch
!!INTERFACE:
        subroutine outsch(zed,energy,l,match,vme,v,r,y,dy)
!!DESCRIPTION:
! outward integration of atomic Schroedinger equation
!
!!USES:
        use common_rho_hsk, only : nmx
!
!!REVISION HISTORY:
! Adapted - A.S. - 2013
!EOP
!
!BOC
        implicit real*8   (a-h,o-z)
        real*8 zed,energy
        integer l,match
        real*8 vme(nmx),v(nmx),r(nmx)
        real*8 y(nmx),dy(nmx)
        real*8 fac(5)

        data fac/1.d0,3.d0,15.d0,105.d0,945.d0/
        real*8 e,x,a,b,c,d,f
        integer i,n
!c =====================================================================
        if(l.lt.0 .or. l.gt.4) then
             write(6,*) ' angular momentum quatum number is ',l
             write(6,*) ' stopping in outsch: l out of bounds'
             stop
        endif
!c =====================================================================
        y = 0.d0
        dy = 0.d0
        last=match
        e=energy
!c
        b1=v(1)
        f1=(v(2)-v(1))/r(2)
        f2=(v(3)-v(1))/r(3)
        f3=(v(4)-v(1))/r(4)
        bx1=f1
        bx2=(f2-f1)/(r(3)-r(2))
        bx3=(f3-f1)/(r(4)-r(2))
        bx3=(bx3-bx2)/(r(4)-r(3))
        b4=bx3
        b3=bx2-bx3*(r(2)+r(3))
        b2=bx1-bx2*r(2)+bx3*r(2)*r(3)
        a=sqrt(-e)**(l+1) / fac(l+1)
        b=b1*a/(2*l+2)
        c=(a*(b2-e)+b*b1)/(4*l+6)
        d=(a*b3+b*(b2-e)+c*b1)/(6*l+12)
        f=(a*b4+b*b3+c*(b2-e)+d*b1)/(8*l+20)
!c
        do 10 i=2,5
            x=r(i)**l
            p1=a+r(i)*( b+ r(i)*( c+ r(i)*( d+ f*r(i) )))
            p2=b+r(i)*(2.d0*c+r(i)*(3.d0*d+4.d0*f*r(i) ))
            y(i)=x*r(i)*p1
            dy(i)=(l+1)*x*p1+x*r(i)*p2
   10   continue
!c =====================================================================
      delold=r(6)-r(5)
      ityp=0
      do 40 n=6,last
         del=r(n)-r(n-1)
!c =====================================================================
         if(abs(del-delold).gt.1.e-7)  then
                 ityp=1
                 delold=del
                 m4=n-7
                 m3=n-5
                 m2=n-3
                 m1=n-1
         else
                 if(ityp.eq.0) then
                     m4=n-4
                     m3=n-3
                     m2=n-2
                     m1=n-1
                 endif
                 if(ityp.eq.2) then
                     ityp=0
                     m4=n-5
                     m3=n-3
                     m2=n-2
                     m1=n-1
                 endif
                 if(ityp.eq.1) then
                     ityp=2
                     m4=n-6
                     m3=n-4
                     m2=n-2
                     m1=n-1
                 endif
         endif
!c =====================================================================
        h24=del/24.d0
!c
        dy(n)=dy(m1)+h24*(55.d0*vme(m1)*y(m1) -59.d0*vme(m2)*y(m2)      &
     &                  + 37.d0*vme(m3)*y(m3) - 9.d0*vme(m4)*y(m4))
        do 37 ijk=1,5
        y(n)=y(m1)+h24*(9.d0*dy(n) +19.d0*dy(m1) -5.d0*dy(m2) + dy(m3))
        dy(n)=dy(m1)+h24*( 9.d0*vme(n)*y(n) +19.d0*vme(m1)*y(m1)        &
     &                   - 5.d0*vme(m2)*y(m2) +  vme(m3)*y(m3))
   37  continue
        y(n)=y(m1)+h24*(9.d0*dy(n) +19.d0*dy(m1) -5.d0*dy(m2) + dy(m3))
!c =====================================================================
   40  continue
!c
      return
      end subroutine outsch
!c _____________________________________________________________________
!c /////////////////////////////////////////////////////////////////////
!BOP
!!ROUTINE: inwsch
!!INTERFACE:
       subroutine inwsch(zed,energy,match1,last1,l,vme,r,v,y,dy)
!!DESCRIPTION:
! inward integration of atomic Schroedinger equation
!
!c =====================================================================
!!USES:
        use common_rho_hsk, only : nmx
!
!!REVISION HISTORY:
! Adapted - A.S. - 2013
!EOP
!
!BOC
        implicit real*8   (a-h,o-z)
        real*8 zed,energy
        integer match1,last1,l
        real*8 r(nmx),v(nmx),vme(nmx),y(nmx),dy(nmx)
        integer match,last,i,n,m1,m2,m3,m4
!c =====================================================================
        y = 0.d0
        dy = 0.d0
        match=match1
        last=last1
        if(mod(last-2,40).lt.4) last=min(last+6,nmx)
!c
        n=last
        p=sqrt(-energy)
!c =====================================================================
        do 10 i=1,4
        xpr=exp(-p*r(n))
        y(n)=xpr
        dy(n)=-p*xpr
            if(i.eq.1) then
                   ym3=y(n)
                   dym3=dy(n)
                   vmem3=vme(n)
            endif
            if(i.eq.2) then
                   ym2=y(n)
                   dym2=dy(n)
                   vmem2=vme(n)
            endif
            if(i.eq.3) then
                   ym1=y(n)
                   dym1=dy(n)
                   vmem1=vme(n)
            endif
        n=n-1
  10    continue
!c =====================================================================
        nend=n
        n=n+1
        delold=r(n+1)-r(n)
        do 80 nn=match,nend
         n=n-1
         del=r(n+1)-r(n)
         m1=n+1
         m2=n+2
         m3=n+3
         m4=n+4
!c =====================================================================
         if(abs(del-delold).gt.1.d-7) then
             a1=dy(m1)
             a2=dy(m2)
             a3=dy(m3)
             call qrfit(delold,a1,a2,a3,f1,f2)
             dym1=a1
             dym2=f1
             dym3=a2
             dym4=f2
!c
             a1=y(m1)
             a2=y(m2)
             a3=y(m3)
             call qrfit(delold,a1,a2,a3,f1,f2)
             ym1=a1
             ym2=f1
             ym3=a2
             ym4=f2
!c
             a1=vme(m1)
             a2=vme(m2)
             a3=vme(m3)
             call qrfit(delold,a1,a2,a3,f1,f2)
             vmem1=a1
             vmem2=f1
             vmem3=a2
             vmem4=f2
!c
             delold=del
!c =====================================================================
        else
!c =====================================================================
             ym4=ym3
             ym3=ym2
             ym2=ym1
             ym1=y(n+1)
!c
             dym4=dym3
             dym3=dym2
             dym2=dym1
             dym1=dy(n+1)
!c
             vmem4=vmem3
             vmem3=vmem2
             vmem2=vmem1
             vmem1=vme(n+1)
         endif
!c =====================================================================
!c
         h24=-del/24.d0
         dy(n)=dym1 + h24 * (55.d0*vmem1*ym1-59.d0*vmem2*ym2            &
     &                      +37.d0*vmem3*ym3- 9.d0*vmem4*ym4)
         do 70 ijk=1,5
         y(n)=ym1 + h24*(9.d0*dy(n)+19.d0*dym1-5.d0*dym2+dym3)
         dy(n)=dym1 + h24 * ( 9.d0*vme(n)*y(n)+19.d0*vmem1*ym1          &
     &                       -5.d0*vmem2*ym2+vmem3*ym3)
   70    continue
         y(n)=ym1 + h24*(9.d0*dy(n)+19.d0*dym1-5.d0*dym2+dym3)
!c =====================================================================
   80 continue
      return
      end subroutine inwsch
!c _____________________________________________________________________
!c /////////////////////////////////////////////////////////////////////
!BOP
!!ROUTINE: qrfit
!!INTERFACE:
       subroutine qrfit(del,a1,a2,a3,f1,f2)
!!DESCRIPTION:
! quadratic (3-points) fit interpolation
!
!!REVISION HISTORY:
! Adapted - A.S. - 2013
!EOP
!
!BOC
       implicit real*8   (a-h,o-z)
       real*8   del,del2,delh
       real*8 a1,a2,a3,f1,f2
       real*8 a,b,c
!       
!c=======================================================================
!c  given a function evaluated at three equally spaced points
!c     returns values at the two midpoints using
!c         quadratic interpolation
!c               needs     del,  the spacing
!c                       a1,a2,a3, the 3 function evaluations
!c              returns    f1,f2,  the interpolated values
!c        f(x)= a + bx + cx(x-d)
!c        f(0)=a1
!c        f(d)=a2
!c        f(2d)=a3
!c        f1=f(d/2)
!c        f2=f(3d/2)
!c =====================================================================
          del2=2*del
          delh=del*0.5d0
          a=a1
          b=(a2-a1)/del
          c=(((a3-a1)/del2)-b)/del
          f1=a+delh*(b-c*delh)
          f2=a+3.d0*delh*(b+c*delh)
      return
      end subroutine qrfit
!c _____________________________________________________________________
!c /////////////////////////////////////////////////////////////////////
!BOP
!!ROUTINE: savout
!!INTERFACE:
       subroutine savout(iter,vr,numsp)
!!USES:
        use common_rho_hsk, only : vrout,nmx,ndgand
!!REVISION HISTORY:
! Adapted - A.S. - 2013
!EOP
!
!BOC
       implicit real*8   (a-h,o-z)
       integer iter,numsp
       real*8 vr(nmx,2)
!       common/vout/vrout(1001,2,4)
       integer i,n,ns
!       integer, save :: iw=1
!c
       n=mod(iter-1,ndgand)+1
       do 10 ns=1,numsp
       do 10 i=1,nmx
   10  vrout(i,ns,n)=vr(i,ns)
      return
      end subroutine savout
!c _____________________________________________________________________
!c /////////////////////////////////////////////////////////////////////
!BOP
!!ROUTINE: savin
!!INTERFACE:
       subroutine savin(iter,vr,numsp)
!!USES:
        use common_rho_hsk, only : vrin,nmx,ndgand
!
! Adapted - A.S. - 2013
!EOP
!
!BOC
       implicit real*8   (a-h,o-z)
       integer iter,numsp
       real*8 vr(nmx,2)
!       common/vin/vrin(1001,2,4)
       integer i,n,ns
!       integer, save :: iw=1
!c
       n=mod(iter-1,ndgand)+1
       do 10 ns=1,numsp
       do 10 i=1,nmx
   10  vrin(i,ns,n)=vr(i,ns)
      return
      end subroutine savin
!c _____________________________________________________________________
!c /////////////////////////////////////////////////////////////////////
!BOP
!!ROUTINE: dpdif
!!INTERFACE:
       function dpdif(i,j,numsp)
!!DESCRIPTION:
! square of weighted L2-norm
!
!!USES:
        use common_rho_hsk, only : vrin,vrout,nmx
!!REVISION HISTORY:
! Adapted - A.S. - 2013
!EOP
!
!BOC
       implicit real*8   (a-h,o-z)
!       common/vin/vrin(1001,2,4)
!       common/vout/vrout(1001,2,4)
!c
       integer i,j,numsp
       real*8 dpdif
       integer k,ns
       dpdif=0.0d0
       do 10 ns=1,numsp
       do 10 k=1,nmx
         dpdif=dpdif+(vrin(k,ns,i)-vrout(k,ns,i))                       &
     &      *k*(vrin(k,ns,j)-vrout(k,ns,j))
   10     continue
      return
      end function dpdif
!c _____________________________________________________________________
!c /////////////////////////////////////////////////////////////////////
!BOP
!!ROUTINE: dgand
!!INTERFACE:
       subroutine dgand(vrnew,ndim,numsp,alpa)
!!DESCRIPTION:
! {\tt vrin/vrout} mixing (of Anderson type?) results in {\tt vrnew}
!
!c =====================================================================
!!USES:
        use common_rho_hsk, only : vrin,vrout,nmx,ndgand
!
!!REVISION HISTORY:
! Adapted - A.S. - 2013
!EOP
!
!BOC
       implicit real*8   (a-h,o-z)
!       common/vin/vrin(1001,2,4)
!       common/vout/vrout(1001,2,4)
       integer ndim,numsp
       real*8 vrnew(nmx,2),alpa
       real*8 a(ndgand+1,ndgand+1),b(ndgand+1)
       integer i,j,k,ns,jdim
       real*8 fsum
!c

!DEBUG       
!       vrnew(1:nmx,1:numsp) = vrin(1:nmx,1:numsp,1)*(1.d0-alpa) +       &
!     &                       vrout(1:nmx,1:numsp,1)*alpa
!
!       if ( nmx>0 ) return
!DEBUG

       jdim=ndim+1
       do 2 i=1,ndim
         a(i,jdim)=1.d0
         a(jdim,i)=1.d0
         b(i)=0.d0
    2  continue
!c
       a(jdim,jdim)=0.d0
       b(jdim)=1.d0
       do 5 j=1,ndim
         jj=j
         do 5 i=1,j
           ii=i
           a(i,j)=dpdif(ii,jj,numsp)
           if(i.ne.j) then
             a(j,i)=a(i,j)
           else     !DEBUG
             a(i,i) = a(i,i) + 0.001d0  !DEBUG
           end if  
    5  continue
!c
       call mxlneq(a,b,jdim)
!c
       do 20 ns=1,numsp
       do 20 k=1,nmx
        fsum=0.d0
        do 10 i=1,ndim
   10    fsum=fsum+b(i)*(vrin(k,ns,i)+alpa*(vrout(k,ns,i)-vrin(k,ns,i)))
        vrnew(k,ns)=fsum
   20  continue
      return
      end subroutine dgand
!c _____________________________________________________________________
!c /////////////////////////////////////////////////////////////////////
!BOP
!!ROUTINE: mxlneq
!!INTERFACE:
       subroutine mxlneq(a,y,nn)
!!DESCRIPTION:
!   (to solve a real system of linear equations)
!
!!USES:
       use common_rho_hsk, only : ndgand
!
!!REVISION HISTORY:
! Adapted - A.S. - 2013
!!REMARKS:
! maybe, to use dgesv?
!EOP
!
!BOC
       implicit real*8   (a-h,o-z)
       real*8 a(ndgand+1,ndgand+1),y(ndgand+1)
       integer nn
       integer i,n
!c
       n=nn
       do 10 i=2,n
            f1=-1.d0/a(i-1,i-1)
            do  2 j=i,n
            f2=f1*a(j,i-1)
            do 5 k=1,n
    5       a(j,k)=a(j,k)+f2*a(i-1,k)
            y(j)=y(j)+f2*y(i-1)
    2       continue
   10  continue
!c
!c ..........................  calculate the determinant ...............
!c
!c         det=1.
!c         do 19 i=1,n
!c 19      det=det*a(i,i)
!c
!c         write(6,1235) n,det
!c1235     format(' in mxlneq: order, det',2x,i4,e17.9)
!c
      y(n)=y(n)/a(n,n)
      do 20 ij=1,n-1
          i=n-ij
          do 15 j=1,ij
   15     y(i)=y(i)-y(i+j)*a(i,i+j)
   20 y(i)=y(i)/a(i,i)
!c
!c
      return
      end subroutine mxlneq
!c _____________________________________________________________________

