      subroutine structure(alat,nbasis,                                 &
     &                     rslatt,kslatt,bases,itype,aij,ndimbas,       &
     &                     numbsub,nsublat,                             &
     &                     R_nn,                                        &
     &                     if0,mapstr,mappnt,mapij,imethod,             &
     &                     omega,                                       &
     &                     madmat,imdlng,                               &
     &                     invadd,                                      &
     &                     dop,lmax,rot,nrot,idop,                      &
     &                     nq,qmesh,ndimq,nmesh,nqpt,ispkpt,            &
     &                     wghtq,kmatchphase,twght,lrot,                &
     &                     Rksp,xknlat,ndimk,nksn,                      &
     &                     Rrsp,xrnlat,ndimr,nrsn,                      &
     &                     xrslatij,nrhp,ndimrhp,                       &
     &                     np2r,numbrs,ncount,                          &
     &                     tmparr,itmpar,iprint,istop)
!c     ==================================================================
      implicit real*8 (a-h,o-z)
!c
!c    ======================================================================
!c    modified for calculation of lattice vectors          by  A.S. Aug 1998
!c    modified for more structures                         by   WAS Jan 1994
!c    modified for mean-field, charge-correlated CPA       by   DDJ Dec 1993
!c    ======================================================================
!c
!c     ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
      include 'mkkrcpa.h'
!c     ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!c

      real*8 alat
!CALAM      real*8 alat,boa,coa
!c
!c                     boa = B/A, coa = C/A
!c
      integer nbasis,ndimbas
      real*8     rslatt(3,3),kslatt(3,3)
      real*8     bases(3,nbasis),aij(3,*),R_nn
!C      real*8     rmt(nsublat)
      integer    nrot,nrsn,nrhp
      integer    mapstr(ndimbas,*),mappnt(ndimbas,*),mapij(2,*)
      integer    imethod,ndimq,nmesh,nksn(nmesh),nqpt(nmesh)
!C      real*8     omegamt(nsublat)
!C      real*8     surfamt(nsublat)
      real*8     madmat(ndimbas,nbasis)

!cab      complex*16   dop(ipkkr,ipkkr,*)
      complex*16   dop((lmax+1)*(lmax+1),(lmax+1)*(lmax+1),*)
      real*8     qmesh(3,ndimq,nmesh)

      integer    nq(3,nmesh)
      integer    wghtq(ndimq,nmesh)
      real*8     twght(nmesh)
      integer    kmatchphase(ndimq,nmesh)

!c      integer    lwght(ipkpts*ipmesh)
!c      integer, allocatable ::   lwght(:)

      integer    itype(ipbase),numbsub(nsublat)
!c
!c                itype(i):  site --> sublattice
!c            numbsub(nsublat) -- number of sites in the sublattice
!c
      integer    ndimk,ndimr,ndimrhp,iprint
      character  istop*10
      real*8     Rksp,Rrsp

!c
!c            input:  Rksp -- in 1/alat-units; Rrsp -- in alat-units
!c
      real*8       xknlat(ndimk,3,nmesh)
      real*8       xrnlat(ndimr,3)
      real*8       xrslatij(ndimrhp,4)
      integer      np2r(ndimr,*)
      integer      numbrs(*),ncount
!c
!c        output:  xknlat -- in 2*pi/alat-units;
!c                 xrnlat,xrslatij -- in alat-units
!c                 aij    --     b_j-b_i,
!c                 ncount --  number of "aij"
!c
!c              here "*" should be > max(ndimk,ndimr)
!c
      integer      itmpar(*)
      real*8       tmparr(*)

      real*8 rot(49,3,3)
      real*8  rx(3,ipbase)
      integer if0(48,nbasis),if0temp(ipbase)
!c      integer jf0(ipbase,ipbase)  !  madmat-array is used instead of jf0

!c      integer kcheck(ipkpts)

      integer, allocatable ::   kcheck(:)
      integer  erralloc

      integer  lrot(ndimq,nmesh)

!c parameter
      character    sname*10
      parameter    (sname='structure')
      real*8     twopi
      parameter (twopi=2.d0*pi)
!c
      real*8 rcut,kcut
      real*8 rsewd(ipmdlng,3),rssq(ipmdlng)
      real*8 ksewd(ipmdlng,3),kssq(ipmdlng)
      integer nmdlng
      parameter (nmdlng=ipmdlng*0.85d0)

!c    ======================================================================

!c  rslatt,bases,rmt -- in alat-units
!c
!c  BTW,   volume*alat^3 -- volume in atomic units
!c

      volume = omega

      if(iprint.ge.-10) then
           write(6,*)
           write(6,'('' STRUCTURE:: real space basis vectors are'')')
           write(6,'(10x,3f10.5)') ((rslatt(i,j),i=1,3),j=1,3)
           if(nbasis.le.20.or.iprint.ge.1) then
            write(6,*)
            write(6,*) ' Atomic data::'
            write(6,1100) (j,itype(j),(bases(i,j),i=1,3),j=1,nbasis)
1100        format (9x,'Atom',i5,' of type',i5,' is at ',3f10.5)
           endif
           write(6,*)
      endif

       if(nbasis.gt.ndimbas) then
         write(*,*) ' STRUCTURE::  nbasis > ndimbas',nbasis,ndimbas
         call fstop(sname//' nbasis > ndimbas')
       end if

      call vectprd(rslatt(1,2),rslatt(1,3),kslatt(1,1))
      call vectprd(rslatt(1,3),rslatt(1,1),kslatt(1,2))
      call vectprd(rslatt(1,1),rslatt(1,2),kslatt(1,3))

      do j=1,3
        do i=1,3
         kslatt(i,j) = kslatt(i,j)/volume
        end do
      end do

      omega = volume*alat**3

      if(iprint.ge.-10) then
       write(6,*)
       write(6,'('' reciprocal space basis vectors are '')')
       write(6,'(10x,3f10.5)') ((kslatt(i,j),i=1,3),j=1,3)
      endif

!C      omegaint=zero
!C      do i=1,nsublat
!C       surfamt(i)=(4.d0*pi)*rmt(i)**2
!C       omegamt(i)=(4.d0*pi/3.d0)*rmt(i)**3
!C       omegaint=omegaint+omegamt(i)*numbsub(i)
!C      enddo
!C      omegaint=omega-omegaint
!c
!c     ***********************************************************
!c     Major printout............................................

      write(6,*)
      write(6,'(''  Volume of Real-space Cell:: '',d14.6,'' at.un'',    &
     &           '' = '',d14.6,'' D.U.'')') omega,volume

      write(6,'(''  Number of basis atoms '',                           &
     &              t40,''='',i5)') nbasis
!c
      if(max(ipxkns,iprsn).lt.ndimk) then
         write(*,*) ' STRUCTURE:: ipxkns,iprsn,ndimk =>',               &
     &              ipxkns,iprsn,ndimk
         call fstop(sname//' ipxkns/iprsn < ndimk')
      end if

      if(ispkpt.eq.0.or.ispkpt.eq.-1) then
       nnmesh = nmesh
      else
       nnmesh = 0
       do i=1,nmesh
        nqpt(i) = 1
       end do
      end if

      allocate(                                                         &
     &         kcheck(1:ndimq)                                          &
     &         ,stat=erralloc)
       if(erralloc.ne.0) then
        write(6,*) ' NDIMQ=',ndimq
        write(6,*) ' NMESH=',nmesh
        write(6,*) ' ERRALLOC=',erralloc
        call fstop(sname//' :: ALLOCATION MEMORY PROBLEM ')
       end if

!CAB      istriz = ikpsym
      istriz = 0
      call genkpt1(rslatt,kslatt,bases,itype,nbasis,                    &
     &              qmesh,wghtq,kmatchphase,nnmesh,istriz,invadd,       &
     &              rot,if0,if0temp,rx,madmat,kcheck,lrot,              &
     &              ndimq,nq,nqpt,nrot)
!c
!c  "madmat" -- temporary array
!c
!CAB     *              rot,if0,if0temp,rx,jf0,kcheck,

      if(iprint.ge.2) then
        write(6,*)
        write(6,*)                                                      &
     &  ('      Rotation matrices in Cartesian coordinates')
        do ir = 1,nrot
         write(6,1002) ir,((rot(ir,j,i),i=1,3),j=1,3)
1002     format (i3,3(1x,3f8.4))
!c        read(82,*) iir,((rot(ir,i,j),i=1,3),j=1,3)
        end do
        write(6,*)
        write(6,*)'          Atom transformation table:'
        do i=1,min(nbasis,999)
         write(6,1003) i,(if0(ir,i),ir=1,nrot)
1003     format(/' Atom ',i4,' is moved to atom ',12i4/                 &
     &          (28x,12i4))
        enddo

      end if

      do nsub=1,nsublat

       do iatom=1,nbasis
        if(itype(iatom).eq.nsub) go to 10
       end do
10     isite0 = iatom

       isite1 = isite0+1
       do iatsub=2,numbsub(nsub)
        do isite=isite1,nbasis
         if(itype(isite).eq.nsub) go to 20
        end do
        write(6,*) ' NSUB=',nsub
        write(6,*) ' NUMBSUB(NSUB)=',numbsub(nsub)
        write(6,*) ' ISITE0=',isite0,' ITYPE(ISITE0)=',itype(isite0)
        write(6,*) ' (ITYPE(isite), isite=',isite1,nbasis,') ='
        write(6,*) (itype(isite),isite=isite1,nbasis)
        call fstop(sname//':: you have wrong NUMBSUB ?')
20      isite1 = isite+1

        do ir=1,nrot
         if(if0(ir,isite0).eq.isite) go to 30
        end do
         write(6,1020) nsub,isite0,isite
1020     format(/3(1x,i5),                                              &
     &' You have wrong IF0 or Atoms are not equivalent by symmetry?')

!CAB      call fstop(sname//': you have wrong IF0()')

30      continue
       end do
      end do

      write(6,*)

      do imesh=1,nmesh
        itotal = 0
        do i=1,nqpt(imesh)
         itotal = itotal + wghtq(i,imesh)
        end do
        twght(imesh) = itotal
      end do

      deallocate(                                                       &
     &          kcheck                                                  &
     &         ,stat=erralloc)

      if(idop.ne.1) then
       call dops( rot, dop, nrot, lmax )
      end if

!CAB    nnmesh = nmesh
      nnmesh = 0

      call latvect(rslatt,bases,aij,kslatt,nbasis,ndimbas,              &
     &              qmesh,ndimq,nnmesh,nqpt,                            &
     &              Rksp,xknlat,ndimk,nksn,                             &
     &              Rrsp,xrnlat,ndimr,ntmprsn,                          &
     &              tmparr,iprint,itmpar)

      do j=1,nbasis
        do i=1,nbasis
          ncount = i+(j-1)*nbasis
          aij(1,ncount) = (bases(1,i)-bases(1,j))
          aij(2,ncount) = (bases(2,i)-bases(2,j))
          aij(3,ncount) = (bases(3,i)-bases(3,j))
        end do
      end do

      if(imdlng.le.1) then

!CDEBUG       rcut = (3.d0/(4.d0*pi)*(volume/nbasis)*nmdlng)**(one/three)
       rcut = (3.d0/(4.d0*pi)*(volume)*nmdlng)**(one/three)

       write(6,*) ' Rcut(Madelung)=',rcut

       call zeroout(tmparr,3)
       call genvec(rslatt,rcut,tmparr,1,                                &
     &              rsewd,ipmdlng,lastrs,iprint,                        &
     &              tmparr(4),itmpar)

       write(6,*) ' Nmdlng(RS)=',lastrs
       if(lastrs.gt.ipmdlng) call fstop(sname//' LASTRS > ipmdlng')

       kcut = (3.d0/(4.d0*pi)/(volume)*nmdlng)**(one/three)

       write(*,*) ' Kcut(Madelung)=',kcut

       call genvec(kslatt,kcut,tmparr,1,                                &
     &                  ksewd,ipmdlng,lastks,iprint,                    &
     &                  tmparr(4),itmpar)
       write(6,*) ' Nmdlng(KS)=',lastks
       if(lastks.gt.ipmdlng) call fstop(sname//' LASTKS > ipmdlng')

       if(imdlng.le.0) then
        do i=1,nbasis

         call madewd(lastrs,ipmdlng,lastks,ipmdlng,i,nbasis,            &
     &               rsewd,ksewd,rssq,kssq,aij,                         &
     &               tmparr(1),tmparr(1+nbasis*2),tmparr(1+nbasis*4),   &
     &               etamdl,volume,madmat(1,i),istop)

        enddo

        if(nbasis.le.10.or.iprint.ge.1) then
         write(6,'(/''  Madelung matrix'')')
         do i=1,nbasis
           write(6,'(1x,i4,8(f8.4))') i,(madmat(i,j),j=1,nbasis)
         enddo
        end if

       end if


       if(imethod.eq.3) then
        ident = -1
       else if(imethod.le.0) then
        ident = -100
       else
!CDEBUG       ident = 0
        ident = -100
       end if

       call strident1(bases,ndimbas,nbasis,itype,mapstr,mappnt,         &
     &                mapij,aij,ncount,ident,iprint)

!c
!c                 a_ij = b_i - b_j
!c

       if(nbasis.le.10.or.iprint.ge.1) then
        write(6,*)
        write(6,*) '  Map of (I,J)-equivalence'
        do j=1,nbasis
          write(6,1006) (mapstr(j,i),mappnt(j,i),i=1,nbasis)
1006      format(8(i6,i2))
        end do
       end if

       R_nn = one

       do i=2,ncount
         ri = sqrt(aij(1,i)**2+aij(2,i)**2+aij(3,i)**2)
         if(ri.gt.1.d-10) R_nn = min(R_nn,ri)
       enddo

       do i = 1,ntmprsn
        ri =                                                            &
     &   sqrt(max(zero,xrnlat(i,1)**2+xrnlat(i,2)**2+xrnlat(i,3)**2))
         if(ri.gt.1.d-10) R_nn = min(R_nn,ri)
       end do

       if(imethod.gt.0) then
        nrsn = 1
       else
        nrsn = ntmprsn
       end if

       write(6,*)
       write(6,*) '  R_nn/Alat=',real(R_nn),                            &
     &             '  Rrsp/R_nn=',real(Rrsp/R_nn)
       R_nn = R_nn*alat

       call ewldvect(xrnlat,nrsn,ndimr,Rrsp,                            &
     &                    aij,ncount,                                   &
     &                    xrslatij,nrhp,ndimrhp,                        &
     &                    np2r,numbrs,tmparr)


       write(6,*)
       write(6,*) '  NRSN=',nrsn,'  NRHP=',nrhp

       do i = 1,nrsn
         xrnlat(i,1) = xrnlat(i,1)*twopi
         xrnlat(i,2) = xrnlat(i,2)*twopi
         xrnlat(i,3) = xrnlat(i,3)*twopi
       end do

       do i = 1,nrhp
         xrslatij(i,1) = xrslatij(i,1)*twopi
         xrslatij(i,2) = xrslatij(i,2)*twopi
         xrslatij(i,3) = xrslatij(i,3)*twopi
         xrslatij(i,4) = xrslatij(i,4)*(twopi*twopi)
       end do

       do i=1,ncount
         aij(1,i)=aij(1,i)*twopi
         aij(2,i)=aij(2,i)*twopi
         aij(3,i)=aij(3,i)*twopi
       enddo
!c                                now again a_ij = b_i-b_j

      end if                    ! end of if(imdlng.le.1)

      if(istop.eq.sname) then
        call fstop(sname)
      endif
      return

      end
