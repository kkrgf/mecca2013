!c
!c     cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      subroutine putpot(nspin,nbasis,atcon,alat,ebot,efermi,            &
     &                  ivpoint,ikpoint,komp,                           &
     &                  jmt,rmt,jws,rr,xstart,                          &
     &                  vr,vdif,                                        &
     &                  rhotot,corden,                                  &
     &                  xvalws,                                         &
     &                  zcor,                                           &
     &                  numc,nc,lc,kc,ec,                               &
     &                  name,header,isDLM,isflipped,flippoint,          &
     &                  iprint,istop)
!c     ==================================================================
!c
      implicit real*8 (a-h,o-z)
!c
!c     ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
      include 'mkkrcpa.h'
!c     ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!c
      character  coerr*80
      character  sname*10
      character  istop*10
      character  name(ipcomp,ipsublat)*10
      character  header*80
!c
      integer   jmt(ipcomp,ipsublat)
      integer   jws(ipcomp,ipsublat)
      integer   komp(ipsublat)
      integer   ivpoint(ipsublat)
      integer   ikpoint(ipcomp,ipsublat)
      integer   numc(ipcomp,ipsublat,ipspin)
      integer   kc(ipeval,ipcomp,ipsublat,ipspin)
      integer   nc(ipeval,ipcomp,ipsublat,ipspin)
      integer   lc(ipeval,ipcomp,ipsublat,ipspin)
!c
      real*8    rmt(ipcomp,ipsublat)
      real*8    zcor(ipcomp,ipsublat)
      real*8    atcon(ipcomp,ipsublat)
      real*8    xvalws(ipcomp,ipsublat,ipspin)
!CALAM      real*8    xr(iprpts,ipcomp,ipsublat)
      real*8    rr(iprpts,ipcomp,ipsublat)
      real*8    h(ipcomp,ipsublat)
      real*8    xstart(ipcomp,ipsublat)
      real*8    vr(iprpts,ipcomp,ipsublat,ipspin)
      real*8    rhotot(iprpts,ipcomp,ipsublat,ipspin)
      real*8    corden(iprpts,ipcomp,ipsublat,ipspin)
!CALAM      real*8    semcor(iprpts,ipcomp,ipsublat,ipspin)
      real*8    ec(ipeval,ipcomp,ipsublat,ipspin)
      real*8    vdif,v0

      logical :: isDLM(ipsublat)

      logical :: isflipped(ipsublat)
      integer :: flippoint(ipsublat)

!c
!c ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
      parameter (sname='putpot')
      parameter (coerr='all right')
!c ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!c     ******************************************************************
!c     ccccc      write out he potentials for current sublattice if
!c     ccccc      they are not equivalent to some other sublattice
!c     cc   for spin polarized calc. vdif measures the difference between
!c     cc   mt-zero for maj. and min. spin electrons, assumes vmtz(maj)=0
!c     ******************************************************************
!c
      write(23,'(a80)') header
      write(23,'(i5,1x,d20.13)') nspin,vdif
      if(iprint.ge.0) then
         write(6,'(a80)') header
         write(6,'(i5,1x,d20.13)') nspin,vdif
      endif

      do nsub=1,nbasis
        do nk=1,komp(nsub)
          do ns=1,nspin
            v0=vdif*(ns-1)
            if(iprint.gt.-10) then
              write(6,'(''  For Spin ='',i3,5x,'' and   V0 ='',d20.13)')&
     &            ns,v0
            end if

               if(ikpoint(nk,nsub).eq.0 .or. ivpoint(nsub).eq.0) then
                if(iprint.ge.0) then
                  write(6,'(''    Sub-lattice ='',i3,                   &
     &                      ''  Component ='',i3)') nsub,nk
                end if

                 if( .not. isflipped(nsub) .and.                        &
     &                (.not. isDLM(nsub) .or. nk <= komp(nsub)/2) ) then

!c               ------------------------------------------------------
                   call potwrt(jmt(nk,nsub),rmt(nk,nsub),               &
     &                        jws(nk,nsub),                             &
     &                        rr(1,nk,nsub),                            &
     &                        xstart(nk,nsub),                          &
     &                        vr(1,nk,nsub,ns),v0,                      &
     &                        rhotot(1,nk,nsub,ns),                     &
     &                        corden(1,nk,nsub,ns),                     &
     &                        xvalws(nk,nsub,ns),                       &
     &                        zcor(nk,nsub),                            &
     &                        numc(nk,nsub,ns),                         &
     &                        nc(1,nk,nsub,ns),lc(1,nk,nsub,ns),        &
     &                        kc(1,nk,nsub,ns),ec(1,nk,nsub,ns),        &
     &                        atcon(nk,nsub),alat,ebot,efermi,          &
     &                        name(nk,nsub),                            &
     &                        iprint,istop)
!c                 ------------------------------------------------------

                 end if
               else
                if(iprint.ge.0) then
                  write(6,'(''    Sub-lattice ='',i3,                   &
     &                       ''  Component ='',i3)') nsub,nk
                  write(6,'(''    Potential is dummied to:'')')
                  write(6,'(''    Sub-lattice ='',i3,                   &
     &                      ''  Component ='',i3)')                     &
     &            ivpoint(nsub),ikpoint(nk,nsub)
                endif
               endif


            enddo
         enddo
      enddo
!c     ------------------------------------------------------------------
      rewind 23
!c     ------------------------------------------------------------------

      if (istop.eq.sname) then
         call fstop(sname)
      else
         return
      endif
      end
