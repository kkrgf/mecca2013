!c     cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      subroutine scalar(nrelv,clight,                                   &
     &                  l,                                              &
     &                  bjl,bnl,g,f,gpl,fpl,                            &
     &                  tandel,cotdel,anorm,alpha,                      &
     &                  energy,prel,                                    &
     &                  rv,r,r2,h,jmt,iend,                             &
     &                  iswzj,                                          &
     &                  iprint,istop)
!c     ================================================================
!c
      implicit complex*16 (a-h,o-z)
!c
!c     ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
      include 'lmax.h'
      include 'mkkrcpa.h'
!c     ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!c
      integer   icmax
!c
      character  istop*10
      character  sname*10
!c
      real*8 r(iprpts)
      real*8 rv(iprpts)
      real*8 r2(iprpts)
      real*8 fact(4),factl1
      real*8 zed
      real*8 h
      real*8 rvr
      real*8 v0
      real*8 cinv
      real*8 clight
      real*8 c2inv
      real*8 power
      real*8 ylag
!c parameter
      real*8 tole
!c
      complex*16 g(iprpts)
      complex*16 f(iprpts)
      complex*16 gpl(iprpts)
      complex*16 fpl(iprpts)
      complex*16 cl(iprpts)
      complex*16 sl(iprpts)
      complex*16 dcl(iprpts)
      complex*16 dsl(iprpts)
      complex*16 bjl(iplmax+2,iprpts)
      complex*16 bnl(iplmax+2,iprpts)
      complex*16 djl(iplmax+2,iprpts)
      complex*16 dnl(iplmax+2,iprpts)
      complex*16 energy
      complex*16 prel,p
!c
!c     ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
      parameter (icmax=5)
      parameter (tole=(one/ten)**10)
      parameter (sname='scalar')
!CAB
      real*8 smallnumber
      parameter (smallnumber=1.d-10)
!CAB
!c     ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

      complex*16, parameter :: sqrtm1 = dcmplx(0.d0,1.d0)
      complex*16 catanf
      catanf(gam)=.5d+00*sqrtm1*cdlog((sqrtm1+gam)/(sqrtm1-gam))

      complex*16 alpha
      complex*16 tl
      real*8 ph
      real*8, save :: lastph(4) = 0.d0
      integer, save :: iter(0:3) = 0
!c

      data fact/1.d0,3.d0,15.d0,105.d0/
!c
!c     ****************************************************************** !*
!c     sets up to solve the non- and scalar-relativistic equation.
!c     based on Calegero Method..............DDJ & FJP  July 1991
!c     uses Ricatti-Bessel fcts. for solution
!c     ****************************************************************** !*
!c
      if(iprint.ge.1) then
         write(6,'('' in SCALAR iprint '',i5)') iprint
         write(6,'('' scalar: l,iswzj='',2i5)') l,iswzj
         write(6,'('' scalar: nrelv,clight='',i5,d12.4)') nrelv,clight
         write(6,'('' scalar: energy='',2d16.8)') energy
         write(6,'('' scalar:   prel='',2d16.8)') prel
      endif
!c     =================================================================
!c     cannot solve for the real part of the energy equal to 0.0!!!!!!!!
      if( abs(energy) .lt. tole ) then
        write(6,'('' scalar:: energy=zero'')')
        call fstop(sname)
      endif
!c     =================================================================
!c     set atomic number................................................
      ized=-rv(1)/2 + 0.5
      zed=ized
!c     =================================================================

!      write(6,*) 'p=', prel
!      write(6,*) 'r=',r(1),r(jmt)
!      write(6,*) 'vr=',rv(1),rv(jmt)
!      write(6,*) 'cinv=',one/clight
!      write(6,*) 'zed=',zed

       if(zed.lt.  0.990d+00.and.nrelv.le.0) then
!CAB         if(iprint.ge.0) then
!CAB          write(6,*) sname//':: ZED=',zed
!CAB          write(6,*) ' for this case (ZED<1) relativistic approach ',
!CAB     *               ' is not implemented'
!CAB         end if
         nrel = 1
!CAB         call fstop(sname//': ZED<1 -- relativistic approach'
!CAB     *                   //' is not implemented')
       else
         nrel = nrelv
       endif

       factl1 = fact(min(l,3)+1)
       if(l.ge.4) then
        do j=2*4+1,2*l+1,2
          factl1 = factl1*j              ! factl1 = (2*l+1)!!
        end do
       end if
!CDEBUG
       ichange=0
       if(ized.eq.0) then
        do j=1,jmt
         if(abs(rv(j)).gt.smallnumber) go to 9
        end do
!c   To be able compute for zero-potential
        ichange=1
        do j=2,jmt
         rv(j) = rv(j)-smallnumber*r(j)
        end do
9       continue
       end if
!CDEBUG

!c     start with small r expansion of wave-fcts
      if(nrel.le.0) then
       cinv=one/clight
      else
       cinv = zero
      end if

      c2inv=cinv*cinv
      eoc2p1=one+energy*c2inv
!     write(6,*) 'eoc2p1=', eoc2p1
      tzoc=2*zed*cinv
      fz2oc2=tzoc*tzoc
      tzoc2=tzoc*cinv
      v0=(rv(1)+2*zed)/r(1)
!c      v0tmp=(rv(3)+2*zed)/r(3)
      em0= 1 + (energy-v0)*c2inv
      v0me= v0-energy
!CAB      if(nrel.le.-1) then

      if(nrel.le.0) then
!c        ==============================================================
!c        scalar-relativistic case......................................
         power=sqrt( l*(l+1) + 1 - fz2oc2 )
!c        a0 is arbitrary, could make Rl a spherical Bessel at origin.
!c              as for non-rel case:   R_l=x*j_l(x)= x*x**l/(2l+1)!!
!CDDJ     a0=1.0
!c              OR just use same expression as non-rel case, i.e.
!CAB      a0= prel**power/factl1
         a0= prel**(l+1)/factl1
         a1p=( fz2oc2 + em0*(power-1 -2*fz2oc2) )/(2*power+1)
         a1 = a0*a1p/tzoc2
         a2p=( a1p*(fz2oc2 + em0*(3*power+1 - 2*fz2oc2)) +              &
     &         em0*em0*(fz2oc2 - 2*power+2) )/(4*power+4)
         a2 = a0*a2p/(tzoc2*tzoc2)
         a3p=( a2p*( fz2oc2 + em0*(5*power+5 - 2*fz2oc2) )              &
     &        - a1p*(4*power+1 - fz2oc2)*em0*em0                        &
     &        + (3*power-3 - fz2oc2)*em0*em0*em0  )/(6*power+9)
         a3 = a0*a3p/(tzoc2*tzoc2*tzoc2)
      else
!c        ==============================================================
!c        non-relativistic case......................................
         power= l+1
!c
!c        a0 is arbitrary, but this make Rl a spherical Bessel at origin.
!c                        R_l=x*j_l(x)= x*x**l/(2l+1)!!
!c        a0= prel**(l+1)/factl1
         a0= prel**power/factl1
         a1p= -2*zed/(2*l+2)
         a1 = a0*a1p
         a2p= (v0me - a1p*2*zed)/(4*l+6)
         a2 = a0*a2p
         a3p= (a1p*v0me - a2p*2*zed)/(6*l+12)
         a3 = a0*a3p
      endif
!c     =================================================================
!c     get first 4 points from power series expansion...................

      do j=1,4

!c        ==============================================================
!c        r*G = g and r*F = f, i.e. this is r*wave-fct.
         g(j)= r(j)**power*(a0 + r(j)*(a1 + a2*r(j) + a3*r2(j)) )
         emr= eoc2p1*r(j) - rv(j)*c2inv
         f(j)= r(j)**power*( a0*(power-1) +                             &
     &   r(j)*(a1*power + a2*(power+1)*r(j) + a3*(power+2)*r2(j)))/emr
!c        ==============================================================
!c        get cl's and sl's
         cl(j)=-bnl(2,j)*g(j) - bnl(1,j)*f(j)/prel
         sl(j)=-bjl(2,j)*g(j) - bjl(1,j)*f(j)/prel
!c        ==============================================================
!c        get derivatives of cl and sl :: for predictor-corrector use
!c        constant increments on log grid therefore need dg/dx = r*dg/dr
         em= 1 + (energy - rv(j)/r(j))*c2inv
         dcl(j)= -bnl(2,j)*(em-1)*f(j)*r(j) -                           &
     &           ( l*(l+1)/(em*r(j)) + rv(j)  +                         &
     &           (eoc2p1-1)*energy*r(j) )*                              &
     &           g(j)*bnl(1,j)/prel
         dsl(j)= -bjl(2,j)*(em-1)*f(j)*r(j) -                           &
     &       ( l*(l+1)/(em*r(j)) + rv(j)  + (eoc2p1-1)*energy*r(j) )*   &
     &                             g(j)*bjl(1,j)/prel

      enddo
!c
!c     =================================================================
!c     regular solution and phase-shifts of scalar relativistic eqs.
      do j=5,jmt
!c        ==============================================================
!c        evaluate predictor
         cl(j)=cl(j-1)+h*( 55.0d+00*dcl(j-1)-59.0d+00*dcl(j-2)+         &
     &                37.0d+00*dcl(j-3)- 9.0d+00*dcl(j-4) )/24.0d+00
         sl(j)=sl(j-1)+h*( 55.0d+00*dsl(j-1)-59.0d+00*dsl(j-2)+         &
     &                37.0d+00*dsl(j-3)- 9.0d+00*dsl(j-4) )/24.0d+00
!c        ==============================================================
!c        evaluate corrector
         em = eoc2p1 - c2inv*rv(j)/r(j)
         emr= em*r(j)
         do icor=1,icmax
           clold=cl(j)
           slold=sl(j)
           y1=clold*bjl(1,j)-slold*bnl(1,j)
           y2=clold*bjl(2,j)-slold*bnl(2,j)
           dcl(j)= prel*bnl(2,j)*(em-1)*y2*r(j) -                       &
     &            ( l*(l+1)/emr + rv(j)  + (eoc2p1-1)*energy*r(j) )*    &
     &                             y1*bnl(1,j)/prel
           dsl(j)= prel*bjl(2,j)*(em-1)*y2*r(j) -                       &
     &            ( l*(l+1)/emr + rv(j)  + (eoc2p1-1)*energy*r(j) )*    &
     &                             y1*bjl(1,j)/prel
           clnew=cl(j-1)+h*( 9.0d+00*dcl(j)+19.0d+00*dcl(j-1)           &
     &                    - 5.0d+00*dcl(j-2)+dcl(j-3) )/24.0d+00
           slnew=sl(j-1)+h*( 9.0d+00*dsl(j)+19.0d+00*dsl(j-1)           &
     &                    - 5.0d+00*dsl(j-2)+dsl(j-3) )/24.0d+00
           cl(j)=clnew
           sl(j)=slnew
        enddo
           y1=cl(j)*bjl(1,j)-sl(j)*bnl(1,j)
           y2=cl(j)*bjl(2,j)-sl(j)*bnl(2,j)
           g(j)=y1
           f(j)=-prel*y2
      enddo
!c     =================================================================
!c     get normalization etc. at sphere boundary........................
!c     At R, need spherical Bessels for all l to get normalization and
!c     physical phase-shifts to determine cl and sl from
!c     g=r*R=r*( cl*jl - sl*nl ) and f=r*R'/M for all l's.
!c     Modify expression to account for difference between spherical and
!c     Ricatti-Bessel fcts.               Recall: R=g/r and dR=f*em/r
      j=jmt
      lmin=max(2,l+1)
      pr=prel*r(jmt)
      em = eoc2p1 - c2inv*rv(j)/r(j)
!c     =================================================================
!c     -----------------------------------------------------------------
        call ricbes(lmin,pr,bjl(1,j),bnl(1,j),djl(1,j),dnl(1,j))
!c     -----------------------------------------------------------------
!c     ==================================================================
!c     sl and cl at the sphere radius
      slmt= (prel*djl(l+1,j)-bjl(l+1,j)/r(j))*g(j) - bjl(l+1,j)*f(j)*em
      clmt= (prel*dnl(l+1,j)-bnl(l+1,j)/r(j))*g(j) - bnl(l+1,j)*f(j)*em
!c     ==================================================================
!c     phase shifts and normalization

      cotdel=clmt/slmt
      tandel=one/cotdel
      anorm=-(bjl(l+1,j)*cotdel-bnl(l+1,j))/g(j)

      ! alpha matrix = ratio between Rl=Zl*tl and sph. bes. Jl at origin
!     ! for lloyd formula
       tl = 1.d0/(prel*(dcmplx(0.d0,1.d0)-cotdel))
       alpha = (factl1/(prel*r(1))**l)*(tl*anorm*g(1)/r(1))
!       ph = real(catanf(1.d0/cotdel))

!       if( ph - lastph(l+1) >  pi/2.d0 ) ph = ph - pi
!       if( ph - lastph(l+1) < -pi/2.d0 ) ph = ph + pi
!      write(6,*) iter, mod(iter,4), 'iter'
!      select case( mod(iter(l),4) )
!        case(0)
!           write(600+l,*) real(energy), ph, dimag(cdlog(alpha))
!        case(1)
!           write(700+l,*) real(energy), ph, dimag(cdlog(alpha))
!        case(2)
!           write(800+l,*) real(energy), ph, dimag(cdlog(alpha))
!        case(3)
!           write(900+l,*) real(energy), ph, dimag(cdlog(alpha))
!      end select
!      iter(l) = iter(l)+1
!       lastph(l+1) = ph
      ! end alpha matrix calculation

!c     =================================================================
!c     get wave-fcts. beyond mt sphere
      lmin=max(2,l+1)
      jmtp1=jmt+1
      do j=jmtp1,iend
         pr=prel*r(j)
!c        --------------------------------------------------------------
         call ricbes(lmin,pr,bjl(1,j),bnl(1,j),djl(1,j),dnl(1,j))
!c        --------------------------------------------------------------
         g(j)=(clmt*bjl(l+1,j)-slmt*bnl(l+1,j))/prel
         f(j)= ( clmt*(djl(l+1,j)-bjl(l+1,j)/pr)                        &
     &         -slmt*(dnl(l+1,j)-bnl(l+1,j)/pr) )
      enddo
!c
!c     =================================================================

!c     zero out irregular solution arrays for case when iswzj=0
      if(iswzj .eq. 0)then
        call zerooutC(gpl,iprpts)
        call zerooutC(fpl,iprpts)
      endif

      if( iswzj .ne. 0 ) then
!c=====================================================================
!c irregular solution of scalar relativistic eqs.
!c=====================================================================
!c
!c  start irregular solution outside m.t. sphere
!c
      p = prel
      m = l+1
      do 70 j=jmt,iend
!c
!c starting with free solution ( v(r)=0 r > R), but using extrapolated
!c potentials at and beyond R to get derivative terms of cl and sl.
!c
        pr=p*r(j)
        gpl(j)=bjl(m,j)/p
        fpl(j)=(djl(m,j) - bjl(m,j)/pr)/eoc2p1
!c
!c  set cl and sl beyond mt sphere radius of potential
!c
!c extrapolate potential at m.t. radius to get starting value for irreg.
         rvr=ylag(r(j),r,rv,0,3,jmt,iex)
!c
         em = eoc2p1 - c2inv*rvr/r(j)
         emr= em*r(j)
!c
         cl(j)=-bnl(2,j)*gpl(j) - bnl(1,j)*fpl(j)/p
         sl(j)=-bjl(2,j)*gpl(j) - bjl(1,j)*fpl(j)/p
!c
!c Note: effect of potential on fpl here ignored: it is small.
         dcl(j)= -bnl(2,j)*(em-1)*fpl(j)*r(j) -                         &
     &       ( l*(l+1)/emr + rvr  + (eoc2p1-one)*energy*r(j) )*         &
     &                             gpl(j)*bnl(1,j)/p
         dsl(j)= -bjl(2,j)*(em-1)*fpl(j)*r(j) -                         &
     &       ( l*(l+1)/emr + rvr  + (eoc2p1-one)*energy*r(j) )*         &
     &                             gpl(j)*bjl(1,j)/p
!c
 70   continue

!c-----------------------------------------------------------------------
!c
!c  get irregular solution inside m.t. sphere
!c
      jmtm1=jmt-1
      do 50 j=jmtm1,1,-1
!c
!c    evaluate predictor
!c
        cl(j)=cl(j+1)-h*( 55.d0*dcl(j+1)-59.d0*dcl(j+2)+                &
     &                    37.d0*dcl(j+3)- 9.d0*dcl(j+4) )/24.d0
!c
        sl(j)=sl(j+1)+h*( 55.d0*dsl(j+1)-59.d0*dsl(j+2)+                &
     &                    37.d0*dsl(j+3)- 9.d0*dsl(j+4) )/24.d0
!c
!c    evaluate corrector
!c
         em = eoc2p1 - c2inv*rv(j)/r(j)
         emr= em*r(j)
!c
        do 60 icor=1,icmax
!c
          clold=cl(j)
          slold=sl(j)
!c
          y1=clold*bjl(1,j)-slold*bnl(1,j)
          y2=clold*bjl(2,j)-slold*bnl(2,j)
!c
         dcl(j)= p*bnl(2,j)*(em-1)*y2*r(j) -                            &
     &       ( l*(l+1)/emr + rv(j)  + (eoc2p1-1)*energy*r(j) )*         &
     &                             y1*bnl(1,j)/p
         dsl(j)= p*bjl(2,j)*(em-1)*y2*r(j) -                            &
     &       ( l*(l+1)/emr + rv(j)  + (eoc2p1-1)*energy*r(j) )*         &
     &                             y1*bjl(1,j)/p
!c
          clnew=cl(j+1)-h*( 9.d0*dcl(j)+19.d0*dcl(j+1)                  &
     &                    - 5.d0*dcl(j+2)+dcl(j+3) )/24.d0
          slnew=sl(j+1)-h*( 9.d0*dsl(j)+19.d0*dsl(j+1)                  &
     &                    - 5.d0*dsl(j+2)+dsl(j+3) )/24.d0
!c
          cl(j)=clnew
          sl(j)=slnew
!c
 60     continue
!c
            y1=cl(j)*bjl(1,j)-sl(j)*bnl(1,j)
            y2=cl(j)*bjl(2,j)-sl(j)*bnl(2,j)
!c
            gpl(j)=y1
            fpl(j)=-p*y2
 50     continue
!c===============end irregular solution calculation====================
      endif
!c
!c     ===============================================================

!CDEBUG
       if(ized.eq.0) then
        if(ichange.ne.0) then
         do j=1,jmt
          rv(j) = rv(j)+smallnumber*r(j)
         end do
        end if
       end if
!CDEBUG


      if(istop.eq.sname) then
         call fstop(sname)
      else
         return
      endif
!c
      end
