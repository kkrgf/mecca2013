!c  ###    PVM   ###

      include 'fpvm3.h'

      integer ME, MASTER
      parameter (MASTER = 1)

!c     NUMPROC is the maximum number of processors.
      integer numproc
      parameter (numproc = 100)

!c     NT_MAX is the maximum number of spawned tasks per processor
      integer nt_max
      parameter (nt_max = 2)

      integer  TIDS(1+numproc*nt_max)
      character*32 NAMEHOST(1:1+numproc)

!c  NHOST    is the available number of processors
      integer NHOST
!c  NPROC    is the number of processors used
      integer NPROC
!c  NTIDS    is the number of processes used
      integer NTIDS
!c  NPRLGRP   is the number of parallel group
      integer NPRLGRP

      character*10 enrggroup
      parameter (enrggroup = ' ENERGY')
      character*10 clstrgroup
      parameter (clstrgroup = ' CLUSTER')

      common /PARALLEL/NPRLGRP,ME,NHOST,NPROC,NTIDS,TIDS,NAMEHOST

      character*10 prllgroup(2)
      data  prllgroup/                                                  &
     &                enrggroup                                         &
     &               ,clstrgroup                                        &
     &               /

      character*8 DUMMY
      parameter (DUMMY=' DUMMY ')

      integer prllscheme

!c      parameter (prllscheme=0)   ! serial

      parameter (prllscheme=1)   ! energy points in parallel

!c      parameter (prllscheme=2)   ! LSMS: cluster tau in parallel,
!cc                                ! otherwise: energy points
