!BOP
!!ROUTINE: gauntd
!!INTERFACE:
      subroutine gauntd( gaunt, nj3, ij3, lmax, kkrsz )
!!DESCRIPTION:
! computes the integral of $ conjg(Y_{lp,mp}) Y_{l,m} Y_{ls,ms} $
! for $ lp+l+ls < 2*ngntd $
! using gaussian quadrature as given by
! M. Abramowitz and I.A. Stegun, handbook of mathematical functions,
! nbs applied mathematics series 55 (1968), pages 887 and 916
!

!!DO_NOT_PRINT
      implicit none
!!DO_NOT_PRINT

!!ARGUMENTS:
      integer lmax
      integer kkrsz
      real*8 gaunt(kkrsz*kkrsz,*)
      integer nj3(*)
      integer ij3(kkrsz*kkrsz,*)
!!REVISION HISTORY:
! Revised by W.A. Shelton, Jr. March 1, 1993
! Adapted - A.S. - 2013
!EOP
!
!BOC
      integer ndlj
      real(8) :: yr(1:(2*lmax+1)**2+1,1:(2*lmax+1)**2)
      real(8) :: w(1:(2*lmax+1)**2+1)
      integer    lmaxp1
      integer    n
      integer    l
      integer    ls
      integer    lp
      integer    m
      integer    ms
      integer    mp
      integer    jlow
      integer    jhigh
      integer    il
      integer    ils
      integer    ilp
      integer    istore
      integer    icount
      real(8), parameter ::  zero=0.d0
!c     ================================================================
      ndlj=(2*lmax+1)**2
      lmaxp1 = lmax + 1
      n = ndlj + 1
      nj3(1:kkrsz*kkrsz) = 0
      call gaunt2( n, lmaxp1, ndlj, w, yr )
      icount = 0
!c     ================================================================
      do l = 0,lmax
        do m = -l,l
          il = l*( l + 1 ) + m + 1
          do ls = 0,lmax
            do ms = -ls,ls
              icount = icount + 1
              ils = ls*( ls + 1 ) + ms + 1
              jlow = abs( l - ls )
              jhigh = l + ls
!c     ================================================================
              do lp = jlow,jhigh,2
                do mp = -lp,lp
                  ilp = lp*( lp + 1 ) + mp + 1
                  if( ms .eq. m + mp ) then !sk --> ? not m + ms = mp ?
                    if( mod( l + ls + lp, 2 ) .eq. 0 ) then
                      if( l - ls + lp .ge. 0 ) then
                        if( l + ls - lp .ge. 0 ) then
                          if( - l + ls + lp .ge. 0 ) then
                            istore = ( ils - 1 )*kkrsz + il
                            nj3(istore) = nj3(istore) + 1
                            ij3(istore,nj3(istore)) = ilp
                            gaunt(istore,nj3(istore)) = sum(w(1:n)*     &
     &                            yr(1:n,ilp)*yr(1:n,il)*yr(1:n,ils))
                          endif  ! end if statement over -l + ls +lp
                        endif    ! end if statement over  l + ls -lp
                      endif      ! end if statement over  l - ls +lp
                    endif        ! end if statement over mod( l + ls +lp )
                  endif          ! end if statement over mp .eq. m + ms
                enddo            ! end do loop over mp
              enddo              ! end do loop over lp
!c     ================================================================
            enddo                ! end do loop over ms
          enddo                  ! end do loop over ls
        enddo                    ! end do loop over m
      enddo                      ! end do loop over l
!c     ================================================================
      return

!EOC
      contains

!BOP
!!IROUTINE: gaunt2
!!INTERFACE:
      subroutine gaunt2( n, lmaxp1, ndlj, w, yr )
!!DESCRIPTION:
! computes normalized associated legendre functions at gauss-legendre 
! quadrature points, also generates their weights
! 
!!REMARKS:
! private procedure of subroutine gauntd
!EOP
!
!BOC
      implicit none
!c     ================================================================
!      include 'lmax.h'
!      integer ip2lmp1,ipdljp1
!      parameter (ip2lmp1 = 2*lcllmax+1)
!      parameter (ipdljp1=ip2lmp1**2+1)
!c     ================================================================
      integer, intent(in) :: lmaxp1
      integer, intent(in) :: n
      integer, intent(in) :: ndlj
      integer    lomax
      integer    nn
      integer    m
      integer    l
      integer    lm
      integer    k
!c     ================================================================
      real*8, intent(out) :: w(1:n)
      real*8, intent(out) :: yr(ndlj+1,ndlj)
      real*8, allocatable :: p(:,:)
      real*8 :: x(1:n)
      real*8 :: b(1:n)
      real*8     endpts(2)
      real*8     fpi
      real*8     rf
      real*8     cth
      real*8     sth
      real*8     fac
      real*8     a
      real*8     t
      real*8     cd
      real*8     sgm
      real*8     half,one,three
      parameter (half=0.50d0,one=1.d0,three=3.d0)
!c     ================================================================
!c      fpi = 4*pi
      fpi = 16.d0*datan(one)
      rf = fpi**( one/three )
      lomax = 2*( lmaxp1 - 1 )
      allocate( p(0:lomax+1,0:lomax) )
!c     ================================================================
!c     Obtain gauss-legendre points and weights
!c     ================================================================
      nn = n
!c     ================================================================
!c     call gaussw( nn, x, w )
!c     ================================================================
      endpts(1) = -one
      endpts(2) =  one
!c     ================================================================
      call gaussq( 1, nn, 0, endpts, b, x, w )
!c     ================================================================
      do m = 1,n
        w(m) = w(m)*half
      enddo
!c     ================================================================
!c     Generate associated legendre functions for m > 0
!c     ================================================================
      do k = 1,n
        cth = x(k)
        sth = sqrt( one - cth*cth )
        fac = one
!c     ================================================================
!c     Loop over m values
!c     ================================================================
        do m = 0,lomax
!c     ================================================================
          fac = -( 2*m - 1 )*fac
!c     ================================================================
          p(m,m) = fac
          p(m+1,m) = ( 2*m + 1 )*cth*fac
!c     ================================================================
!c     Recurse upward in l
!c     ================================================================
          do l = m+2,lomax
!c     ================================================================
            p(l,m) = ( ( 2*l - 1 )*cth*p(l-1,m) - ( l + m - 1 )         &
     &             *p(l-2,m) )/( l - m )
!c     ================================================================
          enddo                                   ! end do loop over l
!c     ================================================================
          fac = fac*sth
!c     ================================================================
        enddo                                     ! end do loop over m
!c     ================================================================
!c     Multiply in the normalization factors
!c     ================================================================
        do l = 0,lomax
!c     ================================================================
          a = rf*sqrt( ( 2*l + 1 )/fpi )
          cd = one
          lm =l*( l + 1 ) + 1
          yr(k,lm) = a*p(l,0)
          sgm = -one
!c     ================================================================
          do m = 1,l
            t = ( l + 1 - m )*( l + m )
            cd = cd/t
            yr(k,lm+m) = a*sqrt( cd )*p(l,m)
!CAB            yr(k,lm-m) = sgm*a*sqrt( cd )*p(l,m)
            yr(k,lm-m) = sgm*yr(k,lm+m)
            sgm = -sgm
!c     ================================================================
          enddo                                   ! end do loop over m
!c     ================================================================
        enddo                                     ! end do loop over l
!c     ================================================================
      enddo                                       ! end do loop over k
!c     ================================================================
      deallocate( p )
      return
!EOC
      end subroutine gaunt2

      end subroutine gauntd

