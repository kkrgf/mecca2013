!c
!c     ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      subroutine qexpp(ip,xl,xm,xh,x1,x2,x3,y1,y2,y3,slm,smh)
!c     =============================================================
!c
      implicit real*8 (a-h,o-z)
!c     ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
      include 'mkkrcpa.h'
!c     ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
      real*8  half,third
!c     ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
      parameter (half=one/two)
      parameter (third=one/three)
!c     ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!c
      xl2=xl*xl
      xm2=xm*xm
      xh2=xh*xh
      xl3=xl2*xl
      xm3=xm2*xm
      xh3=xh2*xh
      if(ip.eq.0) go to 5
        pinv=one/ip
        pinv2=pinv*pinv
        enxl=exp(ip*xl)
        enxm=exp(ip*xm)
        enxh=exp(ip*xh)
        xenxl=xl*enxl
        xenxm=xm*enxm
        xenxh=xh*enxh
        x2enxl=xl2*enxl
        x2enxm=xm2*enxm
        x2enxh=xh2*enxh
5     continue
!c
      a=y1
      b=(y2-y1)/(x2-x1)
      c=( (y3-y1)/(x3-x1) -b )/(x3-x2)
      ap=a-b*x1 + c*x1*x2
      bp=b-c*(x1+x2)
!c
       if(ip.eq.0)then
        slm=ap*(xm-xl)+half*bp*(xm2-xl2)+third*c*(xm3-xl3)
        smh=ap*(xh-xm)+half*bp*(xh2-xm2)+third*c*(xh3-xm3)
       else
        app=ap-bp*pinv+two*c*pinv2
        bpp=bp-two*c*pinv
        slm=(app*(enxm-enxl)+bpp*(xenxm-xenxl)+c*(x2enxm-x2enxl))*pinv
        smh=(app*(enxh-enxm)+bpp*(xenxh-xenxm)+c*(x2enxh-x2enxm))*pinv
       endif
!c
      return
      end
