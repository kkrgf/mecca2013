!c     cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      subroutine potwrt(jmt,rmt,jws,rr,xstart,                          &
     &                  vr,v0,                                          &
     &                  rhotot,corden,                                  &
     &                  xvalws,zcor,                                    &
     &                  numc,nc,lc,kc,ec,                               &
     &                  atcon,alat,ebot,efermi,name,iprint,istop)
!c     ==================================================================

      use mecca_constants
      implicit real*8 (a-h,o-z)
!c
!c ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!      include 'mkkrcpa.h'
!c ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!c
      character istop*10,sname*10,coerr*80
      character name*10
!c
      integer   nc(ipeval)
      integer   lc(ipeval)
      integer   kc(ipeval)
!c
!CALAM      real*8    xr(iprpts),rr(iprpts)
      real*8    rr(iprpts)
      real*8    vr(iprpts),vrtmp(iprpts)
      real*8    rhotot(iprpts)
      real*8    corden(iprpts)
!CALAM      real*8    semcor(iprpts)
      real*8    ec(ipeval)
      real*8    atcon
!c
!c ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
      parameter (sname='potwrt')
!c ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!c
!c     ---------------------------------------------------------
!      character lj(7)*4
!      data lj/'s1/2','p1/2','p3/2','d3/2','d5/2','f5/2','f7/2'/
!c     ---------------------------------------------------------
!c
      if (jws.ge.iprpts) then
         coerr=' jws greater than iprpts'
         write(6,'('' jws='',i3,2x,''iprpts='',i3)') jws,iprpts
         call fstop(sname//':'//coerr)
      endif
!c
!      ized=-vr(1)+1.0
!      ized=ized/2
      ized = g_zed(vr,rr)
!      azed=ized
      aa=alat
      zcd=zcor
      xst=xstart
      xmt=log(rmt)
      jmt0=jmt
      ntchg=jws
      ntcor=jws
!c     ==================================================================
!c     shift minority potentials by vdif !!   (same as in DDJ's old code)
!c     ==================================================================
      do ir=1,jmt0
        vrtmp(ir)= vr(ir) + v0*rr(ir)
      enddo
!c     ==================================================================

!c     ==================================================================
      if(iprint.ge.1) then
       write(6, '(5x,a10,5x,'' conc='',f10.7,4x,'' z='',i3,5x,          &
     &           ''xvalws='',f10.5)') name,atcon,ized,xvalws
       write(6, '(5x,f5.0,17x,f12.5,f5.0,e20.13)')                      &
     &                                float(ized),aa,zcd,efermi
       write(6, '(5x,2(e20.13,2x),i5)') xst,xmt,jmt0
       write(6,'(5x,3e20.13)') (vrtmp(j),j=1,jmt0,50)
       write(6,'(5x,e20.13)') v0
       write(6,'(5x,i5,e20.13)') ntchg,xvalws
       write(6,'(5x,3e20.13)') (rhotot(j),j=1,ntchg,50)
       write(6,'(5x,'' numc='',i5,''    ntor='',i5)') numc,ntcor
       do j=1,numc
        id = lc(j) + iabs(kc(j))
        write(6,'(5x,3i5,f12.5,9x,i1,a4)')                              &
     &                       nc(j),lc(j),kc(j),ec(j),nc(j),lj(id)
       enddo
       write(6,'(5x,3e20.13)') (corden(j),j=1,ntcor,50)
      end if
!c     ==================================================================
!c
!c  write out potential..................................................
!c
!c     ==================================================================
      write(23,'(5x,a10,5x,'' conc='',f10.7,4x,'' z='',i3,5x,           &
     &           ''xvalws='',f10.5)') name,atcon,ized,xvalws
      write(23,'(f5.0,13x,f12.5,f5.0,f12.5,e20.13)')                    &
     &                                float(ized),aa,zcd,ebot,efermi
      write(23,'(17x,2(e20.13,2x),i5)') xst,xmt,jmt0
      write(23,'(4e20.13)') (vrtmp(j),j=1,jmt0)
      write(23,'(35x,e20.13)') v0
!c
!c  write out total charge density........,..............................
!c
      write(23,'(i5,e20.13)') ntchg,xvalws
      write(23,'(4e20.13)') (rhotot(j),j=1,ntchg)
!c
!c  write out core state information.....................................
!c
      ndum=0                  ! for DDJ's old code
      write(23,'(3i5)') numc,ntcor,ndum

      do j=1,numc
       id = lc(j) + iabs(kc(j))
       write(23,'(3i5,f12.5,9x,i1,a4)')                                 &
     &                       nc(j),lc(j),kc(j),ec(j),nc(j),lj(id)
      enddo
!c
!c  write out total core density........................................
!c
      write(23,'(4e20.13)') (corden(j),j=1,ntcor)
!c
!c     ================================================================== !=
!c
      if (istop.eq.sname) then
         call fstop(sname)
      endif
!c
      return
      end
