!c     ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      subroutine getdos(nrelv,clghtspin,                                &
     &                  lmax,kkrsz,                                     &
     &                  nbasis,                                         &
     &                  komp,                                           &
     &                  iswzj,                                          &
     &                  h,jmt,jws,rr,vr,                                &
     &                  energy,prel,                                    &
     &                  tau00,tcpa,tab,                                 &
     &                  pzz,pzj,                                        &
     &                  pzzck,pzjck,                                    &
     &                  dos,dosck,                                      &
     &                  green,                                          &
     &                  w1,w2,w3,w4,                                    &
     &                  iprint,ivpoint,ikpoint,istop)
!c     =================================================================
!c
!c
!c     *****************************************************************
!c      input:  w/ Z regular and J irrregular sols. to Schrodinger Eq.
!c             tau00   (tauc)
!c             tcpa    (tc)
!c             tab     (t-matrix for atoms a & b)
!c             pzz     (ZZ integrals MT)
!c             pzj     (ZJ integrals MT)
!c             pzzck   (ZZ integrals WS)
!c             pzjck   (ZJ integrals WS)
!c             kkrsz   (size of KKR-matrix)
!c             komp    (number of components on sublattice)
!c             istop   (index of subroutine prog. stops in)
!c       output:
!c             dos     (wigner-seitz cell density of states)
!c             dosck   (muffin-tin density of states)
!c     ****************************************************************
!c
      use mecca_constants
      use mtrx_interface
      implicit real*8  (a-h,o-z)
!c
!c     ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!      include 'lmax.h'
!      include 'mkkrcpa.h'
!c     ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!c
      character istop*10
      character sname*10
!c
      integer    komp(ipsublat)
      integer    jmt(ipcomp,ipsublat)
      integer    jws(ipcomp,ipsublat)
      integer    ivpoint(ipsublat)
      integer    ikpoint(ipcomp,ipsublat)

!c
      real*8     vr(iprpts,ipcomp,ipsublat)
!c      real*8     xr(iprpts,ipcomp,ipsublat)
      real*8     rr(iprpts,ipcomp,ipsublat)
      real*8     h(ipcomp,ipsublat)
!c
      complex*16 tau00(ipkkr,ipkkr,ipsublat)
      complex*16 tcpa(ipkkr,ipkkr,ipsublat)
      complex*16 w1(ipkkr,ipkkr)
      complex*16 w2(ipkkr,ipkkr)
      complex*16 w3(ipkkr,ipkkr)
      complex*16 w4(ipkkr,ipkkr)
      complex*16 tab(ipkkr,ipkkr,ipcomp,ipsublat)
      complex*16 pzz(ipkkr,ipkkr,ipcomp,ipsublat)
      complex*16 pzj(ipkkr,ipkkr,ipcomp,ipsublat)
      complex*16 pzzck(ipkkr,ipkkr,ipcomp,ipsublat)
      complex*16 pzjck(ipkkr,ipkkr,ipcomp,ipsublat)
      complex*16 dos(0:iplmax,ipcomp,ipsublat)
      complex*16 dosck(ipcomp,ipsublat)
      complex*16 green(iprpts,ipcomp,ipsublat)
      complex*16 energy,prel
!c parameter
      character(2), parameter :: updn(2) = ['up','dn']
!c
!c     ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!      complex*16 cone
!      parameter (cone=(1.d0,0.d0))
      real(8), parameter ::  onem=-one
      parameter (sname='getdos')
!c     ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!c

!cab
!cab       if(iprint.ge.5) then
!cab        write(6,'('' GETDOS## tau00 ::'',2d12.5)') edu
!cab        call wrtdia2(tau00,kkrsz,nbasis,istop)
!cab        write(6,'('' GETDOS##   tab ::'',2d12.5)') edu
!cab        call wrtdia2(tab,kkrsz,nbasis,istop)
!cab       end if
!cab

      ispin = 1
      if(clghtspin.lt.0) then
       ispin = 2
      end if
      clight = abs(clghtspin)

      if(iprint.ge.4) then
         write(6,'('' getdos:: nbasis,kkrsz'',3i4)')                    &
     &                         nbasis,kkrsz
         do nsub=1,nbasis
               write(6,'('' GETDOS: tau00 '')')
               call wrtmtx(tau00(1,1,nsub),kkrsz,istop)
            do ic=1,komp(nsub)
               write(6,'('' getdos:: pzz ::'')')
!c              -------------------------------------------------------
               call wrtmtx(pzz(1,1,ic,nsub),kkrsz,istop)
!c              -------------------------------------------------------
               write(6,'('' getdos:: pzj ::'')')
!c              -------------------------------------------------------
               call wrtmtx(pzj(1,1,ic,nsub),kkrsz,istop)
!c              -------------------------------------------------------
!c              write(6,'('' getdos:: pzzck ::'')')
!c              -------------------------------------------------------
!c              call wrtmtx(pzzck(1,1,ic,nsub),kkrsz,istop)
!c              -------------------------------------------------------
!c              write(6,'('' getdos:: pzjck ::'')')
!c              -------------------------------------------------------
!c              call wrtmtx(pzjck(1,1,ic,nsub),kkrsz,istop)
!c              -------------------------------------------------------
            enddo
         enddo
      endif
!c
!c     =================================================================
!c     -----------------------------------------------------------------
      call zerooutC(dos,(iplmax+1)*ipcomp*ipsublat)
!c     -----------------------------------------------------------------
      call zerooutC(dosck,ipcomp*ipsublat)
!c     -----------------------------------------------------------------
!c
      do nsub=1,nbasis
         if(iprint.ge.4) then
            write(6,'('' getdos::  nsub ::'',i3)') nsub
         endif
!c         errcpa=zero
!c        =============================================================
!c        tcpa => t_{c}(old) = t_{c}...................................
!c        w4   => t_{c}^{-1} = m_c.....................................
!c        -------------------------------------------------------------
         call matinv(tcpa(1,1,nsub),w4,w3,kkrsz, iprint,istop)
!c        -------------------------------------------------------------
         if(iprint.ge.4) then
            write(6,'('' getdos:: t_c(old) ::'')')
            call wrtmtx(tcpa(1,1,nsub),kkrsz,istop)
            write(6,'('' getdos:: m_c(old) ::'')')
            call wrtmtx(w4,kkrsz,istop)
         endif
         do ic=1,komp(nsub)
!c           ==========================================================
!c           w2 => t_a(b)^{-1} = m_a(b)................................
!c           ----------------------------------------------------------
            call matinv(tab(1,1,ic,nsub),w2,w3,kkrsz, iprint,istop)
!c           ----------------------------------------------------------
            if(iprint.ge.4) then
               write(6,'('' getdos::  ic ::'',i3)') ic
               write(6,'('' getdos:: t_a(b) ::'')')
!c              -------------------------------------------------------
               call wrtmtx(tab(1,1,ic,nsub),kkrsz,istop)
!c              -------------------------------------------------------
               write(6,'('' getdos:: m_a(b) ::'')')
!c              -------------------------------------------------------
               call wrtmtx(w2,kkrsz,istop)
!c              -------------------------------------------------------
            endif
!c           ==========================================================
!c           w1 => m_c - m_a(b)........................................
!c           ----------------------------------------------------------
            call madd(w4,onem,w2,w1,kkrsz,iprint,istop)
!c           ----------------------------------------------------------
!c           ==========================================================
!c           w2 => tau_c*[m_c - m_a(b)]^{-1}...........................
!c           ----------------------------------------------------------
            call mmul(tau00(1,1,nsub),w1,w2,kkrsz,iprint,istop)
!c           ----------------------------------------------------------
            if(iprint.ge.4) then
               write(6,'('' getdos::m_c -  m_a(b) ::'')')
!c              -------------------------------------------------------
               call wrtmtx(w1,kkrsz,istop)
!c              -------------------------------------------------------
               write(6,'('' getdos::tau_c*[m_c -  m_a(b)] ::'')')
!c              -------------------------------------------------------
               call wrtmtx(w2,kkrsz,istop)
!c              -------------------------------------------------------
            endif
!c           ==========================================================
!c           w1 => [1-tau_c*[m_c - m_a(b)]]^{-1}] = D^{-1}.............
!c           ----------------------------------------------------------
            call zerooutC(w3,ipkkr*ipkkr)
!c           ----------------------------------------------------------
            do i=1,kkrsz
               w3(i,i)=cone
            enddo
!c           ----------------------------------------------------------
            call madd(w3,onem,w2,w1,kkrsz,iprint,istop)
!c           ----------------------------------------------------------
            if(iprint.ge.4) then
               write(6,'('' getdos:: D^{-1} ::'')')
!c              -------------------------------------------------------
               call wrtmtx(w1,kkrsz,istop)
!c              -------------------------------------------------------
            endif
!c           ==========================================================
!c           w2 => D ..................................................
!c           ----------------------------------------------------------
            call matinv(w1,w2,w3,kkrsz,iprint,istop)
!c           ----------------------------------------------------------
            if(iprint.ge.4) then
               write(6,'('' getdos:: D ::'')')
!c              -------------------------------------------------------
               call wrtmtx(w2,kkrsz,istop)
!c              -------------------------------------------------------
            endif
!c           ==========================================================
!c           w1 => D*tau_c = tau_a(b).................................
!c           ----------------------------------------------------------
            call mmul(w2,tau00(1,1,nsub),w1,kkrsz,                      &
     &                iprint,istop)
!c           ----------------------------------------------------------

            if(iprint.ge.4) then
               write(6,'('' getdos:: tau_a(b) ::'')')
!c              -------------------------------------------------------
               call wrtmtx(w1,kkrsz,istop)
!c              -------------------------------------------------------
               write(6,'('' getdos:: pzz ::'')')
!c              -------------------------------------------------------
               call wrtmtx(pzz(1,1,ic,nsub),kkrsz,istop)
!c              -------------------------------------------------------
               write(6,'('' getdos:: pzj ::'')')
!c              -------------------------------------------------------
               call wrtmtx(pzj(1,1,ic,nsub),kkrsz,istop)
!c              -------------------------------------------------------
               if(jmt(ic,nsub).lt.jws(ic,nsub)) then
                write(6,'('' getdos:: pzzck ::'')')
!c              -------------------------------------------------------
                call wrtmtx(pzzck(1,1,ic,nsub),kkrsz,istop)
!c              -------------------------------------------------------
                write(6,'('' getdos:: pzjck ::'')')
!c              -------------------------------------------------------
                call wrtmtx(pzjck(1,1,ic,nsub),kkrsz,istop)
!c              -------------------------------------------------------
               endif
            endif
!c           ==========================================================
!c           dos =>  ZZ*tau_a(b) - ZJ_a(b).............................
!c           write(6,'('' getdos:: calling mdos n(e) WS ::'')')
!c           ----------------------------------------------------------
            call mdos(dos(0,ic,nsub),                                   &
     &                pzz(1,1,ic,nsub),                                 &
     &                pzj(1,1,ic,nsub),                                 &
     &                w1,                                               &
     &                kkrsz,0,istop)
!c            if(iprint.ge.0)
!c     *      write(6,1001) energy,dos(ic,nsub),updn(ispin),nsub,ic
!c 1001        format(2f10.7,1x,2g16.8,' E',a2,',dos(E) nsub=',i4,1x,i2)
!c           ----------------------------------------------------------
             dosck(ic,nsub) = dcmplx(0.d0,0.d0)
             do il = 0,lmax
               dosck(ic,nsub) = dosck(ic,nsub) + dos(il,ic,nsub)
             end do
!c            call mdos(dosck(ic,nsub),
!c     >                pzzck(1,1,ic,nsub),
!c     >                pzjck(1,1,ic,nsub),
!c     >                w1,
!c     >                kkrsz,0,istop)
!c           ----------------------------------------------------------
            if( ivpoint(nsub) .eq. 0 ) then
!c             write(6,'('' getdos:: calling mgreen  ::'')')
              call mgreen(green(1,ic,nsub),w1,kkrsz,                    &
     &                    nrelv,clight,                                 &
     &                    lmax,                                         &
     &                    energy,prel,                                  &
     &                    h(ic,nsub),                                   &
     &                    jmt(ic,nsub),                                 &
     &                    jws(ic,nsub),                                 &
!CALAM     >                    xr(1,ic,nsub),
     &                    rr(1,ic,nsub),                                &
     &                    vr(1,ic,nsub),                                &
     &                    iswzj,                                        &
     &                    iprint,istop)
           else
!CAA             do ir=1,jws(ic,nsub)
!C           For VP Integration
             do ir=1,iprpts
               green(ir,ic,nsub) =                                      &
     &         green(ir,ikpoint(ic,nsub),ivpoint(nsub))
             enddo
           endif
!c
         enddo
      enddo
!c     =================================================================
      if(istop.eq.sname) then
        call fstop(sname)
      endif
!c     =================================================================
      return
      end subroutine getdos
