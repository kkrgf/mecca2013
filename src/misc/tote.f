!c     cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      subroutine tote(vrold,vrnew,rho,corden,xr,rr,                     &
     &                jmt,nbasis,nspin,                                 &
     &                numbsub,komp,atcon,ztotss,omegws,rho2,rhoqint,    &
     &                excort,qintex,emad,emadp,                         &
     &                evalsum,ecorv,esemv,                              &
     &                etot,press,Eccsum,mtasa,iprint,istop)
!c     ==================================================================
!c
      use mecca_constants
      implicit real*8 (a-h,o-z)
!c
!c     ==================================================================
!c     total energy for multi-sublattice and multi-component KKR-CPA
!c     within ASA and MT, for both non- and  spin-polarized cases.
!c     written for ASA and spin-polarization     by WAS & DDJ Sept 1993
!c     modified for mean-field, charge-correlated CPA  by DDJ  Dec 1993
!c     ==================================================================
!c
!CAB          nbasis -- number of inequivalent sublattices !
!c
!c
!c
!c     routine calls:  derv5    5 point derivative routine
!c                     qexpup   an integration algorithm
!c                     fstop    to stop in this routine
!c
!c     ==================================================================
!c
!c ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!      include 'mkkrcpa.h'
!c ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!c
      character  sname*10
!c
!c parameter
      real*8   third
      integer  iexch
!c ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
      parameter (sname='tote')
      parameter (iexch=10)
      parameter (third=one/three)
!c     ***********************************
!c     convert pressure from mRy to Mbars
!      parameter (pfact=147.1d00)
!c     ***********************************
!c ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!c
      character  istop*10
!c
      integer nbasis
      integer jmt(ipcomp,nbasis)
!CALAM      integer jws(ipcomp,nbasis)
      integer komp(nbasis),numbsub(nbasis)
      integer nspin
      integer mtasa
      integer iprint
!c
      real*8 vrold(iprpts,ipcomp,ipsublat,ipspin)
      real*8 vrnew(iprpts,ipcomp,ipsublat,ipspin)
      real*8 rho(iprpts,ipcomp,ipsublat,ipspin)
      real*8 corden(iprpts,ipcomp,ipsublat,ipspin)
      real*8 rr(iprpts,ipcomp,nbasis)
      real*8 xr(iprpts,ipcomp,nbasis)
      real*8 atcon(ipcomp,nbasis)
      real*8 ztotss(ipcomp,nbasis)
      real*8 rho2(nbasis)
      real*8 rhoqint(nbasis)
      real*8 excort(ipspin)
      real*8 qintex(ipspin)
      real*8 evalsum(ipcomp,ipsublat,ipspin)
      real*8 ecorv(ipcomp,ipsublat,ipspin)
      real*8 esemv(ipcomp,ipsublat,ipspin)
      real*8 rhov(iprpts,ipcomp)
      real*8 enxc(iprpts,ipcomp)
      real*8 vx(iprpts,ipcomp)
      real*8 rinv(iprpts,ipcomp)
      real*8 derv(iprpts)
      real*8 bndint(iprpts)
      real*8 bnd(iprpts)
      real*8 coren(ipcomp)
      real*8 valen(ipcomp)
      real*8 correc(ipcomp)
      real*8 cor3pv(ipcomp)
      real*8 pterm1(ipcomp)
      real*8 exchen(ipcomp)
      real*8 ezpt(ipcomp)
      real*8 tpzpt(ipcomp)
      real*8 excormt(ipcomp)
      real*8 omegws
      real*8 totrho
      real*8 xmom
      real*8 emad
      real*8 emadp
      real*8 etot,e_per_atom
      real*8 press,three_pv
      real*8 Eccsum
      real*8 effvol
!c     ==================================================================

       nsite = 0
       natom = 0
       do nsub = 1,nbasis
      nsite = nsite + numbsub(nsub)
      zatom = 0
      do ik=1,komp(nsub)
       zatom = zatom + atcon(ik,nsub)*ztotss(ik,nsub)
      end do
!c  to exclude empty spheres
      if(zatom.gt.1.d-7) natom=natom+numbsub(nsub)
       end do

       etot=zero
       three_pv=zero
!c
!c     calculate 1/r . . . . . . . . . . . . . . . . . . . . . .
      do nsub = 1,nbasis
        do is = 1,nspin
          do ik = 1,komp(nsub)
            do ir = 1,jmt(ik,nsub)
              rinv(ir,ik)=1/rr(ir,ik,nsub)
              rhov(ir,ik)=rho(ir,ik,nsub,is)-corden(ir,ik,nsub,is)
            enddo
            call qexpup(1,rhov(1,ik),jmt(ik,nsub),xr(1,ik,nsub),bnd)
!c     ==================================================================
            if( iprint .ge. 1 ) then
              write(6,'('' val+sem chg. '',i5,f14.6)')                  &
     &            ik,bnd(jmt(ik,nsub))+rhov(1,ik)*rr(1,ik,nsub)/three
            endif
!c     ==================================================================
          enddo
!c     ==================================================================
!c     calculate dv/dr per for each component where one piece of dv/dr is
!c     equal to vhartree: add this piece to the above and calculate
          do ik=1,komp(nsub)
            call derv5(vrold(1,ik,nsub,is),derv,rr(1,ik,nsub),          &
     &                 jmt(ik,nsub))
!c     ==================================================================
!cALAM     calculate:  -int 2pi*corden*dv/dr r3 dr = \sum_c[E_c] -
!c                   int 4*pi*r^2*corden*d(rV)/dr * dr
!c     ==================================================================
            do i=1,jmt(ik,nsub)
              bndint(i)=-corden(i,ik,nsub,is)*derv(i)
            enddo
            call qexpup(1,bndint,jmt(ik,nsub),xr(1,ik,nsub),bnd)

            coren(ik)=ecorv(ik,nsub,is)+ bnd(jmt(ik,nsub)) -            &
     &                corden(1,ik,nsub,is)*derv(1)*rr(1,ik,nsub)/three
!c     ==================================================================
!c     pressure: calculate -int 4pi valden d(r2v)/dr r dr ...............
            do i=1,jmt(ik,nsub)
              bndint(i)=rhov(i,ik)                                      &
     &                 *( derv(i)+vrold(i,ik,nsub,is)*rinv(i,ik) )
            enddo
            call qexpup(1,bndint,jmt(ik,nsub),xr(1,ik,nsub),bnd)
            pterm1(ik)=-bnd(jmt(ik,nsub))-rhov(1,ik)*(rr(1,ik,nsub)*    &
     &                  derv(1)+vrold(1,ik,nsub,is))/three
          enddo
!c     ==================================================================
!c     calculate:  -int 4pi rhov*d(rv)/dr r2 dr .........................
          do ik=1,komp(nsub)
            call derv5(vrnew(1,ik,nsub,is),derv,rr(1,ik,nsub),          &
     &                 jmt(ik,nsub))
            do i=1,jmt(ik,nsub)
              bndint(i)=rhov(i,ik)*derv(i)
            enddo
            call qexpup(1,bndint,jmt(ik,nsub),xr(1,ik,nsub),bnd)

            valen(ik)= -bnd(jmt(ik,nsub)) - (rhov(1,ik)*derv(1)*        &
     &                   rr(1,ik,nsub))/three
          enddo
!c     ==================================================================
!c     calculate: int 4pi rho (4exc-3vxc) r2 dr..........................
          sp = dfloat( 3 - 2*is)
          do ik=1,komp(nsub)
            do i = 1,jmt(ik,nsub)
              totrho =                                                  &
     &            rho(i,ik,nsub,1) + ( nspin - 1 )*rho(i,ik,nsub,nspin)
              xmom = rho(i,ik,nsub,1) - rho(i,ik,nsub,nspin)
              dz   = xmom/totrho
              ro3=( three*(rr(i,ik,nsub)**2)/totrho )**third
              vx(i,ik)=alpha2(ro3,dz,sp,iexch,enxc(i,ik))
            enddo
            do i=1,jmt(ik,nsub)
              bndint(i)=rho(i,ik,nsub,is)                               &
     &                 *( four*enxc(i,ik)-three*vx(i,ik) )
            enddo
            call qexpup(1,bndint,jmt(ik,nsub),xr(1,ik,nsub),bnd)
            exchen(ik)=bnd(jmt(ik,nsub))+rho(1,ik,nsub,is)              &
     &                *(four*enxc(1,ik)/three-vx(1,ik))*rr(1,ik,nsub)
          enddo
!c
          call varEnCorr(komp(nsub),jmt(:,nsub),xr(:,1:komp(nsub),nsub),&
     &                   vrold(:,1:komp(nsub),nsub,is),                 &
     &                   vrnew(:,1:komp(nsub),nsub,is),                 &
     &                   rhov(:,1:komp(nsub)),                          &
     &                   corden(:,1:komp(nsub),nsub,is),                &
     &                   correc(ik),cor3pv(ik))
!c     ==================================================================
!          if( mtasa .le. 0 .and. komp(nsub) .gt. 1 ) then
!            write(6,'('' NEED INTERSTITAL MT COULOMB FIX a la DDJ '')')
! TODO??: correction for interstitial rho of each species for MT-case                         
!          endif
!c     ==================================================================
!c     calculate the zeropoint energy....................................
!c     ==================================================================
        effvol=omegws/natom
          call zeropt(ezpt,tpzpt,effvol,ztotss(1,nsub),                 &
     &                                       komp(nsub),nspin)
!c     ==================================================================
        etot0 = zero
        pv3   = zero
        do ik=1,komp(nsub)
         etot0=etot0 + atcon(ik,nsub)*(coren(ik)+valen(ik)+exchen(ik)   &
     &         + correc(ik)+ezpt(ik)                                    &
     &         + evalsum(ik,nsub,is)+esemv(ik,nsub,is)                  &
     &         - rr(jmt(ik,nsub),ik,nsub)*rho(jmt(ik,nsub),ik,nsub,is)  &
     &         *( enxc(jmt(ik,nsub),ik)-vx(jmt(ik,nsub),ik) ) )
!c     ==================================================================
           excormt(ik)=-rr(jmt(ik,nsub),ik,nsub)                        &
     &               *rho(jmt(ik,nsub),ik,nsub,is)                      &
     &               *( enxc(jmt(ik,nsub),ik)-vx(jmt(ik,nsub),ik) )
            if( iprint .ge. 1 ) then
              write(6,'('' Spin '',i5,'' Sublattice '',i5)') is,nsub
              write(6,'('' tote:  pterm1(ik)'',f14.6)')  pterm1(ik)
              write(6,'('' tote:  cor3pv(ik)'',f14.6)')  cor3pv(ik)
              write(6,'('' tote:   tpzpt(ik)'',f14.6)')   tpzpt(ik)
              write(6,'('' tote: evalsum(ik)'',f14.6)')                 &
     &                           evalsum(ik,nsub,is)
              write(6,'('' tote:   esemv(ik)'',f14.6)')                 &
     &                             esemv(ik,nsub,is)
              pexcor=rr(jmt(ik,nsub),ik,nsub)                           &
     &               *rho(jmt(ik,nsub),ik,nsub,is)                      &
     &               *( enxc(jmt(ik,nsub),ik)-vx(jmt(ik,nsub),ik) )
              write(6,'('' tote:  pexcor(ik)'',f14.6)')   pexcor
            endif
!c     ==================================================================
          pv3=pv3 + atcon(ik,nsub)                                      &
     &          *(      pterm1(ik)  +  cor3pv(ik)+tpzpt(ik)             &
     &          + two*(evalsum(ik,nsub,is) + esemv(ik,nsub,is))         &
     &          - rr(jmt(ik,nsub),ik,nsub)*rho(jmt(ik,nsub),ik,nsub,is) &
     &            *(enxc(jmt(ik,nsub),ik)-vx(jmt(ik,nsub),ik))   )
          enddo
        etot = etot + etot0*numbsub(nsub)
        three_pv = three_pv + pv3*numbsub(nsub)
!c     ==================================================================
          if( iprint .ge. 1 ) then
            write(6,'('' tote:       etot'',f14.6)')  etot
          endif
!c     ==================================================================
          etot = etot + ( rho2(nsub) + rhoqint(nsub) )*( 2 - is )       &
     &                * numbsub(nsub)
        three_pv = three_pv + ( rho2(nsub) +rhoqint(nsub) )*( 2 - is )  &
     &                * numbsub(nsub)
!c     ==================================================================
          if( iprint .ge. 1 ) then
            write(6,'('' nsub,rho2,rhoqint,etot '',i5,3f16.6)')         &
     &      nsub,rho2(nsub),rhoqint(nsub),etot
          endif
!c     ==================================================================
          if( nsub .eq. nbasis ) then
!c           etot = etot + qintex(is)
            three_pv = three_pv + excort(is)
!c           WRITE(6,'('' nsub,is,excort,qintex '',2i5,3f12.5)')
!c    >      nsub,is,excort(is),qintex(is),etot
          endif
!c     ==================================================================
          if( nsub .eq. nbasis .and. is .eq. nspin ) then
!c           WRITE(6,'('' emad,etot '',2f12.5)') emad,etot
            etot = etot + emad + qintex(is) + Eccsum
          three_pv = three_pv + emadp
          endif
!c     ==================================================================
          if( iprint .ge. 1 ) then
            write(6,'('' tote:        rho2'',f14.6)')  rho2(nsub)
            write(6,'('' tote:     rhoqint'',f14.6)')  rhoqint(nsub)
            write(6,'('' tote:       emadp'',f14.6)')  emadp
            write(6,'('' tote:       3pv  '',f14.6)')  three_pv
          endif
!c     ******************************************************************
        if( iprint .gt.-10 ) then
!c     Major print out...................................................
        write(6,'(''     ***********************************'',         &
     &               ''**********************************'')')
        write(6,'(''     Total energies: Spin'',i3,'' Sub-lat'',i3)')   &
     &                 is,nsub
        write(6,'(''     ***********************************'',         &
     &               ''**********************************'')')
          do ik=1,komp(nsub)
            write(6,'(''       Component'',t40,i3)') ik

            write(6,'(''         coren ='',f13.5,'' valen ='',f13.5,    &
     &                '' exchen ='',f13.5)') coren(ik),valen(ik),       &
     &                                   exchen(ik)
            write(6,'(''          ezpt ='',f13.5,'' evals ='',f13.5,    &
     &                '' esemv  ='',f13.5)') ezpt(ik),                  &
     &                        evalsum(ik,nsub,is),esemv(ik,nsub,is)

            write(6,'(''        correc ='',f13.5,'' exc-mt='',f13.5)')  &
     &                                    correc(ik),excormt(ik)
            write(6,'(''        pterm1 ='',f13.5,'' c3pv  ='',f13.5,    &
     &                '' tpzpt  ='',f13.5)') pterm1(ik),cor3pv(ik),     &
     &                                    tpzpt(ik)
            write(6,'(''     ***********************************'',     &
     &                ''**********************************'')')
          enddo
        end if
!c     ******************************************************************
        enddo                             ! end do loop over spin
!c     ******************************************************************
        if( nsub .eq. nbasis ) then
         press = three_pv/three*pfact/omegws
!cab          e_per_atom = etot/nbasis
         e_per_atom = etot/natom
           write(6,'(''     ***********************************'',      &
     &               ''**********************************'')')
           write(6,'(''     Total Energy = '',f17.8,                    &
     &               ''     3PV (Ry) = '',f17.8)') etot,three_pv
           write(6,'(''     ***********************************'',      &
     &               ''**********************************'')')
           write(6,'(''     ***********************************'',      &
     &               ''**********************************'')')
           write(6,'(''     Energy/atom  = '',f17.8,                    &
     &               ''     P (Mbars)= '',f17.8)')e_per_atom,press
           write(6,'(''     ***********************************'',      &
     &               ''**********************************'')')
        endif
!c     ******************************************************************
      enddo                               ! end do loop over sublattices
!c     ******************************************************************
      if( istop .eq. sname ) then
       call fstop(sname)
      endif
!c     ******************************************************************
      return
      end
!c     cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc

