!BOP
!!ROUTINE: zzzjws1
!!INTERFACE:
      subroutine zzzjws1(lmax,rmt,rws,prel,                             &
     &                   ct,zzout,zjout,istop)
!!DESCRIPTION:
! calculates integrals over the wigner-seitz
! radius exterior to the muffin-tin radius of the functions
! z(l,e)**2 and z(l,e)*j(l,e) (i.e. {\tt zzout} and {\tt zjout})

!!DO_NOT_PRINT
      implicit none
!!DO_NOT_PRINT

!!ARGUMENTS:
      integer lmax
      real*8        rmt,rws
      complex*16    prel
      complex*16    ct(0:lmax)
      complex*16    zzout(0:lmax)
      complex*16    zjout(0:lmax)
      character*10  istop
!!REVISION HISTORY:
! Initial Version - DDJ - 1996
! Adapted - A.S. - 2013
!EOP
!
!BOC
      integer ndlgndr
      parameter (ndlgndr=6)
      real*8        xg(ndlgndr)
      real*8        wg(ndlgndr)
      complex*16    zl
      complex*16    jl

      integer ndlmax
      parameter (ndlmax=5)
      complex*16    bj(2*ndlmax+1)
      complex*16    bn(2*ndlmax+1)
      complex*16    dj(2*ndlmax+1)
      complex*16    dn(2*ndlmax+1)
      character*10  sname
      parameter (sname='zzzjws1')

      real*8 pi,atan,c,d,fac,r,rsqwpii

      real*8 one
      parameter (one=1.d0)

      integer ig,l
      integer :: isave = 0
      save xg,wg,isave,pi

!cc
!c      data xg/-.93246951d+00,-.66120939d+00,-.23861919d+00,
!c     >         .23861919d+00, .66120939d+00, .93246951d+00/
!cc
!c      data wg/0.17132449d+00,0.36076157d+00,0.46791393d+00,
!c     >        0.46791393d+00,0.36076157d+00,0.17132449d+00/
!c
!c     ****************************************************************
!c     called by grint
!c     calls besselFun
!c
!c               rmt   muffin-tin radius
!c               rws   wigner-seiz radius
!c               ct(l) (phase shift cotangents)
!c               lmax  maximum orbital angular momentum
!c               kdiag maximum number of diagonal symmetry types (1,2,4,7
!c               istop index of subroutine in which pgrm will stop
!c
!c       output: zzout  (contributions to z*z integrals from outside mt)
!c               zjout  (contributions to z*j integrals from outside mt)
!c
!c       this subroutine calculates the integrals over the wigner-seitz
!c       radius exterior to the muffin-tin radius of the functions
!c       z(l,e)**2 and z(l,e)*j(l,e).
!c     ****************************************************************
!c
!c     -----------------------------------------------------------------

      if(lmax.gt.ndlmax) call fstop(sname//': lmax > ndlmax')

      zzout(0:lmax) = (0.d0,0.d0)
      zjout(0:lmax) = (0.d0,0.d0)

      if(rmt.ge.rws) return

      if(isave.eq.0) then
       call gauleg(-one,one,xg,wg,ndlgndr)
       pi = 4.d0*atan(1.d0)
       isave = 1
      end if

!c     -----------------------------------------------------------------
!c     this needs fixing in scalar relativistic calculation............
!c     -----------------------------------------------------------------

      c   = 0.5d0*(rws-rmt)
      d   = 0.5d0*(rws+rmt)
      fac = c/pi
!c     6-point gaussian integration
      do ig=1,ndlgndr
         r=c*xg(ig)+d
         rsqwpii=fac*wg(ig)*r**2
!c        --------------------------------------------------------------
         call besselFun(lmax+1,prel*r,bj,bn,dj,dn)
!c        --------------------------------------------------------------
!c        calculate wave functions

         do l=0,lmax
            zl=prel*(ct(l)*bj(l+1)-bn(l+1))
            jl=bj(l+1)
            zzout(l)=zzout(l)-rsqwpii*zl*zl
            zjout(l)=zjout(l)-rsqwpii*zl*jl
         enddo

      enddo
!c
!c     ================================================================
      if (istop.eq.sname) then
         call fstop(sname)
      else
         return
      endif
!c
      end subroutine zzzjws1
