!c
!c     ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      subroutine itollp(cfac,kkrsz)
!c     =================================================================
!c
      implicit none
!c
      complex(8), intent(out) ::  cfac(kkrsz*kkrsz)
      integer, intent(in)     ::  kkrsz
!c     ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!      character    sname*10
!      parameter    ( sname='itollp')
!c     ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

      complex*16  powi(-4:4)
      data powi/(1.0,0.0),(0.0,1.0),(-1.0,0.0),(0.0,-1.0),              &
     &          (1.0,0.0),                                              &
     &          (0.0,1.0),(-1.0,0.0),(0.0,-1.0),(1.0,0.0) /

      integer ij,lmax,li,mi,lj,mj,lmlp
      complex*16 cij
!c
!c      integer     l3d(49)
!c      data l3d /  0, 3*1, 5*2, 7*3, 9*4, 11*5, 13*6 /
!c
!c     ================================================================
      lmax = nint(sqrt(float(kkrsz))) - 1
      ij = 0
      do li=0,lmax
       do mi=-li,li
      do lj=0,lmax
       lmlp= mod(lj - li,5)
!c         lmlp= mod ( l3d(j)-l3d(i)+16 , 4)
       cij = powi(lmlp)
       do mj=-lj,lj
        ij = ij+1
        cfac(ij)=cij                  ! (0.,1.)**(lj-li)
       enddo
      enddo
       enddo
      enddo
!c
!c     ================================================================
      return
      end
