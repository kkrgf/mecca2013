!c     ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      subroutine getcor(nspin,nbasis,komp,atcon,numbsub,                &
     &                  jmt,jws,xr,rr,h,                                &
     &                  vr,                                             &
     &                  ivpoint,ikpoint,                                &
     &                  numc,nc,lc,kc,ec,                               &
     &                  ztotss,zvalss,zsemss,zcorss,zcortot,            &
     &                  qsemmt,qcormt,                                  &
     &                  qsemtot,qcortot,qcorout,                        &
     &                  ecorv,esemv,                                    &
     &                  corden,semcor,                                  &
     &                  ibot_cont,ebot,nrelc,                           &
     &                  mtasa,iprint,istop)
!c     =================================================================
!c
      implicit real*8 (a-h,o-z)
!c
      character istop*10
      character sname*10
!c
!c     +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
      include 'mkkrcpa.h'
!c     +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!c
      integer   komp(ipsublat)
      integer   jmt(ipcomp,ipsublat)
      integer   jws(ipcomp,ipsublat)
      integer   ivpoint(ipsublat)
      integer   ikpoint(ipcomp,ipsublat)
      integer   numc(ipcomp,ipsublat,ipspin)
      integer   nc(ipeval,ipcomp,ipsublat,ipspin)
      integer   lc(ipeval,ipcomp,ipsublat,ipspin)
      integer   kc(ipeval,ipcomp,ipsublat,ipspin)
      integer   numbsub(nbasis)
      integer   ibot_cont
!c
      real*8    ec(ipeval,ipcomp,ipsublat,ipspin)
      real*8    xr(iprpts,ipcomp,ipsublat)
      real*8    rr(iprpts,ipcomp,ipsublat)
      real*8    h(ipcomp,ipsublat)
!CALAM      real*8    xstart(ipcomp,ipsublat)
      real*8    vr(iprpts,ipcomp,ipsublat,ipspin)
      real*8    corden(iprpts,ipcomp,ipsublat,ipspin)
      real*8    semcor(iprpts,ipcomp,ipsublat,ipspin)
      real*8    atcon(ipcomp,ipsublat)
      real*8    ztotss(ipcomp,ipsublat)
      real*8    zvalss(ipcomp,ipsublat)
      real*8    zsemss(ipcomp,ipsublat)
      real*8    zcorss(ipcomp,ipsublat)
      real*8    zcortot(ipcomp,ipsublat)
      real*8    qsemmt(ipcomp,ipsublat,ipspin)
      real*8    qcormt(ipcomp,ipsublat,ipspin)
      real*8    qsemtot(ipcomp,ipsublat,ipspin)
      real*8    qcortot(ipcomp,ipsublat,ipspin)
      real*8    qcorout(ipcomp,ipsublat,ipspin)
      real*8    ecorv(ipcomp,ipsublat,ipspin)
      real*8    esemv(ipcomp,ipsublat,ipspin)
!CALAM      real*8    wrk1(iprpts)
      real*8    qscoutav(ipspin)
      real*8    ebot,ebot_save

!CDEBUG
      integer    iverybad
      data       iverybad/0/
      save       iverybad
      real*8     ecmax,ectmp
      parameter (ectmp = -100000.d0)
!CDEBUG

!c parameters
      real*8    delec
      real*8    tolch
!c
!c     +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
      parameter (delec=one/(2.0d0*ten))
      parameter (tolch=(one/ten)**9)
      parameter (sname='getcor')
!c     +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!c     =================================================================
!c
!c     -----------------------------------------------------------------
      call zeroout(corden,iprpts*ipcomp*ipsublat*ipspin)
!c     -----------------------------------------------------------------
      call zeroout(semcor,iprpts*ipcomp*ipsublat*ipspin)
!c     -----------------------------------------------------------------
      call zeroout(qsemmt,ipcomp*ipsublat*ipspin)
!c     -----------------------------------------------------------------
      call zeroout(qcormt,ipcomp*ipsublat*ipspin)
!c     -----------------------------------------------------------------
      call zeroout(qsemtot,ipcomp*ipsublat*ipspin)
!c     -----------------------------------------------------------------
      call zeroout(qcortot,ipcomp*ipsublat*ipspin)
!c     -----------------------------------------------------------------
      call zeroout(qcorout,ipcomp*ipsublat*ipspin)
!c     -----------------------------------------------------------------
      call zeroout(ecorv,ipcomp*ipsublat*ipspin)
!c     -----------------------------------------------------------------
      call zeroout(esemv,ipcomp*ipsublat*ipspin)
!c     -----------------------------------------------------------------
!c     =================================================================
!c     calculate the core and semi core densities.......................
!c     ===============================================================
!CDEBUG
      ecmax = ectmp
      ebot_save = ebot
!CDEBUG
      do is=1,nspin
       do nsub=1,nbasis
        do ic=1,komp(nsub)
         if(ikpoint(ic,nsub).eq.0 .or. ivpoint(nsub).eq.0) then
          if(iprint.gt.-10) then
           write(6,'(//,''     Sub-lattice ='',i3,                      &
     &                      ''  Component ='',i3,''  Spin ='',i3,       &
     &                      ''  mtasa ='',i3)')  nsub,ic,is,mtasa
          end if

!c     ---------------------------------------------------------------
          call corslv(is,jmt(ic,nsub),jws(ic,nsub),                     &
     &                        h(ic,nsub),                               &
     &                        rr(1,ic,nsub),xr(1,ic,nsub),              &
     &                        vr(1,ic,nsub,is),                         &
     &                        ztotss(ic,nsub),zvalss(ic,nsub),          &
     &                        zsemss(ic,nsub),numc(ic,nsub,is),         &
     &                        nc(1,ic,nsub,is),lc(1,ic,nsub,is),        &
     &                        kc(1,ic,nsub,is),ec(1,ic,nsub,is),        &
     &                        corden(1,ic,nsub,is),                     &
     &                        semcor(1,ic,nsub,is),                     &
     &                        qcormt(ic,nsub,is),                       &
     &                        qcortot(ic,nsub,is),                      &
     &                        qsemmt(ic,nsub,is),                       &
     &                        qsemtot(ic,nsub,is),                      &
     &                        ebot,ecorv(ic,nsub,is),esemv(ic,nsub,is), &
     &                        ibot_cont,nspin,nrelc,mtasa,iprint,istop)
!c     ---------------------------------------------------------------
!c
!c     ===============================================================
!c            adjust bottom of contour: above highest semi-core state

          if(ibot_cont.ne.0) then
           if(numc(ic,nsub,is).gt.0) then
!C            ebot=max(ec(numc(ic,nsub,is),ic,nsub,is)+delec,ebot)
            ecmax=max(ec(numc(ic,nsub,is),ic,nsub,is),ecmax)
           endif
          endif
!c     ===============================================================
!c
!c     ===============================================================
!c          check the charges.....................................
!c     ---------------------------------------------------------------
!c                 qsemmt(ic,nsub,is)=qsemmtx
!c                 qsemtot(ic,nsub,is)=qsemtotx
!c                 qcormt(ic,nsub,is)=qcormtx
!c                 qcortot(ic,nsub,is)=qcortotx

          qcorout(ic,nsub,is)=zsemss(ic,nsub)/float(nspin) -            &
     &                                qsemtot(ic,nsub,is) +             &
     &                                zcorss(ic,nsub)/float(nspin) -    &
     &                                qcortot(ic,nsub,is)
          if(iprint.gt.-5) then
           write(6,'(''      Charge     core:   interst.'',t35,''='',   &
     &                 f16.8)') qcorout(ic,nsub,is)
          end if
!c
         else
!c
          qcortot(ic,nsub,is)=qcortot(ikpoint(ic,nsub),                 &
     &                                       ivpoint(nsub),is)
          qsemtot(ic,nsub,is)=qsemtot(ikpoint(ic,nsub),                 &
     &                                       ivpoint(nsub),is)
          qcormt(ic,nsub,is)=qcormt(ikpoint(ic,nsub),                   &
     &                                     ivpoint(nsub),is)
          qsemmt(ic,nsub,is)=qsemmt(ikpoint(ic,nsub),                   &
     &                                     ivpoint(nsub),is)
          qcorout(ic,nsub,is)=qcorout(ikpoint(ic,nsub),                 &
     &                                      ivpoint(nsub),is)
          ecorv(ic,nsub,is)=ecorv(ikpoint(ic,nsub),                     &
     &                                   ivpoint(nsub),is)
          esemv(ic,nsub,is)=esemv(ikpoint(ic,nsub),                     &
     &                                   ivpoint(nsub),is)
          do i=1,jws(ic,nsub)
           corden(i,ic,nsub,is)=corden(i,ikpoint(ic,nsub),              &
     &                                            ivpoint(nsub),is)
           semcor(i,ic,nsub,is)=semcor(i,ikpoint(ic,nsub),              &
     &                                            ivpoint(nsub),is)
          enddo
!c
          if(iprint.ge.0) then
           write(6,'(''      Charge     core: mt/asa,tot'',t35,''='',   &
     &                2f16.8)') qcormt(ic,nsub,is),qcortot(ic,nsub,is)
           write(6,'(''      Charge semicore: mt/asa,tot'',t35,''='',   &
     &                2f16.8)') qsemmt(ic,nsub,is),qsemtot(ic,nsub,is)
           write(6,'(''      Charge     core:   interst.'',t35,''='',   &
     &                 f16.8)') qcorout(ic,nsub,is)
          endif

         endif
        enddo
       enddo
      enddo
!CDEBUG

!c      if(ecmax.le.ectmp) then
!c       ebot = ebot0
!c      else if(ecmax+delec.ge.ebot0) then
!c       ebot=ecmax+delec
!c      else if(ecmax.lt.-5.d0) then
!CDEBUG       ebot=(ebot0+ecmax)/2
!c       ebot=max(-10.d0,(ebot0+ecmax)/2)
!c      else
!c       ebot=ebot0
!c      endif
!CDEBUG
!c     ---------------------------------------------------------------
      qscoutav(nspin) = zero
      qscoutav(  1  ) = zero
      do nsub=1,nbasis
         do ic=1,komp(nsub)
            qsemck=qsemtot(ic,nsub,1)                                   &
     &            +( nspin - 1 )*qsemtot(ic,nsub,nspin)
            qcorck=qcortot(ic,nsub,1)                                   &
     &            +( nspin - 1 )*qcortot(ic,nsub,nspin)
            qscoutav(1) = qscoutav(1)                                   &
     &            + atcon(ic,nsub)*qcorout(ic,nsub,1)*numbsub(nsub)
            if(nspin.eq.2) then
            qscoutav(nspin) =    qscoutav(nspin)                        &
     &            + atcon(ic,nsub)*qcorout(ic,nsub,nspin)*numbsub(nsub)
            endif
!C=================================================================
!C            if(abs(qsemck-zsemss(ic,nsub)).gt.tolch) then
!C              write(6,'('' lost semi-core charge'')')
!C               write(6,'('' nsub='',i3,'' comp='',i3,
!C     >                   '' z='',d17.8,'' q='',f17.8)')
!C     >         nsub,ic,zsemss(ic,nsub),qsemck
!C            endif
!c
!C            if(abs(qcorck-zcorss(ic,nsub)).gt.tolch) then
!C              write(6,'('' lost      core charge'')')
!C               write(6,'('' nsub='',i3,'' comp='',i3,
!C     >                   '' z='',d17.8,'' q='',f17.8)')
!C     >         nsub,ic,zcorss(ic,nsub),qcorck
!CAB              call fstop(sname)
!C            endif
!C=================================================================
!c
            if(abs(qcorck-zcorss(ic,nsub)+qsemck-zsemss(ic,nsub))       &
     &          .gt. tolch) then
               write(6,'('': lost semi-core+core charge'')')
               write(6,'('' nsub='',i3,'' comp='',i3,                   &
     &                   '' z='',d17.8,'' q='',f17.8)')                 &
     &         nsub,ic,zcorss(ic,nsub)+zsemss(ic,nsub),qcorck+qsemck
                 if(iverybad.gt.10) then
                   call fstop(sname//':: LOST CHARGE')
                 else
                   iverybad = iverybad+1
                 endif
!CDEBUG
            else
             zcorss(ic,nsub) = dble(nint(qcorck))
             zsemss(ic,nsub) = dble(nint(qsemck))
             zcortot(ic,nsub)= zsemss(ic,nsub) + zcorss(ic,nsub)
!CDEBUG
            endif
             ztot=zvalss(ic,nsub) + zsemss(ic,nsub) +zcorss(ic,nsub)
             if((ztotss(ic,nsub)- ztot ) .gt.tolch ) then
                  write(6,'('': LOST TOTAL CHARGE'')')
                  call fstop(sname)
             endif

         enddo
      enddo
      if(iprint.ge.-1) then
       write(6,'(''      Charge     core: inter. avg'',t35,''='',       &
     &                2f16.8)') qscoutav(1),qscoutav(nspin)
       write(6,'(''     -----------------------------------'',          &
     &          ''----------------------------------'',//)')
      end if


        if(ibot_cont.ne.0) then
          if(ebot.lt.ecmax)then
            ebot = ecmax + delec
         write(6,'(''===========================================        &
     &============================='')')
            write(6,'(''  WARNING:EBOT HAS BEEN SHIFTED TO'',           &
     &             '' EC(MAX)+delec, BECOS ebot<ecor_max(i)'')')
            write(6,112) ebot_save,ebot
            write(6,'(''  User might need to keep the ebot at'',        &
     &             '' OLD value in order not to miss '')')
            write(6,'(''  tiny states (if any) in the DOS '')')
         write(6,'(''===========================================        &
     &============================='')')
          endif
        endif
            write(6,*)
112     format(2x'OLD ebot =',F10.5,',',5x,'NEW ebot =',F10.5)


!c
!c     ===============================================================
      if(istop.eq.sname) then
         call fstop(sname)
      endif
!c
      return
      end
