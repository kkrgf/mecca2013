!c     ================================================================
      subroutine gaunt1( gaunt, ij3, nj3, yr, w,                        &
     &                   lmax, kkrsz, ndlj )
!c     ================================================================
!c     Gaunt computes the integral of conjg(y(lp,mp))*y(l,m)*y(ls,ms)
!c     for lp+l+ls .lt. 2*ngntd
!c     using gaussian quadrature as given by
!c     M. Abramowitz and I.A. Stegun, handbook of mathematical functions,
!c     nbs applied mathematics series 55 (1968), pages 887 and 916
!c     cell kkr-cpa computer code. (  w.a.s.,jr march 1991 )
!c     Revised by W.A. Shelton, Jr. March 1, 1993
!c     ================================================================
      implicit none
!c     ================================================================
      integer    kkrsz
      integer    ij3(kkrsz*kkrsz,*)
      integer    nj3(*)
      integer    lmax
      integer    lmaxp1
      integer    ndlj
      integer    n
      integer    nn
      integer    l
      integer    ls
      integer    lp
      integer    m
      integer    ms
      integer    mp
      integer    jlow
      integer    jhigh
      integer    il
      integer    ils
      integer    ilp
      integer    i
      integer    istore
      integer    icount
      integer    nj3_count
!c     ================================================================
      real*8     w(*)
      real*8     yr(ndlj+1,*)
      real*8     gaunt(kkrsz*kkrsz,*)
      real*8     gauntn
      real*8     zero
      parameter  (zero=0.d0)
!c     ================================================================
      lmaxp1 = lmax + 1
      n = ndlj + 1
!c     ================================================================
!c     Zero out nj3 array
!c     ================================================================
      do l=1,kkrsz*kkrsz
        nj3(l) = 0
      enddo
!c     ================================================================
      call gaunt2( n, lmaxp1, ndlj, w, yr )
!c     ================================================================
      icount = 0
!c     ================================================================
      do l = 0,lmax
!c     ================================================================
        do m = -l,l
!c     ================================================================
          il = l*( l + 1 ) + m + 1
!c     ================================================================
          do ls = 0,lmax
!c     ================================================================
            do ms = -ls,ls
!c     ================================================================
              icount = icount + 1
!c     ================================================================
              ils = ls*( ls + 1 ) + ms + 1
!c     ================================================================
              jlow = abs( l - ls )
              jhigh = l + ls
!c     ================================================================
              nj3_count = 0
!c     ================================================================
              do lp = jlow,jhigh,2
!c     ================================================================
                do mp = -lp,lp
!c     ================================================================
                  ilp = lp*( lp + 1 ) + mp + 1
!c     ================================================================
                  if( ms .eq. m + mp ) then !sk --> ? not m + ms = mp ?
!c     ================================================================
                    if( mod( l + ls + lp, 2 ) .eq. 0 ) then
!c     ================================================================
                      if( l - ls + lp .ge. 0 ) then
!c     ================================================================
                        if( l + ls - lp .ge. 0 ) then
!c     ================================================================
                          if( - l + ls + lp .ge. 0 ) then
!c     ================================================================
                            gauntn = zero
!c     ================================================================
!c                           nn=n/2
                            nn=n
!c     ================================================================
                            do i = 1,nn
                              gauntn = gauntn + w(i)                    &
     &                               *yr(i,ilp)*yr(i,il)*yr(i,ils)
                            enddo                   ! end do loop over n
!c     ================================================================
                            istore = ( ils - 1 )*kkrsz + il
                            nj3_count = nj3_count + 1
                            nj3(istore) = nj3(istore) + 1
!c                           ij3(icount,nj3_count) = ilp
                            ij3(istore,nj3(istore)) = ilp
                            gaunt(istore,nj3(istore)) = gauntn
!c     write(6,'('' l,m,ls,ms,lp,mp,icount,istore,nj3,gaunt '',
!c    > 6i3,3i5,d12.5)')
!c    >    l,m,ls,ms,lp,mp,icount,istore,nj3(istore),
!c    >    gaunt(istore,nj3(istore))
!c     ================================================================
                          endif  ! end if statement over -l + ls +lp
!c     ================================================================
                        endif    ! end if statement over  l + ls -lp
!c     ================================================================
                      endif      ! end if statement over  l - ls +lp
!c     ================================================================
                    endif        ! end if statement over mod( l + ls +lp )
!c     ================================================================
                  endif          ! end if statement over mp .eq. m + ms
!c     ================================================================
                enddo            ! end do loop over mp
!c     ================================================================
              enddo              ! end do loop over lp
!c     ================================================================
!c             nj3(icount) = nj3_count
!c     ================================================================
            enddo                ! end do loop over ms
!c     ================================================================
          enddo                  ! end do loop over ls
!c     ================================================================
        enddo                    ! end do loop over m
!c     ================================================================
      enddo                      ! end do loop over l
!c     ================================================================
      return
      end
