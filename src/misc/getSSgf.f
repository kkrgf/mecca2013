      subroutine getSSgf(energy,lmax,L_limit,ispin,nrelv,ndcomp,nbasis, &
     &                   sublat,rs_box,iprint,                          &
     &                   green,tab,cotdl,almat,zlab)    


!c     =================================================================
!c
!c     to find single-scattering solution (t-matrix and GF-contributions)
!c
      use mecca_constants
      use mecca_types, only : Sublattice,RS_Str
      use gfncts_interface
!DELETE      use mtrx_interface
!
      implicit none
      complex(8), intent(in) :: energy
      integer, intent(in) :: lmax,L_limit,ispin,nrelv,ndcomp,nbasis
      type(Sublattice), intent(in), target :: sublat(nbasis)
      type ( RS_Str ), intent(in) :: rs_box
      integer, intent(in) :: iprint

      complex(8), intent(out) :: green(:,:,:) ! (iprpts,ipcomp,nbasis)
      complex(8), intent(out) :: tab((lmax+1)**2,(lmax+1)**2,           &
     &                                                ndcomp,nbasis)
      complex(8), intent(out), optional ::                              &
     &                              cotdl(0:lmax,ndcomp,nbasis)         &
     &                             ,almat(0:lmax,ndcomp,nbasis)         &
     &              ,zlab(1:size(green,1),0:lmax,ndcomp,nbasis)
!      integer, intent(in) :: mtasa
!c     ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
      character(10), parameter :: sname='getSSgf'
!c     ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

!      integer, external :: gEnvVar
!      integer :: st_env

      integer, external :: indRmesh

      complex(8) :: zlr(1:size(green,1),0:L_limit)
      complex(8) :: jlr(1:size(green,1),0:L_limit)
      complex(8) :: alpha(0:L_limit)
      complex(8) :: cotdl_(0:L_limit)
      complex(8) :: tmatl(0:L_limit)
      complex(8) :: pnrel
      complex(8), parameter :: sqrtm1=(zero,one)

      integer :: l_big,l,lm,m,ndrpts,nsub,nk,ir
      integer, parameter :: lmin=6

      real(8) :: h
      real(8) :: xr(size(green,1))
      real(8) :: rr(size(green,1))
      real(8) :: vr(size(green,1))
      real(8) :: rScat
      real(8) :: clight
      real(8), parameter :: cphot = two*inv_fine_struct_const

      clight = cphot*ten**nrelv
      pnrel = sqrt(energy)
      pnrel = pnrel*sign(1.d0,aimag(pnrel))

!c
!c     ****************************************************************
!c     set up t-matrix, zz, zj for each component and  sub-lattice
!c     ****************************************************************
!c
      if(iprint.ge.1) then
       write(6,'('' gettab:  nrelv,lmax,l_big='',3i5)') nrelv,lmax,l_big
       write(6,'('' gettab: energy,pnrel='',4d12.4)') energy,pnrel
       write(6,'('' gettab: size(tab)='',4i5)') (size(tab,nk),nk=1,3)
      endif

      do nsub=1,nbasis
         do nk=1,sublat(nsub)%ncomp
               call g_meshv(sublat(nsub)%compon(nk)%v_rho_box,          &
     &                                                 h,xr,rr,ispin,vr)
            ndrpts = size(vr)
!            jws = min(ndrpts,1+indRmesh(rs_box%rws(nsub),ndrpts,rr))
!            iend = max(jws,1+indRmesh(rs_box%rcs(nsub),ndrpts,rr))
            rScat = exp(sublat(nsub)%compon(nk)%v_rho_box%xmt)
            l_big = min(max(lmin,nint((real(pnrel)*rScat)**2)),L_limit)
            l_big = max(lmax,l_big)      !  lmax <= l_big <= L_limit

!DEBUGPRINT
!            write(*,*) ' DEBUG: ndrpts,iend,l_big=',ndrpts,iend,l_big
!            write(*,*) ' DEBUG: h,rr=',                                 &
!     &                       sngl(h),sngl(rr(1)),sngl(rr(iend))
!            write(*,*) ' DEBUG: jws,rws=',jws,rs_box%rws(nsub)
!            write(*,*) ' DEBUG: rScat,r(jws)=',rScat,rr(jws)
!DEBUGPRINT

!c           -------------------------------------------------------
            call solveSS(nrelv,clight,                                  &
     &                  l_big,                                          &
     &                  energy,pnrel,                                   &
     &                  zlr,jlr,                                        &
     &                  cotdl_,alpha,                                   &
     &                  rScat,rr,vr,                                    &
     &                  iprint)
!c              -------------------------------------------------------
            tmatl(0:l_big) = cone/(pnrel*( sqrtm1- cotdl_(0:l_big)))
            tab(:,:,nk,nsub) = czero
            do l=0,lmax
             lm = l**2+l+1
             do m=-l,l
              tab(lm+m,lm+m,nk,nsub) = tmatl(l)
             end do
            end do
            if ( present(cotdl) ) then
             cotdl(0:lmax,nk,nsub) = cotdl_(0:lmax)
            end if 
            if ( present(almat) ) then
             almat(0:lmax,nk,nsub) = alpha(0:lmax)
            end if 
!c     ==============================================================
!c     calculate the green function for this (energy,species,sub-lat,
!c     spin) :: Spherical part only in this code.....................
!c     --------------------------------------------------------------
            green(1:,nk,nsub) = czero 
            do l=l_big,0,-1
             lm = 2*l+1
             green(1:ndrpts,nk,nsub) = green(1:ndrpts,nk,nsub) +        &
     &                     lm*zlr(1:ndrpts,l)*                          &
     &                        (tmatl(l)*zlr(1:ndrpts,l)-jlr(1:ndrpts,l))
            enddo
            if(iprint.ge.2) then
             write(6,'(//)')
             write(6,'(''getSSgf:: ir,rr(ir),green(ir)'',i4,3d12.4)')   &
     &             (ir,rr(ir),green(ir,nk,nsub),ir=1,size(green,1),1)
            endif
            if ( present(zlab) ) then
             zlab(1:ndrpts,0:lmax,nk,nsub) = zlr(1:ndrpts,0:lmax)
             zlab(ndrpts+1:,0:lmax,nk,nsub) = czero   
            end if
         enddo
      enddo

!c           -----------------------------------------------------------
      return

      contains
!
!c     ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      subroutine solveSS(nrelv,clight,                                  &
     &                  lmax,                                           &
     &                  energy,pnrel,                                   &
     &                  zlr,jlr,                                        &
     &                  cotdel,almat,                                   &
     &                  rScat,r,rv,                                     &
     &                  iprint)
!c     =================================================================
!c
!c     *****************************************************************
!c     program to solve scalar relativistic equations for complex
!c     energies using Calogero's phase method    ddj & fjp  ---july 1991
!c     *****************************************************************
!c
      use mecca_constants
      implicit none
!c
      integer, intent(in) :: nrelv,lmax
      real(8), intent(in) :: clight
      complex(8), intent(in) :: energy,pnrel
      complex(8), intent(out) :: zlr(:,0:)  ! (iprpts,0:iplmax)
      complex(8), intent(out) :: jlr(:,0:)  ! (iprpts,0:iplmax)
      complex(8), intent(out) :: cotdel(0:lmax)
      complex(8), intent(out) :: almat(0:lmax)
      real(8), intent(in) :: rScat
      real(8), intent(in) :: r(:)
      real(8), intent(in) :: rv(:)
      integer, intent(in) :: iprint
!c
!DELETE      character(:), allocatable :: coerr
!c
      complex(8) :: g(size(r))
      complex(8) :: f(size(r))
      complex(8) :: gpl(size(r))
      complex(8) :: fpl(size(r))
      complex(8) :: prel
      complex(8) :: anorm
      complex(8) :: tandel

      integer :: nrel,ized,iend
!c     +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
      character(10), parameter :: sname='solveSS'
!c     +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!c
!c
!c     Gets radial integrals, t-matrix.
!c
!c     In this version rv is r * v
!c
!c     called by: grint
!c     calls:     bessel,cqexp,cqxpup,csimp,zeroout
!c
!c      requires:
!c            lmax        lmax
!c            energy      energy
!c            h,x,r       grid info (step for log-grid, log grid, r-grid)
!c
!c      returns:
!c            zlr         zl(r) properly normalized..............
!c            jlr         jl(r) properly normalized..............
!c
!c     set c for doing semi-relativistic or non-relativistic calc.
!c     .....c = speed of light in proper units..........
!c     Ryd units: m=1/2  hbar=1 c=2*inverse of fine structure const.
!c
!c     Solving a relativistic (nrelv=0) or non-relativistic (nrelv=10)
!c     problem.   If non-relativistic, then set e=e-elmass equal to e
!c     (where elmass = electron mass) and 1/c is set to ~zero to make
!c     relativistic terms zero.
!c     NOTE:  c is set to c*10**nrelv to make code identical to the
!c            fully relativistic code..............................
!c
!c     Using Ricatti Bessel fcts. Need only l=0 and l=1 to get wave-fcts.
!c
!c     To get physical phase-shifts and normalization, need get Rl in
!c     terms of spherical bessel for all l's at the boundary of sphere.
!c     This is properly done in SCALAR, see clmt and slmt.
!c
!c     With M(r)= [1 + (e-v(r))*c2inv]
!c     Obtain r*(wave-fct.):   regular     g=r*R and   f=r*R'/M
!c                           irregular   gpl=r*R and fpl=r*R'/M
!c
!c     NOTE: ====>   need to get gderiv for mom. matrix elements
!c
!c     *****************************************************************
!c
      ized = g_zed(rv,r)
!c     =================================================================
      if(ized.lt.1 .and. nrelv.le.0) then
        prel = pnrel
        nrel = 1
      else
!c
!c Phaseshifts and wavefcts are calculated with relativsitic mom. -- prel
!c Whereas, because t_inv= i*Kappa + Kappa*cotdel, i*Kappa term must
!c cancel i*Kappa piece of Non-Rel g(k). Therefore, Kappa=sqrt(e) -- pnre !l.
!c
        prel = pnrel*sqrt(cone+energy/clight**2)
        nrel = nrelv
      endif

      if(iprint.ge.2) then
         write(6,'('' semrelzj:         lmax='',i5)')lmax
         write(6,'('' semrelzj: nrel,clight='',i5,d12.4)')nrel,clight
         write(6,'('' semrelzj: energy,pnrel='',4d12.4)') energy,pnrel
         write(6,'('' semrelzj:  energy,prel='',4d12.4)') energy,prel
         write(6,'('' semrelzj:      rScat='',f8.5)') rScat
      endif
!c     =================================================================
!c     for muffin-tins incrs and jmt should be the same

      iend=size(rv)

!c     =================================================================
!c     solve scalar-relativistic or non-relativistic equation.
      do l=0,lmax
!c        ------------------------------------------------------------
         call scalar(nrel,clight,                                       &
     &               l,                                                 &
     &               g,f,gpl,fpl,                                       &
     &               tandel,cotdel(l),anorm,almat(l),                   &
     &               energy,prel,                                       &
     &               rv,r,rScat,iend,                                   &
     &               1,                                                 &
     &               iprint)
         zlr(1:iend,l) = g(1:iend)/r(1:iend)*anorm
         jlr(1:iend,l) = gpl(1:iend)/r(1:iend)
!DEBUGPRINT         
!         !if ( l>=3 ) then
!         write(6,'('' DEBUG l='',i2,'' Re(E)='',g13.5,'' tan='',2g15.5, &
!     &      '' del='',g15.6,'' log(alp)='',g15.6)')                     &
!     &                         l,real(energy),tandel,                   &
!     &                 atan(real(tandel))/pi*(2*l+1),                   &
!     &                 aimag(log(almat(l)))/pi*(2*l+1)
!         !end if
!DEBUGPRINT         
      enddo
      return
!c
      end subroutine solveSS
!      
!c     cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      subroutine scalar(nrelv,clight,                                   &
     &                  l,                                              &
     &                  g,f,gpl,fpl,                                    &
     &                  tandel,cotdel,anorm,alpha,                      &
     &                  energy,prel,                                    &
     &                  rv_in,r,rScat,iend,                             &
     &                  iswzj,                                          &
     &                  iprint)
!c     ================================================================
!c
      use mecca_constants
      use stiffode, only : set_ode_params,odeint2
      implicit none
!c
      integer, intent(in) :: nrelv,l
      real(8), intent(in) :: clight
      complex(8) :: g(:)
      complex(8) :: f(:)
      complex(8) :: gpl(:)
      complex(8) :: fpl(:)
      complex(8) :: cotdel, tandel, anorm, alpha
      complex(8), intent(in) :: energy,prel
      real(8), intent(in) :: rv_in(:)
      real(8), intent(in) :: r(:)
      real(8), intent(in) :: rScat
      integer, intent(in) :: iend,iswzj,iprint
!c
!      real*8 fact(4),factl1
      real(8) :: r2(size(r,1))
      real*8 zed
      real*8 v0
      real*8 cinv
      real*8 c2inv
      real*8 power
!c parameter
      real*8 tole
!c
!c
      complex(8) :: bjl(l+2),djl(l+2)
      complex(8) :: bnl(l+2),dnl(l+2)
      complex(8) :: gpl_s,fpl_s
      complex*16 p

      integer ized, nrel, j, jScat
      integer lmin

      integer, external :: gEnvVar
      real(8), external :: d1mach
      integer i0

      complex*16 eoc2p1, tzoc, fz2oc2, tzoc2, em0, v0me
      complex*16 eoc2,enr,emvr
      complex*16 a0, a1p, a1, a2p, a2, a3p, a3, emr, em
      complex*16  pr,  slmt, clmt, tl
      real(8) z0

      real(8) :: rv(size(rv_in,1))
      real*8 relerr,abserr
!DEBUG      parameter (relerr=1.d-8,abserr=1.d-7)
      parameter (relerr=1.d-8,abserr=1.d-10)
      integer iflag

      real*8 rj,renorm,overflo
      integer l1,j1min

      real*8 rvj
!c     ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
      integer, parameter :: icmax=5
      parameter (tole=(one/ten)**10)
      character(10), parameter :: sname='scalar'
!CAB
      real(8), parameter :: smallnumber=1.d-12
!CAB
!c     stiff ode solver
      real*8 eps
      real*8 hmin
      real*8 x1
      real*8 x2
      complex*16 ys1(2)
      complex*16 ys2(2)

      real*8 cent

      integer ipot(2)

      integer, external :: indRmesh

      integer j1
!c     sub-step information
      integer nss
      real*8 xss(40*size(r))
      real*8 rvss(40*size(r))
      complex*16 yss(2,40*size(r))
      complex*16 dyss(2,40*size(r))
      complex*16 errss(2,40*size(r))
!c
!c     ****************************************************************** !*
!c     sets up to solve the non- and scalar-relativistic equation.
!c     based on Calegero Method..............DDJ & FJP  July 1991
!c     uses Ricatti-Bessel fcts. for solution
!c     ****************************************************************** !*
!c
      if(iprint.ge.2) then
         write(6,'('' in SCALAR iprint '',i5)') iprint
         write(6,'('' scalar: l,iswzj='',2i5)') l,iswzj
         write(6,'('' scalar: nrelv,clight='',i5,d12.4)') nrelv,clight
         write(6,'('' scalar: energy='',2d16.8)') energy
         write(6,'('' scalar:   prel='',2d16.8)') prel
      end if
!c     =================================================================
!c     cannot solve for the real part of the energy equal to 0.0!!!!!!!!
      if( abs(energy) .lt. tole ) then
        write(6,'('' scalar:: energy=zero'')')
      call p_fstop(sname)
      end if

      nrel = nrelv
      if ( gEnvVar('ENV_VAL_REL',.false.)>0 ) then
        nrel = 1
        write(6,*) ' ENV_VAL_REL ==> nrel = ',nrel
      end if

!c     =================================================================
!c     set atomic number................................................
      rv = rv_in
      ized = g_zed(rv,r)
      zed=ized
!c     =================================================================
!c
      if(ized.eq.0) then
        do j=1,iend
         if(abs(rv(j)).gt.smallnumber) go to 9
        end do
!c   To be able to compute for zero-potential
        rv = rv -smallnumber*r(1)
      end if
9     continue

      r2(1:iend) = r(1:iend)**2
      l1 = l+1
!C...............................................................
!c

!c     start with small r expansion of wave-fcts
      if(nrel.le.0) then
       cinv=one/clight
       if(ized.lt.0) then
        tzoc=czero
        fz2oc2=czero
        tzoc2=czero
       else
        tzoc=2*zed*cinv
        fz2oc2=tzoc*tzoc
        tzoc2=tzoc*cinv
       end if
       c2inv=cinv*cinv
       eoc2 = energy*c2inv
       eoc2p1 = cone+eoc2
      else
       cinv = zero
       c2inv= zero
       eoc2 = zero
       eoc2p1 = cone
       tzoc = czero
       fz2oc2=czero
       tzoc2= czero
      end if

      call set_ode_params(c2inv0=c2inv)

      v0 = zero
!DEBUG      
      if(ized.ne.0) then
       v0=(rv(1)+2*zed)/r(1)
      end if
!DEBUG      

      i0 = 1

!c
      v0me= v0-energy
      if(c2inv.eq.zero) then
       em0 = cone
      else
       em0= cone - v0me*c2inv
      end if

      a0=cone

!      IF ( i0>1 ) THEN
       if(nrel.le.0) then
!c        ==============================================================
!c        scalar-relativistic case......................................
         power=sqrt( l*(l+1) + 1 - dreal(fz2oc2) )
!c        a0 is arbitrary, could make Rl a spherical Bessel at origin.
!c              as for non-rel case:   R_l=x*j_l(x)= x*x**l/(2l+1)!!
!c        a0=1
!c              OR just use same expression as non-rel case, i.e.
!c        a0= prel**power/factl1
!cccc         a0= prel**l1/factl1
        if(ized.lt.0) then
         a1 = czero
         a2 = v0me/(4*power+4)
         a3 = czero
        else
         a1p=( fz2oc2 + em0*(power-1 -2*fz2oc2) )/(2*power+1)
         a1 = a0*a1p/tzoc2
         a2p=( a1p*(fz2oc2 + em0*(3*power+1 - 2*fz2oc2)) +              &
     &         em0*em0*(fz2oc2 - 2*power+2) )/(4*power+4)
         a2 = a0*a2p/(tzoc2*tzoc2)
         a3p=( a2p*( fz2oc2 + em0*(5*power+5 - 2*fz2oc2) )              &
     &        - a1p*(4*power+1 - fz2oc2)*em0*em0                        &
     &        + (3*power-3 - fz2oc2)*em0*em0*em0  )/(6*power+9)
         a3 = a0*a3p/(tzoc2*tzoc2*tzoc2)
        end if
       else
!c        ==============================================================
!c        non-relativistic case......................................
        power= l1
!c
!c        a0 is arbitrary, but this make Rl a spherical Bessel at origin.
!c                        R_l=x*j_l(x)= x*x**l/(2l+1)!!
!c        a0= prel**l1/factl1
!c        a0= prel**power/factl1
        if(ized.lt.0) then
         a1 = czero
         a2p= v0me/(4*l+6)
         a2 = a0*a2p
         a3 = czero
        else
         a1p= -2*zed/(2*l+2)
         a2p= (v0me - a1p*2*zed)/(4*l+6)
         a3p= (a1p*v0me - a2p*2*zed)/(6*l+12)
         a1 = a0*a1p
         a2 = a0*a2p
         a3 = a0*a3p
        end if
       end if
       cent = l*(l+1)
       call set_ode_params(cent0=cent,power0=power)
!c
       do j=1,i0
!c
!c        em= 1 + (energy - rv(j)/r(j))*c2inv
!c        emr= eoc2p1*r(j) - rv(j)*c2inv
!c
        rvj = rv(j)
        enr  = r(j)*energy
        emvr = c2inv*(enr-rvj)               ! (em-1)*r
        emr= r(j) + emvr                       ! r+(e*r-rv)*c2inv

!c       ==============================================================
!c       r*G = g and r*F = f, i.e. this is r*wave-fct.
        g(j)= r(j)**power*(a0 + r(j)*(a1 + a2*r(j) + a3*r2(j)) )
!c
        f(j)= r(j)**power*( a0*(power-1) +                              &
     &     r(j)*(a1*power + a2*(power+1)*r(j) + a3*(power+2)*r2(j)))
        f(j) = f(j) / emr
        if(iprint.ge.6) then
         write(6,'('' scalar: j,g,f='',i5,2(1x,2d16.8))') j,g(j),f(j)
        end if 
       end do

       jScat = indRmesh(rScat,iend,r)
       if ( ized == 0 ) then
        j1min = i0
       else
        rj = max(0.5d0,cent)/zed    ! TODO: to adjust with potential behavior
        j1min = max(i0,1+indRmesh(rj,jScat-1,r))
       end if
!c
!c     =================================================================
!c     regular solution and phase-shifts of scalar relativistic eqs.
!c

!c     propagate regular solution with r^power factored out
!c     until the oscillatory region


      hmin=0.0d0
      eps=abserr
      nss=0
      j1 = i0
      ys2(1)=g(j1)
      ys2(2)=f(j1)
      call set_ode_params(in_out0=1,enrg0=energy)
      do j=i0+1,j1min
        j1=j-1
!c       pass info thru common to potential interpolation routines
        ipot(1)=j1
        ipot(2)=j
          call set_ode_params(ipot0=ipot,rfit0=r(j1:j),                 &
     &                                            zfit0=-0.5d0*rv(j1:j))
        x1=r(j1)
        x2=r(j)
        ys1(1)=ys2(1)
        ys1(2)=ys2(2)
        call odeint2(x1,ys1,x2,ys2,iflag,eps,hmin,                      &
     &               xss,yss,dyss,rvss,errss,nss,size(xss))
          if(iflag.ne.0) then
            write(*,*) 'odeint2 error (regular sol.)=',iflag
            if ( iflag.ne.3 ) then
            call fstop(sname//                                          &
     &         ': ODEINT2 ERROR')
            end if
          endif
!        mapss(j)=nss
        g(j)=ys2(1)
        f(j)=ys2(2)
      end do

!c     renormalize the solution 
      renorm = one/g(j1min)
      g(1:j1min) = renorm*g(1:j1min)
      f(1:j1min) = renorm*f(1:j1min)
      do j=1,j1min
        renorm= (r(j)/r(j1min))**power 
        g(j)= renorm*g(j)
        f(j)= renorm*f(j)
      enddo
!c     ------------------------------------------------------------------
!c     propagate regular solution without r^power factored out
!c     out to the V=0 region
!c     ------------------------------------------------------------------

!c       minimum allowed step-size: can be zero
        hmin=0.0d0
!c       specify sub-step tolerance
        eps=abserr
!c       initialize the count of sub-steps
!c       xss,yss, etc are accumulated only for possible internal debugging purposes
        nss=0
!c       pass complex energy to subroutine thru common
      j1 = j1min
      ys2(1)=g(j1)
      ys2(2)=f(j1)
      call set_ode_params(in_out0=0,enrg0=energy)
      do j=j1min+1,jScat+1
        j1=j-1
!c       pass info to potential interpolation routines
        ipot(1)=j1
        ipot(2)=j
          call set_ode_params(ipot0=ipot,rfit0=r(j1:j),                 &
     &                                            zfit0=-0.5d0*rv(j1:j))
        x1=r(j1)
        if ( j==jScat+1 ) then
         x2 = rScat
        else 
         x2=r(j)
        end if 
        ys1(1)=ys2(1)
        ys1(2)=ys2(2)
        call odeint2(x1,ys1,x2,ys2,iflag,eps,hmin,                      &
     &               xss,yss,dyss,rvss,errss,nss,size(xss))
          if(iflag.ne.0) then
            write(*,*) 'odeint2 error (regular sol.)=',iflag
            if ( iflag.ne.3 ) then
            call fstop(sname//                                          &
     &         ': ODEINT2 ERROR')
            end if
          endif
!        mapss(j)=nss
        g(j)=ys2(1)   !  if j=jScat+1, it is for r=rScat, not for r(j)
        f(j)=ys2(2)    ! 
      enddo
!c     =================================================================
!c     At R, need spherical Bessel's for all l to get normalization and
!c     physical phase-shifts to determine cl and sl from
!c     g=r*R=r*( cl*jl - sl*nl ) and f=r*R'/M for all l's.
!c     Modify expression to account for difference between spherical and
!c     Ricatti-Bessel fcts.               Recall: R=g/r and dR=f*em/r
!c     =================================================================
      lmin=max(2,l1)
      pr=prel*rScat
!c     -----------------------------------------------------------------
      call ricbes(lmin,pr,bjl(1:lmin),bnl(1:lmin),                      &
     &                                      djl(1:lmin),dnl(1:lmin))
!c
!c  ==================================================================
!c  sl and cl at the sphere radius
!c
      j = jScat+1
      em = eoc2p1                       ! to match free-space solution
      slmt= (prel*djl(l1)-bjl(l1)/rScat)*g(j) - bjl(l1)*f(j)*em
      clmt= (prel*dnl(l1)-bnl(l1)/rScat)*g(j) - bnl(l1)*f(j)*em

        if(iprint.ge.6) then
         write(6,'('' scalar: l1,pr,prel,em='',2i5,3(1x,2d16.8))')      &
     &                                  l1,j,pr,prel,em
         write(6,'('' scalar: djl,bjl='',2(1x,2d16.8))')                &
     &                                  djl(l1),bjl(l1)
         write(6,'('' scalar: dnl,bnl='',2(1x,2d16.8))')                &
     &                                  dnl(l1),bnl(l1)
         write(6,'('' scalar: slmt,clmt='',2(1x,2d16.8))')              &
     &                                  slmt,clmt
        end if 

      z0 = sqrt(abs(slmt**2+clmt**2))
      slmt = slmt/z0
      clmt = clmt/z0  

!      if(iswzj.ne.0) then
!       gpl(j)=bjl(l1,j)/prel
!       fpl(j)=(djl(l1,j) - bjl(l1,j)/pr)/eoc2p1
!      end if

!DEBUG      p = prel/sqrt(eoc2p1)     ! non-rel. p
      p = prel

!c     =================================================================
!c     get normalization etc. at sphere boundary........................
!c     =================================================================

      cotdel=clmt/slmt
      tandel=slmt/clmt
!      g(jmt)=(clmt*bjl(l1,jmt)-slmt*bnl(l1,jmt))*z0/p
!      anorm=-(bjl(l1,jmt)*cotdel-bnl(l1,jmt))/g(jmt)
      anorm=-p/(z0*slmt)
!c
!c non-relat solution
!c
      do j=jScat+1,iend
       pr=p*r(j)
!c        --------------------------------------------------------------
!?       call ricbes(lmin,pr,bjl(1:lmin,j),bnl(1:lmin,j),                 &
!?     &                                        djl(1:lmin,j),dnl(1:lmin))
       call ricbes(lmin,pr,bjl(1:lmin),bnl(1:lmin),                     &
     &                                      djl(1:lmin),dnl(1:lmin))
!c        --------------------------------------------------------------
       g(j)=(clmt*bjl(l1)-slmt*bnl(l1))*z0/p
       f(j)=( clmt*(djl(l1)-bjl(l1)/pr) - slmt*(dnl(l1)-bnl(l1)/pr) )*z0
       if(iswzj.ne.0) then
!c
!c to start with free solution ( v(r)=0 r > R)
!c
        gpl(j)=bjl(l1)/p
        fpl(j)=(djl(l1) - bjl(l1)/pr)/eoc2p1
       end if
       if(iprint.ge.6) then
         write(6,'('' scalar: j,g,f='',i5,2(1x,2d16.8),'' *'')')        &
     &                                  j,g(j),f(j)
       end if 
      enddo

      ! alpha matrix = ratio between Rl=Zl*tl and sph. bes. Jl at origin
      ! for lloyd formula
      tl = 1.d0/(prel*(dcmplx(0.d0,1.d0)-cotdel))
      alpha = tl*(anorm*g(1))/(a0*r(1)**l1)

!c     =================================================================

      if( iswzj .ne. 0 ) then

!DEBUG      p = prel/sqrt(eoc2p1)     ! non-rel. p
        p = prel
!c=====================================================================
!c irregular solution
!c=====================================================================
!c
!c  start irregular solution outside m.t. sphere
!c

!c
!c starting with free solution ( v(r)=0 r > R)
!c

!      clj =-bnl(2,j)*gpl(j) - bnl(1,j)*fpl(j)/p
!      slj =-bjl(2,j)*gpl(j) - bjl(1,j)*fpl(j)/p
!      gpl(j)=clj*bjl(1,j)-slj*bnl(1,j)
!      fpl(j)=-p*(clj*bjl(2,j)-slj*bnl(2,j))

!      do j=jScat+1,iend 
!         pr = p*r(j)
!         gpl(j)=bjl(l1,j)/p
!         fpl(j)=(djl(l1,j) - bjl(l1,j)/pr)/eoc2p1
!      end do
      pr = p*rScat
      call ricbes(lmin,pr,bjl(1:lmin),bnl(1:lmin),                      &
     &                                          djl(1:lmin),dnl(1:lmin))
      gpl_s = bjl(l1)/p
      fpl_s = (djl(l1) - bjl(l1)/pr)/eoc2p1
!c-----------------------------------------------------------------------
!c
!c  get irregular solution inside m.t. sphere
!c
!c       minimum allowed step-size: can be zero
        hmin=0.0d0
!c       specify sub-step tolerance
        eps=abserr
!c       initialize the count of sub-steps
!c       xss,yss, etc are accumulated only for possible internal debugging purposes
        nss=0
!c       specify outward propagation of soln with no asymptotic behaviour factored out
        call set_ode_params(in_out0=0,enrg0=energy)
!c     ------------------------------------------------------------------
!c     get irregular solution inside m.t. sphere
!c     but dont get too near origin because of 1/r^power divergence
!c     ------------------------------------------------------------------
        j1 = jScat
        ys2(1)=gpl_s
        ys2(2)=fpl_s
        do j=jScat,j1min+1,-1     !!! TODO: to fix potential overflow
          j1=j+1
          ipot(1)=j
          ipot(2)=j1
          call set_ode_params(ipot0=ipot,rfit0=r(j:j1),                 &
     &                                            zfit0=-0.5d0*rv(j:j1))
          if ( j==jScat ) then
           x1=rScat   
          else
           x1=r(j1)
          end if
          x2=r(j)
          ys1(1)=ys2(1)
          ys1(2)=ys2(2)
          call odeint2(x1,ys1,x2,ys2,iflag,eps,hmin,                    &
     &                 xss,yss,dyss,rvss,errss,nss,size(xss))
          if(iflag.ne.0) then
            write(*,*) 'odeint2 error=',iflag
            if ( iflag.ne.3 ) then
            call fstop(sname//                                          &
     &         ': ODEINT2 ERROR')
            end if
          endif
!          mapss(j)=nss
          gpl(j)=ys2(1)
          fpl(j)=ys2(2)
        enddo

        hmin=0.0d0
        eps=abserr
        nss=0
        j1 = j1min+1
!renormalization
        renorm = r(j1)**power
        ys2(1)=gpl(j1)*renorm
        ys2(2)=fpl(j1)*renorm
        call set_ode_params(in_out0=-1,enrg0=energy)
        do j=j1min,1,-1
          j1=j+1
          ipot(1)=j
          ipot(2)=j1
          call set_ode_params(ipot0=ipot,rfit0=r(j:j1),                 &
     &                                            zfit0=-0.5d0*rv(j:j1))
          x1=r(j1)
          x2=r(j)
          ys1(1)=ys2(1)
          ys1(2)=ys2(2)
          call odeint2(x1,ys1,x2,ys2,iflag,eps,hmin,                    &
     &                 xss,yss,dyss,rvss,errss,nss,size(xss))
          if(iflag.ne.0) then
            write(*,*) 'odeint2 error=',iflag
            if ( iflag.ne.3 ) then
            call fstop(sname//                                          &
     &         ': ODEINT2 ERROR')
            end if
          endif
!          mapss(j)=nss
!renormalization
!c         gpl(j)=( r(j)**(-power) )*ys2(1), fpl(j)=( r(j)**(-power) )*ys2(2)
          renorm = r(j)**power

! to prevent overflow
          overflo = max( log10(abs(ys2(1))),log10(abs(ys2(2))) ) -      &
     &                                                    log10(renorm)
          if(overflo.lt.log10(huge(one)/ten)) then
            gpl(j)=ys2(1)/renorm
            fpl(j)=ys2(2)/renorm
          else
            gpl(j)=(huge(one)/ten)*(ys2(1)/abs(ys2(1)))
            fpl(j)=(huge(one)/ten)*(ys2(2)/abs(ys2(2)))
          endif
        end do

!c===============end irregular solution calculation====================
      else
!c
!c  zero out irregular solution arrays for case when iswzj=0
!c
        gpl = 0
        fpl = 0  
      end if
!c
!c     ===============================================================

      return
!c
      end subroutine scalar
!      
      end subroutine getSSgf
