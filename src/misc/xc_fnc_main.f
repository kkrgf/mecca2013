       program xc_fnc_main
       use mecca_types, only : RunState, IniFile
       use xc_mecca, only : xc_mecca_pointer
       use xc_mecca, only : xc_alpha2,gInfo_libxc
       use mecca_run, only : setMS
       implicit none

       type(IniFile), target :: inFile
       type(RunState) :: M_S
       type(xc_mecca_pointer) :: libxc_p
       real(8) :: zeta=0
       real(8) :: v_xc,e_xc,rs
       real(8) :: rsmin=1.d-5,rsmax=1.d0
       real(8) :: one=1.d0,qpls=1.d0
       real(8) :: rho1,rho2,rho_i,Tempr,step
       integer :: i,xc_id,nmax
       character(128) :: xc_descr
       real(8), parameter :: pi4=16.d0*atan(1.d0)
       
       Tempr = 1.d+8
       nmax = 100
       xc_id = 259000

       M_S%intfile => inFile
       M_S%intfile%Tempr = Tempr
       rho1 = qpls / (pi4/3.d0*rsmax**3)
       rho2 = qpls / (pi4/3.d0*rsmin**3)

       rho1 = 1.d-5
       rho2 = rho1+1000.d0

       step = abs(rho2-rho1)/(nmax+1)
       
       call setMS(M_S)
       do i=1,nmax+1
        rho_i = rho1 + (i-1)*step
        rs = (3.d0/(pi4*rho_i))**(1.d0/3.d0)
        v_xc = xc_alpha2(rs,zeta,one,xc_id,e_xc,libxc_p)
        write(6,'(3(g18.8,1x))') rho_i,v_xc,e_xc
       end do
       call gInfo_libxc(libxc_p,xc_descr)
       write(6,'(a)') '#functional '//trim(xc_descr)
       write(6,'(a,g14.6)') '#temperature ',Tempr

       stop
       end program 
