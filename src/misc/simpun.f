!c
!c     ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      subroutine simpun(xx,fx,nx,i,ax)
!c     =============================================================
!c
!c     program author      j. barish,
!c     computing technology center, union carbide corp., nuclear div.,
!c     oak ridge, tenn.
!c
      implicit real*8 (a-h,o-z)
!c
!c     +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!      include 'mkkrcpa.h'
!c     +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!c
      integer i,nx
      real(8), dimension(nx+1) ::  xx,fx,ax
      integer ix,ic
      real(8) :: d1,d2,d3,a2,a3
!c
      if (i.gt.0) then
      ax(1)=0.0d+00
      do ix=2,nx,2
         d1=xx(ix)-xx(ix-1)
         ax(ix)=ax(ix-1)+d1/2.0d+00*(fx(ix)+fx(ix-1))
         if (nx.eq.ix) then
            return
         endif
         d2=xx(ix+1)-xx(ix-1)
         d3=d2/d1
         a2=d3/6.0d+00*d2**2/(xx(ix+1)-xx(ix))
         a3=d2/2.0d+00-a2/d3
         ax(ix+1)=ax(ix-1)+(d2-a2-a3)*fx(ix-1)+a2*fx(ix)+a3*fx(ix+1)
      enddo
      else
      ax(nx)=0.0d+00
      do ix=2,nx,2
         ic=nx+1-ix
         d1=xx(ic+1)-xx(ic)
         ax(ic)=ax(ic+1)+d1/2.0d+00*(fx(ic+1)+fx(ic))
         if (nx.eq.ix) then
            return
         endif
         d2=xx(ic+1)-xx(ic-1)
         d3=d2/(xx(ic)-xx(ic-1))
         a2=d3/6.0d+00*d2**2/d1
         a3=d2/2.0d+00-a2/d3
         ax(ic-1)=ax(ic+1)+(d2-a2-a3)*fx(ic-1)+a2*fx(ic)+ a3*fx(ic+1)
      enddo
      endif
!c
      return
      end
