!BOP
!!ROUTINE: scf_ss
!!INTERFACE:
      subroutine scf_ss(outinfo)
!!DESCRIPTION:
!      

!!USES:
       use mecca_constants
       use mecca_types
       use mecca_run, only : getMS
       use kkrgf, only : gLocalizedRho
       use scf_io,  only : save_inifile
       use iso_fortran_env, only : output_unit

!!DO_NOT_PRINT
      implicit none
!      interface
!       subroutine gRnn(nsub,rs_box,rnb)
!       use mecca_types, only : RS_Str
!       integer, intent(in) :: nsub
!       type ( RS_Str ), pointer :: rs_box
!       real(8), intent(out) :: rnb(*)
!       end subroutine
!      end interface
!!DO_NOT_PRINT

!!OUTPUT ARGUMENTS:
      character*(*) outinfo           ! output info

!!REVISION HISTORY:
! Initial version - A.S. - 2015 
!
!EOP

!BOC
      type(RunState), pointer :: M_S
      type(Sublattice), pointer :: sublat
      character(*), parameter :: sname='scf_ss'
!      type (IniFile),  pointer :: inFile
      integer :: mtasa_save
      integer :: ncpa_save
      integer :: nscf_save
      integer :: sch_save
      logical :: fileexist
      integer :: nsub
      real(8), allocatable :: rnb(:)
      real(8) :: etop

      M_S => getMS()
      if ( .not. associated(M_S) ) return 
      if ( M_S%intfile%ncpa==-1 ) return

      if ( M_S%intfile%iprint<0 ) then
        open(unit=output_unit,file='/dev/null')
      end if
      mtasa_save = M_S%intfile%mtasa
      ncpa_save = M_S%intfile%ncpa
      nscf_save = M_S%intfile%nscf
      sch_save = M_S%intfile%intgrsch
      M_S%intfile%intgrsch = 1
      M_S%intfile%mtasa = 2
      M_S%intfile%ncpa = -1 
      M_S%intfile%nscf = min(nscf_save,20) 

      call mecca_scf(M_S,outinfo)

      M_S%intfile%ncpa = 1 
      etop = -1.e-6
!      M_S%intfile%npts = 40
!      M_S%intfile%eitop = 1.e-8
      call gLocalizedRho(M_S,etop)
      if ( M_S%intfile%iprint<0 ) then
       open(output_unit)
      end if

      if ( associated(M_S%rs_box) ) then
       if ( M_S%rs_box%nsubl <=0 ) call fstop('ERROR in SCF_SS')
       allocate(rnb(1:M_S%rs_box%nsubl))
       do nsub=1,M_S%rs_box%nsubl
        sublat => M_S%intfile%sublat(nsub)
        if ( maxval(sublat%compon(1:sublat%ncomp)%zID)<=0 ) cycle
        call gRnn(nsub,M_S%rs_box,rnb)
!DEBUGPRINT        
        write(6,'(//a,i2,a,(4f8.5))') ' NSUB=',nsub,' RNB=',sngl(rnb)
!DEBUGPRINT        
        call findSPrad(nsub,rnb,M_S%intfile)
!DEBUGPRINT        
        write(6,'(a,i2,4(a,f8.5))') ' nsub=',nsub,                      &
     &         ' IS-r=',M_S%intfile%sublat(nsub)%rIS                    &
     &        ,' rSph:',M_S%intfile%sublat(nsub)%compon(1)%rSph
!DEBUGPRINT        
       end do
       do nsub=1,M_S%rs_box%nsubl
        sublat => M_S%intfile%sublat(nsub)
        if ( maxval(sublat%compon(1:sublat%ncomp)%zID)>0 ) cycle
        call gRnn(nsub,M_S%rs_box,rnb)
!DEBUGPRINT        
        write(6,'(//a,i2,a,(4f8.5))') ' NSUB=',nsub,' RNB=',sngl(rnb)
!DEBUGPRINT        
        call findESrad(nsub,rnb,M_S%intfile)
!DEBUGPRINT        
        write(6,'(a,i2,4(a,f8.5))') ' nsub=',nsub,                      &
     &         ' IS-r=',M_S%intfile%sublat(nsub)%rIS                    &
     &        ,' rSph:',M_S%intfile%sublat(nsub)%compon(1)%rSph
!DEBUGPRINT        
       end do
       deallocate(rnb)
      else
       call fstop('ERROR: RS_BOX is not found in SCF_SS')
      end if

      inquire(file=M_S%intfile%io(nu_inpot)%name,EXIST=fileexist) 
      if ( .not. fileexist ) then
        call cptxtfile(M_S%intfile%io(nu_outpot)%name,                  &
     &                 M_S%intfile%io(nu_inpot)%name)
       M_S%intfile%io(nu_inpot)%status = 'o' 
      end if

      M_S%intfile%intgrsch = sch_save
      M_S%intfile%mtasa = mtasa_save
      M_S%intfile%ncpa = ncpa_save
      M_S%intfile%nscf = nscf_save 

      call save_inifile(M_S)

!      call fstop('DEBUG SCF_SS')
      return
!EOC      
      contains
!
!BOP
!!ROUTINE: gRnn
!!INTERFACE:
      subroutine gRnn(nsub,rs_box,rnb)
!!DESCRIPTION:
! to find nearest neighbor distances, {\tt rnb}, of sublattice {\tt nsub}    

!!USES:
!      use mecca_constants
!      use mecca_types, only : RS_Str
      use mecca_interface, only : genvec
       
!!DO_NOT_PRINT       
      implicit none
!!DO_NOT_PRINT       

!!ARGUMENTS:
      integer, intent(in) :: nsub
      type ( RS_Str ), pointer :: rs_box
      real(8), intent(out) :: rnb(*)

!!REVISION HISTORY:
! Initial version - A.S. - 2015 
!
!EOP
 
!BOC
      integer :: ndimv,nvsn
      integer :: i,iat,j
      integer :: iprint=0
      real(8), allocatable :: vsnlat(:)  ! vsnlat(ndimv,3)
      real(8) :: Rmx,rj,vj(3),r0(3),ri(3),bas(3,3)

      do iat=1,rs_box%natom
       if ( rs_box%itype(iat)==nsub ) then
        r0(1:3) = rs_box%xyzr(1:3,iat)
        exit
       end if
      end do
      bas = rs_box%alat*rs_box%rslatt/(two*pi)
      Rmx = 5*(rs_box%volume/rs_box%natom*three/(four*pi))**(one/three)
      rnb(1:rs_box%nsubl) = rs_box%alat*(one+1.e-10) 
      do iat=1,rs_box%natom
       i = rs_box%itype(iat)
       ri(1:3) = rs_box%xyzr(1:3,iat)-r0(1:3)
       call genvec(bas,Rmx,ri,1,vsnlat,ndimv,nvsn,iprint) 
       do j=1,nvsn
        vj(1) = vsnlat(j) + ri(1)
        vj(2) = vsnlat(ndimv+j) + ri(2)
        vj(3) = vsnlat(2*ndimv+j) + ri(3)
        rj = dot_product(vj,vj)
        if ( rj<= zero ) cycle
        rj = sqrt(rj)
        rnb(i) = min(rnb(i),rj)
       end do 
      end do
      if (allocated(vsnlat)) deallocate(vsnlat)

      return
!EOC      
      end subroutine gRnn
!      
!BOP
!!ROUTINE: findESrad
!!INTERFACE:
      subroutine findESrad(nsub,rnb,inFile)
!!DESCRIPTION:
!

!!DO_NOT_PRINT       
      implicit none
!!DO_NOT_PRINT       

!!ARGUMENTS:
      integer, intent(in) :: nsub
      real(8), intent(in) :: rnb(*)
      type ( IniFile ), pointer ::  inFile

!!REVISION HISTORY:
! Initial version - A.S. - 2015 
!
!EOP
 
!BOC
      type(Sublattice), pointer :: sublat
      integer :: i,isub
      real(8) :: rmn,rout
      rmn = inFile%alat
      isub = nsub
      do i=1,inFile%nsubl
        sublat => inFile%sublat(i)
        if ( maxval(sublat%compon(1:sublat%ncomp)%zID)>0 ) then
         if ( rnb(i)<rmn ) then
          rmn = rnb(i)
          isub = i
         end if
        end if
      end do
      if ( isub==nsub ) then
       rout = rnb(nsub)/2
      else
       rout = inFile%alat
       do i=1,inFile%nsubl
        if ( rmn == rnb(i) ) then
         sublat => inFile%sublat(i)
         rout = min(rout,rnb(i)-sublat%rIS*inFile%alat)
        end if
       end do
      end if
      rout = rout*0.95d0
      write(6,*) '    ROUT=',rout
      sublat => inFile%sublat(nsub)
      sublat%compon(1:sublat%ncomp)%rSph = rout 
      sublat%rIS = rout/inFile%alat
      return
      end subroutine findESrad
!      
!BOP
!!ROUTINE: findSPrad
!!INTERFACE:
      subroutine findSPrad(nsub,rnb,inFile)
!!DESCRIPTION:
!

!!USES:
      use gfncts_interface, only : g_meshv,g_rho,g_rhocor,g_rhoval
       
!!DO_NOT_PRINT       
      implicit none
!!DO_NOT_PRINT       

!!ARGUMENTS:
      integer, intent(in) :: nsub
      real(8), intent(in) :: rnb(*)
      type ( IniFile ), pointer ::  inFile

!!REVISION HISTORY:
! Initial version - A.S. - 2015 
!
!EOP
 
!BOC
      integer :: i,i0,i1,iex,ic,isub,jc,nd0,nd1,md0,md1
      integer :: ndrpts,ndcomp,nsublat,nspin
      real(8) :: h,rmx0,rmx1,step,rin,rout,ris,xm
      real(8), external :: ylag

      type(Sublattice), pointer :: sublat0,sublat
      type(SphAtomDeps), pointer :: v_rho_box,v_rho_box1
      real(8), allocatable :: xr(:),xr0(:),rr(:),rr0(:),rsph(:)
      real(8), allocatable :: tmpchg(:),rhot(:),rhot0(:),rhoj(:)

      sublat0 => inFile%sublat(nsub)
      if ( maxval(sublat0%compon(1:sublat0%ncomp)%zID)<=zero ) return
      call g_ndims(inFile,ndrpts,ndcomp,nsublat,nspin)
      allocate(xr(ndrpts),xr0(ndrpts),rr(ndrpts),rr0(ndrpts))
      allocate(tmpchg(ndrpts),rhot(ndrpts),rhot0(ndrpts),rhoj(ndrpts))
      allocate(rsph(sublat0%ncomp))
      if ( sublat0%ncomp==1 ) then
       sublat0%rIS = rnb(nsub)/two/inFile%alat
      end if
      call g_meshv(sublat0%compon(1)%v_rho_box,h,                       &
     &                                      xr0(1:ndrpts),rr0(1:ndrpts))
      rmx1 = zero
      md0 = 0
      do ic=1,sublat0%ncomp
       v_rho_box => sublat0%compon(ic)%v_rho_box
       nd0 = v_rho_box%ndchg
       md0 = max(md0,nd0)
       h = (v_rho_box%xmt-v_rho_box%xst)/(v_rho_box%jmt-1)
       rmx1 = max(rmx1,exp(v_rho_box%xst+(nd0-1)*h))  
      end do
!DEBUGPRINT
!       write(6,*) ' NSUB=',nsub,' RMX1=',rmx1
!DEBUGPRINT
      step = rmx1/1000
      ris = inFile%alat
      rsph = inFile%alat
      do isub=1,nsublat
        sublat => inFile%sublat(isub)
!DEBUG        if ( isub==nsub .and. sublat%ncomp==1 ) then
!DEBUG         sublat0%compon(1)%rSph = rnb(nsub)/two 
!DEBUG         cycle
!DEBUG        end if
        rmx0 = zero
        do jc=1,sublat%ncomp
         v_rho_box1 => sublat%compon(jc)%v_rho_box
         nd1 = v_rho_box1%ndrpts
         h = (v_rho_box1%xmt-v_rho_box1%xst)/(v_rho_box1%jmt-1)
         rmx0 = max(rmx0,exp(v_rho_box1%xst+(nd1-1)*h))  
        end do
!DEBUGPRINT
!       write(6,*) '   ISUB=',nsub,' RMX0=',sngl(rmx0)
!DEBUGPRINT
        rmx0 = rnb(isub)-rmx0
        if ( rmx0>=rmx1 ) cycle   ! distant neighbors
        
        rhot(1:md0) = zero
        rmx0 = rr0(md0)
        do jc=1,sublat%ncomp
         v_rho_box1 => sublat%compon(jc)%v_rho_box
         nd1 = v_rho_box1%ndchg
         call g_meshv(v_rho_box1,h,xr(1:nd1),rr(1:nd1)) 
         rmx0=min(rmx0,rnb(isub)-exp(v_rho_box1%xst+(nd1-1)*h))
         rmx0 = max(rr0(1),rmx0)
         call g_rho(v_rho_box1,1,tmpchg)
         call g_rho(v_rho_box1,nspin,rhoj)
         tmpchg(1:nd1) = tmpchg(1:nd1)+rhoj(1:nd1)
         tmpchg(1:nd1) = tmpchg(1:nd1)/rr(1:nd1)**2
!DEBUGPRINT
!       write(60+nsub,'(/3(a,i2))')'# NSUB=',nsub,' ISUB=',isub,' JC=',jc
         do i=1,md0
          xm = rnb(isub)-rr0(i)
          if ( xm>min(rr(nd1),rmx1) .or. xm<rr(1) ) then
           rhoj(i) = -1.e20
          else
           rhoj(i)  = ylag(log(xm),xr(1:nd1),tmpchg(1:nd1),0,3,nd1,iex)
!       write(60+nsub,'(i4,3(e15.5))') i,rr0(i),xm,rhoj(i)
!DEBUGPRINT
          end if
         end do

         do i=1,md0
          if ( rhoj(i)<zero ) then
           rhot(i) = rhoj(i)
          else
           rhot(i)=rhot(i)+sublat%compon(jc)%concentr*rhoj(i)
          end if
         end do
        end do
!         rhot -- isub density

        do ic=1,sublat0%ncomp
         v_rho_box => sublat0%compon(ic)%v_rho_box
         md1 = v_rho_box%ndchg
         call g_meshv(v_rho_box,h,xr(1:md1),rr(1:md1)) 
!?         i0=1
!?         do i=2,md1
!?          if ( rr0(i)>rmx0 ) then
!?           i0=max(1,i-3)
!?           exit
!?          end if
!?         end do
!?         i1 = md1
!?         rhoj(1:md0) = zero
         call g_rho(v_rho_box,1,tmpchg)
         call g_rho(v_rho_box,nspin,rhoj)
         tmpchg(1:md1) = tmpchg(1:md1)+rhoj(1:md1)
         tmpchg(1:md1) = tmpchg(1:md1)/rr0(1:md1)**2
!DEBUGPRINT
!       write(70+nsub,'(/3(a,i2))')'# NSUB=',nsub,' ISUB=',isub,' IC=',ic
!       write(70+nsub,'(3(a,i5))') '# I0=',i0,' I1=',i1,' MD0=',md0
!DEBUGPRINT
         do i=1,md0
          if ( rr0(i)<rmx0 .or. rr0(i)>rr0(md1) ) then
           rhoj(i) = -1.e20
          else 
            xm = log(rr0(i))
            rhoj(i)  = ylag(xm,xr(1:md1),tmpchg(1:md1),0,3,md1,iex)
!DEBUGPRINT
!       write(70+nsub,'(i4,3(e15.5))') i,rr0(i),rhoj(i)
!DEBUGPRINT
          end if  
         end do ! i-cycle end
         rin = sublat0%compon(ic)%rSph
         if ( sublat0%compon(ic)%zID>0 ) then
          call minOfSum(md0,rr0(1:md0),rhoj(1:md0),rhot(1:md0),rin,rout)
         else
          rout = rin
         end if
         rsph(ic) = min(rsph(ic),rout)
         if ( sublat0%ncomp>1 ) then
          if ( ic==1 ) then
           rhot0 = rhoj
          else
           do i=1,md0
            if ( rhoj(i)<zero ) then
             rhot0(i) = rhoj(i)
            else
             rhot0(i)=rhot0(i)+sublat0%compon(jc)%concentr*rhoj(i)
            end if
           end do
          end if
         end if ! %ncomp>1 endif
        end do ! ic-enddo
        if ( sublat0%ncomp>1 ) then
         rin = sublat0%rIS*inFile%alat
         call minOfSum(md0,rr0(1:md0),rhot0(1:md0),rhot(1:md0),rin,rout)
         do ic=1,sublat0%ncomp
          if ( sublat0%compon(ic)%zID<=0 ) then
           rsph(ic) = rout
          end if
         end do
        end if
        ris = min(ris,rout/inFile%alat)
      end do  ! isub-cycle end
      sublat0%compon(1:sublat0%ncomp)%rSph = rsph(1:sublat0%ncomp)
      sublat0%rIS = ris

      deallocate(xr,xr0,rr,rr0,rsph)
      deallocate(tmpchg,rhot,rhot0,rhoj)
      return
!EOC      
      end subroutine findSPrad
!
      subroutine minOfSum(n,r,f1,f2,rin,rout)
      implicit none
      integer, intent(in) :: n
      real(8), intent(in) :: r(n)
      real(8), intent(in), target :: f1(n)
      real(8), intent(in) :: f2(n)
      real(8), intent(in) :: rin
      real(8), intent(out) :: rout
      real(8), external :: ylag
      integer, external :: indRmesh

      real(8), pointer :: ft(:)
      real(8), pointer :: dy(:)
      real(8), pointer :: d2y(:)
      integer :: i,i0,i1,i2,ipls,imns,iex
      integer :: k1,k2,kn
      real(8) :: x1,x2,rpls,rmns,f0,s,s1
      logical :: intrsct,found
      real(8), pointer :: fun(:)=>null()

      s = 0
      intrsct = .false.
      do i=1,n
       if ( f1(i)<=0 .or. f2(i)<=0 ) cycle
       s1 = sign(one,f1(i)-f2(i))
       if ( s==0 ) then
        s = s1
       else
        if ( s.ne.s1 ) then
         intrsct = .true.
         exit
        end if
       end if
      end do
      if ( intrsct ) then
       allocate(ft(n))
       ft = f1+f2
      else
       ft => f1
      end if
      i0=indRmesh(rin,n,r)
      k1 = 1
      do i=i0,1,-1
       if ( ft(i)<0 ) then
        k1=i+1
        exit
       end if
      end do
      k2 = n
      do i=i0,n
       if ( ft(i)<0 ) then
        k2=i-1
        exit
       end if
      end do
      kn = k2-k1+1

!DEBUGPRINT
!      write(56,'(/a,i4,a,f10.6)') '# kn=',kn,' rin=',rin
!      write(57,'(/a,i4,a,f10.6)') '# kn=',kn,' rin=',rin
!      write(6,'(/a,i4,a,f10.6)') '# kn=',kn,' rin=',rin
!DEBUGPRINT
      allocate(dy(n))
      call derv5_VP(ft(k1:k2),dy(k1:k2),r(k1:k2),1,kn)
      if ( intrsct ) then
       fun => dy
!DEBUGPRINT
!      do i=k1,k2
!      write(56,'(i5,4e18.7)') i,r(i),ft(i),dy(i) 
!      if (min(f1(i),f2(i))>0) then
!      write(57,*) i,sngl(r(i)),sngl(f1(i)),sngl(f2(i)) 
!      end if
!      end do
!      write(56,*)
!      write(57,*)
!DEBUGPRINT
      else
       allocate(d2y(n))
       call derv5_VP(dy(k1:k2),d2y(k1:k2),r(k1:k2),1,kn)
       fun => d2y
!DEBUGPRINT
!      do i=k1,k2
!      write(56,'(i5,4e18.7)') i,r(i),ft(i),dy(i),d2y(i) 
!      if (min(f1(i),f2(i))>0) then
!      write(57,*) i,sngl(r(i)),sngl(f1(i)),sngl(f2(i)) 
!      end if
!      end do
!      write(56,*)
!DEBUGPRINT
      end if

      ipls = 0
      rpls = rin
      do i=max(k1,i0-1),k2
       if ( intrsct ) then
        found = fun(i-1)<0 .and.fun(i)>=0
       else
        found = ( dy(i-1)>=0 ) .and.                                    &
     &          ( fun(i-1)<0 .and.fun(i)>=0 )
!??     &          ( sign(one,fun(i-1)).ne.sign(one,fun(i)) )
       end if
       if( found ) then
        x1 = r(i-1)
        x2 = r(i)
        ipls = i
        i2 = min(k2,i+2)
        i1 = min(i-3,i2-3)
        s = -sign(one,fun(i-1))
!DEBUGPRINT
!          write(6,'(a,i4,3(a,e14.5))') ' i=',i,' r=',r(i),' fun(i-1)',   &
!     &                                 fun(i-1),' fun(i)=',fun(i)
!DEBUGPRINT
        do 
         rpls = 0.5d0*(x1+x2)
         f0 = s*ylag(rpls,r(i1:i2),fun(i1:i2),0,3,i2-i1+1,iex)
         if ( abs(f0)<1.d-8 .or. abs(x2-x1)<1.e-14 ) then
           rpls = 0.5d0*(x1+x2)
           exit
         end if
         if ( f0<0 ) then
           x1 = rpls
         else
           x2 = rpls
         end if
        end do
        exit
       end if 
      end do
      imns = 0
      rmns = rin
      do i=min(k2,i0+1),k1+1,-1
       if ( intrsct ) then
        found = fun(i-1)<0 .and.fun(i)>=0
       else
        found = ( dy(i-1)>=0 ) .and.                                    &
     &          ( fun(i-1)<0 .and.fun(i)>=0 )
!???     &          ( sign(one,fun(i-1)).ne.sign(one,fun(i)) )
       end if
       if( found ) then
        x1 = r(i-1)
        x2 = r(i)
        imns = i
        i1 = max(k1,i-3)
        i2 = max(i+2,i1+3)
        s = -sign(one,fun(i-1))
!DEBUGPRINT
!          write(6,'(a,i4,3(a,e14.5))') ' i=',i,' r=',r(i),' fun(i-1)',   &
!     &                                 fun(i-1),' fun(i)=',fun(i)
!DEBUGPRINT
        do 
         rmns = 0.5d0*(x1+x2)
         f0 = s*ylag(rmns,r(i1:i2),fun(i1:i2),0,3,i2-i1+1,iex)
         if ( abs(f0)<1.d-8 .or. abs(x2-x1)<1.e-14 ) then
           rmns = 0.5d0*(x1+x2)
           exit
         end if
         if ( f0<0 ) then
           x1 = rmns
         else
           x2 = rmns
         end if
        end do
        exit
       end if 
      end do
!DEBUGPRINT
!      write(6,*) imns,' RMNS=',rmns,f0
!DEBUGPRINT
      if ( ipls==0 .and. imns==0 ) then 
       rout = rin
!DEBUGPRINT      
!      write(6,*) '0 0  ROUT=',rout
!DEBUGPRINT      
      else if ( ipls==0 ) then
        rout = rmns 
!DEBUGPRINT      
!      write(6,*) '1 0  ROUT=',rout
!DEBUGPRINT      
      else if ( imns==0 ) then
        rout = rpls
!DEBUGPRINT      
!      write(6,*) '0 1  ROUT=',rout
!DEBUGPRINT      
      else 
       if ( rin-rmns<rpls-rin ) then
        rout = rmns 
!DEBUGPRINT      
!      write(6,*) 'rmns  ROUT=',rout
!DEBUGPRINT      
       else if ( rpls-rin<rin-rmns) then
        rout = rpls
!DEBUGPRINT      
!      write(6,*) 'rpls  ROUT=',rout
!DEBUGPRINT      
       else
        rout = rin
!DEBUGPRINT      
!           write(6,*) 'ELSE?  ROUT=',rout
!DEBUGPRINT      
       end if
      end if 

      return
      end subroutine minOfSum

      end subroutine scf_ss
