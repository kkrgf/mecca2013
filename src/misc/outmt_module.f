!
!  outMtMod.f
!  meccalib
!
!  Created by andrei on 9/16/07.
!  Copyright 2007 __MyCompanyName__. All rights reserved.
!
      module OutMT

      integer ik, nsub
      complex*16 wghtn, energy
      logical ScatShCorr/.false./ ! yes/no for shape-correction in scattering problem

!c      save ik,nsub,wghtn,energy,ScatShCorr

      end module OutMT
!c
      subroutine OutMTModule(iflag)

      use OutMT
      implicit none
      include 'ShapeCorr.h'

      integer iflag
!c
      if(iflag.eq.2) then
       if(.not.ScatShCorr) iflag = -2
      else if(iflag.eq.-2) then
       ScatShCorr = .false.
      else if(iflag.eq.-3) then
       ScatShCorr = .true.
      else
       call p_fstop(' OutMTModule: wrong iflag..')
      end if

      return
      end
!ccc
      subroutine chggrnmt(iflag,en0,wght0,ik0,nsub0)
      use OutMT
      implicit none

      integer iflag, ik0, nsub0
      complex*16 en0, wght0

      if(iflag.gt.0) then
       if(iflag.eq.1) then
      ik     = ik0
      nsub   = nsub0
       else if(iflag.eq.2) then
      energy = en0
      wghtn  = wght0
       else
      energy = en0
      wghtn  = wght0
      ik     = ik0
      nsub   = nsub0
       end if
      else if(iflag.eq.0) then
       call p_fstop(' chggrnmt: wrong iflag..')
      else
       en0   = energy
       wght0 = wghtn
       ik0   = ik
       nsub0 = nsub
      end if

      return
!ccc
      end
