!c     cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      subroutine zsngls1(zvalss,zcorss,ztotss,zvalav,ztotav,            &
     &                  atcon,numbsub,nsublat,komp,iprint,istop)
!c     ================================================================
!c
      use universal_const
      implicit real*8(a-h,o-z)
!c
!c     +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!      include 'mkkrcpa.h'
!c     +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!c
      character sname*10
      character istop*10
!c
      integer   komp(*)
      integer   numbsub(nsublat)
      integer   iprint
!c
      real(8) ::    atcon(:,:)  ! (ipcomp, nsublat)
      real(8) ::    zvalss(:,:)  ! (ipcomp,nsublat)
      real(8) ::    zcorss(:,:)  ! (ipcomp,nsublat)
      real(8) ::    ztotss(:,:)  ! (ipcomp,nsublat)
      real(8) ::    zvalav
!c parameter
      real*8    tol
!c
!c     +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
      parameter (tol=(one/ten)**3)
      parameter (sname='zsngls')
!c     +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!c
!c     ***************************************************************
!c
!c     initializes:
!c        number of equivalent sites
!c               for each sublattice :  numbsub(sublat)
!c                    atomic charges :  ztotss(comp,sublat)
!c            average valence charge :  zvalav
!c            average atomic  charge :  ztotav
!c
!c
!c     requires   :
!c                   valence charges :  zvalss(comp,sublat)
!c                     core  charges :  zcorss(comp,sublat)
!c             atomic concentrations :  atcon(comp,sublat)
!c
!c     *****************************************************************
!c
!c
      zvalav=zero
      ztotav=zero
!c
      natom = 0
      do in=1,nsublat
         do n=1,komp(in)
          zvalss(n,in) = ztotss(n,in) - zcorss(n,in)
          zvalav       = zvalav+zvalss(n,in)*atcon(n,in)*numbsub(in)
          ztotav       = ztotav+ztotss(n,in)*atcon(n,in)*numbsub(in)
         enddo
       natom = natom + numbsub(in)
      enddo
      if(iprint.ge.1) then
       write(6,'(/''  Total, valence, core, and semi-core charges'')')
       do in=1,nsublat
         write(6,'(/''   Sublattice '',t25,''='',i3)') in
         do n=1,komp(in)
            write(6,'(''     Component '',t25,''='',i3)') n
            write(6,'(''       total'',t25,''='',f10.4)')               &
     &                                       ztotss(n,in)
            write(6,'(''       valence'',t25,''='',f10.4)')             &
     &                                       zvalss(n,in)
            write(6,'(''       core'',t25,''='',f10.4)')                &
     &                                       zcorss(n,in)
         enddo
       enddo
      end if
      write(6,'(''   Average tot. charge'',t25,''='',f10.5)')           &
     &  ztotav/natom
      write(6,'(''   Average val. charge'',t25,''='',f10.5)')           &
     &  zvalav/natom
!c
      if(sname.eq.istop) then
         call fstop(sname)
      endif
!c
      return
      end

!c     cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      subroutine zsngls(zvalss,zsemss,zcorss,ztotss,zvalav,ztotav,      &
     &                  atcon,numbsub,nsublat,komp,iprint,istop)
!c     ================================================================
!c
      use universal_const
      implicit real*8(a-h,o-z)
!c
!c     +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!      include 'mkkrcpa.h'
!c     +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!c
      character sname*10
      character istop*10
!c
      integer   komp(*)
      integer   numbsub(nsublat)
      integer   iprint
!c
      real(8) ::    atcon(:,:)  ! (ipcomp, nsublat)
      real(8) ::    zvalss(:,:)  ! (ipcomp,nsublat)
      real(8) ::    zsemss(:,:)  ! (ipcomp,nsublat)
      real(8) ::    zcorss(:,:)  ! (ipcomp,nsublat)
      real(8) ::    ztotss(:,:)  ! (ipcomp,nsublat)
      real*8    zvalav
!c parameter
      real*8    tol
!c
!c     +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
      parameter (tol=(one/ten)**3)
      parameter (sname='zsngls')
!c     +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!c
!c     ***************************************************************
!c
!c     initializes:
!c        number of equivalent sites
!c               for each sublattice :  numbsub(sublat)
!c                    atomic charges :  ztotss(comp,sublat)
!c            average valence charge :  zvalav
!c            average atomic  charge :  ztotav
!c
!c
!c     requires   :
!c                   valence charges :  zvalss(comp,sublat)
!c                semi-core  charges :  zsemss(comp,sublat)
!c                     core  charges :  zcorss(comp,sublat)
!c             atomic concentrations :  atcon(comp,sublat)
!c
!c     *****************************************************************
!c
!c
      zvalav=zero
      ztotav=zero
!c
      natom = 0
      do in=1,nsublat
         do n=1,komp(in)
            ztotss(n,in) = zvalss(n,in) + zsemss(n,in) + zcorss(n,in)
          zvalav       = zvalav+zvalss(n,in)*atcon(n,in)*numbsub(in)
          ztotav       = ztotav+ztotss(n,in)*atcon(n,in)*numbsub(in)
         enddo
       natom = natom + numbsub(in)
      enddo
      if(iprint.ge.1) then
       write(6,'(/''  Total, valence, core, and semi-core charges'')')
       do in=1,nsublat
         write(6,'(/''   Sublattice '',t25,''='',i3)') in
         do n=1,komp(in)
            write(6,'(''     Component '',t25,''='',i3)') n
            write(6,'(''       total'',t25,''='',f10.4)')               &
     &                                       ztotss(n,in)
            write(6,'(''       valence'',t25,''='',f10.4)')             &
     &                                       zvalss(n,in)
            write(6,'(''       semi-core'',t25,''='',f10.4)')           &
     &                                       zsemss(n,in)
            write(6,'(''       core'',t25,''='',f10.4)')                &
     &                                       zcorss(n,in)
         enddo
       enddo
      end if
      write(6,'(''   Average tot. charge'',t25,''='',f10.5)')           &
     &  ztotav/natom
      write(6,'(''   Average val. charge'',t25,''='',f10.5)')           &
     &  zvalav/natom
!c
      if(sname.eq.istop) then
         call fstop(sname)
      endif
!c
      return
      end
