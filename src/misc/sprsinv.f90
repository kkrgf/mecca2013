
! Suffian Khan May'09

subroutine gsvSPAIsp( &

  Rank, lmRank, MaxClusterSize, nAtoms, memsize,  &
  ClusterSizes, ClusterLists, EquivAtoms,         &
  Entries, InvDiag, Tolerance, MaxIter            &
  
  )

  ! Guidelines for use:

  ! Entries has memory size Rank * lmRank * MaxClusterSize. From block to block 
  ! the storage format is in compressed row order (indices specified in order in
  ! ClusterLists). All intrablock entries are stored and in column-major order.
  ! On output, InvDiag stores the diagonal blocks of the inverse matrix (the
  ! blocks may be rotated but will produce the correct trace).

  use SPAI_Module

  implicit none

  ! Inputs

  integer, intent(in) :: Rank, lmRank, nAtoms
  integer, intent(in) :: MaxClusterSize, memsize

  integer, intent(in) :: ClusterSizes(nAtoms)
  integer, intent(in) :: ClusterLists(MaxClusterSize,nAtoms)
  integer, intent(in) :: EquivAtoms(nAtoms)

  complex*16, intent(in) :: Entries(memsize)

  real*8,  intent(in) :: Tolerance
  integer, intent(in) :: MaxIter

  ! Output

  complex*16, intent(out) :: InvDiag(lmRank,lmRank,nAtoms)

  ! Local Variables

  type(matrixCHW) :: A
  type(matrixDHW) :: B, P

  integer :: lmBlockStride
  integer :: lmBlockRowStride

  real*8 :: EntryTol

  ! TFQMR Variables

  complex*16, allocatable  :: TFQMR_Vecs(:,:)
  integer :: TFQMR_Info(4), CX, CB

  real*8  :: TFQMR_Tol
  integer :: TFQMR_MaxIter

  ! Debugging Variables

  logical, parameter :: DebugFlag   = .false.
  logical, parameter :: PrecondFlag = .true.

  real :: arr(2), xx, s_time

  ! Transient Variables

  integer :: i, j, k, l, m, n, ec
  integer :: iA, iL, jA, jL
  integer, pointer :: iptr(:)
  
  complex*16 :: z
  complex*16, pointer :: zptr(:)

  ! -------------------------------------------------------------------------
  ! Make a copy of input matrix in Harwell-Boeing Format
  ! -------------------------------------------------------------------------

  ! Validate inputs

  if( mod(Rank,lmRank) .ne. 0 ) then
    call fstop("gsvSPAIsp :: matrix rank % kkrsz != 0")
  end if

  if( nAtoms .ne. Rank/lmRank ) then
    call fstop("gsvSPAIsp :: rank / kkrsz != num atoms")
  end if

  if( memsize .ne. Rank*lmRank*MaxClusterSize ) then
    call fstop("gsvSPAIsp :: memory size A != rank * kkrsz * maxclstr")
  end if

  ! Predetermine number of nonzeros entries in matrix A

  lmBlockStride = lmRank * lmRank
  lmBlockRowStride = lmBlockStride * MaxClusterSize

  EntryTol = 1.0d-15 ! max( Tolerance/dble(Rank), 1.0d-15 )

  n = 0
  do iA = 1, nAtoms

    k = (iA-1)*lmBlockRowStride
    l = lmRank*lmRank*ClusterSizes(iA)

    do m = k+1, k+l
    if(  dble(Entries(m)) < -EntryTol .or. EntryTol <  dble(Entries(m)) .or. &
        dimag(Entries(m)) < -EntryTol .or. EntryTol < dimag(Entries(m)) ) then
      n = n + 1
    end if; end do

  end do

  A%rank = Rank
  A%nonzeros = n
 
  ! Allocate room for a fully compressed copy of A

  allocate( &
    A%Entries(A%nonzeros),  &
    A%Indices(A%nonzeros),  &
    A%Columns(A%rank+1),    &
    stat = ec )
  if(ec.ne.0) call fstop("gsvSPAIsp :: insufficient memory :: A")

  ! Make copy of A in Harwell-Boeing format
  ! note: assumes the supplied cluster list is ordered

  if( DebugFlag ) call BeginTiming("Convert A to Harwell-Boeing format")

  allocate( iptr(nAtoms), stat = ec )
  if(ec.ne.0) call fstop("gsvSPAIsp :: insufficient memory :: iptr")
  iptr(1:nAtoms) = 1

  n = 0
  do jA = 1, nAtoms
  do jL = 1, lmRank

    j = (jA-1)*lmRank + jL
    A%Columns(j) = n
  
    k = (jL-1)*lmRank - lmBlockRowStride
    do iA = 1, nAtoms  
    
      k = k + lmBlockRowStride
      l = (iA-1)*lmRank

      if( iptr(iA) > 0 ) then
      if( ClusterLists(iptr(iA),iA) == jA ) then

        m = k + (iptr(iA)-1)*lmBlockStride
       
        do i = m+1, m+lmRank
        if( &
           dble(Entries(i)) < -EntryTol .or. EntryTol <  dble(Entries(i)) .or. &
          dimag(Entries(i)) < -EntryTol .or. EntryTol < dimag(Entries(i)) ) &
        then

          n = n + 1
          A%Entries(n) = Entries(i)
          A%Indices(n) = l+i-m

        end if; end do
        
        if( jL == lmRank ) then

          iptr(iA) = iptr(iA) + 1
          if( iptr(iA) > ClusterSizes(iA) ) then
            iptr(iA) = -1
          end if

        end if

      end if; end if

    end do
  
  end do; end do
  A%Columns(A%rank+1) = n

  deallocate( iptr )

  if( DebugFlag ) call EndTiming()

  ! -------------------------------------------------------------------------
  ! Debugging: Write Matrix to file
  ! -------------------------------------------------------------------------

!  write(80,*) A%Rank, A%Nonzeros

!  do j = 1, A%Rank
!  do k = A%Columns(j)+1, A%Columns(j+1)
!    write(80,*) A%Indices(k), j, real(A%Entries(k)), dimag(A%Entries(k))
!  end do; end do

!  stop

  ! -------------------------------------------------------------------------
  ! Use SPAI to precondition the linear system
  ! -------------------------------------------------------------------------

  if( PrecondFlag ) then

    ! Begin Timer for finding inverse

    if( DebugFlag ) then
      call etime(arr, xx)
      s_time = arr(1)
    end if

    call spai(lmRank, A, P)

    ! Print time for inversion

    if( DebugFlag ) then
      call etime(arr, xx)
      write(6,*) " gsvSPAIsp :: spai complete, timed at ", arr(1)-s_time
    end if

    if( .not. associated(P%ColEntries) ) then
    if( .not. associated(P%ColIndices) ) then
    if( .not. associated(P%ColLengths) ) then

      call fstop("gsvSPAIsp :: invalid output from spai")

    end if; end if; end if

    B%rank = Rank

    allocate( &
      B%ColEntries(B%rank), &
      B%ColIndices(B%rank), &
      B%ColLengths(B%rank), &
      stat = ec )
    if(ec.ne.0) call fstop("gsvSPAIsp :: insufficient memory :: B initial")

    allocate( zptr(rank), stat = ec ) ! deallocate at end of routine
    if(ec.ne.0) call fstop("gsvSPAIsp :: insufficient memory :: zptr")

    do j = 1, rank

      zptr = (0.0d0,0.0d0)
      do k = 1, P%ColLengths(j)

        i = P%ColIndices(j)%p(k)
        z = P%ColEntries(j)%p(k)

        do m = A%Columns(i)+1, A%Columns(i+1)
          l = A%Indices(m)
          zptr(l) = zptr(l) + z*A%Entries(m)
        end do

      end do

      n = 0
      do i = 1, rank
      if(  dble(zptr(i)) < -EntryTol .or. EntryTol <  dble(zptr(i)) .or. &
          dimag(zptr(i)) < -EntryTol .or. EntryTol < dimag(zptr(i)) ) then
        n = n + 1
      end if; end do

      allocate( &
        B%ColEntries(j)%p(n), &
        B%ColIndices(j)%p(n), &
        stat = ec )
      if(ec.ne.0) call fstop("gsvSPAIsp :: insufficient memory :: B extend")
      B%ColLengths(j) = n

      n = 0
      do i = 1, rank
      if(  dble(zptr(i)) < -EntryTol .or. EntryTol <  dble(zptr(i)) .or. &
          dimag(zptr(i)) < -EntryTol .or. EntryTol < dimag(zptr(i)) ) then

        n = n + 1
        B%ColEntries(j)%p(n) = zptr(i)
        B%ColIndices(j)%p(n) = i

      end if; end do

    end do

!    open(unit=80,file="B.mat",status="replace",action="write",iostat=ec)

!    do j = 1, B%rank
!    do i = 1, B%ColLengths(j)
 
!      write(80, "(2I6,2F12.8)") B%ColIndices(j)%p(i), j, B%ColEntries(j)%p(i)
  
!    end do; end do

!    close(unit=80)

    deallocate( A%Entries, A%Indices, A%Columns )

  end if

  ! -------------------------------------------------------------------------
  ! Perform inversion by TFQMR on a column by column basis
  ! -------------------------------------------------------------------------

  ! Begin Timer for finding inverse

  if( DebugFlag ) then
    call etime(arr, xx)
    s_time = arr(1)
  end if

  if( DebugFlag ) call BeginTiming("Perform TFQMR inversion")

  allocate( TFQMR_Vecs(Rank,9), stat = ec )
  if(ec.ne.0) call fstop("gsvSPAIsp :: insufficient memory :: TFQMR_Vecs")

  atomloop: &
  do jA = 1, nAtoms

    ! If this atom is equivalent to a previous one, copy corresponding block
    ! warning: produces correct trace but not correct inverse
   
    do j = 1, jA-1
    if( EquivAtoms(j) == EquivAtoms(jA) ) then

      InvDiag(:,:,jA) = InvDiag(:,:,j)
      cycle atomloop

    end if; end do

    ! Otherwise call TFQMR on each column corresponding to this atom

    do jL = 1, lmRank

      j = (jA-1)*lmRank + jL
      TFQMR_Vecs(:,2) = (0.0d0,0.0d0)
      TFQMR_Vecs(j,2) = (1.0d0,0.0d0)
      TFQMR_Info = 0

      ! debugging: send convergence info to unit 13
      TFQMR_Info(1) = 0 ! 11300
      TFQMR_Tol     = Tolerance
      TFQMR_MaxIter = MaxIter

      n = 0
      do
       
        n = n + 1
        call zutfx( Rank, Rank, TFQMR_MaxIter, TFQMR_Vecs, &
                    TFQMR_Tol, TFQMR_Info )

        if( TFQMR_Info(2) .ne. 1 ) exit

        ! TFQMR callback request: A x --> b

        CX = TFQMR_Info(3)
        CB = TFQMR_Info(4) 
        
        TFQMR_Vecs(:,CB) = (0.0d0,0.0d0)
        if( PrecondFlag ) then

          do j = 1, rank

!           z = TFQMR_Vecs(j,CX)
!           do k = 1, B%ColLengths(j)
!
!             i = B%ColIndices(j)%p(k)
!             TFQMR_Vecs(i,CB) = TFQMR_Vecs(i,CB) + z*B%ColEntries(j)%p(k)
!
!           end do

            call zaxpyi( B%ColLengths(j), TFQMR_Vecs(j,CX),          &
                         B%ColEntries(j)%p(1), B%ColIndices(j)%p(1), &
                         TFQMR_Vecs(1,CB) )

          end do

        else

          do j = 1, rank
 
!           z = TFQMR_Vecs(j,CX)
!           do k = A%Columns(j)+1, A%Columns(j+1)

!             i = A%Indices(k)
!             TFQMR_Vecs(i,CB) = TFQMR_Vecs(i,CB) + z*A%Entries(k)

!           end do

            k = A%Columns(j)
            call zaxpyi( A%Columns(j+1) - k, TFQMR_Vecs(j,CX), &
                         A%Entries(k+1), A%Indices(k+1),       &
                         TFQMR_Vecs(1,CB) )

          end do

        end if

      end do

      select case ( TFQMR_Info(1) )

        case(0); write(*,'(a,i4,a)') &
          "   gsvSPAIsp :: TFQMR :: solution converged in ", n, " iterations"
        case(4); call fstop("gsvSPAIsp :: TFQMR :: failed to converge")

        case default
          call fstop("gsvSPAIsp :: TFQMR :: invalid inputs or breakdown")

      end select

      ! Store diagonal snippet of solution

      if( PrecondFlag ) then

        zptr = (0.0d0,0.0d0)
        do j = 1, rank

!         z = TFQMR_Vecs(j,1)
!         do k = 1, P%ColLengths(j)

!           i = P%ColIndices(j)%p(k)
!           zptr(i) = zptr(i) + z*P%ColEntries(j)%p(k)

!         end do

          call zaxpyi( P%ColLengths(j), TFQMR_Vecs(j,1),           &
                       P%ColEntries(j)%p(1), P%ColIndices(j)%p(1), &
                       zptr(1) )


        end do

        i = (jA-1)*lmRank
        InvDiag(:,jL,jA) = zptr(i+1:i+lmRank)

      else

        i = (jA-1)*lmRank
        InvDiag(:,jL,jA) = TFQMR_Vecs(i+1:i+lmRank,1)

      end if

    end do

  end do atomloop

  deallocate( TFQMR_Vecs )

  if( DebugFlag ) call EndTiming()

  ! Deallocate B and preconditioner M if it exists

  if( PrecondFlag ) then

    do i = 1, rank
      deallocate( B%ColEntries(i)%p, B%ColIndices(i)%p )
      deallocate( P%ColEntries(i)%p, P%ColIndices(i)%p )
    end do

    deallocate( B%ColEntries, B%ColIndices, B%ColLengths )
    deallocate( P%ColEntries, P%ColIndices, P%ColLengths )
    deallocate( zptr )

  else

    deallocate( A%Entries, A%Indices, A%Columns )

  end if

  ! Print time for inversion

  if( DebugFlag ) then
    call etime(arr, xx)
    write(6,'(a,f9.5)') "  gsvSPAIsp :: inversion complete, timed at ", arr(1)-s_time
  end if
 
end subroutine


subroutine gsvSuperLU( &

  Rank, lmRank, MaxClusterSize, nAtoms, memsize,  &
  ClusterSizes, ClusterLists, EquivAtoms,         &
  Entries, InvDiag                                &
  
  )

  ! Guidelines for use:

  ! Entries has memory size Rank * lmRank * MaxClusterSize. From block to block 
  ! the storage format is in compressed row order (indices specified in order in
  ! ClusterLists). All intrablock entries are stored and in column-major order.
  ! On output, InvDiag stores the diagonal blocks of the inverse matrix (the
  ! blocks may be rotated but will produce the correct trace).

  use SPAI_Module

  implicit none

  ! Inputs

  integer, intent(in) :: Rank, lmRank, nAtoms
  integer, intent(in) :: MaxClusterSize, memsize

  integer, intent(in) :: ClusterSizes(nAtoms)
  integer, intent(in) :: ClusterLists(MaxClusterSize,nAtoms)
  integer, intent(in) :: EquivAtoms(nAtoms)

  complex*16, intent(in) :: Entries(memsize)

  ! Output

  complex*16, intent(out) :: InvDiag(lmRank,lmRank,nAtoms)

  ! Local Variables

  type(matrixCHW) :: A

  integer :: myid

  integer :: lmBlockStride
  integer :: lmBlockRowStride

  real*8 :: EntryTol

  ! SuperLU Variables

  integer :: iopt, factors(8), grid(8)
  integer :: nrhs, info, ldb
  real*8  :: berr

  ! Debugging Variables

  logical, parameter :: DebugFlag   = .false.
  real :: arr(2), xx, s_time

  ! Transient Variables

  integer :: i, j, k, l, m, n, ec
  integer :: iA, iL, jA, jL
  integer, pointer :: iptr(:)
 
  real*8 :: re
  complex*16 :: z
  complex*16, pointer :: zptr(:)
  complex*16, pointer :: zptr2(:)

  ! Parallel Matrix Solves

  integer :: Kgroup, numPeers
  logical :: hasKgroup
  common/prllK/ Kgroup, hasKgroup

  ! -------------------------------------------------------------------------
  ! Make a copy of input matrix in Harwell-Boeing Format
  ! -------------------------------------------------------------------------

  ! Validate inputs

  if( mod(Rank,lmRank) .ne. 0 ) then
    call fstop("gsvSuperLU :: matrix rank % kkrsz != 0")
  end if

  if( nAtoms .ne. Rank/lmRank ) then
    call fstop("gsvSuperLU :: rank / kkrsz != num atoms")
  end if

  if( memsize .ne. Rank*lmRank*MaxClusterSize ) then
    call fstop("gsvSuperLU :: memory size A != rank * kkrsz * maxclstr")
  end if

  ! Begin Timer for finding inverse

  if( DebugFlag ) then
    call etime(arr, xx)
    s_time = arr(1)
  end if

  ! Predetermine number of nonzeros entries in matrix A

  lmBlockStride = lmRank * lmRank
  lmBlockRowStride = lmBlockStride * MaxClusterSize

  EntryTol = 1.0d-15 

  n = 0
  do iA = 1, nAtoms

    k = (iA-1)*lmBlockRowStride
    l = lmRank*lmRank*ClusterSizes(iA)

    do m = k+1, k+l
    if(  dble(Entries(m)) < -EntryTol .or. EntryTol <  dble(Entries(m)) .or. &
        dimag(Entries(m)) < -EntryTol .or. EntryTol < dimag(Entries(m)) ) then
      n = n + 1
    end if; end do

  end do

  A%rank = Rank
  A%nonzeros = n

  ! Allocate room for a fully compressed copy of A

  allocate( &
    A%Entries(A%nonzeros),  &
    A%Indices(A%nonzeros),  &
    A%Columns(A%rank+1),    &
    stat = ec )
  if(ec.ne.0) call fstop("gsvSuperLU :: insufficient memory :: A")

  ! Make copy of A in Harwell-Boeing format
  ! note: assumes the supplied cluster list is ordered

  if( DebugFlag ) call BeginTiming("Convert A to Harwell-Boeing format")

  allocate( iptr(nAtoms), stat = ec )
  if(ec.ne.0) call fstop("gsvSuperLU :: insufficient memory :: iptr")
  iptr(1:nAtoms) = 1

  n = 0
  do jA = 1, nAtoms
  do jL = 1, lmRank

    j = (jA-1)*lmRank + jL
    A%Columns(j) = n
  
    k = (jL-1)*lmRank - lmBlockRowStride
    do iA = 1, nAtoms  
    
      k = k + lmBlockRowStride
      l = (iA-1)*lmRank

      if( iptr(iA) > 0 ) then
      if( ClusterLists(iptr(iA),iA) == jA ) then

        m = k + (iptr(iA)-1)*lmBlockStride
       
        do i = m+1, m+lmRank
        if( &
           dble(Entries(i)) < -EntryTol .or. EntryTol <  dble(Entries(i)) .or. &
          dimag(Entries(i)) < -EntryTol .or. EntryTol < dimag(Entries(i)) ) &
        then

          n = n + 1
          A%Entries(n) = Entries(i)
          A%Indices(n) = l+i-m

        end if; end do
        
        if( jL == lmRank ) then

          iptr(iA) = iptr(iA) + 1
          if( iptr(iA) > ClusterSizes(iA) ) then
            iptr(iA) = -1
          end if

        end if

      end if; end if

    end do
  
  end do; end do
  A%Columns(A%rank+1) = n

  deallocate( iptr )

  if( DebugFlag ) call EndTiming()

  ! -------------------------------------------------------------------------
  ! Debugging: Write Matrix to file
  ! -------------------------------------------------------------------------

! open(unit=80,file='matrix',status='replace',action='write')

! write(80,*) A%Rank, A%Nonzeros

! do j = 1, A%Rank
! do k = A%Columns(j)+1, A%Columns(j+1)
!   write(80,*) A%Indices(k), j, A%Entries(k)
! end do; end do

! close(unit=80)
! stop

  ! -------------------------------------------------------------------------
  ! Solve A x = b using an LU decomposition
  ! -------------------------------------------------------------------------

  allocate( zptr(rank), stat = ec )
  if(ec/=0) call fstop("gsvSuperLU :: insufficient memory :: zptr")

  ! Switch to 1-based indexing for c_fortran_zgssv

  do i = 1, A%rank+1
    A%Columns(i) = A%Columns(i)+1
  end do

  ! Factorize A

  ldb = A%Rank; myid = 0
  if( .not. hasKgroup ) then
    write(6,*) "gsvSuperLU :: using sequantial"
    call c_fortran_zgssv(1, A%rank, A%nonzeros, nrhs, A%Entries(1), &
           A%Indices(1), A%Columns(1), zptr, ldb, factors, info)
  else
    call MPI_Comm_Size(Kgroup,numPeers,info)
    call MPI_Comm_Rank(Kgroup,myid,info)
    if(myid == 0) write(6,*) "gsvSuperLU :: using parallel"
    call c_fortran_slugrid(1,Kgroup,1,numPeers,grid)
    call c_fortran_pzgssvx_ABglobal(1, A%rank, A%nonzeros, nrhs,    &
            A%Entries(1), A%Indices(1), A%Columns(1), zptr, ldb, grid, &
            berr, factors, info)
  end if
  if(info/=0) call fstop("gsvSuperLU :: factorization failed")

  if( DebugFlag ) then
    call etime(arr, xx)
    if(myid==0) then
      write(6,*) "gsvSuperLU :: factorization complete, timed at ", &
                 arr(1)-s_time
    end if
    s_time = arr(1)
  end if
 
  ! Solve A x = I, one column at a time

  nrhs = 1
  atomloop: &
  do jA = 1, nAtoms

    ! If this atom is equivalent to a previous one, copy corresponding block
    ! warning: produces correct trace but not correct inverse
   
    do j = 1, jA-1
    if( EquivAtoms(j) == EquivAtoms(jA) ) then

      InvDiag(:,:,jA) = InvDiag(:,:,j)
      cycle atomloop

    end if; end do

    ! Otherwise solve for each column corresponding to this atom

    do jL = 1, lmRank

      j = (jA-1)*lmRank + jL
      zptr(:) = (0.0d0,0.0d0)
      zptr(j) = (1.0d0,0.0d0)
  
      if( .not. hasKgroup ) then
        call c_fortran_zgssv(2, A%rank, A%nonzeros, nrhs, A%Entries(1), &
               A%Indices(1), A%Columns(1), zptr, ldb, factors, info)
      else
        call c_fortran_pzgssvx_ABglobal(3, A%rank, A%nonzeros, nrhs, &
               A%Entries(1), A%Indices(1), A%Columns(1), zptr, ldb,  &
               grid, berr, factors, info)
      end if
      if(info/=0) call fstop("gsvSuperLU :: failed back solve")

      if( DebugFlag .and. myid == 0 ) then

        allocate(zptr2(rank))

        ! slow check.. need to use blas.. recall changed indexing
        zptr2 = (0.0,0.0)
        do j = 1, rank
          z = zptr(j)
          do k = A%Columns(j), A%Columns(j+1)-1
            i = A%Indices(k)
            zptr2(i) = zptr2(i) + z*A%Entries(k)
          end do
        end do

        re = -1.0
        do j = 1, rank
          re = re + dble(zptr2(j))**2 + dimag(zptr2(j))**2
        end do
        write(6,*) "  gsvSuperLU :: residual =", re

        deallocate(zptr2)

      end if

      if(myid == 0) write(*,*) "  gsvSuperLU :: solved for x"

      i = (jA-1)*lmRank
      InvDiag(:,jL,jA) = zptr(i+1:i+lmRank)

    end do

  end do atomloop

  ! Deallocate matrices

  if( .not. hasKgroup ) then
    call c_fortran_zgssv(3, A%rank, A%nonzeros, nrhs, A%Entries(1), &
           A%Indices(1), A%Columns(1), zptr, ldb, factors, info)
  else
    call c_fortran_pzgssvx_ABglobal(4, A%rank, A%nonzeros, nrhs, &
           A%Entries(1), A%Indices(1), A%Columns(1), zptr, ldb,  &
           grid, berr, factors, info)
    call c_fortran_slugrid(2,Kgroup,1,numPeers,grid)
  end if

  deallocate( A%Entries, A%Indices, A%Columns, zptr )

  ! Print time for inversion

  if( DebugFlag ) then
    call etime(arr, xx)
    if(myid == 0) then
      write(6,*) "gsvSuperLU :: inversion complete, timed at ", &
                 arr(1)-s_time
    end if
  end if
 
end subroutine



