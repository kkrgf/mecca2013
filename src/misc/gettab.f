!c
!c     ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      subroutine gettab(nrelv,clight,                                   &
     &                  lmax,kkrsz,nbasis,komp,                         &
     &                  icryst,alat,                                    &
     &                  iswzj,                                          &
     &                  energy,pnrel,                                   &
     &                  vr,ivpoint,ikpoint,                             &
     &                  xr,rr,h,jmt,jws,rmt,rws,                        &
     &                  rmt_true,r_circ,ivar_mtz,                       &
     &                  fcount,weight,rmag,vj,iVP,                      &
     &                  tab,pzz,pzj,pzzck,pzjck,pzzbl,                  &
     &                  cotdl,almat,mtasa,iprint,istop)
!c     =================================================================
!c
      use mecca_constants
      implicit none
!      implicit real*8 (a-h,o-z)
!c
      integer, intent(in) :: nrelv
      real(8), intent(in) :: clight
      integer, intent(in) :: lmax,kkrsz
      integer, intent(in) :: nbasis,komp(nbasis)
      integer, intent(in) :: icryst
      real(8), intent(in) :: alat
      integer, intent(in) :: iswzj
      complex(8), intent(in) :: energy,pnrel
      real(8), intent(in) :: vr(:,:,:)  ! (iprpts,ipcomp,ipsublat)
      integer, intent(in) :: ivpoint(nbasis)
      integer, intent(in) :: ikpoint(:,:)  ! (ipcomp,ipsublat)
      real(8), intent(in) :: xr(:,:,:)  ! (iprpts,ipcomp,ipsublat)
      real(8), intent(in) :: rr(:,:,:)  ! (iprpts,ipcomp,ipsublat)
      real(8), intent(in) :: h(:,:)     ! (ipcomp,ipsublat)
      integer, intent(in) :: jmt(:,:)   ! (ipcomp,ipsublat)
      integer, intent(in) :: jws(:,:)   ! (ipcomp,ipsublat)
      real(8), intent(in) :: rmt(:,:)   ! (ipcomp,ipsublat)
      real(8), intent(in) :: rws(:,:)   ! (ipcomp,ipsublat)
      real(8), intent(in) :: rmt_true(:,:)   ! (ipcomp,nbasis)
      real(8), intent(in) :: r_circ(:,:)     ! (ipcomp,nbasis)
      integer, intent(in) :: ivar_mtz
      integer, intent(in) :: fcount(nbasis)
      real(8), intent(in) :: weight(:)  ! (MNqp)
      real(8), intent(in) :: rmag(:,:,:,:,:) ! (MNqp,MNqp,MNqp,MNF,nbasis)
      real(8), intent(in) :: vj(:,:,:,:,:)   ! (MNqp,MNqp,MNqp,MNF,nbasis)
      integer, intent(in) :: iVP
      complex(8), intent(out) :: tab(:,:,:,:)  ! (ipkkr,ipkkr,ipcomp,ipsublat)
      complex(8), intent(out) :: pzz(:,:,:,:)  ! (ipkkr,ipkkr,ipcomp,ipsublat)
      complex(8), intent(out) :: pzj(:,:,:,:)  ! (ipkkr,ipkkr,ipcomp,ipsublat)
      complex(8), intent(out) :: pzzck(:,:,:,:)  ! (ipkkr,ipkkr,ipcomp,ipsublat)
      complex(8), intent(out) :: pzjck(:,:,:,:)  ! (ipkkr,ipkkr,ipcomp,ipsublat)
      complex(8), intent(out) :: pzzbl(:,:,:,:,:)! (ipkkr,ipkkr,ipcomp,ipcomp,ipsublat)
      complex(8), intent(out) :: cotdl(:,:,:)    ! (iplmax+1,ipsublat,ipcomp)
      complex(8), intent(out) :: almat(:,:,:)    ! (iplmax+1,ipcomp,ipsublat)
      integer, intent(in) :: mtasa, iprint
      character(10), intent(in) :: istop
!c
!c     ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!      include 'lmax.h'
!      include 'mkkrcpa.h'
!      include 'imp_inp.h'
!c     ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!c
!c     ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
      character(:), parameter :: sname='gettab'
!c     ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!c
!c
!CALAM      real*8 xstart(ipcomp,ipsublat)

!c
      complex(8) :: prel,pr

      ! .. now need to store wave fn. to get cross integrals zlzl
      ! between different components at the same site. these terms
      ! are included in the expression for the bloch spectral fn
      ! for alloys given by phys. rev. b 21, 3222

      complex(8) :: zlab(size(xr,1),lmax+1,size(xr,2))
      complex(8) :: zzab(size(xr,1)),izzab(size(xr,1)),pzzab
      complex(8) :: bjl(lmax+1),bnl(lmax+1)
      complex(8) :: djl(lmax+1),dnl(lmax+1)
      complex(8), external :: ylag_cmplx

      integer :: nsub,nk,i,j,l,ll,lmin,m,n,nka,nkb,nk1,nk2,nsubd
      integer :: iR1,ized,ic,iex
      logical :: isordered
!      integer, external :: g_zed

!c
!c     ****************************************************************
!c     set up t-matrix, zz, zj for each component and  sub-lattice
!c     ****************************************************************
!c
      if(iprint.ge.1) then
         write(6,'('' gettab:  nrelv,lmax='',2i5)') nrelv,lmax
         write(6,'('' gettab: icryst,alat='',i5,d12.4)') icryst,alat
         write(6,'('' gettab: energy,pnrel='',4d12.4)') energy,pnrel
      endif

!               write(700,*) ''
!               write(701,*) ''
!               write(702,*) ''
!               write(703,*) ''
!c
      pzzbl = 0

      do nsub=1,nbasis
         do nk=1,komp(nsub)
            if(ikpoint(nk,nsub).eq.0 .or. ivpoint(nsub).eq.0) then

!c              -------------------------------------------------------
               call grint(nrelv,clight,                                 &
     &                    lmax,kkrsz,                                   &
     &                    icryst,alat,                                  &
     &                    energy,pnrel,                                 &
     &                    iswzj,                                        &
     &                    vr(1,nk,nsub),xr(1,nk,nsub),rr(1,nk,nsub),    &
     &                    h(nk,nsub),                                   &
     &                    jmt(nk,nsub),jws(nk,nsub),                    &
     &                    rmt(nk,nsub),rws(nk,nsub),                    &
     &                    rmt_true(nk,nsub),r_circ(nk,nsub),ivar_mtz,   &
     &                    fcount(nsub),weight,rmag(1,1,1,1,nsub),       &
     &                    vj(1,1,1,1,nsub),iVP,                         &
     &                    zlab(1,1,nk),                                 &
     &                    tab(1,1,nk,nsub),                             &
     &                    pzz(1,1,nk,nsub),pzj(1,1,nk,nsub),            &
     &                    pzzck(1,1,nk,nsub),pzjck(1,1,nk,nsub),        &
     &                    cotdl(1,nsub,nk),almat(1,nk,nsub),mtasa,      &
     &                    iprint,istop)

!c              -------------------------------------------------------
!CDEBUG
!CDEBUG              call symmtrx(tab(1,1,nk,nsub),ipkkr,kkrsz,1.d-12)
!CDEBUG              call symmtrx(pzz(1,1,nk,nsub),ipkkr,kkrsz,1.d-12)
!CDEBUG              call symmtrx(pzj(1,1,nk,nsub),ipkkr,kkrsz,1.d-12)
!CDEBUG              call symmtrx(pzzck(1,1,nk,nsub),ipkkr,kkrsz,1.d-12)
!CDEBUG              call symmtrx(pzjck(1,1,nk,nsub),ipkkr,kkrsz,1.d-12)
!CDEBUG
            else
               nka=ikpoint(nk,nsub)
               nsubd=ivpoint(nsub)
               do j=1,kkrsz
                  do i=1,kkrsz
                     tab(i,j,nk,nsub)=tab(i,j,nka,nsubd)
                     pzz(i,j,nk,nsub)=pzz(i,j,nka,nsubd)
                     pzj(i,j,nk,nsub)=pzj(i,j,nka,nsubd)
                     pzzck(i,j,nk,nsub)=pzzck(i,j,nka,nsubd)
                     pzjck(i,j,nk,nsub)=pzjck(i,j,nka,nsubd)
                  enddo
               enddo
            endif
         enddo

         ! suffian
         ! Need mixed component entries of pzz for a calculation
         ! of the Bloch Spectral Fn A(k,E)

         isordered = .true.
         do n = 1, nbasis
           if( komp(n) > 1 ) isordered = .false.
         end do

         if( ( trim(istop) == 'BSF_CALC' .or.                           &
     &         trim(istop) == 'DOSBSF_CALC' .or.                        &
     &         trim(istop) == 'AKEBZ_CALC' .or.                         &
     &         trim(istop) == 'FERMI_CALC' )                            &
     &    .and. .not. isordered ) then

           do nka = 1, komp(nsub)
           do nkb = 1, komp(nsub)

             ! which grid is finer?
             if( rws(nka,nsub) < rws(nkb,nsub) ) then
               nk1 = nka; nk2 = nkb
             else
               nk1 = nkb; nk2 = nka
             end if

             ! momentum needed to extend finer wave fn beyond MT
             ! this needs to be identical to choice in scalar.f
             ized=-vr(1,nk1,nsub)/2 + 0.5
!             ized = g_zed(vr(:,nk1,nsub),rr(:,nk1,nsub))
             if(ized.lt.1 .and. nrelv.le.0) then
               prel = pnrel
             else
               prel = pnrel*sqrt(dcmplx(1.d0,0.d0)+energy/clight**2)
             endif

             ! for all l
             do l = 1, lmax+1


               ! integrate from 0 --> R1
               do i = 1, jmt(nk1,nsub)
                 zzab(i) = zlab(i,l,nk1) *                              &
     &           ylag_cmplx(rr(i,nk1,nsub),rr(1,nk2,nsub),zlab(1,l,nk2),&
     &                 0, 3, jws(nk2,nsub), iex )
               end do
               call cqxpup(1,zzab,jmt(nk1,nsub),xr(1,nk1,nsub),izzab)
               pzzab = izzab(jmt(nk1,nsub))

               ! integrate R1 --> R1', where R1' > R1 and R1' on coarse grid
               ! recall 'zlab' is actually r*Z_l(r).
               iR1 = 1
               do while( rr(iR1,nk2,nsub) < rmt(nk1,nsub) )
                 iR1 = iR1 + 1
               end do
               if( iR1 < jmt(nk2,nsub) ) then
                 pzzab = pzzab + zzab(jmt(nk1,nsub)) *                  &
     &             (rr(iR1,nk2,nsub) - rmt(nk1,nsub))
               end if

               ! integrate from R1' --> R2
               if( iR1 < jmt(nk2,nsub) ) then

                 do i = 1, jmt(nk2,nsub)-iR1+1

                   lmin = max(2,l)
                   ! extend zl1 beyond its MT sphere
                   pr = prel*rr(iR1+i-1,nk2,nsub)
                   call ricbes(lmin,pr,bjl,bnl,djl,dnl)
                   zzab(i) = bjl(l)*(dcmplx(1.d0,0.d0)/                 &
     &               tab(l**2,l**2,nk1,nsub)/pnrel-dcmplx(0.d0,1.d0))   &
     &               + bnl(l)

                   zzab(i) = zzab(i)*zlab(iR1+i-1,l,nk2)

                 end do

                 call cqxpup(1,zzab,jmt(nk2,nsub)-iR1+1,                &
     &                  xr(iR1,nk2,nsub),izzab)
                 pzzab = pzzab + izzab(jmt(nk2,nsub)-iR1+1)

               end if

               ! include prefactor of -1/pi
               pzzab = -pzzab/pi

               ! distribute integral(zal*zbl') across corresponding entries
               ! of matrix 'pzzbl' matrix

               do m = 1, 2*(l-1) + 1

                 ll = (l-1)**2 + m
                 pzzbl(ll,ll,nka,nkb,nsub) = pzzab

               end do

             end do

           end do; end do

         else
           pzzbl(:,:,1,1,nsub) = pzz(:,:,1,nsub)
         end if

      enddo

      if( iprint .ge. 1 ) then
        do nsub=1,nbasis
          do ic=1,komp(nsub)
            call wrtmtx(tab(1,1,ic,nsub),kkrsz,istop)
          enddo
        enddo
      endif
!c
!c     ================================================================
      if(istop.eq.sname) then
        call fstop(sname)
      endif
!c
      return
      end

