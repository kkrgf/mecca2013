!c     ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      subroutine chgden1(greenint,greenlast,                            &
     &                  delef,nspfac,is,                                &
     &                  rho,chgold,                                     &
     &                  corden,semcor,                                  &
     &                  rr,xr,jmt,jws,komp,omegmt,                      &
     &                  qvalmt,qtotmt,qrms,itscf,ivar_mtz,iVP,          &
     &                  r_circ,fcount,weight,rmag,vj,rmt_true,          &
     &                  iprint,istop)
!c     ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
!c
      use mecca_constants
      implicit real*8 (a-h,o-z)
!c
!c +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!      include 'mkkrcpa.h'
!      include 'imp_inp.h'
!c +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!c
      character sname*10
      character istop*10
!c
      integer jmt(ipcomp),jws(ipcomp)
      integer  fcount
      integer  ia

      real*8     rmt_true(ipcomp)
      real*8     r_circ(ipcomp)
      real*8     weight(MNqp)
      real*8     rmag(MNqp,MNqp,MNqp,MNF)
      real*8     vj(MNqp,MNqp,MNqp,MNF)

      real*8 rho(iprpts,ipcomp)
      real*8 corden(iprpts,ipcomp)
      real*8 rr(iprpts,ipcomp)
      real*8 semcor(iprpts,ipcomp)
      real*8 chgold(iprpts,ipcomp)
      real*8 w1(iprpts)
      real*8 w2(iprpts)
      real*8 xr(iprpts,ipcomp)
      real*8 qvalmt(ipcomp)
      real*8 qtotmt(ipcomp)
      real*8 qrms(ipcomp)
!c
      complex*16 greenint(iprpts,ipcomp)
      complex*16 greenlast(iprpts,ipcomp)
      complex*16 wi(iprpts)
      complex*16 delef

      real*8     rrs(iprpts)
      real*8     frhov(iprpts)
      real*8     frhot(iprpts)
      real*8     f(iprpts)
      real*8     rhoint(iprpts)
      real*8     VPInt(2)
      real*8     ylag,rrjmt
!c
!c +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
      parameter (sname='chgden')
!c +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!c
      piinv=one/pi
      call zeroout(qrms,ipcomp)
      do ik=1,komp
!c        =============================================================
!c        calc valence charge density penultimate point on e-grid......
!CAA      do j=1,jws(ik)
!C        For VP Integration
         do j=1,iprpts
            rho(j,ik)=-piinv*(nspfac*rr(j,ik)**2*dimag(greenint(j,ik)))
         enddo
         if(iprint.ge.1) then
            write(6,'(//)')
            write(6,'('' chgden:  komp,jws '',2i5)') komp,jws(ik)
            write(6,'('' chgden:  delef '',2d12.5)') delef
!CAA      do j=1,jws(ik)
!C        For VP Integration
            do j=1,iprpts
              greenint(j,ik)=-piinv*nspfac*rr(j,ik)**2*greenint(j,ik)
            enddo
            call cqxpup(1,greenint(1,ik),jmt(ik),xr(1,ik),wi)
            write(6,'('' chgden: greenint to jmt'',                     &
     &            2d16.8)') wi(jmt(ik))
            call qexpup(1,rho(1,ik),jmt(ik),xr(1,ik),w1)
            write(6,'('' chgden: zmt without greenlast'',               &
     &            d16.8)') w1(jmt(ik))
            write(6,'(//)')
         endif
!c        =============================================================
!c        add contribution from last iterval on energy grid............
!CAA      do j=1,jws(ik)
!C        For VP Integration
         do j=1,iprpts
            rho(j,ik)=rho(j,ik)                                         &
     &         -piinv*(nspfac*rr(j,ik)**2*dimag(greenlast(j,ik)*delef))
         enddo

!c******************************************************************
!c   Sometimes we have low accuracy ==>
!c    small negative numbers can be generated for rho(*,*)
!c
!CAA      do j=1,jmt(ik)
!C        for VP Integration
         do j=1,iprpts
          rho(j,ik)=max(1.d-24,rho(j,ik))
         enddo

         do j = 1,iprpts
           rho(j,ik)= rho(j,ik)+ corden(j,ik)+semcor(j,ik)
         enddo
!c*********************************************************************
        if (iVP.eq.0.and.ivar_mtz.ge.3)then
                     ! only done if VP integ. is done for E_f calc.

            rrjmt= rr(jmt(ik),ik)
            xrmt = log(rmt_true(ik))
            nlag = 3
!C*********************************************************************
!CAA  ******  One can modify the form of the following function f and
!CAA  ******             check the final result
!C*********************************************************************
                do j = 1,iprpts
                 rrs(j)=rr(j,ik)
                 f(j)=(1.0d0 -rrjmt/rrs(j))**2.0d0
                enddo
!C*********************************************************************
               ia=0
             call isopar_integ(ia,rmt_true(ik),rrs,f,r_circ(ik),        &
     &                         fcount,weight,rmag,vj,VPInt)

            VP=VPInt(2)
             call isopar_integ(ia,rmt_true(ik),rrs,f,rrjmt,             &
     &                         fcount,weight,rmag,vj,VPInt)

             a_denom =  4.0d0*pi*(VP - VPInt(2))
!C*********************************************************************
               ia=2
          call isopar_integ(ia,rmt_true(ik),rrs,rho(1,ik),r_circ(ik),   &
     &                       fcount,weight,rmag,vj,VPInt)

          call qexpup(1,rho(1,ik),jmt(ik),xr(1,ik),rhoint)
           drhov = ylag(xrmt,xr(1,ik),rhoint,0,nlag,jmt(ik),iex)

           a_numer =  (drhov + VPInt(2))  - rhoint(jmt(ik))

           do j=jmt(ik)+1,iprpts
               rho(j,ik) = rho(j,ik) - (a_numer/a_denom)*f(j)
           enddo
!C*********************************************************************
         if(itscf.eq.1)then
               ia=2
          call isopar_integ(ia,rmt_true(ik),rrs,chgold(1,ik),r_circ(ik),&
     &                       fcount,weight,rmag,vj,VPInt)

          call qexpup(1,chgold(1,ik),jmt(ik),xr(1,ik),rhoint)
           drhov = ylag(xrmt,xr(1,ik),rhoint,0,nlag,jmt(ik),iex)

           a_numer =  (drhov + VPInt(2)) - rhoint(jmt(ik))

           do j=jmt(ik)+1,iprpts
               chgold(j,ik) = chgold(j,ik) - (a_numer/a_denom)*f(j)
           enddo

         endif
!C*********************************************************************
        endif
!C*********************************************************************
!CAA      do j=1,jws(ik)
!C        For VP Integration

            do j = 1,iprpts
             frhot(j)=rho(j,ik)
             frhov(j)=rho(j,ik)- corden(j,ik)- semcor(j,ik)
            enddo

            call qexpup(1,frhot,jmt(ik),xr(1,ik),w1)

            call qexpup(1,frhov,jmt(ik),xr(1,ik),w2)

       if(iVP.eq.0.and.ivar_mtz.ge.3)then
          ! For Ordered systems only AND Full VP+Variational MT-zero (VP)
            xrmt = log(rmt_true(ik))
            nlag = 3
!c         =============================================================
               ia=2
            call isopar_integ(ia,rmt_true(ik),rrs,frhot,r_circ(ik),     &
     &                       fcount,weight,rmag,vj,VPInt)

!c         =============================================================
!C         Calculate the total charge
            qtotmt(ik) = ylag(xrmt,xr(1,ik),w1,0,nlag,jmt(ik),iex)      &
     &                   + VPInt(2)

!c         =============================================================
!C         Calculate the valence charge
            qvalmt(ik) = w2(jmt(ik))

!C            write(75,121)w1(jmt(ik)),qtotmt(ik),w2(jmt(ik))
!C121         format(1x,3F18.9)
!c      =============================================================
       else
          ! For Disord systems only OR Variational MT-zero (ivar_mtz=0,1,2)
!c      =============================================================
!C********   Calculate the total charge
            qtotmt(ik)=w1(jmt(ik))
!C********   Calculate the valence charge
            qvalmt(ik)=w2(jmt(ik))
       endif
!c      =============================================================

         if(iprint.ge.1) then
!c           writing the valence charge density........................
            write(6,'('' chgden: valence charge density: comp='',       &
     &                                           i3)')  ik
            do i=1,jmt(ik),50
               write(6,'('' i,r,rh(r),n(r)'',i5,f14.6,2d16.8)')         &
     &               i,rr(i,ik),frhov(i),frhov(i)/(rr(i,ik)**2)
            enddo
            write(6,'('' end of charge density'')')
         endif

!c        =============================================================
!c        Calculate rms between old and new charge densities.......

         do j=1,iprpts
            w2(j)= (rho(j,ik)-chgold(j,ik))**2/rr(j,ik)
         enddo
         call simpun(xr(1,ik),w2,jmt(ik),1,w1,iprpts)
         qrms(ik)  =sqrt(w1(jmt(ik))/(four*pi*omegmt))
      enddo
!c
!c     ================================================================
      if(istop.eq.sname) then
        call fstop(sname)
      endif
!c
      return
      end
