       subroutine rhomtcorr(nsublat,numbsub,komp,Smt,                   &
     &                      ndrpts,rr,xr,jws,                           &
     &                      ndcomp,atcon,ndsubl,rho,nspin,              &
     &                      omegint,rhoint,dz,                          &
     &                      deltE,dV,                                   &
     &                      iflag,                                      &
     &                      totrho,wrhotint,                            &
     &                      nchbmx,ach                                  &
     &                     )
!c
!c        rhoint(1) = rhomtc
!c        rhoint(2) = rhomtc(up)-rhomtc(dn), if nspin.eq.2 AND iflag.eq.1
!c
!c        rhomtc -- electron density, NOT charge density,
!c        i.e. rhomtc is always positive!
!c
!c
       implicit none
       integer nsublat,numbsub(nsublat)
       integer komp(nsublat)
       real*8  Smt(nsublat)
       integer ndrpts
       real*8  rr(ndrpts,nsublat),xr(ndrpts,nsublat)
       integer jws(nsublat)
       integer ndcomp
       real*8  atcon(ndcomp,nsublat)
       integer ndsubl
       real*8  rho(ndrpts,ndcomp,ndsubl,*)
       integer nspin
       real*8  omegint,rhoint(*),rhomtc
       real*8  dz(nsublat)
       real*8 deltE,dV(nsublat)
       integer iflag
!c
       real*8  totrho(*),wrhotint(*)        !  working arrays
       integer nchbmx                      !  number of interpol.coeff.
       real*8 ach(1:nchbmx+1,1:nsublat)    !  working array
!c
       character*10 sname/'rhomtcorr'/
       real*8 zero
       parameter (zero=0.d0)
       real*8 pi
       parameter (pi=3.14159265358979323844d0)
       real*8 pi4
       parameter (pi4=4.d0*pi)
       real*8 pi4d3
       parameter (pi4d3=4.d0*pi/3.d0)

       integer nsub,jwsn,i,ik,iex
       integer jmin,jd,nwsmt,nlag
       real*8  omegmt,volume,rws,areaws
       real*8 qintrmad,rmt,rmt1,rhomt,rhomt1,xrmt

       real*8 hik,rik,rhoik,sum,x0,w
       real*8 b0,b1,b2,A0,A1,A2,AR,AS

       real*8 ylag
       external ylag

       omegmt = zero
       volume = zero
       do nsub = 1,nsublat
      rws = rr(jws(nsub),nsub)
      volume = volume + numbsub(nsub)*rws**3
      omegmt = omegmt + numbsub(nsub)*Smt(nsub)**3
       end do

       if(volume-omegmt.lt.1.d-8*volume) then
       RETURN     ! do nothing; it is not ASA-case
       end if

!c       omegmt = pi4d3*omegmt
!c       volume = pi4d3*volume
!c       omegint = volume-omegmt
!c
       omegint = (volume-omegmt)*pi4d3
       dz(1:nsublat) = zero

!CDEBUG       write(*,*) volume,omegmt,omegint,' volume,omegmt,omegint'

      if(iflag.eq.0) then   ! rhomtc -- averaged rho between R_MT and R_ASA
!c
       rhomtc = zero
       if(omegint.le.1.d-7*volume) return

       if(nchbmx.gt.0) ach(1:nchbmx+1,1:nsublat) = zero

       qintrmad       = zero
       do nsub=1,nsublat
      jwsn = jws(nsub)
      rws = rr(jwsn,nsub)
      rmt = Smt(nsub)                   ! rmt -- MT-radius (NOT WS!)
      if(abs(rws-rmt).lt.1.d-7*rws) cycle
      jd = 0
      do i=jwsn-1,1,-1
       if(rr(i,nsub).le.rmt) then
        if(rr(i,nsub)-rmt.lt.rr(i+1,nsub)-rmt) then
         jmin = i-1
         jd = 1
        else
         jmin = i-1
         jd = 2
        end if
        exit
       end if
      end do
      if(jd.lt.1) then
       write(*,*) ' jd=',jd,' nsub=',nsub
       write(*,*) ' rmt=',rmt
       write(*,*) ' rws=',rws
       call fstop(sname//':: RMT is too small ???')
      end if
      nwsmt  = jwsn-jmin

      nlag = 3

      totrho(1:jwsn) = zero
      do ik=1,komp(nsub)
       totrho(1:jwsn) = totrho(1:jwsn) +                                &
     &                  atcon(ik,nsub)*rho(1:jwsn,ik,nsub,1)
      end do
      if(nspin.gt.1) then
       do ik=1,komp(nsub)
        totrho(1:jwsn) = totrho(1:jwsn) +                               &
     &                  atcon(ik,nsub)*rho(1:jwsn,ik,nsub,2)
       end do
      end if

      if(nchbmx.gt.0) then
       wrhotint(jmin:jwsn)=totrho(jmin:jwsn)/rr(jmin:jwsn,nsub)**2
       wrhotint(jmin:jwsn)=wrhotint(jmin:jwsn)/pi4

       x0 = 0.5d0*(rws+rmt)
       w  = 0.5d0*(rws-rmt)

       do i=1,nchbmx+1
        sum = zero
        do ik=1,nchbmx+1
         hik = pi*(ik-0.5d0)/(nchbmx+1)
         rik = x0 + w*cos(hik)
         rhoik = ylag(rik,rr(jmin,nsub),wrhotint(jmin),                 &
     &                               0,nlag,nwsmt+1,iex)
         sum = sum + rhoik*cos(hik*(i-1))
        end do
        ach(i,nsub) = sum*2.d0/(nchbmx+1)
       end do

      end if

      call qexpup(1,totrho,jwsn,xr(1,nsub),wrhotint)

      xrmt = log(rmt)
      dz(nsub) = wrhotint(jwsn) -                                       &
     &    ylag(xrmt,xr(jmin,nsub),wrhotint(jmin),                       &
     &                                0,nlag,nwsmt+1,iex)               &
     &              + totrho(1)*rr(1,nsub)/3.d0

      qintrmad = qintrmad+dz(nsub)*numbsub(nsub)

       enddo

       rhomtc = qintrmad/omegint

       rhoint(1) = rhomtc

!c
!c now we can calculate
!c    Integral{from_S_to_R | d3 r ( Integral{from_S_to_R | d3 u (
!c     (rho(r) - rhomtc)*( 2*rhomtc + (rho(u)-rhomtc)) / |r-u|  )
!CDEBUG                           ?????
!CDEBUG   SIGN ???? between 2*rhomtc and (rho-rhomtc)?
!CDEBUG
!CDEBUG
!c                                          } ) }
!c      rho is approximated by chebyshev polinoms with
!c      coefficients "ach(*,nsub)":
!c
!c      rho(r) = -ach(1,nsub)/2+sum{1,nchbmx|ach(i,nsub)*T_{i-1}(x)},
!c         here r = x0 + w*x
!c
!c      b0,b1,b2,... and A0,A1,A2,... -- polinom.coeff., i.e.
!c
!c        rho(r) - rhomtc = rhomtc*( b0*T0 + b1*T1 + b2*T2 + ...) =
!c                        = rhomtc*( A0 + A1*r + A2*r*r + ...)
!c
       if(nchbmx.gt.0) then
      deltE = zero
      do nsub=1,nsublat
       jwsn = jws(nsub)
       rws = rr(jwsn,nsub)
       rmt = Smt(nsub)                   ! rmt -- MT-radius (NOT WS!)
       if(abs(rws-rmt).lt.1.d-7*rws) cycle
       x0 = 0.5d0*(rws+rmt)
       w  = 0.5d0*(rws-rmt)
       b1 = zero
       b2 = zero
       b0 = (0.5d0*ach(1,nsub) - rhomtc)/rhomtc
       if(nchbmx.gt.1) b1 = ach(2,nsub)/rhomtc
       if(nchbmx.gt.2) b2 = ach(3,nsub)/rhomtc

       A0 = (b0-b2) - x0/w*b1 + 2.d0*b2*(x0/w)**2
       A1 =  b1/w - 4.d0*(x0/w)*b2/w
       A2 =  2.d0*b2/w**2

       AR = rws**2*(A0/2.d0+A1*rws/3.d0+A2*rws**2/4.d0)
       AS = rmt**3*(A0/3.d0+A1*rmt/4.d0+A2*rmt**2/5.d0)

       deltE = deltE -                                                  &
     &                           (2.d0+A0)*AS*(rws**2-rmt**2)/2.d0 ! 1/r*r^2
       deltE = deltE +                                                  &
     &                 ((2.d0+A0)*AR - A1*AS)*(rws**3-rmt**3)/3.d0 ! r^0*r^2
       deltE = deltE +                                                  &
     &                        (A1*AR - A2*AS)*(rws**4-rmt**4)/4.d0 ! r^1*r^2
       deltE = deltE +                                                  &
     &            (A2*AR - A0*(2.d0+A0)/6.d0)*(rws**5-rmt**5)/5.d0 ! r^2*r^2
       deltE = deltE -                                                  &
     &      (A0*A1/6.d0 + A1*(2.d0+A0)/12.d0)*(rws**6-rmt**6)/6.d0 ! r^3*r^2
       deltE = deltE -                                                  &
     &  (A2*(2.d0+A0)/20.d0+A1*A1/12.d0+A0*A2/6.d0)*                    &
     &                                        (rws**7-rmt**7)/7.d0 ! r^4*r^2
       deltE = deltE -                                                  &
     &                     (2.d0/15.d0)*A1*A2*(rws**8-rmt**8)/8.d0 ! r^5*r^2
       deltE = deltE -                                                  &
     &                            A2*A2/20.d0*(rws**9-rmt**9)/9.d0 ! r^6*r^2

       dV(nsub) = A0*(rws**2-rmt**2)/2.d0                               &
     &            + A1*(rws**3-rmt**3)/3.d0                             &
     &            + A2*(rws**4-rmt**4)/4.d0

      end do
      deltE = deltE*(pi4*rhomtc)**2
      dV(1:nsublat) = dV(1:nsublat)*(pi4*rhomtc)*2.d0

       end if

!c
!c

!c
      else if(iflag.eq.1) then   ! rhomtc -- averaged rho for r=R_ASA
!c
      areaws = zero
      rhomtc = zero
      rhoint(1:nspin) = zero
       do nsub=1,nsublat
        jwsn = jws(nsub)
        rws = rr(jwsn,nsub)
        areaws = areaws + numbsub(nsub)*rws**2
        do ik=1,komp(nsub)
         rhomtc = rhomtc+ (numbsub(nsub)*atcon(ik,nsub))*               &
     &                           rho(jwsn,ik,nsub,1)
         if(nspin.gt.1) then
          rhoint(2) = rhoint(2)+ (numbsub(nsub)*atcon(ik,nsub))*        &
     &                           rho(jwsn,ik,nsub,2)
         end if
        end do
       end do

      rhoint(1) = rhomtc+rhoint(2)
      if(nspin.gt.1) then
       rhoint(2) = rhomtc-rhoint(2)
      end if

      areaws = 4.d0*pi*areaws
      rhoint(1:nspin) = rhoint(1:nspin)/areaws
!c
      end if

      return
      end
