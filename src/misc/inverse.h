!C   *                                                                *
!C   *   Parameter file for Multi-sublattice/component KKR-CPA:       *
!C   *     to inverse matrix                                          *
!C   *                                                                *

      integer inverse
      parameter (inverse=1)
      integer nblock
      parameter (nblock=16)
!c      parameter (nblock=32)
!c      parameter (nblock=1)
      integer nlev
      parameter (nlev=4)

!C   *                                                                *
!C   *    inverse = 0  -- Gauss elimination of DDJ                    *
!C   *    inverse = 1  -- LAPACK (LU+partial pivoting)                *
!C   *    inverse = 2  -- Strassen algorithm                          *
!C   *                                                                *
!C   *    nblock       -- blocksize for LAPACK zgetri.f               *
!C   *    nlev         -- number of recursion levels for Strassen     *


!c----------------------------------------------------------------------- !----
      real*8 tolinv
      parameter (tolinv=0.5d-7)
!c
!c          tolinv is a "zero-treshold" for screened (1-G*t)-matrix eleme !nts
!c              and tolerance for iterative methods
!c----------------------------------------------------------------------- !----

!c----------------------------------------------------------------------- !----
      integer MemoryLimit
!c      parameter (MemoryLimit=150)
      parameter (MemoryLimit=0)
!c
!c          MemoryLimit is a limit for "big" memory allocation in
!c                         ILU-inversion (if it is equal to 0, than
!c                         all available memory can be used)
!c
!c----------------------------------------------------------------------- !----

!c----------------------------------------------------------------------- !----
      real*8 tolCut               !   (dosparse: if SPARSE or Rcut.ne.0)
      parameter (tolCut=0.1d0*tolinv)
!c
!c          tolCut is a "zero-treshold" for struct.const.matrix elements
!c                                           (without screening)
!c----------------------------------------------------------------------- !----

!c----------------------------------------------------------------------- !----
      real*8 prlim                !    (imethod=0,2,4)
      parameter (prlim=0.2d0)
!c
!c          prlim (in D.U.) decides whether Cluster Approach can be used
!c          if(dimag(pdu).gt.prlim) --  Cluster Approach
!c
!c !!! Be careful for imethod=0 and Rcut.ne.0  !!!
!c
!c----------------------------------------------------------------------- !----

!c----------------------------------------------------------------------- !----
      real*8 prSLU                ! (sparse technique only)
      parameter (prSLU=10.0d0)
!c      parameter (prSLU=0.1d0)
!c      parameter (prSLU=0.0d0)
!c
!c          prSLU (in D.U.) decides whether SuperLU is used near the real !axis
!c          if(dimag(pdu).gt.prSLU) -- iterative method
!c                             else -- SuperLU
!c----------------------------------------------------------------------- !----

!c----------------------------------------------------------------------- !----
      real*8 prPRC                ! ( iterations only )
      parameter (prPRC=0.3d0)
!c      parameter (prPRC=10.3d0)
!c
!c          prPRC (in D.U.) defines where the preconditioner is used
!c          if(dimag(pdu).gt.prPRC) --  without precond.
!c----------------------------------------------------------------------- !----
