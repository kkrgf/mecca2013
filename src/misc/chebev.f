!
!  chebev.f
!  meccalib
!
!  Created on 1/14/07.
!c
!c
      real*8 function chebev(a,b,c,m,x)
!c=======================================================================
!c     function chebev evaluates f(x) from Chebyshev coefficients.
!c     (based on Numerical Recipes 2nd Edition, Section 5.8)
!c       NO checks for x not in the proper range
!c=======================================================================
      implicit none
      integer m
      real*8 a, b, x, c(m)
      integer j
      real*8 d, dd, sv, y, y2
      d=0.d0
      dd=0.d0
      y = (2.d0*x-a-b)/(b-a)
      y2=y+y
      do j=m,2,-1
        sv=d
        d=y2*d-dd+c(j)
        dd=sv
      end do
      chebev=y*d-dd+0.5d0*c(1)
      return
      end

