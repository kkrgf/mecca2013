
! Implementation of SPAI by suffian khan, mar'09
! See SIAM J. Sci. Comput., 18:838-853, 1997

module SPAI

  use sparse

  implicit none

  ! Interface to get preconditioner

  public :: ini_spai
  public :: BeginTiming
  public :: EndTiming
  public :: PrintTimings

  ! Type Definitions

  private

  type :: indexset
    integer :: order, memsize
    integer, pointer :: Indices(:)
    integer, pointer :: Subsets(:)
    real*8,  pointer :: Rho(:)
  end type

  type :: z2ptr
    complex*16, dimension(:,:), pointer :: p
  end type

  ! Local Variables
 
  complex*16, allocatable :: x(:)  ! approximate solution
  complex*16, allocatable :: r(:)  ! residual
  real*8 :: residualnorm

  type(indexset)   :: setI, setJ
  integer, pointer :: thisI(:), thisJ(:)
  integer :: sizetI, sizetJ

  type(indexset) :: setJp, setJa
  integer, pointer :: bitmapI(:), bitmapJ(:)
  integer :: bitmask, log2_int_bits

  complex*16,  pointer     :: thisQR(:,:)
  type(z2ptr), allocatable :: listQR(:)   ! QR-decomposition (partitioned)
  type(z1ptr), allocatable :: listTau(:)  ! tau factors used by LAPack
  complex*16,  allocatable :: buffer(:)   ! working buffer for LAPack
  integer :: buffersize

  integer :: it, maxiter, maxplus, memblock
  real*8  :: delta, sumRho, meanRho

  real*8, parameter :: singtol = 10.0d-10

  complex*16 :: zdotc
  external zdotc

  ! Debugging Variables

  logical, parameter :: DebugFlag  = .false.
  integer, parameter :: NumTimers  = 25
  real,    parameter :: StopTime   = 999999.0
  real,    parameter :: StopColumn = 999999

  real :: Timers(2,NumTimers) = 0.0
  character(len=125) :: TimerDesc(NumTimers) = ""
  integer :: ActiveTimer = -1
 
  ! Recycled Variables

  integer, pointer :: iptr(:)

  real*8  :: re, im, density
  integer :: h, i, j, k, l, m, n
  integer :: nnzc, ic, ir, ie
  integer :: asI, asJ, dasI, dasJ
  integer :: offset, length, info, ec
  logical :: found_match
  complex*16 :: z

  complex*16 :: zdotui
  external zdotui

contains

  subroutine ini_spai(blockrank, A, B)

  integer, intent(in) :: blockrank
  type(matrixCHW), intent(inout) :: A
  type(matrixDHW), intent(out) :: B

  ! -------------------------------------------------------------------------
  ! Generate random sparse matrix A -- debugging only
  ! -------------------------------------------------------------------------

! A%rank = 10000; density = 0.005

  ! Spread entries across columns uniformly

! allocate( A%Columns(A%rank+1) )
! nnzc = ceiling(density*A%rank)
! do i = 1, A%rank+1
!   A%Columns(i) = (i-1)*nnzc
! end do
! A%nonzeros = nnzc*A%rank

  ! Choose uniformly random indices within a column

! allocate( A%Indices(A%nonzeros) )
! do ic = 1, A%rank

!   offset = (ic-1)*nnzc
!   A%Indices(offset+1) = ic

!   do ie = 2, nnzc

!     ! ith random pick from n-i objects
!     call random_number( re )
!     m = ceiling(re*(A%rank-ie+1))

!     ! choose corresponding index
!     n = 0
!     do while( n+1 < ie .and. A%Indices(offset+n+1) <= m )
!       m = m+1; n = n+1;
!     end do
!     ir = m

!     ! insert so to preserve order
!     n = ie
!     do while( 2 <= n .and. ir < A%Indices(offset+n-1) )
!       A%Indices(offset+n) = A%Indices(offset+n-1)
!       n = n-1
!     end do
!     A%Indices(offset+n) = ir

!   end do 

! end do

  ! Fill in random complex numbers for nonzero entries

! allocate( A%Entries(A%nonzeros) )
! do i = 1, A%nonzeros

!   call random_number( re )
!   call random_number( im )
!   A%Entries(i) = cmplx(re, im)

! end do

  ! -------------------------------------------------------------------------
  ! Load Sparse Matrix from file -- debugging only
  ! -------------------------------------------------------------------------

! open(unit=1,file="Mecca.mat",status="old",action="read",iostat=ec)

! ! Read in rank and nnz

! read(1,*) A%rank, A%nonzeros

! allocate( A%Columns(A%rank+1) )
! allocate( A%Indices(A%nonzeros) )
! allocate( A%Entries(A%nonzeros) )

! ! Read in column pointers

! do i = 1, A%rank+1
!   read(1,*) A%Columns(i)
! end do

! ! Read in row indices and complex entries

! do i = 1, A%nonzeros
!   read(1,*) A%Indices(i), A%Entries(i)
! end do

! close(unit=1)

  ! -------------------------------------------------------------------------
  ! Build ancillary information on Sparse Matrix
  ! -------------------------------------------------------------------------

  ! Determine the row structure

  if( DebugFlag ) call BeginTiming("Build row structure")

  allocate( A%RowIndices(A%nonzeros) )
  allocate( A%Rows(A%rank+1) )
 
  allocate( iptr(A%rank) )
  iptr(1:A%rank) = A%Columns(1:A%rank) + 1

  n = 0
  do i = 1, A%rank; A%Rows(i) = n
  do j = 1, A%rank

      if( iptr(j) > 0 .and. A%Indices( iptr(j) ) == i ) then

        n = n + 1
        A%RowIndices(n) = j

        iptr(j) = iptr(j) + 1
        if( iptr(j) > A%Columns(j+1) ) then
          iptr(j) = -1
        end if

      end if

  end do; end do
  A%Rows( A%rank+1 ) = A%nonzeros

  deallocate( iptr )

  if( DebugFlag ) call EndTiming()

  ! Compute column norms of this matrix

  if( DebugFlag ) call BeginTiming("Compute column norms")

  allocate( A%ColumnSqNorms(A%rank) )

  do j = 1, A%rank
   
    offset = A%Columns(j)
    length = A%Columns(j+1) - offset

    re = 0.0
    do n = 1, length
      re = re +  real(A%Entries(offset+n))**2 
      re = re + aimag(A%Entries(offset+n))**2
    end do

    A%ColumnSqNorms(j) = re
    
  end do

  if( DebugFlag ) call EndTiming()

  ! -------------------------------------------------------------------------
  ! Write Matrix A to file -- debugging only
  ! -------------------------------------------------------------------------

! open(unit=1,file="A.matrix",status="replace",action="write",iostat=ec)

! write(1,*) ""
! write(1,*) " ---- Compressed Column Format"
! write(1,*) ""
! do ic = 1, A%rank
! do ie = 1, A%Columns(ic+1)-A%Columns(ic)

!   write(1, "(2I6,2F8.3)") &
!         ic, A%Indices( A%Columns(ic) + ie ), &
!             A%Entries( A%Columns(ic) + ie )

! end do; end do
! write(1,*) ""

! write(1,*) " ---- Compressed Row Format"
! write(1,*) ""
! do ir = 1, A%rank
! 
!   offset = A%Rows(ir)
!   length = A%Rows(ir+1) - offset
!   
!   write(1,"(I4,A)",advance='no') ir, ": "
!   do n = 1, length
!     write(1,"(I4)",advance='no') A%RowIndices(offset+n)
!   end do
!   write(1,*) ""
!   
! end do
  
!  write(1,*) ""
!  write(1,*) " ---- Sparsity Pattern"
!  n = 1
!  do ir = 1, A%rank; write(1,*) ""
!  do ic = 1, A%rank
!  
!    if( A%RowIndices(n) == ic ) then
!      write(1,"(A)",advance='no') " x"
!      n = n + 1
!    else
!      write(1,"(A)",advance='no') " -"
!    end if
!    
!  end do; end do

!  close(unit=1)

  ! -------------------------------------------------------------------------
  ! SParse Approximate Inverse Algorithm
  ! -------------------------------------------------------------------------

  maxiter  = 5
  maxplus  = 2*blockrank ! reduction in sparsity of x per iteration
  delta    = 0.6       ! tolerance

  memblock = 100

  ! Allocate all lifetime variables

  B%rank = A%rank
  allocate( &
    B%ColEntries(B%rank), &
    B%ColIndices(B%rank), &
    B%ColLengths(B%rank), &
    stat = ec )
  if(ec.ne.0) call fstop("spai :: insufficient memory :: B initial")

  allocate( x(A%rank), r(A%rank), stat = ec )
  if(ec.ne.0) call fstop("spai :: insufficient memory :: x, r")

  setI%memsize = memblock
  setJ%memsize = memblock
  allocate( &
    setI%Indices(setI%memsize), &
    setJ%Indices(setJ%memsize), &
    setI%Subsets(maxiter+1), &
    setJ%Subsets(maxiter+1), &
    stat = ec )
  if(ec.ne.0) call fstop("spai :: insufficient memory :: setI/J")
  setI%order = 0; setJ%order = 0

  setJp%memsize = memblock
  allocate( &
    setJp%Indices(setJp%memsize), &
    setJp%Rho(setJp%memsize),     &
    stat = ec )
  if(ec.ne.0) call fstop("spai :: insufficient memory :: setJp")
  setJp%order = 0

  setJa%memsize = memblock
  allocate( setJa%Indices(setJa%memsize), stat = ec )
  if(ec.ne.0) call fstop("spai :: insufficient memory :: setJa")
  setJa%order = 0

  allocate( &
    bitmapI( ceiling(float(A%rank)/bit_size(n)) ), &
    bitmapJ( ceiling(float(A%rank)/bit_size(n)) ), &
    stat = ec )
  if(ec.ne.0) call fstop("spai :: insufficient memory :: bitmapI/J")

  if( bit_size(n) == 32 ) then
    log2_int_bits = 5; bitmask = 31 ! hex: 1F
  end if

  if( bit_size(n) == 64 ) then
    log2_int_bits = 6; bitmask = 63 ! hex: 3F
  end if

  buffersize = maxplus
  allocate( &
    listQR(maxiter),  &
    listTau(maxiter), &
    buffer(buffersize), &
    stat = ec )
  if(ec.ne.0) call fstop("spai :: insufficient memory :: lists/buffer")
  do i = 1, maxiter
    listQR(i)%p => null(); listTau(i)%p => null()
  end do

  ! For each column of A

  do ic = 1, A%rank

    ! Set sparsity pattern J_1st to diagonal and compute I_1st

    offset = A%Columns(ic)
    n = A%Columns(ic+1) - offset
    if( n > setI%memsize ) then
      call ExtendIntArray( setI%Indices, setI%memsize, n )
    end if

    setI%Indices(1:n) = A%Indices( offset+1:offset+n )
    setJ%Indices(1:1) = ic

    setI%order = n; setJ%order = 1
    setI%Subsets(1) = 0; setI%Subsets(2) = n
    setJ%Subsets(1) = 0; setJ%Subsets(2) = 1

    setJa%order = 0
    bitmapJ = 0

    ! Initialize memory bitmap for set I to include current rows

    bitmapI = 0
    do i = 1, setI%order

      l = setI%Indices(i) - 1
      n = ishft(l, -log2_int_bits) + 1 ! upper bits specity index
      m = iand(l, bitmask)             ! lower bits specify bit

      bitmapI(n) = ibset(bitmapI(n),m)

    end do

    ! Until residual is within tolerance or max iterations reached
    ! .. exit condition inside loop

    it = 1; residualnorm = huge(residualnorm)
    do
      
      if( DebugFlag ) write(*,"(A,I6,A,I3)") "col=", ic, ", iter=", it

      thisI => setI%Indices( setI%Subsets(it)+1: )
      thisJ => setJ%Indices( setJ%Subsets(it)+1: )
      sizetI = setI%Subsets(it+1) - setI%Subsets(it)
      sizetJ = setJ%Subsets(it+1) - setJ%Subsets(it)

      ! Extend QR by J_this and fill with A(I_full, J_this)

      if( DebugFlag ) call BeginTiming("Extend QR and fill with A(I,Jthis)")

      allocate( listQR(it)%p(setI%order, sizetJ), stat = ec )
      if( ec .ne. 0 ) call fstop("spai :: insufficient memory :: listQR(it)")
      thisQR => listQR(it)%p

      thisQR = (0.0,0.0)
      do i = 1, setI%order
      do j = 1, sizetJ

        offset = A%Columns( thisJ(j) )
        length = A%Columns( thisJ(j)+1 ) - offset

        n = 1; m = length
        do while( m - n > 4 )

          h = (m+n)/2
          if( A%Indices(offset + h) < setI%Indices(i) ) then
            n = h + 1
          else
            m = h
          end if

        end do

        do h = n, m
        if( A%Indices(offset + h) == setI%Indices(i) ) then

          thisQR(i,j) = A%Entries(offset + h)
          exit

        end if; end do

      end do; end do

      if( DebugFlag ) call EndTiming()

      ! Compute Q_last^H Z(1:|I_full|-|I_this|,1:|J_this|)

      if( DebugFlag ) call BeginTiming("Multiply by Q_last^H")

      asI = 0; asJ = 0
      do i = 1, it-1

        dasI = setI%Subsets(i+1) - setI%Subsets(i)
        dasJ = setJ%Subsets(i+1) - setJ%Subsets(i)
        asI = asI + dasI

        call zunmqr( 'L','C', asI - asJ, sizetJ, dasJ, listQR(i)%p(asJ+1,1), &
                     asI, listTau(i)%p, thisQR(asJ+1,1), setI%order, buffer, &
                     buffersize, info )

        asJ = asJ + dasJ

      end do

      if( DebugFlag ) call EndTiming()

      ! Find QR decomposition of Z(|J_full|-|J_this|:|I_full|,1:|J_this|)
      
      if( DebugFlag ) call BeginTiming("Factor into QR form")

      allocate( listTau(it)%p(sizetJ), stat = ec )
      if(ec.ne.0) call fstop("spai :: insufficient memory :: listTau(it)")

      call zgeqrf( setI%order - asJ, sizetJ, thisQR(asJ+1,1), setI%order, &
                   listTau(it)%p, buffer, buffersize, info )

      if( DebugFlag ) call EndTiming()

      ! Initialize x to respective column of identity

      if( DebugFlag ) call BeginTiming("Set x to basis vector e")

      do i = 1, setI%order
      if( setI%Indices(i) == ic ) then
        x(i) = (1.0,0.0)
      else
        x(i) = (0.0,0.0)
      end if; end do

      if( DebugFlag ) call EndTiming()

      ! Compute x' = Q^H x

      if( DebugFlag ) call BeginTiming("Multiply by Q^H")

      asI = 0; asJ = 0
      do i = 1, it

        dasI = setI%Subsets(i+1) - setI%Subsets(i)
        dasJ = setJ%Subsets(i+1) - setJ%Subsets(i)
        asI = asI + dasI

        call zunmqr( 'L','C', asI - asJ, 1, dasJ, listQR(i)%p(asJ+1,1), &
                     asI, listTau(i)%p, x(asJ+1), setI%order, buffer,   &
                     buffersize, info )

        asJ = asJ + dasJ

      end do

      if( DebugFlag ) call EndTiming()

      ! Compute x' = R^-1 x by backsolve

      if( DebugFlag ) call BeginTiming("Multiply by R^-1")

      do i = setJ%order, 1, -1

        m = it; n = sizetJ
        do j = setJ%order, i+1, -1

          x(i) = x(i) - listQR(m)%p(i,n)*x(j)
          n = n-1

          if( n == 0 ) then
            m = m - 1; n = setJ%Subsets(m+1) - setJ%Subsets(m)
          end if

        end do

        z = listQR(m)%p(i,n)
        if( -singtol <  dble(z) .and.  dble(z) < singtol .and. &
            -singtol < dimag(z) .and. dimag(z) < singtol ) then

          call fstop("spai :: A close to singular ?")

        end if

        x(i) = x(i)/z

      end do

      if( DebugFlag ) call EndTiming()

      ! Compute residual r = A x - e

      if( DebugFlag ) call BeginTiming("Set residual = A x - e")

      r = (0.0,0.0)
      do k = 1, setJ%order

        j = setJ%Indices(k)
        do l = 1, A%Columns(j+1) - A%Columns(j)

          i = A%Indices( A%Columns(j) + l )
          r(i) = r(i) + A%Entries( A%Columns(j) + l ) * x(k)

        end do

      end do

      r(ic) = r(ic) + (-1.0,0.0)

      if( DebugFlag ) call EndTiming()

      ! If residual less than tolerance or max iterations reached, stop

      if( DebugFlag ) call BeginTiming("Compute residual norm")

      residualnorm = 0.0
      do n = 1, setI%order
        i = setI%Indices(n)
        residualnorm = residualnorm + real(r(i))  * real(r(i)) 
        residualnorm = residualnorm + aimag(r(i)) * aimag(r(i)) 
      end do

      n = ishft(ic-1, -log2_int_bits) + 1 
      m = iand(ic-1, bitmask)             

      if( .not. btest( bitmapI(n), m ) ) then
        residualnorm = residualnorm + 1.0
      end if

      residualnorm = dsqrt(residualnorm)

      if( DebugFlag ) call EndTiming()

!      write(6,*) 'residual = ', residualnorm
      if( residualnorm < delta .or. it == maxiter ) exit

      ! Update Jall = {j| contains i where r(i) != 0 } for new rows
      ! Follows an approach similar to Barnard's implementation

      if( DebugFlag ) call BeginTiming("Update list Jall")

      do i = 1, sizetI

        offset = A%Rows( thisI(i) )
        length = A%Rows( thisI(i)+1 ) - A%Rows( thisI(i) )

        l = setJa%order
        do j = 1, length
          
          k = A%RowIndices(offset+j) - 1
          n = ishft(k, -log2_int_bits) + 1 ! upper bits specity index
          m = iand(k, bitmask)             ! lower bits specify bit

          if( .not. btest(bitmapJ(n),m) ) then
           
            bitmapJ(n) = ibset(bitmapJ(n),m)
            l = l + 1

            if( l > setJa%memsize ) then
              call ExtendIntArray( setJa%Indices, setJa%memsize, l )
            end if

            setJa%Indices(l) = k+1

          end if

        end do
        setJa%order = l
                         
      end do

      if( DebugFlag ) call EndTiming()

      ! Set Jplus = Jall - J

      if( DebugFlag ) call BeginTiming("Set Jplus = Jall - J")

      n = 0
      do j = 1, setJa%order

        found_match = .false.
        do l = 1, setJ%order
        if( setJa%Indices(j) == setJ%Indices(l) ) then
          found_match = .true.; exit 
        end if; end do
        
        if( .not. found_match ) then

          n = n + 1
          if( n > setJp%memsize ) then
            m = setJp%memsize; call ExtendIntArray( setJp%Indices, m, n )
            m = setJp%memsize; call ExtendRealArray( setJp%Rho, m, n )
            setJp%memsize = m
          end if

          setJp%Indices(n) = setJa%Indices(j)

        end if

      end do
      setJp%order = n

      if( DebugFlag ) call EndTiming()

      ! Compute rho (measure of reduction in residual on inclusion of column j)
      ! for each column in Jplus as well as the mean rho

      if( DebugFlag ) call BeginTiming("Compute rho for j in Jplus")

      sumRho = 0.0
      do n = 1, setJp%order

        j = setJp%Indices(n)
        offset = A%Columns(j)
        length = A%Columns(j+1) - A%Columns(j)

!       z = (0.0,0.0)
!       do m = offset+1, offset+length
!         z = z + dconjg(r(A%Indices(m)))*A%Entries(m)
!       end do

        z = zdotui(length,A%Entries(offset+1),A%Indices(offset+1),r(1))

        re = real(z); im = dimag(z)
        setJp%Rho(n) = -(re*re + im*im)/A%ColumnSqNorms(j)
        sumRho = sumRho + setJp%Rho(n)

      end do
      meanRho = sumRho/setJp%order

      if( DebugFlag ) call EndTiming()

      ! Cut all columns above mean rho in Jplus

      if( DebugFlag ) call BeginTiming("Delete j below mean rho in Jplus")

      n = 0
      do j = 1, setJp%order
      if( setJp%Rho(j) < 0.99*meanRho ) then

          n = n + 1
          setJp%Indices(n) = setJp%Indices(j)
          setJp%Rho(n) = setJp%Rho(j)

      end if; end do
      setJp%order = n

      if( DebugFlag ) call EndTiming()

      ! Then cull 'maxplus' best columns from Jplus

      if( DebugFlag ) call BeginTiming("Cull 'maxplus' best columns in Jplus")

      if( setJp%order > maxplus ) then

        do k = 1, maxplus
          
          n = k
          do l = k+1, setJp%order
            if( setJp%Rho(l) < setJp%Rho(n) ) n = l
          end do
          
          call SwapInt( setJp%Indices(k), setJp%Indices(n) )
          call SwapReal(  setJp%Rho(k), setJp%Rho(n) )

        end do
        setJp%order = maxplus

      end if

      if( DebugFlag ) call EndTiming()

      ! As consistency check, make sure Jplus is not empty

      if( setJp%order == 0 ) then
        write(*,*) "consistency check has failed: setJp%order != 0"
        write(*,*) "work stopped on ic=", ic, "it=", it
        stop
      end if

      ! Augment set J by set Jplus

      if( DebugFlag ) call BeginTiming("Augment set J by Jplus")

      n = setJp%order
      if( setJ%order + n > setJ%memsize ) then
        call ExtendIntArray( setJ%Indices, setJ%memsize, setJ%order+n )
      end if
      
      setJ%Indices(setJ%order+1:setJ%order+n) = setJp%Indices(1:n)
      setJ%order = setJ%order + n
      setJ%Subsets(it+2) = setJ%order
      
      if( DebugFlag ) call EndTiming()

      ! Augment set I by indices

      if( DebugFlag ) call BeginTiming("Augment set I")

      do j = 1, setJp%order

        offset = A%Columns( setJp%Indices(j) )
        length = A%Columns( setJp%Indices(j)+1 ) - offset

        do k = 1, length

          l = A%Indices(offset+k) - 1
          n = ishft(l, -log2_int_bits) + 1 ! upper bits specity index
          m = iand(l, bitmask)             ! lower bits specify bit

          if( .not. btest(bitmapI(n),m) ) then
           
            bitmapI(n) = ibset(bitmapI(n),m)
            setI%order = setI%order + 1

            if( setI%order > setI%memsize ) then
              call ExtendIntArray( setI%Indices, setI%memsize, setI%order )
            end if

            setI%Indices( setI%order ) = l+1

          end if

        end do

      end do
      setI%Subsets(it+2) = setI%order

      if( DebugFlag ) call EndTiming()

      ! Next Iteration

      it = it + 1

      ! If we're debugging, stop once we've gathered enough timing data

      if( DebugFlag ) call CheckTime(ic)

    end do

    ! Store approximate solution x in preconditioner

    n = setJ%order
    allocate( &
      B%ColEntries(ic)%p(n), &
      B%ColIndices(ic)%p(n), &
      stat = ec )
    if(ec.ne.0) call fstop("spai :: insufficient memory :: extend B")
    B%ColLengths(ic) = n

    B%ColEntries(ic)%p(1:n) = x(1:n)
    B%ColIndices(ic)%p(1:n) = setJ%Indices(1:n)

    ! Release memory for next column

    do i = 1, maxiter
    if( associated( listQR(i)%p ) ) then
      deallocate( listQR(i)%p, listTau(i)%p )
      listQR(i)%p => null(); listTau(i)%p => null()
    end if; end do

  end do

  ! Release all working memory

  deallocate( x, r, buffer )
  deallocate( listQR, listTau )

  deallocate( setI%Indices, setI%Subsets )
  deallocate( setJ%Indices, setJ%Subsets )

  deallocate( setJp%Indices, setJp%Rho )
  deallocate( setJa%Indices )

  deallocate( bitmapI, bitmapJ )

  deallocate( A%Rows, A%RowIndices )
  deallocate( A%ColumnSqNorms )


  ! Write matrix B to file

!  open(unit=25,file="B.mat",status="replace",action="write",iostat=ec)
 
!  do ic = 1, B%rank
!  do ie = 1, B%ColLengths(ic)
!
!    write(25, "(2I6,2F8.3)") B%ColIndices(ic)%p(ie), ic, &
!                             B%ColEntries(ic)%p(ie)
!
!  end do; end do
 
!  close(unit=25) 

  ! Print Timings

!  if( DebugFlag ) call PrintTimings()

  end subroutine ini_spai

  subroutine ExtendIntArray( array, currentsize, min_newsize )

    implicit none
    integer :: currentsize, min_newsize, ex
    integer, pointer :: array(:), storage(:)

    ex = 0
    do while( ex < min_newsize-currentsize )
      ex = ex + memblock
    end do

    allocate( storage(currentsize + ex), stat = ec )
    if(ec.ne.0) call fstop("spai :: insufficient memory :: extend array")
    storage(1:currentsize) = array(1:currentsize)
    deallocate(array)
    array => storage

    currentsize = currentsize + ex

  end subroutine

  subroutine ExtendRealArray( array, currentsize, min_newsize )

    implicit none
    integer :: currentsize, min_newsize, ex
    real*8, pointer :: array(:), storage(:)

    ex = 0
    do while( ex < min_newsize-currentsize )
      ex = ex + memblock
    end do

    allocate( storage(currentsize + ex), stat = ec )
    if(ec.ne.0) call fstop("spai :: insufficient memory :: extend array")
    storage(1:currentsize) = array(1:currentsize)
    deallocate(array)
    array => storage

    currentsize = currentsize + ex

  end subroutine

  subroutine ExtendComplexArray( array, currentsize, min_newsize )

    implicit none
    integer :: currentsize, min_newsize, ex
    complex*16, pointer :: array(:), storage(:)

    ex = 0
    do while( ex < min_newsize-currentsize )
      ex = ex + memblock
    end do

    allocate( storage(currentsize + ex), stat = ec )
    if(ec.ne.0) call fstop("spai :: insufficient memory :: extend array")
    storage(1:currentsize) = array(1:currentsize)
    deallocate(array)
    array => storage

    currentsize = currentsize + ex

  end subroutine

  subroutine SwapInt(i, j)
    integer :: i, j, k
    k = i; i = j; j = k
  end subroutine

  subroutine SwapReal(a, b)
    real*8 :: a, b, c
    c = a; a = b; b = c
  end subroutine

  ! Write full R matrix to be loaded in Mathematica
  ! Can only be called just after QR factorization
  subroutine DumpMatrixR()
    integer :: i, j, m, n

    write(22,*) 'R MATRIX'
    write(22,*) '{'

    do i = 1, setJ%order

      write(22,*) '{'
      m = 1; n = 0
      do j = 1, setJ%order
         
        n = n + 1
        if( j > setJ%Subsets(m+1) ) then
          m = m + 1; n = 1
        end if

        if( i <= j ) then
          write(22,'(ES15.6E3,A,ES15.6E3)', advance='no') &
            dble(listQR(m)%p(i,n)), ' + I * ', aimag(listQR(m)%p(i,n))
        else
          write(22,'(A)', advance='no') "0.000 + I * 0.000"
        end if

        if( j /= setJ%order ) then
          write(22,*) ','
        else
          write(22,*) ''
        end if

      end do

      if( i /= setJ%order ) then
        write(22,*) '},'
      else
        write(22,*) '}'
      end if

    end do

    write(22,*) '}'
    write(22,*) ''

  end subroutine

  ! Write out Householder vectors to be loaded in Mathematica
  ! Can only be called just after QR factorization
  subroutine DumpVectorsH()
    integer :: i, j, m, n

    m = 1; n = 0;
    do j = 1, setJ%order

      write(23,*) 'H VECTOR'
      write(23,*) '{'

      n = n + 1
      if( j > setJ%Subsets(m+1) ) then
        m = m + 1; n = 1
      end if

      do i = 1, setI%order

        if( 1 <= i .and. i <= j-1) then

          write(23,'(A)',advance='no') '{0.000 + I * 0.000}'

        else if (i == j) then

          write(23,'(A)',advance='no') '{1.000 + I * 0.000}'

        else if (j+1 <= i .and. i <= setI%Subsets(m+1)) then

          write(23,'(A,ES15.6E3,A,ES15.6E3,A)', advance='no') &
          '{',dble(listQR(m)%p(i,n)),' + I * ',aimag(listQR(m)%p(i,n)),'}'

        else if (setI%Subsets(m+1)+1 <= i .and. i <= setI%order) then

          write(23,'(A)',advance='no') '{0.000 + I * 0.000}'

        end if

        if( i /= setI%order ) then
          write(23,*) ','
        else
          write(23,*) ''
        end if

      end do

      write(23,*) '}'

    end do

  end subroutine

  subroutine BeginTiming(message)
    character(len=*) :: message
    real :: arr(2), xx
    integer :: n
  
    do n = 1, NumTimers
      if( Trim(TimerDesc(n)) == Trim(message) ) exit
      if( Trim(TimerDesc(n)) == "" ) then
        TimerDesc(n) = message; exit
      end if
    end do

    if( n > NumTimers ) then
      write(*,*) "Insufficient timers for debugging."
      stop
    end if

    ActiveTimer = n

    call etime(arr, xx)
    Timers(2,ActiveTimer) = arr(1)

  end subroutine

  subroutine EndTiming()
    real :: arr(2), xx

    call etime(arr, xx)
    Timers(1,ActiveTimer) = Timers(1,ActiveTimer)+arr(1)-Timers(2,ActiveTimer)

  end subroutine

  subroutine PrintTimings()
    integer :: i

    do i = 1, NumTimers
    if( Trim(TimerDesc(i)) /= "" ) then

      write(24,'(A,F10.4,A,A)') &
        "time:", Timers(1,i), ", desc: ", Trim(TimerDesc(i))

    end if; end do

  end subroutine

  subroutine CheckTime(ic)
    integer :: i, ic
    real :: time

    time = 0.0
    do i = 1, NumTimers
    if( Trim(TimerDesc(i)) /= "" ) then
      time = time + Timers(1,i)
    end if; end do

    if( time > StopTime .or. ic >= StopColumn ) then
      write(24,"(A,I6)") "Program halted at col = ", ic
      call PrintTimings()
      stop
    end if

  end subroutine

subroutine gsvSPAIsp( &

  Rank, lmRank, MaxClusterSize, nAtoms, memsize,  &
  ClusterSizes, ClusterLists, EquivAtoms,         &
  Entries, InvDiag, Tolerance, MaxIter            &
  
  )

  ! Guidelines for use:

  ! Entries has memory size Rank * lmRank * MaxClusterSize. From block to block 
  ! the storage format is in compressed row order (indices specified in order in
  ! ClusterLists). All intrablock entries are stored and in column-major order.
  ! On output, InvDiag stores the diagonal blocks of the inverse matrix (the
  ! blocks may be rotated but will produce the correct trace).

  implicit none

  ! Inputs

  integer, intent(in) :: Rank, lmRank, nAtoms
  integer, intent(in) :: MaxClusterSize, memsize

  integer, intent(in) :: ClusterSizes(nAtoms)
  integer, intent(in) :: ClusterLists(MaxClusterSize,nAtoms)
  integer, intent(in) :: EquivAtoms(nAtoms)

  complex*16, intent(in) :: Entries(memsize)

  real*8,  intent(in) :: Tolerance
  integer, intent(in) :: MaxIter

  ! Output

  complex*16, intent(out) :: InvDiag(lmRank,lmRank,nAtoms)

  ! Local Variables

  type(matrixCHW) :: A
  type(matrixDHW) :: B, P

  integer :: lmBlockStride
  integer :: lmBlockRowStride

  real*8 :: EntryTol

  ! TFQMR Variables

  complex*16, allocatable  :: TFQMR_Vecs(:,:)
  integer :: TFQMR_Info(4), CX, CB

  real*8  :: TFQMR_Tol
  integer :: TFQMR_MaxIter

  ! Debugging Variables

  logical, parameter :: DebugFlag   = .false.
  logical, parameter :: PrecondFlag = .true.

  real :: arr(2), xx, s_time

  ! Transient Variables

  integer :: i, j, k, l, m, n, ec
  integer :: iA, jA, jL
  integer, pointer :: iptr(:)
  
  complex*16 :: z
  complex*16, pointer :: zptr(:)

  ! -------------------------------------------------------------------------
  ! Make a copy of input matrix in Harwell-Boeing Format
  ! -------------------------------------------------------------------------

  ! Validate inputs

  if( mod(Rank,lmRank) .ne. 0 ) then
    call fstop("gsvSPAIsp :: matrix rank % kkrsz != 0")
  end if

  if( nAtoms .ne. Rank/lmRank ) then
    call fstop("gsvSPAIsp :: rank / kkrsz != num atoms")
  end if

  if( memsize .ne. Rank*lmRank*MaxClusterSize ) then
    call fstop("gsvSPAIsp :: memory size A != rank * kkrsz * maxclstr")
  end if

  ! Predetermine number of nonzeros entries in matrix A

  lmBlockStride = lmRank * lmRank
  lmBlockRowStride = lmBlockStride * MaxClusterSize

  EntryTol = 1.0d-15 ! max( Tolerance/dble(Rank), 1.0d-15 )

  n = 0
  do iA = 1, nAtoms

    k = (iA-1)*lmBlockRowStride
    l = lmRank*lmRank*ClusterSizes(iA)

    do m = k+1, k+l
    if(  dble(Entries(m)) < -EntryTol .or. EntryTol <  dble(Entries(m)) .or. &
        dimag(Entries(m)) < -EntryTol .or. EntryTol < dimag(Entries(m)) ) then
      n = n + 1
    end if; end do

  end do

  A%rank = Rank
  A%nonzeros = n
 
  ! Allocate room for a fully compressed copy of A

  allocate( &
    A%Entries(A%nonzeros),  &
    A%Indices(A%nonzeros),  &
    A%Columns(A%rank+1),    &
    stat = ec )
  if(ec.ne.0) call fstop("gsvSPAIsp :: insufficient memory :: A")

  ! Make copy of A in Harwell-Boeing format
  ! note: assumes the supplied cluster list is ordered

  if( DebugFlag ) call BeginTiming("Convert A to Harwell-Boeing format")

  allocate( iptr(nAtoms), stat = ec )
  if(ec.ne.0) call fstop("gsvSPAIsp :: insufficient memory :: iptr")
  iptr(1:nAtoms) = 1

  n = 0
  do jA = 1, nAtoms
  do jL = 1, lmRank

    j = (jA-1)*lmRank + jL
    A%Columns(j) = n
  
    k = (jL-1)*lmRank - lmBlockRowStride
    do iA = 1, nAtoms  
    
      k = k + lmBlockRowStride
      l = (iA-1)*lmRank

      if( iptr(iA) > 0 ) then
      if( ClusterLists(iptr(iA),iA) == jA ) then

        m = k + (iptr(iA)-1)*lmBlockStride
       
        do i = m+1, m+lmRank
        if( &
           dble(Entries(i)) < -EntryTol .or. EntryTol <  dble(Entries(i)) .or. &
          dimag(Entries(i)) < -EntryTol .or. EntryTol < dimag(Entries(i)) ) &
        then

          n = n + 1
          A%Entries(n) = Entries(i)
          A%Indices(n) = l+i-m

        end if; end do
        
        if( jL == lmRank ) then

          iptr(iA) = iptr(iA) + 1
          if( iptr(iA) > ClusterSizes(iA) ) then
            iptr(iA) = -1
          end if

        end if

      end if; end if

    end do
  
  end do; end do
  A%Columns(A%rank+1) = n

  deallocate( iptr )

  if( DebugFlag ) call EndTiming()

  ! -------------------------------------------------------------------------
  ! Debugging: Write Matrix to file
  ! -------------------------------------------------------------------------

!  write(80,*) A%Rank, A%Nonzeros

!  do j = 1, A%Rank
!  do k = A%Columns(j)+1, A%Columns(j+1)
!    write(80,*) A%Indices(k), j, real(A%Entries(k)), dimag(A%Entries(k))
!  end do; end do

!  stop

  ! -------------------------------------------------------------------------
  ! Use SPAI to precondition the linear system
  ! -------------------------------------------------------------------------

  if( PrecondFlag ) then

    ! Begin Timer for finding inverse

    if( DebugFlag ) then
      call etime(arr, xx)
      s_time = arr(1)
    end if

    call ini_spai(lmRank, A, P)

    ! Print time for inversion

    if( DebugFlag ) then
      call etime(arr, xx)
      write(6,*) " gsvSPAIsp :: spai complete, timed at ", arr(1)-s_time
    end if

    if( .not. associated(P%ColEntries) ) then
    if( .not. associated(P%ColIndices) ) then
    if( .not. associated(P%ColLengths) ) then

      call fstop("gsvSPAIsp :: invalid output from spai")

    end if; end if; end if

    B%rank = Rank

    allocate( &
      B%ColEntries(B%rank), &
      B%ColIndices(B%rank), &
      B%ColLengths(B%rank), &
      stat = ec )
    if(ec.ne.0) call fstop("gsvSPAIsp :: insufficient memory :: B initial")

    allocate( zptr(rank), stat = ec ) ! deallocate at end of routine
    if(ec.ne.0) call fstop("gsvSPAIsp :: insufficient memory :: zptr")

    do j = 1, rank

      zptr = (0.0d0,0.0d0)
      do k = 1, P%ColLengths(j)

        i = P%ColIndices(j)%p(k)
        z = P%ColEntries(j)%p(k)

        do m = A%Columns(i)+1, A%Columns(i+1)
          l = A%Indices(m)
          zptr(l) = zptr(l) + z*A%Entries(m)
        end do

      end do

      n = 0
      do i = 1, rank
      if(  dble(zptr(i)) < -EntryTol .or. EntryTol <  dble(zptr(i)) .or. &
          dimag(zptr(i)) < -EntryTol .or. EntryTol < dimag(zptr(i)) ) then
        n = n + 1
      end if; end do

      allocate( &
        B%ColEntries(j)%p(n), &
        B%ColIndices(j)%p(n), &
        stat = ec )
      if(ec.ne.0) call fstop("gsvSPAIsp :: insufficient memory :: B extend")
      B%ColLengths(j) = n

      n = 0
      do i = 1, rank
      if(  dble(zptr(i)) < -EntryTol .or. EntryTol <  dble(zptr(i)) .or. &
          dimag(zptr(i)) < -EntryTol .or. EntryTol < dimag(zptr(i)) ) then

        n = n + 1
        B%ColEntries(j)%p(n) = zptr(i)
        B%ColIndices(j)%p(n) = i

      end if; end do

    end do

!    open(unit=80,file="B.mat",status="replace",action="write",iostat=ec)

!    do j = 1, B%rank
!    do i = 1, B%ColLengths(j)
 
!      write(80, "(2I6,2F12.8)") B%ColIndices(j)%p(i), j, B%ColEntries(j)%p(i)
  
!    end do; end do

!    close(unit=80)

    deallocate( A%Entries, A%Indices, A%Columns )

  end if

  ! -------------------------------------------------------------------------
  ! Perform inversion by TFQMR on a column by column basis
  ! -------------------------------------------------------------------------

  ! Begin Timer for finding inverse

  if( DebugFlag ) then
    call etime(arr, xx)
    s_time = arr(1)
  end if

  if( DebugFlag ) call BeginTiming("Perform TFQMR inversion")

  allocate( TFQMR_Vecs(Rank,9), stat = ec )
  if(ec.ne.0) call fstop("gsvSPAIsp :: insufficient memory :: TFQMR_Vecs")

  atomloop: &
  do jA = 1, nAtoms

    ! If this atom is equivalent to a previous one, copy corresponding block
    ! warning: produces correct trace but not correct inverse
   
    do j = 1, jA-1
    if( EquivAtoms(j) == EquivAtoms(jA) ) then

      InvDiag(:,:,jA) = InvDiag(:,:,j)
      cycle atomloop

    end if; end do

    ! Otherwise call TFQMR on each column corresponding to this atom

    do jL = 1, lmRank

      j = (jA-1)*lmRank + jL
      TFQMR_Vecs(:,2) = (0.0d0,0.0d0)
      TFQMR_Vecs(j,2) = (1.0d0,0.0d0)
      TFQMR_Info = 0

      ! debugging: send convergence info to unit 13
      TFQMR_Info(1) = 0 ! 11300
      TFQMR_Tol     = Tolerance
      TFQMR_MaxIter = MaxIter

      n = 0
      do
       
        n = n + 1
        call zutfx( Rank, Rank, TFQMR_MaxIter, TFQMR_Vecs, &
                    TFQMR_Tol, TFQMR_Info )

        if( TFQMR_Info(2) .ne. 1 ) exit

        ! TFQMR callback request: A x --> b

        CX = TFQMR_Info(3)
        CB = TFQMR_Info(4) 
        
        TFQMR_Vecs(:,CB) = (0.0d0,0.0d0)
        if( PrecondFlag ) then

          do j = 1, rank

!           z = TFQMR_Vecs(j,CX)
!           do k = 1, B%ColLengths(j)
!
!             i = B%ColIndices(j)%p(k)
!             TFQMR_Vecs(i,CB) = TFQMR_Vecs(i,CB) + z*B%ColEntries(j)%p(k)
!
!           end do

            call zaxpyi( B%ColLengths(j), TFQMR_Vecs(j,CX),          &
                         B%ColEntries(j)%p(1), B%ColIndices(j)%p(1), &
                         TFQMR_Vecs(1,CB) )

          end do

        else

          do j = 1, rank
 
!           z = TFQMR_Vecs(j,CX)
!           do k = A%Columns(j)+1, A%Columns(j+1)

!             i = A%Indices(k)
!             TFQMR_Vecs(i,CB) = TFQMR_Vecs(i,CB) + z*A%Entries(k)

!           end do

            k = A%Columns(j)
            call zaxpyi( A%Columns(j+1) - k, TFQMR_Vecs(j,CX), &
                         A%Entries(k+1), A%Indices(k+1),       &
                         TFQMR_Vecs(1,CB) )

          end do

        end if

      end do

      select case ( TFQMR_Info(1) )

        case(0); write(*,'(a,i4,a)') &
          "   gsvSPAIsp :: TFQMR :: solution converged in ", n, " iterations"
        case(4); call fstop("gsvSPAIsp :: TFQMR :: failed to converge")

        case default
          call fstop("gsvSPAIsp :: TFQMR :: invalid inputs or breakdown")

      end select

      ! Store diagonal snippet of solution

      if( PrecondFlag ) then

        zptr = (0.0d0,0.0d0)
        do j = 1, rank

!         z = TFQMR_Vecs(j,1)
!         do k = 1, P%ColLengths(j)

!           i = P%ColIndices(j)%p(k)
!           zptr(i) = zptr(i) + z*P%ColEntries(j)%p(k)

!         end do

          call zaxpyi( P%ColLengths(j), TFQMR_Vecs(j,1),           &
                       P%ColEntries(j)%p(1), P%ColIndices(j)%p(1), &
                       zptr(1) )


        end do

        i = (jA-1)*lmRank
        InvDiag(:,jL,jA) = zptr(i+1:i+lmRank)

      else

        i = (jA-1)*lmRank
        InvDiag(:,jL,jA) = TFQMR_Vecs(i+1:i+lmRank,1)

      end if

    end do

  end do atomloop

  deallocate( TFQMR_Vecs )

  if( DebugFlag ) call EndTiming()

  ! Deallocate B and preconditioner M if it exists

  if( PrecondFlag ) then

    do i = 1, rank
      deallocate( B%ColEntries(i)%p, B%ColIndices(i)%p )
      deallocate( P%ColEntries(i)%p, P%ColIndices(i)%p )
    end do

    deallocate( B%ColEntries, B%ColIndices, B%ColLengths )
    deallocate( P%ColEntries, P%ColIndices, P%ColLengths )
    deallocate( zptr )

  else

    deallocate( A%Entries, A%Indices, A%Columns )

  end if

  ! Print time for inversion

  if( DebugFlag ) then
    call etime(arr, xx)
    write(6,'(a,f9.5)') "  gsvSPAIsp :: inversion complete, timed at ", arr(1)-s_time
  end if
 
end subroutine gsvSPAIsp


end module SPAI

