!BOP
!!ROUTINE: invprmt
!!INTERFACE:
      subroutine invprmt(invswitch,invparam,ndmatr,ndmtr2)
!!DESCRIPTION:
! provides parameters {\tt invparam} and {\tt ndmtr2} for KKR matrix inversion,
! {\tt ndmatr} is larger size of matrix, {\tt invswitch) is control parameter 
!
!!REMARKS:
! include file inverse.h is used
!EOP
!
!BOC
      implicit none
      include 'inverse.h'
      integer invswitch,invparam,ndmatr,ndmtr2
      character*10 :: sname = 'invprmt'

      if(invswitch.eq.0) then
       invparam = nblock
       ndmtr2 = ndmatr*(ndmatr+3)
      else if(invswitch.eq.1) then
       invparam = nblock
       ndmtr2 = ndmatr*nblock
      else if(invswitch.eq.2) then
       invparam = nlev
       ndmtr2 = (ndmatr/2+1)*(ndmatr+1)
       call fstop(sname//' inverse=2 -- has not been checked')
      else if(invswitch.eq.3) then
       invparam = nblock
       ndmtr2 = ndmatr*9
       call fstop(sname//' inverse=3 -- has not been checked')
      else
       ndmtr2 = 0
       write(6,*) ' INVERSE=',inverse
       call fstop(sname//' :: wrong INVERSE in include-file')
      end if
      return
!EOC
      end subroutine invprmt

