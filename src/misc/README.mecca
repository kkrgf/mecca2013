README, Ver. 1.X

What is MECCA?
===============

MECCA -- Multiple-scattering Electronic-structure Code for Complex Applications -- 
is a code to perform electronic structure calculations for multicomponent alloy 
systems with arbitrary supercell geometry (e.g., containing large structural defects) 
and chemical and magnetic disorder.
It is a hybrid r-space/k-space implementation of traditional and screened
Green's function (Korringa-Kohn-Rostoker, KKR) multiple-scattering methods.

Code development code was initiated by D.D. Johnson (U. of Illinois at Urbana-Champaign). 
Earliest implementation was done by him and W.A. Shelton (Oak Ridge National Laboratory). 
From 1998 to 2002, development was been carried out by A. V. Smirnov with D.D. Johnson.

MECCA was started as a Fortran 77 code, later some Fortran 90 constructions were added 
(mainly for dynamic memory allocation), also several C subroutines are used as drivers 
for other computational packages.
So, Fortran 90 and C compilers are needed to produce MECCA executable.




What additional information could be useful?
============================================

The most related to MECCA are: 

Scaling and Efficiency of KKR-CPA in K-space, Screened K-space,
    Real-Space (e.g. LSMS) and mixed representation
  A.V. Smirnov and D.D. Johnson, Comp. Phys. Comm. {\bf 148}, 74-80 (2002).

Accuracy and limitations of same methods, including finite-T case,
  A.V. Smirnov and D.D. Johnson, Phys. Rev. B {\bf 64}, 235129-9 (2001).

Dealing with Partial Long-range Order States:
 D.D. Johnson, A.V. Smirnov, J.B. Staunton, F.J. Pinski, and W.A. Shelton, 
   Phys. Rev. B 62, RC11917-20 (2000). 
 (Many applications were later made with this approach, including combining
  dislocation mechanics with CPA to predict Yield-stress anomalies, rapid
  precipitation growth, etc., see publications from 2005, 2006, 2007, e.g.)

Original KKR-CPA total energy methods:
   D. D. Johnson, D. M. Nicholson, F. J. Pinski, B. L. Gyorffy, G. M. Stocks,
    Phys. Rev. B {\bf 41}, 9701 (1990).
   D.D. Johnson, D.M. Nicholson, F.J. Pinski, B.L. Gyorffy, and G.M. Stocks, 
    Phys. Rev. Lett. {\bf 56}, 2088-91 (1986).

Total Energy formalism to  include charge-correlations in CPA based methods:
   D.D. Johnson and F.J. Pinski, Phys. Rev. B {\bf 48}, 11553-60 (1993).


SCF charge and potential mixing is done by my original Broyden's Second Method,
  D.D. Johnson, Phys. Rev. B 38, 12807-13 (1988)
   Methods is now common in many electronic structure codes 
     (including MECCA, VASP, NRL-MOL, SNL_LCAO,  FLAPW..)

Fast KKR-CPA method with contour (improve later by using Gauss-Chebyshev contour)
    D.D. Johnson, F.J. Pinski, and G.M. Stocks, Phys. Rev. B {\bf 30}, 5508-15 (1984).
   (see references to other similar work therein.) 


***Description of other methods implemented in MECCA can be found in different
sources, see, e.g.

P. Weinberger, {\it Electron Scattering Theory
for Ordered and Disordered Matter} (Clarendon, Oxford, 1990).

A. Gonis, W. H. Butler, Multiple Scattering in Solids
(New York, Springer-Verlag, 2000).

B. Segall, Phys. Rev.  {\bf 105}, 108 (1957).

Brilloiun Zone integrations
H. J. Monkhorst, J. D. Pack, Phys. Rev. B {\bf 13}, 5188 (1976);
J. D. Pack, H. J. Monkhorst, Phys. Rev. B {\bf 16}, 1748 (1977).

Y. Wang, G. M. Stocks, W. A. Shelton, D. M. C. Nicholson, Z. Szotek, W. M. Temmerman,
Phys. Rev. Lett.  {\bf 75}, 2867 (1995).

R. Zeller, P. H. Dederichs, B. {\'U}jfalussy, L. Szunyogh, P. Weinberger,
Phys. Rev. B {\bf 52}, 8807 (1995).

R. Zeller, Phys. Rev. B {\bf 55}, 9400 (1997).

N. Y. Moghadam, G. M. Stocks, B. {\'U}jfalussy, W. A. Shelton, A. Gonis, J. S. Faulkner,
J. Phys.: Condens. Matter {\bf 11}, 5505 (1999).

L. Petit, S. V. Beiden, W. M. Temmerman, G. M. Stocks, G. A. Gehring,
J. Phys.: Condens. Matter {\bf 12}, 8439 (2000).

-----------------------------------------------------------------------

