!c     cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      subroutine gf2dos(ispin,green)
!c     ================================================================
!
!      to calculate DOS(E) based on GF(r,E)  (after summation over l)
!      [the subroutine is not for calculation of l-dependent DOSes]
!
!       attn: WS-integration should be consistent with the one in density_module      
!
      use mecca_constants, only : czero,pi
      use mecca_types, only : RunState
      use mecca_interface, only  : ws_integral
      use mecca_run, only : getMS
      use gfncts_interface, only : g_meshv
!c
      implicit none
!c
      integer, intent(in) :: ispin
      complex(8), intent(in)  :: green(:,:,:) ! (iprpts,ipcomp,nbasis)
      type(RunState), pointer :: mecca_state
      integer :: iend,nbasis,nsub,nk
      real(8) :: re_ws,im_ws,re_mt,im_mt,h
      logical, external :: gNonMT
      real(8) :: tmp_gf(size(green,1))
      real(8) :: rr(size(green,1))
      real(8) :: xr(size(green,1))
      character(2), parameter :: updn(2) = ['up','dn']
!      character(*), parameter :: sname='gf2dos'

      mecca_state => getMS()

      iend = size(green,1)
      nbasis = size(green,3)
      do nsub=1,nbasis
       mecca_state%gf_box%dos(0:,:,nsub,ispin)   = czero
       mecca_state%gf_box%dosck(:,nsub,ispin) = czero
       do nk=1,mecca_state%intfile%sublat(nsub)%ncomp
        call g_meshv(mecca_state%intfile%sublat(nsub)%compon(nk)        &
     &               %v_rho_box,h,xr(:),rr(:))
        tmp_gf(1:iend) = real(green(1:iend,nk,nsub))*(rr(1:iend)**2)
       call asa_integral(mecca_state,nk,nsub,tmp_gf(1:iend),re_ws,re_mt)
        tmp_gf(1:iend) = aimag(green(1:iend,nk,nsub))*(rr(1:iend)**2)
       call asa_integral(mecca_state,nk,nsub,tmp_gf(1:iend),im_ws,im_mt)
        mecca_state%gf_box%dos(0,nk,nsub,ispin) = -cmplx(re_ws,im_ws)/pi
        if ( gNonMT() ) then
          mecca_state%gf_box%dosck(nk,nsub,ispin)=-cmplx(re_ws,im_ws)/pi
        else
          mecca_state%gf_box%dosck(nk,nsub,ispin)=-cmplx(re_mt,im_mt)/pi
        end if
        if ( mecca_state%intfile%iprint.ge.0 ) then
         write(6,1001) mecca_state%gf_box%energy(ispin),                &
     &             mecca_state%gf_box%dos(0,nk,nsub,ispin),updn(ispin), &
     &                 nsub,nk
 1001    format(2f10.7,1x,2g16.8,' E',a2,',dos(E) nsub=',i4,1x,i2)
        end if
       end do
      end do
!c
      return
      end subroutine gf2dos
