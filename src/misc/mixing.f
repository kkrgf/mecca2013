      subroutine mixing1(fnew,fold,xvalws,xvalwsnw,volws,               &
     &                     qtotmt,qrmsav,vdif,vdold,                    &
     &                     wrk1,wrk2,xr,rr,ivar_mtz,iVP,                &
     &                     ichg,imixrho,alpha,beta,                     &
     &                     itscf,komp,jmt,jws,nbasis,numbsub,mspn,      &
     &                     r_circ,fcount,weight,rmag,vj,rmt_true,       &
     &                     iprint)
!c    ===============================================================
!c      implicit none
       implicit real*8 (a-h,o-z)
!c
!c    ==============================================================
!c     routine for taking care of SCF mixing of charge or potential
!c     both simple mixing and broyden mixing are used for charges.
!c     only simple mixing is used for potentials.
!c     good for magnetic problems and systems with basis.
!c         [mixing on potential is only required sometimes
!c          to get started from bad starting guesses.]
!c     ...........................        written by  DDJ  Dec 1993
!c    ===============================================================
!c     ichg:   indicates whether to mix rho (ichg=0) or pot ichg=(1)
!c     imixrho:mix with Broyden or not (only for charges!)
!c     fnew:   new rho or pot
!c     fold:   old rho or pot
!c     xval:   valence charge (old and new)
!c     volws:  volume of WS cell
!c     vdif:   new dif. of up & down potentials for spin-polarized
!c     vdold:  old dif. of up & down potentials for spin-polarized
!c    ===============================================================
!c    +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
      include 'mkkrcpa.h'
      include 'imp_inp.h'
!c    +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
      integer  nbasis,itscf,komp(nbasis),mspn
      integer  jmt(ipcomp,nbasis),jws(ipcomp,nbasis)
      integer  ichg,imixrho,iprint
      integer  numbsub(nbasis)
      integer  ir,ib,ik,is,jtop,jtop1
      integer  fcount(nbasis)
      integer ia
      integer    num_circ(ipcomp,nbasis)

      real*8     rmt_true(ipcomp,nbasis)
      real*8     r_circ(ipcomp,nbasis)
      real*8     weight(MNqp)
      real*8     rmag(MNqp,MNqp,MNqp,MNF,nbasis)
      real*8     vj(MNqp,MNqp,MNqp,MNF,nbasis)

      real*8     rrs(iprpts)
      real*8     frho(iprpts)
      real*8     VPInt(2)
      real*8     ylag

!c
      real*8   fnew(iprpts,ipcomp,ipsublat,ipspin)
      real*8   fold(iprpts,ipcomp,ipsublat,ipspin)
      real*8   xvalws(ipcomp,ipsublat,ipspin)
      real*8   xvalwsnw(ipcomp,ipsublat,ipspin)
      real*8   xvaltmp(ipcomp,ipsublat,ipspin)
      real*8   volws
      real*8   scale
      real*8   vdif,vdold
      real*8   qtotmt(ipcomp,ipsublat,ipspin)
      real*8   wrk1(iprpts),wrk2(iprpts)
      real*8   xr(iprpts,ipcomp,ipsublat),rr(iprpts,ipcomp,ipsublat)
      real*8   alpha,beta,qrmsav,qrmstmp

      character*10 :: sname = 'mixing'

!c==================================================================

      if(ichg.le.0.and.iprint.ge.0) write(6,*) sname//':'

!c
!c scaling function for XVALWS (possibly scale by WS volume).
!c     scale=volws
      scale=one

!c
!c mixing charges..................................................
      if(ichg.le.0 .and. imixrho .gt. 0) then
!c
!c In this code, jtop is end of grid for all spheres (MT or ASA)
!c (in ASA jmt=jws)


        if(iVP.eq.0.and.ivar_mtz.ge.3)then
!CAA       For VP Integration
         jtop = jmt(1,1)
         do ib=1,nbasis
          do ik=1,komp(ib)
           do ir=1,iprpts
             if(rr(ir,ik,ib).le.r_circ(ik,ib))then
              num_circ(ik,ib)=ir
             endif
           enddo
              num_circ(ik,ib)=num_circ(ik,ib)+1
              jtop=max(jtop,num_circ(ik,ib))
          enddo
         enddo
        else
         jtop = jmt(1,1)
         do ib=1,nbasis
          do ik=1,komp(ib)
            jtop=max(jtop,jmt(ik,ib))
          enddo
         end do
        endif

       jtop1=jtop+1                        ! XVAL info put here

       if(jtop1 .gt. iprpts) then
        write(6,*) ' JTOP=',jtop
        write(6,*) ' IPRPTS=',iprpts
        call fstop(sname//': jtop+1 .gt. iprpts -- you are hosed!!')
       endif
!c
!c broyden mixing ==================================================
!c
!c for Broyden requires only total rho and moments
!c   Must load XVALWS sum (for rho) and difference (for moments) to
!c   jtop+1 array location to maintain proper charge neutrality.
!c          FNEW and FOLD are arrayed well beyond jtop,
!c          so jtop+1 will not overwrite arrays.
       do is = 1,mspn
        do ib=1,nbasis
         do ik=1,komp(ib)
!c  possibly scale XVALWS for mixing
           fnew(jtop1,ik,ib,is)=  xvalwsnw(ik,ib,is)/scale              &
     &                           *numbsub(ib)
           fold(jtop1,ik,ib,is)=  xvalws(ik,ib,is)/scale                &
     &                           *numbsub(ib)
!CAA       For VP Integration
!CAA       do ir=jws(ik,ib)+1,jtop
!CAA         fnew(ir,ik,ib,is) = zero
!CAA         fold(ir,ik,ib,is) = zero
!CAA        enddo
         enddo
        enddo
       enddo
!c====================================================================
       call broyden(fnew,fold,xvaltmp,                                  &
     &             itscf,alpha,beta,komp,nbasis,                        &
     &                       mspn,jtop,qrmsav)
!c====================================================================
!c the scaled XVALWS were put in FNEW and FOLD above and mixed.
!c unscale them for use in SCF interation
       do is = 1,mspn
         do ib=1,nbasis
           do ik=1,komp(ib)
             xvalwsnw(ik,ib,is)= xvaltmp(ik,ib,is)*scale                &
     &                              /numbsub(ib)
!c
!c just in case: zero out jtop1 array location
               fnew(jtop1,ik,ib,is)=zero
               fold(jtop1,ik,ib,is)=zero
           enddo
         enddo
       enddo
!c===================================================================
      else
!c===================================================================
!c simple mixing of charge or potential..............................
!c===================================================================
       call simplmx(fnew,fold,xvalwsnw,xvalws,                          &
     &        vdif,vdold,komp,nbasis,mspn,ichg,alpha,beta)
!c===================================================================
      endif

!c===================================================================
!c
!c have to get qtotmt for mixed charge density case
      if(ichg.le.0) then
!c     ===========================================================
       do is = 1,mspn
        do ib=1,nbasis
         do ik = 1,komp(ib)
!CAB          call qexpup(1,fnew(1,ik,ib,is),jtop,xr(1,ik,ib),wrk1)
!CAB          qtotmt(ik,ib,is) = wrk1(jtop)

          call qexpup(1,fnew(1,ik,ib,is),jmt(ik,ib),xr(1,ik,ib),wrk1)

           if(iVP.eq.0.and.ivar_mtz.ge.3)then
            ! For Ordered systems only AND Full VP+ Variational MT-zero (VP)
               xrmt = log(rmt_true(ik,ib))
               nlag = 3
!c              =======================================================
               do j = 1,iprpts
                frho(j)=fnew(j,ik,ib,is)
                rrs(j) = rr(j,ik,ib)
               enddo

               ia=2
               call isopar_integ(ia,rmt_true(ik,ib),rrs,frho,           &
     &                       r_circ(ik,ib),fcount(ib),weight,           &
     &                       rmag(1,1,1,1,ib),vj(1,1,1,1,ib),VPInt)

               qtotmt(ik,ib,is) = VPInt(2) +                            &
     &               ylag(xrmt,xr(1,ik,ib),wrk1,0,nlag,jmt(ik,ib),iex)
!c          =========================================================
           else
            ! For Disord systems only OR Variational MT-zero (ASA)
             qtotmt(ik,ib,is) = wrk1(jmt(ik,ib))
           endif
!c          =========================================================

!CAB          do ir = 1,jtop
          do ir = 1,jmt(ik,ib)
               wrk1(ir) = ( fnew(ir,ik,ib,is)                           &
     &                     -fold(ir,ik,ib,is) )**2/rr(ir,ik,ib)
          enddo
!CAB          call simpun( xr(1,ik,ib),wrk1,jtop,1,wrk2 )
!CAB          qrmstmp=wrk2(jtop)/(four*pi*volws)
          call simpun( xr(1,ik,ib),wrk1,jmt(ik,ib),1,wrk2 )
          qrmstmp=wrk2(jmt(ik,ib))/(four*pi*volws)
!c
          if(iprint.ge.0) then
           write(6,1000)                                                &
     &         ik,ib,is,xvalwsnw(ik,ib,is),qtotmt(ik,ib,is),qrmstmp
1000       format('  ik=',i2,' ib=',i5,' is=',i1,' xvalws=',f11.7,      &
     &             ' qtotmt=',f11.7,' qrms=',d10.3)
          end if
!c
         enddo
        enddo
       enddo
      endif
!c     ===========================================================
!c
!c       write(6,'(//)')

       return
       end
