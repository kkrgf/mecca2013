      subroutine gettau(ispin,nspin,nrelv,clight,                       &
     &                  icryst,lmax,kkrsz,nbasis,numbsub,               &
     &                  alat,volume,aij,itype,natom,                    &
     &                  if0,mapstr,mappnt,ndimbas,mapij,                &
     &                  komp,atcon,                                     &
!cALAM     >                  npls,
     &                  ij3,nj3,cgaunt,cfac,                            &
     &                  xknlat,ndimks,Rksp0,tmparr,nkns,                &
     &                  qmesh,ndimq,nmesh,nqpt,                         &
     &                  lwght,lrot,                                     &
     &                  ngrp,kptgrp,kptset,ndrot,kptindx,               &
     &                  ivpoint,ikpoint,                                &
     &                  vr,h,rmt,rws,jmt,jws,xr,rr,                     &
     &                  rmt_true,r_circ,ivar_mtz,                       &
     &                  fcount,weight,rmag,vj,iVP,                      &
     &                  energy,                                         &
     &                  itmax,rns,nrns,ndimrs,                          &
     &                  np2r,ndimnp,numbrs,naij,                        &
     &                  ndx,rot,dop,nop,                                &
     &                  hplnm,ndimrhp,ndimlhp,                          &
     &                  dos,dosck,green,                                &
     &                  eneta,enlim,                                    &
     &                  conk,conr,                                      &
     &               rslatt,kslatt,Rnncut,Rsmall,mapsprs,               &
     &                    bsf_ksamplerate,bsf_fileno,                   &
     &                    bsf_nkwaypts,bsf_kwaypts,                     &
     &                  fermi_korig,fermi_nkpts,fermi_kpts,             &
     &                  fermi_kptlabel,fermi_orilabel,                  &
     &             fermi_gridsampling,fermi_raysampling,fermi_filenos,  &
     &             cotdl,mtasa,ksintsch,efermi,                         &
     &             iprint,istop)

!c     =================================================================
!c
      use mecca_constants
      use raymethod
      implicit real*8(a-h,o-z)
!c
!c     ******************************************************************
!c     this routine solves the cpa equations using the brillouin-zone
!c     integration method for calculating tau(0,0).....
!c     ...................................................gms   july 1984
!c
!c     code for gms symm. type array storage    jan'86 ......gms...
!c     code for lmax=3                          jan'86 ......gms...
!c     full t-matrix storage code               mar'92.......gms...
!c
!c     Rewritten logic of calling routines and added to GETTAU to
!c     improve ewald & real-space algorithms .... ddj & was April 1994
!c           (factor of 6 speed-up per energy and reduced storage)
!c     Found origin of symmetry violation in d.o.s., esp. w/ real-space.
!c     Hankel fct converges poorly in complex plane for system w/ basis.
!c     Ewald method now faster and converges quickly in complex plane.
!c     Both hankel and real-space ewald integral passed as DQINT.
!c     ........................................... ddj & was April 1994
!c     (next step: reduce tau storage using group theory (KTOP storage))
!c     (next step: rewrite SUMROT to do only the 3x3 matrices instead)
!c
!c     ******************************************************************
!c
!c     ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!c     ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
      character sname*10
      real*8    third
!c     ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
      parameter (sname='gettau')
      parameter (third=one/three)
!c     ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!c
!c     ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!c     FOR STRUCTURE CONSTANTS: use Ewald and/or real-space methods
!c              real and imaginery energy switch, see below
      integer   ipit               ! for tc accelerator
      real*8    eresw
      real*8    eimsw
      parameter (ipit=4)

!c     ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!c     switch to use EWALD within "eresw" D.U. of Real E axis and
!c       real-space method beyond "eresw" D.U. of Real E axis.
!c     ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
      parameter (eresw=-0.5d0)
      parameter (eimsw=+0.5d0)
!CAB      parameter (eresw=-1.4d0)
!CAB      parameter (eimsw=+1.4d0)
!c     ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!c
      character istop*10
!c
      integer nbasis,numbsub(nbasis)
      integer      ivpoint(nbasis) ! (ipsublat)
      integer      ndx(*)
      integer      ikpoint(ipcomp,nbasis)
      integer      komp(nbasis)
!CALAM      integer      ij3(kkrsz*kkrsz*kkrsz)
!CALAM      integer      nj3(kkrsz*kkrsz)
      integer      ij3(ipkkr*ipkkr*ipkkr)
      integer      nj3(ipkkr*ipkkr)
      integer      jmt(ipcomp,nbasis)
      integer      jws(ipcomp,nbasis)
      integer      itmax
      integer      nitcpa
      integer      nop
      integer      nrns
      integer      ndimrs
      integer      natom,itype(natom)
      integer      if0(48,natom)
      integer      mapstr(ndimbas,natom),mappnt(ndimbas,natom)
      integer    fcount(nbasis)

      real*8     rmt_true(ipcomp,nbasis)
      real*8     r_circ(ipcomp,nbasis)
      real*8     weight(MNqp)
      real*8     rmag(MNqp,MNqp,MNqp,MNF,nbasis)
      real*8     vj(MNqp,MNqp,MNqp,MNF,nbasis)
!c
      real*8       atcon(ipcomp,nbasis)
      real*8       vr(iprpts,ipcomp,nbasis)
      real*8       xr(iprpts,ipcomp,nbasis)
      real*8       rr(iprpts,ipcomp,nbasis)
!CALAM      real*8       xstart(ipcomp,ipsublat)
      real*8       h(ipcomp,nbasis)
      real*8       rmt(ipcomp,nbasis)
      real*8       rws(ipcomp,nbasis)
      real*8       cgaunt(*)
      real*8       rns(ndimrs,4)
!c      real*8       rns2(iprsn)
!c      real*8       rnstmp(iprsn,3)
!c      real*8       rtmp(3)
      real*8       r2tmp
      real*8       alat
      real*8       clight
!c      real*8       scale
      real*8       eneta
      real*8       conk
!c
      complex*16   powe(ipdlj)

      complex*16   dqint1(iprij,2*iplmax+1)
      complex*16   dqtmp(0:2*iplmax)
      complex*16   cfac(ipkkr,ipkkr)
      complex*16   conr(*)
      complex*16   tab(ipkkr,ipkkr,ipcomp,nbasis)
      complex*16   almat(iplmax+1,ipcomp,nbasis)

      complex*16   tcpatmp(ipkkr,ipkkr,natom)
      integer      itype1(natom),iorig(nbasis)

      complex*16   cotdl(iplmax+1,ipsublat,ipcomp)
      complex*16   tcpa(ipkkr,ipkkr,nbasis)
      complex*16   tcin(ipkkr,ipkkr,ipit,nbasis)
      complex*16   tcout(ipkkr,ipkkr,ipit,nbasis)
      complex*16   pzz(ipkkr,ipkkr,ipcomp,nbasis)
      complex*16   pzj(ipkkr,ipkkr,ipcomp,nbasis)
      complex*16   pzzck(ipkkr,ipkkr,ipcomp,nbasis)
      complex*16   pzjck(ipkkr,ipkkr,ipcomp,nbasis)
      complex*16   pzzbl(ipkkr,ipkkr,ipcomp,ipcomp,nbasis)
      complex*16   tau00(ipkkr,ipkkr,nbasis)
      complex*16   green00(ipkkr,ipkkr,nbasis)
      complex*16   w1(ipkkr,ipkkr)
      complex*16   w2(ipkkr,ipkkr)
      complex*16   w3(ipkkr,ipkkr)
      complex*16   w4(ipkkr,ipkkr)
      complex*16   xab(ipkkr,ipkkr,ipcomp)
      complex*16   dos(0:iplmax,ipcomp,nbasis)
      complex*16   dosck(ipcomp,nbasis)
      complex*16   green(iprpts,ipcomp,nbasis)
      complex*16   dop(kkrsz,kkrsz,nop)
      complex*16   hplnm(ndimrhp,ndimlhp,*)
      complex*16   energy
!cab      complex*16   prel
      complex*16   pnrel
      complex*16   edu
      complex*16   pdu
      complex*16   xpr
      complex*16   d00
      complex*16   eoeta
      complex*16   lloydms

      complex*16, allocatable :: greenks(:,:)

!CAB TEMP
!cALAM      complex*16 taun1n2,tautmp
!cALAM      real*8 tcpamax,errcpa

      integer naij,mapij(2,naij)
      integer np2r(ndimnp,naij),numbrs(naij)
      real*8  aij(3,naij)
      integer nqpt(nmesh),nkns(nmesh)
      real*8  qmesh(3,ndimq,nmesh)
      integer lwght(ndimq,nmesh),lrot(ndimq,nmesh)
      integer ngrp(nmesh),ndrot,kptgrp(ndrot+1,nmesh)
      integer kptset(ndrot,nmesh),kptindx(ndimq,nmesh)
      real*8  xknlat(ndimks,3,nmesh),tmparr(ndimks*3+ndimks+ndimks*2)
      real*8  enlim(2,nmesh-1)

      real*8 Rnncut,Rsmall
      real*8 rslatt(3,3)           ! Alat*rslatt(1..3,*)/(2*pi) -- latt.vect.
      real*8 kslatt(3,3)           ! Alat*rslatt(1..3,*)/(2*pi) -- latt.vect.
      integer mapsprs(natom,natom)
      real*8 V0
      integer imethod,isparse

      real*8, save :: lastdos = 0.d0
      real*8 efermi

      real*8 rot(49,3,3)

      ! plotting fermi surface
      real*8 kpnt(3),startk(3),endk(3)
      real*8 displ(3),edge1(3),edge2(3),norm(3)
      real*8 e1len,e2len,dllen,dlang,sint,xyang,th
      real*8 x,y,u,v,du,dv
      integer e1ndiv,e2ndiv
      complex*16 rayp
      real*8 roots(10)
      integer nroots

      real*8 fermi_korig(3)
      real*8 fermi_kpts(3,ipbsfkpts)
      integer fermi_nkpts,fermi_gridsampling,fermi_raysampling
      integer fermi_fileno,fermi_filenos(2)
      character*32 :: fermi_kptlabel(ipbsfkpts)
      character*32 :: fermi_orilabel

      ! bloch spectral function
      real*8 :: bsfNL(0:iplmax,nbasis)
      real*8 :: bsfcubic(kkrsz,nbasis)
      integer bsf_ksamplerate,bsf_nkwaypts
      real*8 :: bsf_kwaypts(3,ipbsfkpts)
      real*8 :: bsfdis
      integer bsf_fileno

      integer ksintsch ! k-space integration scheme
                       ! 1 = monkhorst-pack
                       ! 2 = ray method

      logical, save :: firstcall = .true.
      logical, save :: isdis = .false.
      logical :: didrayint = .false.

      ! post-processing akeBZlo.dat for fermi output
      integer flb,fub
      integer, parameter :: maxraysamples = 10000
      integer :: ips
      character(len=80) :: akestring
      character :: firstch
      real*8 :: akeray(3,maxraysamples), halfmax
      logical :: ispeak
      !!!

      real*8 R2ksp,Rksp0
      real*8 :: etamin = 0.1d0

       real*8       eta0
       integer      irecdlm

      complex*16, parameter :: sqrtm1 = dcmplx(0.d0,1.d0)
!      complex*16 catanf
!      catanf(gam)=.5d+00*sqrtm1*cdlog((sqrtm1+gam)/(sqrtm1-gam))

      real*8 volume
      real*8 lloydform
      real*8 ph
      real*8, save :: lastph(12)=0.d0

      ndkkr = ipkkr
      call timel(time2)

!c     calculate things that depend only on energy
      pnrel = cdsqrt(energy)
!c
!c                Im(pnrel) must be .GE. zero
!c
      pnrel = pnrel*sign(1.d0,dimag(pnrel))

!c     energy and momentum in dimensionless units (DU).

      edu =energy*(alat/(two*pi))**2
      pdu = pnrel*(alat/(two*pi))
!c
!c????             pdu ~ pnrel or sqrt(edu) ???? (for Im(edu)<0 only)
!c

!c     switch for getting irregular part of G(E) at complex E, i.e. ZJ
      iswzj=1
!c     --------------------------------------------------------------
      if(iprint.ge.1) then
       write(6,'(a10,'':  edu,pdu='',1x,''('',f9.5,'','',f9.5,'' ),'',  &
     &                              1x,''('',f9.5,'','',f9.5,'' )'')')  &
     &                      sname,edu,pdu
      endif
!c     --------------------------------------------------------------

      if( real(edu).gt.eresw .and. dimag(edu) .lt. eimsw ) then
       irecdlm = 0
      else
       irecdlm = 1
      end if

      call pickmesh(edu,enlim,nmesh,imesh)
      call DefMethod(1,imethod,isparse)
      if ( imethod == 0 ) then
        V0 = zero
      else
        V0 = U0
      end if     
      if(imethod.le.zero.or.imethod.eq.4) then    ! For K-space calculations

       eta0 = max(etamin,abs(edu)/eneta)
!c                                    to be sure in accuracy of D00
       if( irecdlm.eq.0 ) then
         R2ksp = max(zero,eta0*Rksp0*Rksp0+real(edu))
         if(iprint.ge.0) then
          write(6,1000) eta0,sqrt(R2ksp),imesh,nqpt(imesh)
1000      format(' USING EWALD *****    ETA=',f7.5,                     &
     &          '  Rksp=',f12.5,' NQPT(',i1,')=',i7)
         end if

         ! rooeml --> sqrt(e^-l)
         call rooeml(pdu,powe,lmax,iprint,istop)
         call d003(edu,eta0,d00)
         eoeta=exp( edu/eta0 ) * conk
!c
!c ==================================================================
!c  calculate the real-space integral used in Ewald technique.
!c  This is energy dependent, but k-space independent.
!c  DO once for each energy and store results in upper triangular
!c  form. INTFAC calculated the integral and returns in DQINT.
!c ==================================================================
!c
       else
         if(iprint.ge.0)                                                &
     &   write(6,1001) imesh,nqpt(imesh)
1001     format(' USING HANKEL *****   NQPT(',i1,')=',i7)
!         removed for debugging
!         if(dimag(edu).lt.0.d0)
!     *   call fstop(sname//': Im(E)<0')
!c
!c ==================================================================
!c  calculate the hankel fct used in real-space calculation of
!c   structure constants out in complex plane.
!c  This is energy dependent, but k-space independent.
!c  DO once for each energy and store results in upper triangular
!c  form. HANKEL calculated the fct and returns in DQINT.
!c ==================================================================
!c
!c  do not start a hankel with zero vector length (ir=1)...blows up!
!c  only true for diagonal block, of course.
!c
       endif

!c ==================================================================
          rsp = -two
          do i=1,nrns
!c          rtmp(1)= rns(i,1)
!c          rtmp(2)= rns(i,2)
!c          rtmp(3)= rns(i,3)
           r2tmp=   rns(i,4)
           if( abs(r2tmp-rsp) .ge. 1.0d-6 ) then
             rsp = r2tmp
             if(irecdlm.eq.0) then
                call intfac(rsp,edu,eta0,2*lmax,dqtmp)
             else
                if(r2tmp.le.1.0d-6) then
                 do l1=0,2*lmax
                  dqtmp(l1) = dcmplx(zero,zero)
                 enddo
                else
                 xpr=pdu*sqrt(rsp)
                 call hankel(xpr,dqtmp,2*lmax)
                end if
             end if
             do l1=0,2*lmax
                dqint1(i,l1+1)=dqtmp(l1)
             enddo
           else
             do l1= 1,2*lmax+1
                 dqint1(i,l1)=dqint1(i-1,l1)
             enddo
           endif
          enddo

      else
         if(iprint.ge.0) then
          write(6,1002) Rsmall/(two*pi),Rnncut/(two*pi),                &
     &                  imesh,nqpt(imesh)
1002      format(' REAL SPACE *****    Rsmall=',f6.3,                   &
     &          '  Rnncut=',f6.3,' NQPT(',i1,')=',i7)
         endif
      endif

!c ==================================================================
!c

      call gettab(nrelv,clight,                                         &
     &            lmax,kkrsz,nbasis,komp,                               &
     &            icryst,alat,                                          &
     &            iswzj,                                                &
     &            energy,pnrel,                                         &
     &            vr,ivpoint,ikpoint,                                   &
     &            xr,rr,h,jmt,jws,rmt,rws,                              &
     &            rmt_true,r_circ,ivar_mtz,                             &
     &            fcount,weight,rmag,vj,iVP,                            &
     &            tab,pzz,pzj,pzzck,pzjck,pzzbl,                        &
     &            cotdl,almat,mtasa,iprint,istop)


!c        write(6,*) 't-matrix at en = ', energy
!c        do i=1,kkrsz
!c          write(6,*) tab(i,i,1,1)
!c        end do
!          write(6,*) 'is = ', ispin
!          write(6,*) 'pzzbl(1,1) =', ispin
!          write(6,*) pzzbl(1:kkrsz,1:kkrsz,1,1,2)
!          write(6,*) 'pzzbl(1,2) =', ispin
!          write(6,*) pzzbl(1:kkrsz,1:kkrsz,1,2,2)
!          write(6,*) 'pzzbl(2,1) =', ispin
!          write(6,*) pzzbl(1:kkrsz,1:kkrsz,2,1,2)
!          write(6,*) 'pzzbl(2,2) =', ispin
!          write(6,*) pzzbl(1:kkrsz,1:kkrsz,2,2,2)
!
!c
!c     =================================================================
!c     construct the average t-matrix..................................
!c
        call get0ata(atcon,tab,tcpa,kkrsz,komp,nbasis,iprint,istop)
!c     -----------------------------------------------------------------

      do nsub=1,nbasis
        do iatom=1,natom
         if(itype(iatom).eq.nsub) go to 10
        end do
        call fstop(sname// ' :: You have wrong ITYPE')
10      iorig(nsub) = iatom

!c!!!
!c  it is supposed below  that "iorig(nsub)" -- number of 1st site of
!c  type "nsub" for isite=1,natom
!c!!!
      end do

      do iatom=1,natom
       itype1(iatom) = iatom
      end do

!C!----------------------------------------------------------------------
!C!  Do not change the array itype1(*) below this line; this array is used
!C!  to treat equivalent sites as inequivalent (when it is needed)
!C!----------------------------------------------------------------------

!c     =================================================================

      if(itmax.eq.-1) then          !  ATA SCF solution

       tau00 = tcpa
       iconverge = 0
       nitcpa = 0

      else                          !  CPA SCF

       do ict = 1, abs(itmax)

         nitcpa=ict
         if(ict.gt.1) then
          iprint1 = iprint-2
          iprint2 = 1000
         else
          iprint1 = iprint
          iprint2 = iprint
         end if

         if(ict.ne.1) then
          if(ngrp(1).gt.1                                               &
     &       .or.lwght(kptindx(kptgrp(1,1),1),1).gt.1                   &
     &      ) then
!c
!c  Symmetry of tcpa-matrix could be destroyed due to numerical errors,
!c  to maintain the proper symmetry for each CPA-iteration
!c
           call symmrot(ngrp(imesh),kptgrp(1,imesh),                    &
     &                  kptset(1,imesh),kptindx(1,imesh),               &
     &                  lwght(1,imesh),lrot(1,imesh),                   &
     &                  tcpa,ipkkr,kkrsz,                               &
     &                  if0,dop,nbasis,                                 &
     &                  iorig)
          end if
         end if
!c
!c  Tcpa-matrices for equivalent sites can differ !!!
!c
         call tcpa2all(natom,nbasis,iorig,itype,numbsub,if0,nop,        &
     &                   kkrsz,ndkkr,dop,tcpa,tcpatmp,w2,iprint2)

         ! is the system disordered?
         isdis = .false.
         do n = 1, nbasis
           if(komp(n)>1) isdis = .true.
         end do

         ! do not perform integral for ordered system BSF calc.
         if( (istop /= 'AKEBZ_CALC' .and. istop /= 'FERMI_CALC' .and.   &
     &         istop /= 'BSF_CALC') .or. isdis ) then

         if( ksintsch == 1 .or.                                         &
     &       (ksintsch == 3.and.lastdos > hybrid_cutoff) ) then

           ! BZ integration using special k-pt method
!           write(6,'(a,f7.3,a,f7.3,a)') '     Performing k-space '
!     >       //'integral @ en = (', real(energy),',',dimag(energy),')'


           call intgrtau(                                               &
     &                  lmax,kkrsz,nbasis,numbsub,atcon,komp,           &
     &                  alat,aij,itype,natom,iorig,itype1,              &
     &                  if0,mapstr,mappnt,ndimbas,mapij,                &
!cALAM     >                  npls,
     &                  powe,                                           &
     &                  ij3,nj3,cgaunt,cfac,                            &
     &                  edu,pdu,                                        &
     &                  irecdlm,                                        &
     &                  rns,ndimrs,                                     &
     &                  np2r,ndimnp,numbrs,naij,                        &
     &                  dqint1,iprij,ndx,                               &
     &                  hplnm,ndimrhp,ndimlhp,                          &
     &                  tcpatmp,                                        &
     &                  tau00,lloydms,                                  &
     &                  dop,nop,d00,eoeta,                              &
     &                  eta0,                                           &
     &                  xknlat(1,1,imesh),ndimks,R2ksp,tmparr(1),       &
     &                  conr,nkns(imesh),                               &
     &                  qmesh(1,1,imesh),nqpt(imesh),                   &
     &               lwght(1,imesh),lrot(1,imesh),                      &
     &   ngrp(imesh),kptgrp(1,imesh),kptset(1,imesh),kptindx(1,imesh),  &
     &                  rslatt,Rnncut,Rsmall,mapsprs,                   &
     &                  V0,rws,                                         &
     &                  tmparr(3*ndimks+1),tmparr(4*ndimks+1),          &
     &                  iprint1,istop)
            didrayint = .false.

!            write(6,*) 'itcpa = ', ict
!            write(6,*) 'ispin = ', ispin
!            write(6,*) 'tau00 = ', tau00(1:kkrsz,1:kkrsz,1)

         else

           ! BZ integration using radial wedges in spherical coordinates
!          write(6,'(a,f7.3,a,f7.3,a)') '     Performing k-space '
!     >       //'integral @ en = (', real(energy),',',dimag(energy),')'

           dutory = (two*pi)/alat
           rytodu = alat/(two*pi)

           tcpatmp = dutory * tcpatmp
             ! remember that tcpa has units of 1/sqrt(energy)
             ! so we are *really* changing to DU here

!           write(6,*) 'd00=', d00, eoeta


           ! find diagonal blocks of tau = [t^-1 - G0]^-1
           allocate( greenks(natom*kkrsz,natom*kkrsz) )
           call ksrayint( lmax,kkrsz,natom,aij,mapstr,mappnt,           &
     &         nbasis,mapij,powe,ij3,nj3,cgaunt,cfac,edu,               &
     &         pdu,rns,ndimrs,np2r,ndimnp,numbrs,naij,dqint1,           &
     &         iprij,ndx,hplnm,ndimrhp,ndimlhp,irecdlm,d00,eoeta,eta0,  &
     &         xknlat(1:nkns(imesh),:,imesh),nkns(imesh),conr,          &
     &         greenks,tcpatmp,iorig,itype,kslatt,nop,rot,dop,if0,      &
     &         tau00,lloydms,green00 )
           deallocate( greenks )

           tau00 = rytodu * tau00
           green00 = rytodu * green00
           tcpatmp = rytodu * tcpatmp
           didrayint = .true.

!           write(6,*) 'tau00 = ', tau00(1:kkrsz,1:kkrsz,1)

          end if

         end if

         ! if a random alloy then iterate cpa
         if(itmax.ne.1) then

           if(ict.eq.itmax) then
            iprint1 = max(1,iprint)
           else
            iprint1 = iprint
           end if

           call mcpait1(tau00,tcpa,tab,tcin,tcout,                      &
     &                  xab,                                            &
     &                  w1,w2,w3,w4,                                    &
     &                  kkrsz,                                          &
     &                  nbasis,                                         &
     &                  atcon,                                          &
     &                  komp,numbsub,                                   &
     &                  iconverge,nitcpa,                               &
     &                  iprint1,istop)

!c           -----------------------------------------------------------


         else ! no CPA case

            iconverge=0

         endif

         if(iconverge.eq.0) go to 100

       end do

      end if

100   continue

      if( iconverge.ne.0.and.                                           &
     &  istop/='DOS_CALC' .and.                                         &
     &  istop/='BSF_CALC' .and.                                         &
     &  istop/='DOSBSF_CAL' .and.                                       &
     &  istop/='EBOT_CALC' ) then

!c        CPA failed to converge.......................................

        write(6,*)
        write(6,*) ' ICONVERGE=',iconverge
        write(6,'('' gettau:: CPA failed to converge in '',i3,          &
     &          '' iterations'')') itmax
        if(real(iconverge)/real(nbasis).gt.0.5.and.itmax.gt.0) then

         write(*,1003) energy
1003     format(/' ENERGY=',2d20.12//                                   &
     &'  You have a problem with CPA convergence:'/                     &
     &'  It is very likely that you have numerical instability'/        &
     &'  (inaccuracy in symmetry+CPA --> lack of convergence).'/        &
     &'  For such cases we have never had problems with full zone',     &
     &   ' integration.'/                                               &
     &'  We do not know a good recipe to find CPA solution in',         &
     &   ' general case.'/                                              &
     &'  You can try to increase cpa-tolerance <tctol>,'/               &
     &'  (i.e. decrease accuracy of CPA) in mcpait-routine;'/           &
     &'  or to change mixing parameters in <mcpait1>). You may'/        &
     &'  want to ignore the situation (in this case you should use'/    &
     &'  negative cpa-responsible parameter in input file (absolute'/   &
     &'  value of the parameter -- max.number of iterations))')
         call fstop(sname//' :: change CPA-parameter in input file')
        else if(itmax.gt.0) then
         write(6,*)                                                     &
     &  (' You have no convergence for less then 50% of sublattices')
        end if
        write(6,*)
      else if( iconverge.ne.0 .and. itmax > 1 ) then

         write(6,'(a,i3,a)') '     Using best coherent potential in ',  &
     &          nitcpa,' iterations.'

      end if

!c     ===========================================================
!c     cpa iteration converged or the case of an ordered system...
!c     write(6,'('' gettau:: cpa converged : calling getdos'')')
!c     ===========================================================
!c     calculate the density of states n(e).......................
!c     -----------------------------------------------------------

!      return

      if( (istop=='DOS_CALC'.or.istop=='BSF_CALC'                       &
     & .or.istop=='DOSBSF_CALC')) then

        call addlloydcorr(natom,nbasis,iorig,itype,komp,numbsub,atcon,  &
     &   ispin,nspin,energy,efermi,kkrsz,lmax,nrelv,cotdl,almat,tab,    &
     &   tcpatmp,tau00,                                                 &
     &   lloydms,green00,alat,volume,lloydform )
!        write(6,*) 'lloydms = ', lloydms, 'lloydform = ', lloydform

      end if

      if(itmax > 1 .and. iconverge.eq.0 .and. istop/='EBOTCALC') then
!         write(6,'(a,i3,a)') '     Found coherent potential in ',
!     >     nitcpa,' iterations.'
      end if

      if(ispin.eq.2) then
        clghtspin = -clight
      else
        clghtspin =  clight
      end if

      if( (istop /= 'AKEBZ_CALC' .and. istop /= 'FERMI_CALC' .and.      &
     &         istop /= 'BSF_CALC') .or. isdis ) then


        call getdos(nrelv,clghtspin,                                    &
     &         lmax,kkrsz,                                              &
     &         nbasis,                                                  &
     &         komp,                                                    &
     &         iswzj,                                                   &
     &         h,jmt,jws,rr,vr,                                         &
     &         energy,pnrel,                                            &
     &         tau00,tcpa,tab,                                          &
     &         pzz,pzj,                                                 &
     &         pzzck,pzjck,                                             &
     &         dos,dosck,                                               &
     &         green,                                                   &
     &         w1,w2,w3,w4,                                             &
     &         iprint,ivpoint,ikpoint,istop)

!        write(6,*) 'dos = '
!        write(6,*) dos(0:iplmax,1,1)
!        write(6,*)

        tdos = 0.d0
        do i = 1, nbasis
        do j = 1, komp(i)
        do k = 0, lmax
          tdos = tdos + dimag(dos(k,j,i)*atcon(j,i)*numbsub(i))
        end do; end do; end do
        lastdos = tdos

      end if

      if( istop == 'BSF_CALC' .or. istop == 'DOSBSF_CAL' ) then

        allocate( greenks(natom*kkrsz,natom*kkrsz) )

        do i = 2, bsf_nkwaypts

          ds = ( bsf_kwaypts(1,i) - bsf_kwaypts(1,i-1) ) *              &
     &         ( bsf_kwaypts(1,i) - bsf_kwaypts(1,i-1) ) +              &
     &         ( bsf_kwaypts(2,i) - bsf_kwaypts(2,i-1) ) *              &
     &         ( bsf_kwaypts(2,i) - bsf_kwaypts(2,i-1) ) +              &
     &         ( bsf_kwaypts(3,i) - bsf_kwaypts(3,i-1) ) *              &
     &         ( bsf_kwaypts(3,i) - bsf_kwaypts(3,i-1) )
          ds = dsqrt(ds)
          n = ceiling(ds*bsf_ksamplerate)
          dx = 1.d0/n

          x = dx
          if( i == 2 ) then
            n = n + 1; x = 0.d0; y = -dx*ds
          end if

          write(bsf_fileno,'(a,3f7.3)') '# from ', bsf_kwaypts(:,i-1)
          write(bsf_fileno,'(a,3f7.3)') '#  to  ', bsf_kwaypts(:,i)

          ! debug warning
!cDEBUG          isdis = .true.
          if( isdis ) then

            do j = 1, n

              kpnt(:) = (1.d0-x)*bsf_kwaypts(:,i-1)                     &
     &                  + x*bsf_kwaypts(:,i)
              x = x + dx
              y = y + dx*ds

              rayp = x
              call getblochspecfn(                                      &
     &          natom,nbasis,iorig,itype,komp,numbsub,atcon,            &
     &          energy,kkrsz,tab,tcpatmp,tau00,pzzbl,pzj,               &
     &          bsf_kwaypts(:,i-1),bsf_kwaypts(:,i),rayp,kpnt,bsf,bsfNL,&
     &          bsfcubic,bsfdis,alat,                                   &
     &          lmax,aij,mapstr,mappnt,mapij,powe,ij3,nj3,cgaunt,       &
     &          cfac,edu,pdu,rns,ndimrs,np2r,ndimnp,numbrs,naij,        &
     &         dqint1,iprij,ndx,hplnm,ndimrhp,ndimlhp,irecdlm,d00,eoeta,&
     &          eta0,xknlat(1:nkns(imesh),:,imesh),nkns(imesh),         &
     &          conr,greenks,nop,rot,dop,if0)
              write(bsf_fileno,'(3f20.10)')                             &
     &               y, real(energy)-efermi, bsf

!  uncomment to show cubic projection of A(k,E)
!   ... be sure to add advance='no' to above write
!              do in=1,nbasis
!              do il=1,kkrsz
!              if(in/=nbasis.or.il/=kkrsz) then
!                write(bsf_fileno,'(1f15.7)',advance='no') bsfcubic(il,in)
!              end if; end do; end do
!              write(bsf_fileno,'(1f15.7)') bsfcubic(kkrsz,nbasis)

!              do in=1,nbasis
!              do il=0,lmax
!              if(in/=nbasis.or.il/=lmax) then
!                write(bsf_fileno,'(1f15.7)',advance='no') bsfNL(il,in)
!              end if; end do; end do
!              write(bsf_fileno,'(1f15.7)') bsfNL(lmax,nbasis)


            end do

          else

            ! for ordered crystal, just find ||tau(k,E)^-1|| = 0
            call findblochstate(                                        &
     &        natom,nbasis,iorig,itype,komp,numbsub,atcon,              &
     &        energy,kkrsz,tab,tcpatmp,tau00,pzzbl,pzj,                 &
     &        bsf_kwaypts(:,i-1),bsf_kwaypts(:,i),roots(1),nroots,alat, &
     &        lmax,aij,mapstr,mappnt,mapij,powe,ij3,nj3,cgaunt,         &
     &        cfac,edu,pdu,rns,ndimrs,np2r,ndimnp,numbrs,naij,          &
     &        dqint1,iprij,ndx,hplnm,ndimrhp,ndimlhp,irecdlm,d00,eoeta, &
     &        eta0,xknlat(1:nkns(imesh),:,imesh),nkns(imesh),           &
     &        conr,greenks,nop,rot,dop,if0)

            do j = 1, nroots
              x = roots(j)
              write(bsf_fileno,'(2f20.10)') y+x*ds, real(energy)-efermi
            end do
            y = y + ds

          end if

        end do
!        write(6,'(a,f7.3,a)') '     Completed A(k,E) scan at '
!     >   //'@ E = (',real(energy),')'
        write(bsf_fileno,*) ''

        deallocate( greenks )

      end if

      if( istop == 'AKEBZ_CALC' ) then

        fermi_fileno = fermi_filenos(ispin)

        ! first make a gnuplot script for easy viewing
        if(nspin == 1) then
          open(unit=3,file='akeBZarea.gnu',status='replace',iostat=iec)
        else
          if(ispin==1) then
        open(unit=3,file='akeBZarea_up.gnu',status='replace',iostat=iec)
          else
        open(unit=3,file='akeBZarea_dn.gnu',status='replace',iostat=iec)
          end if
        end if
        write(3,'(a)') 'unset xtics; unset ytics'
        write(3,'(a)') 'unset colorbox; unset key; unset border'
        write(3,'(a)') 'set size square'
        write(3,'(a)') 'set pm3d map'
        write(3,'(a)') 'set palette define (0 "white", 100 "black"'     &
     &    //', 1000 "black")'
        write(3,'(a)') 'set title "A(k,Efermi) plot" offset 0.0,1.0'
        write(3,'(a)') 'set zrange [0:1000]'

         allocate( greenks(natom*kkrsz,natom*kkrsz) )

         xyang = 0.d0
         do i = 2, fermi_nkpts

            write(fermi_fileno,'(a,3f7.3)') '# from ', fermi_kpts(:,i-1)
            write(fermi_fileno,'(a,3f7.3)') '#  to  ', fermi_kpts(:,i)

            edge1(:) = -fermi_korig(:) + fermi_kpts(:,i-1)
            edge2(:) = -fermi_korig(:) + fermi_kpts(:,i)

            e1len = dsqrt(dot(edge1,edge1))
            e2len = dsqrt(dot(edge2,edge2))

            norm = cross(edge1,edge2)
            sint = dsqrt(dot(norm,norm))/(e1len*e2len)
            th = dasin(sint)

            ! for gnuplot script

            if( i == 2 ) then

              write(3,'(a,a,a,f7.3,a,f7.3,a)') 'set label "',           &
     &          trim(fermi_orilabel),'" at ',                           &
     &          0.d0,',',0.d0,                                          &
     &          ' front center offset 0.0,-1.0'

              write(3,'(a,a,a,f7.3,a,f7.3,a)') 'set label "',           &
     &          trim(fermi_kptlabel(1)),'" at ',                        &
     &          e1len,',',0.d0,                                         &
     &          ' front center offset 0.0,-1.0'

              write(3,'(a,f7.3,a,f7.3,a)')                              &
     &          'set arrow from 0.0,0.0 to ',                           &
     &          e1len,',',0.d0,                                         &
     &          ' front nohead'

            end if

            write(3,'(a,a,a,f7.3,a,f7.3,a,f7.3,a,f7.3)')                &
     &        'set label "',                                            &
     &        trim(fermi_kptlabel(i)),'" at ',                          &
     &        e2len*dcos(xyang+th),',',e2len*dsin(xyang+th),            &
     &        ' front center offset ',                                  &
     &        dcos(xyang+th),',',dsin(xyang+th)

            write(3,'(a,f7.3,a,f7.3,a)')                                &
     &        'set arrow from 0.0,0.0 to ',                             &
     &        e2len*dcos(xyang+th),',',e2len*dsin(xyang+th),            &
     &        ' front nohead'

            write(3,'(a,f7.3,a,f7.3,a,f7.3,a,f7.3,a)')                  &
     &        'set arrow from ',                                        &
     &        e1len*dcos(xyang),',',e1len*dsin(xyang),' to ',           &
     &        e2len*dcos(xyang+th),',',e2len*dsin(xyang+th),            &
     &        ' front nohead'

            if( fermi_gridsampling < 10 ) then
              write(0,'(a,i5,a)') 'warning: ake samples/(2pi/a) = ',    &
     &          fermi_gridsampling, ' is low'
            end if

            e1ndiv = ceiling( e1len*fermi_gridsampling*dsqrt(sint) )
            du = 1.d0/e1ndiv

            u = 0.d0
            do n = 1, e1ndiv
              u = u + du

              e2ndiv = ceiling( e2len*(1.d0-u)*                         &
     &          fermi_gridsampling*dsqrt(sint) )
              dv = (1.d0-u)/e2ndiv

              startk = fermi_korig + u*edge1
              endk   = fermi_korig + u*edge1 + (1.d0-u)*edge2

              v = 0.d0
              do m = 1, e2ndiv
                v = v + dv
                if( u + v > 1.0d0 ) exit

                displ = u*edge1 + v*edge2
                dllen = dsqrt(dot(displ,displ))
                dlang = dacos(dot(displ,edge1)/(dllen*e1len))
                x = dllen * dcos(xyang + dlang)
                y = dllen * dsin(xyang + dlang)
                rayp = v/(1.0-u)

                kpnt  = fermi_korig + displ
                call getblochspecfn(                                    &
     &           natom,nbasis,iorig,itype,komp,numbsub,atcon,           &
     &           energy,kkrsz,tab,tcpatmp,tau00,pzzbl,pzj,              &
     &           startk,endk,rayp,kpnt,bsf,bsfNL,bsfcubic,bsfdis,alat,  &
     &           lmax,aij,mapstr,mappnt,mapij,powe,ij3,nj3,cgaunt,      &
     &           cfac,edu,pdu,rns,ndimrs,np2r,ndimnp,numbrs,naij,       &
     &         dqint1,iprij,ndx,hplnm,ndimrhp,ndimlhp,irecdlm,d00,eoeta,&
     &           eta0,xknlat(1:nkns(imesh),:,imesh),nkns(imesh),        &
     &           conr,greenks,nop,rot,dop,if0)
                write(fermi_fileno,*) x, y, bsf

              end do
              write(fermi_fileno,*) ''

            end do

            xyang = xyang + th

         end do
         deallocate( greenks )

         if(nspin == 1) then
           write(3,'(a)') 'splot "./akeBZarea.dat"'
         else
           if(ispin == 1) then
             write(3,'(a)') 'splot "./akeBZarea_up.dat"'
           else
             write(3,'(a)') 'splot "./akeBZarea_dn.dat"'
           end if
         end if

         close(unit=3)

      end if

      if( istop == 'FERMI_CALC' ) then

        fermi_fileno = fermi_filenos(ispin)

        ! temporary debugging switch
!        isdis = .true.

        ! first make a gnuplot script for easy viewing
        if( isdis ) then
          flb = 3; fub = 4
        else
          flb = 4; fub = 4
        end if

        if(nspin == 1) then
          if(isdis) then
           open(unit=3,file='akeBZrays.gnu',status='replace',iostat=iec)
          end if
          open(unit=4,file='fermi.gnu',status='replace',iostat=iec)
        else
          if(ispin==1) then
            if( isdis ) then
              open(unit=3,file='akeBZrays_up.gnu',                      &
     &                     status='replace',iostat=iec)
            end if
            open(unit=4,file='fermi_up.gnu',                            &
     &                     status='replace',iostat=iec)
          else
            if( isdis ) then
              open(unit=3,file='akeBZrays_dn.gnu',                      &
     &                    status='replace',iostat=iec)
            end if
            open(unit=4,file='fermi_dn.gnu',                            &
     &                    status='replace',iostat=iec)
          end if
        end if

        xmin = 0.d10; xmax = 0.d10
        ymin = 0.d10; ymax = 0.d10

        do n = flb,fub
          write(n,'(a)') 'unset xtics; unset ytics; unset ztics'
          write(n,'(a)') 'unset colorbox; unset key; unset border'
          write(n,'(a)') 'set size square'
          write(n,'(a)') 'set title "Fermi surface" offset 0.0,1.0'
        end do

         allocate( greenks(natom*kkrsz,natom*kkrsz) )

         xyang = 0.d0
         do i = 2, fermi_nkpts

            write(fermi_fileno,'(a,3f7.3)') '# from ', fermi_kpts(:,i-1)
            write(fermi_fileno,'(a,3f7.3)') '#  to  ', fermi_kpts(:,i)

            edge1(:) = -fermi_korig(:) + fermi_kpts(:,i-1)
            edge2(:) = -fermi_korig(:) + fermi_kpts(:,i)

            e1len = dsqrt(dot(edge1,edge1))
            e2len = dsqrt(dot(edge2,edge2))

            norm = cross(edge1,edge2)
            sint = dsqrt(dabs(dot(norm,norm)))/(e1len*e2len)
            if( dot(edge1,edge2) > 0.d0 ) then
              th = dasin(sint)
            else
              th = pi - dasin(sint)
            end if

            ! for gnuplot script

            if( i == 2 ) then

              do n = flb,fub

                write(n,'(a,a,a,f7.3,a,f7.3,a)') 'set label "',         &
     &            trim(fermi_orilabel),'" at ',                         &
     &            0.d0,',',0.d0,                                        &
     &            ' front center offset 0.0,-1.0'

                write(n,'(a,a,a,f7.3,a,f7.3,a)') 'set label "',         &
     &            trim(fermi_kptlabel(1)),'" at ',                      &
     &            e1len,',',0.d0,                                       &
     &            ' front center offset 0.0,-1.0'

                write(n,'(a,f7.3,a,f7.3,a)')                            &
     &            'set arrow from 0.0,0.0 to ',                         &
     &            e1len,',',0.d0,                                       &
     &            ' front nohead'

              end do
              if( e1len > xmax ) xmax = e1len

            end if

            do n = flb,fub
             write(n,'(a,a,a,f7.3,a,f7.3,a,f7.3,a,f7.3)')               &
     &        'set label "',                                            &
     &        trim(fermi_kptlabel(i)),'" at ',                          &
     &        e2len*dcos(xyang+th),',',e2len*dsin(xyang+th),            &
     &        ' front center offset ',                                  &
     &        dcos(xyang+th),',',dsin(xyang+th)
             write(n,'(a,f7.3,a,f7.3,a)')                               &
     &        'set arrow from 0.0,0.0 to ',                             &
     &        e2len*dcos(xyang+th),',',e2len*dsin(xyang+th),            &
     &        ' front nohead'
             write(n,'(a,f7.3,a,f7.3,a,f7.3,a,f7.3,a)')                 &
     &        'set arrow from ',                                        &
     &        e1len*dcos(xyang),',',e1len*dsin(xyang),' to ',           &
     &        e2len*dcos(xyang+th),',',e2len*dsin(xyang+th),            &
     &        ' front nohead'
            end do

            ! recompute bounding box
            if(e2len*dcos(xyang+th) > xmax) xmax = e2len*dcos(xyang+th)
            if(e2len*dcos(xyang+th) < xmin) xmin = e2len*dcos(xyang+th)
            if(e2len*dsin(xyang+th) > ymax) ymax = e2len*dsin(xyang+th)
            if(e2len*dsin(xyang+th) < ymin) ymin = e2len*dsin(xyang+th)

            nthdiv = ceiling( fermi_raysampling*th )
            raysamplerate = fermi_gridsampling
            du = 1.d0/nthdiv

            if( i == 2 ) then
              u = -du; nthdiv = nthdiv + 1
            else
              u = 0.d0
            end if
            do n = 1, nthdiv
              u = u + du

              displ = (1.0-u)*edge1 + u*edge2
              dllen = dsqrt(dot(displ,displ))
              nraydiv = ceiling(raysamplerate * dllen)
              dv = 1.d0/nraydiv

              startk = fermi_korig
              endk   = fermi_korig + displ

              if( isdis ) then

                v = 0.d0
                do m = 1, nraydiv
                  v = v + dv

                  displ = v*( (1.0-u)*edge1 + u*edge2 )
                  dllen = dsqrt(dot(displ,displ))
      dlang = dacos(max(min(dot(displ,edge1)/(dllen*e1len),1.d0),-1.d0))
                  x = dllen * dcos(xyang + dlang)
                  y = dllen * dsin(xyang + dlang)
                  rayp = v

                  kpnt  = fermi_korig + displ
                  call getblochspecfn(                                  &
     &             natom,nbasis,iorig,itype,komp,numbsub,atcon,         &
     &             energy,kkrsz,tab,tcpatmp,tau00,pzzbl,pzj,            &
     &             startk,endk,rayp,kpnt,bsf,bsfNL,bsfcubic,bsfdis,alat,&
     &             lmax,aij,mapstr,mappnt,mapij,powe,ij3,nj3,cgaunt,    &
     &             cfac,edu,pdu,rns,ndimrs,np2r,ndimnp,numbrs,naij,     &
     &         dqint1,iprij,ndx,hplnm,ndimrhp,ndimlhp,irecdlm,d00,eoeta,&
     &             eta0,xknlat(1:nkns(imesh),:,imesh),nkns(imesh),      &
     &             conr,greenks,nop,rot,dop,if0)
                  write(fermi_fileno,'(4f15.7)')                        &
     &                   x, y, bsf

!  uncomment to show cubic projection of A(k,E)
!   ... be sure to add advance='no' to above write
!                  do in=1,nbasis
!                  do il=1,kkrsz
!                  if(in/=nbasis.or.il/=kkrsz) then
!                    write(fermi_fileno,'(1f15.7)',advance='no') bsfcubic(il,in)
!                  end if; end do; end do
!                  write(fermi_fileno,'(1f15.7)') bsfcubic(kkrsz,nbasis)
!
                end do

              else

                ! for ordered crystal, just find ||tau(k,E)^-1|| = 0
                call findblochstate(                                    &
     &             natom,nbasis,iorig,itype,komp,numbsub,atcon,         &
     &             energy,kkrsz,tab,tcpatmp,tau00,pzzbl,pzj,            &
     &             startk,endk,roots(1),nroots,alat,                    &
     &             lmax,aij,mapstr,mappnt,mapij,powe,ij3,nj3,cgaunt,    &
     &             cfac,edu,pdu,rns,ndimrs,np2r,ndimnp,numbrs,naij,     &
     &         dqint1,iprij,ndx,hplnm,ndimrhp,ndimlhp,irecdlm,d00,eoeta,&
     &             eta0,xknlat(1:nkns(imesh),:,imesh),nkns(imesh),      &
     &             conr,greenks,nop,rot,dop,if0)

                do j = 1, nroots
                  v = roots(j)
                  displ = v*( (1.0-u)*edge1 + u*edge2 )
                  dllen = dsqrt(dot(displ,displ))
                  dlang = dacos(dot(displ,edge1)/(dllen*e1len))
                  x = dllen * dcos(xyang + dlang)
                  y = dllen * dsin(xyang + dlang)
                  write(fermi_fileno,*) x, y
                end do

              end if
              write(fermi_fileno,*) ''

            end do

            xyang = xyang + th

         end do
         deallocate( greenks )

         del = 0.05
         do n = flb,fub
           write(n,'(a,f7.3,a,f7.3,a)')                                 &
     &       'set xrange [',xmin-del,':',xmax+del,']'
           write(n,'(a,f7.3,a,f7.3,a)')                                 &
     &       'set yrange [',ymin-del,':',ymax+del,']'
         end do

         if(nspin == 1) then
           if( isdis ) then
             write(3,'(a)') 'splot "./akeBZrays.dat" w l'
           else
             write(3,'(a)') 'plot "./akeBZrays.dat"'
           end if
         else
           if(ispin == 1) then
             if( isdis ) then
               write(3,'(a)') 'splot "./akeBZrays_up.dat" w l'
             else
               write(3,'(a)') 'plot "./akeBZrays_up.dat"'
             end if
           else
             if( isdis ) then
               write(3,'(a)') 'splot "./akeBZrays_dn.dat" w l'
             else
               write(3,'(a)') 'plot "./akeBZrays_dn.dat"'
             end if
           end if
         end if

         ! Find peaks of A(k,E) to get the Fermi Surface
         ! find approximate fermi surface for disordered structure
         if( isdis ) then

         close(unit=3)
         close(unit=fermi_fileno)


         if( nspin == 1) then
           open(unit=fermi_fileno,file='akeBZrays.dat',                 &
     &                 status='old',iostat=iec)
           open(unit=94,file='fermi.dat',status='replace',iostat=iec)
         end if
         if( nspin == 2 ) then
          if( ispin == 1 ) then
           open(unit=fermi_fileno,file='akeBZrays_up.dat',              &
     &                    status='old',iostat=iec)
           open(unit=94,file='fermi_up.dat',status='replace',iostat=iec)
          else
           open(unit=fermi_fileno,file='akeBZrays_dn.dat',              &
     &                     status='old',iostat=iec)
           open(unit=94,file='fermi_dn.dat',status='replace',iostat=iec)
          end if
         end if

         do ! <-- infinite loop terminates on eof

           ! ignore comments and white-space to next ray
           n = 0
           do while( n == 0 )
             read(fermi_fileno,'(a70)',end=2001) akestring
             n = verify(akestring,' ')
             if( n/=0 ) then
               if( akestring(n:n) == '#' ) n=0
             end if
           end do

           ! read AkE ray until next comment or white-space
           n = 0; m = 1
           do while( m /= 0 )
             n = n + 1
             read(akestring,*) akeray(1:3,n)
             read(fermi_fileno,'(a70)',end=2000) akestring
             m = verify(akestring,' ')
             if( m/=0 ) then
               if( akestring(m:m) == '#' ) m=0
             end if
           end do

           ! pick out local maxima and corresponding FWHM
2000       ips = 4
           do m = ips+1, n-ips

             ! must be max over this span to qualify as peak
             ispeak = .true.
             do i = 1, ips
               if( akeray(3,m-i) > akeray(3,m-i+1) .or.                 &
     &             akeray(3,m+i) > akeray(3,m+i-1) ) then
                  ispeak = .false.; exit
               end if
             end do
!             if( akeray(3,m) < 1.0d0 ) ispeak = .false.

             if( ispeak ) then
               write(94,*) akeray(1:2,m)

!              .. change for cu3au paper
               halfmax = akeray(3,m)/2.d0
!              halfmax = 10.d0

               do iL = m, 1, -1
                 if( akeray(3,iL) < halfmax ) exit
               end do
               do iR = m, n
                 if( akeray(3,iR) < halfmax ) exit
               end do
               write(4,'(a,f7.3,a,f7.3,a,f7.3,a,f7.3,a)')               &
     &           'set arrow from ',akeray(1,iL),',',                    &
     &           akeray(2,iL),' to ',akeray(1,iR),',',akeray(2,iR),     &
     &           ' heads size 0.005,90'

               ! tmp -- for cu3au paper
!               write(95,*) akeray(1:2,iL)
!               write(96,*) akeray(1:2,iR)
               !!!
             end if

           end do

         end do

         end if ! isdis -- debugging

2001     if( nspin == 1 ) then
           write(4,'(a)') 'plot "fermi.dat" w p'
         else if( ispin == 1 ) then
           write(4,'(a)') 'plot "fermi_up.dat" w p'
         else
           write(4,'(a)') 'plot "fermi_dn.dat" w p'
         end if

         ! close fermi.dat, fermi.gnu, akeBZlo.dat
         close(unit=fermi_fileno)
         close(unit=4); close(unit=94)

      end if

      if(itmax > 1.or.istop=='DOSBSF_CAL') then
!        write(6,*) ''
      end if

      time1 = time2
      call timel(time2)

      if(iprint.ge.0.and.nitcpa.gt.1) then
       write(6,*) sname//' time  ='                                     &
     &           ,real(time2-time1),' NITCPA=',nitcpa
      else if(nitcpa.gt.20.and.iprint.ge.-1.and.itmax>0) then
       write(6,*) ' Number of CPA iterations is ',nitcpa
      end if

!c           -----------------------------------------------------------
            if(istop.eq.sname) then
               call fstop(sname)
            endif
!c

      firstcall = .false.
      return
      end subroutine gettau
