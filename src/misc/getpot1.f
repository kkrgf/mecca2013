!      subroutine getpot7(nspin,nbasis,alat,efpot,komp,                  &
!     &                  atcon,numbsub,                                  &
!     &                  jmt,jws,rmtstr,rws,xr,rr,h,xstart,              &
!     &                  rmt,                                            &
!     &                  vr,v0i,                                         &
!     &                  rhotot,corden,xvalsws,ztot,zcor,                &
!     &                  numc,nc,lc,kc,ec,eBottom,                       &
!     &                  header,                                         &
!     &                  mtasa,iprint,istop)
!c     cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      subroutine getpot1(nspin,nbasis,efpot,                            &
     &                 komp,jmt,jws,rmt_ovlp,rmt,rws,r_circ,            &
     &                 xr,rr,h,                                         &
     &                 xstart,vr,vdif,                                  &
     &                 rhotot,corden,xvalsws,ztot,zcor,zvals,zcors,     &
     &                 numc,nc,lc,kc,ec,ebot,                           &
     &                 ibot_cont,iset_b_cont,mtasa,                     &
     &                 isDLM,isflipped,flippoint,                       &
     &                 iprint,istop)
!c     ==================================================================
      USE mecca_constants
      USE mecca_types
      USE common_rho_hsk, only : r_hsk,rho_hsk,dealloc_rho_hsk
      implicit real*8 (a-h,o-z)
      integer :: nspin
      integer :: nbasis
      real(8) :: efpot
      integer   komp(nbasis)
      integer   jws(ipcomp,nbasis)
      integer   jmt(ipcomp,nbasis)
      real*8    rmt_ovlp(ipcomp,nbasis)
      real*8    rmt(ipcomp,nbasis)
      real*8    rws(ipcomp,nbasis)
      real*8    r_circ(ipcomp,nbasis)
      real*8    xr(iprpts,ipcomp,nbasis)
      real*8    rr(iprpts,ipcomp,nbasis)
      real*8    h(ipcomp,nbasis)
      real*8    xstart(ipcomp,nbasis)
      real*8    vr(iprpts,ipcomp,ipsublat,nspin)
      real(8) :: vdif
      real*8    rhotot(iprpts,ipcomp,ipsublat,nspin)
      real*8    corden(iprpts,ipcomp,ipsublat,nspin)
      real*8    xvalsws(ipcomp,ipsublat,nspin)
      real*8    ztot(ipcomp,nbasis)
      real*8    zcor(ipcomp,nbasis)
      real*8    zvals(ipcomp,nbasis)
      real*8    zcors(ipcomp,nbasis)
      integer   numc(ipcomp,ipsublat,nspin)
      integer   nc(ipeval,ipcomp,ipsublat,nspin)
      integer   lc(ipeval,ipcomp,ipsublat,nspin)
      integer   kc(ipeval,ipcomp,ipsublat,nspin)
      real*8    ec(ipeval,ipcomp,ipsublat,nspin)
      real(8) :: ebot
      integer   ibot_cont,iset_b_cont
      integer    mtasa
      logical :: isDLM(nbasis)
      logical :: isflipped(nbasis)
      integer :: flippoint(nbasis)
      integer :: iprint
      character(10) :: istop
      character(:), parameter :: sname='getpot1'
      logical :: open22
      integer :: ipot_read
!      integer   nspinp,nspin
!      integer   num,niter
!      integer   natin,nbasin,ityp1(natom)
!C      integer   natom,nbasis,ityp1(natom)
!c
!      real*8    rASA(ipcomp,nbasis)
!      real*8    X(ipcomp,nbasis)
!      real*8    xmom(ipcomp,nbasis)
!      real*8    rslatt(3,3)          ! In units of Alat/(2*pi)
!      real*8    pos(3,natom)
!      real*8    transV(3,3)
!      real*8    basisV(3,natom)
!      real*8    vdif,vdif0,alp,alat,volume,ebot

!      character(80) header

!c---------------------------------------------------------------
!c      Zeroout working arrays
!c---------------------------------------------------------------
      ipot_read = 0
      vr = zero
      rhotot = zero
      corden = zero

!CALAM  ***************************************************************
!c       Read in the starting potential and charge densities for
!c             various components, sublattices and spins.
!c       For spin polarized calc. vdif measures the difference between
!c       mt-zero for maj. and min. spin electrons (assume vmtz(maj)=0)
!CALAM  ***************************************************************

!CDDJ   Allow user to supply Ef guess in input for starting potential
       efpotnk=0.0d0
        if(efpot.gt.0.0001d0) then
           efpotnk=efpot
         write(6,'("  Use user-supplied guess: Ef =",f10.6,/)') efpot
        endif

      inquire(unit=22,opened=open22 )
      if ( .not. open22 ) go to 100
!      read(22,'(a80)',end=100) header
      read(22,'(a80)',end=100)
      read(22,*,err=100,end=100)nspinp,vdif
      if(nspinp.gt.ipspin) then
        write(6,'(''   nspinp .gt. ipspin '', 2i5)')nspinp,ipspin
        go to 100
      endif
!c
      write(6,'(/''  Read starting potentials and charge densities'')')
      ipot_read=1             ! POT file exists
      iset_b_cont=0           ! POT file exists
      iefpot = 0
      do nsub=1,nbasis
        do nk=1,komp(nsub)
          do ns=1,nspinp

!cSUFF automatic DLM and Patterned magnetism file

             if( .not. isflipped(nsub) .and.                            &
     &             (.not. isDLM(nsub) .or. nk <= komp(nsub)/2) ) then
                  initialize=0
                  write(6,'(''   Sublat.='',i2,'' Spin='',i2,           &
     &            '' Comp.='',i2,'' V(r)'',t40,''is READ'')') nsub,ns,nk
!c                ------------------------------------------------------
                 call potred(jmt(nk,nsub),rmt(nk,nsub),jws(nk,nsub),    &
     &                       rws(nk,nsub),xr(:,nk,nsub),                &
     &                       rr(:,nk,nsub),h(nk,nsub),                  &
     &                       xstart(nk,nsub),vr(:,nk,nsub,ns),          &
     &                       rhotot(:,nk,nsub,ns),corden(:,nk,nsub,ns), &
     &                       xvalsws(nk,nsub,ns),                       &
     &                       ztot(nk,nsub),zcor(nk,nsub),zvals(nk,nsub),&
     &                       zcors(nk,nsub),numc(nk,nsub,ns),           &
     &                       nc(:,nk,nsub,ns),lc(:,nk,nsub,ns),         &
     &                       kc(:,nk,nsub,ns),ec(:,nk,nsub,ns),         &
     &                       alat,efpotnk,initialize,ebot,              &
     &                       ibot_cont,iset_b_cont,mtasa,iprint,istop)
!c               -----------------------------------------------------
             else
                  initialize=0
                  write(6,'(''   Sublat.='',i2,'' Spin='',i2,           &
     &         '' Comp.='',i2,'' V(r)'',t40,''is COPIED'')') nsub,ns,nk

                  if( isDLM(nsub) .and. nk > komp(nsub)/2 )  then
                     nk0 = 1+mod(nk-1,komp(nsub)/2); nsub0 = nsub
                  else
                     nk0 = nk; nsub0 = flippoint(nsub)
                  end if

                  if( ns == 1 ) ns0 = 2
                  if( ns == 2 ) ns0 = 1

                  jmt(nk,nsub) = jmt(nk0,nsub0)
                  rmt(nk,nsub) = rmt(nk0,nsub0)
                  jws(nk,nsub) = jws(nk0,nsub0)
                  xr(:,nk,nsub) = xr(:,nk0,nsub0)
                  rr(:,nk,nsub) = rr(:,nk0,nsub0)
                  h(nk,nsub) = h(nk0,nsub0)
                  xstart(nk,nsub) = xstart(nk0,nsub0)
                  vr(:,nk,nsub,ns) = vr(:,nk0,nsub0,ns0)
                  rhotot(:,nk,nsub,ns) = rhotot(:,nk0,nsub0,ns0)
                  corden(:,nk,nsub,ns) = corden(:,nk0,nsub0,ns0)
                  xvalsws(nk,nsub,ns) = xvalsws(nk0,nsub0,ns0)
                  ztot(nk,nsub) = ztot(nk0,nsub0)
                  zcor(nk,nsub) = zcor(nk0,nsub0)
                  zvals(nk,nsub) = zvals(nk0,nsub0)
                  zcors(nk,nsub) = zcors(nk0,nsub0)
                  numc(nk,nsub,ns) = numc(nk0,nsub0,ns0)
                  nc(:,nk,nsub,ns) = nc(:,nk0,nsub0,ns0)
                  lc(:,nk,nsub,ns) = lc(:,nk0,nsub0,ns0)
                  kc(:,nk,nsub,ns) = kc(:,nk0,nsub0,ns0)
                  ec(:,nk,nsub,ns) = ec(:,nk0,nsub0,ns0)

             end if

                if(iefpot.eq.0) then
                 efpot = efpotnk
                 write(6,*) 'efpot = ', efpot
                 iefpot = 1
                end if
            enddo
         enddo
      enddo

!C=======  Reading of rV(r) and 4*pi*r^2*rho(r)   COMPLETE  ==========
100   continue
      if ( ipot_read <= 0) then  ! to generate a starting POT
       write(6,'(''====================================================&
     &=============='')')
       write(6,'(/''Generate starting potentials...'')')
!CALAM================================================================
!C    Interpolate this new overlapping charge density from the
!C    herman-skillman grid [rhsk] to our logarithmic radial grid [rr]
!C    Hence calculate the potential corresponding to this new rho
!CALAM================================================================
       if(efpot.lt.0.0001d0)efpot = 0.65d0
                                          ! initial guess (OK for 3D TM metal)
       write(6,'('' GETPOT_OVLP: Use INIOVLP_POT to guess a starting pot&
     & from the '')')
       write(6,'(''             overlapping atomic charge density'')')
       write(6,'(/a,f10.7)') 'Made Fermi level guess of = ', efpot

       do ns=1,nspinp
        do nsub=1,nbasis
         do nk=1,komp(nsub)
          if ( iprint>=0 ) then
           write(6,*)
           write(6,'(''   Sublat.='',i2,'' Spin='',i2,                  &
     &               '' Comp.='',i2)') nsub,ns,nk
          end if
          call iniovlp_pot(jmt(nk,nsub),rmt(nk,nsub),                   &
     &                  jws(nk,nsub),rws(nk,nsub),                      &
     &                  xr(:,nk,nsub),rr(:,nk,nsub),h(nk,nsub),         &
     &                  xstart(nk,nsub),ztot(nk,nsub),zcor(nk,nsub),    &
     &                  r_hsk(:,nk,nsub),rho_hsk(:,ns,nk,nsub),         &
     &                  vr(:,nk,nsub,ns),corden(:,nk,nsub,ns),          &
     &                  rhotot(:,nk,nsub,ns),xvalsws(nk,nsub,ns),       &
     &                  numc(nk,nsub,ns),nc(:,nk,nsub,ns),              &
     &                  lc(:,nk,nsub,ns),kc(:,nk,nsub,ns),              &
     &                  ec(:,nk,nsub,ns),mtasa,iprint)

         enddo
        enddo
       enddo
      end if

!C====================================================================
!c   Magn. --> Non-Magn.
!C====================================================================
      if(nspin.eq.1.and.nspinp.eq.2) then
       vdif = zero
       do nsub=1,nbasis
         xvalsws(1:komp(nsub),nsub,1)=xvalsws(1:komp(nsub),nsub,1)+     &
     &                                xvalsws(1:komp(nsub),nsub,2)
         rhotot(:,1:komp(nsub),nsub,1)=rhotot(:,1:komp(nsub),nsub,1)+   &
     &                                 rhotot(:,1:komp(nsub),nsub,2)
         corden(:,1:komp(nsub),nsub,1)=corden(:,1:komp(nsub),nsub,1)+   &
     &                                 corden(:,1:komp(nsub),nsub,2)
         vr(:,1:komp(nsub),nsub,1)=0.5d0*(vr(:,1:komp(nsub),nsub,1)+    &
     &                                    vr(:,1:komp(nsub),nsub,2))
       enddo
      end if
!C====================================================================
!c   Non-Mag. --> Magn.
!C====================================================================
      if(nspin.eq.2.and.nspinp.eq.1) then
       if(abs(vdif).lt.1.d-10) then
        vdif = vdif0
        write(6,*)
        write(6,'(''   nspinp .ne. nspin '', 2i5)')nspinp,nspin
        write(6,*) ' vdif=',vdif
        write(6,*) ' Non-magnetic initial potential is used for',       &
     &             ' magnetic case:'
        write(6,*) ' VDIF has been changed to',vdif0
        write(6,*)
       end if
       do nsub=1,nbasis
         xvalsws(1:komp(nsub),nsub,2)=0.5d0*xvalsws(1:komp(nsub),nsub,1)
         xvalsws(1:komp(nsub),nsub,1)=xvalsws(1:komp(nsub),nsub,2)
         numc(1:komp(nsub),nsub,2)=numc(1:komp(nsub),nsub,1)
         nc(:,1:komp(nsub),nsub,2)=nc(:,1:komp(nsub),nsub,1)
         lc(:,1:komp(nsub),nsub,2)=lc(:,1:komp(nsub),nsub,1)
         kc(:,1:komp(nsub),nsub,2)=kc(:,1:komp(nsub),nsub,1)
         ec(:,1:komp(nsub),nsub,2)=ec(:,1:komp(nsub),nsub,1)
         rhotot(:,1:komp(nsub),nsub,1)=rhotot(:,1:komp(nsub),nsub,1)/2
         rhotot(:,1:komp(nsub),nsub,2)=rhotot(:,1:komp(nsub),nsub,1)
         corden(:,1:komp(nsub),nsub,1)=corden(:,1:komp(nsub),nsub,1)/2
         corden(:,1:komp(nsub),nsub,2)=corden(:,1:komp(nsub),nsub,1)
         vr(:,1:komp(nsub),nsub,2) = vr(:,1:komp(nsub),nsub,1) -        &
     &                               vdif*rr(:,1:komp(nsub),nsub)
       enddo
      end if
!c     ===============================================================
      call dealloc_rho_hsk
!c     ===============================================================
      if (istop.eq.sname) then
         call fstop(sname)
      endif
!c
      return

      CONTAINS

      !c
!c     cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      subroutine potred(jmt,rmt,jws,rws,xr,rr,h,xstart,                 &
     &                   vr,                                            &
     &                   rhotot,corden,                                 &
     &                   xvalsws,ztot,zcor,zvals,zcors,                 &
     &                   numc,nc,lc,kc,ec,                              &
     &                   alat,efpot,                                    &
     &                   initialize,ebot,                               &
     &                   ibot_cont,iset_b_cont,mtasa,iprint,istop)
!c     ==================================================================
!c
      implicit real*8 (a-h,o-z)
!c
      character jttl2*80
      character lj(ipeval)*5
      character istop*10
      character sname*10
      character coerr*80
!c
      integer   nc(ipeval)
      integer   lc(ipeval)
      integer   kc(ipeval)
      integer   iset_b_cont
!c
      real*8    ec(ipeval)
      real*8    xr(iprpts)
      real*8    rr(iprpts)
      real*8    xrold(2*iprpts)
      real*8    rrold(2*iprpts)
      real*8    vr(iprpts)
      real*8    vrold(2*iprpts)
      real*8    rhotot(iprpts)
      real*8    corden(iprpts)
      real*8    chgold(2*iprpts)
      real*8    corold(2*iprpts)
      real*8    a(iprpts)
      real*8    b(iprpts)
      real*8    v0
      real*8    qmtnew
      real*8    qmtold
      real*8    tol
      real*8    ebot,ebot_read
!c
!c ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
      parameter (tol=(one/ten)**5)
      parameter (sname='potred')
!c ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!c set number of points up to sphere boundary, i.e. m.t. or a.s.a
!c NOTE: For Total Energy calculation (incrs-1) must be divisible by 4
!c ---------------------------------------------------------------------- !--
!c set log-grid starting point
        xstart=-11.13096740d+00
!c
      if(initialize.eq.0) then
           if(mtasa.le.0) then
            jmt=iprpts-50            ! additional points used in "mixing"
           else
            jmt=iprpts-38            ! additional points used in "mixing"
            jws=jmt
           endif
!c
        xlast=log(rmt)
        h=(xlast-xstart)/(jmt-1)
        do j=1,iprpts
           xr(j)=xstart+(j-1)*h
           rr(j)=exp(xr(j))
!c
!c set number of pts. to w.s. radius
           if(mtasa.le.0 .and. rr(j).lt.rws) jws= j + 1
        enddo

        if(jws.ge.iprpts-1) then
          coerr=' jws greater than iprpts-1'
          write(6,'('' jws='',i3,2x,''iprpts='',i3)') jws,iprpts
          call fstop(sname//':'//coerr)
       endif
      endif
      if(iprint.ge.1) then
        write(6,'(''  potred: jmt,h,rr(1),rmt'',i7,3e14.6)')            &
     &                          jmt,h,rr(1),rmt
      endif
!c
!c initialize changed so not to redo grid
      initialize=1
!c     ----------------------------------------------------------
      call zeroout(vrold,2*iprpts)
!c     ----------------------------------------------------------
      call zeroout(chgold,2*iprpts)
!c     ----------------------------------------------------------
      call zeroout(corold,2*iprpts)
!c     ----------------------------------------------------------
!c     ==========================================================
!c     read in potential deck....................................
      read(22,'(a80)',end=100) jttl2
      read(22,'(f5.0,13x,f12.5,f5.0,f12.5,e20.13)',err=100,end=100)     &
     &          azed,aa,zcd,ebot_read,efpot
      read(22,*) xst,xmt,jmt0
      read(22,'(4e20.13)') (vrold(j),j=1,jmt0)
      read(22,'(35x,e20.13)') v0
      if(iprint.ge.1) then
        write(6,'('' potred: jttl2'',a80)') jttl2
        write(6,'('' potred: azed,aa,zcd,efpot'',4e12.4)')              &
     &                       azed,aa,zcd,efpot
        write(6,'('' potred: xst,xmt,jmt0'',9x,2e20.13,i5)')            &
     &                       xst,xmt,jmt0
        write(6,'('' potred: v0'',e12.4)') v0
      endif
!c
      if(abs(ztot-azed).gt.tol) then
         write(6,'('' potred: ztot.ne.azed:'',2d14.6)')                 &
     &                         ztot,azed
         call fstop(sname)
      endif
      if(abs(zcor-zcd).gt.tol) then
         write(6,'('' WARNING: potred: zcor.ne.zcd; Make it equal'',    &
     &              2d14.6)')                                           &
     &                                 zcor,zcd
        zcor=zcd
        zcors=zcor
        zvals=ztot-zcor
!CDEBUG         call fstop(sname)
      endif
!c     ==========================================================
!c     generate grid for potential read-in ......................
!c     set the energy zero correctly for maj./min. spins.........
      hh=(xmt-xst)/(jmt0-1)
      do j=1,2*iprpts
         xrold(j)=xst+(j-1)*hh
         rrold(j)=exp(xrold(j))
      enddo
      do jj=jmt0+1,2*iprpts
        if(rrold(jj).gt.rmt) then
!c
         a0 = vrold(jmt0)
         a1 = (vrold(jmt0-2)-4.0d0*vrold(jmt0-1)+3.0d0*vrold(jmt0))/    &
     &                             (2*hh)
         a2 = (vrold(jmt0-2)-2.0d0*vrold(jmt0-1)+vrold(jmt0))/          &
     &                             (2*hh*hh)
!c
         a2 =0.5d0*a2
         do j=jmt0+1,jj                  ! "Taylor expansion"
          deltax = xrold(j)-xrold(jmt0)
          vrold(j) = a0 + a1*deltax + a2*deltax*deltax
         end do
         exit
        end if
      end do
      do j=1,2*iprpts
         vrold(j)=vrold(j) - v0*rrold(j)
      enddo
!c
!c     ==========================================================
!c     read in the charge density ...............................
      read(22,'(i5,e20.13)') ntchg,xvalsws
      if(iprint.ge.1) then
         write(6,'(i5,e20.13)') ntchg,xvalsws
      endif
      if(ntchg.gt.0) then
         read(22,'(4e20.13)') (chgold(j),j=1,ntchg)
      endif
!c     read in core state definition ............................
      read(22,'(3i5)') numc,ntcor
      if(iprint.ge.2) then
         write(6,'('' potred: numc,ntcor'',3i5)') numc,ntcor
      endif
      if(numc.gt.0) then
        do j=1,numc
         read(22,'(3i5,f12.5,9x,a5)') nc(j),lc(j),kc(j),ec(j),lj(j)
        enddo
        if(iprint.ge.2) then
          do j=1,numc
          write(6,'('' potred: nc,lc,kc,ec,lj'',3i3,e12.4,2x,a5)')      &
     &        nc(j),lc(j),kc(j),ec(j),lj(j)
          enddo
        endif
      endif
!c     ==========================================================
!c     read in the core density cccccccccccccccccccccccc
      if(ntcor.gt.0) then
         read(22,'(4e20.13)') (corold(j),j=1,ntcor)
      endif
!c     ==========================================================
!c     interpolate chg density,cor density onto rsw mesh
      if(iprint.ge.2) then
         iup=max(jmt0,ntchg)
         iup=max(iup,ntcor)
         write(6,'(''  potred: prior interp:'',2i5)')
         write(6,'(''  potred: number of points iup='',i5)') iup
         write(6,'(''  potred: j,r,v(r),chgold(r),corold(r)'')')
         write(6,'(i4,4e12.4)')                                         &
     &   (j,rrold(j),vrold(j),chgold(j),corold(j),j=1,iup,01)
      endif

      do j=1,jmt
         vr(j)=ylag(xr(j),xrold,vrold,0,3,jmt0,iex)
      enddo
      do j=jmt+1,iprpts
         vr(j)= -v0*rr(j)
      end do
      if(ntchg.gt.0) then
         do j=1,jws
            rhotot(j)=ylag(xr(j),xrold,chgold,0,3,ntchg,iex)
         enddo
      endif
      if(ntcor.gt.0) then
         do j=1,jws
            corden(j)=ylag(xr(j),xrold,corold,0,3,ntcor,iex)
         enddo
      endif

!CAB      if ( abs(alat-aa).gt.1.d-5
      if ((abs(alat-aa).gt.1.d-5.or.mtasa.eq.1)                         &
     &          .and.  ntchg.gt.0)  then

!c
         do j=1,ntchg
           a(j)=(chgold(j)-corold(j))*rrold(j)
         enddo
!c
!c    ===========================================
         call simpun(xrold,a,ntchg,1,b,2*iprpts)
!c    ===========================================
         qmtold=b(jmt0)
!c
         do j=1,jws
           a(j)=(rhotot(j)-corden(j))*rr(j)
         enddo
!c    ===========================================
         call simpun(xr,a,jws,1,b,iprpts)
!c    ===========================================
         qmtnew=b(jmt)
!c
         if(abs(qmtnew).gt.1.d-14) then
          fac=qmtold/qmtnew
         else
          fac = 1.d0
         end if

         deltq = xvalsws*(fac-1.d0)
         xvalsws = xvalsws+deltq
         do j=1,jws
           rhotot(j)=fac*rhotot(j)+(1.0d0-fac)*corden(j)
           x = rr(j)/rws
           vr(j) = vr(j) + deltq*x*(3.d0-x*x)
!c
!c   3/2*(Q^{new}-Q^{old})/R*{1-r^2/(3 R^2)}  -- electrostatic potential
!c   correction due to changes in charges
!c
         enddo
         efpot = efpot + 2.d0*deltq/rws
!c
      endif


!c SUFFIAN, move to input file
        if(ibot_cont.ne.0)then
!c           if ( abs(alat-aa).gt.1.d-5)then
              iset_b_cont = 1
!c           else
!c              iset_b_cont = 0
!c              ebot = ebot_read
!c           endif
        else
              iset_b_cont = 0
!C              ebot = ebot_read
        endif

!c
      if(iprint.ge.1) then
        write(6,'(''  potred: after interp:'',2i5)')
        write(6,'(''  potred: j,r,v(r),vrold(r) '')')
        write(6,'(i4,3e16.7)')(j,rr(j),vr(j),vrold(j),j=1,jws,60)
      endif
!c
!c     ============================================================
      if (istop.eq.sname) then
         call fstop(sname)
      endif

      return
!c
100   coerr =  ' POT-FILE is corrupted????'
      write(6,'('' ztot='',f3.0,(a))') ztot,coerr
      call fstop(sname//':'//coerr)

      return

      end subroutine potred

!CALAM================================================================
!C    Interpolate the new overlapping charge density from the
!C    herman-skillman grid [rhsk] to our logarithmic radial grid
!C    Hence calculate the potential corresponding to this new rho
!CALAM================================================================

      subroutine iniovlp_pot(jmt,rmt,jws,rws,xr,rr,h,xstart,ztot,zcor,  &
     &                  rhsk,rhohsk,vr,corden,rhotot,xvalsws,numc,      &
     &                  nc,lc,kc,ec,mtasa,iprint)

      implicit none
!C
      character sname*10
!c
      integer   jmt,jws,iprint,mtasa,iex,ir,j,numc
      integer   nc(:),lc(:),kc(:)
      real*8    rmt,rws,h,xstart,xlast,ztot,zcor
      real*8    xr(iprpts)
      real*8    rr(iprpts)
      real*8    rhsk(iprpts)
      real*8    rhohsk(iprpts)
      real*8    vr(iprpts)
      real*8    corden(iprpts)
      real*8    rhotot(iprpts)
      real*8    rho1int(iprpts)
      real*8    rho2int(iprpts)
      real*8    vx(iprpts)
      real*8    ec(:)

      real*8    xvalsws,fac,ro3,vhart,dummy,alpha2,ylag

      parameter (sname='iniovlp_pt')
!C===============================================================
!c set number of points up to sphere boundary, i.e. m.t. or a.s.a

!c set log-grid starting point
         xstart=-11.13096740d+00
!c
           if(mtasa.le.0) then
            jmt=iprpts-50            ! additional points used in "mixing"
           else
            jmt=iprpts-38            ! additional points used in "mixing"
            jws=jmt
           endif
!c
         xlast=log(rmt)
         h=(xlast-xstart)/(jmt-1)
         do j=1,iprpts
           xr(j)=xstart+(j-1)*h
           rr(j)=exp(xr(j))
!c
!c set number of pts. to w.s. radius
           if(mtasa.le.0 .and. rr(j).lt.rws) jws= j + 1
         enddo

         if(jws.ge.iprpts-1) then
           write(6,'('' jws='',i3,2x,''iprpts='',i3)') jws,iprpts
           call fstop(sname//':'//' jws greater than iprpts-1')
         endif
         if(iprint.ge.1) then
           write(6,'(''  iniovlp_pot: jmt,h,rr(1),rmt'',i7,3e14.6)')    &
     &                          jmt,h,rr(1),rmt
         endif
!C===================================================================
!C      Interpolate the charge density from the herman-skillman
!C      grid (rhsk) to the logarithmic radial grid as constructed
!C      above.
!C===================================================================
         do j=1,iprpts
            rhotot(j)=ylag(rr(j),rhsk,rhohsk,0,3,iprpts,iex)
         enddo
!C===================================================================
!C       Construct the potential corresponding to new charge density
!C===================================================================
         call qexpup(1,rhotot,iprpts,xr,rho2int)
            fac=rho2int(jws)
            rho2int(jws+1:iprpts)=fac       ! this fixes an error

         call qexpup(0,rhotot,iprpts,xr,rho1int)
            fac=rho1int(jws)
            rho1int(jws+1:iprpts)=fac

         do ir=1,iprpts      !calculate new potential
!c
!c Hartree piece
            vhart= 2.0d0*( (-ztot+rho2int(ir))/rr(ir) +                 &
     &                   (rho1int(jmt)-rho1int(ir)) )
!c
!c exchange-correlation potential
!c            if(rhotot(ir).le. 0.0d0) rhotot(ir)=rhotot(ir-1)

            if(rhotot(ir).le. 0.0d0) then
             if(ir==1) then
              rhotot(1) =1.0d-15 !SK
             else
              rhotot(ir)=rhotot(ir-1)
             end if
            end if

            ro3=(3.0d0*(rr(ir)**2)/rhotot(ir))**(1.0d0/3.0d0)
!c
            vx(ir)=alpha2(ro3,0  , 1d0,10   ,dummy) !spin is real*8

!c  totrho->0 ro3-> large, some times this causes an error in alpha2
!c  in this case the vx=0
            if(vx(ir).eq.0.0d0)  then
               vx(ir)=vx(ir-1)
            end if
!c
!c new potential, including mean-field, charge-correlated CPA
!c         vrnew(ir,ik)=( vhart + vx(ir,ik) - vmt1 + vqeff(ik) )*rr(ir)
            vr(ir)=( vhart + vx(ir) )*rr(ir)
         enddo

          call scf_atom_pot(iprpts,jmt,jws,h,rr,xr,ztot,vr,rhotot,      &
     &                      corden,1,numc,nc,lc,kc,ec,mtasa,zcor,       &
     &                      xvalsws,iprint,' ')

      return

      end subroutine iniovlp_pot
!c
      end subroutine getpot1
