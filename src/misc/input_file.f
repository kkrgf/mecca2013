      program INPUT_FILE
      implicit real*8 (a-h,o-z)
      character  iname*32
      character  inifile*32
      character  strfile*32
      character  iname34*34
      character  gname*5
!c      character  iname1*31
      character  stats*1
      character  tcryst*63
      character  idname*10
      integer iunit
      parameter (iunit=1)

1      write(6,*) ' Please, type'
      call printline(6,                                                 &
     & ' the name of MECCA input file [a32]...........................*'&
     &,63)
      read(*,*,err=1,end=1000) iname
      inifile = iname
      open(iunit,file=iname,status='new')
      call printline(iunit,' ',-1)
      write(iunit,90) iname
90    format(a32)
      gname = iname

2      write(6,*) ' Please, type'
      call printline(6,                                                 &
     & ' no. of node processors ......................................*'&
     &,63)
      read(*,*,err=2,end=1000) i1
      if(i1.le.1) then
        i2 = 0        ! serial calculations
        iname='serial'
      else
       i2 = 1        ! typical choice for number of jobs/processor
       write(6,*) ' Please, type'
       call printline(6,                                                &
     & ' name of executable code used on non-master nodes ............*'&
     &,63)
       read(*,*,err=2,end=1000) iname
      end if
      call printline(iunit,' ',-1)
      iname34 = ''''//trim(iname)//''''
      write(iunit,100) i1,i2,iname34
100   format(i4,i4,2x,a34)

3      write(6,*) ' Please, type'
       write(6,*)                                                       &
     & ' the name of output file [a32], if '' '' - output-->screen ...*'
      read(*,*,err=3,end=1000) iname
      write(iunit,90) iname

      call printline(iunit,                                             &
     & 'iixaax12345678901234567890123456789012<-end name .............*'&
     &,63)

4     write(6,*) ' Please, type'
      write(6,*)                                                        &
     & ' the name of initial potential file [a32] .....................'
      read(*,*,err=4,end=1000) iname
      write(iunit,101) 22,'n','f',iname
101   format(1x,i2,1x,2a1,1x,a32)

5      write(6,*) ' Please, type'
      write(6,*)                                                        &
     & ' the name of output potential file [a32] ......................'
      read(*,*,err=5,end=1000) iname
      write(iunit,101) 23,'u','f',iname

6      write(6,*) ' Please, type'
      tcryst =                                                          &
     & ' the crystal structure type, icryst=..........................*'
      call printline(6,tcryst,63)

      write(6,*) '  1 -- FCC'
      write(6,*) '  2 -- BCC'
      write(6,*) '  3 -- SC'
      write(6,*) '  4 -- B2 (CsCl)'
      write(6,*) '  5 -- L12 (Cu3Au)'
      write(6,*) '  6 -- Perovskite (BaTiO3)'
      write(6,*) '  7 -- B1 (NaCl)'
      write(6,*) '  8 -- DO3 (Fe3Al)'
      write(6,*) '  9 -- R3M Trigonal'
      write(6,*) ' 10 -- DO22 (TiAl3)'
      write(6,*) ' 11 -- Si (Si4)'
      write(6,*) '  0 -- you use your own crystal structure '
      write(6,*) '  What is your choice?'

      read(*,*,err=6,end=1000) icryst
      if(icryst.lt.0.or.icryst.gt.11) then
       write(6,*) ' Wrong number:',icryst,', please, try again'
       go to 6
      end if

      if(icryst.eq.0) then
7      write(6,*) ' Please, type'
       write(6,*)                                                       &
     & ' the name of structure file [a32] ............................'
       read(*,*,err=7,end=1000) iname
       write(iunit,101) 60,'o','f',iname
       strfile = iname
      else
       strfile = ' '
      end if

!C      write(6,*)
!C     * ' You can define the names of files used to store information',
!C     * ' about:'
!C      write(6,*) '  mixing (default name -- #BROY1, 61 channel)'
!C      write(6,*) '  mixing (default name -- #BROY2, 62 channel)'
!C      write(6,*)
!C     * '  total energy,etc (default name -- #KEEP,  63 channel)'
!C      write(6,*)
!C     * '  madelung consts  (default name -- #MDLNG, 64 channel)'
!C      write(6,*)
!C     * '  special k-pnts   (default name -- #SPKPT, 65 channel)'
!C      write(6,*)
!C     * '  rotation matrices(default name -- #DOP,   66 channel)'
!C      write(6,*)
!C     * ' If you want to do it now, please, type 1'
!C      read(*,*) i1
!C      if(i1.ne.1) then
!C       write(6,*)
!C     * ' The default names will be used'
!Cc       write(iunit,101) 61,'r','u','#BROY1'
!Cc       write(iunit,101) 62,'r','u','#BROY2'
!Cc       write(iunit,101) 63,'u','f','#KEEP '
!Cc       write(iunit,101) 64,'u','f','#MDLNG'
!Cc       write(iunit,101) 65,'u','f','#SPKPT'
!Cc       write(iunit,101) 66,'u','f','#DOP  '
!C      else

!C8      write(6,*) ' Please, type'
!C       write(6,*) ' the name of 1st mixing-file [a32] ......'
!C       read(*,*,err=8,end=1000) iname
!C       write(6,*) ' status of the file (r - replace, u - unknown,',
!C     *            ' n - new, o - old)'
!C       read(*,102) stats
!C       if(stats.ne.'r'
!C     *    .and.stats.ne.'u'
!C     *    .and.stats.ne.'n'
!C     *    .and.stats.ne.'o'
!C     *    ) stats = 'r'

!C102       format(a1)

       stats = 'r'
       iname = '#BR1.'//gname
       write(iunit,101) 61,stats,'u',iname

!C9      write(6,*) ' Please, type'
!C       write(6,*) ' the name of 2nd mixing-file [a32] ......'
!C       read(*,*,err=9,end=1000) iname

       iname = '#BR2.'//gname
       write(iunit,101) 62,stats,'u',iname

!C10     write(6,*) ' Please, type'
!C       write(6,*) ' the name of keep-file   [a32] ......'
!C       read(*,*,err=10,end=1000) iname
!C       write(6,*) ' status of the file (u - unknown, n - new)'
!C       read(*,102) stats
!C       if(stats.ne.'u'
!C     *    .and.stats.ne.'n'
!C     *    .and.stats.ne.'r'
!C     *    .and.stats.ne.'o'
!C     *    ) stats = 'u'

       stats = 'u'
       iname = '#KEEP.'//gname
       write(iunit,101) 63,stats,'f',iname

!C11     write(6,*) ' Please, type'
!C       write(6,*) ' the name of mdlng-file    [a32] ......'
!C       read(*,*,err=11,end=1000) iname
!C       write(6,*) ' status of the file (u - unknown, n - new)'
!C       read(*,102) stats
!C       if(stats.ne.'u'
!C     *    .and.stats.ne.'n'
!C     *    .and.stats.ne.'r'
!C     *    .and.stats.ne.'o'
!C     *    ) stats = 'u'
       stats = 'u'
       iname = '#MDLN.'//gname
       write(iunit,101) 64,stats,'f',iname

!C12     write(6,*) ' Please, type'
!C       write(6,*) ' the name of spkpts-file   [a32] ......'
!C       read(*,*,err=12,end=1000) iname
!C       write(6,*) ' status of the file (u - unknown, n - new)'
!C       read(*,102) stats
!C       if(stats.ne.'u'
!C     *    .and.stats.ne.'n'
!C     *    .and.stats.ne.'r'
!C     *    .and.stats.ne.'o'
!C     *    ) stats = 'u'
       stats = 'u'
       iname = '#SPKPT.'//gname
       write(iunit,101) 65,stats,'f',iname

!C13     write(6,*) ' Please, type'
!C       write(6,*) ' the name of dop-file   [a32] ......'
!C       read(*,*,err=13,end=1000) iname
!C       write(6,*) ' status of the file (u - unknown, n - new)'
!C       read(*,102) stats
!C       if(stats.ne.'u'
!C     *    .and.stats.ne.'n'
!C     *    .and.stats.ne.'r'
!C     *    .and.stats.ne.'o'
!C     *    ) stats = 'u'
       stats = 'u'
       iname = '#DOP.'//gname
       write(iunit,101) 66,stats,'f',iname

!C      end if

      write(iunit,*) '  0'

      call printline(6,                                                 &
     & ' method[i2], sprstech[i2], subr.stop level[a10] lower case ...*'&
     &,63)
      write(6,*) ' Please, type method chosen only...'

      write(6,*)                                                        &
     & '  method=0 -- Standard K-space KKR'
      write(6,*)                                                        &
     & '  method=1 -- Screened KKR:'
      write(6,*)                                                        &
     & '         RS-struct.consts,  K-space integration'
      write(6,*)                                                        &
     & '  method=2 -- Screened KKR:'
      write(6,*)                                                        &
     & '         R-space inversion or K-space integration'
      write(6,*)                                                        &
     & '  method=3 -- Screened KKR: R-space (LSMS approach)'
!C      write(6,*)
!C     * '  method=-1 -- Screened KKR:'
!C      write(6,*)
!C     * '         KS-struct.consts, K-space integration'
!C      write(6,*)
!C     * '  method=-2 -- Screened KKR:'
!C      write(6,*)
!C     * '   KS-struct.consts, K-space integration, SuperLU inversion'
!C      write(6,*)
!C     * '  method=-3 -- Screened KKR:'
!C      write(6,*)
!C     * '   KS-struct.consts, K-space integration, Iterat. inversion'
!C
      write(6,*) ' Please, type'
      write(6,*) '  your choice for METHOD?'
      read(*,*) method

      msparse=0
!C      write(6,*)
!C     * '  sprstech=0 -- NO sparse matrix techinque'
!C      write(6,*)
!C     * '  sprstech=1 -- JCB preconditioner or SuperLU for sparse mtrx'
!C      write(6,*)
!C     * '  sprstech=2 -- ILU preconditioner or SuperLU for sparse mtrx'
!C      write(6,*)
!C     * '  sprstech=3 -- CLS preconditioner or SuperLU for sparse mtrx'
!C
!C      write(6,*) '  What is your choice for SPARSE_TECHNIQUE?'
!C      read(*,*) msparse
      call printline(iunit,' ',-1)
      write(iunit,103) method,msparse,'main      '
103   format(2i2,1x,a10)

14    write(6,*) ' Please, type'
      call printline(6,                                                 &
     & ' print level [ipr=-100 gets min. o/p, other choices -10,-1,0  *'&
     &,63)
      read(*,*,err=14,end=1000) i1
      call printline(iunit,' ',-1)
      write(iunit,*) i1

15    write(6,*) ' Please, type'
      call printline(6,                                                 &
     & ' maximum angular momentum allowed -- lmax ....................*'&
     &,63)
      read(*,*,err=15,end=1000) i1
      call printline(iunit,' ',-1)
      write(iunit,*) i1

16    write(6,*) ' Please, type'
      call printline(6,                                                 &
     & ' mt(asa)=0(1) :: (non)spinplrzed=(1)2 :: (Non)Relatvstc=(>1)0  '&
     &,63)
      write(6,*) ' Muffin-tin -- 0 or Atomic Sphere Appr. -- 1 ?'
      read(*,*,err=16,end=1000) i1
      if(i1.ne.0) i1 = 1
      write(6,*) ' Non-spinpolarized (1) or Spinpolarized (2) ?'
      read(*,*,err=16,end=1000) i2
      if(i2.ne.2) i2 = 1
      ispin = i2
      write(6,*) ' Non-relativistic (>1) or Relativistic (0) ?'
      read(*,*,err=16,end=1000) i3
      if(i3.gt.0) then
       i3 = 22
      else
       i3 = 0
      end if

      call printline(iunit,' ',-1)
      write(iunit,104) i1,i2,i3
104   format(i4,1x,i4,1x,i4,1x,i4)

17    continue
!C       write(6,*) ' Please, type'
       call printline(6,                                                &
     & ' max of the real/reciprocal space translation vectors, Rclustr '&
     &,63)
       r1 = 3.0d0
       r2 = 3.5d0
       Rclustr = 0.d0
!C      if(method.ge.1) then
!C       write(6,*) ' It seems to me, you do not need RS-,KS-max'
!C      else
!C       write(6,*) real(r1),' <--- reasonable RS-value for FCC'
!C       write(6,*) ' R-space radius in Ewald summation (in Alat) ?'
!C       read(*,*,err=17,end=1000) r1
!C       write(6,*) real(r2),' <--- reasonable KS-value for FCC'
!C       write(6,*) ' K-space radius in Ewald summation (in 1/Alat) ?'
!C       read(*,*,err=17,end=1000) r2
!C      end if

      if(method.ne.0.or.msparse.ne.0) then
       write(6,*) ' Please, type Rclustr only....'
       write(6,*) ' Rclustr=1.5 --> 27 atoms in RS-cluster for FCC'
       write(6,*) ' Radius of cluster (max NN-distance)?'
       read(*,*,err=17,end=1000) Rclustr
!CC     else
!C       write(6,*) ' It seems to me, you do not need Rclustr'
      end if
      call printline(iunit,' ',-1)
      write(iunit,105) r1,r2,Rclustr
105   format(f8.4,1x,f8.4,1x,f8.4)

!C18    write(6,*) ' Please, type'
      call printline(6,                                                 &
     & ' Ewald parameter (enmax, eneta) -- only for KS-str.consts ....*'&
     &,63)
      enmax = 0.7
      eneta = 2.5
      if(method.le.0.or.method.eq.4) then
       write(6,*) ' It seems to me, you do not need them'
      else
       write(6,*) ' Recommended values:  enmax~ef+0.1, eneta~2.5'
!C       write(6,*) ' enmax ?'
!C       read(*,*,err=18,end=1000) enmax
!C       write(6,*) ' eneta ?'
!C       read(*,*,err=18,end=1000) eneta
       write(6,*) ' default values are used...'
       write(6,105) enmax,eneta
      end if

      call printline(iunit,' ',-1)
      write(iunit,105) enmax,eneta

      call printline(6,                                                 &
     & ' <nmesh/iq1,iq2,iq3,enlim_re,enlim_im> K-space intgrtn .......*'&
     &,63)
      call printline(iunit,' ',-1)
      if(method.eq.3) then
       nmesh = 1
       iq1 = 1
       iq2 = 1
       iq3 = 1
       write(6,*) ' It seems to me, you do not need them'
       write(iunit,104) nmesh
       write(iunit,104) iq1,iq2,iq3
      else
       write(6,*) ' Please, type'
       write(6,*) '   nmesh (number of different k-space meshes)?'
       read(*,*,end=1000) nmesh
       write(iunit,104) nmesh
       kmesh = nmesh
       i1 = 100000
       i2 = 100000
       i3 = 100000
       e1 = 10000.d0
       e2 = 0.d0
20     continue
       write(6,*) ' Please, type 3 integer numbers for'
       write(6,*) ' MESH=',kmesh,' iq1,iq2,iq3 ?'                       &
     &            ,'(K-space division for special k-points method)'
       read(*,*,end=1000) iq1,iq2,iq3

       iq1 = (iq1+1)/2*2
       iq2 = (iq2+1)/2*2
       iq3 = (iq3+1)/2*2

       if(iq1.gt.i1.or.iq2.gt.i2.or.iq3.gt.i3) then
        write(6,*) iq1,' has to be .LE.',i1
        write(6,*) iq2,' has to be .LE.',i2
        write(6,*) iq3,' has to be .LE.',i3
        write(6,*) ' Please, try again'
        go to 20
       else
        i1 = iq1
        i2 = iq2
        i3 = iq3
       end if

       if(nmesh.gt.1.and.kmesh.ne.nmesh) then
21      continue
        write(6,*) ' Please, type 2 real numbers'
        write(6,*) ' enlim_re, enlim_im ?'                              &
     &       ,'(define the energy region for given K-space division)'
        write(6,*) ' than the mesh will be defined for a region where'
        write(6,*) ' Re E < enlim_re .OR. Im E > enlim_im'
        read(*,*,end=1000) en1,en2

        if(en1.gt.e1.or.en2.lt.e2) then
         write(6,*) en1,'has to be .LE.',e1
         write(6,*) en2,'has to be .GE.',e2
         write(6,*) ' Please, try again'
         go to 21
        else
         e1 = en1
         e2 = en2
        end if
        write(iunit,106) iq1,iq2,iq3,en1,en2
106     format(i4,1x,i4,1x,i4,2x,f8.4,1x,f8.4)
       else
        write(iunit,104) iq1,iq2,iq3
       end if
       kmesh = kmesh-1
       if(kmesh.gt.0) go to 20
      end if

22    write(6,*) ' Please, type'
      call printline(6,                                                 &
     & ' text to identify the system   >!< end here  (a32 format) ....*'&
     &,63)
       read(*,90,err=22,end=1000) iname
       call printline(iunit,' ',-1)
       write(iunit,90) iname

       call printline(iunit,tcryst,63)
       write(iunit,104) icryst

      write(6,*) ' Please, type 3 numbers:'
      call printline(6,                                                 &
     & ' lattice constant (at.un.), B/A, C/A .........................*'&
     &,63)
       read(*,*,end=1000) alat,ba,ca
       call printline(iunit,' ',-1)
       write(iunit,105) alat,ba,ca

      write(6,*) ' Please, type'
      call printline(6,                                                 &
     & ' number of species with different Z ..........................*'&
     &,63)
       read(*,*,end=1000) nspec
       call printline(iunit,' ',-1)
       write(iunit,104) nspec

      write(6,*) ' Please, type 3 numbers for each ispec:'
      nzs = 0
      call printline(6,                                                 &
     & ' number, valence, semicore and core charge of alloying species '&
     &,63)
      call printline(iunit,' ',-1)
      do i=1,nspec
        write(6,*) ' For ispec=',i,' #Z,Zval,Zcor (3 integer numbers)?'
!C        read(*,*,end=1000) nz,nzv,nzs,nzc
        read(*,*,end=1000) nz,nzv,nzc
        write(iunit,104) nz,nzv,nzs,nzc
      end do

      write(6,*) ' Please, type 2 integer numbers:'
      call printline(6,                                                 &
     & ' number of sublattices and CPA control (NO CPA == 0) .........*'&
     &,63)
      read(*,*,end=1000) nsubl,ncpa
      call printline(iunit,' ',-1)
      write(iunit,104) nsubl,ncpa

      continue
      call printline(6,                                                 &
     & ' sublatt.#,compon.#, MT radius/ ID#,concentration,ID-name of'   &
     &//' each compon'                                                  &
     &,72)
      write(6,*) ' Please, type 2 numbers for each sublattice,',        &
     &           'namely'
      write(6,*) '  number of components (integer) and'                 &
     &,' MT radius (real), and then'
      write(6,*) '  3 numbers for each component:'
      call printline(iunit,' ',-1)
        write(6,*) '  -->  ID# -- one of the species number !'
        if(icryst.ne.0) then
      write(6,*) '  -->  if you do not know what MT-radius is, use 0'
        else
        endif
      do i=1,nsubl
       write(6,*) ' For sublatt.',i,'  compon.#, MT radius ?'
       ns1 = i
       read(*,*,end=1000) ncomp,rmt
!C      read(*,*,end=1000) ns1,ncomp,rmt

       if(icryst.eq.0) then
        write(6,*) ' For icryst=0 rmt cannot be equal to zero'
        write(6,*) ' Default value is used...'
        rmt = 1.d0
       end if

       write(iunit,107) ns1,ncomp,rmt
107    format(i4,1x,i4,1x,f9.5)
       total = 0.d0
       do k=1,ncomp
        write(6,*) ' For component ',k,' ID#, concentration, ID-name ?'
        read(*,*,end=1000) nid,conc,idname
        write(iunit,108) nid,conc,idname
108     format(10x,i6,1x,f7.5,1x,a10)
        total = total + conc
       end do
       if(abs(total-1.d0).gt.1.d-8) then

        write(6,*)
        write(6,*)
        write(6,*) '  ATTENTION:'
        write(6,*) '  Please, check concentrations for sublatt.',i
        write(6,*) '  TOTAL =',total,' .NE. 1'
        write(6,*)
        write(6,*)

       end if
      end do
      if(icryst.eq.0) then
       write(6,*)
       write(6,*)
       write(6,*) '  ATTENTION:'
       write(6,*) '  Please, check if sublattice numbers in your file'
       write(6,*) inifile
       write(6,*) '  correspond to their counterparts in',              &
     &            ' structural file'
       write(6,*) strfile
       write(6,*)
       write(6,*)
      end if

      write(6,*) ' Please, type'
      call printline(6,                                                 &
     & ' igrid, ebot, etop, eitop, eibot, npts/0.1 ry,  npar .........*'&
     &,63)
      call printline(iunit,' ',-1)
      write(6,200)
200   format(                                                           &
     & ' igrid -- specifies energy contour (Ry units):'                 &
     & /'  1=slower rectangular contour (can be used for DOS calcs.),'  &
     & /'  2=faster semicirular contour (for SCF calcs.)'               &
     & /                                                                &
     & /' for igrid =1............................................'     &
     & /'       ebot    : MIN(Re(Energy_on_contour)) - bottom ....'     &
     & /'       etop    : MAX(Re(Energy_on_contour)) - top .......'     &
     & /'       eitop   : MAX(Im(Energy_on_contour)) - top_imag...'     &
     & /'       eibot   : MIN(Im(Energy_on_contour)) - bottom_imag'     &
     & /'       eitop   : top    of contour on imaginary axis.....'     &
     & /'       eibot   : bottom of contour on imaginary axis.....'     &
     & /'       npts    : number of energy points per 0.1 ry......'     &
     & /'       npar    : number of points parallel to real-axis..'     &
     & /'     ....................................................'     &
     & /' for igrid =2........................................'         &
     & /'       ebot    : MIN(Re(Energy_on_contour)) - bottom ....'     &
     & /'       etop    : MAX(Re(Energy_on_contour)) - top .......'     &
     & /'       npts    : number of energy points along smeicircle'     &
     & )
      write(6,*) '  What is your choice for IGRID (1 or 2)?'
      read(*,*,end=1000) igrid
      if(igrid.ne.1) igrid=2
      write(6,*) '  What is your choice for MIN(Re(Energy_on_contour))?'
      read(*,*,end=1000) ebot
      write(6,*) '  What is your choice for MAX(Re(Energy_on_contour),'
      write(6,*) '  (0. -- then default value is used) '
      write(6,*) '  ?'
      read(*,*,end=1000) etop

      if(igrid.eq.1) then
       write(6,*) '  What is your choice for MAX(Im(Energy_on_contour)?'
       read(*,*,end=1000) eitop
       write(6,*)                                                       &
     &  '   What is your choice for MIN(Im(Energy_on_contour))?'
       read(*,*,end=1000) eibot
       if(eibot.lt.0.d0) eibot=0.001d0
       write(6,*) '  What is your choice for NPTS per 0.1 Ry?'
       read(*,*,end=1000) npts
       write(6,*) '  What is your choice for NPAR?'
       read(*,*,end=1000) npar
      else
       eitop = 0.8d0
       eibot = 0.004d0
       npar  = 0
       write(6,*) '  What is your choice for number of energy points?'
       read(*,*,end=1000) npts
      end if

      write(iunit,109) igrid,ebot,etop,eitop,eibot,npts,npar
109   format(1x,i2,1x,f9.4,1x,f9.4,1x,f9.4,1x,f9.4,1x,i3,1x,i3)

      write(6,*) ' Please, type'
      call printline(6,                                                 &
     & ' nscf:  alpha,beta:  ichg:chg(pot)=0(1); imixrho:simple(broyden'&
     &//')=0(1)'                                                        &
     &,69)
      call printline(iunit,' ',-1)
      write(6,*) ' What is your choice for maximum number of SCF'       &
     &,' iterations (0 -- to create structure-dependent files only)?'
      read(*,*,end=1000) nscf
      write(6,*) ' What is your choice for mixing scheme imixro=?'      &
     &,' (1 - broyden, 0 - simple)'
      read(*,*,end=1000) imixrho
      write(6,*) ' What do you prefer to mix, ichg=?'                   &
     &,'(0 - density, 1 - potential)'
      write(6,*) ' If you have no reasonable initial potential,'        &
     &,' use potential mixing first'
      read(*,*,end=1000) ichg
      if(ichg.ne.0) then
       ichg = 1
       if(imixrho.ne.0) then
        imixrho = 0
        write(6,*) ' Simple mixing for potential is used'
       end if
      end if

      write(6,*) ' What is your choice for non-magnetic mixing'         &
     &,' parameter, alpha=?'
      write(6,*)                                                        &
     & ' (broyden-> ~0.05-0.1, simple->~0.1-0.8,'                       &
     &,' for big systems it may be much smaller....'
       read(*,*,end=1000) alpha

      if(ispin.gt.1) then
        write(6,*) ' What is your choice for magnetic mixing'           &
     &,' parameter, beta=?'                                             &
     &,' (~0.5-0.8)'
       read(*,*,end=1000) beta
      else
       beta = 0.7d0
      end if

      write(iunit,110) nscf,alpha,beta,ichg,imixrho
110   format(1x,i3,1x,f7.3,1x,f7.3,1x,i2,1x,i2)

      call printline(iunit,                                             &
     & ' end of data. . . . . . . . . . . . . . . . . . . . . . . . . *'&
     &,63)

      close(iunit)
      stop ' DONE'
1000  stop 1000
      end

      subroutine printline(iunit,line,n)
      integer iunit
      character*1 line(*)
      integer i,m/0/,nmax
      parameter (nmax=110)
      character*1 tmpline(1:nmax)/nmax*' '/
      save m,tmpline
      if(n.lt.0) then
       write(iunit,1) '%%',' ',(tmpline(i),i=1,m)
      else
       m = min(n,nmax)
       tmpline(1:m) = line(1:m)
       write(iunit,1) '%%',' ',(line(i),i=1,n)
      end if
1     format(120a1)
      return
      end
