!BOP
!!ROUTINE: stringToCmpnd
!!INTERFACE:
       subroutine stringToCmpnd(name,compound,nsubl)
!!DESCRIPTION:
! intializes {\tt compound} (type Sublattice) with 
! {\tt nsubl} sublattices using an input compound name {\em name} 
!
!!USES:
       USE mecca_types

!!DO_NOT_PRINT
       implicit none
!!DO_NOT_PRINT

!!ARGUMENTS:
       character*1 name(*)
       type ( Sublattice ), allocatable :: compound(:)
       integer, intent(inout) :: nsubl
!!REVISION HISTORY:
! Initial Version - A.S. - 2007
! sublattice array is allocated internally - A.S. - 2015
!EOP
!
!BOC
       integer numbs
       integer i, is, j, k, ncomp, nsmax
       logical inParenth
       character*2 symb
       real*8 z, conc, w
       character*80 line80

       logical  isParenthL, isParenthR, isLetter, isDigit
       external isParenthL, isParenthR, isLetter, isDigit
       integer idToZ
       external idToZ

       if ( allocated(compound) ) then
        nsmax = size(compound)
       else
        nsmax = 0
       end if

       DO is=1,2

       i = 1
       do while (name(i) .eq. ' ')
        i = i+1
       end do
       k = 0
       j = i
       do while ( name(j).ne.' ')
        if ( isDigit(name(j)) ) k = k+1
        j = j+1
       end do

       if ( nsmax>0 ) then
        if ( (j-i).eq.k .and. k.gt.0 .and. k.lt.4) then
           write(line80,'(4a1)') name(i:j)
           read(line80,*) k
           if ( nsubl.le.nsmax) then
            compound(1)%ns = 1
            compound(1)%ncomp = ncomp
            compound(1)%rIS = 0.d0
            compound(1)%compon(1)%name = ''
            call atomIDc(dble(k),                                       &
     &                   symb,compound(1)%compon(1)%name)
            compound(1)%compon(1)%concentr = 1.d0
            compound(1)%compon(1)%zID = k
            return
           else
            nsubl = 0
            deallocate(compound)
            nsmax = 0
           end if
        end if
       end if

       nsubl = 0
       ncomp = 0
       inParenth = .FALSE.
       do while ( name(i) .ne. ' ' )
        if ( isLetter(name(i)) ) then
         z = idToZ(name(i:i+1))
!         if ( z .lt. 0 ) then
!          symb = ' '//name(i:i) 
!          z = idToZ(symb)
!          i = i-1
!         end if
         if ( z .lt. 0 ) then
!c          ERROR
          write(*,*) ' z<0'
          nsubl = -1
          exit
         end if
         if ( .NOT. inParenth ) then
          ncomp = 1
          nsubl = nsubl + 1
          if ( nsubl.le.nsmax) then
           compound(nsubl)%ncomp = ncomp
           compound(nsubl)%rIS = 0.d0
           compound(nsubl)%ns = 1
           compound(nsubl)%compon(ncomp)%name = ''
           call atomIDc(z,symb,compound(nsubl)%compon(ncomp)%name)
           compound(nsubl)%compon(ncomp)%concentr = 1.d0
           compound(nsubl)%compon(ncomp)%zID = z
          end if
         else
          ncomp = ncomp + 1
          if ( nsubl.le.nsmax) then
           compound(nsubl)%ncomp = ncomp
           compound(nsubl)%rIS = 0.d0
           compound(nsubl)%compon(ncomp)%name = ''
           call atomIDc(z,symb,compound(nsubl)%compon(ncomp)%name)
           if (ncomp.gt.1 .and.                                         &
     &        compound(nsubl)%compon(ncomp-1)%concentr .eq. 0.d0)       &
     &        compound(nsubl)%compon(ncomp-1)%concentr = 1.d0
           compound(nsubl)%compon(ncomp)%concentr = 0.d0
           compound(nsubl)%compon(ncomp)%zID = z
          end if
         end if
         if( isLetter(name(i+1)) ) i = i+1
        else if ( isDigit(name(i)) ) then
         if (i .eq. 1) then
!c         ERROR
          write(*,*) ' ERROR: i.eq.1'
          nsubl = -1
          exit
         else if ( .not. isLetter(name(i-1))                            &
     &        .and.                                                     &
     &              (name(i-1).ne.')') ) then
!c         ERROR
          write(*,*) ' ERROR: not Letter',name(i-1:i)
          nsubl = -1
          exit
         end if
         k = 1
         do while ( isDigit(name(i+k) ) )
          k = k+1
         end do
         if ( name(i+k) .eq. '.' ) then
          if ( inParenth ) then
           k = k+1
           do while ( isDigit(name(i+k) ) )
            k = k+1
           end do
          else
!c          ERROR
           write(*,*) ' ERROR: not inParenth'
           nsubl = -1
           exit
          end if
         end if
         write(line80,*) ' ',name(i:i+k-1),' '
         if ( nsubl.le.nsmax) then
          if ( .not.inParenth) then
           read(line80,*) numbs
           compound(nsubl)%ns = numbs
          else if ( ncomp.ne.0 ) then
           read(line80,*) conc
           compound(nsubl)%compon(ncomp)%concentr = conc
          else
           write(*,*) ' ERROR: unexpected digits'
           nsubl = -1
           exit
          end if
         end if
         i = i+k
         cycle
        else if ( name(i) .eq. '(' ) then
         inParenth = .TRUE.
         nsubl = nsubl + 1
         if (nsubl.le.nsmax) compound(nsubl)%ns = 1
         ncomp = 0
         i = i+1
         cycle
        else if ( name(i) .eq. ')' ) then
         if (i .eq. 1) then
!c         ERROR
          nsubl = -1
          write(*,*) ' ERROR: wrong place for parenthR'
          exit
         else if ( .not. isLetter(name(i-1))                            &
     &        .and.                                                     &
     &              .not. isDigit(name(i-1)) ) then
!c         ERROR
          write(*,*) ' ERROR: not Letter, not Digit ',name(i-1:i)
          nsubl = -1
          exit
         end if
         if ( inParenth ) then
          if ( nsubl.le.nsmax) then
           if (compound(nsubl)%compon(ncomp)%concentr.eq.0.d0)          &
     &       compound(nsubl)%compon(ncomp)%concentr = 1.d0
           w = 0.d0
           do k=1,ncomp
            w = w+compound(nsubl)%compon(k)%concentr
           end do
           do k=1,ncomp
            compound(nsubl)%compon(k)%concentr =                        &
     &      compound(nsubl)%compon(k)%concentr / w
           end do
          end if
          inParenth = .FALSE.
          ncomp = 0
          i = i+1
          cycle
         else
!c         ERROR
          write(*,*) ' ERROR: incorrect parenth-s ',name(i-1:i)
          nsubl = -1
          exit
         end if
        else
!c          ERROR
          write(*,*) ' ERROR: wrong symbol ',name(i)
          nsubl = -1
         exit
        end if
        i = i+1
       end do

       if ( nsubl>0 .and. nsmax==0 ) then
        allocate(compound(nsubl))
        nsmax = nsubl
       else 
        exit
       end if

       END DO

       return
!EOC
       end subroutine stringToCmpnd
