      subroutine varEnCorr(komp,jmt,xr,vrold,vrnew,rhov,corden,         &
     &                     correc,cor3pv)
      implicit none
      integer, intent(in) :: komp
      integer, intent(in) :: jmt(:)
      real(8), intent(in) :: xr(:,:)
      real(8), intent(in) :: vrold(:,:)
      real(8), intent(in) :: vrnew(:,:)
      real(8), intent(in) :: rhov(:,:)
      real(8), intent(in) :: corden(:,:)
!      
      real(8), intent(out) :: correc(komp)
      real(8), intent(out) :: cor3pv(komp)

      integer :: ik
      real(8) :: rr(1:size(xr,1))
      real(8) :: derv(1:size(xr,1))
      real(8) :: bnd(1:size(xr,1))
      real(8) :: bnd_diff(1:size(xr,1))
      real(8) :: bndint(1:size(xr,1))
      real(8) :: bndint1(1:size(xr,1))
      real(8) :: corrV_DV

!c
!c     ==================================================================
!CALAM      calculate terms that make the energy variational............
!c      This part of the calculation has been completely modified
!c              CONTRIBUTION OF THE CORRECTION TERMS (ENERGY)
!c     ==================================================================
!c     Correction coming from the core integ. that contain terms
!c                    involving derivatives of rV(r)
!c     ==================================================================
          do ik=1,komp
            rr = exp(xr(1:jmt(ik),ik))
            bndint1(1:jmt(ik)) = vrold(1:jmt(ik),ik)-vrnew(1:jmt(ik),ik)
            bnd_diff(1:jmt(ik))= bndint1(1:jmt(ik))/rr(1:jmt(ik))
            call derv5(bndint1,derv,rr(:),jmt(ik))
            bndint(1:jmt(ik))=0.5d0*                                    &
     &                            (corden(1:jmt(ik),ik)*derv(1:jmt(ik)))
            call qexpup(1,bndint,jmt(ik),xr(:,ik),bnd)
            correc(ik)=-bnd(jmt(ik))-corden(1,ik)*derv(1)*rr(1)/6.d0
!c     ==================================================================
!c     Correction coming from the core integ. that contain terms
!c                    involving rV(r)
!c     ==================================================================
            bndint(1:jmt(ik))=0.5d0*                                    &
     &                        (corden(1:jmt(ik),ik)*bnd_diff(1:jmt(ik)))
            call qexpup(1,bndint,jmt(ik),xr(:,ik),bnd)
            correc(ik)=correc(ik) + bnd(jmt(ik)) +                      &
     &                                    (corden(1,ik)*bndint1(1))/6.d0
!c     ==================================================================
!c     Correction coming from the valence integ. that contain terms
!c                    involving derivatives of rV(r)
!c     ==================================================================
            bndint(1:jmt(ik)) = rhov(1:jmt(ik),ik)*derv(1:jmt(ik))
            call qexpup(1,bndint,jmt(ik),xr(:,ik),bnd)
            corrV_DV=-bnd(jmt(ik))-(rhov(1,ik)*derv(1)*rr(1))/3.d0
!c===============================================================
!c           TOTAL CORRECTIONS FROM VARIOUS TERMS IN ENERGY
!c===============================================================
            correc(ik)=correc(ik) + corrV_DV
!c===============================================================
!c           CONTRIBUTION OF THE CORRECTION TERMS (PRESSURE)
!c===============================================================
!c        Correction coming from the integ. that contain terms
!c                      involving rV(r)
!c                            +
!c                  derivatives of rV(r)  [same as corrV_DV(ik)]
!c===============================================================
            bndint(1:jmt(ik)) = rhov(1:jmt(ik),ik)*bnd_diff(1:jmt(ik))
            call qexpup(1,bndint,jmt(ik),xr(:,ik),bnd)
            cor3pv(ik)=(-bnd(jmt(ik)) - (rhov(1,ik)*bndint1(1))/3.d0) + &
     &                 corrV_DV

          enddo

          return
          end subroutine varEnCorr
