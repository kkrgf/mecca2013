!c     ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      subroutine effindT(numef,etop,xtws,deltZ,tnen,                    &
     &               dosint,doslast,zvaltot,qrmsav,                     &
     &               nbasis,nspin,komp,atcon,numbsub,                   &
     &               efermi,iprint,istop)
!c     ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
!c
      implicit real*8(a-h,o-z)
!c
!c     ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
      include 'mkkrcpa.h'
!c     ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!c
      character istop*10
!c
      integer   komp(ipsublat)
      integer   nbasis,numbsub(nbasis)
!c
      integer   numef
      real*8      atcon(ipcomp,ipsublat)
!c      real*8      evalsum(ipcomp,ipsublat,ipspin)
      real*8      etop
      real*8      zvaltot,qrmsav
      real*8      efermi
      real*8      delta
!c
      complex*16   xtws
      complex*16   dosint(ipcomp,ipsublat,ipspin)
      complex*16   doslast(ipcomp,ipsublat,ipspin)
      complex*16   tnen
      real*8       efnew

!c      real*8  eftol                    ! now it is in "mkkrcpa.h"
!c      parameter (eftol=0.0001d0)
      real*8      half
      complex*16   czero
      character sname*10
!c
!c     ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
      parameter (half=one/two,defmax=0.1d0,dzmax=0.1d0)
      parameter (czero=(0.d0,0.d0),sname='effindT')
!c     ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!c     ******************************************************************
!c
!c     this routine calculates the fermi energy..........................
!c
!c     It uses a table of component n(e)'s [pne(......)] defined on a
!c     contour [egrd(.)] in the complex energy plane.....................
!c
!c     n.b. the n(e)'s are per spin......................................
!c     Hence the factor:
!c            (3-nspin)  = 2 if non spin polarised: nspin=1
!c            (3-nspin)  = 1 if     spin polarised: nspin=2
!c     in the summations to obtain integrated n(e).......................
!c
!c     ****************************************************************** !*
!c
!c     ================================================================== !=

!c  calculates ::: integrated n(e) ......................................

!c     ================================================================== !=
      ispfac=3-nspin
      if(iprint.ge.0) then
       do is=1,nspin
        do in=1,nbasis
         do ic=1,komp(in)
          write(6,'(''      effind: dosint '',2i4,2d17.8)')             &
     &                        in,ic,ispfac*dosint(ic,in,is)
         enddo
        enddo
       enddo
      end if
!c     ******************************************************************
!c     add contribution to integral from last point on contour...........
!c     zero out working arrays...........................................
      xtws=czero
      do is=1,nspin
       do in=1,nbasis
        do ic=1,komp(in)
         xtws=xtws+atcon(ic,in)*dosint(ic,in,is)*numbsub(in)*ispfac
        enddo
       enddo
      enddo

      deltZ =  zvaltot-dimag(xtws)

!c      if(iprint.ge.-1) then
!c     ******************************************************************
!c     Major printout....... Integrated n(e) at end of contour.....
!c          write(6,*)
!c          write(6,'(''    Zvaltot'',t40,''='',f16.11)') zvaltot
!c          write(6,'(''    Total      Int{n(e)}: etop-ebot'',
!c     >            t40,''='',f16.11)') dimag(xtws)
!c          write(6,'(''    Zvaltot -  Int{n(e)}'',
!c     >          t40,''='',f16.11)') deltZ
!c     ******************************************************************
!c          if(iprint.ge.1) then
!c          write(6,'(''        Before Ef is calculated: '')')
!c           write(6,'(''      total integrated n(e) ='',2d20.11)') xtws
!c          endif
!c      endif
!c     ==================================================================
!c     find the fermi energy....DEPENDENT UPON E GRID...................
!c     ==================================================================
      tnen=czero
      do is=1,nspin
       do in=1,nbasis
        do ic=1,komp(in)
         tnen=tnen +                                                    &
     &             atcon(ic,in)*doslast(ic,in,is)*numbsub(in)*ispfac
        enddo
       enddo
      enddo

!c      if(iprint.ge.-1) then
!c         write(6,'(''    Tot_DOS(~Ef) per cell = '' ,2d19.10)') tnen
!c      end if

      dosen = dimag(tnen)
      if(dosen.lt.zero) then
        write(6,*)
        write(6,*) ' tnen=',tnen
        write(6,*) ' DOS(En)=',dosen
       call fstop(sname//':  DOS(En) < ZERO ????')
      end if

      delta=deltZ / dosen

!c     ==================================================================
!c      Limit change in Ef for Lousy Guesses
!c     ==================================================================
      efermi= etop + sign(  min(defmax, abs(delta)), delta)
!c
      call efappr(etop,deltZ,efermi,efnew,numef)
      efermi = efnew
      if(abs(etop-efermi).lt.min(1.d-5,0.1d0*etol/dosen)) numef=0

!CAB  to decrease number of efermi-calculations per SCF-iteration
      if(qrmsav.gt.rmstol) then
       if(abs(etop-efermi).lt.eftol) numef=0
      end if
!CAB

!c    *************************************************************
      if(istop.eq.sname) call fstop(sname)
!c    *************************************************************

!c      write(6,'(/)')
      return
      end

        subroutine efappr(efold,deltZ,efermi,efnew,numef)
        implicit real*8 (a-h,o-z)
        real*8 efold,deltZ,efermi,efnew
        integer*4 numef

        real*8 e1,delt1,e2,delt2
        save   e1,delt1,e2,delt2
        integer*4 nefmax
        parameter (nefmax=5)

        if(numef.eq.0) then
         delt1 = deltZ
         e1    = efold
         delt2 = deltZ
         e2    = efold
         efnew = efermi
        else

         if(deltZ*delt1.lt.-1.d-32) then
          if(efold.lt.e1) then
           e2 = e1
           delt2 = delt1
           e1 = efold
           delt1 = deltZ
          else
           e2 = efold
           delt2 = deltZ
          end if
          e0 = e1 + (e2-e1)*abs(delt1)/(abs(delt1)+abs(delt2))
!c          efnew = (max(e1,min(efermi,e2))+e0)/2
          efnew = e0
         else if(deltZ*delt2.lt.-1.d-32) then
          if(efold.gt.e2) then
           e1 = e2
           delt1 = delt2
           e2 = efold
           delt2 = deltZ
          else
           e1 = efold
           delt1 = deltZ
          end if
          e0 = e1 + (e2-e1)*abs(delt1)/(abs(delt1)+abs(delt2))
!c          efnew = (max(e1,min(efermi,e2))+e0)/2
          efnew = e0
         else
          if(efold.lt.e1) then
           e2 = e1
           delt2 = delt1
           e1 = efold
           delt1 = deltZ
          else if(efold.gt.e2) then
           e1 = e2
           delt1 = delt2
           e2 = efold
           delt2 = deltZ
          else
           call fstop(' EFOLD is between E1 and E2,'//                  &
     &                  ' but DELT1*DELT2>0 ???')
          end if
          efnew = efermi
         end if
        end if

        if(numef.ge.0. .and. numef.lt.nefmax) then
         numef = numef +1
         if(numef.eq.nefmax) numef = 0
        else
         numef = -nefmax*100-1
        end if

        return
        end

!c     ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      subroutine efadjust(Tempr,efermi,etop,egrd,nume,vshift,           &
     &               dosint,dosckint,doslast,doscklast,evalsum,         &
     &               greenlast,jws,zvaltot,                             &
     &               nbasis,nspin,komp,atcon,numbsub,                   &
     &               xvalws,xvalmt,iprint,istop)
!c     ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
!c
      implicit real*8(a-h,o-z)
!c
!c     ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
      include 'mkkrcpa.h'
!c     ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!c
      character istop*10
!c
      integer   komp(ipsublat)
      integer   nbasis,numbsub(nbasis)
      integer   jws(ipcomp,nbasis)
!c
      real*8      Tempr
      real*8      atcon(ipcomp,ipsublat)
      real*8      evalsum(ipcomp,ipsublat,ipspin)
      real*8      xvalws(ipcomp,ipsublat,ipspin)
      real*8      xvalmt(ipcomp,ipsublat,ipspin)
      real*8      xvaltws
      real*8      xvaltmt
      real*8      etop
!c      real*8      elast
      real*8      zvaltot
      real*8      efermi
      real*8      delta
      real*8      eigensum
      real*8      vshift(2)
!c
      complex*16   xtws
      complex*16   xtmt
      complex*16   egrd(nume)
      complex*16   dosint(ipcomp,ipsublat,ipspin)
      complex*16   dosckint(ipcomp,ipsublat,ipspin)
      complex*16   doslast(ipcomp,ipsublat,ipspin)
      complex*16   doscklast(ipcomp,ipsublat,ipspin)
      complex*16   greenlast(iprpts,ipcomp,ipsublat,ipspin)
      complex*16   tnen
!c      complex*16   del_e
      real*8       dlteff
!c parameter
      real*8      half
!c      real*8      def_tol
      complex*16   czero
      character sname*10
!c
!c     ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
      parameter (half=one/two,defmax=0.1d0,dzmax=0.1d0)
      parameter (czero=(0.d0,0.d0),sname='Efadjust')
!c     ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!c     ******************************************************************
!c
!c     this routine adjusts DOS,greenlast according to the position of
!c     fermi level and put spin factor ..........................
!c            (3-nspin)  = 2 if non spin polarised: nspin=1
!c            (3-nspin)  = 1 if     spin polarised: nspin=2
!c
!c     ****************************************************************** !*
!c
!c     ================================================================== !=

!c     put the spin factor in the densities of states....................
      ispfac=3-nspin
      do is=1,nspin
       do in=1,nbasis
        do ic=1,komp(in)
          dosint(ic,in,is)    = ispfac*dosint(ic,in,is)
          doslast(ic,in,is)   = ispfac*doslast(ic,in,is)
          dosckint(ic,in,is)  = ispfac*dosckint(ic,in,is)
          doscklast(ic,in,is) = ispfac*doscklast(ic,in,is)
          evalsum(ic,in,is)   = ispfac*evalsum(ic,in,is)
          if(iprint.ge.0) then
           write(6,'(a14,'': dosint '',2i4,2d17.8)') sname,             &
     &                         in,ic,dosint(ic,in,is)
          end if
        enddo
       enddo
      enddo
!c     ******************************************************************
!c     add contribution to integral from last point on contour...........
!c     zero out working arrays...........................................
      xtws=czero
      xtmt=czero
      tnen=czero
      do is=1,nspin
         do in=1,nbasis
            do ic=1,komp(in)
              xtws=xtws+atcon(ic,in)*dosint(ic,in,is)*numbsub(in)
              xtmt=xtmt+atcon(ic,in)*dosckint(ic,in,is)*numbsub(in)
              tnen=tnen+atcon(ic,in)*doslast(ic,in,is)*numbsub(in)
            enddo
         enddo
      enddo

      deltZ =  zvaltot-dimag(xtws)

      if(iprint.ge.-1) then
!c     ******************************************************************
!c     Major printout....... Integrated n(e) at end of contour.....
       write(6,*)
       write(6,'(''    Zvaltot'',t40,''='',f16.11)') zvaltot
       write(6,'(''    Total      Int{n(e)}: etop-ebot'',               &
     &          t40,''='',f16.11)') dimag(xtws)
       write(6,'(''    Zvaltot -  Int{n(e)}'',                          &
     &          t40,''='',f16.11)') deltZ
!c     ******************************************************************
       if(iprint.ge.1) then
         write(6,'(''      Not adjusted: '')')
         write(6,'(''      total integrated n(e) ='',2d20.11)') xtws
         write(6,'(''      M-tin integrated n(e) ='',2d20.11)') xtmt
       endif
      endif

      if(iprint.ge.-1) then
       if(Tempr.gt.zero) then
        write(6,'(/''    Temperature (Ry and K)    = ''                 &
     &              ,d19.10,4x,f11.1)') Tempr, Tempr*ry2kelvin
       end if
      end if

      if(dimag(tnen).lt.1.d-7*abs(deltZ)) then
       dlteff = zero
      else
       dlteff = deltZ/dimag(tnen)
      end if

      delta = efermi - etop

!c
!c  dlteff = delt*<dos(En)>/dos(En)
!c

!c     ==================================================================
!c
!c     ******************************************************************
!c     Major printout....... New Fermi energy.....
      if(iprint.ge.-1) then
      write(6,'(''    New Fermi energy'',t40,''='',f16.11)') efermi
       write(6,'(''    Change in Ef'',t40,''='',f16.11)') delta
      end if
!c     ******************************************************************
!c
!c     ==================================================================
!c     compute integrated densities of states up to fermi energy........
!c      (estimation)
!c
      xvaltws=zero
      xvaltmt=zero
      call zeroout(xvalws,ipcomp*ipsublat*ipspin)
      call zeroout(xvalmt,ipcomp*ipsublat*ipspin)
      do is=1,nspin
       do in=1,nbasis
        do ic=1,komp(in)
         dosint(ic,in,is)=dosint(ic,in,is) +                            &
     &                    doslast(ic,in,is)*dlteff
!c
         dosckint(ic,in,is)=dosckint(ic,in,is) +                        &
     &                      doscklast(ic,in,is)*dlteff
!c
         xvalws(ic,in,is)=xvalws(ic,in,is) +                            &
     &   dimag(dosint(ic,in,is))
!c
         xvalmt(ic,in,is)=xvalmt(ic,in,is) +                            &
     &   dimag(dosckint(ic,in,is))
!c
         xvaltws=xvaltws + atcon(ic,in)*xvalws(ic,in,is)*numbsub(in)
         xvaltmt=xvaltmt + atcon(ic,in)*xvalmt(ic,in,is)*numbsub(in)
!c
        enddo
       enddo
      enddo

!c
!c     ==================================================================
!c
      if(iprint.ge.-1) then
!c
!c   Major printout....... Integrated n(e) at New Fermi Energy.........
!c
       write(6,'(''  +++++++++++++++++++++++++++++++++++'',             &
     &           ''++++++++++++++++++++++++++++++++++'')')
       write(6,'(''  Total        Int{n(e)}: E_f'',                     &
     &          t40,''='',f16.11)') xvaltws
       do is=1,nspin
         do in=1,nbasis
          if(nbasis*nspin.ne.1.or.komp(in).ne.1) then
            do ic=1,komp(in)
               write(6,'(''       Spin ='',i2,                          &
     &                   '' Sub-lat ='',i2,'' Comp ='',i2,              &
     &                   t40,''=   '',f16.11)')                         &
     &         is,in,ic,xvalws(ic,in,is)
            enddo
          end if
         enddo
       enddo
       write(6,'(''  +++++++++++++++++++++++++++++++++++'',             &
     &           ''++++++++++++++++++++++++++++++++++'')')
       write(6,'(''  Muffin-tin   Int{n(e)}: E_f'',                     &
     &          t40,''='',f16.11)') xvaltmt
       do is=1,nspin
         do in=1,nbasis
          if(nbasis*nspin.ne.1.or.komp(in).ne.1) then
            do ic=1,komp(in)
               write(6,'(''       Spin ='',i2,                          &
     &                   '' Sub-lat ='',i2,'' Comp ='',i2,              &
     &                   t40,''=   '',f16.11)')                         &
     &         is,in,ic,xvalmt(ic,in,is)
            enddo
          end if
         enddo
       enddo
       write(6,'(''  +++++++++++++++++++++++++++++++++++'',             &
     &           ''++++++++++++++++++++++++++++++++++'')')
       write(6,'(''  Interstitial Int{n(e)}: E_f'',                     &
     &          t40,''='',f16.11)') xvaltws-xvaltmt
       do is=1,nspin
         do in=1,nbasis
          if(nbasis*nspin.ne.1.or.komp(in).ne.1) then
            do ic=1,komp(in)
               write(6,'(''       Spin ='',i2,                          &
     &                   '' Sub-lat ='',i2,'' Comp ='',i2,              &
     &                   t40,''=   '',f16.11)')                         &
     &         is,in,ic,xvalws(ic,in,is)-xvalmt(ic,in,is)
            enddo
          end if
         enddo
       enddo
       write(6,'(''  +++++++++++++++++++++++++++++++++++'',             &
     &           ''++++++++++++++++++++++++++++++++++'')')

      endif

      if(dlteff.ne.one) then
       do is=1,nspin
        do in=1,nbasis
         do ic=1,komp(in)
!CAA       greenlast(1:jws(ic,in),ic,in,is) =
!CAA     *              greenlast(1:jws(ic,in),ic,in,is)*(dlteff/delta)
          greenlast(1:iprpts,ic,in,is) =                                &
     &              greenlast(1:iprpts,ic,in,is)*(dlteff/delta)
         enddo
        enddo
       enddo
      end if

!c
!c     ==================================================================
!c
!c     calculate contibution to eigenvalue sum from e(nume-1) to efermi..

      eigensum=zero
      do is=1,nspin

!c        elast=dreal(egrd(nume))-(is-1)*vdif
        elast=dreal(egrd(nume))+vshift(is)

!c
         do in=1,nbasis
           do ic=1,komp(in)
             write(6,*) 'ic,in,is,evs=', ic,in,is,evalsum(ic,in,is)
             evalsum(ic,in,is)=evalsum(ic,in,is) +                      &
     &                    dimag(doslast(ic,in,is))*dlteff*elast
             eigensum=eigensum +                                        &
     &                atcon(ic,in)*evalsum(ic,in,is)*numbsub(in)
           enddo
         enddo
      enddo
!c
!c     ==================================================================
!c
      if(iprint.ge.-1) then
!c
!c   Major printout.......Band Energy............................
!c
       write(6,'(/)')
       write(6,'(''  +++++++++++++++++++++++++++++++++++'',             &
     &           ''++++++++++++++++++++++++++++++++++'')')
       write(6,'(''  Band Energy: Int{e*n(e)}, Int{(e-Ef)*n(e)}'',      &
     &          t46,''='',f15.11,1x,f15.11)') eigensum                  &
     &                                   ,eigensum-zvaltot*efermi
       do is=1,nspin
         do in=1,nbasis
          if(nbasis*nspin.ne.1.or.komp(in).ne.1) then
            do ic=1,komp(in)
               write(6,'(''       Spin ='',i2,                          &
     &                   '' Sub-lat ='',i2,'' Comp ='',i2,              &
     &                   t40,''=   '',f16.11)')                         &
     &                    is,in,ic,evalsum(ic,in,is)
!c
!CDDJ           write(6,'('' eval-xvalws*vdif,eval+xvalws*vdif '',
!CDDJ >                      2f16.11)')
!CDDJ >         evalsum(ic,in,is)-xvalws(ic,in,is)*vdif,
!CDDJ >         evalsum(ic,in,is)+xvalws(ic,in,is)*vdif
!c
            enddo
          end if
         enddo
       enddo
       write(6,'(''  +++++++++++++++++++++++++++++++++++'',             &
     &           ''++++++++++++++++++++++++++++++++++'')')
      end if
!c
!c    *************************************************************
      if(istop.eq.sname) call fstop(sname)
!c    *************************************************************

      return
      end
