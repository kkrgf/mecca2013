!c
!c     Multiple-scattering      |\  /|  --   --  --  --
!c     Electronic-structure     | \/ | |-   |   |   |__|
!c     Calculations for         |    | |___ |__ |__ |  |
!c     Complex
!c     Alloys
!c
!c     ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
!BOP
!!ROUTINE: mecca_scf
!!INTERFACE:
      recursive subroutine mecca_scf( mecca_state,outinfo )
! !DESCRIPTION:
!   Multi-sublattice, multi-component KKR-CPA code, main function is 
!   to compute scf state {\tt mecca\_state} and/or Green's-Function related properties 
!   (e.g. DOS, Fermi-surface, etc.). 
!   General output information is in {\tt outinfo}, detailed output can be found
!   in {\tt mecca\_state} and in various output-files.

!!USES:
       use mecca_constants
       use mecca_types
       use mecca_run
       use input,   only : saveInput
       use struct,  only : setup_struct
       use vp_str,  only : setOptSphBasis
       use initpot, only : init_potential
       use potential
       use scf_io,  only : save_inifile
       use gfncts_interface, only : g_numNonESsites
       use mecca_interface
       use xc_mecca, only : xc_mecca_pointer,gXCmecca,gInfo_libxc
!

!!DO_NOT_PRINT
      implicit none
!!DO_NOT_PRINT

!INPUT/OUTPUT ARGUMENTS:
      type (RunState), target :: mecca_state
!OUTPUT ARGUMENTS:
      character*(*) outinfo           ! output info

! !REVISION HISTORY:
! Initial Version - A.S. - 2013
!
! !REMARKS:
!   The subroutine can be also used for some other purposes, .e.g.
!   to initialize mecca_state from potential file, 
!   to generate input file, etc. 
!
!EOP

!BOC
      character(*), parameter :: sname='mecca_scf'

      type (IniFile),  pointer :: inFile
      type (Jacbn_VP),  target, save :: jcbn0box
      type(RS_Str),     target, save :: rs0box 
      type(KS_Str),     target, save :: ks0box 
      type(EW_Str),     target, save :: ew0box 
      type(GF_data),    target, save :: gf0box 
      type(GF_output),  target, save :: gf_out0box 
      type(Work_data),  target, save :: work0box 
      type(DosBsf_data),target, save :: bsf0box 
      type(FS_data),    target, save :: fs0box 

      integer :: iprint_save=-1
      integer :: recurs
!c     ******************************************************************
!c     *****  start of executible statements  ***************************
!c     ******************************************************************

      mecca_state%info = -1
      recurs = 0
      if ( associated(mecca_state%intfile) ) then
       inFile => mecca_state%intfile
      else
!      interface
!        character (LEN=30) function buildDate()
!        end function buildDate
!      end interface
        write(6,'(1x,a)') version//buildDate()
        return
      end if

      if ( associated(getMS(),mecca_state) ) then  ! recursive call
       recurs = 1
      else
       call setMS( mecca_state )                   ! definition of the global run_state
       call mecca_hello()
       if ( inFile%nscf>0 ) then
        call scf_ss(outinfo)                        ! single-scattering calculation
        call deallocRunState(mecca_state)      
        mecca_state%info = -1
       end if
      end if

      if ( .not. associated(mecca_state%rs_box) ) then
       mecca_state%rs_box   => rs0box
      end if
      if ( .not. associated(mecca_state%ks_box) ) then
       mecca_state%ks_box   => ks0box
      end if
      if ( .not. associated(mecca_state%ew_box) ) then
       mecca_state%ew_box   => ew0box
      end if
      if ( .not. associated(mecca_state%jcbn_box) ) then
       mecca_state%jcbn_box => jcbn0box
      end if
      if ( .not. associated(mecca_state%gf_box) ) then
       mecca_state%gf_box => gf0box
      end if
      if ( .not. associated(mecca_state%gf_out_box) ) then
       mecca_state%gf_out_box => gf_out0box
      end if
!DEBUG      if ( .not. associated(mecca_state%work_box) ) then
!DEBUG       mecca_state%work_box => work0box
!DEBUG      end if
      if ( .not. associated(mecca_state%bsf_box) ) then
       mecca_state%bsf_box => bsf0box
      end if
      if ( .not. associated(mecca_state%fs_box) ) then
       mecca_state%fs_box => fs0box
      end if

!  printing input information
!
      if ( mecca_state%intfile%nscf > 0 ) then

!  saving input in a file
!
        call save_inifile(mecca_state)
      else if ( mecca_state%intfile%nscf == 0 ) then
        mecca_state%intfile%nmesh = 1
        mecca_state%intfile%nKxyz = 0
        mecca_state%intfile%nKxyz(1:3,1) = 1
        iprint_save = mecca_state%intfile%iprint
        mecca_state%intfile%iprint = -1000
      end if
!  
      call setup_struct(mecca_state)
      if ( mecca_state%fail ) call p_fstop(sname//                      &
     &   ' :: ERROR, unable to continue after structure setup')

!  computing spherical-basis and structure-related properties
!  (mt/asa radii, nearset neighbors, VP ,etc.)
!
      call setOptSphBasis(mecca_state)
      if ( mecca_state%fail ) call p_fstop(sname//                      &
     &   ' :: ERROR, unable to setup optimal sph.basis')

!  intializing radial meshes and potentials
!
      call init_potential(mecca_state)
      if ( mecca_state%fail ) call p_fstop(sname//                      &
     &   ' :: ERROR, unable to initialize potentials')

!  main SCF cycle with calculation of charge density, scf potentials and
!  some other GF-related properties
!
      if ( mecca_state%intfile%nscf > 0 ) then                      !  SCF cycle **
        call iterateSCF(mecca_state)
        if ( mecca_state%fail ) call p_fstop(sname//                    &
     &   ' :: ERROR, unable to complete SCF cycle')

!
        call saveOutput(mecca_state,outinfo) ! subroutine function
        if ( mecca_state%fail ) call p_fstop(sname//                    &
     &   ' :: ERROR, unable to save output')
      end if                                    ! ** end of SCF-cycle **
      if ( mecca_state%intfile%nscf ==0 ) then
        mecca_state%intfile%iprint = iprint_save
      end if

!  computing density of states and/or Bloch spectral function and/or Fermi surface
!      
      call calcDOS_BSF_FS(mecca_state)       ! subroutine function
      if ( mecca_state%fail ) write(6,*) sname//                        &
     &   ' :: ERROR, unable to complete DOS-related calculations'
!
!      call calcFS(mecca_state)
!      if ( mecca_state%fail ) write(6,*) sname//                        &
!     &   ' :: ERROR, unable to complete FS-related calculations'
!     

      if ( recurs == 0 ) then
!  memory deallocation
!  (for recursive calls mmemory should be deallocated,
!   if needed, in upper-level routines)
!      
       call deallocSphDeps(inFile)
       call deallocRunState(mecca_state)      
       call nullMS( mecca_state )
      end if
!
      call fstop('return')

!c     ******************************************************************

      return

!EOC
      contains
       
!BOP
!!IROUTINE: saveOutput
!!INTERFACE:
      subroutine saveOutput(mecca_state,outinfo)
!!DESCRIPTION:
! to collect main output data from {\tt mecca\_state\%work\_box}
! (free energy, total energy, pressure) in {\tt outinfo}

!!DO_NOT_PRINT
      implicit none
!!DO_NOT_PRINT

!!ARGUMENTS:
      type(RunState), intent(in), target :: mecca_state
      character(*) :: outinfo     ! output info
!!REVISION HISTORY:
! Initial Version - A.S. - 2013
!EOP
!
!BOC
      type(IniFile),   pointer :: inFile
      type(Work_data), pointer :: work_box
      real(8) :: tzatom,press,etot,fetot,volume 
!      character(80) :: line80,savename
      if ( .not.associated(mecca_state%intfile) ) return
      if ( .not.associated(mecca_state%rs_box) ) return
      if ( .not.associated(mecca_state%work_box) ) return
      inFile => mecca_state%intfile
      work_box => mecca_state%work_box
      tzatom = g_numNonESsites(inFile)
      press = work_box%press
      etot = work_box%etot
      fetot = work_box%etot - inFile%Tempr*work_box%entropy
      etot = etot/tzatom
      fetot = fetot/tzatom
      volume = mecca_state%rs_box%volume/tzatom
      write(outinfo,'(4(1x,e17.10))') volume,fetot,etot,press

!      write(line80,'(f6.3)') inFile%alat
!      savename = trim(inFile%genName)//'.pot.alat='//                   &
!     &                                          trim(adjustl(line80))
!      call cptxtfile(inFile%io(nu_outpot)%name,trim(savename))

!      savename = trim(inFile%genName)//'.info.alat='//                &
!     &                                          trim(adjustl(line80))
!      call cptxtfile(inFile%io(nu_info)%name,trim(savename))

      return
!EOC
      end subroutine saveOutput
      
!BOP
!!IROUTINE: calcDOS_BSF_FS
!!INTERFACE:
      subroutine calcDOS_BSF_FS( mecca_state )
!!DESCRIPTION:
!  calculation of density of states and/or
!  Bloch spectral function and/or
!  Fermi surface
!
!!USES:
      use mesh3d, only : updateMPmesh
      use raymethod, only : gDosBsfInput,gFsInput,sRayParams
!
!!ARGUMENTS:
      type(RunState), target :: mecca_state
!!REVISION HISTORY:
! Initial Version - A.S. - 2013
!EOP
!
!BOC
      type(IniFile),   pointer :: inFile
      type(DosBsf_data), pointer :: tmp_dosbsf_p =>null()
      type(FS_data),   pointer :: tmp_fs_p => null()
      type(KS_Str),    target, save :: ks_box
      type(KS_Str),   pointer :: tmp_ks_p=>null()
      integer :: nscf_in
      integer nq(3),nu,iec,is
      logical opnd,hxsym
      logical, external :: hexSymm
      character(3), parameter :: sfx(3)=['   ','_up','_dn']
      character(4), parameter :: c_dat='.dat'

      if ( .not.associated(mecca_state%intfile) ) return
      inFile => mecca_state%intfile
      nscf_in = inFile%nscf

      if ( associated(mecca_state%bsf_box) ) then
        tmp_dosbsf_p => mecca_state%bsf_box   
      end if
      call gDosBsfInput(mecca_state%bsf_box)

      if ( mecca_state%bsf_box%ibsfplot>0 ) then
        open(unit=mecca_state%bsf_box%bsf_fileno,                       &
     &                            file=c_ake_band//trim(sfx(1))//c_dat, &
     &                                     status='replace',iostat=iec)
        if ( iec.ne.0 ) then
         write(6,'('' ERROR:: failed to open A(k,E) file, iostat='',i5)'&
     &        ) iec
         mecca_state%bsf_box%ibsfplot = 0
        end if 
      end if

      if ( associated(mecca_state%fs_box) ) then
        tmp_fs_p => mecca_state%fs_box   
      end if
      call gFsInput(mecca_state%fs_box)

      if ( mecca_state%fs_box%ifermiplot>0 ) then
       if ( mecca_state%fs_box%ifermiplot==1 ) then
        do is=1,inFile%nspin
         open(unit=mecca_state%fs_box%fermi_fileno(is),                 &
     &       file=c_ake_bz_area//trim(sfx(inFile%nspin-1+is))//c_dat,   &
     &                                     status='replace',iostat=iec)
         if ( iec.ne.0 ) then
          write(6,'('' ERROR:: failed to open F.S. file, iostat='',i5)' &
     &         ) iec
          exit
         end if 
        end do
       else 
        if( maxval(inFile%sublat(1:inFile%nsubl)%ncomp)>1 ) then
         do is=1,inFile%nspin
          open(unit=mecca_state%fs_box%fermi_fileno(is),                &
     &             file=c_ake_bz_rays//sfx(inFile%nspin-1+is)//c_dat,   &
     &                                     status='replace',iostat=iec)
         if ( iec.ne.0 ) then
          write(6,'('' ERROR:: failed to open F.S. file, iostat='',i5)' &
     &         ) iec
          exit
         end if 
         end do
        else
         do is=1,inFile%nspin
          open(unit=mecca_state%fs_box%fermi_fileno(is),                &
     &             file=c_fermi//sfx(inFile%nspin-1+is)//c_dat,         &
     &                                     status='replace',iostat=iec)
         if ( iec.ne.0 ) then
          write(6,'('' ERROR:: failed to open F.S. file, iostat='',i5)' &
     &         ) iec
          exit
         end if 
         end do
        end if
       end if 
       if ( iec.ne.0 ) mecca_state%fs_box%ifermiplot=0
      end if

      if ( mecca_state%bsf_box%idosplot>0 .or.                          &
     &     mecca_state%bsf_box%ibsfplot>0 .or.                          &
     &     mecca_state%fs_box%ifermiplot>0                              &
     &    ) then
        tmp_ks_p=>mecca_state%ks_box
        inFile%nscf = 0
        nq(1) = mecca_state%bsf_box%nx
        nq(2) = mecca_state%bsf_box%ny
        nq(3) = mecca_state%bsf_box%nz

        if ( mecca_state%bsf_box%dosbsf_kintsch == ksint_scf_sch ) then
         if ( minval(abs(nq)) > 0 ) then
          mecca_state%ks_box => ks_box
          call updateMPmesh(nq,tmp_ks_p,mecca_state) 
         end if 
        else 
         hxsym = hexSymm()
         call sRayParams(nrec_ray=mecca_state%bsf_box%ray_nrec,         &
     &                   nrad_ray=mecca_state%bsf_box%ray_nrad,         &
     &                   nx_ray=nq(1),ny_ray=nq(2),nz_ray=nq(3),        &
     &                   hexbox_ray=hxsym)
        end if 

        call iterateSCF(mecca_state)

        inFile%nscf = nscf_in
        if ( .not. associated(tmp_ks_p,mecca_state%ks_box) ) then
         mecca_state%ks_box => tmp_ks_p
        end if
        nullify(tmp_ks_p)
        call deallocKSstr(ks_box)
        nu = inFile%io(nu_dos)%nunit
        inquire(unit=nu,opened=opnd )
        if ( opnd ) close(nu)
        nu = inFile%io(nu_bsf)%nunit
        inquire(unit=nu,opened=opnd )
        if ( opnd ) close(nu)
        nu = inFile%io(nu_fs1)%nunit
        inquire(unit=nu,opened=opnd )
        if ( opnd ) close(nu)
        nu = inFile%io(nu_fs2)%nunit
        inquire(unit=nu,opened=opnd )
        if ( opnd ) close(nu)
      end if
      mecca_state%bsf_box => tmp_dosbsf_p
      nullify(tmp_dosbsf_p)
      mecca_state%fs_box => tmp_fs_p   
      nullify(tmp_fs_p)
      return
!EOC
      end subroutine calcDOS_BSF_FS

!      subroutine calcFS( mecca_state )
!      use raymethod, only : gFsInput,sRayParams
!      type(RunState), target :: mecca_state
!      type(IniFile),   pointer :: inFile
!      type(FS_data),   pointer :: tmp_fs_p => null()
!      integer :: nscf_in
!      if ( .not.associated(mecca_state%intfile) ) return
!      inFile => mecca_state%intfile
!! initialize fermis....
!      if ( associated(mecca_state%fs_box) ) then
!        tmp_fs_p => mecca_state%fs_box   
!      end if
!      call gFsInput(mecca_state%fs_box)
!      if ( mecca_state%fs_box%ifermiplot>0 ) then
!        nscf_in = inFile%nscf
!        inFile%nscf = 0
!
!        call iterateSCF(mecca_state)
!
!        inFile%nscf = nscf_in
!      end if
!      mecca_state%fs_box => tmp_fs_p   
!      nullify(tmp_fs_p)
!      return
!      end subroutine calcFS

!
!BOP
!!IROUTINE: deallocRunState
!!INTERFACE:
      subroutine deallocRunState( mecca_state )
!!DESCRIPTION:
! deallocation of {\tt mecca\_state} (type RunState)
!
!!ARGUMENTS:
      type(RunState) :: mecca_state
!!REVISION HISTORY:
! Initial Version - A.S. - 2013
!!REMARKS:
!EOP
!
!BOC
      logical all

      if ( associated(mecca_state%rs_box,rs0box) ) then
       call deallocRSstr(rs0box)
       nullify(mecca_state%rs_box)
      end if
      if ( associated(mecca_state%ks_box,ks0box) ) then
       call deallocKSstr(ks0box)
       nullify(mecca_state%ks_box)
      end if
      if ( associated(mecca_state%ew_box,ew0box) ) then
       call deallocEWstr(ew0box)
       nullify(mecca_state%ew_box)
      end if
      if ( associated(mecca_state%jcbn_box,jcbn0box) ) then
       call deallocJacbnVP(jcbn0box)
       nullify(mecca_state%jcbn_box)
      end if
      if ( associated(mecca_state%gf_box,gf0box) ) then
       call deallocGFdata(gf0box)
       nullify(mecca_state%gf_box)
      end if
      if ( associated(mecca_state%gf_out_box,gf_out0box) ) then
       all = .true.
       call deallocGFoutput(gf_out0box,all)
       nullify(mecca_state%gf_out_box)
      end if
      if ( associated(mecca_state%bsf_box,bsf0box) ) then
       call deallocDosBsf(bsf0box)
       nullify(mecca_state%bsf_box)
      end if
      if ( associated(mecca_state%fs_box,fs0box) ) then
       call deallocFSdata(fs0box)
       nullify(mecca_state%fs_box)
      end if
      if ( associated(mecca_state%work_box,work0box) ) then
       call deallocWorkdata(work0box)
       nullify(mecca_state%work_box)
      end if

      return
!EOC
      end subroutine deallocRunState
!
!BOP
!!IROUTINE: mecca_hello
!!INTERFACE:
      subroutine mecca_hello()
!!DESCRIPTION:
!  to print basic input information
!
!!ARGUMENTS:
!!PARAMETERS:
!!REVISION HISTORY:
! Initial Version - A.S. - 2013
!!REMARKS:
!EOP
!
!BOC
      implicit none
      character(60) :: approx='ASA'
      character(60) :: comment='V0:non-var'
      type(xc_mecca_pointer) :: libxc_p  
      character(512) :: xcdescr

      call fstop('CPU_START')
      write(6,*) '                                << MECCA >> '
      write(6,*) '     Multiple-scattering Electronic-structure Code',  &
     &            ' for Complex Alloys'
      write(6,'(19x,''(KKR-CPA code, version '',a,'')''/)') version//   &
     &                 build_date

      if ( mecca_state%intfile%mtasa == 0 ) then
       approx = 'MT-scheme'
!!!  something was lost in MT-branch
       call fstop('MT-scheme (mtasa=0) is disabled')       
      end if
      if ( mecca_state%intfile%mtasa == 1 ) then
       approx = 'ASA-scheme'
       if ( mecca_state%intfile%intgrsch == 1 ) then
        comment = 'V0:var-ASA'
       end if
       if ( mecca_state%intfile%intgrsch == 2 ) then
        comment = 'V0:var-VP'
       end if
      end if
      if ( mecca_state%intfile%mtasa == 2 ) then
       approx = 'spher-scheme-with-rho0_PBC, V0:var'
       comment = '****************************'
       if ( mecca_state%intfile%intgrsch == 1 ) then
        comment = 'ASA-intgr'
       end if
       if ( mecca_state%intfile%intgrsch == 2 ) then
        comment = 'VP-intgr'
       end if
      end if

      write(6,'(6x,a)') ' APPROXIMATION: '//trim(approx)//' + '//       &
     &                     trim(comment)

      if ( abs(mecca_state%intfile%nrel) == 0 ) then
       write(6,'(20x,a)') ': '//'scalar-relativistic for valent and'
       write(6,'(20x,a)') ': '//'relativistic for core electrons'
      else
       write(6,'(20x,a)') ': '//'non-relativistic'
      end if

      if (maxval(mecca_state%intfile%sublat(1:mecca_state%intfile%nsubl)&
     &    %ncomp)>1 ) then
       if ( abs(mecca_state%intfile%ncpa) > 0 ) then
        write(6,'(20x,a,i4)') ': '//'with CPA, max number of iter. =',  &
     &                                    abs(mecca_state%intfile%ncpa)
       end if
      end if

      if ( abs(mecca_state%intfile%nspin) == 1 ) then
       write(6,'(20x,a)') ': '//'non-magnetic'
      else
       if ( abs(mecca_state%intfile%nspin) == 2 ) then
        if ( mecca_state%intfile%ndlm == 1 ) then
         write(6,'(20x,a,i4)') ': '//                                   &
     &                           'DLM spins, max number of CPA iter. =',&
     &                                    abs(mecca_state%intfile%ncpa)
        else
         write(6,'(20x,a)') ': '//'collinear spins'
        end if
        if ( mecca_state%intfile%magnField .ne. 0 ) then
         write(6,'(20x,a,e11.4)')                                       &
     &                       ': '//'with external magn.field (Ry) =',   &
     &                           mecca_state%intfile%magnField
        end if
       else 
        write(6,'('' ERROR :: incorrect input spin value, nspin='',i3)')
        call fstop('mecca_scf')
       end if
      end if 

      call gXCmecca(mecca_state%intfile%iXC,libxc_p)   ! to get xc_mecca_pointer  
      call gInfo_libxc(libxc_p,xcdescr)
      call gXCmecca(-1,libxc_p)                        ! to release xc_mecca_pointer  
      write(6,'(20x,a)') ': functional '//trim(xcdescr)

      write(6,'(20x,a,i4)') ': '//'Lmax =',mecca_state%intfile%lmax

      if ( mecca_state%intfile%Tempr .ne. 0 ) then
         write(6,'(20x,a,e11.4)') ': '//'temperature (Ry) =',           &
     &                      mecca_state%intfile%Tempr
      end if

      write(6,'(/6x,a)') 'SYSTEM id: '//                                &
     &                        trim(mecca_state%intfile%genName)
      if ( mecca_state%intfile%icryst == 0 ) then
       write(6,'(6x,a)') 'Structure description file: '//               &
     &                      trim(mecca_state%intfile%io(nu_xyz)%name)
      end if
      write(6,'(/)')

      return
!EOC
      end subroutine mecca_hello

      end subroutine mecca_scf
