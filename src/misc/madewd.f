      subroutine madewd(lastrs,ndimrs,lastkn,ndimks,nsub1,nbasis,       &
     &                  rsn,xkn,rssq,xknsq,aij,sum1,sum2,dqint,         &
     &                  eta,volume,madmat,istop)
!c    ===========================================================
!c    program calculates the madelung constants
!c    modified by a.s., 9-22-98
!c    written by w.a.s.,jr. 2-21-88
!c
!c***********************************************************************
!c     input :
!c     =====
!c         1.) ewald card
!cccccc        lastrs = number real-space vectors
!cccccc        rsn= real-space vectors
!cccccc        rssq= real-space vectors magnitude squared
!cccccc        lastkn = number reciprocal-space vectors
!cccccc        xkn = reciprocal-space vectors
!cccccc        xknsq= reciprocal-space vectors magnitude squared
!cccccc        eta = ewald parameter
!cccccc        nbasis = number of sublattices
!cccccc        aij = distance between sublattices
!cccccc        nsub1 = the sublattice for which the column of the
!cccccc                madelung matrix is to be calculated
!cccccc        volume = Wigner-Seitz volume
!c
!c   calling sequence
!c   ================
!c    |
!c  intewld                      ! evaluates integral over eta
!c    |
!c    |
!c  madsum                       ! real & recip space sums
!c
!c***********************************************************************
      implicit none
!c
      character istop*10
      character sname*10
      parameter (sname='madewd')
!c
      integer    ndimrs
      integer    ndimks
!c
      integer    lastrs
      integer    lastkn
      integer    nbasis
      integer    ibegin
      integer    nsub1
      integer    nsub2
      integer    i,icount
!c
      real*8     aij(3,*),tmpij(3)
      real*8     dqint(*)
      real*8     xkn(ndimks,3)
      real*8     rsn(ndimrs,3)
      real*8     rssq(*)
      real*8     xknsq(*)
      real*8     madmat(*)
      real*8     sum3
      real*8     rtmp1
      real*8     rtmp2
      real*8     rtmp3
      real*8     r2tmp
      real*8     term
      real*8     volume,volr
      real*8     eta
      real*8     pi,zero,two,eight
      parameter (pi=3.1415926535897932384d0)
      parameter (zero=0.d0)
      parameter (two=2.d0)
      parameter (eight=8.d0)
!c
      complex*16 sum1(nbasis)
      complex*16 sum2(nbasis)

!c    ------------------------------------------------------------------
!c    start madmat calculation
!c    ------------------------------------------------------------------
      sum3 = zero
      volr = volume*(two*pi)**3
      do i = 1,lastkn
        xknsq(i) = xkn(i,1)**2+xkn(i,2)**2+xkn(i,3)**2
      enddo
      do nsub2 =1,nbasis
        ibegin=1
!c    ------------------------------------------------------------------
        if( nsub1 .eq. nsub2 ) then
          ibegin=2
        endif
!c    ------------------------------------------------------------------
!c    subrtact aij from rsn to recalculate rssq which is used in
!c    calculating the real-space integral
!c    ------------------------------------------------------------------

        icount = nsub1+(nsub2-1)*nbasis

        do i=1,3
         tmpij(i) = aij(i,icount)*(2.d0*pi)
        enddo
        do i = 1,lastrs
          rtmp1 = ( rsn(i,1) - aij(1,icount) )*(two*pi)
          rtmp2 = ( rsn(i,2) - aij(2,icount) )*(two*pi)
          rtmp3 = ( rsn(i,3) - aij(3,icount) )*(two*pi)
          rssq(i) = rtmp1*rtmp1 + rtmp2*rtmp2 + rtmp3*rtmp3
        enddo
!c    ------------------------------------------------------------------
        call dsort( rssq, r2tmp, lastrs,  1 )
!c    ------------------------------------------------------------------
!c    perform the real-space integral
!c    ------------------------------------------------------------------
        call intewld(rssq,eta,dqint,lastrs)
!c    ------------------------------------------------------------------
!c    perform the reciprocal-space sum and real-space sum
!c    ----------------------------------------------------------------
        call madsum(lastkn,ndimks,lastrs,nsub1,nsub2,ibegin,            &
     &              dqint,eta,xkn,                                      &
     &              xknsq,tmpij,volr,sum1,sum2,istop)
!c    ----------------------------------------------------------------
        term = eight*pi*pi/( volr*eta )
        madmat(nsub2) = two*pi*( sum1(nsub2) + sum2(nsub2) ) - term
        sum3 = sum3 + madmat(nsub2)
!c       write(6,*) nsub1,nsub2,sum1(nsub2),sum2(nsub2)
!c        write(9,*) madmat(nsub2)
!c    ----------------------------------------------------------------
      enddo                                  ! end do loop over nsub2
!c    ----------------------------------------------------------------
!c      write(6,'('' nsub1,sum3 '',i5,d16.7)')
!c     >nsub1,sum3
!c    ----------------------------------------------------------------
      if( istop .eq. sname ) then
        call fstop(sname)
      endif
!c    ----------------------------------------------------------------
      return

      CONTAINS

      subroutine intewld(rssq,eta,dqint,nrsn)
!c     ================================================================
!c     ----------------------------------------------------------------
!c     Written by W.A.S., Jr.   Feburary 18, 1993
!c     based on the intfac.f program written by Y. Wang
!c     Ref: Chapter 7, "Handbook of Mathematical Functions"
!c          Edited by Milton Abramowitz and Irene A. Stegun
!c     ----------------------------------------------------------------
      implicit   none
!c
      character  sname*10
      parameter (sname='intfac')
!c
!c
      integer    nrsn
      integer    i
!c
      double precision erfc
      external erfc
      real*8     dqint(*)
      real*8     rssq(*)
      real*8     eta
      real*8     pi
      real*8     a
      real*8     r
      real*8     x1
      real*8     zero
      real*8     tol
      real*8     half
      parameter (pi=3.1415926535897932384d0)
      parameter (zero=0.d0)
      parameter (tol=0.000001d0)
      parameter (half=0.5d0)
!c
!c     do i = 1,20
!c       write(6,'('' i,rssq '',i5,d14.6)') i,rssq(i)
!c     enddo
      do i = 1, nrsn
        if( rssq(i) .le. tol ) then
          dqint(i) = zero
        else
          a = sqrt( eta )*half
          r = sqrt( rssq(i) )
          x1 = r*a
          dqint(i) = sqrt( pi )*half*erfc(x1)/r
!c         if( i .le. 20 ) then
!c           write(6,'('' a,r,x1,erfc '',4d14.6)') a,r,x1,erfc(x1)
!c         endif
        endif
      enddo
!c
      return
      end subroutine intewld

      subroutine madsum(lastkn,ndimks,lastrs,i1,i2,ibegin,              &
     &                  dqint,eta,xkn,                                  &
     &                  xknsq,aij,tau,sum1,sum2,istop)
!c    =================================================================
!c
!c    -----------------------------------------------------------------
!c    performs ewald summation for kkr struc consts.
!c    requires 'lastkn,lastrs,pqint,eta,xkn,aij,i1,i2'
!c    returns 'madelung sum'
!c    -----------------------------------------------------------------
      implicit none
!c
      character istop*10
      character  sname*10
      parameter (sname='madsum')
!c
      integer    ndimks
!c
      integer    lastkn
      integer    lastrs
      integer    ibegin
      integer    i1
      integer    i2
      integer    i

!c
      real*8     dqint(*)
      real*8     xkn(ndimks,3)
      real*8     xknsq(*)
      real*8     aij(3)
      real*8     eta
      real*8     dot
      real*8     r0tm
!c      real*8     factor
      real*8     tau
      real*8     pi,zero
      real*8     two
      real*8     four
      parameter (pi=3.1415926535897932384d0)
      parameter (zero=0.d0)
      parameter (two=2.0d+00)
      parameter (four=4.0d+00)
!c
      complex*16 sum1(*)
      complex*16 sum2(*)
      complex*16 czero
      parameter (czero=(0.d0,0.d0))
!c    -----------------------------------------------------------------
!c     calculate real space sum (sum2) and recip. space
!c     sum (sum1).
!c    -----------------------------------------------------------------
!c     note sum starts at 2 since kn=0.0 of 1/(rs-aij) is
!c     canceled by kn=0.0 of the 1/rs sum.
!c    -----------------------------------------------------------------
      sum1(i2) = czero
!c    -----------------------------------------------------------------
      do i = 2,lastkn
        dot =                                                           &
     &  ( xkn(i,1)*aij(1) + xkn(i,2)*aij(2) + xkn(i,3)*aij(3) )
        sum1(i2) = sum1(i2) + ( exp( -xknsq(i)/eta )/xknsq(i) )         &
     &                      *exp( dcmplx(zero,dot) )
      enddo
!c    -----------------------------------------------------------------
      sum1(i2) = ( ( four*pi )/ tau )*sum1(i2)
!c    -----------------------------------------------------------------
!c     note for calculation of aij=0.0 term ibegin=2.
!c    -----------------------------------------------------------------
      sum2(i2)=czero
      do i=ibegin,lastrs
        sum2(i2) = sum2(i2) + dqint(i)
      enddo
      sum2(i2) = two/sqrt( pi )*sum2(i2)
!c    -----------------------------------------------------------------
!c     note for i1 = i2 subtract off sqrt( eta/pi )
!c    -----------------------------------------------------------------
!c      write(6,'('' i1,i2,sum1,sum2 '',2i5,4d12.5)')
!c     > i1,i2,sum1(i2),sum2(i2)
      if( i2 .eq. i1 ) then
        r0tm = sqrt( eta/pi )
        sum2(i2) = sum2(i2) - r0tm
      endif
!c
      if( istop .eq. sname ) then
        call fstop(sname)
      endif
      return
      end subroutine madsum

      end subroutine madewd
