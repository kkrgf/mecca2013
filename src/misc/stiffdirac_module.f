      module stiffdirac

      implicit none
      private
      public odeint1,set_dirac_params,derivs1

      integer, parameter :: maxtry=100
      real*8 xstif(maxtry)
      real(8) ystif(2,maxtry)
      real(8) estif(2,maxtry)
      integer ntry
!
      real*8  c
      real*8  dk
      real(8) en
!
      integer ipot(2)
      real*8 rfit(2)
      real*8 zfit(2)

      contains

      subroutine dirac_matrix(x,A,rv0,dz)
      implicit none
      real(8), intent(in) :: x
      real(8), intent(out) :: A(2,2)
      real(8), intent(out) :: rv0,dz
      real(8) r          
          r = exp(x)
          call zinterp(r,rv0,dz)
          A(1,1) = -dk
          A(2,1) = -(en*r - rv0)/c 
          A(1,2) = c*r - A(2,1)
          A(2,2) = dk
      return
      end subroutine dirac_matrix

      subroutine set_dirac_params(c_in,en_in,dk_in,ipot0,rfit0,zfit0)
      real(8), intent(in), optional :: c_in
      real(8), intent(in), optional :: en_in
      real(8), intent(in), optional :: dk_in
      integer, intent(in), optional :: ipot0(2)
      real(8), intent(in), optional :: rfit0(2)
      real(8), intent(in), optional :: zfit0(2)

      if ( present(rfit0) ) then
        rfit = rfit0  
      end if
      if ( present(zfit0) ) then
        zfit = zfit0  
      end if
      if ( present(ipot0) ) then
        ipot = ipot0
      end if
      if ( present(c_in) ) then
        c = c_in  
      end if
      if ( present(en_in) ) then
        en = en_in  
      end if
      if ( present(dk_in) ) then
        dk = dk_in  
      end if
      return
      end subroutine set_dirac_params

      subroutine zinterp(x,z,dz)
!c     ==================================================================
!c     returns the value of
!c          z(x)
!c          d/dx z(x)
!c     between mesh points ipot1 and ipot2
!c     we assume z(x) is monotonically decreasing
!c     when possible we assume a log fit of z(x)
!C     NOTE OUR METHOD DOES NOT ASSUME CONTINUITY OF SLOPE BETWEEN
!C     ADJACENT GRID INTERVALS
!c     ==================================================================
      implicit real*8 (a-h,o-z)

!      integer ipot
!      real*8 rfit
!      real*8 zfit
!      common/zfit_com/rfit(2),zfit(2),ipot(2)

!c     ------------------------------------------------------------------

      z=0.0d0
      dz=0.0d0
      dx=x-rfit(1)
      dr=rfit(2)-rfit(1)
      p=0.0d0
      if(zfit(1).gt.0.d0) then
        p=zfit(2)/zfit(1)
      endif
      if(p.gt.0.0d0 .and. p.lt.1.0d0) then
        s=dlog(p)/dr
        z =zfit(1)*exp( dx*s )
        dz=zfit(1)*exp( dx*s )*s
      else
        s=( zfit(2)-zfit(1) )/dr
        z =zfit(1)+dx*s
        dz=s
      endif
      return
      end subroutine zinterp

      subroutine odeint1(x1,y1,x2,y2,ierr,                              &
     &                   eps,hmin,                                      &
     &                   xss,yss,dyss,rvss,errss,nss,maxss)
!c     ==================================================================
!c     this is an adaptation of the numberical recipies routine odeint
!c     for a system of two coupled differential equations
!c
!c     input:
!c        x1       ==> initial ordinate
!c        y1(2)    ==> initial value
!c        x2       ==> final ordinate
!c
!c     input from common:
!c        eps      ==> desired accuracy
!c        hmin     ==> sub step size crash minimum
!c        maxss    ==> allocation for sub step arrays
!c
!c        nss should be set to 0 before first call to odeint1
!c
!c     output:
!c        y2(2)    <== value of coordinate at x2
!c
!c     output from common for cumulative calls to odeint1
!c        xss       <== sub step ordinates
!c        yss       <== sub step values
!c        dyss      <== sub step dydx values
!c        errss     <== sub step errors
!c        nss       <== total substeps
!c
!c     ==================================================================
      implicit none
!
      real*8 x1
      real*8 x2
      real(8) y1(2)
      real(8) y2(2)
      integer ierr
      real*8 eps
      real*8 hmin

      integer maxss
      real*8 xss(maxss)
      real*8 rvss(maxss)
      real(8) yss(2,maxss)
      real(8) dyss(2,maxss)
      real(8) errss(2,maxss)
      integer nss


      real*8 htry
      real*8 hdid
      real*8 hnext

      real*8 x
      real(8) y(2)
      real(8) dydx(2)
      real(8) err(2)
      real(8) yscal(2)

      integer nstp
      integer i
      integer ibomb
      real*8  xtry
      real*8  z
      real*8  dz
      real(8) epsn

      real*8 tiny
      parameter (tiny=1.d-30)
      integer maxstp
      parameter (maxstp=500)
!c     ==================================================================

      ierr=0
      ibomb=0
      hdid=0.0d0  !arbitrary intitialization;
      hnext=0.0d0 !proper hdid, hnext values are returned by stiff1

      htry=x2-x1

      x=x1
      y(1)=y1(1)
      y(2)=y1(2)
      call derivs1(x,y,dydx)

      nss=1
      xss(nss)=x
      do i=1,2
        yss(i,nss)=y(1)
        dyss(i,nss)=dydx(1)
        errss(i,nss)=dcmplx(0.0d0,0.0d0)
      enddo

      epsn = eps
!c     take at most maxstp sub-steps
      do 20 nstp=1,maxstp

        do i=1,2
          yscal(i)=abs(y(i))+abs(htry*dydx(i))+tiny
        enddo

        xtry=x+htry
        if((xtry-x2)*(xtry-x1).gt.0.0d0) htry=x2-x

        call stiff1(x,y,dydx,htry,hdid,hnext,epsn,err,yscal,ibomb)
        if(ibomb.ne.0) then
          write(*,*) 'error in subroutine stiff'
          ierr=-ibomb
          return
        endif

        if(nss.ge.maxss)then
          write(*,*) 'exceeded storage of substep in odeint1'
          ierr = 1
          return
        else
          nss=nss+1
          call derivs1(x,y,dydx)
          call zinterp(x,z,dz)
          xss(nss)=x
          rvss(nss)=z
          do i=1,2
            yss(i,nss)=y(i)
            dyss(i,nss)=dydx(i)
            errss(i,nss)=err(i)
          enddo
        endif

        if((x-x2)*(x2-x1).ge.0.0d0)then
          do i=1,2
            y2(i)=y(i)
          enddo
          return
        endif

        if(dabs(hnext).lt.hmin) then
          if ( hmin <= tiny ) then
            write(*,*) 'stepsize smaller than minimum in odeint'
            ierr=2
           return
          else
           hnext = x2-x
           epsn = 2*epsn
          end if
        endif
        htry=hnext
20    continue

!      write(*,*) 'too many steps in odeint'
      ierr=3
      return
      end subroutine odeint1

      subroutine stiff1(x,y,dydx,htry,hdid,hnext,                       &
     &                  eps,err,yscal,ierr)
!c     ==================================================================
!c     integration routine for a particular form of 2 coupled stiff
!c     differential equations
!c
!c     this is an adaptation of the numerical recipies routine stiff
!c     which uses a fourth order rosenbrock step for integrating
!c     stiff ordinary differrential equations
!c
!c     input:
!c       x    ==> initial coordinate
!c       y    ==> inital value
!c       dydx ==> initial value of derivitive
!c       htry ==> trial value for the increment in x
!c       eps  ==> desired accuracy
!c       yscal==> component dependent accuracy criterion
!c
!c     output:
!c       hdid <== acutual step size used fullfilling accuracy
!c       hnext<== estimated next step size
!c       x    <== new x
!c       y    <== new value
!c       err  <== component errors
!c       ierr <== abnormal termination code
!c     ==================================================================
      implicit none

      real*8 x
      real(8) y(2)
      real(8) dydx(2)
      real(8) err(2)
      real(8) yscal(2)
      real*8 htry
      real*8 hdid
      real*8 hnext
      real*8 eps
      integer ierr


      logical dbug
      integer i
      real*8 h
      real*8 h2
      real*8 h25
      real*8 h108
      real*8 hm0
      real*8 gam
      real*8 gm1
      real*8 gm2
      real*8 gm3
      real*8 s
      real*8 errmax
      real(8) d11
      real(8) d12
      real(8) d21
      real(8) d22
      real(8) odenom


!      integer maxtry
!      parameter (maxtry=40)
!      real*8 xstif
!      complex*16 ystif
!      complex*16 estif
!      integer ntry
!!c     this common block usefull for debugging
!      common /stiffcom/ xstif(maxtry),ystif(2,maxtry),                  &
!     &                  estif(2,maxtry),ntry

      real*8 xsav
      real(8) ysav(2)
      real(8) dysav(2)
      real(8) dfdx(2)
      real(8) dfdy(2,2)

      real(8) ua(2)
      real(8) ub(2)
      real(8) uc(2)
      real(8) ud(2)
      real(8) ga(2)
      real(8) gb(2)
      real(8) gc(2)
      real(8) gd(2)


      real*8 safety
      parameter (safety=0.9d0)
      real*8 grow,pgrow
      parameter (grow=1.5d0,pgrow=-.25d0)
      real*8 shrnk,pshrnk
      parameter (shrnk=0.5d0,pshrnk=-1.d0/3.d0)
      real*8 errcon
      parameter (errcon=.1296d0)

      real*8 b1,b2,b3,b4
      parameter (b1=228.0d0,b2=54.0d0,b3=25.0d0,b4=125.0d0)
      real*8 e1,e2,e4
      parameter (e1=34.0d0,e2=21.0d0,e4=125.0d0)
!c     ==================================================================


      ierr=0

      xsav=x
      do i=1,2
        ysav(i)=y(i)
        dysav(i)=dydx(i)
      enddo

      call jacob1(xsav,ysav,dfdx,dfdy)

      h=htry
      do 25 ntry=1,maxtry

!c       perform matrix inversions analytically
!c       --------------------------------------------
!c       let s=gam*h and   f_dot=(  a  b  )
!c                               (  c  d )
!c       then {(1/s)(1-s*f_dot)}^-1 =
!c                             (    1-sd        sb  )
!c                   s/(1-sw)  (      sc      1-sa )
!c
!c       1-sw=(1-sa)(1-sd)-(sb)(sc)
!c       --------------------------------------------
        gam=0.5d0
        s=gam*h
        d11=1.0d0-s*dfdy(2,2)
        d12=      s*dfdy(1,2)
        d21=      s*dfdy(2,1)
        d22=1.0d0-s*dfdy(1,1)
        odenom=gam/(d11*d22-d12*d21)


!c       set up right hand side ga
        hm0=0.5d0*h
        do 14 i=1,2
          ua(i)=dysav(i)+hm0*dfdx(i)
14      continue
!c       matrix multiply ua by s*((1-s dfdy)^-1)
        ga(1)=( d11*ua(1) + d12*ua(2) )*odenom
        ga(2)=( d21*ua(1) + d22*ua(2) )*odenom


        h2=2.0d0*h
        do 15 i=1,2
          y(i)=ysav(i)+h2*ga(i)
15      continue
        x=xsav+h
        call derivs1(x,y,dydx)

!c       set up right hand side gb
        hm0=-1.5d0*h
        gm1=-8.0d0
        do 16 i=1,2
          ub(i)=dydx(i)+hm0*dfdx(i)+gm1*ga(i)
16      continue
!c       matrix multiply ub by s*((1-s dfdy)^-1)
        gb(1)=( d11*ub(1) + d12*ub(2) )*odenom
        gb(2)=( d21*ub(1) + d22*ub(2) )*odenom

        h25=h/25.0d0
        gm1=48.0d0
        gm2= 6.0d0
        do 17 i=1,2
          y(i)=ysav(i)+h25*( gm1*ga(i)+gm2*gb(i) )
17      continue
        x=xsav+(3.0d0*h/5.0d0)
        call derivs1(x,y,dydx)


!c       set up right hand side gc
        hm0=121.0d0*h/50.0d0
        gm1=(372.0d0/25.0d0)
        gm2=( 60.0d0/25.0d0)
        do 18 i=1,2
          uc(i)=dydx(i)+hm0*dfdx(i)+gm1*ga(i)+gm2*gb(i)
18      continue
!c       matrix multiply uc by s*((1-s dfdy)^-1)
        gc(1)=( d11*uc(1) + d12*uc(2) )*odenom
        gc(2)=( d21*uc(1) + d22*uc(2) )*odenom


!c       set up right hand side g4
        hm0=29.0d0*h/250.0d0
        gm1=(-112.0d0/125.0d0)
        gm2=(- 54.0d0/125.0d0)
        gm3=(- 50.0d0/125.0d0)
        do 19 i=1,2
          ud(i)=dydx(i)+hm0*dfdx(i)+gm1*ga(i)+gm2*gb(i)+gm3*gc(i)
19      continue
!c       matrix multiply ud by s*((1-s dfdy)^-1)
        gd(1)=( d11*ud(1) + d12*ud(2) )*odenom
        gd(2)=( d21*ud(1) + d22*ud(2) )*odenom


        h108=h/108.0d0
        do 21 i=1,2
          y(i)=ysav(i)+h108*( b1*ga(i)+b2*gb(i)+b3*gc(i)+b4*gd(i) )
          err(i)=      h108*( e1*ga(i)+e2*gb(i)         +e4*gd(i) )
21      continue

        x=xsav+h

        xstif(ntry)=x
        do 22 i=1,2
          ystif(i,ntry)=y(i)
          estif(i,ntry)=err(i)
22      continue

        if(x.eq.xsav) then
          write(*,*) 'stepsize not significant in stiff1'
          ierr= 1
          return
        endif

        errmax=0.0d0
        do 23 i=1,2
          errmax=dmax1(errmax,abs(err(i)/yscal(i)))
23      continue
        errmax=errmax/eps

        if(errmax.le.1.0d0)then
          hdid=h
          if(errmax.gt.errcon)then
            hnext=safety*h*errmax**pgrow
          else
            hnext=grow*h
          endif
          return
        else
          hnext=safety*h*errmax**pshrnk
          if(hnext.lt.shrnk*h) hnext=shrnk*h
          h=hnext
        endif

25    continue

      write(*,*) 'exceeded maxtry in stiff1'
      ierr = 2
      return
      end  subroutine stiff1

      subroutine derivs1(r,Y,Ydot)
      implicit none
      real(8), intent(in) :: r
      real(8), intent(in) :: Y(2)
      real(8), intent(out) :: Ydot(2)
      real(8) :: A(2,2),z,dz
      call dirac_matrix(r,A,z,dz)
      Ydot(1) = A(1,1)*Y(1)+A(1,2)*Y(2)
      Ydot(2) = A(2,1)*Y(1)+A(2,2)*Y(2)
      return
      end subroutine derivs1

      subroutine jacob1(x,F,Fr,AJ)
      implicit none
      real(8), intent(in) :: x
      real(8), intent(in) :: F(2)
      real(8), intent(out) :: Fr(2)
      real(8), intent(out) :: AJ(2,2)
      real(8) :: z,dz,r
      call dirac_matrix(x,AJ,z,dz)
      r = exp(x)
      Fr(1) = r*(c+en/c-dz/c)*F(2)
      Fr(2) = -r*(en/c-dz)*F(1)
      return
      end subroutine jacob1

      end module stiffdirac

