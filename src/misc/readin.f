!c =====================================================================
      subroutine readin(                                                &
     &                  nproc,ntask,MP_NAME,                            &
     &                  nunit,fname,ist,ifm,nopen,                      &
     &                  istop,iprint,                                   &
     &                  lmax,kkrsz,ivar_mtz,                            &
     &                  mtasa,nspin,ndlm,nrel,                          &
     &                  Rrsp,Rksp0,Rsmall,Rnncut,                       &
     &                  enmax,eneta,                                    &
     &                  nmesh,nq,enlim,                                 &
     &                  namk,icryst,nsublat,alat,boa,coa,invadd,        &
     &                  rmt_comp,rws_comp,iadjsph_ovlp,                 &
     &                  komp,name,atcon,                                &
     &                  ztotss,zvalss,zsemss,zcorss,zcortot,            &
     &                  ivpoint,ikpoint,                                &
     &                  itmax,                                          &
     &                  igrid,etop,eibot,npar,esmear,                   &
     &                  nscf,npts,alpha,beta,ebot,eitop,ibot_cont,      &
     &                  rslatt,basis,natom,itype,volume,numbsub,        &
     &                  idosplot,ibsfplot,dosbsf_kintsch,               &
     &                  dosbsf_nepts,dosbsf_emin,dosbsf_emax,           &
     &                  bsf_ksamplerate,                                &
     &                  bsf_nkwaypts,bsf_kwaypts,bsf_kptlabel,          &
     &                  bsf_fileno,                                     &
     &                  ifermiplot,fermi_en,fermi_korig,fermi_orilabel, &
     &                  fermi_nkpts,fermi_kpts,fermi_kptlabel,          &
     &               fermi_gridsampling,fermi_raysampling,fermi_fileno, &
     &                  isDLM,isflipped,flippoint                       &
     &)

!c =====================================================================
!C      implicit double precision (a-h,o-z)
      implicit none
      include 'lmax.h'
      include 'mkkrcpa.h'
      include 'imp_inp.h'
!c =====================================================================

      integer    imethod,isparse,nproc,ntask,nunit(0:ipunit)
      character*(*) MP_NAME
      character  fname(0:ipunit)*32
      integer    ist(0:ipunit),ifm(0:ipunit)
      character  istop*10
      integer    iprint,lmax,kkrsz,ivar_mtz,mtasa,nspin,ndlm,nrel
      real*8     Rrsp,Rksp0,Rsmall,Rnncut,enmax,eneta
      integer    nmesh,nq(3,ipmesh)
      real*8     enlim(2,ipmesh-1)
      character  namk*32
      integer    icryst,nsublat
      real*8     alat,boa,coa,rmt(ipsublat),rws(ipsublat)
      real*8     rmt0(ipbase),rws0(ipbase)
!CALAM
      real*8     rmt_comp(ipcomp,ipsublat),rws_comp(ipcomp,ipsublat)
      real*8     r_fract(ipcomp,ipsublat),vfract(ipcomp,ipsublat)
!CALAM
      integer    invadd
      integer    komp(ipsublat)
      character  name(ipcomp,ipsublat)*10
      real*8     atcon(ipcomp,ipsublat)
      real*8     zvalss(ipcomp,ipsublat)
      real*8     zsemss(ipcomp,ipsublat)
      real*8     zcorss(ipcomp,ipsublat)
      real*8     zcortot(ipcomp,ipsublat)  ! quick fix for read/write pot
      real*8     ztotss(ipcomp,ipsublat)   ! quick fix for read/write pot
      integer    ivpoint(ipsublat),ikpoint(ipcomp,ipsublat)
      integer    itmax,igrid
      real*8     ebot,etop,eitop,eibot,esmear
      integer    npts,npar
      integer    nscf
      real*8     alpha,beta,sum

      integer    iadjsph_ovlp
!c      integer    imixrho

      ! density of states and bloch spectral function
      integer idosplot,ibsfplot,dosbsf_kintsch,iec
      integer dosbsf_nepts,bsf_ksamplerate,bsf_nkwaypts
      real*8  dosbsf_emin,dosbsf_emax
      real*8 :: bsf_kwaypts(3,ipbsfkpts)
      character*32 :: bsf_kptlabel(ipbsfkpts)
      character*32 bsf_file
      integer bsf_fileno

      real*8 fermi_korig(3),fermi_en
      real*8 fermi_kpts(3,ipbsfkpts)
      integer fermi_nkpts,fermi_gridsampling,fermi_raysampling
      integer ifermiplot,fermi_fileno(2)
      character*32 :: fermi_kptlabel(ipbsfkpts)
      character*32 :: fermi_orilabel

      integer itype(ipbase),natom,numbsub(ipsublat),nsubi,jjj
      real*8 rslatt(3,3),basis(3,ipbase),ba,ca,scale,volume

      integer numspec(ipcomp)
      integer ncmpn,j,isubl,icmpn,icpa,ispec,ibot_cont
      integer izv

      integer nsublat0, nsub0

      real*8 zsemcm(ipbase), zcorcm(ipbase), zvalcm(ipbase)

      character(len=300) :: inputline
      character(len=100) :: dlmstring
      character(len=3) :: dlmtag  = 'DLM'
      character(len=3) :: dlmtag2 = 'Dlm'
      character(len=3) :: dlmtag3 = 'dlm'
      logical :: isDLM(ipsublat)
      integer :: dlm_pos
      real*8 :: dlm_mix

      logical :: isflipped(ipsublat)
      integer :: flippoint(ipsublat)
      integer :: flipcnt

      character  sname*10
      parameter  (sname=' READIN:: ')
      character  text*80,coerr*80
      character  iname*32,inp_file*32
      character :: blnk*32 = '                                '
      character  cstat*7
      character  cfmt*11
!c      character  stats(4)*7
!c     *                   /'old    ','unknown','new    ','replace'/
!c      character  fmt(2)*11/'formatted  ','unformatted'/
      character  st*1,fm*1
      character*6 mix1,mix2,cell,keep,potin,potout,mdlng,spkpt,dop
      character*6 magnfield,dos_fname,bsf_fname
      parameter  (mix1   = '#BROY1')
      parameter  (mix2   = '#BROY2')
      parameter  (cell   = '#CELL')
      parameter  (keep   = '#KEEP')
      parameter  (potin  = '#INPT')
      parameter  (potout = '#OUTPT')
      parameter  (mdlng  = '#MDLNG')
      parameter  (spkpt  = '#SPKPT')
      parameter  (dop    = '#DOP')
      parameter  (magnfield = '#FIELD')
      parameter  (dos_fname = '#DOS')
      parameter  (bsf_fname = '#BSF')

      integer    iopen,nopen,nopen0,i,ic,imesh,nu,irmt
      integer    nsub,intread

      real*8     realv1,realv2
!c      real*8 toler
      real*8  rmtd,tconc,vtot

      integer ntmp
      real*8 rtmp1(3,3),rtmp2(3,ipbase),rtmp3,rtmp4,rtmp5

      character  obroy1*50,obroy2*50
      common/broydn/obroy1,obroy2

      logical isdis
!C*****************************************************************
!CALAM  Declaring the valence number of electrons for 103 elements
!C                   of the periodic table
!C*****************************************************************
      integer izval(103)
      character*2 symbol(0:103)

       data symbol /'--','H','He','Li','Be','B','C','N','O','F','Ne',   &
     &               'Na','Mg','Al','Si','P','S','Cl','Ar','K','Ca',    &
     &               'Sc','Ti','V','Cr','Mn','Fe','Co','Ni','Cu','Zn',  &
     &               'Ga','Ge','As','Se','Br','Kr','Rb','Sr','Y','Zr',  &
     &               'Nb','Mo','Tc','Ru','Rh','Pd','Ag','Cd','In','Sn', &
     &               'Sb','Te','I','Xe','Cs','Ba','La','Ce','Pr','Nd',  &
     &               'Pm','Sm','Eu','Gd','Tb','Dy','Ho','Er','Tm','Yb', &
     &               'Lu','Hf','Ta','W','Re','Os','Ir','Pt','Au','Hg',  &
     &               'Tl','Pb','Bi','Po','At','Rn','Fr','Ra','Ac','Th', &
     &               'Pa','U','Np','Pu','Am','Cm','Bk','Cf','Es','Fm',  &
     &               'Md','No','Lr'/
       data izval /1,2,1,2,3,4,5,6,7,8,                                 &
     &              1,2,3,4,5,6,7,8,1,2,                                &
     &              3,4,5,6,7,8,9,10,11,12,                             &
     &              13,14,5,16,17,18,1,2,3,4,                           &
     &              5,6,7,8,9,10,11,12,13,14,                           &
     &              15,16,17,18,1,2,3,4,5,6,                            &
     &              7,8,9,10,11,12,13,14,15,16,                         &
     &              17,18,5,6,7,8,9,10,11,12,                           &
     &              13,14,15,16,17,18,1,8,3,4,                          &
     &              5,6,7,8,9,10,11,12,13,14,                           &
     &              15,16,17/

        ! warning ^^ altered As

!C*****************************************************************

!c  "Defaults":

      nproc = 1
      ntask = 1
      MP_NAME='mecca.exe'
      imethod = ipmethod
      istop  = 'main'
      iprint = -100     !  level of minimal print
      lmax   = iplmax
      mtasa  = 1        !  ASA
      nspin  = 1        !  Non-magn.
      ndlm   = 0        !  NO DLM
      nrel   = 22       !  Nonrelativ.
      Rrsp   = 3.d0
      Rksp0  = 5.d0
      enmax  = 1.d0     !  best value -- (efermi+delta)*(alat/2/pi)**2
      eneta  = 2.5d0    !  between 0 and 2.8 -- limitation from d003-routine
      icryst = 0
      nmesh  = 1
      enlim(1,1) = 0.d0
      enlim(2,1) = 0.25d0
      nq(1,1)= 16       !  if A=B=C
      nq(2,1)= 16       !  nq(2)~nq(1)*A/B  ?
      nq(3,1)= 16       !  nq(3)~nq(1)*A/C  ?
!c
!c  if nq(i,*)>0:    regular mesh (without k=(0,0,0))
!c  if nq(i,*)<0:
!c   for  i= 1 or 2 or 3           -- Full Zone integration
!c   for i={1,2} or {1,3} or {2,3} -- shifted mesh (to include k=(0,0,0))
!c   for i= 1 and 2 and 3          -- to combine regular and "shifted" me !shes
!c
      nscf   = 1
      alpha  = 0.08    !  for chg/pot mixing (for simple mixing ~0.4-0.8)
      beta   = 0.5     !  for moment mixing
!c      ichg   = 0       !  charge density mixing
!c      imixrho= 1       !  do Broyden scheme

      invadd = 1       ! 1(0) to add (Not to Add) inversion to group opers. in K-space

      Rnncut = 0.d0
      Rsmall = 0.d0


!c  Read input file name
!c      read(5,*,end=3000)
!c      read(5, '(a32)' ,err=3002,end=3000 ) iname
!c      if(iname.eq.'include') then
!c       write(6,*)
!c       call PrintDef(1)
!c       call PrintDef(2)
!c       write(6,*)
!c       call fstop(sname//'::'//iname)
!c      end if
!c      write(*,*)
!c      if(iname.ne.blnk)
!c     * write(*,'('' Input file name:'',(a))') iname
      nunit(0) = 5
      nopen = 0
      fname(0) = inp_file


!c     ------------------------------------------------------------------
!c  Read requested number of processors and number of tasks
!c  Intel and PVM require ntask=1.
!c  CRAY multi-tasking requires ntask > 1.
!      read(5,*,err=3002,end=3000)
!      read(5,*,err=3002,end=3000) nproc, ntask, MP_NAME
!      write(*,'('' Requested No. of processors '',i5)') nproc
!      write(*,'('' Requested No. of tasks      '',i5)') ntask
!      write(*,'('' Executable module              '',(a))') MP_NAME
!c     ------------------------------------------------------------------

!c  Open files using fortran77 open statements

!c     ------------------------------------------------------------------

        write(6,*) ''
        write(6 ,'(a)') ' ========================================'     &
     &    //'=================================='
        write(6,'(''  File Management'')')
        write(6 ,'(a)') ' ========================================'     &
     &    //'=================================='
        write(6,*) ''
        write(6,'(''  List of files opened: '',/)')

!c  Read in names of  input/output files.................
!c
!c        nunit(0) = 5  -- input-file
!c        nunit(1) = 6  -- print-file
!c        nunit(2) = 60 -- structure-file
!c        nunit(3) = 61 -- mixing-file
!c        nunit(4) = 62 -- mixing-file
!c        nunit(5) = 22 -- in-potential file
!c        nunit(6) = 23 -- out-potential file
!c        nunit(7) = 63 -- keep-file
!c        nunit(8) = 64 -- madelung-file
!c        nunit(9) = 65 -- special k-points-file
!c        nunit(10)= 66 -- dop-matrices file
!c        nunit(11)= 24 -- magnetic field (in Tesla)
!c
!c  User may choose any unit numbers except 5-10,21-29,60-69
!c
       nunit(1) = 6
       ist(1)=2
       ifm(1)=1
       nopen = nopen+1
       read(5,'(a32)',end=3000) iname
       fname(1) = iname
       write(*,1001) 1,nunit(1),fname(1)
1001   format(2x,i2,'.  unit number =  ',i3,                            &
     &        '  file name is <<',a32,'>>')

!c  Initialise unit 6 ...................................
       if(fname(1).ne.blnk) then
        open(unit=6,file=iname,form='formatted')
        write(6,'(1x,77(''=''))')
        write(6,'(//''  file management: list of files opened '',       &
     & ''by this program''/)')
        write(*,1001) 1,nunit(1),fname(1)
       end if

!c  Initialize remaining file names and unit numbers
!c  the names and unit numbers are in the run file.

       nunit(2) = 60
       fname(2) = cell
       ist(2)=2
!c                                   unknown
       ifm(2)=1
       nopen = nopen+1

       nunit(3) = 61
       fname(3) = mix1
       ist(3)=4
!c                                   replace
       ifm(3)=2
       nopen = nopen+1

       nunit(4) = 62
       fname(4) = mix2
       ist(4)=4
!c                                   replace
       ifm(4)=2
       nopen = nopen+1

       nunit(5) = 22
       fname(5) = potin
       ist(5)=1
!c                                   old
       ifm(5)=1
       nopen = nopen+1

       nunit(6) = 23
       fname(6) = potout
       ist(6)=2
!c                                   unknown
       ifm(6)=1
       nopen = nopen+1

       nunit(7) = 63
       fname(7) = keep
       ist(7)=2
!c                                   unknown
       ifm(7)=1
       nopen = nopen+1

       nunit(8) = 64
       fname(8) = mdlng
       ist(8)=2
!c                                   unknown
       ifm(8)=1
       nopen = nopen+1

       nunit(9) = 65
       fname(9) = spkpt
       ist(9)=2
!c                                   unknown
       ifm(9)=1
       nopen = nopen+1

       nunit(10) = 66
       fname(10) = dop
       ist(10)=2
!c                                   unknown
       ifm(10)=1
       nopen = nopen+1

       nunit(11) = 24
       fname(11) = magnfield
       ist(11)=2
!c                                   unknown
       ifm(11)=1
       nopen = nopen+1

!c===============================================================
       nopen0 = nopen

       do i=nopen0+1,ipunit
        ist(i)=2
        ifm(i)=1
        fname(i)=blnk
       enddo
       iopen = 0
      read(5,*,end=3000)
10     continue
        read(5,'(i3,1x,2a1,1x,a32)',err=20,end=3000)                    &
     &           nu,st,fm,iname
!c
!c             st='n' -- new file, st='o' -- old file, st='u' -- unknown
!c             fm='f' -- formatted, fm='u' -- unformatted
!c
        if(nu.le.0) go to 20

        do i=2,nopen0
         if(nu.eq.nunit(i)) then
          iopen = i
          nu = nunit(iopen)
          if(iname.eq.blnk) iname = fname(iopen)
          go to 15
         end if
        end do
        nopen = nopen+1
        if(nopen.ge.ipunit) then
          coerr=' too many input/output files'
          call fstop(sname//':'//coerr)
        endif
        iopen = nopen
        nunit(iopen) = nu

15      continue

        fname(iopen) = iname

!c  file status
         if(st.eq.'o') then
!c                                   old
          ist(iopen) = 1
         else if(st.eq.'n') then
!c                                   new
          ist(iopen) = 3
         else if(st.eq.'r') then
!c                                   replace
          ist(iopen) = 4
         else
!c                                   unknown
          ist(iopen) = 2
         end if

!c  format
         if(fm.eq.'u') then
!c                                   unformatted
          ifm(iopen) = 2
         else
!c                                   formatted
          ifm(iopen) = 1
         end if

        write(6,1001) iopen,nunit(iopen),fname(iopen)
        go to 10
20     continue

       obroy1 = fname(3)
       obroy2 = fname(4)

       iname = obroy1
       nu = nunit(3)
       cfmt = fmt(ifm(3))
       cstat= stats(ist(3))
       open(unit=nu,file=obroy1,                                        &
     &       form=cfmt,status=cstat,err=2000)
       if(cstat.eq.'replace'.or.cstat.eq.'new    ') then
        close(nu,status='delete')
       else
        close(nu)
       end if

       iname = obroy2
       nu = nunit(4)
       cfmt = fmt(ifm(4))
       cstat= stats(ist(4))
       open(unit=nu,file=obroy2,                                        &
     &       form=cfmt,status=cstat,err=2000)
       if(cstat.eq.'replace'.or.cstat.eq.'new    ') then
        close(nu,status='delete')
       else
        close(nu)
       end if

       iname = fname(5)
       nu = nunit(5)
       cfmt = fmt(ifm(5))
!CDEBUG       ist(5) = 1
       cstat= stats(ist(5))
       open(unit=nu,file=iname,                                         &
     &       form=cfmt,status=cstat,err=2000)

       iname = fname(6)
       nu = nunit(6)
       cfmt = fmt(ifm(6))
       cstat= stats(ist(6))
       open(unit=nu,file=iname,                                         &
     &       form=cfmt,status=cstat,err=2000)

       iname = fname(7)
       nu = nunit(7)
       cfmt = fmt(ifm(7))
       cstat= stats(ist(7))
       open(unit=nu,file=iname,                                         &
     &       form=cfmt,status=cstat,err=2000)
       if(cstat.eq.'replace'.or.cstat.eq.'new    ') then
        close(nu,status='delete')
       else
        close(nu)
       end if


       do i=nopen0+1,nopen
        iname = fname(i)
        cfmt = fmt(ifm(i))
        cstat= stats(ist(i))
        nu = nunit(i)
        if(nu==22.and.iname=='#INPT') cycle
        open(unit=nu,file=iname,                                        &
     &       form=cfmt,status=cstat,err=2000)
       end do
!       write(6,1002)
1002   format(1x,76('='))
!c     ------------------------------------------------------------------

!c  Start of data reads................................................

!c     ------------------------------------------------------------------

!c  Read in subroutine stop level
!      read(5,*,end=3000)
!      read(5,*,end=3000,err=3002) imethod,isparse,istop
      imethod = 0; isparse = 0; istop = 'main'
      call DefMethod(0,imethod,isparse)

      if(imethod.ne.0) invadd = 0

      write(6,*) ''
      write(6 ,'(a)') ' ========================================'       &
     &    //'=================================='
      write(6,'(''  Simulation Parameters'')')
      write(6 ,'(a)') ' ========================================'       &
     &    //'=================================='
      write(6,*) ''

!      write(6,'(/''  Program will stop in subroutine '',
!     >              t40,''= '',a10)') istop

!c  Read in print level (iprint) for output (-100=min. o/p)
      iprint = iprt_level
!      write(6,'(''  Print level '',t40,''='',i5)') iprint

!c  Set maximum angular momentum allowed -- lmax, and
!c  Non-Variational (Variational) MT-zero calc. -- ivar_mtz
      read(5,*,end=3000)
      read(5,*,end=3000) intread,ivar_mtz
      if(lmax.lt.0 .or. lmax.gt.iplmax) then
       write(6,*) ' Wrong LMAX in input file,',                         &
     &            ' ILMAX is used '
       lmax = iplmax
      else
       lmax = intread
      endif
      write(6,'(''  Maximum angular momentum '',                        &
     &              t40,''='',i5)') lmax

!      write(6,*)
      if (ivar_mtz.eq.0)then
        write(6,'(''  MT-zero calc. '',t40,''=  NON-VARIATIONAL'')')
      elseif(ivar_mtz.eq.1)then
        write(6,'(''  MT-zero calc. '',t40,''=  VARIATIONAL ASA'')')
      elseif(ivar_mtz.eq.2)then
        write(6,'(''  MT-zero calc. '',t40,''=  VARIATIONAL VP'')')
      else
        write(6,'(''  MT-zero calc. '',t40,''=  VARIATIONAL VP'')')
        write(6,'(''  Ef calc.      '',t40,''=  Voronoi-Poly'')')
      endif

!c  Set size of kkr matrices -- kkrsz
      kkrsz=(lmax+1)**2
      if(kkrsz.gt.ipkkr) then
         coerr=' wrong kkrsz'
         call fstop(sname//':'//coerr)
      endif

!c  Read in indices that control
!c       Muffin-Tin or Atom.Sph.Appr.::mtasa
!c                          (0 - MT, other - ASA)
!c       spin-polarization           ::nspin
!c                          (1 - non-magn, 2 - collin. magnetic)
!CDLM
!c                           3 - DLM (vdif->0, vmtz_up=vmtz_dn)
!CDLM
!c       relativity                  :: nrel
!c                          (0 - relativ, other - nonrelativ, e.g. 10)
!c     Ryd units: m=1/2  hbar=1 c=2*inverse of fine structure const.
!c                       choose:    speed of light = c*10**nrelv
!c
      read(5,*,end=3000)
      read(5,*,end=3000) mtasa,iadjsph_ovlp,nspin,nrel

      if(mtasa.ne.0) mtasa=1
      write(6,'(''  MT/ASA Approx. [MT=0, ASA=1]'',t40,''='',i5)')mtasa

!CDLM
      if(nspin.eq.3) then
       nspin = 2
       ndlm  = 1
       write(6,'(''             VDIF ==> 0  (to simulate DLM)  '')')
      else
       ndlm  = 0
      end if
!CDLM

      write(6,'(''  Number of spins          '', t40,''='',i5)')nspin
      if (nspin.lt.0 .or. nspin.gt.ipspin) then
         coerr=' wrong input for nspin'
         call fstop(sname//':'//coerr)
      endif

      if(nrel.eq.0) then
         write(6,'(''  CORE    electron treatement'',                   &
     &                 t40,''=  FULLY-RELATIVISTIC'')')
         write(6,'(''  VALENCE electron treatement'',                   &
     &                 t40,''=  SCALAR-RELATIVISTIC'')')
      else
         write(6,'(''  CORE    electron treatement'',                   &
     &                 t40,''=  NON-RELATIVISTIC'')')
         write(6,'(''  VALENCE electron treatement'',                   &
     &                 t40,''=  NON-RELATIVISTIC'')')
         nrel=22      !!note: "c" is made very BIG
      endif

!c-----------------------------------------------------------------
!c  Set a magnitude which determines the maximum length of the
!c    real/reciprocal space translation vector  -- Rrsp,Rksp0
!c   Rsmall - radius of k-space cluster for Gref(K)
!c   Rnncut - radius of real-space cluster for Gref(R)
!c     Fourier transformation for Gref(R) is defined on real-space
!c      cluster (Rij<Rnncut),
!c     but is performed only for k-space (I,J) if RIJ<Rsmall.
!c-----------------------------------------------------------------
      Rrsp   = R_max_rlsp
      Rksp0  = R_max_klsp
      Rsmall = R_small

      Rnncut = Rsmall
25    if(Rnncut.lt.Rsmall) Rnncut=Rsmall
!      write(6,'(''  Rrsp   '',t40,''='',f8.5)') Rrsp
!      write(6,'(''  Rksp0  '',t40,''='',f8.5)') Rksp0
!      write(6,'(''  Rsmall '',t40,''='',f8.5)') Rsmall
!      write(6,'(''  Rnncut '',t40,''='',f8.5)') Rnncut

!c
!c  Set parameters for Ewald method calculations -- enmax,eneta

      realv1 = enmax_ewald
      realv2 = eneta_ewald

      if(realv1.gt.0.d0) then
       enmax = realv1
      else
!c          default value is used
      end if
!      write(6,'(''  Ewald Max Energy '',t40,''='',f8.5)') enmax

      if(realv2.gt.0.d0) then
       if(realv2.gt.2.8d0) then
        write(*,1003)
1003   format(/                                                         &
     &'     ewald parameters:'/                                         &
     &'    "enmax" (in DU) is used to estimate maximum number of',      &
     &    ' reciprocal lattice vectors;',                               &
     &'     Re(any_energy_point) should be less than "enmax" for any',  &
     &    ' iteration'/                                                 &
     &'    "eneta" should be less than 2.8  (then accuracy is better',  &
     &    ' than 7 digits in Fermi energy for fcc)'/                    &
     &'     large ENETA -- large number of K-space vectors'/            &
     &'     small ENETA -- large number of R-space vectors'/            &
     &'          if the most part of real space harmonical polinoms',   &
     &    ' is stored in memory,'/                                      &
     &'          large ENETA reduces the time of computation'//         &
     &'           eta = abs(Energy)/eneta'//)
        call fstop(sname//':: wrong EnEta ???')
       end if
       eneta = realv2
      else
!c          default value is used
      end if
      write(6,'(''  Ewald Eta '',t40,''='',f8.5)') eneta
      if(eneta.le.enmax) then
       write(*,1004)
1004   format(///'  Check ENMAX and ENETA in input file'/               &
     &           '  ETA in ewald method can be too large. Is it OK?'///)
!       pause
      end if

!c  Read in number of k-mesh -- nmesh
      read(5,*,end=3000)
      read(5,*,end=3000) nmesh

      if(ist(9).ne.1) then
       write(6,'(''  Number of K-meshes '',t40,''='',i3)') nmesh
      else
       write(6,*) ''
       write(6,'(''  See number of K-meshes in file: '',a32)')          &
     &       fname(9)
      end if
      if(nmesh.lt.1) then
       nmesh=1
      else if(nmesh.gt.ipmesh) then
       stop ' READIN::   NMESH > IPMESH'
      else
!c  read in "division" for special k-points method -- nq(1:3,*)
        read(5,*,err=3002,end=3000) nq(1,1),nq(2,1),nq(3,1)
       do imesh=2,nmesh
        read(5,*,err=3002,end=3000)                                     &
     &            nq(1,imesh),nq(2,imesh),nq(3,imesh)
       end do
      enlim(1,1) = enlim_Rl
      enlim(2,1) = enlim_Im

       if(ist(9).ne.1) then
        do imesh=1,nmesh
         if(imesh.eq.1) then
          write(6,*)                                                    &
     &'   Re(E) < +Infinity  OR  Abs[Im(E)] > +Infinity '
         else
          write(6,1005) enlim(1,imesh-1),enlim(2,imesh-1)
1005      format(4x,'Re(E) <',f10.5,'  OR  Abs[Im(E)] >',f10.5)
         end if
         write(6,1006) imesh,(nq(i,imesh),i=1,3)
1006     format(4x,'mesh ',i1,' : ',3i4)
        end do
       end if
      endif
!      write(6,1002)

!c     ------------------------------------------------------------------
!c     ============begin of structure specification =====================
!c     ------------------------------------------------------------------

      write(6,*) ''
      write(6 ,'(a)') ' ========================================'       &
     &    //'=================================='
      write(6,'(''  Crystal Structure'')')
      write(6 ,'(a)') ' ========================================'       &
     &    //'=================================='
      write(6,*) ''

!c  Read in crystal type
!c      read(5,*,end=3000)
!c      read(5,*,end=3000) icryst

      icryst = icryst_struc

      if(icryst.le.0) then
!C======================================================================= !==
       if(icryst.eq.0) then
        iname = fname(2)
        nu = nunit(2)
        write(6,*) '  Reading from file "',trim(iname),'"'
        open(nu,file=iname)
       else
        nu = 5
       end if

       call iostrf1(nu,natom,itype,rslatt,basis,alat,boa,coa,namk,      &
     &             scale,ipbase,invadd,nsublat0,isflipped,flippoint)

       if(icryst.eq.0) then
        close(nu)
       else
        icryst = 0
       end if

       do i=1,natom
        basis(1,i) = basis(1,i)/scale
        basis(2,i) = basis(2,i)/scale*boa
        basis(3,i) = basis(3,i)/scale*coa
       end do
!c                    basis(1,i)*ALAT -- X in at.un.
!c                    basis(2,i)*ALAT -- Y in at.un.
!c                    basis(3,i)*ALAT -- Z in at.un.

!C======================================================================= !==
      end if

        write(6,*)
      write(6,'(''  Specification of alloy '',t40,''=   '',a32)') namk
!      write(6,'(''  Crystal stucture '',t40,''='',i5)') icryst

      write(6,'(''  Lattice spacing, ALAT '',t40,''='',f10.5)') alat
!c  conversion from ryd to d.u.
      write(6,'(''  Conversion factor Ry to DU'',t40,''='',f10.5)')     &
     &                                           (alat/(two*pi))**2
      write(6,'(''     B/A and A/B ratio'',t40,''='',2f10.5)')          &
     &                                                  boa,one/boa
      write(6,'(''     C/A and A/C ratio'',t40,''='',2f10.5)')          &
     &                                                  coa,one/coa
!       write(6,1002)
       write(6,*) ''
!c     ------------------------------------------------------------------

!c     ============begin of alloy specification =========================

!c     ------------------------------------------------------------------

!c
      if (icryst.gt.ipcrys) then
         coerr=' wrong input for icryst'
         call fstop(sname//':'//coerr)
      else if(icryst.le.0) then
       if(icryst.eq.0) then
         iname = fname(2)
         nu = nunit(2)
         ist(2) = 1
         open(unit=nu,file=iname,status='old',err=2010)
         close(unit=nu)
       endif
      endif

!C==================================================================
!c  Read in lattice constant, B/A, C/A, number of sublattices and compone !nts
!c      read(5,*,end=3000)
!c      read(5,*,end=3000) alat,boa,coa
!c      write(6,'(''  Lattice spacing, ALAT '',t40,''='',f10.5)') alat
!c  conversion from ryd to d.u.
!c      write(6,'(''  Conversion factor Ry to DU'',t40,''='',f10.5)')
!c     *                                           (alat/(two*pi))**2
!c      write(6,'(''     B/A and A/B ratio'',t40,''='',2f10.5)')
!c     *                                                  boa,one/boa
!c      write(6,'(''     C/A and A/C ratio'',t40,''='',2f10.5)')
!c     *                                                  coa,one/coa
!C==================================================================
!C      read(5,*,end=3000)
!C      read(5,*,end=3000) ncmpn
!C      write(6,'(''  NCOMPONENT '',t40,''='',i5)') ncmpn
!C
!C      if(ncmpn.gt.ipbase.or.ncmpn.lt.1) then
!C       coerr=' Wrong number of components '
!C       call fstop(sname//':'//coerr)
!C      end if
!C
!C      read(5,*,end=3000)
!C      do nsub=1,ncmpn
!c  Read in valences, semi-core and core charges of alloying species
!C       read(5,*,end=3000) numspec(nsub),zvalcm(nsub),
!C     ,                    zsemcm(nsub),zcorcm(nsub)
!C       write(6,'(''        Component '',
!C     >                     t40,''='',i5)') numspec(nsub)
!C       write(6,'(''           Total charge'',t40,''='',f10.5)')
!C     >             zvalcm(nsub)+zsemcm(nsub)+zcorcm(nsub)
!C       write(6,'(''           Valence'',t42,''='',f10.5)')
!C     >                                             zvalcm(nsub)
!C       write(6,'(''           Semi-core charge'',t42,''='',f10.5)')
!C     >                                             zsemcm(nsub)
!C       write(6,'(''           Core charge'',t42,''='',f10.5)')
!C     >                                             zcorcm(nsub)
!C      end do
!C==================================================================

      read(5,*,end=3000)
!c  Read in number of sublattice and CPA control (NO == 0)
      read(5,*,end=3000) nsublat,icpa

!c after exchange, nsublat0 is no. of sublattices w/o flipped spins
      nsub = nsublat
      nsublat = nsublat0
      nsublat0 = nsub

      if( nsublat /= nsublat0 ) then
!        ndlm = 1
        if(nspin /= 2) then
          call fstop("readin :: flipped spins but nspin == 1 ?")
        end if
      end if

      if(icpa.ne.0) then
!CDEBUG        itmax = maxcpa
       itmax = icpa
       if(itmax.eq.1)  itmax = maxcpa
        write(6,'(''  Max. no. of CPA iterations '',t40,                &
     &                                      ''='',i5)') itmax
      else
        itmax = 1
        write(6,'(''  There are NO CPA iterations '',t40)')
      end if


      write(6,'(''  No. Sublattices ='',i5)') nsublat
      write(6,*) ''

      if(nsublat.gt.ipsublat) then
        coerr=' nsublat > ipsublat; check input- or include-file'
        call fstop(sname//':'//coerr)
      end if


!c  Loop over the sublattices for reading in the alloy set up..........

      do nsub=1,nsublat
       komp(nsub) = 0
      end do
      irmt = 0

      read(5,*,end=3000)
      do nsub=1,nsublat

!c  read in:
!c          the sublattice number,
!c          number of sublattice components
!c          muffin-tin radius (relative units; for instance, in ALAT)
!c          (ASA radius will be proportional MT radius)

      write(6,'(''               Sublattice number '',i3)') nsub
      write(6,                                                          &
     &  '(''  ////////////////////////////////////////////////'')')

       if( nsub <= nsublat0 ) then ! SUFF .. for enforced magnetic patt.

         if( isflipped(nsub) ) then
           call fstop("readin :: flipped spin for nsub < nsublat0?")
         end if

         read(5,'(a)',end=3000) inputline
         read(inputline,*,end=3000) isubl,icmpn,rmtd
!c        write(6,*) 'inputline = "', trim(inputline), '"'
         dlm_pos = index(inputline,dlmtag)
         if( dlm_pos == 0 ) dlm_pos = index(inputline,dlmtag2)
         if( dlm_pos == 0 ) dlm_pos = index(inputline,dlmtag3)

        if(isubl.gt.nsublat0.or.isubl.lt.1) then
         coerr=' Wrong sublattice number '
         call fstop(sname//':'//coerr)
        end if

        if( dlm_pos > 0 ) then
         dlm_mix = 0.5d0
         dlmstring = inputline(dlm_pos+3:len(inputline))
!c         write(6,*) 'dlmstring = "', trim(dlmstring), '"'
         read(dlmstring,*,end=5000) dlm_mix
5000     continue
!c         write(6,*) 'dlm_mix = ', dlm_mix
         write(6,'(a,f6.3,a,f6.3,a)') '  Magnetic Disorder @ ',dlm_mix, &
     &      ' UP + ',1.d0-dlm_mix,' DN'
         isDLM(isubl) = .true.
         icmpn = 2*icmpn
!         ndlm = 1

         if(nspin == 1) call fstop(sname//': no DLM for nspin = 1')
        else
         write(6,*) " No 'DLM' tag found"
        end if

        komp(isubl) = icmpn

        if(rmtd.gt.1.0d-05) then
         irmt = irmt+1
         rmt(isubl)=rmtd
!CAB            rws(isubl)=rwsd
        else
         if(icryst.le.0) then
          coerr=' For icryst.LE.0 MT-radius cannot be equal to ZERO'
          call fstop(sname//':'//coerr)
         end if
         rmt(isubl) = zero
        endif
        rws(isubl)=rmt(isubl)

        write(6,'(''  Number of components '',t40,''='',i5)')           &
     &                                           komp(isubl)
         if (komp(isubl).gt.ipcomp) then
            coerr=' komp greater than ipcomp'
            call fstop(sname//':'//coerr)
         endif
        write(6,'(''  MT-radius/Alat (read from file)'',t40,            &
     &           ''='',f10.7)') rmt(isubl)

        sum=0.0d0
        do i=1,komp(isubl)
!c  Read identificator, concentration and ID-name of each component
!c                                       on nsub'th sublattice .....
         if( .not. isDLM(isubl) .or. i <= komp(isubl)/2 ) then
          read(5,*,end=3000) ispec,izv,atcon(i,isubl),r_fract(i,isubl)
          if( isDLM(isubl) ) then
            atcon(i,isubl) = atcon(i,isubl)*dlm_mix
          end if
         else
          j = 1+mod(i-1,komp(isubl)/2)
          izv   = zvalss(j,isubl)
          ispec = zcorss(j,isubl) + izv
          atcon(i,isubl) = atcon(j,isubl)*(1.d0-dlm_mix)/dlm_mix
          r_fract(i,isubl) = r_fract(j,isubl)
         end if

         vfract(i,isubl) = (r_fract(i,isubl)/r_fract(1,isubl))**3.0d0
         if(i.ne.1)then
           sum = sum +atcon(i,isubl)*vfract(i,isubl)
         endif

!C==================================================================
!C       do icmpn=1,ncmpn
!C        if(ispec.eq.numspec(icmpn)) then
!C         go to 100
!C        end if
!C       end do
!C       write(*,*) ' READIN:: ISPEC=',ispec
!C       write(*,*) ' You give wrong number of component',
!C     *            ' check your input file'
!C       write(*,*)
!C       coerr=' not appropriate component ID'
!C       call fstop(sname//':'//coerr)
!C100     continue
!C       zvalss(i,isubl) = zvalcm(icmpn)
!C       zsemss(i,isubl) = zsemcm(icmpn)
!C       zcorss(i,isubl) = zcorcm(icmpn)
!C       zcortot(i,isubl) = zcorcm(icmpn) +  zsemcm(icmpn)
!C==================================================================

         zvalss(i,isubl) = izv ! izval(ispec)
         zsemss(i,isubl) = 0.0d0
         zcorss(i,isubl) = ispec - izv ! izval(ispec)
         zcortot(i,isubl) = zsemss (i,isubl) + zcorss(i,isubl)
         ztotss(i,isubl) = zvalss (i,isubl) + zcortot(i,isubl)
         name(i,isubl) = symbol(ispec)
        end do
          vfract(1,isubl) = 1.0d0/(atcon(1,isubl) + sum)
         do i=2,komp(isubl)
          vfract(i,isubl) = vfract(1,isubl)*vfract(i,isubl)
         enddo

!c  check the concentrations sum
        tconc= zero
        do i=1,komp(isubl)
           tconc = tconc + atcon(i,isubl)
        enddo
        if(abs(tconc-one).ge.1.d-5) then
          write(6,'('' in sublattice tconc '',i2,f16.7)')               &
     &                                          isubl,tconc
          coerr=' total concentration not equal to 1'
          call fstop(sname//':'//coerr)
        else
          do i=1,komp(isubl)
            atcon(i,isubl) = atcon(i,isubl)/tconc
          enddo
        endif

!c  check whether volume is conserved from vfract
!CALAM
        vtot=zero
        do i=1,komp(isubl)
           vtot = vtot + atcon(i,isubl)*vfract(i,isubl)
        enddo

        if(abs(vtot-one).ge.1.d-5) then
          write(6,'('' in sublattice vtot '',i2,f16.7)')                &
     &                                          isubl,vtot
          coerr=' volume not conserved : sum_{i} a(i)*vfrac(i).ne.1'
          call fstop(sname//':'//coerr)
        endif
!CALAM

       else
         isubl = nsub
         nsub0 = flippoint(nsub)

        write(6,'(''  This is a copy of sublattice '',i3)') nsub0
        write(6,'(''  Number of components '',t40,''='',i5)')           &
     &                                           komp(nsub0)

        write(6,'(''  MT-radius/Alat (read from file)'',t40,            &
     &           ''='',f10.7)') rmt(nsub0)

         isDLM(isubl) = isDLM(nsub0)
         komp(isubl)  = komp(nsub0)
         rmt(isubl) = rmt(nsub0)
         rws(isubl) = rws(nsub0)
         atcon(:,isubl) = atcon(:,nsub0)
         r_fract(:,isubl) = r_fract(:,nsub0)
         vfract(:,isubl) = vfract(:,nsub0)
         zvalss(:,isubl) = zvalss(:,nsub0)
         zsemss(:,isubl) = zsemss(:,nsub0)
         zcorss(:,isubl) = zcorss(:,nsub0)
         zcortot(:,isubl) = zcortot(:,nsub0)
         ztotss(:,isubl) = ztotss(:,nsub0)
         name(:,isubl) = name(:,nsub0)
       end if


!CAB??????????????
         ivpoint(isubl) = 0
         do i=1,komp(isubl)
          ikpoint(i,isubl) = 0
         enddo
!CAB??????????????

!c  Write out the specifications for this sub-lattice...............
       do i=1,komp(isubl)
            write(6,'(''    Component number'',                         &
     &                          t40,''='',i5)') i
            write(6,'(''      Identifier'',t40,''= '',a10)')            &
     &                                                  name(i,isubl)
            write(6,'(''      Concentration'',t40,''='',f10.5)')        &
     &                                                  atcon(i,isubl)
            write(6,'(''      Vol. fraction'',t40,''='',f10.5)')        &
     &                                                 vfract(i,isubl)
            write(6,'(''      Valence'',t40,''='',f10.5)')              &
     &                                                  zvalss(i,isubl)
            write(6,'(''      Semi-core charge'',t40,''='',f10.5)')     &
     &                                                  zsemss(i,isubl)
            write(6,'(''      Core charge'',t40,''='',f10.5)')          &
     &                                                  zcorss(i,isubl)
            write(6,'(''      Core + Semi-core'',t40,''='',f10.5)')     &
     &                                   zsemss(i,isubl)+zcorss(i,isubl)
!CAB            write(6,'(''           Component pointer'',t40,''='',i5)' !)
!CAB     >                                               ikpoint(i,isubl)
       enddo
!       write(6,'(''  End of data reading for sublattice '',
!    >   t40,''='',i5)') isubl
      enddo

      if(irmt.ne.0.and.irmt.ne.nsublat0) then
       write(*,*) ' Problem with MT-radius; check input file'
       do nsub=1,nsublat
        write(*,*) ' nsub=',nsub,' rmt(nsub)=',rmt(nsub)
       end do
       write(*,*)
       coerr = ' wrong MT-radius'
       call fstop(sname//':'//coerr)
      end if
      do nsub=1,nsublat
       if(komp(nsub).eq.0) then
         write(*,*)
         write(*,*) ' Sublattice ',nsub,' is not described '
         write(*,*)
         coerr=' wrong description of the sublattice'
         call fstop(sname//':'//coerr)
       end if
      end do

!c     end of data read in loop over sublattices.......................

!c     ============end of alloy specification ===========================

!c     ------------------------------------------------------------------

!c     read in energy grid information..................................
!c     igrid   : specifies energy contour  1=slower rectangular contour.
!c                                         2=faster semicirular contour.
!c
!c     for igrid =1............................................
!c           ebot    : bottom of contour on      real axis.....
!c           etop    : top    of contour on      real axis.....
!c           eitop   : top    of contour on imaginary axis.....
!c           eibot   : bottom of contour on imaginary axis.....
!c           npts    : number of energy points per 0.1 ry......
!c           npar    : number of points parallel to real-axis..
!c     ........................................................
!c     for igrid =2............................................
!c           ebot    : bottom of contour on      real axis.....
!c           etop    : top    of contour on      real axis.....
!c           npts    : number of energy points along semicircle
!c                   : npts/1000 -- number of energy points
!c                   : along temperature semicircle
!c           eibot   : imaginary part for the point near the Fermi level
!c                     (if negative -- the absolute value is used
!c                      for temperature integration)
!c     Read in parameters of SCF cycle:
!c           nscf  -- number of SCF iterations
!c           alpha -- mixing coefficient for charge
!c           beta  -- mixing coefficient for spin

      read(5,*,end=3000)
      read(5, *,end=3000 ) nscf,npts,alpha,beta,ibot_cont,ebot,eitop

      ibot_cont = 1-ibot_cont
      igrid = igrid2
      etop = etop_cont
      eibot = eibot_cont

      npar = nparallel
!      esmear = en_smear

      write(6,*) ''
      write(6 ,'(a)') ' ========================================'       &
     &    //'=================================='
      write(6,'(''  Energy Contour Description'')')
      write(6 ,'(a)') ' ========================================'       &
     &    //'=================================='
      write(6,*) ''


      if(igrid.gt.3.or.igrid.le.0) then
       write(*,*) ' IGRID=',igrid
       call fstop(sname//                                               &
     &                      ': Wrong IGRID?')
      end if

      if(npts.gt.100.and.npts.lt.1000) then
       write(*,*)
       write(*,*) ' ==>> '
       write(*,*) ' ==>>   NPTS=',npts
       write(*,*) ' ==>> '
       igrid = 1
       write(*,*) '              IGRID=1 '
       write(*,*)
      end if

       if(ibot_cont.eq.0)then
        write(6,'('' Bottom of contour: real axis '',                   &
     &                  t40,''='',f10.5)') ebot
        ibot_cont=0
       else
        write(6,'('' Bottom of contour will be estimated below '',      &
     &                  t48)')
!        ibot_cont=1
       endif

       write(6,'(''  Top    of contour: real axis '',                   &
     &                 t40,''='',f10.5)') etop

      if(igrid.eq.1) then
       write(6,'(''  Bottom of contour: imag axis '',                   &
     &                 t40,''='',f10.5)') eibot
       write(6,'(''  Top    of contour: imag axis '',                   &
     &                 t40,''='',f10.5)') eitop
       write(6,'(''  Number of energy points per 0.1Ry '',              &
     &                 t40,''='',i5)') npts
       write(6,'(''  Number of pts. pll real axis at Ef '',             &
     &                 t40,''='',i5)') npar
      else
       write(6,'(''  Number of energy points on semicircle'',           &
     &                 t40,''='',2i5)') mod(npts,1000),npts/1000
       write(6,'(''  Imaginary part for additional points: '',          &
     &                 t40,''='',f10.5)') abs(eibot)
       if(eibot.lt.0.d0) then
        if(npts/1000.eq.0) then
         eitop = zero
         eibot = abs(eibot)
        else
         write(6,'(''  Temperature Integration (Kelvin) '',             &
     &                 t40,''='',f11.1)') eitop*ry2kelvin
        end if
         write(6,'(/)')
       else
       end if
      endif

!c  Read in parameters of SCF cycle:
!c   nscf  -- number of SCF iterations
!c   alpha -- mixing coefficient for charge
!c   beta  -- mixing coefficient for spin
!c   ichg  -- density or potential mixing
!c   imixrho -- simple or Broyden density mixing
!c              for potential -- simple mixing only
!c
      write(6,'(''  SCF mixing parameters :: '')')
      write(6,'(''     Maximum number of SCF iterations '',             &
     &                 t40,''='',i5)') nscf
      write(6,'(''     Mixing for total density & moment'',             &
     &                 t40,''='',2f10.5)') alpha,beta


      ! read in density of states info
      read(5,*,end=3000)
      read(5,*,end=3000) idosplot,ibsfplot,dosbsf_kintsch,              &
     &                   dosbsf_nepts,dosbsf_emin,dosbsf_emax,          &
     &                   esmear,bsf_nkwaypts,bsf_ksamplerate

      if( dosbsf_kintsch < 0 .or. 3 < dosbsf_kintsch ) then
        call fstop("unrecognized k-space integration scheme")
      end if

      if( idosplot.ne.0 )then
        write(6,'(''  Print the DOS after convergence '',t40)')
      else
        write(6,'(''  DOS will not be written in a separate file ''     &
     &                  ,t40)')
      endif

      if( bsf_nkwaypts > ipbsfkpts ) then
        write(6,*) 'max kpts allowed = ', ipbsfkpts
        call fstop("max. no. of k-space waypts exceeded")
      end if

      if( ibsfplot.ne.0 ) then
        bsf_file = 'akeBAND.dat'
        open(unit=90,file=bsf_file,status='replace',iostat=iec)
        if(iec.ne.0) call fstop('failed to open AkE file')
      end if
      bsf_fileno = 90
      read(5,*,end=3000)
      do i = 1, bsf_nkwaypts
        read(5,*,end=3000) bsf_kwaypts(1:3,i), bsf_kptlabel(i)
      end do


      ! is the system disordered?
      isdis = .false.
      do i = 1, nsublat
        if(komp(i)>1) isdis = .true.
      end do
!c
!c     read in end of data
!c
      read(5,'(a80)',end=9999) text
      if(text=='end of file!') then
        ifermiplot = 0
        goto 9999
      end if

      read(5,*,end=9999) ifermiplot,fermi_en,fermi_nkpts,               &
     &                   fermi_gridsampling,fermi_raysampling
      read(5,*,end=3000) fermi_korig(1:3),fermi_orilabel

      if(ifermiplot/=0.and.fermi_nkpts<2) then
        call fstop("fermi surface plot requires at least 2 kpts")
      end if

      read(5,*,end=3000)
      do i = 1, fermi_nkpts
        read(5,*,end=3000) fermi_kpts(1:3,i), fermi_kptlabel(i)
      end do
      if(ifermiplot.ne.0) then
        if(nspin == 1) then
         if(ifermiplot.eq.1) then
          open(unit=91,file='akeBZarea.dat',status='replace',iostat=iec)
         else if(isdis) then
          open(unit=91,file='akeBZrays.dat',status='replace',iostat=iec)
         else
          open(unit=91,file='fermi.dat',status='replace',iostat=iec)
         end if
         if(iec.ne.0) call fstop('failed to open Fermi file')
        else
          if(ifermiplot.eq.1) then
            open(unit=91,file='akeBZarea_up.dat',                       &
     &                   status='replace',iostat=iec)
            if(iec.ne.0) call fstop('failed to open Fermi file')
            open(unit=92,file='akeBZarea_dn.dat',                       &
     &                   status='replace',iostat=iec)
            if(iec.ne.0) call fstop('failed to open Fermi file')
          else if(isdis) then
            open(unit=91,file='akeBZrays_up.dat',                       &
     &                   status='replace',iostat=iec)
            if(iec.ne.0) call fstop('failed to open Fermi file')
            open(unit=92,file='akeBZrays_dn.dat',                       &
     &                   status='replace',iostat=iec)
            if(iec.ne.0) call fstop('failed to open Fermi file')
          else
            open(unit=91,file='fermi_up.dat',                           &
     &                   status='replace',iostat=iec)
            if(iec.ne.0) call fstop('failed to open Fermi file')
            open(unit=92,file='fermi_dn.dat',                           &
     &                   status='replace',iostat=iec)
            if(iec.ne.0) call fstop('failed to open Fermi file')
          end if
        end if
      end if
      fermi_fileno(1) = 91
      fermi_fileno(2) = 92

      read(5,'(a80)',end=9999) text
      if(text/='end of file!') then
        call fstop("did not see 'end of file!' marker")
      end if

 9999 continue
      write(6,*) text
!c
      write(6,1002)

!c      if(icryst.le.0) then
!c
!c     ============begin of structure specification ====================

!c       if(icryst.eq.0) then
!c       iname = fname(2)
!c       nu = nunit(2)
!c       write(6,*)
!c       write(6,*) '  File <<',iname,'>>'
!c       open(nu,file=iname)
!c       else
!c       nu = 5
!c       end if
!c
!c       call iostrf(nu,natom,itype,rslatt,basis,ba,ca,scale,
!c     *             ipbase,invadd)
!c
!c       if(icryst.eq.0) then
!c       close(nu)
!c       else
!c       icryst = 0
!c       end if
!c
!c       write(6,*) '     NATOM=',natom
!c
!c       if(abs(ba-boa).gt.1.d-4.or.abs(ca-coa).gt.1.d-4)
!c     * write(6,*) ' ==> B/A=',ba,' C/A=',ca,
!c    ,            ' <== not the same as in ini-file'
!c
!c       do i=1,natom
!c       if(itype(i).gt.nsublat.or.itype(i).lt.1) then
!c        write(6,*)
!c        write(6,*) '  NSUBLAT=',nsublat,' TYPE(',i,')=',itype(i)
!c        write(6,*) (basis(j,i),j=1,3)
!c        write(6,*) '   Wrong atom TYPE '
!c        write(6,*)
!c        coerr=' wrong input for itype(i) in str-file'
!c        call fstop(sname//':'//coerr)
!c       end if
!c       end do
!c
!c       do i=1,natom
!c       basis(1,i) = basis(1,i)/scale
!c       basis(2,i) = basis(2,i)/scale*(boa/ba)
!c       basis(3,i) = basis(3,i)/scale*(coa/ca)
!c       end do
!c                    basis(1,i)*ALAT -- X in at.un.
!c                    basis(2,i)*ALAT -- Y in at.un.
!c                    basis(3,i)*ALAT -- Z in at.un.
!c
!c       do i=1,3
!c       rslatt(1,i) = rslatt(1,i)/scale
!c       rslatt(2,i) = rslatt(2,i)/(scale*ba)
!c       rslatt(3,i) = rslatt(3,i)/(scale*ca)
!c       end do
!c                    rslatt(*,i)*ALAT --  in at.un.
!c
!c       do i=1,natom
!c       rmt0(i) = rmt(itype(i))
!c       rws0(i) = rws(itype(i))
!c       end do
!c
!c      end if

       do i=1,natom
        rmt0(i) = rmt(itype(i))
        rws0(i) = rws(itype(i))
       end do

       write(6,*) '     NATOM=',natom

      call givebas(icryst,natom,boa,coa,                                &
     &             rslatt,basis,itype,volume,rmt0,rws0)

      do i=1,natom
       if(itype(i).gt.nsublat.or.itype(i).lt.1) then
         write(6,*)
         write(6,*) '  NSUBLAT=',nsublat,' TYPE(',i,')=',itype(i)
         write(6,*) (basis(j,i),j=1,3)
         write(6,*)
         coerr=' wrong itype(i) after givebas-routine;'//               &
     &         ' check input file'
         call fstop(sname//':'//coerr)
       end if
      end do

      do nsub=1,nsublat
        nsubi = 0
        do i=1,natom
         if(nsub.eq.itype(i)) nsubi = nsubi+1
        end do
        if(nsubi.eq.0) then
         write(6,*)
         do i=1,natom
          write(6,*) i,itype(i)
         end do
         write(6,*) ' There are no atoms for sublattice',nsub
         write(6,*)
         coerr=' wrong sublattice description?'
         call fstop(sname//':'//coerr)
        end if
        numbsub(nsub) = nsubi
        write(6,'(''  In sublattice '',i5,'' there are'',t40,1x,i5,     &
     &            '' site(s)'')') nsub,numbsub(nsub)
      end do

      do 200 nsub=1,nsublat
        do i=1,natom
         if(itype(i).eq.nsub) then
           do ic=1,komp(nsub)
!CALAM
            rws_comp(ic,nsub) = (vfract(ic,nsub)*rws0(i)**3.d0)**       &
     &                          (1.0d0/3.0d0)*alat
!CALAM
           enddo
          go to 200
         end if
        end do
200   continue

      if(mtasa.eq.0) then
       write(6,*)
       do 300 nsub=1,nsublat
        if(rmt(nsub).le.1.d-5) then
         do i=1,natom
          if(itype(i).eq.nsub) then
            do ic=1,komp(nsub)
!CALAM
             rmt_comp(ic,nsub) = rmt0(i)*alat
            enddo

           go to 300
          end if
         end do
        else
            do ic=1,komp(nsub)
!CALAM
             rmt_comp(ic,nsub) = rmt(nsub)*alat
            enddo
        end if
300    continue
      else
       do nsub=1,nsublat
        do ic=1,komp(nsub)
!CALAM
          rmt_comp(ic,nsub) = rws_comp(ic,nsub)
        enddo
       end do
      end if

      write(6,*)
!c     ============end of structure specification ======================

      write(6,1002)

      return
2000   continue
       write(*,*) sname//' NUNIT=',nu,' FORM=',fmt(ifm(i)),             &
     &            ' STATUS=',stats(ist(i))
       write(*,*) '        FILE=<<',iname,'>>'
       write(*,*) '  OPEN ERROR: check file or status '
       stop sname
2010   continue
       write(*,*) sname//' NUNIT=',nu,' FILE=<<',iname,'>>'
       write(*,*) '  OPEN ERROR: check file'
       stop sname
3000   stop ' END OF INPUT-FILE?'
       stop ' END OF STR-FILE?'
3002   stop ' ERROR IN INPUT-FILE?'
       end


       subroutine iostrf1(io,natom,itype,rslatt,basis,alat,boa,coa,namk,&
     &                   scale,ndim,invadd,nsublat0,isflipped,flippoint)
!c
!c   read/write structure data
!c
!c    rslatt(*,i) -- i-th translation vector, i=1..3
!c    basis(*,i) -- position of i-th site in the cell, i=1..natom
!c    alat = Lattice constat
!c    boa = B over A
!c    coa = C over A
!c    scale -- to change units of rslatt, basis
!c    invadd:  =0 -- not to add inversion to symmetry operations in K-spa !ce
!c             =1 --  to add inversion ...
!c
       implicit none
      include 'mkkrcpa.h'
       integer io,natom,ndim,nch,i,j,invadd
       integer itype(ndim)
       real*8  rslatt(3,3),basis(3,ndim),alat,boa,coa,scale

       real*8  rtmp5,volume
       character  namk*32, text*80

       logical :: isflipped(ipsublat)
       integer :: flippoint(ipsublat)
       integer :: cnt,nsublat0

       if(io.eq.0) return

       nch = abs(io)
       if(io.gt.0) then
!        write(6,*) '  Reading in structure data ...'
        read(nch,'(a32)',end=101) namk
        read(nch,*,end=101) alat,boa,coa
        read(nch,*,end=101) natom
!c  Read lattice vectors .........................................
        if(natom.gt.ndim.or.natom.lt.1) then
         write(*,*) ' NATOM=',natom,' NDIM=',ndim
         stop ' IOSTR: wrong number of atoms'
        end if
        do i=1,3
         read(nch,*,err=100,end=101) rslatt(1,i),rslatt(2,i),rslatt(3,i)
        end do

        volume = ( rslatt(2,1)*rslatt(3,2) - rslatt(3,1)*rslatt(2,2) )  &
     &         * rslatt(1,3) +                                          &
     &           ( rslatt(3,1)*rslatt(1,2) - rslatt(1,1)*rslatt(3,2) )  &
     &         * rslatt(2,3) +                                          &
     &           ( rslatt(1,1)*rslatt(2,2) - rslatt(2,1)*rslatt(1,2) )  &
     &         * rslatt(3,3)

        if(volume.lt.0.d0) then
         write(6,*)
         write(6,*) '  YOU HAVE TO USE RIGHT-HAND BASIS '
         write(6,*) '    rslatt(*,3) <--> rslatt(*,2) '
         write(6,*)
         do i=1,3
          rtmp5 = rslatt(i,2)
          rslatt(i,2) = rslatt(i,3)
          rslatt(i,3) = rtmp5
         end do
        end if

        nsublat0 = 0
        read(nch,*,err=100,end=101)
!c  Read coordinates of atoms (basis) and decoration (itype)......
        do i=1,natom
         read(nch,*,err=100,end=101) itype(i),(basis(j,i),j=1,3)
         nsublat0 = max(nsublat0,itype(i))
        end do
        read(nch,'(a80)') text
        if(trim(adjustl(text))/='end of file!') then
          call fstop("did not see 'end of file!' marker in *.xyz file")
        end if

!cSUFF Add additional sublattices for flipped potentials
        isflipped = .false.
        cnt = 0; flippoint = 0
        do i=1,natom
        if( itype(i) < 0 ) then
          cnt = cnt + 1
          flippoint(nsublat0 + cnt) = -itype(i)
          itype(i) = nsublat0 + cnt
          isflipped(itype(i)) = .true.
        end if; end do
        nsublat0 = nsublat0 + cnt

!c       read(nch,*,err=1999,end=1999) scale,invadd

        scale = 1.d0   ! Don't change the units of rslatt, basis
!c        invadd = 0.d0  ! Not to add inversion to symmetry operations in K-space
        invadd = 1

1999    if(scale.le.0.d0) scale=1.d0
        if(invadd.ne.0) invadd=1

       elseif(io.lt.0) then
        write(6,*) '  Writing down structure data ...'
        write(nch,'(a32)',err=99) namk
        write(nch,*,err=99) alat,boa,coa
        write(nch,*,err=99) natom,'  STRUCTURE DATA'
        do i=1,3
         write(nch,*,err=99) rslatt(1,i),rslatt(2,i),rslatt(3,i)
        end do
        write(nch,*,err=99) ' --------------------- '
!c Write coordinates of atoms (basis) and decoration (itype)......
        do i=1,natom
         if(.not. isflipped(itype(i))) then
          write(nch,1,err=99)  itype(i),(basis(j,i),j=1,3)
         else
          write(nch,1,err=99) -flippoint(itype(i)), (basis(j,i),j=1,3)
         end if
1        format(1x,i5,3d20.10)
        end do
        write(nch,*,err=99) scale,invadd
       end if

       return
99    write(*,*)  ' WRITE '
100    write(*,*) ' ERROR '
101    write(*,*) ' IOSTR:: Channel=',nch
       write(*,*) ' I=',i
       write(*,*) ' EOF/ERROR '
       call fstop(' IOSTR: eof/error')
       end


      subroutine givebas(icryst,nbasis,boa,coa,                         &
     &                   bas,bases,itype,volume,rmt,rws)
      implicit real*8 (a-h,o-z)
      dimension bas(3,3),bases(3,*),itype(*),rmt(*),rws(*)
      character*10 sname
      parameter (sname='givebas')
      dimension abc(3)

      dimension basfcc(3,3),basbcc(3,3),basscc(3,3)
!c==================================================================
      data  basfcc/0.5d0, 0.5d0, 0.0d0,                                 &
     &             0.0d0, 0.5d0, 0.5d0,                                 &
     &             0.5d0, 0.0d0, 0.5d0/

      data  basbcc/0.5d0, 0.5d0,-0.5d0,                                 &
     &            -0.5d0, 0.5d0, 0.5d0,                                 &
     &             0.5d0,-0.5d0 ,0.5d0/

      data  basscc/  1.0,  0.0,  0.0,                                   &
     &               0.0,  1.0,  0.0,                                   &
     &               0.0,  0.0,  1.0/
!c==================================================================

      dimension basrhm(3,3),basbct(3,3)
!c==================================================================
      data basrhm/                                                      &
     & 0.000000000000000000E+00,-0.327630600784613202,                  &
     & 0.944805900399396159,                                            &
     & 0.283736423336632848,0.163815300392306601,                       &
     & 0.944805900399396159,                                            &
     &-0.283736423336632848,0.163815300392306601,                       &
     & 0.944805900399396159/

      data  basbct/ -0.5,  0.5,  1.0,                                   &
     &               0.5, -0.5,  1.0,                                   &
     &               0.5,  0.5, -1.0/

      abc(1) = 1.d0
      abc(2) = boa
      abc(3) = coa
      alat = 1.d0
      coef = 1.d0
      lattyp = 0
      pi = 4*atan(1.d0)

!c    =================================================================== !===
!c     FCC lattice setup
!c    =================================================================== !===
      if(icryst.eq.1) then
         nbasis=1
         lattyp=1

         bases(1,1)=0.d0
         bases(2,1)=0.d0
         bases(3,1)=0.d0
         itype(1) = 1

         rmtd=sqrt(2.d0)/4.d0
         rmt(1)=rmtd*alat

         nrot = 48

!c    =================================================================== !===
!c     BCC lattice setup
!c    =================================================================== !===
      else if(icryst.eq.2) then
         nbasis=1
         lattyp=2

         bases(1,1)=0.d0
         bases(2,1)=0.d0
         bases(3,1)=0.d0
         itype(1) = 1

         rmtd=sqrt(3.d0)/4.d0
         rmt(1)=rmtd*alat

         nrot = 48

!c    =================================================================== !==
!c     SC lattice setup
!c    =================================================================== !===
      else if(icryst.eq.3) then
         nbasis=1
         lattyp=3

         bases(1,1)=0.d0
         bases(2,1)=0.d0
         bases(3,1)=0.d0
         itype(1) = 1

         rmtd=0.5d0
         rmt(1)=rmtd*alat

         nrot = 48

!c    =================================================================== !===
!c     B2 (CsCl) lattice setup
!c    =================================================================== !===
      else if(icryst.eq.4) then
         nbasis=2
         lattyp=3

         bases(1,1)=0.d0
         bases(2,1)=0.d0
         bases(3,1)=0.d0
         bases(1,2)=0.5d0
         bases(2,2)=0.5d0
         bases(3,2)=0.5d0
         itype(1) = 1
         itype(2) = 2

         rmtd=sqrt(3.d0)/4.d0
         rmt(1)=rmtd*alat
         rmt(2)=rmt(1)

         nrot = 48

!c    =================================================================== !===
!c     L12 (Cu3Au) lattice setup
!c    =================================================================== !===
      else if(icryst.eq.5) then
         nbasis=4
         lattyp=3
         alat = 1.d0

         bases(1,1)=0.d0
         bases(2,1)=0.d0
         bases(3,1)=0.d0

         bases(1,2)=0.5d0
         bases(2,2)=0.5d0
         bases(3,2)=0.d0

         bases(1,3)=0.5d0
         bases(2,3)=0.d0
         bases(3,3)=0.5d0

         bases(1,4)=0.d0
         bases(2,4)=0.5d0
         bases(3,4)=0.5d0

         itype(1) = 1
         itype(2) = 2
         itype(3) = 2
         itype(4) = 2

         rmtd=sqrt(2.d0)/4.d0
         rmt(1)=rmtd*alat
         rmt(2)=rmt(1)
         rmt(3)=rmt(1)
         rmt(4)=rmt(1)

         nrot = 48

!c    =================================================================== !===
!c     Perovskite (BaTiO3) lattice setup
!c    =================================================================== !===
      else if(icryst.eq.6) then
         nbasis=5
         lattyp=3

         bases(1,1)=0.d0
         bases(2,1)=0.d0
         bases(3,1)=0.d0

         bases(1,2)=0.5d0
         bases(2,2)=0.5d0
         bases(3,2)=0.d0

         bases(1,3)=0.5d0
         bases(2,3)=0.d0
         bases(3,3)=0.5d0

         bases(1,4)=0.d0
         bases(2,4)=0.5d0
         bases(3,4)=0.5d0

         bases(1,5)=0.5d0
         bases(2,5)=0.5d0
         bases(3,5)=0.5d0

         itype(1) = 1
         itype(2) = 2
         itype(3) = 2
         itype(4) = 2
         itype(5) = 3

         rmtd1=(sqrt(3.d0)-1.d0+sqrt(2.d0))/4.d0
         rmtd2=sqrt(2.d0)/2.d0-rmtd1
         rmtd3=0.5d0-rmtd2

         rmt(1)=rmtd1*alat
         rmt(2)=rmtd2*alat
         rmt(3)=rmt(2)
         rmt(4)=rmt(2)
         rmt(5)=rmtd3*alat

         nrot = 48

!c    =================================================================== !===
!c     B1 (NaCl) lattice setup
!c    =================================================================== !===
      else if(icryst.eq.7) then
         nbasis=2
         lattyp=1
         alat = 1.d0

         bases(1,1)=0.d0
         bases(2,1)=0.d0
         bases(3,1)=0.d0
         bases(1,2)=0.5d0
         bases(2,2)=0.5d0
         bases(3,2)=0.5d0

         itype(1) = 1
         itype(2) = 2

         rmtd=0.5d0
         rmt(1)=rmtd*alat
         rmt(2)=rmt(1)

         nrot = 48

!c    =================================================================== !===
!c     DO3 (Fe3Al) lattice setup
!c    =================================================================== !===
      else if(icryst.eq.8) then
         nbasis=4
         lattyp=1
         coef = 2.d0

         bases(1,1)=0.d0
         bases(2,1)=0.d0
         bases(3,1)=0.d0
         bases(1,2)=0.5d0
         bases(2,2)=0.d0
         bases(3,2)=0.d0
         bases(1,3)=0.25d0
         bases(2,3)=0.25d0
         bases(3,3)=0.25d0
         bases(1,4)=-0.25d0
         bases(2,4)=-0.25d0
         bases(3,4)=-0.25d0
         itype(1) = 1
         itype(2) = 2
         itype(3) = 2
         itype(4) = 2

         rmtd = 0.25d0
         rmt(1)=rmtd*alat
         rmt(2)=rmt(1)
         rmt(3)=rmt(1)
         rmt(4)=rmt(1)

         nrot = 48

!c    =================================================================== !===
!c
!c     R3M Trigonal lattice setup
!c
!c    =================================================================== !===
      else if( icryst .eq. 9 ) then
        nbasis=4
        lattyp=4

        u = 0.26d0
        alpha =( ( 32.d0 + 58.d0/60.d0 )/180.d0)*acos( -1.0d0 )
        cosalpha = cos( alpha )
        sqrt2 = sqrt( 2.0d0 )
        sqrt3 = sqrt( 3.0d0 )
        a = ( sqrt2/sqrt3 )*sqrt( 1.d0 - cosalpha )
        c = sqrt( 1.d0 + 2.d0*cosalpha )/sqrt3

!c Lattice vectors of unit cell

        bas(1,1)= 0.d0
        bas(2,1)= -a
        bas(3,1)=  c
        bas(1,2)=  a*sqrt3/2.0d0
        bas(2,2)=  a/2.0d0
        bas(3,2)=  c
        bas(1,3)=-bas(1,2)
        bas(2,3)= bas(2,2)
        bas(3,3)= c
!c    =================================================================== !===
!c     Scaled basis vectors
!c    =================================================================== !===
        bases(1,4) = 0.d0
        bases(2,4) = 0.d0
        bases(3,4) = 0.d0
        bases(1,5) = 0.5d0
        bases(2,5) = 0.5d0
        bases(3,5) = 0.5d0
        bases(1,6) = u
        bases(2,6) = u
        bases(3,6) = u
        bases(1,7) = ( 1.d0 - u )
        bases(2,7) = ( 1.d0 - u )
        bases(3,7) = ( 1.d0 - u )
!c    =================================================================== !===
!c     Construct cartesian real space basis vectors
!c    =================================================================== !===
        rmt(5) = 0.d0
        rmt(6) = 0.d0
        rmt(7) = 0.d0
        do j = 1,3
          do i = 1,3
            rmt(j+4) = rmt(j+4) + bas(j,i)*bases(i,4)
          enddo
        enddo
        bases(1,4) = rmt(5)
        bases(2,4) = rmt(6)
        bases(3,4) = rmt(7)
!c    =================================================================== !===
        rmt(5) = 0.d0
        rmt(6) = 0.d0
        rmt(7) = 0.d0
        do j = 1,3
          do i = 1,3
            rmt(j+4) = rmt(j+4) + bas(j,i)*bases(i,5)
          enddo
        enddo
        bases(1,5) = rmt(5)
        bases(2,5) = rmt(6)
        bases(3,5) = rmt(7)
!c    =================================================================== !===
        rmt(5) = 0.d0
        rmt(6) = 0.d0
        rmt(7) = 0.d0
        do j = 1,3
          do i = 1,3
            rmt(j+4) = rmt(j+4) + bas(j,i)*bases(i,6)
          enddo
        enddo
        bases(1,6) = rmt(5)
        bases(2,6) = rmt(6)
        bases(3,6) = rmt(7)
!c    =================================================================== !===
        rmt(5) = 0.d0
        rmt(6) = 0.d0
        rmt(7) = 0.d0
        do j = 1,3
          do i = 1,3
            rmt(j+4) = rmt(j+4) + bas(j,i)*bases(i,7)
          enddo
        enddo
        bases(1,7) = rmt(5)
        bases(2,7) = rmt(6)
        bases(3,7) = rmt(7)

      omega = ( bas(2,1)*bas(3,2) - bas(3,1)*bas(2,2) )                 &
     &         *bas(1,3)                                                &
     &      + ( bas(3,1)*bas(1,2) - bas(1,1)*bas(3,2) )                 &
     &         *bas(2,3)                                                &
     &      + ( bas(1,1)*bas(2,2) - bas(2,1)*bas(1,2) )                 &
     &         *bas(3,3)
      omega = omega*alat**3
      write(6,'('' Real space volume '',d14.6)') omega

        bases(1,1)= bases(1,4)
        bases(2,1)= bases(2,4)
        bases(3,1)= bases(3,4)
        bases(1,2)= bases(1,6)
        bases(2,2)= bases(2,6)
        bases(3,2)= bases(3,6)
        bases(1,3)= bases(1,5)
        bases(2,3)= bases(2,5)
        bases(3,3)= bases(3,5)
        bases(1,4)= bases(1,7)
        bases(2,4)= bases(2,7)
        bases(3,4)= bases(3,7)

        itype(1) = 1
        itype(2) = 2
        itype(3) = 3
        itype(4) = itype(2)

!c    =================================================================== !===
!c     FOR NOW set Muffin-tin Radius equal to the Wigner-Seitz Radius
!c    =================================================================== !===
        rmt(1) = ( (3.d0*omega/(4.d0*pi))/4.d0 )**(1.d0/3.d0)
        rmt(2) = rmt(1)
        rmt(3) = rmt(1)
        rmt(4) = rmt(1)

        nrot = 12

!c    =================================================================== !===
!c     DO22 (TiAl3) lattice setup
!c    =================================================================== !===
      else if( icryst .eq. 10 ) then
         nbasis=8
         lattyp=3

         bases(1,1) = 0.d0
         bases(2,1) = 0.d0
         bases(3,1) = 0.d0

         bases(1,2) = 0.5d0
         bases(2,2) = 0.5d0
         bases(3,2) = 0.d0

         bases(1,3) = 0.5d0
         bases(2,3) = 0.d0
         bases(3,3) = 0.5d0

         bases(1,4) = 0.d0
         bases(2,4) = 0.5d0
         bases(3,4) = 0.5d0

         do i=1,4
          bases(1,i+4) = bases(1,i)
          bases(2,i+4) = bases(2,i)
          bases(3,i+4) = bases(3,i) + 1.d0
         end do

         do i=1,nbasis
          itype(i) = i
         end do
         itype(1) = 1
         itype(2) = 2
         itype(3) = 3
         itype(4) = itype(3)
         itype(5) = 4
         itype(6) = 5
         itype(7) = itype(3)
         itype(8) = itype(3)

         rmtd=(sqrt(2.d0)/4.d0)
         rmt(1)=rmtd*alat
         rmt(2)=rmt(1)
         rmt(3)=rmt(1)
         rmt(4)=rmt(1)
         rmt(5)=rmt(1)
         rmt(6)=rmt(1)
         rmt(7)=rmt(1)
         rmt(8)=rmt(1)

         nrot = 16

!c     =================================================================
!c     Si (Si4) lattice setup
!c    =================================================================== !===
      else if(icryst.eq.11) then
         nbasis=4
         lattyp=1

         bases(1,1)=0.d0
         bases(2,1)=0.d0
         bases(3,1)=0.d0
         bases(1,2)=0.125d0
         bases(2,2)=0.125d0
         bases(3,2)=0.125d0
         bases(1,3)=0.25d0
         bases(2,3)=0.25d0
         bases(3,3)=0.25d0
         bases(1,4)=0.375
         bases(2,4)=0.375
         bases(3,4)=0.375
         itype(1) = 1
         itype(2) = 1
         itype(3) = 1
         itype(4) = 1

         rmt(1)=alat*(sqrt(3.d0)/8.d0)
         rmt(2)=rmt(1)
         rmt(3)=rmt(1)
         rmt(4)=rmt(1)

         nrot = 48

!c    =================================================================== !===
      else if(icryst.ne.0) then
       write(*,*) '  STRUCTURE::  ICRYST = ',icryst,' ???? '
       stop ' STRUCTURE: wrong ICRYST'
      endif
!c
!c     ===============================================================
!c     set up basis ...............fcc..............
!c     ===============================================================
      if(lattyp .eq. 1) then
         do ns=1,3
            do i=1,3
               bas(i,ns)=basfcc(i,ns)*coef
            enddo
         enddo

!c     ===============================================================
!c     set up basis ...............bcc..............
!c     ===============================================================
      else if(lattyp .eq. 2) then
         do ns=1,3
            do i=1,3
               bas(i,ns)=basbcc(i,ns)*coef
            enddo
         enddo
!c     ===============================================================
!c     set up basis ...............sc...............
!c     ===============================================================
      else if(lattyp .eq. 3) then
         do ns=1,3
            do i=1,3
               bas(i,ns)=basscc(i,ns)*coef
            enddo
         enddo
!c     ===============================================================
      else if( lattyp .eq. 4 ) then

        do ns=1,3
          do i=1,3
            if(abs(bas(i,ns)-basrhm(i,ns)).gt.1.d-6) then
             write(*,*) '  BASRHM::'
             write(*,*) ((basrhm(ii,nsi),ii=1,3),nsi=1,3)
             write(*,*) '  BAS::'
             write(*,*) ((bas(ii,nsi),ii=1,3),nsi=1,3)
             write(*,*)
             write(*,*) ' GIVEBAS:: Check trigonal translation vectors'
             write(*,*) '        and coordinates of atoms in unit cell'
             write(*,*) ' If all is OK -- remove STOP'
             stop ' GIVEBAS::  Check trigonal translation vectors '
            end if
          enddo
        enddo
        do ns=1,3
          do i=1,3
            bas(i,ns)=basrhm(i,ns)*coef
          enddo
        enddo

!c     ===============================================================
      else if( lattyp .eq. 5 ) then
        do ns=1,3
          do i=1,3
            bas(i,ns)=basbct(i,ns)*coef
          enddo
        enddo
      else if( lattyp .ne. 0 ) then
        write(*,*) '  GIVEBAS::  LATTYP=',lattyp,' is not implemented'
        stop ' GIVEBAS:: wrong LATTYP'
      endif
!c     ===============================================================

      do ns=1,3
       do i=1,3
        bas(i,ns) = bas(i,ns)*abc(i)
       end do
      end do

      write(6,'(''  Bravais lattice'',                                  &
     &              t40,''='',i5)') lattyp

      volume = ( bas(2,1)*bas(3,2) - bas(3,1)*bas(2,2) ) * bas(1,3) +   &
     &         ( bas(3,1)*bas(1,2) - bas(1,1)*bas(3,2) ) * bas(2,3) +   &
     &         ( bas(1,1)*bas(2,2) - bas(2,1)*bas(1,2) ) * bas(3,3)



      if(volume.lt.0.d0) then
       write(6,*) ' VOLUME=',volume
       call fstop(sname//':: You use left-hand basis ???')
      end if

      volume = abs(volume)

123    format(1x,5F12.5)

      rws0 = (3.d0*volume/(4.d0*pi))**(1.d0/3.d0)
      delta = 0.d0
      do i=1,nbasis
       delta = delta+rmt(i)**3
      end do


      delta = (rws0**3/delta)**(1.d0/3.d0)
      do i=1,nbasis
        rws(i) = delta*rmt(i)
      end do

      return
      end
!*
      subroutine PrintDef(iflag)
      include 'lmax.h'
      include 'mkkrcpa.h'
      include 'inverse.h'

      if(iflag.eq.1) then
!c                            print 'mkkrcpa.h'
       write(6,*) ' include <mkkrcpa.h>:'
       write(6,*)
       write(6,*) ' ipepts=',ipepts,'    max. pts. on E contour'
       write(6,*) ' iprpts=',iprpts,'    max. pts. on r grid (odd only)'
       write(6,*) ' ipcomp=',ipcomp,'    max. species for alloy'
       write(6,*) ' ipspin=',ipspin,'    max. spin manifolds'
       write(6,*) ' ipbase=',ipbase,'    max. basis atoms'
       write(6,*) ' ipsublat=',ipsublat,'   max. sublattices'
       write(6,*) ' ipcrys=',ipcrys
       write(6,*) ' iplmax=',iplmax,'  max. ang. momentum '
       write(6,*) ' lcllmax=',lcllmax,' max. (intern.) ang. momentum'
!CDEBUG       write(6,*) ' ipktop=',ipktop
!CDEBUG       write(6,*) ' ip2lmp1=',ip2lmp1,'    2*iplmax+1'
       write(6,*) ' ipkkr=',ipkkr,'    (iplmax+1)**2'
!CDEBUG       write(6,*) ' iprmat=',iprmat,'   ipkkr*ipbase'
       write(6,*) ' ipdlj=',ipdlj,'   (2*iplmax+1)**2'
       write(6,*) ' ipeval=',ipeval,'    max. core eigenvalues'
       write(6,*) ' ipxkns=',ipxkns,                                    &
     &            '    max number of k-vectors in Ewald'
       write(6,*) ' iprsn=',iprsn,                                      &
     &            '    max number of r-vectors in Ewald'
       write(6,*) ' iprij=',iprij,                                      &
     &            '    max number of different Rij-vectors'
       write(6,*) ' iprhp=', iprhp,                                     &
     &            '   number of harmon.polyn. in memory'
       write(6,*)
       write(6,*) ' Constants:'
       write(6,*) ' zero=',zero,'   five=',five
       write(6,*) ' one=',one,'   six=',six
       write(6,*) ' two=',two,'   seven=',seven
       write(6,*) ' three=',three,'   eight=',eight
       write(6,*) ' four=',four,'   ten=',ten
       write(6,*) ' Pi = ',pi
       write(6,*)
       write(6,*) ' stats(1)',stats(1)
       write(6,*) ' stats(2)',stats(2)
       write(6,*) ' stats(3)',stats(3)
       write(6,*) ' stats(4)',stats(4)
       write(6,*) ' fmt(1)=',fmt(1)
       write(6,*) ' fmt(2=',fmt(2)
       write(6,*) ' ipunit=',ipunit,'   max. fortran units'
       write(6,*)
       write(6,*) ' etol=',etol,'   energy cnvgs'
       write(6,*) ' rmstol=',rmstol,'   chrg dnsty cnvgs'
       write(6,*) ' ipmesh=',ipmesh,'   max number of k-meshes'
!CAB       write(6,*) ' ikpsym=',ikpsym
       write(6,*) ' maxcpa=',maxcpa,'   max CPA iterations'
       write(6,*) ' ipmdlng=',ipmdlng,'   max Madelung vectors'
       write(6,*) ' etamdl=',etamdl
       write(6,*) ' iptmpi1=',iptmpi1
       write(6,*) ' iptmpr1=',iptmpr1
       write(6,*) ' c_mf_cc=',c_mf_cc
       write(6,*) ' U0=',U0,'  screening potential parameter'

      else if(iflag.eq.2) then
!c                            print 'inverse.h'
       write(6,*) ' include <inverse.h>:'
       write(6,*)
       write(6,*) ' inverse=',inverse
       write(6,*) ' nblock =',nblock
       write(6,*) ' nlev   =',nlev
       write(6,*) ' tolinv =',tolinv
       write(6,*) ' MemoryLimit(MB)=',MemoryLimit
       write(6,*) ' tolCut =',tolCut
       write(6,*) ' prlim  =',prlim
       write(6,*) ' prSLU  =',prSLU
       write(6,*) ' prPRC  =',prPRC
      end if
      return
      end
