      subroutine gettcinv(tcpa,tcinv,kkrsz,nbasis,                      &
     &                    alat,                                         &
     &                    iprint,istop)

!c     ================================================================
!c
      implicit real*8 (a-h,o-z)
!c
      character  istop*10
      character  sname*10
!c
!c +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
      include 'lmax.h'
      include 'mkkrcpa.h'
!c +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!c
!c      real*8     scale
      real*8     alat
      real*8     rytodu
!c parameter
      real*8     half
!c
      complex*16 tcpa(ipkkr,ipkkr,ipsublat)
      complex*16 tcinv(ipkkr,ipkkr,ipsublat)
      complex*16 w1(kkrsz,kkrsz)
!c      complex*16 pdu
      complex*16 sqrtm1
!c
!c +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
      parameter (half=one/two)
      parameter (sqrtm1=(0.d0,1.d0))
      parameter (sname='gettcinv')
!c +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!c
!c     **************************************************************
!c     computes tc**-1 given tc......................................
!c     converts tc**-1 from Ry. to DU................................
!c     calculates scale factor........<=== excluded, A.S. (July 1998)
!c     gms [APRIL 1992]..............................................
!c     **************************************************************
!c
!c     ==============================================================
!c     invert t-matrix...............................................
      do nsub=1,nbasis
!c        write(6,'('' gettcinv: calling matinv: nsub='',i3)') nsub
!c        -----------------------------------------------------------
         call matinv(tcpa(1,1,nsub),tcinv(1,1,nsub),w1,                 &
     &             kkrsz,iprint,istop)
!c        -----------------------------------------------------------
      enddo
!c     ==============================================================
!c     printout if needed............................................
      if(iprint.ge.1) then
         do nsub=1,nbasis
            write(6,'('' fulcpa:: tcinv :: nsub='',i3)') nsub
            call wrtmtx(tcinv(1,1,nsub),kkrsz,istop)
         enddo
      endif
!c     ==============================================================
!c     convert t-matrix to du........................................
      rytodu=half*alat/pi
      do nsub=1,nbasis
         do i=1,kkrsz
            do j=1,kkrsz
               tcinv(j,i,nsub)=rytodu*tcinv(j,i,nsub)
            enddo
         enddo
!c        ===========================================================
!c        printout if needed.........................................
         if( iprint .ge. 1 ) then
            write(6,'('' gettcinv:: tcinv(du) : nsub='',i3)') nsub
            call wrtmtx(tcinv(1,1,nsub),kkrsz,istop)
         endif
      enddo
!c     ===============================================================
      if(istop.eq.sname) then
         call fstop(sname)
      else
         return
      endif
!c
      end

