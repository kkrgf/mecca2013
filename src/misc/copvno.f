!c
!c     cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      subroutine copvno(vrnew,vrold,nbasis,komp,mspn)
!c     ==================================================================
!c
      implicit none
!c
!c    +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!      include 'mkkrcpa.h'
!c    +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!c
      integer, intent(in) :: komp(nbasis)  ! (ipsublat)
      integer, intent(in) :: nbasis
      integer, intent(in) :: mspn
      integer nsub
      integer ik
      integer is
!c
      real(8), intent(in)  :: vrnew(:,:,:,:)  ! (iprpts,ipcomp,ipsublat,ipspin)
      real(8), intent(out) :: vrold(:,:,:,:)  ! (iprpts,ipcomp,ipsublat,ipspin)
      integer nr
!c====================================================================
!c
      nr = min(size(vrnew,1),size(vrold,1))
      do nsub=1,nbasis
        vrold(1:nr,1:komp(nsub),nsub,1:mspn) =                          &
     &                              vrnew(1:nr,1:komp(nsub),nsub,1:mspn)
      enddo
!c
      return
      end
