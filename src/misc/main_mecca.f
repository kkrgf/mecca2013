!c
!c     M ultiple-scattering      |\  /|  --   --  --  --
!c     E lectronic-structure     | \/ | |-   |   |   |__|
!c     C alculations for         |    | |___ |__ |__ |  |
!c     C omplex
!c     A lloys
!c
!c     ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
       subroutine main_mecca(internalFile,outinfo)
!c     =================================================================
!c
       USE mecca_constants
       USE mecca_types
       USE input, only   : saveInput
       USE struct,  only : setup_struct,rs0box,ks0box,ew0box
         USE struct, only : iprot  ! to be removed
       USE vp_str,  only : setOptSphBasis
       USE initpot, only : init_potential

!c     =================================================================
!c
      implicit real*8 (a-h,o-z)

      interface
        character (LEN=30) function buildDate()
        end function buildDate
      end interface
!c    ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
      type (IniFile ), target :: internalFile
      character(len=*) outinfo     ! output info
!c    ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

!      character(:), parameter :: version='13.0 '

      character    name(ipcomp,ipsublat)*10
!C      character    text*72
      character    namk*32
      character    istop*10
      character    sname*10
      character    coerr*80
      character    header*80
!c
      integer      nhost0,ntask
      character*32 MP_NAME
      integer iwait,isleep
      parameter (iwait=10,isleep=5)

      integer      ivpoint(ipsublat)
      integer      ikpoint(ipcomp,ipsublat)
      integer      komp(ipsublat)
      integer      jmt(ipcomp,ipsublat)
      integer      jws(ipcomp,ipsublat)
      integer      ij3(ipkkr*ipkkr*ipkkr)
      integer      nj3(ipkkr*ipkkr)
      integer      nrsn
      integer      numc(ipcomp,ipsublat,ipspin)
      integer      nc(ipeval,ipcomp,ipsublat,ipspin)
      integer      lc(ipeval,ipcomp,ipsublat,ipspin)
      integer      kc(ipeval,ipcomp,ipsublat,ipspin)
      integer      ndx(ipdlj)
      integer      imix,imixrho
!c
      integer      nq(3,ipmesh)
      integer      nqpt(ipmesh)
      real*8       dummymesh(3)
      integer   :: ksintsch=1


      real*8, allocatable :: qmesh(:,:,:)
      integer*4, allocatable :: wghtq(:,:)
      integer*4, allocatable :: kmatchphase(:,:)

!c      integer lrot(48,ipkpts,ipmesh)
      integer*4, allocatable :: lrot(:,:)

      integer*4, allocatable :: ngrp(:),kptgrp(:,:)
      integer*4, allocatable :: kptset(:,:),kptindx(:,:)

      real*8       twght(ipmesh)
      real*8       rot(49,3,3)
      real*8       cgaunt(ipkkr*ipkkr*ipdlj)

      real*8       rsn(iprsn,3)
      real*8       xknlat(ipxkns,3,ipmesh)

      integer      nkns(ipmesh)

      real*8       atcon(ipcomp,ipsublat)
      real*8       zvalss(ipcomp,ipsublat)
      real*8       zsemss(ipcomp,ipsublat)
      real*8       zcorss(ipcomp,ipsublat)
      real*8       zcortot(ipcomp,ipsublat)  ! fix write/read pot
      real*8       ztotss(ipcomp,ipsublat)
      real*8       rhonew(iprpts,ipcomp,ipsublat,ipspin)
      real*8       wrk1(iprpts)
      real*8       wrk2(iprpts)
      real*8       rho2(ipsublat)
      real*8       rhoqint(ipsublat)
      real*8       evalsum(ipcomp,ipsublat,ipspin)
      real*8       ecorv(ipcomp,ipsublat,ipspin)
      real*8       esemv(ipcomp,ipsublat,ipspin)
      real*8       xvalws(ipcomp,ipsublat,ipspin)
      real*8       xvalwsnw(ipcomp,ipsublat,ipspin)
      real*8       xvalmt(ipcomp,ipsublat,ipspin)
      real*8       qvalmt(ipcomp,ipsublat,ipspin)
      real*8       qtotmt(ipcomp,ipsublat,ipspin)
      real*8       qsemmt(ipcomp,ipsublat,ipspin)
      real*8       qcormt(ipcomp,ipsublat,ipspin)
      real*8       qsemtot(ipcomp,ipsublat,ipspin)
      real*8       qcortot(ipcomp,ipsublat,ipspin)
      real*8       qcorout(ipcomp,ipsublat,ipspin)
      real*8       qrms(ipcomp,ipsublat,ipspin)
      real*8       vrms(ipcomp,ipsublat,ipspin)
      real*8       rmt(ipcomp,ipsublat)
      real*8       rws(ipcomp,ipsublat)
      real*8       rmt_true(ipcomp,ipsublat)
      real*8       r_circ(ipcomp,ipsublat)
      real*8       xr(iprpts,ipcomp,ipsublat)
      real*8       rr(iprpts,ipcomp,ipsublat)
      real*8       h(ipcomp,ipsublat)
      real*8       xstart(ipcomp,ipsublat)
      real*8       excort(ipspin)
      real*8       qintex(ipspin)
      real*8       vrold(iprpts,ipcomp,ipsublat,ipspin)
      real*8       vrnew(iprpts,ipcomp,ipsublat,ipspin)
      real*8       rhoold(iprpts,ipcomp,ipsublat,ipspin)
      real*8       corden(iprpts,ipcomp,ipsublat,ipspin)
      real*8       semcor(iprpts,ipcomp,ipsublat,ipspin)
      real*8       ec(ipeval,ipcomp,ipsublat,ipspin)
      real*8       surfamt(ipsublat)
      real*8       omegmt(ipsublat)
      real*8       vdif,vshift(2),FieldMag
      real*8       vdold
      real*8       zvalav
      real*8       efermi
      real*8       vmtzup
      real*8       alpha
      real*8       beta
      real*8       etot,fetot
      real*8       coa
      real*8       ebot, ecmax
      real*8       etop
      real*8       eitop
      real*8       eibot
      real*8       esmear
      real*8       Rksp0,Rksp
      real*8       Rrsp
      real*8       conk
      real*8       con
      real*8       a
      real*8       qinter
      real*8       Eccsum
      real*8       R_nn
      real*8       omegint,surf,volmt,Estep

      real*8     rslatt(3,3),kslatt(3,3),bases(3,ipbase)
      integer    itype(ipbase),numbsub(ipsublat)
      integer    if0(48,ipbase)
!c
      complex*16   conr(ipdlj)
      complex*16   cfac(ipkkr*ipkkr)
      complex*16   egrd(ipepts),dele1(ipepts)

      integer iadjsph_ovlp

      ! For dos calculation (NDOS<ipepts)
      complex*16, allocatable :: ene(:),ewght(:)
      complex*16   xtws,tnen

      complex*16   doslast(ipcomp,ipsublat,ipspin)
      complex*16   doscklast(ipcomp,ipsublat,ipspin)
      complex*16   dosint(ipcomp,ipsublat,ipspin)
      complex*16   dosckint(ipcomp,ipsublat,ipspin)
      complex*16, allocatable :: dostspn(:,:),dostot(:)
      complex*16   dost
      complex*16   greenint(iprpts,ipcomp,ipsublat,ipspin)
      complex*16   greenlast(iprpts,ipcomp,ipsublat,ipspin)
      complex*16   cirl,enreal,coef,dos_Re
      complex*16, allocatable :: cotdl(:,:,:,:,:)

      ! density of states and bloch spectral function
      integer idosplot,ibsfplot,dosbsf_kintsch,iec
      integer dosbsf_nepts,bsf_ksamplerate,bsf_nkwaypts
      real*8  dosbsf_emin,dosbsf_emax,klen
      real*8 :: bsf_kwaypts(3,ipbsfkpts)
      character*32 :: bsf_kptlabel(ipbsfkpts)
      character*32 bsf_file
      integer bsf_fileno

      real*8 fermi_korig(3),fermi_en
      real*8 fermi_kpts(3,ipbsfkpts)
      integer fermi_nkpts,fermi_gridsampling,fermi_raysampling
      integer ifermiplot,fermi_fileno(2)
      character*32 :: fermi_kptlabel(ipbsfkpts)
      character*32 :: fermi_orilabel

      logical :: isflipped(ipsublat)
      integer :: flippoint(ipsublat)

      integer iconverge
      logical isdis
!c
!c=======================================================================
      character  stats(4)*7
      data stats/'old    ','unknown','new    ','replace'/
      character  fmt(2)*11
      data  fmt/'formatted  ','unformatted'/
!c=======================================================================
      character  cstat*7
      character  cfmt*11
      character  iname*32
!c +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
      parameter (sname='MAIN')
      real*8       twopi,fpi
      real*8       toler,toletop
      parameter (twopi=two*pi,fpi=four*pi)
      parameter (toler=.0000001d0)
      parameter (toletop=.0001d0)
!C****** parameters needed for locating the bottom of the contour ******
      parameter (epsilon=0.003d0,eshift=0.02d0)

!      integer iprot
!      parameter (iprot=48)

      complex*16   dop(ipkkr*ipkkr*iprot)
!CAB
      integer    nunit(0:ipunit)
      character  fname(0:ipunit)*32
      integer    ist(0:ipunit),ifm(0:ipunit)

!CAA   Reqd. for isoparametric Integ. (Voronoi Polyhedra)
      integer, allocatable :: fcount(:)
      real*8, allocatable ::    rmag(:,:,:,:,:)
      real*8, allocatable ::    vj(:,:,:,:,:)
      real*8  lattice(3),weight(MNqp)

      real*8, allocatable ::   madmat(:)
      integer, allocatable ::   mapstr(:)
      integer, allocatable ::   mappnt(:)
      real*8, allocatable ::   aij(:)
      integer, allocatable ::   mapij(:)
      integer, allocatable :: np2r(:)
      integer, allocatable :: numbrs(:)
      integer, allocatable :: mapsprs(:)
      complex*16, allocatable ::  hplnm(:)
      integer erralloc

      real*8    rsnij(iprij,4)

      integer   naij

      real*8  enlim(2,ipmesh-1)

      logical :: isDLM(ipsublat)

      real*8 Rnncut,Rsmall
      real*8 V0
      integer imethod,isparse

      character*5  zone
      character*8  date
      character*10 time
      character czone(5),cdate(8),ctime(10)
      equivalence (zone,czone(1)),(date,cdate(1)),(time,ctime(1))
      integer      datval(8)
      real MB
      parameter (MB=1024.*1024.)
!cab
         real*8  tmparr1(iptmpr1)
         integer itmparr(iptmpi1)
!cab
         integer*4 numef,nefmax
         logical exbrdn,FullZone

      type(Jacbn_VP), target :: jcbn0box

      type(RunState) :: mecca_state

      logical SERIAL
!c     ******************************************************************
!c     *****  start of executible statements  ***************************
!c     ******************************************************************

      mecca_state%intfile  => internalFile
      mecca_state%rs_box   => rs0box
      mecca_state%ks_box   => ks0box
      mecca_state%ew_box   => ew0box
      mecca_state%jcbn_box => jcbn0box

      NDOS = 101
      allocate( ene(NDOS), ewght(NDOS), dostspn(2,NDOS), dostot(NDOS),  &
     &   cotdl(iplmax+1,ipsublat,ipcomp,ipspin,NDOS),                   &
     &   stat=ierr)
      if(ierr.ne.0) call fstop("insufficient memory for dos vars")


      call fstop('CPU_START')
      call setDate(buildDate())

      write(6,*) '                                << MECCA >> '
      write(6,*) '     Multiple-scattering Electronic-structure Code',  &
     &            ' for Complex Alloys'
      write(6,'(19x,''(KKR-CPA code, version '',a,'')''//)') version//  &
     &                 build_date
      call saveInput(internalFile)

!c =================================================================
      call readin(                                                      &
     &                  nhost0,ntask,MP_NAME,                           &
     &                  nunit,fname,ist,ifm,nopen,                      &
     &                  istop,iprint,                                   &
     &                  lmax,kkrsz,ivar_mtz,                            &
     &                  mtasa,nspin,ndlm,nrel,                          &
     &                  Rrsp,Rksp0,Rsmall,Rnncut,                       &
     &                  enmax,eneta,                                    &
     &                  nmesh,nq,enlim,                                 &
     &                  namk,icryst,nsublat,alat,boa,coa,invadd,        &
     &                  rmt,rws,iadjsph_ovlp,                           &
     &                  komp,name,atcon,                                &
     &                  ztotss,zvalss,zsemss,zcorss,zcortot,            &
     &                  ivpoint,ikpoint,                                &
     &                  nitcpa,                                         &
     &                  igrid,etop,eibot,npar,esmear,                   &
     &                  nscf,npts,alpha,beta,ebot,eitop,ibot_cont,      &
     &                  rslatt,bases,nbasis,itype,omegws,numbsub,       &
     &                  idosplot,ibsfplot,dosbsf_kintsch,               &
     &                  dosbsf_nepts,dosbsf_emin,dosbsf_emax,           &
     &                  bsf_ksamplerate,                                &
     &                  bsf_nkwaypts,bsf_kwaypts,bsf_kptlabel,          &
     &                  bsf_fileno,                                     &
     &                  ifermiplot,fermi_en,fermi_korig,fermi_orilabel, &
     &                  fermi_nkpts,fermi_kpts,fermi_kptlabel,          &
     &                fermi_gridsampling,fermi_raysampling,fermi_fileno,&
     &                  isDLM,isflipped,flippoint                       &
     &)
!c =================================================================
      call DefMethod(1,imethod,isparse)

!     if(nscf.eq.0) then
      if(.false.) then
!
!      might just want to do a dos or bsf calc.
!     ... so I don't want to write all these files

!         istr   = -1
!         imdlng = -1
!         ispkpt = -1
!         idop   = -1
!         ist(2) =  2
!         ist(8) =  2
!         ist(9) =  2
!         ist(10)=  2
      else
        if(stats(ist(2)).eq.'new') then
!c  to write the structure data
         istr   = -1
        else
         istr   = 0
        end if

        if(stats(ist(8)).eq.'old') then
!c  to read the madelung constants
         imdlng = 1
        else if(stats(ist(8)).eq.'new'                                  &
     &     .or. stats(ist(8)).eq.'replace'                              &
     &          ) then
!c  to write the madelung constants
         imdlng = -1
        else
!c  to calculate only
         imdlng = 0
        end if

        if(stats(ist(9)).eq.'old') then
!c  to read the k-points set
         ispkpt = 1
        else if(stats(ist(9)).eq.'new'                                  &
     &     .or. stats(ist(9)).eq.'replace'                              &
     &          ) then
!c  to write the k-points set
         ispkpt = -1
        else
!c  to calculate only
         ispkpt = 0
        end if

        if(stats(ist(10)).eq.'old') then
!c  to read the dop-matrices
         idop = 1
        else if(stats(ist(10)).eq.'new'                                 &
     &     .or. stats(ist(10)).eq.'replace'                             &
     &          ) then
!c  to write the dop-matrices
         idop = -1
        else
!c  to calculate only
         idop = 0
        end if
      end if

      rytodu= alat/(two*pi)     !  pry*rytodu=pdu, ery*rytodu^2=edu
      dutory= (two*pi)/alat

      if((igrid.eq.2.or.igrid.eq.3).and.npts.gt.1000) then
        Tempr = eitop
      else
        Tempr = zero
      end if
      eibot = abs(eibot)

      V0 = U0
      Rsmall = sign(min(abs(Rsmall),abs(Rnncut)),Rsmall)
      Rnncut = Rnncut*twopi
      Rsmall = Rsmall*twopi
      if(abs(Rsmall).lt.1.d-14) Rsmall = zero

      if( (Rsmall.eq.zero.and.Rnncut.eq.zero).or. (V0.eq.zero) ) then
       if(imethod.ne.0) then
        imethod=0
        isparse=0
        write(6,*) '  METHOD HAS BEEN CHANGED:'
        call DefMethod(0,imethod,isparse)
        write(6,*)
       end if
      end if

!c     ------------------------------------------------------------------

      if(imethod.le.0) then

!c   K-space (standard or screened)

       etamax = enmax/eneta
       R2ksp = Rksp0**2
       Rksp = sqrt(etamax*R2ksp+enmax)
       ndrsn = iprsn
       ndksn = ipxkns
       ndrhp = iprhp
      else
       etamax = zero
       R2ksp = zero
       Rksp0 = zero
       Rksp = zero
       ndrsn = iprsn
       ndksn = 1
       ndrhp = 1
      end if

      ndimlhp = (2*lmax+1)*(lmax+1)

      allocate(                                                         &
     &          mapstr(1:nbasis*nbasis)                                 &
     &         ,mappnt(1:nbasis*nbasis)                                 &
     &         ,aij(1:3*nbasis*nbasis)                                  &
     &         ,mapij(1:2*nbasis*nbasis)                                &
     &         ,np2r(1:ndrsn*nbasis*nbasis)                             &
     &         ,numbrs(1:nbasis*nbasis)                                 &
     &         ,mapsprs(1:nbasis*nbasis)                                &
     &         ,madmat(1:nbasis*nbasis)                                 &
     &         ,hplnm(1:ndrhp*ndimlhp)                                  &
     &         ,stat=erralloc)
      if(erralloc.ne.0) then
        write(6,*) ' NBASIS=',nbasis
        write(6,*) ' NDRSN=',ndrsn
        write(6,*) ' NDRHP=',ndrhp
        write(6,*) ' ERRALLOC=',erralloc
        call fstop(sname//' :: ALLOCATION MEMORY PROBLEM ')
      else

        if(iprint.gt.0) then
!c  ----------------------------------------------------------
         if(nbasis*nbasis*4/MB.gt.1.d0) then
          write(6,*)                                                    &
     &      nbasis*nbasis*4/MB   +   nbasis*nbasis*4/MB  +              &
     &      3*nbasis*nbasis*8/MB +  2*nbasis*nbasis*4/MB +              &
     &      ndrsn*nbasis*nbasis*4/MB + nbasis*nbasis*4/MB+              &
     &      nbasis*nbasis*4/MB,                                         &
     &      'MB HAVE BEEN ALLOCATED (MAPS,PNTRS,AIJ)'
          write(6,*) 2*nbasis*nbasis*8/MB,                              &
     &      'MB HAVE BEEN ALLOCATED (MDLNG)'
         end if
         if(ndrhp*ndimlhp*16/MB.gt.10.d0)                               &
     &    write(6,*) ndrhp*ndimlhp*16/MB,                               &
     &      'MB HAVE BEEN ALLOCATED (HPPLNM)'
!c  ----------------------------------------------------------
        end if
      end if

      allocate(                                                         &
     &          fcount(nsublat)                                         &
     &         ,rmag(MNqp,MNqp,MNqp,MNF,nsublat)                        &
     &         ,vj(MNqp,MNqp,MNqp,MNF,nsublat)                          &
     &         ,stat=erralloc)
       if(erralloc.ne.0) then
        write(6,*) ' NBASIS=',nbasis
        write(6,*) ' MNqp=',MNqp
        write(6,*) ' MNF=',MNF
        write(6,*) ' ERRALLOC=',erralloc
        call fstop(sname//' :: ALLOCATION MEMORY PROBLEM ')
       endif

      ndimq = 1
      if(imethod.ne.3) then
       do imesh=1,nmesh
!CAB        ndimq = max(ndimq,abs(nq(1,imesh)*nq(2,imesh)*nq(3,imesh)))

        nminus = 0
        do i=1,3
         if(nq(i,imesh).ne.abs(nq(i,imesh))) nminus = nminus+1
        end do
        if(nminus.eq.0) then
         nshift = 1
        else if (nminus.eq.1) then
         nshift = 1
        else if (nminus.eq.2) then
         nshift = 1
        else if (nminus.eq.3) then
         nshift = 2
        end if

        ndimq = max(ndimq,                                              &
     &              nshift*abs(nq(1,imesh)*nq(2,imesh)*nq(3,imesh)))
       end do
      else
       ispkpt = -2
      end if
      nmesh = max(1,nmesh)
      allocate(                                                         &
     &          qmesh(1:3,1:ndimq,1:nmesh)                              &
     &         ,wghtq(1:ndimq,1:nmesh)                                  &
     &         ,kmatchphasE(1:ndimq,1:nmesh)                            &
     &         ,lrot(1:ndimq,1:nmesh)                                   &
     &         ,stat=erralloc)
      if(erralloc.ne.0) then
        write(6,*) ' NDIMQ=',ndimq
        write(6,*) ' NMESH=',nmesh
        write(6,*) ' ERRALLOC=',erralloc
        call fstop(sname//' :: ALLOCATION MEMORY PROBLEM ')
      end if

!c===============================================================
      call  structure(alat,nbasis,                                      &
     &                     rslatt,kslatt,bases,itype,aij,nbasis,        &
     &                     numbsub,nsublat,                             &
     &                     R_nn,                                        &
     &                     if0,mapstr,mappnt,mapij,imethod,             &
     &                     omegws,                                      &
     &                     madmat,imdlng,                               &
     &                     invadd,                                      &
     &                     dop,lmax,rot,nrot,idop,                      &
     &                     nq,qmesh,ndimq,nmesh,nqpt,ispkpt,            &
     &                     wghtq,kmatchphase,twght,lrot,                &
     &                     Rksp,xknlat,ndksn,nkns,                      &
     &                     Rrsp,rsn,ndrsn,nrsn,                         &
     &                     rsnij,nrsnij,iprij,                          &
     &                     np2r,numbrs,naij,                            &
     &                     tmparr1,itmparr,iprint,istop)
!c===============================================================
!c
!c  write the structure file
        nu = nunit(2)
        iname = fname(2)
        cfmt = fmt(ifm(2))
        cstat= stats(ist(2))
        if(istr.ne.0) then
         open(unit=nu,file='NEW.'//iname,                               &
     &        status=cstat,form=cfmt,err=2001)

         scale = one
         call iostrf(istr*nu,nbasis,itype,rslatt,bases,                 &
     &     boa,coa,scale,ipbase,invadd,nsublat,isflipped,flippoint)
         if(nu.eq.6) then
          write(6,*) ' +++++++++++++++++++++++++++++++++++++++++'
         else
          close(unit=nu)
         end if
        end if

!c  read/write the madelung constants
        nu = nunit(8)
        iname = fname(8)
        cfmt = fmt(ifm(8))
        cstat= stats(ist(8))
        if(imdlng.ne.0) then
         open(unit=nu,file=iname,status=cstat,form=cfmt,err=2001)
!c     ----------------------------------------------------------------
         call iomdlng(imdlng*nu,nbasis,madmat,nbasis)
!c     ----------------------------------------------------------------
         if(nu.eq.6) then
          write(6,*) ' +++++++++++++++++++++++++++++++++++++++++'
         else
          close(unit=nu)
         end if
        end if

!c  read/write the special k-points file

        nu = nunit(9)
        iname = fname(9)
        cfmt = fmt(ifm(9))
        cstat= stats(ist(9))
        if(abs(ispkpt).le.1) then
         if(abs(ispkpt).ne.0)                                           &
     &    open(unit=nu,file=iname,status=cstat,form=cfmt,err=2001)

         do imesh=1,nmesh

          if(abs(ispkpt).ne.0) then
!c     ----------------------------------------------------------------
           call iokpnt(ispkpt*nu,qmesh(1,1,imesh),wghtq(1,imesh),       &
     &                    lrot(1,imesh), twght(imesh),nqpt(imesh))
!c     ----------------------------------------------------------------
          end if

          if(nqpt(imesh).gt.ndimq.or.nqpt(imesh).lt.1) then
           write(6,*) ' NQPT(',imesh,')=',nqpt(imesh)
           write(6,*) ' NDIMQ=',ndimq
           call fstop(sname//' :: wrong NQPT')
          end if
          if(imesh.gt.1.and.nqpt(imesh).gt.nqpt(1)) then
           write(6,*) ' NQPT(1)=',nqpt(1)
           write(6,*) ' NQPT(',imesh,')=',nqpt(imesh)
           call fstop(sname//' :: NQPT(imesh)>NQPT(1)')
          end if
         end do

         if(nu.eq.6) then
          write(6,*) ' +++++++++++++++++++++++++++++++++++++++++'
         else
          close(unit=nu)
         end if

        end if

!c  read/write the dop-matrices
        nu = nunit(10)
        iname = fname(10)
        cfmt = fmt(ifm(10))
        cstat= stats(ist(10))
        if(idop.ne.0) then
         open(unit=nu,file=iname,status=cstat,form=cfmt,err=2001)

!c     ----------------------------------------------------------------
         call iodop(idop*nu,nrot,kkrsz,dop)
!c     ----------------------------------------------------------------
         if(nu.eq.6) then
          write(6,*) ' +++++++++++++++++++++++++++++++++++++++++'
         else
          close(unit=nu)
         end if
        end if

      ndrot = iprot

      if(nrot.gt.iprot.or.                                              &
     &   ( invadd.eq.1.and.ndrot.lt.min(48,2*nrot) )  ) then
       write(6,*) '  NROT=',nrot,' should be .LE. IPROT=',iprot
       if(invadd.eq.1)                                                  &
     & write(6,*) '  INVADD=1', ' NDROT=',ndrot,                        &
     &            ' should be .GE. > min(48,2*NROT)=',min(48,2*nrot)
       coerr = '  Increase parameter IPROT in include-file'
       call fstop(sname//':'//coerr)
      end if

!c  read magnetic field value
      nu = nunit(11)
      iname = fname(11)
      cfmt = fmt(ifm(11))
      cstat= stats(ist(11))
      FieldMag = zero
      if(nspin.ne.1) then
       if(stats(ist(11)).eq.'old') then
        open(unit=nu,file=iname,status=cstat,form=cfmt,err=2001)
!c
!c  to read magnetic file (in Tesla)
!c
        read(nu,*) Field
        write(6,'(''     External magnetic field (Tesla) '',            &
     &               t40,''='',f10.5)') Field
        FieldMag = Field*amuBoT
       end if
      end if

      allocate(                                                         &
     &          ngrp(1:nmesh)                                           &
     &         ,kptgrp(1:ndrot+1,1:nmesh)                               &
     &         ,kptset(1:ndrot,1:nmesh)                                 &
     &         ,kptindx(1:ndimq,1:nmesh)                                &
     &         ,stat=erralloc)

      FullZone=.true.
      do imesh=1,nmesh

!c   grouping of k-points
!c
!c     ----------------------------------------------------------------
       call grpkpnt(nqpt(imesh),wghtq(1,imesh),lrot(1,imesh),           &
     &   ngrp(imesh),kptgrp(1,imesh),kptset(1,imesh),kptindx(1,imesh))
!c     ----------------------------------------------------------------
       write(6,901) imesh,ngrp(imesh),                                  &
     &           (kptgrp(i+1,imesh)-kptgrp(i,imesh),i=1,ngrp(imesh))
901    format(' imesh=',i1,' ngrp=',i2,'  :: ',(8i6))
       if(FullZone.and.(ngrp(imesh).eq.1)) then
        if(wghtq(kptindx(kptgrp(1,imesh),imesh),imesh).ne.1)            &
     &          FullZone=.false.
       else
        FullZone=.false.
       end if

      end do

      if(FullZone) then
       write(6,*)  ' Full Zone Integration....'
       nrot = 1
      end if

      if (nbasis.gt.ipbase) then
         coerr=' nbasis greater than ipbase'
         call fstop(sname//':'//coerr)
      endif

        do i=1,3
         do j=1,3
          rslatt(j,i) = rslatt(j,i)*twopi
!c  rslatt(1..3,*)/(2*pi)*Alat -- latt.vect.
         end do
        end do

        if(imethod.ne.0.or.Rsmall.ne.zero) then
!c
!c  mapsprs(i,j) - map of sparse matrix
!c  mapsprs(i,j).ne.0, only if I and J are neighbours (Rij<Rcut)
!c                  or I and J have common neighbour
!c              =0, otherwise
!c

         write(6,*) ' MAPSPRS is calculated with Rnncut=',              &
     &                abs(Rnncut)/twopi
!c     ----------------------------------------------------------------
         call sprsmap(nbasis,mapstr,nbasis,aij,                         &
     &             abs(Rnncut),rslatt,mapsprs)
!c     ----------------------------------------------------------------

        end if
        if(imethod.ne.0) then
         write(6,*) ' Real-space G-ref from Screening V0=',V0
        end if

!c  Calculate the atomic charges and average charge and moment.......

!c     ----------------------------------------------------------------
!c      call zsngls(zvalss,zsemss,zcorss,ztotss,zvalav,
!c     >            atcon,numbsub,nsublat,komp,iprint,istop)
!c     ----------------------------------------------------------------

       nzatom = 0
       do nsub = 1,nsublat
        zatom = 0
        do ik=1,komp(nsub)
         zatom = zatom + atcon(ik,nsub)*ztotss(ik,nsub)
        end do
!c  to exclude empty spheres
        if(zatom.gt.1.d-7) nzatom=nzatom+numbsub(nsub)
       end do

!c  nzatom -- number of sites with Z.NE.0
      lattice(1)=alat
      lattice(2)=boa
      lattice(3)=coa
!
      call setup_struct(mecca_state)
!
      call setOptSphBasis(mecca_state)
!

!c  Read in the starting potentials..................................

!CDDJa testing initializing Ef by hand for start potential
      if(abs(etop).gt.toletop) efermi = etop
!CALAM=================================================================
!C  iset_b_cont .eq. 0  ==>  Potential file exists
!C            .ne. 0  ==>  Potential file does'nt exist
!C=====================================================================
!c      write(6,*) 'before getpot'
      call getpot_ovlp(nspin,nsublat,lattice,efermi,ivpoint,ikpoint,    &
     &               komp,jmt,jws,rmt_true,rmt,rws,r_circ,iadjsph_ovlp, &
     &               xr,rr,h,                                           &
     &               xstart,vrold,vdif,fcount,weight,rmag,vj,iVP,       &
     &               rhoold,corden,xvalws,ztotss,zcortot,zvalss,zcorss, &
     &               atcon,numc,nc,lc,kc,ec,                            &
     &               nbasis,itype,rslatt,bases,omegws,numbsub,ebot,     &
     &               ibot_cont,ipot_read,iset_b_cont,header,mtasa,      &
     &               isDLM,isflipped,flippoint,iprint,istop)

      write(6,*) 'efermi = ', efermi

!CALAM=================================================================
!c          CALCULATES THE TOTAL MT AND INTERSTITIAL VOLUME
!c     (In the old code these quantities had been calculated in
!c     structure.f file, but since the saddle pt. representation
!c     evaluates a new set of MT and ASA radii, so now it should be
!c     calculated after the overlapp of the charge density )
!c     ----------------------------------------------------------------

      omegint=zero
      do nsub=1,nsublat

!CALAM   Avg. Sphere surface and volume
        surf=zero
        volmt=zero
        do ic=1,komp(nsub)
          surf = surf + atcon(ic,nsub)*(4.d0*pi)*rmt(ic,nsub)**2
          volmt = volmt + atcon(ic,nsub)*(4.d0*pi/3.d0)*rmt(ic,nsub)**3
        enddo
        surfamt(nsub)= surf
        omegmt(nsub)= volmt
        omegint=omegint + omegmt(nsub)*numbsub(nsub)
      enddo
      omegint=abs(omegws-omegint)

      call init_potential( mecca_state )

!c     ----------------------------------------------------------------
!c     Set-up the input param for chg(pot) mixing and the type
!c                    of mixing simple(broyden)
!c     ----------------------------------------------------------------
      if(ipot_read.eq.0)then
         ichg = 0
         imix = 8
      else
         ichg = 1
         imix = 0
      endif
!c     ----------------------------------------------------------------

      if(ndlm.eq.1) then     ! be careful -- ndlm should be used, ndlm=0
       write(6,*) " --- Forcibly setting VDIF = 0 -- "
       vdif = zero
      end if
!CDLM
      if(abs(etop).lt.toletop) then
        write(6,'(/,''===========================================       &
     &   ======================='')')
        etop=efermi
        write(6,'(''  Top of contour reset to Ef'',t40,''='',           &
     &   f10.5)') etop
      endif
!c     ----------------------------------------------------------------
      write(6,'(''  Total cell volume'',t40,''='',d15.7)') omegws
      write(6,'(''  Interstitial volume'',t40,''='',d15.7)') omegint
!c     ----------------------------------------------------------------
!c
!CAB     set up r-,k-space coefficients  and
!CAB            real-space harmonic polynoms (r^l*Ylm)
!c     ----------------------------------------------------------------
!c      Set up (i)**(l-lp)..............................................
       call itollp(cfac,kkrsz,istop)
!c     ----------------------------------------------------------------
!c  calculate k-space coefficient in D.U. (for Ewald)
!c          conk=-4*pi/((2*pi/alat)^3*omega),
!c              here omega -- volume of unit cell (real space)
!c
      conk=-(four*pi)/(omegws*dutory**3)
!c
!c  calculate r-space coefficents -4.0/sqrt( pi)*2**l (for Ewald)
!c
      a=-four/sqrt(pi)
!c
!c     calculate indexing for array dqint which is the integral in
!c     real-space based on Ewald formulation. This integral only
!c     depends on shell and energy (not on k space).
!c     Calculate dqint once per energy  (see gettau & ewald).
!c
!c      ndlj= (2*lmax+1)**2
!c                         ndx(1:ndlj) -> l+1
      m3=0
      do l=0,2*lmax
        con=a*2**l
        cirl=(-dcmplx(zero,one))**l
        do m=-l,l
          m3=m3+1
          conr(m3) = (con/2)*cirl
!c                                   conr() -- is used in ewald-routine only
          ndx(m3)= l + 1
        enddo
      enddo
!c               (r**l)*Y_lm(r)   [ M.ge.0]
!c     ----------------------------------------------------------------
       call rlylm(rsnij,nrsnij,iprij,lmax,hplnm,ndrhp)
!c     ----------------------------------------------------------------
!c      Set up gaunt numbers............................................
!c     ----------------------------------------------------------------
      call gauntd( cgaunt, nj3, ij3, lmax, kkrsz )
!c     ----------------------------------------------------------------

       if(ibot_cont.ne.0)then
!CALAM ***************************************************************
!C      To estimate the location of the bottom of contour before
!C      actually starting the SCF calculation
!CALAM ***************************************************************
        if(iset_b_cont.ne.0)then
           vshift(1) = zero        ! shift of energy-point
          if(nspin.ne.1) then
            vshift(1) =      +FieldMag
            vshift(2) = -vdif-FieldMag
          else
            vshift(1) = zero
          end if

!          if(nspin.eq.2.and.ndlm.ne.1) then  ! start from non-magnetic potential
!           if(abs(vshift(1)-vshift(2)) .lt. 1.d-10) then
!             vshift(1) =  +0.015d0
!             vshift(2) =  -0.01d0
!           end if
!          end if
!
         write(6,'(/,''===========================================      &
     &======================='')')
         write(6,'(''LOCATE THE BOTTOM OF THE E-CONTOUR BEFORE THE      &
     & SCF ITERATION STARTS'')')
         write(6,*)
111      continue

         if(max(abs(ebot),abs(etop))*rytodu**2.gt.enmax)then ! enmax(in DU)
            enmax = (max(abs(ebot),abs(etop))+toletop)*rytodu**2
            ienmax = 0
            etamax = enmax/eneta
          if(iprint.ge.-100) then
            write(6,*) 'New ENMAX before getting into SCF Loop = ',enmax
          end if
         end if

         if(ienmax.eq.0.and.imethod.le.0) then
            ienmax = 1
            Rksp = sqrt(etamax*R2ksp+enmax)

            do imesh=1,nmesh
!c     ------------------------------------------------------------
             call genvec(kslatt,Rksp,qmesh(1,1,imesh),nqpt(imesh),      &
     &                  xknlat(1,1,imesh),ndksn,nkns(imesh),iprint,     &
     &                  tmparr1,itmparr)
!c     ------------------------------------------------------------
               if(iprint.ge.-100) then
                 write(6,1001) nkns(imesh),imesh
!c
               end if

            end do
         end if

        nume = 2
        egrd(1) = dcmplx(ebot,epsilon)
        egrd(2) = dcmplx(ebot,2.0d0*epsilon)
        enreal = dcmplx(ebot,0.0d0)
!C        enreal = dcmplx(ebot,epsilon/100.0d0)
!c  --------------------------------------------------------------
!c  calculate Int[n(e)], Int[e*n(e)] over contour in complex plane
!c  --------------------------------------------------------------
        istop = 'EBOT_CALC'
        ksintsch = 1
        call zplaneint1(nspin,nrel,icryst,efermi,iset_b_cont,           &
     &                    lmax,kkrsz,nsublat,numbsub,                   &
     &                    alat,omegws,aij,itype,nbasis,                 &
     &                    if0,mapstr,mappnt,nbasis,mapij,               &
     &                    komp,atcon,ztotss,                            &
     &                    ivpoint,ikpoint,                              &
     &                    vrold,vshift,h,rmt,rws,jmt,jws,xr,rr,         &
     &                    rmt_true,r_circ,ivar_mtz,fcount,weight,       &
     &                    rmag,vj,iVP,                                  &
     &                    nume,egrd,dele1,                              &
     &                    nitcpa,                                       &
     &                    nkns,ndx,                                     &
     &                    nqpt,                                         &
     &                    xknlat,ndksn,Rksp0,tmparr1,                   &
     &                    rsnij,nrsnij,iprij,                           &
     &                    np2r,ndrsn,numbrs,naij,                       &
     &                    rot,dop,nrot,                                 &
     &                    hplnm,ndrhp,ndimlhp,                          &
     &                    cfac,cgaunt,nj3,ij3,                          &
     &                    qmesh,ndimq,nmesh,                            &
     &                    wghtq,kmatchphase,lrot,                       &
     &                    ngrp,kptgrp,kptset,ndrot,kptindx,             &
     &                    doslast,doscklast,dosint,dosckint,            &
     &                    dostspn,                                      &
     &                    greenint,greenlast,evalsum,                   &
     &                    eneta,enlim,                                  &
     &                    conk,conr,                                    &
     &                    rslatt,kslatt,Rnncut,Rsmall,mapsprs,          &
     &                    V0,                                           &
     &                    bsf_ksamplerate,bsf_fileno,                   &
     &                    bsf_nkwaypts,bsf_kwaypts,                     &
     &                  fermi_korig,fermi_nkpts,fermi_kpts,             &
     &                  fermi_kptlabel,fermi_orilabel,                  &
     &                fermi_gridsampling,fermi_raysampling,fermi_fileno,&
     &                 cotdl,mtasa,ksintsch,isDLM,isflipped,flippoint,  &
     &                   iprint,istop)
!c     ----------------------------------------------------------------
        if (nspin.eq.1) then
        dostspn(2,1:nume) = dostspn(1,1:nume)
        end if

        do ien=1,nume
         dostot(ien) = (dostspn(1,ien) + dostspn(2,ien))/nbasis
        enddo
!C******  Interpolate the value of DOS on the real axis E--> E + i*0{^+}
        coef = (dostot(2)-dostot(1))/(egrd(2)-egrd(1))
        dos_Re = dostot(1) + coef*(enreal - egrd(1))

         write(6,1011) enreal,dos_Re,' Re+Im: E_R,dos/atom'
         if(ibot_cont == 1) ebotth = 1.0d-06
         if(ibot_cont == 2) ebotth = 1.0d-14
         if (dabs(dimag(dos_Re)).gt.ebotth)then
!c         if (dimag(dos_Re).gt.0.3d-05)then
            ebot=ebot - eshift
            go to 111
         endif
         write(6,*)
         write(6,*)'BOTTOM OF THE E-CONTOUR LOCATED AT = ', ebot
         write(6,'(''=============================================      &
     &====================='')')
!c     ----------------------------------------------------------------
        endif        ! End of iset_b_cont Loop
!c     ----------------------------------------------------------------
       endif         ! End of ibot_cont Loop
!c     ----------------------------------------------------------------
1011   format(2f11.7,1x,2g16.8,1x,a24)


!c     ================================================================
!c  Calculate the core and semi core densities.......................
!c     =================================================================
      write(6,'(/,''       Core states       : Starting potential'')')
!c     -----------------------------------------------------------------
      ecmax = ebot       ! max core e.v. must be below "ebot"
      call getcor(nspin,nsublat,komp,atcon,numbsub,                     &
     &            jmt,jws,xr,rr,h,                                      &
     &            vrold,ivpoint,ikpoint,numc,nc,lc,kc,ec,               &
     &            ztotss,zvalss,zsemss,zcorss,zcortot,qsemmt,qcormt,    &
     &            qsemtot,qcortot,qcorout,                              &
     &            ecorv,esemv,corden,semcor,                            &
     &            ibot_cont,ecmax,nrel,                                 &
     &            mtasa,iprint,istop)
      if(ibot_cont.ne.0)then
        ebot = ecmax       ! max core e.v. must be below "ebot"
      endif
!c     -----------------------------------------------------------------
      call ValRenrm(zvalss,ztotss,zcortot,zvalav,xvalws,ipcomp,         &
     &              ipsublat,nspin,nsublat,komp,atcon,numbsub,          &
     &              rhoold,corden,semcor,iprpts,irenorm,ipot_read)
!c     ----------------------------------------------------------------


      write(6,'(''  End   initialization: Core and Potentials'',//)')
!c     -----------------------------------------------------------------
!c  Save necessary info on disk (potential, structure, ...)
!c
      if(ibot_cont.ne.0.and.iset_b_cont.eq.0)then
       write(6,'(''     Bottom of contour is set at '',                 &
     &                  t40,''='',f10.5)') ebot
      endif


!    might just want to do dos or bsf calc.
!    ... so don't want to write out potential
!      if(nscf.eq.0) then

!c  write the potential and charge densities.........................
!        write(6,'(/,''       Starting Potential unit=23: '')')
!c     ----------------------------------------------------------------
!        call putpot(nspin,nsublat,atcon,alat,ebot,efermi,
!     >            ivpoint,ikpoint,komp,jmt,rmt,jws,rr,xstart,
!     >            vrold,vdif,rhoold,corden,xvalws,
!     >            zcortot,numc,nc,lc,kc,ec,
!     >            name,header,iprint,istop)
!c     ----------------------------------------------------------------
!       end if

!c     -----------------------------------------------------------------
!c  Solve CPA equations along energy contour and find Fermi energy...
!c     -----------------------------------------------------------------
      write(6,'('' Start SCF: Max. No. of iterations ='',i5)') nscf
      write(6,'(''     Mixing: chg. den=0; potential=1  '',             &
     &                 t40,''='',i5)') ichg
      if(ichg.le.0)  then
        ichg   =0
        if(imix.lt.0) imix=1      ! do Broyden
      endif
      if (ichg.gt.0) then
            imix = 0              ! must do simple mix of potential
            ichg = 1
      endif
      write(6,'(''     Mixing:  simple=0;   Broyden>0   '',             &
     &                 t40,''='',i5)') imix
!c
      ienmax = 0
      qrmsmax = one

      iconverge = 0
      do itscf=1,nscf

       vshift(1) = zero                 ! shift of energy-point
       if(nspin.ne.1) then
        vshift(1) =      +FieldMag
        vshift(2) = -vdif-FieldMag
       else
        vshift(1) = zero
       end if

!       if(nspin.eq.2.and.ndlm.ne.1) then  ! start from non-magnetic potential
!        if(itscf==1.and.abs(vshift(1)-vshift(2)) .lt. 1.d-10) then
!!        vshift(1) =  +0.015d0
!!        vshift(2) =  -0.01d0
!         vshift(1) =  +0.009375d0
!         vshift(2) =  -0.00625d0
!
!        end if
!       end if
!c
       if(istop.eq.'TERMINATE') then
        iconverge = 1
        write(6,'('' ************* TERMINATED AFTER'',i5,               &
     &          '' ITERATIONS *****************'')') itscf-1
        go to 100
       end if

       if(iprint.ge.-100) then
         write(6,*) ''
         write(6 ,'(a)') ' ========================================'    &
     &      //'=================================='
         write(6,'(''  Begin iteration '',i3)') itscf
         write(6 ,'(a)') ' ========================================'    &
     &      //'=================================='
         write(6,*) ''
       end if

       numef = 0
       nefmax = 0
       npts1 = mod(npts,1000)
       if(Tempr.le.0) then
        npts2 = 0
        npts = npts1
       else
        npts2 = npts/1000
       end if
       if(min(100,npts1)+npts2+1.gt.ipepts) then
        write(*,*) ' npts1=',npts1
        write(*,*) ' npts2=',npts2
        write(*,*) ' ipepts=',ipepts
        call fstop(sname//':  NPTS1+NPTS2+1 > IPEPTS???')
       end if

!c  set difference between MT potentials for spin-polarized calc.
       if(nspin.ne.1) then
        vdold=-(vshift(1)+vshift(2))            ! = vdif
       else
        vdold=zero
       end if
!c        ==============================================================
!c        solve CPA over energy mesh: ..................................
!c        --------------------------------------------------------------
 140   continue
       if(ebot.gt.etop) then
        etop = ebot + abs(ebot-etop)
       end if

       if(max(abs(ebot),abs(etop))*rytodu**2.gt.enmax) then   ! enmax - in DU
            enmax = (max(abs(ebot),abs(etop))+toletop)*rytodu**2
            ienmax = 0
            etamax = enmax/eneta
       end if

       if(ienmax.eq.0.and.imethod.le.0) then
            ienmax = 1
            Rksp = sqrt(etamax*R2ksp+enmax)

          if(iprint.ge.-100) then
            write(6,*) '    Reciprocal space:'
            write(6,*) '      New EnMax   = ',enmax
            write(6,*) '      New Cut-off = ',Rksp
          end if

            do imesh=1,nmesh
!c     ----------------------------------------------------------------
             call genvec(kslatt,Rksp,qmesh(1,1,imesh),nqpt(imesh),      &
     &                  xknlat(1,1,imesh),ndksn,nkns(imesh),iprint,     &
     &                  tmparr1,itmparr)
!c     ----------------------------------------------------------------
               if(iprint.ge.-100) then
                 write(6,1001) nkns(imesh),imesh
1001     format('       No. Vec. =', i5,'  imesh =',i2)
                end if

            end do
            write(6,*) ''
       end if
!c
!c  set up energy mesh. (contour integration)...........................
!c
       if(igrid.eq.1) then

!C       if(iprint.ge.-10) then
!C        write(6,*) sname//':: rectangular contour'
!C       end if

!c      Use Rectangular contour (igrid=1) as in original "fast kkr"
!c      (Johnson, Pinski, Stocks,  Phys. Rev. B 30, 5508-15 (1984))
!c      ------ Mainly  used to get Density of States
!c      Clearly, scf faster with Gauss-Cheybeshev (igrid.eq.2)
!c     ----------------------------------------------------------------
        call conbox(ebot,etop,eitop,eibot,zero,egrd,                    &
     &              ntmp,npar,dele1,npts1,iprint,istop)
!c     ----------------------------------------------------------------
        nume = ntmp

       else if( igrid .eq. 2 ) then

!C       if(iprint.ge.-10) then
!C       write(6,*) sname//':: Gauss integration contour'
!C       end if

!c
!c     From "congauss.f"  Calculate Gauss-Chebyshev contour
!c     (w/ Matsubara poles if finite T)
!c     ----------------------------------------------------------------
         call conTempr(Tempr,ebot,etop,egrd,dele1,                      &
     &                 npts1,npts2,iprint,istop)
!c     ----------------------------------------------------------------
          nume = npts1+npts2

          nume = nume+1
          egrd(nume) = dcmplx(etop,eibot)          ! last point
          dele1(nume) = dcmplx(zero,zero)

       else
      call fstop(sname//':: code not set up for igrid.ne.(1.or.2)')
       endif

       if(nume.gt.ipepts.or.nume.le.0) then
        write(6,*) '  IPEPTS=',ipepts,'  NUME=',nume
        call fstop(sname//':: nume.gt.ipepts.or.nume.le.0')
       end if

!c  --------------------------------------------------------------
!c  calculate Int[n(e)], Int[e*n(e)] over contour in complex plane
!c  --------------------------------------------------------------
        istop = 'main'
        ksintsch = 1
        call zplaneint(nspin,nrel,icryst,efermi,1,                      &
     &                    lmax,kkrsz,nsublat,numbsub,                   &
     &                    alat,omegws,aij,itype,nbasis,                 &
     &                    if0,mapstr,mappnt,nbasis,mapij,               &
     &                    komp,atcon,ztotss,                            &
     &                    ivpoint,ikpoint,                              &
     &                    vrold,vshift,h,rmt,rws,jmt,jws,xr,rr,         &
     &                    rmt_true,r_circ,ivar_mtz,fcount,weight,       &
     &                    rmag,vj,iVP,                                  &
     &                    nume,egrd,dele1,                              &
     &                    nitcpa,                                       &
     &                    nkns,ndx,                                     &
     &                    nqpt,                                         &
     &                    xknlat,ndksn,Rksp0,tmparr1,                   &
     &                    rsnij,nrsnij,iprij,                           &
     &                    np2r,ndrsn,numbrs,naij,                       &
     &                    rot,dop,nrot,                                 &
     &                    hplnm,ndrhp,ndimlhp,                          &
     &                    cfac,cgaunt,nj3,ij3,                          &
     &                    qmesh,ndimq,nmesh,                            &
     &                    wghtq,kmatchphase,lrot,                       &
     &                    ngrp,kptgrp,kptset,ndrot,kptindx,             &
     &                    doslast,doscklast,dosint,dosckint,            &
     &                    dostspn,                                      &
     &                    greenint,greenlast,evalsum,                   &
     &                    eneta,enlim,                                  &
     &                    conk,conr,                                    &
     &                    rslatt,kslatt,Rnncut,Rsmall,mapsprs,          &
     &                    V0,                                           &
     &                    bsf_ksamplerate,bsf_fileno,                   &
     &                    bsf_nkwaypts,bsf_kwaypts,                     &
     &                  fermi_korig,fermi_nkpts,fermi_kpts,             &
     &                  fermi_kptlabel,fermi_orilabel,                  &
     &               fermi_gridsampling,fermi_raysampling,fermi_fileno, &
     &               cotdl,mtasa,ksintsch,isDLM,isflipped,flippoint,    &
     &                    iprint,istop)
!c     ----------------------------------------------------------------
!c   terminate program if solving cpa for a single energy only...
!c
       if(npts1.ge.200) then
        call fstop(sname//':: NPTS.ge.200 : program terminating')
       endif

!c        ===============================================================
!c        calculate the fermi energy.....................................
!c        --------------------------------------------------------------
         call effindT(numef,etop,xtws,deltZ,tnen,                       &
     &               dosint,doslast,zvalav,qrmsmax,                     &
     &               nsublat,nspin,komp,atcon,numbsub,                  &
     &               efermi,iprint,istop)

          ! WARNING: STOP PERFORMING FERMI CALCULATION
!          numef = 0

          nefmax = max(nefmax,numef)

!C===========  Write the results in the out-file =================
       if(iprint.ge.-100.and.numef.eq.0) then
!c         if(ienmax.eq.0.and.imethod.le.0) then
!c           write(6,*) ' Reciprocal space:   New Cut-off = ',Rksp
!c           do imesh=1,nmesh
!c              if(iprint.ge.-100.and.numef.eq.0) then
!c                write(6,1001) nkns(imesh),imesh
!c1001     format('      Number of vectors',t40,'=',i5,'     imesh =',i3)
!c
!c              end if
!c           end do
!c         endif
!C======================================================================

!c======================================================================= !======
!C   COUNT the no. of atoms in unit cell excluding EMPTY sphere (if any)
!c======================================================================= !======
      natomcnt = 0
      do nsub = 1,nsublat
       zatom = 0
       do ik=1,komp(nsub)
        zatom = zatom + atcon(ik,nsub)*ztotss(ik,nsub)
       end do
!c  to exclude empty spheres
       if(zatom.gt.1.d-7) natomcnt=natomcnt+numbsub(nsub)
      end do

          write(6,*) sname//':: Gauss integration contour'

           if(Tempr.gt.zero) then
             eTempr=pi*Tempr/2.0d0         !  first Matsubara pole
           else
             eTempr = zero
           endif

         write(6,'(''  Bottom of circ-contour'',t40,''='',f10.5)')      &
     &                         ebot
         write(6,'(''  Top of circ-contour'',t40,''='',f10.5)') etop
         if(eTempr.gt.zero) then
         write(6,'(''  Matsubara line-contour at '',t40,''='', f10.5)') &
     &                           eTempr
         write(6,'(''  Top of line-contour'',t40,''='',                 &
     &                       f10.5)') dreal(egrd(npts1+npts2))
         end if
         write(6,'(''  Min. Imag(Egrd) :'',t40,''='',                   &
     &                       f10.5)') dimag(egrd(npts1+npts2))
         write(6,'(''  No. of energies : circ-part'',t40,''='',i4)')    &
     &                       npts1
         if(npts2.gt.0) then
          write(6,'(''                  : Laguerr lin-part'',t40,       &
     &            ''='',i4)') npts2
         end if
!C======================================================================
          if (nspin.eq.1) then
           dostspn(2,1:nume) = dostspn(1,1:nume)
          end if

           write(6,*)
          if(nspin.eq.2) then

           do ie=1,nume
            write(6,1111) egrd(ie),dostspn(1,ie)/natomcnt,              &
     &                   '  Re+Im: E , dos_spin1 '
           end do
1111       format(2f11.7,1x,2g16.8,1x,a24)

            write(6,*)
           do ie=1,nume
            write(6,1111) egrd(ie),dostspn(2,ie)/natomcnt,              &
     &                  '  Re+Im: E , dos_spin2 '
           end do

            write(6,*)
            do ie=1,nume
             dost = dostspn(1,ie) + dostspn(2,ie)
              write(6,1111) egrd(ie),dost/natomcnt,' Re+Im: E,dos/atom'
            end do
          else if(nspin.eq.1) then

            do ie=1,nume
             write(6,1111) egrd(ie),2.d0*dostspn(1,ie)/natomcnt,        &
     &                   ' Re+Im: E, dos/atom'
            end do

          end if

!C======================================================================
!c     Major printout....... Integrated n(e) at end of contour.....
          write(6,*)
          write(6,'(''    Zvaltot'',t40,''='',f16.11)') zvalav
          write(6,'(''    Total      Int{n(e)}: etop-ebot'',            &
     &            t40,''='',f16.11)') dimag(xtws)
          write(6,'(''    Zvaltot -  Int{n(e)}'',                       &
     &          t40,''='',f16.11)') deltZ

         write(6,'(''    Tot_DOS(~Ef) per cell = '' ,2d19.10)') tnen
         write(6,'(/)')
!C======================================================================

          write(6,1100) itscf,numef,etop,efermi
1100      format('  Itscf=',i3,'  numef=',i2,'  Ef_old=',d14.7,         &
     &               ' Ef_new=',d14.7/)
       end if
!C======================================================================

       if(numef.lt.0) then
        write(6,1100) itscf,numef,etop,efermi
        call fstop(sname//': Bad guess for Ef??? ')
       end if

        if(numef.gt.0.and.nscf.gt.1) then
!c         ebot = ebot+(efermi-etop)
          etop = efermi
          if(efermi.le.ebot) then
           write(6,*) '  MKKRCPA: etop .LE. ebot'
           write(6,*) '          ',etop,ebot
           call fstop(sname//':: bad guess for Ef')
          end if
          write(6,*) '    Reset Fermi energy. Repeating energy contour' &
     &//' integral.'
          write(6,*) ''
          go to 140
        endif

!C****************************************************************
!c   --------------------------------------------------------------------
!c   adjust <dos,green> according to a position of Fermi level and
!c   put spin factor in them ............
!c   --------------------------------------------------------------------
        call efadjust(Tempr,efermi,etop,egrd,nume,vshift,               &
     &               dosint,dosckint,doslast,doscklast,evalsum,         &
     &               greenlast,jws,zvalav,                              &
     &               nsublat,nspin,komp,atcon,numbsub,                  &
     &               xvalwsnw,xvalmt,iprint,istop)
!c   --------------------------------------------------------------------
!c   calculate entropy contribution to  free energy (See congauss.f)
!c   --------------------------------------------------------------------
       call entrpterm(Tempr,efermi,egrd(1),dele1(1),                    &
     &                                  1,0,                            &
     &                                  npts1+1,npts2,                  &
     &                  dostspn(1,1),dostspn(1,nume),nspin,             &
     &                  nbasis,entropy,iprint,istop)
!c   --------------------------------------------------------------------
!c   calculate the charge density for the ith scf iteration.........
!c   --------------------------------------------------------------------
        if(iprint.gt.-10) write(6,*)
!c   --------------------------------------------------------------------
        call getchg1(nspin,nsublat,rr,xr,jmt,jws,komp,omegmt,           &
     &               greenint,greenlast,efermi,etop,                    &
     &               rhonew,rhoold,corden,semcor,                       &
     &               qvalmt,qtotmt,qrms,igrid,itscf,ivar_mtz,iVP,       &
     &               r_circ,fcount,weight,rmag,vj,rmt_true,             &
     &               iprint,istop)
!c   --------------------------------------------------------------------
        if(iprint.ge.1) then
         write(6,*) ' For the new charge density (before mixing):'
         do isp = 1,nspin
          do nsub = 1,nsublat
           do ic = 1,komp(nsub)
            write(6,1002) ic,nsub,isp,                                  &
     &                    qvalmt(ic,nsub,isp),qtotmt(ic,nsub,isp)
1002        format(' ic=',i2,' nsub=',i5,' isp=',i1,                    &
     &             ' valence charge=',f10.5,                            &
     &             ' qtotmt=',f10.5)
           enddo
          enddo
         enddo
         write(6,*)
        end if
!c        ---------------------------------------------------------------
!c        compute max rms for charge density..........
!c
!CALAM   qrmsav=zero
        qrmsmax=zero
        do nsub=1,nsublat
         qrmsst=zero
         do isp = 1,nspin
          do ic=1,komp(nsub)
!CALAM           qrmsav=qrmsav + numbsub(nsub)*atcon(ic,nsub)*qrms(ic,nsu !b,isp)
           qrmsst=qrmsst + atcon(ic,nsub)*qrms(ic,nsub,isp)
!CDEBUG           qrmsst=max(qrmsst,qrms(ic,nsub,isp))
          enddo
         enddo
         qrmsmax=max(qrmsmax,qrmsst)
        enddo
!c        ==============================================================
!c        check that the charges are internally consistent..............
!c        --------------------------------------------------------------
        call qcheck(zvalav,qvalmt,xvalwsnw,xvalmt,                      &
     &               zsemss,qsemtot,zcorss,qcortot,                     &
     &               atcon,komp,numbsub,                                &
     &               nsublat,nspin,iprint,istop)
!c        --------------------------------------------------------------
!c        calculate potential corresponding to new charge density......
!c        --------------------------------------------------------------
        iprt = iprint-1
        if(iprint.lt.0) then
         iprt = min(-5,iprt)
        end if

!c       ---------------------------------------------------------------
        call genpot(xvalwsnw,qtotmt,ztotss,zcorss,zsemss,               &
     &               rhonew,vrold,vrnew,vrms,ivar_mtz,                  &
     &               vmtzup,madmat,omegint,omegmt,omegws,alat,          &
     &               itype,numbsub,nsublat,                             &
     &               rr,xr,jmt,rmt,rmt_true,r_circ,                     &
     &               fcount,weight,rmag,vj,iVP,                         &
     &               atcon,surfamt,komp,nbasis,nspin,ndlm,              &
     &               excort,qintex,emad,emadp,emtc,                     &
     &               rho2,rhoqint,qinter,                               &
     &               vdif,R_nn,Eccsum,mtasa,icryst,                     &
     &               isDLM,iprt,istop)
!c       ---------------------------------------------------------------
        vrmsav=zero
        do nsub=1,nsublat
         vrmsst=zero
         do ic=1,komp(nsub)
          do isp = 1,nspin
!CDEBUG           vrmsst=vrmsst + atcon(ic,nsub)*vrms(ic,nsub,isp)
           vrmsst=max(vrmsst,vrms(ic,nsub,isp))
           if(iprint.ge.0) then
           write(6,'(''  Sub-lat ='',i2,'' Comp ='',i2,'' Spin ='',i2,  &
     &       ''   Vrms'',t40,''='',d10.3,''   Qrms'',t60,''='',d10.3)') &
     &         nsub,ic,isp,vrms(ic,nsub,isp),qrms(ic,nsub,isp)
           endif
          enddo
         enddo
         vrmsav=max(vrmsav,vrmsst)
        enddo

        write(6,'(''  RMS: potl.'',t15,''='',d12.4,''      chg.'',      &
     &         10x,''='',d12.4/)') vrmsav,qrmsmax
!c     ----------------------------------------------------------------
!c     Calulate the total energy
!c     ----------------------------------------------------------------
         call tote(vrold,vrnew,rhonew,corden,xr,rr,                     &
     &             jmt,nsublat,nspin,                                   &
     &             numbsub,komp,atcon,ztotss,omegws,rho2,rhoqint,       &
     &             excort,qintex,emad,emadp,evalsum,                    &
     &             ecorv,esemv,                                         &
     &             etot,press,Eccsum,mtasa,iprint,istop)
!c     ----------------------------------------------------------------
           fetot = zero
        if(Tempr.gt.zero) then
           write(6,*)
           write(6,'(''     ***********************************'',      &
     &               ''**********************************'')')
           write(6,'(''         Total Free Energy (Ry) = '',f17.8)')    &
     &                            etot-Tempr*entropy
           write(6,'(''      Extrap. (T=0) Energy (Ry) = '',f17.8)')    &
     &                            etot-Tempr*entropy/2
           write(6,'(''     ***********************************'',      &
     &               ''**********************************'')')
           fetot = etot-Tempr*entropy
        end if

        if(iprint.ge.-100) then
           write(6,*)
        end if
!c     =================================================================
!c     Set etop equal to efermi for the next iteration
!c
        etop = efermi
!c     =================================================================

!c  perform mixing ...............................................

!C BEGINNING OF CHARGE or POTENTIAL MIXING ******************

        alphrho = alpha
        betarho = beta
        irho    = ichg
        imixrho = imix

        alphpot = alpha
        betapot = beta
        ipot    = ichg

        qintval = zero
        if(mtasa.gt.0.and.ichg.eq.0) then            ! ASA
         do nsub=1,nsublat
          do ic=1,komp(nsub)
           do isp=1,nspin
            qintval=qintval+atcon(ic,nsub)* ( xvalws(ic,nsub,isp)       &
     &                                      -zvalss(ic,nsub)/nspin   )  &
     &                          *numbsub(nsub)
           end do
          end do
         end do

         if(irenorm.gt.-4.and.alpha.lt.0.99d0) then
          irenorm = -100
          alphpot = min(alpha*0.99,0.05d0)
          betapot = min(beta*0.99,0.1d0)
          ipot = 1
          if(iprint.ge.0) write(6,*) ' WARNING:',                       &
     &  ' Bad initial charge density ==>',                              &
     &  ' mixing of potential is used (1st iteration)'
          do i=3,4
           nu = nunit(i)
           iname = fname(i)
           open(nu,file=iname,status='unknown')
           close(nu,status='delete')
          end do
         else if(                                                       &
     &           (irenorm.eq.-100)                                      &
     &           .and.alpha.lt.0.99d0) then
          alphpot = alpha*0.99
          betapot = beta*0.99
          irenorm = irenorm-1
          ipot = 1
          if(iprint.ge.0) write(6,*) ' WARNING:',                       &
     &  ' mixing of potential is used (2nd iteration)'
         else if(                                                       &
     &           (irenorm.eq.-101)                                      &
     &           .and.alpha.lt.0.99d0) then
          irenorm = irenorm-1
          imixrho = 0
         end if

        end if

        if(ichg.eq.0.and.ipot.eq.0) then

!c  charge densities are being mixed..........................

         imixkeep = imixrho
         akeep = alphrho
         bkeep = betarho

         if( (imixrho.gt.1.and.mod(itscf,max(1,imixrho)).eq.0)          &
     &      .or.(imixrho.eq.1.and.nefmax.ge.1)                          &
     &      ) then
          if(mod(itscf,imixrho).eq.0.or.nefmax.ge.1) then
           do i=3,4
            nu = nunit(i)
            iname = fname(i)
            open(nu,file=iname,status='unknown')
            close(nu,status='delete')
            imixkeep = 0
           end do
          end if
         end if

         do i=3,4
          nu = nunit(i)
          iname = fname(i)
          inquire(file=iname,EXIST=exbrdn)
          if(.not.exbrdn) then
            imixkeep = 0
            exit
          end if
         end do

!c     ----------------------------------------------------------------
         call mixing1(rhonew,rhoold,xvalws,xvalwsnw,omegws,             &
     &                qtotmt,qrmsav,vdif,vdold,                         &
     &                wrk1,wrk2,xr,rr,ivar_mtz,iVP,                     &
     &                irho,imixrho,alphrho,betarho,                     &
     &                itscf,komp,jmt,jws,nsublat,numbsub,nspin,         &
     &                r_circ,fcount,weight,rmag,vj,rmt_true,            &
     &                iprint)
!c     ----------------------------------------------------------------
!c  calculate potential corresponding to mixed charge density.
!c  ==============================================================
         call maxdiff(iprpts,ipcomp,ipsublat,                           &
     &                jws,komp,nsublat,nspin,                           &
     &                rhonew,rhoold,qrms)
!c  ==============================================================

!c     ----------------------------------------------------------------
         call genpot(xvalwsnw,qtotmt,ztotss,zcorss,zsemss,              &
     &                rhonew,vrold,vrnew,vrms,ivar_mtz,                 &
     &                vmtzup,madmat,omegint,omegmt,omegws,alat,         &
     &                itype,numbsub,nsublat,                            &
     &                rr,xr,jmt,rmt,rmt_true,r_circ,                    &
     &                fcount,weight,rmag,vj,iVP,                        &
     &                atcon,surfamt,komp,nbasis,nspin,ndlm,             &
     &                excort,qintex,emad,emadp,emtc,                    &
     &                rho2,rhoqint,qinter,                              &
     &                vdif,R_nn,Eccsum,mtasa,icryst,                    &
     &                isDLM,iprint,istop)
!c     ----------------------------------------------------------------
        else

!c  potentials are being mixed..............................

         imixkeep = -1
         akeep = alphpot
         bkeep = betapot
!c  ==============================================================
         call mixing1(vrnew,vrold,xvalws,xvalwsnw,omegws,               &
     &                qtotmt,qrmsav,vdif,vdold,                         &
     &                wrk1,wrk2,xr,rr,ivar_mtz,iVP,                     &
     &                ipot,0,alphpot,betapot,                           &
     &                itscf,komp,jmt,jws,nsublat,numbsub,nspin,         &
     &                r_circ,fcount,weight,rmag,vj,rmt_true,            &
     &                iprint)
!c  ==============================================================
        endif

!c  ==============================================================
         call maxdiff(iprpts,ipcomp,ipsublat,                           &
     &                jmt,komp,nsublat,nspin,                           &
     &                vrnew,vrold,vrms)
!c  ==============================================================
         dvmax=zero
         drhomax=zero
         do nsub=1,nsublat
          qrmsst=zero
          vrmsst=zero
          do isp = 1,nspin
           do ic=1,komp(nsub)
            qrmsst=qrmsst + atcon(ic,nsub)*qrms(ic,nsub,isp)
            vrmsst=vrmsst + atcon(ic,nsub)*vrms(ic,nsub,isp)
           enddo
          enddo
          drhomax=max(drhomax,qrmsst)
          dvmax=max(dvmax,vrmsst)
         enddo

        if(iprint.ge.1) then
         write(6,'(''  after mixing:  max dV'', t25,''='',d12.4,        &
     &           ''    dRho'',2x,''='',d12.4,/)') dvmax,drhomax
        end if
!c
!C END MIXING CALLS HERE ***********************************
!c     ----------------------------------------------------------------
!c     now copy new results into old arrays for next iteration
!c     ----------------------------------------------------------------
        call copvno(vrnew ,vrold ,nsublat,komp,jmt,nspin)
        call copvno(rhonew,rhoold,nsublat,komp,jws,nspin)
!c     ----------------------------------------------------------------
        do isp = 1,nspin
         do nsub = 1,nsublat
          do ik = 1,komp(nsub)
           xvalws(ik,nsub,isp) = xvalwsnw(ik,nsub,isp)
          enddo
         enddo
        enddo
!c        ==============================================================
!c        calculate core charge densities for new potential.............

        if(iprint.gt.-10) then
         write(6,'(''   Core states       : Iteration ='',i3)') itscf
        end if
!c        --------------------------------------------------------------
         ecmax=ebot
         call getcor(nspin,nsublat,komp,atcon,numbsub,                  &
     &               jmt,jws,xr,rr,h,                                   &
     &               vrold,ivpoint,ikpoint,numc,nc,lc,kc,ec,            &
     &               ztotss,zvalss,zsemss,zcorss,zcortot,qsemmt,qcormt, &
     &               qsemtot,qcortot,qcorout,ecorv,esemv,               &
     &               corden,semcor,ibot_cont,ecmax,nrel,                &
     &               mtasa,iprint,istop)

       if(ibot_cont.ne.0)then
         ebot = ecmax       ! max core e.v. must be below "ebot"
       endif

!c        --------------------------------------------------------------
!c        write the new potential and charge densities..................
        if(iprint.gt.-10) then
         write(6,'(''   Potential to unit 23: Iteration ='',i3)') itscf
        end if
!c        --------------------------------------------------------------
        call putpot(nspin,nsublat,atcon,alat,ebot,efermi,               &
     &               ivpoint,ikpoint,komp,jmt,rmt,jws,rr,xstart,        &
     &               vrold,vdif,rhoold,corden,xvalws,                   &
     &               zcortot,numc,nc,lc,kc,ec,                          &
     &               name,header,isDLM,isflipped,flippoint,iprint,istop)
!c        --------------------------------------------------------------
!c
!c     Write unit 63  with info you want to "keep" and check convergence
!c     -----------------------------------------------------------------
        nkeep=nunit(7)
        iname = fname(7)
        open(nkeep,file=iname,status='unknown')
!c     ----------------------------------------------------------------
        call keep(alat,boa,coa,atcon,ztotss,nsublat,komp,nspin,         &
     &            etot/nzatom,emtc/nzatom,fetot/nzatom,Tempr*ry2kelvin, &
     &            press,npts,nq,                                        &
     &            qrmsmax,efermi,vmtzup,vdif,namk,xvalws,zvalav,        &
     &            imixkeep,akeep,bkeep,nkeep,iprint,istop)
!c     -----------------------------------------------------------------
        close(nkeep)
!        write(6 ,'(/''  End   iteration '',i3,''  ---------''/)') itscf


      enddo                     ! END of SCF loop iteration

       if(istop.ne.'TERMINATE') then
      write(6,'('' ************* FAILED TO GET CONVERGENCE IN'',i5,     &
     &          '' ITERATIONS *****************'')') nscf
       else
        iconverge = 1
       endif

100   continue

!CALAM==================================================================
!C         Write the Density of states in a file named 'DOS' if the
!C                         calculation is converged
!C      ----------------------------------------------------------------

       if(nscf.eq.0) then

          iconverge = 1
          etop = efermi+dosbsf_emax
          if(max(abs(ebot),abs(etop))*rytodu**2.gt.enmax) then   ! enmax - in DU
               enmax = (max(abs(ebot),abs(etop))+toletop)*rytodu**2
               ienmax = 0
               etamax = enmax/eneta
            if(iprint.ge.-100) then
               write(6,*) '                       New ENMAX = ',enmax
            end if
          end if

          if(ienmax.eq.0.and.imethod.le.0) then

             ienmax = 1
             Rksp = sqrt(etamax*R2ksp+enmax)

             if(iprint.ge.-100) then
               write(6,*) '    Reciprocal space:  New Cut-off = ',Rksp
             end if

             do imesh=1,nmesh
               ksintsch = 0
               if( ksintsch == 1 ) then
                 call genvec(kslatt,Rksp,qmesh(1,1,imesh),nqpt(imesh),  &
     &                  xknlat(1,1,imesh),ndksn,nkns(imesh),iprint,     &
     &                  tmparr1,itmparr)
               else
!                write(6,*) 'Fixed override to Rksp = 4.0'
!                 Rksp = 4.0d0 ! debug warning
                 dummymesh = 0.d0
                 call genvec(kslatt,Rksp,dummymesh,1,                   &
     &                  xknlat(1,1,imesh),ndksn,nkns(imesh),iprint,     &
     &                  tmparr1,itmparr)

               end if
               if(iprint.ge.-100) write(6,1001) nkns(imesh),imesh
             end do

          end if


       end if

       if( iconverge == 1 .and. ifermiplot.ne.0 ) then

         if(ifermiplot.eq.1) istop = 'AKEBZ_CALC'
         if(ifermiplot.eq.2) istop = 'FERMI_CALC'
         write(6,*) ''
         write(6 ,'(a)') ' ========================================'    &
     &     //'=================================='
         write(6,'(''  Calculating Bloch Spectral Fn @ fixed E'')')
         write(6 ,'(a)') ' ========================================'    &
     &     //'=================================='
         write(6,*) ''

         isdis = .false.
         do n = 1, nbasis
           if(komp(n)>1) isdis = .true.
         end do
         if(isdis) then
          if(dosbsf_kintsch.eq.1) then
            write(6,*) ''
            write(6,*) '  Integrating k-space using Monkhorst-Pack'     &
     &        //' special k-points.'
          else if(dosbsf_kintsch .eq. 2 ) then
            write(6,*) ''
            write(6,*) '  Integrating k-space using wedges in '         &
     &        //'spherical coordinates.'
          else
            write(6,*) ''
            write(6,*) '  Integrating k-space using hybrid approach of'
            write(6,*) ''
            write(6,'(a,f5.2,a)') '   (1) DoS > ',hybrid_cutoff,        &
     &        ': Monkhorst-Pack special k-points'
            write(6,'(a,f5.2,a)')'   (2) DoS < ',hybrid_cutoff,         &
     &        ': Radial wedges in spherical coordinates'
            write(6,*) ''
          end if
         end if

         deallocate( ene, ewght, dostspn, dostot, cotdl )

         NDOS = 1
         allocate(ene(NDOS),ewght(NDOS),dostspn(2,NDOS),dostot(NDOS),   &
     &       cotdl(iplmax+1,ipsublat,ipcomp,ipspin,NDOS),               &
     &       stat=ierr)
         if(ierr.ne.0)                                                  &
     &     call fstop("insufficient memory for dos/bsf vars")

!C      ----------------------------------------------------------------
!C         Set up an E-grid parallel to REAL axis shifted into complex
!C                          plane by '' esmear ''
!C      ----------------------------------------------------------------

         ene(1) = dcmplx(efermi+fermi_en,esmear)
         ewght(1)=dcmplx(0.0d0,0.0d0)

!C      ----------------------------------------------------------------
         call zplaneint(nspin,nrel,icryst,efermi,1,                     &
     &                    lmax,kkrsz,nsublat,numbsub,                   &
     &                    alat,omegws,aij,itype,nbasis,                 &
     &                    if0,mapstr,mappnt,nbasis,mapij,               &
     &                    komp,atcon,ztotss,                            &
     &                    ivpoint,ikpoint,                              &
     &                    vrold,vshift,h,rmt,rws,jmt,jws,xr,rr,         &
     &                    rmt_true,r_circ,ivar_mtz,fcount,weight,       &
     &                    rmag,vj,iVP,                                  &
     &                    NDOS,ene,ewght,                               &
     &                    nitcpa,                                       &
     &                    nkns,ndx,                                     &
     &                    nqpt,                                         &
     &                    xknlat,ndksn,Rksp0,tmparr1,                   &
     &                    rsnij,nrsnij,iprij,                           &
     &                    np2r,ndrsn,numbrs,naij,                       &
     &                    rot,dop,nrot,                                 &
     &                    hplnm,ndrhp,ndimlhp,                          &
     &                    cfac,cgaunt,nj3,ij3,                          &
     &                    qmesh,ndimq,nmesh,                            &
     &                    wghtq,kmatchphase,lrot,                       &
     &                    ngrp,kptgrp,kptset,ndrot,kptindx,             &
     &                    doslast,doscklast,dosint,dosckint,            &
     &                    dostspn,                                      &
     &                    greenint,greenlast,evalsum,                   &
     &                    eneta,enlim,                                  &
     &                    conk,conr,                                    &
     &                    rslatt,kslatt,Rnncut,Rsmall,mapsprs,          &
     &                    V0,                                           &
     &                    bsf_ksamplerate,bsf_fileno,                   &
     &                    bsf_nkwaypts,bsf_kwaypts,                     &
     &                  fermi_korig,fermi_nkpts,fermi_kpts,             &
     &                  fermi_kptlabel,fermi_orilabel,                  &
     &               fermi_gridsampling,fermi_raysampling,fermi_fileno, &
     &                  cotdl,mtasa,dosbsf_kintsch,                     &
     &                  isDLM,isflipped,flippoint,                      &
     &                  iprint,istop)
!c     ----------------------------------------------------------------

       end if ! END of FERMI SURFACE CALC

       if( iconverge == 1 .and. (idosplot.ne.0.or.ibsfplot.ne.0) ) then

         write(6,*) ''
         write(6 ,'(a)') ' ========================================'    &
     &     //'=================================='

         if(idosplot/=0.and.ibsfplot/=0) then
           istop = 'DOSBSF_CAL'
           write(6,'(''  Calculating DoS and Bloch Spec. Function'')')
         else if(idosplot/=0) then
           istop = 'DOS_CALC'
           write(6,'(''  Calculating Density of States'')')
         else
           istop = 'BSF_CALC'
           write(6,'(''  Calculating Bloch Spectral Function'')')
         end if

         write(6 ,'(a)') ' ========================================'    &
     &     //'=================================='
         write(6,*) ''

         isdis = .false.
         do n = 1, nbasis
           if(komp(n)>1) isdis = .true.
         end do
         if(idosplot/=0.or.isdis) then
          if(dosbsf_kintsch.eq.1) then
            write(6,*) '  Integrating k-space using Monkhorst-Pack'     &
     &        //' special k-points.'
          else if(dosbsf_kintsch .eq. 2 ) then
            write(6,*) '  Integrating k-space using wedges in '         &
     &        //'spherical coordinates.'
          else
            write(6,*) '  Integrating k-space using hybrid approach of'
            write(6,*) ''
            write(6,'(a,f5.2,a)') '   (1) DoS > ',hybrid_cutoff,        &
     &        ': Monkhorst-Pack special k-points'
            write(6,'(a,f5.2,a)')'   (2) DoS < ',hybrid_cutoff,         &
     &        ': Radial wedges in spherical coordinates'
            write(6,*) ''
          end if
         end if

         deallocate( ene, ewght, dostspn, dostot, cotdl )

         NDOS = dosbsf_nepts
         allocate(ene(NDOS),ewght(NDOS),dostspn(2,NDOS),dostot(NDOS),   &
     &       cotdl(iplmax+1,ipsublat,ipcomp,ipspin,NDOS),               &
     &       stat=ierr)
         if(ierr.ne.0)                                                  &
     &     call fstop("insufficient memory for dos/bsf vars")

!C      ----------------------------------------------------------------
!C         Set up an E-grid parallel to REAL axis shifted into complex
!C                          plane by '' esmear ''
!C      ----------------------------------------------------------------
           Estep = ( (efermi+dosbsf_emax)-(efermi+dosbsf_emin) )/       &
     &                                  dfloat(NDOS)
           do ien=1,NDOS
             ene(ien)=dcmplx(efermi+dosbsf_emin+dfloat(ien-1)*Estep,    &
     &                       esmear)
             ewght(ien)=dcmplx(0.0d0,0.0d0)
           enddo

         ! make a gnuplot script for easy viewing
         ryd_ev = 13.6056923d0
         open(unit=3,file='ake.gnu',status='replace',iostat=iec)
         write(3,'(a)') 'unset xtics; unset colorbox; unset key'
         write(3,'(a)') 'set pm3d map'
         write(3,'(a)') 'set palette define (0 "white", 100 "black"'    &
     &     //', 1000 "black")'
         write(3,'(a,a,a)') 'set title "A(k,E) plot for ',trim(namk),'"'&
     &     //' offset 0.0,1.0'
         write(3,'(a)') 'set ylabel "Energy (eV)"'

         klen = 0.d0
         do i = 1, bsf_nkwaypts

           write(3,'(a,a,a,f10.6,a,f10.6,a)')                           &
     &       'set label "',trim(bsf_kptlabel(i)),'" at ',               &
     &       klen, ',', ryd_ev * (real(ene(1))-efermi),                 &
     &       ' front center offset 0.0,-1.0'

           if( i/=1 .or. i/=bsf_nkwaypts ) then

             write(3,'(a,f10.6,a,f10.6,a,f10.6,a,f10.6,a)')             &
     &         'set arrow from ',                                       &
     &         klen, ',', ryd_ev * (real(ene(1))-efermi), ' to ',       &
     &         klen, ',', ryd_ev * (real(ene(NDOS))-efermi),            &
     &         ' front nohead'

           end if

           if( i /= bsf_nkwaypts ) then
             klen = klen + dsqrt(                                       &
     &         (bsf_kwaypts(1,i+1)-bsf_kwaypts(1,i))*                   &
     &         (bsf_kwaypts(1,i+1)-bsf_kwaypts(1,i))+                   &
     &         (bsf_kwaypts(2,i+1)-bsf_kwaypts(2,i))*                   &
     &         (bsf_kwaypts(2,i+1)-bsf_kwaypts(2,i))+                   &
     &         (bsf_kwaypts(3,i+1)-bsf_kwaypts(3,i))*                   &
     &         (bsf_kwaypts(3,i+1)-bsf_kwaypts(3,i)) )
           end if

         end do

         write(3,'(a,f10.6,a)') 'set xrange [0:', klen, ']'
         write(3,'(a,f10.6,a,f10.6,a)') 'set yrange [',                 &
     &     ryd_ev * (real(ene(1))-efermi), ':',                         &
     &     ryd_ev * (real(ene(NDOS))-efermi), ']'
         write(3,'(a)') 'set zrange [0:1000]'
         write(3,'(a,f10.6,a)') 'splot "./akeBAND.dat" using '          &
     &     //'($1):(',ryd_ev,'*$2):($3)'

         close(unit=3)

!C      ----------------------------------------------------------------
         call zplaneint(nspin,nrel,icryst,efermi,1,                     &
     &                    lmax,kkrsz,nsublat,numbsub,                   &
     &                    alat,omegws,aij,itype,nbasis,                 &
     &                    if0,mapstr,mappnt,nbasis,mapij,               &
     &                    komp,atcon,ztotss,                            &
     &                    ivpoint,ikpoint,                              &
     &                    vrold,vshift,h,rmt,rws,jmt,jws,xr,rr,         &
     &                    rmt_true,r_circ,ivar_mtz,fcount,weight,       &
     &                    rmag,vj,iVP,                                  &
     &                    NDOS,ene,ewght,                               &
     &                    nitcpa,                                       &
     &                    nkns,ndx,                                     &
     &                    nqpt,                                         &
     &                    xknlat,ndksn,Rksp0,tmparr1,                   &
     &                    rsnij,nrsnij,iprij,                           &
     &                    np2r,ndrsn,numbrs,naij,                       &
     &                    rot,dop,nrot,                                 &
     &                    hplnm,ndrhp,ndimlhp,                          &
     &                    cfac,cgaunt,nj3,ij3,                          &
     &                    qmesh,ndimq,nmesh,                            &
     &                    wghtq,kmatchphase,lrot,                       &
     &                    ngrp,kptgrp,kptset,ndrot,kptindx,             &
     &                    doslast,doscklast,dosint,dosckint,            &
     &                    dostspn,                                      &
     &                    greenint,greenlast,evalsum,                   &
     &                    eneta,enlim,                                  &
     &                    conk,conr,                                    &
     &                    rslatt,kslatt,Rnncut,Rsmall,mapsprs,          &
     &                    V0,                                           &
     &                    bsf_ksamplerate,bsf_fileno,                   &
     &                    bsf_nkwaypts,bsf_kwaypts,                     &
     &                  fermi_korig,fermi_nkpts,fermi_kpts,             &
     &                  fermi_kptlabel,fermi_orilabel,                  &
     &               fermi_gridsampling,fermi_raysampling,fermi_fileno, &
     &             cotdl,mtasa,dosbsf_kintsch,                          &
     &                  isDLM,isflipped,flippoint,                      &
     &                  iprint,istop)
!c     ----------------------------------------------------------------


       endif  ! END of DOS/BSF

!CALAM==================================================================

      deallocate(                                                       &
     &          mapstr                                                  &
     &         ,mappnt                                                  &
     &         ,aij                                                     &
     &         ,mapij                                                   &
     &         ,np2r                                                    &
     &         ,numbrs                                                  &
     &         ,mapsprs                                                 &
     &         ,madmat                                                  &
     &         ,hplnm                                                   &
     &         ,qmesh                                                   &
     &         ,wghtq                                                   &
     &         ,lrot                                                    &
     &         ,ngrp                                                    &
     &         ,kptgrp                                                  &
     &         ,kptset                                                  &
     &         ,kptindx                                                 &
     &         ,fcount                                                  &
     &         ,rmag,vj                                                 &
     &         ,stat=erralloc)
!c     =================================================================
      info = 0
      call fstop('return')
      return
!c     =================================================================
!c     terminate the run...first stop all node programs.................
!c     call pvmfexit(info)

2001  continue
      write(*,*) sname//' NUNIT=',nu,' FILE=<<',iname,'>>'
      write(*,*) '  OPEN ERROR: file should be NEW'
      do i=1,nopen
       write(*,*) i,fname(i),stats(ist(i)),fmt(ifm(i))
      end do

      call fstop(sname//':: FILE ERROR ')
      return

      CONTAINS
!
      subroutine maxdiff(ndrpts,ndcomp,ndsublat,                        &
     &                   n1,n2,nsublat,nspin,                           &
     &                   vector1,vector2,dvmax)
      implicit none
      integer ndrpts,ndcomp,ndsublat
      integer nsublat,n1(nsublat),n2(nsublat),nspin
      real*8 vector1(ndrpts,ndcomp,ndsublat,1:nspin)
      real*8 vector2(ndrpts,ndcomp,ndsublat,1:nspin)
      real*8 dvmax(ndcomp,ndsublat,nspin)

      integer i1,i2,i3,i4
      real*8 dv

      dvmax = 0.d0
      do i4=1,nspin
       do i3=1,nsublat
        do i2=1,n2(i3)
       dvmax(i2,i3,i4) = maxval(abs(                                    &
     &                      vector1(1:n1(i3),i2,i3,i4)-                 &
     &                                vector2(1:n1(i3),i2,i3,i4)        &
     &                                ))
        end do
       end do
      end do

      return
      end subroutine maxdiff
!=====================================================================
      subroutine ValRenrm(zvalss,ztotss,zcortot,zvalav,xval,ndcomp,     &
     &                    ndsublat,nspin,nsublat,komp,atcon,numbsub,    &
     &                    rho,corden,semcor,ndrpts,irenorm,ipot_read)
!=====================================================================
      implicit none
      real*8  zvalss(ndcomp,ndsublat)
      real*8  ztotss(ndcomp,ndsublat)
      real*8  zcortot(ndcomp,ndsublat)
      real*8  zvalav
      integer ndcomp,ndsublat,nspin
      real*8  xval(ndcomp,ndsublat,nspin)
      integer nsublat
      integer komp(nsublat)
      real*8  atcon(ndcomp,nsublat)
      integer numbsub(nsublat)
!CALAM      real*8  zval(ndcomp,nsublat)
      integer ndrpts
      real*8  rho(ndrpts,ndcomp,ndsublat,nspin)
      real*8  corden(ndrpts,ndcomp,ndsublat,nspin)
      real*8  semcor(ndrpts,ndcomp,ndsublat,nspin)
      integer irenorm,ipot_read

      integer nsub,ic,isp,i
      real*8 zero,qintval,rhotmp
      parameter (zero=0.d0)

      zvalav=zero
      qintval = zero
      do nsub=1,nsublat
       do ic=1,komp(nsub)
         zvalav = zvalav+zvalss(ic,nsub)*atcon(ic,nsub)*numbsub(nsub)
        do isp=1,nspin
         if(ipot_read.ne.0)then
          xval(ic,nsub,isp) = ztotss(ic,nsub) - zcortot (ic,nsub)
         endif
         qintval=qintval+atcon(ic,nsub)* xval(ic,nsub,isp)              &
     &                       *numbsub(nsub)
        end do
       end do
      end do

      qintval = zvalav/qintval

      irenorm = nint(log10(max(1.d-14,abs(1.d0-qintval))))

      do nsub=1,nsublat
       do ic=1,komp(nsub)
        do isp=1,nspin
         xval(ic,nsub,isp) = xval(ic,nsub,isp)*qintval

         do i=1,ndrpts
          rhotmp = corden(i,ic,nsub,isp)+semcor(i,ic,nsub,isp)
          rho(i,ic,nsub,isp) = rhotmp +                                 &
     &               qintval*(rho(i,ic,nsub,isp)-rhotmp)
         end do
        end do
       end do
      end do

      return
      end subroutine ValRenrm
!======================================================================
       subroutine iokpnt(io,pointk,wghtq,lrot,twght,nkpt)
!======================================================================
!c
!c   read/write k-points
!c
       implicit none
       integer nch,nkpt,i,io
       real*8 pointk(3,*)
       integer wghtq(*)
       integer lrot(*)
       real*8  twght
       integer j,nint,nwghtq

       if(io.eq.0) return

       nch = abs(io)
       if(io.gt.0) then
        write(6,*) '  Reading in k-points mesh ...'
        read(nch,*,end=100,err=100) nkpt
        read(nch,*,end=100,err=100) twght
        write(6,1) nch,nkpt
1       format('   Channel ',i3,' number of special k-pts = ',i5)
        nwghtq = 0
        do i=1,nkpt
!c                                         Mike's format
         read(nch,*,end=100,err=100) wghtq(i),                          &
     &                 pointk(1,i),pointk(2,i),pointk(3,i)
         read(nch,*,end=100,err=100) (lrot(nwghtq+j),j=1,wghtq(i))
         nwghtq = nwghtq+wghtq(i)

        end do
       elseif(io.lt.0) then
        write(6,*) '  Writing down k-points mesh ...'
        write(nch,*) nkpt,'  K-POINTS MESH'
        write(nch,*) twght
        write(6,1) nch,nkpt
        write(6,'(13x,''twght '',d14.6)') twght
        nwghtq = 0
        do i=1,nkpt
!c                                         Andrew's format
!c         write(nch,*) kx(i),ky(i),kz(i),wghtq(i)
!c                                         Mike's format
         write(nch,2,err=100) wghtq(i),                                 &
     &                 pointk(1,i),pointk(2,i),pointk(3,i)
2        format(1x,i10,1x,3(1x,d22.15))
         write(nch,3,err=100) (lrot(nwghtq+j),j=1,wghtq(i))
3        format(1x,16i4)
         nwghtq = nwghtq+wghtq(i)
        end do
       end if

       return
100    write(*,*) ' IOKPNT:: Channel=',nch
       write(*,*) ' NKPT=',nkpt,' I=',i
       write(*,*) ' EOF/ERROR '
       call fstop(' IOKPNT: eof/error')
       end subroutine iokpnt

!======================================================================
       subroutine iodop(io,nop,kkrsz,dop)
!======================================================================
!c
!c   read/write dop-matrices
!c
       implicit none
       integer io,nop,kkrsz,i,kk1,kk2,nop0,kkrsz0,nch
       complex*16   dop(*)
       real*8 r1,r2

       if(io.eq.0) return

       nch = abs(io)
       if(io.gt.0) then
        write(6,*) '  Reading in DOP-matrices ... NOP=',                &
     &                                     nop,' KKRSZ=',kkrsz
        read(nch,*,err=100,end=100) nop0,kkrsz0
!c
        if(nop0.ne.nop.or.kkrsz.ne.kkrsz0) then
         write(*,*) ' NOP=',nop,' NOP(in file)=',nop0
         write(*,*) ' KKRSZ=',kkrsz,' KKRSZ(in file)=',kkrsz0
         call fstop(' IODOP: wrong info in input file')
        end if
!c
        do i = 1,nop
         do kk2 = 1,kkrsz
          read(nch,*,err=100,end=100)                                   &
     &     (dop(kk1+((kk2-1)+(i-1)*kkrsz)*kkrsz), kk1 = 1,kkrsz)
         enddo
         read(nch,*,err=100,end=1)
1        continue
        enddo
!c
       elseif(io.lt.0) then
!c
!C D-operators, rotations of Complex Spherical Harmonics
        write(6,*) '  Writing down DOP-matrices ...'
        write(nch,*) nop,kkrsz,' DOP-matrices'
        do i = 1,nop
!C
         do kk2 = 1,kkrsz
          do kk1=1,kkrsz
           r1 = dreal( dop(kk1+((kk2-1)+(i-1)*kkrsz)*kkrsz) )
           r2 = dimag( dop(kk1+((kk2-1)+(i-1)*kkrsz)*kkrsz) )
           if(abs(r1).lt.1.d-16) r1 = 0.d0
           if(abs(r2).lt.1.d-16) r2 = 0.d0
           dop(kk1+((kk2-1)+(i-1)*kkrsz)*kkrsz) = dcmplx(r1,r2)
          enddo
         enddo
!C
         do kk2 = 1,kkrsz
           write(nch,10,err=100)                                        &
     &     (dop(kk1+((kk2-1)+(i-1)*kkrsz)*kkrsz),kk1 = 1,kkrsz)
10         format(1x,'(',g23.16,',',g23.16,')')
         enddo
         write(nch,*)
        enddo
       end if

       return
100    write(*,*) ' IODOP:: Channel=',nch
       write(*,*) ' KK2=',kk2,' I=',i
       write(*,*) ' EOF/ERROR '
       call fstop(' IODOP: eof/error')
       end subroutine iodop

!======================================================================
       subroutine iomdlng(io,nbasis,madmat,ndimb)
!======================================================================
!c
!c   read/write madelung constants
!c
       implicit none
       integer io,nbasis,ndimb,nbasis0,i,j,nch
       real*8  madmat(ndimb,nbasis)

       if(io.eq.0) return

       nch = abs(io)
       if(io.gt.0) then
        write(6,*) '  Reading in Madelung constants ...'
        read(nch,*,err=100,end=100) nbasis0
        if(nbasis.ne.nbasis0) then
         write(*,*) ' NBASIS=',nbasis,' NBASIS(in file)=',nbasis0
         call fstop(' IOMDLNG: wrong info in input file')
        end if
        do i=1,nbasis
         read(nch,*,err=100,end=100) (madmat(i,j),j=1,nbasis)
        enddo
       elseif(io.lt.0) then
        write(6,*) '  Writing down Madelung constants ...'
        write(nch,*,err=100) nbasis,' MADELUNG CONSTANTS'
        do i=1,nbasis
         write(nch,*) (madmat(i,j),j=1,nbasis)
        enddo
       end if

       return
100    write(*,*) ' IOMDLNG:: Channel=',nch
       write(*,*) ' J=',j,' I=',i
       write(*,*) ' EOF/ERROR '
       call fstop(' IOMDLNG: eof/error')
       end subroutine iomdlng

      end subroutine main_mecca
!c     ============= END of MAIN ROUTINE   =============================
