!c     cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      subroutine scalar(nrelv,clight,                                   &
     &                  l,                                              &
     &                  bjl,bnl,g,f,gpl,fpl,                            &
     &                  tandel,cotdel,anorm,alpha,                      &
     &                  energy,prel,                                    &
     &                  rv_in,r,r2,h,jmt,iend,                          &
     &                  iswzj,                                          &
     &                  iprint,istop)
!c     ================================================================
!c
      use mecca_constants
      use stiffode, only : set_ode_params,odeint2
      implicit none
!c
      integer, intent(in) :: nrelv,l
      real(8), intent(in) :: clight
      complex(8) :: bjl(:,:)
      complex(8) :: bnl(:,:)
      complex(8) :: g(:)
      complex(8) :: f(:)
      complex(8) :: gpl(:)
      complex(8) :: fpl(:)
      complex(8) :: cotdel, tandel, anorm, alpha
      complex(8), intent(in) :: energy,prel
      real(8), intent(in) :: rv_in(:)
      real(8), intent(in) :: r(:)
      real(8), intent(in) :: r2(:),h
      integer, intent(in) :: jmt,iend,iswzj,iprint
      character(10), intent(in) :: istop
!c
      real*8 fact(4),factl1
      real*8 zed
      real*8 v0
      real*8 cinv
      real*8 c2inv
      real*8 power
!c parameter
      real*8 tole
!c
!c
      complex*16 clj,clj1,slj,slj1
!      complex*16 djl(l+2)
      complex(8) :: djl(l+2,jmt:iend)
      complex*16 dnl(l+2)
!c      complex*16 djl(iplmax+2,iprpts)
!c      complex*16 dnl(iplmax+2,iprpts)
      complex*16 p

      integer ized, nrel, j
      integer icor
      integer lmin

      integer, external :: gEnvVar
      integer i0

      complex*16 eoc2p1, tzoc, fz2oc2, tzoc2, em0, v0me
      complex*16 eoc2,enr,emvr,emri
      complex*16 a0, a1p, a1, a2p, a2, a3p, a3, emr, em
      complex*16 clold, slold, y1, y2, clnew, slnew
      complex*16 pnrel,pr,slmt,clmt,tl
      real(8) z0

      complex*16 y(2),dydr(3+6*2)   ! dim[dydr]>(3+6*neqn), neqn=2
      real(8) :: yin(4),dydrin(2*(3+6*2))
      equivalence (y,yin)

      complex*16 wrk1(3+size(r))
!      real*8   yDP(4)
      real(8) :: rv(size(rv_in,1))
      real*8 relerr,abserr
!DEBUG      parameter (relerr=1.d-8,abserr=1.d-7)
      parameter (relerr=1.d-8,abserr=1.d-10)
      integer iflag,iwork(5)

      real*8 rj,rj1,renorm,overflo
      integer l1,j1min
      character(2) :: c_vrel
      integer :: st_vrel

      real*8 pr0,rvj
!c      parameter (pr0 = 0.02d0)     ! pr0 << 1 (if p is large --> adaptive scheme)
      parameter (pr0 = 0.0d0)     ! pr0 << 1 (if p is large --> adaptive scheme)
!c      parameter (pr0 = 1.0d0)     ! pr0 << 1 (if p is large --> adaptive scheme)
!c
!c     ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
      integer, parameter :: icmax=5
      parameter (tole=(one/ten)**10)
      character(10), parameter :: sname='scalar'
!CAB
      real(8), parameter :: smallnumber=1.d-12
!CAB
!c     stiff ode solver
      real*8 eps
      real*8 hmin
      real*8 x1
      real*8 x2
      complex*16 ys1(2)
      complex*16 ys2(2)
      integer ierr

!      real*8 c2inv
      real*8 cent
!      real*8 power
!      complex*16 enrg
!      common/scalrelcom1/c2inv,cent,power,enrg
!      integer inout
!      common/scalrelcom2/inout

      integer ipot(2)
!      real*8 rfit
!      real*8 zfit
!      common/zfit_com/rfit(2),zfit(2),ipot(2)

      integer, external :: indRmesh

      integer j1
!c     sub-step information
      integer nss
      real*8 xss(40*size(r))
      real*8 rvss(40*size(r))
      complex*16 yss(2,40*size(r))
      complex*16 dyss(2,40*size(r))
      complex*16 errss(2,40*size(r))
      integer mapss(size(r))
!c     ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!c
      data fact/1.0,3.0,15.0,105.0/

!c
!c     ****************************************************************** !*
!c     sets up to solve the non- and scalar-relativistic equation.
!c     based on Calegero Method..............DDJ & FJP  July 1991
!c     uses Ricatti-Bessel fcts. for solution
!c     ****************************************************************** !*
!c
      if(iprint.ge.2) then
         write(6,'('' in SCALAR iprint '',i5)') iprint
         write(6,'('' scalar: l,iswzj='',2i5)') l,iswzj
         write(6,'('' scalar: nrelv,clight='',i5,d12.4)') nrelv,clight
         write(6,'('' scalar: energy='',2d16.8)') energy
         write(6,'('' scalar:   prel='',2d16.8)') prel
      end if
!c     =================================================================
!c     cannot solve for the real part of the energy equal to 0.0!!!!!!!!
      if( abs(energy) .lt. tole ) then
        write(6,'('' scalar:: energy=zero'')')
      call p_fstop(sname)
      end if

      nrel = nrelv
      if ( gEnvVar('ENV_VAL_REL',.false.)>0 ) then
        nrel = 1
        write(6,*) ' ENV_VAL_REL ==> nrel = ',nrel
      end if

!DELETE      call get_environment_variable('ENV_VAL_REL'                       &
!DELETE     &                                 ,value=c_vrel,status=st_vrel)
!DELETE      if ( st_vrel == 0 ) then
!DELETE          read(c_vrel,*) nrel
!DELETE          if ( nrel .ne. 0 ) nrel = 1
!DELETE          write(6,*) ' ENV_VAL_REL ==> nrel = ',nrel
!DELETE      end if

!c     =================================================================
!c     set atomic number................................................
!CAB      ized=-rv(1)/2 + 0.5

!      zed = -(rv(1) + (rv(1)-rv(2))*r(1)/(r(2)-r(1)))
!      ized = nint(0.5d0*zed)
      rv = rv_in
      ized = g_zed(rv,r)
      zed=ized
!c     =================================================================
!c
       factl1 = fact(min(l,3)+1)
       if(l.ge.4) then
        do j=2*4+1,2*l+1,2
         factl1 = factl1*j              ! factl1 = (2*l+1)!!
        end do
       end if

      if(ized.eq.0) then
        do j=1,iend
         if(abs(rv(j)).gt.smallnumber) go to 9
        end do
!c   To be able to compute for zero-potential
        rv = rv -smallnumber*r(1)
      end if
9     continue

      l1 = l+1
!C...............................................................
!c

!c     start with small r expansion of wave-fcts
      if(nrel.le.0) then
       cinv=one/clight
       if(ized.lt.0) then
        tzoc=czero
        fz2oc2=czero
        tzoc2=czero
       else
        tzoc=2*zed*cinv
        fz2oc2=tzoc*tzoc
        tzoc2=tzoc*cinv
       end if
       c2inv=cinv*cinv
       eoc2 = energy*c2inv
       eoc2p1 = cone+eoc2
      else
       cinv = zero
       c2inv= zero
       eoc2 = zero
       eoc2p1 = cone
       tzoc = czero
       fz2oc2=czero
       tzoc2= czero
      end if
      pnrel = prel/sqrt(eoc2p1)     ! non-rel. p

      call set_ode_params(c2inv0=c2inv)

      v0 = zero
!DEBUG      
!      if(ized.ne.0) then
!       v0=(rv(1)+2*zed)/r(1)
!      end if
!DEBUG      

      i0 = 1

!c
      v0me= v0-energy
      if(c2inv.eq.zero) then
       em0 = cone
      else
       em0= cone - v0me*c2inv
      end if

      a0=cone

!      IF ( i0>1 ) THEN
       if(nrel.le.0) then
!c        ==============================================================
!c        scalar-relativistic case......................................
         power=sqrt( l*(l+1) + 1 - dreal(fz2oc2) )
!c        a0 is arbitrary, could make Rl a spherical Bessel at origin.
!c              as for non-rel case:   R_l=x*j_l(x)= x*x**l/(2l+1)!!
!c        a0=1
!c              OR just use same expression as non-rel case, i.e.
!c        a0= prel**power/factl1
!cccc         a0= prel**l1/factl1
        if(ized.lt.0) then
         a1 = czero
         a2 = v0me/(4*power+4)
         a3 = czero
        else
         a1p=( fz2oc2 + em0*(power-1 -2*fz2oc2) )/(2*power+1)
         a1 = a0*a1p/tzoc2
         a2p=( a1p*(fz2oc2 + em0*(3*power+1 - 2*fz2oc2)) +              &
     &         em0*em0*(fz2oc2 - 2*power+2) )/(4*power+4)
         a2 = a0*a2p/(tzoc2*tzoc2)
         a3p=( a2p*( fz2oc2 + em0*(5*power+5 - 2*fz2oc2) )              &
     &        - a1p*(4*power+1 - fz2oc2)*em0*em0                        &
     &        + (3*power-3 - fz2oc2)*em0*em0*em0  )/(6*power+9)
         a3 = a0*a3p/(tzoc2*tzoc2*tzoc2)
        end if
       else
!c        ==============================================================
!c        non-relativistic case......................................
        power= l1
!c
!c        a0 is arbitrary, but this make Rl a spherical Bessel at origin.
!c                        R_l=x*j_l(x)= x*x**l/(2l+1)!!
!c        a0= prel**l1/factl1
!c        a0= prel**power/factl1
        if(ized.lt.0) then
         a1 = czero
         a2p= v0me/(4*l+6)
         a2 = a0*a2p
         a3 = czero
        else
         a1p= -2*zed/(2*l+2)
         a2p= (v0me - a1p*2*zed)/(4*l+6)
         a3p= (a1p*v0me - a2p*2*zed)/(6*l+12)
         a1 = a0*a1p
         a2 = a0*a2p
         a3 = a0*a3p
        end if
       end if
       cent = l*(l+1)
       call set_ode_params(cent0=cent,power0=power)
!c
       do j=1,i0
!c
!c        em= 1 + (energy - rv(j)/r(j))*c2inv
!c        emr= eoc2p1*r(j) - rv(j)*c2inv
!c
        rvj = rv(j)
        enr  = r(j)*energy
        emvr = c2inv*(enr-rvj)               ! (em-1)*r
        emr= r(j) + emvr                       ! r+(e*r-rv)*c2inv

!c       ==============================================================
!c       r*G = g and r*F = f, i.e. this is r*wave-fct.
        g(j)= r(j)**power*(a0 + r(j)*(a1 + a2*r(j) + a3*r2(j)) )
!c
        f(j)= r(j)**power*( a0*(power-1) +                              &
     &     r(j)*(a1*power + a2*(power+1)*r(j) + a3*(power+2)*r2(j)))
        f(j) = f(j) / emr
        if(iprint.ge.6) then
         write(6,'('' scalar: j,g,f='',i5,2(1x,2d16.8))') j,g(j),f(j)
        end if 
       end do

       if ( ized == 0 ) then
        j1min = i0
       else
        rj = max(0.5d0,cent)/zed    ! TODO: to adjust with potential behavior
        j1min = max(i0,1+indRmesh(rj,jmt-1,r))
       end if
!c
!c     =================================================================
!c     regular solution and phase-shifts of scalar relativistic eqs.
!c

!c     propagate regular solution with r^power factored out
!c     until the oscillatory region


      hmin=0.0d0
      eps=abserr
      nss=0
      j1 = i0
      ys2(1)=g(j1)
      ys2(2)=f(j1)
      call set_ode_params(in_out0=1,enrg0=energy)
      do j=i0+1,j1min
        j1=j-1
!c       pass info thru common to potential interpolation routines
        ipot(1)=j1
        ipot(2)=j
          call set_ode_params(ipot0=ipot,rfit0=r(j1:j),                 &
     &                                             zfit0=-0.5*rv(j1:j))
        x1=r(j1)
        x2=r(j)
        ys1(1)=ys2(1)
        ys1(2)=ys2(2)
        call odeint2(x1,ys1,x2,ys2,iflag,eps,hmin,                      &
     &               xss,yss,dyss,rvss,errss,nss,size(xss))
          if(iflag.ne.0) then
            write(*,*) 'odeint2 error (regular sol.)=',iflag
            if ( iflag.ne.3 ) then
            call fstop(sname//                                          &
     &         ': ODEINT2 ERROR')
            end if
          endif
!        mapss(j)=nss
        g(j)=ys2(1)
        f(j)=ys2(2)
      end do

!c     renormalize the solution 
      renorm = one/abs(g(j1min))
      g(1:j1min) = renorm*g(1:j1min)
      f(1:j1min) = renorm*f(1:j1min)
      do j=1,j1min
        renorm= (r(j)/r(j1min))**power 
        g(j)= renorm*g(j)
        f(j)= renorm*f(j)
      enddo
!c     ------------------------------------------------------------------
!c     propagate regular solution without r^power factored out
!c     out to the V=0 region
!c     ------------------------------------------------------------------

!c       minimum allowed step-size: can be zero
        hmin=0.0d0
!c       specify sub-step tolerance
        eps=abserr
!c       initialize the count of sub-steps
!c       xss,yss, etc are accumulated only for possible internal debugging purposes
        nss=0
!c       pass complex energy to subroutine thru common
      j1 = j1min
      ys2(1)=g(j1)
      ys2(2)=f(j1)
      call set_ode_params(in_out0=0,enrg0=energy)
      do j=j1min+1,jmt
        j1=j-1
!c       pass info thru common to potential interpolation routines
        ipot(1)=j1
        ipot(2)=j
          call set_ode_params(ipot0=ipot,rfit0=r(j1:j),                 &
     &                                             zfit0=-0.5*rv(j1:j))
        x1=r(j1)
        x2=r(j)
        ys1(1)=ys2(1)
        ys1(2)=ys2(2)
        call odeint2(x1,ys1,x2,ys2,iflag,eps,hmin,                      &
     &               xss,yss,dyss,rvss,errss,nss,size(xss))
          if(iflag.ne.0) then
            write(*,*) 'odeint2 error (regular sol.)=',iflag
            if ( iflag.ne.3 ) then
            call fstop(sname//                                          &
     &         ': ODEINT2 ERROR')
            end if
          endif
!        mapss(j)=nss
        g(j)=ys2(1)
        f(j)=ys2(2)
      enddo
!c     =================================================================
!c     At R, need spherical Bessel's for all l to get normalization and
!c     physical phase-shifts to determine cl and sl from
!c     g=r*R=r*( cl*jl - sl*nl ) and f=r*R'/M for all l's.
!c     Modify expression to account for difference between spherical and
!c     Ricatti-Bessel fcts.               Recall: R=g/r and dR=f*em/r
!c     =================================================================
      j=jmt
      lmin=max(2,l1)
      pr=pnrel*r(j)
!c     -----------------------------------------------------------------
      call ricbes(lmin,pr,bjl(1:lmin,j),bnl(1:lmin,j),                  &
     &                                        djl(1:lmin,j),dnl(1:lmin))
!c
!c  ==================================================================
!c  sl and cl at the sphere radius
!c
      em = eoc2p1                       ! to match free-space solution
      slmt= (pnrel*djl(l1,j)-bjl(l1,j)/r(j))*g(j) - bjl(l1,j)*f(j)*em
      clmt= (pnrel*dnl(l1)-bnl(l1,j)/r(j))*g(j) - bnl(l1,j)*f(j)*em

        if(iprint.ge.6) then
         write(6,'('' scalar: l1,pr,pnrel,em='',2i5,3(1x,2d16.8))')     &
     &                                  l1,j,pr,pnrel,em
         write(6,'('' scalar: djl,bjl='',2(1x,2d16.8))')                &
     &                                  djl(l1,j),bjl(l1,j)
         write(6,'('' scalar: dnl,bnl='',2(1x,2d16.8))')                &
     &                                  dnl(l1),bnl(l1,j)
         write(6,'('' scalar: slmt,clmt='',2(1x,2d16.8))')              &
     &                                  slmt,clmt
        end if 

      z0 = sqrt(abs(slmt**2+clmt**2))
      slmt = slmt/z0
      clmt = clmt/z0  

!      if(iswzj.ne.0) then
!       gpl(j)=r(j)*bjl(l1,j)/pr
!       fpl(j)=(djl(l1,j) - bjl(l1,j)/pr)/eoc2p1
!      end if

!c     =================================================================
!c     get normalization etc. at sphere boundary........................
!c     =================================================================

      p = pnrel

      cotdel=clmt/slmt
      tandel=slmt/clmt
!      g(jmt)=(clmt*bjl(l1,jmt)-slmt*bnl(l1,jmt))*z0/p
!      anorm=-(bjl(l1,jmt)*cotdel-bnl(l1,jmt))/g(jmt)
      anorm=-p/(z0*slmt)
!c
!c non-relat solution
!c
      do j=jmt+1,iend
       pr=p*r(j)
!c        --------------------------------------------------------------
       call ricbes(lmin,pr,bjl(1:lmin,j),bnl(1:lmin,j),                 &
     &                                        djl(1:lmin,j),dnl(1:lmin))
!c        --------------------------------------------------------------
       g(j)=(clmt*bjl(l1,j)-slmt*bnl(l1,j))*z0/p
       f(j)= ( clmt*(djl(l1,j)-bjl(l1,j)/pr)                            &
     &         -slmt*(dnl(l1)-bnl(l1,j)/pr) )*z0
!       if(iswzj.ne.0) then
!!c
!!c to start with free solution ( v(r)=0 r > R)
!!c
!        gpl(j)=bjl(l1,j)/p
!        fpl(j)=(djl(l1,j) - bjl(l1,j)/pr)/eoc2p1
!       end if

        if(iprint.ge.6) then
         write(6,'('' scalar: j,g,f='',i5,2(1x,2d16.8),'' *'')')        &
     &                                  j,g(j),f(j)
        end if 
      enddo

      ! alpha matrix = ratio between Rl=Zl*tl and sph. bes. Jl at origin
      ! expression only valid for spherical potentials
      ! for lloyd formula
      tl = 1.d0/(p*(dcmplx(0.d0,1.d0)-cotdel))
      alpha = (factl1/(p*r(1))**l)*(tl*anorm*g(1)/r(1))
!DEBUG      alpha = tl*(anorm*g(1))/(a0*r(1)**l1)

!c     =================================================================

      if( iswzj .ne. 0 ) then

!c=====================================================================
!c irregular solution
!c=====================================================================
!c
!c  start irregular solution outside m.t. sphere
!c

!c
!c starting with free solution ( v(r)=0 r > R)
!c
        p = pnrel

!      clj =-bnl(2,j)*gpl(j) - bnl(1,j)*fpl(j)/p
!      slj =-bjl(2,j)*gpl(j) - bjl(1,j)*fpl(j)/p
!      gpl(j)=clj*bjl(1,j)-slj*bnl(1,j)
!      fpl(j)=-p*(clj*bjl(2,j)-slj*bnl(2,j))

      do j=jmt,iend 
         pr = p*r(j)
         gpl(j)=bjl(l1,j)/p
         fpl(j)=(djl(l1,j) - bjl(l1,j)/pr)/eoc2p1
      end do

!c-----------------------------------------------------------------------
!c
!c  get irregular solution inside m.t. sphere
!c
        p = prel
!c       minimum allowed step-size: can be zero
        hmin=0.0d0
!c       specify sub-step tolerance
        eps=abserr
!c       initialize the count of sub-steps
!c       xss,yss, etc are accumulated only for possible internal debugging purposes
        nss=0
!c       specify outward propagation of soln with no asymptotic behaviour factored out
!c       pass complex energy to subroutine thru common
        call set_ode_params(in_out0=0,enrg0=energy)
!c     ------------------------------------------------------------------
!c     get irregular solution inside m.t. sphere
!c     but dont get too near origin because of 1/r^power divergence
!c     ------------------------------------------------------------------
        j1 = jmt
        ys2(1)=gpl(j1)
        ys2(2)=fpl(j1)
        do j=jmt-1,j1min+1,-1     !!! TODO: to fix potential overflow
          j1=j+1
          ipot(1)=j
          ipot(2)=j1
          call set_ode_params(ipot0=ipot,rfit0=r(j:j1),                 &
     &                                             zfit0=-0.5*rv(j:j1))
          x1=r(j1)
          x2=r(j)
          ys1(1)=ys2(1)
          ys1(2)=ys2(2)
          call odeint2(x1,ys1,x2,ys2,iflag,eps,hmin,                    &
     &                 xss,yss,dyss,rvss,errss,nss,size(xss))
          if(iflag.ne.0) then
            write(*,*) 'odeint2 error=',iflag
            if ( iflag.ne.3 ) then
            call fstop(sname//                                          &
     &         ': ODEINT2 ERROR')
            end if
          endif
!          mapss(j)=nss
          gpl(j)=ys2(1)
          fpl(j)=ys2(2)
        enddo

        hmin=0.0d0
        eps=abserr
        nss=0
        j1 = j1min+1
!renormalization
        renorm = r(j1)**power
        ys2(1)=gpl(j1)*renorm
        ys2(2)=fpl(j1)*renorm
        call set_ode_params(in_out0=-1,enrg0=energy)
        do j=j1min,1,-1
          j1=j+1
          ipot(1)=j
          ipot(2)=j1
          call set_ode_params(ipot0=ipot,rfit0=r(j:j1),                 &
     &                                             zfit0=-0.5*rv(j:j1))
          x1=r(j1)
          x2=r(j)
          ys1(1)=ys2(1)
          ys1(2)=ys2(2)
          call odeint2(x1,ys1,x2,ys2,iflag,eps,hmin,                    &
     &                 xss,yss,dyss,rvss,errss,nss,size(xss))
          if(iflag.ne.0) then
            write(*,*) 'odeint2 error=',iflag
            if ( iflag.ne.3 ) then
            call fstop(sname//                                          &
     &         ': ODEINT2 ERROR')
            end if
          endif
!          mapss(j)=nss
!renormalization
!c         gpl(j)=( r(j)**(-power) )*ys2(1), fpl(j)=( r(j)**(-power) )*ys2(2)
          renorm = r(j)**power

! to prevent overflow
          overflo = max( log10(abs(ys2(1))),log10(abs(ys2(2))) ) -      &
     &                                                    log10(renorm)
          if(overflo.lt.log10(huge(one)/ten)) then
            gpl(j)=ys2(1)/renorm
            fpl(j)=ys2(2)/renorm
          else
            gpl(j)=(huge(one)/ten)*(ys2(1)/abs(ys2(1)))
            fpl(j)=(huge(one)/ten)*(ys2(2)/abs(ys2(2)))
          endif
        end do

!c===============end irregular solution calculation====================
      else
!c
!c  zero out irregular solution arrays for case when iswzj=0
!c
        gpl = 0
        fpl = 0  
      end if
!c
!c     ===============================================================

      if(istop.eq.sname) then
       call p_fstop(sname)
      end if
      return
!c
      end subroutine scalar
