      subroutine getMSgf(tab,ispin,nrelv,clight,                        &
     &                  lmax,kkrsz,nbasis,numbsub,                      &
     &                  alat,aij,itype,natom,                           &
     &                  if0,mapstr,mappnt,ndimbas,mapij,                &
     &                  komp,atcon,                                     &
     &                  ij3,nj3,cgaunt,cfac,                            &
     &                  xknlat,Rksp0,nkns,                              &
     &                  qmesh,ndimq,nmesh,nqpt,                         &
     &                  lwght,lrot,                                     &
     &                  ngrp,kptgrp,kptset,ndrot,kptindx,               &
     &                  sublat,r_ref,                                   &
     &                  rws,                                            &
!     &                  vr,h,rmt,rws,jmt,jws,xr,rr,                     &
     &                  energy,                                         &
     &                  itmax,rns,nrns,ndimrs,                          &
     &                  np2r,ndimnp,numbrs,naij,                        &
     &                  ndx,rot,dop,nop,                                &
     &                  hplnm,ndimrhp,ndimlhp,                          &
     &                  green,                                          &
     &                  eneta,enlim,                                    &
     &                  conk,conr,                                      &
     &               rslatt,kslatt,Rnncut,Rsmall,mapsprs,               &
     &                  idosplot,                                       &
     &                  ksintsch,                                       &
     &                  iprint,istop)

!c     =================================================================
!c
!c     ******************************************************************
!c     this routine solves the cpa equations using the brillouin-zone
!c     integration method for calculating tau(0,0).....
!c     ...................................................gms   july 1984
!c
!c     code for gms symm. type array storage    jan'86 ......gms...
!c     code for lmax=3                          jan'86 ......gms...
!c     full t-matrix storage code               mar'92.......gms...
!c
!c     Rewritten logic of calling routines and added to GETTAU to
!c     improve ewald & real-space algorithms .... ddj & was April 1994
!c           (factor of 6 speed-up per energy and reduced storage)
!c     Found origin of symmetry violation in d.o.s., esp. w/ real-space.
!c     Hankel fct converges poorly in complex plane for system w/ basis.
!c     Ewald method now faster and converges quickly in complex plane.
!c     Both hankel and real-space ewald integral passed as DQINT.
!c     ........................................... ddj & was April 1994
!c     (next step: reduce tau storage using group theory (KTOP storage))
!c     (next step: rewrite SUMROT to do only the 3x3 matrices instead)
!c
!c     ******************************************************************
!c
!c     ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!c
      use mecca_constants
      use mecca_types, only : Sublattice
      use raymethod, only: ksrayint
      use gfncts_interface
      use mecca_interface, only : get0ata,intgrtau
      use mtrx_interface
!
      implicit none
      interface
       subroutine mcpait1(tau00,tcpa,tab,                               &
     &                  kkrsz,                                          &
     &                  nbasis,                                         &
     &                  atcon,                                          &
     &                  komp,numbsub,                                   &
     &                  iconverge,nitcpa,                               &
     &                  iprint,istop)
       use mecca_constants
       use mtrx_interface
       implicit none
       complex*16 tau00(:,:,:)
       complex*16 tcpa(:,:,:)
       complex*16 tab(:,:,:,:)
       integer kkrsz,nbasis
       real*8  atcon(:,:)
       integer komp(:),numbsub(:)
       integer iconverge,nitcpa,iprint
       character istop*10
       end subroutine mcpait1
      end interface
      integer, intent(in) :: ispin,nrelv
      real(8), intent(in) :: clight
      integer, intent(in) :: lmax,kkrsz,nbasis,numbsub(nbasis)
      real(8), intent(in) :: alat
      integer, intent(in) :: naij
      real(8), intent(in) :: aij(3,naij)
      integer, intent(in) :: natom,itype(natom)
      integer, intent(in) :: if0(48,natom)
      integer, intent(in) :: ndimbas
      integer, intent(in) :: mapstr(ndimbas,natom),mappnt(ndimbas,natom)
      integer, intent(in) :: mapij(2,naij)
      integer, intent(in) :: komp(nbasis)
      real(8), intent(in) :: atcon(:,:)       ! (ipcomp,nbasis)
      complex(8), intent(in) :: tab(kkrsz,kkrsz,size(atcon,1),nbasis)
      integer, intent(in) :: ij3(kkrsz*kkrsz*kkrsz)
      integer, intent(in) :: nj3(kkrsz*kkrsz)
      real(8), intent(in) :: cgaunt(*)
      complex(8), intent(in) :: cfac(kkrsz,kkrsz)
      real(8), intent(in) :: xknlat(*),Rksp0
      integer, intent(in) :: nmesh,nkns(nmesh)
      integer, intent(in) :: ndimq
      real(8), intent(in) :: qmesh(3,ndimq,nmesh)
      integer, intent(in) :: nqpt(nmesh)
      integer, intent(in) :: lwght(ndimq,nmesh),lrot(ndimq,nmesh)
      integer, intent(in) :: ngrp(nmesh),kptindx(ndimq,nmesh)
      integer, intent(in) :: ndrot,kptgrp(ndrot+1,nmesh)
      integer, intent(in) :: kptset(ndrot,nmesh)
      type(Sublattice), intent(in), target :: sublat(nbasis)
      real(8), intent(in) :: r_ref   ! reference (screening) radius
      real(8), intent(in) :: rws(nbasis)
      complex(8), intent(in) :: energy
      integer, intent(in) :: itmax
      integer, intent(in) :: ndimrs,nrns
      real(8), intent(in) :: rns(ndimrs,4)
      integer, intent(in) :: ndimnp
      integer, intent(in) :: np2r(ndimnp,naij),numbrs(naij)
      integer, intent(in) :: ndx(:)
      real(8), intent(in) :: rot(49,3,3)
      integer, intent(in) :: nop
      complex(8), intent(in) :: dop(kkrsz,kkrsz,nop)
      integer, intent(in) :: ndimrhp,ndimlhp
      complex(8), intent(in) :: hplnm(ndimrhp,ndimlhp,*)
!
      complex(8), intent(inout) :: green(:,:,:) ! (iprpts,ipcomp,nbasis)
!
      real(8), intent(in) :: eneta, enlim(2,nmesh-1)
      real(8), intent(in) :: conk
      complex(8), intent(in) :: conr((2*lmax+1)**2)
      real(8), intent(in) :: rslatt(3,3)           ! Alat*rslatt(1..3,*)/(2*pi) -- latt.vect.
      real(8), intent(in) :: kslatt(3,3)           ! Alat*rslatt(1..3,*)/(2*pi) -- latt.vect.
      real(8), intent(in) :: Rnncut,Rsmall
      integer, intent(in) :: mapsprs(:,:)
!
      integer, intent(in) :: idosplot
      integer, intent(in) :: ksintsch ! k-space integration scheme
                                      ! 1 = monkhorst-pack
                                      ! 2 = ray method
      integer, intent(in) :: iprint
      character(10), intent(in) :: istop
!c     ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
      character(10), parameter :: sname='gettau_ray'
!c     ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!DELETE      integer, parameter :: ipit=4               ! for tc accelerator
!c     FOR STRUCTURE CONSTANTS: use Ewald and/or real-space methods
!c              real and imaginery energy switch, see below
!c     ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!c     switch to use EWALD within "eresw" D.U. of Real E axis and
!c       real-space method beyond "eresw" D.U. of Real E axis.
!c     ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
      real(8), parameter :: eresw=-0.5d0,eimsw=0.5d0
!CAB      parameter (eresw=-1.4d0)
!CAB      parameter (eimsw=+1.4d0)
!c     ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!c
!c
!      integer      ivpoint(ipsublat)
!      integer      ikpoint(ipcomp,ipsublat)
      integer      nitcpa
!c
!      type(SphAtomDeps), pointer :: v_rho_box
!      real*8       vr(iprpts,ipcomp,ipsublat)
!      real*8       xr(iprpts,ipcomp,ipsublat)
!      real*8       rr(iprpts,ipcomp,ipsublat)
!!CALAM      real*8       xstart(ipcomp,ipsublat)
!      real*8       h(ipcomp,ipsublat)
!      real*8       rmt(ipcomp,ipsublat)
!      integer      jmt(ipcomp,ipsublat)
!      integer      jws(ipcomp,ipsublat)
!c      real*8       rns2(iprsn)
!c      real*8       rnstmp(iprsn,3)
!c      real*8       rtmp(3)
      real*8       r2tmp
!c      real*8       scale
!c
      complex*16   powe((2*lmax+1)**2)

      complex*16   dqint1(nrns,2*lmax+1)
      complex*16   dqtmp(0:2*lmax)

      complex*16   tcpatmp(size(tab,1),size(tab,1),natom)
      integer      itype1(natom),iorig(nbasis)

      complex*16   tcpa(size(tab,1),size(tab,1),nbasis)
!DELETE      complex*16   tcin(size(tab,1),size(tab,1),ipit,nbasis)
!DELETE      complex*16   tcout(size(tab,1),size(tab,1),ipit,nbasis)
      complex*16   tau00(size(tab,1),size(tab,1),nbasis)
      complex*16   green00(size(tab,1),size(tab,1),nbasis)
!DELETE      complex*16   w1(size(tab,1),size(tab,1))
!DELETE      complex*16   w2(size(tab,1),size(tab,1))
!DELETE      complex*16   w3(size(tab,1),size(tab,1))
!DELETE      complex*16   w4(size(tab,1),size(tab,1))
!DELETE      complex*16   xab(size(tab,1),size(tab,1),ipcomp)
!cab      complex*16   prel
      complex*16   pnrel
      complex*16   edu
      complex*16   pdu
      complex*16   xpr
      complex*16   d00
      complex*16   eoeta
      complex*16   lloydms

!DEBUG      complex*16, allocatable :: greenks(:,:)

!CAB TEMP
!cALAM      complex*16 taun1n2,tautmp
!cALAM      real*8 tcpamax,errcpa


      real*8 V0
      integer imethod,isparse

      integer, external :: gEnvVar

      real(8) :: dutory,rytodu,rsp
      real(8) :: time1,time2
      integer :: i,iatom,iconverge,ict,imesh,iprint1,iprint2
      integer :: iswzj,l1,n,ndkkr,nsub


      logical, save :: isdis = .false.
      logical :: do_ray_int
      logical :: do_integral
      logical :: dos_calc      ! dos only

      ! post-processing akeBZlo.dat for fermi output
      integer, parameter :: maxraysamples = 10000
      !!!

      real*8 R2ksp
      real*8 :: etamin = 0.1d0

      real*8       eta0
      integer      irecdlm,n1,n2
      integer :: st_env

      ndkkr = size(tab,1)

      if ( kkrsz > ndkkr ) then
        write(6,'(a,i3,a,i3,a,a)') ' ERROR :: kkrsz=',kkrsz             &
     &   ,' cannot be larger than ndkkr=',ndkkr,'; to change ndkkr,'    &
     &   ,' code re-compilation with a new lmax-limit is required.'
        call fstop(sname// ' :: kkrsz is too big')
      end if
      call timel(time2)

      do_ray_int = ksintsch.ne.ksint_scf_sch
      do_integral = .true.
      dos_calc = .false.

      if ( idosplot .ne. 0 ) dos_calc =.true.
      if ( idosplot==1 ) do_integral=.true.

!c     calculate things that depend only on energy
      pnrel = cdsqrt(energy)
!c
!c                Im(pnrel) must be .GE. zero
!c
      pnrel = pnrel*sign(1.d0,dimag(pnrel))

!c     energy and momentum in dimensionless units (DU).

      edu =energy*(alat/(two*pi))**2
      pdu = pnrel*(alat/(two*pi))
!c
!c????             pdu ~ pnrel or sqrt(edu) ???? (for Im(edu)<0 only)
!c

!c     switch for getting irregular part of G(E) at complex E, i.e. ZJ
      iswzj=1
!c     --------------------------------------------------------------
      if(iprint.ge.1) then
       write(6,'(a10,'':  edu,pdu='',1x,''('',f9.5,'','',f9.5,'' ),'',  &
     &                              1x,''('',f9.5,'','',f9.5,'' )'')')  &
     &                      sname,edu,pdu
      endif
!c     --------------------------------------------------------------

      if( dreal(edu).gt.eresw .and. dimag(edu) .lt. eimsw ) then
       irecdlm = 0
      else
       irecdlm = 1
      end if
      
      st_env = gEnvVar('ENV_TAU_RECDLM',.false.)
      if ( st_env>=0 ) then
          irecdlm = min(st_env,1)
      end if

      call pickmesh(edu,enlim,nmesh,imesh)
      call DefMethod(1,imethod,isparse)
      if ( imethod == 0 ) then
        V0 = zero
      else
        V0 = U0
      end if     
      if(imethod.le.zero.or.imethod.eq.4) then    ! For K-space calculations

       eta0 = max(etamin,abs(edu)/eneta)
!c                                    to be sure in accuracy of D00
       if( irecdlm.eq.0 ) then
         R2ksp = max(zero,eta0*Rksp0*Rksp0+dreal(edu))
         ! rooeml --> sqrt(e^-l)
         call rooeml(pdu,powe,lmax,iprint,istop)
         call d003(edu,eta0,d00)
         eoeta=exp( edu/eta0 ) * conk
!c
!c ==================================================================
!c  calculate the real-space integral used in Ewald technique.
!c  This is energy dependent, but k-space independent.
!c  DO once for each energy and store results in upper triangular
!c  form. INTFAC calculated the integral and returns in DQINT.
!c ==================================================================
!c
       else
!         removed for debugging
!         if(dimag(edu).lt.0.d0)
!     *   call fstop(sname//': Im(E)<0')
!c
!c ==================================================================
!c  calculate the hankel fct used in real-space calculation of
!c   structure constants out in complex plane.
!c  This is energy dependent, but k-space independent.
!c  DO once for each energy and store results in upper triangular
!c  form. HANKEL calculated the fct and returns in DQINT.
!c ==================================================================
!c
!c  do not start a hankel with zero vector length (ir=1)...blows up!
!c  only true for diagonal block, of course.
!c
       endif

!c ==================================================================
          rsp = -two
          do i=1,nrns
!c          rtmp(1)= rns(i,1)
!c          rtmp(2)= rns(i,2)
!c          rtmp(3)= rns(i,3)
           r2tmp=   rns(i,4)
           if( abs(r2tmp-rsp) .ge. 1.0d-6 ) then
             rsp = r2tmp
             if(irecdlm.eq.0) then
                call intfac(rsp,edu,eta0,2*lmax,dqtmp)
             else
                if(r2tmp.le.1.0d-6) then
                 do l1=0,2*lmax
                  dqtmp(l1) = dcmplx(zero,zero)
                 enddo
                else
                 xpr=pdu*sqrt(rsp)
                 call hankel(xpr,dqtmp,2*lmax)
                end if
             end if
             do l1=0,2*lmax
                dqint1(i,l1+1)=dqtmp(l1)
             enddo
           else
             if ( i==1 ) then
              call fstop(sname//': MEMORY BUG???')
             end if
             do l1= 1,2*lmax+1
              dqint1(i,l1)=dqint1(i-1,l1)
             enddo
           endif
          enddo

      else
         if(iprint.ge.0) then
          write(6,1002) Rsmall/(two*pi),Rnncut/(two*pi),                &
     &                  imesh,nqpt(imesh)
1002      format(' REAL SPACE *****    Rsmall=',f6.3,                   &
     &          '  Rnncut=',f6.3,' NQPT(',i1,')=',i7)
         endif
      endif

!c ==================================================================
!c

!      call gettab()

!c        write(6,*) 't-matrix at en = ', energy
!c        do i=1,kkrsz
!c          write(6,*) tab(i,i,1,1)
!c        end do
!          write(6,*) 'is = ', ispin
!          write(6,*) 'pzzbl(1,1) =', ispin
!          write(6,*) pzzbl(1:kkrsz,1:kkrsz,1,1,2)
!          write(6,*) 'pzzbl(1,2) =', ispin
!          write(6,*) pzzbl(1:kkrsz,1:kkrsz,1,2,2)
!          write(6,*) 'pzzbl(2,1) =', ispin
!          write(6,*) pzzbl(1:kkrsz,1:kkrsz,2,1,2)
!          write(6,*) 'pzzbl(2,2) =', ispin
!          write(6,*) pzzbl(1:kkrsz,1:kkrsz,2,2,2)
!
!c
!c     =================================================================
!c     construct the average t-matrix..................................
!c
      call get0ata(atcon,tab,tcpa,kkrsz,komp,nbasis,iprint,istop)
!c     -----------------------------------------------------------------

      do nsub=1,nbasis
        do iatom=1,natom
         if(itype(iatom).eq.nsub) go to 10
        end do
        call fstop(sname// ' :: You have wrong ITYPE')
10      iorig(nsub) = iatom

!c!!!
!c  it is supposed below  that "iorig(nsub)" -- number of 1st site of
!c  type "nsub" for isite=1,natom
!c!!!
      end do

      do iatom=1,natom
       itype1(iatom) = iatom
      end do

!C!----------------------------------------------------------------------
!C!  Do not change the array itype1(*) below this line; this array is used
!C!  to treat equivalent sites as inequivalent (when it is needed)
!C!----------------------------------------------------------------------

!c     =================================================================

      if(itmax.eq.-1) then          !  ATA SCF solution

       tau00 = tcpa
       iconverge = 0
       nitcpa = 0

      else                          !  CPA SCF

       do ict = 1, abs(itmax)

         nitcpa=ict
         if(ict.gt.1) then
          iprint1 = iprint-2
          iprint2 = 1000
         else
          iprint1 = iprint
          iprint2 = iprint
         end if

         if(ict.ne.1) then
          if(ngrp(1).gt.1                                               &
     &       .or.lwght(kptindx(kptgrp(1,1),1),1).gt.1                   &
     &      ) then
!c
!c  Symmetry of tcpa-matrix could be destroyed due to numerical errors,
!c  to maintain the proper symmetry for each CPA-iteration
!c
           call symmrot(ngrp(imesh),kptgrp(1,imesh),                    &
     &                  kptset(1,imesh),kptindx(1,imesh),               &
     &                  lwght(1,imesh),lrot(1,imesh),                   &
     &                  tcpa,size(tcpa,1),kkrsz,                        &
     &                  if0,dop,nbasis,                                 &
     &                  iorig)
          end if
         end if
!c
!c  Tcpa-matrices for equivalent sites can differ !!!
!c
         call tcpa2all(natom,nbasis,iorig,itype,numbsub,if0,nop,        &
     &                   kkrsz,ndkkr,dop,tcpa,tcpatmp,iprint2)

         ! is the system disordered?
         isdis = .false.
         do n = 1, nbasis
           if(komp(n)>1) isdis = .true.
         end do
         ! do not perform integral for ordered system BSF calc.
!         if( (istop /= 'AKEBZ_CALC' .and. istop /= 'FERMI_CALC' .and.   &
!     &         istop /= 'BSF_CALC') .or. isdis ) then
         if ( isdis .or. do_integral ) then
          if( ksintsch == ksint_scf_sch ) then
!          if( ksintsch == 1 .or.                                        &
!     &       (ksintsch == 3.and.lastdos > hybrid_cutoff) ) then

           ! BZ integration using special k-pt method
!           write(6,'(a,f7.3,a,f7.3,a)') '     Performing k-space '
!     >       //'integral @ en = (', real(energy),',',dimag(energy),')'
           if(iprint.ge.0.and.ict==1) then
             if( irecdlm.eq.0 ) then
              write(6,1000) eta0,sqrt(R2ksp),imesh,nqpt(imesh)
1000          format(' USING EWALD *****    ETA=',f8.5,                 &
     &          '  Rksp=',f12.5,' NQPT(',i1,')=',i7)
             else
              write(6,1001) imesh,nqpt(imesh)
1001          format(' USING HANKEL *****   NQPT(',i1,')=',i7)
             end if
           end if

           n2 = 3*sum(nkns(1:imesh))
           n1 = n2 - 3*nkns(imesh)+1
                           
           call intgrtau(                                               &
     &               lmax,kkrsz,nbasis,numbsub,atcon,size(atcon,1),komp,&
     &                  alat,aij,itype,natom,iorig,itype1,              &
     &                  if0,mapstr,mappnt,ndimbas,mapij,                &
     &                  powe,                                           &
     &                  ij3,nj3,cgaunt,cfac,                            &
     &                  edu,pdu,                                        &
     &                  irecdlm,                                        &
     &                  rns,ndimrs,                                     &
     &                  np2r,ndimnp,numbrs,naij,                        &
     &                  dqint1,size(dqint1,1),ndx,                      &
     &                  hplnm,ndimrhp,ndimlhp,                          &
     &                  tcpatmp,                                        &
     &                  tau00,                                          &
     &                  dop,nop,d00,eoeta,                              &
     &                  eta0,                                           &
     &                  reshape(xknlat(n1:n2),[nkns(imesh),3]),         &
     &                  nkns(imesh),R2ksp,conr(:),nkns(imesh),          &
     &                  qmesh(1,1,imesh),nqpt(imesh),                   &
     &               lwght(1,imesh),lrot(1,imesh),                      &
     &   ngrp(imesh),kptgrp(1,imesh),kptset(1,imesh),kptindx(1,imesh),  &
     &                  rslatt,Rnncut,Rsmall,mapsprs,                   &
     &                  V0,r_ref,                                       &
     &                  iprint1,istop)
           do_ray_int = .false.

!            write(6,*) 'itcpa = ', ict, '   ispin = ', ispin
!            write(6,*) 'tau00 = ', tau00(1:kkrsz,1:kkrsz,1)

          else

           ! BZ integration using radial wedges in spherical coordinates
!          write(6,'(a,f7.3,a,f7.3,a)') '     Performing k-space '
!     >       //'integral @ en = (', real(energy),',',dimag(energy),')'

           dutory = (two*pi)/alat
           rytodu = alat/(two*pi)

           tcpatmp = dutory * tcpatmp
             ! remember that tcpa has units of 1/sqrt(energy)
             ! so we are *really* changing to DU here

!           write(6,*) 'd00=', d00, eoeta


           ! find diagonal blocks of tau = [t^-1 - G0]^-1
           imesh = 1
           n2 = 3*sum(nkns(1:imesh))
           n1 = n2 - 3*nkns(imesh)+1
                           
           call ksrayint( lmax,kkrsz,natom,aij,mapstr(1:natom,1:natom), &
     &                               mappnt(1:natom,1:natom),           &
     &         nbasis,mapij,powe,ij3,nj3,cgaunt,cfac,edu,               &
     &         pdu,rns,ndimrs,np2r,ndimnp,numbrs,naij,dqint1,           &
     &  size(dqint1,1),ndx,hplnm,ndimrhp,ndimlhp,irecdlm,d00,eoeta,eta0,&
     &         reshape(xknlat(n1:n2),[nkns(imesh),3]),nkns(imesh),conr, &
     &                 tcpatmp,iorig,itype,kslatt,nop,rot,dop,if0,      &
     &         tau00,lloydms,green00 )

           tau00 = rytodu * tau00
           green00 = rytodu * green00
           tcpatmp = rytodu * tcpatmp
           do_ray_int = .true.

!           write(6,*) 'tau00 = ', tau00(1:kkrsz,1:kkrsz,1)

          end if

         end if

         ! if a random alloy then iterate cpa
         if(itmax.ne.1) then

           if(ict.eq.itmax) then
            iprint1 = max(1,iprint)
           else
            iprint1 = iprint
           end if

           call mcpait1(tau00,tcpa,tab,                                 &
     &                  kkrsz,                                          &
     &                  nbasis,                                         &
     &                  atcon,                                          &
     &                  komp,numbsub,                                   &
     &                  iconverge,nitcpa,                               &
     &                  iprint1,istop)

!c           -----------------------------------------------------------


         else ! no CPA case

            iconverge=0

         endif

         if(iconverge.eq.0) go to 100

       end do

      end if

100   continue

      if( iconverge.ne.0 .and. .not.dos_calc ) then
! EBOT_CALC can be done with itmax = 1

!c        CPA failed to converge.......................................

        write(6,*)
        write(6,*) ' ICONVERGE=',iconverge
        write(6,'('' gettau:: CPA failed to converge in '',i3,          &
     &          '' iterations'')') itmax
        if(float(iconverge)/float(nbasis).gt.0.5.and.itmax.gt.0) then

         write(*,1003) energy
1003     format(/' ENERGY=',2d20.12//                                   &
     &'  You have a problem with CPA convergence:'/                     &
     &'  It is very likely that you have numerical instability'/        &
     &'  (inaccuracy in symmetry+CPA --> lack of convergence).'/        &
     &'  For such cases we have never had problems with full zone',     &
     &   ' integration.'/                                               &
     &'  We do not know a good recipe to find CPA solution in',         &
     &   ' general case.'/                                              &
     &'  You can try to increase cpa-tolerance <tctol>,'/               &
     &'  (i.e. decrease accuracy of CPA) in mcpait-routine;'/           &
     &'  or to change mixing parameters in <mcpait1>). You may'/        &
     &'  want to ignore the situation (in this case you should use'/    &
     &'  negative cpa-responsible parameter in input file (absolute'/   &
     &'  value of the parameter -- max.number of iterations))')
         call fstop(sname//' :: change CPA-parameter in input file')
        else if(itmax.gt.0) then
         write(6,*)                                                     &
     &  (' You have no convergence for less then 50% of sublattices')
        end if
        write(6,*)
      else if( iconverge.ne.0 .and. itmax > 1 ) then

         write(6,'(a,i3,a)') '     Using best coherent potential in ',  &
     &          nitcpa,' iterations.'

      end if

!c     ===========================================================
!c     cpa iteration converged or the case of an ordered system...
!c     write(6,'('' gettau:: cpa converged : calling calc_gf'')')
!c     ===========================================================

      call calc_gf(pnrel)

      time1 = time2
      call timel(time2)

      if(iprint.ge.0.and.nitcpa.gt.1) then
       write(6,*) sname//' time  ='                                     &
     &           ,real(time2-time1),' NITCPA=',nitcpa
      else if(nitcpa.gt.20.and.iprint.ge.-1.and.itmax>0) then
       write(6,*) ' Number of CPA iterations is ',nitcpa
      end if

!c           -----------------------------------------------------------
      if(istop.eq.sname) then
        call fstop(sname)
      endif
!c
      return

      contains
!
      subroutine calc_gf(prel)
!c
!c     *****************************************************************
!c      input:  w/ Z regular and J irrregular sols. to Schrodinger Eq.
!c             tau00   (tauc)
!c             tcpa    (tc)
!c             tab     (t-matrix for atoms a & b)
!c             kkrsz   (size of KKR-matrix)
!c             komp    (number of components on sublattice)
!c             istop   (index of subroutine prog. stops in)
!c       output:
!c             green   (spherical Green's function)      
!c     ****************************************************************
!c
      implicit none
      character sname*10
!c parameter
      real(8), parameter :: onem=-one
!c
      complex(8) :: w1(kkrsz,kkrsz)
      complex(8), target :: w2(kkrsz,kkrsz)
      complex(8) :: w3(kkrsz,kkrsz)
      complex(8) :: w4(kkrsz,kkrsz)
!DELETE      complex(8) :: w_one(kkrsz,kkrsz),zn_l
      complex(8), intent(in) :: prel

      integer :: i,ic,nsub
      real(8) :: h
      real(8) :: xr(size(green,1))
      real(8) :: rr(size(green,1))
      real(8) :: vr(size(green,1))
      complex(8) :: tmpgreen(size(green,1))
!DELETE      complex(8) :: tmpdos(0:size(dos,1)-1)
      integer :: jmt,jws
      integer, external :: indRmesh
!c parameter
!      complex(8), parameter :: cone=(1.d0,0.d0)
!c
!c     ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
      parameter (sname='calc_gf')
!c     ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!c
      if(iprint.ge.4) then
         write(6,'('' calc_gf:: nbasis,kkrsz'',3i4)')                   &
     &                         nbasis,kkrsz
         do nsub=1,nbasis
           write(6,'('' GETDOS: tau00 '')')
           call wrtmtx(tau00(:,:,nsub),kkrsz)
         enddo
      endif
!c
!c     =================================================================
!c     -----------------------------------------------------------------
!DELETE      dos = czero
!DELETE      dosck = czero
!c
      do nsub=1,nbasis
         if(iprint.ge.4) then
            write(6,'('' calc_gf::  nsub ::'',i3)') nsub
         endif
!c         errcpa=zero
!c        =============================================================
!c        tcpa => t_{c}(old) = t_{c}...................................
!c        w4   => t_{c}^{-1} = m_c.....................................
!c        -------------------------------------------------------------
         call matinv(tcpa(1:kkrsz,1:kkrsz,nsub),w4(1:kkrsz,1:kkrsz),w3, &
     &                                             kkrsz, iprint,istop)
!c        -------------------------------------------------------------
         if(iprint.ge.4) then
            write(6,'('' calc_gf:: t_c(old) ::'')')
            call wrtmtx(tcpa(:,:,nsub),kkrsz)
            write(6,'('' calc_gf:: m_c(old) ::'')')
            call wrtmtx(w4,kkrsz)
         endif
         do ic=1,komp(nsub)
!c           ==========================================================
!c           w2 => t_a(b)^{-1} = m_a(b)................................
!c           ----------------------------------------------------------
            call matinv(tab(1:kkrsz,1:kkrsz,ic,nsub),                   &
     &                      w2(1:kkrsz,1:kkrsz),w3,kkrsz, iprint,istop)
!c           ----------------------------------------------------------
            if(iprint.ge.4) then
               write(6,'('' calc_gf::  ic ::'',i3)') ic
               write(6,'('' calc_gf:: t_a(b) ::'')')
!c              -------------------------------------------------------
               call wrtmtx(tab(:,:,ic,nsub),kkrsz)
!c              -------------------------------------------------------
               write(6,'('' calc_gf:: m_a(b) ::'')')
!c              -------------------------------------------------------
               call wrtmtx(w2,kkrsz)
!c              -------------------------------------------------------
            endif
!c           ==========================================================
!c           w1 => m_c - m_a(b)........................................
!c           ----------------------------------------------------------
            call madd(w4,onem,w2,w1,kkrsz,iprint)
!c           ----------------------------------------------------------
!c           ==========================================================
!c           w2 => tau_c*[m_c - m_a(b)]^{-1}...........................
!c           ----------------------------------------------------------
            call mmul(tau00(:,:,nsub),w1,w2,kkrsz,iprint)
!c           ----------------------------------------------------------
            if(iprint.ge.4) then
               write(6,'('' calc_gf::m_c -  m_a(b) ::'')')
!c              -------------------------------------------------------
               call wrtmtx(w1,kkrsz)
!c              -------------------------------------------------------
               write(6,'('' calc_gf::tau_c*[m_c -  m_a(b)] ::'')')
!c              -------------------------------------------------------
               call wrtmtx(w2,kkrsz)
!c              -------------------------------------------------------
            endif
!c           ==========================================================
!c           w1 => [1-tau_c*[m_c - m_a(b)]]^{-1}] = D^{-1}.............
!c           ----------------------------------------------------------
            w3 = czero
!c           ----------------------------------------------------------
            do i=1,kkrsz
               w3(i,i)=cone
            enddo
!c           ----------------------------------------------------------
            call madd(w3,onem,w2,w1,kkrsz,iprint)
!c           ----------------------------------------------------------
            if(iprint.ge.4) then
               write(6,'('' calc_gf:: D^{-1} ::'')')
!c              -------------------------------------------------------
               call wrtmtx(w1,kkrsz)
!c              -------------------------------------------------------
            endif
!c           ==========================================================
!c           w2 => D ..................................................
!c           ----------------------------------------------------------
            call matinv(w1,w2,w3,kkrsz,iprint,istop)
!c           ----------------------------------------------------------
            if(iprint.ge.4) then
               write(6,'('' calc_gf:: D ::'')')
!c              -------------------------------------------------------
               call wrtmtx(w2,kkrsz)
!c              -------------------------------------------------------
            endif
!c           ==========================================================
!c           w1 => D*tau_c = tau_a(b).................................
!c           ----------------------------------------------------------
            call mmul(w2,tau00(:,:,nsub),w1,kkrsz,                      &
     &                iprint)
!c           ----------------------------------------------------------
            call g_meshv(sublat(nsub)%compon(ic)%v_rho_box,             &
     &                                                 h,xr,rr,ispin,vr)
            jws = 1+indRmesh(rws(nsub),size(rr),rr)
            jmt = sublat(nsub)%compon(ic)%v_rho_box%jmt

!c           ==========================================================
!c           green_MS =>  ZZ*(tau_a(b)-t_a(b)) + green_SS
!c           ----------------------------------------------------------
            call madd(w1,onem,tab(:,:,ic,nsub),w2,kkrsz,iprint)   ! tau-tab
            call ms_green(tmpgreen,w2,kkrsz,                            &
     &                    nrelv,clight,                                 &
     &                    lmax,                                         &
     &                    energy,prel,                                  &
     &                    h,jmt,jws,rr,vr,                              &
     &                    iprint)
            green(:,ic,nsub) = green(:,ic,nsub) + tmpgreen(:)
         enddo
      enddo
!c     =================================================================
      if(istop.eq.sname) then
        call fstop(sname)
      endif
!c     =================================================================
      return
!
      end subroutine calc_gf
!
      subroutine ms_green(green,tau,kkrsz,                              &
     &                  nrelv,clight,                                   &
     &                  lmax,                                           &
     &                  energy,prel,                                    &
     &                  h,jmt,jws,rr,vr,                                &
     &                  iprint)
!      use mecca_constants
      use mecca_interface, only : semrelzj
      implicit none
!      
      complex(8), intent(out) :: green(:)  ! (iprpts)
      complex(8), intent(in)  :: tau(:,:)   ! (ipkkr,ipkkr)
      integer, intent(in) ::    kkrsz
      integer, intent(in) ::    nrelv
      real(8), intent(in) ::     clight
      integer, intent(in) ::    lmax
      complex(8), intent(in) :: energy
      complex(8), intent(in) :: prel
      real(8), intent(in) ::     h
      integer, intent(in) ::    jmt
      integer, intent(in) ::    jws
      real(8), intent(in) ::     vr(:)  ! (iprpts)
      real(8), intent(in) ::     rr(:)  ! (iprpts)
      integer, intent(in) ::    iprint
      character(10) :: istop='nostop'

!c
      integer    ir
      integer    i
      integer    lcur
!c
      complex*16 zlr(size(green),0:lmax)  ! (iprpts,0:iplmax)
      complex*16 jlr(size(green),0:lmax)  ! (iprpts,0:iplmax)
      integer    lindex(kkrsz)

!c     **************************************************************
!c     calculates multiple-scattering contribution to Green's function
!c     **************************************************************

!c     get regular and irregular radial soln's.......................
!c     --------------------------------------------------------------
      call semrelzj(nrelv,clight,                                       &
     &            lmax,                                                 &
     &            energy,prel,                                          &
     &            zlr,jlr,                                              &
     &            h,jmt,jws,rr,vr,                                      &
     &            0,iprint,istop)
!c     --------------------------------------------------------------
!c
      if(iprint.ge.2) then
         do lcur=0,lmax
           write(6,'('' mgreen:: l='',i3)') lcur
           write(6,'('' mgreen::ir,z*z:'',i4,2d12.4)')                  &
     &               (ir,zlr(ir,lcur)*zlr(ir,lcur),ir=1,jws,50)
         enddo
      endif
!c     ==============================================================
!c     calculate the green function for this (energy,species,sub-lat,
!c     spin) :: Spherical part only in this code.....................
!c     --------------------------------------------------------------
       i = 0
       do lcur=0,lmax
        lindex(i+1:i+1+2*lcur) = lcur
        i = i+1+2*lcur
       end do
!
      green(:) = 0 
      do i=1,kkrsz
       green(:) = green(:) + zlr(:,lindex(i))*zlr(:,lindex(i))*tau(i,i)
      enddo
!
      if(iprint.ge.2) then
         write(6,'(//)')
         write(6,'('' mgreen:: ir,rr(ir),green(ir)'',i4,3d12.4)')       &
     &            (ir,rr(ir),green(ir),ir=1,size(green),50)
      endif

      return
      end subroutine ms_green
!
      end subroutine getMSgf
