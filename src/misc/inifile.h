!c*******************************************************************
      type :: IOFile
!c*******************************************************************
       character*30 name
       character*1 status
       character*1 form
       integer nunit
      end type IOFile

!c*******************************************************************
      type :: Element
!c*******************************************************************
       integer ztot
       integer zval
       character*14 name
       character*2 symb
      end type Element
!c*******************************************************************
      type :: Species
!c*******************************************************************
       character*16 name
       real*8  concentr
       integer zID
      end type Species
!c*******************************************************************
      type :: Sublattice
!c*******************************************************************
       type ( Species ) :: compon(1:ipcomp)
       real*8 rmt
       integer ncomp
       integer ns
      end type Sublattice
!c*******************************************************************
      type :: IniFile
!c*******************************************************************

      character*80 name
      character*64 genName
      type ( IOFile ) :: io(1:32)

      integer  numIOfiles

      character*10 istop

      character*32 task   ! MP_NAME (executable name, only for parallel jobs)
      integer nproc
      integer ntask

      integer icryst

      integer imethod    ! k-space (see DefMethod for other options))
      integer sprstech

      integer iprint

      integer lmax

      integer mtasa      ! ASA (0 - MT)
      integer nspin      ! non-magn (2 - ferro, 3 - dlm)
      integer nrel       ! scalar-relat. (>14 for non-relat)

      real*8  Rrsp    !  the RS translation vectors maximum
      real*8  Rksp   !  the KS translation vectors maximum
      real*8  Rclstr  !  the RS cluster radius

      real*8  enmax  !  Ewald parameter, best value is about (efermi+delta)*(alat/2/pi)**2
      real*8  eneta  !  Ewald parameter, (between 0 and 2.8 -- limitation from d003-routine)

      integer nmesh
      integer nKxyz(1:3,1:ipmesh)
      real*8  enKlim(1:2,1:ipmesh-1)

      character*32 systemID

      real*8 alat
      real*8 ba
      real*8 ca

      integer nelements
      type (Element) :: elements(1:25) ! it is assumed that not more than
                                       ! 24 elements of the periodic table
                                       ! can be used simultaneously
      integer ncpa

      integer nsubl
      type ( Sublattice ) :: sublat(1:ipsublat)

      integer igrid
      real*8  ebot
      real*8  etop
      real*8  eibot
      integer npts
      real*8 eitop
      integer npar

      integer nscf
      real*8 alphmix
      real*8 betamix
      integer imixtype              ! charge (1 - potential)
      integer mixscheme             ! Broyden mixing (0 - simple, >1 -- Broyden+simple)

      end type IniFile
!c*******************************************************************
