!c      integer nmom        ! to calculate radial moments (-1:nmom)
!c                          ! over Voronoi polyhedron
!c      parameter (nmom=6)
!c
      integer NShC  ! Define mesh to integrate Coulomb energy over tetrahedron
!c      parameter (NShC=36)
      parameter (NShC=24)
!CDEBUG      integer ipmomylm
!CDEBUG      parameter (ipmomylm=3)  ! nmomylm is a last YY-moment calculated
      integer ipmtout
!c      parameter (ipmtout=0)  ! ipmtout is a last R-moment of rho(outside MT)
      parameter (ipmtout=4)  ! ipmtout is a last R-moment of rho(outside MT)
!c      parameter (ipmtout=6)  ! ipmtout is a last R-moment of rho(outside MT)
!c
      real*8 epsShC
      parameter (epsShC=1.d-6) ! "accuracy" of YY-moment calculations
!c                              ! if |YY-moment| < epsShC ==> YY-mom=zero
!c
      integer ipotoutmt         ! how to calculate potential outside MT-sphere
!c      parameter (ipotoutmt=0)   ! V(r) = V_h(Rasa|Rasa) + Vxc(rho(r)), r>Rasa; [and ASA-Madelung]
!c      parameter (ipotoutmt=1)  ! V(r) = V(Rasa), r>Rasa
!c      parameter (ipotoutmt=2)   ! V(r)  = ASA + shift, r<Rasa; ShC is calculated
      parameter (ipotoutmt=3)   ! V(r) = V_h(Rasa|Rasa)*Rasa/r + Vxc(rho(r)), r>Rasa; [and ASA-Madelung]
!c      parameter (ipotoutmt=4)   ! ?????V(r) = V_h(r|Rcs) + Vxc(rho(r))
