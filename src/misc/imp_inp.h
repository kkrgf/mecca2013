C==========================================================================
C   *    iprt_level  : print level [ ipr<0 gets min. o/p (i3 format) ]   *
C   *    R_max_rlsp  : Max. length of real space translation vector      *
C   *    R_max_klsp  : Max. length of k-space translation vector         *
C   *    R_small     : Radius of k-space cluster for Gref(K)             *
C   *    enmax_ewald : Parameter for Ewald method calculations           *
C   *    eneta_ewald : Parameter for Ewald method calculations           *
C   *    enlim_Rl,
C        enlim_Im    :
C           2nd K-mesh away from Ef, start at E = (enlim_Rl,enlim_Im)Ry  *  
C   *    icryst_struc: Crystal structure type
C   *    igrid2      : Specifies energy contour=2(Faster semicirc. cont) *
C   *    etop_cont   : Top of contour on      real axis                  *
C   *    eibot_cont  : Bottom of contour on      imaginary axis          *
C   *    nparallel   : number of points parallel to real-axis            *
C   *    en_smear    : Shift of E-grid into complex plane for DOS calc.  *
C==========================================================================
C   * ==============       INPUT PARAM for VP-iteg     ==================*
C==========================================================================
!  #1-->MNV ; integer*8
!        maximum number of vertices for atom
!
!  #2-->MNF ; integer*8
!        maximum number of faces for atom

!  #3-->MNqp ; integer*8
!        Maximum number of quadrature points.
!        e.g.: For volume, it takes 2 points to get converge.
!              For volume-integral it takes at least 13 points
!              to get converge usually.

!  #4-->Nf ; integer*8
!        Number of data points. Assume each atom have the same number
!        of data points, no matter the size of atom.
!  npol ; integer*8
!   The order of the polynomial, which use in polynomial fitting

!  #5-->poly_type ; integer*8
!   Type of the polynomial which use in polynomial fitting.
!   poly_type=1 --> simple polynomial
!   poly_type=2 --> Laguerre polynomial
!   poly_type=3 --> Legendre polynomial
!   poly_type=4 --> Chebyshev polynomial
!   poly_type=5 --> ?
C==========================================================================
         integer*8 iprt_level,icryst_struc,igrid2,nparallel,Nf
	 integer*8 MNV,MNF,MNqp,npol,poly_type,Nints
         real*8  R_max_rlsp,R_max_klsp,R_small,enmax_ewald,eneta_ewald
         real*8  enlim_Rl,enlim_Im,etop_cont,eibot_cont
         real*8  en_smear
         real*8  hybrid_cutoff

         parameter (iprt_level = -1)
         parameter (R_max_rlsp = 3.0d0)
         parameter (R_max_klsp = 4.2d0)
         parameter (R_small = 0.0d0)
         parameter (enmax_ewald = 0.75d0)
         parameter (eneta_ewald = 2.7d0)
         parameter (enlim_Rl = 0.0d0)
         parameter (enlim_Im = 0.35d0)
         parameter (icryst_struc = 0)
         parameter (igrid2 = 2)
         parameter (etop_cont = 0.0d0)
         parameter (eibot_cont = 0.003d0)
         parameter (nparallel = 0)

!   suffian: moved esmear --> input file
!         parameter (en_smear = 0.002d0)  
!         parameter (en_smear = 0.004d0)  
!         parameter (en_smear = 0.00001d0)

         parameter (hybrid_cutoff = 5.0d0)
C==========================================================================
C   iadjsph_ovlp .eq. 0 ==> Sphere sizes are not calculated from the
C                            Saddle point representation (SPR)
C                .ne. 0 ==> Sphere sizes are calculated from SPR 


c      integer iadjsph_ovlp
      integer irhoASA

c      parameter (iadjsph_ovlp = 1) 

      parameter (irhoASA = 0) ! irhoASA.eq.0 ==> ASA definition of rho_0
C                             ! irhoASA.ne.0 ==> MT definition of rho_0(Skriver)

      parameter (MNV=100,MNF=50,MNqp=15,Nf=1051)
      parameter (npol=80,poly_type=2,Nints=200)
