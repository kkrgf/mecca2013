!c     ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      subroutine getchg1(nspin,nbasis,rr,xr,jmt,jws,komp,omegmt,        &
     &                  greenint,greenlast,efermi,etop,                 &
     &                  rho,chgold,corden,semcor,                       &
     &                  qvalmt,qtotmt,qrms,igrid,itscf,ivar_mtz,iVP,    &
     &                  r_circ,fcount,weight,rmag,vj,rmt_true,          &
     &                  iprint,istop)
!c     ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
!c
      use mecca_constants
!C      implicit none
      implicit real*8 (a-h,o-z)
!c
!c     +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!      include 'mkkrcpa.h'
!      include 'imp_inp.h'
!c     +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!c
      character sname*10
      character istop*10
!c
      real*8 half
!c     +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
      parameter (half=one/two)
      parameter (sname='getchg')
!c     +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!c
      integer   jmt(ipcomp,ipsublat)
      integer   jws(ipcomp,ipsublat)
      integer   komp(*)
      integer   iprint
      integer   nspfac
!CALAM      integer   nume
      integer   nspin
      integer   nbasis
      integer   is
      integer   in
      integer   igrid,itscf
      integer  fcount(nbasis)

      real*8     rmt_true(ipcomp,nbasis)
      real*8     r_circ(ipcomp,nbasis)
      real*8     weight(MNqp)
      real*8     rmag(MNqp,MNqp,MNqp,MNF,nbasis)
      real*8     vj(MNqp,MNqp,MNqp,MNF,nbasis)
!c
      real*8 rho(iprpts,ipcomp,ipsublat,ipspin)
      real*8 corden(iprpts,ipcomp,ipsublat,ipspin)
      real*8 semcor(iprpts,ipcomp,ipsublat,ipspin)
      real*8 chgold(iprpts,ipcomp,ipsublat,ipspin)
      real*8 qvalmt(ipcomp,ipsublat,ipspin)
      real*8 qtotmt(ipcomp,ipsublat,ipspin)
      real*8 qrms(ipcomp,ipsublat,ipspin)
      real*8 rr(iprpts,ipcomp,ipsublat)
      real*8 xr(iprpts,ipcomp,ipsublat)
      real*8 omegmt(ipsublat)
      real*8 efermi
      real*8 etop
!c
      complex*16 greenint(iprpts,ipcomp,ipsublat,ipspin)
      complex*16 greenlast(iprpts,ipcomp,ipsublat,ipspin)
!CALAM      complex*16 egrd(ipepts)
      complex*16 delef

!c
!c     =============================================================
!c     calculate last step interval.................................
      if( igrid .eq. 1 ) then
!CAB        delef=efermi-half*(egrd(nume)+egrd(nume-1))
        delef=efermi-etop
      else
        delef = efermi - etop
      endif

!c     =============================================================
      nspfac = 3 - nspin
      do is=1,nspin
         do in=1,nbasis
!c           write(6,'('' getchg: call chgden: is,in:'',2i3)') is,in
!c     ------------------------------------------------------------------
            call chgden1(greenint(1,1,in,is),greenlast(1,1,in,is),      &
     &              delef,nspfac,is,                                    &
     &              rho(1,1,in,is),chgold(1,1,in,is),                   &
     &              corden(1,1,in,is),semcor(1,1,in,is),rr(1,1,in),     &
     &              xr(1,1,in),jmt(1,in),jws(1,in),komp(in),omegmt(in), &
     &              qvalmt(1,in,is),qtotmt(1,in,is),qrms(1,in,is),      &
     &              itscf,ivar_mtz,iVP,r_circ(1,in),fcount(in),weight,  &
     &              rmag(1,1,1,1,in),vj(1,1,1,1,in),rmt_true(1,in),     &
     &              iprint,istop)
!c     ------------------------------------------------------------------
         enddo
      enddo
!c     ------------------------------------------------------------------
      if(istop.eq.sname) then
         call fstop(sname)
      endif
!c     ------------------------------------------------------------------
      return
      end
