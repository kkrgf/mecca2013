!BOP
!!ROUTINE: zzzjxt
!!INTERFACE:
      subroutine zzzjxt(iicryst,alat,prel,                              &
     &                  ct,lmax,ktop,zzout,zjout,                       &
     &                  istop)
!!DESCRIPTION:
! calculates the integrals over the wigner-seitz
! cell exterior to the muffin-tin radius of the functions
! z(l,e)**2 and z(l,e)*j(l,e) (i.e. {\tt zzout} and {\tt zjout}); 
! the angular parts of these integrals are given by integral 
! over solid angles within cell
!
!!USES:
      use universal_const

!!DO_NOT_PRINT
      implicit none
!!DO_NOT_PRINT

!!ARGUMENTS:
      integer iicryst
      real*8  alat
      complex*16    prel
      integer lmax
      complex*16    ct(0:lmax)
      integer ktop
      complex*16    zzout(*)
      complex*16    zjout(*)
      character*10 istop
!!REVISION HISTORY:
! Initial Version - DDJ - 1996
! Adapted - A.S. - 2013
!!REMARKS:
! this implementation works only for fcc and bcc cases
! (iicryst = 1, 2, 4, 5)
!EOP
!
!BOC
      integer        lf1(8)
      integer        lf2(8)
      integer        idg(8)
!c
      real*8        ri(4,2)
      real*8        cavg(3)
      real*8        xg(6)
      real*8        wg(6)
      real*8        coef(8,3)
!c
!c
      complex*16    z(0:lmax)
      complex*16    j(0:lmax)
      complex*16    bj(2*lmax+1)
      complex*16    bn(2*lmax+1)
      complex*16    dj(2*lmax+1)
      complex*16    dn(2*lmax+1)
!c
      data xg/-.93246951d+00,-.66120939d+00,-.23861919d+00,             &
     &         .23861919d+00, .66120939d+00, .93246951d+00/
!c
      data wg/.17132449d+00,.36076157d+00,.46791393d+00,                &
     &        .46791393d+00,.36076157d+00,.17132449d+00/
!c
      data lf1/0,1,2,2,3,3,3,3/
      data lf2/0,1,2,2,3,3,3,1/
      data idg/1,3,3,2,3,3,1,3/
!c
      data ri/.3535533906d+00,.4082482905d+00,.4330127019d+00,          &
     &        .5000000000d+00,.4330127019d+00,.5000000000d+00,          &
     &        .5303300859d+00,.5590169944d+00/
!c
      data coef/1.0d+00,3.0d+00,  7.5d+00, -2.5d+00,  13.125d+00,       &
     &          -6.125d+00,  0.0d+00,  6.873863542d+00,                 &
     &          0.0d+00,0.0d+00,-22.5d+00, 22.5d+00, -39.375d+00,       &
     &          39.375d+00,  0.0d+00,-34.369317710d+00,                 &
     &          0.0d+00,0.0d+00,  0.0d+00,  0.0d+00,-236.250d+00,       &
     &          131.250d+00,105.d+00,  0.0d+00         /
!c
      character*10 sname
      data sname/'zzzjxt'/
      integer icryst,ir,ig,l,index,icavg,l1,l2
      real*8 c,d,r,rsqwpii
!     ,pi,one
!      parameter (one=1.d0)
!c
!c     ****************************************************************
!c     called by grint
!c     calls besselFun,intrst
!c
!c       input:  icryst (crystal type, 1=fcc, 2=bcc)
!c               alat   (lattice parameter)
!c               ct(l)  (phase shift cotangents)
!c               lmax   maximum orbital angular momentum
!c               ktop   maximum number of symmetry types (1,2,4,8)
!c               istop  index of subroutine in which pgrm will stop
!c
!c       output: zzout  (contributions to z*z integrals from outside mt)
!c               zjout  (contributions to z*j integrals from outside mt)
!c
!c       this subroutine calculates the integrals over the wigner-seitz
!c       cell exterior to the muffin-tin radius of the functions
!c       z(l,e)**2 and z(l,e)*j(l,e).  the angular parts of these
!c       integrals i(lambda,lambda',r) are given by:
!c       i(lambda,lambda',r)=integral over solid angles within cell
!c       of sum over mu of k(lambda,mu)*k(lambda',mu).                  ( !?)
!c
!c
!c       index   lambda  lambda'  l   l'         i
!c         1       1       1      0   0         <1>
!c         2       2       2      1   1        3<1>/3
!c         3       3       3      2   2    (15/2)[<1>-3<z4>]/3
!c         4       4       4      2   2    (15/6)[9<z4>-<1>]/2
!c         5       5       5      3   3  (105/8)[<1>-3<z4>-18<x2y2z2>]/3
!c         6       6       6      3   3 (7/24)[450<x2y2z2>+135<z4>-21<1>] !/3
!c         7       7       7      3   3       105<x2y2z2>/1
!c         8       2       6      1   3   (sqrt(21)/4)[3<1>-15<z4>]/3
!c
!c     the averages <1>, <z4>, and <x2y2z2> depend on r and are calculate !d
!c     by subroutine intrst.  the coeffecients of the above table are sto !red
!c     in array coef.
!c     ****************************************************************
!c

!      pi = 4*atan(one)
      if(lmax.gt.3) then
       write(*,*) sname//': *** WARNING ***'
       write(*,*) ' This subroutine was not tested for LMAX > 3'
      end if
      icryst=iicryst
      if( iicryst .eq. 4 ) then
         icryst=2
      elseif( iicryst .eq. 5 ) then
         icryst=1
      endif
!c     set zzout and zjout arrays to zero
!c     ----------------------------------------------------------------
      call zerooutC(zzout,ktop)
!c     ----------------------------------------------------------------
      call zerooutC(zjout,ktop)
!c     ----------------------------------------------------------------
!c     this needs fixing in scalar relativistic calculation............
!c     break integral up into three regions
      do ir=1,3
         c= 0.5d+00*alat*(ri(ir+1,icryst)-ri(ir,icryst))
         d=-0.5d+00*alat*(ri(ir+1,icryst)+ri(ir,icryst))
!c        6-point gaussian integration over each region
         do ig=1,6
            r=c*xg(ig)-d
            rsqwpii=c*wg(ig)*r**2/pi
!c           ----------------------------------------------------------
            call besselFun(lmax+1,prel*r,bj,bn,dj,dn)
!c           ----------------------------------------------------------
!c           calculate wave functions
            do l=0,lmax
               z(l)=prel*(ct(l)*bj(l+1)-bn(l+1))
               j(l)=bj(l+1)
            enddo
!c           ----------------------------------------------------------
            call intrst(icryst,r/alat,cavg)
!c           ----------------------------------------------------------
!c           loop over  irr. representations
            do index=1,ktop
               l1=lf1(index)
               l2=lf2(index)
!c              loop over cellular solid angle averages
               do icavg=1,3
                  zzout(index)=zzout(index)-rsqwpii*cavg(icavg)*        &
     &                         z(l1)*z(l2)*coef(index,icavg)/idg(index)
                  zjout(index)=zjout(index)-rsqwpii*cavg(icavg)*        &
     &                         z(l1)*j(l1)*coef(index,icavg)/idg(index)
               enddo
            enddo
         enddo
      enddo
!c
!c     ================================================================
      if (istop.eq.sname) then
         call fstop(sname)
      else
         return
      endif
!c
      end subroutine zzzjxt
