 !#ifort -g -O0 -check bounds,pointers atom_module.f atom_hsk_module.f atom_main.f -module src/atom_dft src/atom_dft/libdftatom.a -o atom.exe
!#mpifort -g -O3 -check bounds,pointers -I finclude_mpifort/ atom_main.f -Llib_mpifort -lMECCA -ldftatom -lVP -L${LIBXC}/lib -lxc -lxcf90 -mkl -o atom.exe
!#ifort -g -check bounds,pointers -I finclude_idb/ atom_main.f -Llib_idb -lMECCA -ldftatom -lVP -L${LIBXC}/lib -lxc -lxcf90 -mkl -o atom.exe
      program atom_main
      use ATOM, only: atom_rdata,get_mesh_exp_params
      use ATOM, only: calcAtom,get_atom,set_atom_procedure
      use ATOM, only: dealloc_atoms
      use mecca_types, only : RunState,IniFile
      use mecca_run, only : setMS
      implicit none
      real(8), pointer :: r(:),rp(:)
      real(8), pointer :: rv(:),rho(:)
      integer :: ized
      character(20) filenm
      character(9) :: ptntag='-atom.ptn'
!............................................................
      integer :: i,j,N
      real(8) :: Etot
      type( atom_rdata ), target :: atomic
!      integer, parameter :: nk=1
      integer, parameter :: nk=4
      real(4) :: cputm(0:nk),sec1,sec2,cputm1(0:nk)
      real(8) :: r_min,r_max,a
      type(RunState) :: M_S
      type(IniFile), target  :: inFile

      M_S%intfile => inFile
      M_S%intfile%nspin = 1
      call setMS(M_S)
!
      cputm = 0
      do i=0,size(cputm)-1

!       if ( i==2 .or. i==4 ) cycle
!       if ( i .ne. 1 ) cycle

       call cpu_time(sec1)
       if ( i==0 ) then
        call set_atom_procedure(0)
       else if ( i==1 ) then
!         M_S%intfile%iXC = 259000
!         M_S%intfile%iXC = 116133
         M_S%intfile%iXC = 101130
!         M_S%intfile%iXC = 001007
        call set_atom_procedure(M_S%intfile%iXC)
       else if ( i==2 ) then
         M_S%intfile%iXC = 001004
        call set_atom_procedure(M_S%intfile%iXC)
       else if ( i==3 ) then

           CYCLE

         M_S%intfile%iXC = 001007
        call set_atom_procedure(M_S%intfile%iXC)
       else if ( i==4 ) then
         M_S%intfile%iXC = 532007
        call set_atom_procedure(M_S%intfile%iXC)
       else
        call set_atom_procedure(i)
       end if
       DO ized= 1,102,1
!        if ( ized.ne.92 ) cycle
!        if ( ized.ne.3 ) cycle
!        if ( ized.ne.5 ) cycle
        if ( ized.ne.5 .and. ized.ne.92 ) cycle
!        if ( ized.ne.1 .and. ized.ne.5 .and. ized.ne.92 ) cycle
        call calcAtom(ized)
        write(6,*)
       END DO
       call cpu_time(sec2)
       cputm(i) = sec2-sec1

       call cpu_time(sec1)
       DO ized= 1,102
!        if ( ized.ne.92 ) cycle
!        if ( ized.ne.3 ) cycle
!        if ( ized.ne.5 ) cycle
        if ( ized.ne.5 .and. ized.ne.92 ) cycle
!        if ( ized.ne.1 .and. ized.ne.5 .and. ized.ne.92 ) cycle
        call get_atom(ized,atomic,6)
        write(6,*)

!DELETE        if ( .not.associated(atomic) ) then
!DELETE             write(*,*) ' I = ',i
!DELETE             write(*,*) ' ized = ',ized
!DELETE        else if ( atomic%iZ .ne. ized ) then
        if ( atomic%iZ .ne. ized ) then
             write(*,*) ' I = ',i
             write(*,*) ' ized = ',ized
             write(*,*) ' atomic%ized = ',atomic%iZ
             write(*,*) ' atomic%mx = ',atomic%mx
             write(*,*) ' atomic%mkind = ',atomic%mkind
!             stop 'ERROR?'
        else
          if ( ized<10 ) then
           write(filenm,'(i1,a,i1)') atomic%iZ,ptntag,i
          else if ( ized>=100) then
           write(filenm,'(i3,a,i1)') atomic%iZ,ptntag,i
          else
           write(filenm,'(i2,a,i1)') atomic%iZ,ptntag,i
          end if
          r => atomic%r
          rp => atomic%rp
          rho => atomic%rho
          rv => atomic%rv
          Etot = atomic%E_tot
          open(11,file=trim(filenm))
          write(11,'(''#'',g21.14,a)') Etot,' Etot (Ry)'
          call get_mesh_exp_params(r, r_min, r_max, a, N)
          write(11,'(''#'',3es15.6,i6,a)') r_min,r_max,a,N,             &
     &                                                ' exp-mesh params'
          do j=1,atomic%mx
           write(11,'(i5,1x,3(2x,es21.14))') j,r(j),rho(j),rv(j)
          end do
          write(11,*)
          do j=atomic%mx+1,size(r)
           write(11,'(i5,1x,3(2x,es21.14))') j,r(j),rho(j),rv(j)
          end do
          close(11)
        end if
       END DO
       call cpu_time(sec2)
       cputm1(i) = sec2-sec1
       call dealloc_atoms()
      end do
      write(6,'(87(''-'')//''compute cpu time:'',5(2x,f8.2))') cputm
      write(6,'(''write cpu time:'',5(2x,f8.2))') cputm1

      stop
      end program

      subroutine getDfltPot(zed,r,rv,nkind)

!!DO_NOT_PRINT
      implicit none
!!DO_NOT_PRINT

!!ARGUMENTS:
      real(8), intent(in) :: zed
      real(8), intent(in) :: r(:)
      real(8), intent(out) :: rv(:)  ! r*potential
      real(8), intent(in)  :: nkind
!!REVISION HISTORY:
! Initial Version - A.S. - 2013
!EOP
!
!BOC
      real(8), parameter :: fact=1.0d+3
      real(8), parameter :: pi=4.d0*atan(1.d0),one=1.d0,two=2.d0
      real(8), parameter :: rf=4.d0/(3.d0*pi),oneth=1.d0/3.d0
      integer :: i,nmx
      real(8) fac,rr,root,rvozin,scl
      nmx = size(r)
!c =====================================================================
      if(zed.le.10.0d0)then
       fac=fact
      else
       fac=1.0d0
      endif

      scl=fac*(zed**oneth)/0.88534138d0
!c
!c =====================================================================
      do i=1,nmx
        rr = r(i)*scl
        root = sqrt(rr)
        rvozin=one            + 0.02747d0 *root                         &
     &      + 1.243d0  *rr    - 0.14860d0 *rr*root                      &
     &      + .2302d0  *rr*rr + 0.007298d0*rr*rr*root                   &
     &      + .006944d0*rr*rr*rr
        rv(i) = -two*zed/rvozin
      end do
!c =====================================================================
      return
!EOC
      end subroutine getDfltPot

