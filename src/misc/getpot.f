!c     cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      subroutine getpot_ovlp(nspin,nbasis,lattice,efpot,ivpoint,ikpoint,&
     &                komp,jmt,jws,rmt_ovlp,rmt,rws,r_circ,iadjsph_ovlp,&
     &                 xr,rr,h,                                         &
     &                 xstart,vr,vdif,fcount,weight,rmag,vj,iVP,        &
     &                 rhotot,corden,xvalsws,ztot,zcor,zvals,zcors,     &
     &                 atcon,numc,nc,lc,kc,ec,                          &
     &                 natom,itype,rslatt,pos,volume,numbsub,ebot,      &
     &                 ibot_cont,ipot_read,iset_b_cont,header,mtasa,    &
     &                 isDLM,isflipped,flippoint,                       &
     &                 iprint,istop)
!c     ==================================================================
      implicit real*8 (a-h,o-z)
!C      implicit double precision(a-h,o-z)
!c     ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
      include 'mkkrcpa.h'
      include 'imp_inp.h'
!c     ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
      character  coerr*80
      character  sname*10
!c     ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
      parameter (sname='getpot_ovl')
      parameter (coerr='all right')
!c     ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
      character  istop*10
      character  header*80
!c
      integer   jmt(ipcomp,ipsublat)
      integer   jws(ipcomp,ipsublat)
      integer   komp(ipsublat)
      integer   ivpoint(ipsublat)
      integer   ikpoint(ipcomp,ipsublat)
      integer   numc(ipcomp,ipsublat,ipspin)
      integer   nc(ipeval,ipcomp,ipsublat,ipspin)
      integer   lc(ipeval,ipcomp,ipsublat,ipspin)
      integer   kc(ipeval,ipcomp,ipsublat,ipspin)
      integer   itype(ipbase),numbsub(ipsublat)
      integer   nspinp,nspin
      integer   ibot_cont,ipot_read,iset_b_cont,iempty_sph
      integer   num,niter
      integer   natin,nbasin,ityp1(natom)
!C      integer   natom,nbasis,ityp1(natom)
!c
      real*8    rmt(ipcomp,ipsublat)
      real*8    rws(ipcomp,ipsublat)
      real*8    rmt_ovlp(ipcomp,ipsublat)
      real*8    r_circ(ipcomp,ipsublat)
      real*8    rASA(ipcomp,ipsublat)
      real*8    X(ipcomp,ipsublat)
      real*8    ztot(ipcomp,ipsublat)
      real*8    zcor(ipcomp,ipsublat)
      real*8    zvals(ipcomp,ipsublat)
      real*8    zcors(ipcomp,ipsublat)
      real*8    xmom(ipcomp,ipsublat)
      real*8    xr(iprpts,ipcomp,ipsublat)
      real*8    rr(iprpts,ipcomp,ipsublat)
      real*8    h(ipcomp,ipsublat)
      real*8    xstart(ipcomp,ipsublat)
      real*8    atcon(ipcomp,ipsublat)
      real*8    vr(iprpts,ipcomp,ipsublat,ipspin)
      real*8    xvalsws(ipcomp,ipsublat,ipspin)
      real*8    rhotot(iprpts,ipcomp,ipsublat,ipspin)
      real*8    corden(iprpts,ipcomp,ipsublat,ipspin)
      real*8    ec(ipeval,ipcomp,ipsublat,ipspin)
      real*8    rslatt(3,3)          ! In units of Alat/(2*pi)
      real*8    pos(3,ipbase)
      real*8    transV(3,3)
      real*8    basisV(3,natom)
      real*8    vdif,vdif0,alp,alat,volume,ebot

!CAA   Reqd. for isoparametric Integ. (VORONOI POLYHEDRA)
      integer    fcount(nbasis)
      real*8    lattice(3)
      real*8    rmag(MNqp,MNqp,MNqp,MNF,nbasis),weight(MNqp)
      real*8    vj(MNqp,MNqp,MNqp,MNF,nbasis)
!CAA
      parameter (vdif0=0.025d0,maxit=5,niter=50,eps=1.0d-07)
      parameter (alp = 0.1d0) ! mixing param for initial V and rho only
!C====================================================================
!C     Reqd. for r-grids [other than log-grid(xr,rr,h)] used for
!C           the initial construction of atomic V and rho
!C                               +
!C         Herman-Skillman Grid [used by J.M.Maclaren] for
!C              overlapping atomic charge density
!C====================================================================
      integer  nhs,ndoub,ichgm,i,n,iat,ishell,ir,ir1,ityp,ntyp,ipa,ima
      integer  nk,nsub,nki,nkn,imin,icdim,nn
      real*8   fac,a,pref,h0,rpa,rma,contrb,contrb1,hh

      logical :: isflipped(ipsublat)
      integer :: flippoint(ipsublat)


      parameter(nhs=1001,icdim=50,nn=20)           ! nhs <= 1001
      integer   ichg(0:icdim,ipcomp,ipsublat),imax(ipcomp,ipsublat)

      real*8    rsmall(2*nn+10),rhosmall(2*nn+10),Z(2*nn+10)
      real*8    req(21*nn+10),rhoeq(21*nn+10)

      real*8    rhsk(iprpts,ipcomp,ipsublat)
      real*8    rhohsk(iprpts,ipspin,ipcomp,ipsublat)
      real*8    rhohsk0(nhs,ipspin,ipcomp,ipsublat)
      real*8    rho_ovlp_rad(nhs),rhokeep(nhs)
      real*8    rhoint(nhs,ipspin,ipcomp,ipsublat)
      real*8    rchg(0:icdim,ipcomp,ipsublat),rmax(ipcomp,ipsublat)
!C                                   !rho on herman skilman grid
      real*8    r_min,rmt_min,rmtmin,rho_min,diff,diffmin,rmin,aNN
      real*8    rsum,rlow,rup,rf,a1,a2,a3,a4,f1,f2,fcube

      logical :: isDLM(ipsublat)
!C==============================================================
!C     Reqd. for shell information of each atoms in the unit cell
!C==============================================================
      integer   nshell
      parameter (nshell=8,small=0.25d0)
      integer   ntypshl(nshell,natom),natshl(nshell,natom),             &
     &          numshell(natom)
      real*8    radshl(nshell,natom)

      ! warning: this is also defined in 'neigh_shell'
      integer, parameter :: max_shell_atoms = 60
      real*8 :: atvshl(3,max_shell_atoms,nshell,natom)
      real*8 :: vec(3)

!C==============================================================
!C     A convinient choice for the step-size of the grid
!C==============================================================
      call step_size(ztot,komp,nbasis,nhs,fac,ndoub,ichgm)
!C==============================================================
      ipot_read = 1     ! = 1 if POT-FILE does'nt exist
!c                       ! = 0 if POT-FILE exist (To Read)
      iset_b_cont = 1     ! = 1 if POT-FILE does'nt exist
!c                       ! = 0 if POT-FILE exist (To Read)
!C==============================================================

      header = ' nspin  vdif :::::::::'
      nspinp = 1
      vdif   = zero
      alat=lattice(1)
!CALAM ***************************************************************
!c     Generate an starting potential and charge density for various
!c                       atoms in the unit cell
!c     The Muffin Tin potential for a given Bulk solid has been
!c     calculated by overlapping these atomic charge densities to
!c               give an approximate bulk charge density.
!CALAM ***************************************************************


      call zeroout(rhohsk,iprpts*ipspin*ipcomp*ipsublat)
!c     ---------------------------------------------------------------
      call zeroout(xmom,ipcomp*ipsublat)
!c     ---------------------------------------------------------------

      do nsub=1,nbasis
        do nk=1,komp(nsub)
         if(ztot(nk,nsub).gt.0.0d0)then
           call get_rho_hsk(0,ztot(nk,nsub),xmom(nk,nsub),nspinp,maxit, &
     &                   alp,rhsk(1,nk,nsub),rchg(0,nk,nsub),           &
     &                   ichg(0,nk,nsub),fac,nhs,ndoub,ichgm,           &
     &                   rhohsk(1,1,nk,nsub),istop)
         endif
        enddo
      enddo


!CALAM ***************************************************************
!c       Overlapp the atomic charge densities to get an approximate
!c                       bulk charge density
!c       J.M. Maclaren's idea of overlapping at. charge densities has
!c                           been used
!C====================================================================
!C     Generate a herman-skillman radial grid as generated by
!C     J.M. Maclaren and interpolate the average potential and
!C                  charge density on that grid
!CALAM ***************************************************************
      do ns=1,nspinp
        do nsub=1,nbasis
         do nk=1,komp(nsub)
          call herm_skill(fac,nhs,ndoub,ichgm,rhsk(1,nk,nsub),          &
     &           ichg(0,nk,nsub),rchg(0,nk,nsub),ztot(nk,nsub),         &
     &           rhohsk(1,ns,nk,nsub),                                  &
     &           rmax(nk,nsub),imax(nk,nsub),alat,iprint,istop)
         enddo
        enddo
      enddo


!C *******   Optional Write statement **************************
!C       do ir=2,nhs
!C         write(71,103)((rhsk(ir,nk,nsub),rhohsk(ir,1,nk,nsub)/
!C     <                  (4.0d0*pi*rhsk(ir,nk,nsub)**2.0d0),
!C     <                   nk=1,komp(nsub)),nsub=1,nbasis)
!C       enddo
!C103    format(1x,8F15.9)

!CALAM ***************************************************************
!C      Generate the neighbouring shell data for each atom in the
!C                           unit cell
!CALAM ***************************************************************
       call neigh_shell(alat,itype,natom,komp,rslatt,                   &
     &                  pos,rmax,nshell,ntypshl,natshl,radshl,          &
     &                  atvshl,numshell,iprint,istop)
!C     ===============================================================
!C     Writing Shell info for each atom in the unit cell (Optional WRITE)
!C     ===============================================================
       if ( iprint >= 0 ) then
        do i1=1,natom
        write(69,*)'Shell info for Atom = ',i1,' type ',itype(i1)
          do i2=1,nshell
            write(69,122)i1,i2,ntypshl(i2,i1),natshl(i2,i1),            &
     &                   radshl(i2,i1),numshell(i1)
122         format(2I5,1x,2I6,4x,F14.6,2x,I5)
!c            do i3=1,natshl(i2,i1)
!c              write(69,'(3f15.5)') atvshl(:,i3,i2,i1)
!c            end do
            write(69,*)
          enddo
        write(69,*)'================================================='
        enddo
        call flush(69)
       end if
!C*********************************************************************
      call zeroout(rhoint,nhs*ipspin*ipcomp*ipsublat)
!c---------------------------------------------------------------------
      do ns=1,nspinp
        do nsub=1,nbasis
         do nk=1,komp(nsub)
           if(ztot(nk,nsub).gt.0.0d0)then
!c---------------------------------------------------------------------
             rhohsk0(1,ns,nk,nsub)=0.0d0

             do ir=2,nhs
              rhohsk0(ir,ns,nk,nsub)=rhohsk(ir,ns,nk,nsub)/(4.0d0*pi*   &
     &                               rhsk(ir,nk,nsub))  ! r*rho(r)
             enddo

!c-- rhoint = integral of r*rho(r)*dr for each atomic species and spin
             call integr(rhohsk0(1,ns,nk,nsub),rhsk(1,nk,nsub),         &
     &                 imax(nk,nsub)+3,ichg(0,nk,nsub),                 &
     &                 rhoint(1,ns,nk,nsub),ichgm,1,istop)
             rhoint(1,ns,nk,nsub)=0.0d0
!c---------------------------------------------------------------------
           endif
          enddo
        enddo
      enddo


!CALAM ***************************************************************
!C      Overlapp the atomic charge densities to find an approximate
!C                           Bulk charge density
!CALAM ***************************************************************
       do iat=1,natom
        ityp=itype(iat)
        do nki=1,komp(ityp)
            num=0
!C***************** For the case of Arithmetic Mean  *****************
            rmt_ovlp(nki,ityp) =0.0d0
!C******************For the case of Geometric Mean   *****************
!C            rmt_ovlp(nki,ityp) =1.0d0
!C********************************************************************
!C------    Loop over shells
!C
           rmtmin=1.0d+20
           do ishell=2,numshell(iat)
            r_min=0.0d0
            rmt_min=0.0d0
             ntyp=ntypshl(ishell,iat)
             a=radshl(ishell,iat)
             pref=2.0d0*pi*float(natshl(ishell,iat))/a

             aNN = abs(radshl(ishell,iat)-radshl(2,iat))    ! Ovlp with NN only
               if(ztot(nki,ityp).gt.1.d-03.and.aNN.lt.1.d-03)then
                    rhokeep(1) = 0.0d0
                    rho_ovlp_rad(1) = 0.0d0
                  do ir=2,nhs
                    rhokeep(ir) = rhohsk0(ir,1,nki,ityp)/               &
     &                                 rhsk(ir,nki,ityp)
                    rho_ovlp_rad(ir) = rhokeep(ir)
                  enddo
               endif

!CALAM================================================================
!C    Contribution of overlapping atomic charge density to the
!C             quantity  rhohsk =  4*pi*r*r*rho(r)   is
!C           (2*pi*n/a)*r*[rhoint(r+a)-rhoint(abs(r-a))]
!C    For estimating the MT-radii from the saddle points we calculate
!C    the contribution of overlapping atomic charge densities from
!C    the nearest neighbor atoms only and this time the contribution
!C    is for rho(r) and not for 4*pi*r*r*rho(r) :
!C    The contribution of overlapping atomic charge density to rho(r) is
!C           (12*pi/a*r)*[rhoint(r+a)-rhoint(abs(r-a))]
!CALAM================================================================
             do nkn=1,komp(ntyp)
              az=ztot(nkn,ntyp)
!C====================================================================
                diffmin=1.0d+20
!C====================================================================
                h0=rhsk(2,nkn,ntyp)
                do ir=1,imax(nki,ityp)
                 rpa=min((rhsk(ir,nki,ityp)+a),rmax(nkn,ntyp))
                 rma=min(abs(rhsk(ir,nki,ityp)-a),rmax(nkn,ntyp))
                 ipa=invgrd(rpa,h0,ntyp,rmax(nkn,ntyp),imax(nkn,ntyp),  &
     &                  ichgm,rchg(0,nkn,ntyp),ichg(0,nkn,ntyp))
                 ima=invgrd(rma,h0,ntyp,rmax(nkn,ntyp),imax(nkn,ntyp),  &
     &                  ichgm,rchg(0,nkn,ntyp),ichg(0,nkn,ntyp))

                    do ns=1,nspinp
                     contrb=rhsk(ir,nki,ityp)*(rhoint(ipa,ns,nkn,ntyp)- &
     &                       rhoint(ima,ns,nkn,ntyp))
                     rhohsk(ir,ns,nki,ityp)=rhohsk(ir,ns,nki,ityp) +    &
     &                                    atcon(nkn,ntyp)*pref*contrb
!C**********************************************************************
!c SUFF -- go ahead and do this for all shells now
                    if(ztot(nki,ityp).gt.1.d-03.and.ztot(nkn,ntyp)      &
!c     .                  gt.1.d-03.and.aNN.lt.1.d-03)then
     &                  .gt.1.d-03)then

!c SUFF -- missing division for sum of terms?? r=0 not important anyway
!C**  Contribution of the ovlping charge density to rho^{onsite} at r=0
                      if(ir.eq.1)then
                        do ir1=1,imax(nki,ityp)
                           diff=abs(a-rhsk(ir1,nki,ityp))
                           diffmin=min(diffmin,diff)
                        enddo
                        do ir1=1,imax(nki,ityp)
                         if(abs(diffmin-abs(a-rhsk(ir1,nki,ityp)))      &
     &                      .lt.1.d-04)then
                          rho_ovlp_rad(1)=rho_ovlp_rad(1) +             &
     &                                8.0d0*pi*atcon(nkn,ntyp)*         &
     &                                rhohsk0(ir1,1,nkn,ntyp)/          &
     &                                   rhsk(ir1,nki,ityp)
                         endif
                        enddo
                      else
!C*********************************************************************

!c SUFF -- old code does L=0 term using one atom from shell

!c                      contrb1=(6.0d0/(dfloat(natshl(ishell,iat))*
!c     .                     rhsk(ir,nki,ityp)))*(rhoint(ipa,ns,nkn,ntyp) !-
!c     .                     rhoint(ima,ns,nkn,ntyp))

!c                      rho_ovlp_rad(ir) = rho_ovlp_rad(ir) +
!c     .                                   atcon(nkn,ntyp)*pref*contrb1

!c SUFF -- addon: now perform direct sum over all atoms in shell

                       contrb1 = 0.d0
                       do i3 = 1,natshl(ishell,iat)

                         vec(:) =                                       &
     &                     rhsk(ir,nki,ityp)/a*atvshl(:,1,ishell,iat) - &
     &                     atvshl(:,i3,ishell,iat)

                         rma=min(rmod(vec(1),vec(2),vec(3)),            &
     &                     rmax(nkn,ntyp))
                         ima=invgrd(rma,h0,ntyp,rmax(nkn,ntyp),         &
     &                     imax(nkn,ntyp),ichgm,rchg(0,nkn,ntyp),       &
     &                     ichg(0,nkn,ntyp))

                         contrb2 = ylag(rma,rhsk(1,nkn,ntyp),           &
     &                     rhohsk0(1,ns,nkn,ntyp),0,6,imax(nkn,ntyp),   &
     &                     iex)/rma

                         if(ima /= 0) then
                           contrb1=contrb1 + contrb2
                         end if
                       end do

                       rho_ovlp_rad(ir) = rho_ovlp_rad(ir) +            &
     &                   atcon(nkn,ntyp)*contrb1

                      endif
                    endif
                    enddo         ! End of spin loop
                enddo             ! End of ir loop
             enddo                ! End of nkn loop


!c SUFF -- print out original & overlapped charge density

!c             do ir = 2,imax(nki,ityp)
!c                write(101,*) rhsk(ir,nki,ityp),
!c     .             (rhohsk0(ir,1,nki,ityp)+rhohsk0(ir,2,nki,ityp))/
!c     .             rhsk(ir,nki,ityp),rho_ovlp_rad(ir)
!c
!c             end do
!c             write(101,*)

!C======================================================================
!C    To estimate the MT-radii for all the real atoms in the unit cell.
!C    The MT-radii of the empty spheres will be calculated afterwards
!C    just by looking at the neighboring environment (no Sadlle pt. rep.)
!C======================================================================
              if(ztot(nki,ityp).gt.1.d-03.and.ztot(1,ntyp)              &
     &              .gt.1.d-03.and.aNN.lt.1.d-03)then
                 rho_min=1.0d+20
               do ir=1,imax(nki,ityp)
!c                  if(rhsk(ir,nki,ityp).ge.(rmt(nki,ityp)-1.0d0).and.
!c     <               rhsk(ir,nki,ityp).le.(rmt(nki,ityp)+1.0d0))then
                if(rhsk(ir,nki,ityp).ge.(radshl(2,iat)/2.0d0-1.0d0).and.&
     &            rhsk(ir,nki,ityp).le.(radshl(2,iat)/2.0d0+1.0d0))then
                     rho_min=min(rho_min,rho_ovlp_rad(ir))
                endif
               enddo

               do ir=1,imax(nki,ityp)
!c                  if(rhsk(ir,nki,ityp).ge.(rmt(nki,ityp)-1.0d0).and.
!c     <               rhsk(ir,nki,ityp).le.(rmt(nki,ityp)+1.0d0))then
                if(rhsk(ir,nki,ityp).ge.(radshl(2,iat)/2.0d0-1.0d0).and.&
     &            rhsk(ir,nki,ityp).le.(radshl(2,iat)/2.0d0+1.0d0))then
                    if(abs(rho_ovlp_rad(ir)-rho_min).lt.1.0d-14)then
                       imin=ir
                       r_min = rhsk(ir,nki,ityp)
                    endif
                endif
!C *******   Optional Write statement **************************
!C                  write(72,112)rhsk(ir,nki,ityp),rho_ovlp_rad(ir),
!C     <                             (rho_ovlp_rad(ir)-rhokeep(ir))

               enddo
!C112               format(1x,3F25.12)
!C**********************************************************************
!C      Cubic spline fit the values of rho_ovlp_rad(ir) between ir=imin-n !n
!C      to ir=imin+nn  onto 21*nn+1 equally spaced points with initial an !d
!C      final value of r as rhsk(imin-nn) and rhsk(imin+nn)
!C**********************************************************************
                 do j=-nn,nn
                   rsmall(j+nn+1)=rhsk(imin+j,nki,ityp)
                   rhosmall(j+nn+1)=rho_ovlp_rad(imin+j)
                 enddo

                 call ZSPL3(2*nn+1,rsmall,rhosmall,Z,istop)

                 hh=(rsmall(2*nn+1)-rsmall(1))/dfloat(21*nn)
                 rho_min=1.0d+20
                 do j=1,21*nn+1
                  req(j)=rsmall(1) + dfloat(j-1)*hh
                  xv=req(j)
                  rhoeq(j)= SPL3(2*nn+1,rsmall,rhosmall,Z,xv,istop)
                     rho_min = min(rho_min,rhoeq(j))
                 enddo
                 do j=1,21*nn+1
                  if(abs(rhoeq(j)- rho_min).lt.1.0d-14)then
                     rmt_min=req(j)
                  endif
!C *******   Optional Write statement **************************
!C                   write(74,126)r_min,rmt_min,req(j),
!C     .              rhoeq(j)
                 enddo
!C126               format(1x,4F20.12)

!C**********************************************************************
!C        Take the minimum of the various saddle points calculated by
!C     overlapping the central atom with its neighbors (in various shell  !)
!C**********************************************************************
               rmtmin = min(rmtmin,rmt_min)
!C**********************************************************************
!C*      Geometric Mean of the distances at saddle points calculated by
!C          overlapping the rho for central atom with its neighbors
!C**********************************************************************
!C       In this case rmt_ovlp(nki,ityp) should be initialized as 1.0
!C**********************************************************************
!C                rmt_ovlp(nki,ityp)=rmt_ovlp(nki,ityp)*
!C     <                             rmt_min**dfloat(natshl(ishell,iat))
!C**********************************************************************
!C                write(72,*)
!C                write(74,*)
              endif
!C======================================================================
           enddo               ! End of ishell loop
              rmt_ovlp(nki,ityp)=rmtmin

!C             rmt_ovlp(nki,ityp)=rmt_ovlp(nki,ityp)**
!C     <                            (1.0d0/dfloat(num))  ! For Geom. mean
!C======================================================================
        enddo                  ! End of nki loop
       enddo                   ! End of iat loop
!C======================================================================
!C      Estimating the MT-radii of the Empty spheres just from the
!C      information about the neighboring environment (No saddle pt. rep. !)
!C======================================================================
       do iat=1,natom
        ityp=itype(iat)
        do nki=1,komp(ityp)
          if(ztot(nki,ityp).lt.1.0d-3)then
             r_min=1.0d+20
            do ishell=2,numshell(iat)
              a=radshl(ishell,iat)
              ntyp=ntypshl(ishell,iat)
              aNN =  abs( a - radshl(2,iat) )
                do nkn=1,komp(ntyp)
                    if(ztot(nkn,ntyp).gt.1.0d-3.and.aNN.lt.1.d-03)then
                     rmin = a - rmt_ovlp(nkn,ntyp)
                     r_min=min(r_min,rmin)
                    endif
                enddo
            enddo
               rmt_ovlp(nki,ityp) = r_min
          endif
        enddo
       enddo

!CAA*************************************************************
!C    CHK If the MT-radii is UNPHYSICAL e.g. too small or too BIG
!C     Althought it is very unlikely, but might happen sometime
!C                    UNDER extreme condition
!CAA*************************************************************
            rmin0=1.0d+03
            rmax0=0.0d0
            do nsub=1,nbasis
              do nk=1,komp(nsub)
               rmin0=min(rmin0,rmt_ovlp(nk,nsub))
               rmax0=max(rmax0,rmt_ovlp(nk,nsub))
              enddo
            enddo
            if(rmin0.gt.small.and.rmax0.lt.1.0d+03)then
              do iat=1,natom
               ityp=itype(iat)
                do nk=1,komp(ityp)
                 rmt_ovlp(nk,ityp)=radshl(2,iat)/2.0d0
                enddo
              enddo
            endif
!C======================================================================
!C      Genrate the Voronoi Polyhedra Information for all the sites
!C      in Unit Cell : The VP info are printed in file Bern_out
!C      Other info for isoparametric integration are also genrated:
!C       These info are stored in: r_circ, fcount,weight,rmag,vj
!C      The version written on 14-Dec, 2009 works only for Ordered
!C      systems
!C======================================================================
            iVP=0
            do iat=1,natom
              ityp=itype(iat)
              if(komp(ityp).gt.1)then
                if( .not.(isDLM(ityp) .and. komp(ityp)==2)) then
                  iVP=1
                end if
              endif
            enddo

           if(iVP.eq.0)then
             natin=natom
             nbasin=nbasis
              do i=1,3
               transV(1,i)= rslatt(1,i)/(2.0d0*pi)
               transV(2,i)= rslatt(2,i)/(2.0d0*pi*lattice(2))
               transV(3,i)= rslatt(3,i)/(2.0d0*pi*lattice(3))
              enddo

              do i=1,natin
               basisV(1,i)= pos(1,i)
               basisV(2,i)= pos(2,i)/(lattice(2))
               basisV(3,i)= pos(3,i)/(lattice(3))
               ityp1(i)=itype(i)
              enddo

            if (iadjsph_ovlp.ne.0)then
              ! the call below fills out both comp. for DLM case
              write(6,*) 'Doing Voronoi Polyhedra construction'
              call VPI_const(natin,nbasin,lattice,transV,basisV,ityp1,  &
     &    nshell,radshl,rmt_ovlp(1,1:nbasin),r_circ(1,1:nbasin),fcount, &
     &                small,weight,rmag,vj,istop)
              if ( maxval(komp(2:nbasis)) > 1 ) then
               do nsub=1,nbasis
                do nk=2,komp(nsub)
                 r_circ(nk,nsub) = r_circ(1,nsub)
                enddo
               enddo
              end if
            end if
           endif

!C======================================================================
!C      Evaluate the ASA-radii for all the components at each
!C      sublattices using the above calculated MT-radii.
!C      In the derivation we restrict the smaller atoms to grow
!C      faster as compared to the bigger ones.
!C      i.e If R_ASA = R_MT + DR, then DR is prop. to (1/R_MT)
!C======================================================================
       r_min=1.0d+20
       do nsub=1,nbasis
        do nk=1,komp(nsub)
          r_min = min(r_min,rmt_ovlp(nk,nsub))
        enddo
       enddo
       rsum = 0.0d0
       do nsub=1,nbasis
        do nk=1,komp(nsub)
          X(nk,nsub) = rmt_ovlp(nk,nsub)/r_min
        enddo
        rsum = rsum + dfloat(numbsub(nsub))
       enddo
       rlow = (3.0d0*volume/(4.0d0*pi*rsum))**(1.0d0/3.0d0) - r_min
       rup = 1.0d0
!C======================================================================
!C       Solve a cubic equation with roots lying between rlow and rup
!C======================================================================
         a1 = 0.0d0
         a2 = 0.0d0
         a3 = 0.0d0
         a4 = 0.0d0
       do nsub=1,nbasis
        do nk=1,komp(nsub)
         a1 = a1 + atcon(nk,nsub)*(X(nk,nsub)**3.0d0)*                  &
     &        dfloat(numbsub(nsub))
         a2 = a2 + atcon(nk,nsub)*X(nk,nsub)*dfloat(numbsub(nsub))
         a3 = a3 + atcon(nk,nsub)*dfloat(numbsub(nsub))/X(nk,nsub)
         a4 = a4 + atcon(nk,nsub)*dfloat(numbsub(nsub))/                &
     &        (X(nk,nsub)**3.0d0)
        enddo
       enddo
         a1 = (r_min**3.0d0)*a1 - (3.0d0*volume/(4.0d0*pi))
         a2 = 3.0d0*(r_min**2.0d0)*a2
         a3 = 3.0d0*(r_min)*a3


         do i=1,niter
           f1 = fcube(rlow,a1,a2,a3,a4)
           f2 = fcube(rup,a1,a2,a3,a4)
          if(abs(f2-f1).gt.(eps*f2))then
             rf = rup - f2*(rup-rlow)/(f2-f1)
               if(abs(rf-rup).le.1.0d-9)go to 105
             rlow = rup
             rup=rf
          endif
         enddo

105    do nsub=1,nbasis
        do nk=1,komp(nsub)
         rASA(nk,nsub) = rmt_ovlp(nk,nsub) + (rf/X(nk,nsub))
        enddo
       enddo
!C==============================================================
!C        call reduc(iVP,ztot,rASA,volume,nbasis,komp,atcon,numbsub)
!C==============================================================
!C  If iadjsph_ovlp = 0, then the sphere size is not calculated
!C                       from the Saddle point representation (SPR)
!C     else              Sphere size is calculated from SPR
!C             iadjsph_ovlp is set in mkkrcpa.h file
!C==============================================================

         iempty_sph = 1
         do nsub=1,nbasis
          do nk=1,komp(nsub)
            if(ztot(nk,nsub).lt.1.0d-3)then
              iempty_sph = 0
            endif
          enddo
         enddo

       write(6,*) 'iadj = ', iadjsph_ovlp
       if (iadjsph_ovlp.ne.0)then
!C         if(iempty_sph.ne.0)then
           do nsub=1,nbasis
            do nk=1,komp(nsub)
             rmt(nk,nsub) = rASA(nk,nsub)
            enddo
           enddo
            if(mtasa.gt.0)then  ! For ASA case
             do nsub=1,nbasis
              do nk=1,komp(nsub)
               rws(nk,nsub) = rmt(nk,nsub)
              enddo
             enddo
            endif
!C         endif
       endif


!C *******   Optional Write statement **************************
       if ( iprint >= 0 ) then
        do nsub=1,nbasis
         do nk=1,komp(nsub)
!c         if(iVP.eq.0.and.nbasis.eq.1)then
           if(iVP.eq.0)then
           write(71,137)nsub,nk,imax(nk,nsub),                          &
     &      (radshl(2,1)/2.0d0),rmt_ovlp(nk,nsub),                      &
     &       rmt(nk,nsub),r_circ(nk,nsub)
           else
           write(71,132)nsub,nk,imax(nk,nsub),                          &
     &      (radshl(2,1)/2.0d0),rmt_ovlp(nk,nsub),                      &
     &       rmt(nk,nsub)
           endif
         enddo
        enddo
       end if
137    format(2I4,I5,1x,4F15.10)
132    format(2I4,I5,1x,3F15.10)
!c---------------------------------------------------------------
!c  Writing the MT, ASA and circums. sphere radii in the out file
!c---------------------------------------------------------------
!c        if(iVP.eq.0.and.nbasis.eq.1)then
           if(iVP.eq.0)then
         write(6,'(/''  MT, ASA and Circums. radii of atoms at''//      &
     &  ''each sublattices in units of ALAT'')')
        else
         write(6,'(/''  MT and ASA radii of various components at''//   &
     &  ''each sublattices in units of ALAT'')')
        endif
      write(6,*)

      do nsub=1,nbasis
       do nk=1,komp(nsub)

!c        if(iVP.eq.0.and.nbasis.eq.1)then
           if(iVP.eq.0)then
         write(6,1007) nsub,nk,rmt_ovlp(nk,nsub)/alat,                  &
     &               rmt(nk,nsub)/alat,r_circ(nk,nsub)/alat
1007     format(' Subl ',i2,'(komp=',i2,'): MT-radius is',f9.5,         &
     &              ': WS-radius is',f9.5,': Circ-radius is',f9.5)
        else
         write(6,1008) nsub,nk,rmt_ovlp(nk,nsub)/alat,                  &
     &               rmt(nk,nsub)/alat
1008     format(' Subl ',i2,'(komp=',i2,'): MT-radius is',f9.5,         &
     &                          ': WS-radius is',f9.5)
        endif
        if(rmt_ovlp(nk,nsub).gt.rASA(nk,nsub)) then
         call fstop(sname//                                             &
     &                      ': MT-radius > WS-radius -- wrong input?')
        end if
       end do
      end do
      write(6,*)
!c---------------------------------------------------------------
!c      Zeroout working arrays
!c---------------------------------------------------------------
      call zeroout(vr,iprpts*ipcomp*ipsublat*ipspin)
!c     ----------------------------------------------------------
      call zeroout(rhotot,iprpts*ipcomp*ipsublat*ipspin)
!c     ----------------------------------------------------------
      call zeroout(corden,iprpts*ipcomp*ipsublat*ipspin)

!CALAM  ***************************************************************
!c       Read in the starting potential and charge densities for
!c             various components, sublattices and spins.
!c       For spin polarized calc. vdif measures the difference between
!c       mt-zero for maj. and min. spin electrons (assume vmtz(maj)=0)
!CALAM  ***************************************************************

!CDDJ   Allow user to supply Ef guess in input for starting potential
       efpotnk=0.0d0
        if(efpot.gt.0.0001d0) then
           efpotnk=efpot
         write(6,'("  Use user-supplied guess: Ef =",f10.6,/)') efpot
        endif


      read(22,'(a80)',end=100) header
      read(22,*,err=100,end=100)nspinp,vdif
      if(nspinp.gt.ipspin) then
        write(6,'(''   nspinp .gt. ipspin '', 2i5)')nspinp,ipspin
        call fstop(sname)
      endif
!c
      write(6,'(/''  Read starting potentials and charge densities'')')
      ipot_read=0             ! POT file exists
      iset_b_cont=0           ! POT file exists
      iefpot = 0
      do nsub=1,nbasis
        do nk=1,komp(nsub)
          do ns=1,nspinp

!cSUFF automatic DLM and Patterned magnetism file

             if( .not. isflipped(nsub) .and.                            &
     &             (.not. isDLM(nsub) .or. nk <= komp(nsub)/2) ) then
!C              if(ikpoint(nk,nsub).eq.0 .or. ivpoint(nsub).eq.0) then
                  initialize=0
                  write(6,'(''   Sublat.='',i2,'' Spin='',i2,           &
     &            '' Comp.='',i2,'' V(r)'',t40,''is READ'')') nsub,ns,nk
!c                ------------------------------------------------------
                 call potred(jmt(nk,nsub),rmt(nk,nsub),jws(nk,nsub),    &
     &                       rws(nk,nsub),xr(1,nk,nsub),                &
     &                       rr(1,nk,nsub),h(nk,nsub),                  &
     &                       xstart(nk,nsub),vr(1,nk,nsub,ns),          &
     &                       rhotot(1,nk,nsub,ns),corden(1,nk,nsub,ns), &
     &                       xvalsws(nk,nsub,ns),                       &
     &                       ztot(nk,nsub),zcor(nk,nsub),zvals(nk,nsub),&
     &                       zcors(nk,nsub),numc(nk,nsub,ns),           &
     &                       nc(1,nk,nsub,ns),lc(1,nk,nsub,ns),         &
     &                       kc(1,nk,nsub,ns),ec(1,nk,nsub,ns),         &
     &                       alat,efpotnk,initialize,ebot,              &
     &                       ibot_cont,iset_b_cont,mtasa,iprint,istop)
!c               -----------------------------------------------------
             else
                  initialize=0
                  write(6,'(''   Sublat.='',i2,'' Spin='',i2,           &
     &         '' Comp.='',i2,'' V(r)'',t40,''is COPIED'')') nsub,ns,nk

                  if( isDLM(nsub) .and. nk > komp(nsub)/2 )  then
                     nk0 = 1+mod(nk-1,komp(nsub)/2); nsub0 = nsub
                  else
                     nk0 = nk; nsub0 = flippoint(nsub)
                  end if

                  if( ns == 1 ) ns0 = 2
                  if( ns == 2 ) ns0 = 1

                  jmt(nk,nsub) = jmt(nk0,nsub0)
                  rmt(nk,nsub) = rmt(nk0,nsub0)
                  jws(nk,nsub) = jws(nk0,nsub0)
                  xr(:,nk,nsub) = xr(:,nk0,nsub0)
                  rr(:,nk,nsub) = rr(:,nk0,nsub0)
                  h(nk,nsub) = h(nk0,nsub0)
                  xstart(nk,nsub) = xstart(nk0,nsub0)
                  vr(:,nk,nsub,ns) = vr(:,nk0,nsub0,ns0)
                  rhotot(:,nk,nsub,ns) = rhotot(:,nk0,nsub0,ns0)
                  corden(:,nk,nsub,ns) = corden(:,nk0,nsub0,ns0)
                  xvalsws(nk,nsub,ns) = xvalsws(nk0,nsub0,ns0)
                  ztot(nk,nsub) = ztot(nk0,nsub0)
                  zcor(nk,nsub) = zcor(nk0,nsub0)
                  zvals(nk,nsub) = zvals(nk0,nsub0)
                  zcors(nk,nsub) = zcors(nk0,nsub0)
                  numc(nk,nsub,ns) = numc(nk0,nsub0,ns0)
                  nc(:,nk,nsub,ns) = nc(:,nk0,nsub0,ns0)
                  lc(:,nk,nsub,ns) = lc(:,nk0,nsub0,ns0)
                  kc(:,nk,nsub,ns) = kc(:,nk0,nsub0,ns0)
                  ec(:,nk,nsub,ns) = ec(:,nk0,nsub0,ns0)

             end if

                if(iefpot.eq.0) then
                 efpot = efpotnk
                 write(6,*) 'efpot = ', efpot
                 iefpot = 1
                end if
!c ---------------------------------------------------------------------
!c  interstitial charge for SCF mixing in MT case
!c ---------------------------------------------------------------------
!CAA               else
!C                  write(6,'('' Spin='',i2,''   Sublat.='',i2,
!C     >            '' Comp.='',i2,'' V(r)'',t40,
!C     >            ''is DUMMIED'')') ns,nsub,nk
!C                  jmt(nk,nsub)=jmt(ikpoint(nk,nsub),ivpoint(nsub))
!C                  jws(nk,nsub)=jws(ikpoint(nk,nsub),ivpoint(nsub))
!C                  h(nk,nsub)=h(ikpoint(nk,nsub),ivpoint(nsub))
!C                  xvalsws(nk,nsub,ns)=
!C     >            xvalsws(ikpoint(nk,nsub),ivpoint(nsub),ns)
!C                 do i=1,iprpts
!C                    xr(i,nk,nsub)=xr(i,ikpoint(nk,nsub),ivpoint(nsub))
!C                    rr(i,nk,nsub)=rr(i,ikpoint(nk,nsub),ivpoint(nsub))
!C                     vr(i,nk,nsub,ns)=
!C     >               vr(i,ikpoint(nk,nsub),ivpoint(nsub),ns)
!C                     rhotot(i,nk,nsub,ns)=
!C     >               rhotot(i,ikpoint(nk,nsub),ivpoint(nsub),ns)
!C                     corden(i,nk,nsub,ns)=
!C     >               corden(i,ikpoint(nk,nsub),ivpoint(nsub),ns)
!C                  enddo
!C               endif
            enddo
         enddo
      enddo

!C=======  Reading of rV(r) and 4*pi*r^2*rho(r)   COMPLETE  ==========
100     if(ipot_read.eq.0)go to 200  ! Not to generate an starting POT
!CALAM================================================================
!C    Interpolate this new overlapping charge density from the
!C    herman-skillman grid [rhsk] to our logarithmic radial grid [rr]
!C    Hence calculate the potential corresponding to this new rho
!CALAM================================================================
      if(efpot.lt.0.0001d0)efpot = 0.65d0
                                          ! initial guess (OK for 3D TM metal)
      write(6,'(''===========================================           &
     &======================='')')
      write(6,'(/''Generate starting potentials and charge densities:'')&
     &         ')
!c      write(6,*)
      write(6,'('' GETPOT_OVLP: Use INIOVLP_POT to guess a starting pot &
     & from the '')')
      write(6,'(''             overlapping atomic charge density'')')
      write(6,'(/a,f10.7)') 'Made Fermi level guess of = ', efpot

       do ns=1,nspinp
        do nsub=1,nbasis
         do nk=1,komp(nsub)

      write(6,*)
      write(6,'(''   Sublat.='',i2,'' Spin='',i2,                       &
     &          '' Comp.='',i2)') nsub,ns,nk

          call iniovlp_pot(jmt(nk,nsub),rmt(nk,nsub),                   &
     &                  jws(nk,nsub),rws(nk,nsub),                      &
     &                  xr(1,nk,nsub),rr(1,nk,nsub),h(nk,nsub),         &
     &                  xstart(nk,nsub),ztot(nk,nsub),zcor(nk,nsub),    &
     &                  rhsk(1,nk,nsub),rhohsk(1,ns,nk,nsub),nhs,       &
     &                  vr(1,nk,nsub,ns),corden(1,nk,nsub,ns),          &
     &                  rhotot(1,nk,nsub,ns),xvalsws(nk,nsub,ns),       &
     &                  numc(nk,nsub,ns),nc(1,nk,nsub,ns),              &
     &                  lc(1,nk,nsub,ns),kc(1,nk,nsub,ns),              &
     &                  ec(1,nk,nsub,ns),mtasa,iprint,istop)

          enddo
        enddo
      enddo

!C====================================================================
!c   Magn. --> Non-Magn.
!C====================================================================
200   if(nspin.eq.1.and.nspinp.eq.2) then
       vdif = zero
       do nsub=1,nbasis
        do nk=1,komp(nsub)
         xvalsws(nk,nsub,1)=xvalsws(nk,nsub,1)+xvalsws(nk,nsub,2)
         do i=1,jws(nk,nsub)
          rhotot(i,nk,nsub,1)=rhotot(i,nk,nsub,1)+rhotot(i,nk,nsub,2)
          corden(i,nk,nsub,1)=corden(i,nk,nsub,1)+corden(i,nk,nsub,2)
         enddo
         do i=1,iprpts
          vr(i,nk,nsub,1)=0.5d0*(vr(i,nk,nsub,1)+vr(i,nk,nsub,2))
         enddo
        enddo
       enddo
      end if
!C====================================================================
!c   Non-Mag. --> Magn.
!C====================================================================
      if(nspin.eq.2.and.nspinp.eq.1) then
       if(abs(vdif).lt.1.d-10) then
        vdif = vdif0
        write(6,*)
        write(6,'(''   nspinp .ne. nspin '', 2i5)')nspinp,nspin
        write(6,*) ' vdif=',vdif
        write(6,*) ' Non-magnetic initial potential is used for',       &
     &             ' magnetic case:'
        write(6,*) ' VDIF has been changed to',vdif0
        write(6,*)
       end if
       do nsub=1,nbasis
        do nk=1,komp(nsub)
         xvalsws(nk,nsub,2)=0.5d0*xvalsws(nk,nsub,1)
         xvalsws(nk,nsub,1)=xvalsws(nk,nsub,2)
         numc(nk,nsub,2)=numc(nk,nsub,1)
         nc(1:ipeval,nk,nsub,2)=nc(1:ipeval,nk,nsub,1)
         lc(1:ipeval,nk,nsub,2)=lc(1:ipeval,nk,nsub,1)
         kc(1:ipeval,nk,nsub,2)=kc(1:ipeval,nk,nsub,1)
         ec(1:ipeval,nk,nsub,2)=ec(1:ipeval,nk,nsub,1)
!CAA      do i=1,jws(nk,nsub)
!C        For Vp Integration
         do i=1,iprpts
          rhotot(i,nk,nsub,1)=0.5d0*rhotot(i,nk,nsub,1)
          rhotot(i,nk,nsub,2)=rhotot(i,nk,nsub,1)
          corden(i,nk,nsub,1)=0.5d0*corden(i,nk,nsub,1)
          corden(i,nk,nsub,2)=corden(i,nk,nsub,1)
         enddo
         do i=1,iprpts
          vr(i,nk,nsub,2) = vr(i,nk,nsub,1) - vdif*rr(i,nk,nsub)
         enddo
        enddo
       enddo
      end if
!c     ===============================================================
      if (istop.eq.sname) then
         call fstop(sname)
      else
         return
      endif
!c
      end
!CALAM ***************************************************************
!C     Generate a herman-skillman radial grid as generated by
!C     J.M. Maclaren and interpolate the average potential and
!C     charge density on that grid : Also calculate rmax(nk,nsub)
!C     for the cut off of charge density for each spin and sublattice.
!CALAM ***************************************************************

      subroutine herm_skill(fac,nhs,ndoub,ichgm,rhsk,                   &
     &                     ichg,rchg,ztot,rhohsk,                       &
     &                     rmax,imax,alat,iprint,istop)
!c====================================================================
      implicit none
!c +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
      include 'mkkrcpa.h'
!c +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
      character istop*10
      character sname*10

      integer   nhs,ndoub,ichgm,iprint
      integer   imax,ir,idoub,i,j,iex
      integer   icdim
      parameter (icdim=50)
      integer   ichg(0:icdim)

      real*8    rhsk(iprpts)
      real*8    rchg(0:icdim)
      real*8    rhohsk(iprpts)
      real*8    ylag
      real*8    ztot,rmax,fac,rstart,h,alat

      parameter (sname='herm_skill')

!C====================================================================
!C      Generate herman-skillman grid for empty sphere.
!C      For empty spheres generate the grid with z=10 (say) to avoid
!C      the blow up of the stepsize h=fac/z^{1/3}
!C====================================================================

       if(ztot.le.0.0d0)then
        h=fac
        rhsk(1)=0.0d0
        rstart=rhsk(1)
        rchg(0)=rstart
        ir=1
        ichg(0)=1
         do idoub=1,ichgm
           do i=1,ndoub
              ir=ir+1
              rhsk(ir)=i*h+rstart
           enddo
           ichg(idoub)=ir
           rstart=rhsk(ir)
           rchg(idoub)=rstart
           h=2.0d0*h
         enddo
       endif
!C====================================================================
!C     Calculating the r-cutoff (and corresponding index i-cutoff)
!C     using the (small) magnitude of charge density at large
!C                         radial distance (r)
!C====================================================================
        do i=2,nhs
          if(ztot.gt.0.0d0)then
           if(abs(rhohsk(i)).gt.1.0d-06)then
              rmax = rhsk(i)
              imax = i
           endif
          else
!C====================================================================
!C       Put a condition for an estimate of rmax and imax for empty
!C                       spheres ( By intution )
!C====================================================================
           if(abs(rhsk(i)).lt.1.5d0*alat)then
              rmax = rhsk(i)
              imax = i
           endif
          endif
        enddo
!c     ===============================================================
      if (istop.eq.sname) then
         call fstop(sname)
      else
         return
      endif
!c
      end

!CALAM ****************************************************************
!C      Generate the neighbouring shell data for each atom in the unit
!C      cell. The maximum number of shells generated for each atom type
!c      is 15, unless they are greater than rmax from the atom of interes !t.
!c      Each shell contains only one atom type, thus if two different
!c      atoms are a distance r away they will be automatically classed as
!c      two shells
!CALAM ****************************************************************

       subroutine neigh_shell(alat,itype,natom,komp,rslatt,             &
     &                        pos,rmax,nshell,typshl,natshl,radshl,     &
     &                        atvshl,shells,iprint,istop)

!c====================================================================
      implicit none
!c ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
      include 'mkkrcpa.h'
!c ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
      character istop*10
      character sname*10

      integer   nbasis,natom,itype(ipbase),komp(ipsublat),iprint
      integer   nshell,typshl(nshell,natom),natshl(nshell,natom),       &
     &          shells(natom)
      integer   i,j,k,ityp,jtyp,nk,n,iat,jat,kat,ishell,ii,ileft
      real*8    alat,pos(3,ipbase),rmax(ipcomp,ipsublat)
      real*8    rslatt(3,3)       ! In units of Alat/(2*pi)
      real*8    rj(3),rmod
      real*8    radshl(nshell,natom)
      real*8    r1,r2,r3,rbig,rmin,rmax1,sep,dr
      ! warning: this is also defined in the calling routine
      integer, parameter :: max_shell_atoms = 60
      real*8 :: atvshl(3,max_shell_atoms,nshell,natom)

      parameter (sname='neigh_shell')
!C====================================================================
      do i=1,3
        do j=1,3
         rslatt(i,j)=rslatt(i,j)*alat/(2.0d0*pi)
!C                              ! Translation vectors in at. units
        enddo
        do k=1,natom
         pos(i,k)=pos(i,k)*alat    ! Atomic positions in the unit cell
        enddo                      !      in at. units
      enddo
!C
!C     initialize
!C
      rbig=0.0d0
       do i=1,natom
        ityp=itype(i)
!C====================================================================
!C       This step is different from J M Maclaren's code: Bcos
!C       that code is applicable only for ordered sublattices
!C       however we have generalised it for sublattices which
!C       may involve substitutional disorder..........
!C====================================================================
         do nk=1,komp(ityp)
           rbig=max(rbig,rmax(nk,ityp))
         enddo
!C====================================================================
          do ishell=1,nshell
            typshl(ishell,i)=0
            natshl(ishell,i)=0
            radshl(ishell,i)=1.0d+06
          enddo
       enddo
!c
!c----- decide approximately how many unit cells are needed
!c
      rmin=1.0d+06
      r1=rmod(rslatt(1,1),rslatt(2,1),rslatt(3,1))
      r2=rmod(rslatt(1,2),rslatt(2,2),rslatt(3,2))
      r3=rmod(rslatt(1,3),rslatt(2,3),rslatt(3,3))
      rmin=min(r1,r2,r3)
      n=nint(rbig/rmin)
!c
!c----- loop over these unit cells
!c
      rmax1=0.0d0
      do 50 i=-n,n
      do 50 j=-n,n
      do 50 k=-n,n
!c
!c----- rj is the vector to unit cell <ijk>
!c
      rj(1)=i*rslatt(1,1)+j*rslatt(1,2)+k*rslatt(1,3)
      rj(2)=i*rslatt(2,1)+j*rslatt(2,2)+k*rslatt(2,3)
      rj(3)=i*rslatt(3,1)+j*rslatt(3,2)+k*rslatt(3,3)
!c
!c----- loop over atoms
!c
      do 50 jat=1,natom
      jtyp=itype(jat)
!c
!c----- build shell table for atom kat
!c
      do 40 kat=1,natom
       sep=rmod(rj(1)+pos(1,jat)-pos(1,kat),                            &
     &          rj(2)+pos(2,jat)-pos(2,kat),                            &
     &          rj(3)+pos(3,jat)-pos(3,kat))
!C====================================================================
!C     This step is different form J M Maclren's code, bcos of
!C     the same reason as stated above.
!C====================================================================
      do nk=1,komp(jtyp)
       rmax1=max(rmax1,rmax(nk,jtyp))
      enddo
!DEBUG CAB000      
      rmax1 = max(rmax1,alat)
!DEBUG CAB000      
      if(sep.le.rmax1) then
!c
!c----- compare this separation with previously found shells
!c
      ishell=0
   20 ishell=ishell+1
       if(ishell.le.nshell) then
        dr=sep-radshl(ishell,kat)
        if(abs(dr).lt.1.0d-04) dr=0.0d0
        if(dr.gt.0.0d0) goto 20
        if(dr.eq.0.0d0) then
         if(typshl(ishell,kat).ne.jtyp) goto 20
         natshl(ishell,kat)=natshl(ishell,kat)+1
!c suffian -- addon: now store vector displacements (at. units)
         atvshl(:,natshl(ishell,kat),ishell,kat) =                      &
     &      rj(:)+pos(:,jat)-pos(:,kat)
        else
         if(ishell.ne.nshell) then
!c
!c----- shift everything up to insert this shell
!c
          do 30 ii=nshell,ishell+1,-1
           typshl(ii,kat)=typshl(ii-1,kat)
           natshl(ii,kat)=natshl(ii-1,kat)
           radshl(ii,kat)=radshl(ii-1,kat)
           atvshl(:,:,ii,kat)=atvshl(:,:,ii-1,kat)
   30     continue
         endif           ! end of ishell.ne.nshell
!c
      typshl(ishell,kat)=jtyp
      natshl(ishell,kat)=1
      radshl(ishell,kat)=sep
!c suffian -- addon: now store vector displacements (at. units)
      atvshl(:,natshl(ishell,kat),ishell,kat) =                         &
     &  rj(:)+pos(:,jat)-pos(:,kat)

        endif            ! end of dr.eq.0.0d0
       endif             ! ishell.le.nshell
      endif              ! sep.le.rmax1
   40 continue
   50 continue
!c
!c----- find out how many shell we got
!c
      do iat=1,natom
       shells(iat)=0
       do ishell=1,nshell
          if(natshl(ishell,iat).gt.0) then
           shells(iat)=shells(iat)+1
          endif
       enddo
      enddo
!C----------------------------------------------------------------
!C     Scale rslatt(3,3) and pos(3,natom) to their original values
      do i=1,3
        do j=1,3
         rslatt(i,j)=rslatt(i,j)*(2.0d0*pi)/alat
!C                                  ! Translation vectors in at. units
        enddo
        do k=1,natom
         pos(i,k)=pos(i,k)/alat    ! Atomic positions in the unit cell
        enddo                      !      in at. units
      enddo
!c
!c----- write out neighourhood for each atom
!c
      if (iprint.ge.1)then
        do iat=1,natom
          write(6,'('' Shell structure for atom '', i4                  &
     &           '' in the unit cell '')')iat
           ishell=shells(iat)
           ileft=ishell/9
           if(mod(ishell,9).ne.0) ileft=ishell/9+1
          do ii=1,ileft
            write(6,'(/'' Shell: '', 9i7)')                             &
     &                     (i,i=9*ii-8,min(9*ii,ishell))
            write(6,'('' Atoms: '', 9i7)')                              &
     &            (natshl(i,iat),i=9*ii-8,min(9*ii,ishell))
            write(6,'(''Radius: '',9f7.3)')                             &
     &             (radshl(i,iat),i=9*ii-8,min(9*ii,ishell))
          enddo
        enddo
      endif
!c     ===============================================================
      if (istop.eq.sname) then
         call fstop(sname)
      else
         return
      endif

      end
!C======================================================================
      subroutine integr(f,r,kmx,ichg,a,ichgm,md,istop)
!C======================================================================
!c----- this routine integrates function f on a 1-d mesh by quadratures
!c      the partial integrals (up to r(k)) are stored in b(k).  the mesh
!c      is assumed to be of herman-skillman type with the doubling points
!c      specified on ichg
!C======================================================================
      implicit real*8 (a-h,o-z)
!c
      parameter(s720=720.0d0,s646=646.0d0,s456=456.0d0,s346=346.0d0,    &
     &         s264=264.0d0,s251=251.0d0,s106=106.0d0,s74=74.0d0,       &
     &         s30=30.0d0,s25=25.0d0,s24=24.0d0,s19=19.0d0, s11=11.0d0, &
     &         s10=10.0d0,s9=9.0d0,s7=7.0d0,s6=6.0d0,s5=5.0d0,s4=4.0d0, &
     &         s2=2.0d0,s0=0.0d0)
!c
      dimension f(kmx),r(kmx),a(kmx)
      parameter (icdim=50)
      integer ichg(0:icdim)

      character  istop*10
      character  sname*10
      parameter (sname='integr')
!c
!c----- initialise
!c
      h=r(2)-r(1)
      if(md.ne.1) then
      k0=1
      a(1)=s0
      f0=f(1)
      else
      k0=0
      f0=s0
      endif
      n=1
      km3=k0+1
      km2=k0+2
      km1=k0+3
      kfst=k0+4
!c
!c----- quadrature formulas  q41(0), q41(1), q41(2) for 1st 3 points
!c
      a(km3)=h*(s251*f0+s646*f(km3)-s264*f(km2)+s106*f(km1)-s19*f(kfst))&
     & /s720
      a(km2)=a(km3)+h*(-s19*f0+s346*f(km3)+s456*f(km2)-s74*f(km1)+s11*f &
     & (kfst))/s720
      a(km1)=a(km2)+h*(s11*f0-s74*f(km3)+s456*f(km2)+s346*f(km1)-s19*f  &
     & (kfst))/s720
!c
!c----- main part of integration q31(2)
!c
   10 klst=min(ichg(n),kmx)
      do 20 k=kfst,klst
      a(k)=a(km1)+h*(s9*f(k)+s19*f(km1)-s5*f(km2)+f(km3))/s24
      km3=km2
      km2=km1
      km1=k
   20 continue
!c
!c----- if grid has doubled apply special formulae
!c
      k=klst+1
      if(klst.ne.kmx) then
      h=r(k)-r(klst)
      a(k)=a(km1)+h*(s2*f(k)+s7*f(km1)-s4*f(km2)+f(km3))/s6
      if(k.ne.kmx) then
      kp1=k+1
      a(kp1)=a(k)+h*(s11*f(kp1)+s25*f(k)-s10*f(km1)+s4*f(km2))/s30
      if(kp1.ne.kmx) then
      km3=klst
      km2=k
      km1=kp1
      kfst=kp1+1
      n=n+1
      goto 10
      endif
      endif
      endif
!c
      if(mod(md,2).eq.0) then
      amax=a(kmx)
      a(kmx)=s0
      do 30 k=1,kmx-1
      a(k)=amax-a(k)
   30 continue
      endif
      if (istop.eq.sname) then
         call fstop(sname)
      endif

      return
      end

!C================================================================
      function invgrd(r,h0,itype,rmax,imax,ichgm,rchg,ichg)
!C================================================================
!c
!c----- this function finds the grid point nearest to r
!c
      implicit real*8 (a-h,o-z)
       parameter(icdim=50)

      integer itype,imax,ichgm,ichg(0:icdim)
      real*8  r,h0,rmax,rchg(0:icdim)
!c
      if(r.ge.rmax) then
!c
!c----- beyond range of rho
!c
      invgrd=imax
      else
!c
!c----- find nearest change point
!c
      h=h0
      do 10 i=0,ichgm-1
      if(r.ge.rchg(i).and.r.lt.rchg(i+1)) then
      r=r-rchg(i)
      invgrd=ichg(i)+nint(r/h)
      goto 20
      endif
      h=2.0d0*h
   10 continue
   20 continue
      endif
      return
      end
!CALAM================================================================
!C    Interpolate the new overlapping charge density from the
!C    herman-skillman grid [rhsk] to our logarithmic radial grid
!C    Hence calculate the potential corresponding to this new rho
!CALAM================================================================

      subroutine iniovlp_pot(jmt,rmt,jws,rws,xr,rr,h,xstart,ztot,zcor,  &
     &                  rhsk,rhohsk,nhs,vr,corden,rhotot,xvalsws,numc,  &
     &                  nc,lc,kc,ec,mtasa,iprint,istop)

      implicit none
!C
      include 'mkkrcpa.h'
!C
      character istop*10
      character sname*10
      character coerr*10
!c
      integer   jmt,jws,nhs,iprint,mtasa,iex,ir,j,numc
      integer   nc(*),lc(*),kc(*)
      real*8    rmt,rws,h,xstart,xlast,ztot,zcor
      real*8    xr(iprpts)
      real*8    rr(iprpts)
      real*8    rhsk(iprpts)
      real*8    rhohsk(iprpts)
      real*8    vr(iprpts)
      real*8    corden(iprpts)
      real*8    rhotot(iprpts)
      real*8    rho1int(iprpts)
      real*8    rho2int(iprpts)
      real*8    vx(iprpts)
      real*8    ec(*)

      real*8    xvalsws,fac,ro3,vhart,dummy,alpha2,ylag

      parameter (sname='iniovlp_pot')
!C===============================================================
!c set number of points up to sphere boundary, i.e. m.t. or a.s.a

!c set log-grid starting point
         xstart=-11.13096740d+00
!c
           if(mtasa.le.0) then
            jmt=iprpts-50            ! additional points used in "mixing"
           else
            jmt=iprpts-38            ! additional points used in "mixing"
            jws=jmt
           endif
!c
         xlast=log(rmt)
         h=(xlast-xstart)/(jmt-1)
         do j=1,iprpts
           xr(j)=xstart+(j-1)*h
           rr(j)=exp(xr(j))
!c
!c set number of pts. to w.s. radius
           if(mtasa.le.0 .and. rr(j).lt.rws) jws= j + 1
         enddo

         if(jws.ge.iprpts-1) then
           coerr=' jws greater than iprpts-1'
           write(6,'('' jws='',i3,2x,''iprpts='',i3)') jws,iprpts
           call fstop(sname//':'//coerr)
         endif
         if(iprint.ge.1) then
           write(6,'(''  iniovlp_pot: jmt,h,rr(1),rmt'',i7,3e14.6)')    &
     &                          jmt,h,rr(1),rmt
         endif
!C===================================================================
!C      Interpolate the charge density from the herman-skillman
!C      grid (rhsk) to the logarithmic radial grid as constructed
!C      above.
!C===================================================================
         do j=1,iprpts
            rhotot(j)=ylag(rr(j),rhsk,rhohsk,0,3,iprpts,iex)
         enddo
!C===================================================================
!C       Construct the potential corresponding to new charge density
!C===================================================================
         call qexpup(1,rhotot,iprpts,xr,rho2int)
            fac=rho2int(jws)
            rho2int(jws+1:iprpts)=fac       ! this fixes an error

         call qexpup(0,rhotot,iprpts,xr,rho1int)
            fac=rho1int(jws)
            rho1int(jws+1:iprpts)=fac

         do ir=1,iprpts      !calculate new potential
!c
!c Hartree piece
            vhart= 2.0d0*( (-ztot+rho2int(ir))/rr(ir) +                 &
     &                   (rho1int(jmt)-rho1int(ir)) )
!c
!c exchange-correlation potential
!c            if(rhotot(ir).le. 0.0d0) rhotot(ir)=rhotot(ir-1)

            if(rhotot(ir).le. 0.0d0) then
             if(ir==1) then
              rhotot(1) =1.0d-15 !SK
             else
              rhotot(ir)=rhotot(ir-1)
             end if
            end if

            ro3=(3.0d0*(rr(ir)**2)/rhotot(ir))**(1.0d0/3.0d0)
!c
            vx(ir)=alpha2(ro3,0  , 1d0,10   ,dummy) !spin is real*8

!c  totrho->0 ro3-> large, some times this causes an error in alpha2
!c  in this case the vx=0
            if(vx(ir).eq.0.0d0)  then
               vx(ir)=vx(ir-1)
            end if
!c
!c new potential, including mean-field, charge-correlated CPA
!c         vrnew(ir,ik)=( vhart + vx(ir,ik) - vmt1 + vqeff(ik) )*rr(ir)
            vr(ir)=( vhart + vx(ir) )*rr(ir)
         enddo

          call scf_atom_pot(iprpts,jmt,jws,h,rr,xr,ztot,vr,rhotot,      &
     &                      corden,1,numc,nc,lc,kc,ec,mtasa,zcor,       &
     &                      xvalsws,iprint,istop)


      if (istop.eq.sname) then
         call fstop(sname)
      else
         return
      endif

      end

!C================================================
      function  rmod(xx,yy,zz)
!C================================================
      implicit real*8 (a-h,o-z)
!c
      rmod=sqrt(xx*xx+yy*yy+zz*zz)
!c
      return
      end
!C================================================
      function  fcube(x,a1,a2,a3,a4)
!C================================================
      implicit real*8 (a-h,o-z)
!c
      fcube = a1 + a2*x + a3*x*x + a4*x*x*x
!c
      return
      end
!*======================================================================= !=
        subroutine ZSPL3(n,x,y,z,istop)
        implicit double precision(a-h,o-z)
        dimension x(n),y(n),z(n),h(n),b(n),u(n),v(n)
        character  istop*10
        character  sname*10
        parameter (sname='ZSPL3')

        do 2 i=1,n-1
          h(i)=x(i+1)-x(i)
          b(i)=(y(i+1)-y(i))/h(i)
 2      continue
          u(2)=2.0d0*(h(1)+h(2))
          v(2)=6.0d0*(b(2)-b(1))
         do 3 i=3,n-1
           u(i)=2.0d0*(h(i)+h(i-1))-(h(i-1)*h(i-1))/u(i-1)
           v(i)=6.0d0*(b(i)-b(i-1))-(h(i-1)*v(i-1))/u(i-1)
 3       continue
          z(n)=0.0d0
         do 4 i=n-1,2,-1
            z(i)=(v(i)-h(i)*z(i+1))/u(i)
 4       continue
          z(1)=0.0d0
!*======================================================================= !==
         if (istop.eq.sname) then
          call fstop(sname)
         endif
            return
            end
!*======================================================================= !==
        function SPL3(n,x,y,z,xv,istop)
        implicit double precision(a-h,o-z)
        dimension x(n),y(n),z(n)
        character  istop*10
        character  sname*10
        parameter (sname='SPL3')
         do 2 i=n-1,2,-1
           diff=xv-x(i)
           if(diff.ge.0.0d0) go to 3
 2       continue
         i=1
         diff=xv-x(1)
 3       h=x(i+1)-x(i)
         b=((y(i+1)-y(i))/h)-(h*(z(i+1)+2.0d0*z(i)))/6.0d0
         p=0.5d0*z(i)+(diff*(z(i+1)-z(i)))/(6.0d0*h)
         p=b+diff*p
         SPL3=y(i)+diff*p
!*=====================================================
         return
         end
!C==============================================================
        subroutine step_size(ztot,komp,nbasis,nhs,fac,ndoub,ichgm)
        implicit double precision(a-h,o-z)
        include 'mkkrcpa.h'
        real*8 ztot(ipcomp,ipsublat)
        integer komp(ipsublat)

        zmax=1.0d0
        do nsub=1,nbasis
           do nk=1,komp(nsub)
             zmax=max(zmax,ztot(nk,nsub))
           enddo
        enddo
         if(zmax.gt.10)then
           fac=0.88534138d0*5.0d-02
           ndoub=200
         else
           fac=0.88534138d0*5.0d-03
           ndoub=100
         endif
            ichgm=nhs/ndoub

         return
         end
!C==============================================================
        subroutine reduc(iVP,ztot,rASA,volume,nbasis,komp,atcon,numbsub)
        implicit double precision(a-h,o-z)
        include 'mkkrcpa.h'
        integer nbasis,komp(ipsublat),numbsub(ipsublat)
        integer nsub,nk

        real*8  rASA(ipcomp,ipsublat),atcon(ipcomp,ipsublat)
        real*8  ztot(ipcomp,ipsublat)
        real*8  rchng(ipcomp,ipsublat)
        real*8  rmin,al,ag,a,x,volume,reduce,zmin
        parameter (reduce = -0.0262d0 )

       rmin=1.0d+20
       zmin=1.0d+20
       do nsub=1,nbasis
        do nk=1,komp(nsub)
          rmin = min(rmin,rASA(nk,nsub))
          zmin = min(zmin,ztot(nk,nsub))
        enddo
       enddo

       al=0.0d0
       ag=0.0d0
       do nsub=1,nbasis
        do nk=1,komp(nsub)
          if(abs(rASA(nk,nsub)-rmin).lt.1.d-03)then
            al=al + atcon(nk,nsub)*numbsub(nsub)
          else
            ratio=(rASA(nk,nsub)/rmin) - reduce
            ag=ag + atcon(nk,nsub)*numbsub(nsub)*ratio**3.0d0
          endif
        enddo
       enddo
        a = al + ag
        x = (3.0d0*volume/(4.0d0*pi*a))**(1.0d0/3.0d0) - rmin
       do nsub=1,nbasis
        do nk=1,komp(nsub)
          if(abs(rASA(nk,nsub)-rmin).lt.1.d-03)then
            rchng(nk,nsub) = rASA(nk,nsub) + x
          else
            rchng(nk,nsub) = rASA(nk,nsub) - x
          endif
        enddo
       enddo

!*===============================================================
!C       Load rchng back to rASA
!*===============================================================
       if(iVP.gt.0.and.(dabs(zmin-26.0d0).lt.1.0d-3))then
       do nsub=1,nbasis
        do nk=1,komp(nsub)
            rASA(nk,nsub) = rchng(nk,nsub)
        enddo
       enddo
       endif
       return
       end
