!c
!c     ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      subroutine corslv(ispn,jmt,jws,h,r,x,                             &
     &                  rv,                                             &
     &                  z,zvalss,zsemss,numc,nc,lc,kc,ecore,            &
     &                  corden,semden,                                  &
     &                  qcormt,qcortot,qsemmt,qsemtot,                  &
     &                  ebot,ecorv,esemv,                               &
     &                  ibot_cont,nspin,nrelc,mtasa,iprint,istop)
!c     =================================================================
!c
      implicit real*8 (a-h,o-z)
!c
      character sname*10
      character istop*10
!c
!c     +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
      include 'mkkrcpa.h'
!c     +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!c
      integer nc(ipeval)
      integer lc(ipeval)
      integer kc(ipeval)
      integer ibot_cont
!c
      real*8 ecore(ipeval)
!c      real*8 ecorein(ipeval)
      real*8 r(iprpts)
      real*8 rv(iprpts)
      real*8 x(iprpts)
      real*8 corden(iprpts)
      real*8 semden(iprpts)
!c      real*8 coret(iprpts)
      real*8  ebot,zvalss,zsemss
      real*8 g(iprpts)
      real*8 f(iprpts)
      real*8 wrk(iprpts)
      real*8 qcormt
      real*8 qsemmt
      real*8 qcortot
      real*8 qsemtot
      real*8 qmt
      real*8 qtot
      real*8 qnelt
!CDEBUG
      integer    iverybad
      data       iverybad/0/
      save       iverybad
!CDEBUG

!c parameters
      real*8     onh
      real*8     tol,tolchg
!c
!c     +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
      parameter (onh=137.036d0)
      parameter (tol=one/ten**10)
      parameter (tolchg=one/ten**10)
      parameter (sname='corslv')
!c     +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
      character lj(7)*4
      data lj/'s1/2','p1/2','p3/2','d3/2','d5/2','f5/2','f7/2'/
!c
!c     //////////////////////////////////////////////////////////////////
!c     This routine drives the radial eq. solution for the core states.
!c     It must be called for each component (sublattice) separately.
!c
!c           ......added INWHNK for inward integration of dirac eq.
!c                 and modified for ASA case....... ddj & fjp  july 1991
!c           ......modified for SPIN-POLARIZED REL. case...ddj sept 1995
!c     //////////////////////////////////////////////////////////////////
!c
!c     *****************************************************************
!c     calls:  deepst(outws,inws)
!c             semcor(outws,hank)
!c
!c     *****************************************************************
!c     r= radial grid,
!c     rv=potential times r
!c     ecore= core levels guesses: to be updated
!c     g,f upper and lower components times r
!c     corden= core charge density
!c     nc= principal q. nos.,
!c     lc= orbital q. nos.,
!c     kc= kappa q.nos.
!c     numc= no. of core states,
!c     z= atomic no.,
!c     jmt=muffin-tin index
!c     jws=top r index
!c     **************************************************************
!c
!c     ks is the plus and minus relativistic solutions.
!c     -------------------------------------------------------------
!c           n =   1      2            3                  4
!c           l =   0   0  1  1   0  1  1  2  2   0  1  1  2  2  3  3
!c     core kc =  -1  -1  1 -2  -1  1 -2  2 -3  -1  1 -2  2 -3  3 -4
!c     core ks =  -1  -1  1 -1  -1  1 -1  1 -1  -1  1 -1  1 -1  1 -1
!c                        \ /       \ /   \ /       \ /   \ /   \ /
!c                         |         |     |         |     |     |
!c     averaged:   s   s   p     s   p     d     s   p     d     f
!c     -------------------------------------------------------------
!c     NOTE: valence KC is may be different by sign for l.ne.0
!c     val. kc =  -1  -1 -1  2  -1 -1  2 -2  3  -1 -1  2 -2  3 -3  4
!c     -------------------------------------------------------------
!c
!c
!c----------------------------------------------------------------------- !-
!c NOTE: For ASA, normalization and energy update of eigenvalue guess are
!c       sensitive to loss of points which describe decaying tail of
!c       wave-fct., so in DEEP and, especially, SEMCST the normalization
!c       and energy update require different calls to integrate wave-fcts !.
!c       r-grids must go to IPRPTS and ASA integrations must go to LAST2.
!c----------------------------------------------------------------------- !-
!c
!c fix the last point.....(cannot be larger than iprpts)........
      last=iprpts
!c
       if(mtasa.le.0) then
!c normalization can be in interstitial, outside MT spheres
         last2=iprpts
       else
!c normalization must be inside ASA spheres
         last2=jws
       endif
!c
      if(iprint.ge.2) write(6,'('' numc '',i5)') numc
      if (numc.le.0) return
!c
      ifail=0
!c      emax=-ten**6
      nitmax=100 ! sk debugging
      nelt=0
      fnstrc=one/onh
      c=two/fnstrc
      c=c*10.0d0**nrelc
!c     ================================================================
!c     call deepst for each core state.................................
      esemv=zero
      ecorv=zero
      ncount=0
      do i=1,numc
        fac1=(3-nspin)*iabs(kc(i))
!c        ecorein(i)=ecore(i)
        if(iprint.ge.2) then
         write(6,'('' i,nc,lc,kc,ecore  '',4i5,d12.5)')                 &
     &            i,nc(i),lc(i),kc(i),ecore(i)
         call flush(6)
!cab         call flush_(6)
        end if
!c       ----------------------------------------------------------
        call deepst(nc(i),lc(i),kc(i),ecore(i),                         &
     &            rv,r,x,g,f,h,z,c,nitmax,tol,jws,                      &
     &            last,iter,ifail)
!        write(6,*) 'ecore(i) = ', ecore(i)
!c       ----------------------------------------------------------
!c       ==========================================================
!c       when core state energy >-10 then treat as semi-core.......
        if( ecore(i) .ge. -ten ) then
!c          ------------------------------------------------------
           call semcst(nc(i),lc(i),kc(i),ecore(i),                      &
     &                 rv,r,x,g,f,h,z,c,nitmax,tol,jmt,                 &
     &                 last2,iter,ifail)
!c          ------------------------------------------------------
!CDDJ check if this should be:
!CDDJ       esemv=esemv + ecore(i)*fac1 + (nspn-1)*vdif*fac1
!CALAM===============================================================
!CALAM      if ibot_cont=0, i.e. if ebot is fixed
!CALAM      CHK semicore level and modify it if ebot<ecore(i), At
!CALAM      the same time one has to modify the valence level :
!CALAM                 ncount does that.
!CALAM===============================================================
           if(ibot_cont.eq.0)then
            if(ebot.gt.ecore(i))then
             nelt=nelt+(3-nspin)*iabs(kc(i))
             esemv=esemv + ecore(i)*fac1
!c            degeneracy of the level ( accumulate the core chg density)
             do j=1,last
               semden(j)=semden(j) + fac1*( g(j)*g(j)+f(j)*f(j) )
             enddo
            else
             ncount=ncount+1
             if(ispn.eq.1)then
              zsemss = zsemss - 2.0d0*iabs(kc(i))
              zvalss = zvalss + 2.0d0*iabs(kc(i))
             endif
           write(6,'(''WARNING : Z_VAL and Z_SEMCOR HAS BEEN MODIFIED'',&
     &              '' BECOS ebot<esemcor(i)'')')
            endif
           else
             nelt=nelt+(3-nspin)*iabs(kc(i))
             esemv=esemv + ecore(i)*fac1
!c            degeneracy of the level ( accumulate the core chg density)
             do j=1,last
               semden(j)=semden(j) + fac1*( g(j)*g(j)+f(j)*f(j) )
             enddo

           endif
        else
           do j=1,last
              corden(j)= corden(j) + fac1*( g(j)*g(j)+f(j)*f(j) )
           enddo
           ecorv=ecorv+ecore(i)*fac1
           nelt=nelt+(3-nspin)*iabs(kc(i))
        endif
      enddo
!CALAM  Modify the semicore level if ebot< ecore(i) for any i-level.
       if(ibot_cont.eq.0)then
        numc=numc-ncount
       endif
!c
!c     ============================================================
!c     Total Density
!cALAM        do j=1,jmt
!cALAM           coret(j)=corden(j)+semden(j)
!cLAMA        enddo
!c     ============================================================
!c
!c     *************************************************************
      if(iprint.ge.-1) then
!c
!c     Major printout: core eigenvalues...........................
!c
       write(6,'(''     ///////////////////////////////////'',          &
     &          ''//////////////////////////////////'')')
       write(6,'(''      Eigenvalues:'',t20,''n'',t25,''l'',t30,        &
     &  ''k'',t42,''energy'',6x,''core'',4x,''occ.'',1x,''iter'')')
       do i=1,numc
        nel= (3-nspin)*iabs(kc(i))
        indx= lc(i) + iabs(kc(i))
!c       write(6,'(t18,i3,t23,i3,t28,i3,t35,d20.13,3x,i1,a4,i5)')
        write(6,'(t18,i3,t23,i3,t28,i3,t35,f16.8,3x,i1,a4,2i5)')        &
     &    nc(i),lc(i),kc(i),ecore(i),nc(i),lj(indx),nel,iter
       enddo
       write(6,'(''     ///////////////////////////////////'',          &
     &          ''//////////////////////////////////'')')
!c
      end if
!c     *************************************************************
!c
      if(iprint.ge.-2) then
!c
!c Energy from core
!c
       write(6,'(/)')
       write(6,'(''      Energy     core:'',t35,''='',f16.8)')          &
     &                                          ecorv
       write(6,'(''      Energy semicore:'',t35,''='',f16.8)')          &
     &                                          esemv
       write(6,'(/)')
!c
      end if
!c
!c integrate averaged single-site density
!c
!c deep core: MT and WS
      call qexpup(1,corden,last2,x,wrk)
      qcormt=wrk(jmt)
      qmt=qcormt
      qcortot=wrk(last2)
      qtot=qcortot
      if(iprint.ge.-2) then
       write(6,'(''     -----------------------------------'',          &
     &          ''----------------------------------'')')
       write(6,'(''      Charge     core: mt/asa,tot'',t35,''='',       &
     &                           2f16.8)') wrk(jmt),wrk(last2)
      end if
!c
!c semi-core core: MT and WS
      call qexpup(1,semden,last2,x,wrk)
      qsemmt=wrk(jmt)
      qmt=qmt+qsemmt
      qsemtot=wrk(last2)
      qtot=qtot+qsemtot
      if(iprint.ge.-2) then
       write(6,'(''      Charge semicore: mt/asa,tot'',t35,''='',       &
     &                           2f16.8)') wrk(jmt),wrk(last2)
!c
!c total core charges: MT and WS
       write(6,'(''     -----------------------------------'',          &
     &          ''----------------------------------'')')
       write(6,'(''      Charge    total: mt/asa,tot'',t35,''='',       &
     &                           2f16.8)') qmt,qtot
       write(6,'(''     -----------------------------------'',          &
     &          ''----------------------------------'')')
      end if
!c
!c check conservation of core charges
      if(nelt.gt.0) then
       qnelt=nelt
      else
       qnelt=1.d0
      end if
      if(abs(nelt-qtot)/qnelt .gt. tolchg) then
         write(6,'('' Lost Core Electrons: nelt,qtot,diff'',            &
     &               /i5,3d16.8)')  nelt,qtot,abs(nelt-qtot)
         if(iverybad.gt.10) then
          call fstop(sname)
         else
          iverybad = iverybad+1
         endif
      endif
!c     ***********************************************************
!c
      if(istop.eq.sname) then
      call fstop(sname)
      endif
!c
      return
      end
