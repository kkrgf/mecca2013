!===================================================================
!            Isoparametric Integration for real function           !
!-------------------------------------------------------------------
!    Generate the    int_{R_MT}^{VP} [fs(r)/(4*pi*r**ia)] d3r      !
!===================================================================
      subroutine isopar_integ(ia,rmt_ovlp,rrs,fs,                       &
     &                        r_circ,fcount,weight,rmag,vj,VPInt)

!***********************************************************************
      use mecca_constants
      implicit none
!      include 'imp_inp.h'
!--[General veriables]-------------------------------------------------
       integer fcount
       integer ia,Ni,Nfn,Ndata
!--[input from Bernal]-------------------------------------------------
      real*8 rmt_ovlp,r_circ
!--[for face analysis]-------------------------------------------------
!--[for abscissas & weight]--------------------------------------------
        real*8 weight(MNqp)
!--[for poly_fit]------------------------------------------------------
      real*8 a1pf(npol+1)
!--[for Jacobian & print result]---------------------------------------
       integer i,j,k,I_face
        real*8 vjj,vj(MNqp,MNqp,MNqp,MNF),func,volume,                  &
     &         basis(npol+1,Nints),ypf(Nints)
        real*8 rmag(MNqp,MNqp,MNqp,MNF),rmagn
        real*8 rrs(nf),fs(nf)
        real*8  VPInt(2)
!----------------------------------------------------------------------
        integer :: Nq1,Nq2,Nq3
        VPInt=0.0D0

        !---[driver for polynomial fitting]---
        call polyn_fit_input0(Ni,Nfn,Ndata,r_circ,rmt_ovlp,rrs)

        call polyn_fit_input(Ni,Nfn,Ndata,basis,rrs)

         ypf(1:Ndata)=fs(Ni:Nfn)
         call polyn_fit(ypf,a1pf,basis,Ndata)

        Nq1 = size(vj,1)
        Nq2 = size(vj,2)
        Nq3 = size(vj,3)
        do 200 I_face=1,fcount
         do 100 i=1,Nq1
         do 100 j=1,Nq2
         do 100 k=1,Nq3
           rmagn=rmag(i,j,k,I_face)
!c******************************************************************
!c   Sometimes we have low accuracy ==>
!c   Large numbers can be generated for vj(*,*,*,*)
!c******************************************************************
          if(abs(vj(i,j,k,I_face)).lt.100.0d0)then
            call polyn_fit_output(ia,poly_type,npol,a1pf,               &
     &                    rmagn,func)
            VPInt(1)=VPInt(1)+weight(i)*weight(j)*weight(k)*            &
     &                   vJ(i,j,k,I_face)
            VPInt(2)=VPInt(2)+weight(i)*weight(j)*weight(k)*            &
     &                   vJ(i,j,k,I_face)*func
          endif
  100    continue
  200   continue

      return
      end
!===================================================================

!===================================================================
!     polyn_fit_input0(Ndata,Ni,r,tnAtom,wholeVP,input)         !
!-------------------------------------------------------------------
!     Generate the # of Ni.                                        !
!===================================================================
      subroutine polyn_fit_input0(Ni,Nfn,Ndata,r_circ,rmt,rr)
      use mecca_constants
      implicit none
!      include 'imp_inp.h'
        integer Ni,Nfn,Ndata,count,count1,i
        real*8 rr(Nf),rmt,r_circ
        logical wholeVP

        wholeVP=.false.

      if(wholeVP)then
         Ni=1
         goto 100
      end if

  100 continue

      count=0
      count1=0
        do i=1,Nf
          if(rr(i) <= rmt .and. .not. wholeVP)then
           count=count+1
           Ni=count
          end if
          if(rr(i) <= r_circ)then
           count1=count1+1
           Nfn=count1
          end if
        enddo
          if(Nfn.lt.Nf)then
            Nfn=Nfn+1
          endif
        Ndata=Nfn-Ni+1


        return
        end

!===================================================================
!     poly_fit_input(poly_type,npol,ni,nf,Ndata,ypf,basis,nAtom)   !
!-------------------------------------------------------------------
!     Generate the basis which using by subroutine polyn_fit       !
!===================================================================
      subroutine polyn_fit_input(ni,Nfn,Ndata,basis,rr)
      use mecca_constants
      implicit none
!      include 'imp_inp.h'
      integer i,j,k,Ndata,ni,Nfn
      real*8 rpf(Nints),basis(npol+1,Nints)
      real*8 rr(nf)

      rpf(1:Ndata)=rr(ni:Nfn)
!C      ypf=input(2*nAtom,ni:nfn)

      do 20 k=1,Ndata
       basis(1,k)=1.0d0
       basis(2,k)=rpf(k)
       do 30 j=3,npol+1
        select case(poly_type)
        case(1) !simple polynomial
         basis(j,k)=rpf(k)**(DFLOAT(j-1))
        case(2) !Laguerre polynomial
         basis(j,k)=((2.0D0*DFLOAT(j)-1.0D0-rpf(k))*basis(j-1,k)-       &
     &               (DFLOAT(j)-1.0D0)*basis(j-2,k))/(DFLOAT(j))
        case(3) !Legendre polynomial
         basis(j,k)=((2.0d0*DFLOAT(j)-1.0d0)*rpf(k)*basis(j-1,k)-       &
     &               (DFLOAT(j)-1.0d0)*basis(j-2,k))/(DFLOAT(j))
        case(4) !Chebyshev polynomial
         basis(j,k)=(2.0D0*rpf(k)*basis(j-1,k))-basis(j-2,k)
        case(5)
         basis(j,k)=rpf(k)**(DFLOAT(j-1)/2.0D0)
        end select
   30  continue
   20 continue
      return
      end

!===================================================================
!     polyn_fit(y,npol,aL,g,n1,n2,maxQP)                               !
!-------------------------------------------------------------------
!     Generate the coefficient aL(npol+1).                         !
!===================================================================
        subroutine polyn_fit(y,aL,g,n2)
        use mecca_constants
        implicit double precision (a-h,o-z)
!        include 'imp_inp.h'
!       dimension x(jbig),y(jbig),g(npol+1,jbig),B(npol+1),
!     .             Acoef(npol+1,npol+1),aL(npol+1)
        real*8 y(n2),g(npol+1,n2),B(npol+1),                            &
     &             Acoef(npol+1,npol+1),aL(npol+1)

          do ii=1,npol+1
           aL(ii)=0.0d0
          enddo
!************************************************************************
          do i=1,npol+1
             sum=0.0d0
            do k=1,n2
             sum=sum+y(k)*g(i,k)
            enddo
              B(i)=sum
             do j=1,npol+1
              sum=0.0d0
              do k1=1,n2
               sum=sum+g(i,k1)*g(j,k1)
              enddo
               Acoef(i,j)=sum
               Acoef(j,i)=sum
             enddo     ! END of j-loop
          enddo     ! END of i-loop

            call Gauss(npol,Acoef,B,aL)
!*======================================================================= !==
            return
            end
!*======================================================================= !==
         subroutine Gauss(npol,A,B,aL)
         implicit double precision (a-h,o-z)
         dimension B(npol+1),A(npol+1,npol+1),aL(npol+1),L(npol+1),     &
     &              S(npol+1)

         do i=1,npol+1
           L(i)=i
           smax=0.0d0
           do j=1,npol+1
            smax=dmax1(smax,dabs(A(i,j)))
           enddo
            S(i)=smax
         enddo

          do 3 k=1,npol+1
            Rmax=0.0d0
            do 4 i1=k,npol+1
              R=dabs(A(L(i1),k))/S(L(i1))
              if(R.le.Rmax)go to 4
               j=i1
               Rmax=R
    4       continue
             Lk=L(j)
             L(j)=L(k)
             L(k)=Lk

               do i=k+1,npol+1
                xmult=A(L(i),k)/A(Lk,k)
                 do j=k+1,npol+1
                  A(L(i),j)=A(L(i),j)-xmult*A(Lk,j)
                 enddo
                   A(L(i),k)=xmult
                   B(L(i))=B(L(i))-A(L(i),k)*B(L(k))
               enddo
    3     continue            ! End of k-Loop
           aL(npol+1)=B(L(npol+1))/A(L(npol+1),npol+1)
            do i=npol,1,-1
              sum=B(L(i))
              do j=i+1,npol+1
                sum=sum-A(L(i),j)*aL(j)
              enddo
               aL(i)=sum/A(L(i),i)
            enddo
            return
            end
!===================================================================
!     polyn_fit_output(poly_typeinpol,jpf,a1pf,rX1,rmag,func)      !
!-------------------------------------------------------------------
!     Get the function value!                                      !
!===================================================================
      subroutine polyn_fit_output(ia,poly_type,npol,a1pf,rmag,func)

      implicit none
      integer ia,poly_type,jpf,npol
      real*8 a1pf(npol+1),rmag,basis0(npol+1),func,pi

      pi=4.0d0*datan(1.0d0)

      basis0(1)=1.0d0
      basis0(2)=rmag
      do 31 jpf=3,npol+1
       select case(poly_type)
       case(1) !simple polynomial
        basis0(jpf)=rmag**(DFLOAT(jpf-1))
       case(2) !Laguerre polynomial
        basis0(jpf)=((2.0D0*DFLOAT(jpf)-1.0D0-rmag)*basis0(jpf-1)-      &
     &               (DFLOAT(jpf)-1.0D0)*basis0(jpf-2))/(DFLOAT(jpf))
       case(3) !Legendre polynomial
        basis0(jpf)=((2.0d0*DFLOAT(jpf)-1.0d0)*rmag*basis0(jpf-1)-      &
     &               (DFLOAT(jpf)-1.0d0)*basis0(jpf-2))/(DFLOAT(jpf))
       case(4) !Chebyshev polynomial
        basis0(jpf)=(2.0D0*rmag*basis0(jpf-1))-basis0(jpf-2)
       case(5)
        basis0(jpf)=rmag**(DFLOAT(jpf-1)/2.0D0)
       end select
   31 continue

      func=dot_product(a1pf,basis0)
        func=func/(4.0d0*pi*rmag**ia)

      return
      end

!===================================================================
!            Isoparametric Integration for complex function        !
!-------------------------------------------------------------------
!    Generate the    int_{R_MT}^{VP} [fs(r)/(4*pi*r**ia)] d3r      !
!===================================================================
      subroutine isopar_integ_cmplx(ia,rmt_ovlp,rrs,fs,                 &
     &                        r_circ,fcount,weight,rmag,vj,VPInt)

!***********************************************************************
      use mecca_constants
      implicit none
!      include 'imp_inp.h'
!--[General veriables]-------------------------------------------------
        integer fcount
        integer ia,Ni,Nfn,Ndata
!--[input from Bernal]-------------------------------------------------
        real*8 rmt_ovlp,r_circ
!--[for face analysis]-------------------------------------------------
!--[for abscissas & weight]--------------------------------------------
        real*8 weight(MNqp)
!--[for Jacobian & print result]---------------------------------------
        integer i,j,k,I_face
        real*8 vjj,vj(MNqp,MNqp,MNqp,MNF),volume,                       &
     &         basis(npol+1,Nints)
        real*8 rmag(MNqp,MNqp,MNqp,MNF),rmagn
        real*8 rrs(nf)
        complex*16 fs(nf),ypf(Nints),a1pf(npol+1),func,VPInt(2)
!----------------------------------------------------------------------
        integer :: Nq1,Nq2,Nq3
        VPInt=dcmplx(0.0D0,0.0D0)

        !---[driver for polynomial fitting]---
        call polyn_fit_input0(Ni,Nfn,Ndata,r_circ,rmt_ovlp,rrs)

        call polyn_fit_input(Ni,Nfn,Ndata,basis,rrs)

         ypf(1:Ndata)=fs(Ni:Nfn)
         call polyn_fit_cmplx(ypf,a1pf,basis,Ndata)

        Nq1 = size(vj,1)
        Nq2 = size(vj,2)
        Nq3 = size(vj,3)
        do 200 I_face=1,fcount
         do 100 i=1,Nq1
         do 100 j=1,Nq2
         do 100 k=1,Nq3
           rmagn=rmag(i,j,k,I_face)
!c******************************************************************
!c   Sometimes we have low accuracy ==>
!c   Large numbers can be generated for vj(*,*,*,*)
!c******************************************************************
          if(abs(vj(i,j,k,I_face)).lt.100.0d0)then
            call polyn_fit_output_cmplx(ia,poly_type,npol,a1pf,         &
     &                    rmagn,func)
            VPInt(1)=VPInt(1)+dcmplx(weight(i)*weight(j)*weight(k)*     &
     &                   vJ(i,j,k,I_face),0.0D0)
            VPInt(2)=VPInt(2)+dcmplx(weight(i)*weight(j)*weight(k)*     &
     &                   vJ(i,j,k,I_face),0.0D0)*func
          endif
  100    continue
  200   continue

      return
      end

!===================================================================
!     polyn_fit(y,npol,aL,g,n1,n2,maxQP)  for complex functiom     !
!-------------------------------------------------------------------
!     Generate the complex coefficients aL(npol+1).                !
!===================================================================
        subroutine polyn_fit_cmplx(y,aL,g,n2)
        use mecca_constants
        implicit double precision (a-h,o-z)
!        include 'imp_inp.h'
!       dimension x(jbig),y(jbig),g(npol+1,jbig),B(npol+1),
!     .             Acoef(npol+1,npol+1),aL(npol+1)
        real*8     g(npol+1,n2)
        complex*16 y(n2),gc(npol+1,n2),B(npol+1)
        complex*16 Acoef(npol+1,npol+1),aL(npol+1)
        complex*16 sum
!        complex*16 czero
!        czero=dcmplx(0.0D0,0.0D0)

          do ii=1,npol+1
             aL(ii)=czero
            do jj=1,n2
             gc(ii,jj)=dcmplx(g(ii,jj),0.0D0)
            enddo
          enddo
!************************************************************************
          do i=1,npol+1
             sum=czero
            do k=1,n2
             sum=sum+y(k)*gc(i,k)
            enddo
              B(i)=sum
             do j=1,npol+1
              sum=czero
              do k1=1,n2
               sum=sum+gc(i,k1)*gc(j,k1)
              enddo
               Acoef(i,j)=sum
               Acoef(j,i)=sum
             enddo     ! END of j-loop
          enddo     ! END of i-loop

            call Gauss_cmplx(npol,Acoef,B,aL)
!*======================================================================= !==
            return
            end
!*======================================================================= !==
         subroutine Gauss_cmplx(npol,A,B,aL)
         implicit double precision (a-h,o-z)
         dimension L(npol+1)
         complex*16 B(npol+1),A(npol+1,npol+1),aL(npol+1)
         real(8) :: S(npol+1)
         complex*16 xmult,sum
!C****************************** Important****************************
!C         For real argument 'r' of the complex function f(r), the
!C         quantity A(npol+1,npol+1) are real, So the algorithm below
!C         works well.
!C****************************** Important****************************

         do i=1,npol+1
           L(i)=i
           smax=0.0d0
           do j=1,npol+1
            smax=dmax1(smax,cdabs(A(i,j)))
           enddo
            S(i)=smax
         enddo

          do 3 k=1,npol+1
            Rmax=0.0d0
            do 4 i1=k,npol+1
              R=cdabs(A(L(i1),k))/S(L(i1))
              if(R.le.Rmax)go to 4
               j=i1
               Rmax=R
    4       continue
             Lk=L(j)
             L(j)=L(k)
             L(k)=Lk

               do i=k+1,npol+1
                xmult=A(L(i),k)/A(Lk,k)
                 do j=k+1,npol+1
                  A(L(i),j)=A(L(i),j)-xmult*A(Lk,j)
                 enddo
                   A(L(i),k)=xmult
                   B(L(i))=B(L(i))-A(L(i),k)*B(L(k))
               enddo
    3     continue            ! End of k-Loop
           aL(npol+1)=B(L(npol+1))/A(L(npol+1),npol+1)
            do i=npol,1,-1
              sum=B(L(i))
              do j=i+1,npol+1
                sum=sum-A(L(i),j)*aL(j)
              enddo
               aL(i)=sum/A(L(i),i)
            enddo
            return
            end
!===================================================================
!     polyn_fit_output(poly_typeinpol,jpf,a1pf,rX1,rmag,func)      !
!-------------------------------------------------------------------
!     Get the function value!                                      !
!===================================================================
       subroutine polyn_fit_output_cmplx(ia,poly_type,npol,a1pf,        &
     &                                   rmag,func)

      implicit none
      integer :: ia,poly_type,jpf,npol
      real(8) :: rmag,basis0(npol+1),pi
      complex(8) :: a1pf(npol+1),func
      complex(8), parameter :: czero=dcmplx(0.0D0,0.0D0)

      pi=4.0d0*datan(1.0d0)

      basis0(1)=1.0d0
      basis0(2)=rmag
      do 31 jpf=3,npol+1
       select case(poly_type)
       case(1) !simple polynomial
        basis0(jpf)=rmag**(DFLOAT(jpf-1))
       case(2) !Laguerre polynomial
        basis0(jpf)=((2.0D0*DFLOAT(jpf)-1.0D0-rmag)*basis0(jpf-1)-      &
     &               (DFLOAT(jpf)-1.0D0)*basis0(jpf-2))/(DFLOAT(jpf))
       case(3) !Legendre polynomial
        basis0(jpf)=((2.0d0*DFLOAT(jpf)-1.0d0)*rmag*basis0(jpf-1)-      &
     &               (DFLOAT(jpf)-1.0d0)*basis0(jpf-2))/(DFLOAT(jpf))
       case(4) !Chebyshev polynomial
        basis0(jpf)=(2.0D0*rmag*basis0(jpf-1))-basis0(jpf-2)
       case(5)
        basis0(jpf)=rmag**(DFLOAT(jpf-1)/2.0D0)
       end select
   31 continue

       func=czero
       do jpf=1,npol+1
         func=func + a1pf(jpf)*dcmplx(basis0(jpf),0.0d0)
       enddo
       func=func/dcmplx(4.0d0*pi*rmag**ia,0.0D0)

      return
      end
