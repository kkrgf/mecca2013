!c =====================================================================
        subroutine simplmx(frn,fro,xvalwsnw,xvalws,                     &
     &                     vdif,vdold,komp,nbasis,mspn,ichg,alla,beta)
!c =====================================================================
!c        implicit none
       implicit real*8 (a-h,o-z)
!c
!c+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!      include 'mkkrcpa.h'
!c+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!c
!c+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
      real(8), parameter :: half=0.5d0, one=1.d0
!c+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!c
      integer  nbasis,mspn
      integer  komp(nbasis)
      integer  ib,ik,ir,ichg
!c
      real(8) ::   vdif,vdold,fa,fb,fc,fd,alla,beta
      real(8) ::   frn(:,:,:,:)  ! (iprpts,ipcomp,ipsublat,ipspin)
      real(8) ::   fro(:,:,:,:)  ! (iprpts,ipcomp,ipsublat,ipspin)
      real(8) ::   xvalwsnw(:,:,:)  ! (ipcomp,ipsublat,ipspin)
      real(8) ::   xvalws(:,:,:)  !   (ipcomp,ipsublat,ipspin)
!c
!c =====================================================================
!c subroutine to simple mix new and old potentials or charges ........
!c     nbasis    number of sublattices
!c     komp      number of compents/basis
!c     mspn      number of electron spins (1 or 2)
!c     alla      mixing parameter of average potential
!c     beta      mixing parameter of difference in potentials
!c     frn:      new      function(r)
!c     fro:      previous function(r)
!c =====================================================================
!c
        do 11 ib=1,nbasis
          do 10 ik=1,komp(ib)
            do  9 ir=1,size(frn,1)
              fa=half*(frn(ir,ik,ib,1)+frn(ir,ik,ib,mspn))
              fb=half*(fro(ir,ik,ib,1)+fro(ir,ik,ib,mspn))
              fc=half*(frn(ir,ik,ib,1)-frn(ir,ik,ib,mspn))
              fd=half*(fro(ir,ik,ib,1)-fro(ir,ik,ib,mspn))
!c
              frn(ir,ik,ib,mspn)= (one-alla)*fb + alla*fa               &
     &                           - (one-beta)*fd - beta*fc
              frn(ir,ik,ib,   1)= (one-alla)*fb + alla*fa               &
     &                           + (one-beta)*fd + beta*fc
   9        continue
  10      continue
  11    continue
!c
        if(ichg.gt.0) then
               vdif=(one-beta)*vdold + beta*vdif
        else
           do ib=1,nbasis
            do ik=1,komp(ib)
               fa = half*( xvalwsnw(ik,ib,1) +  xvalwsnw(ik,ib,mspn) )
               fb = half*( xvalws  (ik,ib,1) +  xvalws  (ik,ib,mspn) )
               fc = half*( xvalwsnw(ik,ib,1) -  xvalwsnw(ik,ib,mspn) )
               fd = half*( xvalws  (ik,ib,1) -  xvalws  (ik,ib,mspn) )
!c
              xvalwsnw(ik,ib,mspn)= (one-alla)*fb + alla*fa             &
     &                           - (one-beta)*fd - beta*fc
              xvalwsnw(ik,ib,   1)= (one-alla)*fb + alla*fa             &
     &                           + (one-beta)*fd + beta*fc
            enddo
           enddo
         endif
!c
          return
          end
!c =====================================================================
