!c  ==================================================================
      subroutine conTempr(Tempr,ebot,etop,egrd,dele1,                   &
     &                 npts1,npts2,iprint,istop)
!c  ==================================================================
!c
!c     constructs energy contour for Temperature integration
!c
!c           ! Im(e)             .   .
!c           !               .           .    x
!c           !            .                 .
!c           !          .                     ._______ eTempr=pi/2*Tempr
!c           !        .
!c           !       .
!c          -!-----------------------------------------------
!c           !     ebot        Real(e)         etop
!c
!c     Tempr     : temperature
!c     ebot      : bottom of the contour on the real axis.............
!c     etop      : top of the contour on the real axis.............
!c     npts1     : number of points with Re(En)<etop (circular contur)
!c     npts2     : number of points with Im(En)>etop (linear contour)
!c     iprint    : print level control (>0 for print of energies)..
!c
!c     egrd[1:npts1+npts2]   : En           (output)
!c     dele1[1:npts1+npts2]  : wieght       (output)
!c
!c  *******************************************************************
!c
      implicit   none
!c
      real*8  Tempr,ebot,etop

      complex*16 egrd(*)
      complex*16 dele1(*)
      integer npts1,npts2

      integer iprint
      character  istop*10

      integer    ie,n2,nume
      real*8     eband
!c      real*8     delta,delta1,step
      real*8     pi,pitempr,atan,erad,alpha,beta
      real*8     eTempr
!CALAM      real*8     eTempr,ztmpm
      complex*16 TemprFun,dcmplx,e0,ztmp

!c parameters
      character  sname*10
      parameter  (sname='conTempr')
      real*8     zero,half,one
      parameter  (zero=0.d0,half=0.5d0,one=1.d0)
      complex*16 sqrtm1
      parameter  (sqrtm1=(0.d0,1.d0))
!c
!c  ==================================================================

      eband = etop-ebot
      if(eband.le.zero) then
       write(6,*) ' ebot=',ebot
       write(6,*) ' etop=',etop
       call fstop(sname//': etop.LT.ebot???')
      end if

      pi = 4.d0*atan(one)

      if(Tempr.gt.zero) then

       pitempr = max(zero,pi*Tempr)         !  first Matsubara pole
       eTempr=pitempr/2                     !

!CALAM       ztmpm = 10.d0*log(10.)
!c       eTplus=ztmpm*max(zero,Tempr)  !  maximum "energy" with non-zero
                                            !  f*log(f)

       erad = half*sqrt(eband**2+eTempr**2) ! radius of the circle
       beta =  atan(eTempr/eband)           ! angle between circle diameter
                                            ! and real axis

      else                              ! T = 0

       eTempr = zero
       beta = zero
       erad = half*eband
       npts2 = 0

      end if

!c
!c  circular part of the contour
!c
      e0 = dcmplx(ebot+half*eband,half*eTempr)  ! center of the circle
      alpha = beta+pi                           ! "left" angle

      call conlegndr(e0,erad,alpha,beta,npts1,egrd,dele1)

      nume = npts1
!c
      if(eTempr.gt.zero) then

       n2 = (npts2+1)/2
       npts2 = n2*2
!c
!c     E<etop
!c

       e0 = dcmplx(etop,eTempr)
       call conlagr(-Tempr,e0,n2,egrd(nume+1),dele1(nume+1))
!c
!c  takes into account Fermi function
!c
       do ie=nume+1,nume+n2
        ztmp = (egrd(ie)-etop)/Tempr
        dele1(ie) = dele1(ie)*(TemprFun(ztmp,0)-one)
       end do
       nume = nume + n2
!c
!c      E>etop
!c
       call conlagr(Tempr,e0,n2,egrd(nume+1),dele1(nume+1))
!c
!c  takes into account Fermi function
!c
       do ie=nume+1,nume+n2
        ztmp = (egrd(ie)-etop)/Tempr
        dele1(ie) = dele1(ie)*TemprFun(ztmp,0)
       end do
       nume = nume + n2
      end if

!c
!c  ===================================================================
!c  write out grid and return.......................................
!c
!c       if(iprint.ge.-10) then
!c        write(6,'(''  Bottom of circ-contour'',t40,''='',f10.5)') ebot
!c        write(6,'(''  Top of circ-contour'',t40,''='',f10.5)') etop
!c        if(eTempr.gt.zero) then
!c       write(6,'(''  Matsubara line-contour at '',t40,''='', f10.5)') e !Tempr
!c       write(6,'(''  Top of line-contour'',t40,''='',
!c     *                              f10.5)') dreal(egrd(npts1+npts2))
!c        end if
!c        write(6,'(''  Min. Imag(Egrd) :'',t40,''='',
!c     *                               f10.5)') dimag(egrd(npts1+npts2))
!c        write(6,'(''  No. of energies : circ-part'',t40,''='',i4)') npt !s1
!c        if(npts2.gt.0) then
!c        write(6,'(''                  : Laguerr lin-part'',t40,
!c     >           ''='',i4)') npts2
!c        end if
!c
!c        if(iprint.ge.1) then
!c          write(6,'('' n='',i5,'' e='',2e12.4,'' de='',2d12.4)')
!c     >    (ie,egrd(ie),dele1(ie),ie=1,npts1+npts2)
!c        endif
!c       end if
!c
!c  ===================================================================
!c
      if(istop.eq.sname) then
         call fstop(sname)
      else
         return
      endif
!c
      end
!c
!c     ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      subroutine conlegndr(e0,erad,alpha,beta,nume,egrd,dele1)
!c     ===============================================================
!c
!c     constructs part of circle contour
!c
!c                               . ! .
!c                           .     !     .
!c                       .         !         .
!c                     .           !
!c                   .             !
!c                                 !
!c          -----------------------!-------------------------
!c                                e0
!c
!c
!c     e0        : center of the circle
!c     erad      : radius of the circle
!c     alpha     : "bottom" angle
!c     beta      : "top" angle
!c               :  for semi-circle -- alpha=-pi, beta=zero
!c     nume      : number of points for Gauss-Legendre quadrature
!c
!c     output    : (egrd(ie),dele1(ie),ie=1,nume)
!c
!c     ================================================================
!c
      implicit   none
!c
!c
      complex*16 e0
      real*8     erad,alpha,beta
      integer    nume
      complex*16 egrd(*)
      complex*16 dele1(*)
!c
      integer    ie
!c
      integer ipts,ipdim
      parameter (ipts=200,ipdim=300)

      real*8     xgs(ipdim)
      real*8     wgs(ipdim)
      real*8  phi,phi0,dphi
      complex*16 zzero
!c
!c parameters
!c
      real*8     zero,one
      parameter  (zero=0.d0,one=1.d0)
      complex*16 sqrtm1
      parameter  (sqrtm1=(0.d0,1.d0))
      character  sname*10
      parameter  (sname='conlegndr')
!c
!c     ================================================================
!c
      if(nume.gt.ipts) then
       write(6,'(a10,'':: nume.gt.ipts'',2i5)') nume,ipts
       call fstop(sname)
      endif
!c     ----------------------------------------------------------------
      call gauleg(-one,one,xgs,wgs,nume)
!c     ----------------------------------------------------------------
!c
!c     loop over energy grid for gaussian integration.................

      phi0 = (beta+alpha)/2
      dphi = (beta-alpha)/2

      if(abs(dphi).lt.1.d-10) then
       do ie=1,nume
        egrd(ie) = e0 + erad*xgs(ie)
        dele1(ie)=dcmplx(wgs(ie)*erad,zero)
       enddo
      else
       do ie=1,nume
        phi =  phi0 + dphi*xgs(ie)
        zzero =  erad*exp( sqrtm1*phi )
        egrd(ie) = e0 + zzero
        dele1(ie)=sqrtm1*dphi*zzero*wgs(ie)
       enddo
      end if
!c
      return
!c
      end

!c     ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      subroutine conlagr(Tempr,e0,ngauss,egrd,dele1)
!c     ===============================================================
!c
!c     constructs Gauss-Laguerr linear energy contour
!c
!c     e0        : starting point
!c     ngauss    : total number of desired points and weights for...
!c                 Gauss-Laguerre quadrature.......................
!c     output    : (egrd(ie),dele1(ie),ie=1,ngauss)
!c
!c     ================================================================
!c
      implicit   none
!c
      real*8     Tempr
      complex*16 e0
      integer    ngauss
      complex*16 egrd(*)
      complex*16 dele1(*)

      integer ipts
      parameter (ipts=100)
      integer    ie
      real*8     xgs(ipts)
      real*8     wgs(ipts)
      real*8     exp
      complex*16 dcmplx
!c
!c     +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
      character  sname*10
      parameter  (sname='conlagr')
!c     +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

      if(ngauss.gt.ipts) then
       write(6,'(a10,'':: ngauss.gt.ipts'',2i5)')                       &
     &       sname,ngauss,ipts
       call fstop(sname)
      endif

      call lagzo(ngauss,xgs,wgs)

      if(Tempr.ge.0.d0) then
       do ie=1,ngauss
        egrd(ie) = e0 + Tempr*xgs(ie)
        dele1(ie)= dcmplx(abs(Tempr)*exp(xgs(ie))*wgs(ie),0.d0)
       enddo
      else
       do ie=1,ngauss
        egrd(ngauss+1-ie) = e0 + Tempr*xgs(ie)
        dele1(ngauss+1-ie)= dcmplx(abs(Tempr)*exp(xgs(ie))*wgs(ie),0.d0)
       enddo
      end if

      return
      end
!c

        SUBROUTINE LAGZO(N,X,W)
!C
!C       =========================================================
!C       Purpose : Compute the zeros of Laguerre polynomial Ln(x)
!C                 in the interval [0,�], and the corresponding
!C                 weighting coefficients for Gauss-Laguerre
!C                 integration
!C       Input :   n    --- Order of the Laguerre polynomial
!C                 X(n) --- Zeros of the Laguerre polynomial
!C                 W(n) --- Corresponding weighting coefficients
!C       =========================================================
!C
        IMPLICIT DOUBLE PRECISION (A-H,O-Z)
        DIMENSION X(N),W(N)
        HN=1.0D0/N
        DO NR=1,N
           IF (NR.EQ.1) Z=HN
           IF (NR.GT.1) Z=X(NR-1)+HN*NR**1.27
           IT=0
10         IT=IT+1
           Z0=Z
           P=1.0D0
            DO  I=1,NR-1
              P=P*(Z-X(I))
            ENDDO
           F0=1.0D0
           F1=1.0D0-Z
            DO K=2,N
              PF=((2.0D0*K-1.0D0-Z)*F1-(K-1.0D0)*F0)/K
              PD=K/Z*(PF-F1)
              F0=F1
              F1=PF
            ENDDO
           FD=PF/P
           Q=0.0D0
            DO I=1,NR-1
              WP=1.0D0
              DO 25 J=1,NR-1
                 IF (J.EQ.I) GO TO 25
                 WP=WP*(Z-X(J))
25            CONTINUE
              Q=Q+WP
            ENDDO
           GD=(PD-Q*FD)/P
           Z=Z-FD/GD
           IF (IT.LE.40.AND.DABS((Z-Z0)/Z).GT.1.0D-15) GO TO 10
           X(NR)=Z
           W(NR)=1.0D0/(Z*PD*PD)
        ENDDO
        RETURN
        END

!cSUBR==END-CONTEMPR===================================================
