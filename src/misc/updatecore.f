      subroutine updatecore(nspin,nbasis,komp,atcon,numbsub,            &
     &                      efermi,tempr,                               &
     &                      numc,nc,lc,kc,ec,qc,                        &
     &                      ztotss,zvalss,zcortot,d_zcortot,            &
     &                      zvalav,zcorav)
!c     ==================================================================
!c     update core orbital occupations with changing efermi, assuming
!c     frozen orbitals
!c     ==================================================================
      implicit real*8 (a-h,o-z)

      include 'mkkrcpa.h'

      integer   komp(ipsublat)
      real*8    atcon(ipcomp,ipsublat)
      integer   numbsub(nbasis)

      integer   numc(ipcomp,ipsublat,ipspin)
      integer   nc(ipeval,ipcomp,ipsublat,ipspin)
      integer   lc(ipeval,ipcomp,ipsublat,ipspin)
      integer   kc(ipeval,ipcomp,ipsublat,ipspin)
      real*8    ec(ipeval,ipcomp,ipsublat,ipspin)
      real*8    qc(ipeval,ipcomp,ipsublat,ipspin)

      real*8    ztotss(ipcomp,ipsublat)
      real*8    zvalss(ipcomp,ipsublat)
      real*8    zcortot(ipcomp,ipsublat)
      real*8    d_zcortot(ipcomp,ipsublat)
!c     ==================================================================

      zvalold=0.0d0
      zcorold=0.0d0
      do nsub=1,nbasis
        do ic=1,komp(nsub)
          concen=atcon(ic,nsub)*numbsub(nsub)
          zcorold = zcorold+concen*zcortot(ic,nsub)
          zvalold = zvalold+concen*zvalss(ic,nsub)
        enddo
      enddo


      zvalav=0.0d0
      zcorav=0.0d0

      do nsub=1,nbasis
        do ic=1,komp(nsub)
          concen=atcon(ic,nsub)*numbsub(nsub)

          zcortot(ic,nsub)=0.0d0
          d_zcortot(ic,nsub)=0.0d0
          do ispn=1,nspin
            mspn=3-nspin

            do j=1,numc(ic,nsub,ispn)
              fermi=fermidist(ec(j,ic,nsub,ispn),efermi,tempr)

              occ=fermi*mspn*iabs(kc(j,ic,nsub,ispn))
              zcortot(ic,nsub)=zcortot(ic,nsub)+occ*qc(j,ic,nsub,ispn)

              d_occ=0.0d0
              if(tempr.gt.0.0d0) d_occ=( fermi*(1.0d0-fermi) )/tempr

              d_zcortot(ic,nsub)=d_zcortot(ic,nsub)                     &
     &                          +d_occ*qc(j,ic,nsub,ispn)
            enddo

          enddo
          zvalss(ic,nsub) = ztotss(ic,nsub)-zcortot(ic,nsub)

          zcorav = zcorav+concen*zcortot(ic,nsub)
          zvalav = zvalav+concen*zvalss(ic,nsub)
        enddo
      enddo

      write(6,'('' subroutine updatecore:'')')
      write(6,'('' zcorold='',1p,e13.6,'' zvalold='',e13.6)')           &
     &      zcorold,zvalold
      write(6,'('' zcorav ='',1p,e13.6,'' zvalav ='',e13.6)')           &
     &      zcorav,zvalav

      return
      end
