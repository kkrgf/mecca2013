      SUBROUTINE TIMEL(TIME)
      REAL*8 TIME

!c_lcp305       character*9 buf
!c_lcp305       real*4 last,secnds,tim,time0
!c_lcp305       real*8 dreal
!c_lcp305       data i/0/
!c_lcp305       save i,time0
!c_lcp305       last=0.
!c_lcp305       if(i.eq.0) then
!c_lcp305        i=1
!c_lcp305        tim = secnds(last)
!c_lcp305        call date(buf)
!c_lcp305        write(*,1) buf
!c_lcp3051       format(10x,'----> CURRENT DATE is  ',a9,' <----')
!c_lcp305        time = 0.d0
!c_lcp305        time0 = tim
!c_lcp305        return
!c_lcp305       end if
!c_lcp305       tim = secnds(last)
!c_lcp305       time = dreal(tim - time0)

!c_decalpha&cpux3&hp&irix   REAL ARR(2)
!c_decalpha&cpux3&hp&irix   XT=ETIME(ARR)
!c_decalpha&cpux3&hp&irix   TIME=ARR(1)

!c_vpp        call clock(time,0,2)

!c_ibm/aix        real*4 elapsed
!c_ibm/aix        type tb_type
!c_ibm/aix         sequence
!c_ibm/aix         real*4 usrtime
!c_ibm/aix         real*4 systime
!c_ibm/aix        end type
!c_ibm/aix        type(tb_type)etime_struct
!c_ibm/aix
!c_ibm/aix        elapsed = etime_(etime_struct)
!c_ibm/aix        time = (elapsed)

!cf90        integer icount,irate,ihuge,isave,ncount
!cf90
!cf90        save isave
!cf90
!cf90        call system_clock(icount,irate,ihuge)
!cf90
!cf90        if(time.le.0.d0) then
!cf90         isave = icount
!cf90         time = nearest(0.d0,1.d0)
!cf90        else
!cf90         ncount = icount-isave
!cf90         isave = icount
!cf90         if(ncount.lt.0) ncount = ncount+ihuge+1
!cf90         time = dfloat(ncount)/dfloat(irate)
!cf90        end if

      REAL ARR(2)
      XT=ETIME(ARR)
      TIME=ARR(1)

      return
      END
