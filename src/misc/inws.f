!c
!c ======================================================================
      subroutine inws (ipdeq,invp,nmax,rg,rf,rv,r,en,c,drg,drf,         &
     &                 dk,dm,nws,imm)
!c ======================================================================
!c
!c adams 5 points  diff. eq. solver for dirac equations
!c
!c **********************************************************************
!      implicit real*8(a-h,o-z)
      use mecca_constants
      implicit none
!c ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!      include 'mkkrcpa.h'
!c ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!c
!c ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
      integer, parameter :: ipdeq2=10
      real(8), parameter :: twntsvn=three**3
      real(8), parameter :: f02=five*ten*ten + two
      real(8), parameter :: f75=f02 - twntsvn
      real(8), parameter :: sixhnd=six*ten*ten
      real(8), parameter :: coef2=twntsvn/f02,coef1=f75/f02
!c     coef1=475./502., coef2=27./502.
!c     parameter (coef1=.9462151394422310d0,coef2=.0537848605577689d0)
!c ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!c
!c    drg,drf enrivatives of dp and dq times r;  c speed of light
!c    rv potential times r; r: radial grid; en energy guess
!c *********************************************************************
!c
      integer :: ipdeq, invp, nmax
      real(8) :: r(:),rv(:),rg(:),rf(:)
      real(8) :: en,c
      real(8) :: drf(:),drg(:)
      real(8) :: dk,dm
      integer :: nws,imm

      integer :: j,i,itop,l
      real(8) :: p,pq,dpr,dqr,dp,dq,em,er,emvoc
!c
!c at first find the last point where the wavefcns are not zero
!c **********************************************************************
!c
      if( imm .eq. 1 ) go to 20
!c
      do 10 j=1,nws,3
        nmax=nws+1-j
!c
        if( (rv(nmax)-en*r(nmax))*r(nmax) .le. sixhnd ) go to 20
!c
  10  continue
!c
!c initial values for inward integration
!c **********************************************************************
  20  p=sqrt(-en*(one+en/(c*c)))
      pq=-p/(c+en/c)
      do 30 i=1,ipdeq
        j=nmax+1-i
        rg(j)=dexp(-p*r(j))
        drg(i)=-p*rg(j)*r(j)
        rf(j)=pq*rg(j)
        drf(i)=pq*drg(i)
  30  continue
      itop=nmax-ipdeq
!c
!c     solve dirac equations now
!c **********************************************************************
      do 50 i=invp,itop
        j=itop-i+invp
!c
!c     5 points predictor
!c
        dpr= rg(j+1) - dm*(2.51d+02*drg(1)-1.274d+03*drg(2)+            &
     &       2.616d+03*drg(3)-2.774d+03*drg(4)+1.901d+03*drg(5) )
        dqr= rf(j+1) - dm*(2.51d+02*drf(1)-1.274d+03*drf(2)+            &
     &       2.616d+03*drf(3)-2.774d+03*drf(4)+1.901d+03*drf(5) )
!c
!c     shift derivatives
!c
      do 40 l=2,ipdeq
        drg(l-1)=drg(l)
        drf(l-1)=drf(l)
  40  continue
!c
!c     dirac equations (log. mesh)
!c
        er=en*r(j)
        emvoc=(er-rv(j))/c
        drg(ipdeq)=-dk*dpr+(c*r(j)+emvoc)*dqr
        drf(ipdeq)=dk*dqr-emvoc*dpr
!c
!c     5 points corrector
!c
        dp= rg(j+1) - dm*(-1.9d+01*drg(1)+1.06d+02*drg(2)               &
     &              -2.64d+02*drg(3)+ 6.46d+02*drg(4)+2.51d+02*drg(5) )
!c
        dq= rf(j+1) - dm*(-1.9d+01*drf(1)+1.06d+02*drf(2)               &
     &              -2.64d+02*drf(3)+ 6.46d+02*drf(4)+2.51d+02*drf(5) )
!c
!c     mixing
!c
        dp=coef1*dp+coef2*dpr
        dq=coef1*dq+coef2*dqr
        rg(j)=dp
        rf(j)=dq
!c
!c     update derivative
!c
        drg(ipdeq)=-dk*dp+(c*r(j)+emvoc)*dq
        drf(ipdeq)=dk*dq-emvoc*dp
!c **********************************************************************
  50  continue
!c
      return
      end
