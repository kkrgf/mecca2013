!c     cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      subroutine zplaneint1(nspin,nrelv,icryst,efermi,iset_b_cont,      &
     &                    lmax,kkrsz,nbasis,numbsub,                    &
     &                    alat,omegws,aij,itype,natom,if0,              &
     &                    mapstr,mappnt,ndimbas,mapij,                  &
     &                    komp,atcon,ztotss,                            &
     &                    ivpoint,ikpoint,                              &
     &                    vr,vshift,h,rmt,rws,jmt,jws,xr,rr,            &
     &                    rmt_true,r_circ,ivar_mtz,fcount,weight,       &
     &                    rmag,vj,iVP,                                  &
     &                    nume,egrd,dele1,                              &
     &                    nitcpa,                                       &
     &                    nkns,ndx,                                     &
     &                    nqpt,                                         &
     &                    xknlat,ndimks,Rksp0,tmparr,                   &
     &                    rsn,nrsn,ndimrs,                              &
     &                    np2r,ndimnp,numbrs,naij,                      &
     &                    rot,dop,nop,                                  &
     &                    hplnm,ndimrhp,ndimlhp,                        &
     &                    cfac,cgaunt,nj3,ij3,                          &
     &                    qmesh,ndimq,nmesh,                            &
     &                    lwght,kmatchphase,lrot,                       &
     &                    ngrp,kptgrp,kptset,ndrot,kptindx,             &
     &                    doslast,doscklast,dosint,dosckint,            &
     &                    dostspn,                                      &
     &                    greenint,greenlast,evalsum,                   &
     &                    eta,enlim,                                    &
     &                    conk,conr,                                    &
     &                    rslatt,kslatt,Rnncut,Rsmall,mapsprs,          &
     &                    V0,                                           &
     &                    bsf_ksamplerate,bsf_fileno,                   &
     &                    bsf_nkwaypts,bsf_kwaypts,                     &
     &                fermi_korig,fermi_nkpts,fermi_kpts,               &
     &                fermi_kptlabel,fermi_orilabel,                    &
     &                fermi_gridsampling,fermi_raysampling,fermi_fileno,&
     &                cotdl,mtasa,ksintsch,isDLM,isflipped,flippoint,   &
     &                iprint,istop)
!c     cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
!c
      implicit real*8(a-h,o-z)
!c
!c     ****************************************************************
!c     this routine solves the cpa equations for a mesh of energies
!c     stored in egrd..............................................
!c     return  :: cpa solutions and n(e) etc.......................
!c     ****************************************************************
!c
!c     ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
      include 'lmax.h'
      include 'mkkrcpa.h'
      include 'imp_inp.h'
!c     ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!c
      character    istop*10
      character    sname*10
!c      character    namk*32
!c
      integer nbasis,numbsub(nbasis)
      integer      ivpoint(ipsublat)
      integer      ndx(*)
      integer      ikpoint(ipcomp,ipsublat)
      integer      komp(ipsublat)
      integer      jmt(ipcomp,ipsublat)
      integer      jws(ipcomp,ipsublat)
      integer      ij3(ipkkr*ipkkr*ipkkr)
      integer      nj3(ipkkr*ipkkr)
      integer      nume
      integer      nitcpa
      integer      nmesh,nkns(nmesh)
      integer      nrsn
!cALAM      integer      npls
      integer      nqpt(nmesh)
      integer      natom,itype(natom)
      integer      if0(48,natom)
      integer      mapstr(ndimbas,nbasis),mappnt(ndimbas,nbasis)
      integer      mapij(2,*)
      integer      np2r(ndimnp,*),numbrs(*),naij
      integer    fcount(nbasis)
!c
      real*8     rmt_true(ipcomp,nbasis)
      real*8     r_circ(ipcomp,nbasis)
      real*8     weight(MNqp)
      real*8     rmag(MNqp,MNqp,MNqp,MNF,nbasis)
      real*8     vj(MNqp,MNqp,MNqp,MNF,nbasis)

!c
      real*8       qmesh(3,ndimq,nmesh)
      integer      lwght(ndimq,nmesh),lrot(ndimq,nmesh)
      integer      kmatchphase(ndimq,nmesh)
      integer      ngrp(nmesh),kptgrp(ndrot+1,nmesh)
      integer      kptset(ndrot,nmesh),kptindx(ndimq,nmesh)
      real*8       evalsum(ipcomp,ipsublat,ipspin)
!c      real*8       rot(48,3,3)
      real*8       atcon(ipcomp,ipsublat)
      real*8       ztotss(ipcomp,ipsublat)
      real*8       rmt(ipcomp,ipsublat)
      real*8       rws(ipcomp,ipsublat)
!c      real*8       aij(3*ipbase*ipbase)
      real*8       aij(*)
      real*8       rsn(ndimrs,4)
      real*8       cgaunt(*)
      real*8       xknlat(ndimks,3,nmesh)
      real*8       tmparr(*)
      real*8       vr(iprpts,ipcomp,ipsublat,ipspin)
      real*8       xr(iprpts,ipcomp,ipsublat)
      real*8       rr(iprpts,ipcomp,ipsublat)
!c      real*8       xstart(ipcomp,ipsublat)
      real*8       h(ipcomp,ipsublat)
      real*8       alat,omegws
      real*8       vshift(2)
      real*8       clight
      real*8       eta
      real*8       conk
!c parameter
      real*8       cphot

      ! bsf
      integer bsf_ksamplerate,bsf_nkwaypts
      real*8 :: bsf_kwaypts(3,ipbsfkpts)
      integer bsf_fileno

      logical :: isDLM(ipsublat)
!c
      complex*16   egrd(nume)
      complex*16   dele1(nume)
      complex*16   dop(*)
      complex*16   hplnm(ndimrhp,ndimlhp)
      complex*16   cfac(ipkkr*ipkkr)
      complex*16   conr(*)
      complex*16   dos(0:iplmax,ipcomp,ipsublat,ipspin)
      complex*16   dosck(ipcomp,ipsublat,ipspin)
      complex*16   doslast(ipcomp,ipsublat,ipspin)
      complex*16   doscklast(ipcomp,ipsublat,ipspin)
      complex*16   dosint(ipcomp,ipsublat,ipspin)
      complex*16   dosckint(ipcomp,ipsublat,ipspin)
      complex*16   dostspn(2,*)
      complex*16   green(iprpts,ipcomp,ipsublat,ipspin)
      complex*16   greenint(iprpts,ipcomp,ipsublat,ipspin)
      complex*16   greenlast(iprpts,ipcomp,ipsublat,ipspin)
      complex*16   energy,dostot,energysp(2)
      complex*16   cotdl(iplmax+1,ipsublat,ipcomp,ipspin,nume)

      integer ksintsch ! k-space integration scheme
                       ! 1 = monkhorst-pack
                       ! 2 = ray method

      real*8 fermi_korig(3)
      real*8 fermi_kpts(3,ipbsfkpts)
      integer fermi_nkpts,fermi_gridsampling,fermi_raysampling
      integer fermi_fileno(2)
      character*32 :: fermi_kptlabel(ipbsfkpts)
      character*32 :: fermi_orilabel

      real*8 rot(49,3,3)

      real*8 Rnncut,Rsmall
      real*8 rslatt(3,3)           ! Alat*rslatt(1..3,*)/(2*pi) -- latt.vect.
      real*8 kslatt(3,3)
      integer mapsprs(natom,natom)
      real*8 V0,V0sp,efermi

      logical isfirstpass
      complex*16  dos_save(0:iplmax,ipcomp,ipsublat)
      complex*16  dosck_save(ipcomp,ipsublat)
      real*8  dossc(ipcomp,ipsublat)
      real*8  dostotup

      integer in0

      logical :: isflipped(ipsublat)
      integer :: flippoint(ipsublat)

      logical, save :: opendos = .true.
      integer :: dfo = 1000
      character*100 :: dfs
!      integer :: dfo(ipcomp,ipsublat)

      integer from_prcs,itag1,itag2,itag3,itag4
      parameter (itag1=10,itag2=20,itag3=30,itag4=40)

!c
!c     ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
      parameter    ( sname='zplaneint1')
      parameter    ( cphot=274.072d0)
!c     ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!c
!c     ****************************************************************
!c     Note:
!c             In semi-relativistic case p=sqrt[e(1+e/cphot**2)]
!c     ================================================================

      clight=cphot*10.0d0**nrelv
!     if(ME.eq.MASTER) then
!      if(iprint.ge.5) then
!        write(6,'('' in mp_zplanint iprint '',i10 )') iprint
!        write(6,'('' zplanint::               nrelv='',i5)')
!    >         nrelv
!        write(6,'('' zplanint::              clight='',d12.4)')
!    >         clight
!        write(6,'('' zplanint:: start:  lmax,nbasis='',2i5)')
!    >         lmax,nbasis
!        write(6,'('' zplanint:: start: nume,iprint='',2i5)')
!    >         nume,iprint
!        write(6,'('' zplanint:: start:          jmt='',4i5)')
!    >         ((jmt(ic,nsub),ic=1,komp(nsub)),nsub=1,nbasis)
!        write(6,'('' zplanint:: start:          jws='',4i5)')
!    >         ((jws(ic,nsub),ic=1,komp(nsub)),nsub=1,nbasis)
!      endif
!     endif


!      If DOS scan, create the neccesary files

       if(istop.eq.'DOS_CALC'.or.istop.eq.'DOSBSF_CAL') then
       if(opendos) then

        open(dfo,file='dos_all',status='unknown')
        write(dfo,'(a,f18.10,a,f12.8,a,/)') '# efermi = ',              &
     &       efermi, " + ",dimag(egrd(1)),' i'
        if(nspin == 2) then
          write(dfo,'(a)') '#      energy(ryd)        total/cell'       &
     &//'           up/cell           dn/cell'
        else
          write(dfo,'(a)') '#      energy(ryd)        total/cell'
        end if

        do in=1,nbasis
        do ic=1,komp(in)
          write(dfs,'(a,i2.2,a,i2.2)') 'dos_s',in,'c',ic
          open(dfo+10*in+ic,file=trim(dfs),status='unknown')
          write(dfo+10*in+ic,'(a,f18.10,a,f12.8,a,/)') '# efermi = ',   &
     &        efermi,' + ',dimag(egrd(1)),' i'
          if(nspin == 2) then
          write(dfo+10*in+ic,'(a)') '#      energy(ryd)'                &
     &//'       sublattice '                                            &
     &//'            s (up)            p (up)            d (up)'        &
     &//'            f (up)            s (dn)            p (dn)'        &
     &//'            d (dn)            f (dn)'
          else
           write(dfo+10*in+ic,'(a)') '#      energy(ryd)'               &
     &//'       sublattice '                                            &
     &//'            s                 p                 d     '        &
     &//'            f     '
          end if

        end do; end do

        opendos = .false.

       endif; end if


!c     ================================================================
!c     solve cpa equations over the energy mesh........................
!c     ----------------------------------------------------------------
      call zerooutC(dostspn,2*nume)
!c     ----------------------------------------------------------------
      call zerooutC(dosint,ipcomp*ipsublat*ipspin)
!c     ----------------------------------------------------------------
      call zerooutC(dosckint,ipcomp*ipsublat*ipspin)
!c     ----------------------------------------------------------------
      call zerooutC(greenint,iprpts*ipcomp*ipsublat*ipspin)
!c     ----------------------------------------------------------------
!CDDJ  in ZPLANEWD:   ZERO out of EVALSUM  (THIS is CORRECT!)
      call zeroout(evalsum,ipcomp*ipsublat*ipspin)
!c     ----------------------------------------------------------------
!c
!c======================================================================= !======
!C   COUNT the no. of atoms in unit cell excluding EMPTY sphere (if any)
!c======================================================================= !======
      natomcnt = 0
      do nsub = 1,nbasis
       zatom = 0
       do ik=1,komp(nsub)
        zatom = zatom + atcon(ik,nsub)*ztotss(ik,nsub)
       end do
!c  to exclude empty spheres
       if(zatom.gt.1.d-7) natomcnt=natomcnt+numbsub(nsub)
      end do

      do ie=1,nume
       do is=1,nspin
!c           ----------------------------------------------------------
            energy=egrd(ie)+vshift(is)
            energysp(is)=energy
            V0sp = V0+vshift(is)
!c           ----------------------------------------------------------
            isfirstpass = .true.
1000        continue
            call gettau(is,nspin,nrelv,clight,                          &
     &                  icryst,lmax,kkrsz,nbasis,numbsub,               &
     &                  alat,omegws,aij,itype,natom,                    &
     &                  if0,mapstr,mappnt,ndimbas,mapij,                &
     &                  komp,atcon,                                     &
!cALAM     *                  npls,
     &                  ij3,nj3,cgaunt,cfac,                            &
     &                  xknlat,ndimks,Rksp0,tmparr,nkns,                &
     &                  qmesh,ndimq,nmesh,nqpt,                         &
     &                  lwght,kmatchphase,lrot,                         &
     &                  ngrp,kptgrp,kptset,ndrot,kptindx,               &
     &                  ivpoint,ikpoint,                                &
     &                  vr(1,1,1,is),h,rmt,rws,jmt,jws,xr,rr,           &
     &                  rmt_true,r_circ,ivar_mtz,fcount,weight,         &
     &                  rmag,vj,iVP,                                    &
     &                  energy,                                         &
     &                  nitcpa,rsn,nrsn,ndimrs,                         &
     &                  np2r,ndimnp,numbrs,naij,                        &
     &                  ndx,rot,dop,nop,                                &
     &                  hplnm,ndimrhp,ndimlhp,                          &
     &                  dos(0,1,1,is),dosck(1,1,is),green(1,1,1,is),    &
     &                  eta,enlim,                                      &
     &                  conk,conr,                                      &
     &                  rslatt,kslatt,Rnncut,Rsmall,mapsprs,            &
     &                  V0sp,                                           &
     &                  bsf_ksamplerate,bsf_fileno,                     &
     &                  bsf_nkwaypts,bsf_kwaypts,                       &
     &                  fermi_korig,fermi_nkpts,fermi_kpts,             &
     &                  fermi_kptlabel,fermi_orilabel,                  &
     &              fermi_gridsampling,fermi_raysampling,fermi_fileno,  &
     &                  cotdl(1,1,1,is,ie),mtasa,ksintsch,efermi,       &
     &                  iprint,istop)
       enddo

!cSUFF .. Perform DLM symmetrization if necessary
!c No more averaging. The 2nd component is meant to be a flipped 1st.
       do in = 1,nbasis
       if( isDLM(in) ) then
         do ic = 1,komp(in)/2
           ik = komp(in)/2+ic
           do il = 0,iplmax
             dos(il,ik,in,2) = dos(il,ic,in,1)
             dos(il,ik,in,1) = dos(il,ic,in,2)
           end do
           do ir = 1,iprpts
             green(ir,ik,in,2) = green(ir,ic,in,1)
             green(ir,ik,in,1) = green(ir,ic,in,2)
           end do
           dosck(ik,in,2) = dosck(ic,in,1)
           dosck(ik,in,1) = dosck(ic,in,2)
         enddo
       end if; enddo

!c .. Enforce a magnetic pattern of flipped spins
       do in = 1,nbasis
       if( isflipped(in) ) then
         in0 = flippoint(in)
         do ic = 1,komp(in)
!           write(6,'(4f15.10)') energysp(1), energysp(2)
           do il = 0,iplmax
!             write(6,'(i3,4f15.10)') il, dos(il,ic,in,2), dos(il,ic,in0,1)
             dos(il,ic,in,2) = dos(il,ic,in0,1)
             dos(il,ic,in,1) = dos(il,ic,in0,2)
           end do
           do ir = 1,iprpts
             green(ir,ic,in,2) = green(ir,ic,in0,1)
             green(ir,ic,in,1) = green(ir,ic,in0,2)
           end do
           dosck(ic,in,2) = dosck(ic,in0,1)
           dosck(ic,in,1) = dosck(ic,in0,2)
         enddo
       end if; enddo
!cSUFF

       do is=1,nspin

          ! uncomment to extrapolate back to real axis
          ! (this doubles the computation time)
          ! (since n(E) is analytic ..  maybe exploit Cauchy-Riemann?)
!          if(istop.eq.'DOS_CALC') then
!            if(isfirstpass) then
!              dos_save   = dos
!              dosck_save = dosck
!              energy = energy + dcmplx(0.d0,1.d0)*dimag(energy)
!              isfirstpass = .false.
!              goto 1000
!            else
!              dos = 2.d0*dos_save - dos
!              dosck = 2.d0*dosck_sav - dosck
!            end if
!          end if
          ! end extrapolation block

          if(ie.eq.nume) then
             do in=1,nbasis
               do ic=1,komp(in)

                 doslast(ic,in,is) = dcmplx(0.d0,0.d0)
                 do il = 0,lmax
                   doslast(ic,in,is) =doslast(ic,in,is) +               &
     &              dos(il,ic,in,is)
                 end do

                 doscklast(ic,in,is)=dosck(ic,in,is)
!CAA                 do ir=1,jws(ic,in)
!C             For VP Integration
                 do ir=1,iprpts
                   greenlast(ir,ic,in,is)= green(ir,ic,in,is)
                 enddo
               enddo
             enddo
          endif

          dostot = (0.d0,0.d0)
          do in=1,nbasis
            do ic=1,komp(in)

              is0 = is
              if( isDLM(in) .and. ic > komp(in)/2 ) then
                is0 = is + 1; if(is0 == 3) is0 = 1
              end if
              if( isflipped(in) ) then
                is0 = is + 1; if(is0 == 3) is0 = 1
              end if

              do il = 0, lmax
               dostot = dostot+dos(il,ic,in,is)*atcon(ic,in)*numbsub(in)
               dosint(ic,in,is) =  dosint(ic,in,is) +                   &
     &                               dos(il,ic,in,is)*dele1(ie)

                evalsum(ic,in,is) = evalsum(ic,in,is) +                 &
     &                   dimag(energysp(is0)*dele1(ie)*dos(il,ic,in,is))
!CDEBUG     >                     dimag(egrd(ie)*dele1(ie)*dos(ic,in))
              end do

              dosckint(ic,in,is)= dosckint(ic,in,is) +                  &
     &                               dosck(ic,in,is)*dele1(ie)

!CAA             do ir=1,jws(ic,in)
!C             For VP Integration
              do ir=1,iprpts
                greenint(ir,ic,in,is)=greenint(ir,ic,in,is) +           &
     &                                green(ir,ic,in,is)*dele1(ie)
              enddo
            enddo
          enddo

          dostspn(is,ie) = dostspn(is,ie) + dostot

!CALAM **********************************************************
           if(istop.eq.'DOS_CALC'.or.istop=='DOSBSF_CAL') then

            if(nspin == 1) then
              write(dfo,'(2f18.10)') real(egrd(ie)), 2.d0*dimag(dostot)
            else
              if(is == 1) then
                write(dfo,'(1f18.10)',advance='no') real(egrd(ie))
                dostotup = dimag(dostot)
              else
                write(dfo,'(3f18.10)') dostotup + dimag(dostot),        &
     &            dostotup, dimag(dostot)
              end if
            end if

            do in=1,nbasis
            do ic=1,komp(in)

              if(is == 1) dossc(ic,in) = 0.d0
              do il=0,lmax
                dossc(ic,in) = dossc(ic,in) + dimag(dos(il,ic,in,is))
              end do

              if(nspin == 1) then
                  write(dfo+10*in+ic,'(6f18.10)') real(egrd(ie)),       &
     &      2.d0*dossc(ic,in), (2.d0*dimag(dos(il,ic,in,is)),il=0,lmax)
              else
                if(is == 1) then
                  write(dfo+10*in+ic,'(5f18.10)')                       &
     &              real(egrd(ie)),(dimag(dos(il,ic,in,is)),il=0,lmax)
                else
                  backspace(unit=dfo+10*in+ic)
                  read(dfo+10*in+ic,'(a90)') dfs
                  backspace(unit=dfo+10*in+ic)
                  write(dfo+10*in+ic,'(a18)',advance='no') dfs(1:18)
                write(dfo+10*in+ic,'(f18.10)',advance='no') dossc(ic,in)
                  write(dfo+10*in+ic,'(a72)',advance='no') dfs(19:90)
                  write(dfo+10*in+ic,'(4f18.10)')                       &
     &             (dimag(dos(il,ic,in,is)),il=0,lmax)
                end if
              endif
            end do; end do

           endif
!CALAM **********************************************************

       end do
      end do
!c======================================================================= !======

      if (nspin.eq.1) then
       dostspn(2,1:nume) = dostspn(1,1:nume)
      end if

      if(iprint.ge.-1) then

       if(iset_b_cont.eq.0)then
         write(6,*)
        if(nspin.eq.2) then

         do ie=1,nume
          write(6,1001) egrd(ie),dostspn(1,ie)/natomcnt,                &
     &                  '  Re+Im: E , dos_spin1 '
         end do
1001       format(2f11.7,1x,2g16.8,1x,a24)

         write(6,*)
         do ie=1,nume
          write(6,1001) egrd(ie),dostspn(2,ie)/natomcnt,                &
     &                  '  Re+Im: E , dos_spin2 '
         end do

         write(6,*)
         do ie=1,nume
          dostot = dostspn(1,ie) + dostspn(2,ie)
            write(6,1001) egrd(ie),dostot/natomcnt,' Re+Im: E,dos/atom'
         end do

        else if(nspin.eq.1) then

         do ie=1,nume
          write(6,1001) egrd(ie),2.d0*dostspn(1,ie)/natomcnt,           &
     &                  ' Re+Im: E, dos/atom'
         end do

        end if
       endif        ! End of iset_b_cont loop
      end if

!c     ================================================================

      if(istop.eq.sname) then
        call fstop(sname)
      endif
!c
      return
      end
