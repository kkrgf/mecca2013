      subroutine tausprs(lmax,natom,                                    &
     &              aij,itype,itype1,                                   &
     &              mapstr,ndimbas,                                     &
     &              kx,ky,kz,                                           &
     &              tcpa,                                               &
     &              iscreen,                                            &
     &              ndkkr,deltat,deltinv,                               &
     &              numnbi,jclust,                                      &
     &              mapsnni,ndimnn,                                     &
     &              nnptr,maxclstr,nnclmn,                              &
     &              rsij,param1,inum,                                   &
     &              invswitch,isprs,ineq,tolinv,bigmem,                 &
     &              greenrs,ndgreen,greenKS,tau00,                      &
     &              iprint,istop)
!c     ==================================================================
!c
!c  To calculate diagonal block of Tau (tau00) in k-space
!c
!c     ==================================================================

!c
      implicit none
!c
      integer lmax,natom,ndimbas

!c
      real*8      aij(3,*)
      real*8      kx
      real*8      ky
      real*8      kz
!c
      integer      ndgreen
      complex*16   greenrs(ndgreen,*)
      complex*16   greenKS(*)
!c                (1:nbasis*(kkrsz*kkrsz)*maxclstr+...)

      integer      ndkkr
      complex*16   tcpa(ndkkr,ndkkr,*)
!CALAM      complex*16   tref(ndkkr,*)
!CALAM      complex*16   trefinv(ndkkr,*)
      complex*16   deltat(ndkkr,ndkkr,*)
      complex*16   deltinv(ndkkr,ndkkr,*)


      complex*16   tau00(ndkkr,ndkkr,natom)

      integer      itype(natom),itype1(natom)
      integer      mapstr(ndimbas,natom)

      integer iscreen

      integer numnbi(*),ndimnn,jclust(ndimnn,*)
!CALAM      integer ntypecl(ndimnn,*),ntau(ndimnn,*)
      integer mapsnni(ndimnn*ndimnn,*)
      real*8  rsij(ndimnn*ndimnn*3,*)
      real*8  param1
      integer inum

      integer invswitch,iprint
      character istop*10
!c     ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
      character    sname*10
      parameter    (sname='tausprs')
!c     ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
      integer kkrsz,icut
      integer iatom,iatom0,jatom,jatom0,isub,ia0,i,m,l
      integer ndmat,indgKS

      integer iswitch,isparse,isprs(3),ineq
      real*8  tolinv,bigmem
      integer indclstr

!CALAM      real*8 time1,time2

      external indclstr

      integer ndbmt,ndKS

      integer inn,inn0,nnmax
      integer maxclstr
      integer nnptr(0:1,natom),nnclmn(2,maxclstr,natom)

      kkrsz = (lmax+1)*(lmax+1)

      isparse = isprs(1)
      if(isparse.eq.0) call fstop(sname//'  ISPARSE=0')

      iswitch = isprs(2)                ! to choose sparse method techniques
!c                                           SuperLU - 0, TFQMR - 1 , MAR !-2
      icut    = isprs(3)                ! to use NN only (if icut.EQ.1)

      ndKS = 0

!c***********************************************************************
      if(iscreen.gt.0) then
!c***********************************************************************

!c
!c  Fourier transformation of real-space screened Greens function
!c
        ndmat = kkrsz
        call zerooutC(greenKS,((ndmat*ndmat)*maxclstr)*natom)

        do iatom = 1,natom
         iatom0 = nnptr(1,iatom)
         isub = itype(iatom0)
!c         indgKS = (iatom-1)*(ndmat*ndmat)*maxclstr
         indgKS = ndKS
!c         call zeroout(greenKS(indgKS+1),2*kkrsz*kkrsz*nnptr(0,iatom0))
         nnmax = nnptr(0,iatom)

         do inn=1,nnmax
          inn0 = nnclmn(2,inn,iatom)
          jatom = nnclmn(1,inn,iatom)
          jatom0 = jclust(inn0,isub)    ! origin of "jatom"
          ia0 = mapstr(iatom,jatom)
          call ksmatr(1,inn,jatom0,                                     &
     &                 kx,ky,kz,aij(1,ia0),greenrs(1,isub),             &
     &                 numnbi(isub),kkrsz,rsij(1,isub),                 &
     &                 mapsnni(1,isub),ndimnn,nnmax,                    &
     &                 jclust(1,isub),                                  &
     &                 greenKS(indgKS+1),ndmat,                         &
     &                 invswitch,                                       &
     &                 istop)
         end do
         ndKS = ndKS+(ndmat*ndmat)*maxclstr

        end do

!c
!c  greenKS has been calculated for inequivalent atoms only! (isub=1:nbasis)
!c

!c***********************************************************************
      else
!c***********************************************************************

       call fstop(sname//'  ISCREEN.LE.0 -'                             &
     &                 //' not implemented')
!c
!c  ?? We should have here K-space calculation of G0
!c  ??  (if icut=0 -- full matrix, icut.ne.0 -- sparse matrix) or
!c  ?? Gscreen (iscreen<0)
!c
!c     if no screening (iscreen=0) you have to be sure that
!c               deltat = tcpa, deltinv = tcpa^(-1)
!c        to obtain proper Greens function from G0
!c

!c***********************************************************************
      end if
!c***********************************************************************

!c   deltat,deltinv -- matrix [NDKKRxNDKKR,*]
!c          deltinv == deltat^(-1)

       ndbmt = kkrsz*kkrsz*maxclstr
!c
!c   greenKS(1+ndKS:...) can be used as temporary array, check
!c                       dimension in upper-level routine
!c

!c***********************************************************************
       if(ineq.eq.0) then
!c***********************************************************************

!c             all atoms are treated as inequivalent
!c             ( it is supposed that itype1(k) = k )

        call inv1sprs(                                                  &
     &              greenKS,                                            &
     &              kkrsz,natom,deltat,ndkkr,itype1,                    &
     &              tau00,                                              &
     &              nnptr,nnclmn,maxclstr,                              &
     &              greenKS(1+ndKS),ndbmt,                              &
     &              param1,                                             &
     &              iswitch,inum,tolinv,bigmem,                         &
     &              iprint)
        if(iscreen.ne.0) then
         do iatom=1,natom
          isub = itype1(iatom)
          i = 0
          do l=1,kkrsz
           do m=1,kkrsz
            i = i+1
            greenKS(i) = tau00(m,l,iatom)
           end do
          end do
          call matrm1t(greenKS(1),deltinv(1,1,isub),                    &
     &                 ndkkr,1,1,kkrsz,invswitch,0)
          call caltau00(greenKS(1),kkrsz,tcpa(1,1,iatom),ndkkr,         &
     &                 1,1,kkrsz,tau00(1,1,iatom),invswitch)
         end do
        else
         call fstop(sname//' :: iscreen = 0 -- not implemented')
        end if

!c***********************************************************************
       else
!c***********************************************************************

        call inv1sprs(                                                  &
     &              greenKS,                                            &
     &              kkrsz,natom,deltat,ndkkr,itype,                     &
     &              tau00,                                              &
     &              nnptr,nnclmn,maxclstr,                              &
     &              greenKS(1+ndKS),ndbmt,                              &
     &              param1,                                             &
     &              iswitch,inum,tolinv,bigmem,                         &
     &              iprint)

        if(iscreen.ne.0) then
         do iatom=1,natom
          isub = itype(iatom)
          i = 0
          do l=1,kkrsz
           do m=1,kkrsz
            i = i+1
            greenKS(i) = tau00(m,l,iatom)
           end do
          end do
          call matrm1t(greenKS(1),deltinv(1,1,iatom),                   &
     &                 ndkkr,1,1,kkrsz,invswitch,0)
          call caltau00(greenKS(1),kkrsz,tcpa(1,1,iatom),ndkkr,         &
     &                 1,1,kkrsz,tau00(1,1,iatom),invswitch)
         end do
        else
         call fstop(sname//' :: iscreen = 0 -- not implemented')
        end if

!c***********************************************************************
       end if
!c***********************************************************************

      if (istop.eq.sname) call fstop(sname)
!c

      return
      end

!c     cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      subroutine inv1sprs(amt,kkrsz,natom,deltat,ndimt,                 &
     &                    itype,                                        &
     &                    taudlt,                                       &
     &                    nnptr,nnclmn,ndimnn,                          &
     &                    bmt,ndbmt,                                    &
     &                    param1,                                       &
     &                    iswitch,invparam,tolinv,bigmem,               &
     &                    iprint)
!c     ================================================================
!c
!c  To calculate diagonal blocks of matrix <deltat*[1-amt*deltat]^(-1)>,
!c         amt  -- matrix, (KKRSZ*KKRSZ)*(NDIMNN)*NBASIS
!c         deltat -- "diagonal" of full matrix, order KKRSZ
!c
!c  output: taudlt[ndimt,ndimt,natom]
!c
!c
!cab   TFQMR (iswitch=1,2), SuperLU (iswitch=0)
!c
      implicit real*8 (a-h,o-z)
!c
      include 'lmax.h'
      integer kkrsz,natom,ndmat,iprint
      integer ndimt
      complex*16 amt(kkrsz*kkrsz,ndimnn,*)
      complex*16   deltat(ndimt,ndimt,*)
      integer    itype(natom)

      complex*16 taudlt(ndimt,ndimt,*)

      integer nnptr(0:1,*),ndimnn,nnclmn(2,ndimnn,*)
      complex*16 bmt(*)
      real*8 param1                     ! dimag(pdu)
!CALAM      integer inum
!CALAM      character  istop*10
      character  sname*10
!c

!c parameter
      complex*16 czero
      complex*16 cone
!c
!c     ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
      parameter (czero=(0.d0,0.d0))
      parameter (cone=(1.d0,0.d0))
      parameter (sname='inv1sprs')

      real*8 tolinv,bigmem

      logical lprecond

      integer isave/1/
      save isave

      character*3 ILU,JCB,CLS
      parameter (ILU='ILU',JCB='JCB',CLS='CLS')
      character*3 PRECONDITIONER(0:3)
      data  PRECONDITIONER/'   ',JCB,ILU,CLS/

      complex*16 tmpmtr(kkrsz*kkrsz)
!c     ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!c

      ndmat = kkrsz*kkrsz*ndimnn

      if(iswitch.gt.0) then
           lprecond = .true.
      else
           lprecond = .false.
      end if

      do iatom=1,natom
        do inn=1,nnptr(0,iatom)
         jatom = nnclmn(1,inn,iatom)

         call zgemm('N','N',kkrsz,kkrsz,kkrsz,cone,                     &
     &            amt(1,inn,iatom),kkrsz,deltat(1,1,jatom),ndimt,czero, &
     &            tmpmtr,kkrsz)

!CDEBUG         if(nnclmn(2,inn,iatom).eq.1) then
         if(iatom.eq.jatom) then
          tmpmtr(1) = tmpmtr(1)-cone
          aiimin = abs(tmpmtr(1))
          do lm=2,kkrsz
           tmpmtr((lm-1)*kkrsz+lm) = tmpmtr((lm-1)*kkrsz+lm)-cone
           aiimin = min(aiimin,abs(tmpmtr((lm-1)*kkrsz+lm)))
          end do
         end if
!c         do lm=1,kkrsz*kkrsz
!c           amt(lm,inn,iatom) = -tmpmtr(lm)
!c         end do
         amt(1:kkrsz*kkrsz,inn,iatom) = -tmpmtr(1:kkrsz*kkrsz)
        end do

        aiimin = max(aiimin*tolinv,tolinv**2)

        do inn=1,nnptr(0,iatom)
         do lm=1,kkrsz*kkrsz
          if(abs(amt(lm,inn,iatom)).lt.aiimin)                          &
     &     amt(lm,inn,iatom) = czero
         end do
        end do

      end do

!c                  Now, amt --> (1-amt*deltat)

10     nitmax = kkrsz*(natom+2)
       if(iswitch.ne.0) then
        iprcnd = max(0,abs(iswitch))
        if(PRECONDITIONER(iprcnd).eq.ILU) then
         bmt(1) = dcmplx(param1,bigmem)
         call gsvILUsp(amt,ndmat,                                       &
     &                   nnptr,nnclmn,ndimnn,bmt,                       &
     &                   taudlt,ndimt,                                  &
     &                   itype,kkrsz,natom,lprecond,                    &
     &                   invparam,nitmax,tolinv,iprint)
        else if(PRECONDITIONER(iprcnd).eq.JCB) then
         call gsvJCBsp(amt,ndmat,                                       &
     &                   nnptr,nnclmn,ndimnn,                           &
     &                   taudlt,ndimt,                                  &
     &                   itype,kkrsz,natom,lprecond,                    &
     &                   nitmax,tolinv,iprint)
        else if(PRECONDITIONER(iprcnd).eq.CLS) then
         call gsvCLSsp(amt,ndmat,                                       &
     &                   nnptr,nnclmn,ndimnn,bmt,ndbmt,                 &
     &                   taudlt,ndimt,                                  &
     &                   itype,kkrsz,natom,lprecond,                    &
     &                   nitmax,tolinv,iprint)
       else
         call gsvJCBsp(amt,ndmat,                                       &
     &                   nnptr,nnclmn,ndimnn,                           &
     &                   taudlt,ndimt,                                  &
     &                   itype,kkrsz,natom,.false.,                     &
     &                   nitmax,tolinv,iprint)
       end if
       if(nitmax.lt.0) then
         write(*,*) '  ITERATIVE METHOD HAS NOT BEEN CONVERGED;'
         if(isave.eq.2.or.nitmax.lt.-2) then
          write(6,*) ' ISWITCH=',iswitch
          write(6,*) ' NITMAX=',nitmax
          call fstop(sname//':: inversion error')
         else
          write(*,*) '  CODE IS CHANGING THE METHOD TO INVERT MATRIX'
          if(nitmax.eq.-1) then
           iswitch = 0
          else if(nitmax.eq.-2.and.iswitch.ne.1) then
           iswitch = 1
          else
           iswitch = 0
          end if
         end if
         write(*,*) '  NOW ISWITCH=',iswitch
         isave=2
         go to 10
        end if
       else                             ! for instance, SuperLU, UFMPACK, ...

        bmt(1) = dcmplx(param1,bigmem)

        call gsvlusp(amt,ndmat,                                         &
     &                   nnptr,nnclmn,ndimnn,                           &
     &                   taudlt,ndimt,                                  &
     &                   itype,kkrsz,natom,                             &
!CALAM     >                   invparam,nitmax,tolinv,iprint
     &                   iprint)

       end if
!c
!c   Now taudlt <-- deltat*(1-amt*deltat)(-1)
!c

!c
      return
      end

