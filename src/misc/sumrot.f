!c     ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      subroutine sumrot(tauir,tau00,nbasis,kkrsz,                       &
     &                  natom,itype,istop)
!c     =============================================================
!c
      use mecca_constants
      implicit none
!      include 'lmax.h'
!c     ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
      integer      nbasis,natom,itype(natom)
      complex*16   tauir(ipkkr,ipkkr,natom)
      complex*16   tau00(ipkkr,ipkkr,nbasis)
!CALAM      integer      if0(48,natom)
      integer      kkrsz
!CALAM      complex*16   dop(kkrsz,kkrsz,nrot)
!CALAM      integer      iprint
      character    istop*10

!c      complex*16   tautmp,taun1n2
!c     ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
      character(:), parameter :: sname='sumrot'
!      complex*16   czero
!      parameter    (czero=(0.d0,0.d0))

      integer nsub,iatom,i,j
!c     ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!c
!c     ***************************************************************
!c     given integral of tau00 over irriducible part of Brillouin Zone
!c     calculates the full integral by summing over rotations.........
!c     ***************************************************************
!c
!c

!CDEBUG      call zeroout(tau00,2*ipkkr*ipkkr*nbasis)
!CDEBUG      do nsub=1,natom
!CDEBUG         do i=1,kkrsz
!CDEBUG            do j=1,kkrsz
!CDEBUG               do n1=1,kkrsz
!CDEBUG                  do n2=1,kkrsz
!CDEBUG                    taun1n2 = tauir(n1,n2,nsub)
!CDEBUG                     do ir=1,nrot
!CDEBUG              tau00(j,i,itype(if0(ir,nsub))) =
!CDEBUG     *                    tau00(j,i,itype(if0(ir,nsub)))+
!CDEBUG     >                               dop(j,n1,ir)*
!CDEBUG     >                               taun1n2*
!CDEBUG     >                               conjg( dop(i,n2,ir) )
!CDEBUG                     enddo
!CDEBUG                  enddo
!CDEBUG               enddo
!CDEBUG            enddo
!CDEBUG         enddo
!CDEBUG      enddo

      do nsub=1,nbasis

        do iatom=1,natom
         if(itype(iatom).eq.nsub) go to 10
        end do
        call fstop(sname// ' :: You have wrong ITYPE')

10      do i=1,kkrsz
         do j=1,kkrsz
          tau00(j,i,nsub) = tauir(j,i,iatom)
         end do
        end do

      end do

!c
      if(istop.eq.sname) then
       call fstop(sname)
      endif

      return
!c
      end
