!c     ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
!BOP
!!ROUTINE: semrel
!!INTERFACE:
      subroutine semrel(nrelv,clight,                                   &
     &                  lmax,                                           &
     &                  energy,pnrel,                                   &
     &                  g,                                              &
     &                  tmatl,cotdl,almat,                              &
     &                  zlzl,zljl,                                      &
     &                  h,jmt,jws,x,r,rv,                               &
     &                  rmt_true,r_circ,ivar_mtz,                       &
     &                  fcount,weight,rmag,vj,lVP,                      &
     &                  iswzj,                                          &
     &                  iprint,istop)
!!DESCRIPTION:
!   this subroutine solves scalar relativistic equations for complex
!   energies using Calogero's phase method and gets various phase shift 
!   related values, radial wavefunctions and their integrals over ASA 
!   spheres and, optionally, VP
!   (single-scattering problem)  \\
!....based on ddj \& fjp version --- july 1991
!
!!USES:
      use mecca_constants
      use isoparintgr_interface
      use sctrng, only : scalar
!
!!REVISION HISTORY:
! Adapted - A.S. - 2013
!!REMARKS:
! old non-module version,
! but empty spheres are always treated as non-relativistic
!EOP
!
!BOC
!      use mecca_interface, only : scalar
      implicit real*8 (a-h,o-z)
!c
      character sname*10
      character(:), allocatable :: coerr
      character(10) :: istop
!c
      integer fcount
      integer ia

      real(8) :: rmt_true,xrmt
      real(8) :: r_circ
      real(8) :: weight(:)  ! (MNqp)
      real(8) :: rmag(:,:,:,:)  ! (MNqp,MNqp,MNqp,MNF)
      real(8) :: vj(:,:,:,:)  ! (MNqp,MNqp,MNqp,MNF)
      logical, intent(in) :: lVP

      real(8) :: x(:)
      real(8) :: rv(:)
      real(8) :: r(:)
      real(8) :: r2(size(r))
      real(8) :: h
      real(8) :: clight
!c
      complex(8) :: almat(lmax+1),alpha
      complex(8) :: bjl(lmax+2,size(r))
      complex(8) :: bnl(lmax+2,size(r))
      complex(8) :: zlzl(lmax+1)
      complex(8) :: zljl(lmax+1)
      complex(8) :: g(:,:)  ! (iprpts,lmax+1)
      complex(8) :: f(size(r))
      complex(8) :: gpl(size(r))
      complex(8) :: fpl(size(r))
      complex(8) :: cn1(size(r))
      complex(8) :: dcn1(size(r))
      complex(8) :: cj1(size(r))
      complex(8) :: dcj1(size(r))
      complex(8) :: cotdl(lmax+1)
      complex(8) :: tmatl(lmax+1)
      complex(8) :: energy
      complex(8) :: pnrel
      complex(8) :: prel
      complex(8) :: pr
      complex(8) :: cotdel
      complex(8) :: tandel
      complex(8) :: anorm
      complex(8) :: VPInt,dpsi_MT
      complex(8), external :: ylag_cmplx
!c parameter
!      complex*16 cone
      complex*16 sqrtm1

!c
!c     +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!      parameter (cone=(1.d0,0.d0))
      parameter (sqrtm1=(0.d0,1.d0))
      parameter (sname='semrel')
!c     +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!c
!c     *****************************************************************
!c     Gets radial integrals, t-matrix.
!c     If iswzj =1, then also gets the z*j interals............
!c
!c     In this version rv is r * v
!c
!c     called by: grint
!c     calls:     bessel,cqexp,cqxpup,csimp,zeroout
!c
!c      requires:
!c            lmax        lmax
!c            energy      energy
!c            iswzj       switch: if iswzj.ne.0 gets z*j integrals.

!c            istop       switch: if istop=21 stop at end of this routine
!c            h,x,r       grid info (step for log-grid, log grid, r-grid)
!c
!c      returns:
!c            cotdl       phase shift cotangent
!c            tmatl       t-matrix
!c            zlzl        integral of (rad wf)**2 over M.T.
!c            zljl        integral of (rad wf * ir rad wf) over M.T.
!c
!c     set c for doing semi-relativistic or non-relativistic calc.
!c     .....c = speed of light in proper units..........
!c     Ryd units: m=1/2  hbar=1 c=2*inverse of fine structure const.
!c
!c     Solving a relativistic (nrelv=0) or non-relativistic (nrelv=10)
!c     problem.   If non-relativistic, then set e=e-elmass equal to e
!c     (where elmass = electron mass) and 1/c is set to ~zero to make
!c     relativistic terms zero.
!c     NOTE:  c is set to c*10**nrelv to make code identical to the
!c            fully relativistic code..............................
!c
!c     Using Ricatti Bessel fcts. Need only l=0 and l=1 to get wave-fcts.
!c
!c     To get physical phase-shifts and normalization, need get Rl in
!c     terms of spherical bessel for all l's at the boundary of sphere.
!c     This is properly done in SCALAR, see clmt and slmt.
!c
!c     With M(r)= [1 + (e-v(r))*c2inv]
!c     Obtain r*(wave-fct.):   regular     g=r*R and   f=r*R'/M
!c                           irregular   gpl=r*R and fpl=r*R'/M
!c
!c     NOTE: ====>   need to get gderiv for mom. matrix elements
!c
!c     *****************************************************************
!c

!CAB      prel=sqrt( energy*(cone+energy/clight**2) )
!CAB??
!CAB??       What about sqrt(1+energy/clight^2) , when energy is complex  !????
!CAB??

!      zed = -(rv(1) + (rv(1)-rv(2))*r(1)/(r(2)-r(1)))
!      ized = nint(0.5d0*zed)
      ized = g_zed(rv,r)
!c     =================================================================
      if(ized.lt.1 .and. nrelv.le.0) then
        prel = pnrel
        nrel = 1
      else
!c
!c Phaseshifts and wavefcts are calculated with relativsitic mom. -- prel
!c Whereas, because t_inv= i*Kappa + Kappa*cotdel, i*Kappa term must
!c cancel i*Kappa piece of Non-Rel g(k). Therefore, Kappa=sqrt(e) -- pnre !l.
!c
        prel = pnrel*sqrt(cone+energy/clight**2)
        nrel = nrelv
      endif

!c     for muffin-tins incrs and jmt should be the same
!CAB      iend=jmt+11               ! this line is used in DDJ code
!CAB

      if (jmt.gt.jws) then
       write(6,'(a10,'': jmt,jws='',2i6)') sname,jmt,jws
       call fstop(sname//': JMT > JWS ???')
      else
        if( lVP .and. ivar_mtz.ge.2 ) then
!C          for VP Integration
           iend=min(size(r),size(rv))
        else
           iend = min(size(r),size(rv),jmt+max(11,(jws-jmt)/2*2+1))
        end if
      end if

      if( iend .lt. jws ) then
        coerr=' iend less than jws'
        write(6,'('' semrel: jmt,jws,iend='',3i5)') jmt,jws,iend
        write(6,'(2(2x,a))')coerr,sname
        call fstop(sname)
      endif

!DELETE      if( iend .gt. size(r) ) then
!DELETE        coerr=' iend greater than array size'
!DELETE        write(6,'('' semrel: iend,size(r)='',3i5)') iend,size(r)
!DELETE        write(6,'(2(2x,a))')coerr,sname
!DELETE        call fstop(sname)
!DELETE      endif

!c     =================================================================
!c
!c Using Ricatti Bessel fcts. Need only l=0 and l=1 to get wave-fcts.
!c -------------------------------------------------------------------
!c To get physical phase-shifts and normalization, need get Rl in
!c terms of spherical bessel for all l at the boundary of sphere.
!c This is properly done in SCALAR, see clmt and slmt.
!c -------------------------------------------------------------------
      do j=1,iend
          r2(j)=r(j)*r(j)
          pr=prel*r(j)
          bjl(1,j)= sin(pr)
          bnl(1,j)=-cos(pr)
          bjl(2,j)= bnl(1,j) + bjl(1,j)/pr
          bnl(2,j)=-bjl(1,j) + bnl(1,j)/pr
      enddo
!c     =================================================================
!c
!c solve scalar-relativistic or non-relativistic equation.
!c
!c ------------------Major loop over l's-------------------------------

       do l=0,lmax
!c
!c -------------------------------------------------------------------
!c With M(r)= [1 + (e-v(r))*c2inv]
!c Obtain r*(wave-fct.):   regular     g=r*R and   f=r*R'/M
!c                       irregular   gpl=r*R and fpl=r*R'/M
!c -------------------------------------------------------------------

!DELETE         call scalar(nrel,clight,                                       &
!DELETE     &               l,                                                 &
!DELETE     &               bjl,bnl,g(:,l+1),f,gpl,fpl,                        &
!DELETE     &               tandel,cotdel,anorm,alpha,                         &
!DELETE     &               energy,prel,                                       &
!DELETE     &               rv,r,r2,h,jmt,iend,                                &
!DELETE     &               iswzj,                                             &
!DELETE     &               iprint,istop)
!c        ------------------------------------------------------------
         anorm = cone
         call scalar(nrel,clight,                                       &
     &               ized,                                              &
     &               l,                                                 &
     &               g(:,l+1),f,gpl,fpl,                                &
     &               tandel,cotdel,alpha,                               &
     &               energy,prel,                                       &
     &               rv,r,r(jmt),iend,                                  &
     &               iswzj,.true.,                                      &
     &               iprint)
!c        ==============================================================
!c        store cotangent of phase-shift, normalization and t-matrix

         cotdl(l+1)=cotdel
         almat(l+1)=alpha

!c store single-site t-matrix
!c  Must use pnrel here (not prel) to cancel pole in struc. const.
!c  Actually, because of anorm (in SCALAR) this is wrong
!c  one way or the other. If prel, then normalized correctly;
!c  if pnrel, then does not cancel pole correctly. CHECK!!!!

!c
          tmatl(l+1)=cone/( pnrel*( sqrtm1- cotdel ) )
          ! debug warning
!c         tmatl(l+1)=cone/( prel*( sqrtm1- cotdel ) )

!c    ==================================================================
!c    To calculate the wave function integrals over ASA sphere 
!c      OR over VP-boundary (if ivar_matz >= 2 .and. lVP)
!c    ==================================================================
        if( lVP .and. ivar_mtz.ge.2 ) then
         ! For Ordered systems only AND Full VP+Variational MT-zero (VP)
!c       ==============================================================
             xrmt = log(rmt_true)
             nlag=3
!C*********************************************************************** 
!c       integrals for zl*zl and zl*zj: with factor of r2 included here
!C***********************************************************************
             do j=1,iend
                 cn1(j)= g(j,l+1)*g(j,l+1)
             enddo
             call cqxpup(1,cn1,jws,x,dcn1)
             dpsi_MT=ylag_cmplx(xrmt,x,dcn1,0,nlag,jmt,iex)

                ia=2
             call isopar_chbint_cmplx(ia,rmt_true,iend,r,cn1,           &
     &                        r_circ,fcount,weight,rmag,vj,VPInt)
!             call isopar_integ_cmplx(ia,rmt_true,r,cn1,                 &
!     &                         r_circ,fcount,weight,                    &
!     &                         rmag,vj,VPInt)

             zlzl(l+1) = -(dpsi_MT + VPInt)*anorm**2/pi
!Cc           =========================================================== !=
!Cc            get zj integrals if needed................................ !...
             if(iswzj.eq.1) then
               do j=1,iend
                  cj1(j)= g(j,l+1)*gpl(j)
               enddo
!C
               call cqxpup(1,cj1,jws,x,dcj1)
               dpsi_MT=ylag_cmplx(xrmt,x,dcj1,0,nlag,jmt,iex)
!C
                ia=2
                call isopar_chbint_cmplx(ia,rmt_true,iend,r,cj1,        &
     &                        r_circ,fcount,weight,rmag,vj,VPInt)
!               call isopar_integ_cmplx(ia,rmt_true,r,cj1,              &
!    &                            r_circ,fcount,weight,                 &
!    &                            rmag,vj,VPInt)

                zljl(l+1) = (dpsi_MT + VPInt)*anorm/pi
             endif
!Cc      ==============================================================
        else
           ! For Disord systems only OR Variational MT-zero (ivar_mtz=0,1,2)
!Cc      ==============================================================
!Cc        integrals for zl*zl and zl*zj: with factor of r2 included here
!suff .. no factor of 4*pi since angular integrations are taken to
!     .. come out to unity for l = l'

            do j=1,iend
                cn1(j)= g(j,l+1)*g(j,l+1)
            enddo
            call cqxpup(1,cn1,jws,x,dcn1)
            zlzl(l+1)=-dcn1(jmt)*anorm**2/pi

!Cc           =========================================================== !==
!Cc           get zj integrals if needed................................. !..
            if(iswzj.eq.1) then
               do j=1,iend
                  cj1(j)= g(j,l+1)*gpl(j)
               enddo
               call cqxpup(1,cj1,jws,x,dcj1)
               zljl(l+1)= dcj1(jmt)*anorm/pi
            endif
!Cc      ==============================================================
        endif

        ! note: following aftab's bloch spec. code, the regular fns
        ! are stored for the (occasional) case when we need to calc.
        ! integrals of form int(zlzl) for _different_ components on
        ! the same site .. and recall: g(r) = r Z(r)

         g(:,l+1) = anorm*g(:,l+1)

!c     ================================================================
       enddo
!c     ================================================================
      if (istop.eq.sname) then
        call fstop(sname)
      else
         return
      endif
!c
      end subroutine semrel
