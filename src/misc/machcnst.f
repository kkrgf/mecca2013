!BOP
!!ROUTINE: D1MACH
!!INTERFACE:
      real*8 FUNCTION D1MACH (I)
!!DESCRIPTION:
! {\bv
! D1MACH can be used to obtain machine-dependent parameters for the
! local machine environment.  It is a function subprogram with one
! (input) argument, and can be referenced as follows:
!
!      D = D1MACH(I)
!
! where I=1,...,5.  The (output) value of D above is determined by
! the (input) value of I.  The results for various values of I are
! discussed below.
!
! D1MACH( 1) = B**(EMIN-1), the smallest positive magnitude.
! D1MACH( 2) = B**EMAX*(1 - B**(-T)), the largest magnitude.
! D1MACH( 3) = B**(-T), the smallest relative spacing.
! D1MACH( 4) = B**(1-T), the largest relative spacing.
! D1MACH( 5) = LOG10(B)
!
! Assume double precision numbers are represented in the T-digit,
! base-B form
!
!            sign (B**E)*( (X(1)/B) + ... + (X(T)/B**T) )
!
! where 0 .LE. X(I) .LT. B for I=1,...,T, 0 .LT. X(1), and
! EMIN .LE. E .LE. EMAX.
!
! The values of B, T, EMIN and EMAX are provided in I1MACH as
! follows:
!
! I1MACH(10) = B, the base.
! I1MACH(14) = T, the number of base-B digits.
! I1MACH(15) = EMIN, the smallest exponent E.
! I1MACH(16) = EMAX, the largest exponent E.
! \ev}
!!ARGUMENTS:
      integer :: I
!!REVISION HISTORY:
! Adapted - A.S. - 2013
!!REMARKS:
! slatec original code is replaced by internal fortran functions
!EOP
!
!BOC
      integer, save :: isave=0
      real(8), save :: DMACH(5)
      real(8), parameter :: x=1.d0
      IF (I .LT. 1 .OR. I .GT. 5) STOP ' D1MACH ERROR'
!C
      if(isave.eq.0) then
       dmach(1) = tiny(x)
       dmach(2) = huge(x)
       dmach(3) = epsilon(x)/radix(x)
       dmach(4) = epsilon(x)
       dmach(5) = log10(dble(radix(x)))
       isave = 1
      end if

      D1MACH = DMACH(I)
      RETURN
!EOC
      END
!
!BOP
!!ROUTINE: I1MACH
!!INTERFACE:
      INTEGER FUNCTION I1MACH (I)
!!DESCRIPTION:
! {\bv
! I1MACH can be used to obtain machine-dependent parameters for the
! local machine environment.  It is a function subprogram with one
! (input) argument and can be referenced as follows:
!
!      K = I1MACH(I)
!
! where I=1,...,16.  The (output) value of K above is determined by
! the (input) value of I.  The results for various values of I are
! discussed below.
!
! I/O unit numbers:
!
!   I1MACH( 1) = the standard input unit.
!   I1MACH( 2) = the standard output unit.
!   I1MACH( 3) = the standard punch unit.
!   I1MACH( 4) = the standard error message unit.
!
! Words:
!
!   I1MACH( 5) = the number of bits per integer storage unit.
!   I1MACH( 6) = the number of characters per integer storage unit.
!
! Integers:
!
!   assume integers are represented in the S-digit, base-A form
!
!    sign ( X(S-1)*A**(S-1) + ... + X(1)*A + X(0) ) ,
!              where 0 .LE. X(I) .LT. A for I=0,...,S-1.
!
!   I1MACH( 7) = A, the base.
!   I1MACH( 8) = S, the number of base-A digits.
!   I1MACH( 9) = A**S - 1, the largest magnitude.
!
! Floating-Point Numbers:
!   Assume floating-point numbers are represented in the T-digit,
!   base-B form
!
!              sign (B**E)*( (X(1)/B) + ... + (X(T)/B**T) )
!
!              where 0 .LE. X(I) .LT. B for I=1,...,T,
!              0 .LT. X(1), and EMIN .LE. E .LE. EMAX.
!
!   I1MACH(10) = B, the base.
!
! Single-Precision:
!
!   I1MACH(11) = T, the number of base-B digits.
!   I1MACH(12) = EMIN, the smallest exponent E.
!   I1MACH(13) = EMAX, the largest exponent E.
!
! Double-Precision:
!
!   I1MACH(14) = T, the number of base-B digits.
!   I1MACH(15) = EMIN, the smallest exponent E.
!   I1MACH(16) = EMAX, the largest exponent E.
!
! The values of I1MACH(1) - I1MACH(6) should be
! checked for consistency with the local operating system.
! \ev}
!!ARGUMENTS:
      integer :: I
!!REVISION HISTORY:
! Adapted - A.S. - 2013
!!REMARKS:
! slatec original code is replaced by internal fortran functions
!EOP
!
!BOC
      integer, save :: isave=0
      integer, save :: IMACH(16)
      INTEGER OUTPUT
      real(4) :: xr4
      real(8) :: xr8
      EQUIVALENCE (IMACH(4),OUTPUT)

      IF (I .LT. 1  .OR.  I .GT. 16) GO TO 10
!C
      if(isave.eq.0) then
       IMACH( 1) =  5
       IMACH( 2) =  6
       IMACH( 3) =  6
       IMACH( 4) =  6
       IMACH( 5) = 32
       IMACH( 6) =  4
!c
       imach(7) = radix(isave)
       imach(9) = huge(isave)
       imach(8) = nint(log(dble(imach(9)))/log(2.d0))
       imach(9) = 1 + 2*(imach(7)**(imach(8)-1)-1)
       imach(10) = radix(xr8)
!c
       imach(11) = -(nint(log(epsilon(xr4))/log(2.)) - 1)
       imach(12) = minexponent(xr4)
       imach(13) = maxexponent(xr4)
       imach(14) = -(nint(log(epsilon(xr8))/log(2.)) - 1)
       imach(15) = minexponent(xr8)
       imach(16) = maxexponent(xr8)
       isave = 1
      end if
      I1MACH = IMACH(I)
      RETURN
!C
   10 CONTINUE
      WRITE (UNIT = OUTPUT, FMT = 9000)
 9000 FORMAT ('1ERROR    1 IN I1MACH - I OUT OF BOUNDS')
      STOP
!EOC
      END

!BOP
!!ROUTINE: R1MACH
!!INTERFACE:
      real FUNCTION R1MACH (I)
!!DESCRIPTION:
! {\bv
!
! R1MACH can be used to obtain machine-dependent parameters for the
! local machine environment.  It is a function subprogram with one
! (input) argument, and can be referenced as follows:
!
!      A = R1MACH(I)
!
! where I=1,...,5.  The (output) value of A above is determined by
! the (input) value of I.  The results for various values of I are
! discussed below.
!
! R1MACH(1) = B**(EMIN-1), the smallest positive magnitude.
! R1MACH(2) = B**EMAX*(1 - B**(-T)), the largest magnitude.
! R1MACH(3) = B**(-T), the smallest relative spacing.
! R1MACH(4) = B**(1-T), the largest relative spacing.
! R1MACH(5) = LOG10(B)
!
! Assume single precision numbers are represented in the T-digit,
! base-B form
!
!            sign (B**E)*( (X(1)/B) + ... + (X(T)/B**T) )
!
! where 0 .LE. X(I) .LT. B for I=1,...,T, 0 .LT. X(1), and
! EMIN .LE. E .LE. EMAX.
!
! The values of B, T, EMIN and EMAX are provided in I1MACH as
! follows:
!
! I1MACH(10) = B, the base.
! I1MACH(11) = T, the number of base-B digits.
! I1MACH(12) = EMIN, the smallest exponent E.
! I1MACH(13) = EMAX, the largest exponent E.
! \ev}
!!ARGUMENTS:
      integer :: I
!!REVISION HISTORY:
! Adapted - A.S. - 2013
!!REMARKS:
! slatec original code is replaced by internal fortran functions
!EOP
!
!BOC
      integer, save :: isave=0
      real(4), save :: RMACH(5)
      real(4), parameter :: x=1.
      IF (I .LT. 1 .OR. I .GT. 5) STOP ' R1MACH ERROR'
!C
      if(isave.eq.0) then
       rmach(1) = tiny(x)
       rmach(2) = huge(x)
       rmach(3) = epsilon(x)/radix(x)
       rmach(4) = epsilon(x)
       rmach(5) = log10(float(radix(x)))
       isave = 1
      end if
      R1MACH = RMACH(I)
      RETURN
!EOC
      END


!C*********************************************************************

!CC!BOP
!CC!!ROUTINE: PIMACH
!CC!!INTERFACE:
!CC      FUNCTION PIMACH ( )
!CC!!DESCRIPTION:
!CC! {\bv
!CC! This subprogram supplies the value of the constant PI correct to
!CC! machine precision where
!CC!
!CC! PI=3.1415926535897932384626433832795028841971693993751058209749446
!CC! \ev}
!CC!!ARGUMENTS:
!CC! none
!CC!!REVISION HISTORY:
!CC! Adapted - A.S. - 2013
!CC!!REMARKS:
!CC! slatec original code is replaced by internal fortran functions
!CC!EOP
!CC!
!CC!BOC
!CC      REAL*8 PIMACH
!CC
!CC      integer isave/0/
!CC      real*8 pi
!CC      save pi,isave
!CC
!CC      if(isave.eq.0) then
!CC       pi = 4.d0*atan(1.d0)
!CC       isave = 1
!CC      end if
!CC      PIMACH = pi
!CC
!CC      RETURN
!CC!EOC
!CC      END
!CC

!C*DECK D1MACH
!C      DOUBLE PRECISION FUNCTION D1MACH (I)
!CC***BEGIN PROLOGUE  D1MACH
!CC***PURPOSE  Return floating point machine dependent constants.
!CC***LIBRARY   SLATEC
!CC***CATEGORY  R1
!CC***TYPE      DOUBLE PRECISION (R1MACH-S, D1MACH-D)
!CC***KEYWORDS  MACHINE CONSTANTS
!CC***AUTHOR  Fox, P. A., (Bell Labs)
!CC           Hall, A. D., (Bell Labs)
!CC           Schryer, N. L., (Bell Labs)
!CC***DESCRIPTION
!CC
!CC   D1MACH can be used to obtain machine-dependent parameters for the
!CC   local machine environment.  It is a function subprogram with one
!CC   (input) argument, and can be referenced as follows:
!CC
!CC        D = D1MACH(I)
!CC
!CC   where I=1,...,5.  The (output) value of D above is determined by
!CC   the (input) value of I.  The results for various values of I are
!CC   discussed below.
!CC
!CC   D1MACH( 1) = B**(EMIN-1), the smallest positive magnitude.
!CC   D1MACH( 2) = B**EMAX*(1 - B**(-T)), the largest magnitude.
!CC   D1MACH( 3) = B**(-T), the smallest relative spacing.
!CC   D1MACH( 4) = B**(1-T), the largest relative spacing.
!CC   D1MACH( 5) = LOG10(B)
!CC
!CC   Assume double precision numbers are represented in the T-digit,
!CC   base-B form
!CC
!CC              sign (B**E)*( (X(1)/B) + ... + (X(T)/B**T) )
!CC
!CC   where 0 .LE. X(I) .LT. B for I=1,...,T, 0 .LT. X(1), and
!CC   EMIN .LE. E .LE. EMAX.
!CC
!CC   The values of B, T, EMIN and EMAX are provided in I1MACH as
!CC   follows:
!CC   I1MACH(10) = B, the base.
!CC   I1MACH(14) = T, the number of base-B digits.
!CC   I1MACH(15) = EMIN, the smallest exponent E.
!CC   I1MACH(16) = EMAX, the largest exponent E.
!CC
!CC   To alter this function for a particular environment, the desired
!CC   set of DATA statements should be activated by removing the C from
!CC   column 1.  Also, the values of D1MACH(1) - D1MACH(4) should be
!CC   checked for consistency with the local operating system.
!CC
!CC***REFERENCES  P. A. Fox, A. D. Hall and N. L. Schryer, Framework for
!CC                 a portable library, ACM Transactions on Mathematical
!CC                 Software 4, 2 (June 1978), pp. 177-188.
!CC***ROUTINES CALLED  XERMSG
!CC***REVISION HISTORY  (YYMMDD)
!CC   750101  DATE WRITTEN
!CC   890213  REVISION DATE from Version 3.2
!CC   891214  Prologue converted to Version 4.0 format.  (BAB)
!CC   900315  CALLs to XERROR changed to CALLs to XERMSG.  (THJ)
!CC   900618  Added DEC RISC constants.  (WRB)
!CC   900723  Added IBM RS 6000 constants.  (WRB)
!CC   900911  Added SUN 386i constants.  (WRB)
!CC   910710  Added HP 730 constants.  (SMR)
!CC   911114  Added Convex IEEE constants.  (WRB)
!CC   920121  Added SUN -r8 compiler option constants.  (WRB)
!CC   920229  Added Touchstone Delta i860 constants.  (WRB)
!CC   920501  Reformatted the REFERENCES section.  (WRB)
!CC   920625  Added CONVEX -p8 and -pd8 compiler option constants.
!CC           (BKS, WRB)
!CC   930201  Added DEC Alpha and SGI constants.  (RWC and WRB)
!CC***END PROLOGUE  D1MACH
!CC
!C      INTEGER SMALL(4)
!C      INTEGER LARGE(4)
!C      INTEGER RIGHT(4)
!C      INTEGER DIVER(4)
!C      INTEGER LOG10(4)
!CC
!C      DOUBLE PRECISION DMACH(5)
!C      SAVE DMACH
!CC
!C      EQUIVALENCE (DMACH(1),SMALL(1))
!C      EQUIVALENCE (DMACH(2),LARGE(1))
!C      EQUIVALENCE (DMACH(3),RIGHT(1))
!C      EQUIVALENCE (DMACH(4),DIVER(1))
!C      EQUIVALENCE (DMACH(5),LOG10(1))
!CC
!CC     MACHINE CONSTANTS FOR THE AMIGA
!CC     ABSOFT FORTRAN COMPILER USING THE 68020/68881 COMPILER OPTION
!CC
!CC     DATA SMALL(1), SMALL(2) / Z'00100000', Z'00000000' /
!CC     DATA LARGE(1), LARGE(2) / Z'7FEFFFFF', Z'FFFFFFFF' /
!CC     DATA RIGHT(1), RIGHT(2) / Z'3CA00000', Z'00000000' /
!CC     DATA DIVER(1), DIVER(2) / Z'3CB00000', Z'00000000' /
!CC     DATA LOG10(1), LOG10(2) / Z'3FD34413', Z'509F79FF' /
!CC
!CC     MACHINE CONSTANTS FOR THE AMIGA
!CC     ABSOFT FORTRAN COMPILER USING SOFTWARE FLOATING POINT
!CC
!CC     DATA SMALL(1), SMALL(2) / Z'00100000', Z'00000000' /
!CC     DATA LARGE(1), LARGE(2) / Z'7FDFFFFF', Z'FFFFFFFF' /
!CC     DATA RIGHT(1), RIGHT(2) / Z'3CA00000', Z'00000000' /
!CC     DATA DIVER(1), DIVER(2) / Z'3CB00000', Z'00000000' /
!CC     DATA LOG10(1), LOG10(2) / Z'3FD34413', Z'509F79FF' /
!CC
!CC     MACHINE CONSTANTS FOR THE APOLLO
!CC
!CC     DATA SMALL(1), SMALL(2) / 16#00100000, 16#00000000 /
!CC     DATA LARGE(1), LARGE(2) / 16#7FFFFFFF, 16#FFFFFFFF /
!CC     DATA RIGHT(1), RIGHT(2) / 16#3CA00000, 16#00000000 /
!CC     DATA DIVER(1), DIVER(2) / 16#3CB00000, 16#00000000 /
!CC     DATA LOG10(1), LOG10(2) / 16#3FD34413, 16#509F79FF /
!CC
!CC     MACHINE CONSTANTS FOR THE BURROUGHS 1700 SYSTEM
!CC
!CC     DATA SMALL(1) / ZC00800000 /
!CC     DATA SMALL(2) / Z000000000 /
!CC     DATA LARGE(1) / ZDFFFFFFFF /
!CC     DATA LARGE(2) / ZFFFFFFFFF /
!CC     DATA RIGHT(1) / ZCC5800000 /
!CC     DATA RIGHT(2) / Z000000000 /
!CC     DATA DIVER(1) / ZCC6800000 /
!CC     DATA DIVER(2) / Z000000000 /
!CC     DATA LOG10(1) / ZD00E730E7 /
!CC     DATA LOG10(2) / ZC77800DC0 /
!CC
!CC     MACHINE CONSTANTS FOR THE BURROUGHS 5700 SYSTEM
!CC
!CC     DATA SMALL(1) / O1771000000000000 /
!CC     DATA SMALL(2) / O0000000000000000 /
!CC     DATA LARGE(1) / O0777777777777777 /
!CC     DATA LARGE(2) / O0007777777777777 /
!CC     DATA RIGHT(1) / O1461000000000000 /
!CC     DATA RIGHT(2) / O0000000000000000 /
!CC     DATA DIVER(1) / O1451000000000000 /
!CC     DATA DIVER(2) / O0000000000000000 /
!CC     DATA LOG10(1) / O1157163034761674 /
!CC     DATA LOG10(2) / O0006677466732724 /
!CC
!CC     MACHINE CONSTANTS FOR THE BURROUGHS 6700/7700 SYSTEMS
!CC
!CC     DATA SMALL(1) / O1771000000000000 /
!CC     DATA SMALL(2) / O7770000000000000 /
!CC     DATA LARGE(1) / O0777777777777777 /
!CC     DATA LARGE(2) / O7777777777777777 /
!CC     DATA RIGHT(1) / O1461000000000000 /
!CC     DATA RIGHT(2) / O0000000000000000 /
!CC     DATA DIVER(1) / O1451000000000000 /
!CC     DATA DIVER(2) / O0000000000000000 /
!CC     DATA LOG10(1) / O1157163034761674 /
!CC     DATA LOG10(2) / O0006677466732724 /
!CC
!CC     MACHINE CONSTANTS FOR THE CDC 170/180 SERIES USING NOS/VE
!CC
!CC     DATA SMALL(1) / Z"3001800000000000" /
!CC     DATA SMALL(2) / Z"3001000000000000" /
!CC     DATA LARGE(1) / Z"4FFEFFFFFFFFFFFE" /
!CC     DATA LARGE(2) / Z"4FFE000000000000" /
!CC     DATA RIGHT(1) / Z"3FD2800000000000" /
!CC     DATA RIGHT(2) / Z"3FD2000000000000" /
!CC     DATA DIVER(1) / Z"3FD3800000000000" /
!CC     DATA DIVER(2) / Z"3FD3000000000000" /
!CC     DATA LOG10(1) / Z"3FFF9A209A84FBCF" /
!CC     DATA LOG10(2) / Z"3FFFF7988F8959AC" /
!CC
!CC     MACHINE CONSTANTS FOR THE CDC 6000/7000 SERIES
!CC
!CC     DATA SMALL(1) / 00564000000000000000B /
!CC     DATA SMALL(2) / 00000000000000000000B /
!CC     DATA LARGE(1) / 37757777777777777777B /
!CC     DATA LARGE(2) / 37157777777777777777B /
!CC     DATA RIGHT(1) / 15624000000000000000B /
!CC     DATA RIGHT(2) / 00000000000000000000B /
!CC     DATA DIVER(1) / 15634000000000000000B /
!CC     DATA DIVER(2) / 00000000000000000000B /
!CC     DATA LOG10(1) / 17164642023241175717B /
!CC     DATA LOG10(2) / 16367571421742254654B /
!CC
!CC     MACHINE CONSTANTS FOR THE CELERITY C1260
!CC
!CC     DATA SMALL(1), SMALL(2) / Z'00100000', Z'00000000' /
!CC     DATA LARGE(1), LARGE(2) / Z'7FEFFFFF', Z'FFFFFFFF' /
!CC     DATA RIGHT(1), RIGHT(2) / Z'3CA00000', Z'00000000' /
!CC     DATA DIVER(1), DIVER(2) / Z'3CB00000', Z'00000000' /
!CC     DATA LOG10(1), LOG10(2) / Z'3FD34413', Z'509F79FF' /
!CC
!CC     MACHINE CONSTANTS FOR THE CONVEX
!CC     USING THE -fn OR -pd8 COMPILER OPTION
!CC
!CC     DATA DMACH(1) / Z'0010000000000000' /
!CC     DATA DMACH(2) / Z'7FFFFFFFFFFFFFFF' /
!CC     DATA DMACH(3) / Z'3CC0000000000000' /
!CC     DATA DMACH(4) / Z'3CD0000000000000' /
!CC     DATA DMACH(5) / Z'3FF34413509F79FF' /
!CC
!CC     MACHINE CONSTANTS FOR THE CONVEX
!CC     USING THE -fi COMPILER OPTION
!CC
!CC     DATA DMACH(1) / Z'0010000000000000' /
!CC     DATA DMACH(2) / Z'7FEFFFFFFFFFFFFF' /
!CC     DATA DMACH(3) / Z'3CA0000000000000' /
!CC     DATA DMACH(4) / Z'3CB0000000000000' /
!CC     DATA DMACH(5) / Z'3FD34413509F79FF' /
!CC
!CC     MACHINE CONSTANTS FOR THE CONVEX
!CC     USING THE -p8 COMPILER OPTION
!CC
!CC     DATA DMACH(1) / Z'00010000000000000000000000000000' /
!CC     DATA DMACH(2) / Z'7FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF' /
!CC     DATA DMACH(3) / Z'3F900000000000000000000000000000' /
!CC     DATA DMACH(4) / Z'3F910000000000000000000000000000' /
!CC     DATA DMACH(5) / Z'3FFF34413509F79FEF311F12B35816F9' /
!CC
!CC     MACHINE CONSTANTS FOR THE CRAY
!CC
!CC     DATA SMALL(1) / 201354000000000000000B /
!CC     DATA SMALL(2) / 000000000000000000000B /
!CC     DATA LARGE(1) / 577767777777777777777B /
!CC     DATA LARGE(2) / 000007777777777777774B /
!CC     DATA RIGHT(1) / 376434000000000000000B /
!CC     DATA RIGHT(2) / 000000000000000000000B /
!CC     DATA DIVER(1) / 376444000000000000000B /
!CC     DATA DIVER(2) / 000000000000000000000B /
!CC     DATA LOG10(1) / 377774642023241175717B /
!CC     DATA LOG10(2) / 000007571421742254654B /
!CC
!CC     MACHINE CONSTANTS FOR THE DATA GENERAL ECLIPSE S/200
!CC     NOTE - IT MAY BE APPROPRIATE TO INCLUDE THE FOLLOWING CARD -
!CC     STATIC DMACH(5)
!CC
!CC     DATA SMALL /    20K, 3*0 /
!CC     DATA LARGE / 77777K, 3*177777K /
!CC     DATA RIGHT / 31420K, 3*0 /
!CC     DATA DIVER / 32020K, 3*0 /
!CC     DATA LOG10 / 40423K, 42023K, 50237K, 74776K /
!CC
!CC     MACHINE CONSTANTS FOR THE DEC ALPHA
!CC     USING G_FLOAT
!CC
!CC     DATA DMACH(1) / '0000000000000010'X /
!CC     DATA DMACH(2) / 'FFFFFFFFFFFF7FFF'X /
!CC     DATA DMACH(3) / '0000000000003CC0'X /
!CC     DATA DMACH(4) / '0000000000003CD0'X /
!CC     DATA DMACH(5) / '79FF509F44133FF3'X /
!CC
!CC     MACHINE CONSTANTS FOR THE DEC ALPHA
!CC     USING IEEE_FORMAT
!CC
!CC     DATA DMACH(1) / '0010000000000000'X /
!CC     DATA DMACH(2) / '7FEFFFFFFFFFFFFF'X /
!CC     DATA DMACH(3) / '3CA0000000000000'X /
!CC     DATA DMACH(4) / '3CB0000000000000'X /
!CC     DATA DMACH(5) / '3FD34413509F79FF'X /
!CC
!CC     MACHINE CONSTANTS FOR THE DEC RISC
!CC
!CC     DATA SMALL(1), SMALL(2) / Z'00000000', Z'00100000'/
!CC     DATA LARGE(1), LARGE(2) / Z'FFFFFFFF', Z'7FEFFFFF'/
!CC     DATA RIGHT(1), RIGHT(2) / Z'00000000', Z'3CA00000'/
!CC     DATA DIVER(1), DIVER(2) / Z'00000000', Z'3CB00000'/
!CC     DATA LOG10(1), LOG10(2) / Z'509F79FF', Z'3FD34413'/
!CC
!CC     MACHINE CONSTANTS FOR THE DEC VAX
!CC     USING D_FLOATING
!CC     (EXPRESSED IN INTEGER AND HEXADECIMAL)
!CC     THE HEX FORMAT BELOW MAY NOT BE SUITABLE FOR UNIX SYSTEMS
!CC     THE INTEGER FORMAT SHOULD BE OK FOR UNIX SYSTEMS
!CC
!CC     DATA SMALL(1), SMALL(2) /        128,           0 /
!CC     DATA LARGE(1), LARGE(2) /     -32769,          -1 /
!CC     DATA RIGHT(1), RIGHT(2) /       9344,           0 /
!CC     DATA DIVER(1), DIVER(2) /       9472,           0 /
!CC     DATA LOG10(1), LOG10(2) /  546979738,  -805796613 /
!CC
!CC     DATA SMALL(1), SMALL(2) / Z00000080, Z00000000 /
!CC     DATA LARGE(1), LARGE(2) / ZFFFF7FFF, ZFFFFFFFF /
!CC     DATA RIGHT(1), RIGHT(2) / Z00002480, Z00000000 /
!CC     DATA DIVER(1), DIVER(2) / Z00002500, Z00000000 /
!CC     DATA LOG10(1), LOG10(2) / Z209A3F9A, ZCFF884FB /
!CC
!CC     MACHINE CONSTANTS FOR THE DEC VAX
!CC     USING G_FLOATING
!CC     (EXPRESSED IN INTEGER AND HEXADECIMAL)
!CC     THE HEX FORMAT BELOW MAY NOT BE SUITABLE FOR UNIX SYSTEMS
!CC     THE INTEGER FORMAT SHOULD BE OK FOR UNIX SYSTEMS
!CC
!CC     DATA SMALL(1), SMALL(2) /         16,           0 /
!CC     DATA LARGE(1), LARGE(2) /     -32769,          -1 /
!CC     DATA RIGHT(1), RIGHT(2) /      15552,           0 /
!CC     DATA DIVER(1), DIVER(2) /      15568,           0 /
!CC     DATA LOG10(1), LOG10(2) /  1142112243, 2046775455 /
!CC
!CC     DATA SMALL(1), SMALL(2) / Z00000010, Z00000000 /
!CC     DATA LARGE(1), LARGE(2) / ZFFFF7FFF, ZFFFFFFFF /
!CC     DATA RIGHT(1), RIGHT(2) / Z00003CC0, Z00000000 /
!CC     DATA DIVER(1), DIVER(2) / Z00003CD0, Z00000000 /
!CC     DATA LOG10(1), LOG10(2) / Z44133FF3, Z79FF509F /
!CC
!CC     MACHINE CONSTANTS FOR THE ELXSI 6400
!CC     (ASSUMING REAL*8 IS THE DEFAULT DOUBLE PRECISION)
!CC
!CC     DATA SMALL(1), SMALL(2) / '00100000'X,'00000000'X /
!CC     DATA LARGE(1), LARGE(2) / '7FEFFFFF'X,'FFFFFFFF'X /
!CC     DATA RIGHT(1), RIGHT(2) / '3CB00000'X,'00000000'X /
!CC     DATA DIVER(1), DIVER(2) / '3CC00000'X,'00000000'X /
!CC     DATA LOG10(1), LOG10(2) / '3FD34413'X,'509F79FF'X /
!CC
!CC     MACHINE CONSTANTS FOR THE HARRIS 220
!CC
!CC     DATA SMALL(1), SMALL(2) / '20000000, '00000201 /
!CC     DATA LARGE(1), LARGE(2) / '37777777, '37777577 /
!CC     DATA RIGHT(1), RIGHT(2) / '20000000, '00000333 /
!CC     DATA DIVER(1), DIVER(2) / '20000000, '00000334 /
!CC     DATA LOG10(1), LOG10(2) / '23210115, '10237777 /
!CC
!CC     MACHINE CONSTANTS FOR THE HONEYWELL 600/6000 SERIES
!CC
!CC     DATA SMALL(1), SMALL(2) / O402400000000, O000000000000 /
!CC     DATA LARGE(1), LARGE(2) / O376777777777, O777777777777 /
!CC     DATA RIGHT(1), RIGHT(2) / O604400000000, O000000000000 /
!CC     DATA DIVER(1), DIVER(2) / O606400000000, O000000000000 /
!CC     DATA LOG10(1), LOG10(2) / O776464202324, O117571775714 /
!CC
!CC     MACHINE CONSTANTS FOR THE HP 730
!CC
!CC     DATA DMACH(1) / Z'0010000000000000' /
!CC     DATA DMACH(2) / Z'7FEFFFFFFFFFFFFF' /
!CC     DATA DMACH(3) / Z'3CA0000000000000' /
!CC     DATA DMACH(4) / Z'3CB0000000000000' /
!CC     DATA DMACH(5) / Z'3FD34413509F79FF' /
!CC
!CC     MACHINE CONSTANTS FOR THE HP 2100
!CC     THREE WORD DOUBLE PRECISION OPTION WITH FTN4
!CC
!CC     DATA SMALL(1), SMALL(2), SMALL(3) / 40000B,       0,       1 /
!CC     DATA LARGE(1), LARGE(2), LARGE(3) / 77777B, 177777B, 177776B /
!CC     DATA RIGHT(1), RIGHT(2), RIGHT(3) / 40000B,       0,    265B /
!CC     DATA DIVER(1), DIVER(2), DIVER(3) / 40000B,       0,    276B /
!CC     DATA LOG10(1), LOG10(2), LOG10(3) / 46420B,  46502B,  77777B /
!CC
!CC     MACHINE CONSTANTS FOR THE HP 2100
!CC     FOUR WORD DOUBLE PRECISION OPTION WITH FTN4
!CC
!CC     DATA SMALL(1), SMALL(2) /  40000B,       0 /
!CC     DATA SMALL(3), SMALL(4) /       0,       1 /
!CC     DATA LARGE(1), LARGE(2) /  77777B, 177777B /
!CC     DATA LARGE(3), LARGE(4) / 177777B, 177776B /
!CC     DATA RIGHT(1), RIGHT(2) /  40000B,       0 /
!CC     DATA RIGHT(3), RIGHT(4) /       0,    225B /
!CC     DATA DIVER(1), DIVER(2) /  40000B,       0 /
!CC     DATA DIVER(3), DIVER(4) /       0,    227B /
!CC     DATA LOG10(1), LOG10(2) /  46420B,  46502B /
!CC     DATA LOG10(3), LOG10(4) /  76747B, 176377B /
!CC
!CC     MACHINE CONSTANTS FOR THE HP 9000
!CC
!CC     DATA SMALL(1), SMALL(2) / 00040000000B, 00000000000B /
!CC     DATA LARGE(1), LARGE(2) / 17737777777B, 37777777777B /
!CC     DATA RIGHT(1), RIGHT(2) / 07454000000B, 00000000000B /
!CC     DATA DIVER(1), DIVER(2) / 07460000000B, 00000000000B /
!CC     DATA LOG10(1), LOG10(2) / 07764642023B, 12047674777B /
!CC
!CC     MACHINE CONSTANTS FOR THE IBM 360/370 SERIES,
!CC     THE XEROX SIGMA 5/7/9, THE SEL SYSTEMS 85/86, AND
!CC     THE PERKIN ELMER (INTERDATA) 7/32.
!CC
!CC     DATA SMALL(1), SMALL(2) / Z00100000, Z00000000 /
!CC     DATA LARGE(1), LARGE(2) / Z7FFFFFFF, ZFFFFFFFF /
!CC     DATA RIGHT(1), RIGHT(2) / Z33100000, Z00000000 /
!CC     DATA DIVER(1), DIVER(2) / Z34100000, Z00000000 /
!CC     DATA LOG10(1), LOG10(2) / Z41134413, Z509F79FF /
!CC
!CC     MACHINE CONSTANTS FOR THE IBM PC
!CC     ASSUMES THAT ALL ARITHMETIC IS DONE IN DOUBLE PRECISION
!CC     ON 8088, I.E., NOT IN 80 BIT FORM FOR THE 8087.
!CC
!CC     DATA SMALL(1) / 2.23D-308  /
!CC     DATA LARGE(1) / 1.79D+308  /
!CC     DATA RIGHT(1) / 1.11D-16   /
!CC     DATA DIVER(1) / 2.22D-16   /
!CC     DATA LOG10(1) / 0.301029995663981195D0 /
!CC
!CC     MACHINE CONSTANTS FOR THE IBM RS 6000
!CC
!CC     DATA DMACH(1) / Z'0010000000000000' /
!CC     DATA DMACH(2) / Z'7FEFFFFFFFFFFFFF' /
!CC     DATA DMACH(3) / Z'3CA0000000000000' /
!CC     DATA DMACH(4) / Z'3CB0000000000000' /
!CC     DATA DMACH(5) / Z'3FD34413509F79FF' /
!CC
!CC     MACHINE CONSTANTS FOR THE INTEL i860
!CC
!C      DATA DMACH(1) / Z'0010000000000000' /
!C      DATA DMACH(2) / Z'7FEFFFFFFFFFFFFF' /
!C      DATA DMACH(3) / Z'3CA0000000000000' /
!C      DATA DMACH(4) / Z'3CB0000000000000' /
!C      DATA DMACH(5) / Z'3FD34413509F79FF' /
!C
!CCAB      DATA DMACH(1)/0.22250738585072014026D-307/   ! 2^(-1021-1)
!CCAB      DATA DMACH(2)/1.79769313486231569228D+308/   ! 2^1024*(1-2^(-53))
!CCAB      DATA DMACH(3) /1.11022302462515654092D-16/   ! 2^(-53)
!CCAB      DATA DMACH(4) /2.22044604925031308184D-16/   ! 2^(1-53)
!CCAB      DATA DMACH(5) /0.30102999566398119521D0/     ! ln(2)/ln(10)
!C
!CC
!CC     MACHINE CONSTANTS FOR THE PDP-10 (KA PROCESSOR)
!CC
!CC     DATA SMALL(1), SMALL(2) / "033400000000, "000000000000 /
!CC     DATA LARGE(1), LARGE(2) / "377777777777, "344777777777 /
!CC     DATA RIGHT(1), RIGHT(2) / "113400000000, "000000000000 /
!CC     DATA DIVER(1), DIVER(2) / "114400000000, "000000000000 /
!CC     DATA LOG10(1), LOG10(2) / "177464202324, "144117571776 /
!CC
!CC     MACHINE CONSTANTS FOR THE PDP-10 (KI PROCESSOR)
!CC
!CC     DATA SMALL(1), SMALL(2) / "000400000000, "000000000000 /
!CC     DATA LARGE(1), LARGE(2) / "377777777777, "377777777777 /
!CC     DATA RIGHT(1), RIGHT(2) / "103400000000, "000000000000 /
!CC     DATA DIVER(1), DIVER(2) / "104400000000, "000000000000 /
!CC     DATA LOG10(1), LOG10(2) / "177464202324, "476747767461 /
!CC
!CC     MACHINE CONSTANTS FOR PDP-11 FORTRAN SUPPORTING
!CC     32-BIT INTEGERS (EXPRESSED IN INTEGER AND OCTAL).
!CC
!CC     DATA SMALL(1), SMALL(2) /    8388608,           0 /
!CC     DATA LARGE(1), LARGE(2) / 2147483647,          -1 /
!CC     DATA RIGHT(1), RIGHT(2) /  612368384,           0 /
!CC     DATA DIVER(1), DIVER(2) /  620756992,           0 /
!CC     DATA LOG10(1), LOG10(2) / 1067065498, -2063872008 /
!CC
!CC     DATA SMALL(1), SMALL(2) / O00040000000, O00000000000 /
!CC     DATA LARGE(1), LARGE(2) / O17777777777, O37777777777 /
!CC     DATA RIGHT(1), RIGHT(2) / O04440000000, O00000000000 /
!CC     DATA DIVER(1), DIVER(2) / O04500000000, O00000000000 /
!CC     DATA LOG10(1), LOG10(2) / O07746420232, O20476747770 /
!CC
!CC     MACHINE CONSTANTS FOR PDP-11 FORTRAN SUPPORTING
!CC     16-BIT INTEGERS (EXPRESSED IN INTEGER AND OCTAL).
!CC
!CC     DATA SMALL(1), SMALL(2) /    128,      0 /
!CC     DATA SMALL(3), SMALL(4) /      0,      0 /
!CC     DATA LARGE(1), LARGE(2) /  32767,     -1 /
!CC     DATA LARGE(3), LARGE(4) /     -1,     -1 /
!CC     DATA RIGHT(1), RIGHT(2) /   9344,      0 /
!CC     DATA RIGHT(3), RIGHT(4) /      0,      0 /
!CC     DATA DIVER(1), DIVER(2) /   9472,      0 /
!CC     DATA DIVER(3), DIVER(4) /      0,      0 /
!CC     DATA LOG10(1), LOG10(2) /  16282,   8346 /
!CC     DATA LOG10(3), LOG10(4) / -31493, -12296 /
!CC
!CC     DATA SMALL(1), SMALL(2) / O000200, O000000 /
!CC     DATA SMALL(3), SMALL(4) / O000000, O000000 /
!CC     DATA LARGE(1), LARGE(2) / O077777, O177777 /
!CC     DATA LARGE(3), LARGE(4) / O177777, O177777 /
!CC     DATA RIGHT(1), RIGHT(2) / O022200, O000000 /
!CC     DATA RIGHT(3), RIGHT(4) / O000000, O000000 /
!CC     DATA DIVER(1), DIVER(2) / O022400, O000000 /
!CC     DATA DIVER(3), DIVER(4) / O000000, O000000 /
!CC     DATA LOG10(1), LOG10(2) / O037632, O020232 /
!CC     DATA LOG10(3), LOG10(4) / O102373, O147770 /
!CC
!CC     MACHINE CONSTANTS FOR THE SILICON GRAPHICS
!CC
!CC     DATA SMALL(1), SMALL(2) / Z'00100000', Z'00000000' /
!CC     DATA LARGE(1), LARGE(2) / Z'7FEFFFFF', Z'FFFFFFFF' /
!CC     DATA RIGHT(1), RIGHT(2) / Z'3CA00000', Z'00000000' /
!CC     DATA DIVER(1), DIVER(2) / Z'3CB00000', Z'00000000' /
!CC     DATA LOG10(1), LOG10(2) / Z'3FD34413', Z'509F79FF' /
!CC
!CC     MACHINE CONSTANTS FOR THE SUN
!CC
!CC     DATA DMACH(1) / Z'0010000000000000' /
!CC     DATA DMACH(2) / Z'7FEFFFFFFFFFFFFF' /
!CC     DATA DMACH(3) / Z'3CA0000000000000' /
!CC     DATA DMACH(4) / Z'3CB0000000000000' /
!CC     DATA DMACH(5) / Z'3FD34413509F79FF' /
!CC
!CC     MACHINE CONSTANTS FOR THE SUN
!CC     USING THE -r8 COMPILER OPTION
!CC
!CC     DATA DMACH(1) / Z'00010000000000000000000000000000' /
!CC     DATA DMACH(2) / Z'7FFEFFFFFFFFFFFFFFFFFFFFFFFFFFFF' /
!CC     DATA DMACH(3) / Z'3F8E0000000000000000000000000000' /
!CC     DATA DMACH(4) / Z'3F8F0000000000000000000000000000' /
!CC     DATA DMACH(5) / Z'3FFD34413509F79FEF311F12B35816F9' /
!CC
!CC     MACHINE CONSTANTS FOR THE SUN 386i
!CC
!CC     DATA SMALL(1), SMALL(2) / Z'FFFFFFFD', Z'000FFFFF' /
!CC     DATA LARGE(1), LARGE(2) / Z'FFFFFFB0', Z'7FEFFFFF' /
!CC     DATA RIGHT(1), RIGHT(2) / Z'000000B0', Z'3CA00000' /
!CC     DATA DIVER(1), DIVER(2) / Z'FFFFFFCB', Z'3CAFFFFF'
!CC     DATA LOG10(1), LOG10(2) / Z'509F79E9', Z'3FD34413' /
!CC
!CC     MACHINE CONSTANTS FOR THE UNIVAC 1100 SERIES FTN COMPILER
!CC
!CC     DATA SMALL(1), SMALL(2) / O000040000000, O000000000000 /
!CC     DATA LARGE(1), LARGE(2) / O377777777777, O777777777777 /
!CC     DATA RIGHT(1), RIGHT(2) / O170540000000, O000000000000 /
!CC     DATA DIVER(1), DIVER(2) / O170640000000, O000000000000 /
!CC     DATA LOG10(1), LOG10(2) / O177746420232, O411757177572 /
!CC
!CC***FIRST EXECUTABLE STATEMENT  D1MACH
!CCC      IF (I .LT. 1 .OR. I .GT. 5) CALL XERMSG ('SLATEC', 'D1MACH',
!CCC     +   'I OUT OF BOUNDS', 1, 2)
!C      IF (I .LT. 1 .OR. I .GT. 5) STOP ' D1MACH ERROR'
!CC
!C      D1MACH = DMACH(I)
!C      RETURN
!CC
!C      END
!C*DECK I1MACH
!C      INTEGER FUNCTION I1MACH (I)
!CC***BEGIN PROLOGUE  I1MACH
!CC***PURPOSE  Return integer machine dependent constants.
!CC***LIBRARY   SLATEC
!CC***CATEGORY  R1
!CC***TYPE      INTEGER (I1MACH-I)
!CC***KEYWORDS  MACHINE CONSTANTS
!CC***AUTHOR  Fox, P. A., (Bell Labs)
!CC           Hall, A. D., (Bell Labs)
!CC           Schryer, N. L., (Bell Labs)
!CC***DESCRIPTION
!CC
!CC   I1MACH can be used to obtain machine-dependent parameters for the
!CC   local machine environment.  It is a function subprogram with one
!CC   (input) argument and can be referenced as follows:
!CC
!CC        K = I1MACH(I)
!CC
!CC   where I=1,...,16.  The (output) value of K above is determined by
!CC   the (input) value of I.  The results for various values of I are
!CC   discussed below.
!CC
!CC   I/O unit numbers:
!CC     I1MACH( 1) = the standard input unit.
!CC     I1MACH( 2) = the standard output unit.
!CC     I1MACH( 3) = the standard punch unit.
!CC     I1MACH( 4) = the standard error message unit.
!CC
!CC   Words:
!CC     I1MACH( 5) = the number of bits per integer storage unit.
!CC     I1MACH( 6) = the number of characters per integer storage unit.
!CC
!CC   Integers:
!CC     assume integers are represented in the S-digit, base-A form
!CC
!CC                sign ( X(S-1)*A**(S-1) + ... + X(1)*A + X(0) )
!CC
!CC                where 0 .LE. X(I) .LT. A for I=0,...,S-1.
!CC     I1MACH( 7) = A, the base.
!CC     I1MACH( 8) = S, the number of base-A digits.
!CC     I1MACH( 9) = A**S - 1, the largest magnitude.
!CC
!CC   Floating-Point Numbers:
!CC     Assume floating-point numbers are represented in the T-digit,
!CC     base-B form
!CC                sign (B**E)*( (X(1)/B) + ... + (X(T)/B**T) )
!CC
!CC                where 0 .LE. X(I) .LT. B for I=1,...,T,
!CC                0 .LT. X(1), and EMIN .LE. E .LE. EMAX.
!CC     I1MACH(10) = B, the base.
!CC
!CC   Single-Precision:
!CC     I1MACH(11) = T, the number of base-B digits.
!CC     I1MACH(12) = EMIN, the smallest exponent E.
!CC     I1MACH(13) = EMAX, the largest exponent E.
!CC
!CC   Double-Precision:
!CC     I1MACH(14) = T, the number of base-B digits.
!CC     I1MACH(15) = EMIN, the smallest exponent E.
!CC     I1MACH(16) = EMAX, the largest exponent E.
!CC
!CC   To alter this function for a particular environment, the desired
!CC   set of DATA statements should be activated by removing the C from
!CC   column 1.  Also, the values of I1MACH(1) - I1MACH(4) should be
!CC   checked for consistency with the local operating system.
!CC
!CC***REFERENCES  P. A. Fox, A. D. Hall and N. L. Schryer, Framework for
!CC                 a portable library, ACM Transactions on Mathematical
!CC                 Software 4, 2 (June 1978), pp. 177-188.
!CC***ROUTINES CALLED  (NONE)
!CC***REVISION HISTORY  (YYMMDD)
!CC   750101  DATE WRITTEN
!CC   891012  Added VAX G-floating constants.  (WRB)
!CC   891012  REVISION DATE from Version 3.2
!CC   891214  Prologue converted to Version 4.0 format.  (BAB)
!CC   900618  Added DEC RISC constants.  (WRB)
!CC   900723  Added IBM RS 6000 constants.  (WRB)
!CC   901009  Correct I1MACH(7) for IBM Mainframes. Should be 2 not 16.
!CC           (RWC)
!CC   910710  Added HP 730 constants.  (SMR)
!CC   911114  Added Convex IEEE constants.  (WRB)
!CC   920121  Added SUN -r8 compiler option constants.  (WRB)
!CC   920229  Added Touchstone Delta i860 constants.  (WRB)
!CC   920501  Reformatted the REFERENCES section.  (WRB)
!CC   920625  Added Convex -p8 and -pd8 compiler option constants.
!CC           (BKS, WRB)
!CC   930201  Added DEC Alpha and SGI constants.  (RWC and WRB)
!CC   930618  Corrected I1MACH(5) for Convex -p8 and -pd8 compiler
!CC           options.  (DWL, RWC and WRB).
!CC***END PROLOGUE  I1MACH
!CC
!C      INTEGER IMACH(16),OUTPUT
!C      SAVE IMACH
!C      EQUIVALENCE (IMACH(4),OUTPUT)
!CC
!CC     MACHINE CONSTANTS FOR THE AMIGA
!CC     ABSOFT COMPILER
!CC
!CC     DATA IMACH( 1) /          5 /
!CC     DATA IMACH( 2) /          6 /
!CC     DATA IMACH( 3) /          5 /
!CC     DATA IMACH( 4) /          6 /
!CC     DATA IMACH( 5) /         32 /
!CC     DATA IMACH( 6) /          4 /
!CC     DATA IMACH( 7) /          2 /
!CC     DATA IMACH( 8) /         31 /
!CC     DATA IMACH( 9) / 2147483647 /
!CC     DATA IMACH(10) /          2 /
!CC     DATA IMACH(11) /         24 /
!CC     DATA IMACH(12) /       -126 /
!CC     DATA IMACH(13) /        127 /
!CC     DATA IMACH(14) /         53 /
!CC     DATA IMACH(15) /      -1022 /
!CC     DATA IMACH(16) /       1023 /
!CC
!CC     MACHINE CONSTANTS FOR THE APOLLO
!CC
!CC     DATA IMACH( 1) /          5 /
!CC     DATA IMACH( 2) /          6 /
!CC     DATA IMACH( 3) /          6 /
!CC     DATA IMACH( 4) /          6 /
!CC     DATA IMACH( 5) /         32 /
!CC     DATA IMACH( 6) /          4 /
!CC     DATA IMACH( 7) /          2 /
!CC     DATA IMACH( 8) /         31 /
!CC     DATA IMACH( 9) / 2147483647 /
!CC     DATA IMACH(10) /          2 /
!CC     DATA IMACH(11) /         24 /
!CC     DATA IMACH(12) /       -125 /
!CC     DATA IMACH(13) /        129 /
!CC     DATA IMACH(14) /         53 /
!CC     DATA IMACH(15) /      -1021 /
!CC     DATA IMACH(16) /       1025 /
!CC
!CC     MACHINE CONSTANTS FOR THE BURROUGHS 1700 SYSTEM
!CC
!CC     DATA IMACH( 1) /          7 /
!CC     DATA IMACH( 2) /          2 /
!CC     DATA IMACH( 3) /          2 /
!CC     DATA IMACH( 4) /          2 /
!CC     DATA IMACH( 5) /         36 /
!CC     DATA IMACH( 6) /          4 /
!CC     DATA IMACH( 7) /          2 /
!CC     DATA IMACH( 8) /         33 /
!CC     DATA IMACH( 9) / Z1FFFFFFFF /
!CC     DATA IMACH(10) /          2 /
!CC     DATA IMACH(11) /         24 /
!CC     DATA IMACH(12) /       -256 /
!CC     DATA IMACH(13) /        255 /
!CC     DATA IMACH(14) /         60 /
!CC     DATA IMACH(15) /       -256 /
!CC     DATA IMACH(16) /        255 /
!CC
!CC     MACHINE CONSTANTS FOR THE BURROUGHS 5700 SYSTEM
!CC
!CC     DATA IMACH( 1) /          5 /
!CC     DATA IMACH( 2) /          6 /
!CC     DATA IMACH( 3) /          7 /
!CC     DATA IMACH( 4) /          6 /
!CC     DATA IMACH( 5) /         48 /
!CC     DATA IMACH( 6) /          6 /
!CC     DATA IMACH( 7) /          2 /
!CC     DATA IMACH( 8) /         39 /
!CC     DATA IMACH( 9) / O0007777777777777 /
!CC     DATA IMACH(10) /          8 /
!CC     DATA IMACH(11) /         13 /
!CC     DATA IMACH(12) /        -50 /
!CC     DATA IMACH(13) /         76 /
!CC     DATA IMACH(14) /         26 /
!CC     DATA IMACH(15) /        -50 /
!CC     DATA IMACH(16) /         76 /
!CC
!CC     MACHINE CONSTANTS FOR THE BURROUGHS 6700/7700 SYSTEMS
!CC
!CC     DATA IMACH( 1) /          5 /
!CC     DATA IMACH( 2) /          6 /
!CC     DATA IMACH( 3) /          7 /
!CC     DATA IMACH( 4) /          6 /
!CC     DATA IMACH( 5) /         48 /
!CC     DATA IMACH( 6) /          6 /
!CC     DATA IMACH( 7) /          2 /
!CC     DATA IMACH( 8) /         39 /
!CC     DATA IMACH( 9) / O0007777777777777 /
!CC     DATA IMACH(10) /          8 /
!CC     DATA IMACH(11) /         13 /
!CC     DATA IMACH(12) /        -50 /
!CC     DATA IMACH(13) /         76 /
!CC     DATA IMACH(14) /         26 /
!CC     DATA IMACH(15) /     -32754 /
!CC     DATA IMACH(16) /      32780 /
!CC
!CC     MACHINE CONSTANTS FOR THE CDC 170/180 SERIES USING NOS/VE
!CC
!CC     DATA IMACH( 1) /          5 /
!CC     DATA IMACH( 2) /          6 /
!CC     DATA IMACH( 3) /          7 /
!CC     DATA IMACH( 4) /          6 /
!CC     DATA IMACH( 5) /         64 /
!CC     DATA IMACH( 6) /          8 /
!CC     DATA IMACH( 7) /          2 /
!CC     DATA IMACH( 8) /         63 /
!CC     DATA IMACH( 9) / 9223372036854775807 /
!CC     DATA IMACH(10) /          2 /
!CC     DATA IMACH(11) /         47 /
!CC     DATA IMACH(12) /      -4095 /
!CC     DATA IMACH(13) /       4094 /
!CC     DATA IMACH(14) /         94 /
!CC     DATA IMACH(15) /      -4095 /
!CC     DATA IMACH(16) /       4094 /
!CC
!CC     MACHINE CONSTANTS FOR THE CDC 6000/7000 SERIES
!CC
!CC     DATA IMACH( 1) /          5 /
!CC     DATA IMACH( 2) /          6 /
!CC     DATA IMACH( 3) /          7 /
!CC     DATA IMACH( 4) /    6LOUTPUT/
!CC     DATA IMACH( 5) /         60 /
!CC     DATA IMACH( 6) /         10 /
!CC     DATA IMACH( 7) /          2 /
!CC     DATA IMACH( 8) /         48 /
!CC     DATA IMACH( 9) / 00007777777777777777B /
!CC     DATA IMACH(10) /          2 /
!CC     DATA IMACH(11) /         47 /
!CC     DATA IMACH(12) /       -929 /
!CC     DATA IMACH(13) /       1070 /
!CC     DATA IMACH(14) /         94 /
!CC     DATA IMACH(15) /       -929 /
!CC     DATA IMACH(16) /       1069 /
!CC
!CC     MACHINE CONSTANTS FOR THE CELERITY C1260
!CC
!CC     DATA IMACH( 1) /          5 /
!CC     DATA IMACH( 2) /          6 /
!CC     DATA IMACH( 3) /          6 /
!CC     DATA IMACH( 4) /          0 /
!CC     DATA IMACH( 5) /         32 /
!CC     DATA IMACH( 6) /          4 /
!CC     DATA IMACH( 7) /          2 /
!CC     DATA IMACH( 8) /         31 /
!CC     DATA IMACH( 9) / Z'7FFFFFFF' /
!CC     DATA IMACH(10) /          2 /
!CC     DATA IMACH(11) /         24 /
!CC     DATA IMACH(12) /       -126 /
!CC     DATA IMACH(13) /        127 /
!CC     DATA IMACH(14) /         53 /
!CC     DATA IMACH(15) /      -1022 /
!CC     DATA IMACH(16) /       1023 /
!CC
!CC     MACHINE CONSTANTS FOR THE CONVEX
!CC     USING THE -fn COMPILER OPTION
!CC
!CC     DATA IMACH( 1) /          5 /
!CC     DATA IMACH( 2) /          6 /
!CC     DATA IMACH( 3) /          7 /
!CC     DATA IMACH( 4) /          6 /
!CC     DATA IMACH( 5) /         32 /
!CC     DATA IMACH( 6) /          4 /
!CC     DATA IMACH( 7) /          2 /
!CC     DATA IMACH( 8) /         31 /
!CC     DATA IMACH( 9) / 2147483647 /
!CC     DATA IMACH(10) /          2 /
!CC     DATA IMACH(11) /         24 /
!CC     DATA IMACH(12) /       -127 /
!CC     DATA IMACH(13) /        127 /
!CC     DATA IMACH(14) /         53 /
!CC     DATA IMACH(15) /      -1023 /
!CC     DATA IMACH(16) /       1023 /
!CC
!CC     MACHINE CONSTANTS FOR THE CONVEX
!CC     USING THE -fi COMPILER OPTION
!CC
!CC     DATA IMACH( 1) /          5 /
!CC     DATA IMACH( 2) /          6 /
!CC     DATA IMACH( 3) /          7 /
!CC     DATA IMACH( 4) /          6 /
!CC     DATA IMACH( 5) /         32 /
!CC     DATA IMACH( 6) /          4 /
!CC     DATA IMACH( 7) /          2 /
!CC     DATA IMACH( 8) /         31 /
!CC     DATA IMACH( 9) / 2147483647 /
!CC     DATA IMACH(10) /          2 /
!CC     DATA IMACH(11) /         24 /
!CC     DATA IMACH(12) /       -125 /
!CC     DATA IMACH(13) /        128 /
!CC     DATA IMACH(14) /         53 /
!CC     DATA IMACH(15) /      -1021 /
!CC     DATA IMACH(16) /       1024 /
!CC
!CC     MACHINE CONSTANTS FOR THE CONVEX
!CC     USING THE -p8 COMPILER OPTION
!CC
!CC     DATA IMACH( 1) /          5 /
!CC     DATA IMACH( 2) /          6 /
!CC     DATA IMACH( 3) /          7 /
!CC     DATA IMACH( 4) /          6 /
!CC     DATA IMACH( 5) /         64 /
!CC     DATA IMACH( 6) /          4 /
!CC     DATA IMACH( 7) /          2 /
!CC     DATA IMACH( 8) /         63 /
!CC     DATA IMACH( 9) / 9223372036854775807 /
!CC     DATA IMACH(10) /          2 /
!CC     DATA IMACH(11) /         53 /
!CC     DATA IMACH(12) /      -1023 /
!CC     DATA IMACH(13) /       1023 /
!CC     DATA IMACH(14) /        113 /
!CC     DATA IMACH(15) /     -16383 /
!CC     DATA IMACH(16) /      16383 /
!CC
!CC     MACHINE CONSTANTS FOR THE CONVEX
!CC     USING THE -pd8 COMPILER OPTION
!CC
!CC     DATA IMACH( 1) /          5 /
!CC     DATA IMACH( 2) /          6 /
!CC     DATA IMACH( 3) /          7 /
!CC     DATA IMACH( 4) /          6 /
!CC     DATA IMACH( 5) /         64 /
!CC     DATA IMACH( 6) /          4 /
!CC     DATA IMACH( 7) /          2 /
!CC     DATA IMACH( 8) /         63 /
!CC     DATA IMACH( 9) / 9223372036854775807 /
!CC     DATA IMACH(10) /          2 /
!CC     DATA IMACH(11) /         53 /
!CC     DATA IMACH(12) /      -1023 /
!CC     DATA IMACH(13) /       1023 /
!CC     DATA IMACH(14) /         53 /
!CC     DATA IMACH(15) /      -1023 /
!CC     DATA IMACH(16) /       1023 /
!CC
!CC     MACHINE CONSTANTS FOR THE CRAY
!CC     USING THE 46 BIT INTEGER COMPILER OPTION
!CC
!CC     DATA IMACH( 1) /        100 /
!CC     DATA IMACH( 2) /        101 /
!CC     DATA IMACH( 3) /        102 /
!CC     DATA IMACH( 4) /        101 /
!CC     DATA IMACH( 5) /         64 /
!CC     DATA IMACH( 6) /          8 /
!CC     DATA IMACH( 7) /          2 /
!CC     DATA IMACH( 8) /         46 /
!CC     DATA IMACH( 9) / 1777777777777777B /
!CC     DATA IMACH(10) /          2 /
!CC     DATA IMACH(11) /         47 /
!CC     DATA IMACH(12) /      -8189 /
!CC     DATA IMACH(13) /       8190 /
!CC     DATA IMACH(14) /         94 /
!CC     DATA IMACH(15) /      -8099 /
!CC     DATA IMACH(16) /       8190 /
!CC
!CC     MACHINE CONSTANTS FOR THE CRAY
!CC     USING THE 64 BIT INTEGER COMPILER OPTION
!CC
!CC     DATA IMACH( 1) /        100 /
!CC     DATA IMACH( 2) /        101 /
!CC     DATA IMACH( 3) /        102 /
!CC     DATA IMACH( 4) /        101 /
!CC     DATA IMACH( 5) /         64 /
!CC     DATA IMACH( 6) /          8 /
!CC     DATA IMACH( 7) /          2 /
!CC     DATA IMACH( 8) /         63 /
!CC     DATA IMACH( 9) / 777777777777777777777B /
!CC     DATA IMACH(10) /          2 /
!CC     DATA IMACH(11) /         47 /
!CC     DATA IMACH(12) /      -8189 /
!CC     DATA IMACH(13) /       8190 /
!CC     DATA IMACH(14) /         94 /
!CC     DATA IMACH(15) /      -8099 /
!CC     DATA IMACH(16) /       8190 /
!CC
!CC     MACHINE CONSTANTS FOR THE DATA GENERAL ECLIPSE S/200
!CC
!CC     DATA IMACH( 1) /         11 /
!CC     DATA IMACH( 2) /         12 /
!CC     DATA IMACH( 3) /          8 /
!CC     DATA IMACH( 4) /         10 /
!CC     DATA IMACH( 5) /         16 /
!CC     DATA IMACH( 6) /          2 /
!CC     DATA IMACH( 7) /          2 /
!CC     DATA IMACH( 8) /         15 /
!CC     DATA IMACH( 9) /      32767 /
!CC     DATA IMACH(10) /         16 /
!CC     DATA IMACH(11) /          6 /
!CC     DATA IMACH(12) /        -64 /
!CC     DATA IMACH(13) /         63 /
!CC     DATA IMACH(14) /         14 /
!CC     DATA IMACH(15) /        -64 /
!CC     DATA IMACH(16) /         63 /
!CC
!CC     MACHINE CONSTANTS FOR THE DEC ALPHA
!CC     USING G_FLOAT
!CC
!CC     DATA IMACH( 1) /          5 /
!CC     DATA IMACH( 2) /          6 /
!CC     DATA IMACH( 3) /          5 /
!CC     DATA IMACH( 4) /          6 /
!CC     DATA IMACH( 5) /         32 /
!CC     DATA IMACH( 6) /          4 /
!CC     DATA IMACH( 7) /          2 /
!CC     DATA IMACH( 8) /         31 /
!CC     DATA IMACH( 9) / 2147483647 /
!CC     DATA IMACH(10) /          2 /
!CC     DATA IMACH(11) /         24 /
!CC     DATA IMACH(12) /       -127 /
!CC     DATA IMACH(13) /        127 /
!CC     DATA IMACH(14) /         53 /
!CC     DATA IMACH(15) /      -1023 /
!CC     DATA IMACH(16) /       1023 /
!CC
!CC     MACHINE CONSTANTS FOR THE DEC ALPHA
!CC     USING IEEE_FLOAT
!CC
!CC     DATA IMACH( 1) /          5 /
!CC     DATA IMACH( 2) /          6 /
!CC     DATA IMACH( 3) /          6 /
!CC     DATA IMACH( 4) /          6 /
!CC     DATA IMACH( 5) /         32 /
!CC     DATA IMACH( 6) /          4 /
!CC     DATA IMACH( 7) /          2 /
!CC     DATA IMACH( 8) /         31 /
!CC     DATA IMACH( 9) / 2147483647 /
!CC     DATA IMACH(10) /          2 /
!CC     DATA IMACH(11) /         24 /
!CC     DATA IMACH(12) /       -125 /
!CC     DATA IMACH(13) /        128 /
!CC     DATA IMACH(14) /         53 /
!CC     DATA IMACH(15) /      -1021 /
!CC     DATA IMACH(16) /       1024 /
!CC
!CC     MACHINE CONSTANTS FOR THE DEC RISC
!CC
!CC     DATA IMACH( 1) /          5 /
!CC     DATA IMACH( 2) /          6 /
!CC     DATA IMACH( 3) /          6 /
!CC     DATA IMACH( 4) /          6 /
!CC     DATA IMACH( 5) /         32 /
!CC     DATA IMACH( 6) /          4 /
!CC     DATA IMACH( 7) /          2 /
!CC     DATA IMACH( 8) /         31 /
!CC     DATA IMACH( 9) / 2147483647 /
!CC     DATA IMACH(10) /          2 /
!CC     DATA IMACH(11) /         24 /
!CC     DATA IMACH(12) /       -125 /
!CC     DATA IMACH(13) /        128 /
!CC     DATA IMACH(14) /         53 /
!CC     DATA IMACH(15) /      -1021 /
!CC     DATA IMACH(16) /       1024 /
!CC
!CC     MACHINE CONSTANTS FOR THE DEC VAX
!CC     USING D_FLOATING
!CC
!CC     DATA IMACH( 1) /          5 /
!CC     DATA IMACH( 2) /          6 /
!CC     DATA IMACH( 3) /          5 /
!CC     DATA IMACH( 4) /          6 /
!CC     DATA IMACH( 5) /         32 /
!CC     DATA IMACH( 6) /          4 /
!CC     DATA IMACH( 7) /          2 /
!CC     DATA IMACH( 8) /         31 /
!CC     DATA IMACH( 9) / 2147483647 /
!CC     DATA IMACH(10) /          2 /
!CC     DATA IMACH(11) /         24 /
!CC     DATA IMACH(12) /       -127 /
!CC     DATA IMACH(13) /        127 /
!CC     DATA IMACH(14) /         56 /
!CC     DATA IMACH(15) /       -127 /
!CC     DATA IMACH(16) /        127 /
!CC
!CC     MACHINE CONSTANTS FOR THE DEC VAX
!CC     USING G_FLOATING
!CC
!CC     DATA IMACH( 1) /          5 /
!CC     DATA IMACH( 2) /          6 /
!CC     DATA IMACH( 3) /          5 /
!CC     DATA IMACH( 4) /          6 /
!CC     DATA IMACH( 5) /         32 /
!CC     DATA IMACH( 6) /          4 /
!CC     DATA IMACH( 7) /          2 /
!CC     DATA IMACH( 8) /         31 /
!CC     DATA IMACH( 9) / 2147483647 /
!CC     DATA IMACH(10) /          2 /
!CC     DATA IMACH(11) /         24 /
!CC     DATA IMACH(12) /       -127 /
!CC     DATA IMACH(13) /        127 /
!CC     DATA IMACH(14) /         53 /
!CC     DATA IMACH(15) /      -1023 /
!CC     DATA IMACH(16) /       1023 /
!CC
!CC     MACHINE CONSTANTS FOR THE ELXSI 6400
!CC
!CC     DATA IMACH( 1) /          5 /
!CC     DATA IMACH( 2) /          6 /
!CC     DATA IMACH( 3) /          6 /
!CC     DATA IMACH( 4) /          6 /
!CC     DATA IMACH( 5) /         32 /
!CC     DATA IMACH( 6) /          4 /
!CC     DATA IMACH( 7) /          2 /
!CC     DATA IMACH( 8) /         32 /
!CC     DATA IMACH( 9) / 2147483647 /
!CC     DATA IMACH(10) /          2 /
!CC     DATA IMACH(11) /         24 /
!CC     DATA IMACH(12) /       -126 /
!CC     DATA IMACH(13) /        127 /
!CC     DATA IMACH(14) /         53 /
!CC     DATA IMACH(15) /      -1022 /
!CC     DATA IMACH(16) /       1023 /
!CC
!CC     MACHINE CONSTANTS FOR THE HARRIS 220
!CC
!CC     DATA IMACH( 1) /          5 /
!CC     DATA IMACH( 2) /          6 /
!CC     DATA IMACH( 3) /          0 /
!CC     DATA IMACH( 4) /          6 /
!CC     DATA IMACH( 5) /         24 /
!CC     DATA IMACH( 6) /          3 /
!CC     DATA IMACH( 7) /          2 /
!CC     DATA IMACH( 8) /         23 /
!CC     DATA IMACH( 9) /    8388607 /
!CC     DATA IMACH(10) /          2 /
!CC     DATA IMACH(11) /         23 /
!CC     DATA IMACH(12) /       -127 /
!CC     DATA IMACH(13) /        127 /
!CC     DATA IMACH(14) /         38 /
!CC     DATA IMACH(15) /       -127 /
!CC     DATA IMACH(16) /        127 /
!CC
!CC     MACHINE CONSTANTS FOR THE HONEYWELL 600/6000 SERIES
!CC
!CC     DATA IMACH( 1) /          5 /
!CC     DATA IMACH( 2) /          6 /
!CC     DATA IMACH( 3) /         43 /
!CC     DATA IMACH( 4) /          6 /
!CC     DATA IMACH( 5) /         36 /
!CC     DATA IMACH( 6) /          6 /
!CC     DATA IMACH( 7) /          2 /
!CC     DATA IMACH( 8) /         35 /
!CC     DATA IMACH( 9) / O377777777777 /
!CC     DATA IMACH(10) /          2 /
!CC     DATA IMACH(11) /         27 /
!CC     DATA IMACH(12) /       -127 /
!CC     DATA IMACH(13) /        127 /
!CC     DATA IMACH(14) /         63 /
!CC     DATA IMACH(15) /       -127 /
!CC     DATA IMACH(16) /        127 /
!CC
!CC     MACHINE CONSTANTS FOR THE HP 730
!CC
!CC     DATA IMACH( 1) /          5 /
!CC     DATA IMACH( 2) /          6 /
!CC     DATA IMACH( 3) /          6 /
!CC     DATA IMACH( 4) /          6 /
!CC     DATA IMACH( 5) /         32 /
!CC     DATA IMACH( 6) /          4 /
!CC     DATA IMACH( 7) /          2 /
!CC     DATA IMACH( 8) /         31 /
!CC     DATA IMACH( 9) / 2147483647 /
!CC     DATA IMACH(10) /          2 /
!CC     DATA IMACH(11) /         24 /
!CC     DATA IMACH(12) /       -125 /
!CC     DATA IMACH(13) /        128 /
!CC     DATA IMACH(14) /         53 /
!CC     DATA IMACH(15) /      -1021 /
!CC     DATA IMACH(16) /       1024 /
!CC
!CC     MACHINE CONSTANTS FOR THE HP 2100
!CC     3 WORD DOUBLE PRECISION OPTION WITH FTN4
!CC
!CC     DATA IMACH( 1) /          5 /
!CC     DATA IMACH( 2) /          6 /
!CC     DATA IMACH( 3) /          4 /
!CC     DATA IMACH( 4) /          1 /
!CC     DATA IMACH( 5) /         16 /
!CC     DATA IMACH( 6) /          2 /
!CC     DATA IMACH( 7) /          2 /
!CC     DATA IMACH( 8) /         15 /
!CC     DATA IMACH( 9) /      32767 /
!CC     DATA IMACH(10) /          2 /
!CC     DATA IMACH(11) /         23 /
!CC     DATA IMACH(12) /       -128 /
!CC     DATA IMACH(13) /        127 /
!CC     DATA IMACH(14) /         39 /
!CC     DATA IMACH(15) /       -128 /
!CC     DATA IMACH(16) /        127 /
!CC
!CC     MACHINE CONSTANTS FOR THE HP 2100
!CC     4 WORD DOUBLE PRECISION OPTION WITH FTN4
!CC
!CC     DATA IMACH( 1) /          5 /
!CC     DATA IMACH( 2) /          6 /
!CC     DATA IMACH( 3) /          4 /
!CC     DATA IMACH( 4) /          1 /
!CC     DATA IMACH( 5) /         16 /
!CC     DATA IMACH( 6) /          2 /
!CC     DATA IMACH( 7) /          2 /
!CC     DATA IMACH( 8) /         15 /
!CC     DATA IMACH( 9) /      32767 /
!CC     DATA IMACH(10) /          2 /
!CC     DATA IMACH(11) /         23 /
!CC     DATA IMACH(12) /       -128 /
!CC     DATA IMACH(13) /        127 /
!CC     DATA IMACH(14) /         55 /
!CC     DATA IMACH(15) /       -128 /
!CC     DATA IMACH(16) /        127 /
!CC
!CC     MACHINE CONSTANTS FOR THE HP 9000
!CC
!CC     DATA IMACH( 1) /          5 /
!CC     DATA IMACH( 2) /          6 /
!CC     DATA IMACH( 3) /          6 /
!CC     DATA IMACH( 4) /          7 /
!CC     DATA IMACH( 5) /         32 /
!CC     DATA IMACH( 6) /          4 /
!CC     DATA IMACH( 7) /          2 /
!CC     DATA IMACH( 8) /         32 /
!CC     DATA IMACH( 9) / 2147483647 /
!CC     DATA IMACH(10) /          2 /
!CC     DATA IMACH(11) /         24 /
!CC     DATA IMACH(12) /       -126 /
!CC     DATA IMACH(13) /        127 /
!CC     DATA IMACH(14) /         53 /
!CC     DATA IMACH(15) /      -1015 /
!CC     DATA IMACH(16) /       1017 /
!CC
!CC     MACHINE CONSTANTS FOR THE IBM 360/370 SERIES,
!CC     THE XEROX SIGMA 5/7/9, THE SEL SYSTEMS 85/86, AND
!CC     THE PERKIN ELMER (INTERDATA) 7/32.
!CC
!CC     DATA IMACH( 1) /          5 /
!CC     DATA IMACH( 2) /          6 /
!CC     DATA IMACH( 3) /          7 /
!CC     DATA IMACH( 4) /          6 /
!CC     DATA IMACH( 5) /         32 /
!CC     DATA IMACH( 6) /          4 /
!CC     DATA IMACH( 7) /          2 /
!CC     DATA IMACH( 8) /         31 /
!CC     DATA IMACH( 9) /  Z7FFFFFFF /
!CC     DATA IMACH(10) /         16 /
!CC     DATA IMACH(11) /          6 /
!CC     DATA IMACH(12) /        -64 /
!CC     DATA IMACH(13) /         63 /
!CC     DATA IMACH(14) /         14 /
!CC     DATA IMACH(15) /        -64 /
!CC     DATA IMACH(16) /         63 /
!CC
!CC     MACHINE CONSTANTS FOR THE IBM PC
!CC
!CC     DATA IMACH( 1) /          5 /
!CC     DATA IMACH( 2) /          6 /
!CC     DATA IMACH( 3) /          0 /
!CC     DATA IMACH( 4) /          0 /
!CC     DATA IMACH( 5) /         32 /
!CC     DATA IMACH( 6) /          4 /
!CC     DATA IMACH( 7) /          2 /
!CC     DATA IMACH( 8) /         31 /
!CC     DATA IMACH( 9) / 2147483647 /
!CC     DATA IMACH(10) /          2 /
!CC     DATA IMACH(11) /         24 /
!CC     DATA IMACH(12) /       -125 /
!CC     DATA IMACH(13) /        127 /
!CC     DATA IMACH(14) /         53 /
!CC     DATA IMACH(15) /      -1021 /
!CC     DATA IMACH(16) /       1023 /
!CC
!CC     MACHINE CONSTANTS FOR THE IBM RS 6000
!CC
!CC     DATA IMACH( 1) /          5 /
!CC     DATA IMACH( 2) /          6 /
!CC     DATA IMACH( 3) /          6 /
!CC     DATA IMACH( 4) /          0 /
!CC     DATA IMACH( 5) /         32 /
!CC     DATA IMACH( 6) /          4 /
!CC     DATA IMACH( 7) /          2 /
!CC     DATA IMACH( 8) /         31 /
!CC     DATA IMACH( 9) / 2147483647 /
!CC     DATA IMACH(10) /          2 /
!CC     DATA IMACH(11) /         24 /
!CC     DATA IMACH(12) /       -125 /
!CC     DATA IMACH(13) /        128 /
!CC     DATA IMACH(14) /         53 /
!CC     DATA IMACH(15) /      -1021 /
!CC     DATA IMACH(16) /       1024 /
!CC
!CC     MACHINE CONSTANTS FOR THE INTEL i860
!CC
!C      DATA IMACH( 1) /          5 /
!C      DATA IMACH( 2) /          6 /
!C      DATA IMACH( 3) /          6 /
!C      DATA IMACH( 4) /          6 /
!C      DATA IMACH( 5) /         32 /
!C      DATA IMACH( 6) /          4 /
!C      DATA IMACH( 7) /          2 /
!C      DATA IMACH( 8) /         31 /
!C      DATA IMACH( 9) / 2147483647 /
!C      DATA IMACH(10) /          2 /
!C      DATA IMACH(11) /         24 /
!C      DATA IMACH(12) /       -125 /
!C      DATA IMACH(13) /        128 /
!C      DATA IMACH(14) /         53 /
!C      DATA IMACH(15) /      -1021 /
!C      DATA IMACH(16) /       1024 /
!CC
!CC     MACHINE CONSTANTS FOR THE PDP-10 (KA PROCESSOR)
!CC
!CC     DATA IMACH( 1) /          5 /
!CC     DATA IMACH( 2) /          6 /
!CC     DATA IMACH( 3) /          5 /
!CC     DATA IMACH( 4) /          6 /
!CC     DATA IMACH( 5) /         36 /
!CC     DATA IMACH( 6) /          5 /
!CC     DATA IMACH( 7) /          2 /
!CC     DATA IMACH( 8) /         35 /
!CC     DATA IMACH( 9) / "377777777777 /
!CC     DATA IMACH(10) /          2 /
!CC     DATA IMACH(11) /         27 /
!CC     DATA IMACH(12) /       -128 /
!CC     DATA IMACH(13) /        127 /
!CC     DATA IMACH(14) /         54 /
!CC     DATA IMACH(15) /       -101 /
!CC     DATA IMACH(16) /        127 /
!CC
!CC     MACHINE CONSTANTS FOR THE PDP-10 (KI PROCESSOR)
!CC
!CC     DATA IMACH( 1) /          5 /
!CC     DATA IMACH( 2) /          6 /
!CC     DATA IMACH( 3) /          5 /
!CC     DATA IMACH( 4) /          6 /
!CC     DATA IMACH( 5) /         36 /
!CC     DATA IMACH( 6) /          5 /
!CC     DATA IMACH( 7) /          2 /
!CC     DATA IMACH( 8) /         35 /
!CC     DATA IMACH( 9) / "377777777777 /
!CC     DATA IMACH(10) /          2 /
!CC     DATA IMACH(11) /         27 /
!CC     DATA IMACH(12) /       -128 /
!CC     DATA IMACH(13) /        127 /
!CC     DATA IMACH(14) /         62 /
!CC     DATA IMACH(15) /       -128 /
!CC     DATA IMACH(16) /        127 /
!CC
!CC     MACHINE CONSTANTS FOR PDP-11 FORTRAN SUPPORTING
!CC     32-BIT INTEGER ARITHMETIC.
!CC
!CC     DATA IMACH( 1) /          5 /
!CC     DATA IMACH( 2) /          6 /
!CC     DATA IMACH( 3) /          5 /
!CC     DATA IMACH( 4) /          6 /
!CC     DATA IMACH( 5) /         32 /
!CC     DATA IMACH( 6) /          4 /
!CC     DATA IMACH( 7) /          2 /
!CC     DATA IMACH( 8) /         31 /
!CC     DATA IMACH( 9) / 2147483647 /
!CC     DATA IMACH(10) /          2 /
!CC     DATA IMACH(11) /         24 /
!CC     DATA IMACH(12) /       -127 /
!CC     DATA IMACH(13) /        127 /
!CC     DATA IMACH(14) /         56 /
!CC     DATA IMACH(15) /       -127 /
!CC     DATA IMACH(16) /        127 /
!CC
!CC     MACHINE CONSTANTS FOR PDP-11 FORTRAN SUPPORTING
!CC     16-BIT INTEGER ARITHMETIC.
!CC
!CC     DATA IMACH( 1) /          5 /
!CC     DATA IMACH( 2) /          6 /
!CC     DATA IMACH( 3) /          5 /
!CC     DATA IMACH( 4) /          6 /
!CC     DATA IMACH( 5) /         16 /
!CC     DATA IMACH( 6) /          2 /
!CC     DATA IMACH( 7) /          2 /
!CC     DATA IMACH( 8) /         15 /
!CC     DATA IMACH( 9) /      32767 /
!CC     DATA IMACH(10) /          2 /
!CC     DATA IMACH(11) /         24 /
!CC     DATA IMACH(12) /       -127 /
!CC     DATA IMACH(13) /        127 /
!CC     DATA IMACH(14) /         56 /
!CC     DATA IMACH(15) /       -127 /
!CC     DATA IMACH(16) /        127 /
!CC
!CC     MACHINE CONSTANTS FOR THE SILICON GRAPHICS
!CC
!CC     DATA IMACH( 1) /          5 /
!CC     DATA IMACH( 2) /          6 /
!CC     DATA IMACH( 3) /          6 /
!CC     DATA IMACH( 4) /          6 /
!CC     DATA IMACH( 5) /         32 /
!CC     DATA IMACH( 6) /          4 /
!CC     DATA IMACH( 7) /          2 /
!CC     DATA IMACH( 8) /         31 /
!CC     DATA IMACH( 9) / 2147483647 /
!CC     DATA IMACH(10) /          2 /
!CC     DATA IMACH(11) /         24 /
!CC     DATA IMACH(12) /       -125 /
!CC     DATA IMACH(13) /        128 /
!CC     DATA IMACH(14) /         53 /
!CC     DATA IMACH(15) /      -1021 /
!CC     DATA IMACH(16) /       1024 /
!CC
!CC     MACHINE CONSTANTS FOR THE SUN
!CC
!CC     DATA IMACH( 1) /          5 /
!CC     DATA IMACH( 2) /          6 /
!CC     DATA IMACH( 3) /          6 /
!CC     DATA IMACH( 4) /          6 /
!CC     DATA IMACH( 5) /         32 /
!CC     DATA IMACH( 6) /          4 /
!CC     DATA IMACH( 7) /          2 /
!CC     DATA IMACH( 8) /         31 /
!CC     DATA IMACH( 9) / 2147483647 /
!CC     DATA IMACH(10) /          2 /
!CC     DATA IMACH(11) /         24 /
!CC     DATA IMACH(12) /       -125 /
!CC     DATA IMACH(13) /        128 /
!CC     DATA IMACH(14) /         53 /
!CC     DATA IMACH(15) /      -1021 /
!CC     DATA IMACH(16) /       1024 /
!CC
!CC     MACHINE CONSTANTS FOR THE SUN
!CC     USING THE -r8 COMPILER OPTION
!CC
!CC     DATA IMACH( 1) /          5 /
!CC     DATA IMACH( 2) /          6 /
!CC     DATA IMACH( 3) /          6 /
!CC     DATA IMACH( 4) /          6 /
!CC     DATA IMACH( 5) /         32 /
!CC     DATA IMACH( 6) /          4 /
!CC     DATA IMACH( 7) /          2 /
!CC     DATA IMACH( 8) /         31 /
!CC     DATA IMACH( 9) / 2147483647 /
!CC     DATA IMACH(10) /          2 /
!CC     DATA IMACH(11) /         53 /
!CC     DATA IMACH(12) /      -1021 /
!CC     DATA IMACH(13) /       1024 /
!CC     DATA IMACH(14) /        113 /
!CC     DATA IMACH(15) /     -16381 /
!CC     DATA IMACH(16) /      16384 /
!CC
!CC     MACHINE CONSTANTS FOR THE UNIVAC 1100 SERIES FTN COMPILER
!CC
!CC     DATA IMACH( 1) /          5 /
!CC     DATA IMACH( 2) /          6 /
!CC     DATA IMACH( 3) /          1 /
!CC     DATA IMACH( 4) /          6 /
!CC     DATA IMACH( 5) /         36 /
!CC     DATA IMACH( 6) /          4 /
!CC     DATA IMACH( 7) /          2 /
!CC     DATA IMACH( 8) /         35 /
!CC     DATA IMACH( 9) / O377777777777 /
!CC     DATA IMACH(10) /          2 /
!CC     DATA IMACH(11) /         27 /
!CC     DATA IMACH(12) /       -128 /
!CC     DATA IMACH(13) /        127 /
!CC     DATA IMACH(14) /         60 /
!CC     DATA IMACH(15) /      -1024 /
!CC     DATA IMACH(16) /       1023 /
!CC
!CC     MACHINE CONSTANTS FOR THE Z80 MICROPROCESSOR
!CC
!CC     DATA IMACH( 1) /          1 /
!CC     DATA IMACH( 2) /          1 /
!CC     DATA IMACH( 3) /          0 /
!CC     DATA IMACH( 4) /          1 /
!CC     DATA IMACH( 5) /         16 /
!CC     DATA IMACH( 6) /          2 /
!CC     DATA IMACH( 7) /          2 /
!CC     DATA IMACH( 8) /         15 /
!CC     DATA IMACH( 9) /      32767 /
!CC     DATA IMACH(10) /          2 /
!CC     DATA IMACH(11) /         24 /
!CC     DATA IMACH(12) /       -127 /
!CC     DATA IMACH(13) /        127 /
!CC     DATA IMACH(14) /         56 /
!CC     DATA IMACH(15) /       -127 /
!CC     DATA IMACH(16) /        127 /
!CC
!CC***FIRST EXECUTABLE STATEMENT  I1MACH
!C      IF (I .LT. 1  .OR.  I .GT. 16) GO TO 10
!CC
!C      I1MACH = IMACH(I)
!C      RETURN
!CC
!C   10 CONTINUE
!C      WRITE (UNIT = OUTPUT, FMT = 9000)
!C 9000 FORMAT ('1ERROR    1 IN I1MACH - I OUT OF BOUNDS')
!CC
!CC     CALL FDUMP
!CC
!C      STOP
!C      END
!C*DECK PIMACH
!C      FUNCTION PIMACH (DUM)
!CC***BEGIN PROLOGUE  PIMACH
!CC***SUBSIDIARY
!CC***PURPOSE  Subsidiary to HSTCSP, HSTSSP and HWSCSP
!CC***LIBRARY   SLATEC
!CC***TYPE      SINGLE PRECISION (PIMACH-S)
!CC***AUTHOR  (UNKNOWN)
!CC***DESCRIPTION
!CC
!CC     This subprogram supplies the value of the constant PI correct to
!CC     machine precision where
!CC
!CC     PI=3.141592653589793238462643383279502884197169399375105820974944 !6
!CC
!CC***SEE ALSO  HSTCSP, HSTSSP, HWSCSP
!CC***ROUTINES CALLED  (NONE)
!CC***REVISION HISTORY  (YYMMDD)
!CC   801001  DATE WRITTEN
!CC   891214  Prologue converted to Version 4.0 format.  (BAB)
!CC   900402  Added TYPE section.  (WRB)
!CC***END PROLOGUE  PIMACH
!CC
!CC***FIRST EXECUTABLE STATEMENT  PIMACH
!CCCC   PIMACH = 3.14159265358979
!C
!C      REAL*8 PIMACH
!C      PIMACH = 3.1415926535897932D0
!C
!C      RETURN
!C      END
!C*DECK R1MACH
!C      REAL FUNCTION R1MACH (I)
!CC***BEGIN PROLOGUE  R1MACH
!CC***PURPOSE  Return floating point machine dependent constants.
!CC***LIBRARY   SLATEC
!CC***CATEGORY  R1
!CC***TYPE      SINGLE PRECISION (R1MACH-S, D1MACH-D)
!CC***KEYWORDS  MACHINE CONSTANTS
!CC***AUTHOR  Fox, P. A., (Bell Labs)
!CC           Hall, A. D., (Bell Labs)
!CC           Schryer, N. L., (Bell Labs)
!CC***DESCRIPTION
!CC
!CC   R1MACH can be used to obtain machine-dependent parameters for the
!CC   local machine environment.  It is a function subprogram with one
!CC   (input) argument, and can be referenced as follows:
!CC
!CC        A = R1MACH(I)
!CC
!CC   where I=1,...,5.  The (output) value of A above is determined by
!CC   the (input) value of I.  The results for various values of I are
!CC   discussed below.
!CC
!CC   R1MACH(1) = B**(EMIN-1), the smallest positive magnitude.
!CC   R1MACH(2) = B**EMAX*(1 - B**(-T)), the largest magnitude.
!CC   R1MACH(3) = B**(-T), the smallest relative spacing.
!CC   R1MACH(4) = B**(1-T), the largest relative spacing.
!CC   R1MACH(5) = LOG10(B)
!CC
!CC   Assume single precision numbers are represented in the T-digit,
!CC   base-B form
!CC
!CC              sign (B**E)*( (X(1)/B) + ... + (X(T)/B**T) )
!CC
!CC   where 0 .LE. X(I) .LT. B for I=1,...,T, 0 .LT. X(1), and
!CC   EMIN .LE. E .LE. EMAX.
!CC
!CC   The values of B, T, EMIN and EMAX are provided in I1MACH as
!CC   follows:
!CC   I1MACH(10) = B, the base.
!CC   I1MACH(11) = T, the number of base-B digits.
!CC   I1MACH(12) = EMIN, the smallest exponent E.
!CC   I1MACH(13) = EMAX, the largest exponent E.
!CC
!CC   To alter this function for a particular environment, the desired
!CC   set of DATA statements should be activated by removing the C from
!CC   column 1.  Also, the values of R1MACH(1) - R1MACH(4) should be
!CC   checked for consistency with the local operating system.
!CC
!CC***REFERENCES  P. A. Fox, A. D. Hall and N. L. Schryer, Framework for
!CC                 a portable library, ACM Transactions on Mathematical
!CC                 Software 4, 2 (June 1978), pp. 177-188.
!CC***ROUTINES CALLED  XERMSG
!CC***REVISION HISTORY  (YYMMDD)
!CC   790101  DATE WRITTEN
!CC   890213  REVISION DATE from Version 3.2
!CC   891214  Prologue converted to Version 4.0 format.  (BAB)
!CC   900315  CALLs to XERROR changed to CALLs to XERMSG.  (THJ)
!CC   900618  Added DEC RISC constants.  (WRB)
!CC   900723  Added IBM RS 6000 constants.  (WRB)
!CC   910710  Added HP 730 constants.  (SMR)
!CC   911114  Added Convex IEEE constants.  (WRB)
!CC   920121  Added SUN -r8 compiler option constants.  (WRB)
!CC   920229  Added Touchstone Delta i860 constants.  (WRB)
!CC   920501  Reformatted the REFERENCES section.  (WRB)
!CC   920625  Added CONVEX -p8 and -pd8 compiler option constants.
!CC           (BKS, WRB)
!CC   930201  Added DEC Alpha and SGI constants.  (RWC and WRB)
!CC***END PROLOGUE  R1MACH
!CC
!C      INTEGER SMALL(2)
!C      INTEGER LARGE(2)
!C      INTEGER RIGHT(2)
!C      INTEGER DIVER(2)
!C      INTEGER LOG10(2)
!CC
!C      REAL RMACH(5)
!C      SAVE RMACH
!CC
!C      EQUIVALENCE (RMACH(1),SMALL(1))
!C      EQUIVALENCE (RMACH(2),LARGE(1))
!C      EQUIVALENCE (RMACH(3),RIGHT(1))
!C      EQUIVALENCE (RMACH(4),DIVER(1))
!C      EQUIVALENCE (RMACH(5),LOG10(1))
!CC
!CC     MACHINE CONSTANTS FOR THE AMIGA
!CC     ABSOFT FORTRAN COMPILER USING THE 68020/68881 COMPILER OPTION
!CC
!CC     DATA SMALL(1) / Z'00800000' /
!CC     DATA LARGE(1) / Z'7F7FFFFF' /
!CC     DATA RIGHT(1) / Z'33800000' /
!CC     DATA DIVER(1) / Z'34000000' /
!CC     DATA LOG10(1) / Z'3E9A209B' /
!CC
!CC     MACHINE CONSTANTS FOR THE AMIGA
!CC     ABSOFT FORTRAN COMPILER USING SOFTWARE FLOATING POINT
!CC
!CC     DATA SMALL(1) / Z'00800000' /
!CC     DATA LARGE(1) / Z'7EFFFFFF' /
!CC     DATA RIGHT(1) / Z'33800000' /
!CC     DATA DIVER(1) / Z'34000000' /
!CC     DATA LOG10(1) / Z'3E9A209B' /
!CC
!CC     MACHINE CONSTANTS FOR THE APOLLO
!CC
!CC     DATA SMALL(1) / 16#00800000 /
!CC     DATA LARGE(1) / 16#7FFFFFFF /
!CC     DATA RIGHT(1) / 16#33800000 /
!CC     DATA DIVER(1) / 16#34000000 /
!CC     DATA LOG10(1) / 16#3E9A209B /
!CC
!CC     MACHINE CONSTANTS FOR THE BURROUGHS 1700 SYSTEM
!CC
!CC     DATA RMACH(1) / Z400800000 /
!CC     DATA RMACH(2) / Z5FFFFFFFF /
!CC     DATA RMACH(3) / Z4E9800000 /
!CC     DATA RMACH(4) / Z4EA800000 /
!CC     DATA RMACH(5) / Z500E730E8 /
!CC
!CC     MACHINE CONSTANTS FOR THE BURROUGHS 5700/6700/7700 SYSTEMS
!CC
!CC     DATA RMACH(1) / O1771000000000000 /
!CC     DATA RMACH(2) / O0777777777777777 /
!CC     DATA RMACH(3) / O1311000000000000 /
!CC     DATA RMACH(4) / O1301000000000000 /
!CC     DATA RMACH(5) / O1157163034761675 /
!CC
!CC     MACHINE CONSTANTS FOR THE CDC 170/180 SERIES USING NOS/VE
!CC
!CC     DATA RMACH(1) / Z"3001800000000000" /
!CC     DATA RMACH(2) / Z"4FFEFFFFFFFFFFFE" /
!CC     DATA RMACH(3) / Z"3FD2800000000000" /
!CC     DATA RMACH(4) / Z"3FD3800000000000" /
!CC     DATA RMACH(5) / Z"3FFF9A209A84FBCF" /
!CC
!CC     MACHINE CONSTANTS FOR THE CDC 6000/7000 SERIES
!CC
!CC     DATA RMACH(1) / 00564000000000000000B /
!CC     DATA RMACH(2) / 37767777777777777776B /
!CC     DATA RMACH(3) / 16414000000000000000B /
!CC     DATA RMACH(4) / 16424000000000000000B /
!CC     DATA RMACH(5) / 17164642023241175720B /
!CC
!CC     MACHINE CONSTANTS FOR THE CELERITY C1260
!CC
!CC     DATA SMALL(1) / Z'00800000' /
!CC     DATA LARGE(1) / Z'7F7FFFFF' /
!CC     DATA RIGHT(1) / Z'33800000' /
!CC     DATA DIVER(1) / Z'34000000' /
!CC     DATA LOG10(1) / Z'3E9A209B' /
!CC
!CC     MACHINE CONSTANTS FOR THE CONVEX
!CC     USING THE -fn COMPILER OPTION
!CC
!CC     DATA RMACH(1) / Z'00800000' /
!CC     DATA RMACH(2) / Z'7FFFFFFF' /
!CC     DATA RMACH(3) / Z'34800000' /
!CC     DATA RMACH(4) / Z'35000000' /
!CC     DATA RMACH(5) / Z'3F9A209B' /
!CC
!CC     MACHINE CONSTANTS FOR THE CONVEX
!CC     USING THE -fi COMPILER OPTION
!CC
!CC     DATA RMACH(1) / Z'00800000' /
!CC     DATA RMACH(2) / Z'7F7FFFFF' /
!CC     DATA RMACH(3) / Z'33800000' /
!CC     DATA RMACH(4) / Z'34000000' /
!CC     DATA RMACH(5) / Z'3E9A209B' /
!CC
!CC     MACHINE CONSTANTS FOR THE CONVEX
!CC     USING THE -p8 OR -pd8 COMPILER OPTION
!CC
!CC     DATA RMACH(1) / Z'0010000000000000' /
!CC     DATA RMACH(2) / Z'7FFFFFFFFFFFFFFF' /
!CC     DATA RMACH(3) / Z'3CC0000000000000' /
!CC     DATA RMACH(4) / Z'3CD0000000000000' /
!CC     DATA RMACH(5) / Z'3FF34413509F79FF' /
!CC
!CC     MACHINE CONSTANTS FOR THE CRAY
!CC
!CC     DATA RMACH(1) / 200034000000000000000B /
!CC     DATA RMACH(2) / 577767777777777777776B /
!CC     DATA RMACH(3) / 377224000000000000000B /
!CC     DATA RMACH(4) / 377234000000000000000B /
!CC     DATA RMACH(5) / 377774642023241175720B /
!CC
!CC     MACHINE CONSTANTS FOR THE DATA GENERAL ECLIPSE S/200
!CC     NOTE - IT MAY BE APPROPRIATE TO INCLUDE THE FOLLOWING CARD -
!CC     STATIC RMACH(5)
!CC
!CC     DATA SMALL /    20K,       0 /
!CC     DATA LARGE / 77777K, 177777K /
!CC     DATA RIGHT / 35420K,       0 /
!CC     DATA DIVER / 36020K,       0 /
!CC     DATA LOG10 / 40423K,  42023K /
!CC
!CC     MACHINE CONSTANTS FOR THE DEC ALPHA
!CC     USING G_FLOAT
!CC
!CC     DATA RMACH(1) / '00000080'X /
!CC     DATA RMACH(2) / 'FFFF7FFF'X /
!CC     DATA RMACH(3) / '00003480'X /
!CC     DATA RMACH(4) / '00003500'X /
!CC     DATA RMACH(5) / '209B3F9A'X /
!CC
!CC     MACHINE CONSTANTS FOR THE DEC ALPHA
!CC     USING IEEE_FLOAT
!CC
!CC     DATA RMACH(1) / '00800000'X /
!CC     DATA RMACH(2) / '7F7FFFFF'X /
!CC     DATA RMACH(3) / '33800000'X /
!CC     DATA RMACH(4) / '34000000'X /
!CC     DATA RMACH(5) / '3E9A209B'X /
!CC
!CC     MACHINE CONSTANTS FOR THE DEC RISC
!CC
!CC     DATA RMACH(1) / Z'00800000' /
!CC     DATA RMACH(2) / Z'7F7FFFFF' /
!CC     DATA RMACH(3) / Z'33800000' /
!CC     DATA RMACH(4) / Z'34000000' /
!CC     DATA RMACH(5) / Z'3E9A209B' /
!CC
!CC     MACHINE CONSTANTS FOR THE DEC VAX
!CC     (EXPRESSED IN INTEGER AND HEXADECIMAL)
!CC     THE HEX FORMAT BELOW MAY NOT BE SUITABLE FOR UNIX SYSTEMS
!CC     THE INTEGER FORMAT SHOULD BE OK FOR UNIX SYSTEMS
!CC
!CC     DATA SMALL(1) /       128 /
!CC     DATA LARGE(1) /    -32769 /
!CC     DATA RIGHT(1) /     13440 /
!CC     DATA DIVER(1) /     13568 /
!CC     DATA LOG10(1) / 547045274 /
!CC
!CC     DATA SMALL(1) / Z00000080 /
!CC     DATA LARGE(1) / ZFFFF7FFF /
!CC     DATA RIGHT(1) / Z00003480 /
!CC     DATA DIVER(1) / Z00003500 /
!CC     DATA LOG10(1) / Z209B3F9A /
!CC
!CC     MACHINE CONSTANTS FOR THE ELXSI 6400
!CC     (ASSUMING REAL*4 IS THE DEFAULT REAL)
!CC
!CC     DATA SMALL(1) / '00800000'X /
!CC     DATA LARGE(1) / '7F7FFFFF'X /
!CC     DATA RIGHT(1) / '33800000'X /
!CC     DATA DIVER(1) / '34000000'X /
!CC     DATA LOG10(1) / '3E9A209B'X /
!CC
!CC     MACHINE CONSTANTS FOR THE HARRIS 220
!CC
!CC     DATA SMALL(1), SMALL(2) / '20000000, '00000201 /
!CC     DATA LARGE(1), LARGE(2) / '37777777, '00000177 /
!CC     DATA RIGHT(1), RIGHT(2) / '20000000, '00000352 /
!CC     DATA DIVER(1), DIVER(2) / '20000000, '00000353 /
!CC     DATA LOG10(1), LOG10(2) / '23210115, '00000377 /
!CC
!CC     MACHINE CONSTANTS FOR THE HONEYWELL 600/6000 SERIES
!CC
!CC     DATA RMACH(1) / O402400000000 /
!CC     DATA RMACH(2) / O376777777777 /
!CC     DATA RMACH(3) / O714400000000 /
!CC     DATA RMACH(4) / O716400000000 /
!CC     DATA RMACH(5) / O776464202324 /
!CC
!CC     MACHINE CONSTANTS FOR THE HP 730
!CC
!CC     DATA RMACH(1) / Z'00800000' /
!CC     DATA RMACH(2) / Z'7F7FFFFF' /
!CC     DATA RMACH(3) / Z'33800000' /
!CC     DATA RMACH(4) / Z'34000000' /
!CC     DATA RMACH(5) / Z'3E9A209B' /
!CC
!CC     MACHINE CONSTANTS FOR THE HP 2100
!CC     3 WORD DOUBLE PRECISION WITH FTN4
!CC
!CC     DATA SMALL(1), SMALL(2) / 40000B,       1 /
!CC     DATA LARGE(1), LARGE(2) / 77777B, 177776B /
!CC     DATA RIGHT(1), RIGHT(2) / 40000B,    325B /
!CC     DATA DIVER(1), DIVER(2) / 40000B,    327B /
!CC     DATA LOG10(1), LOG10(2) / 46420B,  46777B /
!CC
!CC     MACHINE CONSTANTS FOR THE HP 2100
!CC     4 WORD DOUBLE PRECISION WITH FTN4
!CC
!CC     DATA SMALL(1), SMALL(2) / 40000B,       1 /
!CC     DATA LARGE(1), LARGE(2) / 77777B, 177776B /
!CC     DATA RIGHT(1), RIGHT(2) / 40000B,    325B /
!CC     DATA DIVER(1), DIVER(2) / 40000B,    327B /
!CC     DATA LOG10(1), LOG10(2) / 46420B,  46777B /
!CC
!CC     MACHINE CONSTANTS FOR THE HP 9000
!CC
!CC     DATA SMALL(1) / 00004000000B /
!CC     DATA LARGE(1) / 17677777777B /
!CC     DATA RIGHT(1) / 06340000000B /
!CC     DATA DIVER(1) / 06400000000B /
!CC     DATA LOG10(1) / 07646420233B /
!CC
!CC     MACHINE CONSTANTS FOR THE IBM 360/370 SERIES,
!CC     THE XEROX SIGMA 5/7/9, THE SEL SYSTEMS 85/86  AND
!CC     THE PERKIN ELMER (INTERDATA) 7/32.
!CC
!CC     DATA RMACH(1) / Z00100000 /
!CC     DATA RMACH(2) / Z7FFFFFFF /
!CC     DATA RMACH(3) / Z3B100000 /
!CC     DATA RMACH(4) / Z3C100000 /
!CC     DATA RMACH(5) / Z41134413 /
!CC
!CC     MACHINE CONSTANTS FOR THE IBM PC
!CC
!CC     DATA SMALL(1) / 1.18E-38      /
!CC     DATA LARGE(1) / 3.40E+38      /
!CC     DATA RIGHT(1) / 0.595E-07     /
!CC     DATA DIVER(1) / 1.19E-07      /
!CC     DATA LOG10(1) / 0.30102999566 /
!CC
!CC     MACHINE CONSTANTS FOR THE IBM RS 6000
!CC
!CC     DATA RMACH(1) / Z'00800000' /
!CC     DATA RMACH(2) / Z'7F7FFFFF' /
!CC     DATA RMACH(3) / Z'33800000' /
!CC     DATA RMACH(4) / Z'34000000' /
!CC     DATA RMACH(5) / Z'3E9A209B' /
!CC
!CC     MACHINE CONSTANTS FOR THE INTEL i860
!CC
!C      DATA RMACH(1) / Z'00800000' /
!C      DATA RMACH(2) / Z'7F7FFFFF' /
!C      DATA RMACH(3) / Z'33800000' /
!C      DATA RMACH(4) / Z'34000000' /
!C      DATA RMACH(5) / Z'3E9A209B' /
!CC
!CC     MACHINE CONSTANTS FOR THE PDP-10 (KA OR KI PROCESSOR)
!CC
!CC     DATA RMACH(1) / "000400000000 /
!CC     DATA RMACH(2) / "377777777777 /
!CC     DATA RMACH(3) / "146400000000 /
!CC     DATA RMACH(4) / "147400000000 /
!CC     DATA RMACH(5) / "177464202324 /
!CC
!CC     MACHINE CONSTANTS FOR PDP-11 FORTRAN SUPPORTING
!CC     32-BIT INTEGERS (EXPRESSED IN INTEGER AND OCTAL).
!CC
!CC     DATA SMALL(1) /    8388608 /
!CC     DATA LARGE(1) / 2147483647 /
!CC     DATA RIGHT(1) /  880803840 /
!CC     DATA DIVER(1) /  889192448 /
!CC     DATA LOG10(1) / 1067065499 /
!CC
!CC     DATA RMACH(1) / O00040000000 /
!CC     DATA RMACH(2) / O17777777777 /
!CC     DATA RMACH(3) / O06440000000 /
!CC     DATA RMACH(4) / O06500000000 /
!CC     DATA RMACH(5) / O07746420233 /
!CC
!CC     MACHINE CONSTANTS FOR PDP-11 FORTRAN SUPPORTING
!CC     16-BIT INTEGERS  (EXPRESSED IN INTEGER AND OCTAL).
!CC
!CC     DATA SMALL(1), SMALL(2) /   128,     0 /
!CC     DATA LARGE(1), LARGE(2) / 32767,    -1 /
!CC     DATA RIGHT(1), RIGHT(2) / 13440,     0 /
!CC     DATA DIVER(1), DIVER(2) / 13568,     0 /
!CC     DATA LOG10(1), LOG10(2) / 16282,  8347 /
!CC
!CC     DATA SMALL(1), SMALL(2) / O000200, O000000 /
!CC     DATA LARGE(1), LARGE(2) / O077777, O177777 /
!CC     DATA RIGHT(1), RIGHT(2) / O032200, O000000 /
!CC     DATA DIVER(1), DIVER(2) / O032400, O000000 /
!CC     DATA LOG10(1), LOG10(2) / O037632, O020233 /
!CC
!CC     MACHINE CONSTANTS FOR THE SILICON GRAPHICS
!CC
!CC     DATA RMACH(1) / Z'00800000' /
!CC     DATA RMACH(2) / Z'7F7FFFFF' /
!CC     DATA RMACH(3) / Z'33800000' /
!CC     DATA RMACH(4) / Z'34000000' /
!CC     DATA RMACH(5) / Z'3E9A209B' /
!CC
!CC     MACHINE CONSTANTS FOR THE SUN
!CC
!CC     DATA RMACH(1) / Z'00800000' /
!CC     DATA RMACH(2) / Z'7F7FFFFF' /
!CC     DATA RMACH(3) / Z'33800000' /
!CC     DATA RMACH(4) / Z'34000000' /
!CC     DATA RMACH(5) / Z'3E9A209B' /
!CC
!CC     MACHINE CONSTANTS FOR THE SUN
!CC     USING THE -r8 COMPILER OPTION
!CC
!CC     DATA RMACH(1) / Z'0010000000000000' /
!CC     DATA RMACH(2) / Z'7FEFFFFFFFFFFFFF' /
!CC     DATA RMACH(3) / Z'3CA0000000000000' /
!CC     DATA RMACH(4) / Z'3CB0000000000000' /
!CC     DATA RMACH(5) / Z'3FD34413509F79FF' /
!CC
!CC     MACHINE CONSTANTS FOR THE UNIVAC 1100 SERIES
!CC
!CC     DATA RMACH(1) / O000400000000 /
!CC     DATA RMACH(2) / O377777777777 /
!CC     DATA RMACH(3) / O146400000000 /
!CC     DATA RMACH(4) / O147400000000 /
!CC     DATA RMACH(5) / O177464202324 /
!CC
!CC     MACHINE CONSTANTS FOR THE Z80 MICROPROCESSOR
!CC
!CC     DATA SMALL(1), SMALL(2) /     0,    256/
!CC     DATA LARGE(1), LARGE(2) /    -1,   -129/
!CC     DATA RIGHT(1), RIGHT(2) /     0,  26880/
!CC     DATA DIVER(1), DIVER(2) /     0,  27136/
!CC     DATA LOG10(1), LOG10(2) /  8347,  32538/
!CC
!CC***FIRST EXECUTABLE STATEMENT  R1MACH
!CC     IF (I .LT. 1 .OR. I .GT. 5) CALL XERMSG ('SLATEC', 'R1MACH',
!CC    +   'I OUT OF BOUNDS', 1, 2)
!C      IF (I .LT. 1 .OR. I .GT. 5) STOP ' R1MACH ERROR'
!CC
!C      R1MACH = RMACH(I)
!C      RETURN
!CC
!C      END
