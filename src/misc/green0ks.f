!c     ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
!BOP
!!ROUTINE: green0ks
!!INTERFACE:
      subroutine green0ks(lmax,kkrsz,natom,                             &
     &                  aij,                                            &
     &                  mapstr,mappnt,ndimbas,mapij,                    &
     &                  powe,                                           &
     &                  ij3,nj3,cgaunt,cfac,                            &
     &                  kx,ky,kz,                                       &
     &                  edu,pdu,                                        &
     &                  irecdlm,                                        &
     &                  rsnij,ndimrij,                                  &
     &                  np2r,ndimnp,numbrs,naij,                        &
     &                  dqint,ndimdqr,ndx,                              &
     &                  hplnm,ndimrhp,ndimlhp,                          &
     &                  d00,eoeta,                                      &
     &                  eta,xknlat,ndimks,                              &
     &                  conr,nkns,                                      &
     &                  icut,                                           &
     &                  mapsprs,                                        &
     &                  greenks,                                        &
     &                  invswitch,                                      &
     &                  iprint,istop)
!!DESCRIPTION:
! calculates Green's function, {\tt greenks}, in K-space, 
! order of $ kkrszr*natom $
!
!!USES:
      use mecca_constants
      use mtrx_interface
!
!!REVISION HISTORY:
! Adapted - A.S. - 2013
!!REMARKS:
!  DISABLED: icut = 1  --  to apply artificial "screening"
!EOP
!
!BOC
!DEBUG      use raymethod
      implicit real*8 (a-h,o-z)
!c
!c     ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!      include 'lmax.h'
!      include 'mkkrcpa.h'
!c     ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
      character    sname*10
!c
!c     ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
      parameter    (sname='green0ks')
!c     ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!c
      character istop*10
!c
      integer     ij3(kkrsz*kkrsz,kkrsz)
      integer     nj3(kkrsz*kkrsz)
      integer     ndx(*)
      integer     nkns
!c
      real*8      aij(3,naij)
      real*8      kx
      real*8      ky
      real*8      kz
      real(8), allocatable :: rsn(:,:)  ! (iprsn,3)
      real*8      rsnij(ndimrij,4)
      real*8      cgaunt(kkrsz*kkrsz,*)
      real*8      xknlat(ndimks,3)
      real*8      dotka
      real*8      eta
!c
      real*8      pi4
      parameter (pi4 = 4.d0*pi)
!c
      complex*16   conr(*)

      complex*16   dlm((2*lmax+1)**2)

      complex*16   powe(*)
      complex*16   dqint(ndimdqr,*)
      complex(8), allocatable :: dqint1(:,:) ! (iprsn,2*lmax+1)
      complex*16   cfac(kkrsz*kkrsz)
      complex*16   g(kkrsz*kkrsz)
      complex*16   greenks(*)
      complex*16   edu
      complex*16   pdu
      complex*16   d00
      complex*16   eoeta
      complex*16   expdot
      real(8) :: expdot_(2)
      equivalence (expdot,expdot_)


      integer icut
      integer mapsprs(*)


!      parameter    (iplhp=(2*lmax+1)*(iplmax+1))
      complex(8), allocatable :: hplnm2(:,:)  !  (iprsn,(2*lmax+1)**2)  ! (iprsn,iplhp)

      complex*16   hplnm(ndimrhp,ndimlhp)

!CALAM      integer      itype(natom)
      integer      mapstr(ndimbas,natom),mappnt(ndimbas,natom)

      integer mapij(2,naij)
      integer np2r(ndimnp,*),numbrs(*),naij

      complex*16 sqrtm1
      parameter (sqrtm1=(0.d0,1.d0))
!CAB
!CAB      complex*16   tmpB(ipkkr,ipkkr)
!CAB      equivalence  (tmpB(1,1),g(1))
!CAB

!c
!c     ******************************************************************
!c     full matrix storage version.........bg & gms [Nov 1991]...........
!c     improved ewald & real-space algorithms .... ddj & was April 1994
!c           (factor of 6 speed-up per energy and reduced storage)
!c     found origin of symmetry violation in d.o.s., esp. w/ real-space
!c     hankel fct converges poorly in complex plane for system w/ basis
!c     (next step: reduce tau storage by using group theory (KTOP storage !)
!c     ....ddj & was April 1994
!c     ******************************************************************


!  pdu -- has to be NON-relativistic for single-scattering (?)

!  dqint         : either a hankel fct in real-space calculation
!                  or a real-space Ewald integral from INTFAC
!c     ==================================================================

       if ( invswitch.eq.2 ) then
        call fstop(sname//' invswitch=2 - not implemented')
       end if

       allocate(hplnm2(maxval(numbrs(1:naij)),(2*lmax+1)**2))
       allocate(dqint1(maxval(numbrs(1:naij)),2*lmax+1))
       allocate(rsn(maxval(numbrs(1:naij)),3))
!
      call zerooutC(greenks,(kkrsz*natom)**2)
!c     =================================================================
!c     for each k-point set up Tau*(-1)=[t*(-1)-G(l,lp)] and invert.....

         ia = 1
         nrsnij = numbrs(ia)

!DEBUG         call begintiming("g0 // make r^l Ylm")
         call rlylmb(ia,hplnm,ndimrhp,                                  &
     &               nrsnij,np2r(1,ia),                                 &
     &               aij(1,ia),rsnij,ndimrij,rsn,size(rsn,1),           &
     &               edu,pdu,eta,irecdlm,                               &
     &               dqint,dqint1,ndimdqr,                              &
     &               lmax,hplnm2,size(hplnm2,1),ndimlhp)
!DEBUG         call endtiming()

         if(irecdlm.eq.0) then
!c           ===========================================================
!c           site diagonal dlm ewald calculation........................
!c           -----------------------------------------------------------
            call ewald(kx,ky,kz,                                        &
     &            edu,eoeta,powe,lmax,aij(1,ia),                        &
     &            dlm,eta,                                              &
     &            hplnm2,size(hplnm2,1),ndimlhp,                        &
     &            xknlat,ndimks,conr,                                   &
     &            rsn,nrsnij,size(rsn,1),                               &
     &            dqint1,size(dqint1,1),ndx,                            &
     &            nkns,d00,ia)
!c           -----------------------------------------------------------
         else
!c           dlm real space calculation.................................
!c           -----------------------------------------------------------
            call makedrs(kx,ky,kz,pdu,lmax,                             &
     &            rsn,nrsnij,size(rsn,1),                               &
     &            hplnm2,size(hplnm2,1),                                &
     &            dqint1,size(dqint1,1),                                &
     &            ia,dlm)
!c           -----------------------------------------------------------
         endif

!c        ---------------------------------------------------------------
         call zerooutC(g,kkrsz*kkrsz)
!c        ---------------------------------------------------------------
!c        ==============================================================
!c        calculate g(l,lp).............................................

!CAB
!CAB            write(98+irecdlm,*) ' DLM: ',edu
!CAB
!CAB            write(98+irecdlm,1002) (dlm(k),k,k=1,(2*lmax+1)**2)
!CAB1002        format((2f10.6,1x,i3))
!CAB


!DEBUG         call begintiming("g0 // sum(gaunt*dlm)")
         do i=1,kkrsz*kkrsz
            do k=1,nj3(i)
               g(i)=g(i)+cgaunt(i,k)*dlm(ij3(i,k))
            enddo
         enddo
!DEBUG         call endtiming()

!CAB
!CAB          do l1=1,kkrsz
!CAB           do l2=l1+1,kkrsz
!CAB             tmpB(l1,l2) = 0
!CAB             tmpB(l2,l1) = 0
!CAB           end do
!CAB          end do


!CAB            write(90+irecdlm,*) ' B ################',edu
!CAB            do l1=1,kkrsz
!CAB               if(imag(edu).ge.0) then
!CAB           write(90+irecdlm,1001) tmpB(l1,l1),tmpB(l1,l1),l1,l1
!CAB               else
!CAB           write(90+irecdlm,1001)
!CAB     *              conjg(tmpB(l1,l1)),conjg(tmpB(l1,l1)),l1,l1
!CAB               end if
!CAB             do l2=l1+1,kkrsz
!CAB              if(abs(tmpB(l1,l2)).gt.1.d-7) then
!CAB               if(imag(edu).ge.0) then
!CAB           write(90+irecdlm,1001) tmpB(l1,l2),tmpB(l2,l1),l1,l2
!CAB               else
!CAB           write(90+irecdlm,1001)
!CAB     *              conjg(tmpB(l2,l1)),conjg(tmpB(l1,l2)),l1,l2
!CAB               end if
!CAB              end if
!CAB            end do
!CAB            end do
!CAB

!c                  cfac(i|l1,l2) <==> (0.d0,1d0)**(l1-l2)

!DEBUG         call begintiming("g0 // post arithmetic")
         do i=1,kkrsz*kkrsz
            g(i) = pi4*cfac(i) * g(i)
         enddo
!DEBUG         call endtiming()

         if(iprint.ge.7) then
           write(6,'(1x,a)') sname//':  Bllp (diagonal)'
!c          call wrtmtx(g,kkrsz,istop)
           call wrtdia2(reshape(g(1:kkrsz*kkrsz),[kkrsz,kkrsz,1]),      &
     &                                                    kkrsz,1,istop)
         endif
!c

!DEBUG         call begintiming("g0 // post arithmetic")
         do i=1,kkrsz*kkrsz,kkrsz+1
            g(i) = g(i)+pdu*sqrtm1
         enddo
!DEBUG         call endtiming()

!c        ===============================================================
         if(iprint.ge.7) then
           write(6,'(1x,a)') sname//':  gllp (diagonal)'
!c          call wrtmtx(g,kkrsz,istop)
           call wrtdia2(reshape(g(1:kkrsz*kkrsz),[kkrsz,kkrsz,1]),      &
     &                                                    kkrsz,1,istop)
          endif
!c        ===============================================================
!c        set up G ......................................................

           expdot = one
!DEBUG           call begintiming("g0 // post arithmetic")
           do i=1,natom
!inv            if(invswitch.eq.2) then
!inv             call matrix1(g,greenks,i,i,kkrsz*natom,kkrsz,expdot_)
!inv            else
             call matrix(reshape(g(1:kkrsz*kkrsz),[kkrsz,kkrsz]),       &
     &                          greenks,i,i,kkrsz*natom,kkrsz,expdot)
!inv            end if
!c           ------------------------------------------------------------
           enddo
!DEBUG           call endtiming()

!c        For site off-diagonal part of Tau*(-1).........................

         do ia= 2,naij

!         call dumptimings()

         i1 = mapij(1,ia)
         i2 = mapij(2,ia)

         if(i1.ne.i2) then

          nrsnij = numbrs(ia)

!DEBUG          call begintiming("g0 // make r^l Ylm")
          call rlylmb(ia,hplnm,ndimrhp,                                 &
     &                nrsnij,np2r(1,ia),                                &
     &                aij(1,ia),rsnij,ndimrij,rsn,size(rsn,1),          &
     &                edu,pdu,eta,irecdlm,                              &
     &                dqint,dqint1,ndimdqr,                             &
     &                lmax,hplnm2,size(hplnm2,1),ndimlhp)
!DEBUG          call endtiming()


          if(irecdlm.eq.0) then
!c           ===========================================================
!c           site diagonal dlm ewald calculation........................
!c           -----------------------------------------------------------
            call ewald(kx,ky,kz,                                        &
     &            edu,eoeta,powe,lmax,aij(1,ia),                        &
     &            dlm,eta,                                              &
     &            hplnm2,size(hplnm2,1),ndimlhp,                        &
     &            xknlat,ndimks,conr,                                   &
     &            rsn,nrsnij,size(rsn,1),                               &
     &            dqint1,size(dqint1,1),ndx,                            &
     &            nkns,d00,ia)
!c           -----------------------------------------------------------
          else
!c           dlm real space calculation.................................
!c           -----------------------------------------------------------
            call makedrs(kx,ky,kz,pdu,lmax,                             &
     &            rsn,nrsnij,size(rsn,1),                               &
     &            hplnm2,size(hplnm2,1),                                &
     &            dqint1,size(dqint1,1),                                &
     &            ia,dlm)
!c           -----------------------------------------------------------
          endif


                  dotka = kx*aij(1,ia) +                                &
     &                  ky*aij(2,ia) +                                  &
     &                  kz*aij(3,ia)
                  expdot=exp(dcmplx(zero,dotka))

!c                 ------------------------------------------------------
                  call zerooutC(g,kkrsz*kkrsz)
!c                 ------------------------------------------------------
!c                 ======================================================
!c                 calculate g(l,lp).....................................
!DEBUG                  call begintiming("g0 // sum(gaunt*dlm)")
                  do i=1,kkrsz*kkrsz
                     do k=1,nj3(i)
                        g(i)=g(i)+cgaunt(i,k)*dlm(ij3(i,k))
                     enddo
                  enddo
!DEBUG                  call endtiming()

!DEBUG                  call begintiming("g0 // post arithmetic")
                  do i=1,kkrsz*kkrsz
                     g(i) = pi4*cfac(i) * g(i)
                  enddo
!DEBUG                  call endtiming()

!CAB
!CAB            if(i1.eq.1.or.i2.eq.1) then
!CAB
!CAB            if(imag(edu).ge.0) then
!CAB             if(i1.lt.i2) ichan = 70
!CAB             if(i1.gt.i2) ichan = 80
!CAB            else
!CAB             if(i1.lt.i2) ichan = 10
!CAB             if(i1.gt.i2) ichan = 50
!CAB            endif
!CAB            write(ichan+irecdlm,*) ' B ###',edu
!CAB            write(ichan+irecdlm,*) ' MATRIX::',i1,i2,kkrsz
!CAB            do l1=1,kkrsz
!CAB             do l2=l1+1,kkrsz
!CAB              if(abs(tmpB(l1,l2)).gt.1.d-6) then
!CAB               if(imag(edu).ge.0) then
!CAB           write(ichan+irecdlm,1001) tmpB(l1,l2),tmpB(l2,l1),l1,l2
!CAB1001           format(4f12.7,2i3)
!CAB               else
!CAB           write(ichan+irecdlm,1001)
!CAB     *              conjg(tmpB(l2,l1)),conjg(tmpB(l1,l2)),l1,l2
!CAB               end if
!CAB              end if
!CAB            end do
!CAB            end do
!CAB
!CAB           end if
!CAB

             if( iprint .ge. 7 ) then
           write(6,'(1x,a,2i3)')                                        &
     &                        sname//': gllp (off-diag)  i1,i2=',i1,i2
!c           call wrtmtx(g,kkrsz,istop)
           call wrtdia2(reshape(g(1:kkrsz*kkrsz),[kkrsz,kkrsz,1]),      &
     &                                                    kkrsz,1,istop)
             endif

!DEBUG             call begintiming("g0 // post arithmetic")
!inv             if(invswitch.eq.2) then
!inv             call matrix1(g,greenks,i1,i2,kkrsz*natom,kkrsz,expdot_)
!inv             else
             call matrix(reshape(g(1:kkrsz*kkrsz),[kkrsz,kkrsz]),       &
     &                          greenks,i1,i2,kkrsz*natom,kkrsz,expdot)
!inv             end if
!DEBUG             call endtiming()
         end if
         enddo

!DEBUG         call begintiming("g0 // copy operations")
         do i1= 1,natom
          do i2= 1,natom
           if(i1.ne.i2) then
!CAB            if(mappnt(i1,i2).eq.0) then
            if(mappnt(i1,i2).ne.1) then
             i0 = mapij(1,mapstr(i1,i2))
             j0 = mapij(2,mapstr(i1,i2))
             call cpmatrz(greenks,i1,i2,i0,j0,kkrsz*natom,kkrsz)
            end if
           endif
          enddo
         enddo
!DEBUG         call endtiming()


!      if(icut.eq.1) then
!       call dosparse(greenks,                                           &
!!CALAM     *                   ndimbas,
!     &                   natom,kkrsz                                    &
!     &                   ,mapsprs,1,invswitch                           &
!     &                   )
!      end if

       deallocate(rsn)
       deallocate(dqint1)
       deallocate(hplnm2)

!c     ==================================================================
      if (istop.eq.sname) call fstop(sname)
!c

      return
!EOC
      end subroutine green0ks

!      subroutine dosparse(g,                                            &
!!CALAM     *                   ndimbas,
!     &                   natom,kkrsz                                    &
!     &                   ,mapsprs,lsprs,invswitch                       &
!     &                   )
!!c     ================================================================
!      use mecca_constants
!      implicit real*8 (a-h,o-z)
!!c
!!c     input:  g=            -- full matrix
!!c
!!c     output: g= the same, if I, J -- neighbours or have common neighbours
!!c              = zero    , if not
!!c
!!      include 'lmax.h'
!      include 'inverse.h'
!      complex*16 g(*)
!!CALAM      integer    mapstr(ndimbas,natom),mappnt(ndimbas,natom)
!!CALAM      integer    mapij(2,naij)
!
!      integer mapsprs(natom,natom),lsprs,invswitch
!
!      nrmat = kkrsz*natom
!
!!      if(lkkrmax.lt.kkrsz) stop ' lkkrmax.lt.kkrsz '
!
!!c
!!c  Off diagonal (I,J)-block
!!c
!
!!CAB          lbnd=0+1
!!CABc            only orbital-diagonal elements (L=L'=0) are not equal to 0
!!CAB          lbnd=2+1
!!CABc            only orbital-diagonal elements (L=L'.LE.2) are not equal to 0
!
!          lbnd=0
!!c            all non-NN elements are equal to zero
!
!!CDEBUG
!      call sprslvlz(g,nrmat,tolCut)
!!CDEBUG
!
!      do i1= 1,natom
!        do i2= 1,natom
!         if(mapsprs(i1,i2).lt.lsprs) then
!          call diagmtr(lbnd,g,i1,i2,nrmat,kkrsz,invswitch)
!         end if
!       enddo
!      enddo
!
!!CDEBUG
!      call sprslvlz(g,nrmat,tolCut)
!!CDEBUG
!
!      return
!
!      CONTAINS
!!c======================================================================
!      subroutine sprslvlz(a,n,tolCut)
!      integer n
!      complex*16 a(n,n)
!      real*8 tolCut
!      integer i,j,m
!
!      m = 0
!      an2 = n*n
!      do i=1,n
!       do j=1,n
!        if(abs(a(i,j)).le.tolCut) m=m+1
!       end do
!      end do
!      write(6,*) '*** DOSPARSE: Percentage of zero elements is about ', &
!     &           m*100/an2,'%  N=',n
!      return
!      end subroutine sprslvlz
!!c======================================================================
!
!      end subroutine dosparse

