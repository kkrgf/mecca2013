!c     ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      subroutine qcheck1(zvalav,qval,xvalws,xval,                       &
     &                  zcorss,qcortot,                                 &
     &                  atcon,komp,numbsub,                             &
     &                  nbasis,nspin,                                   &
     &                  iprint,istop)
!c     =================================================================
!c
      use mecca_constants
      implicit none
!c
      character sname*10
      character istop*10
!c
      integer    nbasis,nspin,iprint
      integer    komp(ipsublat)
      integer    numbsub(nbasis)
!c
      real*8     atcon(ipcomp,ipsublat)
      real*8     zcorss(ipcomp,ipsublat)
      real*8     qcortot(ipcomp,ipsublat,ipspin)
      real*8     qval(ipcomp,ipsublat,ipspin)
      real*8     xvalws(ipcomp,ipsublat,ipspin)
      real*8     xval(ipcomp,ipsublat,ipspin)
      real*8     qc
      real*8     zvalav
      real*8     xvalav

!      integer    ndrpts,ndcomp,ndsublat
!      real*8     corden(ndrpts,ndcomp,ndsublat,nspin)
!      real*8     semcor(ndrpts,ndcomp,ndsublat,nspin)
!      real*8     rho(ndrpts,ndcomp,ndsublat,nspin)

!c parameter
      real*8     qtol
      real*8     qtolmt
      real*8     qtolcor
      real*8     qtolsem
!c
!c ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
      parameter (sname='qcheck')
      parameter (qtol=(one/ten)**6)
      parameter (qtolmt=(one/ten)**6)
      parameter (qtolcor=(one/ten)**6)
      parameter (qtolsem=(one/ten)**6)

      integer is,ic,nsub
!c
!c     debugging.......................................................
      if(iprint.ge.1) then
       write(6,'(''  qcheck:    nspin   ='',i5)') nspin
       write(6,'(''  qcheck:    nsublat ='',i5)') nbasis
       write(6,'(''  qcheck:    komp    ='',5i5)')                      &
     &   (komp(nsub),nsub=1,nbasis)
         do nsub=1,nbasis
            do ic=1,komp(nsub)
               write(6,'(''  qcheck: nsub,ic,atcon  ='',2i5,d20.11)')   &
     &         nsub,ic,atcon(ic,nsub)
               write(6,'(''  qcheck: nsub,ic,zcorss  ='',2i5,d20.11)')  &
     &         nsub,ic,zcorss(ic,nsub)
               do is=1,nspin
                  write(6,'(''  qcheck: nsub,ic,is,xvalws,xvalmt  ='',  &
     &            3i5,2d20.11)')                                        &
     &            nsub,ic,is,xvalws(ic,nsub,is),xval(ic,nsub,is)
                  write(6,'(''  qcheck: is,nsub,ic,qcortot ='',         &
     &            3i5,d20.11)')  is,nsub,ic,qcortot(ic,nsub,is)
               enddo
            enddo
         enddo
      endif
!c     ================================================================
!c     sum intgrated n(e) over spins...................................
      xvalav=zero
      do nsub=1,nbasis
         do ic=1,komp(nsub)
             xvalav=xvalav+numbsub(nsub)*(atcon(ic,nsub)*                &
     &                            sum(xvalws(ic,nsub,1:nspin)))
         enddo
      enddo
!c     ==================================================================
!c     check that average integrated valence DOS = average valence.......
      if(abs(xvalav-zvalav).gt.qtol) then
         write(6,'(''  ******** Trouble in qcheck ********'')')
         write(6,'(''  xvalav,zvalav'',2d20.11)') xvalav,zvalav
      endif
!c ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!c
!c     check integrated valence DOS = integrated valence charge........
!c
      if(iprint.ge.-1) then
       do is=1,nspin
        do nsub=1,nbasis
          do ic=1,komp(nsub)
          if(abs(xval(ic,nsub,is)-qval(ic,nsub,is)).ge.qtol)            &
     &      then
            write(6,'('' ****** Trouble in qcheck ******'')')
            write(6,'('' is,nsub,ic,xval,qval'',3i5,2d20.11)')          &
     &            is,nsub,ic,xval(ic,nsub,is),qval(ic,nsub,is)
          endif
          enddo
        enddo
       enddo
      end if

!c     ==================================================================
!c     check      core charge = Z      core..............................
!c     if( iprint .ge. 3) then
      do nsub=1,nbasis
         do ic=1,komp(nsub)
          qc=sum(qcortot(ic,nsub,1:nspin))
          if( abs((zcorss(ic,nsub)-qc) ).ge.qtolcor) then
               write(6,'('' ******** Trouble in qcheck ********'')')
               write(6,'('' nsub,ic,zcorss,qc'',2i5,2d20.11)')          &
     &         nsub,ic,zcorss(ic,nsub),qc
!c              call p_fstop(sname)
          endif
         enddo
      enddo
!c     endif
!c
!c     ==================================================================
      if(istop.eq.sname) then
       call p_fstop(sname)
      else
         return
      endif
!c
      end subroutine qcheck1

!      subroutine ValRenrm(qval,xval,                                    &
!     &                 nbasis,komp,nspin,                               &
!     &                 ndrpts,ndcomp,ndsublat,                          &
!     &                 corden,semcor,rho,                               &
!     &                 iprint)
!!c     =================================================================
!!c
!      implicit real*8 (a-h,o-z)
!!c
!      integer    nbasis,nspin,ndrpts,ndcomp,ndsublat
!      integer    komp(nbasis)
!      real*8     qval(ndcomp,ndsublat,nspin)
!      real*8     xval(ndcomp,ndsublat,nspin)
!
!      real*8     corden(ndrpts,ndcomp,ndsublat,nspin)
!      real*8     semcor(ndrpts,ndcomp,ndsublat,nspin)
!      real*8     rho(ndrpts,ndcomp,ndsublat,nspin)
!      integer    iprint
!
!      real*8     renorm,rhotmp
!
!!c parameter
!      real*8     qtol,tol
!      parameter (qtol=1.d-7)
!      parameter (tol=1.d-10)
!!c ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!!c
!!c     check integrated valence DOS = integrated valence charge........
!!c
!      do is=1,nspin
!        do nsub=1,nbasis
!          do ic=1,komp(nsub)
!          if(abs(xval(ic,nsub,is)-qval(ic,nsub,is)).ge.qtol)            &
!     &      then
!           if(iprint.ge.-1) then
!            write(6,'('' ****** Trouble in qcheck ******'')')
!            write(6,'('' is,nsub,ic,xval,qval'',3i5,2d20.11)')          &
!     &            is,nsub,ic,xval(ic,nsub,is),qval(ic,nsub,is)
!           end if
!!CDEBUG             renorm = xval(ic,nsub,is)/qval(ic,nsub,is)
!!CDEBUG             if(abs(renorm-1.d0).gt.tol) then
!!CDEBUG              do i=1,ndrpts
!!CDEBUG               rhotmp = corden(i,ic,nsub,is)+semcor(i,ic,nsub,is)
!!CDEBUG               rho(i,ic,nsub,is) = rhotmp +
!!CDEBUG     +               renorm*(rho(i,ic,nsub,is)-rhotmp)
!!CDEBUG              end do
!!CDEBUG             endif
!          endif
!          enddo
!        enddo
!      enddo
!
!      return
!!c
!      end
