!c ======================================================================
      subroutine inwhnk (invp,rg,rf,r,en,c,drg,drf,dk)
!c ======================================================================
!      implicit real*8  (a-h,o-z)
      use mecca_constants
      implicit none
!c ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!      include 'mkkrcpa.h'
!c ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!c
!c ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
      complex(8), parameter :: sqrtm1=(0.d0,1.d0)
      complex(8), parameter :: mi=(0.d0,-1.d0)

      integer, parameter :: ipdeq=5
!c ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!c
!c   drg,drf enrivatives of dp and dq times r;  c speed of light
!c   free solution..... riccati-hankel functions    fixed DDJ & FJP 1991
!c *********************************************************************
!c
      integer :: invp
      real(8) :: r(:),rg(:),rf(:)
      real(8) :: en,c
      real(8) :: drf(:),drg(:)
      real(8) :: dk
      integer, parameter :: lmaxcore=20
      complex(8) :: bh(lmaxcore),dh(lmaxcore)
      complex(8) :: x,p,mil
      real(8) :: mass

      real(8) :: cinv,c2inv,ak,sgnk,factor
      integer :: l,lp,ll,n
!c
!c **********************************************************************
!c
      cinv=one/c
      c2inv=cinv*cinv
      mass=(one + en*c2inv)
      p=sqrt( dcmplx(en*mass,zero) )
!c
!c      kappa=dk
      ak=abs(dk)
      sgnk=dk/ak
      factor= sgnk*sqrt( -en/mass )*cinv
!c
!c officially this is L+1
      l = ak + (1+sgnk)/2
      lp=l - sgnk
      mil=sqrtm1**(l-1)
!c
!c maximum l needed for this kappa
      ll=max(l,lp)

      if(ll.gt.lmaxcore) then
       write(*,*) 'INWHNK: LL=',ll,' LMAXCORE=',lmaxcore
       call fstop('INWHNK:  increase LMAXCORE')
      end if
!c
      do 5 n=iprpts,invp,-1
      x=p*r(n)
!c==============================
      call richnk(ll,x,bh,dh)
!c==============================
!c
!c NOTE: working on log-mesh so need extra factor of r for rdg and drf.
!c
      rg(n)=-mi*mil*bh(l)
!c     drg(n)=-x*mil*dh(l)
      rf(n)=-factor*mil*bh(lp)
!c     drf(n)=-x*factor*mil*dh(lp)
  5   continue
      drg(ipdeq)=-x*(mi*mil*dh(l))
      drf(ipdeq)=-x*factor*mil*dh(lp)
!c
!c     write(6,*)'  rg(invp), rf(invp) ',rg(invp), rf(invp)
      return
      end
