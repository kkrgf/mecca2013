!
!  rmomMod.f
!  meccalib
!
!  Created by andrei on 9/16/07.
!  Copyright 2007 __MyCompanyName__. All rights reserved.
!
      module Rmom

      real*8, allocatable, save :: datsaved(:,:)
      integer i0/0/,i1/-10/,ndm/-10/

!c      save datsaved
!c      save i0,i1,ndm

      end module Rmom
!c
      subroutine RmomModule(iflag,nmom,nm,nsub1,nsub2,vdata)

      USE Rmom

      implicit none

      integer iflag,nsub1,nsub2,nm,nmom
      real*8  vdata(-1:nm,nsub1:nsub2)

      integer i
      character*4 sname/'Rmom'/

      if(iflag.eq.0) then              ! read
       if(nsub2.lt.nsub1) return
       vdata(-1:nmom,nsub1:nsub2) = datsaved(-1:nmom,nsub1:nsub2)
      else if(iflag.eq.1) then         ! write
       if(nsub2.lt.nsub1) return
       datsaved(-1:nmom,nsub1:nsub2) = vdata(-1:nmom,nsub1:nsub2)
      else if(iflag.eq.2) then         ! allocate
       if(allocated(datsaved)) then
      if(nsub1.lt.i0.or.nsub2.gt.i1.or.nm.gt.ndm) then
       deallocate(datsaved)
      else
       return
      end if
       end if
       if(nsub2.lt.nsub1) return
       allocate(datsaved(-1:nm,nsub1:nsub2))
       i0 = nsub1
       i1 = nsub2
       ndm = nm
      else if(iflag.eq.-1) then        ! deallocate
       if(allocated(datsaved)) deallocate(datsaved)
      else if(iflag.eq.-2) then        ! info
       if(allocated(datsaved)) then
      nsub1 = i0
      nsub2 = i1
      nmom  = ndm
       else
      nsub1 = 0
      nsub2 = -10
      nmom  = -10
       end if
      else if(iflag.eq.-3) then        ! print
       do i=nsub1,nsub2
      write(6,'(''  nsub='',i4,2x,a,''='',12f9.6)')                     &
     &        i,sname,datsaved(-1:nmom,i)
       end do
      end if

      return
      end
!c
      subroutine noVPrmom
      USE Rmom
      implicit none

      if(allocated(datsaved))                                           &
     &  datsaved = 1.d0
      return
      end

