!c**************************************************************
      module VPlist
!      USE mecca_constants
      USE mecca_types
!      USE MECCADEF
      implicit none

      public

      type(VP), allocatable :: vpsubs(:)

      integer  nsubbgn/0/,nsubend/-1/

      CONTAINS

      subroutine VPallc(ns1,ns2)
      implicit none
      integer ns1,ns2

      if(allocated(vpsubs)) then
       if(ns1.lt.nsubbgn.or.ns2.gt.nsubend) then
      deallocate(vpsubs)
       else
      return
       end if
      end if
      nsubbgn = ns1
      nsubend = ns2
      allocate(vpsubs(nsubbgn:nsubend))

      return
      end subroutine VPallc

      subroutine VPdeallc
      implicit none

      if(allocated(vpsubs)) then
      deallocate(vpsubs)
      end if
      nsubbgn = 0
      nsubend = -1

      return
      end subroutine VPdeallc
!c
      subroutine VPalat(ns1,ns2,alat,ierr)
      implicit none
      integer ns1,ns2,ierr
      real*8 alat

      integer nsub,nn,nv,nfv,i

      if(ns1.lt.nsubbgn.or.ns2.gt.nsubend                               &
     &   .or..not.allocated(vpsubs)                                     &
     &  ) then
       ierr = -1                  ! wrong array allocation
       return
      else
       ierr = 0
      end if

      vpsubs(ns1:ns2)%Rcs = alat*vpsubs(ns1:ns2)%Rcs
      vpsubs(ns1:ns2)%Ris = alat*vpsubs(ns1:ns2)%Ris
      do nsub=ns1,ns2

       nn = vpsubs(nsub)%nnvp
       vpsubs(nsub)%Rij(0:nn) =                                         &
     &    alat*vpsubs(nsub)%Rij(0:nn)
       vpsubs(nsub)%Dij(1:nn) =                                         &
     &    alat*vpsubs(nsub)%Dij(1:nn)

       nv = vpsubs(nsub)%nvtx
       vpsubs(nsub)%vertex(1:3,1:nv) =                                  &
     &    alat*vpsubs(nsub)%vertex(1:3,1:nv)

       nfv = maxval(vpsubs(nsub)%nfvtx(1:nn))

       do i=1,nn
      vpsubs(nsub)%facevtx(1:3,1:vpsubs(nsub)%nfvtx(i),i) =             &
     &    alat*vpsubs(nsub)%facevtx(1:3,1:vpsubs(nsub)%nfvtx(i),i)
       end do

      end do

      return
      end subroutine VPalat

      end module VPlist
!c**************************************************************

!c**************************************************************
!      module IntrstlChD
!      USE mecca_constants
!      USE mecca_types
!!      USE MECCADEF
!      implicit none
!
!      public
!
!!c      type(ChDens3d), allocatable :: ChIntrstl(:)
!!c      type(SurfIntgr), allocatable :: SurfElmnt(:)
!      type(IntrstlQ), allocatable :: QintIntgrl(:,:)
!      type(IntrstlQ), allocatable :: QasaIntr(:,:)
!
!      CONTAINS
!
!      subroutine PntsAllc(nsmin,nsmax)
!      implicit none
!      integer nsmin,nsmax
!
!!c      if(allocated(ChIntrstl)) then
!!c     deallocate(ChIntrstl)
!!c      end if
!!c      allocate(ChIntrstl(nsmin:nsmax))
!!c      ChIntrstl(nsmin:nsmax)%Nk = 0
!!c
!!c      if(allocated(SurfElmnt)) then
!!c     deallocate(SurfElmnt)
!!c      end if
!!c      allocate(SurfElmnt(nsmin:nsmax))
!!c      SurfElmnt(nsmin:nsmax)%Nk = 0
!
!      return
!      end subroutine PntsAllc
!
!      subroutine PntsDeallc
!      implicit none
!!c      if(allocated(ChIntrstl)) deallocate(ChIntrstl)
!      return
!      end subroutine PntsDeallc
!!c
!      subroutine QintAllc(ncomp,nsublat)
!      implicit none
!      integer ncomp,nsublat
!
!      if(allocated(QintIntgrl)) then
!      deallocate(QintIntgrl)
!      deallocate(QasaIntr)
!      end if
!      allocate(QintIntgrl(1:ncomp,1:nsublat))
!      allocate(QasaIntr(1:ncomp,1:nsublat))
!
!      return
!      end subroutine QintAllc
!
!      subroutine QintDeallc
!      implicit none
!      if(allocated(QintIntgrl)) deallocate(QintIntgrl)
!      if(allocated(QasaIntr))   deallocate(QasaIntr)
!      return
!      end subroutine QintDeallc
!
!      subroutine QintZero(ik,nsub)
!      implicit none
!      integer ik,nsub
!
!      QintIntgrl(ik,nsub)%q0   = zero
!      QintIntgrl(ik,nsub)%qm1  = zero
!      QintIntgrl(ik,nsub)%qvxc = zero
!      QintIntgrl(ik,nsub)%qexc = zero
!      QintIntgrl(ik,nsub)%Uc   = zero
!
!      return
!      end subroutine QintZero
!
!      subroutine QintSum(ik,nsub,Qintr)
!      implicit none
!      integer ik,nsub
!      type(IntrstlQ) Qintr
!
!      QintIntgrl(ik,nsub)%q0   = QintIntgrl(ik,nsub)%q0                 &
!     &                         + Qintr%q0
!      QintIntgrl(ik,nsub)%qm1  = QintIntgrl(ik,nsub)%qm1                &
!     &                         + Qintr%qm1
!      QintIntgrl(ik,nsub)%qvxc = QintIntgrl(ik,nsub)%qvxc               &
!     &                         + Qintr%qvxc
!      QintIntgrl(ik,nsub)%qexc = QintIntgrl(ik,nsub)%qexc               &
!     &                         + Qintr%qexc
!      QintIntgrl(ik,nsub)%Uc   = QintIntgrl(ik,nsub)%Uc                 &
!     &                         + Qintr%Uc
!
!      return
!      end subroutine QintSum
!
!!c      subroutine QintCopy(ik,nsub,Q1,Q2)
!!c      implicit none
!!c      integer ik,nsub
!!c      type(IntrstlQ) Q1,Q2
!!c
!!c      Q2(ik,nsub)%q0   = Q1(ik,nsub)%SphR
!!c      Q2(ik,nsub)%q0   = Q1(ik,nsub)%qsph
!!c      Q2(ik,nsub)%q0   = Q1(ik,nsub)%q0
!!c      Q2(ik,nsub)%qm1  = Q1(ik,nsub)%qm1
!!c      Q2(ik,nsub)%qvxc = Q1(ik,nsub)%qvxc
!!c      Q2(ik,nsub)%qexc = Q1(ik,nsub)%qexc
!!c      Q2(ik,nsub)%Uc   = Q1(ik,nsub)%Uc
!!c      Q2(ik,nsub)%Uc0   = Q1(ik,nsub)%Uc0
!!c
!!c      return
!!c      end subroutine QintCopy
!!c
!      end module IntrstlChD
!!c**************************************************************
