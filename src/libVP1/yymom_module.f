      module YYmom

      character(5), parameter :: sname='YYmom'
      complex(8), allocatable, save :: YYsaved(:,:,:,:,:)
      integer, save :: i0=0,i1=-10,ndm=-10,ldm=-10
!c      save YYsaved
!c      save i0,i1,ndm,ldm

      end module YYmom

!cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc !c
      subroutine YYmomModule(iflag,                                     &
     &                       nmom,nm,                                   &
     &                       LM,llm,llmax,                              &
     &                       nsub1,nsub2,data1,data2)

      USE YYmom

      implicit none

      integer iflag,nsub1,nsub2,nm,nmom,LM,llm,llmax
      complex*16  data1(0:nm,1:llmax,nsub1:nsub2)
      complex*16  data2(0:nm,1:llmax,nsub1:nsub2)

      integer i,l,ls,kkrsz,l1,l2
!      integer i0r,i1r,ndmr,ldmr,lmr
!      integer nch,io,ndmltp,ndmltp1

      if ( llmax<1 ) then
        if ( .not. (iflag==-1 .or. iflag==-2) ) return
      end if  

      if(iflag.eq.0) then              ! read
       if(nsub2.lt.nsub1.or.nm.lt.0.or.llm.lt.1) return
       data1(0:nmom,1:llm,nsub1:nsub2) =                                &
     &            YYsaved(0:nmom,1:llm,nsub1:nsub2,LM,1)
       data2(0:nmom,1:llm,nsub1:nsub2) =                                &
     &            YYsaved(0:nmom,1:llm,nsub1:nsub2,LM,2)
      else if(iflag.eq.1) then         ! write
       if(nsub2.lt.nsub1) return
       YYsaved(0:nmom,1:llm,nsub1:nsub2,LM,1) =                         &
     &            data1(0:nmom,1:llm,nsub1:nsub2)
       YYsaved(0:nmom,1:llm,nsub1:nsub2,LM,2) =                         &
     &            data2(0:nmom,1:llm,nsub1:nsub2)
      else if(iflag.eq.2) then         ! allocate
       if(allocated(YYsaved)) then
        if(nsub1.lt.i0.or.nsub2.gt.i1.or.nm.gt.ndm.or.llmax.gt.ldm)     &
     &  then
         deallocate(YYsaved)
        else
         return
        end if
       end if
       if(nsub2.lt.nsub1) return
       allocate(YYsaved(0:nm,1:llmax,nsub1:nsub2,1:LM,1:2))
       i0 = nsub1
       i1 = nsub2
       ndm = nm
       ldm = llmax
      else if(iflag.eq.-1) then        ! deallocate
       if(allocated(YYsaved)) deallocate(YYsaved)
      else if(iflag.eq.-2) then        ! info
       if(allocated(YYsaved)) then
        nsub1 = i0
        nsub2 = i1
        nmom  = ndm
        llm   = ldm
       else
        nsub1 = 0
        nsub2 = -10
        nmom  = -10
        llm   = -10
       end if
      else if(iflag.eq.-3) then        ! print
       kkrsz = nint(sqrt(float(llm)))
       do i=nsub1,nsub2
        if(i.gt.nsub1) write(6,*)
        if(kkrsz**2.eq.llm) then
         do l1=1,kkrsz
          l = l1+(l1-1)*kkrsz
          write(6,'(''  nsub='',i4,1x,                                  &
     &             '' LM='',i2,'' llm='',i4,2x,a,''(Re)='',             &
     &                                              (12f10.6))')        &
     &        i,LM,l,sname,YYsaved(0:nmom,l,i,LM,1)
          write(6,'(''  nsub='',i4,1x,                                  &
     &             '' LM='',i2,'' llm='',i4,2x,a,''(Im)='',             &
     &                                              (12f10.6))')        &
     &        i,LM,l,sname,YYsaved(0:nmom,l,i,LM,2)
         end do
         do l2=1,kkrsz
          do l1=l2+1,kkrsz
           l  = l1+(l2-1)*kkrsz
           ls = l2+(l1-1)*kkrsz
           if(abs(YYsaved(nmom,l,i,LM,1)).gt.1.d-6                      &
     &        .or.abs(YYsaved(nmom,ls,i,LM,1)).gt.1.d-6                 &
     &       ) then
            write(6,'(''  nsub='',i4,1x,                                &
     &             '' LM='',i2,'' llm='',i4,2x,a,''(Re)='',             &
     &                                              (12f10.6))')        &
     &        i,LM,l,sname,YYsaved(0:nmom,l,i,LM,1)
            write(6,'(''  nsub='',i4,1x,                                &
     &             '' LM='',i2,'' llm='',i4,2x,a,''(Re)='',             &
     &                                              (12f10.6))')        &
     &        i,LM,ls,sname,YYsaved(0:nmom,l,i,LM,1)
           end if
           if(abs(YYsaved(nmom,l,i,LM,2)).gt.1.d-6                      &
     &        .or.abs(YYsaved(nmom,ls,i,LM,2)).gt.1.d-6                 &
     &       ) then
            write(6,'(''  nsub='',i4,1x,                                &
     &             '' LM='',i2,'' llm='',i4,2x,a,''(Im)='',             &
     &                                              (12f10.6))')        &
     &        i,LM,l,sname,YYsaved(0:nmom,l,i,LM,2)
            write(6,'(''  nsub='',i4,1x,                                &
     &             '' LM='',i2,'' llm='',i4,2x,a,''(Im)='',             &
     &                                              (12f10.6))')        &
     &        i,LM,ls,sname,YYsaved(0:nmom,l,i,LM,2)
           end if
          end do
         end do
        else
         do l=1,llm
          if(abs(YYsaved(nmom,l,i,LM,1)).gt.1.d-6) then
           write(6,'(''  nsub='',i4,1x,                                 &
     &             '' LM='',i2,'' llm='',i4,2x,a,''(Re)='',             &
     &                                              (12f10.6))')        &
     &        i,LM,l,sname,YYsaved(0:nmom,l,i,LM,1)
          end if
          if(abs(YYsaved(nmom,l,i,LM,2)).gt.1.d-6) then
           write(6,'(''  nsub='',i4,1x,                                 &
     &             '' LM='',i2,'' llm='',i4,2x,a,''(Im)='',             &
     &                                              (12f10.6))')        &
     &        i,LM,l,sname,YYsaved(0:nmom,l,i,LM,2)
          end if
         end do
        end if
       end do
      end if
      return
      end subroutine YYmomModule

      subroutine ioYYmom(io,ndmltp,nmom)

      USE YYmom

      implicit none

      integer io, ndmltp, nmom

      integer nch, ndmltp1, i0r, i1r, ldmr, ndmr
      integer ls, l, l1, l2, lmr, i

      if(io.eq.0) return
      if(.not.allocated(YYsaved)) return

      nch = abs(io)

      if(io.lt.0) then
       write(nch,*) ndmltp,' LM_max'
       write(nch,'(2(1x,i8),''    nsub0,nsub1'')') i0,i1
       write(nch,'(2(1x,i8),''    l_max,nmom_max'')') ldm,ndm
      else
       read(nch,*)  ndmltp1
       if(ndmltp.gt.ndmltp1)   then
         call p_fstop(sname//': ndmltp > ndmltp in file')
       end if  
       read(nch,*)  i0r,i1r
       if(i0r.ne.i0) call p_fstop(sname//':  Read i0-error???')
       if(i1r.ne.i1) call p_fstop(sname//':  Read i1-error???')
       read(nch,*)  ldmr,ndmr
       if(ldmr.ne.ldm) call p_fstop(sname//':  Read ldm-error???')
       if(ndmr.ne.ndm) call p_fstop(sname//':  Read ndm-error???')
      end if

      do ls=1,ndmltp                         ! ls is "LM"

       if(io.lt.0) then        ! write to file
        write(nch,'(1x,i8,''     LM'')') ls
        do i=i0,i1
         l2 = 0
         do l=1,ldm
          if(abs(YYsaved(nmom,l,i,ls,1)).gt.1.d-7) then
           l2 = l2+1
          end if 
         end do
         write(nch,'(1x,i8,''   #_of_nonzero (Re)'')') l2
         do l=1,ldm
          if(abs(YYsaved(nmom,l,i,ls,1)).gt.1.d-7) then
           write(nch,*) l
10         format(10( 1x,f13.10,1x,f13.10,1x))
           write(nch,10) YYsaved(0:ndm,l,i,ls,1)
          end if
         end do
        end do

        do i=i0,i1
         l2 = 0
         do l=1,ldm
          if(abs(YYsaved(nmom,l,i,ls,2)).gt.1.d-7) then
           l2 = l2+1
          end if 
         end do
         write(nch,'(1x,i8,''   #_of_nonzero (Im)'')') l2
         do l=1,ldm
          if(abs(YYsaved(nmom,l,i,ls,2)).gt.1.d-7) then
           write(nch,*) l
           write(nch,10) YYsaved(0:ndm,l,i,ls,2)
          end if
         end do
        end do

       else                               !      to read from file

        read(nch,*) lmr
        if(lmr.ne.ls) call p_fstop(sname//':  Read lm-error???')

        YYsaved(0:ndm,1:ldm,i0r:i1r,ls,1) = (0.d0,0.d0)
        YYsaved(0:ndm,1:ldm,i0r:i1r,ls,2) = (0.d0,0.d0)

        do i=i0r,i1r
         read(nch,*) l2
         do l=1,l2
          read(nch,*) l1
          read(nch,10) YYsaved(0:ndm,l1,i,ls,1)
         end do
        end do

        do i=i0r,i1r
         read(nch,*) l2
         do l=1,l2
          read(nch,*) l1
          read(nch,10) YYsaved(0:ndm,l1,i,ls,2)
         end do
        end do

       end if

      end do

      return
      end subroutine ioYYmom

      subroutine ndzeroYYmom(LM)

      USE YYmom

      implicit none

      integer LM
      integer ls, kkrsz, l1, l2, l

      if(.not.allocated(YYsaved)) return
      if(LM.ne.1) then
        call p_fstop(' ndzeroYYmom: LM.ne.1, NOT IMPLEMENTED???')
      end if

      ls = 1
      kkrsz = nint(sqrt(float(ldm)))
!C     do ls=1,LM
      do l1=1,kkrsz
       do l2=1,kkrsz
        if(l1.eq.l2) cycle
        l = l2 + (l1-1)*kkrsz
        YYsaved(0:ndm,l,i0:i1,ls,1) = (0.d0,0.d0)
        YYsaved(0:ndm,l,i0:i1,ls,2) = (0.d0,0.d0)
       end do
      end do
!C      end do

      return
      end subroutine ndzeroYYmom


      subroutine d1YYmom(LM)

      USE YYmom

      implicit none
      integer LM
      integer ls, kkrsz, l1, l

      if(.not.allocated(YYsaved)) return
      if(LM.ne.1) then
        call p_fstop(' d1YYmom: LM.ne.1, NOT IMPLEMENTED???')
      end if

      ls = 1
      kkrsz = nint(sqrt(float(ldm)))
      do l1=1,kkrsz
       l = l1 + (l1-1)*kkrsz
       YYsaved(0:ndm,l,i0:i1,ls,1) = (1.d0,0.d0)
       YYsaved(0:ndm,l,i0:i1,ls,2) = (0.d0,0.d0)
      end do

      return
      end subroutine d1YYmom


      subroutine L1L2mapModule(LM,nsub,ndkkr,L1L2map,eps)
      implicit none

      integer LM,nsub,ndkkr
      integer L1L2map(ndkkr,ndkkr)
      real*8 eps

      complex*16, allocatable :: yL1yL2Re(:,:)
      complex*16, allocatable :: yL1yL2Im(:,:)

      integer lm1,lm2,llmax,index
      integer kkrsz/0/
      save kkrsz,yL1yL2Re,yL1yL2Im

      if(LM.eq.-1) then
       if(allocated(yL1yL2Re)) deallocate(yL1yL2Re,yL1yL2Im)
       kkrsz = 0
       return
      end if

      if(.not.allocated(yL1yL2Re)) then
       allocate(yL1yL2Re(1:ndkkr,1:ndkkr),yL1yL2Im(1:ndkkr,1:ndkkr))
       kkrsz = ndkkr
      else
       if(ndkkr.gt.kkrsz) then
      deallocate(yL1yL2Re,yL1yL2Im)
      allocate(yL1yL2Re(1:ndkkr,1:ndkkr),yL1yL2Im(1:ndkkr,1:ndkkr))
      kkrsz = ndkkr
       end if
      end if

      llmax = kkrsz*kkrsz
      call YYmomModule(0,0,0,LM,llmax,llmax,                            &
     &                            nsub,nsub,yL1yL2Re,yL1yL2Im)

      do lm1=1,ndkkr
      L1L2map(lm1,lm1) = lm1
      end do
      index = ndkkr

      do lm1=1,ndkkr
       do lm2=1,ndkkr
      if(lm2.eq.lm1) cycle
      if(abs(yL1yL2Re(lm2,lm1))+abs(yL1yL2Im(lm2,lm1)).le.eps)          &
     &  then
       L1L2map(lm2,lm1)=0
      else
       index = index+1
       L1L2map(lm2,lm1) = index
      end if
       end do
      end do
      return
      end subroutine L1L2mapModule
!cccccccccccccccccccccccccccccccccccccccccccccccccccccccccc

