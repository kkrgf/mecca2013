      subroutine VPdata2(mtasa,alat,boa,coa,                            &
     &                  rslatt,natom,basis,itype,Smtin,                 &
     &                  Lmltp,nmom,lmax,nmomylm,epsShC,                 &
     &                  iyymom,OutVPvlm,                                &
     &                  iprint                                          &
     &                 )
      use VPlist
!CDEBUG      USE PntsList
      implicit none
      integer mtasa
      real*8  alat,boa,coa
      real*8 rslatt(3,3)
      integer natom
      real*8 basis(3,natom)
      integer itype(natom)
      real*8  Smtin(*)              ! input MT-radii (if not zero)
      integer Lmltp
      integer nmom
      integer lmax,nmomylm
      real*8  epsShC
      integer iyymom
      real*8 OutVPvlm
      integer iprint
!c
!c   output ==> {Smt,Rasa,...}Module
!c
!c      integer ipnmom
!c      parameter (ipnmom=10)
!c      real*8 rmoments(-1:ipnmom)

      real*8, allocatable :: rmoments(:)
      real*8 Smt,Rasa,Rcs,Rnn
      real(8) :: Smt_(1),Rasa_(1),Rcs_(1),Rnn_(1)
      equivalence (Smt,Smt_(1))
      equivalence (Rasa,Rasa_(1))
      equivalence (Rcs,Rcs_(1))
      equivalence (Rnn,Rnn_(1))
      integer ndnf                   ! maximum number of trianglesfaces (i.e. NN)

      parameter (ndnf=200)
      integer nf1(1)
      integer nwF(ndnf)
      real*8  Face(3,3,ndnf)
!c

      integer  iorig(natom)             !????????????
      integer ii
      real*8   ESmin
      parameter (ESmin=0.1d0)
      real*8  rji(3),rsji
      real*8  P0(1:3),P1(1:3),rslmin,delta
      logical check
      integer nijk(3),nj,nsubii
      integer ndESmax,ndESadd,ndESnsub
      real*8  EmptSphrs(1:3,natom*4)      ! ndESmax=natom*ndnf would be better

      real*8 CellV,volume,dV,asascale

      real*8 RcsSmtMax0,RcsSmtmax
      parameter (RcsSmtMax0=-1.0d0)    ! any OK (no search for ES)
!c        parameter (RcsSmtMax0=1.42d0)    ! fcc- and ideal-hcp OK
!c        parameter (RcsSmtMax0=1.30d0)    ! bcc OK
!c        parameter (RcsSmtMax0=1.0d0)     ! to look for ES in any structure

!c        parameter (RcsSmtMax0=2.000001d0)

      integer nsublat,nsub,iat,i,j,nnatom,nf0
      integer kkrsz,icmplx,ndmltp,lmaxylm
      integer l1l2max,l1,m1,l2,m2,lm1,lm2,l2l1
      integer lm1s,lm2s,l2sl1s,l1sl2s,l1l2

      integer L,M,LM,ichsgn,ierr

!c      real*8 rmomI(-1:ipnmom)
      real*8, allocatable :: rmomI(:)
      real*8 R0(3)

      complex*16, allocatable ::  yL1yL2p(:)
      complex*16, allocatable ::  yL1yL2m(:)
      complex*16, allocatable ::  yL1yL2re(:,:)
      complex*16, allocatable ::  yL1yL2im(:,:)

      integer nnmax
!CDEBUG      parameter  (nnmax=500)                 ! max size of cluster
      parameter  (nnmax=216)                 ! max size of cluster
      integer itypenn(nnmax)
      real*8 xyznn(3,nnmax),rmax

      integer npl,npt
      parameter (npl=nnmax,npt=50)

      real*8 planel(3,npl),pointl(3,npt,npl)
      real*8 pntRcs(3,npl)
      integer nRcsPnt
      integer mpoint(npl),iplane(npl)
      integer mface(npl),nmeqfc(npl)
      real*8  atmp(3+nnmax)
      integer listvrt(3,npt,npl)
      integer nvert,nface,nn

!CDEBUG
!CDEBUG      integer mlayer
!CDEBUG      integer nsphmax
!CDEBUG      integer npntmax
!CDEBUGc?      integer, allocatable :: npnts(:)
!CDEBUGc?      real*8, allocatable ::  Points(:,:,:)
!CDEBUG      real*8 rstep,Rmin
!CDEBUG

      logical lScatShCorr
      real*8 SphRad,Acube

!c      real*8 SphMom(-1:ipnmom)
      real*8 SphMom

      real*8 VolmPol

      character*10  sname
      parameter    (sname='VPdata')

      real*8 third
      parameter (third=one/three)
      real*8 pi4d3inv
      parameter (pi4d3inv=3.d0/(4.d0*pi))
      real*8 fourpi
      parameter (fourpi=4.d0*pi)

      if(nmom.lt.-1) return

      if(iprint.ge.0) write(6,*)

      nsublat = maxval(abs(itype(1:natom)))
!c
      kkrsz = (lmax+1)**2
      lmaxylm = lmax
      l1l2max = kkrsz**2
      ndmltp  = (Lmltp+1)*(Lmltp+2)/2

      ndESmax = natom*4
      ndESadd = 0

!c
      allocate(                                                         &
     &         rmoments(-1:nmom),rmomI(-1:nmom)                         &
     &,        yL1yL2p(0:nmomylm)                                       &
     &,        yL1yL2m(0:nmomylm)                                       &
     &,        yL1yL2re(0:nmomylm,1:l1l2max)                            &
     &,        yL1yL2im(0:nmomylm,1:l1l2max)                            &
     &        )
!c
      call SmtModule(2,1,nsublat,Smt_)
      call RasaModule(2,1,nsublat,Rasa_)
      call RcsModule(2,1,nsublat,Rcs_)
      call RnnModule(2,1,nsublat,Rnn_)
      call RmomModule(2,nmom,nmom,1,nsublat,rmoments)
      call FaceModule(2,ndnf,1,nsublat,                                 &
     &                      nf1,nwF,Face)

      i = 2
      call OutMTModule(i)
      if(i.eq.2) then
       lScatShCorr = .true.
       call YYmomModule(2,nmomylm,nmomylm,ndmltp,                       &
     &                 l1l2max,l1l2max,                                 &
     &                 1,nsublat,yL1yL2re,yL1yL2im)
      else
       lScatShCorr = .false.
!CDEBUG
       call YYmomModule(2,nmomylm,nmomylm,ndmltp,                       &
     &                 l1l2max,l1l2max,                                 &
     &                 1,nsublat,yL1yL2re,yL1yL2im)
!CDEBUG       lmaxylm = 0
!CDEBUG       l1l2max = 1
!CDEBUG       call YYmomModule(2,nmomylm,nmomylm,ndmltp,
!CDEBUG     *                 l1l2max,l1l2max,
!CDEBUG     *                 1,nsublat,yL1yL2re,yL1yL2im)
      end if

      SphRad = (one*pi4d3inv)**third           ! for VolmPol=1.d0
      rslmin = SphRad
      do i=1,3
       rslmin = min(rslmin,sqrt(dot_product(rslatt(1:3,i),              &
     &                                      rslatt(1:3,i))))
      end do

      do nsub=1,nsublat
       do iat=1,natom
      if(abs(itype(iat)).eq.nsub) then
       iorig(nsub) = iat
       exit
      end if
       end do
       call neighbors(itype(iat),basis(1,iorig(nsub)),                  &
     &                rslatt,natom,basis,itype,                         &
     &                nnmax,nnatom,xyznn,itypenn,rmax)

       Rnn = rmax
       Smt = rmax
       do ii=2,nnatom
      rji(1:3) = xyznn(1:3,ii)-xyznn(1:3,1)
      rsji = sqrt(dot_product(rji,rji))
      Rnn = min(Rnn,rsji)             ! min NN-distance
      if(Smtin(nsub).eq.zero) then
       nsubii = abs(itypenn(ii))
       if(Smtin(nsubii).ne.zero) then
        Smt = min(Smt,rsji-Smtin(nsubii))
       else
        Smt = min(Smt,rsji/2)
       end if
      end if
       end do
       if(Smtin(nsub).ne.zero) Smt  = Smtin(nsub)
       call SmtModule(1,nsub,nsub,Smt_)
       call RnnModule(1,nsub,nsub,Rnn_)
      end do

      CellV =                                                           &
     &    ( rslatt(2,1)*rslatt(3,2) - rslatt(3,1)*rslatt(2,2) )         &
     &         * rslatt(1,3) +                                          &
     &    ( rslatt(3,1)*rslatt(1,2) - rslatt(1,1)*rslatt(3,2) )         &
     &         * rslatt(2,3) +                                          &
     &    ( rslatt(1,1)*rslatt(2,2) - rslatt(2,1)*rslatt(1,2) )         &
     &         * rslatt(3,3)

      volume  = zero

      call VPallc(1,nsublat)            ! array allocation (list of VPs)

      do nsub=1,nsublat

       rmoments(-1:nmom) = zero
       nsubii = itype(iorig(nsub))            ! nsub = abs(nsubii)

       nn = nnmax

       call neighbors(nsubii,basis(1,iorig(nsub)),                      &
     &                rslatt,natom,basis,itype,                         &
     &                nn,nnatom,xyznn,itypenn,rmax)
!CDEBUG     ,                nnmax,nnatom,xyznn,itypenn,rmax)
       itypenn(1:nnatom) = abs(itypenn(1:nnatom))
!c
!c   list of polyhedron planes and vertices
!c

       if(nsub.ne.nsubii) then
      RcsSmtmax = -1.d0
       else
      RcsSmtmax = RcsSmtmax0
       end if

       call vrngeom1(1,                                                 &
     &              nnatom,xyznn,itypenn,rmax,                          &
     &              Rcs,RcsSmtmax,                                      &
     &              planel,pntRcs,iplane,nface,mface,nmeqfc,            &
     &              pointl,mpoint,npt,nvert,nRcsPnt,atmp)

!CDEBUG
       write(*,*) ' Ris=',vpsubs(nsub)%Ris                              &
     &           ,' Rcs=',vpsubs(nsub)%Rcs
!CDEBUG
       call RcsModule(1,nsub,nsub,Rcs_)

        if(iprint.ge.0) then
         write(6,'('' nsub='',i4,'' nface='',i2,                        &
     &               '' nvert='',i2)') nsub,nface,nvert
        end if

       VolmPol = zero
       do i=1,nface

      if(mpoint(i).gt.ipfvtx) then
       write(6,'('' i='',i2,'' mpoint(i)='',i2,                         &
     &        '' should be smaller than IPFVTX='',i2)')                 &
     &     i,mpoint(i),ipfvtx
       call p_fstop(sname//                                             &
     &         ': max.number of VPface-vertices is exceeded')
      end if

      call CalcVol(mpoint(i),pointl(1,1,i),dV)
      VolmPol = VolmPol - dV               ! "-" -- due to choice
!c                                            ! of orientation in "faceorder"
      if(abs(dV)*nface.lt.1.d-12*Smt**3) then
       write(*,*) ' nsub=',nsub
       write(*,*) ' xyz(nsub)=',xyznn(1:3,1)
       write(*,*) ' i=',i,' dV=',dV,' Smt=',Smt
       write(*,*) ' mpoint(i)=',mpoint(i)
       do j=1,mpoint(i)
        write(*,'('' j='',i2,2x,3f20.10)') j,pointl(1:3,j,i)
       end do
       write(*,*)
!       write(*,*) ' List of neighbours (Rj-Ri) for atom:'
!        write(*,'(3f20.10)') xyznn(1:3,1)
!       do j=2,nnatom
!        write(*,'('' nnj='',i3,2x,3f20.10)')                            &
!     &       j,xyznn(1:3,j)-xyznn(1:3,1)
!       end do
!
!       call p_fstop(sname//' ERROR: wrong structure? (dV~0)')
      end if
       end do
       do i=1,natom
      if(abs(itype(i)).eq.nsub)                                         &
     &   volume = volume + VolmPol
       end do

       delta = ESmin*min(SphRad,rslmin)

       call CheckPnts(rslatt,delta,nRcsPnt,pntRcs,ndESnsub)

       do i=1,ndESnsub
      EmptSphrs(1:3,ndESadd+i) = pntRcs(1:3,i)
       end do
       ndESadd = ndESadd + ndESnsub

       IF (ndESadd.gt.0) THEN        ! ndESadd-if

      write(*,*) ' NEW nRcsPnt=',ndESnsub

      do i=1,ndESnsub
       write(*,*) ' i=',i,' pnt(i)=',real(pntRcs(1:3,i))
      end do

       ELSE

!c
!c    Voronoi polyhedra are defined relative to the respective site
!c    (i.e. "center" is always in R0=(0,0,0))
!c
      R0(1:3) = zero
      nf0 = 0
      do i=1,nface
       call facetriang(mpoint(i),pointl(1:3,1:mpoint(i),i),             &
     &                   listvrt(1:3,1:mpoint(i),i))
       do j=1,mpoint(i)-2
        nf0 = nf0+1
        nwF(nf0) = nmeqfc(i)
        Face(1:3,1,nf0) = pointl(1:3,listvrt(1,j,i),i)
        Face(1:3,2,nf0) = pointl(1:3,listvrt(2,j,i),i)
        Face(1:3,3,nf0) = pointl(1:3,listvrt(3,j,i),i)
       end do
      end do

      if(nf0.gt.ndnf) then
       write(*,*) '  NDNF=',ndnf
       write(*,*) ' NSUB=',nsub
       write(*,*) '   NF=',nf0
       write(*,*)
       call fstop(sname//': NF > NDNF')
      end if

!c
      Acube  = VolmPol**third
      Rasa = SphRad*Acube

      Face(1:3,1:3,1:nf0) =                                             &
     &       Face(1:3,1:3,1:nf0)/Acube
      nf1 = nf0

      call FaceModule(1,ndnf,nsub,nsub,                                 &
     &                      nf1,nwF,Face)

      do i=1,nf1(1)
       if(nwF(i).gt.0) then
        call momntint(R0,                                               &
     &                   Face(1,1,i),                                   &
     &                   Face(1,2,i),                                   &
     &                   Face(1,3,i),                                   &
     &                    nmom,rmomI(-1))

        rmoments(-1:nmom)   = rmoments(-1:nmom)                         &
     &                        + rmomI(-1:nmom)*nwF(i)
       end if
      end do
!c
!c   rmoments(-1) -- potential for r=0
!c   rmoments(0)  -- volume, should be equal to 1 (if normalized)
!c   rmoments(n)  -- n-radial_moment; integral{ over V.P. | r**n }
!c
!c

!c     rmoments(-1:nmom) = rmoments(-1:nmom)/SphMom(-1:nmom)


      SphMom = 1.5d0*(one)/SphRad          ! for VolmPol=1.d0
      rmoments(-1) = rmoments(-1)/(1.5d0*(one)/SphRad)

      SphMom = one
      do i= 1,nmom
       SphMom = SphMom*SphRad*(one-one/(i+3.d0))
       rmoments(i) = rmoments(i)/SphMom
      end do
!c
!c
!c   rmoments(*) -- deviation from spherical shape:
!c                  rmoments == 1 for sphere.

      if(abs(rmoments(0)-one).gt.1.d-8) then
       write(*,*) ' NSUB=',nsub
       write(*,*) ' rmoments(0)=',rmoments(0)
       write(*,*) ' ERROR: Polyhedron volume is wrong....'
       write(*,*)
       call fstop(sname//': error in Voronoi tesselation?')
      end if

!C???        call SmtModule(1,nsub,nsub,Smt)

      call RasaModule(1,nsub,nsub,Rasa_)

!C???        call RnnModule(1,nsub,nsub,Rnn)

      call RmomModule(1,nmom,nmom,nsub,nsub,rmoments)

!c
!ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
!c


      DO L=0,Lmltp
      DO M=0,L

      LM = L*(L+1)/2+M+1
!CDEBUG        if(M/2*2.ne.M) then
!CDEBUG         ichsgn = -1
!CDEBUG        else
!CDEBUG         ichsgn = 1
!CDEBUG        end if

      yL1yL2re(0:nmomylm,1:l1l2max) = dcmplx(zero,zero)
      yL1yL2im(0:nmomylm,1:l1l2max) = dcmplx(zero,zero)

!CDEBUGCDEBUG        IF (.not.lScatShCorr) THEN
!CDEBUGCDEBUG         if(LM.eq.1) then
!CDEBUG        IF (.not.lScatShCorr.and.LM.eq.1) THEN
!CDEBUG          do l1 = 1,kkrsz
!CDEBUG           l2l1 = kkrsz*(l1-1)+l1
!CDEBUG           yL1yL2re(0:nmomylm,l2l1) = one
!CDEBUG          end do
!CDEBUGCDEBUG         end if
!CDEBUG
!CDEBUG        ELSE IF(iyymom.le.0) THEN
      IF(iyymom.le.0) THEN
!CDEBUG
       do l1=0,lmaxylm
       do m1=-l1,l1

        lm1  = l1*(l1+1) + m1 + 1
        lm1s = l1*(l1+1) - m1 + 1

!CDEBUG
!CDEBUG         do l2 = 0,lmaxylm
!CDEBUG         do m2 = -l2,l2
!CDEBUG

        do l2=0,l1
        if(l2.eq.0.and.m1.lt.0) cycle
        do m2=0,l2
         if(l1.eq.l2) then
          if(m2.gt.abs(m1)) cycle
         end if
         if(m2.eq.0.and.m1.lt.0) cycle

!CDEBUG
!CDEBUG          IF(m2.ne.m1.or.l1.ne.l2) CYCLE
!CDEBUG

         lm2 = l2*(l2+1) + m2 + 1
         lm2s= l2*(l2+1) - m2 + 1

!CDEBUG
         if(.not.lScatShCorr) then
          if(.not.(lm1.eq.lm2)) cycle
         end if
!CDEBUG

!C
!C   ! integer lyl1l2(1:kkrsz,1:kkrsz)
!C   ! lyl1yl2(,) < 0, if yl1yl2 = 0 due to symmetry
!C
!C            if(lyl1yl2(lm1,lm2).lt.0) cycle

         l2l1   = lm2  + kkrsz*(lm1-1)

         do i=1,nf1(1)
          call ylmtint(R0,                                              &
     &                    Face(1,1,i),                                  &
     &                    Face(1,2,i),                                  &
     &                    Face(1,3,i),                                  &
     &                     nmomylm,                                     &
     &                     lm1,lm2,                                     &
!CDEBUG     ,                     lm2,lm1,
     &                     L,M,epsShC,                                  &
     &                     yL1yL2p,yL1yL2m,iprint)

          yL1yL2re(0:nmomylm,l2l1) =                                    &
     &         yL1yL2re(0:nmomylm,l2l1) +                               &
     &                                     yL1yL2p(0:nmomylm)
          yL1yL2im(0:nmomylm,l2l1) =                                    &
     &         yL1yL2im(0:nmomylm,l2l1) +                               &
     &                                     yL1yL2m(0:nmomylm)
         end do

         if(m2.eq.0.and.m1.eq.0.and.l1.eq.l2) cycle
!c
!c Accuracy depends on "ngauss" in integration subroutine "ylmintdy"
!c
          if(lm2.ne.lm1) then
           if(abs(yL1yL2re(nmomylm,l2l1)).lt.epsShC) then
            yL1yL2re(0:nmomylm,l2l1) = dcmplx(zero,zero)
           end if
           if(abs(yL1yL2im(nmomylm,l2l1)).lt.epsShC) then
            yL1yL2im(0:nmomylm,l2l1) = dcmplx(zero,zero)
           end if
          end if
!c
          l1l2   = lm1  + kkrsz*(lm2-1)
          l1sl2s = lm1s + kkrsz*(lm2s-1)
          l2sl1s = lm2s + kkrsz*(lm1s-1)
!c
          if(l1sl2s.ne.l2l1) then
           if((m1+m2)/2*2.eq.(m1+m2)) then
            yL1yL2re(0:nmomylm,l1sl2s) = yL1yL2re(0:nmomylm,l2l1)
            yL1yL2im(0:nmomylm,l1sl2s) = yL1yL2im(0:nmomylm,l2l1)
           else
            yL1yL2re(0:nmomylm,l1sl2s) = -yL1yL2re(0:nmomylm,l2l1)
            yL1yL2im(0:nmomylm,l1sl2s) = -yL1yL2im(0:nmomylm,l2l1)
           end if
          else
           if((m1+m2)/2*2.eq.(m1+m2)) then
           else
            yL1yL2re(0:nmomylm,l1sl2s) = dcmplx(zero,zero)
            yL1yL2im(0:nmomylm,l1sl2s) = dcmplx(zero,zero)
           end if
          end if
!c
          if(lm1.ne.lm2.and.(m1.ne.0.or.m2.ne.0)) then

!CDEBUG             if(ichsgn.gt.0) then
!c
            yL1yL2re(0:nmomylm,l1l2) =                                  &
     &                             conjg(yL1yL2re(0:nmomylm,l2l1))
            yL1yL2im(0:nmomylm,l1l2) =                                  &
     &                            -conjg(yL1yL2im(0:nmomylm,l2l1))
!c
            yL1yL2re(0:nmomylm,l2sl1s) =                                &
     &                             conjg(yL1yL2re(0:nmomylm,l1sl2s))
            yL1yL2im(0:nmomylm,l2sl1s) =                                &
     &                             -conjg(yL1yL2im(0:nmomylm,l1sl2s))
!c
!CDEBUG             else
!CDEBUGc
!CDEBUG              yL1yL2re(0:nmomylm,l1l2) =
!CDEBUG     *                             conjg(yL1yL2re(0:nmomylm,l2l1))
!CDEBUG              yL1yL2im(0:nmomylm,l1l2) =
!CDEBUG     *                            -conjg(yL1yL2im(0:nmomylm,l2l1))
!CDEBUGc
!CDEBUG              yL1yL2re(0:nmomylm,l2sl1s) =
!CDEBUG     *                             conjg(yL1yL2re(0:nmomylm,l1sl2s !))
!CDEBUG              yL1yL2im(0:nmomylm,l2sl1s) =
!CDEBUG     *                            -conjg(yL1yL2im(0:nmomylm,l1sl2s !))
!CDEBUGc
!CDEBUG             end if

          end if

         end do
         end do
        end do
        end do

!c
!c  yL1yL2mom(n) -- n-radial_moment;
!c                  integral{ over V.P. | r**n Ylm1*conjg(Ylm2)*r**L*Y_LM !}
!c
        do i= L,L+nmomylm
         SphMom = one/(i+3)*SphRad**(i+3)
         yL1yL2re(i-L,1:l1l2max) =                                      &
     &          yL1yL2re(i-L,1:l1l2max)/SphMom
         yL1yL2im(i-L,1:l1l2max) =                                      &
     &          yL1yL2im(i-L,1:l1l2max)/SphMom
        end do
        if(LM.eq.1) then
         do l1 = 1,kkrsz
        l2l1 = kkrsz*(l1-1)+l1
        yL1yL2re(0:nmomylm,l2l1) = yL1yL2re(0:nmomylm,l2l1)-one
         end do
        end if
!c
!cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
!c
        call YYmomModule(1,nmomylm,nmomylm,LM,l1l2max,l1l2max,          &
     &                             nsub,nsub,yL1yL2re,yL1yL2im)

       END IF

      END DO   ! M-cycle
      END DO   ! L-cycle

       END IF    ! ndESadd-if

      end do   ! nsub-cycle

      OutVPvlm = CellV - volume
      if(abs((volume-CellV)/CellV).gt.1.d-9) then
       write(6,*)
       write(6,*) ' CellVolume=',CellV                                  &
     &,             ' Integral over VP=',volume
!c         call fstop(' VPdata:  DEBUG')

       if(OutVPvlm.lt.zero) then
      write(6,*)
      write(6,*) ' Integral over VP > CellVolume ????'
      call fstop(sname//': ERROR in Voronoi tesselation?')
       end if

       asascale = (CellV/volume)**third
       do nsub=1,nsublat
      call RasaModule(0,nsub,nsub,Rasa_)
      Rasa = Rasa*asascale
      call RasaModule(1,nsub,nsub,Rasa_)
       end do
      else
       OutVPvlm = zero
      end if

      if(iprint.ge.0.or.ndESadd.gt.0) then
       call SmtModule(-3,1,nsublat,Smt_)
       if(nsublat.gt.1) write(6,*)
       if(ndESadd.eq.0) then
        call RasaModule(-3,1,nsublat,Rasa_)
        if(nsublat.gt.1) write(6,*)
       end if
       call RcsModule(-3,1,nsublat,Rcs_)
       if(nsublat.gt.1) write(6,*)
       call RnnModule(-3,1,nsublat,Rnn_)
       write(6,*)

       if (ndESadd.gt.0) then
        delta = ESmin*min(SphRad,rslmin)
        call CheckPnts(rslatt,delta,ndESadd,EmptSphrs,nRcsPnt)
        write(6,*)
        RcsSmtmax = zero
        do nsub=1,nsublat
         call SmtModule(0,nsub,nsub,Smt_)
         call RcsModule(0,nsub,nsub,Rcs_)
         nsubii = itype(iorig(nsub))            ! nsub = abs(nsubii)
         write(6,*) ' nsub=',nsubii,' Rcs/Smt=',Rcs/Smt
         if(nsubii.gt.0) RcsSmtmax = max(RcsSmtmax,Rcs/Smt)
        end do
        write(6,*)
        write(6,*) ' WARNING:'
        write(6,*)
        write(6,'('' Rcs/Smt_max ='',f9.6,                              &
     &            '' > RcsSmtmax0 ='',f9.6)') RcsSmtmax,RcsSmtmax0
        write(6,*)
        write(6,'('' Number of Empty Spheres to be added:'',i5)')       &
     &           nRcsPnt
        write(6,'('' Positions:'')')
        do i=1,nRcsPnt
         write(6,'(i5,3(1x,f16.10))') nsublat+i                         &
     &,                       EmptSphrs(1,i)                            &
     &,                       EmptSphrs(2,i)/boa                        &
     &,                       EmptSphrs(3,i)/coa
        end do

        write(6,*)
        write(6,*) ' If you do not want to add empty spheres,'
        write(6,*) ' please, recompile the subroutine with '
        write(6,*) '     RcsSmtMax0=-1.0d0 '
        write(6,*)

        call fstop(sname//': you have to add empty spheres')
       end if

       call RmomModule(-3,nmom,nmom,1,nsublat,rmoments)
       write(6,*)

!CDEBUG       if(lScatShCorr.and.iyymom.le.0) then
       if(iyymom.le.0 .and. iprint>3) then
        do LM=1,ndmltp
         call YYmomModule(-3,nmomylm,nmomylm,LM,l1l2max,l1l2max,        &
     &                            1,nsublat,yL1yL2re,yL1yL2im)
         write(6,*)
        end do
       end if

      end if

      if(mtasa.eq.1.and.(.not.lScatShCorr)) then
       do nsub=1,nsublat
      rmoments(-1:nmom) = one
      call RmomModule(1,nmom,nmom,nsub,nsub,rmoments)
       end do
       write(6,*)
       write(6,*) '  '//sname//':: WARNING ::  mtasa=',mtasa            &
     &            ,'     Rmoments --> 1.'
       write(6,*)
      end if

      deallocate(rmoments,rmomI,yL1yL2p,yL1yL2m,yL1yL2re,yL1yL2im)

!CDEBUG
!CDEBUG      mlayer =  5
!CDEBUG      nsphmax = 60
!CDEBUG      npntmax = (3*nsphmax)*(3+mlayer)
!CDEBUG
!CDEBUG      call PntsAllc(nsublat,vpsubs(1:nsublat)%nnvp,npntmax)
!CDEBUG
!CDEBUG      do nsub = 1,nsublat
!CDEBUG       rstep = (vpsubs(nsub)%Rcs - vpsubs(nsub)%Ris)/5
!CDEBUG       Rmin  = 0.95d0*vpsubs(nsub)%Ris
!CDEBUG       call outMTpnts(nsub,nsub,rstep,Rmin)
!CDEBUGc     ,                PointsSubs(nsub))
!CDEBUGc       if(PointsSubs(nsub)%nxyz(0).gt.npntmax
!CDEBUGc     ,   .or. PointsSubs(nsub)%nxyz(0).lt.0
!CDEBUGc     ,   ) then
!CDEBUGc        write(*,*) ' npntmax=',npntmax
!CDEBUGc        write(*,*) ' Nxyz(',nsub,')=',PointsSubs(nsub)%nxyz(0)
!CDEBUGc        call fstop(sname//': Nxyz < 1 or > Npntmax')
!CDEBUGc       end if
!CDEBUGc       write(70+nsub,*) PointsSubs(nsub)%nxyz(0),nsub
!CDEBUGc     ,                  ,' npnts(nsub),nsub'
!CDEBUGc       write(70+nsub,*) rstep,Rmin,' rstep,Rmin'
!CDEBUGc?       do ii=1,npnts(nsub)
!CDEBUGc?        write(70+nsub,'(1x,3f10.5)') Points(1:3,ii,nsub)
!CDEBUGc?       end do
!CDEBUGc       close(70+nsub)
!CDEBUG
!CDEBUG      end do
!CDEBUG
!c      deallocate(npnts,Points)
!CDEBUG

      call VPalat(1,nsublat,alat,ierr)
      if(ierr.lt.0) call fstop(sname//':: ERROR in VPalat ::')

      return
      end subroutine VPdata2
!c
      subroutine neighbors(nsub,point,                                  &
     &                     rslatt,natom,basis,itype,                    &
     &                     nnmax,nnatom,xyznn,itypenn,rmax)
!c
!c   To generate a list containing Voronoi-tesselation neighbours
!c   for site of type "nsub" at a point "point"
!c
      implicit none

!c   input:
!c
      integer nsub
      real*8  point(3)
      real*8 rslatt(3,3)
      integer natom
      real*8 basis(3,natom)
      integer itype(natom)

!c   output:
!c
      integer nnatom
      real*8 xyznn(3,*)
      integer itypenn(*)
!c
      integer nnmax
      real*8 Volume,CellV

      real*8 delta(3),tmpxyz(3),bi(3),rnij(3)
      real*8 rmax,rmin,rij,eps
!CDEBUG
      real*8 dstn(nnmax),aindx(nnmax),tmpnn(3,nnmax)
      integer itmp(nnmax)
!CDEBUG
      integer i,j,nmin,nm1,nm2,nm3,n1,n2,n3

      character sname*10
      parameter (sname='neighbors')

      real*8 third,half
      parameter (third=1.d0/3.d0,half=0.5d0)

      CellV =                                                           &
     &    ( rslatt(2,1)*rslatt(3,2) - rslatt(3,1)*rslatt(2,2) )         &
     &         * rslatt(1,3) +                                          &
     &    ( rslatt(3,1)*rslatt(1,2) - rslatt(1,1)*rslatt(3,2) )         &
     &         * rslatt(2,3) +                                          &
     &    ( rslatt(1,1)*rslatt(2,2) - rslatt(2,1)*rslatt(1,2) )         &
     &         * rslatt(3,3)

      rmax = half*(nnmax*CellV/natom)**third
      eps = 1.d-14*rmax

      delta(1:3) =                                                      &
     &     half*(rslatt(1:3,1)+rslatt(1:3,2)+rslatt(1:3,3))

      rmin = sqrt(dot_product(delta,delta))
      do i=1,3
       tmpxyz(1:3) = delta(1:3)-rslatt(1:3,i)
       rmin = min(rmin,sqrt(dot_product(tmpxyz,tmpxyz)))
      end do
!CDEBUG      nmin = 2+rmax/rmin
      nmin = rmax/rmin
      nm1 = rmax/sqrt(dot_product(rslatt(1:3,1),rslatt(1:3,1)))
      nm2 = rmax/sqrt(dot_product(rslatt(1:3,2),rslatt(1:3,2)))
      nm3 = rmax/sqrt(dot_product(rslatt(1:3,3),rslatt(1:3,3)))

!CDEBUG      nm1 = max(nm1,nmin)
!CDEBUG      nm2 = max(nm2,nmin)
!CDEBUG      nm3 = max(nm3,nmin)
      nm1 = 1+max(nm1,nmin)
      nm2 = 1+max(nm2,nmin)
      nm3 = 1+max(nm3,nmin)

      xyznn(1:3,1) = point(1:3)
      itypenn(1) = nsub
      nnatom = 1

      do n1=-nm1,nm1
       do n2=-nm2,nm2
      do n3=-nm3,nm3
       bi(1:3) = xyznn(1:3,1) +                                         &
     &                n1*rslatt(1:3,1)                                  &
     &               +n2*rslatt(1:3,2)                                  &
     &               +n3*rslatt(1:3,3)
       do j=1,natom
        rnij(1:3) = basis(1:3,j) - bi(1:3)
        rij = sqrt(dot_product(rnij,rnij))
        if(rij.le.rmax) then
         if(rij.lt.eps) cycle
         nnatom = nnatom+1
         dstn(nnatom) = rij
         aindx(nnatom) = nnatom
         tmpnn(1:3,nnatom) = rnij(1:3) + xyznn(1:3,1)
         itmp(nnatom) = itype(j)
          end if
       end do
      end do
       end do
      end do

      if(nnatom.gt.nnmax) then
       write(*,*) ' NNMAX=',nnmax
       write(*,*) ' NNATOM=',nnatom
       call fstop(sname//': NNATOM > NNMAX')
      end if

      call dsort(dstn(2),aindx(2),nnatom-1,2)

      do j=2,nnatom
       i = nint(aindx(j))
       itypenn(j) = itmp(i)
       xyznn(1:3,j) = tmpnn(1:3,i)
      end do

      return
      end

       subroutine faceorder(Nf,Pf,Hf)
       implicit none
!c
!c   ordering of face vertexes
!c
!c   Nf -- number of face vertexes,
!c   Pf(1:3,*) -- vertex coordinates
!c   Hf(1:3)   -- vector perpendicular to a face
!c
       integer Nf
       real*8 Pf(3,*),Hf(3)

       real*8 a1(3),ai(3),hn(3),atmp(3)

       integer ndvrt
       parameter (ndvrt=50)
       real*8 sn(ndvrt),rtmp(3,ndvrt),aindex(ndvrt),sni
       real*8 Dm,Di,Sz
       integer i,im,imp,ic

       real*8 zero
       parameter (zero=0.d0)
       real*8 eps
       parameter (eps=1.d-13)
       character*10 sname
       parameter (sname='faceorder')

       if(Nf.gt.ndvrt) then
        write(*,*) ' Nf=',Nf,'>  NDVRT=',ndvrt
        stop  sname
       end if

       Dm = sum(Pf(1:3,1))
       im = 1
       Sz = sign(1.d0,Hf(3))

       do i=2,Nf
        Di = sum(Pf(1:3,i))
        if(Sz*(Di-Dm).gt.eps) then
         if(abs(Di-Dm).lt.eps) then
          if(Sz*(Pf(3,i)-Pf(3,im)).gt.eps) then
           Dm = Di
           im = i
          else if(abs(Pf(3,i)-Pf(3,im)).lt.eps) then
           if(Sz*(Pf(2,i)-Pf(2,im)).gt.-eps) then
            Dm = Di
            im = i
           end if
          end if
         else
          Dm = Di
          im = i
         end if
        end if
       end do

       imp = im+1
       if(imp.gt.Nf) imp=1

       a1(1:3) = Pf(1:3,imp) - Pf(1:3,im)
       a1(1:3) = a1(1:3)/sqrt(dot_product(a1,a1))
       aindex(1) = im
       aindex(2) = imp
       sn(2) = zero

       ic = 2
       do i=1,Nf
        if(i.eq.im.or.i.eq.imp) cycle
        ic = ic+1
        ai(1:3) = Pf(1:3,i)-Pf(1:3,im)
        ai(1:3) = ai(1:3)/sqrt(dot_product(ai,ai))

        sni = dot_product(a1,ai)
        if(abs(sni).gt.1.d0) then
         sni = sign(1.d0,sni)
        end if
        sni = acos(sni)
        call vectprd(ai,a1,hn)
        if(dot_product(hn,Hf).lt.zero) then
         sn(ic) = -sni
        else
         sn(ic) = sni
        end if
        aindex(ic) = i
       end do

       call dsort(sn(2),aindex(2),Nf-1,2)

       rtmp(1:3,1:Nf) = Pf(1:3,1:Nf)
       Pf(1:3,1:Nf) = rtmp(1:3,nint(aindex(1:Nf)))

       return
       end

       function cmpplgn(nf1,Pf1,nf2,Pf2,atmp)
       implicit none
!c
!c  Pf1,Pf2 -- polygons with "ordered" vertexes
!c
       logical cmpplgn
       integer nf1,nf2
       real*8 Pf1(3,nf1),Pf2(3,nf2),atmp(*)

       integer i,nf,i1,i2,i2m
       real*8 v1i(3),v2i(3)

       real*8 eps
       parameter (eps=1.d-13)

       cmpplgn = .false.
!CDEBUG
!CDEBUG         return
!CDEBUG

       if(nf1.ne.nf2) return
       nf = nf1

       do i=1,nf
        atmp(i) = dot_product(Pf2(1:3,i),Pf2(1:3,i))
        atmp(i+nf) = dot_product(Pf1(1:3,i),Pf1(1:3,i))
       end do
       call dsort(atmp(1),atmp,nf,1)
       call dsort(atmp(1+nf),atmp,nf,1)
       do i=1,nf
        if(abs(atmp(i)-atmp(i+nf)).gt.eps) then
         return
        end if
       end do

       v1i(1:3) = Pf1(1:3,1)-Pf1(1:3,Nf)
       atmp(1) = dot_product(v1i,v1i)

       do i=1,nf
        i1 = i
        i2 = i+(nf-1)
        if(i2.gt.nf) i2 = i2-nf
        v2i(1:3) = Pf2(1:3,i1)-Pf2(1:3,i2)
        atmp(1+nf) = dot_product(v2i,v2i)
        if(abs(atmp(1)-atmp(1+nf)).le.eps) exit
       end do

       do i=2,nf
        v1i(1:3) = Pf1(1:3,i)-Pf1(1:3,i-1)
        i2 = (i1-1)+i
        i2m = i2-1
        if(i2.gt.nf) i2=i2-nf
        if(i2m.gt.nf) i2m=i2m-nf
        v2i(1:3) = Pf2(1:3,i2)-Pf2(1:3,i2m)
        atmp(i) = dot_product(v1i,v1i)
        atmp(i+nf) = dot_product(v2i,v2i)
       end do
       call dsort(atmp(1),atmp,nf,1)
       call dsort(atmp(1+nf),atmp,nf,1)
       do i=1,nf
        if(abs(atmp(i)-atmp(i+nf)).gt.eps) then
         return
        end if
       end do

       cmpplgn = .true.

       return
       end
!c
       subroutine facetriang(Nf,Pf,listvrt)
       implicit none
!c
!c   triangulization of polygon
!c
!c   Nf -- number of face vertexes,
!c   Pf(1:3,*) -- vertex coordinates
!c
       integer Nf
       real*8 Pf(3,*)
       integer listvrt(3,*)

       real*8 a1(3),ai(3),hn(3),atmp(3)

       real*8 aindex(NF)
       integer i

       real*8 zero
       parameter (zero=0.d0)
       character*10 sname
       parameter (sname='facetriang')

       do i=1,Nf
        aindex(i) = i
       end do

       do i=Nf,3,-1
!        call fndtrngl(Pf,aindex,i,listvrt(1:3,Nf-i+1),a1,ai)
        listvrt(1,Nf-i+1) = nint(aindex(1)) 
        listvrt(2,Nf-i+1) = nint(aindex(i)) 
        listvrt(3,Nf-i+1) = nint(aindex(i-1)) 
       end do

       return
       end

       subroutine fndtrngl(Pf,aindex,Ni,lstv,a1,a2)
       implicit none
!c
!c   Ni -- number of non-zero elements in aindex
!c
       integer Ni,lstv(3)
       real*8 Pf(1:3,*),aindex(*)
       real*8 a1(3),a2(3)
       real*8 zero
       parameter (zero=0.d0)

       if(Ni.lt.3) return
!c
       lstv(1) = nint(aindex(1))
       lstv(3) = nint(aindex(Ni))
       lstv(2) = nint(aindex(Ni-1))

       aindex(Ni) = zero

!CDEBUG        write(*,*) ' lstv=',lstv

       return
       end


!C      function TetraVol(R0,p1,p2,p3)
!C      implicit none
!C      real*8 TetraVol
!C      real*8 R0(3),p1(3),p2(3),p3(3)
!C      real*8 v1(3),v2(3),hn(3)
!C      real*8 a1,a2,H,csa
!C      v1 = p2 - p1
!C      a1 = sqrt(dot_product(v1,v1))
!C      v1 = v1/a1
!C      v2 = p3 - p1
!C      a2 = sqrt(dot_product(v2,v2))
!C      v2 = v2/a2
!C      csa = dot_product(v1,v2)
!C
!C      csa = max(0.d0,1-csa*csa)
!C
!C      call vectprd(v1,v2,hn)
!C      hn = hn / sqrt(dot_product(hn,hn))
!C      H = abs(dot_product(p1-R0,hn))
!Cc      TetraVol = H*(0.5d0*a1*a2*sqrt(1.d0-csa*csa))/3.d0
!C      TetraVol = H*(0.5d0*a1*a2*sqrt(csa))/3.d0
!C      return
!C      end

      subroutine momntint(R0,P1,P2,P3,nmom,rmoments)
!c
!c        R0 -- origin
!c        (P1,P2,P3) -- triangle in 3d
!c
!c         rmoments == integral[ |R-R0|**n dx dy dz over tetrahedron {R0, !(R1,R2,R3)} ]
!c          if R is a point from (R1,R2,R3) then
!c          Theta is an angle between (R-R0) and a unit vector
!c          perpendicular to the triangle
!c
      implicit none

      real*8 R0(3)
      real*8 P1(3),P2(3),P3(3)

      integer nmom
      real*8 rmoments(-1:nmom)

      real*8 VA1(3),VA2(3),VA3(3),P0(3)
      real*8 a1,a2,a3
      real*8 csa,csb,tga,tgb,a2x,h,p0x,p0y

      integer ndfun
      parameter (ndfun=10)
      real*8 funpar(ndfun)

      real*8 err0
      parameter (err0=1.d-8)

      integer ierr,i
      real*8 err,x0,x1,sum

      real*8 cosds

      real*8 tridylog
      external tridylog

      if(nmom.lt.-1) return

      rmoments = 0.d0

      P0  = P1-R0
      VA1 = P3-P1
      VA2 = P2-P1
      VA3 = VA2-VA1
      a1 = sqrt(dot_product(VA1,VA1))
      a2 = sqrt(dot_product(VA2,VA2))
      a3 = sqrt(dot_product(VA3,VA3))

      if(a2.gt.max(a1,a3)) then
       VA1 = VA2
       a1 = a2
       VA2 = P3-P1
       a2 = sqrt(dot_product(VA2,VA2))
       VA3 = VA2-VA1
       a3 = sqrt(dot_product(VA3,VA3))
      else if(a3.gt.max(a1,a2)) then
       P0 = P3-R0
       VA1 = VA3                      ! P2-P3
       a1 = a3
       VA2 = P1-P3
       a2 = sqrt(dot_product(VA2,VA2))
       VA3 = VA2-VA1
       a3 = sqrt(dot_product(VA3,VA3))
      end if

      if(abs(a1-a2-a3).le.1.d-7*(a1+a2+a3)) return

      csa = (dot_product(VA1,VA2)/(a1*a2))
      tga = sqrt(1.d0-csa*csa)/csa
      csb = -(dot_product(VA1,VA3)/(a1*a3))
      tgb = sqrt(1.d0-csb*csb)/csb
      a2x = a2*csa

      VA1 = VA1/a1
      VA2 = VA2 - dot_product(VA2,VA1)*VA1
      VA2 = VA2/sqrt(dot_product(VA2,VA2))
      call vectprd(VA1,VA2,VA3)
      VA3 = VA3/sqrt(dot_product(VA3,VA3))

      h = abs(dot_product(P0,VA3))
      p0x = dot_product(P0,VA1)
      p0y = dot_product(P0,VA2)

       err = err0
       x0 = 0.d0
       x1 = a2x
       funpar(1) = p0y
       funpar(2) = tga
       funpar(3) = p0x
       funpar(4) = h*h
       funpar(5) = p0y

       do i=-1,nmom
        funpar(6) = i
        call dgaus8m (tridylog,x0,x1,funpar,ERR,sum,ierr)

        if(abs(ierr).gt.1) then
         write(*,*)
         write(*,*) ' ierr=',ierr
         write(*,*) ' x0,x1:',x0,x1
         write(*,*) ' p0x,p0y:',p0x,p0y
         write(*,*) ' tga,tgb:',tga,tgb
         write(*,*) ' a2x,a1,h=',a2x,a1,h
         write(*,*) ' R0:',r0
         write(*,*) ' P1:',p1
         write(*,*) ' P2:',p2
         write(*,*) ' P3:',p3
         write(*,*) ' I=',i
         write(*,*)
         write(*,*) ' ERR=',ERR
         write(*,*) ' sum=',sum
         write(*,*)

         stop ' momntint: integration error 1'
        end if

        rmoments(i) = rmoments(i) + sum
       end do

       err = err0
       x0 = a2x
       x1 = a1
       funpar(1) = p0y+a1*tgb
       funpar(2) = -tgb
       funpar(3) = p0x
       funpar(4) = h*h
       funpar(5) = p0y

       do i=-1,nmom
        funpar(6) = i
        call dgaus8m (tridylog,x0,x1,funpar,ERR,sum,ierr)
        if(abs(ierr).gt.1) then
         write(*,*)
         write(*,*) ' ierr=',ierr
         write(*,*) ' x0,x1:',x0,x1
         write(*,*) ' p0x,p0y:',p0x,p0y
         write(*,*) ' tga,tgb:',tga,tgb
         write(*,*) ' a2x,a1,h=',a2x,a1,h
         write(*,*) ' R0:',r0
         write(*,*) ' P1:',p1
         write(*,*) ' P2:',p2
         write(*,*) ' P3:',p3
         write(*,*) ' I=',i
         write(*,*)

         stop ' momntint: integration error 2'
        end if

        rmoments(i) = rmoments(i) + sum

       end do

       do i=-1,nmom
        rmoments(i) = rmoments(i)/(i+3)*h
       end do

      return
      end

      function tridylog(x,param)
      implicit none
      real*8 x,tridylog
      real*8 param(6)
      real*8 x2,ax
      real*8 y1,y2
      real*8 cy1,cy2
      real*8 acy1,acy2

      real*8 tmpi

      integer n,n1,i,m
      real*8 one
      parameter (one=1.d0)

      y1 = param(5)
      y2 = param(1) + param(2)*x

      n = nint(param(6))

      if(mod(n,2).eq.0) then
!c
       tridylog = y2 - y1
!c
       if(n.le.0) return
!c
       x2 = (param(3)+x)**2
       ax = param(4) + x2                  ! ="x*x+h*h"
       cy2 = ax+(y2*y2)
       cy1 = ax+(y1*y1)
       acy2 = y2
       acy1 = y1
       n1 = 0
       m = n/2
!c
      else
!c
       x2 = (param(3)+x)**2
       ax = param(4) + x2                  ! ="x*x+h*h"
       cy2 = ax+y2*y2
       cy1 = ax+y1*y1
       acy2 = sqrt(cy2)
       acy1 = sqrt(cy1)
       tridylog = log((y2+acy2)/(y1+acy1))
!c
       if(n.le.0) return
!c
       acy2 = y2/acy2
       acy1 = y1/acy1
       n1 = -1
       m = (n+1)/2
      end if

      do i=1,m
       n1 = n1+2
       acy2 = acy2*cy2
       acy1 = acy1*cy1
       tridylog = (acy2 - acy1 + n1*ax*tridylog)/(n1+1)
      end do

      return
      end

       subroutine EnergShC(NShC,iprint)
       implicit none
!c
       integer NShC
       integer iprint

       character*10 sname/'EnergShC'/

       integer nf(1)

       integer, allocatable :: nwF(:)
       real*8, allocatable ::  Face(:,:,:)

       integer ndnf,nsublat,nsub1,nsub2
       integer N1,N2,nsub
       real*8  alphen1,alphen2,energy(1)
       integer nwFdummy(1)
       real*8  Facedummy(1)

       real*8 rmomns(1),rmomi(1)
       real*8 epsmom
       parameter (epsmom=1.d-8)
       integer nsubi,i,iex,norder

       N1 = abs(NShC)
       N2 = 2*N1

       if(iprint.ge.0) write(6,*)

       nsub1 = 1
       nsub2 = 1
       call FaceModule(-2,ndnf,nsub1,nsub2,                             &
     &                      nf,nwFdummy,Facedummy)

       nsublat = nsub2-nsub1+1
       if(nsublat.lt.0) then
      if(iprint.ge.0) write(6,*) sname//':: WARNING, nsublat <0 ::'     &
     &                  ,' nsub1=',nsub1,' nsub2=',nsub2
      return
       end if

       ndnf = nf(1)

       allocate(Face(1:3,1:3,ndnf),nwF(1:ndnf))
       call ShCModule(2,1,nsublat,energy)

       norder = 0
       call RmomModule(-2,norder,norder,i,iex,rmomns)
!c                                     ! i,iex,rmomns -- dummy args
       nsub = 1
       call FaceModule(0,ndnf,nsub,nsub,                                &
     &                      nf,nwF,Face)
       call CalcEnShC(N1,nf(1),nwF,Face,alphen1)
       call CalcEnShC(N2,nf(1),nwF,Face,alphen2)
       energy = (N2**2*alphen2-N1**2*alphen1)/(N2**2-N1**2)
       if(iprint.ge.1)                                                  &
     &   write(6,'(''  nsub='',i4,'' EnergShC='',3(1x,f9.6),            &
     &                                         '' NShC='',i5)')         &
     &         nsub,alphen1,alphen2,energy,NShC
       call ShCModule(1,nsub,nsub,energy)

       do nsub=2,nsublat
      energy = -10.d0
      if(norder.ge.-1) then
       call RmomModule(0,-1,-1,nsub,nsub,rmomns)
       do nsubi=1,nsub-1
        call RmomModule(0,-1,-1,nsubi,nsubi,rmomi)
        if(abs(rmomns(1)-rmomi(1)).lt.epsmom) then
         call ShCModule(0,nsubi,nsubi,energy)
         exit
        end if
       end do
      end if
      if(energy(1).eq.-10.d0) then
       call FaceModule(0,ndnf,nsub,nsub,                                &
     &                      nf,nwF,Face)
       call CalcEnShC(N1,nf(1),nwF,Face,alphen1)
       call CalcEnShC(N2,nf(1),nwF,Face,alphen2)
       energy = (N2**2*alphen2-N1**2*alphen1)/(N2**2-N1**2)
       if(iprint.ge.1)                                                  &
     &   write(6,'(''  nsub='',i4,'' EnergShC='',3(1x,f9.6),            &
     &                                         '' NShC='',i5)')         &
     &         nsub,alphen1,alphen2,energy,NShC
      end if
      call ShCModule(1,nsub,nsub,energy)
       end do

       if(iprint.ge.0) then
      call ShCModule(-3,1,nsublat,energy)
       end if

       if(NShC.gt.0) then
      call FaceModule(-1,ndnf,nsub1,nsub2,                              &
     &                      nf,nwF,Face)
       end if

       deallocate(Face,nwF)

       return
       end

       subroutine CalcEnShC(N,nf,nwF,Face,energy)
       implicit none

       integer N
       integer nf
       integer nwF(nf)
       real*8 Face(3,3,nf)
       real*8 energy                    ! normalized Coulomb energy
!c                            energy = 1.d0 for sphere of Volume=1 and rh !o=1
!c
       real*8 SphRad,SphEn,dV,en0
       real*8 tetrsum
       real*8 R0(3)
       integer j,i

       real*8 VolmPol,zero
       parameter (VolmPol=1.d0,zero=0.d0)
       real*8 pi
       parameter (pi=3.1415926535897932384d0)
       real*8 pi4d3inv
       parameter (pi4d3inv=3.d0/(4.d0*pi))

       SphRad = (VolmPol*pi4d3inv)**(1.d0/3.d0)
       SphEn = 0.6d0*VolmPol**2/SphRad

       dV = VolmPol/dfloat(N)**3

       R0 = zero
       energy = zero
       do j=1,nf
      if(nwF(j).gt.0) then
       en0 = zero
       do i=1,nf
        call  tetrint(R0,                                               &
     &                    Face(1,1,j),                                  &
     &                    Face(1,2,j),                                  &
     &                    Face(1,3,j),                                  &
     &                    Face(1,1,i),                                  &
     &                    Face(1,2,i),                                  &
     &                    Face(1,3,i),                                  &
     &                    dV,                                           &
     &                    tetrsum)
        en0    = en0+tetrsum
       end do
       energy = energy + en0*nwF(j)
      end if
       end do

       energy = (0.5d0*energy)/SphEn

       dV = -1.d0                              ! to deallocate
       call tetrint(R0,                                                 &
     &              Face(1,1,1),                                        &
     &              Face(1,2,1),                                        &
     &              Face(1,3,1),                                        &
     &              Face(1,1,1),                                        &
     &              Face(1,2,1),                                        &
     &              Face(1,3,1),                                        &
     &              dV,                                                 &
     &              tetrsum)


       return
       end

      subroutine tetrint(R0,Fj1,Fj2,Fj3,                                &
     &                        Fi1,Fi2,Fi3,                              &
     &                     dV,                                          &
     &                     tetrsum)
      implicit none
!c
!c    R0 - top of a "tetrahedron"
!c    {Fj1,Fj2,Fj3} - base of a tetrahedron to calculate j-i-"energy" ter !m
!c     hn           -- perpendicular to {Fj}, |hn|=1
!c    {Fi1,Fi2,Fi3} - base of a tetrahedron to calculate "potential"
!c                    in each point from i-tetrahedron
!c
!c     N - number of integration layers
!c
!c    tetrsum == Integral[ 1/|R-R'|, {R' is from (R0,Fi)}
!c                                   {R  is from (R0,Fj)} ]
!c
      real*8 R0(3)
      real*8 Fj1(3),Fj2(3),Fj3(3)
      real*8 Fi1(3),Fi2(3),Fi3(3)
      real*8 dV
      real*8 tetrsum

      integer N
      real*8 VA1(3),VA2(3)
      real*8 hn(3)
      real*8 h0

!CDEBUG        real*8 Volume,CheckVolm

      real*8 H,a1,a2,csa,AreaN,dlt,vi,h1,h2,cosds(1)
      real*8 q1(3),q2(3),q3(3),Pm(3)

      integer i,j,k,M,Mp
!CDEBUG        integer Mtot

      integer ierr
      real*8, allocatable :: Ri(:),Zi(:)

      real*8 pi
      parameter (pi=3.1415926535897932384d0)
      real*8 pi4d3inv
      parameter (pi4d3inv=3.d0/(4.d0*pi))

      integer iprint/1/,N0/0/
      save iprint
!CDEBUG
      save Ri,Zi,N0
!CDEBUG

      if(dV.le.0) then
       if(allocated(Zi)) deallocate(Ri,Zi,stat=ierr)
       return
      end if

      tetrsum = 0.d0
      VA1 = Fj2-Fj1
      a1 = sqrt(dot_product(VA1,VA1))
      VA1 = VA1/a1
      VA2 = Fj3-Fj1
      a2 = sqrt(dot_product(VA2,VA2))
      VA2 = VA2/a2
      csa = dot_product(VA1,VA2)
      AreaN = 0.5d0*a1*a2*sqrt(1.d0-csa*csa)

      call vectprd(VA1,VA2,hn)
      hn = hn / sqrt(dot_product(hn,hn))
      H = abs(dot_product(Fj1-R0,hn))
      hn = (Fj1 - R0)/H

!c
!c       mesh generation
!c
      h0 = dv**(1.d0/3.d0)
      N = H/h0+1
      h0 = H/N

      if(.not.allocated(Zi)) then
        N0 = N
        allocate(Ri(0:N0),Zi(1:N0))
      else
       if(N.gt.N0) then
        deallocate(Ri,Zi)
        N0 = N
        allocate(Ri(0:N0),Zi(1:N0))
       end if
      end if

      Ri(0) = 0.d0
      do i=1,N
       Ri(i) = h0*i
      end do

      Zi(1) = 0.75d0*Ri(1)
      do i=2,N
       Zi(i) = 0.75d0*(Ri(i)**4-Ri(i-1)**4)/(Ri(i)**3-Ri(i-1)**3)
      end do

!CDEBUG        Zi(1) = h0/2
!CDEBUG        do i=2,N
!CDEBUG         Zi(i) = Zi(i-1)+h0
!CDEBUG        end do

!CDEBUG        Mtot = 0

      do i = 1,N
!c
!c  layer: R(i-1) < z < R(i) ;   Zi -- centroid of the 3-pyramid  (z-axis !== hn)
!c
       vi = (AreaN/(3.d0*H**2))*                                        &
     &             (Ri(i)**3-Ri(i-1)*Ri(i-1)*Ri(i-1))

       M = sqrt(vi/dV)+1         ! M**2 -- number of prisms in the layer

       Mp = M**2
!CDEBUG         Mtot = Mtot+Mp

       vi = vi/Mp                          ! volume of each prism

       h1 = (a1/H*Zi(i))/M
       h2 = (a2/H*Zi(i))/M

       do j=0,M-1

        do k=0,(M-1)-j
         q1 =     j*h1*VA1 +     k*h2*VA2       !
         q2 =     j*h1*VA1 + (k+1)*h2*VA2       !
         q3 = (j+1)*h1*VA1 +     k*h2*VA2       !

!C           Pm = Fj1+Zi(i)*hn + (q1+q2+q3)/3.d0
         Pm = R0 + Zi(i)*hn + (q1+q2+q3)/3.d0

         call momntint(Pm,Fi1,Fi2,Fi3,-1,cosds)

         tetrsum = tetrsum + cosds(1)*vi

        end do

        do k=(M-1)-j,1,-1
         q1 = (j+1)*h1*VA1 +     k*h2*VA2        !
         q2 = (j+1)*h1*VA1 + (k-1)*h2*VA2        !
         q3 =     j*h1*VA1 +     k*h2*VA2        !

         Pm = R0 + Zi(i)*hn + (q1+q2+q3)/3.d0

         call momntint(Pm,Fi1,Fi2,Fi3,-1,cosds)

         tetrsum = tetrsum + cosds(1)*vi

        end do
       end do
      end do

!CDEBUG        if(iprint.eq.1) then
!CDEBUG        write(*,*) ' Mtot=',Mtot
!CDEBUG        write(*,*) ' Volume=',Volume
!CDEBUG        write(*,*) ' CheckVolm=',CheckVolm
!CDEBUG        iprint = 0
!CDEBUG        end if

      return
      end

      subroutine ylmtint(R0,P1,P2,P3,nmom,lm1,lm2,                      &
     &                     L,M,epsShC,                                  &
     &                     yyre,yyim,iprint)
!c
!c        R0 -- origin of the tetrahedron
!c        (P1,P2,P3) -- triangle in 3d (base of the tetrahedron)
!c
!c        ylmylm2 --
!c         Real or Imag [ integral{over tetrahedron| Ylm1*conjg(Ylm2)}*Y_ !LM]
!c           icmplx.ne.1 -- to calculate Real part
!c           icmplx.eq.1 -- to calculate Imag part
!c
      implicit none

      real*8 R0(3)
      real*8 P1(3),P2(3),P3(3)

      integer nmom
      integer lm1,lm2,L,M
      real*8 epsShC
      complex*16 yyre(0:nmom)                !  YY*Re(Y)
      complex*16 yyim(0:nmom)                !  YY*Im(Y)

      integer iprint

      real*8 ylm1ylm2(0:nmom)
      real*8 VA1(3),VA2(3),VA3(3),P0(3)
      real*8 a1,a2,a3
      real*8 csa,csb,tga,tgb,a2x,h,p0x,p0y

      integer ndfun
      parameter (ndfun=21)
      real*8 funpar(ndfun)

      real*8 err0
      parameter (err0=1.d-8)

      integer ic,im,icim
      integer ierr,i,l1,l2,LM
      real*8 err,x0,x1,sum,pz

!c        real*8 cosds

      real*8 ylmintdy
      external ylmintdy

      real*8 zero
      parameter (zero=0.d0)

      integer nerr/10/
      save nerr

      if(nmom.lt.0) return

      VA1 = P3-P1
      VA2 = P2-P1
      VA3 = VA2-VA1                   ! P2-P3
      a1 = sqrt(dot_product(VA1,VA1))
      a2 = sqrt(dot_product(VA2,VA2))
      a3 = sqrt(dot_product(VA3,VA3))
      if(abs(a1-a2-a3).le.1.d-7*(a1+a2+a3)) return

      P0 = P1 - R0

      if(a2.gt.max(a1,a3)) then
       VA1 = -VA2                     ! P1-P2
       a1 = a2
       VA2 = -VA3                     ! P3-P2
       a2 = a3
       P0 = P2 - R0
       VA3 = VA2-VA1
       a3 = sqrt(dot_product(VA3,VA3))
      else if(a3.gt.max(a1,a2)) then
       VA2 = -VA1                     ! P1-P3
       a2 = a1
       VA1 = VA3                      ! P2-P3
       a1 = a3
       P0 = P3 - R0
       VA3 = VA2-VA1
       a3 = sqrt(dot_product(VA3,VA3))
      end if

      csa = (dot_product(VA1,VA2)/(a1*a2))
      tga = sqrt(1.d0-csa*csa)/csa
      csb = -(dot_product(VA1,VA3)/(a1*a3))
      tgb = sqrt(1.d0-csb*csb)/csb
      a2x = a2*csa                    ! projection a2-->a1

      VA1 = VA1/a1
      VA2 = VA2 - dot_product(VA2,VA1)*VA1
      VA2 = VA2/sqrt(dot_product(VA2,VA2))
      call vectprd(VA1,VA2,VA3)
      VA3 = VA3/sqrt(dot_product(VA3,VA3))

      h = dot_product(P0,VA3)

        h = abs(h)

      p0x = dot_product(P0,VA1)
      p0y = dot_product(P0,VA2)

       err = err0
       x0 = 0.d0
       x1 = a2x

       funpar(5:7) = P0(1:3)
       funpar(8:10) = VA1(1:3)
       funpar(11:13) = VA2(1:3)

       funpar(14) = lm1
       funpar(15) = lm2
       l1 = sqrt(dfloat(lm1))
       if(l1*l1.eq.lm1) l1 = l1-1
       funpar(16) = l1
       l2 = sqrt(dfloat(lm2))
       if(l2*l2.eq.lm2) l2 = l2-1
       funpar(17) = l2
       funpar(18) = max(l1,l2,L)

       yyre(0:nmom) = dcmplx(zero,zero)
       yyim(0:nmom) = dcmplx(zero,zero)

       do icim=0,3
!c
!c  im=0,ic=0 -- real(YY*)*real(Y) (0)
!c  im=0,ic=1 -- imag*real         (1)
!c  im=2,ic=0 -- real*imag         (2)
!c  im=2,ic=1 -- imag*imag         (3)

        funpar(19) = icim      ! to calculate real/imag part of <YYY>
        ic = mod(icim,2)
        im = icim-ic

        if(im.eq.0) then
         LM = L*(L+1) +M +1
!CDEBUG
         err = min(err0,epsShC/50.d0)
!CDEBUG
        else
         if(M.eq.0) cycle
         LM = L*(L+1) -M +1
         err = epsShC/50.d0
        end if
        funpar(20) = LM
!c
        x0 = 0.d0
        x1 = a2x
        funpar(1) = zero
        funpar(2) = zero
        funpar(3) = tga
        ylm1ylm2(0:nmom)  = zero
        do i=0,nmom
         funpar(4) = i+L
         call dgaus8m (ylmintdy,x0,x1,funpar,err,sum,ierr)
         if(abs(ierr).gt.1.and.nerr.gt.0.and.abs(sum).gt.err)           &
     &      then
          write(*,*) ' WARNING: I=',i                                   &
     &                 ,' (L,M)=',LM,' icim =',icim
          write(*,*) ' ierr=',ierr
          write(*,*) ' err=',err
          write(*,*) ' x0,x1:',x0,x1
          write(*,*) ' p0x,p0y:',p0x,p0y
          write(*,*) ' tga,tgb:',tga,tgb
          write(*,*) ' a2x,a1,h=',a2x,a1,h
          write(*,*) ' R0:',r0
          write(*,*) ' P1:',p1
          write(*,*) ' P2:',p2
          write(*,*) ' P3:',p3
          write(*,*) ' I=',i
          nerr = nerr - 1
          write(*,*)
!CDEBUG      stop ' momntint: integration error 1'
         end if

         ylm1ylm2(i) = ylm1ylm2(i) + sum

       end do

        err = err0
        x0 = a2x
        x1 = a1

        funpar(1) = zero
        funpar(2) = a1*tgb
        funpar(3) = -tgb

        do i=0,nmom
         funpar(4) = i+L
         call dgaus8m (ylmintdy,x0,x1,funpar,err,sum,ierr)
         if(abs(ierr).gt.1.and.nerr.gt.0.and.abs(sum).gt.err)           &
     &      then
          write(*,*) ' WARNING: I=',i                                   &
     &                 ,' (L,M)=',LM,' icim =',icim
          write(*,*) ' ierr=',ierr
          write(*,*) ' sum=',sum,' err=',err
          write(*,*) ' x0,x1:',x0,x1
          write(*,*) ' p0x,p0y:',p0x,p0y
          write(*,*) ' tga,tgb:',tga,tgb
          write(*,*) ' a2x,a1,h=',a2x,a1,h
          write(*,*) ' R0:',r0
          write(*,*) ' P1:',p1
          write(*,*) ' P2:',p2
          write(*,*) ' P3:',p3
          write(*,*) ' I=',i
          nerr = nerr-1
          write(*,*)

!CDEBUG      stop ' momntint: integration error 2'
         end if

         ylm1ylm2(i) = ylm1ylm2(i) + sum

        end do

        if(icim.eq.0) then                                 ! real*real
         yyre(0:nmom) = yyre(0:nmom) +                                  &
     &                         dcmplx(ylm1ylm2(0:nmom),zero)
        else if(icim.eq.1) then                            ! imag*real
         yyre(0:nmom) = yyre(0:nmom) +                                  &
     &                         dcmplx(zero,ylm1ylm2(0:nmom))
        else if(icim.eq.2) then                            ! real*imag
         yyim(0:nmom) = yyim(0:nmom) +                                  &
     &                         dcmplx(zero,ylm1ylm2(0:nmom))
        else if(icim.eq.3) then                            ! imag*imag
         yyim(0:nmom) = yyim(0:nmom) -                                  &
     &                         dcmplx(ylm1ylm2(0:nmom),zero)
        end if

       end do                 ! icim-do

       do i=L,L+nmom
        yyre(i-L) = yyre(i-L)*(h/(i+3))
        yyim(i-L) = yyim(i-L)*(h/(i+3))
       end do

      return
      end

      function ylmintdy(x,param)
      use universal_const
      implicit none
!      include 'lmax.h'
      real*8 x,ylmintdy
      real*8 param(21)

      integer ngauss
      parameter (ngauss=6)
!c
!c ngauss=6
!c
      real(8), parameter :: xg(ngauss) = [                              &
     &     -0.932469514203d0                                            &
     &,    -0.661209386466d0                                            &
     &,    -0.238619186083d0                                            &
     &,     0.238619186083d0                                            &
     &,     0.661209386466d0                                            &
     &,     0.932469514203d0                                            &
     &       ]
!c
      real(8), parameter :: wg(ngauss) = [                              &
     &      0.171324492379d0                                            &
     &,     0.360761573048d0                                            &
     &,     0.467913934573d0                                            &
     &,     0.467913934573d0                                            &
     &,     0.360761573048d0                                            &
     &,     0.171324492379d0                                            &
     &       ]
!c
!C      parameter (ngauss=8)
!C
!C      real*8 xg(ngauss),wg(ngauss)
!Cc
!Cc ngauss=8
!Cc
!C      data xg/
!C     ,    -0.960289856498d0
!C     ,,   -0.796666477414d0
!C     ,,   -0.525532409916d0
!C     ,,   -0.183434642496d0
!C     ,,    0.183434642496d0
!C     ,,    0.525532409916d0
!C     ,,    0.796666477414d0
!C     ,,    0.960289856498d0
!C     *       /
!C      data wg/
!C     ,     0.10122853629d0
!C     ,,    0.22238103445d0
!C     ,,    0.31370664588d0
!C     ,,    0.36268378338d0
!C     ,,    0.36268378338d0
!C     ,,    0.31370664588d0
!C     ,,    0.22238103445d0
!C     ,,    0.10122853629d0
!C     *       /

      integer n,ig
      real*8 R(3),Rx(3)
      real*8 c,d,y1,y2,ax,h,y,x2,func
!      real*8 zero,half,one
!      parameter (zero=0.d0,half=0.5d0,one=1.d0)
!      real*8 pi
!      parameter (pi=3.1415926535897932384d0)
      real(8), parameter :: half = one/two
      real(8), parameter :: fourpi=four*pi

      integer lmax,l1,l2,lm1,lm2
      integer LM,icim
      real*8 r2(1)
      complex(8), allocatable :: ylm(:)  ! (ipkkr)

      y1 = param(1)
      y2 = param(2) + param(3)*x

      n = nint(param(4))

      c= half*(y2-y1)
      d= half*(y2+y1)

      Rx(1:3) = param(5:7) + param(8:10)*x
!c
      lm1  = nint(param(14))
      lm2  = nint(param(15))
      l1   = nint(param(16))
      l2   = nint(param(17))
      lmax = nint(param(18))
      icim = nint(param(19))
      LM = nint(param(20))

      allocate(ylm(max(1,LM,(lmax+1)**2)))
      ylm = 0
!c         lmax = max(l1,l2)
!c         l1 = sqrt(dfloat(lm1))
!c         if(l1*l1.eq.lm1) l1 = l1-1
!c         l2 = sqrt(dfloat(lm2))
!c         if(l2*l2.eq.lm2) l2 = l2-1
!c
      ylmintdy = zero
!c
!c        ngauss-point gaussian integration
!c
      if(LM.eq.1) then
       if(mod(icim,2).eq.0) then             ! real part
      do ig=1,ngauss
        y=c*xg(ig)+d
        R(1:3) = Rx(1:3) + param(11:13)*y
        call hrmplnm(zero,zero,zero,                                    &
     &                 R, 1, r2, ylm, 1,                                &
     &                      -10, 1, lmax , -1)
        func = dreal(ylm(lm1)*conjg(ylm(lm2)))                          &
     &            *sqrt(dot_product(R,R))**n
        ylmintdy = ylmintdy + wg(ig)*func
      enddo
       else                                  ! imag part
      do ig=1,ngauss
        y=c*xg(ig)+d
        R(1:3) = Rx(1:3) + param(11:13)*y
        call hrmplnm(zero,zero,zero,                                    &
     &                 R, 1, r2, ylm, 1,                                &
     &                      -10, 1, lmax , -1)

         func = dimag(ylm(lm1)*conjg(ylm(lm2)))                         &
     &           *sqrt(dot_product(R,R))**n

        ylmintdy = ylmintdy + wg(ig)*func
      enddo
       end if
      else
       if(icim.eq.0) then             ! real*real part
      do ig=1,ngauss
        y=c*xg(ig)+d
        R(1:3) = Rx(1:3) + param(11:13)*y
        call hrmplnm(zero,zero,zero,                                    &
     &                 R, 1, r2, ylm, 1,                                &
     &                      -10, 1, lmax , -1)
        func = dreal(ylm(lm1)*conjg(ylm(lm2)))*dreal(ylm(LM))           &
     &            *sqrt(dot_product(R,R))**n
        ylmintdy = ylmintdy + wg(ig)*func
      enddo
       else if(icim.eq.1) then ! imaginary*real part
      do ig=1,ngauss
        y=c*xg(ig)+d
        R(1:3) = Rx(1:3) + param(11:13)*y
        call hrmplnm(zero,zero,zero,                                    &
     &                 R, 1, r2, ylm, 1,                                &
     &                      -10, 1, lmax , -1)
        func = dimag(ylm(lm1)*conjg(ylm(lm2)))*dreal(ylm(LM))           &
     &           *sqrt(dot_product(R,R))**n
        ylmintdy = ylmintdy + wg(ig)*func
      enddo
       else if(icim.eq.2) then             ! real*imag part
      do ig=1,ngauss
        y=c*xg(ig)+d
        R(1:3) = Rx(1:3) + param(11:13)*y
        call hrmplnm(zero,zero,zero,                                    &
     &                 R, 1, r2, ylm, 1,                                &
     &                      -10, 1, lmax , -1)
         func = dreal(ylm(lm1)*conjg(ylm(lm2)))*dimag(ylm(LM))          &
     &            *sqrt(dot_product(R,R))**n
        ylmintdy = ylmintdy + wg(ig)*func
      enddo
       else if(icim.eq.3) then !           imag*imag part
      do ig=1,ngauss
        y=c*xg(ig)+d
        R(1:3) = Rx(1:3) + param(11:13)*y
        call hrmplnm(zero,zero,zero,                                    &
     &                 R, 1, r2, ylm, 1,                                &
     &                      -10, 1, lmax , -1)
        func = dimag(ylm(lm1)*conjg(ylm(lm2)))*dimag(ylm(LM))           &
     &           *sqrt(dot_product(R,R))**n
        ylmintdy = ylmintdy + wg(ig)*func
      enddo
       end if
      end if
!c
      ylmintdy = ylmintdy*c
      deallocate(ylm)

      return
      end

      subroutine vrngeom1(i,                                            &
     &                    nnatom,xyznn,itype,rcut,                      &
     &                    Rcs,RcsSmtmax,                                &
     &                    planel,pntsarr,iplane,nface,mface,nmeqfc,     &
     &                    pointl,mpoint,npt,nvert,nRcsPnt,atmp)
      USE VPlist
      implicit none
      integer i,nnatom
      real*8 xyznn(3,nnatom)
      integer itype(nnatom)
      real*8 rcut
      real*8 Rcs
      real*8 RcsSmtMax
      integer npt,nface,nvert
      real*8 planel(3,*),pointl(3,npt,*),rnn(nnatom)
      real*8 pntsarr(3,*)
      integer nRcspnt
      integer iplane(*),mpoint(*)

      integer nmeqfc(*)    ! nmeqfc(mface(j)) -- weight (number) of face "j"
      integer mface(*)     ! # of "equivalent" face (pointer to "original" face)

      real*8 atmp(*)       ! tmp-array

      integer npll,nsubi,nsubj,ii,ij,jj,kj
      integer ipn,jpn,kpn,j

      real*8 Smti,Smtj,rcut2,delta
      real(8) :: Smti_(1),Smtj_(1)
      equivalence (Smti,Smti_(1))
      equivalence (Smtj,Smtj_(1))

      real*8 a(3,3),d(3),rp(3)
      integer ipiv(3),info

      real*8 a1,b1,c1,v1,d1
      real*8 a2,b2,c2,v2,d2
      real*8 a3,b3,c3,v3,d3
      real*8 p1,p2,p3,pa2,pa3,pb1,pb3,pc1,pc2
      real*8 det,det1,det2,det3,ddet
      real*8 xp,yp,zp
      real*8 epspnts

      logical bad
        real*8 rij2,xi,yi,zi
      real*8 rji(3),rsji
      integer jm,l
      logical cmpplgn
      external cmpplgn

      real*8 epsdet,epscs,eps
      parameter (epsdet=1.d-14,epscs=1.d-10,eps=1.d-10)

      character*10 sname
      parameter (sname='vrngeom1')

       nsubi = itype(i)

       rji(1:3) = xyznn(1:3,i)
       rsji = sqrt(dot_product(rji,rji))
       if(rsji.gt.epscs) then
        rji(1:3) = rji(1:3)/rsji
       else
        rji(1:2) = zero
        rji(3)   = one
        rsji     = zero
       end if
       vpsubs(nsubi)%enn(1:3,0) = rji(1:3)
       vpsubs(nsubi)%Rij(0)     = rsji
       vpsubs(nsubi)%nntype(0)  = nsubi

       call SmtModule(0,nsubi,nsubi,Smti_)
       epspnts = eps*Smti
       rcut2 = rcut**2
       npll=0
       do ii=1,nnatom
        if(ii.eq.i) cycle
        rji(1:3) = xyznn(1:3,ii)-xyznn(1:3,i)
        rij2 = dot_product(rji,rji)
        if(rij2.lt.rcut2) then
         npll=npll+1
         iplane(npll) = ii
         rsji = sqrt(rij2)
         rnn(npll) = rsji
         planel(1:3,npll) = rji(1:3)/rsji
         nsubj = itype(ii)
         call SmtModule(0,nsubj,nsubj,Smtj_)
         delta = rsji - (Smti+Smtj)

         if(delta.lt.-1.d-10*(Smti+Smtj)) then
          write(*,*) ' nsubi=',nsubi,' Smti=',Smti
          write(*,*) ' xyznn(i)=',xyznn(1:3,i)
          write(*,*) ' nsubj=',nsubj,' Smtj=',Smtj
          write(*,*) ' xyznn(ii)=',xyznn(1:3,ii)
          write(*,*) ' rsji=',rsji
          call fstop(sname//': ERROR :: wrong Smt, Smti+Smtj>rji')
         end if

         if(max(Smti,Smtj).le.rsji/2) then
          delta = rsji/2
         else
          if(Smti.gt.Smtj) then
           delta = Smti
          else
           delta = rsji - Smtj
          end if
         end if
         pntsarr(1:3,npll) = xyznn(1:3,i) +                             &
     &                         delta*planel(1:3,npll)
        end if
       end do
         xi = xyznn(1,i)
         yi = xyznn(2,i)
         zi = xyznn(3,i)

       pointl(1:3,1:npt,1:npll)=zero
       mpoint(1:npll) = 0

       nvert = 0
       do ij=1,npll
        p1 = dot_product(planel(1:3,ij),pntsarr(1:3,ij))
        do jj=ij+1,npll
         p2 = dot_product(planel(1:3,jj),pntsarr(1:3,jj))
         do kj=jj+1,npll
          a(1,1:3) = planel(1:3,ij)
          a(2,1:3) = planel(1:3,jj)
          a(3,1:3) = planel(1:3,kj)
          call dgetrf(3,3,a,3,ipiv,info)

          if(info.eq.0) then
           d(1) = p1
           d(2) = p2
           d(3) = dot_product(planel(1:3,kj),pntsarr(1:3,kj))
           call dgetrs('N',3,1,a,3,ipiv,d,3,info)

!CDEBUG             if(maxval(abs(d(1:3))).gt.rcut) then
!CDEBUG              bad = .true.
!CDEBUG              else
            rp(1:3) = d(1:3)
!c              xp = d(1)
!c              yp = d(2)
!c              zp = d(3)
            bad = .false.
            do l=1,npll
             if(l.ne.ij.and.l.ne.jj.and.l.ne.kj) then
           d(1:3) = rp(1:3) - pntsarr(1:3,l)
           ddet = (dot_product(planel(1:3,l),d(1:3)))
           bad = ddet.gt.epsdet
           if(bad) exit
             end if
            end do
!CDEBUG              end if
             if(.not.bad) then

            ipn=1
            do l=1,mpoint(ij)
             ddet = maxval(abs(rp(1:3)-pointl(1:3,l,ij)))
             if(ddet.le.epspnts) then
           ipn=0
           exit
             end if
            end do
            if(ipn.ne.0) then
             if(mpoint(ij).ge.npt) then
           write(*,*) ' MPOINT(',ij,')=',mpoint(ij)
           write(*,*) ' NPT           =',npt
           write(*,*)
           call fstop(sname//': mpoint.gt.npt (1)')
             end if
             mpoint(ij) = mpoint(ij)+1

             pointl(1:3,mpoint(ij),ij) = rp(1:3)
              end if

            jpn=1
            do l=1,mpoint(jj)
             ddet = maxval(abs(rp(1:3)-pointl(1:3,l,jj)))
             if(ddet.le.epspnts) then
           jpn=0
           exit
             end if
            end do
              if(jpn.ne.0) then
             if(mpoint(ij).ge.npt) then
           write(*,*) ' MPOINT(',ij,')=',mpoint(ij)
           write(*,*) ' NPT           =',npt
           write(*,*)
           call fstop(sname//'mpoint.gt.npt (2)')
             end if
               mpoint(jj) = mpoint(jj)+1
             pointl(1:3,mpoint(jj),jj) = rp(1:3)
              end if

            kpn=1
            do l=1,mpoint(kj)
             ddet = maxval(abs(rp(1:3)-pointl(1:3,l,kj)))
             if(ddet.le.epspnts) then
           kpn=0
           exit
             end if
            end do
              if(kpn.ne.0) then
             if(mpoint(kj).ge.npt) then
           write(*,*) ' MPOINT(',kj,')=',mpoint(kj)
           write(*,*) ' NPT           =',npt
           write(*,*)
           call fstop(sname//'mpoint.gt.npt (3)')
             end if
             mpoint(kj) = mpoint(kj)+1
             pointl(1:3,mpoint(kj),kj) = rp(1:3)
              end if
            if(min(ipn,jpn,kpn).ne.0)  then
             nvert=nvert+1
             vpsubs(nsubi)%vertex(1:3,nvert) =                          &
     &            rp(1:3) - xyznn(1:3,i)   ! VP-vertex
            end if
           end if

          end if              ! end if(det.ne.0)

         end do
        end do
       end do

       if(nvert.le.0) then
        write(*,*) ' NVERT=',NVERT
        write(*,*) ' IPN/JPN/KPN=',ipn,jpn,kpn
        write(*,*) ' NPLL=',npll
        call fstop(sname//': NVERT < 1')
       end if

       mface(1:npll) = mpoint(1:npll)
       nface = 0
       do j=1,npll
          if(mpoint(j).gt.2) then
!            if ( mface(j) == 3 ) then
!             call CalcVol(mface(j),pointl(1:3,1:mface(j),j),ddet)
!             if(abs(ddet).lt.epspnts**2*Smti) cycle
!            end if    
            nface = nface + 1
            planel(1:3,nface) = planel(1:3,j)
            rnn(nface)    = rnn(j)
            iplane(nface) = iplane(j)
            pointl(1:3,1:mface(j),nface) = pointl(1:3,1:mface(j),j)
            mpoint(nface) = mface(j)
          end if
       end do

       Rcs = zero
       nRcspnt = 0
       do j=1,nface
        do l = 1,mpoint(j)
         rji(1:3)=pointl(1:3,l,j)-xyznn(1:3,i)
         rsji = sqrt(dot_product(rji,rji))
         if(rsji-Rcs.gt.-epscs) then
          if(rsji-Rcs.gt.epscs) then
            Rcs = max(Rcs,rsji)  ! radius of circumscribed sphere
            if(Rcs/Smti.gt.RcsSmtMax) then
             nRcspnt = 1
             pntsarr(1:3,nRcspnt) = pointl(1:3,l,j)
            end if
          else
           ipn = nRcspnt
           do kj=1,nRcspnt
            ddet = maxval(abs(pointl(1:3,l,j)-pntsarr(1:3,kj)))
            if(ddet.le.epspnts) then
             ipn=0
             exit
            end if
           end do
           if(ipn.gt.0) then
            Rcs = Rcs*nRcspnt + rsji
            nRcspnt = nRcspnt+1
            Rcs = Rcs/nRcspnt
            pntsarr(1:3,nRcspnt) = pointl(1:3,l,j)
           end if
          end if
         end if
         pointl(1:3,l,j) = rji(1:3)   ! now VP is defined
!c                                       ! relative to xyznn(*,i)
        end do
       end do
       if(RcsSmtMax.lt.zero) nRcspnt = 0

       call faceorder(mpoint(1),pointl(1,1,1),                          &
     &                   planel(1,1))

       j = 1
       mface(j) = 1
       mface(2:nface) = 0
       nmeqfc(mface(j)) = 1
       nmeqfc(2:nface) = 0

       do j=2,nface
        call faceorder(mpoint(j),pointl(1,1,j),                         &
     &                   planel(1,j))
!c
        do l=1,j-1
         if(nmeqfc(mface(l)).gt.0) then
          if(cmpplgn(mpoint(mface(l)),pointl(1,1,mface(l)),             &
     &                mpoint(j),pointl(1,1,j),atmp)) then

           nmeqfc(l) = nmeqfc(l)+1
           mface(j) = l
           exit
          end if
         end if
        end do

        if(mface(j).eq.0) then
         mface(j) = j
         nmeqfc(j) = 1
        end if

       end do

       do j=1,nface
        vpsubs(nsubi)%nfvtx(j)  = mpoint(j)       ! number of face-vertices
        vpsubs(nsubi)%facevtx(1:3,1:mpoint(j),j) =                      &
     &               pointl(1:3,1:mpoint(j),j)      ! face-vertices coordnts
        vpsubs(nsubi)%enn(1:3,j) = planel(1:3,j)  ! normal to a face pointing to NN
        vpsubs(nsubi)%Rij(j)     = rnn(j)         ! VP-NN distance
        vpsubs(nsubi)%nntype(j)  =                                      &
     &                             itype(iplane(j)) ! NN sublattice (type)
       end do
       vpsubs(nsubi)%nnvp = nface                 ! number of faces (VP-NN)
       vpsubs(nsubi)%nvtx = nvert                 ! number of VP-vertices
       vpsubs(nsubi)%Rcs = Rcs                    ! circumscribed  sph.rad.

       vpsubs(nsubi)%Ris = Rcs
       do j=1,nface
        call pnts2plane(                                                &
     &                    pointl(1,1,j)                                 &
     &,                   pointl(1,2,j)                                 &
     &,                   pointl(1,3,j)                                 &
     &,                   d                                             &
     &,                   d3                                            &
     &                   )
!c
!c      d(1:3)*Pi(1:3) + d3 = 0, |d|=1,  in particular, for Pi=P1,P2,P3
!c
        d3 = abs(d3)
        vpsubs(nsubi)%Dij(j) = d3                     ! distance between
!c                                                       ! a face and VP-center
!c
        vpsubs(nsubi)%Ris = min(vpsubs(nsubi)%Ris,d3) ! inscribed sph.radius
       end do

!CDEBUG
       vpsubs(nsubi)%Rcs = zero
       do j=1,nface
        do l = 1,mpoint(j)
         rji(1:3) = vpsubs(nsubi)%facevtx(1:3,l,j)
         rsji = sqrt(dot_product(rji,rji))
         vpsubs(nsubi)%Rcs =                                            &
     &          max(vpsubs(nsubi)%Rcs,rsji)  ! radius of circumscribed sphere
        end do
       end do
       if(abs(Rcs-vpsubs(nsubi)%Rcs).gt.1.d-14) then
        write(*,*) ' ERROR in vrngeom? '
        write(*,*) '  nsubi=',nsubi
        write(*,*) '  Rcs=',Rcs
        write(*,*) '  vpsubs(nsubi)%Rcs=',vpsubs(nsubi)%Rcs
        call fstop(sname//': Rcs-ERROR ????????')
       end if
!CDEBUG

       return
       end

       subroutine CheckEqP(P1,P2,delta,rslatt,P3,nijk,check)
       implicit none
       real*8 P1(3),P2(3)
       real*8 delta
       real*8 rslatt(3,3)
       real*8 P3(1:3)
       integer nijk(3)
       logical check


       real*8 a(3,3)
       real*8 c(3)
       integer i,info

       c(1:3) = P1(1:3)-P2(1:3)

       if(sqrt(dot_product(c(1:3),c(1:3))).lt.delta) then
        check  = .true.
        P3(1:3) = P2(1:3)
        nijk(1:3) = 0
        return
       end if

       do i=1,3
        a(i,i) = dot_product(rslatt(1:3,i),rslatt(1:3,i))
        P3(i)   = dot_product(c(1:3),rslatt(1:3,i))
       end do
       a(1,2) = dot_product(rslatt(1:3,1),rslatt(1:3,2))
       a(2,1) = a(1,2)
       a(2,3) = dot_product(rslatt(1:3,2),rslatt(1:3,3))
       a(3,2) = a(2,3)
       a(3,1) = dot_product(rslatt(1:3,3),rslatt(1:3,1))
       a(1,3) = a(3,1)
       call dgetrf(3,3,a,3,nijk,info)
       if(info.ne.0) then
        write(*,*)
        write(*,*) ' INFO=',info,' after DGETRF in CheckEqP'
        call fstop(' troubles in CheckEqP')
       end if
       call dgetrs('N',3,1,a,3,nijk,P3,3,info)
       if(info.ne.0) then
        write(*,*)
        write(*,*) ' INFO=',info,' after DGETRS in CheckEqP'
        call fstop(' troubles in CheckEqP')
       end if

       nijk(1:3) = nint(P3(1:3))

       do i=1,3
        P3(i) = P2(i) + nijk(1)*rslatt(i,1) +                           &
     &                    nijk(2)*rslatt(i,2) +                         &
     &                    nijk(3)*rslatt(i,3)
       end do

       c(1:3) = P1(1:3)-P3(1:3)

       if(sqrt(dot_product(c(1:3),c(1:3))).lt.delta) then
        check = .true.
        return
       end if

       check  = .false.

       return
       end

       subroutine CheckPnts(rslatt,delta,Npnts,points,Neqp)
       implicit none
       real*8 rslatt(3,3)
       real*8 delta
       integer Npnts
       real*8 points(3,1+Npnts)
       integer Neqp

       real*8 P0(3),P1(3)
       integer j,nj,i,ii
       integer nijk(3)
       logical check

       Neqp = Npnts
       j = 1
10     continue
        if(j.gt.Neqp) return
        nj = 1
        P0(1:3) = points(1:3,j)
20      continue
        do i=j+1,Neqp
         call CheckEqP(P0,points(1,i),delta,rslatt,P1,nijk,check)
         if(check) then
          P0 = P0*nj + P1
          nj = nj+1
          P0 = P0/nj
          do ii=i,Neqp-1
           points(1:3,ii) = points(1:3,ii+1)
          end do
          Neqp = Neqp-1
          go to 20
         end if
        end do
        points(1:3,j) = P0(1:3)
        j = j+1
       go to 10

       return
       end

