!BOP
!!ROUTINE: jacobian
!!INTERFACE:
      subroutine jacobian(sphR,Xi,Eta,Zeta,s1234,s567,alpha,rX1,vj)
!!DESCRIPTION:
! computes 3d jacobian {\tt vj}({\tt rX1}) for isoparametric integration;
! ( {\tt rX1} = rX1({\tt Xi,Eta,Zeta}) ) 
!

!!DO_NOT_PRINT
      implicit none
      integer, parameter :: DBL_R = selected_real_kind(14)
!!DO_NOT_PRINT

!!ARGUMENTS:
      real(DBL_R), intent(in) :: sphR         ! radius of the sphere 
      real(DBL_R), intent(in) :: Xi,Eta,Zeta  ! coordinates in the cube
      real(DBL_R), intent(in) :: alpha(0:7,3) 
      real(DBL_R), intent(in) :: s1234(4),s567(4)
      real(DBL_R), intent(out) :: rX1(3)
      real(DBL_R), intent(out) :: vj     ! jacobian
!!REVISION HISTORY:
! Initial version - Aftab Alam - 2010
! Adapted - A.S. - 2013
!EOP
!
!BOC
      real(DBL_R), parameter :: epszero = epsilon(1._DBL_R)
      real(DBL_R), parameter :: pi=3.1415926535897932385d0
      real(DBL_R) rX2(3),r1,r2
      real(DBL_R) vj1,vj2,vj3,dir(3),vl1,vl2
      real(DBL_R) Xi_vector(8)
!      real(DBL_R) theta1,phi1
      real(DBL_R) theta2,phi2
      real(DBL_R) al1467,be1467,ga1467,al2457,be2457,ga2457
      real(DBL_R) al3567,be3567,ga3567

!      Xi=abscissas(i)
!      Eta=abscissas(j)
!      Zeta=abscissas(k)

!---------------------------------------
!---[get coordinate rX2]----------------
      Xi_vector(1)=1._DBL_R
      Xi_vector(2)=Xi
      Xi_vector(3)=Eta
      Xi_vector(4)=Zeta
      Xi_vector(5)=Xi*Eta
      Xi_vector(6)=Eta*Zeta
      Xi_vector(7)=Zeta*Xi
      Xi_vector(8)=Xi*Eta*Zeta

      call dMRRRR(1,8,Xi_vector,1,8,3,alpha,8,1,3,rX2,1)

      if( sphR > epszero)then
!---[get coordinate r2]----------------
        r2=dsqrt(rX2(1)**2+rX2(2)**2+rX2(3)**2)
        if(dabs(rX2(1))<epszero .and. dabs(rX2(2))<epszero)then
         !x=y=0 ==> theta2=phi2=0.0 ==> dir=(0,0,+1) or (0,0,-1)
          dir(3)=1._DBL_R
          if(rX2(3)<0._DBL_R)dir(3)=-1._DBL_R
          vl1=-s567(4)/(s567(3)*dir(3))
          vl2=-s1234(4)/(s1234(3)*dir(3))
          r1=(vl2*(sphR-vl1)+r2*(vl2-sphR))/(vl2-vl1)
          vJ1=r1**2/r2**2
          rX1(1:2)=0._DBL_R
          rX1(3)=r1*dir(3)
        else   ! rX2(1) .ne. 0 .or. rX2(2) .ne. 0 
         if(dabs(rX2(1))<epszero .and. rX2(2)>epszero)then !x=0, y>0
          theta2=dacos(rX2(3)/r2)
          phi2=pi/2._DBL_R
         else if(dabs(rX2(1))<epszero .and. rX2(2)<epszero)then !x=0, y<0
          theta2=dacos(rX2(3)/r2)
          phi2=pi*3._DBL_R/2._DBL_R
         else
          theta2=dacos(rX2(3)/r2)
          if(rX2(1)>0._DBL_R)then !x>0, normal
           phi2=datan(rX2(2)/rX2(1))
          else if(rX2(1)<0._DBL_R)then !x<0, +pi
           phi2=datan(rX2(2)/rX2(1))+pi
          end if
         end if
!---[get coordinate r1]----------------
         dir(1)=dsin(theta2)*dcos(phi2)
         dir(2)=dsin(theta2)*dsin(phi2)
         dir(3)=dcos(theta2) !direction vector
      !r=dsqrt(X1(5)**2+Y1(5)**2+Z1(5)**2) !radius of inner sphere

         vl1=-s567(4)/(s567(1)*dir(1)+s567(2)*dir(2)+s567(3)*dir(3))
         vl2=-s1234(4)/(s1234(1)*dir(1)+s1234(2)*dir(2)+s1234(3)*dir(3))
         r1=(vl2*(sphR-vl1)+r2*(vl2-sphR))/(vl2-vl1)

!---[get coordinate rX1]----------------
         rX1(1)=r1*dir(1)
         rX1(2)=r1*dir(2)
         rX1(3)=r1*dir(3) !for calculate function value

      !call dwrrrl('rX1',3,1,rX1,3,0,'(f20.16)','number','number')

!---------------------------------------
!---------------------------------------
         vJ1=r1**2/r2**2
        end if
        vJ2=(vl2-sphR)/(vl2-vl1)
      else        ! sphR == 0, skip Transformation(2)
        vJ1=1._DBL_R
        vJ2=1._DBL_R
        rX1=rX2
      end if

      !because the notation, we set the row of alpha from 0-7;==> alpha(0:7,3) not alpha(8,3)
      al1467=alpha(1,1)+alpha(4,1)*eta+alpha(6,1)*zeta+                 &
     &       alpha(7,1)*eta*zeta
      be1467=alpha(1,2)+alpha(4,2)*eta+alpha(6,2)*zeta+                 &
     &       alpha(7,2)*eta*zeta
      ga1467=alpha(1,3)+alpha(4,3)*eta+alpha(6,3)*zeta+                 &
     &       alpha(7,3)*eta*zeta

      al2457=alpha(2,1)+alpha(4,1)*xi+alpha(5,1)*zeta+alpha(7,1)*xi*zeta
      be2457=alpha(2,2)+alpha(4,2)*xi+alpha(5,2)*zeta+alpha(7,2)*xi*zeta
      ga2457=alpha(2,3)+alpha(4,3)*xi+alpha(5,3)*zeta+alpha(7,3)*xi*zeta

      al3567=alpha(3,1)+alpha(5,1)*eta+alpha(6,1)*xi+alpha(7,1)*eta*xi
      be3567=alpha(3,2)+alpha(5,2)*eta+alpha(6,2)*xi+alpha(7,2)*eta*xi
      ga3567=alpha(3,3)+alpha(5,3)*eta+alpha(6,3)*xi+alpha(7,3)*eta*xi

      vJ3=(al1467*be2457*ga3567+al2457*be3567*ga1467+                   &
     &     al3567*be1467*ga2457)-(al3567*be2457*ga1467+                 &
     &     al2457*be1467*ga3567+al1467*be3567*ga2457)

      vJ=vJ1*vJ2*vJ3
      if ( vJ<epszero ) then
       if ( vJ < 1._DBL_R-14 ) then
        write(6,'(3e12.3,1x,3f8.5,1x,a)') vJ1,vj2,vj3,Xi,Eta,Zeta,      &
     &                                    ' NEGATIVE JACOBIAN!'

       else
        vJ = 0._DBL_R
       end if
      end if    
      return
!EOC
      end subroutine jacobian
