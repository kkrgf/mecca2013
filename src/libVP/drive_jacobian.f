!BOP
!!ROUTINE: drive_jacobian
!!INTERFACE:
      subroutine drive_jacobian(MNV,sphR,vertices,vf,                   &
     &                          s1234,s567,alpha,wholeVP,istop)
!!DESCRIPTION:
! generates data for further jacobian calculation
!

!!DO_NOT_PRINT
      implicit none
!!DO_NOT_PRINT

!!ARGUMENTS:
      integer, intent(in) :: MNV
      real(8), intent(in) :: sphR
      real(8), intent(in) :: vertices(3,MNV)
      integer, intent(in) :: vf(4)
      real(8), intent(out) :: s1234(4),s567(4) 
      real(8), intent(out) :: alpha(8,3)
      logical, intent(in) ::  wholeVP
      character(10), intent(in) :: istop
!!REVISION HISTORY:
! Adapted - A.S. - 2013
!EOP
!
!BOC
!      integer iRmt,iDis,pwr1
      real(8) :: X1(8),Y1(8),Z1(8)
      real*8 X2(8),Y2(8),Z2(8)
      real*8 R1(4),L_scale(4)
      real*8 XYZ2(8,3)
        character sname*10
        parameter (sname='drive_jcbn')

      real(8), parameter :: T1(8,8) = reshape([                         &
     &        1.0D0, 1.0D0,-1.0D0, 1.0D0,  -1.0D0,-1.0D0, 1.0D0,-1.0D0, &
     &        1.0D0,-1.0D0,-1.0D0, 1.0D0,   1.0D0,-1.0D0,-1.0D0, 1.0D0, &
     &        1.0D0,-1.0D0, 1.0D0, 1.0D0,  -1.0D0, 1.0D0,-1.0D0,-1.0D0, &
     &        1.0D0, 1.0D0, 1.0D0, 1.0D0,   1.0D0, 1.0D0, 1.0D0, 1.0D0, &
     &        1.0D0, 1.0D0,-1.0D0,-1.0D0,  -1.0D0, 1.0D0,-1.0D0, 1.0D0, &
     &        1.0D0,-1.0D0,-1.0D0,-1.0D0,   1.0D0, 1.0D0, 1.0D0,-1.0D0, &
     &        1.0D0,-1.0D0, 1.0D0,-1.0D0,  -1.0D0,-1.0D0, 1.0D0, 1.0D0, &
     &        1.0D0, 1.0D0, 1.0D0,-1.0D0,   1.0D0,-1.0D0,-1.0D0,-1.0D0  &
     &                ] / 8.d0,[8,8])

      R1(1)=sqrt(dot_product(vertices(1:3,vf(1)),vertices(1:3,vf(1))))
      R1(2)=sqrt(dot_product(vertices(1:3,vf(1)),vertices(1:3,vf(1))))
      R1(3)=sqrt(dot_product(vertices(1:3,vf(1)),vertices(1:3,vf(1))))
      R1(4)=sqrt(dot_product(vertices(1:3,vf(1)),vertices(1:3,vf(1))))

      X1(1)=vertices(1,vf(1))
      X1(2)=vertices(1,vf(2))
      X1(3)=vertices(1,vf(3))
      X1(4)=vertices(1,vf(4))

      Y1(1)=vertices(2,vf(1))
      Y1(2)=vertices(2,vf(2))
      Y1(3)=vertices(2,vf(3))
      Y1(4)=vertices(2,vf(4))

      Z1(1)=vertices(3,vf(1))
      Z1(2)=vertices(3,vf(2))
      Z1(3)=vertices(3,vf(3))
      Z1(4)=vertices(3,vf(4))

      L_scale(1:4) = sphR/R1(1:4)

      X1(5:8) = L_scale(1:4)*X1(1:4)    ! projection on sphere with radius sphR
      Y1(5:8) = L_scale(1:4)*Y1(1:4)    ! 
      Z1(5:8) = L_scale(1:4)*Z1(1:4)    ! 

!--------[generate the coefficient of plane equation]--------

      call pnts2plane(vertices(1:3,vf(1)),vertices(1:3,vf(2)),          &
     &                         vertices(1:3,vf(3)),s1234(1:3),s1234(4))

      call pnts2plane(vertices(1:3,vf(1))*L_scale(1),                   &
     &                 vertices(1:3,vf(2))*L_scale(2),                  &
     &                 vertices(1:3,vf(3))*L_scale(3),s567(1:3),s567(4))

!-----[check the muffin]
!CDEBUG
!      distance=-s1234(4)/dsqrt(dot_product(s1234(1:3),s1234(1:3)))
!        distance=dabs(distance)
!        pwr1=6
!        shift=DFLOAT(10**pwr1)
!        iRmt=int(r(nAtom)*shift)
!        iDis=int(distance*shift)
!C       if(iRmt>iDis) stop 'muffin-tin radius exceed the VP boundary!'
!        !If lattice>>1, we might need to decrease 'pwr1' to save digits
!        !for integer part.
!---[Generate X2]-----------------------
      X2=X1
      Y2=Y1
      Z2=Z1
      !if(r(nAtom)==0.0D0)then
      if(wholeVP)then
       goto 210 ! skip the modify
      end if
!???      t=-s567(4)/(s567(1)*X1(8)+s567(2)*Y1(8)+s567(3)*Z1(8))      !   <==== t is always 1
!???      !assume t[X1(8),Y1(8),Z1(8)] is the solution for ax+by+cz+d=0
!???      !ps: t[X1(8),Y1(8),Z1(8)] --> [X2(8),Y2(8),Z2(8)] which is what we need
!???      !-->t[a*X1(8)+b*Y1(8)+c*Z1(8)]+d=0
!???      !so t=-d/[a*X1(8)+b*Y1(8)+c*Z1(8)]
!???      X2(8)=t*X2(8)    ! we choose point-5, -6 and -7 to forming a plane, so,
!???      Y2(8)=t*Y2(8)    ! point 8 may not be on plane 567, thus we modify point 8
!???      Z2(8)=t*Z2(8)
  210 continue

      XYZ2(:,1)=X2
      XYZ2(:,2)=Y2
      XYZ2(:,3)=Z2
      !call dwrrrn('XYZ2=',8,3,XYZ2,8,0)
      !call dwrrrl('XYZ2=',8,3,XYZ2,8,0,'(f20.16)','number','number')

      call dMRRRR(8,8,T1,8,8,3,XYZ2,8,8,3,alpha,8) !attain the coefficients
      !call dwrrrl('alpha=',8,3,alpha,8,0,'(f20.16)','number','number')

       if (istop.eq.sname) then
         call fstop(sname)
       endif

      return
!EOC
      end subroutine drive_jacobian
