!BOP
!!ROUTINE: dMRRRR
!!INTERFACE:
       SUBROUTINE dMRRRR(NRA,NCA,A,LDA,NRB,NCB,B,LDB,                   &
     &                   NRC,NCC,C,LDC)
!!DESCRIPTION:
! $  C_{ij} = \sum\limits_{k} \( A_{ik} B_{kj}  \) $
!
!!REVISION HISTORY:
! Adapted - A.S. - 2013
!EOP
!
!BOC
       implicit none
       integer, intent(in) :: NRA,NCA,LDA,NRB,NCB,LDB,NRC,NCC,LDC
       real*8,  intent(in) :: A(LDA,*),B(LDB,*)
       real*8, intent(out) :: C(LDC,*)
       integer I, J
       ! integer K
       ! real*8 SM
       DO I=1,NRA
          DO J=1,NCB
          !   SM=0D0
          !   DO K=1,NCA
          !      SM=SM+A(I,K)*B(K,J)
          !   END DO
          !   C(I,J) = SM
             C(I,J) = dot_product(A(I,1:NCA),B(1:NCA,J))
          END DO
       END DO
       RETURN
!EOC
       END SUBROUTINE dMRRRR
