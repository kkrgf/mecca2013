!BOP
!!MODULE: bernal_vp
!!INTERFACE:
      module bernal_vp
!!DESCRIPTION:
! Bernal's VP tesselation library
! \\
! {\bf REF?}
!

!!DO_NOT_PRINT
               PRIVATE
!!DO_NOT_PRINT

!!PARAMETERS:
!! Fortran 77 word lengths
      integer*8, parameter :: mhalf_I=32768 !2**(16-1)
      integer*8, parameter :: mfull_I=1073741824 !2**(32-2)
!!PRIVATE MEMBER FUNCTIONS:
!  80 subroutines
!!PUBLIC DATA MEMBERS:
      integer, parameter :: npts_per_site=2000
!DELETE      PUBLIC :: nmax,nhmax,nfmax,nvmax
      integer, parameter :: pnts_wght_un = 11
      integer, parameter :: tetrahedr_un = 12
      integer, parameter :: power_vrt_un = 13
      integer, parameter :: areas_vol_un = 14
      PUBLIC :: pnts_wght_un,tetrahedr_un,power_vrt_un,areas_vol_un
!!PUBLIC MEMBER FUNCTIONS:
!! subroutines
      PUBLIC :: alloc_berncom
      PUBLIC :: drive_regtet
      PUBLIC :: drive_pwrvtx
      PUBLIC :: drive_volare
      PUBLIC :: delete_bernal_vp_files
!!REVISION HISTORY:
! Adapted - A.S. - 2013
!EOP
!
!BOC

!EOC
      integer, parameter :: tetra_unbd_un = 16
      integer*8  :: nmax
      integer*8  :: nfmax
      integer*8  :: nvmax
!  /berncom1/
      real(8), allocatable :: x(:), y(:), z(:), w(:)  ! nmax
!      PUBLIC :: x,y,z,w
!  /berncom2/
      integer(8), allocatable :: ix(:), iy(:), iz(:), iw(:)  ! nmax
!      PUBLIC :: ix,iy,iz,iw
!  /berncom3/
      integer(8), allocatable :: ix2(:),iy2(:),iz2(:),iw2(:)  ! nmax
!      PUBLIC :: ix2,iy2,iz2,iw2
!  /berncom4/
      integer(8), allocatable :: icon(:,:)      ! (8,nvmax)
      integer(8), allocatable :: ifl(:), id(:)  ! nvmax
      integer(8), allocatable :: is(:)          ! nmax
!      PUBLIC :: icon,ifl,id,is
!  /berncom5/
      real(8), allocatable :: xp(:), yp(:), zp(:) ! nvmax
!      PUBLIC :: xp,yp,zp
!  /berncom6/
      integer(8), allocatable :: itl(:), ifn(:) ! nmax
      integer(8), allocatable :: ifc(:) ! nfmax
!      PUBLIC :: itl,ifn,ifc
!  /berncom7/
      real(8), allocatable :: vol(:)            ! nmax
      real(8), allocatable :: area(:), ndst(:)  ! nfmax
!      PUBLIC :: vol,area,ndst
      CONTAINS

!BOP
!!IROUTINE: allocate_berncom
!!INTERFACE:
      subroutine alloc_berncom(nsite)
!!DESCRIPTION:
! allocates/deallocates working arrays

!!DO_NOT_PRINT
      implicit none
!!DO_NOT_PRINT

!!ARGUMENTS:
      integer, intent(in) :: nsite
!
!!REVISION HISTORY:
! Initial version - A.S. - 2017
!EOP
!
!BOC
      if ( allocated(x) ) deallocate(x)
      if ( allocated(y) ) deallocate(y)
      if ( allocated(z) ) deallocate(z)
      if ( allocated(w) ) deallocate(w)
      if ( allocated(ix) ) deallocate(ix)
      if ( allocated(iy) ) deallocate(iy)
      if ( allocated(iz) ) deallocate(iz)
      if ( allocated(iw) ) deallocate(iw)
      if ( allocated(ix2) ) deallocate(ix2)
      if ( allocated(iy2) ) deallocate(iy2)
      if ( allocated(iz2) ) deallocate(iz2)
      if ( allocated(iw2) ) deallocate(iw2)
      if ( allocated(is) ) deallocate(is)
      if ( allocated(icon) ) deallocate(icon)
      if ( allocated(ifl) ) deallocate(ifl)
      if ( allocated(id) ) deallocate(id)
      if ( allocated(xp) ) deallocate(xp)
      if ( allocated(yp) ) deallocate(yp)
      if ( allocated(zp) ) deallocate(zp)
      if ( allocated(itl) ) deallocate(itl)
      if ( allocated(ifn) ) deallocate(ifn)
      if ( allocated(ifc) ) deallocate(ifc)
      if ( allocated(vol) ) deallocate(vol)
      if ( allocated(area) ) deallocate(area)
      if ( allocated(ndst) ) deallocate(ndst)
      nmax = 0
      nfmax = 0
      nvmax = 0

      if ( nsite<=0 ) return

      nmax = npts_per_site*(1+nsite)
      nfmax = 8*nmax
      nvmax = 7*nmax

      allocate(x(max(nmax,nvmax)))
      x = 0
      allocate(y(max(nmax,nvmax)))
      y = 0
      allocate(z(max(nmax,nvmax)))
      z = 0
      allocate(w(nmax))
      w = 0
      allocate(ix(max(nmax,nvmax)))
      ix = 0
      allocate(iy(max(nmax,nvmax)))
      iy = 0
      allocate(iz(max(nmax,nvmax)))
      iz = 0
      allocate(iw(nmax))
      iw = 0
      allocate(ix2(max(nmax,nvmax)))
      ix2 = 0
      allocate(iy2(max(nmax,nvmax)))
      iy2 = 0
      allocate(iz2(max(nmax,nvmax)))
      iz2 = 0
      allocate(iw2(nmax))
      iw2 = 0
      allocate(is(nmax))
      is = 1
      allocate(icon(8,nvmax))
      icon = 0
      allocate(ifl(nvmax))
      ifl = 0
      allocate(id(nvmax))
      id = 0
      allocate(xp(nvmax))
      xp = 0
      allocate(yp(nvmax))
      yp = 0
      allocate(zp(nvmax))
      zp = 0
      allocate(itl(nmax))
      itl = 0
      allocate(ifn(nmax))
      ifn = 0
      allocate(ifc(nfmax))
      ifc = 0
      allocate(vol(nmax))
      vol = 0
      allocate(area(nfmax))
      area = 0
      allocate(ndst(nfmax))
      ndst = 0
      return
!EOC
      end subroutine alloc_berncom

!BOP
!!IROUTINE: delete_bernal_vp_files
!!INTERFACE:
      subroutine delete_bernal_vp_files
!!DESCRIPTION:
! deletes files created by Bernal's library procedures
!
!!REVISION HISTORY:
! Initial version - A.S. - 2013
!EOP
!
!BOC
      integer st_env
       close(pnts_wght_un)
       close(tetrahedr_un)
       close(power_vrt_un)
       close(areas_vol_un)
       close(tetra_unbd_un)
!       open (1, file='pnts-wghts')
!       close (1,status='delete')
!       open (1, file='pnts-wghts_dup')
!       close (1,status='delete')
!       open (1, file = 'tetrahedra')
!       close (1,status='delete')
!       open (1, file = 'power-vrts')
!       close (1,status='delete')
!       open (1, file = 'tetra-unbd')
!       close (1,status='delete')
!       open (1, file = 'areas-vols')
!       close (1,status='delete')
       call get_environment_variable('ENV_TOMATH',status=st_env)
       if ( st_env .ne. 0 ) then
        open (1, file = 'AtomToMath')
        close (1,status='delete')
        open (1, file = 'VertToMath')
        close (1,status='delete')
        open (1, file = 'EdgeToMath')
        close (1,status='delete')
       end if 
       return
!EOC
      end subroutine delete_bernal_vp_files

!BOP
!!IROUTINE: drive_regtet
!!INTERFACE:
      subroutine drive_regtet
!!DESCRIPTION:
! {\bv
!c     ------------------------------------------------------------------
!c     This program will compute a Regular tetrahedralization for
!c     a set of points in 3-dimensional space.
!c
!c     A Regular tetrahedralization is the dual of a Power diagram.
!c     It is essentially a Delaunay tetrahedralization with weights.
!c
!c     If no weights are present the program will compute a Delaunay
!c     tetrahedralization. It is noted that if weights are present
!c     and degeneracies exist there may be points whose Power cells
!c     are not 3-dimensional.
!c
!c     The program is based on an algorithm for constructing Regular
!c     tetrahedralizations with incremental topological flipping.
!c
!c     Computations in this program for the purpose of computing
!c     the tetrahedralization are done in exact arithmetic whenever
!c     floating point arithmetic (done in double precision) does not
!c     seem appropriate.
!c
!c     Subroutine regtet is the driver routine of the program and is
!c     called from the main routine.
!c
!c     Documentation appears below in driver routine regtet.
!c
!c     Author: Javier Bernal
!c             National Institute of Standards and Technology (NIST)
!c
!c     Disclaimer:
!c
!c     This software was developed at the National Institute of Standards
!c     and Technology by employees of the Federal Government in the
!c     course of their official duties. Pursuant to title 17 Section 105
!c     of the United States Code this software is not subject to
!c     copyright protection and is in the public domain. This software is
!c     experimental. NIST assumes no responsibility whatsoever for its
!c     use by other parties, and makes no guarantees, expressed or
!c     implied, about its quality, reliability, or any other
!c     characteristic. We would appreciate acknowledgement if the
!c     software is used.
!c
!c     ------------------------------------------------------------------
!c     NOTE I MODIFIED pntins over routine
!c     found in bernal installed 11-21-07
!c     ------------------------------------------------------------------
!c
!c     Setting of parameters:
!c
!c     1. Flipping history used for locating points:
!c
!c     integer*8 nmax, nvmax, nhmax
!c     parameter (nmax=150000, nvmax=55*nmax, nhmax=1500)
!c
!c     2. Flipping history not used for locating points:
!c
! \ev}
!!ARGUMENTS:
! none
!!REMARKS:
! input files "pnts-wghts" and "tetrahedra" are required;
! fortran units 11, 12 are in use
!EOP
!
!BOC
      real(8), allocatable :: v(:) ! nmax
      integer(8), allocatable :: io(:)  ! nmax
      integer(8), allocatable :: ih(:) ! >=1500
!c
      integer*8 nv, nw, nt, nd, naddl, isu, jsu, ksu, nsu, icfig, iwfig
      double precision wlenx, wleny, wlenz, wlenw, epz
      logical delaun, pntoff, flphis, artfcl, random, reccor, redchk
!c
!c     logical prompt
!c     character*1 answ
!c     logical bigbox
      double precision xcor, ycor, zcor, wght
!c     integer*8 np, iric, irec
      integer*8 i, j
      integer*8 ideli, ipnti, iflpi, iarti, irani, ireci, iredi
!DEBUG      integer(8), parameter :: nhmax=1500
      integer(8) :: nhmax

      if ( allocated(v) ) stop 5001
      allocate(v(nmax))
      if ( allocated(io) ) stop 5002
      allocate(io(nmax))
      if ( allocated(ih) ) stop 5003
      allocate(ih(nmax))
      nhmax = size(ih)
!c
!c----------------------------------------------------------------------
!cc
!c     write(*,*)' '
!c     write(*,*)'This program is for computing a Regular ',
!c    *          'tetrahedralization for a set'
!c     write(*,*)'of points in 3-d space, ',
!c    *          'i. e. a Delaunay tetrahedralization with weights.'
!c     write(*,*)' '
!c     write(*,*)'If no weights are present a Delaunay ',
!c    *          'tetrahedralization is computed.'
!c     write(*,*)'Note that if weights are present and degeneracies ',
!c    *          'exist there may'
!c     write(*,*)'be points whose Power cells are ',
!c    *          'not 3-dimensional.'
!c     write(*,*)' '
!c     write(*,*)'Computations in this program are done in exact ',
!c    *          'arithmetic whenever'
!c     write(*,*)'floating point arithmetic (done in double precision) ',
!c    *          'does not seem'
!c     write(*,*)'appropriate.'
!c     write(*,*)' '
!c     write(*,*)'Documentation about input/output variables, etc. ',
!c    *          'appears in driver'
!c     write(*,*)'routine regtet'
!cc
!c----------------------------------------------------------------------
!cc
!cc     inquire about existence of prompts file
!cc
!c     inquire(file = 'prompts.data', exist = prompt)
!c     write(*,*)' '
!c     if(prompt)then
!c        write(*,*)'User intervention not required: responses to ',
!c    *             'would-be program generated'
!c        write(*,*)'prompts will be read from file prompts.data ',
!c    *             '(prompts will be suppressed).'
!c        open (9, file = 'prompts.data')
!c     else
!c        write(*,*)'User intervention required: file named ',
!c    *             'prompts.data does not exist, thus'
!c        write(*,*)'program will generate prompts for which ',
!c    *             'user must provide responses (in'
!c        write(*,*)'order to avoid user intervention a file ',
!c    *             'named prompts.data must exist that'
!c        write(*,*)'contains the responses to the would-be ',
!c    *             'prompts; prompts are then suppressed;'
!c        write(*,*)'for this purpose the program will generate ',
!c    *             'a file named prompts.tentative'
!c        write(*,*)'that will contain the responses provided ',
!c    *             'by the user during the current run,'
!c        write(*,*)'and which can then be renamed prompts.data ',
!c    *             ' by the user for future runs).'
!c        open (10, file = 'prompts.tentative')
!c     endif
!c
!c----------------------------------------------------------------------
!c
!c  10 format (a1)
!c
!c----------------------------------------------------------------------
!cc
!c     write(*,*)' '
!c     answ = ' '
!c     if(prompt)then
!c        read(9,10) answ
!c     else
!c        write(*,*)'A Delaunay tetrahedralization (no weights) to be ',
!c    *             'computed?(y/n)'
!c        read(5,10) answ
!c        write(10,10) answ
!c     endif
!c     if(answ.eq.'y'.or.answ.eq.'Y') then
         delaun=.false.
!c         delaun=.false.
!c        write(*,*)'A Delaunay tetrahedralization will be computed.'
!c     else
!c        delaun=.false.
!c        write(*,*)'A Regular tetrahedralization will be computed.'
!c     endif
!cc
!c----------------------------------------------------------------------
!cc
!c     write(*,*)' '
!c     answ = ' '
!c     if(prompt)then
!c        read(9,10) answ
!c     else
!c        write(*,*)'Any input points to be inactive during ',
!c    *             'tetrahedralization computation?(y/n)'
!c        read(5,10) answ
!c        write(10,10) answ
!c     endif
!c     if(answ.eq.'y'.or.answ.eq.'Y') then
!c        pntoff =.true.
!c        write(*,*)'Some input points will be inactive during ',
!c    *             'tetrahedralization computation.'
!c     else
         pntoff =.false.
!c        write(*,*)'All input points will be active during ',
!c    *             'tetrahedralization computation.'
!c     endif
!cc
!c----------------------------------------------------------------------
!cc
!c     write(*,*)' '
!c     answ = ' '
!c     if(prompt)then
!c        read(9,10) answ
!c     else
!c        write(*,*)'Tetrahedron list with flipping history to be ',
!c    *             'used for locating points?(y/n)'
!c        write(*,*)'(Otherwise shishkebab method will be used. Note ',
!c    *             'that in order'
!c        write(*,*)'to use flipping history, parameter nvmax in the ',
!c    *             'program should'
!c        write(*,*)'be set to about 55*nmax, otherwise to about ',
!c    *             '7*nmax).'
!c        read(5,10) answ
!c        write(10,10) answ
!c     endif
!c     if(answ.eq.'y'.or.answ.eq.'Y') then
!c        flphis=.true.
!c        write(*,*)'Tetrahedron list with flipping history will be ',
!c    *             'used.'
!c     else
         flphis=.false.
!c        write(*,*)'Shishkebab method will be used.'
!c     endif
!cc
!c----------------------------------------------------------------------
!cc
!c     write(*,*)' '
!c     answ = ' '
!c     if(prompt)then
!c        read(9,10) answ
!c     else
!c        write(*,*)'Do you want output tetrahedron list to include ',
!c    *             'both real and artificial'
!c        write(*,*)'tetrahedra in the final tetrahedralization?(y/n)'
!c        read(5,10) answ
!c        write(10,10) answ
!c     endif
!c     if(answ.eq.'y'.or.answ.eq.'Y') then
!c        artfcl=.true.
!c        write(*,*)'Output tetrahedron list will include both real ',
!c    *             'and artificial tetrahedra'
!c        write(*,*)'in the final tetrahedralization together with ',
!c    *             'flipping history tetrahedra'
!c        write(*,*)'if flipping history was used for locating points.'
!c     else
         artfcl=.false.
!c        write(*,*)'Output tetrahedron list will only include ',
!c    *             'real tetrahedra in the final'
!c        write(*,*)'tetrahedralization.'
!c     endif
!cc
!c----------------------------------------------------------------------
!cc
!c     write(*,*)' '
!c     answ = ' '
!c     if(prompt)then
!c        read(9,10) answ
!c     else
!c        write(*,*)'Do you want input points to be inserted in ',
!c    *             'a random order?(y/n)'
!c        read(5,10) answ
!c        write(10,10) answ
!c     endif
!c     if(answ.eq.'y'.or.answ.eq.'Y') then
!c        random = .true.
!c        write(*,*)'Input points will be inserted in a random order.'
!c        write(*,*)' '
!c        if(prompt)then
!c           read(9,*) isu, jsu, ksu, nsu
!c           write(*,*)'The four seeds for randomizing are:'
!c           write(*,*)isu, jsu, ksu, nsu
!c        else
!c           write(*,*)'Enter seeds for randomizing (4 integers):'
!c           read(5,*) isu, jsu, ksu, nsu
!cc
!cc           isu = 521288629
!cc           jsu = 362436069
!cc           ksu = 16163801
!cc           nsu = 131199299
!cc
!c           write(10,*) isu, jsu, ksu, nsu
!c        endif
!c     else
         isu = 0
         jsu = 0
         ksu = 0
         nsu = 0
         random = .false.
!c        write(*,*)'Input points will be inserted in their ',
!c    *             'current order.'
!c     endif
!cc
!c----------------------------------------------------------------------
!cc
!c     write(*,*)' '
!c     answ = ' '
!c     if(prompt)then
!c        read(9,10) answ
!c     else
!c        write(*,*)'Do you want points that define a rectangular ',
!c    *             'regular grid on the surface'
!c        write(*,*)'of a rectangular polyhedron that contains ',
!c    *             'set of input points to become,'
!c        write(*,*)'together with set of input points and ',
!c    *             ' artificial points, the set for'
!c        write(*,*)'which a tetrahedralization is to be computed?(y/n)'
!c        read(5,10) answ
!c        write(10,10) answ
!c     endif
!c     if(answ.eq.'y'.or.answ.eq.'Y') then
!c        reccor=.true.
!c        write(*,*)'Points that define a rectangular regular grid on ',
!c    *             'the surface of a'
!c        write(*,*)'rectangular polyhedron that contains set of input ',
!c    *             'points will be part'
!c        write(*,*)'of the set for which tetrahedralization is to be ',
!c    *             'computed; dimensions'
!c        write(*,*)'of polyhedron and choice of points that define ',
!c    *             'grid are according to'
!c        write(*,*)'specifications provided by user.'
!c        write(*,*)' '
!c        write(*,*)'If xmax, ymax, zmax are the maximum values of the ',
!c    *             'x, y, and z coordinates'
!c        write(*,*)'of the input points, respectively, and xmin, ',
!c    *             'ymin, zmin are the minimum'
!c        write(*,*)'values, then for positive numbers ',
!c    *             'wlenx, wleny, wlenz (provided by user),'
!c        write(*,*)'the eight vertices of the the polyhedron will be: '
!c        write(*,*)'(xmin-wlenx, ymin-wleny, zmin-wlenz), ',
!c    *             '(xmax+wlenx, ymin-wleny, zmin-wlenz),'
!c        write(*,*)'(xmax+wlenx, ymax+wleny  zmin-wlenz), ',
!c    *             '(xmin-wlenx, ymax+wleny, zmin-wlenz),'
!c        write(*,*)'(xmin-wlenx, ymin-wleny, zmax+wlenz), ',
!c    *             '(xmax+wlenx, ymin-wleny, zmax+wlenz),'
!c        write(*,*)'(xmax+wlenx, ymax+wleny  zmax+wlenz), ',
!c    *             '(xmin-wlenx, ymax+wleny, zmax+wlenz).'
!c        write(*,*)' '
!c        write(*,*)'For positive integer naddl (provided by user) ',
!c    *             'for each facet of the'
!c        write(*,*)'polyhedron a set of naddl x naddl points is ',
!c    *             'generated by program;'
!c        write(*,*)'this set defines a rectangular regular grid ',
!c    *             'on the facet and contains'
!c        write(*,*)'the four vertices of the facet; the points in ',
!c    *             'the union of the six'
!c        write(*,*)'sets thus generated define the rectangular ',
!c    *             'grid on the surface of'
!c        write(*,*)'the polyhedron; naddl can not be less than 2; ',
!c    *             'if it equals 2 then'
!c        write(*,*)'the grid is defined exactly by the 8 vertices ',
!c    *             'of the polyhedron.'
!c        if(.not.delaun) then
!c           write(*,*)' '
!c           write(*,*)'If wmin is the minimum value of the weights of ',
!c    *                'the input points then'
!c           write(*,*)'for a real number wlenw (provided ',
!c    *                'by user) a weight equal to wmin - wlenw'
!c           write(*,*)'is assigned by the program to ',
!c    *                'each point in the rectangular grid on the'
!c           write(*,*)'surface of the polyhedron.'
!c        endif
!c        write(*,*)' '
!c        if(prompt)then
!c           read(9,*) wlenx, wleny, wlenz
!c           if(.not.delaun) read(9,*) wlenw
!c           read(9,*) naddl
!c           write(*,*)'The values of wlenx, wleny, wlenz are:'
!c           write(*,*) wlenx, wleny, wlenz
!c           if(.not.delaun) then
!c              write(*,*)'The value of wlenw is:'
!c              write(*,*) wlenw
!c           endif
!c           write(*,*)'The value of naddl is:'
!c           write(*,*) naddl
!c        else
!c           write(*,*)'Enter wlenx, wleny, wlenz ',
!c    *      '(3 positive real numbers):'
!c           read(5,*) wlenx, wleny, wlenz
!c           if(.not.delaun) then
!c              write(*,*)'Enter wlenw (a real number):'
!c              read(5,*) wlenw
!c           endif
!c           write(*,*)'Enter naddl (an integer greater than 1):'
!c           read(5,*) naddl
!c           write(10,*) wlenx, wleny, wlenz
!c           if(.not.delaun) write(10,*) wlenw
!c           write(10,*) naddl
!c        endif
!c     else
         reccor=.false.
!c        write(*,*)'Points that define a rectangular regular grid on ',
!c    *             'the surface of a'
!c        write(*,*)'rectangular polyhedron that ',
!c    *             'contains set of input points will not'
!c        write(*,*)'be part of the set for which a tetrahedralization ',
!c    *             'is to be computed.'
!c     endif
!cc
!c----------------------------------------------------------------------
!cc
      if(delaun) go to 50
!c     write(*,*)' '
!c     answ = ' '
!c     if(prompt)then
!c        read(9,10) answ
!c     else
!c        write(*,*)'Do you want redundant points to be checked for ',
!c    *             'redundancy after '
!c        write(*,*)'final tetrahedralization has been computed?(y/n)'
!c        write(*,*)'(doing so may require some additional time).'
!c        read(5,10) answ
!c        write(10,10) answ
!c     endif
!c     if(answ.eq.'y'.or.answ.eq.'Y') then
         redchk=.true.
!c        write(*,*)'Redundant points will be checked for redundancy.'
!c     else
!c        redchk=.false.
!c        write(*,*)'Redundant points will not be checked for ',
!c    *             'redundancy.'
!c     endif
   50 continue
!cc
!c----------------------------------------------------------------------
!c
!c     bigbox=.false.
!c     if(.not.reccor) go to 55
!c     write(*,*)' '
!c     answ = ' '
!c     if(prompt)then
!c        read(9,10) answ
!c     else
!c        write(*,*)'Do you want points that define rectangular grid ',
!c    *             'as described above'
!c        write(*,*)'to be saved in a file?(y/n)'
!c        read(5,10) answ
!c        write(10,10) answ
!c     endif
!c     if(answ.eq.'y'.or.answ.eq.'Y') then
!c        bigbox=.true.
!c        write(*,*)'Rectangular grid will be saved in a file.'
!c     else
!c        write(*,*)'Rectangular grid will not be saved in a file.'
!c     endif
!c  55 continue
!cc
!c----------------------------------------------------------------------
!cc
!c     write(*,*)' '
!c     if(prompt)then
!c        read(9,*) icfig
!c        write(*,*)'The number of significant figures of decimal part ',
!c    *             'of point coordinates is ', icfig
!c     else
!c        write(*,*)'Enter icfig, i. e. number of significant figures ',
!c    *             'of decimal part'
!c        write(*,*)'of point coordinates, -1 < icfig < 10 (total ',
!c    *             'number of significant'
!c        write(*,*)'figures should be at most 14 with at most 9 to ',
!c    *             'either the left or'
!c        write(*,*)'the right of the decimal point):'
!c        read(5,*) icfig
!c        write(10,*) icfig
!c     endif
!c     if(delaun) go to 60
!c     write(*,*)' '
!c     if(prompt)then
!c        read(9,*) iwfig
!c        write(*,*)'The number of significant figures of decimal part ',
!c    *             'of point weights is ', iwfig
!c     else
!c        write(*,*)'Enter iwfig, i. e. number of significant figures ',
!c    *             'of decimal part'
!c        write(*,*)'of point weights, -1 < iwfig < 10, ',
!c    *             '-1 < 2*icfig - iwfig < 10 (total'
!c        write(*,*)'number of significant figures should ',
!c    *             'be at most 14 with at most 9'
!c        write(*,*)'to either the left or the right of the ',
!c    *             'decimal point):'
!c        read(5,*) iwfig
!c        write(10,*) iwfig
!c     endif
!c  60 continue
!c
!c
      icfig=9 !!!change significant digit here!!! default --> icfig=6
      iwfig=9 !                                           --> iwfig=6
!c
!cc
!c----------------------------------------------------------------------
!c
!c     open files
!c
!      open (11, file = 'pnts-wghts')
      rewind(pnts_wght_un)
!      open (12, file = 'tetrahedra')
      open(tetrahedr_un,status='scratch',form='formatted')
!c
!c----------------------------------------------------------------------
!c
!c     set tolerance
!c
      epz = 1.d-3
!c
!c     read vertex data
!c
!c     write(*,*)' '
!c     write(*,*)'Reading vertex data ...'
      nv = 0
      if(delaun) go to 130
!c
!c     read vertex data with weights
!c
  100 continue
      read (pnts_wght_un, *, end = 120) xcor, ycor, zcor, wght
      nv = nv + 1
      if (nv .gt. nmax) stop 1001
      x(nv) = xcor
      y(nv) = ycor
      z(nv) = zcor
      w(nv) = wght
      go to 100
  120 continue
      go to 140
!c
!c     read vertex data without weights
!c
  130 continue
      read (pnts_wght_un, *, end = 140) xcor, ycor, zcor
      nv = nv + 1
      if (nv .gt. nmax) stop 20
      x(nv) = xcor
      y(nv) = ycor
      z(nv) = zcor
      w(nv) = 0.0d0
      go to 130
  140 continue
!c
!c     read off-on information about input points if there are any that
!c     are to be inactive during the tetrahedralization computation
!c
!c     if(pntoff) then
!c        open (13, file = 'points-off')
!c        read (13,*) np
!c        if(np .ne. nv) stop 30
!c        read (13,150) (is(i), i = 1, np)
!c     endif
!c 150 format (40(1x,i1))
!c
!c----------------------------------------------------------------------
!cc
!c     write(*,*)' '
!c     write(*,*)'Computation of tetrahedralization to begin: driver ',
!c    *          'subroutine regtet'
!c     write(*,*)'will be called from the ',
!c    *          'main routine and points will be processed.'
!c     write(*,*)' '
!c     write(*,*)'Please wait ...'
!c     write(*,*)' '
!c
!c----------------------------------------------------------------------
!c
!c     call regtet to compute tetrahedralization
!c
      call regtet(x, y, z, w, v, ix, iy, iz, iw, ix2, iy2, iz2, iw2,    &
     &            icon, is, ifl, io, id, ih, nv, nw, nt, nd, nmax,      &
     &            nvmax, nhmax, wlenx, wleny, wlenz, wlenw, naddl,      &
     &            isu, jsu, ksu, nsu, icfig, iwfig, epz, delaun,        &
     &            pntoff, flphis, artfcl, random, reccor, redchk)
!c
!c----------------------------------------------------------------------
!cc
!      write(*,*)' '
!      write(*,*)'(Back to the main routine).'
!      write(*,*)' '
!      write(*,*)'Computation of tetrahedralization has been completed.'
!      write(*,*)' '
!      write(*,*)'Number of input vertices = ',nv
!      write(*,*)' '
!      write(*,*)'Number of output vertices = ',nw
!      write(*,*)' '
!      write(*,*)'Length of final tetrahedron list = ',nt
!      write(*,*)' '
!      write(*,*)'Number of real tetrahedra = ',nd
!DEBUG
      i = minval(is(1:nw))
      if ( i<0 ) then
       write(6,*) ' ERROR :: IS<0,: min=',i
       write(6,'(20I6)') is(1:min(nw,120))
      end if
!DEBUG
!      write(*,*)' '
!      write(*,*)'(The output vertices are the vertices of tetrahedra ',
!     *          'in the final'
!      write(*,*)'tetrahedron list).'
!cc
!c----------------------------------------------------------------------
!c
!c     write tetrahedralization information in files
!c
      ideli = 0
      ipnti = 0
      iflpi = 0
      iarti = 0
      irani = 0
      ireci = 0
      iredi = 0
      if(delaun) ideli = 1
!c     if(pntoff) ipnti = 1
!c     if(flphis) iflpi = 1
!c     if(artfcl) iarti = 1
!c     if(random) irani = 1
!c     if(reccor) ireci = 1
      if(redchk) iredi = 1
!c     write(*,*)' '
!c     write(*,*)'Saving tetrahedralization data in output file ...'
      if(nt.ne.0)then
         write (tetrahedr_un,480) ideli,ipnti,iflpi,iarti,irani,ireci,  &
     &                            iredi
         if(.not.delaun) then
            write (tetrahedr_un,*) nw, nt, icfig, iwfig
         else
            write (tetrahedr_un,*) nw, nt, icfig
         endif
         write (tetrahedr_un,500) ((icon(i,j), i = 1, 8), j = 1, nt)
         write (tetrahedr_un,550) (is(i), i = 1, nw)
!c        if(reccor .and. .not.delaun) then
!c           write (12,*) wlenx, wleny, wlenz, wlenw
!c           write (12,*) naddl
!c        elseif(reccor) then
!c           write (12,*) wlenx, wleny, wlenz
!c           write (12,*) naddl
!c         endif
      else
       write (*,*)'warning: no real tetrahedra were created.'
       write (*,*)' '
       write (tetrahedr_un,*)'warning: no real tetrahedra were created.'
       write (tetrahedr_un,*)' '
      endif
!c
  480 format (7(1x,i1))
  500 format (8i10)
  550 format (7i10)
!c
!c     if(bigbox) then
!c        open (14, file = 'bgbox-pnts')
!c        iric = 1
!c        if(artfcl) iric = 9
!c        irec = iric + 6*(naddl**2) - 12*naddl + 7
!c        if(delaun) then
!c           do 580 i = iric, irec
!c              write (14,*) x(i), y(i), z(i)
!c 580       continue
!c        else
!c           do 590 i = iric, irec
!c              write (14,*) x(i), y(i), z(i), w(i)
!c 590       continue
!c        endif
!c     endif
!c
!c----------------------------------------------------------------------
!c
!c     write information in file in a readable form
!c
!c      open (23, file = 'exeinfor')
!c      write (23,*)' '
!c      write (23,*)'Number of vertices   =',nw
!c      write (23,*)'Number of tetrahedra =',nt
!c      write (23,*)' '
!c     if(nt.ne.0)then
!c        do 600 i = 1, nt
!c           write (23,800) i, (icon(j,i), j=1,8)
!c 600    continue
!c        do 650 i = 1, nw
!c           write (23,850) i, is(i)
!c 650    continue
!c     else
!c        write (23,*)'warning: no real tetrahedra were created.'
!c        write (23,*)' '
!c     endif
!c 800 format ('tetr: ', i4, ' info: ', 8(1x,i4))
!c 850 format ('vert: ', i4, ' is: ',i4)
!c
!c----------------------------------------------------------------------
!c
!c     write(*,*)' '
!      close(unit=11)
      deallocate(v)
      deallocate(io)
      deallocate(ih)
      rewind(pnts_wght_un)
!      close(unit=12)
      rewind(tetrahedr_un)
      return
!EOC
      end subroutine drive_regtet

!BOP
!!IROUTINE: drive_pwrvtx
!!INTERFACE:
      subroutine drive_pwrvtx(HighP)
!!DESCRIPTION:
! {\bv
!c     ------------------------------------------------------------------
!c     This program will compute the Power diagram vertices and the
!c     unbounded Power diagram edges for a set of points in 3-d
!c     space from a Regular tetrahedralization for the set.
!c
!c     A Power diagram is essentially a Voronoi diagram with weights.
!c
!c     If no weights are present the program will compute the Voronoi
!c     diagram vertices and the unbounded Voronoi diagram edges for
!c     the set of points from a Delaunay tetrahedralization for the set.
!c
!c     It is assumed that a Regular/Delaunay tetrahedralization for
!c     the set of points has already been computed using existing
!c     program regtet and that the output tetrahedron list produced
!c     by regtet contains only real tetrahedra (no artificial ones),
!c     i. e. logical variable artfcl was set to .false. during the
!c     execution of regtet. The output tetrahedron list from regtet
!c     is then used as input below. It is noted that because of
!c     degeneracies more than one tetrahedron may correspond to the
!c     same Power/Voronoi vertex. In addition, because of degeneracies,
!c     Power/Voronoi cells of points may have facets that are not
!c     2-dimensional, and if weights are present there may be Power
!c     cells of points that are not 3-dimensional.
!c
!c     Subroutine pwrvtx is the driver routine of the program and is
!c     called from the main routine.
!c
!c     Description of output variables appears in driver routine pwrvtx.
!c
!c     Author: Javier Bernal
!c
!c     Disclaimer:
!c
!c     This software was developed at the National Institute of Standards
!c     and Technology by employees of the Federal Government in the
!c     course of their official duties. Pursuant to title 17 Section 105
!c     of the United States Code this software is not subject to
!c     copyright protection and is in the public domain. This software is
!c     experimental. NIST assumes no responsibility whatsoever for its
!c     use by other parties, and makes no guarantees, expressed or
!c     implied, about its quality, reliability, or any other
!c     characteristic. We would appreciate acknowledgement if the
!c     software is used.
!c
!c     ------------------------------------------------------------------
! \ev}

!!ARGUMENTS:
      logical, intent(in) :: HighP
!!REMARKS:
! Input files are required: "pnts-wghts", "tetrahedra", "power-vrts", "tetra-unbd";
! fortran units 11, 12, 13, 14 are in use
!EOP
!
!BOC
      integer*8 nv, nw, nt, nq, naddl, icfig, iwfig
      double precision wlenx, wleny, wlenz, wlenw
      logical delaun, pntoff, flphis, artfcl, random, reccor, redchk
!c
      double precision xcor, ycor, zcor, wght
      double precision xpmin, ypmin, zpmin, xpmax, ypmax, zpmax
      integer*8 ideli, ipnti, iflpi, iarti, irani, ireci, iredi
      integer*8 i, j, no
!c
      integer*8 ng, ifir, isec, ithi, ifou, larg
      double precision derro, dist1, dist2, dist3, dist4
      double precision dista, disti, diffd, dnom
!c
!c----------------------------------------------------------------------
!cc
!c     write(*,*)' '
!c     write(*,*)'This program is for computing the Power diagram ',
!c    *          'vertices and the'
!c     write(*,*)'unbounded Power diagram edges for a set of ',
!c    *          'points in 3-d space'
!c     write(*,*)'from a Regular tetrahedralization for the set.'
!c     write(*,*)' '
!c     write(*,*)'If no weights are present the Voronoi vertices ',
!c    *          'and the unbounded'
!c     write(*,*)'Voronoi edges are computed from a Delaunay ',
!c    *          'tetrahedralization for'
!c     write(*,*)'the set.'
!c     write(*,*)' '
!c     write(*,*)'It is assumed that a Regular/Delaunay ',
!c    *          'tetrahedralization for the'
!c     write(*,*)'set has already been computed using existing program ',
!c    *          'regtet and'
!c     write(*,*)'that the output tetrahedron list produced by regtet ',
!c    *          'contains only'
!c     write(*,*)'real tetrahedra (logical variable artfcl was set to ',
!c    *          '.false. during'
!c     write(*,*)'the execution of regtet).'
!c     write(*,*)' '
!c     write(*,*)'The output tetrahedron list from regtet ',
!c    *          'is then used as input by'
!c     write(*,*)'this program. Note that because of degeneracies ',
!c    *          'more than one'
!c     write(*,*)'tetrahedron may correspond to the same Power/Voronoi ',
!c    *          'vertex.'
!c     write(*,*)'In addition, because of degeneracies, Power/Voronoi ',
!c    *          'cells of'
!c     write(*,*)'points may have facets that are not 2-dimensional, ',
!c    *          'and if weights'
!c     write(*,*)'are present there may be Power cells of points that ',
!c    *          'are not'
!c     write(*,*)'3-dimensional.'
!c     write(*,*)' '
!c     write(*,*)'Arrays xp, yp, zp, icon, ifl will contain output ',
!c    *          'information.'
!c     write(*,*)'Description of these arrays appears in driver ',
!c    *          'routine pwrvtx.'
!c     write(*,*)' '
!c     write(*,*)'In the current main program arrays ifl, xp, yp, zp ',
!c    *          'are saved in'
!c     write(*,*)'output file power-vrts, and array icon in output ',
!c    *          'file tetra-unbd.'
!cc
!c----------------------------------------------------------------------
!c
!c     open files
!c
!      open (11, file = 'pnts-wghts')
!      open (12, file = 'tetrahedra')
!      open (13, file = 'power-vrts')
!      open (14, file = 'tetra-unbd')
      rewind(pnts_wght_un)
      rewind(tetrahedr_un)
      open(power_vrt_un,status='scratch',form='formatted')
      open(tetra_unbd_un,status='scratch',form='formatted')
!c
!c----------------------------------------------------------------------
      iwfig = 0
!c
!c     read tetrahedralization information
!c
!c     write(*,*)' '
!c     write(*,*)'Reading tetrahedralization information ...'
      read(tetrahedr_un,80) ideli,ipnti,iflpi,iarti,irani,ireci,iredi
      delaun = .false.
      pntoff = .false.
      flphis = .false.
      artfcl = .false.
      random = .false.
      reccor = .false.
      redchk = .false.
      if(ideli.eq.1) delaun = .true.
      if(ipnti.eq.1) pntoff = .true.
      if(iflpi.eq.1) flphis = .true.
      if(iarti.eq.1) artfcl = .true.
      if(irani.eq.1) random = .true.
      if(ireci.eq.1) reccor = .true.
      if(iredi.eq.1) redchk = .true.
      if(.not.delaun) then
         read (tetrahedr_un,*) nw, nt, icfig, iwfig
      else
         read (tetrahedr_un,*) nw, nt, icfig
      endif
      if(nw.gt.nmax .or. nt.gt.nvmax) stop 1002
      read (tetrahedr_un,90) ((icon(i,j), i = 1, 8), j = 1, nt)
      read (tetrahedr_un,95) (is(i), i = 1, nw)
!c     if(reccor .and. .not.delaun) then
!c        read (12,*) wlenx, wleny, wlenz, wlenw
!c        read (12,*) naddl
!c     elseif(reccor) then
!c        read (12,*) wlenx, wleny, wlenz
!c        read (12,*) naddl
!c     endif
!c
   80 format (7(1x,i1))
   90 format (8i10)
   95 format (7i10)
!c
!c----------------------------------------------------------------------
!c
      if(artfcl)then
         write(*,*)' '
         write(*,*)'Input tetrahedra contain artificial points.'
         write(*,*)'Such points are not allowed in this program.'
         write(*,*)'Program terminated.'
         stop 20
      endif
!c
!c----------------------------------------------------------------------
!c
!c     code for avoiding certain messages during compilation
!c
      if(pntoff) ipnti = 1
      if(flphis) iflpi = 1
      if(random) irani = 1
      if(redchk) iredi = 1
!c
!c----------------------------------------------------------------------
!c
!c     read vertex data
!c
!c     write(*,*)' '
!c     write(*,*)'Reading vertex data ...'
      nv = 0
      if(delaun) go to 130
!c
!c     read vertex data with weights
!c
  100 continue
      read (pnts_wght_un, *, end = 120) xcor, ycor, zcor, wght
      nv = nv + 1
      if (nv .gt. nmax) stop 30
      x(nv) = xcor
      y(nv) = ycor
      z(nv) = zcor
      w(nv) = wght
      go to 100
  120 continue
      go to 140
!c
!c     read vertex data without weights
!c
  130 continue
      read (pnts_wght_un, *, end = 140) xcor, ycor, zcor
      nv = nv + 1
      if (nv .gt. nmax) stop 40
      x(nv) = xcor
      y(nv) = ycor
      z(nv) = zcor
      w(nv) = 0.0d0
      go to 130
  140 continue
!c
!c----------------------------------------------------------------------
!cc
!c     write(*,*)' '
!c     write(*,*)'Computation of Power/Voronoi vertices to begin: ',
!c    *          'driver subroutine'
!c     write(*,*)'pwrvtx will be called from the main routine and ',
!c    *          'vertices will be'
!c     write(*,*)'computed.'
!c     write(*,*)' '
!c     write(*,*)'Please wait ...'
!c     write(*,*)' '
!c
!c----------------------------------------------------------------------
!c
!c     call pwrvtx to compute Power/Voronoi vertices
!c
      call pwrvtx(x, y, z, w, ix, iy, iz, iw, ix2, iy2, iz2, iw2,       &
     &            xp, yp, zp, icon, ifl, is, nv, nw, nt, nq, nmax,      &
     &            nvmax, wlenx, wleny, wlenz, wlenw, naddl, icfig,      &
     &            iwfig, delaun, artfcl, reccor)
!c
!c----------------------------------------------------------------------
!cc
!c     write(*,*)' '
!c     write(*,*)'(Back to the main routine).'
!c     write(*,*)' '
!c     write(*,*)'Computation of Power/Voronoi vertices has been ',
!c    *          'completed.'
!c     write(*,*)' '
!c     write(*,*)'Number of points in set whose Power/Voronoi ',
!c    *          'diagram is'
!c     write(*,*)'under consideration = ',nw
!c     write(*,*)' '
!c     write(*,*)'Number of (bounded) tetrahedra in initial tetrahedron'
!c     write(*,*)'list, i. e. number of Power/Voronoi vertices = ',nt
!c     write(*,*)' '
!c     write(*,*)'Number of bounded and unbounded tetrahedra in ',
!c    *          'final'
!c     write(*,*)'tetrahedron list, i. e. number of Power/Voronoi'
!c     write(*,*)'vertices and unbounded edges = ',nq
!cc
!c----------------------------------------------------------------------
!c
!c     save Power/Voronoi vertices
!c
!c     write(*,*)' '
!c     write(*,*)'Saving Power/Voronoi vertices and unbounded edges ',
!c    *          'information ...'
      write (power_vrt_un,*) nw, nt, nq
      write (power_vrt_un,400) (ifl(i),i=1,nq)
      do 300 i = 1, nq
         write (power_vrt_un,*) xp(i), yp(i), zp(i)
  300 continue
  400 format (40(1x,i1))
!c
!c----------------------------------------------------------------------
!c
!c     save tetrahedralization information including unbounded edges
!c
!c     write(*,*)' '
!c     write(*,*)'Saving final tetrahedralization information ...'
      write (tetra_unbd_un,80) ideli,ipnti,iflpi,iarti,irani,ireci,iredi
      write (tetra_unbd_un,'(2i10,1x,2i3)') nw, nq, icfig, iwfig
      write (tetra_unbd_un,90) ((icon(i,j), i = 1, 8), j = 1, nq)
      write (tetra_unbd_un,95) (is(i), i = 1, nw)
!c     if(reccor .and. .not.delaun) then
!c        write (14,*) wlenx, wleny, wlenz, wlenw
!c        write (14,*) naddl
!c     elseif(reccor) then
!c        write (14,*) wlenx, wleny, wlenz
!c        write (14,*) naddl
!c     endif
!c
!c----------------------------------------------------------------------
!c
!c     calculating min and max of Power/Voronoi vertices coordinates
!c
      do 520 i = 1, nt
         if(ifl(i).eq.0) go to 520
         xpmax = xp(i)
         xpmin = xp(i)
         ypmax = yp(i)
         ypmin = yp(i)
         zpmax = zp(i)
         zpmin = zp(i)
         go to 530
  520 continue
!c     write(*,*)' '
!c     write(*,*)'All Power/Voronoi vertices marked as too large.'
      go to 900
  530 continue
      do 550 no = 1, nt
         if(ifl(i).eq.0) go to 550
         if (xp(no) .gt. xpmax) xpmax = xp(no)
         if (xp(no) .lt. xpmin) xpmin = xp(no)
         if (yp(no) .gt. ypmax) ypmax = yp(no)
         if (yp(no) .lt. ypmin) ypmin = yp(no)
         if (zp(no) .gt. zpmax) zpmax = zp(no)
         if (zp(no) .lt. zpmin) zpmin = zp(no)
  550 continue
!c
!c     write(*,*)' '
!c     write(*,*)'min and max of Power/Voronoi vertices coordinates:'
!c     write(*,*)' '
!c     write(*,*)'xmin = ',xpmin
!c     write(*,*)'xmax = ',xpmax
!c     write(*,*)'ymin = ',ypmin
!c     write(*,*)'ymax = ',ypmax
!c     write(*,*)'zmin = ',zpmin
!c     write(*,*)'zmax = ',zpmax
!c
!c----------------------------------------------------------------------
!c
!c     test Power/Voronoi vertices
!c
      ng = 0
      derro = 0.0d0
      larg = 0
      do 800 i = 1, nt
         if(ifl(i).eq.0) then
            larg = larg+1
            go to 800
         endif
         ifir = icon(5,i)
         isec = icon(6,i)
         ithi = icon(7,i)
         ifou = icon(8,i)
         if(ifir.le.ng .or. isec.le.ng .or. ithi.le.ng .or.             &
     &      ifou.le.ng) then
          write(6,'('' test Power/Voronoi vertices...'')')
          if ( ifir.le.ng ) write(6,'('' ifir.le.ng: '',2i6)') ifir,ng
          if ( isec.le.ng ) write(6,'('' isec.le.ng: '',2i6)') isec,ng
          if ( ithi.le.ng ) write(6,'('' ithi.le.ng: '',2i6)') ithi,ng
          if ( ifou.le.ng ) write(6,'('' ifou.le.ng: '',2i6)') ifou,ng
          stop 51
         end if
!c
         xcor = x(ifir)-xp(i)
         ycor = y(ifir)-yp(i)
         zcor = z(ifir)-zp(i)
         wght= w(ifir)
         dnom=dmax1(dabs(xcor),dabs(ycor),dabs(zcor),dsqrt(dabs(wght)))
         if(dnom.gt.1.0d0) then
            xcor = xcor/dnom
            ycor = ycor/dnom
            zcor = zcor/dnom
            wght = (wght/dnom)/dnom
            dist1 = dnom*dsqrt(dabs(xcor**2 + ycor**2 + zcor**2 - wght))
         else
            dist1 = dsqrt(dabs(xcor**2 + ycor**2 + zcor**2 - wght))
         endif
!c
         xcor = x(isec)-xp(i)
         ycor = y(isec)-yp(i)
         zcor = z(isec)-zp(i)
         wght= w(isec)
         dnom=dmax1(dabs(xcor),dabs(ycor),dabs(zcor),dsqrt(dabs(wght)))
         if(dnom.gt.1.0d0) then
            xcor = xcor/dnom
            ycor = ycor/dnom
            zcor = zcor/dnom
            wght = (wght/dnom)/dnom
            dist2 = dnom*dsqrt(dabs(xcor**2 + ycor**2 + zcor**2 - wght))
         else
            dist2 = dsqrt(dabs(xcor**2 + ycor**2 + zcor**2 - wght))
         endif
!c
         xcor = x(ithi)-xp(i)
         ycor = y(ithi)-yp(i)
         zcor = z(ithi)-zp(i)
         wght= w(ithi)
         dnom=dmax1(dabs(xcor),dabs(ycor),dabs(zcor),dsqrt(dabs(wght)))
         if(dnom.gt.1.0d0) then
            xcor = xcor/dnom
            ycor = ycor/dnom
            zcor = zcor/dnom
            wght = (wght/dnom)/dnom
            dist3 = dnom*dsqrt(dabs(xcor**2 + ycor**2 + zcor**2 - wght))
         else
            dist3 = dsqrt(dabs(xcor**2 + ycor**2 + zcor**2 - wght))
         endif
!c
         xcor = x(ifou)-xp(i)
         ycor = y(ifou)-yp(i)
         zcor = z(ifou)-zp(i)
         wght= w(ifou)
         dnom=dmax1(dabs(xcor),dabs(ycor),dabs(zcor),dsqrt(dabs(wght)))
         if(dnom.gt.1.0d0) then
            xcor = xcor/dnom
            ycor = ycor/dnom
            zcor = zcor/dnom
            wght = (wght/dnom)/dnom
            dist4 = dnom*dsqrt(dabs(xcor**2 + ycor**2 + zcor**2 - wght))
         else
            dist4 = dsqrt(dabs(xcor**2 + ycor**2 + zcor**2 - wght))
         endif
!c
         dista = dmax1(dist1,dist2,dist3,dist4)
         disti = dmin1(dist1,dist2,dist3,dist4)
         diffd = dista-disti
         if(diffd.gt.derro) derro = diffd
  800 continue
!c     write(*,*)' '
!c     write(*,*)'Number of Power/Voronoi vertices marked as too ',
!c    *          'large = ',larg
!c     write(*,*)' '
!c     write(*,*)'Power/Voronoi distance error = ',derro
!c     write(*,*)'(This number should be close to zero).'
  900 continue
!c
!c     test unbounded edges
!c
      derro = 0.0d0
      ifir = 0
      do 1000 i = nt+1, nq
         call sitord(icon, ifir, i)
         isec = icon(6,i)
         ithi = icon(7,i)
         ifou = icon(8,i)
         dnom = dabs((x(isec)-x(ifou))*xp(i) + (y(isec)-y(ifou))*yp(i) +&
     &          (z(isec)-z(ifou))*zp(i))
         if(dnom.gt.derro) derro = dnom
         dnom = dabs((x(ithi)-x(ifou))*xp(i) + (y(ithi)-y(ifou))*yp(i) +&
     &          (z(ithi)-z(ifou))*zp(i))
         if(dnom.gt.derro) derro = dnom
 1000 continue
!c     write(*,*)' '
!c     write(*,*)'Power/Voronoi unbounded edge error = ',derro
!c     write(*,*)'(This number should be close to zero).'
!c
!c----------------------------------------------------------------------
!c
!c     write(*,*)' '
!      close(unit=11)
!      close(unit=12)
!      close(unit=13)
!      close(unit=14)
      rewind(pnts_wght_un)
      rewind(tetrahedr_un)
      rewind(power_vrt_un)
      rewind(tetra_unbd_un)
      return
!EOC
      end subroutine drive_pwrvtx

!BOP
!!IROUTINE: drive_volare
!!INTERFACE:
      subroutine drive_volare(first_only)
!!DESCRIPTION:
! {\bv
!c     ------------------------------------------------------------------
!c     This program will compute volumes of Power/Voronoi cells in the
!c     Power/Voronoi diagram of a set of points in 3-dimensional space,
!c     areas of facets of these cells, and the distances obtained by
!c     computing for each facet the distance between the two points
!c     in the set that define the facet (inter-neighbor distances).
!c
!c     It is assumed that a Regular/Delaunay tetrahedralization for
!c     the set has already been computed using existing program
!c     regtet/ragtat and that the output tetrahedron list produced by
!c     regtet/ragtat contains only real tetrahedra (no artificial ones),
!c     i. e. logical variable artfcl was set to .false. during the
!c     execution of regtet/ragtat. It is also assumed that the
!c     Power/Voronoi vertices and unbounded edges of the Power/Voronoi
!c     diagram of the set have already been computed using existing
!c     program pwrvtx with the output tetrahedron list from
!c     regtet/ragtat as input. Besides the list of points in the set,
!c     the output Power/Voronoi vertex and tetrahedron lists from pwrvtx
!c     are then used as input below.
!c     It is noted that because of degeneracies there may be facets
!c     that are not 2-dimensional any one of which will have zero
!c     area if bounded. In addition, if weights are present, there
!c     may be Power cells that are not 3-dimensional any one of which
!c     will have zero volume if bounded.
!c
!c     Subroutine volare is the driver routine of the program and is
!c     called from the main routine.
!c
!c     Description of output variables appears in driver routine volare.
!c
!c     Author: Javier Bernal
!c
!c     Disclaimer:
!c
!c     This software was developed at the National Institute of Standards
!c     and Technology by employees of the Federal Government in the
!c     course of their official duties. Pursuant to title 17 Section 105
!c     of the United States Code this software is not subject to
!c     copyright protection and is in the public domain. This software is
!c     experimental. NIST assumes no responsibility whatsoever for its
!c     use by other parties, and makes no guarantees, expressed or
!c     implied, about its quality, reliability, or any other
!c     characteristic. We would appreciate acknowledgement if the
!c     software is used.
!c
!c     ------------------------------------------------------------------
! \ev}
!
!!ARGUMENTS:
      logical, optional :: first_only 
!!REMARKS:
! Input files are required: "pnts-wghts", "tetra-unbd", "power-vrts", "areas-vols";
! fortran units 11, 12, 13, 14 are in use
!EOP
!
!BOC
      real(8), allocatable :: xa(:), ya(:), za(:)  ! nmax
      double precision xcor, ycor, zcor
      integer*8 nw, nv, nt, nq, nb, icfig
      integer*8 idumy, nw2, nt2, nq2, i, j
      if (allocated(xa) .or. allocated(ya) .or. allocated(za)) stop 5010
      allocate(xa(nmax),ya(nmax),za(nmax))
!c
!c----------------------------------------------------------------------
!cc
!c     write(*,*)' '
!c     write(*,*)'This program is for computing volumes of ',
!c    *          'Power/Voronoi cells in'
!c     write(*,*)'the Power/Voronoi diagram of a set of ',
!c    *          'points in 3-d space, for'
!c     write(*,*)'computing areas of facets of these cells, ',
!c    *          'and for computing the'
!c     write(*,*)'distance between the two points that define ',
!c    *          'a facet for each facet.'
!c     write(*,*)' '
!c     write(*,*)'It is assumed that a Regular/Delaunay ',
!c    *          'tetrahedralization of the'
!c     write(*,*)'set has already been computed using existing program ',
!c    *          'regtet/ragtat'
!c     write(*,*)'and that the output tetrahedron list produced by ',
!c    *          'regtet/ragtat'
!c     write(*,*)'contains only real tetrahedra (logical variable ',
!c    *          'artfcl was set to'
!c     write(*,*)'.false. during the execution of regtet/ragtat).'
!c     write(*,*)' '
!c     write(*,*)'It is also assumed that the Power/Voronoi vertices ',
!c    *          'and the'
!c     write(*,*)'unbounded edges of the Power/Voronoi diagram of the ',
!c    *          'set have'
!c     write(*,*)'already been computed using existing program pwrvtx ',
!c    *          'with the'
!c     write(*,*)'output tetrahedron list from regtet/ragtat as input.'
!c     write(*,*)' '
!c     write(*,*)'Besides the list of points in the set, the output ',
!c    *          'Power/Voronoi'
!c     write(*,*)'vertex and tetrahedron lists from pwrvtx are then ',
!c    *          'used as input'
!c     write(*,*)'by this program. Note that because of degeneracies ',
!c    *          'there may be'
!c     write(*,*)'facets that are not 2-dimensional any one of which ',
!c    *          'will have zero'
!c     write(*,*)'area if bounded. In addition, if weights are ',
!c    *          'present, there may'
!c     write(*,*)'be Power cells that are not 3-dimensional any one ',
!c    *          'of which will'
!c     write(*,*)'have zero volume if bounded.'
!c     write(*,*)' '
!c     write(*,*)'Arrays ifn, ifc, vol, area, ndst  will contain ',
!c    *          'output information.'
!c     write(*,*)'Description of these arrays appears in driver ',
!c    *          'routine volare.'
!c     write(*,*)' '
!c     write(*,*)'In the curret main program these arrays are saved ',
!c    *          'in output file'
!c     write(*,*)'areas-vols.'
!cc
!c----------------------------------------------------------------------
!c
!c     open files
!c
!      open (11, file = 'pnts-wghts')
!      open (12, file = 'tetra-unbd')
!      open (13, file = 'power-vrts')
!      open (14, file = 'areas-vols')
      open(unit=areas_vol_un,status='scratch',form='formatted')
      rewind(pnts_wght_un)
      rewind(tetra_unbd_un)
      rewind(power_vrt_un)
!c
!c----------------------------------------------------------------------
!c
!c     read tetrahedralization information
!c
!c     write(*,*)' '
!c     write(*,*)'Reading tetrahedralization information ...'
      read (tetra_unbd_un,*) idumy
      read (tetra_unbd_un,*) nw, nq, icfig
      if(nw.gt.nmax .or. nq.gt.nvmax) then
        write(6,'('' Reading tetrahedralization information ...'')')
        write(6,'('' nw.gt.nmax .or. nt.ge.nq .or. nq.gt.nvmax'')') 
        if ( nw>nmax ) write(6,'('' nw='',i6,'' > nmax='',i6)') nw,nmax
        if ( nw>nmax) write(6,'('' nq='',i6,'' > nvmax='',i6)') nq,nvmax
        stop 1003
      end if  
      read (tetra_unbd_un,90) ((icon(i,j), i = 1, 8), j = 1, nq)
      read (tetra_unbd_un,95) (is(i), i = 1, nw)
   90 format (8i10)
   95 format (7i10)
!c
!c----------------------------------------------------------------------
!c
      if(idumy.lt.0) stop 20
!c
!c----------------------------------------------------------------------
!c
!c     read point/site data
!c
!c     write(*,*)' '
!c     write(*,*)'Reading point/site data ...'
      nv = 0
  100 continue
      read (pnts_wght_un, *, end = 120) xcor, ycor, zcor
      nv = nv + 1
      if (nv .gt. nmax) stop 30
      xa(nv) = xcor
      ya(nv) = ycor
      za(nv) = zcor
      go to 100
  120 continue
      if(nv.ne.nw) stop 40
!c
!c----------------------------------------------------------------------
!c
!c     set icfig for power vertices
!c
      icfig = 9
!c
!c----------------------------------------------------------------------
!c
!c     read power vertices
!c
!c     write(*,*)' '
!c     write(*,*)'Reading power vertices ...'
      read (power_vrt_un,*) nw2, nt2, nq2
      nt = nt2  !DEBUG
      if(nw2.ne.nw .or. nt2.ne.nt .or. nq2.ne.nq) then
       write(6,'('' nw='',i6,'' nt='',i6,'' nq='',i6)') nw,nt,nq
       write(6,'('' nw2='',i6,'' nt2='',i6,'' nq2='',i6)') nw2,nt2,nq2
       write(6,'('' bernal: nw2.ne.nw .or. nt2.ne.nt .or. nq2.ne.nq'')')
       stop 50
      end if
      if ( nt >= nq ) then
        write(6,'('' nt='',i6,'' >= nq='',i6)') nt,nq
        stop 1004
      end if
      read (power_vrt_un,*) (ifl(i),i=1,nq)
      do 300 i = 1, nt
         read (power_vrt_un,*) xp(i), yp(i), zp(i)
  300 continue
!c
!c----------------------------------------------------------------------
!c
!c     write(*,*)' '
!c     write(*,*)'Computation of volumes, areas, distances to begin: ',
!c    *          'driver subroutine volare'
!c     write(*,*)'will be called from the main routine and volumes, ',
!c    *          'areas, distances'
!c     write(*,*)'will be computed.'
!c     write(*,*)' '
!c     write(*,*)'Please wait ...'
!c     write(*,*)' '
!c
!c----------------------------------------------------------------------
!c
!c     call volare to compute volumes, areas, distances
!c
      if ( present(first_only) ) then
       if ( first_only ) nw = 1
      end if
      call volare(xa, ya, za, xp, yp, zp, ix, iy, iz, ix2, iy2, iz2,    &
     &            icon, ifl, id, is, itl, ifn, ifc, vol, area, ndst,    &
     &            nmax, nvmax, nfmax, nw, nt, nq, nb, icfig)
!c
!c----------------------------------------------------------------------
!c
!c     write(*,*)' '
!c     write(*,*)'(Back to the main routine).'
!c     write(*,*)'Computation of volumes, areas, distances has been ',
!c    *          'completed.'
!c
!c----------------------------------------------------------------------
!c
!c     save volumes, areas, distances information
!c
!c     write(*,*)' '
!c     write(*,*)'Saving volumes, areas, distances information ...'
      write (areas_vol_un,*) nw, nb
      write (areas_vol_un,600) (ifn(i),i=1,nw)
      write (areas_vol_un,700) (ifc(i),i=1,nb)
      write (areas_vol_un,*) (vol(i),i=1,nw)
      write (areas_vol_un,*) (area(i),i=1,nb)
      write (areas_vol_un,*) (ndst(i),i=1,nb)
  600 format (7i10)
  700 format (8i10)
!c
!c----------------------------------------------------------------------
!c
!c     write(*,*)' '
!      close(unit=11)
!      close(unit=12)
!      close(unit=13)
!      close(unit=14)
      rewind(pnts_wght_un)
      rewind(tetra_unbd_un)
      rewind(power_vrt_un)
      rewind(areas_vol_un)
      deallocate(xa,ya,za)
      return
!EOC
      end subroutine drive_volare

!======================================================================

      subroutine artsig(x, y, z, x2, y2, z2, xc, yc, zc, ib, id, ic, k, &
     &                  mhalf, mfull, isclp, iasign)
!c
      integer(8) :: x(:),y(:),z(:),x2(:),y2(:),z2(:),xc(:),yc(:),zc(:)
      integer(8) :: isclp(:)
      integer*8 mhalf, mfull
      integer*8 b, d, c, ib, id, ic, k, iasign, ifn
!c
      call vrtarr(ib, id, ic, b, d, c)
!c
      if(d.gt.8 .and. c.gt.8) then
         call ipsign(x, y, z, x2, y2, z2, b, d, c, k, mhalf,            &
     &               mfull, isclp, iasign)
         go to 100
      elseif(b.le.8) then
         iasign = 1
         go to 100
      elseif(d.le.8.and.c.le.8) then
         call ipsig4(x, y, z, x2, y2, z2, xc, yc, zc, d, c, k, b,       &
     &               mhalf, mfull, isclp, iasign)
         if(iasign.ne.0) go to 100
         call ipsig6(x, y, z, x2, y2, z2, xc, yc, zc, d, c, k, b,       &
     &               mhalf, mfull, isclp, iasign)
         go to 100
      elseif(d.le.8) then
         ifn = 0
         call ipsig3(x, y, z, x2, y2, z2, xc, yc, zc, b, c, k, d,       &
     &               mhalf, mfull, isclp, ifn, iasign)
      else
         ifn = 1
         call ipsig3(x, y, z, x2, y2, z2, xc, yc, zc, b, d, k, c,       &
     &               mhalf, mfull, isclp, ifn, iasign)
      endif
      if(iasign.ne.0) go to 100
      call ipsign(x, y, z, x2, y2, z2, b, d, c, k, mhalf,               &
     &            mfull, isclp, iasign)
!c
  100 continue
      return
      end subroutine artsig

!=======================================================================

      subroutine bisphr(x, y, z, w, opvert, ivrt, epz,                  &
     &                  xctr, yctr, zctr, tdist, delaun, ipossi)
!c
      real(8) :: x(:), y(:), z(:), w(:)
      integer*8 opvert, ivrt, ipossi
      double precision epz, tdist, wambda, xctr, yctr, zctr, dif
      double precision xm, ym, zm, xu, yu, zu, xd, yd, zd, dmax
      double precision xu2, yu2, zu2,norm
      logical delaun
!c
!c     find midpoint of edge from opvert to ivrt
!c
      xm = (x(opvert) + x(ivrt)) / 2.0d0
      ym = (y(opvert) + y(ivrt)) / 2.0d0
      zm = (z(opvert) + z(ivrt)) / 2.0d0
!c
!c     find vector from ivrt to opvert
!c
      xu = x(opvert) - x(ivrt)
      yu = y(opvert) - y(ivrt)
      zu = z(opvert) - z(ivrt)
!c
      norm = dsqrt(xu**2 + yu**2 + zu**2)
      if(norm .lt. epz) then
        ipossi = 1
        go to 1000
      endif
      xu2 = xu/norm
      yu2 = yu/norm
      zu2 = zu/norm
!c
!c     compute orthogonal center of edge ivrt-opvert
!c
      if(.not.delaun)then
         wambda = ((w(ivrt)-w(opvert))/norm)/2.0d0
         xm = xm + wambda*xu2
         ym = ym + wambda*yu2
         zm = zm + wambda*zu2
      endif
!c
!c     compute distance
!c
      xd = xctr - xm
      yd = yctr - ym
      zd = zctr - zm
      dif = dsqrt(xd**2 + yd**2 + zd**2)
      dmax = dmax1(norm,dif)
      tdist = (xd*xu + yd*yu + zd*zu) / dmax
      if(tdist.gt. -epz .and. tdist.lt. epz) then
         ipossi = 1
      endif
!c
 1000 continue
      return
      end subroutine bisphr

!=======================================================================

      subroutine consis(icon, is, ifl, n, ivnxt)
!c
      integer(8) :: icon(:,:), is(:), ifl(:)
      integer(8) :: ikon(8,1)
      integer*8 site0, site1, site2, site3, n, ivnxt
      integer*8 i, iscur, isone, islst, isini, indx
!c
!c     test initial tetrahedron for each site
!c
      do 50 i = 1, n
          iscur = is(i)
          if (iscur .le. 0) goto 50
          if(icon(5,iscur) .ne. i .and. icon(6,iscur) .ne. i .and.      &
     &       icon(7,iscur) .ne. i .and. icon(8,iscur) .ne. i) stop 2810
   50 continue
!c
!c     initialize
!c
      isone = 1
      do 60 i = 1, n
         if(is(i) .gt. 0) go to 80
   60 continue
      stop 2820
   80 continue
      islst = is(i)
      isini = islst
!c
      do 100 i = 1, ivnxt
          ifl(i) = 0
  100 continue
!c
      ifl(isini) = 1
      indx = 1
      iscur = icon(1,isini)
      if(iscur.eq.0) go to 500
      site0 = icon(5,isini)
      site1 = icon(6,isini)
      site2 = icon(7,isini)
      site3 = icon(8,isini)
!c
!c     reorder iscur relative to site1 and site2, and test
!c
  200 continue
      if(site0.eq.site1 .or. site0.eq.site2 .or. site0.eq.site3 .or.    &
     &   site1.eq.site2 .or. site1.eq.site3 .or. site2.eq.site3)        &
     &   stop 2830
      call reordr(icon, site1, site2, iscur)
      if(icon(7,iscur) .ne. site3) stop 2840
      if(icon(4,iscur) .ne. islst) stop 2850
      if(icon(8,iscur) .eq. site0) stop 2855
      ifl(iscur) = 1
!c
!c     obtain next tetrahedron
!c
      islst = iscur
      indx = 1
      iscur = icon(1,islst)
      if(iscur.eq.0) go to 500
      site0 = icon(5,islst)
      site1 = icon(6,islst)
      site2 = icon(7,islst)
      site3 = icon(8,islst)
      if(ifl(iscur) .ne. 1) go to 200
!c
!c     reorder iscur relative to site1 and site2, and test
!c
  300 continue
      if(site0.eq.site1 .or. site0.eq.site2 .or. site0.eq.site3 .or.    &
     &   site1.eq.site2 .or. site1.eq.site3 .or. site2.eq.site3)        &
     &   stop 2860
      do 400 i = 1, 8
          ikon(i,1) = icon(i,iscur)
  400 continue
      call reordr(ikon, site1, site2, isone)
      if(ikon(7,1) .ne. site3) stop 2865
      if(ikon(4,1) .ne. islst) stop 2870
      if(ikon(8,1) .eq. site0) stop 2875
!c
!c     obtain next tetrahedron
!c
  500 continue
      if(indx.eq.1) then
          indx = 2
          iscur = icon(2,islst)
          if(iscur.eq.0) go to 500
          site0 = icon(6,islst)
          site1 = icon(5,islst)
          site2 = icon(8,islst)
          site3 = icon(7,islst)
          if(ifl(iscur) .ne. 1) go to 200
          go to 300
      elseif(indx.eq.2) then
          indx = 3
          iscur = icon(3,islst)
          if(iscur.eq.0) go to 500
          site0 = icon(7,islst)
          site1 = icon(5,islst)
          site2 = icon(6,islst)
          site3 = icon(8,islst)
          if(ifl(iscur) .ne. 1) go to 200
          go to 300
      elseif(indx.eq.3) then
          if(islst .ne. isini) then
              iscur = islst
              islst = icon(4,iscur)
              if(islst .eq. 0) stop 2880
              if(icon(1,islst) .eq. iscur) then
                  indx = 1
              elseif(icon(2,islst) .eq. iscur) then
                  indx = 2
              elseif(icon(3,islst) .eq. iscur) then
                  indx = 3
              elseif(icon(4,islst) .eq. iscur) then
                  indx = 4
              else
                  stop 2885
              endif
              go to 500
          else
              indx = 4
              iscur = icon(4,islst)
              if(iscur.eq.0) go to 500
              site0 = icon(8,islst)
              site1 = icon(5,islst)
              site2 = icon(7,islst)
              site3 = icon(6,islst)
              if(ifl(iscur) .ne. 1) go to 200
              go to 300
          endif
      endif
      if(islst .ne. isini) stop 2890
!c
!c     write (*,*) ' '
!c     write (*,*) '**************************************'
!c     write (*,*) 'consistency check satisfied'
!c     write (*,*) '**************************************'
!c     write (*,*) ' '
!c
      return
      end subroutine consis

!=======================================================================

      subroutine convex (icon, tetra, ifl, xi, yi, zi, x, y, z, x2, y2, &
     &                   z2, idmin, mhalf, mfull, isclp, epz)
!c
      real(8) :: xi(:), yi(:), zi(:)
      integer(8) :: x(:), y(:), z(:), x2(:), y2(:), z2(:)
      integer(8) :: icon(:,:),ifl(:),isclp(:)
      integer*8 tetra, curr, adj
      double precision epz
      integer*8 mhalf, mfull
      integer*8 idmin, i, j, nop, now, iside,one
      integer*8 ikon(8,1), site1, site2, site3, a, b, c, vert
!c
      idmin = 0
      one=1
!c
!c     classify all tetra
!c
      do 100 curr = 1, tetra
         if (icon(5,curr) .lt. 0) then
            ifl(curr) = -1
         elseif ((icon(5,curr) .le. 8) .or. (icon(6,curr) .le. 8) .or.  &
     &   (icon(7,curr) .le. 8) .or. (icon(8,curr) .le. 8)) then
            ifl(curr) = 0
         else
            ifl(curr) = 1
         endif
  100 continue
!c
!c     take all tetra s.t. ifl(tetra) = 1 and check convexity
!c
      do 300 curr = 1, tetra
         if (ifl(curr) .ne. 1) goto 300
         do 200 i = 1, 4
            adj = icon(i,curr)
            if (adj .eq. 0) stop 2510
            if (ifl(adj) .ne. 0) goto 200
            do 150 j = 1, 8
               ikon(j,1) = icon(j,curr)
  150       continue
            site1 = icon(i+4,curr)
            call sitord (ikon, site1, one)
            a = ikon(6,1)
            b = ikon(7,1)
            c = ikon(8,1)
            site1 = a
            site2 = b
            site3 = c
            call reordr (ikon, site1, site2, one)
            do 175 j = 1, 3
               nop = curr
               now = ikon(4,1)
  160          continue
               if (now .eq. curr .or. now .eq. 0) stop 2520
               call reordr (icon, site1, site2, now)
               if (ifl(now) .ne. 0) then
                  nop = now
                  now = icon(4,nop)
                  goto 160
               else
                  if(nop .eq. curr) go to 170
                  vert = icon(8,now)
                  call irsign(xi, yi, zi, x, y, z, x2, y2, z2, vert,    &
     &                        site1, site2, site3, mhalf, mfull,        &
     &                        isclp, epz, iside)
                  if(iside .gt. 0) idmin = idmin+1
               endif
!c
  170          continue
               if (j .eq. 1) then
                  site1 = b
                  site2 = c
                  site3 = a
                  call reordr (ikon, site1, site2, one)
               endif
               if (j .eq. 2) then
                  site1 = c
                  site2 = a
                  site3 = b
                  call reordr (ikon, site1, site2, one)
               endif
  175       continue
  200    continue
  300 continue
!c
      return
      end subroutine convex

!=======================================================================

      subroutine crossp(x, y, z, x2, y2, z2, ifir, isec, ithi,          &
     &                  mhalf, mfull, isclp, iox, isgox, ikox,          &
     &                  ioy, isgoy, ikoy, ioz, isgoz, ikoz)
!c
      integer(8) :: x(:), y(:), z(:), x2(:), y2(:), z2(:)
      integer(8) :: iox(:),ioy(:), ioz(:)
      integer(8) :: isclp(:)
      integer*8 ifir, isec, ithi
      integer*8 mhalf, mfull, nkmax
      parameter (nkmax = 30)
      integer*8 io(nkmax), iu(nkmax), iv(nkmax)
      integer*8 ix2(nkmax), iy2(nkmax), iz2(nkmax)
      integer*8 ix3(nkmax), iy3(nkmax), iz3(nkmax)
      integer*8 ixf(nkmax), iyf(nkmax), izf(nkmax)
      integer*8 ixfiw, iyfiw, izfiw, ixsew, iysew, izsew
      integer*8 ixthw, iythw, izthw
      integer*8 ixfi2, iyfi2, izfi2, ixse2, iyse2, izse2
      integer*8 ixth2, iyth2, izth2
      integer*8 isgxf, isgyf, isgzf, ikxf, ikyf, ikzf
      integer*8 isgx2, isgy2, isgz2, ikx2, iky2, ikz2
      integer*8 isgx3, isgy3, isgz3, ikx3, iky3, ikz3
      integer*8 isgox, ikox, isgoy, ikoy, isgoz, ikoz
      integer*8 isgo, isgu, isgv, iko, iku, ikv
!c
      ixfiw = x(ifir)
      iyfiw = y(ifir)
      izfiw = z(ifir)
      ixsew = x(isec)
      iysew = y(isec)
      izsew = z(isec)
      ixthw = x(ithi)
      iythw = y(ithi)
      izthw = z(ithi)
!c
      ixfi2 = x2(ifir)
      iyfi2 = y2(ifir)
      izfi2 = z2(ifir)
      ixse2 = x2(isec)
      iyse2 = y2(isec)
      izse2 = z2(isec)
      ixth2 = x2(ithi)
      iyth2 = y2(ithi)
      izth2 = z2(ithi)
!c
      call decmp2(ixf, isgxf, ikxf, ixfiw, ixfi2, mhalf, mfull, isclp)
      call decmp2(iyf, isgyf, ikyf, iyfiw, iyfi2, mhalf, mfull, isclp)
      call decmp2(izf, isgzf, ikzf, izfiw, izfi2, mhalf, mfull, isclp)
!c
      call decmp2(io, isgo, iko, ixsew, ixse2, mhalf, mfull, isclp)
      call muldif(io, ixf, ix2, isgo, isgxf, isgx2, iko, ikxf, ikx2,    &
     &              nkmax, mhalf)
      call decmp2(io, isgo, iko, iysew, iyse2, mhalf, mfull, isclp)
      call muldif(io, iyf, iy2, isgo, isgyf, isgy2, iko, ikyf, iky2,    &
     &              nkmax, mhalf)
      call decmp2(io, isgo, iko, izsew, izse2, mhalf, mfull, isclp)
      call muldif(io, izf, iz2, isgo, isgzf, isgz2, iko, ikzf, ikz2,    &
     &              nkmax, mhalf)
      call decmp2(io, isgo, iko, ixthw, ixth2, mhalf, mfull, isclp)
      call muldif(io, ixf, ix3, isgo, isgxf, isgx3, iko, ikxf, ikx3,    &
     &              nkmax, mhalf)
      call decmp2(io, isgo, iko, iythw, iyth2, mhalf, mfull, isclp)
      call muldif(io, iyf, iy3, isgo, isgyf, isgy3, iko, ikyf, iky3,    &
     &              nkmax, mhalf)
      call decmp2(io, isgo, iko, izthw, izth2, mhalf, mfull, isclp)
      call muldif(io, izf, iz3, isgo, isgzf, isgz3, iko, ikzf, ikz3,    &
     &              nkmax, mhalf)
!c
      call mulmul(iy2, iz3, iv, isgy2, isgz3, isgv, iky2, ikz3, ikv,    &
     &              nkmax, mhalf)
      call mulmul(iz2, iy3, iu, isgz2, isgy3, isgu, ikz2, iky3, iku,    &
     &              nkmax, mhalf)
      call muldif(iv, iu, iox, isgv, isgu, isgox, ikv, iku, ikox,       &
     &              nkmax, mhalf)
!c
      call mulmul(iz2, ix3, iv, isgz2, isgx3, isgv, ikz2, ikx3, ikv,    &
     &              nkmax, mhalf)
      call mulmul(ix2, iz3, iu, isgx2, isgz3, isgu, ikx2, ikz3, iku,    &
     &              nkmax, mhalf)
      call muldif(iv, iu, ioy, isgv, isgu, isgoy, ikv, iku, ikoy,       &
     &              nkmax, mhalf)
!c
      call mulmul(ix2, iy3, iv, isgx2, isgy3, isgv, ikx2, iky3, ikv,    &
     &              nkmax, mhalf)
      call mulmul(iy2, ix3, iu, isgy2, isgx3, isgu, iky2, ikx3, iku,    &
     &              nkmax, mhalf)
      call muldif(iv, iu, ioz, isgv, isgu, isgoz, ikv, iku, ikoz,       &
     &              nkmax, mhalf)
!c
      return
      end subroutine crossp

!=======================================================================

      subroutine ctrad(x, y, z, w, xctr, yctr, zctr, a, b, c, d,        &
     &                 epz, delaun, ipossi)
!c
      real(8) :: x(:), y(:), z(:), w(:)
      double precision epz, xctr, yctr, zctr
      double precision xm, ym, zm, xn, yn, zn, xu, yu, zu, xv, yv, zv
      double precision xw, yw, zw, xq, yq, zq, xe, ye, ze, xl, yl, zl
      double precision xt, yt, zt
      double precision norm, lambda, normu, normv, denom, dmax
      integer*8 a, b, c, d, ipossi
      logical delaun
!c
!c     initialize
!c
      ipossi = 0
!c
!c     find midpoints of edges ac and ab
!c
      xm = (x(a) + x(c)) / 2.0d0
      ym = (y(a) + y(c)) / 2.0d0
      zm = (z(a) + z(c)) / 2.0d0
!c
      xn = (x(a) + x(b)) / 2.0d0
      yn = (y(a) + y(b)) / 2.0d0
      zn = (z(a) + z(b)) / 2.0d0
!c
!c     compute edge vectors u and v for edges ac and ab
!c
      xu = x(c) - x(a)
      yu = y(c) - y(a)
      zu = z(c) - z(a)
!c
      xv = x(b) - x(a)
      yv = y(b) - y(a)
      zv = z(b) - z(a)
!c
!c     compute lengths of u and v
!c
      normu = dsqrt(xu**2 + yu**2 + zu**2)
      normv = dsqrt(xv**2 + yv**2 + zv**2)
      if(normu.lt.epz .or. normv.lt.epz) then
         ipossi = 1
         go to 1000
      endif
      dmax = dmax1(normu,normv)
!c
!c     find perpendicular to facet abc of tetrahedron
!c
      xw = yu * zv - zu * yv
      yw = -xu * zv + zu * xv
      zw = xu * yv - yu * xv
!c
!c     test whether edges ac, ab are colinear
!c
      norm = dsqrt(xw**2 + yw**2 + zw**2)/dmax
      if(norm .lt. epz)then
         ipossi = 1
         go to 1000
      endif
      xw = xw/normu
      yw = yw/normu
      zw = zw/normu
!c
!c     normalize u and v
!c
      xu = xu / normu
      yu = yu / normu
      zu = zu / normu
      xv = xv / normv
      yv = yv / normv
      zv = zv / normv
!c
!c     compute orthogonal center of edge ac
!c
      if(.not.delaun)then
         lambda = ((w(a)-w(c))/normu)/2.0d0
         xm = xm + lambda*xu
         ym = ym + lambda*yu
         zm = zm + lambda*zu
      endif
!c
!c     compute orthogonal center of edge ab
!c
      if(.not.delaun)then
         lambda = ((w(a)-w(b))/normv)/2.0d0
         xn = xn + lambda*xv
         yn = yn + lambda*yv
         zn = zn + lambda*zv
      endif
!c
!c     find perpendicular to edge v in plane that contains facet abc
!c
      xq = yw * zv - zw * yv
      yq = -xw * zv + zw * xv
      zq = xw * yv - yw * xv
      norm = dsqrt(xq**2 + yq**2 + zq**2)
      if(norm.lt.epz) then
         ipossi = 1
         go to 1000
      endif
!c
!c     compute orthogonal center of facet abc
!c
      denom = xu*xq + yu*yq + zu*zq
      if(denom .gt. -epz .and. denom .lt. epz) then
         ipossi = 1
         go to 1000
      endif
      lambda = (xu*(xm-xn) + yu*(ym-yn) + zu*(zm-zn)) / denom
!c
      xe = xn + lambda*xq
      ye = yn + lambda*yq
      ze = zn + lambda*zq
!c
!c     compute edge vector t for edge ad
!c
      xl = (x(a) + x(d)) / 2.0d0
      yl = (y(a) + y(d)) / 2.0d0
      zl = (z(a) + z(d)) / 2.0d0
!c
      xt = x(d) - x(a)
      yt = y(d) - y(a)
      zt = z(d) - z(a)
      norm = dsqrt(xt**2 + yt**2 + zt**2)
      if(norm .lt. epz) then
         ipossi = 1
         go to 1000
      endif
      xt = xt / norm
      yt = yt / norm
      zt = zt / norm
!c
!c     compute orthogonal center of edge ad
!c
      if(.not.delaun)then
         lambda = ((w(a)-w(d))/norm)/2.0d0
         xl = xl + lambda*xt
         yl = yl + lambda*yt
         zl = zl + lambda*zt
      endif
!c
!c     compute orthogonal center of tetrahedron
!c
      denom = xt*xw + yt*yw + zt*zw
      if(denom .gt. -epz .and. denom .lt. epz) then
         ipossi = 1
         go to 1000
      endif
      lambda = (xt*(xl-xe) + yt*(yl-ye) + zt*(zl-ze)) / denom
!c
      xctr = xe + lambda*xw
      yctr = ye + lambda*yw
      zctr = ze + lambda*zw
!c
 1000 continue
      return
      end subroutine ctrad

!=======================================================================

      subroutine decmp2(ia, isga, ika, iwi, iwi2, mhalf, mfull, isclp)
!c
      integer(8) :: ia(:),isclp(:)
      integer*8 isga, ika, iwi, iwi2, mhalf, mfull
      integer*8 nkmax
      parameter (nkmax = 30)
      integer*8 iu(nkmax), io(nkmax), isgu, isgo, iku, iko, isgcl, ikcl
!c
      call decomp(ia, isga, iwi, mhalf)
      ika = 2
      if(iwi2.ne.0) then
         isgcl = 1
         ikcl = 2
         call mulmul(ia, isclp, iu, isga, isgcl, isgu, ika, ikcl,       &
     &                 iku, nkmax, mhalf) !ia*isclp=iu
         if(iwi2.eq.mfull) iwi2 = 0
      !  they doing this because they use iwi2=0 or /=0 to distinguish
      !  whether a number large than dfill or not!
      !  so, after they treat it as a number>dfill
      !  they turn it back to 0 which it should be
         call decomp(io, isgo, iwi2, mhalf)
         isgo = -isgo !this make muldif doing plus rather than substract
         iko = 2
         call muldif(iu, io, ia, isgu, isgo, isga, iku, iko, ika,       &
     &                 nkmax, mhalf) !iu-io=ia
      endif
!c
      return
      end subroutine decmp2

!=======================================================================

      subroutine decomp(ia, isga, iwi, mhalf)
!c
      integer(8) :: ia(:)
      integer*8 isga, iwi, mhalf, ivi
!c
      if(iwi.gt.0) then
         isga = 1
         ivi = iwi
      elseif(iwi.lt.0) then
         isga =-1
         ivi = -iwi
      else
         isga = 0
         ia(1) = 0
         ia(2) = 0
         return
      endif
      ia(2) = ivi/mhalf
      ia(1) = ivi - ia(2)*mhalf
!c
      return
      end subroutine decomp

!=======================================================================

      subroutine delchk(tetra, icon, ifl, xi, yi, zi, wi, x, y, z, w,   &
     &                  x2, y2, z2, w2, idmax, delaun, mhalf, mfull,    &
     &                  isclp, isclw, isclr, epz)
!c
      real(8) :: xi(:), yi(:), zi(:), wi(:)
      integer(8) :: x(:), y(:), z(:), w(:), x2(:), y2(:), z2(:), w2(:)
      integer(8) :: icon(:,:), ifl(:)
      integer(8) :: isclp(:), isclw(:), isclr(:)
      double precision epz, tdist, xctr, yctr, zctr
      integer*8 idmax, mhalf, mfull, i, j, k, isite, ipossi, itide
      integer*8 ikon(8,1), site1, site2, site3, tetra
      integer*8 opvert, a, b, c, d, adj,one
      logical delaun
!c
!c     initialize
!c
      idmax = 0
      one = 1
!c
!c     test all tetra with ifl(tetra)=1 for the Regular/Delaunay property
!c
      do 200 i = 1, tetra
         if(ifl(i) .ne. 1) go to 200
         a = icon(5,i)
         b = icon(6,i)
         c = icon(7,i)
         d = icon(8,i)
         do 100 j = 1, 4
            adj = icon(j,i)
            if (adj .eq. 0) stop 2610
            if(ifl(adj).ne.1) go to 100
            if(adj.gt.i) go to 100
            do 50 k = 1, 8
               ikon(k,1) = icon(k,i)
   50       continue
            isite = icon(j+4,i)
            call sitord (ikon, isite, one)
!c
            site1 = ikon(6,1)
            site2 = ikon(7,1)
            site3 = ikon(8,1)
            call reordr (icon, site1, site2, adj)
            if (icon(7,adj) .ne. site3) stop 2620
            if (icon(4,adj) .ne. i) stop 2630
!c
            opvert = icon(8,adj)
            call ctrad(xi, yi, zi, wi, xctr, yctr, zctr, site1, site2,  &
     &                 site3, opvert, epz, delaun, ipossi)
            if(ipossi.eq.1) go to 70
            call bisphr(xi, yi, zi, wi, isite, site1, epz, xctr, yctr,  &
     &                  zctr, tdist, delaun, ipossi)
            if(ipossi.eq.1) go to 70
            if(tdist.le.0.0d0) go to 100
            go to 80
   70       continue
            call iqsign(x, y, z, w, x2, y2, z2, w2, a, b, c, d,         &
     &      opvert, mhalf, mfull, isclp, isclw, isclr, delaun, itide)
            if(itide.ge.0) go to 100
   80       continue
            idmax = idmax+1
  100    continue
  200 continue
!c
      return
      end subroutine delchk

!=======================================================================

      subroutine detrm0(iq, ix2, iy2, ix3, iy3, isgq, isgx2, isgy2,     &
     &                  isgx3, isgy3, ikq, ikx2, iky2, ikx3, iky3,      &
     &                  io, isgo, iko, mhalf)
!c
      integer(8) :: io(:),iq(:),ix2(:),iy2(:),ix3(:),iy3(:)
      integer*8 nkmax
      parameter (nkmax = 30)
      integer*8 iu(nkmax), iv(nkmax), iw(nkmax)
      integer*8 isgq, isgx2, isgy2, isgx3, isgy3
      integer*8 ikq, ikx2, iky2, ikx3, iky3, isgo, iko, mhalf
      integer*8 isgu, isgv, isgw, iku, ikv, ikw
!c
      call mulmul(iq, ix2, iv, isgq, isgx2, isgv, ikq, ikx2, ikv,       &
     &              nkmax, mhalf)
      call mulmul(iv, iy3, iu, isgv, isgy3, isgu, ikv, iky3, iku,       &
     &              nkmax, mhalf)
      call mulmul(iq, iy2, iv, isgq, isgy2, isgv, ikq, iky2, ikv,       &
     &              nkmax, mhalf)
      call mulmul(iv, ix3, iw, isgv, isgx3, isgw, ikv, ikx3, ikw,       &
     &              nkmax, mhalf)
      call muldif(iu, iw, io, isgu, isgw, isgo, iku, ikw, iko,          &
     &              nkmax, mhalf)
!c
      return
      end subroutine detrm0

!=======================================================================

      subroutine detrm1(iq, ix2, iy2, iz2, ix3, iy3, iz3,               &
     &                  isgq, isgx2, isgy2, isgz2, isgx3, isgy3, isgz3, &
     &                  ikq, ikx2, iky2, ikz2, ikx3, iky3, ikz3,        &
     &                  io, isgo, iko, mhalf)
!c
      integer(8) ::io(:),iq(:),ix2(:),iy2(:),iz2(:),ix3(:),iy3(:),iz3(:)
      integer*8 nkmax
      parameter (nkmax = 30)
      integer*8 iv(nkmax), iw(nkmax)
      integer*8 isgq, isgx2, isgy2, isgz2, isgx3, isgy3, isgz3
      integer*8 ikq, ikx2, iky2, ikz2, ikx3, iky3, ikz3
      integer*8 isgo, isgv, isgw, iko, ikv, ikw, mhalf
!c
      call mulmul(iq, ix2, iv, isgq, isgx2, isgv, ikq, ikx2, ikv,       &
     &              nkmax, mhalf)
      call mulmul(iv, ix3, io, isgv, isgx3, isgo, ikv, ikx3, iko,       &
     &              nkmax, mhalf)
!c
      call mulmul(iq, iy2, iv, isgq, isgy2, isgv, ikq, iky2, ikv,       &
     &              nkmax, mhalf)
      call mulmul(iv, iy3, iw, isgv, isgy3, isgw, ikv, iky3, ikw,       &
     &              nkmax, mhalf)
      isgw =-isgw
      call muldif(io, iw, iv, isgo, isgw, isgv, iko, ikw, ikv,          &
     &              nkmax, mhalf)
!c
      call mulmul(iq, iz2, io, isgq, isgz2, isgo, ikq, ikz2, iko,       &
     &              nkmax, mhalf)
      call mulmul(io, iz3, iw, isgo, isgz3, isgw, iko, ikz3, ikw,       &
     &              nkmax, mhalf)
      isgw =-isgw
      call muldif(iv, iw, io, isgv, isgw, isgo, ikv, ikw, iko,          &
     &              nkmax, mhalf)
!c
      return
      end subroutine detrm1

!=======================================================================

      subroutine detrm2(iq2, ix2, iy2, iz2, isgq2, isgx2, isgy2, isgz2, &
     &                  iq3, ix3, iy3, iz3, isgq3, isgx3, isgy3, isgz3, &
     &                  ikq2, ikx2, iky2, ikz2, ikq3, ikx3, iky3, ikz3, &
     &                  io, isgo, iko, mhalf)
!c
      integer(8) :: io(:)
      integer(8) :: iq2(:), iq3(:)
      integer(8) :: ix2(:), iy2(:), iz2(:), ix3(:), iy3(:), iz3(:)
      integer*8 nkmax
      parameter (nkmax = 30)
      integer*8 ip(nkmax), ir(nkmax), iv(nkmax), iw(nkmax)
      integer*8 isgq2, isgx2, isgy2, isgz2, isgq3, isgx3, isgy3, isgz3
      integer*8 ikq2, ikx2, iky2, ikz2, ikq3, ikx3, iky3, ikz3
      integer*8 isgo, isgp, isgr, isgv, isgw, iko, ikp, ikr, ikv, ikw
      integer*8 mhalf
!c
      call detrm0(iq2, ix2, iy2, ix3, iy3, isgq2, isgx2, isgy2,         &
     &            isgx3, isgy3, ikq2, ikx2, iky2, ikx3, iky3,           &
     &            iv, isgv, ikv, mhalf)
      call mulmul(iv, iy3, io, isgv, isgy3, isgo, ikv, iky3, iko,       &
     &              nkmax, mhalf)
!c
      call detrm0(iq2, iz2, ix2, iz3, ix3, isgq2, isgz2, isgx2,         &
     &            isgz3, isgx3, ikq2, ikz2, ikx2, ikz3, ikx3,           &
     &            iv, isgv, ikv, mhalf)
      call mulmul(iv, iz3, ip, isgv, isgz3, isgp, ikv, ikz3, ikp,       &
     &              nkmax, mhalf)
!c
      call muldif(io, ip, ir, isgo, isgp, isgr, iko, ikp, ikr,          &
     &              nkmax, mhalf)
!c
      call detrm0(iq3, ix2, iy2, ix3, iy3, isgq3, isgx2, isgy2,         &
     &            isgx3, isgy3, ikq3, ikx2, iky2, ikx3, iky3,           &
     &            iv, isgv, ikv, mhalf)
      call mulmul(iv, iy2, io, isgv, isgy2, isgo, ikv, iky2, iko,       &
     &              nkmax, mhalf)
!c
      call detrm0(iq3, iz2, ix2, iz3, ix3, isgq3, isgz2, isgx2,         &
     &            isgz3, isgx3, ikq3, ikz2, ikx2, ikz3, ikx3,           &
     &            iv, isgv, ikv, mhalf)
      call mulmul(iv, iz2, ip, isgv, isgz2, isgp, ikv, ikz2, ikp,       &
     &              nkmax, mhalf)
!c
      call muldif(io, ip, iw, isgo, isgp, isgw, iko, ikp, ikw,          &
     &              nkmax, mhalf)
      call muldif(ir, iw, io, isgr, isgw, isgo, ikr, ikw, iko,          &
     &              nkmax, mhalf)
!c
      return
      end subroutine detrm2

!=======================================================================

      subroutine detrm3(ix2, iy2, iz2, isgx2, isgy2, isgz2,             &
     &                  ix3, iy3, iz3, isgx3, isgy3, isgz3,             &
     &                  ix4, iy4, iz4, isgx4, isgy4, isgz4,             &
     &                  ikx2, ikx3, ikx4, iky2, iky3, iky4,             &
     &                  ikz2, ikz3, ikz4, io, isgo, iko, mhalf)
!c
      integer(8) :: io(:)
      integer(8) :: ix2(:), iy2(:), iz2(:), ix3(:), iy3(:), iz3(:)
      integer(8) :: ix4(:), iy4(:), iz4(:)
      integer*8 nkmax
      parameter (nkmax = 30)
      integer*8 iu(nkmax), iv(nkmax), iw(nkmax)
      integer*8 isgx2, isgy2, isgz2, isgx3, isgy3, isgz3
      integer*8 isgx4, isgy4, isgz4, ikx2, ikx3, ikx4, iky2, iky3, iky4
      integer*8 ikz2, ikz3, ikz4, isgo, iko, mhalf
      integer*8 isgu, isgv, isgw, iku, ikv, ikw
!c
      call mulmul(ix3, iy4, iv, isgx3, isgy4, isgv, ikx3, iky4, ikv,    &
     &              nkmax, mhalf) !iv=ix3*iy4
      call mulmul(ix4, iy3, iu, isgx4, isgy3, isgu, ikx4, iky3, iku,    &
     &              nkmax, mhalf) !iu=ix4*iy3
      call muldif(iv, iu, iw, isgv, isgu, isgw, ikv, iku, ikw,          &
     &              nkmax, mhalf) !iw=ix3*iy4-ix4*iy3
      call mulmul(iw, iz2, io, isgw, isgz2, isgo, ikw, ikz2, iko,       &
     &              nkmax, mhalf) !io=iz2*(ix3*iy4-ix4*iy3)
!c
      call mulmul(ix2, iy4, iv, isgx2, isgy4, isgv, ikx2, iky4, ikv,    &
     &              nkmax, mhalf) !iv=ix2*iy4
      call mulmul(ix4, iy2, iu, isgx4, isgy2, isgu, ikx4, iky2, iku,    &
     &              nkmax, mhalf) !iu=ix4*iy2
      call muldif(iv, iu, iw, isgv, isgu, isgw, ikv, iku, ikw,          &
     &              nkmax, mhalf) !iw=ix2*iy4-ix4*iy2
      call mulmul(iw, iz3, iu, isgw, isgz3, isgu, ikw, ikz3, iku,       &
     &              nkmax, mhalf) !iu=iz3*(ix2*iy4-ix4*iy2)
      call muldif(io, iu, iw, isgo, isgu, isgw, iko, iku, ikw,          &
     &              nkmax, mhalf)
      !iw=iz2*(ix3*iy4-ix4*iy3)-iz3*(ix2*iy4-ix4*iy2)
!c
      call mulmul(ix3, iy2, iv, isgx3, isgy2, isgv, ikx3, iky2, ikv,    &
     &              nkmax, mhalf) !iv=ix3*iy2
      call mulmul(ix2, iy3, iu, isgx2, isgy3, isgu, ikx2, iky3, iku,    &
     &              nkmax, mhalf) !iu=ix2*iy3
      call muldif(iv, iu, io, isgv, isgu, isgo, ikv, iku, iko,          &
     &              nkmax, mhalf) !io=ix3*iy2-ix2*iy3
      call mulmul(io, iz4, iu, isgo, isgz4, isgu, iko, ikz4, iku,       &
     &              nkmax, mhalf) !iu=iz4*(ix3*iy2-ix2*iy3)
      call muldif(iw, iu, io, isgw, isgu, isgo, ikw, iku, iko,          &
     &              nkmax, mhalf)
      !io=iz2*(ix3*iy4-ix4*iy3)-iz3*(ix2*iy4-ix4*iy2)-iz4*(ix3*iy2-ix2*iy3)
      !  =iz2*(ix3*iy4-ix4*iy3)+iz3*(ix4*iy2-ix2*iy4)+iz4*(ix2*iy3-ix3*iy2)
      !  =[z2,z3,z4]dot[x2,x3,x4]cross[y2,y3,y4]
      !  =volume of [1,2,3,4]=volume of vector[2,3,4]
!c
      return
      end subroutine detrm3

!=======================================================================

      subroutine doubnm(io, isgo, iko, r215, dnum)
!c
      integer(8) :: io(:)
      double precision dnum, r215, rpwr
      integer*8 isgo, iko, i
!c
      if(isgo.eq.0) then
         dnum = dble(0)
         go to 1000
      else
         if(iko .lt. 2) stop 6010
         if(iko .gt. 68) stop 6020
         rpwr = dble(1)
         dnum = dble(io(1))
         do 100 i = 2, iko
            rpwr = rpwr*r215 !215 means 2**15....
            dnum = dnum + dble(io(i))*rpwr
  100    continue
      endif
      if(isgo.lt.0) dnum = -dnum
!c
 1000 continue
      return
      end subroutine doubnm

!=======================================================================

      subroutine dstnce(x, y, z, p, q, r, epz, k, dist, ipossi)
!c
      integer*8 p, q, r, k, ipossi
      real(8) :: x(:), y(:), z(:)
      double precision epz, dist
      double precision xvec1, yvec1, zvec1, xvec2, yvec2, zvec2
      double precision xvec3, yvec3, zvec3, dst1, dst2, dst3
      double precision dotx, doty, dotz, dmax, dlun
      double precision xvecp, yvecp, zvecp, dstp, dlen
!c
      ipossi = 0
      xvec1 = x(q) - x(p)
      yvec1 = y(q) - y(p)
      zvec1 = z(q) - z(p)
      xvec2 = x(r) - x(p)
      yvec2 = y(r) - y(p)
      zvec2 = z(r) - z(p)
      xvec3 = x(q) - x(r)
      yvec3 = y(q) - y(r)
      zvec3 = z(q) - z(r)
      dst1=dsqrt(xvec1**2+yvec1**2+zvec1**2)
      dst2=dsqrt(xvec2**2+yvec2**2+zvec2**2)
      dst3=dsqrt(xvec3**2+yvec3**2+zvec3**2)
      if(dst1.lt.epz .or. dst2.lt.epz .or. dst3.lt.epz) then
         ipossi = 1
         go to 1000
      endif
      dmax = dmax1(dst1,dst2,dst3)
!c
      dotx = yvec1 * zvec2 - yvec2 * zvec1
      doty = - xvec1 * zvec2 + xvec2 * zvec1
      dotz = xvec1 * yvec2 - xvec2 * yvec1
      dlen = dsqrt (dotx**2 + doty**2 + dotz**2)
      if(dlen.lt.epz .or. dlen/dmax.lt.epz)then
         ipossi = 1
         go to 1000
      endif
!c
      xvecp = x(k) - x(p)
      yvecp = y(k) - y(p)
      zvecp = z(k) - z(p)
      dstp=dsqrt(xvecp**2+yvecp**2+zvecp**2)
      if(dstp.lt.epz) then
         ipossi = 1
         go to 1000
      endif
!c
      dlun=dstp*dmax
      dlun=dmax1(dlen,dlun)
      dist=(xvecp*dotx+yvecp*doty+zvecp*dotz)/dlun
      if(dist.gt.-epz .and. dist.lt.epz)then
         ipossi = 1
      endif
!c
 1000 continue
      return
      end subroutine dstnce

!=======================================================================

      subroutine edgins(k, x, y, z, w, x2, y2, z2, w2, icon, ih, ihn,   &
     &                  nhmax, nvmax, tetra, curr, is, side1, side2,    &
     &                  ifl, newtts, ired, delaun, flphis,              &
     &                  mhalf, mfull, isclp, isclw, isclr)
!c
      integer(8) :: x(:), y(:), z(:), w(:), x2(:), y2(:), z2(:), w2(:)
      integer(8) :: icon(:,:), is(:), ifl(:), ih(:)
      integer*8 ihn, nhmax, nvmax, newtts, ired
      integer(8) :: isclp(:), isclw(:), isclr(:), mhalf, mfull
      integer*8 tetra, curr, side1, side2, fndsit
      integer*8 site1, site2, prvtet, adj, firtet
      logical delaun, flphis
      integer*8 k, i, itide, new1, new2, nel1, nel2, now1, now2, j
!c
!c     find endpoints of edge
!c
      site1 = 0
      fndsit = 0
      do 100 i = 5, 8
         if (fndsit .eq. 0) then
            if (i .eq. (side1+4) .or. i .eq. (side2+4)) then
               goto 100
            else
               site1 = icon(i,curr)
               fndsit = 1
            endif
         else
            if (i .eq. (side1+4) .or. i .eq. (side2+4)) then
               goto 100
            else
               site2 = icon(i,curr)
               goto 150
            endif
         endif
  100 continue
      stop 1510
  150 continue
!c
      if(delaun) go to 160
      if(site1.le.8 .or. site2.le.8) go to 160
      call iqsig1(x, y, z, w, x2, y2, z2, w2, site1, site2, k,          &
     &            mhalf, mfull, isclp, isclw, isclr, delaun, itide)
      if(itide .le. 0) go to 160
      is(k) = -3
      ired = 1
      go to 1000
!c
!c     order vertices of tetrahedra around edge
!c
  160 continue
      firtet = curr
!c
  163 continue
      call reordr(icon, site1, site2, curr)
      curr = icon(3,curr)
      if(curr.ne.firtet) go to 163
!c
      ired = 0
      if(flphis) then
         new1 = tetra + 1
         new2 = tetra + 2
      else
         if(ihn.eq.0) then
            new1 = tetra+1
            new2 = tetra+2
         elseif(ihn.eq.1) then
            new1 = ih(1)
            new2 = tetra+1
         else
            new1 = ih(ihn)
            new2 = ih(ihn-1)
         endif
      endif
      nel1 = new1
      nel2 = new2
      newtts = 0
!c
      is(k) = new1
      is(site1) = new2
      is(site2) = new1
!c
!c     create 2 new tetra
!c
  175 continue
      if(flphis) then
         now1 = tetra + 1
         now2 = tetra + 2
         tetra = now2
         if(tetra .gt. nvmax) stop 1520
      else
         if(ihn.eq.0) then
            now1 = tetra+1
            tetra = now1
            if(tetra .gt. nvmax) stop 1530
         else
            now1 = ih(ihn)
            ihn = ihn-1
         endif
         if(ihn.eq.0) then
            now2 = tetra+1
            tetra = now2
            if(tetra .gt. nvmax) stop 1540
         else
            now2 = ih(ihn)
            ihn = ihn-1
         endif
      endif
!c
      newtts = newtts + 1
      ifl(newtts) = now1
      newtts = newtts + 1
      ifl(newtts) = now2
!c
      do 180 i = 1, 8
         icon(i,now1) = icon(i,curr)
         icon(i,now2) = icon(i,curr)
  180 continue
!c
      icon(2,now1) = now2
      icon(5,now1) = k
      icon(1,now2) = now1
      icon(6,now2) = k
!c
      icon(3,nel1) = now1
      icon(3,nel2) = now2
      icon(4,now1) = nel1
      icon(4,now2) = nel2
      call sitord (icon, k, now1)
      call sitord (icon, k, now2)
!c
!c     update is(:)
!c
      is(icon(7,curr)) = now1
!c
!c     update neighbors of curr
!c
      do 300 i = 1, 2
         adj = icon(i,curr)
         if (adj .ne. 0) then
            do 250 j = 1, 4
               if (icon(j,adj) .eq. curr) then
                  if (i .eq. 1) then
                     icon(j,adj) = now1
                  else
                     icon(j,adj) = now2
                  endif
                  goto 300
               endif
  250       continue
            stop 1550
         endif
  300 continue
!c
      prvtet = curr
      curr = icon(3,curr)
!c
!c     show children of old tetra
!c
      if(flphis) then
         icon(5,prvtet) = - icon(5,prvtet)
         icon(1,prvtet) = now1
         icon(2,prvtet) = now2
         icon(3,prvtet) = -curr
         icon(4,prvtet) = -icon(4,prvtet)
      else
         icon(5,prvtet) = - icon(5,prvtet)
         ihn = ihn+1
         if(ihn.gt.nhmax) stop 1560
         ih(ihn) = prvtet
      endif
!c
!c     go to next tetrahedron until we're back at firtet
!c
      if (curr .ne. firtet) then
         nel1 = now1
         nel2 = now2
         go to 175
      else
         icon(4,new1) = now1
         icon(2,new2) = now2
         icon(3,now1) = new1
         icon(3,now2) = new2
      endif
!c
 1000 continue
      return
      end subroutine edgins

!=======================================================================

      subroutine fcedge(x, y, z, x2, y2, z2, xc, yc, zc, itype, ileft,  &
     &                  k, icon, iscur, imist, ivnxt, site0, site1,     &
     &                  side1, side2, mhalf, mfull, isclp, ITCHK)
!c
      integer(8) :: x(:), y(:), z(:), x2(:), y2(:), z2(:), xc(:), yc(:),&
     &         zc(:)
      integer(8) :: icon(:,:),isclp(:)
      integer*8 mhalf, mfull
      integer*8 itype, ileft, k, iscur, imist, ivnxt
      integer*8 iside(4), site0, site1, site2, site3, side1, side2
      integer*8 isnow, idut, iasign, idot0, itchk, ipout
!c
!c     find intersecting facet
!c
      call reordr(icon, site0, site1, iscur)
      site2 = icon(7,iscur)
      site0 = icon(8,iscur)
      isnow = abs(icon(1,iscur))
!c
  300 continue
      ITCHK = ITCHK+1
      if(isnow.le.0.or.isnow.gt.ivnxt) stop 961
      if(isnow.eq.iscur) stop 963
      call reordr(icon, site0, site1, isnow)
      site3 = icon(8,isnow)
      if(site1.gt.8 .and. site2.gt.8 .and. site3.gt.8) then
         call ipsign(x, y, z, x2, y2, z2, site1, site3, site2, k,       &
     &               mhalf, mfull, isclp, idut)
      else
         call artsig(x, y, z, x2, y2, z2, xc, yc, zc, site1, site3,     &
     &               site2, k, mhalf, mfull, isclp, idut)
      endif
      if(idut.ge.0) go to 400
      site0 = site3
      isnow = abs(icon(1,isnow))
      go to 300
!c
  400 continue
      iscur = isnow
!c
!c     determine whether point k is in tetrahedron
!c
      if(site0.le.8 .or. site1.le.8 .or. site2.le.8 .or. site3.le.8)    &
     &   go to 500
!c
      call ipsign(x, y, z, x2, y2, z2, site1, site0, site3, k,          &
     &            mhalf, mfull, isclp, ipout)
      iside(3) = ipout
      call ipsign(x, y, z, x2, y2, z2, site2, site3, site0, k,          &
     &            mhalf, mfull, isclp, ipout)
      iside(2) = ipout
!c
      if(iside(2).lt.0 .or. iside(3).lt.0) go to 600
!c
  450 continue
      iside(1) = idut
      iside(4) = 1
      call pntype(iside, itype, side1, side2)
      go to 1000
!c
!c     there is at least one artificial vertex
!c
  500 continue
      call artsig(x, y, z, x2, y2, z2, xc, yc, zc, site1, site0, site3, &
     &            k, mhalf, mfull, isclp, iasign)
      iside(3) = iasign
      call artsig(x, y, z, x2, y2, z2, xc, yc, zc, site2, site3, site0, &
     &            k, mhalf, mfull, isclp, iasign)
      iside(2) = iasign
!c
      if(iside(2).lt.0 .or. iside(3).lt.0) go to 600
      go to 450
!c
!c     k is not in tetrahedron but ray intersects one of the facets
!c     of the tetrahedron opposite the edge
!c
  600 continue
      if(site0.gt.8 .and. site3.gt.8) then
         call ipsign(x, y, z, x2, y2, z2, ileft, site0, site3, k,       &
     &               mhalf, mfull, isclp, idot0)
      else
         call artsig(x, y, z, x2, y2, z2, xc, yc, zc, ileft, site0,     &
     &               site3, k, mhalf, mfull, isclp, idot0)
      endif
      if(idot0.gt.0)then
         if(idut.gt.0) then
            itype = -2
            imist = site1
         else
            itype = -4
            imist = site2
         endif
      elseif(idot0.lt.0)then
         if(idut.gt.0) then
            itype = -2
            imist = site2
         else
            itype = -4
            imist = site1
         endif
      else
         if(idut.gt.0) then
            itype = -4
            imist = site0
         else
            itype = -3
            imist = site3
         endif
      endif
!c
 1000 continue
      return
      end subroutine fcedge

!=======================================================================

      subroutine fcfind(xi, yi, zi, x, y, z, x2, y2, z2, xc, yc, zc,    &
     &                  itype, ileft, k, ilift, imist, b, c, d,         &
     &                  side1, side2, mhalf, mfull, isclp, epz)
!c
      real(8) :: xi(:), yi(:), zi(:)
      integer(8) :: x(:), y(:), z(:), x2(:), y2(:), z2(:), xc(:), yc(:),&
     &         zc(:)
      double precision epz
      integer(8) :: isclp(:)
      integer*8 mhalf, mfull
      integer*8 itype, ileft, k, ilift, imist, iasign, ipout
      integer*8 idut1, idut2, idut3, idot1, idot2, idot3
      integer*8 iside(4), b, c, d, side1, side2
!c
!c     determine whether point k is in tetrahedron
!c
      if(b.le.8 .or. c.le.8 .or. d.le.8 .or. ilift.le.8) go to 100
!c
      call irsign(xi, yi, zi, x, y, z, x2, y2, z2, k, ilift, d, c,      &
     &            mhalf, mfull, isclp, epz, ipout)
      iside(2) = ipout
      call irsign(xi, yi, zi, x, y, z, x2, y2, z2, k, ilift, c, b,      &
     &            mhalf, mfull, isclp, epz, ipout)
      iside(3) = ipout
      call irsign(xi, yi, zi, x, y, z, x2, y2, z2, k, ilift, b, d,      &
     &            mhalf, mfull, isclp, epz, ipout)
      iside(4) = ipout
!c
      if(iside(2).lt.0 .or. iside(3).lt.0 .or. iside(4).lt.0) go to 200
!c
   50 continue
      iside(1) = 1
      call pntype(iside, itype, side1, side2)
      go to 1000
!c
!c     there is at least one artificial vertex
!c
  100 continue
      call artsig(x, y, z, x2, y2, z2, xc, yc, zc, ilift, d, c, k,      &
     &            mhalf, mfull, isclp, iasign)
      iside(2) = iasign
      call artsig(x, y, z, x2, y2, z2, xc, yc, zc, ilift, b, d, k,      &
     &            mhalf, mfull, isclp, iasign)
      iside(4) = iasign
      call artsig(x, y, z, x2, y2, z2, xc, yc, zc, ilift, c, b, k,      &
     &            mhalf, mfull, isclp, iasign)
      iside(3) = iasign
!c
      if(iside(2).lt.0 .or. iside(3).lt.0 .or. iside(4).lt.0) go to 200
      go to 50
!c
!c     k is not in tetrahedron
!c
!c     determine position of ilift with repect to current situation
!c
  200 continue
      if(b.le.8 .or. c.le.8 .or. d.le.8 .or. ilift.le.8) go to 300
!c
      call irsign(xi, yi, zi, x, y, z, x2, y2, z2, ileft, ilift, c,     &
     &            b, mhalf, mfull, isclp, epz, idut1)
      call irsign(xi, yi, zi, x, y, z, x2, y2, z2, ileft, ilift, d,     &
     &            c, mhalf, mfull, isclp, epz, idut2)
      call irsign(xi, yi, zi, x, y, z, x2, y2, z2, ileft, ilift, b,     &
     &            d, mhalf, mfull, isclp, epz, idut3)
!c
      if(idut1.le.0 .or. idut2.le.0 .or. idut3.le.0) go to 700
      go to 400
!c
!c     there is at least one artificial vertex
!c
  300 continue
      call artsig(x, y, z, x2, y2, z2, xc, yc, zc, ilift, c, b, ileft,  &
     &            mhalf, mfull, isclp, idut1)
      call artsig(x, y, z, x2, y2, z2, xc, yc, zc, ilift, d, c, ileft,  &
     &            mhalf, mfull, isclp, idut2)
      call artsig(x, y, z, x2, y2, z2, xc, yc, zc, ilift, b, d, ileft,  &
     &            mhalf, mfull, isclp, idut3)
!c
      if(idut1.le.0 .or. idut2.le.0 .or. idut3.le.0) go to 700
!c
!c     ilift, ileft, b, d, c, form a strictly convex set
!c
  400 continue
      if(b.le.8 .or. c.le.8 .or. d.le.8 .or. ilift.le.8) go to 500
!c
      call irsign(xi, yi, zi, x, y, z, x2, y2, z2, k, ileft, b,         &
     &            ilift, mhalf, mfull, isclp, epz, idot1)
      call irsign(xi, yi, zi, x, y, z, x2, y2, z2, k, ileft, c,         &
     &            ilift, mhalf, mfull, isclp, epz, idot2)
      call irsign(xi, yi, zi, x, y, z, x2, y2, z2, k, ileft, d,         &
     &            ilift, mhalf, mfull, isclp, epz, idot3)
      go to 600
!c
  500 continue
      call artsig(x, y, z, x2, y2, z2, xc, yc, zc, ileft, b, ilift, k,  &
     &            mhalf, mfull, isclp, idot1)
      call artsig(x, y, z, x2, y2, z2, xc, yc, zc, ileft, c, ilift, k,  &
     &            mhalf, mfull, isclp, idot2)
      call artsig(x, y, z, x2, y2, z2, xc, yc, zc, ileft, d, ilift, k,  &
     &            mhalf, mfull, isclp, idot3)
!c
  600 continue
      itype = -2
      if(idot1 .lt. 0 .and. idot2 .gt. 0) then
          imist = d
      elseif(idot2 .lt. 0 .and. idot3 .gt. 0) then
          imist = b
      elseif(idot3 .lt. 0 .and. idot1 .gt. 0) then
          imist = c
      elseif(idot1 .eq. 0 .and. idot2 .eq. 0 .and. idot3 .eq.0) then
          itype = -3
      elseif(idot1 .eq. 0) then
          itype = -4
          imist = b
      elseif(idot2 .eq. 0) then
          itype = -4
          imist = c
      elseif(idot3 .eq. 0) then
          itype = -4
          imist = d
      else
          stop 951
      endif
      go to 1000
!c
  700 continue
      if(idut1.le.0 .and. idut2.le.0 .and. idut3.le.0) stop 952
      itype = -2
      if(idut1.le.0 .and. idut2.le.0)then
         imist = c
      elseif(idut2.le.0 .and. idut3.le.0)then
         imist = d
      elseif(idut3.le.0 .and. idut1.le.0)then
         imist = b
      elseif(idut1.le.0)then
         if(d.gt.8 .and. ilift.gt.8) then
            call irsign(xi, yi, zi, x, y, z, x2, y2, z2, k, ileft, d,   &
     &                  ilift, mhalf, mfull, isclp, epz, idot3)
         else
            call artsig(x, y, z, x2, y2, z2, xc, yc, zc, ileft, d,      &
     &                  ilift, k, mhalf, mfull, isclp, idot3)
         endif
         if(idot3.gt.0)then
            imist = b
         elseif(idot3.lt.0)then
            imist = c
         else
            itype = -4
            imist = d
         endif
      elseif(idut2.le.0)then
         if(b.gt.8 .and. ilift.gt.8) then
            call irsign(xi, yi, zi, x, y, z, x2, y2, z2, k, ileft, b,   &
     &                  ilift, mhalf, mfull, isclp, epz, idot1)
         else
            call artsig(x, y, z, x2, y2, z2, xc, yc, zc, ileft, b,      &
     &                  ilift, k, mhalf, mfull, isclp, idot1)
         endif
         if(idot1.gt.0)then
            imist = c
         elseif(idot1.lt.0)then
            imist = d
         else
            itype = -4
            imist = b
         endif
      else
         if(c.gt.8 .and. ilift.gt.8) then
            call irsign(xi, yi, zi, x, y, z, x2, y2, z2, k, ileft, c,   &
     &                  ilift, mhalf, mfull, isclp, epz, idot2)
         else
            call artsig(x, y, z, x2, y2, z2, xc, yc, zc, ileft, c,      &
     &                  ilift, k, mhalf, mfull, isclp, idot2)
         endif
         if(idot2.gt.0)then
            imist = d
         elseif(idot2.lt.0)then
            imist = b
         else
            itype = -4
            imist = c
         endif
      endif
!c
 1000 continue
      return
      end subroutine fcfind

!=======================================================================

      subroutine fctest(xi, yi, zi, x, y, z, x2, y2, z2, xc, yc, zc,    &
     &                  itype, k, imist, a, b, c, d, side1, side2,      &
     &                  mhalf, mfull, isclp, epz)
!c
      real(8) :: xi(:), yi(:), zi(:)
      integer(8) :: x(:), y(:), z(:), x2(:), y2(:), z2(:), xc(:), yc(:),&
     &         zc(:)
      double precision epz
      integer(8) :: isclp(:)
      integer*8 itype, k, imist, mhalf, mfull, iasign, ipout
      integer*8 iside(4), a, b, c, d, side1, side2
!c
!c     determine whether ray with origin point a and through point k
!c     intersects facet of current tetrahedron opposite to point a
!c
      if(b.le.8 .or. c.le.8 .or. d.le.8) go to 100
!c
      call irsign(xi, yi, zi, x, y, z, x2, y2, z2, k, a, c, d,          &
     &            mhalf, mfull, isclp, epz, ipout)
      iside(2) = ipout
      call irsign(xi, yi, zi, x, y, z, x2, y2, z2, k, a, d, b,          &
     &            mhalf, mfull, isclp, epz, ipout)
      iside(3) = ipout
      call irsign(xi, yi, zi, x, y, z, x2, y2, z2, k, a, b, c,          &
     &            mhalf, mfull, isclp, epz, ipout)
      iside(4) = ipout
!c
      if(iside(2).lt.0 .or. iside(3).lt.0 .or. iside(4).lt.0) go to 1000
!c
!c     determine whether point k is in tetrahedron
!c
      call irsign(xi, yi, zi, x, y, z, x2, y2, z2, k, b, d, c,          &
     &            mhalf, mfull, isclp, epz, ipout)
      iside(1) = ipout
      if(iside(1).lt.0) go to 500
!c
   50 continue
      call pntype(iside, itype, side1, side2)
      go to 1000
!c
!c     there is at least one artificial vertex
!c
  100 continue
      call artsig(x, y, z, x2, y2, z2, xc, yc, zc, a, c, d, k,          &
     &            mhalf, mfull, isclp, iasign)
      iside(2) = iasign
      call artsig(x, y, z, x2, y2, z2, xc, yc, zc, a, d, b, k,          &
     &            mhalf, mfull, isclp, iasign)
      iside(3) = iasign
      call artsig(x, y, z, x2, y2, z2, xc, yc, zc, a, b, c, k,          &
     &            mhalf, mfull, isclp, iasign)
      iside(4) = iasign
!c
      if(iside(2).lt.0 .or. iside(3).lt.0 .or. iside(4).lt.0) go to 1000
!c
!c     determine whether point k is in tetrahedron
!c
      call artsig(x, y, z, x2, y2, z2, xc, yc, zc, b, d, c, k,          &
     &            mhalf, mfull, isclp, iasign)
      iside(1) = iasign
      if(iside(1).lt.0) go to 500
      go to 50
!c
!c     ray intersects facet but point k is not in tetrahedron
!c
  500 continue
!c
!c     ray intersects interior of facet
!c
      if(iside(2).gt.0 .and. iside(3).gt.0 .and. iside(4).gt.0) then
         itype = -2
         go to 1000
      endif
!c
      if(iside(2).eq.0 .and. iside(3).eq.0 .and. iside(4).eq.0) stop 931
!c
!c     ray intersects a vertex of facet
!c
      if (iside(2).eq.0 .and. iside(3).eq.0) then
         itype = -3
         imist = d
         go to 1000
      elseif (iside(2).eq.0 .and. iside(4).eq.0) then
         itype = -3
         imist = c
         go to 1000
      elseif (iside(3).eq.0 .and. iside(4).eq.0) then
         itype = -3
         imist = b
         go to 1000
      endif
!c
!c     ray intersects the interior of an edge of facet
!c
      itype = -4
      if (iside(2) .eq. 0) then
         imist = c
      elseif (iside(3) .eq. 0) then
         imist = d
      elseif (iside(4) .eq. 0) then
         imist = b
      else
         stop 932
      endif
!c
 1000 continue
      return
      end subroutine fctest

!=======================================================================

      subroutine flip21 (icon, k, now, adj, ifl, ih, ihn, nhmax,        &
     &                   flphis, tetra, newtts, is, zersid, nvmax)
!c
      integer(8) :: icon(:,:), ifl(:), is(:), ih(:)
      integer*8 k, now, ihn, nhmax, newtts, nvmax
      integer*8 adj, tetra, zersid, site1, site2, site3, site4
      logical flphis
      integer*8 nxtnow, nxtadj, initet, new1, nel1, neigh, j
      integer*8 lstnow, lstadj
!c
!c     reorder now
!c
      site1 = icon(zersid+4,now)
      call reordr(icon, k, site1, now)
      site2 = icon(7,now)
      site3 = icon(8,now)
!c
!c     reordr adj
!c
      call reordr(icon, site1, site2, adj)
!c
      nxtnow = icon(3,now)
      nxtadj = icon(2,adj)
      if(nxtnow.eq.nxtadj) stop 2310
!c
!c     go around edge to test for flipping
!c
  100 continue
      call reordr(icon, k, site1, nxtnow)
      call reordr(icon, site1, site3, nxtadj)
      if(icon(1,nxtnow).ne.nxtadj) go to 2000
      site3 = icon(8,nxtnow)
      nxtnow = icon(3,nxtnow)
      nxtadj = icon(2,nxtadj)
      if(nxtnow .eq. now) go to 200
      if(nxtadj .eq. adj) stop 2320
      go to 100
!c
!c     flip
!c
  200 continue
!c
      if(nxtadj .ne. adj) stop 2330
      if(flphis) then
         initet = tetra+1
      else
         if(ihn.eq.0) then
            initet = tetra+1
         else
            initet = ih(ihn)
         endif
      endif
      new1 = initet
      site4 = icon(8,adj)
!c
!c     go around edge for creating new tetrahedra
!c
  300 continue
      nel1 = new1
      if(flphis) then
         new1 = tetra + 1
         tetra = new1
         if(tetra .gt. nvmax) stop 2340
      else
         if(ihn.eq.0) then
            new1 = tetra+1
            tetra = new1
            if(tetra .gt. nvmax) stop 2350
         else
            new1 = ih(ihn)
            ihn = ihn-1
         endif
      endif
!c
      newtts = newtts + 1
      if(newtts.gt.nvmax) stop 2355
      ifl(newtts) = new1
!c
!c     create tetra
!c
      icon(1,new1) = icon(1,nxtadj)
      icon(2,new1) = icon(2,nxtnow)
      icon(3,nel1) = new1
      icon(4,new1) = nel1
      icon(5,new1) = k
      icon(6,new1) = site4
      icon(7,new1) = icon(7,nxtnow)
      icon(8,new1) = icon(8,nxtnow)
!c
!c     update is(:)
!c
      is(icon(8,new1)) = new1
!c
!c     update neighbor of nxtnow
!c
      neigh = icon(2,nxtnow)
      if(neigh .eq. 0) go to 400
      do 340 j = 1, 4
         if(icon(j,neigh) .eq. nxtnow) go to 360
  340 continue
      stop 2360
  360 continue
      icon(j,neigh) = new1
  400 continue
!c
!c     update neighbor of nxtadj
!c
      neigh = icon(1,nxtadj)
      if(neigh .eq. 0) go to 500
      do 440 j = 1, 4
         if(icon(j,neigh) .eq. nxtadj) go to 460
  440 continue
      stop 2370
  460 continue
      icon(j,neigh) = new1
  500 continue
!c
!c     show children of nxtnow, nxtadj
!c
      lstnow = nxtnow
      lstadj = nxtadj
      nxtnow = icon(3,lstnow)
      nxtadj = icon(2,lstadj)
!c
      if(flphis) then
         icon(5,lstnow) = -icon(5,lstnow)
         icon(1,lstnow) = new1
         icon(2,lstnow) = 0
         icon(3,lstnow) = -nxtnow
         icon(4,lstnow) = -icon(4,lstnow)
!c
         icon(5,lstadj) = -icon(5,lstadj)
         icon(1,lstadj) = new1
         icon(2,lstadj) = -nxtadj
         icon(3,lstadj) = -icon(3,lstadj)
         icon(4,lstadj) = 0
      else
         icon(5,lstnow) = -icon(5,lstnow)
         icon(5,lstadj) = -icon(5,lstadj)
         ihn = ihn+2
         if(ihn.gt.nhmax) stop 2380
         ih(ihn) = lstnow
         ih(ihn-1) = lstadj
      endif
!c
      if(nxtnow .ne. now) go to 300
      icon(3,new1) = initet
      icon(4,initet) = new1
      is(k) = new1
      is(site4) = new1
      is(site1) = -4
!c
 2000 continue
      return
      end subroutine flip21

!=======================================================================

      subroutine flip22 (icon, k, now, adj, ifl, ih, ihn, nhmax,        &
     &                   flphis, tetra, newtts, is, zersid, nvmax)
!c
      integer(8) :: icon(:,:),ifl(:),is(:), ih(:)
      integer*8 adj, tetra, zersid
      integer*8 k, now, ihn, nhmax, newtts, nvmax, site1, site2
      logical flphis
      integer*8 nxtnow, nxtadj, i, new1, new2, new3, new4, neigh, j
!c
!c     reorder now
!c
      site1 = icon(zersid+4, now)
      call reordr (icon, k, site1, now)
!c
!c     define nxtnow
!c
      nxtnow = icon(2,now)
      if(icon(5,nxtnow).ne.k) stop 2010
!c
!c     reorder adj
!c
      call sitord (icon, site1, adj)
!c
!c     define nxtadj
!c
      nxtadj = icon(1,adj)
!c
!c     are nxtnow and nxtadj neighbors?
!c
      do 5 i = 1, 4
         if (icon(i,nxtnow) .eq. nxtadj) goto 6
    5 continue
      goto 2000
    6 continue
!c
      if(flphis) then
         new1 = tetra + 1
         new2 = tetra + 2
         new3 = tetra + 3
         new4 = tetra + 4
         tetra = new4
         if (tetra .gt. nvmax) stop 2020
      else
         if(ihn.eq.0) then
            new1 = tetra+1
            tetra = new1
            if(tetra .gt. nvmax) stop 2030
         else
            new1 = ih(ihn)
            ihn = ihn-1
         endif
         if(ihn.eq.0) then
            new2 = tetra+1
            tetra = new2
            if(tetra .gt. nvmax) stop 2040
         else
            new2 = ih(ihn)
            ihn = ihn-1
         endif
         if(ihn.eq.0) then
            new3 = tetra+1
            tetra = new3
            if(tetra .gt. nvmax) stop 2050
         else
            new3 = ih(ihn)
            ihn = ihn-1
         endif
         if(ihn.eq.0) then
            new4 = tetra+1
            tetra = new4
            if(tetra .gt. nvmax) stop 2060
         else
            new4 = ih(ihn)
            ihn = ihn-1
         endif
      endif
!c
      newtts = newtts+4
      if(newtts.gt.nvmax) stop 2065
      ifl(newtts-3) = new1
      ifl(newtts-2) = new2
      ifl(newtts-1) = new3
      ifl(newtts) = new4
!c
!c     reorder adj, nxtnow, nxtadj
!c
      do 10 i = 2, 4
         if (icon(i,adj) .eq. now) then
            site2 = icon(i+4,adj)
            goto 15
         endif
   10 continue
      stop 2070
   15 continue
      call reordr (icon, site1, site2, adj)
!c
      do 20 i = 2, 4
         if (icon(i,nxtnow) .eq. now) then
            site1 = icon(i+4,nxtnow)
            goto 25
         endif
  20  continue
      stop 2080
  25  continue
      call reordr (icon, k, site1, nxtnow)
!c
      call reordr (icon, site1, site2, nxtadj)
!c
!c     create new1
!c
      icon(1,new1) = icon(4,adj)
      icon(2,new1) = new3
      icon(3,new1) = new2
      icon(4,new1) = icon(4,now)
      icon(5,new1) = k
      icon(6,new1) = icon(6,now)
      icon(7,new1) = icon(7,now)
      icon(8,new1) = icon(6,adj)
!c
!c     create new2
!c
      icon(1,new2) = icon(3,adj)
      icon(2,new2) = new4
      icon(3,new2) = icon(3,now)
      icon(4,new2) = new1
      icon(5,new2) = k
      icon(6,new2) = icon(6,now)
      icon(7,new2) = icon(6,adj)
      icon(8,new2) = icon(8,now)
!c
!c     create new3
!c
      icon(1,new3) = icon(3,nxtadj)
      icon(2,new3) = new4
      icon(3,new3) = new1
      icon(4,new3) = icon(3,nxtnow)
      icon(5,new3) = k
      icon(6,new3) = icon(7,now)
      icon(7,new3) = icon(6,nxtnow)
      icon(8,new3) = icon(6,adj)
!c
!c     create new4
!c
      icon(1,new4) = icon(4,nxtadj)
      icon(2,new4) = new2
      icon(3,new4) = new3
      icon(4,new4) = icon(4,nxtnow)
      icon(5,new4) = k
      icon(6,new4) = icon(6,nxtnow)
      icon(7,new4) = icon(8,now)
      icon(8,new4) = icon(6,adj)
!c
!c     update is(:)
!c
      is(icon(5,now)) = new1
      is(icon(6,now)) = new1
      is(icon(7,now)) = new1
      is(icon(8,now)) = new2
      is(icon(6,adj)) = new1
      is(icon(6,nxtnow)) = new3
!c
!c     update neighbors of now
!c
      do 100 i = 3, 4
         neigh = icon(i,now)
         if (neigh .eq. 0) goto 100
         do 50 j = 1, 4
            if (icon(j,neigh) .eq. now) goto 75
   50    continue
         stop 2090
   75    continue
         if (i .eq. 3) then
            icon(j,neigh) = new2
         else
            icon(j,neigh) = new1
         endif
  100 continue
!c
!c     update neighbors of adj
!c
      do 400 i = 3, 4
         neigh = icon(i,adj)
         if (neigh .eq. 0) goto 400
         do 350 j = 1, 4
            if (icon(j,neigh) .eq. adj) goto 375
  350    continue
         stop 2110
  375    continue
         if (i .eq. 3) then
            icon(j,neigh) = new2
         else
            icon(j,neigh) = new1
         endif
  400 continue
!c
!c     update neighbors of nxtnow
!c
      do 600 i = 3, 4
         neigh = icon(i,nxtnow)
         if (neigh .eq. 0) goto 600
         do 575 j = 1, 4
            if (icon(j,neigh) .eq. nxtnow) goto 590
  575    continue
         stop 2120
  590    continue
         if (i .eq. 3) then
            icon(j,neigh) = new3
         else
            icon(j,neigh) = new4
         endif
  600    continue
!c
!c     update neighbors of nxtadj
!c
      do 900 i = 3, 4
         neigh = icon(i,nxtadj)
         if (neigh .eq. 0) goto 900
         do 875 j = 1, 4
            if (icon(j,neigh) .eq. nxtadj) goto 890
  875    continue
         stop 2130
  890    continue
         if (i .eq. 3) then
            icon(j,neigh) = new3
         else
            icon(j,neigh) = new4
         endif
  900 continue
!c
!c     show children of old tetra
!c
      if(flphis) then
         icon(5,now) = -icon(5,now)
         icon(1,now) = new1
         icon(2,now) = new2
         icon(3,now) = -nxtnow
         icon(4,now) = 0
!c
         icon(5,adj) = -icon(5,adj)
         icon(1,adj) = new1
         icon(2,adj) = new2
         icon(3,adj) = -nxtadj
         icon(4,adj) = 0
!c
         icon(5,nxtnow) = -icon(5,nxtnow)
         icon(1,nxtnow) = new3
         icon(2,nxtnow) = new4
         icon(3,nxtnow) = -now
         icon(4,nxtnow) = 0
!c
         icon(5,nxtadj) = -icon(5,nxtadj)
         icon(1,nxtadj) = new3
         icon(2,nxtadj) = new4
         icon(3,nxtadj) = -adj
         icon(4,nxtadj) = 0
      else
         icon(5,now) = - icon(5,now)
         icon(5,adj) = - icon(5,adj)
         icon(5,nxtnow) = -icon(5,nxtnow)
         icon(5,nxtadj) = -icon(5,nxtadj)
         ihn = ihn+4
         if(ihn.gt.nhmax) stop 2140
         ih(ihn) = now
         ih(ihn-1) = adj
         ih(ihn-2) = nxtnow
         ih(ihn-3) = nxtadj
      endif
!c
 2000 continue
      return
      end subroutine flip22

!=======================================================================

      subroutine flip23 (icon, k, now, adj, ifl, ih, ihn, nhmax,        &
     &                   flphis, tetra, newtts, is, nvmax)
!c
      integer(8) :: icon(:,:),ifl(:),is(:), ih(:)
      integer*8 adj, tetra
      integer*8 k, now, ihn, nhmax, newtts, nvmax
      logical flphis
      integer*8 new1, new2, new3, i, next, j
!c
      if(flphis) then
         new1 = tetra + 1
         new2 = tetra + 2
         new3 = tetra + 3
         tetra = new3
         if(tetra.gt.nvmax) stop 1810
      else
         if(ihn.eq.0) then
            new1 = tetra+1
            tetra = new1
            if(tetra .gt. nvmax) stop 1820
         else
            new1 = ih(ihn)
            ihn = ihn-1
         endif
         if(ihn.eq.0) then
            new2 = tetra+1
            tetra = new2
            if(tetra .gt. nvmax) stop 1830
         else
            new2 = ih(ihn)
            ihn = ihn-1
         endif
         if(ihn.eq.0) then
            new3 = tetra+1
            tetra = new3
            if(tetra .gt. nvmax) stop 1840
         else
            new3 = ih(ihn)
            ihn = ihn-1
         endif
      endif
!c
      newtts = newtts + 3
      if(newtts.gt.nvmax) stop 1845
      ifl(newtts-2) = new1
      ifl(newtts-1) = new2
      ifl(newtts) = new3
!c
!c     create new1
!c
      icon(1,new1) = icon(1,adj)
      icon(2,new1) = new2
      icon(3,new1) = new3
      icon(4,new1) = icon(2,now)
      icon(5,new1) = k
      icon(6,new1) = icon(6,adj)
      icon(7,new1) = icon(7,adj)
      icon(8,new1) = icon(8,adj)
!c
!c     create new2
!c
      icon(1,new2) = icon(2,adj)
      icon(2,new2) = new3
      icon(3,new2) = new1
      icon(4,new2) = icon(3,now)
      icon(5,new2) = k
      icon(6,new2) = icon(7,adj)
      icon(7,new2) = icon(5,adj)
      icon(8,new2) = icon(8,adj)
!c
!c     create new3
!c
      icon(1,new3) = icon(3,adj)
      icon(2,new3) = new2
      icon(3,new3) = icon(4,now)
      icon(4,new3) = new1
      icon(5,new3) = k
      icon(6,new3) = icon(6,adj)
      icon(7,new3) = icon(8,adj)
      icon(8,new3) = icon(5,adj)
!c
!c     update neighboring tetrahedra
!c
      do 100 i = 2, 4
         next = icon(i,now)
         if (next .eq. 0) goto 100
         do 50 j = 1,4
            if (icon(j,next) .eq. now) goto 60
   50    continue
         stop 1850
   60    continue
         if (i .eq. 2) then
            icon(j,next) = new1
         elseif (i .eq. 3) then
            icon(j,next) = new2
         else
            icon(j,next) = new3
         endif
  100 continue
!c
      do 200 i = 1, 3
         next = icon(i,adj)
         if (next .eq. 0) goto 200
         do 150 j = 1,4
            if (icon(j,next) .eq. adj) goto 160
  150    continue
         stop 1860
  160    continue
         if (i .eq. 1) then
            icon(j,next) = new1
         elseif (i .eq. 2) then
            icon(j,next) = new2
         else
            icon(j,next) = new3
         endif
  200 continue
!c
!c     update is(:)
!c
      is(icon(5,now)) = new3
      is(icon(6,now)) = new3
      is(icon(7,now)) = new3
      is(icon(8,now)) = new1
      is(icon(8,adj)) = new3
!c
!c     mark 2 old tetra to show children
!c
      if(flphis) then
         icon(5,now) = -icon(5,now)
         icon(1,now) = new1
         icon(2,now) = new2
         icon(3,now) = new3
         icon(4,now) = 0
!c
         icon(5,adj) = -icon(5,adj)
         icon(1,adj) = new1
         icon(2,adj) = new2
         icon(3,adj) = new3
         icon(4,adj) = 0
      else
         icon(5,now) = - icon(5,now)
         icon(5,adj) = - icon(5,adj)
         ihn = ihn+2
         if(ihn.gt.nhmax) stop 1870
         ih(ihn) = now
         ih(ihn-1) = adj
      endif
!c
      return
      end subroutine flip23

!=======================================================================

      subroutine flip31 (icon, k, now, adj, ifl, ih, ihn, nhmax,        &
     &                   flphis, tetra, newtts, is, zersid, nvmax)
!c
      integer(8) :: icon(:,:),ifl(:),is(:), ih(:)
      integer*8 k, now, ihn, nhmax, newtts, nvmax
      integer*8 adj, tetra, zersid
      integer*8 site1, site2, site3, site4, site5
      logical flphis
      integer*8 nside, nrt, nrd, nxtnow, nxtadj, nxtnrd
      integer*8 nitet, nitat, neigh, j
!c
      nside = abs(zersid)
!c
!c     reorder now
!c
      site1 = icon(nside+4,now)
      call reordr(icon, k, site1, now)
!c
      if(zersid .gt. 0) go to 10
!c
!c     define nrt
!c
      nrt = icon(4,now)
!c
!c     reordr nrt
!c
      call reordr(icon, k, site1, nrt)
      if(icon(1,nrt) .ne. adj) go to 2000
!c
!c     define nrd, redefine now
!c
      nrd = now
      now = nrt
      go to 20
!c
!c     define nrd
!c
   10 continue
      nrd = icon(3,now)
!c
!c     reorder nrd
!c
      call reordr(icon, k, site1, nrd)
      if(icon(1,nrd) .ne. adj) go to 2000
!c
   20 continue
!c
!c     reorder adj
!c
      site2 = icon(7,now)
      call reordr(icon, site1, site2, adj)
!c
!c     define nxtnow, nxtadj, nxtnrd, and reorder
!c
      nxtnow = icon(4,now)
      nxtadj = icon(3,adj)
      nxtnrd = icon(3,nrd)
      call reordr(icon, k, site1, nxtnow)
      call reordr(icon, site1, site2, nxtadj)
      call reordr(icon, k, site1, nxtnrd)
      site5 = icon(7,nxtnow)
      if(icon(8,nxtadj) .ne. site5 .or.                                 &
     &   icon(8,nxtnrd) .ne. site5) go to 2000
!c
!c     flip
!c
      site3 = icon(8,now)
      site4 = icon(8,adj)
      if(flphis) then
         nitet = tetra + 1
         nitat = tetra + 2
         tetra = nitat
         if(tetra .gt. nvmax) stop 2410
      else
         if(ihn.eq.0) then
            nitet = tetra+1
            tetra = nitet
            if(tetra .gt. nvmax) stop 2420
         else
            nitet = ih(ihn)
            ihn = ihn-1
         endif
         if(ihn.eq.0) then
            nitat = tetra+1
            tetra = nitat
            if(tetra .gt. nvmax) stop 2430
         else
            nitat = ih(ihn)
            ihn = ihn-1
         endif
      endif
!c
      newtts = newtts + 2
      if(newtts.gt.nvmax) stop 2435
      ifl(newtts-1) = nitet
      ifl(newtts) = nitat
!c
!c     create new tetrahedra
!c
      icon(1,nitet) = icon(1,adj)
      icon(2,nitet) = icon(2,nrd)
      icon(3,nitet) = nitat
      icon(4,nitet) = icon(2,now)
      icon(5,nitet) = k
      icon(6,nitet) = site2
      icon(7,nitet) = site3
      icon(8,nitet) = site4
!c
      icon(1,nitat) = icon(1,nxtadj)
      icon(2,nitat) = icon(2,nxtnrd)
      icon(3,nitat) = icon(2,nxtnow)
      icon(4,nitat) = nitet
      icon(5,nitat) = k
      icon(6,nitat) = site2
      icon(7,nitat) = site4
      icon(8,nitat) = site5
!c
!c     update is(:)
!c
      is(k) = nitat
      is(site1) = -4
      is(site2) = nitat
      is(site3) = nitet
      is(site4) = nitat
      is(site5) = nitat
!c
!c     update neighbors of adj, nrd, now
!c
      neigh = icon(2,now)
      if(neigh .eq. 0) go to 200
      do 140 j = 1, 4
         if(icon(j,neigh) .eq. now) go to 160
  140 continue
      stop 2440
  160 continue
      icon(j,neigh) = nitet
  200 continue
!c
      neigh = icon(1,adj)
      if(neigh .eq. 0) go to 300
      do 240 j = 1, 4
         if(icon(j,neigh) .eq. adj) go to 260
  240 continue
      stop 2450
  260 continue
      icon(j,neigh) = nitet
  300 continue
!c
      neigh = icon(2,nrd)
      if(neigh .eq. 0) go to 330
      do 310 j = 1, 4
         if(icon(j,neigh) .eq. nrd) go to 320
  310 continue
      stop 2460
  320 continue
      icon(j,neigh) = nitet
  330 continue
!c
!c     update neighbors of nxtnow, nxtadj, nxtnrd
!c
      neigh = icon(2,nxtnow)
      if(neigh .eq. 0) go to 400
      do 340 j = 1, 4
         if(icon(j,neigh) .eq. nxtnow) go to 360
  340 continue
      stop 2470
  360 continue
      icon(j,neigh) = nitat
  400 continue
!c
      neigh = icon(1,nxtadj)
      if(neigh .eq. 0) go to 500
      do 440 j = 1, 4
         if(icon(j,neigh) .eq. nxtadj) go to 460
  440 continue
      stop 2480
  460 continue
      icon(j,neigh) = nitat
  500 continue
!c
      neigh = icon(2,nxtnrd)
      if(neigh .eq. 0) go to 600
      do 540 j = 1, 4
         if(icon(j,neigh) .eq. nxtnrd) go to 560
  540 continue
      stop 2490
  560 continue
      icon(j,neigh) = nitat
  600 continue
!c
!c     show children of old tetrahedra
!c
      if(flphis) then
         icon(5,now) = -icon(5,now)
         icon(1,now) = nitet
         icon(2,now) = -nxtnow
         icon(3,now) = 0
         icon(4,now) = 0
!c
         icon(5,adj) = -icon(5,adj)
         icon(1,adj) = nitet
         icon(2,adj) = -nxtadj
         icon(3,adj) = 0
         icon(4,adj) = 0
!c
         icon(5,nrd) = -icon(5,nrd)
         icon(1,nrd) = nitet
         icon(2,nrd) = -nxtnrd
         icon(3,nrd) = 0
         icon(4,nrd) = 0
!c
         icon(5,nxtnow) = -icon(5,nxtnow)
         icon(1,nxtnow) = nitat
         icon(2,nxtnow) = -now
         icon(3,nxtnow) = 0
         icon(4,nxtnow) = 0
!c
         icon(5,nxtadj) = -icon(5,nxtadj)
         icon(1,nxtadj) = nitat
         icon(2,nxtadj) = -adj
         icon(3,nxtadj) = 0
         icon(4,nxtadj) = 0
!c
         icon(5,nxtnrd) = -icon(5,nxtnrd)
         icon(1,nxtnrd) = nitat
         icon(2,nxtnrd) = -nrd
         icon(3,nxtnrd) = 0
         icon(4,nxtnrd) = 0
      else
         icon(5,now) = -icon(5,now)
         icon(5,adj) = -icon(5,adj)
         icon(5,nrd) = -icon(5,nrd)
         icon(5,nxtnow) = -icon(5,nxtnow)
         icon(5,nxtadj) = -icon(5,nxtadj)
         icon(5,nxtnrd) = -icon(5,nxtnrd)
         ihn = ihn+6
         if(ihn.gt.nhmax) stop 2495
         ih(ihn) = now
         ih(ihn-1) = adj
         ih(ihn-2) = nrd
         ih(ihn-3) = nxtnow
         ih(ihn-4) = nxtadj
         ih(ihn-5) = nxtnrd
      endif
!c
 2000 continue
      return
      end subroutine flip31

!=======================================================================

      subroutine flip32 (icon, k, now, adj, ifl, ih, ihn, nhmax,        &
     &                   flphis, tetra, newtts, is, negsid, nvmax)
!c
      integer(8) :: icon(:,:),ifl(:),is(:), ih(:)
      integer*8 adj, tetra, site1, site2
      integer*8 k, now, ihn, nhmax, newtts, negsid, nvmax
      logical flphis
      integer*8 nxtnow, nxtadj, i, new1, new2, neigh, j
!c
!c     reorder now
!c
      site1 = icon(negsid+4, now)
      call reordr (icon, k, site1, now)
!c
!c     check if now & adj have same neighbor, reorder adj
!c
      nxtnow = icon(2,now)
      if(icon(5,nxtnow).ne.k) stop 1910
!c
      call sitord (icon, site1, adj)
      nxtadj = icon(1,adj)
      if (nxtnow .ne. nxtadj) goto 1000
!c
      do 210 i = 1, 4
         if (icon(i,adj) .eq. now) then
            site2 = icon(i+4,adj)
            goto 215
         endif
  210 continue
      stop 1920
  215 continue
      call reordr (icon, site1, site2, adj)
!c
!c     reorder nxtnow
!c
      call reordr (icon, k, site2, nxtnow)
!c
      if(flphis) then
         new1 = tetra + 1
         new2 = tetra + 2
         tetra = new2
         if (tetra .gt. nvmax) stop 1930
      else
         if(ihn.eq.0) then
            new1 = tetra+1
            tetra = new1
            if(tetra .gt. nvmax) stop 1940
         else
            new1 = ih(ihn)
            ihn = ihn-1
         endif
         if(ihn.eq.0) then
            new2 = tetra+1
            tetra = new2
            if(tetra .gt. nvmax) stop 1950
         else
            new2 = ih(ihn)
            ihn = ihn-1
         endif
      endif
!c
      newtts = newtts + 2
      if(newtts.gt.nvmax) stop 1955
      ifl(newtts-1) = new1
      ifl(newtts) = new2
!c
!c     create new1
!c
      icon(1,new1) = icon(4,adj)
      icon(2,new1) = icon(3,nxtnow)
      icon(3,new1) = new2
      icon(4,new1) = icon(4,now)
      icon(5,new1) = k
      icon(6,new1) = icon(6,now)
      icon(7,new1) = icon(7,now)
      icon(8,new1) = icon(6,adj)
!c
!c     create new2
!c
      icon(1,new2) = icon(3,adj)
      icon(2,new2) = icon(4,nxtnow)
      icon(3,new2) = icon(3,now)
      icon(4,new2) = new1
      icon(5,new2) = k
      icon(6,new2) = icon(6,now)
      icon(7,new2) = icon(6,adj)
      icon(8,new2) = icon(8,now)
!c
!c     update neighboring tetrahedra
!c
      do 400 i = 3, 4
         neigh = icon(i,now)
         if (neigh .eq. 0) goto 400
         do 350 j = 1, 4
            if (icon(j,neigh) .eq. now) goto 375
  350    continue
         stop 1960
  375    continue
         if (i .eq. 3) then
            icon(j,neigh) = new2
         else
            icon(j,neigh) = new1
         endif
  400 continue
!c
      do 500 i = 3, 4
         neigh = icon(i,adj)
         if (neigh .eq. 0) goto 500
         do 450 j = 1, 4
            if (icon(j,neigh) .eq. adj) goto 475
  450    continue
         stop 1970
  475    continue
         if (i .eq. 3) then
            icon(j,neigh) = new2
         else
            icon(j,neigh) = new1
         endif
  500 continue
!c
      do 600 i = 3, 4
         neigh = icon(i,nxtnow)
         if (neigh .eq. 0) goto 600
         do 550 j = 1, 4
            if (icon(j,neigh) .eq. nxtnow) goto 575
  550    continue
         stop 1980
  575    continue
         if (i .eq. 3) then
            icon(j,neigh) = new1
         else
            icon(j,neigh) = new2
         endif
  600 continue
!c
!c     update is(:)
!c
      is(icon(5,now)) = new1
      is(icon(6,now)) = new1
      is(icon(7,now)) = new1
      is(icon(8,now)) = new2
      is(icon(6,adj)) = new1
!c
!c     show children of adj, now, nxtnow
!c
      if(flphis) then
         icon(5,now) = -icon(5,now)
         icon(1,now) = new1
         icon(2,now) = new2
         icon(3,now) = 0
         icon(4,now) = 0
!c
         icon(5,adj) = -icon(5,adj)
         icon(1,adj) = new1
         icon(2,adj) = new2
         icon(3,adj) = 0
         icon(4,adj) = 0
!c
         icon(5,nxtnow) = -icon(5,nxtnow)
         icon(1,nxtnow) = new1
         icon(2,nxtnow) = new2
         icon(3,nxtnow) = 0
         icon(4,nxtnow) = 0
      else
         icon(5,now) = - icon(5,now)
         icon(5,adj) = - icon(5,adj)
         icon(5,nxtnow) = -icon(5,nxtnow)
         ihn = ihn+3
         if(ihn.gt.nhmax) stop 1990
         ih(ihn) = now
         ih(ihn-1) = adj
         ih(ihn-2) = nxtnow
      endif
!c
 1000 continue
      return
      end subroutine flip32

!=======================================================================

      subroutine flip41 (icon, k, now, adj, ifl, ih, ihn, nhmax,        &
     &                   flphis, tetra, newtts, is, zersid, nvmax)
!c
      integer(8) :: icon(:,:),ifl(:),is(:), ih(:)
      integer*8 k, now, ihn, nhmax, newtts, nvmax
      integer*8 adj, tetra, zersid, site1, site2
      logical flphis
      integer*8 nxtnow, nxtadj, new1, neigh, j
!c
!c     reorder now
!c
      site1 = icon(zersid+4,now)
      call reordr(icon, k, site1, now)
      site2 = icon(7,now)
!c
!c     reorder adj
!c
      call reordr(icon, site1, site2, adj)
!c
!c     define nxtnow and nxtadj
!c
      nxtnow = icon(4,now)
      nxtadj = icon(3,now)
!c
!c     do now, adj, nxtnow, nxtadj form a tetrahedron?
!c
      if(icon(3,adj).ne.nxtnow .or. icon(2,adj).ne.nxtadj) go to 2000
!c
!c     flip
!c
      if(flphis) then
         new1 = tetra + 1
         tetra = new1
         if(tetra .gt. nvmax) stop 2210
      else
         if(ihn.eq.0) then
            new1 = tetra+1
            tetra = new1
            if(tetra .gt. nvmax) stop 2220
         else
            new1 = ih(ihn)
            ihn = ihn-1
         endif
      endif
!c
      newtts = newtts + 1
      if(newtts.gt.nvmax) stop 2225
      ifl(newtts) = new1
!c
!c     reorder nxtnow and nxtadj
!c
      call reordr(icon, k, site2, nxtnow)
      call reordr(icon, k, site1, nxtadj)
      if(icon(2,nxtnow) .ne. nxtadj) stop 2230
!c
!c     create tetra
!c
      icon(1,new1) = icon(1,adj)
      icon(2,new1) = icon(2,nxtadj)
      icon(3,new1) = icon(3,nxtnow)
      icon(4,new1) = icon(2,now)
      icon(5,new1) = k
      icon(6,new1) = site2
      icon(7,new1) = icon(8,now)
      icon(8,new1) = icon(8,adj)
!c
!c     update is(:)
!c
      is(site1) = -4
      is(icon(5,new1)) = new1
      is(icon(6,new1)) = new1
      is(icon(7,new1)) = new1
      is(icon(8,new1)) = new1
!c
!c     update neighbor of now
!c
      neigh = icon(2,now)
      if(neigh .eq. 0) go to 200
      do 140 j = 1, 4
         if(icon(j,neigh) .eq. now) go to 160
  140 continue
      stop 2240
  160 continue
      icon(j,neigh) = new1
  200 continue
!c
!c     update neighbor of adj
!c
      neigh = icon(1,adj)
      if(neigh .eq. 0) go to 300
      do 240 j = 1, 4
         if(icon(j,neigh) .eq. adj) go to 260
  240 continue
      stop 2250
  260 continue
      icon(j,neigh) = new1
  300 continue
!c
!c     update neighbor of nxtnow
!c
      neigh = icon(3,nxtnow)
      if(neigh .eq. 0) go to 400
      do 340 j = 1, 4
         if(icon(j,neigh) .eq. nxtnow) go to 360
  340 continue
      stop 2260
  360 continue
      icon(j,neigh) = new1
  400 continue
!c
!c     update neighbor of nxtadj
!c
      neigh = icon(2,nxtadj)
      if(neigh .eq. 0) go to 500
      do 440 j = 1, 4
         if(icon(j,neigh) .eq. nxtadj) go to 460
  440 continue
      stop 2270
  460 continue
      icon(j,neigh) = new1
  500 continue
!c
!c     show children of old tetrahedra
!c
      if(flphis) then
         icon(5,now) = -icon(5,now)
         icon(1,now) = new1
         icon(2,now) = 0
         icon(3,now) = 0
         icon(4,now) = 0
!c
         icon(5,adj) = -icon(5,adj)
         icon(1,adj) = new1
         icon(2,adj) = 0
         icon(3,adj) = 0
         icon(4,adj) = 0
!c
         icon(5,nxtnow) = -icon(5,nxtnow)
         icon(1,nxtnow) = new1
         icon(2,nxtnow) = 0
         icon(3,nxtnow) = 0
         icon(4,nxtnow) = 0
!c
         icon(5,nxtadj) = -icon(5,nxtadj)
         icon(1,nxtadj) = new1
         icon(2,nxtadj) = 0
         icon(3,nxtadj) = 0
         icon(4,nxtadj) = 0
      else
         icon(5,now) = - icon(5,now)
         icon(5,adj) = - icon(5,adj)
         icon(5,nxtnow) = -icon(5,nxtnow)
         icon(5,nxtadj) = -icon(5,nxtadj)
         ihn = ihn+4
         if(ihn.gt.nhmax) stop 2280
         ih(ihn) = now
         ih(ihn-1) = adj
         ih(ihn-2) = nxtnow
         ih(ihn-3) = nxtadj
      endif
!c
 2000 continue
      return
      end subroutine flip41

!=======================================================================

      subroutine frterm(ixsew, iysew, izsew, ixf, iyf, izf, isgxf,      &
     &                  isgyf, isgzf, ikxf, ikyf, ikzf, ixf2, iyf2,     &
     &                  izf2, isgxf2, isgyf2, isgzf2, ikxf2,            &
     &                  ikyf2, ikzf2, iw2, ix2, iy2, iz2,               &
     &                  iq2, isgw2, isgx2, isgy2, isgz2, isgq2, ikw2,   &
     &                  ikx2, iky2, ikz2, ikq2, mhalf, mfull,           &
     &                  ixse2, iyse2, izse2, isclp)
!c
      integer(8) :: ixsew, iysew, izsew, isgxf, isgyf, isgzf
      integer(8) :: isgxf2, isgyf2, isgzf2
      integer(8) :: ikxf, ikyf, ikzf, ikxf2, ikyf2, ikzf2
      integer(8) :: isgw2, isgx2, isgy2, isgz2, isgq2
      integer(8) :: ikw2, ikx2, iky2, ikz2, ikq2, mhalf, mfull
      integer(8) :: ixse2, iyse2, izse2, isclp(:)
      integer(8) :: isgo, isgu, isgv, isgp, iko, iku, ikv, ikp
      integer(8) :: ixf(:), iyf(:), izf(:), ixf2(:), iyf2(:), izf2(:)
      integer(8) :: iw2(:), ix2(:), iy2(:), iz2(:), iq2(:)
      integer*8 nkmax
      parameter (nkmax = 30)
      integer*8 io(nkmax), iu(nkmax), iv(nkmax), ip(nkmax)
!c
      call decmp2(io, isgo, iko, ixsew, ixse2, mhalf, mfull, isclp)
      call muldif(io, ixf, ix2, isgo, isgxf, isgx2, iko, ikxf, ikx2,    &
     &              nkmax, mhalf) !io-->ix2nd ; ixf-->ix1st
                                  !ix2=io-ixf=ix2nd-ix1st (displacement vector)
                                  !ix2 is not using in this sub., but pass it outside
                                  !for other usage!(the same for iy2, iz2)
      call mulmul(io, io, iu, isgo, isgo, isgu, iko, iko, iku,          &
     &              nkmax, mhalf) !iu=ix2nd**2
      call muldif(iu, ixf2, iv, isgu, isgxf2, isgv, iku, ikxf2, ikv,    &
     &              nkmax, mhalf) !ixf2=ixf**2
                                  !iv=ix2nd**2-ixf**2
      call muldif(iv, iw2, ip, isgv, isgw2, isgp, ikv, ikw2, ikp,       &
     &              nkmax, mhalf) !ip=(ix2nd**2-ixf**2)-iw2
      !if delaun=.true., then isgw2=isgw3=isgw4=0, iw2=iw3=iw4=0
!c
      call decmp2(io, isgo, iko, iysew, iyse2, mhalf, mfull, isclp)
      call muldif(io, iyf, iy2, isgo, isgyf, isgy2, iko, ikyf, iky2,    &
     &              nkmax, mhalf) !iy2=iy2nd-iy1st (displacement vector)
      call mulmul(io, io, iu, isgo, isgo, isgu, iko, iko, iku,          &
     &              nkmax, mhalf) !iu=iy2nd**2
      call muldif(iu, iyf2, iv, isgu, isgyf2, isgv, iku, ikyf2, ikv,    &
     &              nkmax, mhalf) !iv=iy2nd**2-iyf**2
      isgv=-isgv
      call muldif(ip, iv, iu, isgp, isgv, isgu, ikp, ikv, iku,          &
     &              nkmax, mhalf) !iu=(ix2nd**2-ixf**2)+(iy2nd**2-iyf**2)-iw2
!c
      call decmp2(io, isgo, iko, izsew, izse2, mhalf, mfull, isclp)
      call muldif(io, izf, iz2, isgo, isgzf, isgz2, iko, ikzf, ikz2,    &
     &              nkmax, mhalf) !iz2=iz2nd-iz1st
      call mulmul(io, io, iv, isgo, isgo, isgv, iko, iko, ikv,          &
     &              nkmax, mhalf) !iv=iz2nd**2
      call muldif(iv, izf2, ip, isgv, isgzf2, isgp, ikv, ikzf2, ikp,    &
     &              nkmax, mhalf) !ip=iz2nd**2-izf**2
      isgp=-isgp
      call muldif(iu, ip, iq2, isgu, isgp, isgq2, iku, ikp, ikq2,       &
     &              nkmax, mhalf)
      !iq2=(ix2nd**2-ixf**2)+(iy2nd**2-iyf**2)+(iz2nd**2-izf**2)-iw2
!c
!c
      return
      end subroutine frterm

!=======================================================================

      subroutine gette2(a, b, c, d, xi, yi, zi, x, y, z, x2, y2, z2,    &
     &                  itype, k, side1, side2, flag, xc, yc, zc,       &
     &                  mhalf, mfull, isclp, epz)
!c
      real(8) :: xi(:), yi(:), zi(:)
      integer(8) :: x(:), y(:), z(:), x2(:), y2(:), z2(:)
      integer(8) :: xc(:), yc(:), zc(:)
      integer(8) :: isclp(:)
      double precision epz
      integer*8 itype, k, mhalf, mfull, ifn, ipout
      integer*8 iside(4), a, b, c, d, side1, side2, flag
!c
!c     determine position of point k relative to facets of tetrahedron
!c
      if(b.le.8 .or. c.le.8 .or. d.le.8) go to 100
!c
      call irsign(xi, yi, zi, x, y, z, x2, y2, z2, k, b, d, c,          &
     &            mhalf, mfull, isclp, epz, ipout)
      iside(1) = ipout
      call irsign(xi, yi, zi, x, y, z, x2, y2, z2, k, a, c, d,          &
     &            mhalf, mfull, isclp, epz, ipout)
      iside(2) = ipout
      call irsign(xi, yi, zi, x, y, z, x2, y2, z2, k, a, d, b,          &
     &            mhalf, mfull, isclp, epz, ipout)
      iside(3) = ipout
      call irsign(xi, yi, zi, x, y, z, x2, y2, z2, k, a, b, c,          &
     &            mhalf, mfull, isclp, epz, ipout)
      iside(4) = ipout
!c
!c     point k is not in tetrahedron
!c
   50 continue
      if(iside(1).lt.0 .or. iside(2).lt.0 .or. iside(3).lt.0 .or.       &
     &   iside(4).lt.0) go to 1000
!c
      if(flag.lt.0) then
         itype = 0
         go to 1000
      endif
!c
!c     point k is in the interior of tetrahedron
!c
      if(iside(1).gt.0 .and. iside(2).gt.0 .and. iside(3).gt.0 .and.    &
     &   iside(4).gt.0) then
         itype = 2
         go to 1000
      endif
!c
!c     unacceptable situation
!c
      if(iside(1).eq.0 .and. iside(2).eq.0 .and. iside(3).eq.0 .and.    &
     &   iside(4).eq.0) stop 805
!c
!c     point k is a vertex of tetrahedron
!c
      if(iside(1).eq.0 .and. iside(2).eq.0 .and. iside(3).eq.0) then
         itype = 1
         side1 = 4
         go to 1000
      elseif(iside(1).eq.0 .and. iside(2).eq.0 .and. iside(4).eq.0) then
         itype = 1
         side1 = 3
         go to 1000
      elseif(iside(1).eq.0 .and. iside(3).eq.0 .and. iside(4).eq.0) then
         itype = 1
         side1 = 2
         go to 1000
      elseif(iside(2).eq.0 .and. iside(3).eq.0 .and. iside(4).eq.0) then
         itype = 1
         side1 = 1
         go to 1000
      endif
!c
!c     point k is in the interior of an edge of tetrahedron
!c
      if (iside(1).eq.0 .and. iside(2).eq.0) then
         itype = 3
         side1 = 1
         side2 = 2
         go to 1000
      elseif (iside(1).eq.0 .and. iside(3).eq.0) then
         itype = 3
         side1 = 1
         side2 = 3
         go to 1000
      elseif (iside(1).eq.0 .and. iside(4).eq.0) then
         itype = 3
         side1 = 1
         side2 = 4
         go to 1000
      elseif (iside(2).eq.0 .and. iside(3).eq.0) then
         itype = 3
         side1 = 2
         side2 = 3
         go to 1000
      elseif (iside(2).eq.0 .and. iside(4).eq.0) then
         itype = 3
         side1 = 2
         side2 = 4
         go to 1000
      elseif (iside(3).eq.0 .and. iside(4).eq.0) then
         itype = 3
         side1 = 3
         side2 = 4
         go to 1000
      endif
!c
!c     point k is in the interior of a facet of tetrahedron
!c
      itype = 4
      if (iside(1) .eq. 0) then
         side1 = 1
      elseif (iside(2) .eq. 0) then
         side1 = 2
      elseif (iside(3) .eq. 0) then
         side1 = 3
      elseif (iside(4) .eq. 0) then
         side1 = 4
      else
         stop 807
      endif
      go to 1000
!c
!c     there is at least one artificial vertex
!c
  100 continue
      if(b.le.8) then
         iside(1) = 1
         go to 120
      elseif(d.le.8.and.c.le.8) then
         call ipsig4(x, y, z, x2, y2, z2, xc, yc, zc,                   &
     &               d, c, k, b, mhalf, mfull, isclp, ipout)
         iside(1) = ipout
         if(iside(1).ne.0) go to 120
         call ipsig6(x, y, z, x2, y2, z2, xc, yc, zc,                   &
     &               d, c, k, b, mhalf, mfull, isclp, ipout)
         iside(1) = ipout
         go to 120
      elseif(d.le.8) then
         ifn = 0
         call ipsig3(x, y, z, x2, y2, z2, xc, yc, zc,                   &
     &               b, c, k, d, mhalf, mfull, isclp, ifn, ipout)
         iside(1) = ipout
      else
         ifn = 1
         call ipsig3(x, y, z, x2, y2, z2, xc, yc, zc,                   &
     &               b, d, k, c, mhalf, mfull, isclp, ifn, ipout)
         iside(1) = ipout
      endif
      if(iside(1).ne.0) go to 120
      call ipsign(x, y, z, x2, y2, z2, b, d, c, k, mhalf,               &
     &            mfull, isclp, ipout)
      iside(1) = ipout
  120 continue
!c
      if(c.le.8.and.d.le.8) then
         call ipsig4(x, y, z, x2, y2, z2, xc, yc, zc,                   &
     &               c, d, k, a, mhalf, mfull, isclp, ipout)
         iside(2) = ipout
         if(iside(2).ne.0) go to 140
         call ipsig6(x, y, z, x2, y2, z2, xc, yc, zc,                   &
     &               c, d, k, a, mhalf, mfull, isclp, ipout)
         iside(2) = ipout
         go to 140
      elseif(c.le.8) then
         ifn = 0
         call ipsig3(x, y, z, x2, y2, z2, xc, yc, zc,                   &
     &               a, d, k, c, mhalf, mfull, isclp, ifn, ipout)
         iside(2) = ipout
      else
         ifn = 1
         call ipsig3(x, y, z, x2, y2, z2, xc, yc, zc,                   &
     &               a, c, k, d, mhalf, mfull, isclp, ifn, ipout)
         iside(2) = ipout
      endif
      if(iside(2).ne.0) go to 140
      call ipsign(x, y, z, x2, y2, z2, a, c, d, k, mhalf,               &
     &            mfull, isclp, ipout)
      iside(2) = ipout
  140 continue
!c
      if(d.gt.8) then
         call ipsign(x, y, z, x2, y2, z2, a, d, b, k, mhalf,            &
     &               mfull, isclp, ipout)
         iside(3) = ipout
         go to 160
      elseif(b.le.8) then
         call ipsig4(x, y, z, x2, y2, z2, xc, yc, zc,                   &
     &               d, b, k, a, mhalf, mfull, isclp, ipout)
         iside(3) = ipout
         if(iside(3).ne.0) go to 160
         call ipsig6(x, y, z, x2, y2, z2, xc, yc, zc,                   &
     &               d, b, k, a, mhalf, mfull, isclp, ipout)
         iside(3) = ipout
         go to 160
      else
         ifn = 0
         call ipsig3(x, y, z, x2, y2, z2, xc, yc, zc,                   &
     &               a, b, k, d, mhalf, mfull, isclp, ifn, ipout)
         iside(3) = ipout
      endif
      if(iside(3).ne.0) go to 160
      call ipsign(x, y, z, x2, y2, z2, a, d, b, k, mhalf,               &
     &            mfull, isclp, ipout)
      iside(3) = ipout
  160 continue
!c
      if(c.gt.8) then
         call ipsign(x, y, z, x2, y2, z2, a, b, c, k, mhalf,            &
     &               mfull, isclp, ipout)
         iside(4) = ipout
         go to 180
      elseif(b.le.8) then
         call ipsig4(x, y, z, x2, y2, z2, xc, yc, zc,                   &
     &               b, c, k, a, mhalf, mfull, isclp, ipout)
         iside(4) = ipout
         if(iside(4).ne.0) go to 180
         call ipsig6(x, y, z, x2, y2, z2, xc, yc, zc,                   &
     &               b, c, k, a, mhalf, mfull, isclp, ipout)
         iside(4) = ipout
         go to 180
      else
         ifn = 1
         call ipsig3(x, y, z, x2, y2, z2, xc, yc, zc,                   &
     &               a, b, k, c, mhalf, mfull, isclp, ifn, ipout)
         iside(4) = ipout
      endif
      if(iside(4).ne.0) go to 180
      call ipsign(x, y, z, x2, y2, z2, a, b, c, k, mhalf,               &
     &            mfull, isclp, ipout)
      iside(4) = ipout
  180 continue
      go to 50
!c
 1000 continue
      return
      end subroutine gette2

!=======================================================================

      subroutine gettet(itype, k, xi, yi, zi, x, y, z, x2, y2, z2, icon,&
     &                  curr, side1, side2, xc, yc, zc, mhalf, mfull,   &
     &                  isclp, epz, ITCHK)
!c
      real(8) :: xi(:), yi(:), zi(:)
      integer(8) :: x(:), y(:), z(:), x2(:), y2(:), z2(:), xc(:), yc(:),&
     &          zc(:)
      integer(8) :: icon(:,:),isclp(:)
      integer*8 curr, a, b, c, d, side1, side2, flag
      integer*8 mhalf, mfull
      double precision epz
      integer*8 itype, k, itchk
!c
      do 100 curr = 1, 12
         ITCHK = ITCHK+1
         a = abs(icon(5, curr))
         b = icon(6, curr)
         c = icon(7, curr)
         d = icon(8, curr)
!c
         flag = icon(5,curr)
         icon(5,curr)=a
!c
         call vrtord(icon, curr, a, b, c, d)
         if(flag.lt.0) icon(5,curr)=-a
         if(a.le.8.or.b.gt.8.or.c.gt.8.or.d.gt.8) stop 610
!c
         call gette2(a, b, c, d, xi, yi, zi, x, y, z, x2, y2, z2,       &
     &               itype, k, side1, side2, flag, xc, yc, zc,          &
     &               mhalf, mfull, isclp, epz)
         if (itype .ne. -1) goto 200
!c
  100 continue
      stop 620
  200 continue
      return
      end subroutine gettet

!=======================================================================

      subroutine idenom(x, y, z, x2, y2, z2, ifir, isec, ithi,          &
     &                  ifou, mhalf, mfull, isclp, io, isgo, iko)
!c
      integer(8) :: x(:), y(:), z(:), x2(:), y2(:), z2(:)
      integer(8) :: io(:), isclp(:)
      integer*8 ifir, isec, ithi, ifou
      integer*8 mhalf, mfull, nkmax
      parameter (nkmax = 30)
      integer(8) :: iu(nkmax), iv(nkmax), iw(nkmax)
      integer*8 ix2(nkmax), iy2(nkmax), iz2(nkmax)
      integer*8 ix3(nkmax), iy3(nkmax), iz3(nkmax)
      integer*8 ix4(nkmax), iy4(nkmax), iz4(nkmax)
      integer*8 ixf(nkmax), iyf(nkmax), izf(nkmax)
      integer*8 ixfiw, iyfiw, izfiw, ixsew, iysew, izsew
      integer*8 ixthw, iythw, izthw, ixfow, iyfow, izfow
      integer*8 ixfi2, iyfi2, izfi2, ixse2, iyse2, izse2
      integer*8 ixth2, iyth2, izth2, ixfo2, iyfo2, izfo2
      integer*8 isgxf, isgyf, isgzf, ikxf, ikyf, ikzf
      integer*8 isgx2, isgy2, isgz2, ikx2, iky2, ikz2
      integer*8 isgx3, isgy3, isgz3, ikx3, iky3, ikz3
      integer*8 isgx4, isgy4, isgz4, ikx4, iky4, ikz4
      integer*8 isgo, isgu, isgv, isgw, iko, iku, ikv, ikw
!c
      ixfiw = x(ifir)
      iyfiw = y(ifir)
      izfiw = z(ifir)
      ixsew = x(isec)
      iysew = y(isec)
      izsew = z(isec)
      ixthw = x(ithi)
      iythw = y(ithi)
      izthw = z(ithi)
      ixfow = x(ifou)
      iyfow = y(ifou)
      izfow = z(ifou)
!c
      ixfi2 = x2(ifir)
      iyfi2 = y2(ifir)
      izfi2 = z2(ifir)
      ixse2 = x2(isec)
      iyse2 = y2(isec)
      izse2 = z2(isec)
      ixth2 = x2(ithi)
      iyth2 = y2(ithi)
      izth2 = z2(ithi)
      ixfo2 = x2(ifou)
      iyfo2 = y2(ifou)
      izfo2 = z2(ifou)
!c
      call decmp2(ixf, isgxf, ikxf, ixfiw, ixfi2, mhalf, mfull, isclp)
      call decmp2(iyf, isgyf, ikyf, iyfiw, iyfi2, mhalf, mfull, isclp)
      call decmp2(izf, isgzf, ikzf, izfiw, izfi2, mhalf, mfull, isclp)
      !generate first point, use it to be origin later
!c
      call decmp2(io, isgo, iko, ixsew, ixse2, mhalf, mfull, isclp) !X-second
      call muldif(io, ixf, ix2, isgo, isgxf, isgx2, iko, ikxf, ikx2,    &
     &              nkmax, mhalf)!io-ixf=ix2
      !defined a displacement vector ix2--> io-ixf--> (ix2nd-ix1st)
      call decmp2(io, isgo, iko, iysew, iyse2, mhalf, mfull, isclp) !Y-second
      call muldif(io, iyf, iy2, isgo, isgyf, isgy2, iko, ikyf, iky2,    &
     &              nkmax, mhalf)!io-iyf=iy2
      !defined a displacement vector iy2--> io-iyf--> (iy2nd-iy1st)
      call decmp2(io, isgo, iko, izsew, izse2, mhalf, mfull, isclp) !Z-second
      call muldif(io, izf, iz2, isgo, isgzf, isgz2, iko, ikzf, ikz2,    &
     &              nkmax, mhalf)!io-izf=iz2
      !defined a displacement vector iz2--> io-izf--> (iz2nd-iz1st)
      call decmp2(io, isgo, iko, ixthw, ixth2, mhalf, mfull, isclp) !X-third
      call muldif(io, ixf, ix3, isgo, isgxf, isgx3, iko, ikxf, ikx3,    &
     &              nkmax, mhalf)!io-ixf=ix3
      !defined a displacement vector ix3--> io-ixf--> (ix3rd-ix1st)
      call decmp2(io, isgo, iko, iythw, iyth2, mhalf, mfull, isclp) !Y-third
      call muldif(io, iyf, iy3, isgo, isgyf, isgy3, iko, ikyf, iky3,    &
     &              nkmax, mhalf)!io-iyf=iy3
      !defined a displacement vector iy3--> io-iyf--> (iy3rd-iy1st)
      call decmp2(io, isgo, iko, izthw, izth2, mhalf, mfull, isclp) !Z-third
      call muldif(io, izf, iz3, isgo, isgzf, isgz3, iko, ikzf, ikz3,    &
     &              nkmax, mhalf)!io-izf=iz3
      !defined a displacement vector iz3--> io-izf--> (iz3rd-iz1st)
      call decmp2(io, isgo, iko, ixfow, ixfo2, mhalf, mfull, isclp) !X-fourth
      call muldif(io, ixf, ix4, isgo, isgxf, isgx4, iko, ikxf, ikx4,    &
     &              nkmax, mhalf)!io-ixf=ix4
      !defined a displacement vector ix4--> io-ixf--> (ix4th-ix1st)
      call decmp2(io, isgo, iko, iyfow, iyfo2, mhalf, mfull, isclp) !Y-fourth
      call muldif(io, iyf, iy4, isgo, isgyf, isgy4, iko, ikyf, iky4,    &
     &              nkmax, mhalf)!io-iyf=iy4
      !defined a displacement vector iy4--> io-iyf--> (iy4th-iy1st)
      call decmp2(io, isgo, iko, izfow, izfo2, mhalf, mfull, isclp) !Z-fourth
      call muldif(io, izf, iz4, isgo, isgzf, isgz4, iko, ikzf, ikz4,    &
     &              nkmax, mhalf)!io-izf=iz4
      !defined a displacement vector iz4--> io-izf--> (iz4th-iz1st)
!c
      call mulmul(iy2, iz3, iv, isgy2, isgz3, isgv, iky2, ikz3, ikv,    &
     &              nkmax, mhalf)!iy2*iz3=iv
      call mulmul(iz2, iy3, iu, isgz2, isgy3, isgu, ikz2, iky3, iku,    &
     &              nkmax, mhalf)!iz2*iy3=iu
      call muldif(iv, iu, iw, isgv, isgu, isgw, ikv, iku, ikw,          &
     &              nkmax, mhalf)
      !iw=iy2*iz3-iz2*iy3
      call mulmul(iw, ix4, io, isgw, isgx4, isgo, ikw, ikx4, iko,       &
     &              nkmax, mhalf)   !iw*ix4=io
      !io=ix4*(iy2*iz3-iz2*iy3)
!c
      call mulmul(iz2, ix3, iv, isgz2, isgx3, isgv, ikz2, ikx3, ikv,    &
     &              nkmax, mhalf)!iz2*ix3=iv
      call mulmul(ix2, iz3, iu, isgx2, isgz3, isgu, ikx2, ikz3, iku,    &
     &              nkmax, mhalf)!ix2*iz3=iu
      call muldif(iv, iu, iw, isgv, isgu, isgw, ikv, iku, ikw,          &
     &              nkmax, mhalf)
      !iw=iv-iu=iz2*ix3-ix2*iz3
      call mulmul(iw, iy4, iu, isgw, isgy4, isgu, ikw, iky4, iku,       &
     &              nkmax, mhalf)   !iw*iy4=iu
      !iu=iy4*(iz2*ix3-ix2*iz3)
      isgu =-isgu !turn it into sum instead of substract
      call muldif(io, iu, iw, isgo, isgu, isgw, iko, iku, ikw,          &
     &              nkmax, mhalf)      !io+iu=iw
      !iw=ix4*(iy2*iz3-iz2*iy3)+iy4*(iz2*ix3-ix2*iz3)

!c
      call mulmul(ix2, iy3, iv, isgx2, isgy3, isgv, ikx2, iky3, ikv,    &
     &              nkmax, mhalf)!ix2*iy3=iv
      call mulmul(iy2, ix3, iu, isgy2, isgx3, isgu, iky2, ikx3, iku,    &
     &              nkmax, mhalf)!iy2*ix3=iu
      call muldif(iv, iu, io, isgv, isgu, isgo, ikv, iku, iko,          &
     &              nkmax, mhalf)      !iv-iu=io
      !io=ix2*iy3-iy2*ix3
      call mulmul(io, iz4, iu, isgo, isgz4, isgu, iko, ikz4, iku,       &
     &              nkmax, mhalf)   !io*iz4=iu
      !iu=iz4*(ix2*iy3-iy2*ix3)
      isgu =-isgu
      call muldif(iw, iu, io, isgw, isgu, isgo, ikw, iku, iko,          &
     &              nkmax, mhalf)      !iw+iu=io
      !io=ix4*(iy2*iz3-iz2*iy3)+iy4*(iz2*ix3-ix2*iz3)+iz4*(ix2*iy3-iy2*ix3)
      !  =[x4, y4, z4]dot[x2, y2, z2]cross[x3, y3, z3]
      !  ="six times" the volume of tetrahedral[1234]
!c
      return
      end subroutine idenom

!=======================================================================

      subroutine intins(k, xi, yi, zi, wi, x, y, z, w, x2, y2, z2, w2,  &
     &                  icon, ih, ihn, nhmax, nvmax, tetra, curr, is,   &
     &                  ifl, newtts, ired, delaun, flphis, mhalf,       &
     &                  mfull, isclp, isclw, isclr, epz)
!c
      real(8) :: xi(:), yi(:), zi(:), wi(:)
      integer(8) :: x(:), y(:), z(:), w(:), x2(:), y2(:), z2(:), w2(:)
      integer(8) :: icon(:,:), is(:), ifl(:), ih(:)
      double precision epz, tdist, xctr, yctr, zctr
      integer*8 ihn, nhmax, nvmax, newtts, ired, mhalf, mfull
      integer(8) :: isclp(:), isclw(:), isclr(:)
      integer*8 tetra, curr, adj, a, b, c, d, newi(4)
      logical delaun, flphis
      integer*8 k, ipossi, itide, new1, new2, new3, new4, i, j
!c
      a = icon(5,curr)
      b = icon(6,curr)
      c = icon(7,curr)
      d = icon(8,curr)
!c
      if(delaun) go to 30
      if(a.le.8 .or. b.le.8 .or. c.le.8 .or. d.le.8) go to 30
      call ctrad(xi, yi, zi, wi, xctr, yctr, zctr, a, b, c, d,          &
     &           epz, delaun, ipossi)
      if(ipossi.eq.1) go to 20
      call bisphr(xi, yi, zi, wi, k, a, epz, xctr, yctr, zctr, tdist,   &
     &            delaun, ipossi)
      if(ipossi.eq.1) go to 20
      itide = 1
      if(tdist.gt.0.0d0) itide = -1
      go to 25
   20 continue
      call iqsign(x, y, z, w, x2, y2, z2, w2, a, b, c, d, k,            &
     &            mhalf, mfull, isclp, isclw, isclr, delaun, itide)
   25 continue
      if(itide.le.0) go to 30
      is(k) = -3
      ired = 1
      go to 1000
!c
   30 continue
      ired = 0
!c
      if(flphis) then
         new1 = tetra + 1
         new2 = tetra + 2
         new3 = tetra + 3
         new4 = tetra + 4
         tetra = new4
         if (tetra .gt. nvmax) stop 1210
      else
         if(ihn.eq.0) then
            new1 = tetra+1
            tetra = new1
            if(tetra .gt. nvmax) stop 1220
         else
            new1 = ih(ihn)
            ihn = ihn-1
         endif
         if(ihn.eq.0) then
            new2 = tetra+1
            tetra = new2
            if(tetra .gt. nvmax) stop 1230
         else
            new2 = ih(ihn)
            ihn = ihn-1
         endif
         if(ihn.eq.0) then
            new3 = tetra+1
            tetra = new3
            if(tetra .gt. nvmax) stop 1240
         else
            new3 = ih(ihn)
            ihn = ihn-1
         endif
         if(ihn.eq.0) then
            new4 = tetra+1
            tetra = new4
            if(tetra .gt. nvmax) stop 1250
         else
            new4 = ih(ihn)
            ihn = ihn-1
         endif
      endif
!c
      ifl(1) = new1
      ifl(2) = new2
      ifl(3) = new3
      ifl(4) = new4
!c
      newtts = 4
!c
      do 50 i = 1, 8
         icon(i,new1) = icon(i,curr)
         icon(i,new2) = icon(i,curr)
         icon(i,new3) = icon(i,curr)
         icon(i,new4) = icon(i,curr)
   50 continue
!c
!c     update new1
!c
      icon(2,new1) = new2
      icon(3,new1) = new3
      icon(4,new1) = new4
      icon(5,new1) = k
!c
!c     update new2
!c
      icon(1,new2) = new1
      icon(3,new2) = new3
      icon(4,new2) = new4
      icon(6,new2) = k
      call sitord (icon, k, new2)
!c
!c     update new3
!c
      icon(1,new3) = new1
      icon(2,new3) = new2
      icon(4,new3) = new4
      icon(7,new3) = k
      call sitord (icon, k, new3)
!c
!c     update new4
!c
      icon(1,new4) = new1
      icon(2,new4) = new2
      icon(3,new4) = new3
      icon(8,new4) = k
      call sitord (icon, k, new4)
!c
!c     update is(:)
!c
      is(k) = new1
!c
      is(a) = new2
      is(b) = new1
      is(c) = new1
      is(d) = new1
!c
!c     update neighboring tetra
!c
      newi(1) = new1
      newi(2) = new2
      newi(3) = new3
      newi(4) = new4
      do 200 i = 1, 4
         adj = icon(i,curr)
         if (adj .ne. 0) then
            do 175 j = 1, 4
               if (icon(j,adj) .eq. curr) then
                  icon(j,adj) = newi(i)
                  goto 200
               endif
  175       continue
            stop 1260
         endif
  200 continue
!c
!c     flag current tetra to denote that it has children
!c
      if(flphis) then
         icon(5,curr) = - icon(5,curr)
         icon(1,curr) = new1
         icon(2,curr) = new2
         icon(3,curr) = new3
         icon(4,curr) = new4
      else
         icon(5,curr) = - icon(5,curr)
         ihn = ihn+1
         if(ihn.gt.nhmax) stop 1270
         ih(ihn) = curr
      endif
!c
 1000 continue
      return
      end subroutine intins

!=======================================================================

      SUBROUTINE ipiksr4(n,arr,brr)
      INTEGER*8 n
      INTEGER*8 arr(4,n),brr(n)
      INTEGER*8 i,j,k
      INTEGER*8 a(4),b

      do 12 j=2,n

        do 6 k=1,4
          a(k)=arr(k,j)
 6      continue
        b=brr(j)

        do 9 i=j-1,1,-1
          if(arr(1,i).gt.a(1)) then
            go to 7
          elseif(arr(1,i).lt.a(1)) then
            go to 10
          else
            if(arr(2,i).gt.a(2)) then
              go to 7
            elseif(arr(2,i).lt.a(2)) then
              go to 10
            else
              if(arr(3,i).gt.a(3)) then
                go to 7
              elseif(arr(3,i).lt.a(3)) then
                go to 10
              else
                if(arr(4,i).gt.a(4)) then
                  go to 7
                else
                  go to 10
                endif
              endif
            endif
          endif
 7        do 8 k=1,4
            arr(k,i+1)=arr(k,i)
 8        continue
          brr(i+1)=brr(i)
 9      continue
        i=0

10      do 11 k=1,4
          arr(k,i+1)=a(k)
11      continue
        brr(i+1)=b

12    continue

      return
      END SUBROUTINE ipiksr4

!=======================================================================

      SUBROUTINE ipiksrt(n,arr)
      INTEGER*8 n
      INTEGER*8 arr(n)
      INTEGER*8 i,j
      INTEGER*8 a

      do 12 j=2,n
        a=arr(j)
        do 11 i=j-1,1,-1
          if(arr(i).le.a)goto 10
          arr(i+1)=arr(i)
11      continue
        i=0
10      arr(i+1)=a
12    continue

      return
      END SUBROUTINE ipiksrt

!=======================================================================

      subroutine ipsig1(x, y, z, x2, y2, z2, xc, yc, zc, ifir,          &
     &                  isec, ithi, ifou, ifif, isix, mhalf,            &
     &                  mfull, isclp, ipout)
!c
      integer(8) :: x(:),y(:),z(:), x2(:),y2(:),z2(:), xc(:), yc(:),    &
     &         zc(:)
      integer*8 ifir, isec, ithi, ifou, ifif, isix
      integer(8) :: isclp(:), mhalf, mfull, nkmax, ipout
      parameter (nkmax = 30)
      integer*8 io(nkmax), iu(nkmax), iv(nkmax)
      integer*8 ix2(nkmax), iy2(nkmax), iz2(nkmax)
      integer*8 ix3(nkmax), iy3(nkmax), iz3(nkmax)
      integer*8 ix4(nkmax), iy4(nkmax), iz4(nkmax)
      integer*8 ix5(nkmax), iy5(nkmax), iz5(nkmax)
      integer*8 ix6(nkmax), iy6(nkmax), iz6(nkmax)
      integer*8 ixf(nkmax), iyf(nkmax), izf(nkmax)
      integer*8 ixthw, iythw, izthw, ixfow, iyfow, izfow
      integer*8 ixfiw, iyfiw, izfiw, ixsiw, iysiw, izsiw
      integer*8 ixfuw, iyfuw, izfuw, ixsuw, iysuw, izsuw
      integer*8 ixth2, iyth2, izth2, ixfo2, iyfo2, izfo2
      integer*8 ixfi2, iyfi2, izfi2, ixsi2, iysi2, izsi2
      integer*8 isgxf, isgyf, isgzf, ikxf, ikyf, ikzf
      integer*8 isgx2, isgy2, isgz2, ikx2, iky2, ikz2
      integer*8 isgx3, isgy3, isgz3, ikx3, iky3, ikz3
      integer*8 isgx4, isgy4, isgz4, ikx4, iky4, ikz4
      integer*8 isgx5, isgy5, isgz5, ikx5, iky5, ikz5
      integer*8 isgx6, isgy6, isgz6, ikx6, iky6, ikz6
      integer*8 isgo, isgu, isgv, iko, iku, ikv
!c
      ixthw = x(ithi)
      iythw = y(ithi)
      izthw = z(ithi)
      ixfow = x(ifou)
      iyfow = y(ifou)
      izfow = z(ifou)
      ixfiw = x(ifif)
      iyfiw = y(ifif)
      izfiw = z(ifif)
      ixsiw = x(isix)
      iysiw = y(isix)
      izsiw = z(isix)
!c
      ixth2 = x2(ithi)
      iyth2 = y2(ithi)
      izth2 = z2(ithi)
      ixfo2 = x2(ifou)
      iyfo2 = y2(ifou)
      izfo2 = z2(ifou)
      ixfi2 = x2(ifif)
      iyfi2 = y2(ifif)
      izfi2 = z2(ifif)
      ixsi2 = x2(isix)
      iysi2 = y2(isix)
      izsi2 = z2(isix)
!c
      ixfuw = xc(ifir)
      iyfuw = yc(ifir)
      izfuw = zc(ifir)
      ixsuw = xc(isec)
      iysuw = yc(isec)
      izsuw = zc(isec)
!c
      ikxf = 2
      ikyf = 2
      ikzf = 2
      call decomp(ixf, isgxf, ixfuw, mhalf)
      call decomp(iyf, isgyf, iyfuw, mhalf)
      call decomp(izf, isgzf, izfuw, mhalf)
!c
      call decmp2(ix3, isgx3, ikx3, ixthw, ixth2, mhalf, mfull, isclp)
      call decmp2(iy3, isgy3, iky3, iythw, iyth2, mhalf, mfull, isclp)
      call decmp2(iz3, isgz3, ikz3, izthw, izth2, mhalf, mfull, isclp)
      call decmp2(ix5, isgx5, ikx5, ixfiw, ixfi2, mhalf, mfull, isclp)
      call decmp2(iy5, isgy5, iky5, iyfiw, iyfi2, mhalf, mfull, isclp)
      call decmp2(iz5, isgz5, ikz5, izfiw, izfi2, mhalf, mfull, isclp)
!c
      iko = 2
      call decomp(io, isgo, ixsuw, mhalf)
      call muldif(io, ixf, ix2, isgo, isgxf, isgx2, iko, ikxf, ikx2,    &
     &              nkmax, mhalf)
      call decomp(io, isgo, iysuw, mhalf)
      call muldif(io, iyf, iy2, isgo, isgyf, isgy2, iko, ikyf, iky2,    &
     &              nkmax, mhalf)
      call decomp(io, isgo, izsuw, mhalf)
      call muldif(io, izf, iz2, isgo, isgzf, isgz2, iko, ikzf, ikz2,    &
     &              nkmax, mhalf)
!c
      call decmp2(io, isgo, iko, ixfow, ixfo2, mhalf, mfull, isclp)
      call muldif(io, ix3, ix4, isgo, isgx3, isgx4, iko, ikx3, ikx4,    &
     &              nkmax, mhalf)
      call decmp2(io, isgo, iko, iyfow, iyfo2, mhalf, mfull, isclp)
      call muldif(io, iy3, iy4, isgo, isgy3, isgy4, iko, iky3, iky4,    &
     &              nkmax, mhalf)
      call decmp2(io, isgo, iko, izfow, izfo2, mhalf, mfull, isclp)
      call muldif(io, iz3, iz4, isgo, isgz3, isgz4, iko, ikz3, ikz4,    &
     &              nkmax, mhalf)
      call decmp2(io, isgo, iko, ixsiw, ixsi2, mhalf, mfull, isclp)
      call muldif(io, ix5, ix6, isgo, isgx5, isgx6, iko, ikx5, ikx6,    &
     &              nkmax, mhalf)
      call decmp2(io, isgo, iko, iysiw, iysi2, mhalf, mfull, isclp)
      call muldif(io, iy5, iy6, isgo, isgy5, isgy6, iko, iky5, iky6,    &
     &              nkmax, mhalf)
      call decmp2(io, isgo, iko, izsiw, izsi2, mhalf, mfull, isclp)
      call muldif(io, iz5, iz6, isgo, isgz5, isgz6, iko, ikz5, ikz6,    &
     &              nkmax, mhalf)
!c
      call mulmul(iy2, iz4, io, isgy2, isgz4, isgo, iky2, ikz4, iko,    &
     &              nkmax, mhalf)
      call mulmul(io, ix6, iv, isgo, isgx6, isgv, iko, ikx6, ikv,       &
     &              nkmax, mhalf)
!c
      call mulmul(iz2, ix4, io, isgz2, isgx4, isgo, ikz2, ikx4, iko,    &
     &              nkmax, mhalf)
      call mulmul(io, iy6, iu, isgo, isgy6, isgu, iko, iky6, iku,       &
     &              nkmax, mhalf)
      isgu =-isgu
      call muldif(iv, iu, io, isgv, isgu, isgo, ikv, iku, iko,          &
     &              nkmax, mhalf)
!c
      call mulmul(ix2, iy4, iv, isgx2, isgy4, isgv, ikx2, iky4, ikv,    &
     &              nkmax, mhalf)
      call mulmul(iv, iz6, iu, isgv, isgz6, isgu, ikv, ikz6, iku,       &
     &              nkmax, mhalf)
      isgu =-isgu
      call muldif(io, iu, iv, isgo, isgu, isgv, iko, iku, ikv,          &
     &              nkmax, mhalf)
!c
      call mulmul(iz2, iy4, io, isgz2, isgy4, isgo, ikz2, iky4, iko,    &
     &              nkmax, mhalf)
      call mulmul(io, ix6, iu, isgo, isgx6, isgu, iko, ikx6, iku,       &
     &              nkmax, mhalf)
      call muldif(iv, iu, io, isgv, isgu, isgo, ikv, iku, iko,          &
     &              nkmax, mhalf)
!c
      call mulmul(ix2, iz4, iv, isgx2, isgz4, isgv, ikx2, ikz4, ikv,    &
     &              nkmax, mhalf)
      call mulmul(iv, iy6, iu, isgv, isgy6, isgu, ikv, iky6, iku,       &
     &              nkmax, mhalf)
      call muldif(io, iu, iv, isgo, isgu, isgv, iko, iku, ikv,          &
     &              nkmax, mhalf)
!c
      call mulmul(iy2, ix4, io, isgy2, isgx4, isgo, iky2, ikx4, iko,    &
     &              nkmax, mhalf)
      call mulmul(io, iz6, iu, isgo, isgz6, isgu, iko, ikz6, iku,       &
     &              nkmax, mhalf)
      call muldif(iv, iu, io, isgv, isgu, isgo, ikv, iku, iko,          &
     &              nkmax, mhalf)
!c
      ipout = isgo
!c
      return
      end subroutine ipsig1

!=======================================================================

      subroutine ipsig2(x, y, z, x2, y2, z2, xc, yc, zc, ifir,          &
     &                  isec, ithi, ifou, ifif, isix, mhalf,            &
     &                  mfull, isclp, ipout)
!c
      integer(8) :: x(:),y(:),z(:),x2(:),y2(:),z2(:),xc(:),yc(:),       &
     &          zc(:)
      integer*8 ifir, isec, ithi, ifou, ifif, isix
      integer(8) :: isclp(:), mhalf, mfull, nkmax, ipout
      parameter (nkmax = 30)
      integer*8 io(nkmax), iu(nkmax), iv(nkmax)
      integer*8 ix2(nkmax), iy2(nkmax), iz2(nkmax)
      integer*8 ix3(nkmax), iy3(nkmax), iz3(nkmax)
      integer*8 ix4(nkmax), iy4(nkmax), iz4(nkmax)
      integer*8 ix5(nkmax), iy5(nkmax), iz5(nkmax)
      integer*8 ix6(nkmax), iy6(nkmax), iz6(nkmax)
      integer*8 ixf(nkmax), iyf(nkmax), izf(nkmax)
      integer*8 ixthw, iythw, izthw, ixfow, iyfow, izfow
      integer*8 ixfiw, iyfiw, izfiw, ixsiw, iysiw, izsiw
      integer*8 ixfuw, iyfuw, izfuw, ixsuw, iysuw, izsuw
      integer*8 ixfi2, iyfi2, izfi2, ixsi2, iysi2, izsi2
      integer*8 isgxf, isgyf, isgzf, ikxf, ikyf, ikzf
      integer*8 isgx2, isgy2, isgz2, ikx2, iky2, ikz2
      integer*8 isgx3, isgy3, isgz3, ikx3, iky3, ikz3
      integer*8 isgx4, isgy4, isgz4, ikx4, iky4, ikz4
      integer*8 isgx5, isgy5, isgz5, ikx5, iky5, ikz5
      integer*8 isgx6, isgy6, isgz6, ikx6, iky6, ikz6
      integer*8 isgo, isgu, isgv, iko, iku, ikv
!c
      ixfiw = x(ifif)
      iyfiw = y(ifif)
      izfiw = z(ifif)
      ixsiw = x(isix)
      iysiw = y(isix)
      izsiw = z(isix)
!c
      ixfi2 = x2(ifif)
      iyfi2 = y2(ifif)
      izfi2 = z2(ifif)
      ixsi2 = x2(isix)
      iysi2 = y2(isix)
      izsi2 = z2(isix)
!c
      ixfuw = xc(ifir)
      iyfuw = yc(ifir)
      izfuw = zc(ifir)
      ixsuw = xc(isec)
      iysuw = yc(isec)
      izsuw = zc(isec)
      ixthw = xc(ithi)
      iythw = yc(ithi)
      izthw = zc(ithi)
      ixfow = xc(ifou)
      iyfow = yc(ifou)
      izfow = zc(ifou)
!c
      ikxf = 2
      ikyf = 2
      ikzf = 2
      ikx3 = 2
      iky3 = 2
      ikz3 = 2
      call decomp(ixf, isgxf, ixfuw, mhalf)
      call decomp(iyf, isgyf, iyfuw, mhalf)
      call decomp(izf, isgzf, izfuw, mhalf)
      call decomp(ix3, isgx3, ixthw, mhalf)
      call decomp(iy3, isgy3, iythw, mhalf)
      call decomp(iz3, isgz3, izthw, mhalf)
!c
      call decmp2(ix5, isgx5, ikx5, ixfiw, ixfi2, mhalf, mfull, isclp)
      call decmp2(iy5, isgy5, iky5, iyfiw, iyfi2, mhalf, mfull, isclp)
      call decmp2(iz5, isgz5, ikz5, izfiw, izfi2, mhalf, mfull, isclp)
!c
      iko = 2
      call decomp(io, isgo, ixsuw, mhalf)
      call muldif(io, ixf, ix2, isgo, isgxf, isgx2, iko, ikxf, ikx2,    &
     &              nkmax, mhalf)
      call decomp(io, isgo, iysuw, mhalf)
      call muldif(io, iyf, iy2, isgo, isgyf, isgy2, iko, ikyf, iky2,    &
     &              nkmax, mhalf)
      call decomp(io, isgo, izsuw, mhalf)
      call muldif(io, izf, iz2, isgo, isgzf, isgz2, iko, ikzf, ikz2,    &
     &              nkmax, mhalf)
      call decomp(io, isgo, ixfow, mhalf)
      call muldif(io, ix3, ix4, isgo, isgx3, isgx4, iko, ikx3, ikx4,    &
     &              nkmax, mhalf)
      call decomp(io, isgo, iyfow, mhalf)
      call muldif(io, iy3, iy4, isgo, isgy3, isgy4, iko, iky3, iky4,    &
     &              nkmax, mhalf)
      call decomp(io, isgo, izfow, mhalf)
      call muldif(io, iz3, iz4, isgo, isgz3, isgz4, iko, ikz3, ikz4,    &
     &              nkmax, mhalf)
!c
      call decmp2(io, isgo, iko, ixsiw, ixsi2, mhalf, mfull, isclp)
      call muldif(io, ix5, ix6, isgo, isgx5, isgx6, iko, ikx5, ikx6,    &
     &              nkmax, mhalf)
      call decmp2(io, isgo, iko, iysiw, iysi2, mhalf, mfull, isclp)
      call muldif(io, iy5, iy6, isgo, isgy5, isgy6, iko, iky5, iky6,    &
     &              nkmax, mhalf)
      call decmp2(io, isgo, iko, izsiw, izsi2, mhalf, mfull, isclp)
      call muldif(io, iz5, iz6, isgo, isgz5, isgz6, iko, ikz5, ikz6,    &
     &              nkmax, mhalf)
!c
      call mulmul(iy2, iz4, io, isgy2, isgz4, isgo, iky2, ikz4, iko,    &
     &              nkmax, mhalf)
      call mulmul(io, ix6, iv, isgo, isgx6, isgv, iko, ikx6, ikv,       &
     &              nkmax, mhalf)
!c
      call mulmul(iz2, ix4, io, isgz2, isgx4, isgo, ikz2, ikx4, iko,    &
     &              nkmax, mhalf)
      call mulmul(io, iy6, iu, isgo, isgy6, isgu, iko, iky6, iku,       &
     &              nkmax, mhalf)
      isgu =-isgu
      call muldif(iv, iu, io, isgv, isgu, isgo, ikv, iku, iko,          &
     &              nkmax, mhalf)
!c
      call mulmul(ix2, iy4, iv, isgx2, isgy4, isgv, ikx2, iky4, ikv,    &
     &              nkmax, mhalf)
      call mulmul(iv, iz6, iu, isgv, isgz6, isgu, ikv, ikz6, iku,       &
     &              nkmax, mhalf)
      isgu =-isgu
      call muldif(io, iu, iv, isgo, isgu, isgv, iko, iku, ikv,          &
     &              nkmax, mhalf)
!c
      call mulmul(iz2, iy4, io, isgz2, isgy4, isgo, ikz2, iky4, iko,    &
     &              nkmax, mhalf)
      call mulmul(io, ix6, iu, isgo, isgx6, isgu, iko, ikx6, iku,       &
     &              nkmax, mhalf)
      call muldif(iv, iu, io, isgv, isgu, isgo, ikv, iku, iko,          &
     &              nkmax, mhalf)
!c
      call mulmul(ix2, iz4, iv, isgx2, isgz4, isgv, ikx2, ikz4, ikv,    &
     &              nkmax, mhalf)
      call mulmul(iv, iy6, iu, isgv, isgy6, isgu, ikv, iky6, iku,       &
     &              nkmax, mhalf)
      call muldif(io, iu, iv, isgo, isgu, isgv, iko, iku, ikv,          &
     &              nkmax, mhalf)
!c
      call mulmul(iy2, ix4, io, isgy2, isgx4, isgo, iky2, ikx4, iko,    &
     &              nkmax, mhalf)
      call mulmul(io, iz6, iu, isgo, isgz6, isgu, iko, ikz6, iku,       &
     &              nkmax, mhalf)
      call muldif(iv, iu, io, isgv, isgu, isgo, ikv, iku, iko,          &
     &              nkmax, mhalf)
!c
      ipout = isgo
!c
      return
      end subroutine ipsig2

!=======================================================================

      subroutine ipsig3(x, y, z, x2, y2, z2, xc, yc, zc, ifir, isec,    &
     &                  ithi, ifou, mhalf, mfull, isclp, ifn, ipout)
!c
      integer(8) :: x(:), y(:), z(:), x2(:), y2(:), z2(:), xc(:),yc(:), &
     &         zc(:)
      integer*8 ifir, isec, ithi, ifou
      integer(8) :: isclp(:), mhalf, mfull, ifn, nkmax, ipout
      parameter (nkmax = 30)
      integer*8 io(nkmax), iu(nkmax), iv(nkmax)
      integer*8 ix2(nkmax), iy2(nkmax), iz2(nkmax)
      integer*8 ix3(nkmax), iy3(nkmax), iz3(nkmax)
      integer*8 ixf(nkmax), iyf(nkmax), izf(nkmax)
      integer*8 ix4(nkmax), iy4(nkmax), iz4(nkmax)
      integer*8 ixfiw, iyfiw, izfiw, ixsew, iysew, izsew
      integer*8 ixthw, iythw, izthw, ixfow, iyfow, izfow
      integer*8 ixfi2, iyfi2, izfi2, ixse2, iyse2, izse2
      integer*8 ixth2, iyth2, izth2
      integer*8 isgxf, isgyf, isgzf, ikxf, ikyf, ikzf
      integer*8 isgx2, isgy2, isgz2, ikx2, iky2, ikz2
      integer*8 isgx3, isgy3, isgz3, ikx3, iky3, ikz3
      integer*8 isgx4, isgy4, isgz4, ikx4, iky4, ikz4
      integer*8 isgo, isgu, isgv, iko, iku, ikv
!c
      ixfiw = x(ifir)
      iyfiw = y(ifir)
      izfiw = z(ifir)
      ixsew = x(isec)
      iysew = y(isec)
      izsew = z(isec)
      ixthw = x(ithi)
      iythw = y(ithi)
      izthw = z(ithi)
!c
      ixfi2 = x2(ifir)
      iyfi2 = y2(ifir)
      izfi2 = z2(ifir)
      ixse2 = x2(isec)
      iyse2 = y2(isec)
      izse2 = z2(isec)
      ixth2 = x2(ithi)
      iyth2 = y2(ithi)
      izth2 = z2(ithi)
!c
      ixfow = xc(ifou)
      iyfow = yc(ifou)
      izfow = zc(ifou)
!c
      call decmp2(ixf, isgxf, ikxf, ixfiw, ixfi2, mhalf, mfull, isclp)
      call decmp2(iyf, isgyf, ikyf, iyfiw, iyfi2, mhalf, mfull, isclp)
      call decmp2(izf, isgzf, ikzf, izfiw, izfi2, mhalf, mfull, isclp)
!c
      ikx4 = 2
      iky4 = 2
      ikz4 = 2
      call decomp(ix4, isgx4, ixfow, mhalf)
      call decomp(iy4, isgy4, iyfow, mhalf)
      call decomp(iz4, isgz4, izfow, mhalf)
!c
      call decmp2(io, isgo, iko, ixsew, ixse2, mhalf, mfull, isclp)
      call muldif(io, ixf, ix2, isgo, isgxf, isgx2, iko, ikxf, ikx2,    &
     &              nkmax, mhalf)
      call decmp2(io, isgo, iko, iysew, iyse2, mhalf, mfull, isclp)
      call muldif(io, iyf, iy2, isgo, isgyf, isgy2, iko, ikyf, iky2,    &
     &              nkmax, mhalf)
      call decmp2(io, isgo, iko, izsew, izse2, mhalf, mfull, isclp)
      call muldif(io, izf, iz2, isgo, isgzf, isgz2, iko, ikzf, ikz2,    &
     &              nkmax, mhalf)
      call decmp2(io, isgo, iko, ixthw, ixth2, mhalf, mfull, isclp)
      call muldif(io, ixf, ix3, isgo, isgxf, isgx3, iko, ikxf, ikx3,    &
     &              nkmax, mhalf)
      call decmp2(io, isgo, iko, iythw, iyth2, mhalf, mfull, isclp)
      call muldif(io, iyf, iy3, isgo, isgyf, isgy3, iko, ikyf, iky3,    &
     &              nkmax, mhalf)
      call decmp2(io, isgo, iko, izthw, izth2, mhalf, mfull, isclp)
      call muldif(io, izf, iz3, isgo, isgzf, isgz3, iko, ikzf, ikz3,    &
     &              nkmax, mhalf)
!c
      call mulmul(iy4, iz2, io, isgy4, isgz2, isgo, iky4, ikz2, iko,    &
     &              nkmax, mhalf)
      call mulmul(io, ix3, iv, isgo, isgx3, isgv, iko, ikx3, ikv,       &
     &              nkmax, mhalf)
!c
      call mulmul(iz4, ix2, io, isgz4, isgx2, isgo, ikz4, ikx2, iko,    &
     &              nkmax, mhalf)
      call mulmul(io, iy3, iu, isgo, isgy3, isgu, iko, iky3, iku,       &
     &              nkmax, mhalf)
      isgu =-isgu
      call muldif(iv, iu, io, isgv, isgu, isgo, ikv, iku, iko,          &
     &              nkmax, mhalf)
!c
      call mulmul(ix4, iy2, iv, isgx4, isgy2, isgv, ikx4, iky2, ikv,    &
     &              nkmax, mhalf)
      call mulmul(iv, iz3, iu, isgv, isgz3, isgu, ikv, ikz3, iku,       &
     &              nkmax, mhalf)
      isgu =-isgu
      call muldif(io, iu, iv, isgo, isgu, isgv, iko, iku, ikv,          &
     &              nkmax, mhalf)
!c
      call mulmul(iz4, iy2, io, isgz4, isgy2, isgo, ikz4, iky2, iko,    &
     &              nkmax, mhalf)
      call mulmul(io, ix3, iu, isgo, isgx3, isgu, iko, ikx3, iku,       &
     &              nkmax, mhalf)
      call muldif(iv, iu, io, isgv, isgu, isgo, ikv, iku, iko,          &
     &              nkmax, mhalf)
!c
      call mulmul(ix4, iz2, iv, isgx4, isgz2, isgv, ikx4, ikz2, ikv,    &
     &              nkmax, mhalf)
      call mulmul(iv, iy3, iu, isgv, isgy3, isgu, ikv, iky3, iku,       &
     &              nkmax, mhalf)
      call muldif(io, iu, iv, isgo, isgu, isgv, iko, iku, ikv,          &
     &              nkmax, mhalf)
!c
      call mulmul(iy4, ix2, io, isgy4, isgx2, isgo, iky4, ikx2, iko,    &
     &              nkmax, mhalf)
      call mulmul(io, iz3, iu, isgo, isgz3, isgu, iko, ikz3, iku,       &
     &              nkmax, mhalf)
      call muldif(iv, iu, io, isgv, isgu, isgo, ikv, iku, iko,          &
     &              nkmax, mhalf)
!c
      ipout = isgo
      if(ifn.eq.1) ipout = -isgo
!c
      return
      end subroutine ipsig3

!=======================================================================

      subroutine ipsig4(x, y, z, x2, y2, z2, xc, yc, zc, ifir, isec,    &
     &                  ithi, ifou, mhalf, mfull, isclp, ipout)
!c
      integer(8) :: x(:),y(:),z(:), x2(:),y2(:),z2(:), xc(:), yc(:),    &
     &          zc(:)
      integer*8 ifir, isec, ithi, ifou
      integer(8) :: isclp(:), mhalf, mfull, nkmax, ipout
      parameter (nkmax = 30)
      integer*8 io(nkmax), iu(nkmax), iv(nkmax)
      integer*8 ix2(nkmax), iy2(nkmax), iz2(nkmax)
      integer*8 ix3(nkmax), iy3(nkmax), iz3(nkmax)
      integer*8 ix4(nkmax), iy4(nkmax), iz4(nkmax)
      integer*8 ixf(nkmax), iyf(nkmax), izf(nkmax)
      integer*8 ixthw, iythw, izthw, ixfow, iyfow, izfow
      integer*8 ixfiw, iyfiw, izfiw, ixsew, iysew, izsew
      integer*8 ixth2, iyth2, izth2, ixfo2, iyfo2, izfo2
      integer*8 isgxf, isgyf, isgzf, ikxf, ikyf, ikzf
      integer*8 isgx2, isgy2, isgz2, ikx2, iky2, ikz2
      integer*8 isgx3, isgy3, isgz3, ikx3, iky3, ikz3
      integer*8 isgx4, isgy4, isgz4, ikx4, iky4, ikz4
      integer*8 isgo, isgu, isgv, iko, iku, ikv
!c
      ixthw = x(ithi)
      iythw = y(ithi)
      izthw = z(ithi)
      ixfow = x(ifou)
      iyfow = y(ifou)
      izfow = z(ifou)
!c
      ixth2 = x2(ithi)
      iyth2 = y2(ithi)
      izth2 = z2(ithi)
      ixfo2 = x2(ifou)
      iyfo2 = y2(ifou)
      izfo2 = z2(ifou)
!c
      ixfiw = xc(ifir)
      iyfiw = yc(ifir)
      izfiw = zc(ifir)
      ixsew = xc(isec)
      iysew = yc(isec)
      izsew = zc(isec)
!c
      call decmp2(ixf, isgxf, ikxf, ixfow, ixfo2, mhalf, mfull, isclp)
      call decmp2(iyf, isgyf, ikyf, iyfow, iyfo2, mhalf, mfull, isclp)
      call decmp2(izf, isgzf, ikzf, izfow, izfo2, mhalf, mfull, isclp)
!c
      ikx2 = 2
      iky2 = 2
      ikz2 = 2
      ikx3 = 2
      iky3 = 2
      ikz3 = 2
      call decomp(ix2, isgx2, ixfiw, mhalf)
      call decomp(iy2, isgy2, iyfiw, mhalf)
      call decomp(iz2, isgz2, izfiw, mhalf)
      call decomp(ix3, isgx3, ixsew, mhalf)
      call decomp(iy3, isgy3, iysew, mhalf)
      call decomp(iz3, isgz3, izsew, mhalf)
!c
      call decmp2(io, isgo, iko, ixthw, ixth2, mhalf, mfull, isclp)
      call muldif(io, ixf, ix4, isgo, isgxf, isgx4, iko, ikxf, ikx4,    &
     &              nkmax, mhalf)
      call decmp2(io, isgo, iko, iythw, iyth2, mhalf, mfull, isclp)
      call muldif(io, iyf, iy4, isgo, isgyf, isgy4, iko, ikyf, iky4,    &
     &              nkmax, mhalf)
      call decmp2(io, isgo, iko, izthw, izth2, mhalf, mfull, isclp)
      call muldif(io, izf, iz4, isgo, isgzf, isgz4, iko, ikzf, ikz4,    &
     &              nkmax, mhalf)
!c
      call mulmul(iy2, iz3, io, isgy2, isgz3, isgo, iky2, ikz3, iko,    &
     &              nkmax, mhalf)
      call mulmul(io, ix4, iv, isgo, isgx4, isgv, iko, ikx4, ikv,       &
     &              nkmax, mhalf)
!c
      call mulmul(iz2, ix3, io, isgz2, isgx3, isgo, ikz2, ikx3, iko,    &
     &              nkmax, mhalf)
      call mulmul(io, iy4, iu, isgo, isgy4, isgu, iko, iky4, iku,       &
     &              nkmax, mhalf)
      isgu =-isgu
      call muldif(iv, iu, io, isgv, isgu, isgo, ikv, iku, iko,          &
     &              nkmax, mhalf)
!c
      call mulmul(ix2, iy3, iv, isgx2, isgy3, isgv, ikx2, iky3, ikv,    &
     &              nkmax, mhalf)
      call mulmul(iv, iz4, iu, isgv, isgz4, isgu, ikv, ikz4, iku,       &
     &              nkmax, mhalf)
      isgu =-isgu
      call muldif(io, iu, iv, isgo, isgu, isgv, iko, iku, ikv,          &
     &              nkmax, mhalf)
!c
      call mulmul(iz2, iy3, io, isgz2, isgy3, isgo, ikz2, iky3, iko,    &
     &              nkmax, mhalf)
      call mulmul(io, ix4, iu, isgo, isgx4, isgu, iko, ikx4, iku,       &
     &              nkmax, mhalf)
      call muldif(iv, iu, io, isgv, isgu, isgo, ikv, iku, iko,          &
     &              nkmax, mhalf)
!c
      call mulmul(ix2, iz3, iv, isgx2, isgz3, isgv, ikx2, ikz3, ikv,    &
     &              nkmax, mhalf)
      call mulmul(iv, iy4, iu, isgv, isgy4, isgu, ikv, iky4, iku,       &
     &              nkmax, mhalf)
      call muldif(io, iu, iv, isgo, isgu, isgv, iko, iku, ikv,          &
     &              nkmax, mhalf)
!c
      call mulmul(iy2, ix3, io, isgy2, isgx3, isgo, iky2, ikx3, iko,    &
     &              nkmax, mhalf)
      call mulmul(io, iz4, iu, isgo, isgz4, isgu, iko, ikz4, iku,       &
     &              nkmax, mhalf)
      call muldif(iv, iu, io, isgv, isgu, isgo, ikv, iku, iko,          &
     &              nkmax, mhalf)
!c
      ipout = isgo
!c
      return
      end subroutine ipsig4

!=======================================================================

      subroutine ipsig6(x, y, z, x2, y2, z2, xc, yc, zc, ifir, isec,    &
     &                  ithi, ifou, mhalf, mfull, isclp, ipout)
!c
      integer(8) :: x(:), y(:), z(:), x2(:), y2(:), z2(:), xc(:), yc(:),&
     &         zc(:)
      integer*8 ifir, isec, ithi, ifou
      integer(8) :: isclp(:), mhalf, mfull, nkmax, ipout
      parameter (nkmax = 30)
      integer*8 io(nkmax), iu(nkmax), iv(nkmax)
      integer*8 ix2(nkmax), iy2(nkmax), iz2(nkmax)
      integer*8 ix3(nkmax), iy3(nkmax), iz3(nkmax)
      integer*8 ix4(nkmax), iy4(nkmax), iz4(nkmax)
      integer*8 ix5(nkmax), iy5(nkmax), iz5(nkmax)
      integer*8 ix6(nkmax), iy6(nkmax), iz6(nkmax)
      integer*8 ixf(nkmax), iyf(nkmax), izf(nkmax)
      integer*8 ixthw, iythw, izthw, ixfow, iyfow, izfow
      integer*8 ixfiw, iyfiw, izfiw, ixsew, iysew, izsew
      integer*8 ixfuw, iyfuw, izfuw, ixsuw, iysuw, izsuw
      integer*8 ixth2, iyth2, izth2, ixfo2, iyfo2, izfo2
      integer*8 ixfi2, iyfi2, izfi2, ixse2, iyse2, izse2
      integer*8 isgxf, isgyf, isgzf, ikxf, ikyf, ikzf
      integer*8 isgx2, isgy2, isgz2, ikx2, iky2, ikz2
      integer*8 isgx3, isgy3, isgz3, ikx3, iky3, ikz3
      integer*8 isgx4, isgy4, isgz4, ikx4, iky4, ikz4
      integer*8 isgx5, isgy5, isgz5, ikx5, iky5, ikz5
      integer*8 isgx6, isgy6, isgz6, ikx6, iky6, ikz6
      integer*8 isgo, isgu, isgv, iko, iku, ikv
!c
      ixfiw = x(ifir)
      iyfiw = y(ifir)
      izfiw = z(ifir)
      ixsew = x(isec)
      iysew = y(isec)
      izsew = z(isec)
      ixthw = x(ithi)
      iythw = y(ithi)
      izthw = z(ithi)
      ixfow = x(ifou)
      iyfow = y(ifou)
      izfow = z(ifou)
!c
      ixfi2 = x2(ifir)
      iyfi2 = y2(ifir)
      izfi2 = z2(ifir)
      ixse2 = x2(isec)
      iyse2 = y2(isec)
      izse2 = z2(isec)
      ixth2 = x2(ithi)
      iyth2 = y2(ithi)
      izth2 = z2(ithi)
      ixfo2 = x2(ifou)
      iyfo2 = y2(ifou)
      izfo2 = z2(ifou)
!c
      ixfuw = xc(ifir)
      iyfuw = yc(ifir)
      izfuw = zc(ifir)
      ixsuw = xc(isec)
      iysuw = yc(isec)
      izsuw = zc(isec)
!c
      call decmp2(ixf, isgxf, ikxf, ixfow, ixfo2, mhalf, mfull, isclp)
      call decmp2(iyf, isgyf, ikyf, iyfow, iyfo2, mhalf, mfull, isclp)
      call decmp2(izf, isgzf, ikzf, izfow, izfo2, mhalf, mfull, isclp)
!c
      ikx5 = 2
      iky5 = 2
      ikz5 = 2
      ikx6 = 2
      iky6 = 2
      ikz6 = 2
      call decomp(ix5, isgx5, ixfuw, mhalf)
      call decomp(iy5, isgy5, iyfuw, mhalf)
      call decomp(iz5, isgz5, izfuw, mhalf)
      call decomp(ix6, isgx6, ixsuw, mhalf)
      call decomp(iy6, isgy6, iysuw, mhalf)
      call decomp(iz6, isgz6, izsuw, mhalf)
!c
      call decmp2(io, isgo, iko, ixfiw, ixfi2, mhalf, mfull, isclp)
      call muldif(io, ixf, ix2, isgo, isgxf, isgx2, iko, ikxf, ikx2,    &
     &              nkmax, mhalf)
      call decmp2(io, isgo, iko, iyfiw, iyfi2, mhalf, mfull, isclp)
      call muldif(io, iyf, iy2, isgo, isgyf, isgy2, iko, ikyf, iky2,    &
     &              nkmax, mhalf)
      call decmp2(io, isgo, iko, izfiw, izfi2, mhalf, mfull, isclp)
      call muldif(io, izf, iz2, isgo, isgzf, isgz2, iko, ikzf, ikz2,    &
     &              nkmax, mhalf)
      call decmp2(io, isgo, iko, ixsew, ixse2, mhalf, mfull, isclp)
      call muldif(io, ixf, ix3, isgo, isgxf, isgx3, iko, ikxf, ikx3,    &
     &              nkmax, mhalf)
      call decmp2(io, isgo, iko, iysew, iyse2, mhalf, mfull, isclp)
      call muldif(io, iyf, iy3, isgo, isgyf, isgy3, iko, ikyf, iky3,    &
     &              nkmax, mhalf)
      call decmp2(io, isgo, iko, izsew, izse2, mhalf, mfull, isclp)
      call muldif(io, izf, iz3, isgo, isgzf, isgz3, iko, ikzf, ikz3,    &
     &              nkmax, mhalf)
      call decmp2(io, isgo, iko, ixthw, ixth2, mhalf, mfull, isclp)
      call muldif(io, ixf, ix4, isgo, isgxf, isgx4, iko, ikxf, ikx4,    &
     &              nkmax, mhalf)
      call decmp2(io, isgo, iko, iythw, iyth2, mhalf, mfull, isclp)
      call muldif(io, iyf, iy4, isgo, isgyf, isgy4, iko, ikyf, iky4,    &
     &              nkmax, mhalf)
      call decmp2(io, isgo, iko, izthw, izth2, mhalf, mfull, isclp)
      call muldif(io, izf, iz4, isgo, isgzf, isgz4, iko, ikzf, ikz4,    &
     &              nkmax, mhalf)
!c
      call mulmul(iy5, iz3, io, isgy5, isgz3, isgo, iky5, ikz3, iko,    &
     &              nkmax, mhalf)
      call mulmul(io, ix4, iv, isgo, isgx4, isgv, iko, ikx4, ikv,       &
     &              nkmax, mhalf)
!c
      call mulmul(iz5, ix3, io, isgz5, isgx3, isgo, ikz5, ikx3, iko,    &
     &              nkmax, mhalf)
      call mulmul(io, iy4, iu, isgo, isgy4, isgu, iko, iky4, iku,       &
     &              nkmax, mhalf)
      isgu =-isgu
      call muldif(iv, iu, io, isgv, isgu, isgo, ikv, iku, iko,          &
     &              nkmax, mhalf)
!c
      call mulmul(ix5, iy3, iv, isgx5, isgy3, isgv, ikx5, iky3, ikv,    &
     &              nkmax, mhalf)
      call mulmul(iv, iz4, iu, isgv, isgz4, isgu, ikv, ikz4, iku,       &
     &              nkmax, mhalf)
      isgu =-isgu
      call muldif(io, iu, iv, isgo, isgu, isgv, iko, iku, ikv,          &
     &              nkmax, mhalf)
!c
      call mulmul(iz5, iy3, io, isgz5, isgy3, isgo, ikz5, iky3, iko,    &
     &              nkmax, mhalf)
      call mulmul(io, ix4, iu, isgo, isgx4, isgu, iko, ikx4, iku,       &
     &              nkmax, mhalf)
      call muldif(iv, iu, io, isgv, isgu, isgo, ikv, iku, iko,          &
     &              nkmax, mhalf)
!c
      call mulmul(ix5, iz3, iv, isgx5, isgz3, isgv, ikx5, ikz3, ikv,    &
     &              nkmax, mhalf)
      call mulmul(iv, iy4, iu, isgv, isgy4, isgu, ikv, iky4, iku,       &
     &              nkmax, mhalf)
      call muldif(io, iu, iv, isgo, isgu, isgv, iko, iku, ikv,          &
     &              nkmax, mhalf)
!c
      call mulmul(iy5, ix3, io, isgy5, isgx3, isgo, iky5, ikx3, iko,    &
     &              nkmax, mhalf)
      call mulmul(io, iz4, iu, isgo, isgz4, isgu, iko, ikz4, iku,       &
     &              nkmax, mhalf)
      call muldif(iv, iu, io, isgv, isgu, isgo, ikv, iku, iko,          &
     &              nkmax, mhalf)
!c
      call mulmul(iy6, iz2, iv, isgy6, isgz2, isgv, iky6, ikz2, ikv,    &
     &              nkmax, mhalf)
      call mulmul(iv, ix4, iu, isgv, isgx4, isgu, ikv, ikx4, iku,       &
     &              nkmax, mhalf)
      call muldif(io, iu, iv, isgo, isgu, isgv, iko, iku, ikv,          &
     &              nkmax, mhalf)
!c
      call mulmul(iz6, ix2, io, isgz6, isgx2, isgo, ikz6, ikx2, iko,    &
     &              nkmax, mhalf)
      call mulmul(io, iy4, iu, isgo, isgy4, isgu, iko, iky4, iku,       &
     &              nkmax, mhalf)
      call muldif(iv, iu, io, isgv, isgu, isgo, ikv, iku, iko,          &
     &              nkmax, mhalf)
!c
      call mulmul(ix6, iy2, iv, isgx6, isgy2, isgv, ikx6, iky2, ikv,    &
     &              nkmax, mhalf)
      call mulmul(iv, iz4, iu, isgv, isgz4, isgu, ikv, ikz4, iku,       &
     &              nkmax, mhalf)
      call muldif(io, iu, iv, isgo, isgu, isgv, iko, iku, ikv,          &
     &              nkmax, mhalf)
!c
      call mulmul(iz6, iy2, io, isgz6, isgy2, isgo, ikz6, iky2, iko,    &
     &              nkmax, mhalf)
      call mulmul(io, ix4, iu, isgo, isgx4, isgu, iko, ikx4, iku,       &
     &              nkmax, mhalf)
      isgu =-isgu
      call muldif(iv, iu, io, isgv, isgu, isgo, ikv, iku, iko,          &
     &              nkmax, mhalf)
!c
      call mulmul(ix6, iz2, iv, isgx6, isgz2, isgv, ikx6, ikz2, ikv,    &
     &              nkmax, mhalf)
      call mulmul(iv, iy4, iu, isgv, isgy4, isgu, ikv, iky4, iku,       &
     &              nkmax, mhalf)
      isgu =-isgu
      call muldif(io, iu, iv, isgo, isgu, isgv, iko, iku, ikv,          &
     &              nkmax, mhalf)
!c
      call mulmul(iy6, ix2, io, isgy6, isgx2, isgo, iky6, ikx2, iko,    &
     &              nkmax, mhalf)
      call mulmul(io, iz4, iu, isgo, isgz4, isgu, iko, ikz4, iku,       &
     &              nkmax, mhalf)
      isgu =-isgu
      call muldif(iv, iu, io, isgv, isgu, isgo, ikv, iku, iko,          &
     &              nkmax, mhalf)
!c
      ipout = isgo
!c
      return
      end subroutine ipsig6

!=======================================================================

      subroutine ipsign(x, y, z, x2, y2, z2, ifir, isec, ithi,          &
     &                  ifou, mhalf, mfull, isclp, ipout)
!c
      integer(8) :: x(:), y(:), z(:), x2(:), y2(:), z2(:)
      integer*8 ifir, isec, ithi, ifou
      integer(8) :: isclp(:), mhalf, mfull, nkmax, ipout
      parameter (nkmax = 30)
      integer*8 io(nkmax), iu(nkmax), iv(nkmax), iw(nkmax)
      integer*8 ix2(nkmax), iy2(nkmax), iz2(nkmax)
      integer*8 ix3(nkmax), iy3(nkmax), iz3(nkmax)
      integer*8 ix4(nkmax), iy4(nkmax), iz4(nkmax)
      integer*8 ixf(nkmax), iyf(nkmax), izf(nkmax)
      integer*8 ixfiw, iyfiw, izfiw, ixsew, iysew, izsew
      integer*8 ixthw, iythw, izthw, ixfow, iyfow, izfow
      integer*8 ixfi2, iyfi2, izfi2, ixse2, iyse2, izse2
      integer*8 ixth2, iyth2, izth2, ixfo2, iyfo2, izfo2
      integer*8 isgxf, isgyf, isgzf, ikxf, ikyf, ikzf
      integer*8 isgx2, isgy2, isgz2, ikx2, iky2, ikz2
      integer*8 isgx3, isgy3, isgz3, ikx3, iky3, ikz3
      integer*8 isgx4, isgy4, isgz4, ikx4, iky4, ikz4
      integer*8 isgo, isgu, isgv, isgw, iko, iku, ikv, ikw
!c
      ixfiw = x(ifir)
      iyfiw = y(ifir)
      izfiw = z(ifir)
      ixsew = x(isec)
      iysew = y(isec)
      izsew = z(isec)
      ixthw = x(ithi)
      iythw = y(ithi)
      izthw = z(ithi)
      ixfow = x(ifou)
      iyfow = y(ifou)
      izfow = z(ifou)
!c
      ixfi2 = x2(ifir)
      iyfi2 = y2(ifir)
      izfi2 = z2(ifir)
      ixse2 = x2(isec)
      iyse2 = y2(isec)
      izse2 = z2(isec)
      ixth2 = x2(ithi)
      iyth2 = y2(ithi)
      izth2 = z2(ithi)
      ixfo2 = x2(ifou)
      iyfo2 = y2(ifou)
      izfo2 = z2(ifou)
!c
      call decmp2(ixf, isgxf, ikxf, ixfiw, ixfi2, mhalf, mfull, isclp)
      call decmp2(iyf, isgyf, ikyf, iyfiw, iyfi2, mhalf, mfull, isclp)
      call decmp2(izf, isgzf, ikzf, izfiw, izfi2, mhalf, mfull, isclp)
!c
      call decmp2(io, isgo, iko, ixsew, ixse2, mhalf, mfull, isclp)
      call muldif(io, ixf, ix2, isgo, isgxf, isgx2, iko, ikxf, ikx2,    &
     &              nkmax, mhalf)
      call decmp2(io, isgo, iko, iysew, iyse2, mhalf, mfull, isclp)
      call muldif(io, iyf, iy2, isgo, isgyf, isgy2, iko, ikyf, iky2,    &
     &              nkmax, mhalf)
      call decmp2(io, isgo, iko, izsew, izse2, mhalf, mfull, isclp)
      call muldif(io, izf, iz2, isgo, isgzf, isgz2, iko, ikzf, ikz2,    &
     &              nkmax, mhalf)
      call decmp2(io, isgo, iko, ixthw, ixth2, mhalf, mfull, isclp)
      call muldif(io, ixf, ix3, isgo, isgxf, isgx3, iko, ikxf, ikx3,    &
     &              nkmax, mhalf)
      call decmp2(io, isgo, iko, iythw, iyth2, mhalf, mfull, isclp)
      call muldif(io, iyf, iy3, isgo, isgyf, isgy3, iko, ikyf, iky3,    &
     &              nkmax, mhalf)
      call decmp2(io, isgo, iko, izthw, izth2, mhalf, mfull, isclp)
      call muldif(io, izf, iz3, isgo, isgzf, isgz3, iko, ikzf, ikz3,    &
     &              nkmax, mhalf)
      call decmp2(io, isgo, iko, ixfow, ixfo2, mhalf, mfull, isclp)
      call muldif(io, ixf, ix4, isgo, isgxf, isgx4, iko, ikxf, ikx4,    &
     &              nkmax, mhalf)
      call decmp2(io, isgo, iko, iyfow, iyfo2, mhalf, mfull, isclp)
      call muldif(io, iyf, iy4, isgo, isgyf, isgy4, iko, ikyf, iky4,    &
     &              nkmax, mhalf)
      call decmp2(io, isgo, iko, izfow, izfo2, mhalf, mfull, isclp)
      call muldif(io, izf, iz4, isgo, isgzf, isgz4, iko, ikzf, ikz4,    &
     &              nkmax, mhalf)
!c
      call mulmul(iy2, iz3, iv, isgy2, isgz3, isgv, iky2, ikz3, ikv,    &
     &              nkmax, mhalf)
      call mulmul(iz2, iy3, iu, isgz2, isgy3, isgu, ikz2, iky3, iku,    &
     &              nkmax, mhalf)
      call muldif(iv, iu, iw, isgv, isgu, isgw, ikv, iku, ikw,          &
     &              nkmax, mhalf)
      call mulmul(iw, ix4, io, isgw, isgx4, isgo, ikw, ikx4, iko,       &
     &              nkmax, mhalf)
!c
      call mulmul(iz2, ix3, iv, isgz2, isgx3, isgv, ikz2, ikx3, ikv,    &
     &              nkmax, mhalf)
      call mulmul(ix2, iz3, iu, isgx2, isgz3, isgu, ikx2, ikz3, iku,    &
     &              nkmax, mhalf)
      call muldif(iv, iu, iw, isgv, isgu, isgw, ikv, iku, ikw,          &
     &              nkmax, mhalf)
      call mulmul(iw, iy4, iu, isgw, isgy4, isgu, ikw, iky4, iku,       &
     &              nkmax, mhalf)
      isgu =-isgu
      call muldif(io, iu, iw, isgo, isgu, isgw, iko, iku, ikw,          &
     &              nkmax, mhalf)
!c
      call mulmul(ix2, iy3, iv, isgx2, isgy3, isgv, ikx2, iky3, ikv,    &
     &              nkmax, mhalf)
      call mulmul(iy2, ix3, iu, isgy2, isgx3, isgu, iky2, ikx3, iku,    &
     &              nkmax, mhalf)
      call muldif(iv, iu, io, isgv, isgu, isgo, ikv, iku, iko,          &
     &              nkmax, mhalf)
      call mulmul(io, iz4, iu, isgo, isgz4, isgu, iko, ikz4, iku,       &
     &              nkmax, mhalf)
      isgu =-isgu
      call muldif(iw, iu, io, isgw, isgu, isgo, ikw, iku, iko,          &
     &              nkmax, mhalf)
!c
      ipout = isgo
!c
      return
      end subroutine ipsign

!=======================================================================

      subroutine iqsig1(x, y, z, w, x2, y2, z2, w2, ifir, isec, ifif,   &
     &                 mhalf, mfull, isclp, isclw, isclr, delaun, ipout)
!c
      integer(8) :: x(:), y(:), z(:), w(:), x2(:), y2(:), z2(:), w2(:)
      integer*8 ifir, isec, ifif, mhalf, mfull, nkmax, ipout
      integer(8) :: isclp(:), isclw(:), isclr(:)
      parameter (nkmax = 30)
      integer*8 io(nkmax), iu(nkmax), iv(nkmax)
      integer*8 iq2(nkmax), iq5(nkmax)
      integer*8 ix2(nkmax), iy2(nkmax), iz2(nkmax)
      integer*8 ix5(nkmax), iy5(nkmax), iz5(nkmax)
      integer*8 ixf(nkmax), iyf(nkmax), izf(nkmax)
      integer*8 ixf2(nkmax), iyf2(nkmax), izf2(nkmax)
      integer*8 iwf(nkmax), iw2(nkmax), iw5(nkmax)
      logical delaun
      integer*8 iwfuw, iwsew, iwfiw
      integer*8 ixfiw, iyfiw, izfiw, ixsew, iysew, izsew
      integer*8 ixfuw, iyfuw, izfuw
      integer*8 iwfu2, iwse2, iwfi2
      integer*8 ixfi2, iyfi2, izfi2, ixse2, iyse2, izse2
      integer*8 ixfu2, iyfu2, izfu2
      integer*8 isgw2, isgw5, ikw2, ikw5
      integer*8 isgq2, isgq5, ikq2, ikq5
      integer*8 isgxf, isgyf, isgzf, ikxf, ikyf, ikzf
      integer*8 isgxf2, isgyf2, isgzf2, ikxf2, ikyf2, ikzf2
      integer*8 isgx2, isgy2, isgz2, ikx2, iky2, ikz2
      integer*8 isgx5, isgy5, isgz5, ikx5, iky5, ikz5
      integer*8 isgo, isgu, isgv, iko, iku, ikv
      integer*8 isgwf, isgcl, ikwf, ikcl
!c
      if(delaun) then
         isgw2 = 0
         isgw5 = 0
      else
         iwfuw = w(ifir)
         iwsew = w(isec)
         iwfiw = w(ifif)
!c
         iwfu2 = w2(ifir)
         iwse2 = w2(isec)
         iwfi2 = w2(ifif)
!c
         call decmp2(iwf,isgwf,ikwf, iwfuw,iwfu2, mhalf, mfull, isclw)
         isgcl = 1
         ikcl = 2
         call decmp2(io, isgo, iko, iwsew, iwse2, mhalf, mfull, isclw)
         call muldif(io, iwf, iu, isgo, isgwf, isgu, iko, ikwf, iku,    &
     &                 nkmax, mhalf)
         call mulmul(iu, isclr, iw2, isgu, isgcl, isgw2, iku, ikcl,     &
     &                 ikw2, nkmax, mhalf)
         call decmp2(io, isgo, iko, iwfiw, iwfi2, mhalf, mfull, isclw)
         call muldif(io, iwf, iu, isgo, isgwf, isgu, iko, ikwf, iku,    &
     &                 nkmax, mhalf)
         call mulmul(iu, isclr, iw5, isgu, isgcl, isgw5, iku, ikcl,     &
     &                 ikw5, nkmax, mhalf)
      endif
!c
      ixfuw = x(ifir)
      iyfuw = y(ifir)
      izfuw = z(ifir)
      ixsew = x(isec)
      iysew = y(isec)
      izsew = z(isec)
      ixfiw = x(ifif)
      iyfiw = y(ifif)
      izfiw = z(ifif)
!c
      ixfu2 = x2(ifir)
      iyfu2 = y2(ifir)
      izfu2 = z2(ifir)
      ixse2 = x2(isec)
      iyse2 = y2(isec)
      izse2 = z2(isec)
      ixfi2 = x2(ifif)
      iyfi2 = y2(ifif)
      izfi2 = z2(ifif)
!c
      call decmp2(ixf, isgxf, ikxf, ixfuw, ixfu2, mhalf, mfull, isclp)
      call decmp2(iyf, isgyf, ikyf, iyfuw, iyfu2, mhalf, mfull, isclp)
      call decmp2(izf, isgzf, ikzf, izfuw, izfu2, mhalf, mfull, isclp)
      call mulmul(ixf, ixf, ixf2, isgxf, isgxf, isgxf2, ikxf, ikxf,     &
     &              ikxf2, nkmax, mhalf)
      call mulmul(iyf, iyf, iyf2, isgyf, isgyf, isgyf2, ikyf, ikyf,     &
     &              ikyf2, nkmax, mhalf)
      call mulmul(izf, izf, izf2, isgzf, isgzf, isgzf2, ikzf, ikzf,     &
     &              ikzf2, nkmax, mhalf)
      if(isgxf2.lt.0 .or. isgyf2.lt.0 .or. isgzf2.lt.0) stop 5205
!c
      call frterm(ixsew, iysew, izsew, ixf, iyf, izf, isgxf, isgyf,     &
     &            isgzf, ikxf, ikyf, ikzf, ixf2, iyf2, izf2,            &
     &            isgxf2, isgyf2, isgzf2, ikxf2,                        &
     &            ikyf2, ikzf2, iw2, ix2, iy2, iz2, iq2, isgw2, isgx2,  &
     &            isgy2, isgz2, isgq2, ikw2, ikx2, iky2, ikz2, ikq2,    &
     &            mhalf, mfull, ixse2, iyse2, izse2, isclp)
!c
      call frterm(ixfiw, iyfiw, izfiw, ixf, iyf, izf, isgxf, isgyf,     &
     &            isgzf, ikxf, ikyf, ikzf, ixf2, iyf2, izf2,            &
     &            isgxf2, isgyf2, isgzf2, ikxf2,                        &
     &            ikyf2, ikzf2, iw5, ix5, iy5, iz5, iq5, isgw5, isgx5,  &
     &            isgy5, isgz5, isgq5, ikw5, ikx5, iky5, ikz5, ikq5,    &
     &            mhalf, mfull, ixfi2, iyfi2, izfi2, isclp)
!c
      call detrm1(iq5, ix2, iy2, iz2, ix2, iy2, iz2, isgq5, isgx2,      &
     &            isgy2, isgz2, isgx2, isgy2, isgz2, ikq5, ikx2, iky2,  &
     &            ikz2, ikx2, iky2, ikz2, iu, isgu, iku, mhalf)
!c
      call detrm1(iq2, ix2, iy2, iz2, ix5, iy5, iz5, isgq2, isgx2,      &
     &            isgy2, isgz2, isgx5, isgy5, isgz5, ikq2, ikx2, iky2,  &
     &            ikz2, ikx5, iky5, ikz5, iv, isgv, ikv, mhalf)
!c
      call muldif(iu, iv, io, isgu, isgv, isgo, iku, ikv, iko,          &
     &              nkmax, mhalf)
!c
      ipout = isgo
!c
      return
      end subroutine iqsig1

!=======================================================================

      subroutine iqsig2(x, y, z, w, x2, y2, z2, w2, ifir, isec, ithi,   &
     &           ifif, mhalf, mfull, isclp, isclw, isclr, delaun, ipout)
!c
      integer(8) :: x(:), y(:), z(:), w(:), x2(:), y2(:), z2(:), w2(:)
      integer*8 ifir, isec, ithi, ifif, mhalf, mfull, nkmax, ipout
      integer(8) :: isclp(:), isclw(:), isclr(:)
      parameter (nkmax = 30)
      integer*8 io(nkmax), iu(nkmax), iv(nkmax), ip(nkmax)
      integer*8 iq2(nkmax), iq3(nkmax), iq5(nkmax)
      integer*8 ix2(nkmax), iy2(nkmax), iz2(nkmax)
      integer*8 ix3(nkmax), iy3(nkmax), iz3(nkmax)
      integer*8 ix5(nkmax), iy5(nkmax), iz5(nkmax)
      integer*8 ixf(nkmax), iyf(nkmax), izf(nkmax)
      integer*8 ixf2(nkmax), iyf2(nkmax), izf2(nkmax)
      integer*8 iwf(nkmax), iw2(nkmax), iw3(nkmax), iw5(nkmax)
      logical delaun
      integer*8 iwfuw, iwsew, iwthw, iwfiw
      integer*8 ixthw, iythw, izthw, ixfiw, iyfiw, izfiw
      integer*8 ixfuw, iyfuw, izfuw, ixsew, iysew, izsew
      integer*8 iwfu2, iwse2, iwth2, iwfi2
      integer*8 ixth2, iyth2, izth2, ixfi2, iyfi2, izfi2
      integer*8 ixfu2, iyfu2, izfu2, ixse2, iyse2, izse2
      integer*8 isgw2, isgw3, isgw5, ikw2, ikw3, ikw5
      integer*8 isgq2, isgq3, isgq5, ikq2, ikq3, ikq5
      integer*8 isgxf, isgyf, isgzf, ikxf, ikyf, ikzf
      integer*8 isgxf2, isgyf2, isgzf2, ikxf2, ikyf2, ikzf2
      integer*8 isgx2, isgy2, isgz2, ikx2, iky2, ikz2
      integer*8 isgx3, isgy3, isgz3, ikx3, iky3, ikz3
      integer*8 isgx5, isgy5, isgz5, ikx5, iky5, ikz5
      integer*8 isgo, isgu, isgv, isgp, iko, iku, ikv, ikp
      integer*8 isgwf, isgcl, ikwf, ikcl
!c
      if(delaun) then
         isgw2 = 0
         isgw3 = 0
         isgw5 = 0
      else
         iwfuw = w(ifir)
         iwsew = w(isec)
         iwthw = w(ithi)
         iwfiw = w(ifif)
!c
         iwfu2 = w2(ifir)
         iwse2 = w2(isec)
         iwth2 = w2(ithi)
         iwfi2 = w2(ifif)
!c
         call decmp2(iwf,isgwf,ikwf, iwfuw,iwfu2, mhalf, mfull, isclw)
         isgcl = 1
         ikcl = 2
         call decmp2(io, isgo, iko, iwsew, iwse2, mhalf, mfull, isclw)
         call muldif(io, iwf, iu, isgo, isgwf, isgu, iko, ikwf, iku,    &
     &                 nkmax, mhalf)
         call mulmul(iu, isclr, iw2, isgu, isgcl, isgw2, iku, ikcl,     &
     &                 ikw2, nkmax, mhalf)
         call decmp2(io, isgo, iko, iwthw, iwth2, mhalf, mfull, isclw)
         call muldif(io, iwf, iu, isgo, isgwf, isgu, iko, ikwf, iku,    &
     &                 nkmax, mhalf)
         call mulmul(iu, isclr, iw3, isgu, isgcl, isgw3, iku, ikcl,     &
     &                 ikw3, nkmax, mhalf)
         call decmp2(io, isgo, iko, iwfiw, iwfi2, mhalf, mfull, isclw)
         call muldif(io, iwf, iu, isgo, isgwf, isgu, iko, ikwf, iku,    &
     &                 nkmax, mhalf)
         call mulmul(iu, isclr, iw5, isgu, isgcl, isgw5, iku, ikcl,     &
     &                 ikw5, nkmax, mhalf)
      endif
!c
      ixfuw = x(ifir)
      iyfuw = y(ifir)
      izfuw = z(ifir)
      ixsew = x(isec)
      iysew = y(isec)
      izsew = z(isec)
      ixthw = x(ithi)
      iythw = y(ithi)
      izthw = z(ithi)
      ixfiw = x(ifif)
      iyfiw = y(ifif)
      izfiw = z(ifif)
!c
      ixfu2 = x2(ifir)
      iyfu2 = y2(ifir)
      izfu2 = z2(ifir)
      ixse2 = x2(isec)
      iyse2 = y2(isec)
      izse2 = z2(isec)
      ixth2 = x2(ithi)
      iyth2 = y2(ithi)
      izth2 = z2(ithi)
      ixfi2 = x2(ifif)
      iyfi2 = y2(ifif)
      izfi2 = z2(ifif)
!c
      call decmp2(ixf, isgxf, ikxf, ixfuw, ixfu2, mhalf, mfull, isclp)
      call decmp2(iyf, isgyf, ikyf, iyfuw, iyfu2, mhalf, mfull, isclp)
      call decmp2(izf, isgzf, ikzf, izfuw, izfu2, mhalf, mfull, isclp)
      call mulmul(ixf, ixf, ixf2, isgxf, isgxf, isgxf2, ikxf, ikxf,     &
     &              ikxf2, nkmax, mhalf)
      call mulmul(iyf, iyf, iyf2, isgyf, isgyf, isgyf2, ikyf, ikyf,     &
     &              ikyf2, nkmax, mhalf)
      call mulmul(izf, izf, izf2, isgzf, isgzf, isgzf2, ikzf, ikzf,     &
     &              ikzf2, nkmax, mhalf)
      if(isgxf2.lt.0 .or. isgyf2.lt.0 .or. isgzf2.lt.0) stop 5305
!c
      call frterm(ixsew, iysew, izsew, ixf, iyf, izf, isgxf, isgyf,     &
     &            isgzf, ikxf, ikyf, ikzf, ixf2, iyf2, izf2,            &
     &            isgxf2, isgyf2, isgzf2, ikxf2,                        &
     &            ikyf2, ikzf2, iw2, ix2, iy2, iz2, iq2, isgw2, isgx2,  &
     &            isgy2, isgz2, isgq2, ikw2, ikx2, iky2, ikz2, ikq2,    &
     &            mhalf, mfull, ixse2, iyse2, izse2, isclp)
!c
      call frterm(ixthw, iythw, izthw, ixf, iyf, izf, isgxf, isgyf,     &
     &            isgzf, ikxf, ikyf, ikzf, ixf2, iyf2, izf2,            &
     &            isgxf2, isgyf2, isgzf2, ikxf2,                        &
     &            ikyf2, ikzf2, iw3, ix3, iy3, iz3, iq3, isgw3, isgx3,  &
     &            isgy3, isgz3, isgq3, ikw3, ikx3, iky3, ikz3, ikq3,    &
     &            mhalf, mfull, ixth2, iyth2, izth2, isclp)
!c
      call frterm(ixfiw, iyfiw, izfiw, ixf, iyf, izf, isgxf, isgyf,     &
     &            isgzf, ikxf, ikyf, ikzf, ixf2, iyf2, izf2,            &
     &            isgxf2, isgyf2, isgzf2, ikxf2,                        &
     &            ikyf2, ikzf2, iw5, ix5, iy5, iz5, iq5, isgw5, isgx5,  &
     &            isgy5, isgz5, isgq5, ikw5, ikx5, iky5, ikz5, ikq5,    &
     &            mhalf, mfull, ixfi2, iyfi2, izfi2, isclp)
!c
      call detrm0(iq5, iy2, iz2, iy3, iz3, isgq5, isgy2, isgz2,         &
     &            isgy3, isgz3, ikq5, iky2, ikz2, iky3, ikz3,           &
     &            iv, isgv, ikv, mhalf)
!c
      call detrm0(iq5, iz2, ix2, iz3, ix3, isgq5, isgz2, isgx2,         &
     &            isgz3, isgx3, ikq5, ikz2, ikx2, ikz3, ikx3,           &
     &            iu, isgu, iku, mhalf)
!c
      call detrm0(iq5, ix2, iy2, ix3, iy3, isgq5, isgx2, isgy2,         &
     &            isgx3, isgy3, ikq5, ikx2, iky2, ikx3, iky3,           &
     &            ip, isgp, ikp, mhalf)
!c
      call detrm3(iv, ix2, ix3, isgv, isgx2, isgx3,                     &
     &            iu, iy2, iy3, isgu, isgy2, isgy3,                     &
     &            ip, iz2, iz3, isgp, isgz2, isgz3,                     &
     &            ikv, iku, ikp, ikx2, iky2, ikz2,                      &
     &            ikx3, iky3, ikz3, io, isgo, iko, mhalf)
!c
      call detrm2(iq2, ix2, iy2, iz2, isgq2, isgx2, isgy2, isgz2,       &
     &            iq3, ix3, iy3, iz3, isgq3, isgx3, isgy3, isgz3,       &
     &            ikq2, ikx2, iky2, ikz2, ikq3, ikx3, iky3, ikz3,       &
     &            iu, isgu, iku, mhalf)
      call mulmul(iu, ix5, ip, isgu, isgx5, isgp, iku, ikx5, ikp,       &
     &              nkmax, mhalf)
      call muldif(io, ip, iv, isgo, isgp, isgv, iko, ikp, ikv,          &
     &              nkmax, mhalf)
!c
      call detrm2(iq2, iy2, iz2, ix2, isgq2, isgy2, isgz2, isgx2,       &
     &            iq3, iy3, iz3, ix3, isgq3, isgy3, isgz3, isgx3,       &
     &            ikq2, iky2, ikz2, ikx2, ikq3, iky3, ikz3, ikx3,       &
     &            iu, isgu, iku, mhalf)
      call mulmul(iu, iy5, io, isgu, isgy5, isgo, iku, iky5, iko,       &
     &              nkmax, mhalf)
      call muldif(iv, io, ip, isgv, isgo, isgp, ikv, iko, ikp,          &
     &              nkmax, mhalf)
!c
      call detrm2(iq2, iz2, ix2, iy2, isgq2, isgz2, isgx2, isgy2,       &
     &            iq3, iz3, ix3, iy3, isgq3, isgz3, isgx3, isgy3,       &
     &            ikq2, ikz2, ikx2, iky2, ikq3, ikz3, ikx3, iky3,       &
     &            iu, isgu, iku, mhalf)
      call mulmul(iu, iz5, iv, isgu, isgz5, isgv, iku, ikz5, ikv,       &
     &              nkmax, mhalf)
      call muldif(ip, iv, io, isgp, isgv, isgo, ikp, ikv, iko,          &
     &              nkmax, mhalf)
!c
      ipout = isgo
!c
      return
      end subroutine iqsig2

!=======================================================================

      subroutine iqsign(x, y, z, w, x2, y2, z2, w2, ifir, isec,         &
     &                  ithi, ifou, ifif, mhalf, mfull, isclp,          &
     &                  isclw, isclr, delaun, ipout)
!c
      integer(8) :: x(:), y(:), z(:), w(:), x2(:), y2(:), z2(:), w2(:)
      integer*8 ifir, isec, ithi, ifou, ifif, mhalf, mfull, nkmax, ipout
      integer(8) :: isclp(:), isclw(:), isclr(:)
      parameter (nkmax = 30)
      integer*8 io(nkmax), iu(nkmax), iv(nkmax), ip(nkmax)
      integer*8 iq2(nkmax), iq3(nkmax), iq4(nkmax), iq5(nkmax)
      integer*8 ix2(nkmax), iy2(nkmax), iz2(nkmax)
      integer*8 ix3(nkmax), iy3(nkmax), iz3(nkmax)
      integer*8 ix4(nkmax), iy4(nkmax), iz4(nkmax)
      integer*8 ix5(nkmax), iy5(nkmax), iz5(nkmax)
      integer*8 ixf(nkmax), iyf(nkmax), izf(nkmax)
      integer*8 ixf2(nkmax), iyf2(nkmax), izf2(nkmax)
      integer*8 iwf(nkmax), iw2(nkmax), iw3(nkmax), iw4(nkmax),         &
     &          iw5(nkmax)
      logical delaun
      integer*8 iwfuw, iwsew, iwthw, iwfow, iwfiw
      integer*8 ixthw, iythw, izthw, ixfow, iyfow, izfow
      integer*8 ixfiw, iyfiw, izfiw, ixsew, iysew, izsew
      integer*8 ixfuw, iyfuw, izfuw
      integer*8 iwfu2, iwse2, iwth2, iwfo2, iwfi2
      integer*8 ixth2, iyth2, izth2, ixfo2, iyfo2, izfo2
      integer*8 ixfi2, iyfi2, izfi2, ixse2, iyse2, izse2
      integer*8 ixfu2, iyfu2, izfu2
      integer*8 isgw2, isgw3, isgw4, isgw5, ikw2, ikw3, ikw4, ikw5
      integer*8 isgq2, isgq3, isgq4, isgq5, ikq2, ikq3, ikq4, ikq5
      integer*8 isgxf, isgyf, isgzf, ikxf, ikyf, ikzf
      integer*8 isgxf2, isgyf2, isgzf2, ikxf2, ikyf2, ikzf2
      integer*8 isgx2, isgy2, isgz2, ikx2, iky2, ikz2
      integer*8 isgx3, isgy3, isgz3, ikx3, iky3, ikz3
      integer*8 isgx4, isgy4, isgz4, ikx4, iky4, ikz4
      integer*8 isgx5, isgy5, isgz5, ikx5, iky5, ikz5
      integer*8 isgo, isgu, isgv, isgp, iko, iku, ikv, ikp
      integer*8 isgwf, isgcl, ikwf, ikcl
!c
      if(delaun) then
         isgw2 = 0
         isgw3 = 0
         isgw4 = 0
         isgw5 = 0
      else
         iwfuw = w(ifir)
         iwsew = w(isec)
         iwthw = w(ithi)
         iwfow = w(ifou)
         iwfiw = w(ifif)
!c
         iwfu2 = w2(ifir)
         iwse2 = w2(isec)
         iwth2 = w2(ithi)
         iwfo2 = w2(ifou)
         iwfi2 = w2(ifif)
!c
         call decmp2(iwf,isgwf,ikwf, iwfuw,iwfu2, mhalf, mfull, isclw)
         isgcl = 1
         ikcl = 2
         call decmp2(io, isgo, iko, iwsew, iwse2, mhalf, mfull, isclw)
         call muldif(io, iwf, iu, isgo, isgwf, isgu, iko, ikwf, iku,    &
     &                 nkmax, mhalf)
         call mulmul(iu, isclr, iw2, isgu, isgcl, isgw2, iku, ikcl,     &
     &                 ikw2, nkmax, mhalf)
         call decmp2(io, isgo, iko, iwthw, iwth2, mhalf, mfull, isclw)
         call muldif(io, iwf, iu, isgo, isgwf, isgu, iko, ikwf, iku,    &
     &                 nkmax, mhalf)
         call mulmul(iu, isclr, iw3, isgu, isgcl, isgw3, iku, ikcl,     &
     &                 ikw3, nkmax, mhalf)
         call decmp2(io, isgo, iko, iwfow, iwfo2, mhalf, mfull, isclw)
         call muldif(io, iwf, iu, isgo, isgwf, isgu, iko, ikwf, iku,    &
     &                 nkmax, mhalf)
         call mulmul(iu, isclr, iw4, isgu, isgcl, isgw4, iku, ikcl,     &
     &                 ikw4, nkmax, mhalf)
         call decmp2(io, isgo, iko, iwfiw, iwfi2, mhalf, mfull, isclw)
         call muldif(io, iwf, iu, isgo, isgwf, isgu, iko, ikwf, iku,    &
     &                 nkmax, mhalf)
         call mulmul(iu, isclr, iw5, isgu, isgcl, isgw5, iku, ikcl,     &
     &                 ikw5, nkmax, mhalf)
      endif
!c
      ixfuw = x(ifir)
      iyfuw = y(ifir)
      izfuw = z(ifir)
      ixsew = x(isec)
      iysew = y(isec)
      izsew = z(isec)
      ixthw = x(ithi)
      iythw = y(ithi)
      izthw = z(ithi)
      ixfow = x(ifou)
      iyfow = y(ifou)
      izfow = z(ifou)
      ixfiw = x(ifif)
      iyfiw = y(ifif)
      izfiw = z(ifif)
!c
      ixfu2 = x2(ifir)
      iyfu2 = y2(ifir)
      izfu2 = z2(ifir)
      ixse2 = x2(isec)
      iyse2 = y2(isec)
      izse2 = z2(isec)
      ixth2 = x2(ithi)
      iyth2 = y2(ithi)
      izth2 = z2(ithi)
      ixfo2 = x2(ifou)
      iyfo2 = y2(ifou)
      izfo2 = z2(ifou)
      ixfi2 = x2(ifif)
      iyfi2 = y2(ifif)
      izfi2 = z2(ifif)
!c
      call decmp2(ixf, isgxf, ikxf, ixfuw, ixfu2, mhalf, mfull, isclp)
      call decmp2(iyf, isgyf, ikyf, iyfuw, iyfu2, mhalf, mfull, isclp)
      call decmp2(izf, isgzf, ikzf, izfuw, izfu2, mhalf, mfull, isclp)
      call mulmul(ixf, ixf, ixf2, isgxf, isgxf, isgxf2, ikxf, ikxf,     &
     &              ikxf2, nkmax, mhalf)
      call mulmul(iyf, iyf, iyf2, isgyf, isgyf, isgyf2, ikyf, ikyf,     &
     &              ikyf2, nkmax, mhalf)
      call mulmul(izf, izf, izf2, isgzf, isgzf, isgzf2, ikzf, ikzf,     &
     &              ikzf2, nkmax, mhalf)
      if(isgxf2.lt.0 .or. isgyf2.lt.0 .or. isgzf2.lt.0) stop 5105
!c
      call frterm(ixsew, iysew, izsew, ixf, iyf, izf, isgxf, isgyf,     &
     &            isgzf, ikxf, ikyf, ikzf, ixf2, iyf2, izf2,            &
     &            isgxf2, isgyf2, isgzf2, ikxf2,                        &
     &            ikyf2, ikzf2, iw2, ix2, iy2, iz2, iq2, isgw2, isgx2,  &
     &            isgy2, isgz2, isgq2, ikw2, ikx2, iky2, ikz2, ikq2,    &
     &            mhalf, mfull, ixse2, iyse2, izse2, isclp)
!c
      call frterm(ixthw, iythw, izthw, ixf, iyf, izf, isgxf, isgyf,     &
     &            isgzf, ikxf, ikyf, ikzf, ixf2, iyf2, izf2,            &
     &            isgxf2, isgyf2, isgzf2, ikxf2,                        &
     &            ikyf2, ikzf2, iw3, ix3, iy3, iz3, iq3, isgw3, isgx3,  &
     &            isgy3, isgz3, isgq3, ikw3, ikx3, iky3, ikz3, ikq3,    &
     &            mhalf, mfull, ixth2, iyth2, izth2, isclp)
!c
      call frterm(ixfow, iyfow, izfow, ixf, iyf, izf, isgxf, isgyf,     &
     &            isgzf, ikxf, ikyf, ikzf, ixf2, iyf2, izf2,            &
     &            isgxf2, isgyf2, isgzf2, ikxf2,                        &
     &            ikyf2, ikzf2, iw4, ix4, iy4, iz4, iq4, isgw4, isgx4,  &
     &            isgy4, isgz4, isgq4, ikw4, ikx4, iky4, ikz4, ikq4,    &
     &            mhalf, mfull, ixfo2, iyfo2, izfo2, isclp)
!c
      call frterm(ixfiw, iyfiw, izfiw, ixf, iyf, izf, isgxf, isgyf,     &
     &            isgzf, ikxf, ikyf, ikzf, ixf2, iyf2, izf2,            &
     &            isgxf2, isgyf2, isgzf2, ikxf2,                        &
     &            ikyf2, ikzf2, iw5, ix5, iy5, iz5, iq5, isgw5, isgx5,  &
     &            isgy5, isgz5, isgq5, ikw5, ikx5, iky5, ikz5, ikq5,    &
     &            mhalf, mfull, ixfi2, iyfi2, izfi2, isclp)
!c
      call mulmul(iq5, ix2, iv, isgq5, isgx2, isgv, ikq5, ikx2, ikv,    &
     &              nkmax, mhalf)
      call mulmul(iq5, ix3, iu, isgq5, isgx3, isgu, ikq5, ikx3, iku,    &
     &              nkmax, mhalf)
      call mulmul(iq5, ix4, ip, isgq5, isgx4, isgp, ikq5, ikx4, ikp,    &
     &              nkmax, mhalf)
      call detrm3(iv, iy2, iz2, isgv, isgy2, isgz2,                     &
     &            iu, iy3, iz3, isgu, isgy3, isgz3,                     &
     &            ip, iy4, iz4, isgp, isgy4, isgz4,                     &
     &            ikv, iku, ikp, iky2, iky3, iky4,                      &
     &            ikz2, ikz3, ikz4, io, isgo, iko, mhalf)
!c
      call detrm3(iq2, iy2, iz2, isgq2, isgy2, isgz2,                   &
     &            iq3, iy3, iz3, isgq3, isgy3, isgz3,                   &
     &            iq4, iy4, iz4, isgq4, isgy4, isgz4,                   &
     &            ikq2, ikq3, ikq4, iky2, iky3, iky4,                   &
     &            ikz2, ikz3, ikz4, iu, isgu, iku, mhalf)
      call mulmul(iu, ix5, ip, isgu, isgx5, isgp, iku, ikx5, ikp,       &
     &              nkmax, mhalf)
      call muldif(io, ip, iv, isgo, isgp, isgv, iko, ikp, ikv,          &
     &              nkmax, mhalf)
!c
      call detrm3(iq2, iz2, ix2, isgq2, isgz2, isgx2,                   &
     &            iq3, iz3, ix3, isgq3, isgz3, isgx3,                   &
     &            iq4, iz4, ix4, isgq4, isgz4, isgx4,                   &
     &            ikq2, ikq3, ikq4, ikz2, ikz3, ikz4,                   &
     &            ikx2, ikx3, ikx4, iu, isgu, iku, mhalf)
      call mulmul(iu, iy5, io, isgu, isgy5, isgo, iku, iky5, iko,       &
     &              nkmax, mhalf)
      call muldif(iv, io, ip, isgv, isgo, isgp, ikv, iko, ikp,          &
     &              nkmax, mhalf)
!c
      call detrm3(iq2, ix2, iy2, isgq2, isgx2, isgy2,                   &
     &            iq3, ix3, iy3, isgq3, isgx3, isgy3,                   &
     &            iq4, ix4, iy4, isgq4, isgx4, isgy4,                   &
     &            ikq2, ikq3, ikq4, ikx2, ikx3, ikx4,                   &
     &            iky2, iky3, iky4, iu, isgu, iku, mhalf)
      call mulmul(iu, iz5, iv, isgu, isgz5, isgv, iku, ikz5, ikv,       &
     &              nkmax, mhalf)
      call muldif(ip, iv, io, isgp, isgv, isgo, ikp, ikv, iko,          &
     &              nkmax, mhalf)
!c
      ipout = isgo
!c
      return
      end subroutine iqsign

!=======================================================================

      subroutine irsign(xi, yi, zi, x, y, z, x2, y2, z2, site0, site1,  &
     &                  site2, site3, mhalf, mfull, isclp, epz, ipout)
!c
      real(8) :: xi(:), yi(:), zi(:)
      integer(8) :: x(:), y(:), z(:), x2(:), y2(:), z2(:)
      double precision epz, dist
      integer(8) :: isclp(:), mhalf, mfull, ipossi
      integer*8 site0, site1, site2, site3, ipout
!c
      call dstnce(xi, yi, zi, site1, site2, site3, epz, site0, dist,    &
     &            ipossi)
      if(ipossi.eq.0) then
         ipout = 1
         if(dist.lt.0.0d0) ipout = -1
      else
         call ipsign(x, y, z, x2, y2, z2, site1, site2, site3, site0,   &
     &               mhalf, mfull, isclp, ipout)
      endif
!c
      return
      end subroutine irsign

!=======================================================================

      subroutine iwsign(w, w2, ifir, isec, mhalf, mfull, isclw, ipout)
!c
      integer(8) :: w(:), w2(:)
      integer*8 ifir, isec, mhalf, mfull, nkmax, ipout
      parameter (nkmax = 30)
      integer(8) :: isclw(:), iu(nkmax), iw1(nkmax), iw2(nkmax)
      integer*8 iwfiw, iwsew, iwfi2, iwse2
      integer*8 isgw1, isgw2, ikw1, ikw2, isgu, iku
!c
      iwfiw = w(ifir)
      iwsew = w(isec)
!c
      iwfi2 = w2(ifir)
      iwse2 = w2(isec)
!c
      call decmp2(iw1,isgw1,ikw1, iwfiw,iwfi2, mhalf, mfull, isclw)
      call decmp2(iw2,isgw2,ikw2, iwsew,iwse2, mhalf, mfull, isclw)
      call muldif(iw1, iw2, iu, isgw1, isgw2, isgu, ikw1, ikw2, iku,    &
     &              nkmax, mhalf)
!c
      ipout = isgu
!c
      return
      end subroutine iwsign

!=======================================================================

      subroutine lkdown(icon, curr, xi, yi, zi, x, y, z, x2, y2, z2,    &
     &                  itype, k, side1, side2, xc, yc, zc, mhalf,      &
     &                  mfull, isclp, epz, ITCHK)
!c
      real(8) :: xi(:), yi(:), zi(:)
      integer(8) :: x(:), y(:), z(:), x2(:), y2(:), z2(:)
      integer*8 icon(:,:), xc(:), yc(:), zc(:)
      double precision epz
      integer*8 isclp(:), itype, k, mhalf, mfull, i, newcur, itchk
      integer*8 curr, side1, side2, a, b, c, d, flag
!c
!c     test children of current tetrahedron
!c
      itype = -1
      do 100 i = 1, 4
         newcur = icon(i,curr)
         if (newcur .le. 0) goto 100
         ITCHK = ITCHK+1
!c
         a = abs(icon(5,newcur))
         b = icon(6,newcur)
         c = icon(7,newcur)
         d = icon(8,newcur)
!c
         flag = icon(5,newcur)
         icon(5,newcur)=a
!c
         call vrtord(icon, newcur, a, b, c, d)
         if(flag.lt.0) icon(5,newcur)=-a
         if(a.le.8) stop 710
!c
         call gette2(a, b, c, d, xi, yi, zi, x, y, z, x2, y2, z2,       &
     &               itype, k, side1, side2, flag, xc, yc, zc,          &
     &               mhalf, mfull, isclp, epz)
         if (itype .eq. -1) goto 100
         curr = newcur
         goto 1000
  100 continue
!c
 1000 continue
      return
      end subroutine lkdown

!=======================================================================

      subroutine muldif(ia, ib, io, isga, isgb, isgo, ika, ikb, iko,    &
     &                    nkmax, mhalf)
!c
      integer*8 ia(:), ib(:), io(:)
      integer*8 isga, isgb, isgo, ika, ikb, iko, nkmax, mhalf
      integer*8 i, iko1, irel
!c
      if(isgb.eq.0)then
         if(isga.eq.0)then
            isgo=0
            iko = 2
            io(1) = 0
            io(2) = 0
            return
         endif
         isgo = isga
         iko = ika
         do 100 i=1,iko
            io(i) = ia(i)
  100    continue
      elseif(isga.eq.0)then
         isgo =-isgb
         iko = ikb
         do 200 i=1,iko
            io(i) = ib(i)
  200    continue
      else
         iko = ika
         if(ikb.lt.ika) then
            do 300 i=ikb+1,ika
               ib(i) = 0
  300       continue
         elseif(ika.lt.ikb) then
            iko = ikb
            do 400 i=ika+1,ikb
               ia(i) = 0
  400       continue
         endif
         if(isga*isgb.gt.0)then
            irel = 0
            do 500 i = iko, 1, -1
               if(ia(i).gt.ib(i))then
                  irel = 1
                  go to 600
               elseif(ia(i).lt.ib(i))then
                  irel = -1
                  go to 600
               endif
  500       continue
  600       continue
            if(irel.eq.0)then
               isgo = 0
               do 700 i=1,iko
                  io(i) = 0
  700          continue
            else
               isgo=isga*irel
               io(1) = (ia(1)-ib(1))*irel
               do 800 i=2,iko
                  if(io(i-1).lt.0) then
                     io(i) =-1
                     io(i-1) = io(i-1) + mhalf
                  else
                     io(i) = 0
                  endif
                  io(i) = io(i) + (ia(i)-ib(i))*irel
  800          continue
               if(io(iko).lt.0) stop 4810
            endif
         else
            isgo=isga
            io(1) = ia(1)+ib(1)
            do 900 i=2,iko
               if(io(i-1).ge.mhalf) then
                  io(i) = 1
                  io(i-1) = io(i-1) - mhalf
               else
                  io(i) = 0
               endif
               io(i) = io(i) + ia(i)+ib(i)
  900       continue
            if(io(iko).ge.mhalf) then
               iko = iko+1
               if(iko.gt.nkmax) stop 4820
               io(iko) = 1
               io(iko-1) = io(iko-1) - mhalf
            endif
         endif
      endif
!c
      if(iko .eq. 2) go to 1400
      iko1 = iko
      do 1300 i = iko1, 3, -1
         if(io(i) .ne. 0) go to 1400
         iko = iko - 1
 1300 continue
 1400 continue
!c
      return
      end subroutine muldif

!=======================================================================

      subroutine mulmul(ia, ib, io, isga, isgb, isgo, ika, ikb, iko,    &
     &                    nkmax, mhalf)
!c
      integer*8 ia(:), ib(:), io(:)
      integer*8 isga, isgb, isgo, ika, ikb, iko, nkmax, mhalf
      integer*8 i, ipt, ipr, iko1, k, j
!c
      if(isga.eq.0.or.isgb.eq.0)then
         isgo=0
         iko = 2
         io(1) = 0
         io(2) = 0
         return
      endif
!c
      iko = ika + ikb
      if(iko.gt.nkmax) stop 4710
!c
      if(isga.gt.0)then
         if(isgb.gt.0)then
            isgo = 1 ! 1*1=1
         else
            isgo =-1 ! 1*(-1)=-1
         endif
      else
         if(isgb.gt.0)then
            isgo =-1 ! (-1)*1=-1
         else
            isgo = 1 ! (-1)*(-1)=1
         endif
      endif
!c
      iko1 = iko - 1
      ipr = 0
!c
      do 200 i = 1, iko1
         ipt = ipr
         k = i
         do 180 j = 1, ikb
            if(k .lt. 1) go to 190
            if(k .gt. ika) go to 150
            ipt = ipt + ia(k)*ib(j)
  150       continue
            k = k - 1
  180    continue
  190    continue
         ipr = ipt/mhalf
         io(i) = ipt - ipr*mhalf
  200 continue
!c
      io(iko) = ipr
      if(ipr.ge.mhalf) then
       write(*,*) ' IKO=',iko,iko/mhalf
       stop 4720
      end if
!c
      iko1 = iko
      do 300 i = iko1, ika+1, -1
         if(io(i) .ne. 0) go to 400
         iko = iko - 1
  300 continue
  400 continue
!c
      return
      end subroutine mulmul

!=======================================================================

      subroutine mzran(i,j,k,n,mzrn)
!c
      integer*8 i, j, k, n, mzrn
!c
      mzrn = i - k
      if(mzrn .lt. 0) mzrn = mzrn + 2147483579
      i = j
      j = k
      k = mzrn
      n = 69069*n + 1013904243
      mzrn = mzrn + n
!c
      return
      end subroutine mzran

!=======================================================================

      subroutine mzrans(is,js,ks,ns,i,j,k,n)
!c
!c     save is,js,ks,ns
!c     data is,js,ks,ns/521288629,362436069,16163801,1131199299/
!c
      integer*8 i, j, k, n
      integer*8 is, js, ks, ns
!c
      i = 1+abs(is)
      j = 1+abs(js)
      k = 1+abs(ks)
      n = ns
!c
      return
      end subroutine mzrans

!=======================================================================

      subroutine nghpair(xa, ya, za, x, y, z, x2, y2, z2, icon, ifl,    &
     &                   is, ifn, ifc, id, itl, vol, area, ndst, nmax,  &
     &                   nvmax, nfmax, nw, nt, nq, nb, ng, mhalf,       &
     &                   mfull, isclp, r215, dscle, deps)
!c
      real(8) :: xa(:), ya(:), za(:)
      integer*8 x(:), y(:), z(:)
      integer*8 x2(:), y2(:), z2(:)
      integer*8 icon(:,:), ifl(:), is(:), ifn(:), ifc(:), id(:), itl(:)
      real(8) :: vol(:), area(:), ndst(:)
      integer*8 nkmax
      parameter (nkmax = 30)
      integer*8 io(nkmax), iox(nkmax), ioy(nkmax), ioz(nkmax)
      integer*8 nmax, nvmax, nfmax, nw, nt, nq, nb, ng, mhalf, mfull
      double precision r215, dscle, deps, dnum, dnux, dvol, dare
      double precision xnum, ynum, znum
      integer*8 ifval, i, ileft, ine1, iscur, isini, ilift
      integer*8 ivoli, ivuli, iboli, ibuli
      integer*8 isadj, isbeg, ine, nb1, isclp(:)
      integer*8 isgox, isgoy, isgoz, ikox, ikoy, ikoz
      integer*8 isgo, iko
!c
!c     initialize
!c
      do 30 i=1,nmax
         ifn(i)=0
   30 continue
!c
      ifval=0
      do 50 i=1,nvmax
         id(i)=0
   50 continue
!c
      do 70 i=1,nmax
         itl(i)=0
   70 continue
!c
      do 80 i=1,nmax
         vol(i) = -40.0d0
   80 continue
!c
      ileft = ng
      nb = 0
!c
!c     process next point
!c
  100 continue
      ine1 = nb + 1
      ivoli = 1
      ivuli = 1
      ileft = ileft + 1
      if(ileft.gt.nw) go to 2000
      if(ileft.le.(ileft/1000)*1000)                                    &
     &   write(*,*)'Number of processed cells =',ileft
      ifval = ifval + 1
      iscur = is(ileft)
      if(iscur.le.0) go to 100
      if(iscur.gt.nt) then
       write(*,*) ' ILEFT=',ileft,' NT=',NT,' IS(ILEFT)=',is(ileft)
       write(*,*) 'ISCUR=',iscur,' max(IS)=',maxval(is(1:nvmax))
       stop 410
      end if
      isini = iscur
!c
!c     reorder isini so that ileft equals icon(5,isini)
!c
      call site1ordr(icon, ileft, isini)
!c
      do 300 i=6,8
         ilift=icon(i,isini)
         if(ilift.le.ng) stop 420
         itl(ilift) = ifval
         nb = nb + 1
         if(nb.gt.nfmax) stop 425
         ifc(nb) = ilift
         is(ilift) = isini
  300 continue
!c
!c     mark current tetrahedron
!c
  400 continue
      if(ifl(iscur).eq.0) ivuli = -1
      id(iscur) = ifval
!c
!c     obtain next tetrahedron with ileft as a vertex
!c
      isadj = icon(2,iscur)
      if(isadj.le.0.or.isadj.gt.nq) stop 430
      if(ifl(isadj).eq.2) then
         if(isadj.le.nt) stop 432
         ivoli =-1
         go to 600
      endif
      if(isadj.gt.nt) stop 434
      if(id(isadj).eq.ifval) go to 600
      ilift = icon(8,iscur)
      go to 900
  600 continue
      isadj = icon(3,iscur)
      if(isadj.le.0.or.isadj.gt.nq) stop 440
      if(ifl(isadj).eq.2) then
         if(isadj.le.nt) stop 442
         ivoli =-1
         go to 700
      endif
      if(isadj.gt.nt) stop 444
      if(id(isadj).eq.ifval) go to 700
      ilift = icon(6,iscur)
      go to 900
  700 continue
      isadj = icon(4,iscur)
      if(isadj.le.0.or.isadj.gt.nq) stop 450
      if(iscur .eq. isini) go to 800
      if(isadj.gt.nt) stop 452
      if(icon(3,isadj) .eq. iscur) then
          iscur = isadj
          go to 700
      elseif(icon(2,isadj) .eq. iscur) then
          iscur = isadj
          go to 600
      elseif(icon(4,isadj) .eq. iscur) then
          if(isadj .ne. isini) stop 460
          go to 1000
      else
          stop 465
      endif
  800 continue
      if(ifl(isadj).eq.2) then
         if(isadj.le.nt) stop 467
         ivoli =-1
         go to 1000
      endif
      if(isadj.gt.nt) stop 468
      if(id(isadj).eq.ifval) go to 1000
      ilift = icon(7,iscur)
!c
!c     reorder isadj so that ileft equals icon(5,isadj) and ilift
!c     equals icon(6,isadj)
!c
  900 continue
      call reordr(icon, ileft, ilift, isadj)
      iscur = isadj
      ilift=icon(8,iscur)
      if(ilift.le.ng) stop 470
      if(itl(ilift).eq.ifval) go to 400
      itl(ilift) = ifval
      nb = nb + 1
      if(nb.gt.nfmax) stop 475
      ifc(nb) = ilift
      is(ilift) = iscur
      go to 400
!c
!c     compute volume, areas, distances for current point
!c
 1000 continue
      if(ine1.gt.nb) stop 480
      isbeg = is(ifc(ine1))
      dvol = 0.0d0
      do 1200 ine = ine1, nb
         dare = 0.0d0
         iboli = 1
         ibuli = 1
         ilift = ifc(ine)
         isini = is(ilift)
         if(isini.le.0 .or. isini.gt.nt) stop 490
         if(ifl(isini).eq.0) ibuli =-1
         call reordr(icon, ileft, ilift, isini)
         iscur = icon(3,isini)
         if(iscur.le.0 .or. iscur.gt.nq) stop 510
         if(ifl(iscur).eq.2) then
            if(iscur.le.nt) stop 512
            iboli =-1
         endif
         if(ifl(iscur).ne.2 .and. iscur.gt.nt) stop 514
         if(ifl(iscur).eq.0) ibuli =-1
         call reordr(icon, ileft, ilift, iscur)
         isadj = icon(3,iscur)
         if(isadj .eq. isini) stop 520
 1150    continue
         if(isadj.le.0 .or. isadj.gt.nq) stop 530
         if(ifl(isadj).eq.2) then
            if(isadj.le.nt) stop 532
            iboli =-1
         endif
         if(ifl(isadj).ne.2 .and. isadj.gt.nt) stop 534
         if(ifl(isadj).eq.0) ibuli =-1
         call reordr(icon, ileft, ilift, isadj)
!c
         if(iboli.ne.-1 .and. ibuli.ne.-1) then
!c
!c        here compute area of isini-iscur-isadj triangle and
!c        add to area(ine)
!c
            call crossp(x, y, z, x2, y2, z2, isini, iscur, isadj,       &
     &                  mhalf, mfull, isclp, iox, isgox, ikox,          &
     &                  ioy, isgoy, ikoy, ioz, isgoz, ikoz)
            if(isgox.eq.0 .and. isgoy.eq.0 .and. isgoz.eq.0) go to 1160
            call doubnm(iox, isgox, ikox, r215, xnum)
            call doubnm(ioy, isgoy, ikoy, r215, ynum)
            call doubnm(ioz, isgoz, ikoz, r215, znum)
            dnux = dmax1(dabs(xnum),dabs(ynum),dabs(znum))
            if(dnux.lt.deps) dnux = 1.0d0
            xnum = xnum/dnux
            ynum = ynum/dnux
            znum = znum/dnux
            dnum = dsqrt(xnum**2+ynum**2+znum**2)
            dare = dare + ((dnux/dscle)/dscle)*dnum
         endif
!c
         if(ivoli.ne.-1 .and. ivuli.ne.-1) then
!c
!c           here compute volume of isbeg-isini-iscur-isadj tetrahedron
!c           and add to vol(ileft)
!c
            if(iboli.eq.-1 .or. ibuli.eq.-1) stop 540
            call tetvol(x, y, z, x2, y2, z2, isbeg, isini, iscur, isadj,&
     &                  mhalf, mfull, isclp, io, isgo, iko)
            if(isgo.le.0) go to 1160
            call doubnm(io, isgo, iko, r215, dnum)
            dvol = dvol + (((dnum/dscle)/dscle)/dscle)
         endif
!c
 1160    continue
         iscur = isadj
         isadj = icon(3,iscur)
         if(isadj .ne. isini) go to 1150
         if(iboli.eq.-1 .and. ibuli.eq.-1) then
            area(ine) = -30.0
         elseif(iboli.eq.-1) then
            area(ine) = -20.0
         elseif(ibuli.eq.-1) then
            area(ine) = -10.0
         else
            area(ine) = dare/2.0d0
         endif
!c
!c     compute distances
!c
         ndst(ine) = dsqrt((xa(ileft)-xa(ilift))**2 +                   &
     &                     (ya(ileft)-ya(ilift))**2 +                   &
     &                     (za(ileft)-za(ilift))**2)
 1200 continue
!c
      if(ivoli.eq.-1 .and. ivuli.eq.-1) then
         vol(ileft) = -30.0
      elseif(ivoli.eq.-1) then
         vol(ileft) = -20.0
      elseif(ivuli.eq.-1) then
         vol(ileft) = -10.0
      else
         vol(ileft) = dvol/6.0d0
      endif
!c
      nb1 = nb
      nb = ine1 - 1
      do 1600 ine = ine1, nb1
         if(ifc(ine).lt.ileft) go to 1600
         nb = nb+1
         ifc(nb) = ifc(ine)
         area(nb) = area(ine)
         ndst(nb) = ndst(ine)
 1600 continue
!c
      if(nb.lt.ine1) go to 100
      ifn(ileft) = nb
      go to 100
 2000 continue
!c
      return
      end subroutine nghpair

!=======================================================================

      subroutine orient(tetra, icon, ifl, xi, yi, zi, x, y, z, x2, y2,  &
     &                  z2, idmin, mhalf, mfull, isclp, epz)
!c
      real(8) :: xi(:), yi(:), zi(:)
      integer*8 x(:), y(:), z(:), x2(:), y2(:), z2(:)
      double precision epz
      integer*8 tetra, icon(:,:), ifl(:), a, b, c, d, idmin
      integer*8 isclp(:), mhalf, mfull
      integer*8 i, iside
!c
!c     test all tetrahedra with ifl equal to 1
!c
      idmin = 0
      do 200 i=1,tetra
         if(ifl(i).ne.1) go to 200
         a=icon(5,i)
         b=icon(6,i)
         c=icon(7,i)
         d=icon(8,i)
         call irsign(xi, yi, zi, x, y, z, x2, y2, z2, d, a, b, c,       &
     &               mhalf, mfull, isclp, epz, iside)
         if(iside .le. 0) idmin = idmin+1
  200 continue
!c
      return
      end subroutine orient

!=======================================================================

      subroutine pntins(xi, yi, zi, wi, x, y, z, w, x2, y2, z2, w2,     &
     &                  icon, is, ifl, id, ih, ihn, k, nvmax, nhmax,    &
     &                  tetra, mxlook, xc, yc, zc, issin, iftal, delaun,&
     &                  flphis, mhalf, mfull, isclp, isclw, isclr, epz, &
     &                  ITCHK,ITCN1,ITCN2,ITCN3,ITCN4)
!c
      real(8) :: xi(:), yi(:), zi(:), wi(:)
      integer*8 x(:), y(:), z(:), w(:)
      integer*8 x2(:), y2(:), z2(:), w2(:)
      integer*8 icon(:,:), is(:), ifl(:), id(:), ih(:)
      integer*8 xc(:), yc(:), zc(:)
      integer*8 ihn, k, nvmax, nhmax, mxlook, issin, iftal
      integer*8 isclp(:), isclw(:), isclr(:), curr, side1, side2, tetra
      double precision epz
      logical delaun, flphis
      integer*8 mhalf, mfull, itype, look, newtts, ired
      integer*8 itchk, itcn1, itcn2, itcn3, itcn4
!c
      look = 0
      if(flphis) then
         itype = -1
         look = 1
         call gettet(itype, k, xi, yi, zi, x, y, z, x2, y2, z2, icon,   &
     &               curr, side1, side2, xc, yc, zc, mhalf, mfull,      &
     &               isclp, epz, ITCHK)
!c
   50    continue
         if (icon(5,curr) .lt. 0) then
            call lkdown(icon, curr, xi, yi, zi, x, y, z, x2, y2, z2,    &
     &                  itype, k, side1, side2, xc, yc, zc, mhalf,      &
     &                  mfull, isclp, epz, ITCHK)
            look = look + 1
            if(itype.eq.-1) stop 410
            goto 50
         endif
      else
         call shishk(xi, yi, zi, x, y, z, x2, y2, z2, is, icon, id,     &
     &               issin, k, side1, side2, curr, iftal, itype, tetra, &
     &               xc, yc, zc, mhalf, mfull, isclp, epz, ITCHK)
         if(itype.eq.-1) stop 420
      endif
!c
      if (itype .eq. 1) then
         call vrtins (k, w, w2, icon, nvmax, tetra, curr, is, id, iftal,&
     &                side1, ifl, newtts, ired, delaun, flphis, mhalf,  &
     &                mfull, isclw)
         ITCN1 = ITCN1+1
      elseif (itype .eq. 2) then
         call intins (k, xi, yi, zi, wi, x, y, z, w, x2, y2, z2, w2,    &
     &                icon, ih, ihn, nhmax, nvmax, tetra, curr, is, ifl,&
     &                newtts, ired, delaun, flphis, mhalf, mfull,       &
     &                isclp, isclw, isclr, epz)
         ITCN2 = ITCN2+1
      elseif (itype .eq. 3) then
         call edgins (k, x, y, z, w, x2, y2, z2, w2, icon, ih, ihn,     &
     &                nhmax, nvmax, tetra, curr, is, side1, side2,      &
     &                ifl, newtts, ired, delaun, flphis, mhalf, mfull,  &
     &                isclp, isclw, isclr)
         ITCN3 = ITCN3+1
      elseif (itype .eq. 4) then
         call sidins (k, x, y, z, w, x2, y2, z2, w2, icon, ih, ihn,     &
     &                nhmax, nvmax, tetra, curr, is, side1, ifl,        &
     &                newtts, ired, delaun, flphis, mhalf, mfull,       &
     &                isclp, isclw, isclr)
         ITCN4 = ITCN4+1
      else
         stop 430
      endif
      if(ired .eq. 1) go to 1000
      issin = k
!c
!c     optimize for Regular/Delaunay property
!c
      call sphere(k, icon, ifl, ih, ihn, nhmax, newtts, xi, yi, zi, wi, &
     &            x, y, z, w, x2, y2, z2, w2, tetra, is, nvmax, xc, yc, &
     &            zc, delaun, flphis, mhalf, mfull, isclp, isclw, isclr,&
     &            epz)
 1000 continue
!c
!c     if (look .gt. mxlook) mxlook = look
      if (flphis .and. look .gt. mxlook) mxlook = look
!c
      return
      end subroutine pntins

!=======================================================================

      subroutine pntred(xi, yi, zi, wi, x, y, z, w, x2, y2, z2, w2,     &
     &                  icon, is, id, k, tetra, xc, yc, zc,             &
     &                  idmax, issin, iftal, delaun, flphis, mhalf,     &
     &                  mfull, isclp, isclw, isclr, epz,                &
     &                  ITCHK,ITCN1,ITCN2,ITCN3,ITCN4)
!c
      real(8) :: xi(:), yi(:), zi(:), wi(:)
      integer*8 x(:), y(:), z(:), w(:)
      integer*8 x2(:), y2(:), z2(:), w2(:)
      integer*8 icon(:,:), is(:), id(:)
      integer*8 xc(:), yc(:), zc(:)
      integer*8 k, idmax, issin, iftal, mhalf, mfull
      integer*8 curr, side1, side2, tetra, a, b, c, d
      integer*8 isclp(:), isclw(:), isclr(:)
      integer*8 site1, site2, site3, site4, fndsit
      double precision epz, tdist, xctr, yctr, zctr
      logical delaun, flphis
      integer*8 itype, itide, ipossi, i
      integer*8 itchk, itcn1, itcn2, itcn3, itcn4
!c
      if(flphis) then
         itype = -1
         call gettet(itype, k, xi, yi, zi, x, y, z, x2, y2, z2, icon,   &
     &               curr, side1, side2, xc, yc, zc, mhalf, mfull,      &
     &               isclp, epz, ITCHK)
!c
   50    continue
         if (icon(5,curr) .lt. 0) then
            call lkdown(icon, curr, xi, yi, zi, x, y, z, x2, y2, z2,    &
     &                  itype, k, side1, side2, xc, yc, zc, mhalf,      &
     &                  mfull, isclp, epz, ITCHK)
            if(itype.eq.-1) stop 510
            goto 50
         endif
      else
         call shishk(xi, yi, zi, x, y, z, x2, y2, z2, is, icon, id,     &
     &               issin, k, side1, side2, curr, iftal, itype, tetra, &
     &               xc, yc, zc, mhalf, mfull, isclp, epz, ITCHK)
         if(itype.eq.-1) stop 515
      endif
!c
      if (itype .eq. 1) then
         ITCN1 = ITCN1+1
         site1 = icon(side1+4,curr)
!c        itide = 1
!c        if(w(k) .gt. w(site1)) itide =-1
         call iwsign(w, w2, site1, k, mhalf, mfull, isclw, itide)
      elseif (itype .eq. 2) then
         ITCN2 = ITCN2+1
         a = icon(5,curr)
         b = icon(6,curr)
         c = icon(7,curr)
         d = icon(8,curr)
         if(a.le.8 .or. b.le.8 .or. c.le.8 .or. d.le.8) stop 520
         call ctrad(xi, yi, zi, wi, xctr, yctr, zctr, a, b, c, d,       &
     &              epz, delaun, ipossi)
         if(ipossi.eq.1) go to 60
         call bisphr(xi, yi, zi, wi, k, a, epz, xctr, yctr, zctr, tdist,&
     &               delaun, ipossi)
         if(ipossi.eq.1) go to 60
         itide = 1
         if(tdist.gt.0.0d0) itide = -1
         go to 1000
   60    continue
         call iqsign(x, y, z, w, x2, y2, z2, w2, a, b, c, d, k,         &
     &               mhalf, mfull, isclp, isclw, isclr, delaun, itide)
      elseif (itype .eq. 3) then
         ITCN3 = ITCN3+1
         fndsit = 0
         do 100 i = 5, 8
            if (fndsit .eq. 0) then
               if (i .eq. (side1+4) .or. i .eq. (side2+4)) then
                  goto 100
               else
                  site1 = icon(i,curr)
                  fndsit = 1
               endif
            else
               if (i .eq. (side1+4) .or. i .eq. (side2+4)) then
                  goto 100
               else
                  site2 = icon(i,curr)
                  goto 150
               endif
            endif
  100    continue
         stop 530
  150    continue
!c
         if(site1.le.8 .or. site2.le.8) stop 540
         call iqsig1(x, y, z, w, x2, y2, z2, w2, site1, site2, k,       &
     &               mhalf, mfull, isclp, isclw, isclr, delaun, itide)
      elseif (itype .eq. 4) then
         ITCN4 = ITCN4+1
         site1 = icon(side1+4,curr)
         call sitord(icon, site1, curr)
!c
         site2 = icon(6,curr)
         site3 = icon(7,curr)
         site4 = icon(8,curr)
!c
         if(site2.le.8 .or. site3.le.8 .or. site4.le.8) stop 550
         call iqsig2(x, y, z, w, x2, y2, z2, w2, site2, site3, site4, k,&
     &               mhalf, mfull, isclp, isclw, isclr, delaun, itide)
      else
         stop 560
      endif
!c
 1000 continue
      if(itide.lt.0) idmax = idmax+1
!c
      return
      end subroutine pntred

!=======================================================================

      subroutine pntype(iside, itype, side1, side2)
!c
      integer*8 iside(:), itype, side1, side2
!c
!c     point is in the interior of tetrahedron
!c
      if(iside(1).gt.0 .and. iside(2).gt.0 .and. iside(3).gt.0 .and.    &
     &   iside(4).gt.0) then
         itype = 2
         go to 1000
      endif
!c
!c     unacceptable situation
!c
      if(iside(1).eq.0 .and. iside(2).eq.0 .and. iside(3).eq.0 .and.    &
     &   iside(4).eq.0) stop 971
!c
!c     point is a vertex of tetrahedron
!c
      if(iside(1).eq.0 .and. iside(2).eq.0 .and. iside(3).eq.0) then
         itype = 1
         side1 = 4
         go to 1000
      elseif(iside(1).eq.0 .and. iside(2).eq.0 .and. iside(4).eq.0) then
         itype = 1
         side1 = 3
         go to 1000
      elseif(iside(1).eq.0 .and. iside(3).eq.0 .and. iside(4).eq.0) then
         itype = 1
         side1 = 2
         go to 1000
      elseif(iside(2).eq.0 .and. iside(3).eq.0 .and. iside(4).eq.0) then
         itype = 1
         side1 = 1
         go to 1000
      endif
!c
!c     point is in the interior of an edge of tetrahedron
!c
      if (iside(1).eq.0 .and. iside(2).eq.0) then
         itype = 3
         side1 = 1
         side2 = 2
         go to 1000
      elseif (iside(1).eq.0 .and. iside(3).eq.0) then
         itype = 3
         side1 = 1
         side2 = 3
         go to 1000
      elseif (iside(1).eq.0 .and. iside(4).eq.0) then
         itype = 3
         side1 = 1
         side2 = 4
         go to 1000
      elseif (iside(2).eq.0 .and. iside(3).eq.0) then
         itype = 3
         side1 = 2
         side2 = 3
         go to 1000
      elseif (iside(2).eq.0 .and. iside(4).eq.0) then
         itype = 3
         side1 = 2
         side2 = 4
         go to 1000
      elseif (iside(3).eq.0 .and. iside(4).eq.0) then
         itype = 3
         side1 = 3
         side2 = 4
         go to 1000
      endif
!c
!c     point is in the interior of a facet of tetrahedron
!c
      itype = 4
      if (iside(1) .eq. 0) then
         side1 = 1
      elseif (iside(2) .eq. 0) then
         side1 = 2
      elseif (iside(3) .eq. 0) then
         side1 = 3
      elseif (iside(4) .eq. 0) then
         side1 = 4
      else
         stop 972
      endif
!c
 1000 continue
      return
      end subroutine pntype

!=======================================================================

      subroutine poltri(x, y, z, w, ix, iy, iz, iw, ix2, iy2, iz2, iw2, &
     &                  icon, is, ifl, id, ih, ihn, xmin, ymin, zmin,   &
     &                  wmin, xmax, ymax, zmax, wmax, iftal, nv, nmax,  &
     &                  nvmax, nhmax, wlenx, wleny, wlenz, wlenw, tetra,&
     &                  mxlook, irec, naddl, iredx, delaun, flphis,     &
     &                  redchk, icsfig, iwsfig, mhalf, mfull, isclp,    &
     &                  isclw, isclr, epz)
!c
      real(8) :: x(:), y(:), z(:), w(:)
      integer*8 ix(:), iy(:), iz(:), iw(:)
      integer*8 ix2(:), iy2(:), iz2(:), iw2(:)
      integer*8 icon(:,:), is(:), ifl(:), id(:), ih(:)
      double precision xc(8), yc(8), zc(8)
      integer*8 ixc(8), iyc(8), izc(8)
      integer*8 nmax, nvmax, nhmax
      integer*8 ihn, nv, naddl, icsfig, iwsfig, mxlook, irec, iredx
      integer*8 mhalf, mfull, ibsfig, itsfig
      double precision wlenx, wleny, wlenz, wlenw, epz, wbig, wmen, wman
      logical delaun, flphis, redchk
      integer*8 isclp(:), isclw(:), isclr(:), tetra
      double precision xmin, xmax, ymin, ymax, zmin, zmax, wmin, wmax
      double precision xlen, ylen, zlen, wlen, xctr, yctr, zctr
      double precision xlan, ylan, zlan, wlan, xint, yint, zint
      double precision xcor, ycor, zcor
      double precision xmx, ymx, zmx, xmn, ymn, zmn
      double precision dscle, dscli, dfull, dfill, decml
      integer*8 iftal, i, naddm, ng, j, i9, icsfi2, irsfig, isclu, isgcl
      integer*8 i10, issin, k, iredu
      integer*8 itchk, itcn1, itcn2, itcn3, itcn4

!c
!c     initialize
!c
      mxlook = 0
!c
!c     test parameters
!c
      if (nv .gt. nmax) stop 210
      if (nv .lt. 9) stop 220
      if (nvmax .lt. 12) stop 230
!c
!c     construct cube
!c
      xlen=xmax-xmin
      ylen=ymax-ymin
      zlen=zmax-zmin
      wlan=(dmax1(xlen,ylen,zlen)/2.0d0) + dmax1(wlenx,wleny,wlenz)
!!BERNAL      wlen=wlan + 4.0d0
      wlen=wlan*(1.d0 + 1.d-6)
      if(wlen.le.wlan) stop 235
!c
      xctr=(xmax+xmin)/2.0d0
      yctr=(ymax+ymin)/2.0d0
      zctr=(zmax+zmin)/2.0d0
!c     WRITE(*,*)'XYCTR=',XCTR,YCTR,ZCTR,' WLEN=',WLEN
!c
!c     identify cube corner direction vectors
!c
      xc(1) = - 1.0d0
      yc(1) = - 1.0d0
      zc(1) = + 1.0d0
!c
      xc(2) = - 1.0d0
      yc(2) = + 1.0d0
      zc(2) = + 1.0d0
!c
      xc(3) = + 1.0d0
      yc(3) = + 1.0d0
      zc(3) = + 1.0d0
!c
      xc(4) = + 1.0d0
      yc(4) = - 1.0d0
      zc(4) = + 1.0d0
!c
      xc(5) = - 1.0d0
      yc(5) = - 1.0d0
      zc(5) = - 1.0d0
!c
      xc(6) = - 1.0d0
      yc(6) = + 1.0d0
      zc(6) = - 1.0d0
!c
      xc(7) = + 1.0d0
      yc(7) = + 1.0d0
      zc(7) = - 1.0d0
!c
      xc(8) = + 1.0d0
      yc(8) = - 1.0d0
      zc(8) = - 1.0d0
!c
!c     compute corners of cube
!c
      do 50 i=1,8
         x(i)=xctr+wlen*xc(i)
         y(i)=yctr+wlen*yc(i)
         z(i)=zctr+wlen*zc(i)
         if((x(i).ge.xmin.and.x(i).le.xmax).or.(y(i).ge.ymin.and.       &
     &       y(i).le.ymax).or.(z(i).ge.zmin.and.z(i).le.zmax)) stop 240
   50 continue
!c
!c     make coordinates of corners of cube into whole numbers
!c
      dfull = dble(mfull)
      if(dabs(x(3)+1.0d0).ge.dfull) stop 242
      if(dabs(y(3)+1.0d0).ge.dfull) stop 243
      if(dabs(z(3)+1.0d0).ge.dfull) stop 244
      if(dabs(x(5)-1.0d0).ge.dfull) stop 245
      if(dabs(y(5)-1.0d0).ge.dfull) stop 246
      if(dabs(z(5)-1.0d0).ge.dfull) stop 247
!c
      xmx = dble(idnint(x(3)+1.0d0))
      ymx = dble(idnint(y(3)+1.0d0))
      zmx = dble(idnint(z(3)+1.0d0))
      xmn = dble(idnint(x(5)-1.0d0))
      ymn = dble(idnint(y(5)-1.0d0))
      zmn = dble(idnint(z(5)-1.0d0))
!c
      xlan = xmx - xmn
      ylan = ymx - ymn
      zlan = zmx - zmn
      wlan = dmax1(xlan,ylan,zlan)
!c
      x(1) = xmn
      y(1) = ymn
      z(1) = zmn + wlan
!c
      x(2) = xmn
      y(2) = ymn + wlan
      z(2) = zmn + wlan
!c
      x(3) = xmn + wlan
      y(3) = ymn + wlan
      z(3) = zmn + wlan
!c
      x(4) = xmn + wlan
      y(4) = ymn
      z(4) = zmn + wlan
!c
      x(5) = xmn
      y(5) = ymn
      z(5) = zmn
!c
      x(6) = xmn
      y(6) = ymn + wlan
      z(6) = zmn
!c
      x(7) = xmn + wlan
      y(7) = ymn + wlan
      z(7) = zmn
!c
      x(8) = xmn + wlan
      y(8) = ymn
      z(8) = zmn
!c
      do 55 i=1,8
         if((x(i).ge.xmin.and.x(i).le.xmax).or.(y(i).ge.ymin.and.       &
     &       y(i).le.ymax).or.(z(i).ge.zmin.and.z(i).le.zmax)) stop 250
   55 continue
!c
      if(irec.eq.8) go to 155
!c
!c     compute corners of rectangular polyhedron
!c
      x( 9) = xmin - wlenx
      y( 9) = ymin - wleny
      z( 9) = zmax + wlenz
!c
      x(10) = xmin - wlenx
      y(10) = ymax + wleny
      z(10) = zmax + wlenz
!c
      x(11) = xmax + wlenx
      y(11) = ymax + wleny
      z(11) = zmax + wlenz
!c
      x(12) = xmax + wlenx
      y(12) = ymin - wleny
      z(12) = zmax + wlenz
!c
      x(13) = xmin - wlenx
      y(13) = ymin - wleny
      z(13) = zmin - wlenz
!c
      x(14) = xmin - wlenx
      y(14) = ymax + wleny
      z(14) = zmin - wlenz
!c
      x(15) = xmax + wlenx
      y(15) = ymax + wleny
      z(15) = zmin - wlenz
!c
      x(16) = xmax + wlenx
      y(16) = ymin - wleny
      z(16) = zmin - wlenz
!c
      do 60 i=9,16
         if((x(i).ge.xmin.and.x(i).le.xmax).or.(y(i).ge.ymin.and.       &
     &       y(i).le.ymax).or.(z(i).ge.zmin.and.z(i).le.zmax)) stop 260
   60 continue
      if(x(1).ge.x( 9) .or. y(1).ge.y( 9) .or. z(1).le.z( 9) .or.       &
     &   x(2).ge.x(10) .or. y(2).le.y(10) .or. z(2).le.z(10) .or.       &
     &   x(3).le.x(11) .or. y(3).le.y(11) .or. z(3).le.z(11) .or.       &
     &   x(4).le.x(12) .or. y(4).ge.y(12) .or. z(4).le.z(12) .or.       &
     &   x(5).ge.x(13) .or. y(5).ge.y(13) .or. z(5).ge.z(13) .or.       &
     &   x(6).ge.x(14) .or. y(6).le.y(14) .or. z(6).ge.z(14) .or.       &
     &   x(7).le.x(15) .or. y(7).le.y(15) .or. z(7).ge.z(15) .or.       &
     &   x(8).le.x(16) .or. y(8).ge.y(16) .or. z(8).ge.z(16)) stop 270
!c
      xmin = xmin - wlenx
      ymin = ymin - wleny
      zmin = zmin - wlenz
      xmax = xmax + wlenx
      ymax = ymax + wleny
      zmax = zmax + wlenz
!c
      if(naddl.eq.2) go to 155
!c
!c     compute other points in grid on surface of polyhedron
!c
      naddm = naddl-2
      xint = (xmax-xmin)/dble(naddl-1)
      yint = (ymax-ymin)/dble(naddl-1)
      zint = (zmax-zmin)/dble(naddl-1)
      ng = 16
!c
      do 119 i = 1, naddm
         xcor = xmin + dble(i)*xint
         ng = ng + 4
         x(ng-3) = xcor
         y(ng-3) = ymin
         z(ng-3) = zmin
         x(ng-2) = xcor
         y(ng-2) = ymin
         z(ng-2) = zmax
         x(ng-1) = xcor
         y(ng-1) = ymax
         z(ng-1) = zmin
         x(ng) = xcor
         y(ng) = ymax
         z(ng) = zmax
  119 continue
!c
      do 120 i = 1, naddm
         ycor = ymin + dble(i)*yint
         ng = ng + 4
         y(ng-3) = ycor
         z(ng-3) = zmin
         x(ng-3) = xmin
         y(ng-2) = ycor
         z(ng-2) = zmin
         x(ng-2) = xmax
         y(ng-1) = ycor
         z(ng-1) = zmax
         x(ng-1) = xmin
         y(ng) = ycor
         z(ng) = zmax
         x(ng) = xmax
  120 continue
!c
      do 121 i = 1, naddm
         zcor = zmin + dble(i)*zint
         ng = ng + 4
         z(ng-3) = zcor
         x(ng-3) = xmin
         y(ng-3) = ymin
         z(ng-2) = zcor
         x(ng-2) = xmin
         y(ng-2) = ymax
         z(ng-1) = zcor
         x(ng-1) = xmax
         y(ng-1) = ymin
         z(ng) = zcor
         x(ng) = xmax
         y(ng) = ymax
  121 continue
!c
      do 130 i = 1, naddm
         xcor = xmin + dble(i)*xint
         do 125 j = 1, naddm
            ycor = ymin + dble(j)*yint
            ng = ng + 2
            x(ng-1) = xcor
            y(ng-1) = ycor
            z(ng-1) = zmin
            x(ng) = xcor
            y(ng) = ycor
            z(ng) = zmax
  125    continue
  130 continue
!c
      do 140 i = 1, naddm
         ycor = ymin + dble(i)*yint
         do 135 j = 1, naddm
            zcor = zmin + dble(j)*zint
            ng = ng + 2
            y(ng-1) = ycor
            z(ng-1) = zcor
            x(ng-1) = xmin
            y(ng) = ycor
            z(ng) = zcor
            x(ng) = xmax
  135    continue
  140 continue
!c
      do 150 i = 1, naddm
         zcor = zmin + dble(i)*zint
         do 145 j = 1, naddm
            xcor = xmin + dble(j)*xint
            ng = ng + 2
            z(ng-1) = zcor
            x(ng-1) = xcor
            y(ng-1) = ymin
            z(ng) = zcor
            x(ng) = xcor
            y(ng) = ymax
  145    continue
  150 continue
!c
      if(ng.ne.irec) stop 280
!c
  155 continue
!c
!c     find i9
!c
      do 157 i = 9, nv
         if(is(i).ne.0) go to 158
  157 continue
      stop 282
  158 continue
      i9 = i
!c
!c     divide cube into 12 tetrahedra
!c
      icon(1,1) = 0
      icon(2,1) = 5
      icon(3,1) = 9
      icon(4,1) = 2
      icon(5,1) = i9
      icon(6,1) = 7
      icon(7,1) = 3
      icon(8,1) = 4
!c
      icon(1,2) = 0
      icon(2,2) = 1
      icon(3,2) = 12
      icon(4,2) = 3
      icon(5,2) = i9
      icon(6,2) = 2
      icon(7,2) = 3
      icon(8,2) = 7
!c
      icon(1,3) = 0
      icon(2,3) = 5
      icon(3,3) = 2
      icon(4,3) = 4
      icon(5,3) = i9
      icon(6,3) = 2
      icon(7,3) = 1
      icon(8,3) = 3
!c
      icon(1,4) = 0
      icon(2,4) = 3
      icon(3,4) = 11
      icon(4,4) = 6
      icon(5,4) = i9
      icon(6,4) = 5
      icon(7,4) = 1
      icon(8,4) = 2
!c
      icon(1,5) = 0
      icon(2,5) = 6
      icon(3,5) = 1
      icon(4,5) = 3
      icon(5,5) = i9
      icon(6,5) = 3
      icon(7,5) = 1
      icon(8,5) = 4
!c
      icon(1,6) = 0
      icon(2,6) = 4
      icon(3,6) = 7
      icon(4,6) = 5
      icon(5,6) = i9
      icon(6,6) = 4
      icon(7,6) = 1
      icon(8,6) = 5
!c
      icon(1,7) = 0
      icon(2,7) = 6
      icon(3,7) = 8
      icon(4,7) = 9
      icon(5,7) = i9
      icon(6,7) = 8
      icon(7,7) = 4
      icon(8,7) = 5
!c
      icon(1,8) = 0
      icon(2,8) = 10
      icon(3,8) = 9
      icon(4,8) = 7
      icon(5,8) = i9
      icon(6,8) = 8
      icon(7,8) = 5
      icon(8,8) = 7
!c
      icon(1,9) = 0
      icon(2,9) = 7
      icon(3,9) = 8
      icon(4,9) = 1
      icon(5,9) = i9
      icon(6,9) = 7
      icon(7,9) = 4
      icon(8,9) = 8
!c
      icon(1,10) = 0
      icon(2,10) = 11
      icon(3,10) = 12
      icon(4,10) = 8
      icon(5,10) = i9
      icon(6,10) = 7
      icon(7,10) = 5
      icon(8,10) = 6
!c
      icon(1,11) = 0
      icon(2,11) = 4
      icon(3,11) = 12
      icon(4,11) = 10
      icon(5,11) = i9
      icon(6,11) = 6
      icon(7,11) = 5
      icon(8,11) = 2
!c
      icon(1,12) = 0
      icon(2,12) = 2
      icon(3,12) = 10
      icon(4,12) = 11
      icon(5,12) = i9
      icon(6,12) = 6
      icon(7,12) = 2
      icon(8,12) = 7
!c
      tetra = 12
!c
      wmen = wmin
      if(.not.delaun .and. irec.gt.8) then
         wmen = wmen - wlenw
         do 160 i=9,irec
            w(i) = wmen
  160    continue
      endif
      if(wmen.lt.wmin) wmin = wmen
      if(wmen.gt.wmax) wmax = wmen
      wman = wmin - 10.0d0
      do 170 i=1,8
         w(i) = wman
  170 continue
!c
      is(1) = 5
      is(2) = 12
      is(3) = 1
      is(4) = 9
      is(5) = 10
      is(6) = 12
      is(7) = 12
      is(8) = 9
      is(i9) = 12
!c
!c     test # of significant figures of nondecimal part of coordinates
!c
      wbig = 0.0d0
      if(wbig .lt. dabs(xmax)) wbig = dabs(xmax)
      if(wbig .lt. dabs(xmin)) wbig = dabs(xmin)
      if(wbig .lt. dabs(ymax)) wbig = dabs(ymax)
      if(wbig .lt. dabs(ymin)) wbig = dabs(ymin)
      if(wbig .lt. dabs(zmax)) wbig = dabs(zmax)
      if(wbig .lt. dabs(zmin)) wbig = dabs(zmin)
      wbig = wbig + epz
!c     WRITE(*,*)'COORDINATES WBIG=',WBIG
      ibsfig = 0
  180 continue
      ibsfig = ibsfig+1
      wbig = wbig/10.0d0
      if(wbig .ge. 1.0d0) go to 180
      if(ibsfig.gt.9) then
         write(*,*)'Number of significant figures of largest ',         &
     &             'nondecimal part of'
         write(*,*)'a point coordinate appears to be greater than 9.'
         write(*,*)'Program is terminated.'
         stop 286
      endif
      itsfig = ibsfig + icsfig
!c     WRITE(*,*)'ITSFIG=',ITSFIG,' IBSFIG=',IBSFIG,' ICSFIG=',ICSFIG
      if(itsfig.gt.14) then
         write(*,*)' '
         write(*,*)'For this execution of the program the largest ',    &
     &             'total number of'
         write(*,*)'significant figures ',                              &
     &             'that a coordinate requires appears to be ',itsfig
         write(*,*)'Since the maximum allowed is 14, the number of ',   &
     &             'significant'
         write(*,*)'figures of the decimal part of the coordinates ',   &
     &             'for this run is '
         write(*,*)'decreased accordingly.'
         icsfig = 14 - ibsfig
         write(*,*)' '
         write(*,*)'Now icfig = ',icsfig
         write(*,*)' '
      endif
!c
!c     if a Regular tetrahedralization test # of significant figures
!c     of nondecimal part of weights
!c
      if(delaun) go to 200
      wbig = 0.0d0
      if(wbig .lt. dabs(wmax)) wbig = dabs(wmax)
      if(wbig .lt. dabs(wmin)) wbig = dabs(wmin)
      wbig = wbig + epz
!c     WRITE(*,*)'COORDINATES WBIG=',WBIG
      ibsfig = 0
  190 continue
      ibsfig = ibsfig+1
      wbig = wbig/10.0d0
      if(wbig .ge. 1.0d0) go to 190
      if(ibsfig.gt.9) then
         write(*,*)'Number of significant figures of largest ',         &
     &             'nondecimal part of'
         write(*,*)'a weight appears to be greater than 9.'
         write(*,*)'Program is terminated.'
         stop 288
      endif
      itsfig = ibsfig + iwsfig
!c     WRITE(*,*)'ITSFIG=',ITSFIG,' IBSFIG=',IBSFIG,' IWSFIG=',IWSFIG
      if(itsfig.gt.14) then
         write(*,*)' '
         write(*,*)'For this execution of the program the largest ',    &
     &             'total number of'
         write(*,*)'significant figures ',                              &
     &             'that a weight requires appears to be ',itsfig
         write(*,*)'Since the maximum allowed is 14, the number of ',   &
     &             'significant'
         write(*,*)'figures of the decimal part of the weights for ',   &
     &             'this run is '
         write(*,*)'decreased accordingly.'
         iwsfig = 14 - ibsfig
         icsfi2 = 2*icsfig
         irsfig = icsfi2 - iwsfig
         if(irsfig.gt.9) then
            write(*,*)'In order to make this number compatible with ',  &
     &                'that of the'
            write(*,*)'coordinates, the latter is also decreased ',     &
     &                'accordingly.'
            icsfig = (iwsfig+9)/2
         endif
         write(*,*)' '
         write(*,*)'Now icfig = ',icsfig,' iwfig = ',iwsfig
         write(*,*)' '
      endif
  200 continue
!c
!c     test number of significant figures of decimal part of coordinates
!c
      if(icsfig.lt.0 .or. icsfig.gt.9) then
         write(*,*)'Number of significant figures of decimal'
         write(*,*)'part of coordinates is out of range.'
         write(*,*)'Program is terminated.'
         stop 290
      endif
      isclu = 1
      dscle = 1.0d0
      if(icsfig.eq.0) go to 220
      do 210 i = 1, icsfig
         isclu = 10*isclu
         dscle = 10.0d0*dscle
  210 continue
  220 continue
      if(abs(isclu).ge.mfull) stop 295
      call decomp(isclp, isgcl, isclu, mhalf)
      if(isgcl.ne.1) stop 297
!c
!c     test lengths of x, y, z-coordinates, shift and make them integers
!c
      dfull = dble(mfull)
      dfill=dfull/dscle
      do 235 i = 1, nv
         ix2(i) = 0
         iy2(i) = 0
         iz2(i) = 0
         if(dabs(x(i)).lt.dfill) then
            ix(i) = idnint(dscle*x(i))
            if(abs(ix(i)).lt.mfull) then
               x(i) = dble(ix(i))/dscle
               go to 225
            endif
         endif
         if(dabs(x(i)).ge.dfull) stop 305
         ix(i) = idint(x(i))
         if(abs(ix(i)).ge.mfull) stop 310
         decml = (x(i) - dint(x(i)))*dscle
         if(dabs(decml).ge.dfull) stop 312
         ix2(i) = idnint(decml)
         if(abs(ix2(i)).ge.mfull) stop 315
         if(abs(ix2(i)).eq.0) then
            x(i) = dble(ix(i))
            ix2(i) = mfull
         else
            x(i) = dble(ix(i)) + (dble(ix2(i))/dscle)
         endif
  225    continue
         if(dabs(y(i)).lt.dfill) then
            iy(i) = idnint(dscle*y(i))
            if(abs(iy(i)).lt.mfull) then
               y(i) = dble(iy(i))/dscle
               go to 230
            endif
         endif
         if(dabs(y(i)).ge.dfull) stop 320
         iy(i) = idint(y(i))
         if(abs(iy(i)).ge.mfull) stop 325
         decml = (y(i) - dint(y(i)))*dscle
         if(dabs(decml).ge.dfull) stop 327
         iy2(i) = idnint(decml)
         if(abs(iy2(i)).ge.mfull) stop 330
         if(abs(iy2(i)).eq.0) then
            y(i) = dble(iy(i))
            iy2(i) = mfull
         else
            y(i) = dble(iy(i)) + (dble(iy2(i))/dscle)
         endif
  230    continue
         if(dabs(z(i)).lt.dfill) then
            iz(i) = idnint(dscle*z(i))
            if(abs(iz(i)).lt.mfull) then
               z(i) = dble(iz(i))/dscle
               go to 235
            endif
         endif
         if(dabs(z(i)).ge.dfull) stop 335
         iz(i) = idint(z(i))
         if(abs(iz(i)).ge.mfull) stop 340
         decml = (z(i) - dint(z(i)))*dscle
         if(dabs(decml).ge.dfull) stop 342
         iz2(i) = idnint(decml)
         if(abs(iz2(i)).ge.mfull) stop 345
         if(abs(iz2(i)).eq.0) then
            z(i) = dble(iz(i))
            iz2(i) = mfull
         else
            z(i) = dble(iz(i)) + (dble(iz2(i))/dscle)
         endif
  235 continue
!c
!c     if a Regular tetrahedralization test number of significant figures
!c     of decimal part of weights, test lengths of weights, shift and
!c     make them integers
!c
      if(delaun) go to 300
      icsfi2 = 2*icsfig
      irsfig = icsfi2 - iwsfig
      if(iwsfig.lt.0.or.iwsfig.gt.9 .or. irsfig.lt.0.or.irsfig.gt.9)then
         write(*,*)'Either number of significant figures of decimal'
         write(*,*)'part of weights is out of range or it is not'
         write(*,*)'compatible with that of point coordinates.'
         write(*,*)'Program is terminated.'
         stop 350
      endif
      isclu = 1
      dscli = 1.0d0
      if(iwsfig.eq.0) go to 250
      do 240 i = 1, iwsfig
         isclu = 10*isclu
         dscli = 10.0d0*dscli
  240 continue
  250 continue
      if(abs(isclu).ge.mfull) stop 360
      call decomp(isclw, isgcl, isclu, mhalf)
      if(isgcl.ne.1) stop 363
      dfill = dfull/dscli
      do 260 i = 1, nv
         iw2(i) = 0
         if(dabs(w(i)).lt.dfill) then
            iw(i) = idnint(dscli*w(i))
            if(abs(iw(i)).lt.mfull) then
               w(i) = dble(iw(i))/dscli
               go to 260
            endif
         endif
         if(dabs(w(i)).ge.dfull) stop 365
         iw(i) = idint(w(i))
         if(abs(iw(i)).ge.mfull) stop 370
         decml = (w(i) - dint(w(i)))*dscli
         if(dabs(decml).ge.dfull) stop 372
         iw2(i) = idnint(decml)
         if(abs(iw2(i)).ge.mfull) stop 375
         if(abs(iw2(i)).eq.0) then
            w(i) = dble(iw(i))
            iw2(i) = mfull
         else
            w(i) = dble(iw(i)) + (dble(iw2(i))/dscli)
         endif
  260 continue
      isclu = 1
      if(irsfig.eq.0) go to 290
      do 270 i = 1, irsfig
         isclu = 10*isclu
  270 continue
  290 continue
      if(abs(isclu).ge.mfull) stop 385
      call decomp(isclr, isgcl, isclu, mhalf)
      if(isgcl.ne.1) stop 390
  300 continue
!c
!c     get cube corner directions in their integer form
!c
      do 320 i = 1,8
         ixc(i) = idnint(xc(i))
         iyc(i) = idnint(yc(i))
         izc(i) = idnint(zc(i))
  320 continue
!c
!c     add all points to tetrahedralization
!c
      i10 = i9+1
      if(nv .lt. i10) go to 400
!c     write(*,*)' '
!c     write(*,*)'Adding all points to tetrahedralization ...'
      ITCHK = 0
      ITCN1 = 0
      ITCN2 = 0
      ITCN3 = 0
      ITCN4 = 0
      issin = i9
      do 380 k = i10, nv
         if(k.le.(k/1000)*1000)                                         &
     &   write(*,*)'Number of points processed = ',k
         if(is(k).eq.0) go to 380
         call pntins(x, y, z, w, ix, iy, iz, iw, ix2, iy2, iz2, iw2,    &
     &               icon, is, ifl, id, ih, ihn, k, nvmax, nhmax, tetra,&
     &               mxlook, ixc, iyc, izc, issin, iftal, delaun,       &
     &               flphis, mhalf, mfull, isclp, isclw, isclr, epz,    &
     &               ITCHK,ITCN1,ITCN2,ITCN3,ITCN4)
  380 continue
!c     WRITE(*,*)' '
!c     WRITE(*,*)'POINTINS ITCHK=',ITCHK
!c     WCPNT = REAL(ITCHK)/REAL(NV-9)
!c     WRITE(*,*)' '
!c     WRITE(*,*)'DURING INSERTION AVERAGE NUMBER OF ',
!c    *          'TETRAHEDRA CHECKED PER POINT WAS '
!c     WRITE(*,*) WCPNT
!c     WRITE(*,*)' '
!c     WRITE(*,*)'ITCN1234=',ITCN1,ITCN2,ITCN3,ITCN4
  400 continue
!c
!c     test redundant points
!c
      iredx = 0
      if(delaun) go to 1000
      if(nv.lt.10) go to 1000
      do 420 k = 9, nv
         if(is(k).le.0) go to 420
  420 continue
      if(redchk) then
!c        write(*,*)' '
!c        write(*,*)'Testing redundant points ...'
      endif
      ITCHK = 0
      ITCN1 = 0
      ITCN2 = 0
      ITCN3 = 0
      ITCN4 = 0
      iredu = 0
      do 500 k = 9, nv
         if(is(k).ge.0) go to 450
         iredu = iredu+1
         if(.not.redchk) go to 500
         call pntred(x, y, z, w, ix, iy, iz, iw, ix2, iy2, iz2, iw2,    &
     &               icon, is, id, k, tetra, ixc, iyc, izc,             &
     &               iredx, issin, iftal, delaun, flphis, mhalf,        &
     &               mfull, isclp, isclw, isclr, epz,                   &
     &               ITCHK,ITCN1,ITCN2,ITCN3,ITCN4)
  450    continue
         if(is(k).le.0) go to 500
         issin = k
  500 continue
!c     WRITE(*,*)' '
!c     WRITE(*,*)'POINTRED ITCHK=',ITCHK
!c     WRITE(*,*)' '
!c     WRITE(*,*)'ITCN1234=',ITCN1,ITCN2,ITCN3,ITCN4
!c     write(*,*)' '
!c     write(*,*)'Number of redundant points = ',iredu
!c
 1000 continue
      return
      end subroutine poltri

!=======================================================================

      subroutine pwrvtx(x, y, z, w, ix, iy, iz, iw, ix2, iy2, iz2, iw2, &
     &                  xp, yp, zp, icon, ifl, is, nv, nw, nt, nq, nmax,&
     &                  nvmax, wlenx, wleny, wlenz, wlenw, naddl, icfig,&
     &                  iwfig, delaun, artfcl, reccor)
!c
      real(8) :: x(:), y(:), z(:), w(:)
      real(8) :: xp(:), yp(:), zp(:)
      integer*8 ix(:), iy(:), iz(:), iw(:)
      integer*8 ix2(:), iy2(:), iz2(:), iw2(:)
      integer*8 icon(:,:), ifl(:), is(:)
      integer*8 nv, nw, nt, nq, nmax, nvmax, naddl, icfig, iwfig
      double precision dscle, wlenx, wleny, wlenz, wlenw
      logical delaun, artfcl, reccor
!c
      double precision xmin, ymin, zmin, wmin, xmax, ymax, zmax
      integer*8 isclp(2), isclw(2), isclr(2), mhalf, mfull


!c
!c     test for artificial points (not allowed)
!c
      if(artfcl) then
         write(*,*)' '
         write(*,*)'Input tetrahedra contain artificial points.'
         write(*,*)'Such points are not allowed in this program.'
         write(*,*)'Program terminated.'
         stop 1004
      endif
!c
!c     set up data structures properly
!c
      call setsup(x, y, z, w, ix, iy, iz, iw, ix2, iy2, iz2, iw2,       &
     &            xmin, ymin, zmin, wmin, xmax, ymax, zmax,             &
     &            nv, nw, nt, nmax, nvmax, dscle,                       &
     &            wlenx, wleny, wlenz, wlenw, naddl,                    &
     &            delaun, reccor, icfig, iwfig,                         &
     &            mhalf, mfull, isclp, isclw, isclr)
!c
!c     compute Power/Voronoi vertices
!c
!     vrtcmp generate {xp,yp,zp}
      call vrtcmp(ix, iy, iz, iw, ix2, iy2, iz2, iw2, xp, yp, zp,       &
     &            icon, ifl, is, nv, nt, nq, nvmax, dscle, delaun,      &
     &            mhalf, mfull, isclp, isclw, isclr)
!c
      return
      end  subroutine pwrvtx

!=======================================================================

      subroutine rdmord(wr, io, n, isu, jsu, ksu, nsu)
!c
      real(8) :: wr(:), rumi
      integer(8) :: io(:), n, isu, jsu, ksu, nsu, i
      integer*8 iu, ju, ku, nu
!c
!c     initialize
!c
!c     isu = 521288629
!c     jsu = 362436069
!c     ksu = 16163801
!c     nsu = 131199299
!c
      call mzrans(isu,jsu,ksu,nsu,iu,ju,ku,nu)
!c
!c     get numbers
!c
      do 10 i = 1, n
          call umi(rumi,iu,ju,ku,nu)
          wr(i) = rumi
          io(i) = i
   10 continue
!c
!c     sort in increasing order thus obtaining random order
!c     of integers from 1 to n
!c
!c     OPEN(21,FILE='umi.dat')
!c     WRITE(21,*)(WR(I),I=1,N)
!c
      call trsort(wr, io, n)
!c
      return
      end subroutine rdmord

!=======================================================================

      subroutine regtet(x, y, z, w, v, ix, iy, iz, iw, ix2, iy2, iz2,   &
     &                  iw2, icon, is, ifl, io, id, ih, nv, nw, nt, nd, &
     &                  nmax, nvmax, nhmax, wlenx, wleny, wlenz, wlenw, &
     &                  naddl, isu, jsu, ksu, nsu, icfig, iwfig, epz,   &
     &                  delaun, pntoff, flphis, artfcl, random, reccor, &
     &                  redchk)
!c
      integer*8 nmax, nvmax, nhmax
      double precision :: x(:), y(:), z(:), w(:)
      double precision :: v(:)
      integer(8) :: ix(:), iy(:), iz(:), iw(:)
      integer(8) :: ix2(:), iy2(:), iz2(:), iw2(:)
      integer(8) :: icon(:,:), is(:), ifl(:), io(:)
      integer(8) :: id(:), ih(:)
      integer(8) :: nv,nw,nt,nd,naddl, isu,jsu,ksu,nsu, icfig,iwfig
      double precision wlenx, wleny, wlenz, wlenw, epz
      logical delaun, pntoff, flphis, artfcl, random, reccor, redchk
!c
      integer*8 isclp(2), isclw(2), isclr(2), tetra, tetru
      double precision xmin, xmax, ymin, ymax, zmin, zmax, wmin, wmax
      double precision xpre, ypre, zpre, wpre, xnow, ynow, znow, wnow
      integer*8 mhalf, mfull, i, ihn, iftal, no, ipre, jpre, inow, jnow
      integer*8 irec, irec1, nv1, nu, mxlook, j, npre, ni
      integer*8 iredx, iconx, iorix, isphx
      integer*8 iredp, ired0, ired1, ired2, ired3, ired4
!c     integer*8 nva
!c
!c     initialize Fortran 77 word lengths
!c
      mhalf=mhalf_I !2**(16-1)
      mfull=mfull_I !2**(32-2)
!c
!c     testing parameters and number of input points or vertices
!c
      if (nv .lt. 1 .or. nv .gt. nmax .or. nvmax .lt. 12) then
       write(6,*) ' nv=',nv,' nmax=',nmax,' nvmax=',nvmax
       write(6,*) ' ERROR in regtet: nv<1 .or. nv>nmax .or. nvmax<12'
       stop 110
      end if
!c
!c     initialize arrays ih, w, is and id
!c
      do 50 i = 1, nhmax
         ih(i) = 0
   50 continue
      if(delaun)then
         do 60 i = 1, nmax
            w(i) = 0.0d0
   60    continue
      endif
      if(.not.pntoff)then
         do 80 i = 1, nmax
            is(i) = 1
   80    continue
      endif
      if(.not.flphis)then
         ihn = 0
         iftal = 0
         do 100 i = 1, nvmax
            id(i) = 0
  100    continue
      endif
!c
!c     test variables associated with a possible rectangular polyhedron
!c
      if(reccor)then
         if(wlenx.le.0.0d0 .or. wleny.le.0.0d0 .or. wlenz.le.0.0d0)     &
     &      stop 120
         if(naddl.lt.2) stop 130
      else
         wlenx = 0.0d0
         wleny = 0.0d0
         wlenz = 0.0d0
         wlenw = 0.0d0
         naddl = 0
      endif
!c
!c     calculating min and max
!c
      xmin = minval(x(1:nv))
      xmax = maxval(x(1:nv))
      ymin = minval(y(1:nv))
      ymax = maxval(y(1:nv))
      zmin = minval(z(1:nv))
      zmax = maxval(z(1:nv))
      wmin = minval(w(1:nv))
      wmax = maxval(w(1:nv))
!DELETE      xmax = x(1)
!DELETE      xmin = x(1)
!DELETE      ymax = y(1)
!DELETE      ymin = y(1)
!DELETE      zmax = z(1)
!DELETE      zmin = z(1)
!DELETE      wmax = w(1)
!DELETE      wmin = w(1)
!DELETE      do 150 no = 1, nv
!DELETE         if (x(no) .gt. xmax) xmax = x(no)
!DELETE         if (x(no) .lt. xmin) xmin = x(no)
!DELETE         if (y(no) .gt. ymax) ymax = y(no)
!DELETE         if (y(no) .lt. ymin) ymin = y(no)
!DELETE         if (z(no) .gt. zmax) zmax = z(no)
!DELETE         if (z(no) .lt. zmin) zmin = z(no)
!DELETE         if (w(no) .gt. wmax) wmax = w(no)
!DELETE         if (w(no) .lt. wmin) wmin = w(no)
!DELETE  150 continue
!c
!c     if randomizing input data then obtain random order of integers
!c     from 1 to nv and randomize data
!c
      if(.not.random) go to 185
      call rdmord(v, io, nv, isu, jsu, ksu, nsu)
      do 180 no = 1, nv
         if(io(no).lt.0) go to 180
         ipre = no
         xpre = x(ipre)
         ypre = y(ipre)
         zpre = z(ipre)
         wpre = w(ipre)
         jpre = is(ipre)
  170    continue
         inow = io(ipre)
         io(ipre) = -inow
         xnow = x(inow)
         ynow = y(inow)
         znow = z(inow)
         wnow = w(inow)
         jnow = is(inow)
         x(inow) = xpre
         y(inow) = ypre
         z(inow) = zpre
         w(inow) = wpre
         is(inow) = jpre
         if(inow .eq. no) go to 180
         ipre = inow
         xpre = xnow
         ypre = ynow
         zpre = znow
         wpre = wnow
         jpre = jnow
         if(io(ipre) .lt. 0) stop 140
         go to 170
  180 continue
!c
!c     OPEN(22,FILE='ran.dat')
!c     DO 182 I=1,NV
!c        WRITE(22,*)X(I),Y(I),Z(I),W(I)
!c 182 CONTINUE
!c
!c     shift data
!c
  185 continue
      irec = 8
      if(reccor) irec = irec + 6*(naddl**2) - 12*naddl + 8
      irec1 = irec + 1
      nv1 = nv
      nv = nv + irec
      if(nv .gt. nmax) stop 150
      do 190 no = nv, irec1, -1
         nu = no - irec
         x(no) = x(nu)
         y(no) = y(nu)
         z(no) = z(nu)
         w(no) = w(nu)
         is(no) = is(nu)
         if(random)then
            if(io(nu).ge.0) stop 160
            io(nu) = -io(nu) + irec
         endif
  190 continue
!c
!c     initialize is for additional data
!c
      do 200 i = 1, irec
         is(i) = 1
  200 continue
!c
!c     write(*,*)' '
!c     write(*,*)'Entering poltri ...'
      call poltri(x, y, z, w, ix, iy, iz, iw, ix2, iy2, iz2, iw2, icon, &
     &            is, ifl, id, ih, ihn, xmin, ymin, zmin, wmin, xmax,   &
     &            ymax, zmax, wmax, iftal, nv, nmax, nvmax, nhmax,      &
     &            wlenx, wleny, wlenz, wlenw, tetra, mxlook, irec,      &
     &            naddl, iredx, delaun, flphis, redchk, icfig, iwfig,   &
     &            mhalf, mfull, isclp, isclw, isclr, epz)
!c     write(*,*)' '
!c     write(*,*)'Checking tetrahedralization ...'
!c     WRITE(*,*)' '
!c     WRITE(*,*)'MAXLOOK=',MAXLOOK,' IHN=',IHN
!c     write (*,*)' '
!c     write (*,*)'Leaving poltri ...'
!c
!c     write(*,*)' '
!c     write(*,*)'Entering consis ...'
      call consis(icon, is, ifl, nv, tetra)
!c
!c     write(*,*)' '
!c     write(*,*)'Entering convex ...'
      call convex(icon, tetra, ifl, x, y, z, ix, iy, iz, ix2, iy2, iz2, &
     &            iconx, mhalf, mfull, isclp, epz)
!c
!c     write(*,*)' '
!c     write(*,*)'Entering orient ...'
      call orient(tetra, icon, ifl, x, y, z, ix, iy, iz, ix2, iy2, iz2, &
     &            iorix, mhalf, mfull, isclp, epz)
!c
!c     write(*,*)' '
!c     write(*,*)'Entering delchk ...'
      call delchk(tetra, icon, ifl, x, y, z, w, ix, iy, iz, iw, ix2,    &
     &            iy2, iz2, iw2, isphx, delaun, mhalf, mfull, isclp,    &
     &            isclw, isclr, epz)
!c
!c     checking for possible warnings
!c
      if(iredx.ne.0) then
         write(*,*)' '
         write(*,*)'Warning: redundancy violations detected'
         write(*,*)'Number of violations = ',iredx
      endif
      if(iconx.ne.0) then
         write(*,*)' '
         write(*,*)'Warning: convexity violations detected'
         write(*,*)'Number of violations = ',iconx
      endif
      if(iorix.ne.0) then
         write(*,*)' '
         write(*,*)'Warning: tetrahedra orientation violations detected'
         write(*,*)'Number of violations = ',iorix
      endif
      if(isphx.ne.0) then
         write(*,*)' '
         write(*,*)'Warning: sphere criterion violations detected'
         write(*,*)'Number of violations = ',isphx
      endif
      if(iredx.ne.0 .or. iconx.ne.0 .or. iorix.ne.0 .or. isphx.ne.0)then
         write(*,*)' '
         write(*,*)'Increasing tolerance epz could improve situation'
      endif
!c
!c     readjust data structure for randomizing
!c
      if(.not.random) go to 290
!c     write(*,*)' '
!c     write(*,*)'Readjusting data structure for randomizing ...'
      if(nv .gt. nvmax) stop 170
      nu = nv-irec
      do 230 no = 1, nu
         ifl(io(no)) = no + irec
  230 continue
      do 250 i = 1, tetra
         do 240 j = 5, 8
            if(icon(j,i).gt.irec) then
               icon(j,i) = ifl(icon(j,i))
            elseif(icon(j,i).lt.-irec) then
               icon(j,i) = -ifl(-icon(j,i))
            endif
  240    continue
  250 continue
      do 255 i = irec1, nv
         if(is(i).lt.-8) is(i) = -ifl(-is(i))
  255 continue
      do 260 i = irec1, nv
         ifl(i) = is(i)
  260 continue
      do 270 i = irec1, nv
         is(i) = ifl(io(i-irec))
  270 continue
      do 280 no = 1, nv1
         if(io(no).lt.0) go to 280
         nu = no + irec
         ipre = nu
         npre = no
         xpre = x(ipre)
         ypre = y(ipre)
         zpre = z(ipre)
         wpre = w(ipre)
  275    continue
         inow = io(npre)
         io(npre) = -inow
         if(inow .eq. nu) then
            x(ipre) = xpre
            y(ipre) = ypre
            z(ipre) = zpre
            w(ipre) = wpre
            go to 280
         endif
         x(ipre) = x(inow)
         y(ipre) = y(inow)
         z(ipre) = z(inow)
         w(ipre) = w(inow)
         ipre = inow
         npre = ipre - irec
         if(io(npre) .lt. 0) stop 180
         go to 275
  280 continue
!c
!c     write(*,*)' '
!c     write(*,*)'Entering consis ...'
      call consis(icon, is, ifl, nv, tetra)
!c
  290 continue
      nu=nv-8
      if(.not.artfcl) then
!c        write(*,*)' '
!c        write(*,*)'Entering revtet ...'
         call revtet(tetra, tetru, icon, nv, is, ifl, flphis)
         do 293 no = 1, nu
            x(no) = x(no+8)
            y(no) = y(no+8)
            z(no) = z(no+8)
            w(no) = w(no+8)
  293    continue
         if(tetru .eq. 0) go to 300
!c
!c        write(*,*)' '
!c        write(*,*)'Entering consis ...'
         call consis(icon, is, ifl, nu, tetru)
      elseif(.not.flphis) then
         call ruvtet(tetra, tetru, icon, is, ifl)
         call consis(icon, is, ifl, nv, tetra)
      else
!c
!c        count true tetrahedra
!c
         tetru = 0
         do 295 i = 1, tetra
            if ((icon(6,i) .le. 8) .or. (icon(7,i) .le. 8) .or.         &
     &      (icon(8,i) .le. 8) .or. (icon(5,i) .le. 8)) goto 295
            tetru = tetru + 1
  295    continue
      endif
!c
!c     count redundant vertices
!c
  300 continue
      nd = tetru
      iredp = 0
      ired0 = 0
      ired1 = 0
      ired2 = 0
      ired3 = 0
      ired4 = 0
      if(artfcl) then
         ni = 9
         nw = nv
         nt = tetra
      else
         ni = 1
         nw = nu
         nt = tetru
      endif
      do 400 i = ni, nw
         if(is(i) .gt. 0) then
            iredp = iredp + 1
         elseif(is(i) .eq.  0) then
            ired0 = ired0 + 1
         elseif(is(i) .lt. -8) then
            ired1 = ired1 + 1
         elseif(is(i) .eq. -2) then
            ired2 = ired2 + 1
         elseif(is(i) .eq. -3) then
            ired3 = ired3 + 1
         elseif(is(i) .eq. -4) then
            ired4 = ired4 + 1
         else
            stop 190
         endif
  400 continue
!c
!c     OPEN(23,FILE='unr.dat')
!c     DO 500 I=1,NW
!c        WRITE(23,*)X(I),Y(I),Z(I),W(I)
!c 500 CONTINUE
!c
!c     nva = nv
      nv = nv1
!c
!c     write info to screen
!c
!c     wtenv=float(tetra)/float(nva)
!c     wtena=float(tetra)/float(iredp)
!c     wtuna=float(tetru)/float(iredp)
!c     write (*,*) ' '
!c     write (*,*) 'Tetrahedralization data ...'
!c     write (*,*) ' '
!c     write (*,*) 'minimum weight = ',wmin
!c     write (*,*) 'maximum weight = ',wmax
!c     write (*,*) 'number of true vertices: ', nu
!c     write (*,*) 'number of active vertices: ',iredp
!c     write (*,*) 'maximum number of vertices parameter = ', nmax
!c     write (*,*) 'maximum number of tetrahed parameter = ', nvmax
!c     write (*,*) 'number of tetrahedra of all kinds: ', tetra
!c     write (*,*) 'all tetrahedra-all vertices ratio: ',wtenv
!c     write (*,*) 'number of true tetrahedra: ', tetru
!c     write (*,*) ' all tetrahedra-active vertices ratio: ',wtena
!c     write (*,*) 'true tetrahedra-active vertices ratio: ',wtuna
!c     write (*,*) 'max levels gone down in hierarchy = ', mxlook
!c     write (*,*) 'points active at the end of current run   = ',iredp
!c     write (*,*) 'points inactive at the end of current run = ',ired0
!c     write (*,*) 'points redundant by initial substitution  = ',ired1
!c     write (*,*) 'points redundant by eventual substitution = ',ired2
!c     write (*,*) 'points redundant by initial elimination   = ',ired3
!c     write (*,*) 'points redundant by eventual elimination  = ',ired4
!c
      return
      end subroutine regtet

!=======================================================================

      subroutine reordr(icon, site1, site2, iscur)
!c
      integer*8 icon(:,:), site1, site2, iscur, itemp
!c
      if(icon(5,iscur) .eq. site1) go to 200
      if(icon(6,iscur) .eq. site1) then
          itemp = icon(1,iscur)
          icon(1,iscur) = icon(2,iscur)
          icon(2,iscur) = icon(4,iscur)
          icon(4,iscur) = itemp
          itemp = icon(5,iscur)
          icon(5,iscur) = icon(6,iscur)
          icon(6,iscur) = icon(8,iscur)
          icon(8,iscur) = itemp
      elseif(icon(7,iscur) .eq. site1) then
          itemp = icon(1,iscur)
          icon(1,iscur) = icon(3,iscur)
          icon(3,iscur) = icon(2,iscur)
          icon(2,iscur) = itemp
          itemp = icon(5,iscur)
          icon(5,iscur) = icon(7,iscur)
          icon(7,iscur) = icon(6,iscur)
          icon(6,iscur) = itemp
      elseif(icon(8,iscur) .eq. site1) then
          itemp = icon(1,iscur)
          icon(1,iscur) = icon(4,iscur)
          icon(4,iscur) = icon(3,iscur)
          icon(3,iscur) = itemp
          itemp = icon(5,iscur)
          icon(5,iscur) = icon(8,iscur)
          icon(8,iscur) = icon(7,iscur)
          icon(7,iscur) = itemp
      else
          stop 2910
      endif
  200 continue
!c
      if(icon(6,iscur) .eq. site2) go to 300
      if(icon(7,iscur) .eq. site2) then
          itemp = icon(2,iscur)
          icon(2,iscur) = icon(3,iscur)
          icon(3,iscur) = icon(4,iscur)
          icon(4,iscur) = itemp
          itemp = icon(6,iscur)
          icon(6,iscur) = icon(7,iscur)
          icon(7,iscur) = icon(8,iscur)
          icon(8,iscur) = itemp
      elseif(icon(8,iscur) .eq. site2) then
          itemp = icon(2,iscur)
          icon(2,iscur) = icon(4,iscur)
          icon(4,iscur) = icon(3,iscur)
          icon(3,iscur) = itemp
          itemp = icon(6,iscur)
          icon(6,iscur) = icon(8,iscur)
          icon(8,iscur) = icon(7,iscur)
          icon(7,iscur) = itemp
      else
          stop 2920
      endif
  300 continue
!c
      return
      end subroutine reordr

!=======================================================================

      subroutine revtet(tetra, tetru, icon, nv, is, ifl, flphis)
!c
      integer*8 tetra, tetru, icon(:,:), is(:), ifl(:)
      integer*8 nv, i, j, ii, ielm
      logical flphis
!c
!c     identify true tetrahedra
!c
      tetru = 0
      ielm = 0
      do 100 i = 1, tetra
         if ((icon(5,i).le.0) .or. (icon(6,i).le.0) .or.                &
     &       (icon(7,i).le.0) .or. (icon(8,i).le.0)) then
            ielm = ielm + 1
            ifl(i) = 0
         elseif ((icon(5,i).le.8) .or. (icon(6,i).le.8) .or.            &
     &           (icon(7,i).le.8) .or. (icon(8,i).le.8)) then
            ifl(i) = 0
         else
            tetru=tetru+1
            ifl(i) = 1
         endif
  100 continue
      if(tetru.eq.0) go to 1000
!c
!c     zero out nonexistent tetrahedra in icon
!c
      do 300 i=1,tetra
         if(ifl(i).eq.0) go to 300
         do 200 j=1,4
            if(icon(j,i).le.0.or.icon(j,i).gt.tetra) stop 2710
            if(ifl(icon(j,i)).eq.0) icon(j,i)=0
  200    continue
  300 continue
!c
!c     compress icon
!c
      ii=0
      do 500 i=1,tetra
         if(ifl(i).eq.0) go to 500
         ii=ii+1
         ifl(i)=ii
         do 400 j=1,8
            icon(j,ii)=icon(j,i)
  400    continue
  500 continue
!c
!c     update icon for tetrahedra and is for vertices
!c
      do 550 i=9,nv
         if(is(i).gt.0)then
            is(i-8)=1
         else
            is(i-8)=is(i)
         endif
  550 continue
      do 800 i=1,tetru
         do 600 j=1,4
            if(icon(j,i).eq.0) go to 600
            icon(j,i)=ifl(icon(j,i))
  600    continue
         do 700 j=5,8
            icon(j,i)=icon(j,i)-8
            if(is(icon(j,i)).le.0) stop 2720
            is(icon(j,i))=i
  700    continue
  800 continue
!c
 1000 continue
      if(.not.flphis) tetra = tetra - ielm
!c
      return
      end subroutine revtet

!=======================================================================

      subroutine ruvtet(tetra, tetru, icon, is, ifl)
!c
      integer*8 tetra, tetru, icon(:,:), is(:), ifl(:)
      integer*8 i, j, ii, ielm
!c
!c     identify true tetrahedra
!c
      tetru = 0
      ielm = 0
      do 100 i = 1, tetra
         if ((icon(5,i).le.0) .or. (icon(6,i).le.0) .or.                &
     &       (icon(7,i).le.0) .or. (icon(8,i).le.0)) then
            ielm = ielm + 1
            ifl(i) = 0
         elseif ((icon(5,i).le.8) .or. (icon(6,i).le.8) .or.            &
     &           (icon(7,i).le.8) .or. (icon(8,i).le.8)) then
            ifl(i) = 1
         else
            tetru=tetru+1
            ifl(i) = 1
         endif
  100 continue
!c
!c     compress icon
!c
      ii=0
      do 500 i=1,tetra
         if(ifl(i).eq.0) go to 500
         ii=ii+1
         ifl(i)=ii
         do 400 j=1,8
            icon(j,ii)=icon(j,i)
  400    continue
  500 continue
!c
      tetra = tetra - ielm
!c
!c     update icon for tetrahedra and is for vertices
!c
      do 800 i=1,tetra
         do 600 j=1,4
            if(icon(j,i).eq.0) go to 600
            icon(j,i)=ifl(icon(j,i))
  600    continue
         do 700 j=5,8
            if(is(icon(j,i)).le.0) stop 2730
            is(icon(j,i))=i
  700    continue
  800 continue
!c
      return
      end subroutine ruvtet

!=======================================================================

      subroutine setsup(x, y, z, w, ix, iy, iz, iw, ix2, iy2, iz2, iw2, &
     &                  xmin, ymin, zmin, wmin, xmax, ymax, zmax,       &
     &                  nv, nw, nt,  nmax, nvmax, dscle,                &
     &                  wlenx, wleny, wlenz, wlenw, naddl,              &
     &                  delaun, reccor, icsfig, iwsfig,                 &
     &                  mhalf, mfull, isclp, isclw, isclr)
!c
      real(8) :: x(:), y(:), z(:), w(:)
      integer*8 ix(:), iy(:), iz(:), iw(:)
      integer*8 ix2(:), iy2(:), iz2(:), iw2(:)
      double precision xmin, ymin, zmin, wmin, xmax, ymax, zmax
      integer*8 nv, nw, nt, nmax, nvmax, naddl
      double precision dscle, wlenx, wleny, wlenz, wlenw
      integer*8 icsfig, iwsfig, mhalf, mfull
      logical delaun, reccor
      integer*8 isclp(:), isclw(:), isclr(:)
!c
      double precision dscli, dfull, dfill, decml
      integer*8 no, irec, irec1, nu, ng, i, naddm, j, icsfi2, irsfig
      integer*8 isgcl, isclu, ibsfig, itsfig
      double precision xint, yint, zint, xcor, ycor, zcor
      double precision wmen, wbig, epz, wmax

!c
!c     initialize Fortran 77 word lengths
!c
      mhalf=mhalf_I
      mfull=mfull_I
!c
!c     testing number of points or vertices
!c
      if (nv.lt.1 .or. nv.gt.nmax) stop 110
      if (nt.lt.1 .or. nt.gt.nvmax) stop 120
!c
!c     test variables associated with a possible rectangular polyhedron
!c
      if(reccor)then
         if(wlenx.le.0.0d0 .or. wleny.le.0.0d0 .or. wlenz.le.0.0d0)     &
     &      stop 130
         if(naddl.lt.2) stop 140
      else
         wlenx = 0.0d0
         wleny = 0.0d0
         wlenz = 0.0d0
         wlenw = 0.0d0
         naddl = 0
      endif
!c
!c     calculating min and max
!c
      xmax = x(1)
      xmin = x(1)
      ymax = y(1)
      ymin = y(1)
      zmax = z(1)
      zmin = z(1)
      wmax = w(1)
      wmin = w(1)
      do 15 no = 1, nv
         if (x(no) .gt. xmax) xmax = x(no)
         if (x(no) .lt. xmin) xmin = x(no)
         if (y(no) .gt. ymax) ymax = y(no)
         if (y(no) .lt. ymin) ymin = y(no)
         if (z(no) .gt. zmax) zmax = z(no)
         if (z(no) .lt. zmin) zmin = z(no)
         if (w(no) .gt. wmax) wmax = w(no)
         if (w(no) .lt. wmin) wmin = w(no)
   15 continue
!c
!c     shift data
!c
      irec = 0
      irec1 = irec+1
      if(reccor) irec = irec + 6*(naddl**2) - 12*naddl + 8
      if(irec.eq.0) go to 25
      irec1 = irec + 1
      nv = nv + irec
      if(nv .gt. nmax) stop 150
      do 20 no = nv, irec1, -1
         nu = no - irec
         x(no) = x(nu)
         y(no) = y(nu)
         z(no) = z(nu)
         w(no) = w(nu)
   20 continue
   25 continue
      if(nv.ne.nw) stop 160
!c
!c     compute corners of rectangular polyhedron
!c
      if(.not.reccor) go to 165
      ng = 0
      x(ng+1) = xmin - wlenx
      y(ng+1) = ymin - wleny
      z(ng+1) = zmax + wlenz
!c
      x(ng+2) = xmin - wlenx
      y(ng+2) = ymax + wleny
      z(ng+2) = zmax + wlenz
!c
      x(ng+3) = xmax + wlenx
      y(ng+3) = ymax + wleny
      z(ng+3) = zmax + wlenz
!c
      x(ng+4) = xmax + wlenx
      y(ng+4) = ymin - wleny
      z(ng+4) = zmax + wlenz
!c
      x(ng+5) = xmin - wlenx
      y(ng+5) = ymin - wleny
      z(ng+5) = zmin - wlenz
!c
      x(ng+6) = xmin - wlenx
      y(ng+6) = ymax + wleny
      z(ng+6) = zmin - wlenz
!c
      x(ng+7) = xmax + wlenx
      y(ng+7) = ymax + wleny
      z(ng+7) = zmin - wlenz
!c
      x(ng+8) = xmax + wlenx
      y(ng+8) = ymin - wleny
      z(ng+8) = zmin - wlenz
!c
      do 60 i=ng+1,ng+8
         if((x(i).ge.xmin.and.x(i).le.xmax).or.(y(i).ge.ymin.and.       &
     &       y(i).le.ymax).or.(z(i).ge.zmin.and.z(i).le.zmax)) stop 260
   60 continue
!c
      xmin = xmin - wlenx
      ymin = ymin - wleny
      zmin = zmin - wlenz
      xmax = xmax + wlenx
      ymax = ymax + wleny
      zmax = zmax + wlenz
!c
      if(naddl.eq.2) go to 155
!c
!c     compute other points in grid on surface of polyhedron
!c
      naddm = naddl-2
      xint = (xmax-xmin)/dble(naddl-1)
      yint = (ymax-ymin)/dble(naddl-1)
      zint = (zmax-zmin)/dble(naddl-1)
      ng = ng+8
!c
      do 119 i = 1, naddm
         xcor = xmin + dble(i)*xint
         ng = ng + 4
         x(ng-3) = xcor
         y(ng-3) = ymin
         z(ng-3) = zmin
         x(ng-2) = xcor
         y(ng-2) = ymin
         z(ng-2) = zmax
         x(ng-1) = xcor
         y(ng-1) = ymax
         z(ng-1) = zmin
         x(ng) = xcor
         y(ng) = ymax
         z(ng) = zmax
  119 continue
!c
      do 120 i = 1, naddm
         ycor = ymin + dble(i)*yint
         ng = ng + 4
         y(ng-3) = ycor
         z(ng-3) = zmin
         x(ng-3) = xmin
         y(ng-2) = ycor
         z(ng-2) = zmin
         x(ng-2) = xmax
         y(ng-1) = ycor
         z(ng-1) = zmax
         x(ng-1) = xmin
         y(ng) = ycor
         z(ng) = zmax
         x(ng) = xmax
  120 continue
!c
      do 121 i = 1, naddm
         zcor = zmin + dble(i)*zint
         ng = ng + 4
         z(ng-3) = zcor
         x(ng-3) = xmin
         y(ng-3) = ymin
         z(ng-2) = zcor
         x(ng-2) = xmin
         y(ng-2) = ymax
         z(ng-1) = zcor
         x(ng-1) = xmax
         y(ng-1) = ymin
         z(ng) = zcor
         x(ng) = xmax
         y(ng) = ymax
  121 continue
!c
      do 130 i = 1, naddm
         xcor = xmin + dble(i)*xint
         do 125 j = 1, naddm
            ycor = ymin + dble(j)*yint
            ng = ng + 2
            x(ng-1) = xcor
            y(ng-1) = ycor
            z(ng-1) = zmin
            x(ng) = xcor
            y(ng) = ycor
            z(ng) = zmax
  125    continue
  130 continue
!c
      do 140 i = 1, naddm
         ycor = ymin + dble(i)*yint
         do 135 j = 1, naddm
            zcor = zmin + dble(j)*zint
            ng = ng + 2
            y(ng-1) = ycor
            z(ng-1) = zcor
            x(ng-1) = xmin
            y(ng) = ycor
            z(ng) = zcor
            x(ng) = xmax
  135    continue
  140 continue
!c
      do 150 i = 1, naddm
         zcor = zmin + dble(i)*zint
         do 145 j = 1, naddm
            xcor = xmin + dble(j)*xint
            ng = ng + 2
            z(ng-1) = zcor
            x(ng-1) = xcor
            y(ng-1) = ymin
            z(ng) = zcor
            x(ng) = xcor
            y(ng) = ymax
  145    continue
  150 continue
!c
      if(ng.ne.irec) stop 280
!c
  155 continue
      wmen = wmin
      if(.not.delaun .and. reccor) then
         wmen = wmen - wlenw
         do 160 i=irec1,irec
            w(i) = wmen
  160    continue
      endif
      if(wmen.lt.wmin) wmin = wmen
      if(wmen.gt.wmax) wmax = wmen
!c
!c     test # of significant figures of nondecimal part of coordinates
!c
  165 continue
!c     write(*,*)' '
!c     write(*,*)'min and max of points coordinates:'
!c     write(*,*)' '
!c     write(*,*)'xmin = ',xmin
!c     write(*,*)'xmax = ',xmax
!c     write(*,*)'ymin = ',ymin
!c     write(*,*)'ymax = ',ymax
!c     write(*,*)'zmin = ',zmin
!c     write(*,*)'zmax = ',zmax
      epz = 0.01d0
      wbig = 0.0d0
      if(wbig .lt. dabs(xmax)) wbig = dabs(xmax)
      if(wbig .lt. dabs(xmin)) wbig = dabs(xmin)
      if(wbig .lt. dabs(ymax)) wbig = dabs(ymax)
      if(wbig .lt. dabs(ymin)) wbig = dabs(ymin)
      if(wbig .lt. dabs(zmax)) wbig = dabs(zmax)
      if(wbig .lt. dabs(zmin)) wbig = dabs(zmin)
      wbig = wbig + epz
!c     WRITE(*,*)'COORDINATES WBIG=',WBIG
      ibsfig = 0
  180 continue
      ibsfig = ibsfig+1
      wbig = wbig/10.0d0
      if(wbig .ge. 1.0d0) go to 180
      if(ibsfig.gt.9) then
         write(*,*)'Number of significant figures of largest ',         &
     &             'nondecimal part of'
         write(*,*)'a point coordinate appears to be greater than 9.'
         write(*,*)'Program is terminated.'
         stop 282
      endif
      itsfig = ibsfig + icsfig
!c     WRITE(*,*)'ITSFIG=',ITSFIG,' IBSFIG=',IBSFIG,' ICSFIG=',ICSFIG
      if(itsfig.gt.14) then
         write(*,*)' '
         write(*,*)'For this execution of the program the largest ',    &
     &             'total number of'
         write(*,*)'significant figures ',                              &
     &             'that a coordinate requires appears to be ',itsfig
         write(*,*)'Program is terminated since the maximum ',          &
     &             'allowed is 14.'
         stop 284
      endif
!c
!c     if a Regular tetrahedralization test # of significant figures
!c     of nondecimal part of weights
!c
      if(delaun) go to 200
      wbig = 0.0d0
      if(wbig .lt. dabs(wmax)) wbig = dabs(wmax)
      if(wbig .lt. dabs(wmin)) wbig = dabs(wmin)
      wbig = wbig + epz
!c     WRITE(*,*)'COORDINATES WBIG=',WBIG
      ibsfig = 0
  190 continue
      ibsfig = ibsfig+1
      wbig = wbig/10.0d0
      if(wbig .ge. 1.0d0) go to 190
      if(ibsfig.gt.9) then
         write(*,*)'Number of significant figures of largest ',         &
     &             'nondecimal part of'
         write(*,*)'a weight appears to be greater than 9.'
         write(*,*)'Program is terminated.'
         stop 286
      endif
      itsfig = ibsfig + iwsfig
!c     WRITE(*,*)'ITSFIG=',ITSFIG,' IBSFIG=',IBSFIG,' IWSFIG=',IWSFIG
      if(itsfig.gt.14) then
         write(*,*)' '
         write(*,*)'For this execution of the program the largest ',    &
     &             'total number of'
         write(*,*)'significant figures ',                              &
     &             'that a weight requires appears to be ',itsfig
         write(*,*)'Program is terminated since the maximum ',          &
     &             'allowed is 14.'
         stop 288
      endif
  200 continue
!c
!c     test number of significant figures of decimal part of coordinates
!c
      if(icsfig.lt.0 .or. icsfig.gt.9) stop 290
      isclu = 1
      dscle = 1.0d0
      if(icsfig.eq.0) go to 220
      do 210 i = 1, icsfig
         isclu = 10*isclu
         dscle = 10.0d0*dscle
  210 continue
  220 continue
      if(abs(isclu).ge.mfull) stop 295
      call decomp(isclp, isgcl, isclu, mhalf)
      if(isgcl.ne.1) stop 297
!c
!c     test lengths of x, y, z-coordinates, shift and make them integers
!c
      dfull = dble(mfull)
      dfill=dfull/dscle
      do 235 i = 1, nv !MAIN route
         ix2(i) = 0
         iy2(i) = 0
         iz2(i) = 0
         if(dabs(x(i)).lt.dfill) then
            ix(i) = idnint(dscle*x(i))
            if(abs(ix(i)).lt.mfull) then
               x(i) = dble(ix(i))/dscle !there should be no change in x(i)
               go to 225 !finish for x, jump!
            endif
         endif
         if(dabs(x(i)).ge.dfull) stop 305
         ix(i) = idint(x(i))
         if(abs(ix(i)).ge.mfull) stop 310
      !  statement 'stop 305' & 'stop 310' are the same
      !  'cause if x(i) not >= dfull
      !  then ix(i) will never >= mfull
         decml = (x(i) - dint(x(i)))*dscle
         if(dabs(decml).ge.dfull) stop 312
      !  at normal condition, statement 'stop 312' will never happen
      !  eg: the largest number for x is 1,073,741,823.999 999 999
      !      so the largest decml will be--> 999,999,999 < dfull
      !  ps: for case-->icsfig=9
         ix2(i) = idnint(decml)
         if(abs(ix2(i)).ge.mfull) stop 315 !never happen, same reason for 312
         if(abs(ix2(i)).eq.0) then !mean x doesn't have decimal part
            x(i) = dble(ix(i))
            ix2(i) = mfull
      !  in sub. decomp2 will turn it back to 0 again...
      !  they doing this because they use iwi2=0 or not to distinguish
      !  whether a number large than dfill or not!
         else !means x does have decimal part
            x(i) = dble(ix(i)) + (dble(ix2(i))/dscle)
             !this truncate the significant figures.....
            !after this point, we lose precision!
         endif
  225    continue
         if(dabs(y(i)).lt.dfill) then
            iy(i) = idnint(dscle*y(i))
            if(abs(iy(i)).lt.mfull) then
               y(i) = dble(iy(i))/dscle
               go to 230
            endif
         endif
         if(dabs(y(i)).ge.dfull) stop 320
         iy(i) = idint(y(i))
         if(abs(iy(i)).ge.mfull) stop 325
         decml = (y(i) - dint(y(i)))*dscle
         if(dabs(decml).ge.dfull) stop 327
         iy2(i) = idnint(decml)
         if(abs(iy2(i)).ge.mfull) stop 330
         if(abs(iy2(i)).eq.0) then
            y(i) = dble(iy(i))
            iy2(i) = mfull
         else
            y(i) = dble(iy(i)) + (dble(iy2(i))/dscle)
         endif
  230    continue
         if(dabs(z(i)).lt.dfill) then
            iz(i) = idnint(dscle*z(i))
            if(abs(iz(i)).lt.mfull) then
               z(i) = dble(iz(i))/dscle
               go to 235
            endif
         endif
         if(dabs(z(i)).ge.dfull) stop 335
         iz(i) = idint(z(i))
         if(abs(iz(i)).ge.mfull) stop 340
         decml = (z(i) - dint(z(i)))*dscle
         if(dabs(decml).ge.dfull) stop 342
         iz2(i) = idnint(decml)
         if(abs(iz2(i)).ge.mfull) stop 345
         if(abs(iz2(i)).eq.0) then
            z(i) = dble(iz(i))
            iz2(i) = mfull
         else
            z(i) = dble(iz(i)) + (dble(iz2(i))/dscle)
         endif
  235 continue
!c
!c     if a Regular tetrahedralization test number of significant figures
!c     of decimal part of weights, test lengths of weights, shift and
!c     make them integers
!c
      if(delaun) go to 300
      icsfi2 = 2*icsfig
      irsfig = icsfi2 - iwsfig
      if(iwsfig.lt.0 .or. iwsfig.gt.9 .or. irsfig.lt.0 .or. irsfig.gt.9)&
     &   stop 350
      isclu = 1
      dscli = 1.0d0
      if(iwsfig.eq.0) go to 250
      do 240 i = 1, iwsfig
         isclu = 10*isclu
         dscli = 10.0d0*dscli
  240 continue
  250 continue
      if(abs(isclu).ge.mfull) stop 360
      call decomp(isclw, isgcl, isclu, mhalf)
      if(isgcl.ne.1) stop 363
      dfill = dfull/dscli
      do 260 i = 1, nv
         iw2(i) = 0
         if(dabs(w(i)).lt.dfill) then
            iw(i) = idnint(dscli*w(i))
            if(abs(iw(i)).lt.mfull) then
               w(i) = dble(iw(i))/dscli
               go to 260
            endif
         endif
         if(dabs(w(i)).ge.dfull) stop 365
         iw(i) = idint(w(i))
         if(abs(iw(i)).ge.mfull) stop 370
         decml = (w(i) - dint(w(i)))*dscli
         if(dabs(decml).ge.dfull) stop 372
         iw2(i) = idnint(decml)
         if(abs(iw2(i)).ge.mfull) stop 375
         if(abs(iw2(i)).eq.0) then
            w(i) = dble(iw(i))
            iw2(i) = mfull
         else
            w(i) = dble(iw(i)) + (dble(iw2(i))/dscli)
         endif
  260 continue
      isclu = 1
      if(irsfig.eq.0) go to 290
      do 270 i = 1, irsfig
         isclu = 10*isclu
  270 continue
  290 continue
      if(abs(isclu).ge.mfull) stop 385
      call decomp(isclr, isgcl, isclu, mhalf)
      if(isgcl.ne.1) stop 390
  300 continue
!c
      return
      end subroutine setsup

!=======================================================================

      subroutine shishk(xi, yi, zi, x, y, z, x2, y2, z2, is, icon, id,  &
     &                  ileft, k, side1, side2, iscur, iftal, itype,    &
     &                  ivnxt, xc, yc, zc, mhalf, mfull, isclp, epz,    &
     &                  ITCHK)
!c
      real(8) :: xi(:), yi(:), zi(:)
      integer*8 x(:), y(:), z(:)
      integer*8 x2(:), y2(:), z2(:)
      integer*8 is(:), icon(:,:), id(:)
      double precision epz
      integer*8 xc(:), yc(:), zc(:)
      integer*8 isclp(:), ileft, k, iftal, itype, ivnxt, mhalf, mfull
      integer*8 a, b, c, d, side1, side2, site0, site1
      integer*8 iscur, isini, imist, isadj, ilift, islst, itchk, i
!c
!c     reinitialize array id if necessary
!c
      if(iftal.gt.10000000) then
         iftal = 0
         do 50 i = 1, ivnxt
            id(i) = 0
   50    continue
      endif
!c
      if(ileft .le. 8) stop 911
      a = ileft
  100 continue
!c
!c     find tetrahedron with point a as a vertex for which the ray with
!c     origin point a and through point k intersects the facet of the
!c     tetrahedron opposite to point a
!c
      itype = 0
      iftal = iftal + 1
      iscur = is(a)
      if(iscur.le.0.or.iscur.gt.ivnxt) stop 912
      isini = iscur
!c
!c     reorder isini so that vertex a equals icon(5,isini)
!c
      call sitord(icon, a, isini)
!c
!c     test current facet
!c
  400 continue
      b = icon(6,iscur)
      c = icon(7,iscur)
      d = icon(8,iscur)
      id(iscur) = iftal
!c
      ITCHK = ITCHK+1
      call fctest(xi, yi, zi, x, y, z, x2, y2, z2, xc, yc, zc, itype, k,&
     &            imist, a, b, c, d, side1, side2, mhalf, mfull, isclp, &
     &            epz)
      if(itype .gt. 0) go to 9000
      if(itype .eq. 0) go to 500
      if(itype .eq. -2) go to 1100
      if(itype .eq. -3) then
         a = imist
         go to 100
      elseif(itype .eq. -4) then
         site0 = a
         site1 = imist
         go to 2000
      else
         stop 913
      endif
!c
!c     obtain next tetrahedron with point a as a vertex
!c
  500 continue
      isadj = abs(icon(2,iscur))
      if(isadj.le.0.or.isadj.gt.ivnxt) stop 914
      if(id(isadj) .eq. iftal) go to 600
      ilift = icon(8,iscur)
      go to 900
  600 continue
      isadj = abs(icon(3,iscur))
      if(isadj.le.0.or.isadj.gt.ivnxt) stop 915
      if(id(isadj) .eq. iftal) go to 700
      ilift = icon(6,iscur)
      go to 900
  700 continue
      isadj = abs(icon(4,iscur))
      if(isadj.le.0.or.isadj.gt.ivnxt) stop 916
      if(iscur .eq. isini) go to 800
      if(abs(icon(3,isadj)) .eq. iscur) then
          iscur = isadj
          go to 700
      elseif(abs(icon(2,isadj)) .eq. iscur) then
          iscur = isadj
          go to 600
      elseif(abs(icon(4,isadj)) .eq. iscur) then
          if(isadj .ne. isini) stop 917
          go to 1000
      else
          stop 918
      endif
  800 continue
      if(id(isadj) .eq. iftal) go to 1000
      ilift = icon(7,iscur)
!c
!c     reorder isadj so that a equals icon(5,isadj) and ilift
!c     equals icon(6,isadj)
!c
  900 continue
      call reordr(icon, a, ilift, isadj)
      iscur = isadj
      go to 400
!c
!c     can not find intersected tetrahedron
!c
 1000 continue
      stop 919
!c
!c     obtain next tetrahedron along line segment as it crosses a facet
!c
 1100 continue
      islst = iscur
      isadj = abs(icon(1,iscur))
      if(isadj.le.0.or.isadj.gt.ivnxt) stop 921
      iscur = isadj
      if(abs(icon(1,iscur)) .eq. islst) then
          ilift = icon(5,iscur)
      elseif(abs(icon(2,iscur)) .eq. islst) then
          ilift = icon(6,iscur)
      elseif(abs(icon(3,iscur)) .eq. islst) then
          ilift = icon(7,iscur)
      elseif(abs(icon(4,iscur)) .eq. islst) then
          ilift = icon(8,iscur)
      else
          stop 922
      endif
!c
!c     obtain opposite facet of tetrahedron intersected by line
!c     segment
!c
      ITCHK = ITCHK+1
      call fcfind(xi, yi, zi, x, y, z, x2, y2, z2, xc, yc, zc, itype,   &
     &        ileft, k, ilift, imist, b, c, d, side1, side2, mhalf,     &
     &        mfull, isclp, epz)
      if(itype .gt. 0) then
         call reordr(icon, ilift, b, iscur)
         go to 9000
      elseif(itype .eq. -2) then
         call sitord(icon, imist, iscur)
         b = icon(6,iscur)
         c = icon(7,iscur)
         d = icon(8,iscur)
         go to 1100
      elseif(itype .eq. -3) then
         a = ilift
         go to 100
      elseif(itype .eq. -4) then
         if(imist.eq.b)then
            site0 = c
         elseif(imist.eq.c)then
            site0 = d
         else
            site0 = b
         endif
         site1 = imist
         go to 2000
      else
         stop 923
      endif
!c
!c     obtain next tetrahedron along line segment as it crosses an edge
!c
 2000 continue
      call fcedge(x, y, z, x2, y2, z2, xc, yc, zc, itype, ileft, k,     &
     &            icon, iscur, imist, ivnxt, site0, site1, side1,       &
     &            side2, mhalf, mfull, isclp, ITCHK)
      if(itype .gt. 0) go to 9000
      if(itype .eq. -2) then
         call sitord(icon, imist, iscur)
         b = icon(6,iscur)
         c = icon(7,iscur)
         d = icon(8,iscur)
         go to 1100
      elseif(itype .eq. -3) then
         a = imist
         go to 100
      elseif(itype.eq.-4) then
         if(imist.eq.site1)then
            site0 = icon(7,iscur)
         elseif(imist.eq.site0)then
            site0 = site1
         endif
         site1 = imist
         go to 2000
      else
         stop 924
      endif
!c
 9000 continue
      return
      end subroutine shishk

!=======================================================================

      subroutine sidins(k, x, y, z, w, x2, y2, z2, w2, icon, ih, ihn,   &
     &                   nhmax, nvmax, tetra, curr, is, side, ifl,      &
     &                   newtts, ired, delaun, flphis, mhalf, mfull,    &
     &                   isclp, isclw, isclr)
!c
      integer*8 x(:), y(:), z(:), w(:), x2(:), y2(:), z2(:), w2(:)
      integer*8 icon(:,:), is(:), ifl(:), ih(:)
      integer*8 tetra, curr, adj, side, site1, site2, site3, site4, temp
      integer*8 ihn, nhmax, nvmax, newtts, ired, mhalf, mfull
      integer*8 isclp(:), isclw(:), isclr(:), newi(6)
      logical delaun, flphis
      integer*8 k, itide, new1, new2, new3, new4, new5, new6, i, j
!c
      adj = icon(side, curr)
      if (adj .eq. 0) stop 1310
!c
!c     rearrange curr
!c
      site1 = icon(side+4,curr)
      call sitord(icon, site1, curr)
!c
      site2 = icon(6,curr)
      site3 = icon(7,curr)
      site4 = icon(8,curr)
!c
      if(delaun) go to 30
      if(site2.le.8 .or. site3.le.8 .or. site4.le.8) go to 30
      call iqsig2(x, y, z, w, x2, y2, z2, w2, site2, site3, site4,      &
     &            k, mhalf, mfull, isclp, isclw, isclr, delaun, itide)
      if(itide .le. 0) go to 30
      is(k) = -3
      ired = 1
      go to 1000
!c
   30 continue
      ired = 0
!c
      if(flphis) then
         new1 = tetra + 1
         new2 = tetra + 2
         new3 = tetra + 3
         new4 = tetra + 4
         new5 = tetra + 5
         new6 = tetra + 6
         tetra = new6
         if(tetra .gt. nvmax) stop 1320
      else
         if(ihn.eq.0) then
            new1 = tetra+1
            tetra = new1
            if(tetra .gt. nvmax) stop 1330
         else
            new1 = ih(ihn)
            ihn = ihn-1
         endif
         if(ihn.eq.0) then
            new2 = tetra+1
            tetra = new2
            if(tetra .gt. nvmax) stop 1340
         else
            new2 = ih(ihn)
            ihn = ihn-1
         endif
         if(ihn.eq.0) then
            new3 = tetra+1
            tetra = new3
            if(tetra .gt. nvmax) stop 1350
         else
            new3 = ih(ihn)
            ihn = ihn-1
         endif
         if(ihn.eq.0) then
            new4 = tetra+1
            tetra = new4
            if(tetra .gt. nvmax) stop 1360
         else
            new4 = ih(ihn)
            ihn = ihn-1
         endif
         if(ihn.eq.0) then
            new5 = tetra+1
            tetra = new5
            if(tetra .gt. nvmax) stop 1370
         else
            new5 = ih(ihn)
            ihn = ihn-1
         endif
         if(ihn.eq.0) then
            new6 = tetra+1
            tetra = new6
            if(tetra .gt. nvmax) stop 1380
         else
            new6 = ih(ihn)
            ihn = ihn-1
         endif
      endif
!c
      ifl(1) = new1
      ifl(2) = new2
      ifl(3) = new3
      ifl(4) = new4
      ifl(5) = new5
      ifl(6) = new6
!c
      newtts = 6
!c
!c     create new1, new2, new3
!c
      do 50 i = 1, 8
         icon(i,new1) = icon(i,curr)
         icon(i,new2) = icon(i,curr)
         icon(i,new3) = icon(i,curr)
   50 continue
!c
      icon(1,new1) = new4
      icon(3,new1) = new2
      icon(4,new1) = new3
      icon(6,new1) = k
      call sitord (icon, k, new1)
!c
      icon(1,new2) = new6
      icon(2,new2) = new1
      icon(4,new2) = new3
      icon(7,new2) = k
      call sitord (icon, k, new2)
!c
      icon(1,new3) = new5
      icon(2,new3) = new1
      icon(3,new3) = new2
      icon(8,new3) = k
      call sitord (icon, k, new3)
!c
!c     update is(:)
!c
      is(k) = new1
!c
      is(site1) = new1
      is(site2) = new2
      is(site3) = new1
      is(site4) = new1
!c
!c     update curr's neighbors
!c
      newi(1) = new1
      newi(2) = new2
      newi(3) = new3
      do 200 i = 2, 4
         temp = icon(i,curr)
         if (temp .ne. 0) then
            do 150 j = 1, 4
               if (icon(j,temp) .eq. curr) then
                  icon(j,temp) = newi(i-1)
                  goto 200
               endif
  150       continue
            stop 1390
         endif
  200 continue
!c
!c     flag curr to show its children
!c
      if(flphis) then
         icon(5,curr) = - icon(5,curr)
         icon(1,curr) = new1
         icon(2,curr) = new2
         icon(3,curr) = new3
         icon(4,curr) = -adj
      else
         icon(5,curr) = - icon(5,curr)
         ihn = ihn+1
         if(ihn.gt.nhmax) stop 1410
         ih(ihn) = curr
      endif
!c
!c     update 2nd tet (adj)
!c
      do 300 i = 1, 4
         if (icon(i,adj) .eq. curr) then
            site1 = icon(i+4,adj)
            goto 325
         endif
  300 continue
      stop 1420
  325 continue
!c
      call reordr (icon, site1, site2, adj)
!c
!c     create new4, new5, new6
!c
      do 350 i = 1, 8
         icon(i,new4) = icon(i,adj)
         icon(i,new5) = icon(i,adj)
         icon(i,new6) = icon(i,adj)
  350 continue
!c
      icon(1,new4) = new1
      icon(3,new4) = new5
      icon(4,new4) = new6
      icon(6,new4) = k
      call sitord (icon, k, new4)
!c
      icon(1,new5) = new3
      icon(2,new5) = new4
      icon(4,new5) = new6
      icon(7,new5) = k
      call sitord (icon, k, new5)
!c
      icon(1,new6) = new2
      icon(2,new6) = new4
      icon(3,new6) = new5
      icon(8,new6) = k
      call sitord (icon, k, new6)
!c
!c     update is(:)
!c
      is(site1) = new4
!c
!c     update adj's neighbors
!c
      newi(4) = new4
      newi(5) = new5
      newi(6) = new6
      do 500 i = 2, 4
         temp = icon(i,adj)
         if (temp .ne. 0) then
            do 450 j = 1, 4
               if (icon(j,temp) .eq. adj) then
                  icon(j,temp) = newi(i+2)
                  goto 500
               endif
  450       continue
            stop 1430
         endif
  500 continue
!c
!c     flag adj to show its children
!c
      if(flphis) then
         icon(5,adj) = - icon(5,adj)
         icon(1,adj) = new4
         icon(2,adj) = new5
         icon(3,adj) = new6
         icon(4,adj) = -curr
      else
         icon(5,adj) = - icon(5,adj)
         ihn = ihn+1
         if(ihn.gt.nhmax) stop 1440
         ih(ihn) = adj
      endif
!c
 1000 continue
      return
      end subroutine sidins

!=======================================================================

      subroutine site1ordr(icon, site1, iscur)
!c
      integer*8 icon(:,:), site1, iscur, itemp
!c
      if(icon(5,iscur) .eq. site1) go to 200
      if(icon(6,iscur) .eq. site1) then
          itemp = icon(1,iscur)
          icon(1,iscur) = icon(2,iscur)
          icon(2,iscur) = icon(4,iscur)
          icon(4,iscur) = itemp
          itemp = icon(5,iscur)
          icon(5,iscur) = icon(6,iscur)
          icon(6,iscur) = icon(8,iscur)
          icon(8,iscur) = itemp
      elseif(icon(7,iscur) .eq. site1) then
          itemp = icon(1,iscur)
          icon(1,iscur) = icon(3,iscur)
          icon(3,iscur) = icon(2,iscur)
          icon(2,iscur) = itemp
          itemp = icon(5,iscur)
          icon(5,iscur) = icon(7,iscur)
          icon(7,iscur) = icon(6,iscur)
          icon(6,iscur) = itemp
      elseif(icon(8,iscur) .eq. site1) then
          itemp = icon(1,iscur)
          icon(1,iscur) = icon(4,iscur)
          icon(4,iscur) = icon(3,iscur)
          icon(3,iscur) = itemp
          itemp = icon(5,iscur)
          icon(5,iscur) = icon(8,iscur)
          icon(8,iscur) = icon(7,iscur)
          icon(7,iscur) = itemp
      else
          stop 3010
      endif
  200 continue
      return
      end subroutine site1ordr

!=======================================================================

      subroutine sitord(icon, site1, iscur)
!c
      integer*8 icon(:,:), site1, iscur, itemp
!c
      if(icon(5,iscur) .eq. site1) go to 200
      if(icon(6,iscur) .eq. site1) then
          itemp = icon(1,iscur)
          icon(1,iscur) = icon(2,iscur)
          icon(2,iscur) = icon(4,iscur)
          icon(4,iscur) = itemp
          itemp = icon(5,iscur)
          icon(5,iscur) = icon(6,iscur)
          icon(6,iscur) = icon(8,iscur)
          icon(8,iscur) = itemp
      elseif(icon(7,iscur) .eq. site1) then
          itemp = icon(1,iscur)
          icon(1,iscur) = icon(3,iscur)
          icon(3,iscur) = icon(2,iscur)
          icon(2,iscur) = itemp
          itemp = icon(5,iscur)
          icon(5,iscur) = icon(7,iscur)
          icon(7,iscur) = icon(6,iscur)
          icon(6,iscur) = itemp
      elseif(icon(8,iscur) .eq. site1) then
          itemp = icon(1,iscur)
          icon(1,iscur) = icon(4,iscur)
          icon(4,iscur) = icon(3,iscur)
          icon(3,iscur) = itemp
          itemp = icon(5,iscur)
          icon(5,iscur) = icon(8,iscur)
          icon(8,iscur) = icon(7,iscur)
          icon(7,iscur) = itemp
      else
          stop 3010
      endif
  200 continue
      return
      end subroutine sitord

!=======================================================================

      subroutine sort2(n,arr,brr)
!c     ------------------------------------------------------------------
!c     sorts an array arr(1:n) into ascding numerical order using the
!c     quicksort algorithm, while making the corresponding rearrangement
!c     of the array brr(1:n)
!c     n is input, arr is replaced on output by its sorted rearrangement
!c     parameters: m is the size of subarrays sorted by straight insertion
!c                 nstack is the required auxillary storage
!c     ------------------------------------------------------------------
      INTEGER*8 n,M,NSTACK
      REAL*8 arr(n)
      INTEGER*8 brr(n)
      PARAMETER (M=7,NSTACK=50)
      INTEGER*8 i,ir,j,jstack,k,l,istack(NSTACK)
      REAL*8 a,temp
      INTEGER*8 b,btemp

      jstack=0
      l=1
      ir=n
1     if(ir-l.lt.M)then
        do 12 j=l+1,ir
          a=arr(j)
          b=brr(j)
          do 11 i=j-1,1,-1
            if(arr(i).le.a)goto 2
            arr(i+1)=arr(i)
            brr(i+1)=brr(i)
11        continue
          i=0
2         arr(i+1)=a
          brr(i+1)=b
12      continue
        if(jstack.eq.0)return
        ir=istack(jstack)
        l=istack(jstack-1)
        jstack=jstack-2
      else
        k=(l+ir)/2
        temp=arr(k)
        arr(k)=arr(l+1)
        arr(l+1)=temp
        btemp=brr(k)
        brr(k)=brr(l+1)
        brr(l+1)=btemp
        if(arr(l+1).gt.arr(ir))then
          temp=arr(l+1)
          arr(l+1)=arr(ir)
          arr(ir)=temp
          btemp=brr(l+1)
          brr(l+1)=brr(ir)
          brr(ir)=btemp
        endif
        if(arr(l).gt.arr(ir))then
          temp=arr(l)
          arr(l)=arr(ir)
          arr(ir)=temp
          btemp=brr(l)
          brr(l)=brr(ir)
          brr(ir)=btemp
        endif
        if(arr(l+1).gt.arr(l))then
          temp=arr(l+1)
          arr(l+1)=arr(l)
          arr(l)=temp
          btemp=brr(l+1)
          brr(l+1)=brr(l)
          brr(l)=btemp
        endif
        i=l+1
        j=ir
        a=arr(l)
        b=brr(l)
3       continue
          i=i+1
        if(arr(i).lt.a)goto 3
4       continue
          j=j-1
        if(arr(j).gt.a)goto 4
        if(j.lt.i)goto 5
        temp=arr(i)
        arr(i)=arr(j)
        arr(j)=temp
        btemp=brr(i)
        brr(i)=brr(j)
        brr(j)=btemp
        goto 3
5       arr(l)=arr(j)
        arr(j)=a
        brr(l)=brr(j)
        brr(j)=b
        jstack=jstack+2
        if(jstack.gt.NSTACK) then
          write(*,*) 'NSTACK too small in sort2'
        endif
        if(ir-i+1.ge.j-l)then
          istack(jstack)=ir
          istack(jstack-1)=i
          ir=j-1
        else
          istack(jstack)=j-1
          istack(jstack-1)=l
          l=i
        endif
      endif
      goto 1
      end subroutine sort2

!=======================================================================

      subroutine sphere(k, icon, ifl, ih, ihn, nhmax, newtts, xi, yi,   &
     &                  zi, wi, x, y, z, w, x2, y2, z2, w2, tetra, is,  &
     &                  nvmax, xc, yc, zc, delaun, flphis, mhalf, mfull,&
     &                  isclp, isclw, isclr, epz)
!c
      real(8) :: xi(:), yi(:), zi(:), wi(:)
      integer(8) :: x(:), y(:), z(:), w(:), x2(:), y2(:), z2(:), w2(:)
      integer(8) :: icon(:,:), ifl(:), is(:), ih(:)
      double precision epz, tdist, xctr, yctr, zctr
      integer*8 ihn, nhmax, newtts, nvmax, mhalf, mfull
      integer*8 xc(:), yc(:), zc(:)
      integer*8 adj, opvert, tetra, a, b, c, d, oddsid
      integer*8 isclp(:), isclw(:), isclr(:), iside(4), sidist(4)
      logical delaun, flphis
      integer*8 k, i, now, ipossi, itide, j, ib, ic, id, ifn, istt
      integer*8 isodd, isite, ipout
!c
      i = 1
  100 continue
      if (i .gt. newtts) goto 1500
!c
      now = ifl(i)
      if (icon(5,now) .lt. 0) goto 1000
      a = icon(5,now)
      if(a.ne.k) stop 1610
!c
!c     look at adj tet
!c
      adj = icon(1,now)
      if (adj .eq. 0) goto 1000
      if(icon(5,adj).eq.k) stop 1620
!c
      b = icon(6,now)
      c = icon(7,now)
      d = icon(8,now)
      if(b.lt.c) b=c
      if(b.lt.d) b=d
      if(b.le.8) stop 1630
      call reordr(icon,a,b,now)
      c=icon(7,now)
      d=icon(8,now)
!c
!c     reorder adj
!c
      call reordr (icon, b, c, adj)
!c
      if (icon(7,adj) .ne. d) stop 1640
      if (icon(4,adj) .ne. now) stop 1650
!c
      opvert = icon(8,adj)
      if(opvert.le.8.or.c.le.8.or.d.le.8) go to 300
!c
!c     test whether now and adj form a Regular configuration
!c
      call ctrad(xi, yi, zi, wi, xctr, yctr, zctr, b, c, d, opvert,     &
     &           epz, delaun, ipossi)
      if(ipossi.eq.1) go to 150
      call bisphr(xi, yi, zi, wi, a, b, epz, xctr, yctr, zctr, tdist,   &
     &            delaun, ipossi)
      if(ipossi.eq.1) go to 150
      if(tdist.le.0.0d0) go to 1000
      go to 170
  150 continue
      call iqsign(x, y, z, w, x2, y2, z2, w2, a, b, c, d, opvert,       &
     &            mhalf, mfull, isclp, isclw, isclr, delaun, itide)
      if(itide .ge. 0) go to 1000
!c
!c     compute distances from opvert to facets of now
!c
  170 continue
      call irsign(xi, yi, zi, x, y, z, x2, y2, z2, a, c, opvert,        &
     &            d, mhalf, mfull, isclp, epz, ipout)
      iside(2) = ipout
      call irsign(xi, yi, zi, x, y, z, x2, y2, z2, a, b, d,             &
     &            opvert, mhalf, mfull, isclp, epz, ipout)
      iside(3) = ipout
      call irsign(xi, yi, zi, x, y, z, x2, y2, z2, a, b, opvert,        &
     &            c, mhalf, mfull, isclp, epz, ipout)
      iside(4) = ipout
!c
!c     set sidist array
!c
      do 200 j = 2, 4
         if(iside(j) .gt. 0) then
            sidist(j) = 0
         elseif(iside(j) .lt. 0) then
            sidist(j) = -1
         else
            sidist(j) = 1
         endif
  200 continue
!c
!c     flip according to type of flip
!c
      if ((sidist(2) .eq. 0) .and. (sidist(3) .eq. 0) .and.             &
     &(sidist(4) .eq. 0)) then
         call flip23 (icon, k, now, adj, ifl, ih, ihn, nhmax, flphis,   &
     &                tetra, newtts, is, nvmax)
         go to 1000
      elseif ((sidist(2).eq.-1) .and. (sidist(3).eq.0) .and.            &
     &(sidist(4).eq.0)) then
         oddsid = 2
         call flip32 (icon, k, now, adj, ifl, ih, ihn, nhmax, flphis,   &
     &                tetra, newtts, is, oddsid, nvmax)
         go to 1000
      elseif ((sidist(2).eq.0) .and. (sidist(3).eq.-1) .and.            &
     &(sidist(4).eq.0)) then
         oddsid = 3
         call flip32 (icon, k, now, adj, ifl, ih, ihn, nhmax, flphis,   &
     &                tetra, newtts, is, oddsid, nvmax)
         go to 1000
      elseif ((sidist(2).eq.0) .and. (sidist(3).eq.0) .and.             &
     &(sidist(4).eq.-1)) then
         oddsid = 4
         call flip32 (icon, k, now, adj, ifl, ih, ihn, nhmax, flphis,   &
     &                tetra, newtts, is, oddsid, nvmax)
         go to 1000
      elseif ((sidist(2).eq.1) .and. (sidist(3).eq.0) .and.             &
     &(sidist(4).eq.0)) then
         oddsid = 2
         call flip22 (icon, k, now, adj, ifl, ih, ihn, nhmax, flphis,   &
     &                tetra, newtts, is, oddsid, nvmax)
         go to 1000
      elseif ((sidist(2).eq.0) .and. (sidist(3).eq.1) .and.             &
     &(sidist(4).eq.0)) then
         oddsid = 3
         call flip22 (icon, k, now, adj, ifl, ih, ihn, nhmax, flphis,   &
     &                tetra, newtts, is, oddsid, nvmax)
         go to 1000
      elseif ((sidist(2).eq.0) .and. (sidist(3).eq.0) .and.             &
     &(sidist(4).eq.1)) then
         oddsid = 4
         call flip22 (icon, k, now, adj, ifl, ih, ihn, nhmax, flphis,   &
     &                tetra, newtts, is, oddsid, nvmax)
         go to 1000
      elseif(delaun) then
         write(*,*)'Warning: Delaunay sphere violation'
         go to 1000
      endif
!c
      if ((sidist(2).eq.0) .and. (sidist(3).eq.-1) .and.                &
     &(sidist(4).eq.-1)) then
         oddsid = 2
         call flip41 (icon, k, now, adj, ifl, ih, ihn, nhmax, flphis,   &
     &                tetra, newtts, is, oddsid, nvmax)
      elseif ((sidist(2).eq.-1) .and. (sidist(3).eq.0) .and.            &
     &(sidist(4).eq.-1)) then
         oddsid = 3
         call flip41 (icon, k, now, adj, ifl, ih, ihn, nhmax, flphis,   &
     &                tetra, newtts, is, oddsid, nvmax)
      elseif ((sidist(2).eq.-1) .and. (sidist(3).eq.-1) .and.           &
     &(sidist(4).eq.0)) then
         oddsid = 4
         call flip41 (icon, k, now, adj, ifl, ih, ihn, nhmax, flphis,   &
     &                tetra, newtts, is, oddsid, nvmax)
      elseif ((sidist(2).eq.0) .and. (sidist(3).eq.1) .and.             &
     &(sidist(4).eq.1)) then
         oddsid = 2
         call flip21 (icon, k, now, adj, ifl, ih, ihn, nhmax, flphis,   &
     &                tetra, newtts, is, oddsid, nvmax)
      elseif ((sidist(2).eq.1) .and. (sidist(3).eq.0) .and.             &
     &(sidist(4).eq.1)) then
         oddsid = 3
         call flip21 (icon, k, now, adj, ifl, ih, ihn, nhmax, flphis,   &
     &                tetra, newtts, is, oddsid, nvmax)
      elseif ((sidist(2).eq.1) .and. (sidist(3).eq.1) .and.             &
     &(sidist(4).eq.0)) then
         oddsid = 4
         call flip21 (icon, k, now, adj, ifl, ih, ihn, nhmax, flphis,   &
     &                tetra, newtts, is, oddsid, nvmax)
      elseif ((sidist(2).eq.0) .and. (sidist(3).eq.-1) .and.            &
     &(sidist(4).eq.1)) then
         oddsid = 2
         call flip31 (icon, k, now, adj, ifl, ih, ihn, nhmax, flphis,   &
     &                tetra, newtts, is, oddsid, nvmax)
      elseif ((sidist(2).eq.1) .and. (sidist(3).eq.0) .and.             &
     &(sidist(4).eq.-1)) then
         oddsid = 3
         call flip31 (icon, k, now, adj, ifl, ih, ihn, nhmax, flphis,   &
     &                tetra, newtts, is, oddsid, nvmax)
      elseif ((sidist(2).eq.-1) .and. (sidist(3).eq.1) .and.            &
     &(sidist(4).eq.0)) then
         oddsid = 4
         call flip31 (icon, k, now, adj, ifl, ih, ihn, nhmax, flphis,   &
     &                tetra, newtts, is, oddsid, nvmax)
      elseif ((sidist(2).eq.0) .and. (sidist(3).eq.1) .and.             &
     &(sidist(4).eq.-1)) then
         oddsid = -2
         call flip31 (icon, k, now, adj, ifl, ih, ihn, nhmax, flphis,   &
     &                tetra, newtts, is, oddsid, nvmax)
      elseif ((sidist(2).eq.-1) .and. (sidist(3).eq.0) .and.            &
     &(sidist(4).eq.1)) then
         oddsid = -3
         call flip31 (icon, k, now, adj, ifl, ih, ihn, nhmax, flphis,   &
     &                tetra, newtts, is, oddsid, nvmax)
      elseif ((sidist(2).eq.1) .and. (sidist(3).eq.-1) .and.            &
     &(sidist(4).eq.0)) then
         oddsid = -4
         call flip31 (icon, k, now, adj, ifl, ih, ihn, nhmax, flphis,   &
     &                tetra, newtts, is, oddsid, nvmax)
      else
         write(*,*)'Warning: Regular sphere violation'
      endif
      go to 1000
!c
  300 continue
      if(opvert.le.8.and.c.gt.8.and.d.gt.8) go to 1000
!c
!c     determine signs of distances from opvert to facets of
!c     tetrahedron now
!c
      if(opvert.gt.8)then
         if(c.le.8.and.d.le.8)then
            call ipsig4(x, y, z, x2, y2, z2, xc, yc, zc,                &
     &                  d, c, a, opvert, mhalf, mfull, isclp, ipout)
            iside(2) = ipout
            if(iside(2).ne.0) go to 310
            call ipsig6(x, y, z, x2, y2, z2, xc, yc, zc,                &
     &                  d, c, a, opvert, mhalf, mfull, isclp, ipout)
            iside(2) = ipout
            go to 310
         endif
         call vrtarr(c,d,opvert,ib,ic,id)
         if(ic.gt.8)then
            ifn = 0
            call ipsig3(x, y, z, x2, y2, z2, xc, yc, zc,                &
     &                  ib, ic, a, id, mhalf, mfull, isclp, ifn, ipout)
            iside(2) = ipout
         else
            ifn = 1
            call ipsig3(x, y, z, x2, y2, z2, xc, yc, zc,                &
     &                  ib, id, a, ic, mhalf, mfull, isclp, ifn, ipout)
            iside(2) = ipout
         endif
         if(iside(2).ne.0) go to 310
         call ipsign(x, y, z, x2, y2, z2, ib, id, ic, a, mhalf,         &
     &               mfull, isclp, ipout)
         iside(2) = ipout
  310    continue
!c
         call vrtarr(b,opvert,d,ib,ic,id)
         if(d.gt.8)then
            call ipsign(x, y, z, x2, y2, z2, ib, id, ic, a, mhalf,      &
     &                  mfull, isclp, ipout)
            iside(3) = ipout
            go to 320
         endif
         if(ic.gt.8)then
            ifn = 0
            call ipsig3(x, y, z, x2, y2, z2, xc, yc, zc,                &
     &                  ib, ic, a, id, mhalf, mfull, isclp, ifn, ipout)
            iside(3) = ipout
         else
            ifn = 1
            call ipsig3(x, y, z, x2, y2, z2, xc, yc, zc,                &
     &                  ib, id, a, ic, mhalf, mfull, isclp, ifn, ipout)
            iside(3) = ipout
         endif
         if(iside(3).ne.0) go to 320
         call ipsign(x, y, z, x2, y2, z2, ib, id, ic, a, mhalf,         &
     &               mfull, isclp, ipout)
         iside(3) = ipout
  320    continue
!c
         call vrtarr(b,c,opvert,ib,ic,id)
         if(c.gt.8)then
            call ipsign(x, y, z, x2, y2, z2, ib, id, ic, a, mhalf,      &
     &                  mfull, isclp, ipout)
            iside(4) = ipout
            go to 330
         endif
         if(ic.gt.8)then
            ifn = 0
            call ipsig3(x, y, z, x2, y2, z2, xc, yc, zc,                &
     &                  ib, ic, a, id, mhalf, mfull, isclp, ifn, ipout)
            iside(4) = ipout
         else
            ifn = 1
            call ipsig3(x, y, z, x2, y2, z2, xc, yc, zc,                &
     &                  ib, id, a, ic, mhalf, mfull, isclp, ifn, ipout)
            iside(4) = ipout
         endif
         if(iside(4).ne.0) go to 330
         call ipsign(x, y, z, x2, y2, z2, ib, id, ic, a, mhalf,         &
     &               mfull, isclp, ipout)
         iside(4) = ipout
  330    continue
!c
      else
         if(c.le.8.and.d.le.8) then
            iside(2) = 1
         elseif(c.gt.8) then
            call ipsig4(x, y, z, x2, y2, z2, xc, yc, zc,                &
     &                  opvert, d, a, c, mhalf, mfull, isclp, ipout)
            iside(2) = ipout
            if(iside(2).ne.0) go to 340
            call ipsig6(x, y, z, x2, y2, z2, xc, yc, zc,                &
     &                  opvert, d, a, c, mhalf, mfull, isclp, ipout)
            iside(2) = ipout
         else
            call ipsig4(x, y, z, x2, y2, z2, xc, yc, zc,                &
     &                  c, opvert, a, d, mhalf, mfull, isclp, ipout)
            iside(2) = ipout
            if(iside(2).ne.0) go to 340
            call ipsig6(x, y, z, x2, y2, z2, xc, yc, zc,                &
     &                  c, opvert, a, d, mhalf, mfull, isclp, ipout)
            iside(2) = ipout
         endif
  340    continue
!c
         if(d.gt.8)then
            ifn = 1
            call ipsig3(x, y, z, x2, y2, z2, xc, yc, zc,                &
     &                 b, d, a, opvert, mhalf, mfull, isclp, ifn, ipout)
            iside(3) = ipout
         else
            call ipsig4(x, y, z, x2, y2, z2, xc, yc, zc,                &
     &                  d, opvert, a, b, mhalf, mfull, isclp, ipout)
            iside(3) = ipout
            if(iside(3).ne.0) go to 350
            call ipsig6(x, y, z, x2, y2, z2, xc, yc, zc,                &
     &                  d, opvert, a, b, mhalf, mfull, isclp, ipout)
            iside(3) = ipout
            go to 350
         endif
         if(iside(3).ne.0) go to 350
         call ipsign(x, y, z, x2, y2, z2, b, d, opvert, a, mhalf,       &
     &               mfull, isclp, ipout)
         iside(3) = ipout
  350    continue
!c
         if(c.gt.8)then
            ifn = 0
            call ipsig3(x, y, z, x2, y2, z2, xc, yc, zc,                &
     &                 b, c, a, opvert, mhalf, mfull, isclp, ifn, ipout)
            iside(4) = ipout
         else
            call ipsig4(x, y, z, x2, y2, z2, xc, yc, zc,                &
     &                  opvert, c, a, b, mhalf, mfull, isclp, ipout)
            iside(4) = ipout
            if(iside(4).ne.0) go to 360
            call ipsig6(x, y, z, x2, y2, z2, xc, yc, zc,                &
     &                  opvert, c, a, b, mhalf, mfull, isclp, ipout)
            iside(4) = ipout
            go to 360
         endif
         if(iside(4).ne.0) go to 360
         call ipsign(x, y, z, x2, y2, z2, b, opvert, c, a, mhalf,       &
     &               mfull, isclp, ipout)
         iside(4) = ipout
  360    continue
      endif
!c
!c     set sidist array
!c
      do 400 j = 2, 4
         if(iside(j) .gt. 0) then
            sidist(j) = 0
         elseif(iside(j) .lt. 0) then
            sidist(j) = -1
         else
            sidist(j) = 1
         endif
  400 continue
!c
!c     flip according to type of flip if possible
!c
      if ((sidist(2) .eq. 0) .and. (sidist(3) .eq. 0) .and.             &
     &    (sidist(4) .eq. 0)) then
         if(opvert.gt.8) go to 420
         if(c.le.8.and.d.le.8)then
            call ipsig2(x, y, z, x2, y2, z2, xc, yc, zc, opvert, d,     &
     &                  opvert, c, a, b, mhalf, mfull, isclp, istt)
            if(istt.gt.0) go to 420
            if(istt.lt.0) go to 1000
            call iqsign(x, y, z, w, x2, y2, z2, w2, opvert, d, c, b,    &
     &           a, mhalf, mfull, isclp, isclw, isclr, delaun, itide)
         elseif(c.le.8)then
            call ipsig1(x, y, z, x2, y2, z2, xc, yc, zc, c, opvert,     &
     &                  b, d, a, b, mhalf, mfull, isclp, istt)
            if(istt.gt.0) go to 420
            if(istt.lt.0) go to 1000
            call iqsign(x, y, z, w, x2, y2, z2, w2, opvert, d, c, b,    &
     &           a, mhalf, mfull, isclp, isclw, isclr, delaun, itide)
         else
            call ipsig1(x, y, z, x2, y2, z2, xc, yc, zc, d, opvert,     &
     &                  c, b, a, c, mhalf, mfull, isclp, istt)
            if(istt.gt.0) go to 420
            if(istt.lt.0) go to 1000
            call iqsign(x, y, z, w, x2, y2, z2, w2, opvert, d, c, b,    &
     &           a, mhalf, mfull, isclp, isclw, isclr, delaun, itide)
         endif
         if(itide .ge. 0) go to 1000
  420    continue
         call flip23 (icon, k, now, adj, ifl, ih, ihn, nhmax, flphis,   &
     &                tetra, newtts, is, nvmax)
         go to 1000
      elseif ((sidist(2).eq.-1) .and. (sidist(3).eq.0) .and.            &
     &        (sidist(4).eq.0)) then
         if(c.le.8.and.d.le.8) stop 1660
         if(opvert.gt.8) go to 440
         if(c.le.8)then
            call ipsig1(x, y, z, x2, y2, z2, xc, yc, zc, c, opvert,     &
     &                  b, d, a, b, mhalf, mfull, isclp, istt)
            if(istt.gt.0) go to 440
            if(istt.lt.0) go to 1000
            call iqsign(x, y, z, w, x2, y2, z2, w2, opvert, d, c, b,    &
     &           a, mhalf, mfull, isclp, isclw, isclr, delaun, itide)
         else
            call ipsig1(x, y, z, x2, y2, z2, xc, yc, zc, d, opvert,     &
     &                  c, b, a, c, mhalf, mfull, isclp, istt)
            if(istt.gt.0) go to 440
            if(istt.lt.0) go to 1000
            call iqsign(x, y, z, w, x2, y2, z2, w2, opvert, d, c, b,    &
     &           a, mhalf, mfull, isclp, isclw, isclr, delaun, itide)
         endif
         if(itide .ge. 0) go to 1000
  440    continue
         oddsid = 2
         call flip32 (icon, k, now, adj, ifl, ih, ihn, nhmax, flphis,   &
     &                tetra, newtts, is, oddsid, nvmax)
         go to 1000
      elseif ((sidist(2).eq.0) .and. (sidist(3).eq.-1) .and.            &
     &        (sidist(4).eq.0)) then
         if(d.gt.8) go to 1000
         if(opvert.gt.8.and.c.gt.8) go to 460
         if(opvert.gt.8)then
            call ipsig1(x, y, z, x2, y2, z2, xc, yc, zc, d, c, b,       &
     &                  opvert, a, b, mhalf, mfull, isclp, istt)
            if(istt.gt.0) go to 460
            if(istt.lt.0) go to 1000
            call iqsign(x, y, z, w, x2, y2, z2, w2, opvert, d, c, b,    &
     &           a, mhalf, mfull, isclp, isclw, isclr, delaun, itide)
         elseif(c.le.8)then
            call ipsig2(x, y, z, x2, y2, z2, xc, yc, zc, opvert, d,     &
     &                  opvert, c, a, b, mhalf, mfull, isclp, istt)
            if(istt.gt.0) go to 460
            if(istt.lt.0) go to 1000
            call iqsign(x, y, z, w, x2, y2, z2, w2, opvert, d, c, b,    &
     &           a, mhalf, mfull, isclp, isclw, isclr, delaun, itide)
         else
            call ipsig1(x, y, z, x2, y2, z2, xc, yc, zc, d, opvert,     &
     &                  c, b, a, c, mhalf, mfull, isclp, istt)
            if(istt.gt.0) go to 460
            if(istt.lt.0) go to 1000
            call iqsign(x, y, z, w, x2, y2, z2, w2, opvert, d, c, b,    &
     &           a, mhalf, mfull, isclp, isclw, isclr, delaun, itide)
         endif
         if(itide .ge. 0) go to 1000
  460    continue
         oddsid = 3
         call flip32 (icon, k, now, adj, ifl, ih, ihn, nhmax, flphis,   &
     &                tetra, newtts, is, oddsid, nvmax)
         go to 1000
      elseif ((sidist(2).eq.0) .and. (sidist(3).eq.0) .and.             &
     &        (sidist(4).eq.-1)) then
         if(c.gt.8) go to 1000
         if(opvert.gt.8.and.d.gt.8) go to 480
         if(opvert.gt.8)then
            call ipsig1(x, y, z, x2, y2, z2, xc, yc, zc, d, c, b,       &
     &                  opvert, a, b, mhalf, mfull, isclp, istt)
            if(istt.gt.0) go to 480
            if(istt.lt.0) go to 1000
            call iqsign(x, y, z, w, x2, y2, z2, w2, opvert, d, c, b,    &
     &           a, mhalf, mfull, isclp, isclw, isclr, delaun, itide)
         elseif(d.le.8)then
            call ipsig2(x, y, z, x2, y2, z2, xc, yc, zc, opvert, d,     &
     &                  opvert, c, a, b, mhalf, mfull, isclp, istt)
            if(istt.gt.0) go to 480
            if(istt.lt.0) go to 1000
            call iqsign(x, y, z, w, x2, y2, z2, w2, opvert, d, c, b,    &
     &           a, mhalf, mfull, isclp, isclw, isclr, delaun, itide)
         else
            call ipsig1(x, y, z, x2, y2, z2, xc, yc, zc, c, opvert,     &
     &                  b, d, a, b, mhalf, mfull, isclp, istt)
            if(istt.gt.0) go to 480
            if(istt.lt.0) go to 1000
            call iqsign(x, y, z, w, x2, y2, z2, w2, opvert, d, c, b,    &
     &           a, mhalf, mfull, isclp, isclw, isclr, delaun, itide)
         endif
         if(itide .ge. 0) go to 1000
  480    continue
         oddsid = 4
         call flip32 (icon, k, now, adj, ifl, ih, ihn, nhmax, flphis,   &
     &                tetra, newtts, is, oddsid, nvmax)
         go to 1000
      elseif ((sidist(2).eq.1) .and. (sidist(3).eq.0) .and.             &
     &        (sidist(4).eq.0)) then
         if(c.le.8.and.d.le.8) stop 1670
         if(opvert.gt.8) go to 500
         if(c.le.8)then
            call ipsig1(x, y, z, x2, y2, z2, xc, yc, zc, c, opvert,     &
     &                  b, d, a, b, mhalf, mfull, isclp, istt)
            if(istt.gt.0) go to 500
            if(istt.lt.0) go to 1000
            call iqsign(x, y, z, w, x2, y2, z2, w2, opvert, d, c, b,    &
     &           a, mhalf, mfull, isclp, isclw, isclr, delaun, itide)
         else
            call ipsig1(x, y, z, x2, y2, z2, xc, yc, zc, d, opvert,     &
     &                  c, b, a, c, mhalf, mfull, isclp, istt)
            if(istt.gt.0) go to 500
            if(istt.lt.0) go to 1000
            call iqsign(x, y, z, w, x2, y2, z2, w2, opvert, d, c, b,    &
     &           a, mhalf, mfull, isclp, isclw, isclr, delaun, itide)
         endif
         if(itide .ge. 0) go to 1000
  500    continue
         oddsid = 2
         call flip22 (icon, k, now, adj, ifl, ih, ihn, nhmax, flphis,   &
     &                tetra, newtts, is, oddsid, nvmax)
         go to 1000
      elseif ((sidist(2).eq.0) .and. (sidist(3).eq.1) .and.             &
     &        (sidist(4).eq.0)) then
         if(opvert.gt.8.and.d.gt.8)then
            call iqsig2(x, y, z, w, x2, y2, z2, w2, b, d, opvert, a,    &
     &           mhalf, mfull, isclp, isclw, isclr, delaun, itide)
            if(itide.ge.0) go to 1000
            go to 520
         endif
         if(opvert.gt.8) go to 520
         if(d.gt.8) go to 1000
         if(c.gt.8)then
            call ipsig1(x, y, z, x2, y2, z2, xc, yc, zc, d, opvert,     &
     &                  c, b, a, c, mhalf, mfull, isclp, istt)
            if(istt.gt.0) go to 520
            if(istt.lt.0) go to 1000
            call iqsign(x, y, z, w, x2, y2, z2, w2, opvert, d, c, b,    &
     &           a, mhalf, mfull, isclp, isclw, isclr, delaun, itide)
         else
            call ipsig2(x, y, z, x2, y2, z2, xc, yc, zc, opvert, d,     &
     &                  opvert, c, a, b, mhalf, mfull, isclp, istt)
            if(istt.gt.0) go to 520
            if(istt.lt.0) go to 1000
            call iqsign(x, y, z, w, x2, y2, z2, w2, opvert, d, c, b,    &
     &           a, mhalf, mfull, isclp, isclw, isclr, delaun, itide)
         endif
         if(itide .ge. 0) go to 1000
  520    continue
         oddsid = 3
         call flip22 (icon, k, now, adj, ifl, ih, ihn, nhmax, flphis,   &
     &                tetra, newtts, is, oddsid, nvmax)
         go to 1000
      elseif ((sidist(2).eq.0) .and. (sidist(3).eq.0) .and.             &
     &        (sidist(4).eq.1)) then
         if(opvert.gt.8.and.c.gt.8)then
            call iqsig2(x, y, z, w, x2, y2, z2, w2, b, opvert, c, a,    &
     &           mhalf, mfull, isclp, isclw, isclr, delaun, itide)
            if(itide.ge.0) go to 1000
            go to 540
         endif
         if(opvert.gt.8) go to 540
         if(c.gt.8) go to 1000
         if(d.gt.8)then
            call ipsig1(x, y, z, x2, y2, z2, xc, yc, zc, c, opvert,     &
     &                  b, d, a, b, mhalf, mfull, isclp, istt)
            if(istt.gt.0) go to 540
            if(istt.lt.0) go to 1000
            call iqsign(x, y, z, w, x2, y2, z2, w2, opvert, d, c, b,    &
     &           a, mhalf, mfull, isclp, isclw, isclr, delaun, itide)
         else
            call ipsig2(x, y, z, x2, y2, z2, xc, yc, zc, opvert, d,     &
     &                  opvert, c, a, b, mhalf, mfull, isclp, istt)
            if(istt.gt.0) go to 540
            if(istt.lt.0) go to 1000
            call iqsign(x, y, z, w, x2, y2, z2, w2, opvert, d, c, b,    &
     &           a, mhalf, mfull, isclp, isclw, isclr, delaun, itide)
         endif
         if(itide .ge. 0) go to 1000
  540    continue
         oddsid = 4
         call flip22 (icon, k, now, adj, ifl, ih, ihn, nhmax, flphis,   &
     &                tetra, newtts, is, oddsid, nvmax)
         go to 1000
      elseif(delaun) then
         go to 1000
      endif
!c
      if ((sidist(2).eq.0) .and. (sidist(3).eq.-1) .and.                &
     &    (sidist(4).eq.-1)) then
         oddsid = 2
         go to 900
      elseif ((sidist(2).eq.-1) .and. (sidist(3).eq.0) .and.            &
     &        (sidist(4).eq.-1)) then
         oddsid = 3
         go to 900
      elseif ((sidist(2).eq.-1) .and. (sidist(3).eq.-1) .and.           &
     &        (sidist(4).eq.0)) then
         oddsid = 4
         go to 900
      elseif ((sidist(2).eq.0) .and. (sidist(3).eq.1) .and.             &
     &        (sidist(4).eq.1)) then
         oddsid = 2
         if(opvert.le.8)go to 900
         isodd = icon(6,now)
         if(isodd.le.8) stop 1680
         call iqsig1(x, y, z, w, x2, y2, z2, w2, isodd, opvert, k,      &
     &               mhalf, mfull, isclp, isclw, isclr, delaun, itide)
         if(itide .ge. 0) go to 1000
         call flip21 (icon, k, now, adj, ifl, ih, ihn, nhmax, flphis,   &
     &                tetra, newtts, is, oddsid, nvmax)
      elseif ((sidist(2).eq.1) .and. (sidist(3).eq.0) .and.             &
     &        (sidist(4).eq.1)) then
         oddsid = 3
         if(opvert.le.8)go to 900
         isodd = icon(7,now)
         if(isodd.le.8) stop 1690
         call iqsig1(x, y, z, w, x2, y2, z2, w2, isodd, opvert, k,      &
     &               mhalf, mfull, isclp, isclw, isclr, delaun, itide)
         if(itide .ge. 0) go to 1000
         call flip21 (icon, k, now, adj, ifl, ih, ihn, nhmax, flphis,   &
     &                tetra, newtts, is, oddsid, nvmax)
      elseif ((sidist(2).eq.1) .and. (sidist(3).eq.1) .and.             &
     &        (sidist(4).eq.0)) then
         oddsid = 4
         if(opvert.le.8)go to 900
         isodd = icon(8,now)
         if(isodd.le.8) stop 1710
         call iqsig1(x, y, z, w, x2, y2, z2, w2, isodd, opvert, k,      &
     &               mhalf, mfull, isclp, isclw, isclr, delaun, itide)
         if(itide .ge. 0) go to 1000
         call flip21 (icon, k, now, adj, ifl, ih, ihn, nhmax, flphis,   &
     &                tetra, newtts, is, oddsid, nvmax)
      elseif ((sidist(2).eq.0) .and. (sidist(3).eq.-1) .and.            &
     &        (sidist(4).eq.1)) then
         oddsid = 2
         isite = icon(7,now)
         if(opvert.le.8 .or. isite.le.8)go to 900
         isodd = icon(6,now)
         if(isodd.le.8) stop 1720
         call iqsig2(x, y, z, w, x2, y2, z2, w2, isodd, isite, opvert,  &
     &        k, mhalf, mfull, isclp, isclw, isclr, delaun, itide)
         if(itide .ge. 0) go to 1000
         call flip31 (icon, k, now, adj, ifl, ih, ihn, nhmax, flphis,   &
     &                tetra, newtts, is, oddsid, nvmax)
      elseif ((sidist(2).eq.1) .and. (sidist(3).eq.0) .and.             &
     &        (sidist(4).eq.-1)) then
         oddsid = 3
         isite = icon(8,now)
         if(opvert.le.8 .or. isite.le.8)go to 900
         stop 1730
      elseif ((sidist(2).eq.-1) .and. (sidist(3).eq.1) .and.            &
     &        (sidist(4).eq.0)) then
         oddsid = 4
         isite = icon(6,now)
         if(opvert.le.8 .or. isite.le.8)go to 900
         isodd = icon(8,now)
         if(isodd.le.8) stop 1740
         call iqsig2(x, y, z, w, x2, y2, z2, w2, isodd, isite, opvert,  &
     &        k, mhalf, mfull, isclp, isclw, isclr, delaun, itide)
         if(itide .ge. 0) go to 1000
         call flip31 (icon, k, now, adj, ifl, ih, ihn, nhmax, flphis,   &
     &                tetra, newtts, is, oddsid, nvmax)
      elseif ((sidist(2).eq.0) .and. (sidist(3).eq.1) .and.             &
     &        (sidist(4).eq.-1)) then
         oddsid = -2
         isite = icon(8,now)
         if(opvert.le.8 .or. isite.le.8)go to 800
         isodd = icon(6,now)
         if(isodd.le.8) stop 1750
         call iqsig2(x, y, z, w, x2, y2, z2, w2, isodd, isite, opvert,  &
     &        k, mhalf, mfull, isclp, isclw, isclr, delaun, itide)
         if(itide .ge. 0) go to 1000
         call flip31 (icon, k, now, adj, ifl, ih, ihn, nhmax, flphis,   &
     &                tetra, newtts, is, oddsid, nvmax)
      elseif ((sidist(2).eq.-1) .and. (sidist(3).eq.0) .and.            &
     &        (sidist(4).eq.1)) then
         oddsid = -3
         isite = icon(6,now)
         if(opvert.le.8 .or. isite.le.8)go to 800
         isodd = icon(7,now)
         if(isodd.le.8) stop 1760
         call iqsig2(x, y, z, w, x2, y2, z2, w2, isodd, isite, opvert,  &
     &        k, mhalf, mfull, isclp, isclw, isclr, delaun, itide)
         if(itide .ge. 0) go to 1000
         call flip31 (icon, k, now, adj, ifl, ih, ihn, nhmax, flphis,   &
     &                tetra, newtts, is, oddsid, nvmax)
      elseif ((sidist(2).eq.1) .and. (sidist(3).eq.-1) .and.            &
     &(sidist(4).eq.0)) then
         oddsid = -4
         isite = icon(7,now)
         if(opvert.le.8 .or. isite.le.8) go to 800
         stop 1770
      endif
      go to 1000
!c
  800 continue
      oddsid=-oddsid
  900 continue
      isite = icon(oddsid+4,now)
      if(isite.le.8) stop 1780
!c
 1000 continue
      i = i + 1
      goto 100
!c
 1500 continue
!c
      return
      end subroutine sphere

!=======================================================================

      subroutine srtbytet(n,list,itetra,dist)
!c     ------------------------------------------------------------------
!c     sorts list by a criterion based on the four values of itetra
!c     within blocks of common values of dist
!c     common is defined by equivalence to within a tolerance
!c     ------------------------------------------------------------------
      integer*8 n
      integer*8 list(n)
      integer*8 itetra(4,n)
      double precision dist(n)

      integer*8 j
      integer*8 i1,i2
      integer*8 len


      i1=1
 10   continue
      do j=i1+1,n
        if( abs(dist(j)-dist(i1)).gt.1.0d-5 ) then
          i2=j-1
          go to 20
        endif
      enddo
      i2=n
 20   len=i2-i1+1
      if(len.gt.1) call ipiksr4(len,itetra(1,i1),list(i1))
      if(i2.lt.n) then
        i1=i2+1
        go to 10
      endif

      return
      end subroutine srtbytet

!=======================================================================

      subroutine srtwithin(n,list,dist)
!c     ------------------------------------------------------------------
!c     ------------------------------------------------------------------
      integer*8 n
      integer*8 list(n)
      double precision dist(n)

      integer*8 j
      integer*8 i1,i2
      integer*8 len


      i1=1
 10   continue
      do j=i1+1,n
        if( abs(dist(j)-dist(i1)).gt.1.0d-5 ) then
          i2=j-1
          go to 20
        endif
      enddo
      i2=n
 20   len=i2-i1+1
      if(len.gt.1) call ipiksrt(len,list(i1))
      if(i2.lt.n) then
        i1=i2+1
        go to 10
      endif

      return
      end subroutine srtwithin

!=======================================================================

      subroutine tetvol(x, y, z, x2, y2, z2, ifir, isec, ithi,          &
     &                  ifou, mhalf, mfull, isclp, io, isgo, iko)
!c
      integer*8 x(:), y(:), z(:), x2(:), y2(:), z2(:)
      integer*8 ifir, isec, ithi, ifou
      integer*8 isclp(:), mhalf, mfull, nkmax
      parameter (nkmax = 30)
      integer*8 io(:), iu(nkmax), iv(nkmax), iw(nkmax)
      integer*8 ix2(nkmax), iy2(nkmax), iz2(nkmax)
      integer*8 ix3(nkmax), iy3(nkmax), iz3(nkmax)
      integer*8 ix4(nkmax), iy4(nkmax), iz4(nkmax)
      integer*8 ixf(nkmax), iyf(nkmax), izf(nkmax)
      integer*8 ixfiw, iyfiw, izfiw, ixsew, iysew, izsew
      integer*8 ixthw, iythw, izthw, ixfow, iyfow, izfow
      integer*8 ixfi2, iyfi2, izfi2, ixse2, iyse2, izse2
      integer*8 ixth2, iyth2, izth2, ixfo2, iyfo2, izfo2
      integer*8 isgxf, isgyf, isgzf, ikxf, ikyf, ikzf
      integer*8 isgx2, isgy2, isgz2, ikx2, iky2, ikz2
      integer*8 isgx3, isgy3, isgz3, ikx3, iky3, ikz3
      integer*8 isgx4, isgy4, isgz4, ikx4, iky4, ikz4
      integer*8 isgo, isgu, isgv, isgw, iko, iku, ikv, ikw
!c
      ixfiw = x(ifir)
      iyfiw = y(ifir)
      izfiw = z(ifir)
      ixsew = x(isec)
      iysew = y(isec)
      izsew = z(isec)
      ixthw = x(ithi)
      iythw = y(ithi)
      izthw = z(ithi)
      ixfow = x(ifou)
      iyfow = y(ifou)
      izfow = z(ifou)
!c
      ixfi2 = x2(ifir)
      iyfi2 = y2(ifir)
      izfi2 = z2(ifir)
      ixse2 = x2(isec)
      iyse2 = y2(isec)
      izse2 = z2(isec)
      ixth2 = x2(ithi)
      iyth2 = y2(ithi)
      izth2 = z2(ithi)
      ixfo2 = x2(ifou)
      iyfo2 = y2(ifou)
      izfo2 = z2(ifou)
!c
      call decmp2(ixf, isgxf, ikxf, ixfiw, ixfi2, mhalf, mfull, isclp)
      call decmp2(iyf, isgyf, ikyf, iyfiw, iyfi2, mhalf, mfull, isclp)
      call decmp2(izf, isgzf, ikzf, izfiw, izfi2, mhalf, mfull, isclp)
!c
      call decmp2(io, isgo, iko, ixsew, ixse2, mhalf, mfull, isclp)
      call muldif(io, ixf, ix2, isgo, isgxf, isgx2, iko, ikxf, ikx2,    &
     &              nkmax, mhalf)
      call decmp2(io, isgo, iko, iysew, iyse2, mhalf, mfull, isclp)
      call muldif(io, iyf, iy2, isgo, isgyf, isgy2, iko, ikyf, iky2,    &
     &              nkmax, mhalf)
      call decmp2(io, isgo, iko, izsew, izse2, mhalf, mfull, isclp)
      call muldif(io, izf, iz2, isgo, isgzf, isgz2, iko, ikzf, ikz2,    &
     &              nkmax, mhalf)
      call decmp2(io, isgo, iko, ixthw, ixth2, mhalf, mfull, isclp)
      call muldif(io, ixf, ix3, isgo, isgxf, isgx3, iko, ikxf, ikx3,    &
     &              nkmax, mhalf)
      call decmp2(io, isgo, iko, iythw, iyth2, mhalf, mfull, isclp)
      call muldif(io, iyf, iy3, isgo, isgyf, isgy3, iko, ikyf, iky3,    &
     &              nkmax, mhalf)
      call decmp2(io, isgo, iko, izthw, izth2, mhalf, mfull, isclp)
      call muldif(io, izf, iz3, isgo, isgzf, isgz3, iko, ikzf, ikz3,    &
     &              nkmax, mhalf)
      call decmp2(io, isgo, iko, ixfow, ixfo2, mhalf, mfull, isclp)
      call muldif(io, ixf, ix4, isgo, isgxf, isgx4, iko, ikxf, ikx4,    &
     &              nkmax, mhalf)
      call decmp2(io, isgo, iko, iyfow, iyfo2, mhalf, mfull, isclp)
      call muldif(io, iyf, iy4, isgo, isgyf, isgy4, iko, ikyf, iky4,    &
     &              nkmax, mhalf)
      call decmp2(io, isgo, iko, izfow, izfo2, mhalf, mfull, isclp)
      call muldif(io, izf, iz4, isgo, isgzf, isgz4, iko, ikzf, ikz4,    &
     &              nkmax, mhalf)
!c
      call mulmul(iy2, iz3, iv, isgy2, isgz3, isgv, iky2, ikz3, ikv,    &
     &              nkmax, mhalf)
      call mulmul(iz2, iy3, iu, isgz2, isgy3, isgu, ikz2, iky3, iku,    &
     &              nkmax, mhalf)
      call muldif(iv, iu, iw, isgv, isgu, isgw, ikv, iku, ikw,          &
     &              nkmax, mhalf)
      call mulmul(iw, ix4, io, isgw, isgx4, isgo, ikw, ikx4, iko,       &
     &              nkmax, mhalf)
!c
      call mulmul(iz2, ix3, iv, isgz2, isgx3, isgv, ikz2, ikx3, ikv,    &
     &              nkmax, mhalf)
      call mulmul(ix2, iz3, iu, isgx2, isgz3, isgu, ikx2, ikz3, iku,    &
     &              nkmax, mhalf)
      call muldif(iv, iu, iw, isgv, isgu, isgw, ikv, iku, ikw,          &
     &              nkmax, mhalf)
      call mulmul(iw, iy4, iu, isgw, isgy4, isgu, ikw, iky4, iku,       &
     &              nkmax, mhalf)
      isgu =-isgu
      call muldif(io, iu, iw, isgo, isgu, isgw, iko, iku, ikw,          &
     &              nkmax, mhalf)
!c
      call mulmul(ix2, iy3, iv, isgx2, isgy3, isgv, ikx2, iky3, ikv,    &
     &              nkmax, mhalf)
      call mulmul(iy2, ix3, iu, isgy2, isgx3, isgu, iky2, ikx3, iku,    &
     &              nkmax, mhalf)
      call muldif(iv, iu, io, isgv, isgu, isgo, ikv, iku, iko,          &
     &              nkmax, mhalf)
      call mulmul(io, iz4, iu, isgo, isgz4, isgu, iko, ikz4, iku,       &
     &              nkmax, mhalf)
      isgu =-isgu
      call muldif(iw, iu, io, isgw, isgu, isgo, ikw, iku, iko,          &
     &              nkmax, mhalf)
!c
      return
      end subroutine tetvol

!=======================================================================

      subroutine trsort(var, ji, klen)
!c
      real(8) :: var(:)
      integer(8) :: ji(:), klen, iast, k, i, jj, iis
!c
!c     create initial tree in decreasing order
!c
      iast = klen
      do 100 k = 1, klen
          i = k
   50     continue
!c
!c     check if current node is as small as father
!c
          if(i .eq. 1) go to 100
          if(var(ji(i)) .le. var(ji(i/2))) go to 100
          jj = ji(i)
          ji(i) = ji(i/2)
          ji(i/2) = jj
          i = i/2
          go to 50
  100 continue
      if(iast .eq. 1) go to 160
!c
!c     sort by shrinking tree: last element is moved to the
!c     first position
!c
  102 continue
      i = 1
      jj = ji(1)
      ji(1) = ji(iast)
      ji(iast) = jj
      if(iast .eq. 2) go to 160
      iast = iast - 1
!c
  105 continue
      iis = 2*i
!c
!c     check which sons exist
!c
!c     if(iis - iast) 110, 140, 150
      if(iis.lt.iast) then
        go to 110
      elseif(iis.eq.iast) then
        go to 140
      else
        go to 150
      endif
!c
!c     both sons exist
!c
  110 continue
      if(var(ji(i)) .lt. var(ji(iis))) go to 120
      if(var(ji(i)) .ge. var(ji(iis+1))) go to 150
      go to 125
!c
!c     check which son to be switched
!c
  120 continue
      if(var(ji(iis)) .ge. var(ji(iis+1))) go to 130
!c
!c     adjust to switch with right son
!c
  125 continue
      iis = iis + 1
!c
!c     switch
!c
  130 continue
      jj = ji(i)
      ji(i) = ji(iis)
      ji(iis) = jj
      i = iis
      go to 105
!c
!c     only left son exists
!c
  140 continue
      if(var(ji(i)) .ge. var(ji(iis))) go to 150
      jj = ji(i)
      ji(i) = ji(iis)
      ji(iis) = jj
!c
!c     no more switching needed
!c
  150 continue
      go to 102
!c
!c     sorting is finished
!c
  160 continue
!c
      return
      end subroutine trsort

!=======================================================================

      subroutine umi(rumi,i,j,k,n)
!c
      double precision rumi
      integer*8 mzrn,i,j,k,n
!c
      call mzran(i,j,k,n,mzrn)
      rumi = .5 + .2328306e-9*mzrn
!c
      return
      end subroutine umi

!=======================================================================

      subroutine vacomp(xa, ya, za, x, y, z, ix, iy, iz, ix2, iy2, iz2, &
     &                  icon, ifl, id, is, itl, ifn, ifc, vol, area,    &
     &                  ndst, nmax, nvmax, nfmax, nw, nt, nq, nb,       &
     &                  icsfig, mhalf, mfull)
!c
      real(8) :: xa(:), ya(:), za(:) !x-->xp ; y-->yp ; z-->zp
      real(8) :: x(:), y(:), z(:)
      integer*8 ix(:), iy(:), iz(:)
      integer*8 ix2(:), iy2(:), iz2(:)
      integer*8 icon(:,:), ifl(:), id(:), is(:), itl(:), ifn(:), ifc(:)
      real(8) :: vol(:), area(:), ndst(:)
      integer*8 nmax, nvmax, nfmax, nw, nt, nq, nb, icsfig, mhalf, mfull
      double precision r215, deps, dscle, dfull, dfill, decml
      integer*8 i, ng, isclu, isclp(2), isgcl
!c
!c     test number of significant figures of decimal part of coordinates
!c
      if(icsfig.lt.0 .or. icsfig.gt.9) stop 290
      isclu = 1
      dscle = 1.0d0
      if(icsfig.eq.0) go to 220
      do 210 i = 1, icsfig
         isclu = 10*isclu
         dscle = 10.0d0*dscle
  210 continue
  220 continue
      deps = 0.9d0
      if(dscle.lt.deps) stop 293
      if(abs(isclu).ge.mfull) stop 295
      call decomp(isclp, isgcl, isclu, mhalf)
      if(isgcl.ne.1) stop 297
!c
!c     test lengths of x, y, z-coordinates, shift and make them integers
!c
      dfull = dble(mfull)
      dfill=dfull/dscle
      do 235 i = 1, nt
         if(ifl(i).eq.0) cycle
         ix2(i) = 0
         iy2(i) = 0
         iz2(i) = 0
         if(dabs(x(i)).lt.dfill) then
            ix(i) = idnint(dscle*x(i))
            if(abs(ix(i)).lt.mfull) then
               x(i) = dble(ix(i))/dscle
               go to 225
            endif
         endif
         if(dabs(x(i)).ge.dfull) stop 305
         ix(i) = idint(x(i))
         if(abs(ix(i)).ge.mfull) stop 310
         decml = (x(i) - dint(x(i)))*dscle
         if(dabs(decml).ge.dfull) stop 312
         ix2(i) = idnint(decml)
         if(abs(ix2(i)).ge.mfull) stop 315
         if(abs(ix2(i)).eq.0) then
            x(i) = dble(ix(i))
            ix2(i) = mfull
         else
            x(i) = dble(ix(i)) + (dble(ix2(i))/dscle)
         endif
  225    continue
         if(dabs(y(i)).lt.dfill) then
            iy(i) = idnint(dscle*y(i))
            if(abs(iy(i)).lt.mfull) then
               y(i) = dble(iy(i))/dscle
               go to 230
            endif
         endif
         if(dabs(y(i)).ge.dfull) stop 320
         iy(i) = idint(y(i))
         if(abs(iy(i)).ge.mfull) stop 325
         decml = (y(i) - dint(y(i)))*dscle
         if(dabs(decml).ge.dfull) stop 327
         iy2(i) = idnint(decml)
         if(abs(iy2(i)).ge.mfull) stop 330
         if(abs(iy2(i)).eq.0) then
            y(i) = dble(iy(i))
            iy2(i) = mfull
         else
            y(i) = dble(iy(i)) + (dble(iy2(i))/dscle)
         endif
  230    continue
         if(dabs(z(i)).lt.dfill) then
            iz(i) = idnint(dscle*z(i))
            if(abs(iz(i)).lt.mfull) then
               z(i) = dble(iz(i))/dscle
               go to 235
            endif
         endif
         if(dabs(z(i)).ge.dfull) stop 335
         iz(i) = idint(z(i))
         if(abs(iz(i)).ge.mfull) stop 340
         decml = (z(i) - dint(z(i)))*dscle
         if(dabs(decml).ge.dfull) stop 342
         iz2(i) = idnint(decml)
         if(abs(iz2(i)).ge.mfull) stop 345
         if(abs(iz2(i)).eq.0) then
            z(i) = dble(iz(i))
            iz2(i) = mfull
         else
            z(i) = dble(iz(i)) + (dble(iz2(i))/dscle)
         endif
  235 continue
!c
!c     identify Power/Voronoi neighbor pairs
!c     compute volumes, areas, distances
!c
      ng = 0
      r215 = dble(mhalf)
!c
      call nghpair(xa, ya, za, ix, iy, iz, ix2, iy2, iz2, icon, ifl,    &
     &             is, ifn, ifc, id, itl, vol, area, ndst, nmax,        &
     &             nvmax, nfmax, nw, nt, nq, nb, ng, mhalf, mfull,      &
     &             isclp, r215, dscle, deps)
!c
      return
      end subroutine vacomp

!=======================================================================

      subroutine volare(xa, ya, za, xp, yp, zp, ix, iy, iz, ix2, iy2,   &
     &                  iz2, icon, ifl, id, is, itl, ifn, ifc, vol,     &
     &                  area, ndst, nmax, nvmax, nfmax, nw, nt, nq,     &
     &                  nb, icfig)
!c
      real(8) :: xa(:), ya(:), za(:)
      real(8) :: xp(:), yp(:), zp(:)
      integer(8) :: ix(:), iy(:), iz(:)
      integer(8) :: ix2(:), iy2(:), iz2(:)
      integer(8) :: icon(:,:),ifl(:),id(:),is(:),itl(:),ifn(:),ifc(:)
      real(8) :: vol(:), area(:), ndst(:)
      integer*8 nmax, nvmax, nfmax, nw, nt, nq, nb, icfig, mhalf, mfull
!c
!c     initialize Fortran 77 word lengths
!c
      mhalf=mhalf_I
      mfull=mfull_I
!c
!c     test parameters
!c
      if(nw.gt.nmax .or. nt.ge.nq .or. nq.gt.nvmax) stop 110
!c
!c     write(*,*)' '
!c     write(*,*)'Entering vacomp ...'
      call vacomp(xa, ya, za, xp, yp, zp, ix, iy, iz, ix2, iy2, iz2,    &
     &            icon, ifl, id, is, itl, ifn, ifc, vol, area, ndst,    &
     &            nmax, nvmax, nfmax, nw, nt, nq, nb, icfig,            &
     &            mhalf, mfull)
!c     write(*,*)' '
!c      write(*,*)'Leaving vacomp ...'
!c
      return
      end subroutine volare

!=======================================================================

      subroutine vrtarr(i2,i3,i4,b,c,d)
!c
      integer*8 b, c, d, i2, i3, i4, ix
!c
      b=i2
      c=i3
      d=i4
      ix = max0(b,c,d)
      if(b .eq. ix) go to 100
      if(c .eq. ix) then
         ix = b
         b = c
         c = d
         d = ix
      else
         ix = b
         b = d
         d = c
         c = ix
      endif
  100 continue
      if(c.gt.b .or. d.gt.b) stop 3210
!c
      return
      end subroutine vrtarr

!=======================================================================

      subroutine vrtcmp(ix, iy, iz, iw, ix2, iy2, iz2, iw2, xp, yp, zp, &
     &                  icon, ifl, is, nv, nt, nq, nvmax, dscle, delaun,&
     &                  mhalf, mfull, isclp, isclw, isclr)
!c
      integer*8 ix(:), iy(:), iz(:), iw(:)
      integer*8 ix2(:), iy2(:), iz2(:), iw2(:)
      real(8) :: xp(:), yp(:), zp(:)
      integer*8 icon(:,:), ifl(:), is(:)
      integer*8 nv, nt, nq, nvmax
      integer*8 mhalf, mfull, nkmax
      logical delaun
      integer*8 isclp(:), isclw(:), isclr(:), ikon(8,1), a, b, c
      parameter(nkmax=30)
      integer*8 io(nkmax), inx(nkmax), iny(nkmax), inz(nkmax)
      double precision r215, dnom, xnum, ynum, znum
      double precision deps, dscle, dscl2, dfull
!c
      integer*8 ng, i, j, ifir, isec, ithi, ifou, isgo, iko
      integer*8 isgnx, isgny, isgnz, iknx, ikny, iknz
      integer*8 ist1, ist2, ist3, icur, icar, now
      integer*8 one
!c
!c     compute vertices
!c
      one=1
      ng = 0
      dfull = dble(mfull) !2**30
      r215 = dble(mhalf) !2**15
      deps = dble(0.9)
      if(dscle.lt.deps) stop 400
      dscl2 = 2.0d0*dscle
!c     write(*,*)' '
      do 1000 i = 1, nt
         if(i.le.(i/10000)*10000)                                       &
     &   write(*,*)'Number of computed vertices = ',i
         xp(i) = 0.0d0
         yp(i) = 0.0d0
         zp(i) = 0.0d0
         ifir = icon(5,i) !first
         isec = icon(6,i) !second
         ithi = icon(7,i) !third
         ifou = icon(8,i) !fourth
         if(ifir.le.ng .or. isec.le.ng .or. ithi.le.ng .or.             &
     &      ifou.le.ng) stop 405
         call idenom(ix, iy, iz, ix2, iy2, iz2, ifir, isec, ithi,       &
     &               ifou, mhalf, mfull, isclp, io, isgo, iko)
!     the output from sub idenom:
!        | x'(4) y'(4) z'(4) |
!	io=| x'(2) y'(2) z'(2) |
!        | x'(3) y'(3) z'(3) |
!     where
!     x'(i)=x(i)-x(1)
!     y'(i)=y(i)-x(1)
!     z'(i)=z(i)-x(1) i={2,3,4}
!     1-->ifir, 2-->isec, 3-->ithi, 4-->ifou
         if(isgo.le.0) stop 410
         call xyznum(ix, iy, iz, iw, ix2, iy2, iz2, iw2, ifir, isec,    &
     &               ithi, ifou, mhalf, mfull, isclp, isclw, isclr,     &
     &               delaun, inx, iny, inz, isgnx, isgny, isgnz,        &
     &               iknx, ikny, iknz)
!     The output from sub xyznum:
!     inx=z'(isec2)*[iq3*y'(ifou)-iq4*y'(ithi)]+z'(ithi)*[iq4*y'(isec)-iq2*y'(ifou)]+
!         z'(ifou)*[iq2*y'(ithi)-iq3*y'(isec)]
!         | z'(isec) z'(ithi) z'(ifou) |
!	   =|    iq2      iq3      iq4   |
!         | y'(isec) y'(ithi) y'(ifou) |
!
!     iny=x'(isec)*[iq3*z'(ifou)-iq4*z'(ithi)]+x'(ithi)*[iq4*z'(isec)-iq2*z'(ifou)]+
!         x'(ifou)*[iq2*z'(ithi)-iq3*z'(isec)]
!         | x'(isec) x'(ithi) x'(ifou) |
!	   =|    iq2      iq3      iq4   |
!         | z'(isec) z'(ithi) z'(ifou) |
!
!     inz=y'(isec)*[iq3*x'(ifou)-iq4*x'(ithi)]+y'(ithi)*[iq4*x'(isec)-iq2*x'(ifou)]+
!         y'(ifou)*[iq2*x'(ithi)-iq3*x'(isec)]
!         | y'(isec) y'(ithi) y'(ifou) |
!	   =|    iq2      iq3      iq4   |
!         | x'(isec) x'(ithi) x'(ifou) |
!     where
!     iq2=R'(isec)**2-R(ifir)**2-(w2**2-w1**2)
!     iq3=R'(ithi)**2-R(ifir)**2-(w3**2-w1**2)
!     iq4=R'(ifou)**2-R(ifir)**2-(w4**2-w1**2)
!     R(i)=dsqrt(x(i)**2+y(i)**2+z(i)**2)
!     R'(i)=dsqrt(x'(i)**2+y'(i)**2+z'(i)**2)
!     x'(i)=x(i)-x(ifir)
!     y'(i)=y(i)-x(ifir)
!     z'(i)=z(i)-x(ifir) i={isec,ithi,ifou}
         call doubnm(io, isgo, iko, r215, dnom)
         if(dnom.lt.deps) stop 420
         call doubnm(inx, isgnx, iknx, r215, xnum)
         call doubnm(iny, isgny, ikny, r215, ynum)
         call doubnm(inz, isgnz, iknz, r215, znum)
         xp(i) = (xnum/dnom)/dscl2
         yp(i) = (ynum/dnom)/dscl2
         zp(i) = (znum/dnom)/dscl2
         ifl(i) = 1
 1000 continue
!c
!c     compute unbounded edges
!c
      nq = nt
!     recorded the unbounded edges after xp,yp,zp
!     ps: 200 rows for xp,yp,zp, so it recorded
!         these information at xp(nq) which nq>=201
!         nq=nt; nq=nq+1 ; nt=200
      do 1300 i = 1, nt
         do 1250 j = 1, 4
            if(icon(j,i).ne.0) go to 1250
            nq = nq + 1
            if(nq.gt.nvmax) stop 430
            icon(j,i) = nq
            if(j.eq.1)then
               ist1 = icon(6,i)
               ist2 = icon(7,i)
               ist3 = icon(8,i)
            elseif(j.eq.2)then
               ist1 = icon(7,i)
               ist2 = icon(5,i)
               ist3 = icon(8,i)
            elseif(j.eq.3)then
               ist1 = icon(8,i)
               ist2 = icon(5,i)
               ist3 = icon(6,i)
            else
               ist1 = icon(5,i)
               ist2 = icon(7,i)
               ist3 = icon(6,i)
            endif
            icon(1,nq) = i
            icon(5,nq) = 0
            icon(6,nq) = ist3
            icon(7,nq) = ist2
            icon(8,nq) = ist1
            call crossp(ix, iy, iz, ix2, iy2, iz2, ist1, ist2, ist3,    &
     &                  mhalf, mfull, isclp, inx, isgnx, iknx,          &
     &                  iny, isgny, ikny, inz, isgnz, iknz)
            call doubnm(inx, isgnx, iknx, r215, xnum)
            call doubnm(iny, isgny, ikny, r215, ynum)
            call doubnm(inz, isgnz, iknz, r215, znum)
            dnom = dmax1(dabs(xnum),dabs(ynum),dabs(znum))
            if(dnom.lt.deps) stop 435
            xnum = xnum/dnom
            ynum = ynum/dnom
            znum = znum/dnom
            dnom = dsqrt(xnum**2+ynum**2+znum**2)
            if(dnom.lt.deps) stop 440
            xp(nq) = xnum/dnom
            yp(nq) = ynum/dnom
            zp(nq) = znum/dnom
            ifl(nq) = 2
 1250    continue
 1300 continue
!c
!c     connect unbounded edges
!c
      if(nq.le.nt) stop 445
      do 1500 icur = nt+1, nq
         icar = icon(1,icur)
         a = icon(8,icur)
         b = icon(7,icur)
         c = icon(6,icur)
         do 1350 j = 1, 8
            ikon(j,1) = icon(j,icar)
 1350    continue
         ist1 = a
         ist2 = b
         ist3 = c
         call reordr(ikon,ist1,ist2,one)
         do 1375 j = 2, 4
            now = ikon(4,1)
 1360       continue
            if(now.eq.icur .or. now.eq.icar .or. now.eq.0) stop 450
            if(ifl(now).eq.2) then
               icon(j,icur) = now
            elseif(ifl(now).eq.1) then
               call reordr(icon,ist1,ist2,now)
               now = icon(4,now)
               go to 1360
            else
               stop 480
            endif
            if(j.eq.2) then
               ist1 = c
               ist2 = a
               ist3 = b
               call reordr(ikon,ist1,ist2,one)
            endif
            if(j.eq.3) then
               ist1 = b
               ist2 = c
               ist3 = a
               call reordr(ikon,ist1,ist2,one)
            endif
 1375    continue
 1500 continue
!c
!c     test consistency of tetrahedralization
!c
      call consis(icon, is, ifl, nv, nq)
!c
!c     redefine array ifl
!c
      do 1600 i = 1, nt
         ifl(i) = 1
         dnom = dmax1(dabs(xp(i)),dabs(yp(i)),dabs(zp(i)))
         if(dnom.gt.dfull) ifl(i) = 0
 1600 continue
      do 1700 i = nt+1, nq
         ifl(i) = 2
 1700 continue
!c
      return
      end subroutine vrtcmp

!=======================================================================

      subroutine vrtins (k, w, w2, icon, nvmax, tetra, curr, is, id,    &
     &                   iftal, side, ifl, newtts, ired, delaun,        &
     &                   flphis, mhalf, mfull, isclw)
!c
      integer*8 w(:), w2(:), icon(:,:), is(:), ifl(:), id(:)
      integer*8 k, nvmax, iftal, newtts, ired, mhalf, mfull, itide
      integer*8 tetra, curr, side, site1, site2, isclw(:)
      logical delaun, flphis
      integer*8 isini, isfrt, islst, iscur, indx, i, j, isadj, ilift
!c
      site1 = icon(side+4,curr)
      if(site1.le.8) stop 1010
!c
      if(delaun) go to 50
      call iwsign(w, w2, k, site1, mhalf, mfull, isclw, itide)
      if(itide .gt. 0) go to 100
   50 continue
      is(k) = -site1
      ired = 1
      go to 2000
!c
  100 continue
      if(.not.flphis) go to 1000
      ired = 0
      is(site1) = -2
      call sitord(icon, site1, curr)
      isini = curr
      tetra = tetra + 1
      if(tetra .gt. nvmax) stop 1020
      isfrt = tetra
      icon(5,isini) = -tetra
      icon(5,tetra) = isini
      islst = isini
      iscur = icon(2,islst)
      if(iscur.le.0) stop 1030
      site2 = icon(8,islst)
      is(k) = tetra
      is(site2) = tetra
      is(icon(6,islst)) = tetra
      is(icon(7,islst)) = tetra
      newtts = 1
      ifl(1) = tetra
!c
  200 continue
      call reordr(icon, site1, site2, iscur)
      if(icon(4,iscur).ne.islst) stop 1040
      tetra = tetra + 1
      if(tetra .gt. nvmax) stop 1050
      icon(5,iscur) = -tetra
      icon(5,tetra) = iscur
      islst = iscur
      indx = 2
      iscur = icon(2,islst)
      if(iscur.le.0) stop 1060
      site2 = icon(8,islst)
      is(site2) = tetra
      newtts = newtts + 1
      ifl(newtts) = tetra
      if(icon(5,iscur).gt.0) go to 200
!c
  500 continue
      if(indx .eq. 2) then
         indx = 3
         iscur = icon(3,islst)
         if(iscur.le.0) stop 1070
         site2 = icon(6,islst)
         if(icon(5,iscur).gt.0) go to 200
         go to 500
      elseif(indx .eq. 3) then
         if(islst .ne. isini) then
            iscur = islst
            islst = icon(4,iscur)
            if(icon(2,islst) .eq. iscur) then
               indx = 2
            elseif(icon(3,islst) .eq. iscur) then
               indx = 3
            elseif(icon(4,islst) .eq. iscur) then
               indx = 4
            else
               stop 1080
            endif
            go to 500
         else
            indx = 4
            iscur = icon(4,islst)
            if(iscur.le.0) stop 1090
            site2 = icon(7,islst)
            if(icon(5,iscur).gt.0) go to 200
            go to 500
         endif
      endif
      if(islst .ne. isini) stop 1110
!c
      do 800 i = isfrt, tetra
         iscur = icon(5,i)
         do 600 j = 2, 4
            icon(j,i)=-icon(5,icon(j,iscur))
  600    continue
         do 700 j = 6, 8
            icon(j,i) = icon(j,iscur)
  700    continue
  800 continue
!c
      do 900 i = isfrt, tetra
         iscur = icon(5,i)
         icon(5,i) = k
         icon(5,iscur) = -site1
         isadj = icon(1,iscur)
         icon(1,iscur) = i
         icon(2,iscur) = 0
         icon(3,iscur) = 0
         icon(4,iscur) = 0
         icon(1,i) = isadj
         if(isadj .eq. 0) go to 900
         do 840 j = 1, 4
            if(icon(j,isadj) .eq. iscur) go to 860
  840    continue
         stop 1120
  860    continue
         icon(j,isadj) = i
  900 continue
      go to 2000
!c
 1000 continue
      ired = 0
      iftal = iftal + 1
      iscur = is(site1)
      if(iscur.le.0.or.iscur.gt.tetra) stop 1130
      isini = iscur
      call sitord(icon, site1, isini)
      is(site1) = -2
      is(k) = isini
      newtts = 0
!c
 1400 continue
      newtts = newtts+1
      ifl(newtts) = iscur
      id(iscur) = iftal
      icon(5,iscur) = k
!c
      isadj = icon(2,iscur)
      if(isadj.le.0.or.isadj.gt.tetra) stop 1140
      if(id(isadj).eq.iftal) go to 1600
      ilift = icon(8,iscur)
      go to 1900
 1600 continue
      isadj = icon(3,iscur)
      if(isadj.le.0.or.isadj.gt.tetra) stop 1150
      if(id(isadj).eq.iftal) go to 1700
      ilift = icon(6,iscur)
      go to 1900
 1700 continue
      isadj = icon(4,iscur)
      if(isadj.le.0.or.isadj.gt.tetra) stop 1160
      if(iscur .eq. isini) go to 1800
      if(icon(3,isadj) .eq. iscur) then
          iscur = isadj
          go to 1700
      elseif(icon(2,isadj) .eq. iscur) then
          iscur = isadj
          go to 1600
      elseif(icon(4,isadj) .eq. iscur) then
          if(isadj .ne. isini) stop 1170
          go to 2000
      else
          stop 1180
      endif
 1800 continue
      if(id(isadj).eq.iftal) go to 2000
      ilift = icon(7,iscur)
!c
 1900 continue
      call reordr(icon, site1, ilift, isadj)
      iscur = isadj
      go to 1400
!c
 2000 continue
      return
      end subroutine vrtins

!=======================================================================

      subroutine vrtord(icon, curr, a, b, c, d)
!c
      integer*8 icon(:,:), a, b, c, d, curr, it
!c
      if(a.lt.b)then
         it=a
         a=b
         b=it
      endif
      if(a.lt.c)then
         it=a
         a=c
         c=it
      endif
      if(a.lt.d)then
         it=a
         a=d
         d=it
      endif
      if(b.lt.c) b=c
      if(b.lt.d) b=d
      call reordr(icon,a,b,curr)
      c = icon(7,curr)
      d = icon(8,curr)
      if(b.gt.a.or.c.gt.b.or.d.gt.b) stop 3110
!c
      return
      end subroutine vrtord

!=======================================================================

      subroutine xsect(la,na,lb,nb,lx,nx)
!c     ------------------------------------------------------------------
!c     find intersection of two lists of integers
!c     ------------------------------------------------------------------
      implicit double precision (a-h,o-z)
      integer*8 la(100),lb(100),lx(100)
      integer*8 na,nb,nx
      integer*8 ia,ib
      integer*8 ja,jb

      nx=0
      do ia=1,na
        ja=la(ia)

        do ib=1,nb
          jb=lb(ib)

          if(ja.eq.jb) then
            nx=nx+1
            lx(nx)=ja
          endif

        enddo
      enddo

      return
      end subroutine xsect

!=======================================================================

      subroutine xyznum(x, y, z, w, x2, y2, z2, w2, ifir, isec, ithi,   &
     &                  ifou, mhalf, mfull, isclp, isclw, isclr,        &
     &                  delaun, inx, iny, inz, isgnx, isgny, isgnz,     &
     &                  iknx, ikny, iknz)
!c
      integer*8 x(:), y(:), z(:), w(:), x2(:), y2(:), z2(:), w2(:)
      integer*8 inx(:), iny(:), inz(:)
      integer*8 isgnx, isgny, isgnz, iknx, ikny, iknz
      integer*8 ifir, isec, ithi, ifou, mhalf, mfull, nkmax
      integer*8 isclp(:), isclw(:), isclr(:)
      parameter (nkmax = 30)
      integer*8 io(nkmax), iu(nkmax)
      integer*8 iq2(nkmax), iq3(nkmax), iq4(nkmax)
      integer*8 ix2(nkmax), iy2(nkmax), iz2(nkmax)
      integer*8 ix3(nkmax), iy3(nkmax), iz3(nkmax)
      integer*8 ix4(nkmax), iy4(nkmax), iz4(nkmax)
      integer*8 ixf(nkmax), iyf(nkmax), izf(nkmax)
      integer*8 ixf2(nkmax), iyf2(nkmax), izf2(nkmax)
      integer*8 iwf(nkmax), iw2(nkmax), iw3(nkmax), iw4(nkmax)
      logical delaun
      integer*8 iwfuw, iwsew, iwthw, iwfow
      integer*8 ixfuw, iyfuw, izfuw, ixsew, iysew, izsew
      integer*8 ixthw, iythw, izthw, ixfow, iyfow, izfow
      integer*8 iwfu2, iwse2, iwth2, iwfo2
      integer*8 ixfu2, iyfu2, izfu2, ixse2, iyse2, izse2
      integer*8 ixth2, iyth2, izth2, ixfo2, iyfo2, izfo2
      integer*8 isgw2, isgw3, isgw4, ikw2, ikw3, ikw4
      integer*8 isgq2, isgq3, isgq4, ikq2, ikq3, ikq4
      integer*8 isgxf, isgyf, isgzf, ikxf, ikyf, ikzf
      integer*8 isgxf2, isgyf2, isgzf2, ikxf2, ikyf2, ikzf2
      integer*8 isgx2, isgy2, isgz2, ikx2, iky2, ikz2
      integer*8 isgx3, isgy3, isgz3, ikx3, iky3, ikz3
      integer*8 isgx4, isgy4, isgz4, ikx4, iky4, ikz4
      integer*8 isgo, isgu, iko, iku
      integer*8 isgwf, isgcl, ikwf, ikcl
!c
      if(delaun) then
         isgw2 = 0
         isgw3 = 0
         isgw4 = 0
      else
         iwfuw = w(ifir)
         iwsew = w(isec)
         iwthw = w(ithi)
         iwfow = w(ifou)
!c
         iwfu2 = w2(ifir)
         iwse2 = w2(isec)
         iwth2 = w2(ithi)
         iwfo2 = w2(ifou)
!c
         call decmp2(iwf,isgwf,ikwf, iwfuw,iwfu2, mhalf, mfull, isclw)
         isgcl = 1 !iwf=w(1st)
         ikcl = 2
         call decmp2(io, isgo, iko, iwsew, iwse2, mhalf, mfull, isclw)
          !io=w(2nd)
         call muldif(io, iwf, iu, isgo, isgwf, isgu, iko, ikwf, iku,    &
     &                 nkmax, mhalf) !iu=w(2nd)-w(1st)=w'(2nd)
         call mulmul(iu, isclr, iw2, isgu, isgcl, isgw2, iku, ikcl,     &
     &                 ikw2, nkmax, mhalf) !iw2=iu*isclr=w'(2nd)*10**(icfig)
         call decmp2(io, isgo, iko, iwthw, iwth2, mhalf, mfull, isclw)
          !io=w(3rd)
         call muldif(io, iwf, iu, isgo, isgwf, isgu, iko, ikwf, iku,    &
     &                 nkmax, mhalf) !iu=w(3rd)-w(1st)=w'(3rd)
         call mulmul(iu, isclr, iw3, isgu, isgcl, isgw3, iku, ikcl,     &
     &                 ikw3, nkmax, mhalf) !iw3=iu*isclr=w'(3rd)*10**(icfig)
         call decmp2(io, isgo, iko, iwfow, iwfo2, mhalf, mfull, isclw)
          !io=w(4th)
         call muldif(io, iwf, iu, isgo, isgwf, isgu, iko, ikwf, iku,    &
     &                 nkmax, mhalf) !iu=w(4th)-w(1st)=w'(4th)
         call mulmul(iu, isclr, iw4, isgu, isgcl, isgw4, iku, ikcl,     &
     &                 ikw4, nkmax, mhalf) !iw4=iu*isclr=w'(4th)*10**(icfig)
      endif
!c
      ixfuw = x(ifir)
      iyfuw = y(ifir)
      izfuw = z(ifir)
      ixsew = x(isec)
      iysew = y(isec)
      izsew = z(isec)
      ixthw = x(ithi)
      iythw = y(ithi)
      izthw = z(ithi)
      ixfow = x(ifou)
      iyfow = y(ifou)
      izfow = z(ifou)
!c
      ixfu2 = x2(ifir)
      iyfu2 = y2(ifir)
      izfu2 = z2(ifir)
      ixse2 = x2(isec)
      iyse2 = y2(isec)
      izse2 = z2(isec)
      ixth2 = x2(ithi)
      iyth2 = y2(ithi)
      izth2 = z2(ithi)
      ixfo2 = x2(ifou)
      iyfo2 = y2(ifou)
      izfo2 = z2(ifou)
!c
      call decmp2(ixf, isgxf, ikxf, ixfuw, ixfu2, mhalf, mfull, isclp)
      call decmp2(iyf, isgyf, ikyf, iyfuw, iyfu2, mhalf, mfull, isclp)
      call decmp2(izf, isgzf, ikzf, izfuw, izfu2, mhalf, mfull, isclp)
      call mulmul(ixf, ixf, ixf2, isgxf, isgxf, isgxf2, ikxf, ikxf,     &
     &              ikxf2, nkmax, mhalf) !ixf2=ixf**2
      call mulmul(iyf, iyf, iyf2, isgyf, isgyf, isgyf2, ikyf, ikyf,     &
     &              ikyf2, nkmax, mhalf) !iyf2=iyf**2
      call mulmul(izf, izf, izf2, isgzf, isgzf, isgzf2, ikzf, ikzf,     &
     &              ikzf2, nkmax, mhalf) !izf2=izf**2
      if(isgxf2.lt.0 .or. isgyf2.lt.0 .or. isgzf2.lt.0) stop 5105
!c
      call frterm(ixsew, iysew, izsew, ixf, iyf, izf, isgxf, isgyf,     &
     &            isgzf, ikxf, ikyf, ikzf, ixf2, iyf2, izf2,            &
     &            isgxf2, isgyf2, isgzf2, ikxf2,                        &
     &            ikyf2, ikzf2, iw2, ix2, iy2, iz2, iq2, isgw2, isgx2,  &
     &            isgy2, isgz2, isgq2, ikw2, ikx2, iky2, ikz2, ikq2,    &
     &            mhalf, mfull, ixse2, iyse2, izse2, isclp)
!     ix2=ix2nd-ix1st=ix2nd-ixf
!     iy2=iy2nd-iy1st=iy2nd-iyf
!     iz2=iz2nd-iz1st=iz2nd-izf
!     iq2=(ix2nd**2-ixf**2)+(iy2nd**2-iyf**2)+(iz2nd**2-izf**2)-iw2
!	   =R(2)**2-R(1)**2-iw2
!     watch out! this iw2 is not the iw2 we use in sub vrtcmp!
!     this iw2-->w'(2nd)=w(2nd)-w(1st)
!c
      call frterm(ixthw, iythw, izthw, ixf, iyf, izf, isgxf, isgyf,     &
     &            isgzf, ikxf, ikyf, ikzf, ixf2, iyf2, izf2,            &
     &            isgxf2, isgyf2, isgzf2, ikxf2,                        &
     &            ikyf2, ikzf2, iw3, ix3, iy3, iz3, iq3, isgw3, isgx3,  &
     &            isgy3, isgz3, isgq3, ikw3, ikx3, iky3, ikz3, ikq3,    &
     &            mhalf, mfull, ixth2, iyth2, izth2, isclp)
!     ix3=ix3rd-ix1st=ix3rd-ixf
!     iy3=iy3rd-iy1st=iy3rd-iyf
!     iz3=iz3rd-iz1st=iz3rd-izf
!     iq3=(ix3rd**2-ixf**2)+(iy3rd**2-iyf**2)+(iz3rd**2-izf**2)-iw3
!	   =R(3)**2-R(1)**2-iw3
!c
      call frterm(ixfow, iyfow, izfow, ixf, iyf, izf, isgxf, isgyf,     &
     &            isgzf, ikxf, ikyf, ikzf, ixf2, iyf2, izf2,            &
     &            isgxf2, isgyf2, isgzf2, ikxf2,                        &
     &            ikyf2, ikzf2, iw4, ix4, iy4, iz4, iq4, isgw4, isgx4,  &
     &            isgy4, isgz4, isgq4, ikw4, ikx4, iky4, ikz4, ikq4,    &
     &            mhalf, mfull, ixfo2, iyfo2, izfo2, isclp)
!     ix4=ix4th-ix1st=ix4th-ixf
!     iy4=iy4th-iy1st=iy4th-iyf
!     iz4=iz4th-iz1st=iz4th-izf
!     iq4=(ix4th**2-ixf**2)+(iy4th**2-iyf**2)+(iz4th**2-izf**2)-iw4
!	   =R(4)**2-R(1)**2-iw4
!c
      call detrm3(iq2, iy2, iz2, isgq2, isgy2, isgz2,                   &
     &            iq3, iy3, iz3, isgq3, isgy3, isgz3,                   &
     &            iq4, iy4, iz4, isgq4, isgy4, isgz4,                   &
     &            ikq2, ikq3, ikq4, iky2, iky3, iky4,                   &
     &            ikz2, ikz3, ikz4, inx, isgnx, iknx, mhalf)
      !inx=iz2*(iq3*iy4-iq4*iy3)+iz3*(iq4*iy2-iq2*iy4)+iz4*(iq2*iy3-iq3*iy2)
      !   =[z2,z3,z4]dot[q2,q3,q4]cross[y2,y3,y4]
!c
      call detrm3(iq2, iz2, ix2, isgq2, isgz2, isgx2,                   &
     &            iq3, iz3, ix3, isgq3, isgz3, isgx3,                   &
     &            iq4, iz4, ix4, isgq4, isgz4, isgx4,                   &
     &            ikq2, ikq3, ikq4, ikz2, ikz3, ikz4,                   &
     &            ikx2, ikx3, ikx4, iny, isgny, ikny, mhalf)
      !iny=ix2*(iq3*iz4-iq4*iz3)+ix3*(iq4*iz2-iq2*iz4)+ix4*(iq2*iz3-iq3*iz2)
      !   =[x2,x3,x4]dot[q2,q3,q4]cross[z2,z3,z4]
!c
      call detrm3(iq2, ix2, iy2, isgq2, isgx2, isgy2,                   &
     &            iq3, ix3, iy3, isgq3, isgx3, isgy3,                   &
     &            iq4, ix4, iy4, isgq4, isgx4, isgy4,                   &
     &            ikq2, ikq3, ikq4, ikx2, ikx3, ikx4,                   &
     &            iky2, iky3, iky4, inz, isgnz, iknz, mhalf)
      !inz=iy2*(iq3*ix4-iq4*ix3)+iy3*(iq4*ix2-iq2*ix4)+iy4*(iq2*ix3-iq3*ix2)
      !   =[y2,y3,y4]dot[q2,q3,q4]cross[x2,x3,x4]
!c
      !ixf:ix1st ; iyf:iy1st ; izf:iz1st
      !iq2=(ix2i**2-ixf**2)+(iy2i**2-iyf**2)+(iz2i**2-izf**2)-iw2i
      !iq3=(ix3i**2-ixf**2)+(iy3i**2-iyf**2)+(iz3i**2-izf**2)-iw3i
      !iq4=(ix4i**2-ixf**2)+(iy4i**2-iyf**2)+(iz4i**2-izf**2)-iw4i
      !inx=iz2i*(iq3*iy4i-iq4*iy3i)+iz3i*(iq4*iy2i-iq2*iy4i)+iz4i*(iq2*iy3i-iq3*iy2i)
      !   =[z2,z3,z4]dot[q2,q3,q4]cross[y2,y3,y4]
      !iny=ix2i*(iq3*iz4i-iq4*iz3i)+ix3i*(iq4*iz2i-iq2*iz4i)+ix4i*(iq2*iz3i-iq3*iz2i)
      !   =[x2,x3,x4]dot[q2,q3,q4]cross[z2,z3,z4]
      !inz=iy2i*(iq3*ix4i-iq4*ix3i)+iy3i*(iq4*ix2i-iq2*ix4i)+iy4i*(iq2*ix3i-iq3*ix2i)
      !   =[y2,y3,y4]dot[q2,q3,q4]cross[x2,x3,x4]
      return
      end subroutine xyznum

!=======================================================================

      end module bernal_vp
