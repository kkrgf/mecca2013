!BOP
!!ROUTINE: VPI_const
!!INTERFACE:
       subroutine VPI_const(MNV,MNF,MNqp,                               &
     &                      tnAtom,nsublat,lattice,transV,basisV,iType, &
     &                    nshell,radshl,rmt_ovlp,vp_V,r_circ,fcount,    &
     &                      small,weight,rmag,vj,nunit,istop)
!!DESCRIPTION:
! computes VP data, including weights, quadrature points and jacobians
! for isoparametric 3d integration over interstitial region \\
! (fortran unit {\tt nunit} is used to write out VP faces and vertices data)
!

!!DO_NOT_PRINT
         implicit none
!!DO_NOT_PRINT

!!ARGUMENTS:
        integer, intent(in) ::  MNV   ! maximum number of VP vertices
        integer, intent(in) ::  MNF   ! maximum number of VP faces
        integer, intent(in) ::  MNqp  ! number of quadrature points
!                                     ! (for isoparametric integration)
        integer, intent(in) ::  tnAtom,nsublat  ! total number of sites
!                                               ! and sublattices in the cell
        real(8), intent(in) ::  lattice(3)  ! a, b/a, c/a for the cell
        real(8), intent(in) ::  transV(3,3),basisV(3,tnAtom)
        integer, intent(in) ::  iType(tnAtom)   ! list of equivalent sites
        integer, intent(in) ::  nshell
        real(8), intent(in) ::  radshl(nshell,tnAtom)
        real(8), intent(inout) ::  rmt_ovlp(nsublat) ! weights (radii) used for Bernall code,
!                                                    ! spheres should not overlapp
        real(8), intent(out) ::  vp_V(nsublat)
        real(8), intent(out) ::  r_circ(nsublat)
        integer, intent(out) ::  fcount(nsublat)
        real(8), intent(in)  ::  small        ! if input rmt_ovlp cannot be bigger than "small"
!                                             ! then rmt_ovlp is replaced by half of NN distance
        real(8), intent(out) ::  weight(MNqp)
        real(8), intent(out) ::  rmag(0:3,MNqp,MNqp,MNqp,MNF,nsublat)
        real(8), intent(out) ::  vj(MNqp,MNqp,MNqp,MNF,nsublat)
        integer, intent(in)  :: nunit
        character(10), intent(in) :: istop
!!REVISION HISTORY:
! Adapted - A.S. - 2013
!EOP
!
! tnAtom(in VPI)=nAtomInCell(in Bernal)
!
! r(tnAtom)
!        muffin-tin radius for each atom in unit cell (in unit of a)
!
! iType
!        List of the equivalent atoms.
!        e.g.: if there are 7 atoms a unit cell, and 1st, 6th and 7th
!              atoms are sub-lattice 1, 2nd and 3rd are sub-lattice 2,
!              4th is sub-lattice 4, 5th is sub-lattice 3. Then iType=
!              {1, 2, 2, 4, 3, 1, 1}
!              ps: don't need to be in a sequence.
!-----------------------------------------------------------------------
!
!BOC
!
        integer i,ksub
        real*8 r(tnAtom)
!     &         rmt_ovlp(ipcomp,*),                                      &
        real*8 rmtin(nsublat)
        !---
        integer VP_nVertex1(tnAtom)
        integer VP_nVertex2(tnAtom)
        integer VP_Ivdex2(tnAtom,MNV,2)
        integer VP_nfaces(tnAtom)
        integer VP_Iface(tnAtom,MNF,MNV+1)
        real(8) :: VP_vertices(tnAtom,3,MNV)
        real(8) :: cir(tnAtom),vp_volum(tnAtom)
        real(8) :: convDU,scale0,scale_xyz(3),cellvolm
        real(8) :: transV_(3,3),basisV_(3,size(basisV,2))
            logical lop
            integer j,k,nr
!            integer kz
!            real(8) :: vv,dV
!            real(8) :: points(3,4*MNF)

        character sname*10
        parameter (sname='VPI_const')
!*****************************************************
        rmtin(1:nsublat) = rmt_ovlp(1:nsublat)

!            do i=1,nsublat
!              do j=1,komp(i)
!               rmtin(i)=rmt_ovlp(j,i)
!              enddo
!            enddo

!!         r(1:tnAtom) = rmtin(iType(1:tnAtom))
!         call rmtpower(rmtin,r,lattice,iType,nsublat,tnAtom)

!------[input]-----------------------------------------
!C    CHK If the MT-radii is UNPHYSICAL e.g. too small or too BIG
!C     Although it is very unlikely, but might happen sometime
!C                    UNDER extreme condition
!------[input]-----------------------------------------
!              do i=1,tnAtom
!               ityp=iType(i)
!                rmtin(ityp)=r(i)*lattice(1)
!              enddo

        call RNNo2(nsublat,nshell,tnAtom,rmtin,radshl,iType,small)
        scale_xyz(1) = 1.d0
        scale_xyz(2:3) = lattice(2:3)
        do i=1,3
         transV_(i,1:3) = transV(i,1:3)*scale_xyz(i)
         basisV_(i,1:tnAtom) = basisV(i,1:tnAtom)*scale_xyz(i)
        end do
        cellvolm =                                                      &
     &    ( transV_(2,1)*transV_(3,2) - transV_(3,1)*transV_(2,2) )     &
     &         * transV_(1,3) +                                         &
     &    ( transV_(3,1)*transV_(1,2) - transV_(1,1)*transV_(3,2) )     &
     &         * transV_(2,3) +                                         &
     &    ( transV_(1,1)*transV_(2,2) - transV_(2,1)*transV_(1,2) )     &
     &         * transV_(3,3)
        scale0 = max(1.d0,cellvolm**(1.d0/3.d0))
        convDU = scale0*lattice(1)

        r(1:tnAtom) = rmtin(iType(1:tnAtom))/convDU
        transV_ = transV_/scale0
        basisV_ = basisV_/scale0

!-----[generate the structure]--------------

        call VPI_Bernal(MNV,MNF,                                        &
     &                  tnAtom,transV_,basisV_,r,                       &
     &                VP_nVertex1,VP_nVertex2,VP_Ivdex2,                &
     &                VP_nfaces,VP_Iface,VP_vertices,vp_volum,cir,istop)

        write(*,*) ' Done VP tessellation' ! DEBUG

!-----[generate necessary info for VP Integ]------------
        call VPI_main(MNV,MNF,MNqp,                                     &
     &                tnAtom,convDU,r,rmtin,nsublat,iType,              &
     &              VP_nVertex1,VP_nVertex2,VP_Ivdex2,VP_nfaces,        &
     &            VP_Iface,VP_vertices,fcount,weight,rmag,vj,istop)
            write(6,*) ' Done VP integration setup' ! DEBUG
            r_circ(1:nsublat) = -1.d0
            vp_V = 0
            do i=1,tnAtom
              ksub=iType(i)
              if ( r_circ(ksub) .le. 0.d0 ) then
                  r_circ(ksub) = cir(i)*convDU
                  vp_V(ksub) = vp_volum(i)*convDU**3
              end if
            enddo
            rmt_ovlp(1:nsublat) = rmtin(1:nsublat)

           inquire(unit=nunit,opened=lop)
           if ( lop ) then
            nr = 0
            do ksub=1,nsublat
             do i=1,tnAtom
               if ( ksub==iType(i) ) exit
             end do
             if ( i>tnAtom) then
              call fstop(sname//' ERROR: inconsistent input data???')
             end if
!DEBUG             vv = 0
             write(nunit) ksub,VP_nfaces(i)
             nr = nr+1
             write(nunit) VP_Iface(i,1:VP_nfaces(i),1)
             nr = nr+1
             do j=1,VP_nfaces(i)
              write(nunit) (VP_vertices(i,1:3,VP_Iface(i,j,k+1)),       &
     &                                      k=1,VP_Iface(i,j,1))
              nr = nr+1
!DEBUG              do k=1,VP_Iface(i,j,1)
!DEBUG               points(1:3,k) = VP_vertices(i,1:3,VP_Iface(i,j,k+1))
!DEBUG              end do
!DEBUG
!DEBUG              write(nunit) (points(1:3,k),k=1,VP_Iface(i,j,1))
!DEBUG
!DEBUG              call CalcVol(VP_Iface(i,j,1),points,dV)
!DEBUG              vv = vv+dV
             end do
            end do
            do i=1,nr
             backspace(nunit)
            end do
           end if


!            do i=1,tnAtom
!              ksub=iType(i)
!              do j=1,komp(ksub)
!               rmt_ovlp(j,ksub)=rmtin(ksub)
!               r_circ(j,ksub)=cir(i)*lattice(1)
!              enddo
!            enddo

           if (istop.eq.sname) then
             call fstop(sname)
           endif

        write(*,*) ' Done VP-shapes file' ! DEBUG

        return

!EOC
        CONTAINS

!!BOP
!!!IROUTINE: rmtpower
!!!INTERFACE:
!      subroutine rmtpower(rmt_ovlp,r,lattice,iType,nsublat,tnAtom)
!!!DESCRIPTION:
!! generates radiii {\tt r} (used as weights in VP tessalation) for
!! all {\tt tnAtom} sites in the cell
!!
!
!!!DO_NOT_PRINT
!         implicit none
!!!DO_NOT_PRINT
!
!!!ARGUMENTS:
!       integer, intent(in) :: nsublat,tnAtom
!       integer, intent(in) :: iType(tnAtom)
!       real*8, intent(in) ::  rmt_ovlp(nsublat),lattice(3)
!       real*8, intent(out) ::  r(tnAtom)
!!!REMARKS:
!! private procedure of subroutine VPI_const
!!EOP
!!
!!BOC
!       real*8 fsq,xf,rmin,rmax
!       real*8 rmt(nsublat)
!       integer i
!!       integer barr(nsublat)
!!       real*8 rmt1(nsublat)
!
!       rmt(1:nsublat)=rmt_ovlp(1:nsublat)/lattice(1)
!!       rmt1(1:nsublat)=rmt(1:nsublat)
!
!!       call sort2(nsublat,rmt1,barr)
!
!       rmin = minval(rmt(1:nsublat))
!       rmax = maxval(rmt(1:nsublat))
!
!!         rmin = rmt1(1)
!!c         rmax = rmt1(2)
!!         rmax = rmt1(nsublat)
!
!       fsq=(rmax/rmin)**2
!       xf=(rmin*fsq-rmax)/(fsq + 1.0d0)
!       do i=1,nsublat
!          if(dabs(rmt(i)-rmin).lt.1.0d-4)then
!             rmt(i)=rmin - xf
!          elseif(dabs(rmt(i)-rmax).lt.1.0d-4)then
!             rmt(i)=rmax + xf
!          else
!             fsq=(rmt(i)/(rmax-xf))**2
!             rmt(i)=fsq*(rmax-xf)
!          endif
!!           if(i.eq.nsublat)then
!!            write(6,*)
!!           endif
!       enddo
!
!       r(1:tnAtom) = rmt(iType(1:tnAtom))
!
!       return
!!EOC
!      end subroutine rmtpower

!BOP
!!IROUTINE: RNNo2
!!INTERFACE:
      subroutine RNNo2(nsublat,nshell,tnAtom,rmtin,radshl,iType,small)
!!DESCRIPTION:
! checks consistency of input radii {\tt rmtin}:
! if {\tt rmtin} cannot be bigger than {\tt small}
! then value of {\tt rmtin} is replaced by half of NN distance
! defined by {\tt radshl}
!
!!REMARKS:
! private procedure of subroutine VPI_const
!EOP
!
!BOC
        implicit none
        integer  nshell
        integer nsublat,tnAtom
        integer iType(tnAtom)
        real*8 rmtin(nsublat),radshl(nshell,tnAtom)
        real*8 small
!        real*8 rmin0, rmax0
        integer i,j

!        rmin0 = minval(rmtin(1:nsublat))
!        rmax0 = maxval(rmtin(1:nsublat))

        if ( nshell>1 ) then
         do i=1,tnAtom
          if ( radshl(2,i) > 2*small ) then
           j = iType(i)
           rmtin(j) = min(rmtin(j),radshl(2,i)-small)
           rmtin(j) = max(rmtin(j),small)
          end if
         enddo
        else
         do i=1,tnAtom
           j = iType(i)
           rmtin(j) = max(rmtin(j),small)
         enddo
        end if

        return
!EOC
       end subroutine RNNo2

       end subroutine VPI_const
