!BOP
!!ROUTINE: VPI_Bernal
!!INTERFACE:
        subroutine VPI_Bernal(MNV,MNF,                                  &
     &             nAtomInCell,transV,basisV,                           &
     &             r_wghts,VP_nVertex1,VP_nVertex2,VP_Ivdex2,           &
     &                VP_nfaces,VP_Iface,VP_vertices,vp_volum,cir,istop)
!!DESCRIPTION:
! calls Voronoi tessellation code and prepares its output for further
! use in {\sc mecca}
!
!!USES:
        USE bernal_vp

!!DO_NOT_PRINT
        implicit none
!!DO_NOT_PRINT

!!ARGUMENTS:
        integer, intent(in) :: MNV    ! maximum number of VP vertices
        integer, intent(in) :: MNF    ! maximum number of VP faces
        integer, intent(in) :: nAtomInCell ! number of sites in the cell
        real(8), intent(in) :: transV(3,3) ! RS translation vectors (in units of scale0)
        real(8), intent(in) :: basisV(3,nAtomInCell) ! sites positions in the cell
!                                                    ! in the same units as transV
        real(8), intent(in) ::  r_wghts(nAtomInCell)  ! (muffin-tin) radius-weight
!                                                     ! for each site in unit cell
!                                                     ! in the same units as transV
!   OUTPUT is in DU of "scale0"
        integer, intent(out) :: VP_nVertex1(nAtomInCell) ! number of
!                                                        ! non-degenerate vertices
        integer, intent(out) :: VP_nVertex2(nAtomInCell) ! number of
!                                                        ! degenerare vertices
        integer, intent(out) :: VP_Ivdex2(nAtomInCell,MNV,2) ! index of
!                                                        ! degenerate vertices
        integer, intent(out) :: VP_nfaces(nAtomInCell)   ! number of
!                                                        ! faces for each site
        integer, intent(out) :: VP_Iface(nAtomInCell,MNF,MNV+1) ! face-index(fdex)
!                                                        ! and vertices-set(vdex_set)
        real(8), intent(out) :: VP_vertices(nAtomInCell,3,MNV) ! vertices description
!                                                        ! (in units of "alat"
        real(8) ,intent(out) :: vp_volum(nAtomInCell)    ! VP volume, in units sum(vp_vol)==cell_volume
        real(8) ,intent(out) :: cir(nAtomInCell)         ! circumscr. radii
        character(10), intent(in) :: istop
!
!!REVISION HISTORY:
! Initial version - Aftab Alam - 2010
! Adapted - A.S. - 2013
!!REMARKS:
! file "Bern_out" with details of VP tessellation is deleted by default;
! it can be prevented by setting env.variable ENV_BERN_OUT
!EOP
!
! VP_Ivdex2(nAtomInCell,MNV,2) ; integer
!        index for degenerate vertices
!        e.g.: if vertex 1(non-degen.) & 19(degen.) in atom-1 are identical
!           ==>VP_Ivdex2(1,i,1)=1 ; VP_Ivdex2(1,i,2)=19
!              i is some number depend on the sequence of output from Bernal
!
! VP_Iface(nAtomInCell,MNF,MNV+1) ; integer
!        face-index(fdex) and vertices-set(vdex_set)
!        e.g.: for atom-i, if fdex 5 consists 6 vertices said {4  6 13 15 17 19}
!           ==>VP_Iface(i,5,1)= 6 (# of vertices) <-- this is the reason why the 3rd
!                                                     dimension is MNV+1
!              VP_Iface(i,5,2)= 4 (1st vdex)
!              VP_Iface(i,5,3)= 6 (2nd vdex)
!              VP_Iface(i,5,4)=13 (3rd vdex)
!              VP_Iface(i,5,5)=15 (4th vdex)
!              VP_Iface(i,5,6)=17 (5th vdex)
!              VP_Iface(i,5,7)=19 (6th vdex)
!        ps: Bernal's code has a limit on number of vertices for a face!
!            the limit should be 30.
!            ==> The correspond code in subroutine output:
!                -------
!                write(3,323) n,nvrtFace(n),(ivrtFace(m,n),m=1,nvrtFace(n))
!                  VP_Iface(i,count,1)=nvrtFace(n) !for VP_integrate
!                  VP_Iface(i,count,2:nvrtFace(n)+1)=ivrtFace(1:nvrtFace(n),n)
!                323        format(i5,2x,i2,5x,30(1x,i2))
!                -------
!                First, nvrtFace(mxFace)-->mxFace=50.
!                Second, the format 323 limit the output within 30 times!
!
! VP_vertices(nAtomInCell,3,MNV) ; real*8
!        vertices information which in unit of "a"
!        e.g.: from atom-i, if 13th vertex is listed as follow
!              vdex        relative position
!               13   0.0D+00    0.0D+00    5.0D-01
!           ==>VP_vertices(i,1,13)=0.0D+00 <-- x component
!              VP_vertices(i,2,13)=0.0D+00 <-- y component
!              VP_vertices(i,3,13)=5.0D-01 <-- z component
!
!BOC
        integer :: funit(5)
        integer :: out_un
        character(10), parameter :: sname='VPI_Bernal'
        character(*), parameter :: PNTS_W='#pnts_wght.tmp'
        character(2) :: ctmp
        integer :: st_env,i,iat,j,nsub
        integer :: nnatom,itype(nAtomInCell),itypenn(4*MNF)
        real(8) :: rmax,xyznn(3,size(itypenn,1)),w_tmp(size(itypenn,1))
        real(8) :: point0(3),point(3),sites(3,nAtomInCell)

        logical :: HighP,nn_cluster=.true.

        interface
      subroutine VP_output(MNV,MNF,VP_nVertex1,VP_nVertex2,             &
     &            VP_Ivdex2,VP_nfaces,VP_Iface,                         &
     &            VP_vertices,vp_volum,cir,nAtomInCell,funit)
      implicit none
      integer, intent(in) :: MNV,MNF
      integer, intent(in) :: nAtomInCell
      integer, intent(inout) :: funit(:)
      integer, intent(out) :: VP_nVertex1(nAtomInCell)
      integer, intent(out) :: VP_nVertex2(nAtomInCell)
      integer, intent(out) :: VP_Ivdex2(nAtomInCell,MNV,2)
      integer, intent(out) :: VP_nfaces(nAtomInCell)
      integer, intent(out) :: VP_Iface(nAtomInCell,MNF,MNV+1)
      real(8), intent(out) :: VP_vertices(nAtomInCell,3,MNV)
      real(8), intent(out) :: vp_volum(nAtomInCell)
      real(8), intent(out) :: cir(nAtomInCell)
      end subroutine VP_output
        end interface

        HighP=.true.
        VP_nVertex1=0
        VP_nVertex2=0
        VP_Ivdex2=0
        VP_nfaces=0
        VP_Iface=0
        VP_vertices=0.0d0

        funit(1) = tetrahedr_un
        funit(2) = pnts_wght_un
        funit(3) = power_vrt_un
        funit(4) = areas_vol_un
        out_un = max(20,maxval(funit(1:4))+1)
        funit(5) = out_un
        call get_environment_variable('ENV_BERN_OUT',status=st_env)
        if ( st_env==0 ) then
         open (out_un, file = 'Bern_out')
        else
         open (out_un, status='SCRATCH')
        end if

        call gEnvVar('ENV_NN_CLUSTER',.true.,st_env)
        if ( st_env==1 ) then
         nn_cluster = .true.
        else if (st_env==0) then
        nn_cluster = .false.
        end if

      if ( nn_cluster ) then
       do iat=1,nAtomInCell
        itype(iat) = iat
       end do
       call alloc_berncom(size(itypenn))
       open(unit=pnts_wght_un,status='scratch',form='formatted')
!       open(unit=pnts_wght_un,file=PNTS_W,form='formatted')
       point0(1:3) = 0
       do iat = 1,nAtomInCell
!        write(6,*) ' ....IAT=',iat
        nsub=iat
        point(1:3) = basisV(1:3,iat)
        do j=1,nAtominCell
         sites(1:3,j) = basisV(1:3,j) - point(1:3)
        end do
        call neighbors(nsub,point0,                                     &
     &                     transV,nAtomInCell,sites,itype,              &
     &                     size(itypenn),nnatom,xyznn,itypenn,rmax)
        w_tmp(1:nnatom) = r_wghts(itypenn(1:nnatom))**2

        rewind(unit=pnts_wght_un)

        do i=1,nnatom
         write(pnts_wght_un,202) xyznn(1:3,i),w_tmp(i)
 202     format(4(2x,d23.16))
        enddo
        rewind(pnts_wght_un)
!
!------------------------------- Bernal's subroutines calls
        call drive_regtet
        call drive_pwrvtx(HighP)
        call drive_volare(.true.)
!-------------------------------
        call VP_output(MNV,MNF,VP_nVertex1(iat),VP_nVertex2(iat),       &
     &            VP_Ivdex2(iat,:,:),VP_nfaces(iat),VP_Iface(iat,:,:),  &
     &            VP_vertices(iat,:,:),vp_volum(iat),cir(iat),1,funit)
       end do
      else
       call alloc_berncom(nAtomInCell)
       call supercell(transV,basisV,nAtomInCell,r_wghts)
!
!------------------------------- Bernal's subroutines calls
       call drive_regtet
       call drive_pwrvtx(HighP)
       call drive_volare
!-------------------------------
       call VP_output(MNV,MNF,VP_nVertex1,VP_nVertex2,                  &
     &            VP_Ivdex2,VP_nfaces,VP_Iface,                         &
     &            VP_vertices,vp_volum,cir,nAtomInCell,funit)

!      call output_VP(nAtomInCell,MNV,VP_vertices,lattice)
!
      end if

      if ( funit(5).ne.out_un ) stop 'UNEXPECTED ERROR in VP_OUTPUT'

      if (istop.eq.sname) then
         call fstop(sname)
      endif

      call alloc_berncom(-1)
      call delete_bernal_vp_files
      close(unit=out_un)

      return
!EOC
      CONTAINS

!      subroutine output_VP(tnAtom,MNV,vertices,lattice)
!      implicit none
!        integer*8 i,tnAtom,nAtom,MNV
!        real*8 vertices(tnAtom,3,MNV),x,y,z,lattice(3)
!        character(len=1) c1,c2,c3
!        character(len=8) name
!        integer*8 N1,N10,N100
!
!      if(tnAtom>=1000) stop 'the lengh of the file name is too short!'
!        do 10 nAtom=1,tnAtom
!       N100=nAtom/100
!       N10=(nAtom-100*N100)/10
!       N1=int(nAtom-N10*10-N100*100)
!       c1=char(N100+48)
!       c2=char(N10+48)
!       c3=char(N1+48)
!       open(unit=1,file="VP for atom-"//c1//c2//c3//".dat",
!     &                   status='unknown')
!
!         do 20 i=1,MNV
!          x=vertices(nAtom,1,i)
!          y=vertices(nAtom,2,i)
!          z=vertices(nAtom,3,i)
!          if(x==0.0d0 .and. y==0.0d0 .and. z==0.0d0) goto 20
!          write(1,"(3(2x,d23.16))") x,y,z
!   20    continue
!      close(1)
!   10 continue
!      !Be careful, vertices are not written in sequence, so there may
!        !have (0,0,0) between valid vetices(i,j,k)
!        !e.g.: If there are 6 vertices for atom-1, and the index are 1,2
!        !      3,4,5,7. Then vertices(1,:,6)=(0,0,0)
!
!        return
!        end subroutine output_VP

!BOP
!!IROUTINE: supercell
!!INTERFACE:
      subroutine supercell(t,basisV,nAtomInCell,r_wghts)
!!DESCRIPTION:
! computes sites positions in the cell and its nearest PBC replicas and
! saves the result in file "pnts-wghts" for further use by Bernal's
! Voronoi tessellation code
!

!!DO_NOT_PRINT
      implicit none
!!DO_NOT_PRINT

!!ARGUMENTS:
      real(8), intent(in) :: t(3,3)      ! transV - lattice vectors ("scale0" units)
      integer, intent(in) :: nAtomInCell
      real(8), intent(in) :: basisV(3,nAtomInCell) ! site positions in the cell
      real(8), intent(in) :: r_wghts(nAtomInCell)  ! radii-weights
!!REMARKS:
! fortran unit 1 is in use;
! private procedure of subroutine VPI_Bernal
!EOP
!
!BOC
      integer n

      real*8 coor(3),trans(3)
      real(8) :: site(3,size(basisV,2))
      real*8 w(nAtomInCell)
!      real*8 a,boa,coa,tt(3,3) ! for VP_integrate
      integer :: i1,i2,i3,nmax1,nmax2,nmax3
      real(8) :: bmr1,bmr2,bmr3,Rmx

!      t(1,:)=transV(1,:)
!      t(2,:)=transV(2,:)*lattice(2)
!      t(3,:)=transV(3,:)*lattice(3)

      bmr1 = sqrt(t(1,1)**2+t(2,1)**2+t(3,1)**2)
      bmr2 = sqrt(t(1,2)**2+t(2,2)**2+t(3,2)**2)
      bmr3 = sqrt(t(1,3)**2+t(2,3)**2+t(3,3)**2)
      Rmx = (bmr1+bmr2+bmr3)*(1.d0+1.d-12)
      nmax1 = 1+ceiling(Rmx/bmr1)
      nmax2 = 1+ceiling(Rmx/bmr2)
      nmax3 = 1+ceiling(Rmx/bmr3)

!     read in atom coordinates in bohr and nuclear charge
!      site(1,:)=basisV(1,:)
!      site(2,:)=basisV(2,:)*lattice(2)
!      site(3,:)=basisV(3,:)*lattice(3)
      site = basisV
      call close2zero(t,size(site,2),site)
      trans(1:3) = (t(1:3,1)+t(1:3,2)+t(1:3,3))*0.5d0
      do n=1,nAtomInCell
       site(1:3,n) = site(1:3,n) + trans(1:3)
      end do
      w=r_wghts**2

!     ============================
!     print out coordinates
!     ============================

      open(unit=pnts_wght_un,status='scratch',form='formatted')
!      open(unit=pnts_wght_un,file=PNTS_W,form='formatted')

      do n=1,nAtomInCell
          write(pnts_wght_un,202) site(1,n),site(2,n),site(3,n),w(n)
 202      format(4(2x,d23.16))
      enddo
      do i1=-nmax1,nmax1
       do i2=-nmax2,nmax2
        do i3=-nmax3,nmax3
         if ( max(abs(i1),abs(i2),abs(i3))<1 ) cycle
         trans(1:3)=t(1:3,1)*i1+t(1:3,2)*i2+t(1:3,3)*i3
         do n=1,nAtomInCell
          coor(1:3)=site(1:3,n)+trans(1:3)
          write(pnts_wght_un,202) coor(1),coor(2),coor(3),w(n)
         enddo
        enddo
       enddo
      enddo
      rewind(pnts_wght_un)

      return
!EOC
      end subroutine supercell

      pure subroutine vectprd(a,b,c)
       implicit none
       real(8), intent(in) :: a(3),b(3)
       real(8), intent(out) :: c(3)
       c(1) = a(2)*b(3) - b(2)*a(3)
       c(2) = a(3)*b(1) - b(3)*a(1)
       c(3) = a(1)*b(2) - b(1)*a(2)
       return
      end subroutine vectprd

      subroutine close2zero(cell,np,points)
       implicit none
!
       real(8), intent(in) :: cell(3,3)
       integer, intent(in) :: np
       real(8), intent(inout) :: points(3,np)
!
       real(8) :: u(3)
       real(8) :: tmpinv(3,3),tmpdot(3,3),vlm
       integer :: i,j

       do i=1,3
        do j=i,3
         tmpdot(i,j) = dot_product(cell(1:3,i),cell(1:3,j))
         tmpdot(j,i) = tmpdot(i,j)
        end do
       end do
       vlm =                                                            &
     &    ( tmpdot(2,1)*tmpdot(3,2) - tmpdot(3,1)*tmpdot(2,2) )         &
     &         * tmpdot(1,3) +                                          &
     &    ( tmpdot(3,1)*tmpdot(1,2) - tmpdot(1,1)*tmpdot(3,2) )         &
     &         * tmpdot(2,3) +                                          &
     &    ( tmpdot(1,1)*tmpdot(2,2) - tmpdot(2,1)*tmpdot(1,2) )         &
     &         * tmpdot(3,3)
       call vectprd(tmpdot(:,2),tmpdot(:,3),tmpinv(1:3,1))
       call vectprd(tmpdot(:,3),tmpdot(:,1),tmpinv(1:3,2))
       call vectprd(tmpdot(:,1),tmpdot(:,2),tmpinv(1:3,3))
       tmpinv = tmpinv/vlm
       tmpdot = matmul(tmpinv,cell)
       do i=1,np
        u = matmul(tmpdot,points(1:3,i))
        points(1:3,i) = points(1:3,i) -                                 &
     &                ( int(u(1))*cell(1:3,1)                           &
     &                + int(u(2))*cell(1:3,2)                           &
     &                + int(u(3))*cell(1:3,3) )
       end do
       return
       end subroutine close2zero

      end subroutine VPI_Bernal

!BOP
!!ROUTINE: NEIGHBORS
!!INTERFACE:
      subroutine neighbors(nsub,point,                                  &
     &                     rslatt,natom,basis,itype,                    &
     &                     nnmax,nnatom,xyznn,itypenn,rmax)
!!DESCRIPTION:
! To generate a list containing Voronoi-tesselation neighbours
! for site of type "nsub" at a point "point";
! the list contains number of neighbors, nnatom,
! coordinates of neighbors, xyznn(3,*),
! type of the respective neighbors, itypenn(*)
!
!!DO_NOT_PRINT
      implicit none
!!DO_NOT_PRINT


!!ARGUMENTS:
!c   input:
!c
      integer, intent(in) :: nsub
      real(8), intent(in) ::  point(3)
      real(8), intent(in) :: rslatt(3,3)
      integer, intent(in) :: natom
      real(8), intent(in) :: basis(3,natom)
      integer, intent(in) :: itype(natom)
      integer, intent(in) :: nnmax

!c   output:
!c
      integer, intent(out) :: nnatom
      real(8), intent(out) :: xyznn(3,*)
      integer, intent(out) :: itypenn(*)
      real(8), intent(out) :: rmax
!
!!REVISION HISTORY:
! Initial version - A.S. - 2002
!!REMARKS:
!EOP
!c
      real(8) :: Volume,CellV

      real(8) :: delta(3),tmpxyz(3),bi(3),rnij(3)
      real(8) :: rmin,rij,eps
      real(8) :: dstn(nnmax),aindx(nnmax),tmpnn(3,nnmax)
      integer itmp(nnmax)
      integer i,j,nmin,nm1,nm2,nm3,n1,n2,n3

      character(10), parameter :: sname='neighbors'

      real(8), parameter :: third=1.d0/3.d0,half=0.5d0

      CellV =                                                           &
     &    ( rslatt(2,1)*rslatt(3,2) - rslatt(3,1)*rslatt(2,2) )         &
     &         * rslatt(1,3) +                                          &
     &    ( rslatt(3,1)*rslatt(1,2) - rslatt(1,1)*rslatt(3,2) )         &
     &         * rslatt(2,3) +                                          &
     &    ( rslatt(1,1)*rslatt(2,2) - rslatt(2,1)*rslatt(1,2) )         &
     &         * rslatt(3,3)

      rmax = half*(1+nnmax*CellV/natom)**third
      eps = 1.d-14*rmax
      nm1 = nint(rmax/sqrt(dot_product(rslatt(1:3,1),rslatt(1:3,1))))
      nm2 = nint(rmax/sqrt(dot_product(rslatt(1:3,2),rslatt(1:3,2))))
      nm3 = nint(rmax/sqrt(dot_product(rslatt(1:3,3),rslatt(1:3,3))))


      delta(1:3) =                                                      &
     &     half*(rslatt(1:3,1)+rslatt(1:3,2)+rslatt(1:3,3))

      rmin = sqrt(dot_product(delta,delta))
      do i=1,3
       tmpxyz(1:3) = delta(1:3)-rslatt(1:3,i)
       rmin = min(rmin,sqrt(dot_product(tmpxyz,tmpxyz)))
      end do
!
      nmin = rmax/rmin
      nm1 = 1+max(nm1,nmin)
      nm2 = 1+max(nm2,nmin)
      nm3 = 1+max(nm3,nmin)

      xyznn(1:3,1) = point(1:3)
      itypenn(1) = nsub
      nnatom = 1

      do n1=-nm1,nm1
       do n2=-nm2,nm2
      do n3=-nm3,nm3
       bi(1:3) = xyznn(1:3,1) +                                         &
     &                n1*rslatt(1:3,1)                                  &
     &               +n2*rslatt(1:3,2)                                  &
     &               +n3*rslatt(1:3,3)
       do j=1,natom
        rnij(1:3) = basis(1:3,j) - bi(1:3)
        rij = sqrt(dot_product(rnij,rnij))
        if(rij.le.rmax) then
         if(rij.lt.eps) cycle
         nnatom = nnatom+1
         dstn(nnatom) = rij
         aindx(nnatom) = nnatom
         tmpnn(1:3,nnatom) = rnij(1:3) + xyznn(1:3,1)
         itmp(nnatom) = itype(j)
          end if
       end do
      end do
       end do
      end do

      if(nnatom.gt.nnmax) then
       write(*,*) ' NNMAX=',nnmax
       write(*,*) ' NNATOM=',nnatom
       call fstop(sname//': NNATOM > NNMAX')
      end if

      call dsort(dstn(2),aindx(2),nnatom-1,2)

      do j=2,nnatom
       i = nint(aindx(j))
       itypenn(j) = itmp(i)
       xyznn(1:3,j) = tmpnn(1:3,i)
      end do

      return
      end subroutine neighbors
