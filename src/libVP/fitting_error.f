!BOP
!!ROUTINE: fitting_error
!!INTERFACE:
      subroutine fitting_error(poly_type,npol,jpf,a1pf,input,           &
     &                         nAtom,tnAtom,Ni,Nf,error,SD,diff)
!!DESCRIPTION:
! computes fitting error for several polynomial fitting types
! (r**n, Laguerre, Legendre, Chebyshev polynomials)
!
!EOP
!
!BOC
      implicit none
      integer*8 poly_type,jpf,npol,nAtom,tnAtom,Ni,Nf,i,Ndata,error
      real*8 a1pf(npol+1),func,rmag,basis0(npol+1)
        real*8 input(2*tnAtom,Nf),SD,diff(4,Nf)

      Ndata=Nf-Ni+1
      func=0.0d0
        SD=0.0d0
      do i=Ni,Nf
         rmag=input(2*nAtom-1,i)
       basis0(1)=1.0d0
       basis0(2)=rmag
       do 31 jpf=3,npol+1
        select case(poly_type)
        case(1) !simple polynomial
         basis0(jpf)=rmag**(DFLOAT(jpf-1))
        case(2) !Laguerre polynomial
         basis0(jpf)=((2.0D0*DFLOAT(jpf)-1.0D0-rmag)*basis0(jpf-1)-     &
     &                (DFLOAT(jpf)-1.0D0)*basis0(jpf-2))/(DFLOAT(jpf))
        case(3) !Legendre polynomial
         basis0(jpf)=((2.0d0*DFLOAT(jpf)-1.0d0)*rmag*basis0(jpf-1)-     &
     &                (DFLOAT(jpf)-1.0d0)*basis0(jpf-2))/(DFLOAT(jpf))
        case(4) !Chebyshev polynomial
         basis0(jpf)=(2.0D0*rmag*basis0(jpf-1))-basis0(jpf-2)
        case(5)
         basis0(jpf)=rmag**(DFLOAT(jpf-1)/2.0D0)
        end select
   31  continue
       !func=dot_product(a1pf,basis0)
         diff(1,i)=input(2*nAtom-1,i)
         diff(2,i)=input(2*nAtom,i)
         diff(3,i)=dot_product(a1pf,basis0)
         diff(4,i)=diff(3,i)-input(2*nAtom,i)
      enddo

      SD=dsqrt(dot_product(diff(4,:),diff(4,:)))/DFLOAT(Ndata)
      write(6,"('standard deviation of the interpolation                &
     &(atom ',I2,')',d23.16)") nAtom,SD
      if(SD>10.0d0**(-error)) stop 'fail to interpolate                 &
     & Standard Deviation > 10.0d0**(-error)'
        !If SD>10.0d0**(-error), either impose a denser grid of
        !data or lowering down error.

      return
!EOC
      end subroutine fitting_error
