!BOP
!!ROUTINE: VPI_main
!!INTERFACE:
      subroutine VPI_main(MNV,MNF,MNqp,                                 &
     &                    tnAtom,convDU,r,rmt,nsublat,iType,            &
     &                    nVertex1,nVertex2,Ivdex2,nfaces,              &
     &                    Iface,vertices,fcount,weight,rmag,vj,istop)
!!DESCRIPTION:
! calculates data for isoparametric integration, and also inscribed radii
! {\tt r, rmt} for each site and for each sublattice
!

!!DO_NOT_PRINT
      implicit none
!!DO_NOT_PRINT

!!ARGUMENTS:
      integer, intent(in) ::  MNV  ! maximum number of VP-vertices
      integer, intent(in) ::  MNF  ! maximum number of VP-faces
      integer, intent(in) ::  MNqp ! maximum number of quadrature points
      integer, intent(in) ::  tnAtom ! number of sites in the cell
      real(8), intent(in) ::  convDU   ! conversion coeff from d.u. to natural latt.units
      real(8), intent(out) ::  r(tnAtom)  ! inscribed sphere radius for each site VP 
      integer, intent(in) ::  nsublat ! number of sublattices in the cell
      real(8), intent(out) ::  rmt(nsublat) ! sublattice inscribed sphere radius
      integer, intent(in) ::  iType(tnAtom) ! list of equivalent atoms
      integer, intent(in) ::  nVertex1(tnAtom),nVertex2(tnAtom)
      integer, intent(in) ::  Ivdex2(tnAtom,MNV,2)
      integer, intent(in) ::  nfaces(tnAtom)
      integer, intent(inout) ::  Iface(tnAtom,MNF,MNV+1)
      real(8), intent(inout) ::  vertices(tnAtom,3,MNV)
      integer, intent(out) ::  fcount(nsublat)
      real(8), intent(out) ::  weight(MNqp)
      real(8), intent(out) ::  rmag(0:3,MNqp,MNqp,MNqp,MNF,nsublat)
      real(8), intent(out) ::  vj(MNqp,MNqp,MNqp,MNF,nsublat)
      character(10), intent(in) :: istop
!!PARAMETERS:
!!REVISION HISTORY:
! Initial version - Aftab Alam - 2010
! Adapted - A.S. - 2013
!!REMARKS:
! MNqp should be less or equal 20 due to limitation in internal
! procedure absc_weight generating quadtrature points
!EOP
!
! [Reference]
!  "The Finite Elements Method (linear static and dynamic finite element
!   analysis" by Thomas J.R. Hughes
!   ps: check chapter-3
!
!-----------------------------------------------------------------------
!
! iType :  iType(site #) --> sub-lattice #
!
!  e.g.: if there are 7 atoms a unit cell, and 1st, 6th and 7th
!        atoms are sub-lattice 1, 2nd and 3rd are sub-lattice 2,
!        4th is sub-lattice 4, 5th is sub-lattice 3. Then iType=
!        {1, 2, 2, 4, 3, 1, 1}
!
! [input variables from VPI_Bernal]
! t(3,3) ; real*8
!  translation vectors in unit of "a"
!    t(1,:)=transV(1,:)
!    t(2,:)=transV(2,:)*lattice(2)
!    t(3,:)=transV(3,:)*lattice(3)
!  ps: transV & lattice are defined in subroutine VPI_Bernal
!
! nVertex1 ; integer
!  number of non-degenerate vertices
!
! nVertex2 ; integer
!  number of degenerare vertices
!
! Ivdex2(tnAtom,MNV,2) ; integer
!  index for degenerate vertices
!  e.g.: if vertex 1(non-degen.) & 19(degen.) in atom-1 are identical
!     ==>Ivdex2(1,i,1)=1 ; Ivdex2(1,i,2)=19
!        i is some number depend on the sequence of output from Bernal
!
! nfaces(tnAtom) ; integer
!  number of faces for each atom
!
! Iface(tnAtom,MNF,MNV+1) ; integer
!  face-index(fdex) and vertices-set(vdex_set)
!  e.g.: for atom-i, if fdex 5 consists 6 vertices said {4  6 13 15 17 19}
!     ==>Iface(i,5,1)= 6 (# of vertices) <-- this is the reason why the 3rd
!                                               dimension is MNV+1
!        Iface(i,5,2)= 4 (1st vdex)
!        Iface(i,5,3)= 6 (2nd vdex)
!        Iface(i,5,4)=13 (3rd vdex)
!        Iface(i,5,5)=15 (4th vdex)
!        Iface(i,5,6)=17 (5th vdex)
!        Iface(i,5,7)=19 (6th vdex)
!  ps: Bernal's code has a limit on number of vertices for a face!
!      the limit should be 30(check later)    ::  WRONG (CAB 07/18/2013)
!
! vertices(tnAtom,3,MNV) ; real*8
!  vertices information
!  e.g.: from atom-i, if 11th vertex is listed as follow
!        vdex        relative position
!         13   0.0D+00    0.0D+00    5.0D-01
!     ==>vertices(i,1,13)=0.0D+00 <-- x component
!        vertices(i,2,13)=0.0D+00 <-- y component
!        vertices(i,3,13)=5.0D-01 <-- z component
!
!-----------------------------------------------------------------------
!BOC
      integer nAtom,ITYP
      integer NEA(nsublat)
      logical wholeVP
!      logical ErrTest,ConvTest,UseSym
!--[for face analysis]-------------------------------------------------
        integer vf(tnAtom,MNF,4)
!--[for abscissas]--------------------------------------------
        integer Nqp
        real*8 abscissas(MNqp)
!
      integer i,j,k,I_face,iii

!        real*8 T1(8,8)
        real*8 Rmt_chk(tnAtom,MNF)
!        real*8 X1(8),Y1(8),Z1(8)
        real*8 s1234(4),s567(4),alpha(0:7,3),rX1(3)
        real(8) :: vjj
        real(8) :: Xi,Eta,Zeta,rmx,r0,rmn

        real(8), parameter :: oner=1.d0+1.d-8,onel=1.d0-1.d-8

        character sname*10
        parameter (sname='VPI_main')
!-------------------------------------------------------------------

!  when wholeVP=.true. ==> calculate whole Voronoi-Polyhedral(VP)
!  when wholeVP=.false.==> calculate VP with a inscribe sphere

        wholeVP=.false.

!        ErrTest=.false.
!        ConvTest=.true.

!     nsublat: Total # of different sub-lattice.
!      NEA: total # of equivalent atoms for certain sub-lattice.
!            e.g.: NEA(2)=3 indicate the sub-lattice 2 has 3 equivalent
!                  atoms.

!      data T1/1.0D0, 1.0D0,-1.0D0, 1.0D0,  -1.0D0,-1.0D0, 1.0D0,-1.0D0, &
!     &        1.0D0,-1.0D0,-1.0D0, 1.0D0,   1.0D0,-1.0D0,-1.0D0, 1.0D0, &
!     &        1.0D0,-1.0D0, 1.0D0, 1.0D0,  -1.0D0, 1.0D0,-1.0D0,-1.0D0, &
!     &        1.0D0, 1.0D0, 1.0D0, 1.0D0,   1.0D0, 1.0D0, 1.0D0, 1.0D0, &
!     &        1.0D0, 1.0D0,-1.0D0,-1.0D0,  -1.0D0, 1.0D0,-1.0D0, 1.0D0, &
!     &        1.0D0,-1.0D0,-1.0D0,-1.0D0,   1.0D0, 1.0D0, 1.0D0,-1.0D0, &
!     &        1.0D0,-1.0D0, 1.0D0,-1.0D0,  -1.0D0,-1.0D0, 1.0D0, 1.0D0, &
!     &        1.0D0, 1.0D0, 1.0D0,-1.0D0,   1.0D0,-1.0D0,-1.0D0,-1.0D0/
!      T1=T1/8.0D0

        call find_Rmt(MNV,MNF,tnAtom,Iface,nfaces,Ivdex2,               &
     &                    nVertex2,vertices,Rmt_chk)

        do iii=1,tnAtom
          r(iii) = minval(Rmt_chk(iii,1:nfaces(iii)))
        enddo

        !---[put scale back]---
!DEBUG        t=t*lattice(1)
        r=r*convDU
        vertices=vertices*convDU
        !----------------------
        do i=1,nsublat
         do nAtom=1,tnAtom
           if ( iType(nAtom) == i ) then
             rmt(i)=r(nAtom)
             exit  
           end if
         enddo
        enddo
        if(wholeVP) r=0.0d0
       vf=0
       fcount=0
       NEA=0

      if ( MNV>0 .and. MNF>0 .and. MNqp>0 ) then
       do 230 nAtom=1,tnAtom
        ITYP=iType(nAtom)
        NEA(ITYP)=NEA(ITYP)+1
        !---[use Sym.]---
          if ( NEA(ITYP) > 1 ) cycle

        !---[driver for polynomial fitting]---
!        call polyn_fit_input0(Ni(nAtom),r,nAtom,tnAtom,wholeVP,rr)
!        call polyn_fit_input(Ni(nAtom),basis(1,1,nAtom),nAtom,tnAtom,rr)
!        call polyn_fit(ypf,a1pf(1,ITYP),basis,Ndata(nAtom))

       ! Iface(i,j,k) : i - atom #, j - face #, k - face vertex # ? 

        call face_analysis(MNV,MNF,nAtom,tnAtom,nfaces,Iface,           &
     &                 nVertex2,Ivdex2,vertices,vf,fcount(ITYP),istop)
        if ( fcount(ITYP)>MNF ) then
         write(6,'('' ERROR :: Number of faces '',i3,'' > MNF='',i3,    &
     &             '' (type '',i3,'')'')') fcount(ITYP),MNF,ITYP 
         stop 'ERROR: increase maximum number of nearest VP-neighbors'
        end if

        rmn = r(nAtom)
        Nqp=MNqp
        call absc_weight(Nqp,abscissas,weight)
        do I_face=1,fcount(ITYP)
!
        ! write(*,*) ' i_face=',I_face
        rmx=maxval(sqrt(vertices(nAtom,1,vf(nAtom,I_face,1:4))**2+      &
     &                  vertices(nAtom,2,vf(nAtom,I_face,1:4))**2+      &
     &                  vertices(nAtom,3,vf(nAtom,I_face,1:4))**2))
!
         call drive_jacobian(MNV,r(nAtom),vertices(nAtom,1:3,:),        &
     &              vf(nAtom,I_face,1:4),s1234,s567,alpha,wholeVP,istop)

         do i=1,Nqp
          Xi=abscissas(i)
          do j=1,Nqp
           Eta=abscissas(j)
           do k=1,Nqp
            Zeta=abscissas(k)
       call jacobian(r(nAtom),Xi,Eta,Zeta,s1234,s567,alpha,rX1,vjj)
       vjj = vjj*weight(i)*weight(j)*weight(k)  ! weighted jacobian
       r0 = sqrt(rX1(1)**2+rX1(2)**2+rX1(3)**2)
       if ( r0>=rmn .and. r0<=rmx ) then
         rmag(1:3,i,j,k,I_face,ITYP) = rX1(1:3)
         rmag(0,i,j,k,I_face,ITYP)=r0
         vj(i,j,k,I_face,ITYP) = vjj
       else if ( r0<rmn .and. r0>rmn*onel ) then
         rmag(1:3,i,j,k,I_face,ITYP) = rX1(1:3)*(rmn/r0)
         rmag(0,i,j,k,I_face,ITYP)=rmn
         vj(i,j,k,I_face,ITYP) = vjj
       else if ( r0>rmx .and. r0<rmx*oner ) then
         rmag(1:3,i,j,k,I_face,ITYP) = rX1(1:3)*(rmx/r0)
         rmag(0,i,j,k,I_face,ITYP)=rmx
         vj(i,j,k,I_face,ITYP) = vjj
       else
           write(6,'('' nsub='',i3,'' I_face='',i2,'' i,j,k,r0='',      &
     &                   3i5,1x,g14.5,1x,a)') ITYP,I_face,i,j,k,r0      &
     &              ,' POINT OUTSIDE INTEGR.REGION! '//sname
        if ( r0>rmx ) rmag(0,i,j,k,I_face,ITYP)=rmx
        if ( r0<r(nAtom) ) r0=r(nAtom)
        vj(i,j,k,I_face,ITYP) = 0.d0
       end if  
!       
           enddo
          enddo
         enddo

        enddo

  230  continue

      end if
      if (istop.eq.sname) then
         call fstop(sname)
      endif

      return

!EOC
      CONTAINS

!BOP
!!IROUTINE: find_Rmt
!!DESCRIPTION:
!
!!INTERFACE:
      subroutine find_Rmt(MNV,MNF,tnAtom,Iface,nfaces,Ivdex2,           &
     &                    nVertex2,vertices,Rmt_chk)
!!REMARKS:
! private procedure of subroutine VPI_main
!EOP
!
!BOC
        implicit none
        integer, intent(in) :: MNV,MNF,tnAtom
        integer, intent(inout) :: Iface(tnAtom,MNF,MNV+1)
        integer, intent(in) :: nfaces(tnAtom),Ivdex2(tnAtom,MNV,2)
        integer, intent(in) :: nVertex2(tnAtom)
        real*8, intent(in) :: vertices(tnAtom,3,MNV)
        real(8), intent(out) :: Rmt_chk(tnAtom,MNF)
!
        integer NF,i,j,n1,n2,nn,nAtom,v1,v2,v3
        real*8 x1(4),y1(4),z1(4)
        real*8 s1234(4)

      Rmt_chk=0.0d0
       do 10 nAtom=1,tnAtom
         do 20 NF=1,nfaces(nAtom)
          do 30 i=2,Iface(nAtom,NF,1)+1
           do 40 j=1,nVertex2(nAtom)
            if(Iface(nAtom,NF,i)==Ivdex2(nAtom,j,1))then
             Iface(nAtom,NF,i)=Ivdex2(nAtom,j,2)
            end if
   40      continue
   30     continue
   20    continue
   10  continue

      do 100 nAtom=1,tnAtom
       do 200 NF=1,nfaces(nAtom)
!*****  To find 1st three distinct vertices on a face *****
         v1=Iface(nAtom,NF,1+1) !+1 is because the 1st column is # of vertex
         v2 = v1
         v3 = v1
         nn = 2
         do i=3,Iface(nAtom,NF,1)+1
           n1=Iface(nAtom,NF,i)
            if(n1.ne.v1)then
               v2=n1
               nn=i
               go to 98
            endif
         enddo
         if ( v1 == v2 ) then
          write(6,'('' nAtom='',i8)')
          write(6,'('' nF='',i3)') NF
          write(6,'('' Iface(nAtom,NF,1)='',i3)') Iface(nAtom,NF,1)
          stop ' ERROR: unable to find distinct vertices on a face'
         end if
98       do j=nn+1,Iface(nAtom,NF,1)+1
!           n1=Iface(nAtom,NF,nn)
           n2=Iface(nAtom,NF,j)
            if(n2.ne.v1.and.n2.ne.v2)then
               v3=n2
               go to 99
            endif
         enddo
         if ( v1 == v3 ) then
          write(6,'('' nAtom='',i8)')
          write(6,'('' nF='',i3)') NF
          write(6,'('' Iface(nAtom,NF,1)='',i3)') Iface(nAtom,NF,1)
          stop ' ERROR: unable to find 3 distinct vertices on a face'
         end if
!*******************************************************
99      X1(1)=vertices(nAtom,1,v1)
        X1(2)=vertices(nAtom,1,v2)
        X1(3)=vertices(nAtom,1,v3)

        Y1(1)=vertices(nAtom,2,v1)
        Y1(2)=vertices(nAtom,2,v2)
        Y1(3)=vertices(nAtom,2,v3)

        Z1(1)=vertices(nAtom,3,v1)
        Z1(2)=vertices(nAtom,3,v2)
        Z1(3)=vertices(nAtom,3,v3)

        s1234(1)=(Y1(1)-Y1(2))*(Z1(2)-Z1(3))-(Z1(1)-Z1(2))*(Y1(2)-Y1(3))
        s1234(2)=(Z1(1)-Z1(2))*(X1(2)-X1(3))-(X1(1)-X1(2))*(Z1(2)-Z1(3))
        s1234(3)=(X1(1)-X1(2))*(Y1(2)-Y1(3))-(Y1(1)-Y1(2))*(X1(2)-X1(3))
        s1234(4)=-(s1234(1)*X1(1)+s1234(2)*Y1(1)+s1234(3)*Z1(1))

       Rmt_chk(nAtom,NF)=-s1234(4)/                                     &
     &                    dsqrt(dot_product(s1234(1:3),s1234(1:3)))
  200  continue
  100 continue
      Rmt_chk=dabs(Rmt_chk)

      return
!EOC
      end subroutine find_Rmt

!BOP
!!IROUTINE: absc_weight
!!INTERFACE:
        subroutine absc_weight(N,abscissas,weight)
!!DESCRIPTION:
! generates gauss-chebyshev quadrature integration points (on segment [-1,1]) 
! and weights
!!REMARKS:
! 1 <= N <=20
! private procedure of subroutine VPI_main
!EOP
!
!BOC
        implicit none
        integer N
        real*8 abscissas(N),weight(N)

        if ( N > 20 .or. N<=0 ) then
         write(6,*) '  ERROR in absc_weight: N =',N,                    &
     &                                          ' is out of range 1..20'
         return
        end if
        if(N==1)then
         abscissas(1)=0.0d0
         weight(1)=2.0d0

        else if(N==2)then
         abscissas(1)=1.0d0/dsqrt(3.0d0)
         abscissas(2)=-1.0d0/dsqrt(3.0d0)
         weight(1)=1.0d0
         weight(2)=1.0d0

        else if(N==3)then
         abscissas(1)=0.0d0
         abscissas(2)=dsqrt(3.0d0/5.0d0)
         abscissas(3)=-dsqrt(3.0d0/5.0d0)
         weight(1)=8.0d0/9.0d0
         weight(2)=5.0d0/9.0d0
         weight(3)=5.0d0/9.0d0

        else if(N==4)then
         abscissas(1)= dsqrt(525.0d0-70.0d0*dsqrt(30.0d0))/35.0d0
         abscissas(2)=-dsqrt(525.0d0-70.0d0*dsqrt(30.0d0))/35.0d0
         abscissas(3)= dsqrt(525.0d0+70.0d0*dsqrt(30.0d0))/35.0d0
         abscissas(4)=-dsqrt(525.0d0+70.0d0*dsqrt(30.0d0))/35.0d0
         weight(1)=(18.0d0+dsqrt(30.0d0))/36.0d0
         weight(2)=weight(1)
         weight(3)=(18.0d0-dsqrt(30.0d0))/36.0d0
         weight(4)=weight(3)

        else if(N==5)then
         abscissas(1)=0.0d0
         abscissas(2)= dsqrt(245.0d0-14.0d0*dsqrt(70.0d0))/21.0d0
         abscissas(3)=-dsqrt(245.0d0-14.0d0*dsqrt(70.0d0))/21.0d0
         abscissas(4)= dsqrt(245.0d0+14.0d0*dsqrt(70.0d0))/21.0d0
         abscissas(5)=-dsqrt(245.0d0+14.0d0*dsqrt(70.0d0))/21.0d0
         weight(1)=128.0d0/225.0d0
         weight(2)=(322.0d0+13.0d0*dsqrt(70.0d0))/900.0d0
         weight(3)=(322.0d0+13.0d0*dsqrt(70.0d0))/900.0d0
         weight(4)=(322.0d0-13.0d0*dsqrt(70.0d0))/900.0d0
         weight(5)=(322.0d0-13.0d0*dsqrt(70.0d0))/900.0d0

        else if(N==6)then
         abscissas(1)=-0.932469514203152027812301554493994609130d0
         abscissas(2)=-0.661209386466264513661399595019905347006d0
         abscissas(3)=-0.238619186083196908630501721680711935419d0
         abscissas(4)=-abscissas(3)
         abscissas(5)=-abscissas(2)
         abscissas(6)=-abscissas(1)
         weight(1)=0.17132449237917034504029614217273289353d0
         weight(2)=0.36076157304813860756983351383771611166d0
         weight(3)=0.46791393457269104738987034398955099481d0
         weight(4)=weight(3)
         weight(5)=weight(2)
         weight(6)=weight(1)

        else if(N==7)then
         abscissas(1)=-0.94910791234275852452618968404785126240d0
         abscissas(2)=-0.74153118559939443986386477328078840707d0
         abscissas(3)=-0.40584515137739716690660641207696146335d0
         abscissas(4)=0.0d0
         abscissas(5)=-abscissas(3)
         abscissas(6)=-abscissas(2)
         abscissas(7)=-abscissas(1)
         weight(1)=0.12948496616886969327061143267908201833d0
         weight(2)=0.27970539148927666790146777142377958249d0
         weight(3)=0.38183005050511894495036977548897513388d0
         weight(4)=0.41795918367346938775510204081632653061d0
         weight(5)=weight(3)
         weight(6)=weight(2)
         weight(7)=weight(1)

        else if(N==8)then
         abscissas(1)=-0.96028985649753623168356086856947299043d0
         abscissas(2)=-0.79666647741362673959155393647583043684d0
         abscissas(3)=-0.52553240991632898581773904918924634904d0
         abscissas(4)=-0.183434642495649804939476142360183980667d0
         abscissas(5)=-abscissas(4)
         abscissas(6)=-abscissas(3)
         abscissas(7)=-abscissas(2)
         abscissas(8)=-abscissas(1)
         weight(1)=0.1012285362903762591525313543099621901d0
         weight(2)=0.22238103445337447054435599442624088443d0
         weight(3)=0.31370664587788728733796220198660131326d0
         weight(4)=0.36268378337836198296515044927719561219d0
         weight(5)=weight(4)
         weight(6)=weight(3)
         weight(7)=weight(2)
         weight(8)=weight(1)

        else if(N==9)then
         abscissas(1)=-0.9681602395076260898355762029036728700d0
         abscissas(2)=-0.83603110732663579429942978806973487654d0
         abscissas(3)=-0.61337143270059039730870203934147418479d0
         abscissas(4)=-0.32425342340380892903853801464333660857d0
         abscissas(5)=0.0d0
         abscissas(6)=-abscissas(4)
         abscissas(7)=-abscissas(3)
         abscissas(8)=-abscissas(2)
         abscissas(9)=-abscissas(1)
         weight(1)=0.0812743883615744119718921581105236507d0
         weight(2)=0.1806481606948574040584720312429128095d0
         weight(3)=0.26061069640293546231874286941863284977d0
         weight(4)=0.31234707704000284006863040658444366560d0
         weight(5)=0.33023935500125976316452506928697404888d0
         weight(6)=weight(4)
         weight(7)=weight(3)
         weight(8)=weight(2)
         weight(9)=weight(1)

        else if(N==10)then
         abscissas(1)=-0.9739065285171717200779640120844520534d0
         abscissas(2)=-0.8650633666889845107320966884234930485d0
         abscissas(3)=-0.6794095682990244062343273651148735758d0
         abscissas(4)=-0.43339539412924719079926594316578416220d0
         abscissas(5)=-0.14887433898163121088482600112971998462d0
         abscissas(6)=-abscissas(5)
         abscissas(7)=-abscissas(4)
         abscissas(8)=-abscissas(3)
         abscissas(9)=-abscissas(2)
         abscissas(10)=-abscissas(1)
         weight(1)=0.0666713443086881375935688098933317929d0
         weight(2)=0.1494513491505805931457763396576973324d0
         weight(3)=0.2190863625159820439955349342281631925d0
         weight(4)=0.2692667193099963550912269215694693529d0
         weight(5)=0.2955242247147528701738929946513383294d0
         weight(6)=weight(5)
         weight(7)=weight(4)
         weight(8)=weight(3)
         weight(9)=weight(2)
         weight(10)=weight(1)

        else if(N==11)then
         abscissas(1)=-0.9782286581460569928039380011228573908d0
         abscissas(2)=-0.8870625997680952990751577693039272666d0
         abscissas(3)=-0.7301520055740493240934162520311534580d0
         abscissas(4)=-0.5190961292068118159257256694586095545d0
         abscissas(5)=-0.2695431559523449723315319854008615247d0
         abscissas(6)=0.0d0
         abscissas(7)=-abscissas(5)
         abscissas(8)=-abscissas(4)
         abscissas(9)=-abscissas(3)
         abscissas(10)=-abscissas(2)
         abscissas(11)=-abscissas(1)
         weight(1)=0.055668567116173666482753720442548579d0
         weight(2)=0.1255803694649046246346942992239401002d0
         weight(3)=0.1862902109277342514260976414316558917d0
         weight(4)=0.2331937645919904799185237048431751394d0
         weight(5)=0.2628045445102466621806888698905091954d0
         weight(6)=0.2729250867779006307144835283363421892d0
         weight(7)=weight(5)
         weight(8)=weight(4)
         weight(9)=weight(3)
         weight(10)=weight(2)
         weight(11)=weight(1)

        else if(N==12)then
         abscissas(1)=-0.9815606342467192506905490901492808230d0
         abscissas(2)=-0.9041172563704748566784658661190961925d0
         abscissas(3)=-0.7699026741943046870368938332128180760d0
         abscissas(4)=-0.5873179542866174472967024189405342804d0
         abscissas(5)=-0.3678314989981801937526915366437175613d0
         abscissas(6)=-0.1252334085114689154724413694638531300d0
         abscissas(7)=-abscissas(6)
         abscissas(8)=-abscissas(5)
         abscissas(9)=-abscissas(4)
         abscissas(10)=-abscissas(3)
         abscissas(11)=-abscissas(2)
         abscissas(12)=-abscissas(1)
         weight(1)=0.047175336386511827194615961485017060d0
         weight(2)=0.106939325995318430960254718193996224d0
         weight(3)=0.1600783285433462263346525295433590719d0
         weight(4)=0.2031674267230659217490644558097983765d0
         weight(5)=0.2334925365383548087608498989248780563d0
         weight(6)=0.2491470458134027850005624360429512108d0
         weight(7)=weight(6)
         weight(8)=weight(5)
         weight(9)=weight(4)
         weight(10)=weight(3)
         weight(11)=weight(2)
         weight(12)=weight(1)

        else if(N==13)then
         abscissas(1)=-0.984183054718588149472829448807109611d0
         abscissas(2)=-0.917598399222977965206547836500719512d0
         abscissas(3)=-0.801578090733309912794206489582859890d0
         abscissas(4)=-0.642349339440340220643984606995515650d0
         abscissas(5)=-0.4484927510364468528779128521276398678d0
         abscissas(6)=-0.2304583159551347940655281210979888352d0
         abscissas(7)=0.0d0
         abscissas(8)=-abscissas(6)
         abscissas(9)=-abscissas(5)
         abscissas(10)=-abscissas(4)
         abscissas(11)=-abscissas(3)
         abscissas(12)=-abscissas(2)
         abscissas(13)=-abscissas(1)
         weight(1)=0.040484004765315879520021592200986060d0
         weight(2)=0.092121499837728447914421775953797121d0
         weight(3)=0.138873510219787238463601776868871468d0
         weight(4)=0.178145980761945738280046691996097996d0
         weight(5)=0.207816047536888502312523219306052763d0
         weight(6)=0.226283180262897238412090186039776618d0
         weight(7)=0.2325515532308739101945895152688359482d0
         weight(8)=weight(6)
         weight(9)=weight(5)
         weight(10)=weight(4)
         weight(11)=weight(3)
         weight(12)=weight(2)
         weight(13)=weight(1)


        else if(N==14)then
         abscissas(1)=-0.986283808696812338841597266704052802d0
         abscissas(2)=-0.928434883663573517336391139377874264d0
         abscissas(3)=-0.827201315069764993189794742650394961d0
         abscissas(4)=-0.687292904811685470148019803019334138d0
         abscissas(5)=-0.515248636358154091965290718551188662d0
         abscissas(6)=-0.319112368927889760435671824168475467d0
         abscissas(7)=0.1080549487073436620662446502198347476d0
         abscissas(8)=-abscissas(7)
         abscissas(9)=-abscissas(6)
         abscissas(10)=-abscissas(5)
         abscissas(11)=-abscissas(4)
         abscissas(12)=-abscissas(3)
         abscissas(13)=-abscissas(2)
         abscissas(14)=-abscissas(1)
         weight(1)=0.03511946033175186303183287613819178d0
         weight(2)=0.080158087159760209805633277062854310d0
         weight(3)=0.121518570687903184689414809072476626d0
         weight(4)=0.157203167158193534569601938623842157d0
         weight(5)=0.185538397477937813741716590125157036d0
         weight(6)=0.205198463721295603965924065661218056d0
         weight(7)=0.215263853463157790195876443316260035d0
         weight(8)=weight(7)
         weight(9)=weight(6)
         weight(10)=weight(5)
         weight(11)=weight(4)
         weight(12)=weight(3)
         weight(13)=weight(2)
         weight(14)=weight(1)

        else if(N==15)then
         abscissas(1)=-0.987992518020485428489565718586612581d0
         abscissas(2)=-0.937273392400705904307758947710209471d0
         abscissas(3)=-0.848206583410427216200648320774216851d0
         abscissas(4)=-0.724417731360170047416186054613938010d0
         abscissas(5)=-0.570972172608538847537226737253910641d0
         abscissas(6)=-0.394151347077563369897207370981045468d0
         abscissas(7)=-0.201194093997434522300628303394596208d0
         abscissas(8)=0.0d0
         abscissas(9)=-abscissas(7)
         abscissas(10)=-abscissas(6)
         abscissas(11)=-abscissas(5)
         abscissas(12)=-abscissas(4)
         abscissas(13)=-abscissas(3)
         abscissas(14)=-abscissas(2)
         abscissas(15)=-abscissas(1)
         weight(1)=0.03075324199611726835462839357720442d0
         weight(2)=0.07036604748810812470926741645066734d0
         weight(3)=0.107159220467171935011869546685869303d0
         weight(4)=0.139570677926154314447804794511028323d0
         weight(5)=0.166269205816993933553200860481208811d0
         weight(6)=0.186161000015562211026800561866422825d0
         weight(7)=0.198431485327111576456118326443839325d0
         weight(8)=0.202578241925561272880620199967519315d0
         weight(9)=weight(7)
         weight(10)=weight(6)
         weight(11)=weight(5)
         weight(12)=weight(4)
         weight(13)=weight(3)
         weight(14)=weight(2)
         weight(15)=weight(1)

        else if(N==16)then
         abscissas(1)=-0.98940093499164993259615417345033263d0
         abscissas(2)=-0.94457502307323257607798841553460835d0
         abscissas(3)=-0.86563120238783174388046789771239313d0
         abscissas(4)=-0.75540440835500303389510119484744227d0
         abscissas(5)=-0.61787624440264374844667176404879102d0
         abscissas(6)=-0.458016777657227386342419442983577574d0
         abscissas(7)=-0.281603550779258913230460501460496106d0
         abscissas(8)=-0.095012509837637440185319335424958063d0
         abscissas(9)=-abscissas(8)
         abscissas(10)=-abscissas(7)
         abscissas(11)=-abscissas(6)
         abscissas(12)=-abscissas(5)
         abscissas(13)=-abscissas(4)
         abscissas(14)=-abscissas(3)
         abscissas(15)=-abscissas(2)
         abscissas(16)=-abscissas(1)
         weight(1)=0.02715245941175409485178057245601810d0
         weight(2)=0.06225352393864789286284383699437769d0
         weight(3)=0.09515851168249278480992510760224623d0
         weight(4)=0.12462897125553387205247628219201642d0
         weight(5)=0.14959598881657673208150173054747855d0
         weight(6)=0.169156519395002538189312079030359962d0
         weight(7)=0.182603415044923588866763667969219939d0
         weight(8)=0.189450610455068496285396723208283105d0
         weight(9)=weight(8)
         weight(10)=weight(7)
         weight(11)=weight(6)
         weight(12)=weight(5)
         weight(13)=weight(4)
         weight(14)=weight(3)
         weight(15)=weight(2)
         weight(16)=weight(1)

        else if(N==17)then
         abscissas(1)=-0.99057547531441733567543401994066528d0
         abscissas(2)=-0.95067552176876776122271695789580302d0
         abscissas(3)=-0.88023915372698590212295569448815569d0
         abscissas(4)=-0.78151400389680140692523005552047605d0
         abscissas(5)=-0.65767115921669076585030221664300234d0
         abscissas(6)=-0.51269053708647696788624656862955187d0
         abscissas(7)=-0.35123176345387631529718551709534601d0
         abscissas(8)=-0.178484181495847855850677493654065557d0
         abscissas(9)=0.0d0
         abscissas(10)=-abscissas(8)
         abscissas(11)=-abscissas(7)
         abscissas(12)=-abscissas(6)
         abscissas(13)=-abscissas(5)
         abscissas(14)=-abscissas(4)
         abscissas(15)=-abscissas(3)
         abscissas(16)=-abscissas(2)
         abscissas(17)=-abscissas(1)
         weight(1)=0.024148302868547931960110026287565324d0
         weight(2)=0.055459529373987201129440165358244660d0
         weight(3)=0.085036148317179180883535370191062073d0
         weight(4)=0.111883847193403971094788385626355926d0
         weight(5)=0.135136368468525473286319981702350197d0
         weight(6)=0.154045761076810288081431594801958611d0
         weight(7)=0.168004102156450044509970663788323155d0
         weight(8)=0.176562705366992646325270990113197239d0
         weight(9)=0.179446470356206525458265644261885621d0
         weight(10)=weight(8)
         weight(11)=weight(7)
         weight(12)=weight(6)
         weight(13)=weight(5)
         weight(14)=weight(4)
         weight(15)=weight(3)
         weight(16)=weight(2)
         weight(17)=weight(1)

        else if(N==18)then
         abscissas(1)=-0.99156516842093094673001600470615077d0
         abscissas(2)=-0.95582394957139775518119589292977631d0
         abscissas(3)=-0.89260246649755573920606059112714552d0
         abscissas(4)=-0.80370495897252311568241745501459080d0
         abscissas(5)=-0.69168704306035320787489108128884839d0
         abscissas(6)=-0.55977083107394753460787154852532914d0
         abscissas(7)=-0.41175116146284264603593179383305164d0
         abscissas(8)=-0.25188622569150550958897285487791123d0
         abscissas(9)=-0.084775013041735301242261852935783812d0
         abscissas(10)=-abscissas(9)
         abscissas(11)=-abscissas(8)
         abscissas(12)=-abscissas(7)
         abscissas(13)=-abscissas(6)
         abscissas(14)=-abscissas(5)
         abscissas(15)=-abscissas(4)
         abscissas(16)=-abscissas(3)
         abscissas(17)=-abscissas(2)
         abscissas(18)=-abscissas(1)
         weight(1)=0.021616013526483310313342710266452469d0
         weight(2)=0.049714548894969796453334946202638641d0
         weight(3)=0.076425730254889056529129677616636525d0
         weight(4)=0.100942044106287165562813984924834607d0
         weight(5)=0.122555206711478460184519126800201555d0
         weight(6)=0.140642914670650651204731303751947228d0
         weight(7)=0.154684675126265244925418003836374772d0
         weight(8)=0.164276483745832722986053776465927590d0
         weight(9)=0.169142382963143591840656470134986610d0
         weight(10)=weight(9)
         weight(11)=weight(8)
         weight(12)=weight(7)
         weight(13)=weight(6)
         weight(14)=weight(5)
         weight(15)=weight(4)
         weight(16)=weight(3)
         weight(17)=weight(2)
         weight(18)=weight(1)

        else if(N==19)then
         abscissas(1)=-0.9924068438435844031890176702532605d0
         abscissas(2)=-0.9602081521348300308527788406876515d0
         abscissas(3)=-0.9031559036148179016426609285323125d0
         abscissas(4)=-0.8227146565371428249789224867127139d0
         abscissas(5)=-0.7209661773352293786170958608237816d0
         abscissas(6)=-0.6005453046616810234696381649462393d0
         abscissas(7)=-0.4645707413759609457172671481041024d0
         abscissas(8)=-0.31656409996362983199011732884984492d0
         abscissas(9)=-0.16035864564022537586809611574074355d0
         abscissas(10)=0.0d0
         abscissas(11)=-abscissas(9)
         abscissas(12)=-abscissas(8)
         abscissas(13)=-abscissas(7)
         abscissas(14)=-abscissas(6)
         abscissas(15)=-abscissas(5)
         abscissas(16)=-abscissas(4)
         abscissas(17)=-abscissas(3)
         abscissas(18)=-abscissas(2)
         abscissas(19)=-abscissas(1)
         weight(1)=0.019461788229726477036312041464438435d0
         weight(2)=0.044814226765699600332838157401994211d0
         weight(3)=0.069044542737641226580708258006013044d0
         weight(4)=0.091490021622449999464462094123839652d0
         weight(5)=0.111566645547333994716023901681765997d0
         weight(6)=0.128753962539336227675515784856877117d0
         weight(7)=0.142606702173606611775746109441902972d0
         weight(8)=0.152766042065859666778855400897662998d0
         weight(9)=0.158968843393954347649956439465047201d0
         weight(10)=0.161054449848783695979163625320916735d0
         weight(11)=weight(9)
         weight(12)=weight(8)
         weight(13)=weight(7)
         weight(14)=weight(6)
         weight(15)=weight(5)
         weight(16)=weight(4)
         weight(17)=weight(3)
         weight(18)=weight(2)
         weight(19)=weight(1)


        else if(N==20)then
         abscissas(1)=-0.9931285991850949247861223884713203d0
         abscissas(2)=-0.9639719272779137912676661311972772d0
         abscissas(3)=-0.9122344282513259058677524412032981d0
         abscissas(4)=-0.8391169718222188233945290617015207d0
         abscissas(5)=-0.7463319064601507926143050703556416d0
         abscissas(6)=-0.6360536807265150254528366962262859d0
         abscissas(7)=-0.5108670019508270980043640509552510d0
         abscissas(8)=-0.3737060887154195606725481770249272d0
         abscissas(9)=-0.2277858511416450780804961953685746d0
         abscissas(10)=-0.07652652113349733375464040939883821d0
         abscissas(11)=-abscissas(10)
         abscissas(12)=-abscissas(9)
         abscissas(13)=-abscissas(8)
         abscissas(14)=-abscissas(7)
         abscissas(15)=-abscissas(6)
         abscissas(16)=-abscissas(5)
         abscissas(17)=-abscissas(4)
         abscissas(18)=-abscissas(3)
         abscissas(19)=-abscissas(2)
         abscissas(20)=-abscissas(1)
         weight(1)=0.017614007139152118311861962351852816d0
         weight(2)=0.040601429800386941331039952274932109d0
         weight(3)=0.062672048334109063569506535187041606d0
         weight(4)=0.083276741576704748724758143222046206d0
         weight(5)=0.101930119817240435036750135480349876d0
         weight(6)=0.118194531961518417312377377711382287d0
         weight(7)=0.131688638449176626898494499748163134d0
         weight(8)=0.142096109318382051329298325067164933d0
         weight(9)=0.149172986472603746787828737001969436d0
         weight(10)=0.152753387130725850698084331955097593d0
         weight(11)=weight(10)
         weight(12)=weight(9)
         weight(13)=weight(8)
         weight(14)=weight(7)
         weight(15)=weight(6)
         weight(16)=weight(5)
         weight(17)=weight(4)
         weight(18)=weight(3)
         weight(19)=weight(2)
         weight(20)=weight(1)
        else
         weight(1:N) = 0.d0
         abscissas(1:N) = 0.d0
        end if

        return
!EOC
        end subroutine absc_weight

!BOP
!!IROUTINE: face_analysis
!!DESCRIPTION:
!
!!INTERFACE:
        subroutine face_analysis(MNV,MNF,nAtom,tnAtom,nfaces,Iface,     &
     &                 nVertex2,Ivdex2,vertices,vf,fcount,istop)

!!REMARKS:
! private procedure of subroutine VPI_main
!EOP
!
!BOC
      implicit none
      integer i,j,k,MNV,MNF,nAtom,tnAtom,L2,n1
      integer Ivdex2(tnAtom,MNV,2)
      integer Iface(tnAtom,MNF,MNV+1),nfaces(tnAtom)
      integer nVertex2(tnAtom)
      integer vf(tnAtom,MNF,4),Vdex0(MNV)
      integer cut_count,fcount,FI,faceDex(MNF,4),NN
      real*8 vertices(tnAtom,3,MNV)
      character istop*10
      character sname*10
      parameter (sname='face_analy')

      do 403 i=1,nfaces(nAtom) !faces
       if ( (Iface(nAtom,i,1)+1) > size(Iface,3) )                      &
     &          call fstop(sname//' UNEXPECTED MEMORY ERROR')
       do j=2,1+Iface(nAtom,i,1)  !vertices index
        if(Iface(nAtom,i,j)==0) goto 403
        L2=Iface(nAtom,i,j)
        do k=1,nVertex2(nAtom)
         if(L2==Ivdex2(nAtom,k,1)) Iface(nAtom,i,j)=Ivdex2(nAtom,k,2)
           !replace the degenerate vertex to non-degenerate vertex
        end do
       end do
  403 continue

      !eliminate the duplicate vertices
      do 406 i=1,nfaces(nAtom) !faces
       j = Iface(nAtom,i,1)+1
       do while ( j > 2 ) 
        if ( Iface(nAtom,i,j)==0 ) exit
        k = j-1
        do while ( k > 1 )
         if ( iFace(nAtom,i,1) == 0 ) exit
         if ( Iface(nAtom,i,k)==Iface(nAtom,i,j) ) then
           do n1=j,iFace(nAtom,i,1)
            Iface(natom,i,n1) =  Iface(natom,i,n1+1)
           end do 
           iFace(nAtom,i,iFace(nAtom,i,1)+1)=0
           iFace(nAtom,i,1)=iFace(nAtom,i,1)-1
           exit
         end if
         k = k-1
        end do
        j = j-1
       end do
!       n1 = 1+Iface(nAtom,i,1)   ! or 31 
!DEBUG       do 407 j=3,31 !start from 3 because the first column is not vertex
!       do 407 j=3,n1        !start from 3 because the first column is not vertex
!  409   do 408 k=j-1,2,-1
!!??         if(Iface(nAtom,i,j)==Iface(nAtom,i,k).and.j<=Iface(nAtom,i,1)) &
!         if(Iface(nAtom,i,j)==Iface(nAtom,i,k))                         &
!     &       then
!            Iface(nAtom,i,j:30)=Iface(nAtom,i,j+1:31)
!!CAB            Iface(nAtom,i,j:Iface(nAtom,i,1))=                          &
!!CAB     &                  Iface(nAtom,i,j+1:Iface(nAtom,i,1)+1)
!            Iface(nAtom,i,j+1) = 0
!!?            Iface(nAtom,i,1) = Iface(nAtom,i,1) - 1
!                  !if there is one vertex repeat, over write it by the remain column
!            goto 409                      !then re-compare it again==> goto 407
!         else if(Iface(nAtom,i,j)==0)then
!             Iface(nAtom,i,1)=j-2
!        !substract 2 is because the first column is "number of vertices with possible duplication"
!        !so if the element of 5th-vertex(which is the 6th-column of Iface) is 0,
!        !then 6(6th)-2=4 ==> 4 sides for this plane
!            goto 406 !when it hit "0", change to next face
!         end if
!  408   continue
!  407  continue
  406 continue

!=======================================================================

      fcount=0
      do FI=1,nfaces(nAtom) !Number of Face
       NN = Iface(nAtom,FI,1)
       Vdex0=0
       fcount=fcount+1
       Vdex0(1:NN)=Iface(nAtom,FI,2:NN+1)
       call determineSQ(NN,MNV,Vdex0,                                   & !re-arrange the order
     &                       vertices(nAtom,:,:),Iface(nAtom,FI,2:NN+1))
       Iface(nAtom,FI,2:NN+1) = Vdex0(1:NN)
       call simple_cut(NN,MNV,MNF,Vdex0,cut_count,faceDex)
       vf(nAtom,fcount:fcount+cut_count-1,:)=faceDex(1:cut_count,:)
       fcount=fcount+cut_count-1
!       end if
!DEBUG       
!       i = 0
!       vv = 0
!       do j=1,fcount
!        do k=1,4
!        points(:,k) = vertices(nAtom,:,vf(nAtom,j,k))
!        end do
!        call CalcVol(4,points,dV)
!        vv = vv+dV
!       enddo
!       write(*,*) ' FI=',FI,' dV=',vv
!DEBUG       
      enddo

      if (istop.eq.sname) then
        call fstop(sname)
      endif
      return
!EOC
      end subroutine face_analysis

!BOP
!!IROUTINE: simple_cut
!!INTERFACE:
      subroutine simple_cut(NN,MNV,MNF,vdex0,cut_count,faceDex)
!!REMARKS:
! private procedure of subroutine VPI_main
!EOP
!
!BOC
      implicit none
      integer i,j,NN,MNV,MNF,Vdex0(MNV),faceDex(MNF,4)
      integer cut_count,temp(4)
!
      if ( NN <= 2 ) return
      faceDex=0
      cut_count=0

      j=NN
      temp(1)=Vdex0(1)
      do i=1,NN-2
       if(j>=4)then
        temp(2:4)=Vdex0(2*i:2*i+2)
        faceDex(i,:)=temp
        j=j-2
        if(j==2) exit
       else if(j==3)then
        temp(2:3)=Vdex0(2*i:2*i+1)
        temp(4)=Vdex0(2*i+1) !degenerate point
        faceDex(i,:)=temp
        exit
       endif
      end do
      cut_count=i
      ! write(6,*)  cut_count
      return
!EOC
      end subroutine simple_cut

!BOP
!!IROUTINE: determineSQ
!!INTERFACE:
      subroutine determineSQ(NN,MNV,Vdex0,vertices,Iface)
!!REMARKS:
! private procedure of subroutine VPI_main
!EOP
!
!BOC
      implicit none
      integer NN,MNV !NN-->nVertex
      integer I,Iface(NN),Vdex0(NN)
      real*8 vertices(3,MNV),origin(3)
      real*8 theta(NN),rx(3),ry(3),rz(3),x,y,pi,bri
      real(8) :: tmpind(NN),evert(3,NN)
      real(8), parameter :: eps=2.d0*epsilon(1.d0)
      integer :: irx,iry

      if ( NN <= 2 ) return
      pi=4*datan(1.0D0)
!      vertices1=0.0D0
!      origin=vertices(:,Iface(FI,2)) !define 1st-column(1st vertex) to be origin
      origin = vertices(:,Iface(1))
      irx = 0
      iry = 0
      do i=2,NN-1
       rx = vertices(:,Iface(i))-origin(:)
       if ( maxval(abs(rx)) > tiny(1.d0) ) then
        irx = i
        do j=NN,i+1,-1
         ry = vertices(:,Iface(j))-origin(:)
         if ( maxval(abs(ry)) > tiny(1.d0) ) then
          if ( maxval(abs(rx-ry))>tiny(1.d0) ) then
           iry = j
           go to 10
          end if
         end if
        end do 
       end if 
      end do
10    continue
      if ( irx==0 .or. iry==0 ) then
       write(6,*) ' NN=',NN
       write(6,'(10i5)') ' Iface=',Iface(1:NN)
       write(6,'(3g20.10)') ' vertex(1)=',vertices(:,Iface(1))
       write(6,*) ' vertex(i)-vertex(1), i=2:NN'
       do i=2,NN
        write(6,'(i4,3g20.10)')vertices(:,Iface(i))-vertices(:,Iface(1))
       end do
       call p_fstop(sname//' unexpected error due to degenerate face')
      end if
      origin = origin + 0.4d0 * ( rx + ry )
      do i=1,NN !define a relative-position vector
       evert(:,i)=vertices(:,Iface(i))-origin
       bri = sqrt(dot_product(evert(:,i),evert(:,i)))
       if ( bri>0.d0 ) then
        evert(:,i) = evert(:,i)/bri
       end if
      end do
!      write(*,*) ' Face with NN=',NN 

      rx=evert(:,irx)
      ry = evert(:,iry)- dot_product(evert(:,iry),rx)*rx
      bri = sqrt(dot_product(ry,ry))
      if ( bri>0.d0 ) then
       ry = ry / bri
      end if
      call cross(rx,ry,rz)
      if ( dot_product(rz(1:3),vertices(:,Iface(1))) > 0.d0 ) then
       rx = ry
       ry = evert(:,irx)
      end if 

!      Rot(1,1:3)=rx
!      Rot(2,1:3)=ry
!      Rot(3,1:3)=rz
!
!      call dMRRRR(3,3,Rot,3,3,NN,vertices1(:,1:NN),3,3,NN,vector,3)
!      !!!call dwrrrl('   Rot   ',3,3,Rot,3,0,'(f6.3)','number','number')
!      vertices1(:,1:NN)=vector !here, we turn the vertices into x-y plane
!      !!!call dwrrrl('   vertices(after)   ',3,NN,vertices1,3,0,'(f16.13)','number','number')
!      !define the angle between each vector(:,i) and x-axis
!      theta(1)=0.0d0
!      theta(2)=pi/2.0d0
!
      do i=1,NN
!       x=vertices1(1,i)
!       y=vertices1(2,i)
        x  = dot_product(evert(:,i),rx)
        y  = dot_product(evert(:,i),ry)
!
!        z = dot_product(evert(:,i),rz)
!        write(*,*) x,y,z
       if( abs(y) < eps )then
        if(x>0) theta(i)=0.0d0
        if(x<0) theta(i)=pi
       else if( y > 0 ) then
        if( x > 0 )  theta(i) = datan(y/x)
        if( x < 0 )  theta(i) = pi+datan(y/x) !in this region, datan(y/x)<0
        if( x == 0 ) theta(i) = pi/2.d0
       else if( y < 0 ) then
        if( x > 0 )  theta(i) = 2.0d0*pi+datan(y/x) !in this region, datan(y/x)<0
        if( x < 0 )  theta(i) = pi+datan(y/x)
        if( x == 0 ) theta(i) = 3.d0*pi/2.d0
       end if
!
!       tmptheta(i) = theta(i)
      enddo

      !determine the sequence of the vertices

      tmpind(1:NN) = Vdex0(1:NN)
      call dsort(theta,tmpind,NN,2)
      Vdex0(1:NN) = nint(tmpind(1:NN))
      return
!EOC
      end  subroutine determineSQ

!BOP
!!IROUTINE: cross
!!INTERFACE:
      subroutine cross(a,b,c) !a (cross) b = c
!!REMARKS:
! private procedure of subroutine VPI_main
!EOP
!
!BOC
      implicit none
      real*8 a(3),b(3),c(3)

      c(1)=a(2)*b(3)-a(3)*b(2)
      c(2)=a(3)*b(1)-a(1)*b(3)
      c(3)=a(1)*b(2)-a(2)*b(1)

      return
!EOC
      end subroutine cross

      end subroutine VPI_main
