!BOP
!!ROUTINE: qexpup
!!INTERFACE:
      subroutine qexpup(ip,f,n,x,fint)
!!DESCRIPTION:
! integrates real function exp(ip*x) * f(x); {\tt x} is log-mesh
!
!!USES:
      use universal_const

!!DO_NOT_PRINT
      implicit none
!!DO_NOT_PRINT

!!ARGUMENTS:
      integer, intent(in) :: ip,n
      real(8), intent(in) :: f(n),x(n)
      real(8), intent(out) :: fint(n)
!!REVISION HISTORY:
! Adapted - A.S. - 2013
!EOP
!
!BOC
      real(8), parameter :: half=one/two
      real(8), parameter :: third=one/three
!c
      real(8) :: xm,xh,xl,x1,x2,x3,y1,y2,y3,smhsav,slm,smh
      integer i
!c
      fint(1)=zero
      xm=x(1)
      xh=half*(x(1)+x(2))
!      xl=x(2)
!      call qexpp(ip,xl,xm,xh,x(1),x(2),x(3),f(1),f(2),f(3),slm,smh)
      x1 = x(1) - (x(2)-x(1))
      xl=half*(x1+x(1))
      x2 = x(1)
      x3 = x(2)
      y2 = f(1)
      y3 = f(2)
      y1 = y2 + (x1-x2)*(y3-y2)/(x3-x2)  
      call qexpp(ip,xl,xm,xh,x1,x2,x3,y1,y2,y3,slm,smh)
      do i=2,n-1
         smhsav=smh
         xl=half*(x(i-1)+x(i))
         xm=x(i)
         xh=half*(x(i+1)+x(i))
         x1=x(i-1)
         x2=x(i)
         x3=x(i+1)
         y1=f(i-1)
         y2=f(i)
         y3=f(i+1)
         call qexpp(ip,xl,xm,xh,x1,x2,x3,y1,y2,y3,slm,smh)
         fint(i)=fint(i-1)+smhsav+slm
      enddo
      smhsav=smh
      xl=x(n-1)
      xm=half*(x(n)+x(n-1))
      xh=x(n)
      call qexpp(ip,xl,xm,xh,x(n-2),x(n-1),x(n),f(n-2),f(n-1),f(n),     &
     &            slm,smh)
      fint(n)=fint(n-1)+smhsav+smh
      return
!EOC
      CONTAINS
!
!BOP
!!IROUTINE: qexpp
!!INTERFACE:
      subroutine qexpp(ip,xl,xm,xh,x1,x2,x3,y1,y2,y3,slm,smh)
!!DESCRIPTION:
! one-step integration
!
!!REMARKS:
! private procedure of subroutine qexpup
!EOP
!
!BOC
      implicit none
      integer, intent(in)  :: ip
      real(8), intent(in)  :: xl,xm,xh,x1,x2,x3,y1,y2,y3
      real(8), intent(out) :: slm,smh
      real(8) :: a,b,c,ap,bp,pinv,pinv2,app,bpp
      real(8) :: xl2,xm2,xh2,xl3,xm3,xh3
      real(8) :: enxl,enxm,enxh,xenxl,xenxm,xenxh,x2enxl,x2enxm,x2enxh
!c     ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!      real*8  half,third
!c     ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!      parameter (half=one/two)
!      parameter (third=one/three)
!c     ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
      xl2=xl*xl
      xm2=xm*xm
      xh2=xh*xh
      xl3=xl2*xl
      xm3=xm2*xm
      xh3=xh2*xh
      a=y1
      b=(y2-y1)/(x2-x1)
      c=( (y3-y1)/(x3-x1) -b )/(x3-x2)
      ap=a-b*x1 + c*x1*x2
      bp=b-c*(x1+x2)
!c
      if ( ip.ne.0 ) then
       pinv=one/ip
       pinv2=pinv*pinv
       enxl=exp(ip*xl)
       enxm=exp(ip*xm)
       enxh=exp(ip*xh)
       xenxl=xl*enxl
       xenxm=xm*enxm
       xenxh=xh*enxh
       x2enxl=xl2*enxl
       x2enxm=xm2*enxm
       x2enxh=xh2*enxh
       app=ap-bp*pinv+two*c*pinv2
       bpp=bp-two*c*pinv
       slm=(app*(enxm-enxl)+bpp*(xenxm-xenxl)+c*(x2enxm-x2enxl))*pinv
       smh=(app*(enxh-enxm)+bpp*(xenxh-xenxm)+c*(x2enxh-x2enxm))*pinv
      else
       slm=ap*(xm-xl)+half*bp*(xm2-xl2)+third*c*(xm3-xl3)
       smh=ap*(xh-xm)+half*bp*(xh2-xm2)+third*c*(xh3-xm3)
      end if
!c
      return
!EOC
      end subroutine qexpp
!c
      end subroutine qexpup
