!BOP
!!MODULE: polyintrpl
!!INTERFACE:
      module polyintrpl
!!DESCRIPTION:
! {\sc mecca} related integration/differentiation based on
! polynomial interpolation
!

!!DO_NOT_PRINT
      implicit none
      private
!!DO_NOT_PRINT

!!PUBLIC MEMBER FUNCTIONS:
      public :: cpolyval,cpolyintegral
      public :: rpolyval,derv_expmesh
!!REVISION HISTORY:
! Initial Version - A.S. - 2013
!TO DO:
! to include (or not?) ylag.f in the module
!EOP
!
!BOC

!EOC
      contains
!
!BOP
!!IROUTINE: cpolyval
!!INTERFACE:
      function cpolyval( E,zk,Fk )
!!DESCRIPTION:
!  evaluates Lagrange polynomial interpolant (in complex plane)

!!DO_NOT_PRINT
      implicit none
      complex(8) :: cpolyval
!!DO_NOT_PRINT

!!ARGUMENTS:
      complex(8), intent(in) :: E
      complex(8), intent(in) :: zk(:),Fk(:)
!!REVISION HISTORY:
! Initial Version - A.S. - 2013
!!REMARKS:
!EOP
!
!BOC
      real(8)    :: rtmp(size(zk))
      complex(8) :: ctmp(size(zk))
      complex(8) :: P
      integer k,nk,j

      nk = size(zk)
      ctmp(1:nk) = E-zk(1:nk)
      rtmp(1:nk) = abs(ctmp(1:nk))
      if ( minval(rtmp) .ne. 0 ) then
       cpolyval = 0
       do j=1,nk
        P = Fk(j)
        do k=1,nk
         if ( k==j ) cycle
         P=P*(ctmp(k)/(zk(j)-zk(k)))
        end do
        cpolyval = cpolyval + P
       end do
      else
       ctmp(1:nk) = 0
       ctmp(minloc(rtmp)) = 1
       cpolyval = sum(ctmp(1:nk)*fk(1:nk))
      end if

      return
!EOC
      end function cpolyval
!
!BOP
!!IROUTINE: cpolyintegral
!!INTERFACE:
      function cpolyintegral(Tempr,e0,e1,nL,nR,zk,Fk)
!!DESCRIPTION:
!
! integration over semi-circular contour (based on segment [e0,e1])
! of polynomial interpolant
!

!!DO_NOT_PRINT
      implicit none
      complex(8) :: cpolyintegral
!!DO_NOT_PRINT

!!ARGUMENTS:
      real(8), intent(in) :: Tempr    ! if Tempr>0, interpolation may be not good as Fermi factor is included in weights
      real(8), intent(in) :: e0,e1    ! e0<e1
      integer, intent(in) :: nL,nR    ! nL is for points below e1, if Tempr>0 then nR is for points above e1
      complex(8), intent(in) :: zk(:),Fk(:) ! function F(z), Re(zk(1))>e0
!!REVISION HISTORY:
! Initial Version - A.S. - 2013
!EOP
!
!BOC
      complex(8) :: zi(nL+nR),wi(nL+nR),gi(nL+nR)
      integer :: i,n,n1,n2
      character(10) :: dummy='dummy'
      n1 = nL ; n2 = nR
      call conTempr(Tempr,e0,e1,zi,wi,n1,n2,-100,.false.,dummy)
!      em = (e0+e1)/2
!      erad = (e1-e0)/2
!      beta =  0.d0
!      alpha = 4.d0*atan(1.d0)
!      call conlegndr(em,erad,alpha,beta,n1,zi,wi)
      do i=1,n1
       gi(i) = cpolyval(zi(i),zk(1:n1),Fk(1:n1))
      end do
      n = n1+n2
      do i=n1+1,n
       gi(i) = cpolyval(zi(i),zk(n1:n),Fk(n1:n))
      end do
      cpolyintegral = sum(wi(1:n)*gi(1:n))
!DEBUGPRINT
!      write(*,*) ' DEBUG cPolyInt=',cpolyintegral
!      em = (zk(1)+zk(n1))/2
!      erad = abs(zk(1)-em)
!      call conlegndr(em,erad,alpha,beta,n1,zi,wi)
!      write(*,*) ' DEBUG w*F=',sum(wi(1:n1)*Fk(1:n1))
!DEBUGPRINT
      return
!EOC
      end function cpolyintegral
!
!BOP
!!IROUTINE: rpolyval
!!INTERFACE:
      function rpolyval( x0,zk,Fk )
!!DESCRIPTION:
! evaluates Lagrange polynomial interpolant (real axis)

!!DO_NOT_PRINT
      implicit none
      real(8) :: rpolyval
!!DO_NOT_PRINT

!!ARGUMENTS:
      real(8), intent(in) :: x0
      real(8), intent(in) :: zk(:),Fk(:)
!!REVISION HISTORY:
! Initial Version - A.S. - 2013
!EOP
!
!BOC
      real(8) :: rtmp(size(zk))
      real(8) :: ctmp(size(zk))
      real(8) :: P
      integer k,nk,j

      nk = size(zk)
      ctmp(1:nk) = x0-zk(1:nk)
      rtmp(1:nk) = abs(ctmp(1:nk))
      if ( minval(rtmp) .ne. 0 ) then
       rpolyval = 0
       do j=1,nk
        P = Fk(j)
        do k=1,nk
         if ( k==j ) cycle
         P=P*(ctmp(k)/(zk(j)-zk(k)))
        end do
        rpolyval = rpolyval + P
       end do
      else
       ctmp(1:nk) = 0
       ctmp(minloc(rtmp)) = 1
       rpolyval = sum(ctmp(1:nk)*fk(1:nk))
      end if

      return
!EOC
      end function rpolyval
!
!BOP
!!IROUTINE: derv_expmesh
!!INTERFACE:
      subroutine derv_expmesh(y,dy,r,nn)
!!DESCRIPTION:
! calculates derivative {\tt dy} of real function {\tt y} defined
! on the exponential mesh {\tt r}, 4-points interpolation is applied;
! it is assumed that r(i) = exp(x0+h*i)

!!DO_NOT_PRINT
      implicit none
!!DO_NOT_PRINT

!!ARGUMENTS:
      real(8), intent(in) :: y(nn)
      real(8), intent(out) :: dy(nn)
      real(8), intent(in) :: r(nn)
      integer, intent(in) :: nn
!!REVISION HISTORY:
! Initial Version - A.S. - 2013
!!REMARKS:
!  the procedure aborts if nn<=4
!EOP
!
!BOC
!
      real(8), parameter :: a1(3)=[-1.5d0,2.d0,-0.5d0]
      real(8), parameter :: c(5)=                                       &
     &                [1.d0/12.d0,-2.d0/3.d0,0.d0,2.d0/3.d0,-1.d0/12.d0]
      real(8), parameter :: b1(3)=[0.5d0,-2.d0,1.5d0]
      integer :: i,k,k1
      real(8) :: h0
      real(8), parameter :: nh = 10
      real(8) :: p(5),x

      if ( nn>= 5 ) then
       h0 = log(r(2)/r(1))/nh
       k=1
       p(1) = y(k)
       do i=2,3
        x = r(k)*exp((i-1)*h0)
        p(i) = rpolyval(x,r(1:3),y(1:3))
       end do
       dy(k) = dot_product(a1(1:3),p(1:3))
       do k=2,nn-1
        k1 = min(nn-4,max(1,k-2))
        do i=-2,2
         x = r(k)*exp(i*h0)
         p(3+i) = rpolyval(x,r(k1:k1+4),y(k1:k1+4))
        end do
        dy(k) = dot_product(c(1:5),p(1:5))
       end do
       k=nn
       p(3) = y(k)
       do i=1,2
        x = r(k)*exp((-3+i)*h0)
        p(i) = rpolyval(x,r(nn-2:nn),y(nn-2:nn))
       end do
       dy(k) = dot_product(b1(1:3),p(1:3))
      else
       write(*,*) ' ERROR: nn=',nn,' < 5'
       stop ' polynomial derv_expmesh is not implemented for nn<5'
      end if
      dy = dy/(h0*r)
      return
!EOC
      end subroutine derv_expmesh
!
      end module polyintrpl
