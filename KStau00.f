!BOP
!!ROUTINE: KStau00
!!INTERFACE:
      subroutine KStau00(greens,                                        &
     &                    kkrsz,natom,tcpa,tau00,ndkkr,itype,           &
     &                    invswitch,invparam,                           &
     &                    iprint,istop)
!!DESCRIPTION:
! {\bv
! input:
!         greens -- ref-Green's function (G0), order of KKRSZ*NATOM
!         tcpa -- diagonal of t-matrix, order KKRSZ
!
! output:
!         tau00 -- block-diagonal Tau (rank of full-matrix KKRSZ*NATOM)
!                == t*[1-G0*t]^(-1)
! \ev}

!!USES:
      use mtrx_interface, only : mmul
!DEBUGPRINT
!DELETE      use sparse, only : wrt1ccs,wrt2ccs

!!DO_NOT_PRINT
      implicit none
!!DO_NOT_PRINT

!!ARGUMENTS:
      complex*16   greens(*)
      integer      kkrsz,natom,ndkkr
      complex(8), intent(in) :: tcpa(ndkkr,ndkkr,*)
      integer      itype(natom)
      integer      invswitch,invparam,iprint
      complex(8), intent(out) :: tau00(ndkkr,ndkkr,*)
      character    istop*10
!!REVISION HISTORY:
! Initial Version - A.S. - 2017
!EOP
!
!BOC
!c      integer      mapstr(ndimbas,natom),mappnt(ndimbas,natom)
!c      integer      mapij(2,*)
      integer :: i0,ns,nrmat
      complex(8) :: a(kkrsz,kkrsz)

!c     ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
      character    sname*10
      parameter    (sname='KStau00')
      integer, parameter :: idiag=0
!c     ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!c
!c  invert matrix to get (1-G*t)^(-1) ...........................
!c  ---------------------------------------------------------------------

      nrmat = kkrsz*natom
!DEBUGPRINT
!DELETE      i0 = 2
!DELETE      call cpblk2(greens,nrmat,i0,i0,a,kkrsz,1,1,kkrsz)
!DEBUGPRINT

      call inv1mgt(greens,nrmat,kkrsz,natom,tcpa,                       &
     &                  ndkkr,idiag,                                    &
     &                  itype,                                          &
     &                  invswitch,invparam,                             &
     &                  iprint,istop)

!c  Now greens = [1-G*t]^(-1)  if t = tcpa
!c        ---------------------------------------------------------------
!DEBUGPRINT
!DELETE        call mmul(a,reshape(greens(1:kkrsz**2),[kkrsz,kkrsz]),          &
!DELETE     &                      tau00(:,:,i0),kkrsz)
!DELETE        write(6,*) ' DEBUG G (KS):'
!DELETE         call wrt2ccs(kkrsz,tau00(1:kkrsz,1:kkrsz,i0),1.d-9)
!DEBUGPRINT

!c
!c  diagonal (I,I)-block
!c
      do i0 = 1,natom
        call cpblk2(greens,nrmat,i0,i0,a,kkrsz,1,1,kkrsz)
        ns = itype(i0)
        call mmul(tcpa(:,:,ns),a,tau00(:,:,i0),kkrsz)
!        tau00(1:kkrsz,1:kkrsz,i0) = matmul(tcpa(1:kkrsz,1:kkrsz,ns),a)
      end do
!
!c  Now tau00 = t*[1-G*t]^(-1)
!c     ==================================================================
      if (istop.eq.sname) call fstop(sname)
!c

      return
!EOC
      end subroutine KStau00
