!c
!c     ================================================================== !=
!BOP
!!ROUTINE: alpha2
!!INTERFACE:
      function alpha2(rs0,dz0,sp,iexch,exchg)
!!DESCRIPTION:
! exchange-correlation potential and energy:
! {\bv
!  Hedin and Lundqvist (LDA) 
!  Vosko-Wilk-Nusair   (LDA)
!  AM05 (GGA)
! \ev}
!!USES:
      use mecca_constants, only : XC_LDA_HEDIN,XC_LDA_VOSKO,XC_GGA_AM05

!!DO_NOT_PRINT
      implicit real*8 (a-h,o-z)
      real(8) :: alpha2
!!DO_NOT_PRINT

!!ARGUMENTS:
      real(8), intent(in) :: rs0,dz0,sp
      real(8), intent(out) :: exchg     ! XC energy density
!                         !   alpha2    ! XC potential
      integer :: iexch
!!REVISION HISTORY:
! Initial version - DDJ - 1984 
! Adapted - A.S. - 2013
!EOP
!
!BOC
      real(8), parameter :: gup2=0, gupdn=0, gdn2=0
      real(8) :: rs,dz,rhoup,rhodn,v2rho2_uu,v2rho2_dd,v2rho2_ud
      real(8) :: sp_
      real(8), external :: g_pi

      dimension vxx(2),vxcs(2),g(3),dg(3),tbq(3),tbxq(3)
      dimension bxx(3),a(3),b(3),c(3),q(3),x0(3),bb(3),cx(3)
!c
!c  ===================================================================== !=
!c     exchange-correlation potential and energy  written by DDJ 1984
!c  ===================================================================== !=
!c
!c     data for hedin
!c
      data ccp,rp,ccf,rf/0.045d0,21.d0,0.0225d0,52.916682d0/
!c
!c     data for vosko-wilk-nusair
!c
      data incof/0/
      data  a/-0.033774d0,0.0621814d0,0.0310907d0/
      data  b/1.13107d0,3.72744d0,7.06042d0/
      data  c/13.0045d0,12.9352d0,18.0578d0/
      data x0/-0.0047584d0,-0.10498d0,-0.3250d0/
      data cst,aip/1.92366105d0,0.91633059d0/
      data fnot,bip/1.70992095d0,0.25992105d0/
      parameter (for3=4.d0/3.d0,thrd=1.d0/3.d0)
      save
!c
!c  the following are constants needed to obtain potential
!c  which are in g.s. painter's paper
!c  =====given here for check=====(generated below)
!c
!c     data q/.7123108918d+01,.6151990820d+01,.4730926910d+01/
!c     data bxx/-.4140337943d-03,-.3116760868d-01,-.1446006102/
!c     data tbq/.3175776232d0,.1211783343d+01,.2984793524d+01/
!c     data tbxq/.3149055315d0,.1143525764d+01,.2710005934d+01/
!c     data bb/.4526137444d+01,.1534828576d+02,.3194948257d+02/
!c
!c     write(6,'('' iexch,rs,dz,sp '',i5,3f10.5)') iexch,rs,dz,sp
!c

      if( rs0==0.d0 ) then
       alpha2 = 0.d0
       exchg  = 0.d0
       return
      end if
      
      rs = abs(rs0)  ! no physics here !!!
      if ( abs(dz0)>1.d0 ) then
       dz = sign(1.d0,dz0)
      else
       dz = dz0
      end if
      if ( rs0<0.d0 ) dz=-dz

      sp_ = sign(1.d0,sp)

      select case (iexch)
      case (XC_LDA_HEDIN)
!c
!c     von barth-hedin  exch-corr potential
!c     j. phys. c5,1629(1972)
!c
!c 05.28.2013:
!c  IT LOOKS LIKE THAT PARAMETERS USED ARE NOT FROM VON BARTH-HEDIN XC;
!c  NUMERICALLY, FUNCTIONAL IS VERY CLOSE TO 
!c     L Hedin and BI Lundqvist, J. Phys. C4, 2064 (1971) 
!c     (idx=XC_LDA_X(=1),idc=XC_LDA_C_HL(=4) in libXc)    
!c
        incof = 10
        fm=2.d0**(for3)-2.d0
        fdz=((1.d0+dz)**(for3) +                                        &
     &       (1.d0-dz)**(for3)-2.d0)/fm
        ex=-0.91633d0/rs
        exf=ex*2.d0**thrd
        xp=rs/rp
        xf=rs/rf
        gp=(1.d0+xp**3)*log(1.d0+1.d0/xp)                               &
     &   - xp*xp +xp/2.d0 - thrd
        gf=(1.d0+xf**3)*log(1.d0+1.d0/xf)                               &
     &   - xf*xf +xf/2.d0 - thrd
        exc=ex-ccp*gp
        excf=exf-ccf*gf
        dedz=(for3)*(excf-exc)*((1.d0+dz)**(thrd)                       &
     &   -(1.d0-dz)**(thrd))/fm
        gpp=3.d0*xp*xp*log(1.d0+1.d0/xp)-1.d0/xp                        &
     &   +1.5d0-3.d0*xp
        gfp=3.d0*xf*xf*log(1.d0+1.d0/xf)-1.d0/xf                        &
     &   +1.5d0-3.d0*xf
        depd=-ex/rs-ccp/rp*gpp
        defd=-exf/rs-ccf/rf*gfp
        decd=depd+(defd-depd)*fdz
!c exchange-correlation energy
        exchg= exc + (excf-exc)*fdz
!c exchange-correlation potential
!c      alpha2=exc+(excf-exc)*fdz-rs*decd/3.d0+sp*(1.d0-sp*dz)*dedz
        alpha2 = -rs*decd/3.d0
!C
        alpha2 = alpha2+sp_*(1.d0-sp_*dz)*dedz
        alpha2 = exchg + alpha2
        return
!
      case (XC_LDA_VOSKO)
!c
!c   vosko-wilk-nusair exch-corr potential 
!c    taken from g.s. painter  ( phys. rev. b24 4264,1981)
!c     (idx=XC_LDA_X(=1),idc=XC_LDA_C_VWN(=7) in libXc)    
!c
!c   generate constant coefficients for the parameterization (v-w-n)
!c
        if(incof.ne.20) then
          incof=20
          do i=1,3
           cx(i)= x0(i)**2 + b(i)*x0(i) + c(i)
           bfc= 4.d0*c(i) - b(i)**2
           q(i)= sqrt(bfc)
           bxx(i)= b(i)*x0(i)/cx(i)
           tbq(i)= 2.d0*b(i)/q(i)
           tbxq(i)= tbq(i) + 4.d0*x0(i)/q(i)
           bb(i)= 4.d0*b(i)*( 1.d0 - x0(i)*(b(i) + 2.d0*x0(i))/cx(i) )
          end do
!c
        end if
        zp1= 1.d0 + dz
        zm1= 1.d0 - dz
        xr=sqrt(rs)
        pex= -aip/rs
        xrsq= rs
!c
!c   generate g(i)=alpha,epsilon fct.s
!c     and their derivatives dg(i)
!c    1=alpha(spin stiffness)  2=ecp  3=ecf
!c
        do i=1,3
         qi=q(i)
         txb= 2.d0*xr + b(i)
         fx= xrsq + xr*b(i) + c(i)
         arct= atan2(qi,txb)
         dxs= (xr-x0(i))**2/fx
!c
         g(i)=a(i)*( log(xrsq/fx) + tbq(i)*arct-bxx(i)*                 &
     &                       (log(dxs) + tbxq(i)*arct) )
!c
         dg(i)=a(i)*( 2.d0/xr - txb/fx -                                &
     &    bxx(i)*(2.d0/(xr-x0(i))-txb/fx) - bb(i)/(qi**2 + txb**2) )
!c
        end do
!c
        ecp=g(2)
        zp3=zp1**thrd
        zm3=zm1**thrd
        zp3m3=zp3-zm3
!c part of last term in vx   eq(13)
        fx1=.5d0*for3*pex*zp3m3
        z4= dz**4
        fz= cst*(zp1**for3 + zm1**for3 - 2.d0)
        beta= fnot*( g(3)-g(2) )/g(1) -1.d0
        ec= ecp + fz*g(1)*( 1.d0 + z4*beta )/fnot
        ex= pex*( 1.d0 + fz*bip )
        f3ex= for3*ex
!c exchange-correlation energy
        exchg= ec + ex
!c exchange potential
        vxx(1)= f3ex + fx1*zm1
        vxx(2)= f3ex - fx1*zp1
!c correlation potential
        vcc= ec - xr*( (1.d0-z4*fz)*dg(2) + z4*fz*dg(3)                 &
     &                  +  (1.d0 - z4)*fz*dg(1)/fnot     )/6.d0
!c
        facc= 4.d0*g(1)*( dz**3*fz*beta + ( 1.d0 + beta*z4 )*zp3m3      &
     &                                      /(6.d0*bip) )/fnot
!c
!c exch-corr. potential for each spin as called in newpot
!c
        vxcs(1)= vcc + zm1*facc + vxx(1)
        vxcs(2)= vcc - zp1*facc + vxx(2)
!c
!        alpha2 = 0.5d0*(vxcs(1)+vxcs(2)) - exchg
!        if( sp.ge.0 ) then
!         alpha2 = alpha2 + 0.5d0*(vxcs(1)-vxcs(2))
!        else
!         alpha2 = alpha2 - 0.5d0*(vxcs(1)-vxcs(2))
!        end if
!        alpha2 = exchg + alpha2     ! exchange + correlation
!c
        if( sp.ge.0 ) then
          alpha2= vxcs(1)
        else
          alpha2= vxcs(2)
        end if

        return
      case (XC_GGA_AM05)
         rhoup = 0.5d0*(1.d0+dz)/(for3*g_pi()*rs**3) 
         rhodn = 0.5d0*(1.d0-dz)/(for3*g_pi()*rs**3)
         call am05pgjs(rhoup,rhodn,gup2,gdn2,gupdn,                     &
     &                exchg,vxcs(1),vxcs(2),                            &
     &                v2rho2_uu,v2rho2_dd,v2rho2_ud)
         exchg = exchg/(ry2H*(rhoup+rhodn))    ! now in Ry
         if( sp.ge.0 ) then
          alpha2= vxcs(1)/ry2H
         else
          alpha2= vxcs(2)/ry2H
         end if
        return
      case default
          alpha2 = 0.d0
          exchg = 0.d0 
      end select

      return
!EOC
      end function alpha2

