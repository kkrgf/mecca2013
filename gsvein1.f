!BOP
!!ROUTINE: gsvein1
!!INTERFACE:
      subroutine gsvein1(amt,ndmat,                                     &
     &                  cof,ndcof,                                      &
     &                  kkrsz,natom,                                    &
     &                  logdet)
!!DESCRIPTION:
! inversion of complex matrix {\tt amt} using {\em LAPACK}; \\
! inverted matrix replaces input {\tt amt};\\
! if ndcof$>$0, output argument {\tt cof} is a set of diagonal blocks 
! of inverted matrix {\tt amt}$^{-1}$;\\
! optional argument {\tt logdet} is $log(det(amt))$
! (here {\tt amt} is {\em input} matrix)
!

!!DO_NOT_PRINT
      implicit none
!!DO_NOT_PRINT

!!ARGUMENTS:
      integer, intent(in) :: ndmat,ndcof,kkrsz,natom
      complex(8) :: amt(ndmat,kkrsz*natom)    ! (ndmat,*)
      complex(8) :: cof(*)                    ! (ndcof*ndcof*natom)
      complex(8), intent(out), optional :: logdet
!!PARAMETERS:
!!REVISION HISTORY:
! Adapted - A.S. - 2013
!!REMARKS:
!EOP
!
!BOC

      integer :: info,lwork
      integer :: nrmat,jc,jcs,jr,jrs,ii

      complex(8), allocatable :: bmt(:)
      integer :: ipiv(kkrsz*natom)
      integer :: nblock
      integer, external :: ilaenv

!c
!c     ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!c parameter
      complex*16 czero
      parameter (czero=(0.d0,0.d0))
      complex(8), parameter :: log_i=cdlog((-1.d0,0.d0))  ! (0,pi)
      character  sname*10
      parameter (sname='gsvein1')
      real(8), parameter :: memlim = 100*2**20

!      integer, save :: isave = 0

!c     ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!c
!c     **************************************************************
!c     input tau**(-1) and calculates tau............................
!c     full matrix storage version...........bg & gms [Nov 1991].....
!c     **************************************************************
!c
      nrmat = kkrsz*natom
      nblock = max(1,ilaenv(1,'ZGETRI','',nrmat,nrmat,0,0))

!      if(isave.eq.0) then
!       write(6,'(/,''INVERT MATRIX: LAPACK LU-DECOMP+PARTIAL PIVOT'')')
!       isave=1
!      end if

      call zgetrf( nrmat,nrmat,amt,ndmat,ipiv,info)

      if(info.ne.0) then

!c          = 0:  successful exit
!c          < 0:  if INFO = -i, the i-th argument had an illegal value
!c          > 0:  if INFO = i, U(i,i) is exactly zero. The factorization
!c                has been completed, but the factor U is exactly
!c                singular, and division by zero will occur if it is used
!c                to solve a system of equations.

       write(6,*) sname//' :: AFTER LU-FACTORIZATION  INFO=',info
       if(info.lt.0) write(6,*)                                         &
     & '  if INFO = -i, the i-th argument had an illegal value'
       if(info.gt.0) write(6,*)                                         &
     & '  if INFO = +i, U(i,i) is exactly zero, and division by zero'   &
     &, ' will occur if it is used to solve a system of equations'
       call fstop(sname)
      end if

      if ( present(logdet) ) then
      ! suff: extract log of determinant for lloyd's formula
       logdet = czero
       do ii = 1, nrmat
!        det_ = det_ * amt(ii,ii)
        logdet = logdet + cdlog(amt(ii,ii))
        if( ipiv(ii) /= ii ) then
         logdet = logdet + log_i
        end if
       end do
      end if

      lwork = kkrsz*natom*nblock
      if ( allocated(bmt) ) then
       if ( size(bmt)<lwork ) deallocate(bmt)
      end if
      if ( .not.allocated(bmt) ) allocate(bmt(lwork))

      call zgetri(nrmat,amt,ndmat,ipiv,bmt,lwork,info)

      if ( allocated(bmt) ) then
       if ( lwork > memlim/16 ) deallocate(bmt)
      end if

      if(info.ne.0) then
       write(6,*) sname//' ::  INFO=',info
       call fstop(sname)
      end if

!c     fill tau by the inverse of amt...................................

      if(ndcof.gt.0) then
       do ii=1,natom
       do jc=1,kkrsz
          jcs=jc+kkrsz*(ii-1)
            do jr=1,kkrsz
               jrs=jr+kkrsz*(ii-1)
!CAB               cof(jr,jc,ii)=amt(jrs,jcs)
             cof((((ii-1)*ndcof+(jc-1))*ndcof)+jr)=amt(jrs,jcs)
            enddo
         enddo
       enddo
      end if
!c     ==============================================================
!c
      return
      end subroutine gsvein1
