      subroutine gsvlusp(a,ndmat,                                       &
     &                   nnptr,nnclmn,ndimnn,                           &
     &                   cof,ndcof,                                     &
     &                   itype,kkrsz,natom,                             &
!CALAM     *                   invparam,niter,tolinv,iprint
     &                   iprint)
!c     ================================================================
!c
      implicit real*8 (a-h,o-z)
!c
!c      To find solution of equation:  A*X=1  (A=1-G*t)
!c      using sparse LU algorithm
!c
!c      and then output:  cof = X  (tau = t*cof)
!c
!c      niter -- maximum number of iterations
!c
!c
!c
!c     ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!c
      integer ndmat,natom,kkrsz
      complex*16 a(ndmat,*)
!c  a(ndmat,1:natom), ndmat .ge. maxclstr*kkrsz**2

      integer nnptr(0:1,natom),ndimnn,nnclmn(2,ndimnn,natom)
!CALAM      complex*16 bmt(ndbmt*ndbmt,*)
!c      complex*16 vecs(kkrsz*natom,*)
      integer ndcof
!CALAM      complex*16 t(ndcof,ndcof,*)
      complex*16 cof(ndcof,ndcof,natom)
      integer    itype(natom)
!CALAM      logical precond
!CALAM      integer invparam,niter
!CALAM      real*8 tolinv
      integer iprint

!c      complex*16 sum,sum0
      complex*16 czero,cone
!CALAM      integer cx,cb,revcom
      parameter (czero=(0.d0,0.d0),cone=(1.d0,0.d0))

      integer erralloc
      complex*16,  allocatable :: value(:)
      complex*16,  allocatable :: vecs(:,:)

      integer,     allocatable :: iptrow(:)
      integer,     allocatable :: indcol(:)

      integer,  allocatable :: work(:)
      integer lwork

      character sname*10
      parameter (sname='gsvlusp')

      complex*16 aij
!c      complex*16 aij,aii
      integer isave/0/,l1a,j,ii
!c      integer isave/0/,ntry,l1a,j,jj,ii
      save isave
!CDEBUG
      real timeEn,timeIneq
      common /Alltime/timeEn,timeIneq
!CDEBUG

      if(isave.eq.0) then
       write(6,*)
       write(6,*)                                                       &
     & '    SuperLU PACKAGE ',                                          &
     &     'IS USED TO INVERT MATRIX'
!c       write(6,*) '  MAXIMUM NUMBER OF ITERATIONS = ',niter,
!c     *     ' AND TOLINV=',real(tolinv)
       write(6,*)
       isave=1
      end if

      time2 = 0.d0
      call timel(time2)

      nrmat = kkrsz*natom

      nrhs  = 1
!CDEBUG      nrhs  = kkrsz

      allocate(vecs(1:nrmat,nrhs),                                      &
     &         iptrow(1:nrmat+1),                                       &
     &         stat=erralloc)
      if(erralloc.ne.0) then
      write(6,*) ' NRMAT=',nrmat
      write(6,*) ' NRHS+1=',nrhs+1
      write(6,*) ' ERRALLOC=',erralloc
      call fstop(sname//' :: ALLOCATION MEMORY PROBLEM')
      end if

      ne = 0
      i=0
      do iatom=1,natom
      indI = (iatom-1)*ndmat
      do lmi=1,kkrsz
!c         i = (iatom-1)*kkrsz+lmi
       i = i + 1
         indI = lmi-kkrsz
       do jnn=1,nnptr(0,iatom)
          indIJ = indI + (jnn-1)*(kkrsz*kkrsz)
        do lmj=1,kkrsz
         if(a(indIJ+lmj*kkrsz,iatom).ne.czero) then
          ne = ne+1
           end if
        end do
       end do
      end do
      end do

      if(iprint.ge.0) then
       write(6,*) ' NE=',ne,' NRMAT*NRMAT=',nrmat*nrmat
      end if

      allocate(value(1:ne),                                             &
     &         indcol(1:ne),                                            &
     &         stat=erralloc)
      if(erralloc.ne.0) then
      write(6,*) ' NE=',ne
      write(6,*) ' NRMAT=',nrmat
      write(6,*) ' NDMAT=',ndmat
      write(6,*) ' ALLOCATION (C16) =',ne+nrmat+(nrmat+1+ne)/4+1
      write(6,*) ' ERRALLOC=',erralloc
      call fstop(sname//' :: ALLOCATION MEMORY PROBLEM')
      end if

      kelem = 0
      iptrow(1) = 1

      i = 0
      do iatom=1,natom
       indI = (iatom-1)*ndmat
       do lmi=1,kkrsz
!c        i = (iatom-1)*kkrsz+lmi
      i = i + 1
      do inn=1,nnptr(0,iatom)
       jatom = nnclmn(1,inn,iatom)
       indJ = (lmi-kkrsz) + (inn-1)*(kkrsz*kkrsz)
       do lmj=1,kkrsz
        j = (jatom-1)*kkrsz+lmj
        lmij = lmj*kkrsz +indJ
!c                          lmij = (lmj-1)*kkrsz + lmi
!CDEBUG
        aij = a(lmij,iatom)
!CDEBUG          aij = a(indI+lmij)
        if(aij.ne.czero) then
         kelem = kelem+1
         value(kelem) = aij
         indcol(kelem) = j
        end if
       end do
      end do
      iptrow(i+1) = kelem+1
       end do
      end do

!c      call ge2crs(a,ndmat,nrmat,tolinv,value,iptrow,indcol,ne,2)

!CAB      do i =1,ne
!CAB       a(i) = value(i)
!CAB      end do

      if(nrhs.eq.1) then
!c       l1mx = kkrsz
!c       l1mn = 1
      else if(nrhs.eq.kkrsz) then
!c       l1mx = 1
!c       l1mn = 1
      else
       write(6,*) ' KKRSZ=',kkrsz
       write(6,*) ' NRHS=',nrhs
       call fstop(sname//' :: not implemented ')
      end if

!c     ==================================================================
!CDEBUG
!CDEBUG      lwork = 4*((nrmat*nrmat + 50*ne) + 2*(nrmat+1))
!CDEBUG

      lwork = 0

      if(lwork.gt.0) then
3      continue
       if(lwork.lt.8*(nrmat+1)+ne) then
      write(6,*) ' NE=',ne
      write(6,*) ' NRMAT=',nrmat
      write(6,*) ' LWORK=',lwork
      call fstop(sname//':: MEMORY PROBLEM')
       end if
       ALLOCATE(                                                        &
     &         work(0:lwork),                                           &
     &        stat=erralloc)
       if(erralloc.ne.0) then
      if(allocated(work)) deallocate(work)
      lwork = dble(lwork)*0.9d0
      go to 3
       endif
       lwork=lwork*4                     ! now LWORK in bytes
      end if
!c     ==================================================================

!CDEBUG
      if(iprint.ge.0.and.lwork.gt.0) then
       write(6,*) sname//':: LWORK=',lwork
      end if
!CDEBUG

      iflag = 1                         ! Factorize A
      call c_bridgez(nrmat,ne,nrhs,value,iptrow,indcol,                 &
     &               vecs(1,1),nrmat,info,work,lwork,iflag)

!CDEBUG
      time1 = time2
      call timel(time2)
      timePreit = time2-time1
      if(iprint.ge.0) then
       write(*,*) sname//' :: LU-DECOMP-time=',real(timePreit)
      end if
!CDEBUG


      if(info.ne.0) then
      write(6,*) ' IFLAG=',iflag
      write(6,*) ' NE=',ne
      write(6,*) ' NRHS=',nrhs
      write(6,*) ' NRMAT=',nrmat
      write(6,*) ' NDMAT=',ndmat
      write(6,*) ' INFO=',info
      call fstop(sname//' :: INFO.ne.0 after c_bridgez 2')
      end if

      ntime = 0
      iflag = 2

      do j=1,natom*kkrsz,nrhs

       nsub1 = (j-1)/kkrsz+1
       ns1 = itype(nsub1)

       l1a = mod(j-1,kkrsz)         ! l1a=0:kkrsz-1

       do nsub2=1,nsub1-1
      if(ns1.eq.itype(nsub2)) then            ! copy
       do l1 = 1,kkrsz
        do i=1,kkrsz
         cof(i,l1,nsub1) = cof(i,l1,nsub2)
        end do
       end do
       go to 10
      end if
       end do

       ntime = ntime+1

       call zerooutC(vecs(1,1),nrhs*nrmat)
       do i = 1,nrhs
!c        l1 = mod((i-1)+l1a,kkrsz) + 1           ! l1=1:kkrsz

      vecs(j+i-1,i) = cone

!c                                        VECS(*,i) = 1*Precond[A]
       end do

       call c_bridgez(nrmat,ne,nrhs,value,iptrow,indcol,                &
     &               vecs(1,1),nrmat,info,work,lwork,iflag)

       if(info.ne.0) then
      write(6,*) ' IFLAG=',iflag
      write(6,*) ' NE=',ne
      write(6,*) ' NRHS=',nrhs
      write(6,*) ' NRMAT=',nrmat
      write(6,*) ' NDMAT=',ndmat
      write(6,*) ' INFO=',info
      call fstop(sname//' :: INFO.ne.0 after c_bridgez 2')
       end if

       Jns = (nsub1-1)*kkrsz
       do i = 1,nrhs
      l1 = mod((i-1)+l1a,kkrsz) + 1           ! l1=1:kkrsz
      do ii=1,kkrsz
!CDEBUG         sum = czero
!CDEBUG         do jj=1,kkrsz
!CDEBUGc          Jn = (nsub1-1)*kkrsz+jj
!CDEBUG          Jn = Jns +jj
!CDEBUG          sum = sum + t(ii,jj,ns1)*vecs(Jn,i)
!CDEBUG         end do
!CDEBUG         cof(ii,l1,nsub1) = sum

!c          Jn = (nsub1-1)*kkrsz+ii
        Jn = Jns +ii
        cof(ii,l1,nsub1) = vecs(Jn,i)
!CDEBUG
      end do
       end do
10     continue
      end do

!CDEBUG
      time1 = time2
      call timel(time2)
      tsolve = (time2-time1)/ntime
      tsolve = tsolve*kkrsz/nrhs
      if(iprint.ge.0) then
       write(*,*) sname//' :: SPARSEINV-time=',real(tsolve)             &
     &            ,real(tsolve*natom),real(tsolve*natom+timePreit)
      end if
      timeIneq = timeIneq + tsolve*natom+timePreit
!CDEBUG

      iflag = 3
      call c_bridgez(nrmat,ne,nrhs,value,iptrow,indcol,                 &
     &               vecs(1,1),nrmat,info,work,lwork,iflag)

      if(allocated(work)) deallocate(work)
      deallocate(value,vecs,iptrow,indcol,stat=erralloc)

      if(erralloc.ne.0) then
      write(6,*) ' NE=',ne
      write(6,*) ' ERRALLOC=',erralloc
      call fstop(sname//' :: DEALLOCATION MEMORY PROBLEM')
      end if

      return
      end

