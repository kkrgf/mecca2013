!BOP
!!ROUTINE: scr_mfcc_const
!!INTERFACE:
      function scr_mfcc_const(rws_over_rnn)
!!DESCRIPTION:
! returns scr-CPA constant
!
!!USES:
      use mecca_constants
!
!!REMARKS:
! if scr_mfcc_const(1.0)==0 then scr_mfcc_const is null
!
!!REVISION HISTORY:
! Revised - A.S. - 2019
! Initial Version - A.S. - 2013
!EOP
!
!BOC
      implicit none
      real(8) :: scr_mfcc_const
!c   c_mf_cc = 1.0/2    MF (scr-CPA)   Johnson & Pinsk, PRB48, 11553 (1993)
!c   c_mf_cc = 1.3147/2 FCC analytic  Magri et al. PRB42, 11388 (1990)
!c   c_mf_cc = 1.3831/2 BCC analytic
!c   c_mf_cc = 2.0/2    SIM       Korzhavyi et al. PRB51,  5773 (1995)
      real(8), intent(in) :: rws_over_rnn   ! Rws/Rnn ratio (or something else)

      real(8), parameter :: cc_mf  = one/two
      real(8), parameter :: cc_fcc = 1.3147d0/2.d0
      real(8), parameter :: cc_bcc = 1.3831d0/2.d0
      real(8), parameter :: cc_sim = one
      real(8), parameter :: rws_over_rnn_mf = one
      real(8), parameter :: rws_over_rnn_fcc = one/sqrt(two/four) /     &
     &                            ((four*(four*pi)/three)**(one/three))   ! = 0.55266945441850884927
      real(8), parameter :: rws_over_rnn_bcc = one/sqrt(three/four) /   &
     &                              (three/(two*(four*pi))**(one/three))  ! = 0.56854280064436316668 
!
      select case (trim(cc_mod))
      case('MF')
       scr_mfcc_const = cc_mf
      case('FCC')
       scr_mfcc_const = cc_fcc
      case('BCC')
       scr_mfcc_const = cc_bcc
      case('SIM')
       scr_mfcc_const = cc_sim
      case('TRIAL')         ! do not use
        if ( rws_over_rnn < rws_over_rnn_fcc ) then
         scr_mfcc_const = (cc_mf*(rws_over_rnn_fcc-rws_over_rnn) +      &
     &                        cc_fcc*(rws_over_rnn-rws_over_rnn_mf)) /  &
     &                              (rws_over_rnn_fcc-rws_over_rnn_mf)
        else if ( rws_over_rnn <= rws_over_rnn_bcc ) then
         scr_mfcc_const = (cc_fcc*(rws_over_rnn_bcc-rws_over_rnn) +     &
     &                        cc_bcc*(rws_over_rnn-rws_over_rnn_fcc)) / &
     &                              (rws_over_rnn_bcc-rws_over_rnn_fcc)
        else
         scr_mfcc_const = cc_bcc*rws_over_rnn_bcc/rws_over_rnn +        &
     &                     cc_sim*(one - rws_over_rnn_bcc/rws_over_rnn)
        end if    
        scr_mfcc_const = min(one,max(zero,scr_mfcc_const))
      case('NUL')
       scr_mfcc_const = zero
      case default
       scr_mfcc_const = cc_mf
      end select

      return
!EOC
      end function scr_mfcc_const
