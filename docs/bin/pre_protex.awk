#!/usr/bin/awk -f

BEGIN{md=0;rt=0;boc=0;use=0}
{
L = tolower($1)

dscrp=0
if ( L == "subroutine" || L == "function" || L == "module" || L == "program" ) {
 bp=1 
 if ( L == "module" ) md=1;
 if ( L == "subroutine" || L == "function" || L == "program" ) rt=rt+1;
 if ( index($0,"(")>0 && index($0,")")>0 ) dscrp=1
 if ( index($0,"(")==0 && index($0,")")==0 ) dscrp=1
} else { 
 bp=0 
}

if ( bp == 1 ) {print "\!BOP" ; boc=0} ;

fcomment = index(L,"!")
if ( fcomment==1 ) {
 print
 next
}

if ( L == "end" ) {
 prnt = 0
 word=tolower($2)
 if ( word == "module" ) {
  if ( md==1 ) prnt=1
  md = 0
 }
 if ( rt>0 ) {
  if ( word == "subroutine" || 
       word == "function"   || 
       tolower($0) == "end" || 
       word == "program" ) { 
    rt=rt-1 ; prnt=1
  } else {
    if (index(word,"!")==1) {
     rt=rt-1 ; prnt=1
    } 
  }
 }
 if ( prnt>0 ) {
  if ( rt>0 || md>0 ) print "\!EOC"
 } 
 print
 next
}

if ( L == "contains" ) { 
 if ( boc==0 ) {
  boc=1; 
  if ( md==1 ) { md=2; print "\EOP"; print "\!"}
 } else {
  if ( rt>0 || md>0 ) {
   boc=0; print "\!EOC"
  }
 }
 print
 next
}

if ( boc == 0 ) {
 prnt = 0
 if ( $2 == "=" || L == "call" || L == "do" || L == "if" ) { prnt=1 } else { 
  if ( index(L,"=") > 1 ) { prnt=1 } else {
   if ( index(L,"if") > 1 ) {
    c=split(L,s,"("); if ( s[1]=="if" ) prnt=1 }
  }
 }
 if ( prnt == 1 ) { 
  boc=1 ; 
 print "\!EOP" ; ; print "\!" ; print "\!BOC" }   
}

if ( L == "subroutine" || L == "function" || L == "program" ) {
 c = split($2,s,"(")
 if ( md>0 || rt>1) { 
   print "\!\!IROUTINE: " s[1] 
 } else { 
   print "\!\!ROUTINE: " s[1] 
 }
} else { 
 if ( L == "module" ) { c = split($2,s,"(") ; print "\!\!MODULE: " s[1] } 
}

if ( tolower($2) == "function" && L != "end" ) {
 c = split($3,s,"(")
 if ( md>0 || rt>1 ) { 
   print "\!\!IROUTINE: " s[1] 
 } else { 
   print "\!\!ROUTINE: " s[1] 
 }
}

if ( bp==1 ) {
 if ( dscrp==0 ) {
  print "\!\!DESCRIPTION:" ; 
  print "\!"; 
 }
 print "\!\!INTERFACE:" ;
}

if ( use==0 && L == "use" ) use=1
if ( use==1 ) { use=2 ; print "\!\!USES:" }
if ( use>0 && L != "use" ) { use=0 ; print "\!" }

prnt = 0
if ( boc==0 ) {
 if ( L=="implicit" || index(L,"integer")>0 || index(L,"real")>0 || index(L,"type")>0 ||
      index(L,"logical")>0 || index(L,"character")>0 ) prnt=1
}
if ( prnt==1 ) {
 if ( md == 0 || (md>0 && rt>0) ) {
  print "\!\!ARGUMENTS:"
#  print "\!\!INPUT/OUTPUT ARGUMENTS:"
#  print "\!\!INPUT ARGUMENTS:"
#  print "\!\!OUTPUT ARGUMENTS:"
  print "\!\!PARAMETERS:"
 } else {
  print "\!\!PUBLIC TYPES:"
  print "\!\!PUBLIC MEMBER FUNCTIONS:"
  print "\!\!PRIVATE MEMBER FUNCTIONS:"
  print "\!\!PUBLIC DATA MEMBERS:"
 }
 print "\!\!REVISION HISTORY:"
 print "\! Adapted - A.S. - 2013"
 print "\!\!REMARKS:"
# print "\!AUTHOR:"
 if ( boc==0 ) {
  print "\!EOP" ; print "\!"
 }
 print "\!BOC"
 boc = 1
}

print ;

if ( dscrp>0 ) { 
 print "\!\!DESCRIPTION:" ; 
 print "\!";
} 

}
