!BOP
!!ROUTINE: hankel
!!INTERFACE:
       subroutine hankel(x,h1,ltop)
!!DESCRIPTION:
! calculates hankel functions times $(-i)^{(l+1)}$ \\
! ($h1 \to  exp(-x)$   h1 decays for real x); \\
! produces ltop+1 values of h1*(-i)**(l+1)        (l=0,ltop)
!

!!DO_NOT_PRINT
       implicit real*8 (a-h,o-z)
!!DO_NOT_PRINT

!!ARGUMENTS:
       complex*16 x
       integer    ltop
       complex*16 h1(0:ltop)
!!REVISION HISTORY:
! modified from old  routine - DDJ & WAS - April 1994
! Adapted - A.S. - 2013
!!REMARKS:
!EOP
!
!BOC
       integer    i
       integer    ll
!c
       complex*16 xinv
       complex*16 y1
!c parameter
       complex*16 sqrtm1
       complex*16 cone
!c
       parameter (sqrtm1=(0.d0,1.d0))
       parameter (cone=(1.d0,0.d0))
!c      ***********************************************************
!c      warning::::: will not work for small values of x
!c      ***********************************************************
       xinv=cone/x
       y1=exp(sqrtm1*x)*xinv
       h1(0)=-sqrtm1*y1
       if ( ltop>0 ) then
        h1(1)=-y1*(cone+sqrtm1*xinv)
!c      -----------------------------------------------------------------
        do ll=1,ltop-1
         h1(ll+1)=(ll+ll+1)*(xinv*h1(ll))-h1(ll-1)
        enddo
!c      -----------------------------------------------------------------
       end if
       do i=0,ltop
         h1(i)=h1(i)*(-sqrtm1)**(i+1)
       enddo
!c      -----------------------------------------------------------------
       return
!EOC
       end subroutine hankel
