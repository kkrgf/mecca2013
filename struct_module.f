!BOP
!!MODULE: struct
!!DESCRIPTION: to set-up structure-related data (types RS\_Str,KS\_Str,EW\_Str),
!              in particular, reading structure-description file,
!              initialization of rs\_box-,ks\_box-,ew\_box-structures of run-state, etc.

!
! !INTERFACE:
      module struct

!!
! !REVISION HISTORY:
! Initial Version - A.S. - 2013
!!

! !USES:
      use mecca_constants
      use mecca_types
      use mecca_interface
      use gfncts_interface, only : g_numNonESsites
      use struct_interface

! !PUBLIC DATA MEMBERS
      integer, save :: invadd = 0
!EOP
      integer, save :: iprot = 48
      character(10), parameter, private :: sname = 'struct'

      contains

!BOP
! !IROUTINE: setup_struct
! !DESCRIPTION: main module subroutine
!\\
! !INTERFACE:
      subroutine setup_struct( mecca_state )
      implicit none
! !INPUT/OUTPUT PARAMETERS:
      type(RunState), target :: mecca_state

!EOP
!BOC
      mecca_state%rs_box%alat = mecca_state%intfile%alat

      mecca_state%rs_box%nsubl = mecca_state%intfile%nsubl

      call str_readin(mecca_state%intfile,mecca_state%rs_box,invadd)

      call allocStrMem(mecca_state%intfile,                             &
     &                  mecca_state%rs_box,                             &
     &                  mecca_state%ks_box,                             &
     &                  mecca_state%ew_box)

      call setStrModules(mecca_state%intfile,                           &
     &                  mecca_state%rs_box,                             &
     &                  mecca_state%ks_box,                             &
     &                  mecca_state%ew_box)

      call ioStrModules(-1,mecca_state%intfile,                         &
     &                  mecca_state%rs_box,                             &
     &                  mecca_state%ks_box)

      return
      end subroutine setup_struct
!EOC

!      subroutine gSublatVlms(iflag,rs_box,omega,omegaint,omegamt,       &
!     &                                                          surfamt)
!      implicit none
!      integer, intent(in) :: iflag
!      type(RS_Str), intent(in) :: rs_box
!      real(8), intent(out) :: omega,omegaint
!      real(8), intent(out) :: omegamt(:)
!      real(8), intent(out), optional :: surfamt(:)
!
!      real(8) :: R(rs_box%nsubl)
!
!      omega=zero
!      omegaint=zero
!      omegamt = zero
!      omegaint = zero
!      if ( present(surfamt) ) surfamt=zero
!      if ( rs_box%volume == zero .or. rs_box%alat<=zero .or.            &
!     &                                          rs_box%nsubl<=0 ) return
!      omega = rs_box%volume*rs_box%alat**3
!      select case (iflag)
!      case(0)
!          R = rs_box%rmt(1:rs_box%nsubl)
!      case(1)
!          R = rs_box%rws(1:rs_box%nsubl)
!      case default
!          return
!      end select
!      if ( present(surfamt) ) surfamt=(4.d0*pi)*R**2
!      omegamt(1:rs_box%nsubl)=(4.d0*pi/3.d0)*R**3
!      omegaint = omega - dot_product(omegamt(1:rs_box%nsubl),           &
!     &                                  rs_box%numbsubl(1:rs_box%nsubl))
!      return
!      end subroutine gSublatVlms

      subroutine str_readin(intfile,rs_box,invadd)
      implicit none
      type ( IniFile ), target :: intfile
      type ( RS_Str ), target :: rs_box
      integer  :: invadd
!
      real*8  :: boa,coa
      real*8, pointer  :: rslatt(:,:) ! (3,3)
      integer, pointer  :: natom
      real*8,  pointer :: basis(:,:)
      integer,  pointer :: itype(:)
      integer  :: nsublat

      real*8 :: ba,ca,scale
      real(8) :: eps,tmp(1),Rrsp,cvlm
      real(8), parameter :: cnst=1.93d0  ! .gt.(six/pi)
      real(8), parameter :: third=dble(1)/dble(3)
      logical :: add_trs
      integer :: i,j,nsub,nsubi,nu
      character*80 :: coerr
      real(8), external :: cellvolm

!c
!c     ============begin of structure specification ====================
!c
      ba = zero ; ca = zero; scale = one
      rslatt   => rs_box%rslatt
      if ( intfile%icryst.ne.0 ) then
!        allocate(rs_box%xyzr(3,rs_box%natom),rs_box%itype(rs_box%natom))
        call getbas(intfile%icryst,rs_box%natom,intfile%ba,intfile%ca,  &
     &        rs_box%rslatt,rs_box%xyzr,rs_box%itype,rs_box%volume)
        nsublat = maxval(rs_box%itype)
        rs_box%nsubl = nsublat
      else
        nu = 1
        open(nu,file=intfile%io(nu_xyz)%name)
        call iostrf(nu,rs_box%natom,rs_box%itype,rs_box%rslatt,         &
     &                                   rs_box%xyzr,ba,ca,scale,invadd)
        close(nu)
        boa      = intfile%ba
        coa      = intfile%ca
        natom    => rs_box%natom
        basis    => rs_box%xyzr
        itype    => rs_box%itype
        nsublat  = rs_box%nsubl
        write(6,*) '     NATOM=',natom
        do i=1,natom
         if(itype(i).gt.natom.or.itype(i).lt.1) then
          write(6,*)
          write(6,*) '  inconsistent structure setup:'
          write(6,*) '  NSUBLAT=',nsublat,' TYPE(',i,')=',itype(i)
          write(6,*) (basis(j,i),j=1,3)
          write(6,*) '   Wrong atom TYPE '
          write(6,*)
          coerr=' wrong input for itype(i) in str-file'
          call p_fstop(sname//':'//coerr)
         end if
        end do
! time-reversal symmetry for k-space integration
        add_trs = .false.
        if ( intfile%ncpa==-1 .or. intfile%ncpa==0 ) then
         add_trs = .true.
        else
         if ( intfile%nspin==1 ) add_trs = .true.   ! cpa
        end if

        if ( invadd==1 ) then
         if ( .not.add_trs ) then
          write(6,'(/a/a,a/)')                                          &
     & ' WARNING: TIME-REVERSAL SYMMETRY IS REQUESTED in STRUCT.FILE',  &
     & ' WARNING: ENFORCING SUCH A SYMMETRY IS NOT RECOMMENDED WITH CPA'
         end if
        else if (invadd.ne.0 ) then      ! to enforce non-default behavior'
         write(6,'(/a,i2,a/)')                                          &
     & ' WARNING: INVADD=',invadd,' UNKNOWN OPTION'
        end if
!
        do i=1,natom
         basis(1,i) = basis(1,i)/scale
         basis(2,i) = basis(2,i)/scale*(boa/ba)
         basis(3,i) = basis(3,i)/scale*(coa/ca)
        end do
!c                    basis(1,i)*ALAT -- X in at.un.
!c                    basis(2,i)*ALAT -- Y in at.un.
!c                    basis(3,i)*ALAT -- Z in at.un.
!c
        do i=1,3
         rslatt(1,i) = rslatt(1,i)/scale
         rslatt(2,i) = rslatt(2,i)/scale*(boa/ba)
         rslatt(3,i) = rslatt(3,i)/scale*(coa/ca)
        end do
!c                    rslatt(*,i)*ALAT --  in at.un.

!c     ============end of structure specification ======================
      end if
      if ( allocated(rs_box%numbsubl) ) deallocate(rs_box%numbsubl)
      allocate( rs_box%numbsubl(1:nsublat) )
      do nsub=1,rs_box%nsubl
       nsubi = 0
       do i=1,rs_box%natom
        if(nsub.eq.rs_box%itype(i)) nsubi = nsubi+1
       end do
       if(nsubi.eq.0) then
        write(6,*)
        do i=1,rs_box%natom
         write(6,*) i,rs_box%itype(i)
        end do
        write(6,*) ' There are no atoms for sublattice',nsub
        write(6,*)
        write(*,*) ' INCONSISTENT DATA in INPUT FILES'
        call fstop(sname//': check ITYPE in structure-file')
       end if
       rs_box%numbsubl(nsub) = nsubi
       intfile%sublat(nsub)%ns = nsubi
       write(6,'(''  In sublattice '',i5,'' there are'',t40,1x,i5,      &
     &            '' site(s)'')') nsub,rs_box%numbsubl(nsub)
      end do
      write(6,1002)
1002  format(/,1x,76('=')/)
      cvlm = cellvolm(rslatt,tmp,0)
      Rrsp = abs(cvlm*cnst)**(one/three)
      Rrsp = max(three,Rrsp)
      eps = 0.5d-3   ! depends on how many digits are printed below
      if ( intfile%Rrsp <= zero ) then
       if ( Rrsp>three) Rrsp = Rrsp+eps
       intfile%Rrsp = Rrsp
      else if ( intfile%Rrsp<Rrsp ) then
       if ( Rrsp>three) Rrsp = Rrsp+eps
       write(6,'(/2a,f7.3,a,f7.3/)') ' WARNING: accuracy may be low;',  &
     & ' input value Rrsp (rs-cutoff) ',intfile%Rrsp,                   &
     & ' is smaller that  min recommended value ',Rrsp
      end if
      if ( intfile%Rksp <= zero ) intfile%Rksp = four

      return
      end subroutine str_readin

      subroutine setIOflag(control,nu,stats,iflag)
      implicit none
      integer, intent(in) :: control  !  -1: write if possible; 1: read if possible
      integer, intent(in) :: nu
      character(1), intent(inout) :: stats
      integer, intent(out) :: iflag

      iflag = control
      select case (stats)
       case ('n')
        if ( control >=0 ) iflag = 0
       case ('r')
        if ( control >=0 ) iflag = 0
       case ('o')
        if ( control <=0 ) iflag = 0
       case default
        iflag = 0
      end select
!      if ( stats.ne.'o' .and. iflag==-1 ) then
!        stats = 'u'
!      end if

!      if ( control==0 .and. stats .ne. 'o' ) then
!          iflag = -1
!          stats = 'u'
!      else
!       if ( nu == nu_xyz ) then
!           if ( stats == 'n' ) then
!!c  to write the structure data
!               iflag = -1
!           else
!               iflag = 0
!           end if
!       else
!           if ( stats == 'o' ) then
!!c  to read constants
!               iflag = 1
!           else if ( stats == 'n' .or. stats == 'r' ) then
!!c  to write constants
!               iflag = -1
!           else
!!c  to calculate only
!               iflag = 0
!           end if
!       end if
!      end if

      end subroutine setIOflag

!c*******************************************************************

      subroutine getbas(icryst,nbasis,boa,coa,                          &
     &             bas,bases,itype,volume)
!c
!c   if icryst.eq.0, then rmt is input parameter (ALAT)
!c      icryst.eq.[1-11], then rmt is calculated (in ALAT)
!c   Thus, output rmt are always in ALAT
!c
      implicit real*8 (a-h,o-z)
      integer :: icryst, nbasis
      real(8) :: boa, coa
      real(8) :: bas(3,3)
      real(8), allocatable :: bases(:,:)
      integer, allocatable :: itype(:)
      real(8) :: volume,tmpv(1)
      real(8) :: rmt(8)
      character(*), parameter :: sname='getbas'
      real(8) :: abc(3)

      real(8) :: basfcc(3,3),basbcc(3,3),basscc(3,3)
!c==================================================================
      data  basfcc/0.5d0, 0.5d0, 0.0d0,                                 &
     &             0.0d0, 0.5d0, 0.5d0,                                 &
     &             0.5d0, 0.0d0, 0.5d0/

      data  basbcc/0.5d0, 0.5d0,-0.5d0,                                 &
     &            -0.5d0, 0.5d0, 0.5d0,                                 &
     &             0.5d0,-0.5d0 ,0.5d0/

      data  basscc/  1.0,  0.0,  0.0,                                   &
     &               0.0,  1.0,  0.0,                                   &
     &               0.0,  0.0,  1.0/
!c==================================================================

      real(8) :: basrhm(3,3),basbct(3,3)
!c==================================================================
      data basrhm/                                                      &
     & 0.000000000000000000E+00,-0.327630600784613202,                  &
     & 0.944805900399396159,                                            &
     & 0.283736423336632848,0.163815300392306601,                       &
     & 0.944805900399396159,                                            &
     &-0.283736423336632848,0.163815300392306601,                       &
     & 0.944805900399396159/

      data  basbct/ -0.5,  0.5,  1.0,                                   &
     &               0.5, -0.5,  1.0,                                   &
     &               0.5,  0.5, -1.0/

      if(icryst.ne.0) then
       abc(1) = 1.d0
       abc(2) = boa
       abc(3) = coa
      else
       abc(1) = 1.d0
       abc(2) = 1.d0
       abc(3) = 1.d0
      end if
      alat = 1.d0
      coef = 1.d0
      lattyp = 0
!      pi = 4*atan(1.d0)

      if ( allocated(bases) ) deallocate(bases)
      if ( allocated(itype) ) deallocate(itype)
!c    =================================================================== !===
!c     FCC lattice setup
!c    =================================================================== !===
      if(icryst.eq.1) then
       nbasis=1
       lattyp=1
       allocate(bases(3,nbasis),itype(nbasis))

       bases(1,1)=0.d0
       bases(2,1)=0.d0
       bases(3,1)=0.d0
       itype(1) = 1

       rmtd=sqrt(2.d0)/4.d0
         rmt(1)=rmtd*alat
       nrot = 48

!c    =================================================================== !===
!c     BCC lattice setup
!c    =================================================================== !===
      else if(icryst.eq.2) then
         nbasis=1
         lattyp=2
       allocate(bases(3,nbasis),itype(nbasis))

       bases(1,1)=0.d0
       bases(2,1)=0.d0
       bases(3,1)=0.d0
       itype(1) = 1

       rmtd=sqrt(3.d0)/4.d0
         rmt(1)=rmtd*alat

       nrot = 48

!c    =================================================================== !==
!c     SC lattice setup
!c    =================================================================== !===
      else if(icryst.eq.3) then
         nbasis=1
         lattyp=3
       allocate(bases(3,nbasis),itype(nbasis))

       bases(1,1)=0.d0
       bases(2,1)=0.d0
       bases(3,1)=0.d0
       itype(1) = 1

       rmtd=0.5d0
         rmt(1)=rmtd*alat

         nrot = 48

!c    =================================================================== !===
!c     B2 (CsCl) lattice setup
!c    =================================================================== !===
      else if(icryst.eq.4) then
         nbasis=2
         lattyp=3
       allocate(bases(3,nbasis),itype(nbasis))

       bases(1,1)=0.d0
       bases(2,1)=0.d0
       bases(3,1)=0.d0
       bases(1,2)=0.5d0
       bases(2,2)=0.5d0
       bases(3,2)=0.5d0
       itype(1) = 1
       itype(2) = 2

       rmtd=sqrt(3.d0)/4.d0
       rmt(1)=rmtd*alat
         rmt(2)=rmt(1)

       nrot = 48

!c    =================================================================== !===
!c     L12 (Cu3Au) lattice setup
!c    =================================================================== !===
      else if(icryst.eq.5) then
         nbasis=4
         lattyp=3
       allocate(bases(3,nbasis),itype(nbasis))
       alat = 1.d0

       bases(1,1)=0.d0
       bases(2,1)=0.d0
       bases(3,1)=0.d0

       bases(1,2)=0.5d0
       bases(2,2)=0.5d0
       bases(3,2)=0.d0

       bases(1,3)=0.5d0
       bases(2,3)=0.d0
       bases(3,3)=0.5d0

       bases(1,4)=0.d0
       bases(2,4)=0.5d0
       bases(3,4)=0.5d0

       itype(1) = 1
       itype(2) = 2
       itype(3) = 2
       itype(4) = 2

       rmtd=sqrt(2.d0)/4.d0
         rmt(1)=rmtd*alat
         rmt(2)=rmt(1)
         rmt(3)=rmt(1)
         rmt(4)=rmt(1)

         nrot = 48

!c    =================================================================== !===
!c     Perovskite (BaTiO3) lattice setup
!c    =================================================================== !===
      else if(icryst.eq.6) then
         nbasis=5
         lattyp=3
       allocate(bases(3,nbasis),itype(nbasis))

       bases(1,1)=0.d0
       bases(2,1)=0.d0
       bases(3,1)=0.d0

       bases(1,2)=0.5d0
       bases(2,2)=0.5d0
       bases(3,2)=0.d0

       bases(1,3)=0.5d0
       bases(2,3)=0.d0
       bases(3,3)=0.5d0

       bases(1,4)=0.d0
       bases(2,4)=0.5d0
       bases(3,4)=0.5d0

       bases(1,5)=0.5d0
       bases(2,5)=0.5d0
       bases(3,5)=0.5d0

       itype(1) = 1
       itype(2) = 2
       itype(3) = 2
       itype(4) = 2
       itype(5) = 3

       rmtd1=(sqrt(3.d0)-1.d0+sqrt(2.d0))/4.d0
       rmtd2=sqrt(2.d0)/2.d0-rmtd1
       rmtd3=0.5d0-rmtd2

       rmt(1)=rmtd1*alat
         rmt(2)=rmtd2*alat
         rmt(3)=rmt(2)
         rmt(4)=rmt(2)
         rmt(5)=rmtd3*alat

         nrot = 48

!c    =================================================================== !===
!c     B1 (NaCl) lattice setup
!c    =================================================================== !===
      else if(icryst.eq.7) then
         nbasis=2
         lattyp=1
       allocate(bases(3,nbasis),itype(nbasis))
       alat = 1.d0

       bases(1,1)=0.d0
       bases(2,1)=0.d0
       bases(3,1)=0.d0
       bases(1,2)=0.5d0
       bases(2,2)=0.5d0
       bases(3,2)=0.5d0

       itype(1) = 1
       itype(2) = 2

       rmtd=0.5d0
         rmt(1)=rmtd*alat
         rmt(2)=rmt(1)

         nrot = 48

!c    =================================================================== !===
!c     DO3 (Fe3Al) lattice setup
!c    =================================================================== !===
      else if(icryst.eq.8) then
         nbasis=4
         lattyp=1
       allocate(bases(3,nbasis),itype(nbasis))
       coef = 2.d0

       bases(1,1)=0.d0
       bases(2,1)=0.d0
       bases(3,1)=0.d0
       bases(1,2)=0.5d0
       bases(2,2)=0.d0
       bases(3,2)=0.d0
       bases(1,3)=0.25d0
       bases(2,3)=0.25d0
       bases(3,3)=0.25d0
       bases(1,4)=-0.25d0
       bases(2,4)=-0.25d0
       bases(3,4)=-0.25d0
       itype(1) = 1
       itype(2) = 2
       itype(3) = 2
       itype(4) = 2

       rmtd = 0.25d0
         rmt(1)=rmtd*alat
         rmt(2)=rmt(1)
         rmt(3)=rmt(1)
         rmt(4)=rmt(1)

       nrot = 48

!c    =================================================================== !===
!c
!c     R3M Trigonal lattice setup
!c
!c    =================================================================== !===
      else if( icryst .eq. 9 ) then
        nbasis=4
        lattyp=4
       allocate(bases(3,nbasis),itype(nbasis))

        u = 0.26d0
        alpha =( ( 32.d0 + 58.d0/60.d0 )/180.d0)*cos( -1.0d0 )
        cosalpha = cos( alpha )
        sqrt2 = sqrt( 2.0d0 )
        sqrt3 = sqrt( 3.0d0 )
        a = ( sqrt2/sqrt3 )*sqrt( 1.d0 - cosalpha )
        c = sqrt( 1.d0 + 2.d0*cosalpha )/sqrt3

!c Lattice vectors of unit cell

      bas(1,1)= 0.d0
      bas(2,1)= -a
      bas(3,1)=  c
      bas(1,2)=  a*sqrt3/2.0d0
      bas(2,2)=  a/2.0d0
      bas(3,2)=  c
      bas(1,3)=-bas(1,2)
      bas(2,3)= bas(2,2)
      bas(3,3)= c
!c    =================================================================== !===
!c     Scaled basis vectors
!c    =================================================================== !===
      bases(1,4) = 0.d0
      bases(2,4) = 0.d0
      bases(3,4) = 0.d0
      bases(1,5) = 0.5d0
      bases(2,5) = 0.5d0
      bases(3,5) = 0.5d0
        bases(1,6) = u
        bases(2,6) = u
        bases(3,6) = u
      bases(1,7) = ( 1.d0 - u )
      bases(2,7) = ( 1.d0 - u )
      bases(3,7) = ( 1.d0 - u )
!c    =================================================================== !===
!c     Construct cartesian real space basis vectors
!c    =================================================================== !===
      rmt(5) = 0.d0
      rmt(6) = 0.d0
      rmt(7) = 0.d0
        do j = 1,3
          do i = 1,3
          rmt(j+4) = rmt(j+4) + bas(j,i)*bases(i,4)
          enddo
        enddo
        bases(1,4) = rmt(5)
        bases(2,4) = rmt(6)
        bases(3,4) = rmt(7)
!c    =================================================================== !===
      rmt(5) = 0.d0
      rmt(6) = 0.d0
      rmt(7) = 0.d0
        do j = 1,3
          do i = 1,3
          rmt(j+4) = rmt(j+4) + bas(j,i)*bases(i,5)
          enddo
        enddo
        bases(1,5) = rmt(5)
        bases(2,5) = rmt(6)
        bases(3,5) = rmt(7)
!c    =================================================================== !===
      rmt(5) = 0.d0
      rmt(6) = 0.d0
      rmt(7) = 0.d0
        do j = 1,3
          do i = 1,3
          rmt(j+4) = rmt(j+4) + bas(j,i)*bases(i,6)
          enddo
        enddo
        bases(1,6) = rmt(5)
        bases(2,6) = rmt(6)
        bases(3,6) = rmt(7)
!c    =================================================================== !===
      rmt(5) = 0.d0
      rmt(6) = 0.d0
      rmt(7) = 0.d0
        do j = 1,3
          do i = 1,3
          rmt(j+4) = rmt(j+4) + bas(j,i)*bases(i,7)
          enddo
        enddo
        bases(1,7) = rmt(5)
        bases(2,7) = rmt(6)
        bases(3,7) = rmt(7)

      omega = cellvolm(bas,tmpv,0)

      omega = omega*alat**3

      bases(1,1)= bases(1,4)
        bases(2,1)= bases(2,4)
        bases(3,1)= bases(3,4)
        bases(1,2)= bases(1,6)
        bases(2,2)= bases(2,6)
        bases(3,2)= bases(3,6)
        bases(1,3)= bases(1,5)
        bases(2,3)= bases(2,5)
        bases(3,3)= bases(3,5)
        bases(1,4)= bases(1,7)
        bases(2,4)= bases(2,7)
        bases(3,4)= bases(3,7)

      itype(1) = 1
      itype(2) = 2
      itype(3) = 3
      itype(4) = itype(2)

!c    =================================================================== !===
!c     FOR NOW set Muffin-tin Radius equal to the Wigner-Seitz Radius
!c    =================================================================== !===
      rmt(1) = ( (3.d0*omega/(4.d0*pi))/4.d0 )**(1.d0/3.d0)
        rmt(2) = rmt(1)
        rmt(3) = rmt(1)
        rmt(4) = rmt(1)
      call fstop(sname//': check MT-radii')

      nrot = 12

!c    =================================================================== !===
!c     DO22 (TiAl3) lattice setup
!c    =================================================================== !===
      else if( icryst .eq. 10 ) then
         nbasis=8
         lattyp=3
       allocate(bases(3,nbasis),itype(nbasis))

       bases(1,1) = 0.d0
       bases(2,1) = 0.d0
       bases(3,1) = 0.d0

       bases(1,2) = 0.5d0
       bases(2,2) = 0.5d0
       bases(3,2) = 0.d0

       bases(1,3) = 0.5d0
       bases(2,3) = 0.d0
       bases(3,3) = 0.5d0

       bases(1,4) = 0.d0
       bases(2,4) = 0.5d0
       bases(3,4) = 0.5d0

       do i=1,4
        bases(1,i+4) = bases(1,i)
        bases(2,i+4) = bases(2,i)
        bases(3,i+4) = bases(3,i) + 1.d0
       end do

       do i=1,nbasis
        itype(i) = i
       end do
       itype(1) = 1
       itype(2) = 2
       itype(3) = 3
       itype(4) = itype(3)
       itype(5) = 4
       itype(6) = 5
       itype(7) = itype(3)
       itype(8) = itype(3)

       rmtd=(sqrt(2.d0)/4.d0)
         rmt(1)=rmtd*alat
         rmt(2)=rmt(1)
         rmt(3)=rmt(1)
         rmt(4)=rmt(1)
         rmt(5)=rmt(1)
         rmt(6)=rmt(1)
         rmt(7)=rmt(1)
         rmt(8)=rmt(1)

         nrot = 16

!c     =================================================================
!c     Si (Si4) lattice setup
!c    =================================================================== !===
!      else if(icryst.eq.11) then
!         nbasis=4
!         lattyp=1
!       allocate(bases(3,nbasis),itype(nbasis))
!
!       bases(1,1)=0.d0
!       bases(2,1)=0.d0
!       bases(3,1)=0.d0
!       bases(1,2)=0.125d0
!       bases(2,2)=0.125d0
!       bases(3,2)=0.125d0
!       bases(1,3)=0.25d0
!       bases(2,3)=0.25d0
!       bases(3,3)=0.25d0
!       bases(1,4)=0.375
!       bases(2,4)=0.375
!       bases(3,4)=0.375
!       itype(1) = 1
!       itype(2) = 1
!       itype(3) = 1
!       itype(4) = 1
!
!       rmt(1)=alat*(sqrt(3.d0)/8.d0)
!         rmt(2)=rmt(1)
!         rmt(3)=rmt(1)
!         rmt(4)=rmt(1)
!
!       nrot = 48
!
!c    =================================================================== !===
      else if(icryst.ne.0) then
       write(*,*) '  STRUCTURE::  ICRYST = ',icryst,' ???? '
       stop ' STRUCTURE: wrong ICRYST'
      endif
!c
!c     ===============================================================
!c     set up basis ...............fcc..............
!c     ===============================================================
      if(lattyp .eq. 1) then
         do ns=1,3
            do i=1,3
             bas(i,ns)=basfcc(i,ns)*coef
            enddo
         enddo

!c     ===============================================================
!c     set up basis ...............bcc..............
!c     ===============================================================
      else if(lattyp .eq. 2) then
         do ns=1,3
            do i=1,3
             bas(i,ns)=basbcc(i,ns)*coef
            enddo
         enddo
!c     ===============================================================
!c     set up basis ...............sc...............
!c     ===============================================================
      else if(lattyp .eq. 3) then
         do ns=1,3
            do i=1,3
             bas(i,ns)=basscc(i,ns)*coef
            enddo
         enddo
!c     ===============================================================
      else if( lattyp .eq. 4 ) then

      do ns=1,3
          do i=1,3
          if(abs(bas(i,ns)-basrhm(i,ns)).gt.1.d-6) then
           write(*,*) '  BASRHM::'
           write(*,*) ((basrhm(ii,nsi),ii=1,3),nsi=1,3)
           write(*,*) '  BAS::'
           write(*,*) ((bas(ii,nsi),ii=1,3),nsi=1,3)
           write(*,*)
           write(*,*) ' GIVEBAS:: Check trigonal translation vectors'
           write(*,*) '        and coordinates of atoms in unit cell'
           write(*,*) ' If all is OK -- remove STOP'
           stop ' GIVEBAS::  Check trigonal translation vectors '
          end if
        enddo
        enddo
      do ns=1,3
          do i=1,3
          bas(i,ns)=basrhm(i,ns)*coef
          enddo
        enddo

!c     ===============================================================
      else if( lattyp .eq. 5 ) then
        do ns=1,3
          do i=1,3
          bas(i,ns)=basbct(i,ns)*coef
          enddo
        enddo
      else if( lattyp .ne. 0 ) then
      write(*,*) '  GIVEBAS::  LATTYP=',lattyp,' is not implemented'
      stop ' GIVEBAS:: wrong LATTYP'
      endif
!c     ===============================================================

      do ns=1,3
       do i=1,3
      bas(i,ns) = bas(i,ns)*abc(i)
       end do
      end do

      write(6,'(''  Bravais lattice'',                                  &
     &              t40,''='',i5)') lattyp

      volume = cellvolm(bas,tmpv,0)

      if(volume.lt.0.d0) then
       write(6,*) ' VOLUME=',volume
       call fstop(sname//':: You use left-hand basis ???')
      end if

      volume = abs(volume)

      delta = 0.d0
      do i=1,nbasis
       delta = delta+rmt(i)**3
      end do

      eta = (4.d0*pi/3.d0)*delta/volume

      if(icryst.eq.0) then
       write(6,'(''  MT_Volume/ASA_Volume ='',f7.4)') eta
!CDEBUG
       if(eta.lt.0.68d0) then
      write(6,*)
      write(6,*) '  WARNING: INTERSTITIAL VOLUME IS LARGE'
      write(6,'(''          RWS = '',f6.3)')                            &
     &      (volume/nbasis*3.d0/(4.d0*pi))**(1.d0/3.d0)
      write(6,*) '      MT-radii:'
      write(6,'(2x,10(1x,f6.3))') rmt(1:min(nbasis,100))
      write(6,*)
       end if
!CDEBUG
      end if

      return
      end subroutine getbas

      subroutine deallocStrMem(rs_box,ks_box,ew_box)
      implicit none
      optional rs_box,ks_box,ew_box
      type ( RS_Str ) :: rs_box
      type ( KS_Str ) :: ks_box
      type ( EW_Str ) :: ew_box
!
      if ( present(rs_box) ) then
       if ( allocated( rs_box%xyzr ) ) deallocate( rs_box%xyzr )
       if ( allocated( rs_box%itype ) ) deallocate( rs_box%itype )
       if ( allocated( rs_box%rmt ) ) deallocate( rs_box%rmt )
       if ( allocated( rs_box%rws ) ) deallocate( rs_box%rws )
       if ( allocated( rs_box%rcs ) ) deallocate( rs_box%rcs )
       if ( allocated( rs_box%Rnn ) ) deallocate( rs_box%Rnn )
       if ( allocated( rs_box%if0 ) ) deallocate( rs_box%if0 )
       if ( allocated( rs_box%mapstr ) ) deallocate( rs_box%mapstr )
       if ( allocated( rs_box%mappnt ) ) deallocate( rs_box%mappnt )
       if ( allocated( rs_box%mapij ) ) deallocate( rs_box%mapij )
       if ( allocated( rs_box%aij ) ) deallocate( rs_box%aij )
      end if
!
      if ( present(ks_box) ) then
       if ( allocated(ks_box%dop) ) deallocate( ks_box%dop )
       if ( allocated(ks_box%nqpt) ) deallocate( ks_box%nqpt )
       if ( allocated(ks_box%qmesh) ) deallocate( ks_box%qmesh )
       if ( allocated(ks_box%wghtq) ) deallocate( ks_box%wghtq )
       if ( allocated(ks_box%twght) ) deallocate( ks_box%twght )
!
       if ( allocated(ks_box%lrot) ) deallocate( ks_box%lrot )
       if ( allocated(ks_box%ngrp) ) deallocate( ks_box%ngrp )
       if ( allocated(ks_box%kptgrp) ) deallocate( ks_box%kptgrp )
       if ( allocated(ks_box%kptset) ) deallocate( ks_box%kptset )
       if ( allocated(ks_box%kptindx) ) deallocate( ks_box%kptindx )
      end if
!
      if ( present(ew_box) ) then
       if ( allocated(ew_box%nksn) ) deallocate( ew_box%nksn )
       if ( allocated(ew_box%xknlat) ) deallocate( ew_box%xknlat )
!       if ( allocated(ew_box%xrnlat) ) deallocate( ew_box%xrnlat )
       if ( allocated(ew_box%xrslatij) ) deallocate( ew_box%xrslatij )
       if ( allocated(ew_box%np2r) ) deallocate( ew_box%np2r )
       if ( allocated(ew_box%numbrs) ) deallocate( ew_box%numbrs )
      end if
!
      return
      end subroutine deallocStrMem

      subroutine allocStrMem(inFile,rs_box,ks_box,ew_box)
      implicit none
      type ( IniFile ) :: inFile
      type ( RS_Str ) :: rs_box
      type ( KS_Str ) :: ks_box
      type ( EW_Str ) :: ew_box

      integer :: imethod,lmax,kkrsz,nbasis,nsubl
      integer :: nmesh,ndrsn,ndksn,ndimq,nminus,nshift
      integer :: i,imesh
      real*8 :: Rksp0,Rksp,R2ksp,etamax,enmax

      nbasis = rs_box%natom
      nsubl = rs_box%nsubl
      if ( nbasis<=0 .or. nsubl <=0 ) then
        write(*,*) ' ERROR:: incorrect structure setup'
        write(*,*) ' nbasis=',nbasis,' nsubl=',nsubl
        call fstop('struct_module')
      end if
      if ( .not. allocated (rs_box%xyzr) )                              &
     &    allocate( rs_box%xyzr(1:3,1:nbasis) )
      if ( .not. allocated (rs_box%itype) )                             &
     &    allocate( rs_box%itype(1:nbasis) )
      if ( allocated (rs_box%rmt) ) deallocate(rs_box%rmt)
      allocate( rs_box%rmt(1:nsubl) )
      rs_box%rmt = 0
      if ( allocated (rs_box%rws) ) deallocate(rs_box%rws)
      allocate( rs_box%rws(1:nsubl) )
      rs_box%rws = 0
      if ( allocated (rs_box%rcs) ) deallocate(rs_box%rcs)
      allocate( rs_box%rcs(1:nsubl) )
      rs_box%rcs = 0
      if ( allocated (rs_box%Rnn) ) deallocate(rs_box%Rnn)
      allocate( rs_box%Rnn(1:nsubl) )
      rs_box%Rnn = 0
      if ( allocated (rs_box%if0) ) deallocate(rs_box%if0)
      allocate( rs_box%if0(1:iprot,1:nbasis) )
      rs_box%if0 = 0
      if ( allocated (rs_box%mapstr) ) deallocate(rs_box%mapstr)
      allocate( rs_box%mapstr(1:nbasis,1:nbasis) )
      rs_box%mapstr = 0
      if ( allocated (rs_box%mappnt) ) deallocate(rs_box%mappnt)
      allocate( rs_box%mappnt(1:nbasis,1:nbasis) )
      rs_box%mappnt = 0
      if ( allocated (rs_box%mapsprs) ) deallocate(rs_box%mapsprs)
      allocate( rs_box%mapsprs(1:nbasis,1:nbasis) )
      rs_box%mapsprs = 0
      if ( allocated (rs_box%mapij) ) deallocate(rs_box%mapij)
      allocate( rs_box%mapij(1:2,1:nbasis*nbasis) )
      rs_box%mapij = 0
      if ( allocated (rs_box%aij) ) deallocate(rs_box%aij)
      allocate( rs_box%aij(1:3,1:nbasis*nbasis) )
      rs_box%aij = 0

      lmax = inFile%lmax
      kkrsz = (1+lmax)*(1+lmax)
      imethod = inFile%imethod
      nmesh = inFile%nmesh
      ks_box%lmax = lmax

      if(imethod.ne.3) then   !   K-space (standard or screened)
       Rksp0 = inFile%Rksp
       R2ksp = Rksp0**2
!       etamax = inFile%enmax/inFile%eneta
!       etamax = enmax*(inFile%alat/(two*pi))**2 / inFile%eneta
!       Rksp = sqrt(etamax*R2ksp+enmax)
!???       enmax = (max(inFile%etop,inFile%enmax) + four*inFile%Tempr) *    &
!???     &                                         (inFile%alat/(two*pi))**2
       enmax = 1.5d0       ! in D.U.
       if ( inFile%Tempr > zero ) then   ! it is assumed that for high temperatures real-space summation will be used
         enmax = enmax + four*min(eimsw,                                &
     &                         inFile%Tempr * (inFile%alat/(two*pi))**2)
       end if
       Rksp = sqrt(enmax*(one+R2ksp/inFile%eneta))
!???       Rksp = (one + one/ten)*Rksp  ! 10% is to compensate possible efermi variations in SCF cycle
       ndrsn = iprsn
       ndksn = ipxkns
!?       ndrhp = iprhp
      else                    ! R-space only
       etamax = zero
       R2ksp = zero
       Rksp0 = zero
       Rksp = zero
       ndrsn = iprsn
       ndksn = 1
!?       ndrhp = 1
      end if
!?      ndimlhp = (2*lmax+1)*(lmax+1)
      ndimq = 1
!DEBUG      if(imethod.ne.3) then
       do imesh=1,nmesh
        nminus = 0
        do i=1,3
         if(inFile%nKxyz(i,imesh).ne.abs(inFile%nKxyz(i,imesh)))        &
     &     nminus = nminus+1
        end do
        if(nminus.eq.0) then
         nshift = 1
        else if (nminus.eq.1) then
         nshift = 1
        else if (nminus.eq.2) then
         nshift = 1
        else if (nminus.eq.3) then
         nshift = 2
        end if
        ndimq = max(ndimq,nshift*abs(product(inFile%nKxyz(1:3,imesh))))
       end do
!DEBUG      end if
      nmesh = max(1,nmesh)

      ks_box%ndimq = ndimq
      if ( allocated(ks_box%dop) ) deallocate(ks_box%dop)
      allocate( ks_box%dop(1:kkrsz*kkrsz,1:iprot) )
      ks_box%dop=0
      if ( allocated(ks_box%nqpt) ) deallocate(ks_box%nqpt)
      allocate( ks_box%nqpt(1:nmesh) )
      ks_box%nqpt=0
      if ( allocated(ks_box%qmesh) ) deallocate(ks_box%qmesh)
      allocate( ks_box%qmesh(1:3,1:ks_box%ndimq,1:nmesh) )
      ks_box%qmesh=0
      if ( allocated(ks_box%wghtq) ) deallocate(ks_box%wghtq)
      allocate( ks_box%wghtq(1:ks_box%ndimq,1:nmesh) )
      ks_box%wghtq=0
      if ( allocated(ks_box%twght) ) deallocate(ks_box%twght)
      allocate( ks_box%twght(nmesh) )
      ks_box%twght=0
!
      if ( allocated(ks_box%lrot) ) deallocate(ks_box%lrot)
      allocate( ks_box%lrot(1:ks_box%ndimq,1:nmesh) )
      ks_box%lrot=0
      if ( allocated(ks_box%ngrp) ) deallocate(ks_box%ngrp)
      allocate( ks_box%ngrp(1:nmesh) )
      ks_box%ngrp=0
      if ( allocated(ks_box%kptgrp) ) deallocate(ks_box%kptgrp)
      allocate( ks_box%kptgrp(1:iprot+1,1:nmesh) )
      ks_box%kptgrp=0
      if ( allocated(ks_box%kptset) ) deallocate(ks_box%kptset)
      allocate( ks_box%kptset(1:iprot,1:nmesh) )
      ks_box%kptset=0
      if ( allocated(ks_box%kptindx) ) deallocate(ks_box%kptindx)
      allocate( ks_box%kptindx(1:ks_box%ndimq,1:nmesh) )
      ks_box%kptindx=0

      ew_box%Rksp = Rksp
      ew_box%Rrsp = inFile%Rrsp
      ew_box%ndimk = ndksn
      if ( allocated(ew_box%nksn) ) deallocate(ew_box%nksn)
      allocate( ew_box%nksn(1:nmesh) )
      ew_box%nksn=0
!      allocate( ew_box%xknlat(1:ew_box%ndimk*3*nmesh) )
!      ew_box%xknlat=0

      ew_box%ndimr = ndrsn
      ew_box%ndimrij = iprij
!     &   ew_box%Rrsp, ew_box%xrnlat, ew_box%ndimr, ew_box%nrsn,         &
!     &   ew_box%xrslatij, ew_box%nrsij, ew_box%ndimrij,                 &
!      call getAllocSize(ew_box%Rrsp,ew_box%xrnlat,ew_box%ndimr,ew_box%nrsn,
!     &                rs_box%aij,rs_box%ncount,
!     &                np2rdim,ndimrsnij)
!
!      ew_box%ndimr = max(ndrsn,np2rdim)
!      ew_box%ndimrij = max(iprij,ndimrsnij)
!      allocate( ew_box%xrnlat(1:ew_box%ndimr*3) )
!      ew_box%xrnlat=0
!      allocate( ew_box%xrslatij(1:ew_box%ndimrij,1:4) )
!      ew_box%xrslatij=0
!      allocate( ew_box%np2r(1:ew_box%ndimr,1:nbasis*nbasis) )
!      ew_box%np2r=0
!      allocate( ew_box%numbrs(1:nbasis*nbasis) )
!      ew_box%numbrs=0

      return
      end subroutine allocStrMem

      subroutine getAllocSize(                                          &  ! preStruct2
     &                    cutoff,vectin,ndimin,nin,                     &
     &                    point,npnt,                                   &
     &                    nvmax,nout)
!c     ==================================================================
!c     finds the lead dimension of np2r; called here nvmax
!c     this is also the dimension needed for tmparr of sub ewldvect
!
!c     also finds the lead dimension of xrslatij, ie ndimrhp;
!c     called here nout
!c
!c     the internal variable ndimout is a guaranteed overestimate of the
!c     actual number nout
!c
!c     see sub ewldvect, where np2r is np2v
!c                       and numbrs is numbv
!c                       and xrslatij is vectout
!c     ==================================================================
      implicit none

      integer nin
      integer ndimin
      real*8  vectin(ndimin,3)
      real*8  cutoff

      integer npnt
      real*8  point(3,npnt)

      integer nvmax
      integer nvsum

      integer i,j,k
      real*8  px,py,pz
      real*8  v(3),vsq2
      real*8  rmax2
      integer nvect

      integer ndimout
      integer nout
      real*8, allocatable :: vectout(:,:)
      integer    erralloc

      real*8  tol
      parameter (tol=1.d-8)
!c     ==================================================================

      rmax2 = cutoff**2

      nvmax = 0
      nvsum = 0
      do k=1,npnt
        px = point(1,k)
        py = point(2,k)
        pz = point(3,k)

        nvect = 0
        do i=1,nin
          v(1) = vectin(i,1) - px
          v(2) = vectin(i,2) - py
          v(3) = vectin(i,3) - pz
          vsq2 = v(1)*v(1)+v(2)*v(2)+v(3)*v(3)
          if(vsq2.le.rmax2) then
            nvect = nvect+1
          end if
        end do
        nvmax = max(nvmax,nvect)
        nvsum = nvsum+nvect

      end do

      ndimout = nvsum
      allocate( vectout(1:ndimout,1:3),stat=erralloc )

      nout  = 0
      do k=1,npnt
        px = point(1,k)
        py = point(2,k)
        pz = point(3,k)

        do i=1,nin
          v(1) = vectin(i,1) - px
          v(2) = vectin(i,2) - py
          v(3) = vectin(i,3) - pz
          vsq2 = v(1)*v(1)+v(2)*v(2)+v(3)*v(3)
          if(vsq2.le.rmax2) then
            do j=1,nout
              if(abs(v(1)-vectout(j,1)).lt.tol.and.                     &
     &           abs(v(2)-vectout(j,2)).lt.tol.and.                     &
     &           abs(v(3)-vectout(j,3)).lt.tol) then
                cycle
              end if
            end do
            nout = nout+1
            vectout(nout,1:3) = v(1:3)
          end if
        end do

      end do

      deallocate( vectout,stat=erralloc )

      return
      end subroutine getAllocSize

      subroutine setStrModules(inFile,rs_box,ks_box,ew_box)
      implicit none
      type ( IniFile ) :: inFile
      type ( RS_Str ) :: rs_box
      type ( KS_Str ) :: ks_box
      type ( EW_Str ) :: ew_box

      real*8 :: alat,boa,coa,rcut,rws0
      real(8), allocatable :: xrnlat(:)
      integer :: nmesh,icryst,iprint,imdlng,iyymom,idop,ispkpt
!      integer itypeES(rs_box%natom)
      real*8 :: omegaint
      real*8 :: omegamt(rs_box%nsubl),surfamt(rs_box%nsubl)
      integer :: iptmpr1

      logical :: fullZone = .true.
      integer :: iat,ncomp,nsub,i,imesh,mtasa_tmp
      integer :: st_env

      alat = inFile%alat
      boa = inFile%ba
      coa = inFile%ca
      icryst = inFile%icryst
      iprint = inFile%iprint
      nmesh = inFile%nmesh
      ks_box%lmax = inFile%lmax

!DEBUG
      if ( inFile%Rclstr > zero ) then
       write(6,*) ' DEBUG :: setStrModule'
       write(6,*) ' DEBUG :: Vol,natom=',rs_box%volume,rs_box%natom
       write(6,*) ' DEBUG :: Rclstr=',inFile%Rclstr
       write(6,*) ' DEBUG :: Rnncut=',rs_box%Rnncut
      end if
!DEBUG
!      do iat = 1,rs_box%natom
!       nsub = rs_box%itype(iat)
!       ncomp = inFile%sublat(nsub)%ncomp
!       if ( any( inFile%sublat(nsub)%compon(1:ncomp)%zID.ne.0 ) ) then
!        itypeES(iat) =  nsub
!       else
!        itypeES(iat) = -nsub
!       end if
!      end do

      call setIOflag(1,nu_mdlng,                                        &
     &                   inFile%io(nu_mdlng)%status,imdlng)
      call setIOflag(1,nu_yymom,                                        &
     &                   inFile%io(nu_yymom)%status,iyymom)
      call setIOflag(1,nu_dop,                                          &
     &                   inFile%io(nu_dop)%status,idop)
      if ( idop > 0 ) then
        call ioStrModules(1,inFile,rs_box,ks_box,nu_dop)
      end if
!DEBUG      if ( inFile%imethod == 3 ) then
!DEBUG        ispkpt = -2
!DEBUG      else
        call setIOflag(1,nu_spkpt,                                      &
     &                   inFile%io(nu_spkpt)%status,ispkpt)
        if ( ispkpt > 0 ) then
         call ioStrModules(1,inFile,rs_box,ks_box,nu_spkpt)
        end if
!DEBUG      end if

      mtasa_tmp = min(1,inFile%mtasa)

      iptmpr1 = iprij+ipmdlng+ rs_box%natom*3 +ipxkns+ipxkns*3+ipxkns*2

      allocate( xrnlat(1:ew_box%ndimr*3) )
      call structure1(alat,boa,coa,                                     &
     &   rs_box%natom,rs_box%rslatt,ks_box%kslatt,                      &
     &   rs_box%xyzr(1:3,1:rs_box%natom),                               &
     &   rs_box%itype(1:rs_box%natom),rs_box%aij,rs_box%natom,          &
     &   rs_box%numbsubl,rs_box%nsubl,mtasa_tmp,                        &
     &   rs_box%if0,rs_box%mapstr,rs_box%mappnt,rs_box%mapij,           &
     &   inFile%imethod,                                                &
     &   rs_box%volume,                                                 &
     &   imdlng, invadd, iyymom,
     &   ks_box%dop, ks_box%lmax, ks_box%rot,ks_box%ib,ks_box%nrot,idop,&
     &   inFile%nKxyz,ks_box%qmesh,ks_box%ndimq,nmesh,ks_box%nqpt,      &
     &   ispkpt, ks_box%wghtq, ks_box%twght, ks_box%lrot,               &
     &   ew_box%Rksp, ew_box%xknlat, ew_box%ndimk, ew_box%nksn,         &
     &   ew_box%Rrsp,        xrnlat, ew_box%ndimr, ew_box%nrsn,         &
     &   ew_box%xrslatij, ew_box%nrsij, ew_box%ndimrij,                 &
     &   ew_box%np2r, ew_box%numbrs, rs_box%ncount,                     &
     &   iprint, inFile%istop)
      deallocate(xrnlat)
      do imesh=1,nmesh
!c
!c   groupping of k-points
!c
       call grpkpnt(ks_box%nqpt(imesh),ks_box%wghtq(:,imesh),           &
     &   ks_box%lrot(:,imesh),                                          &
     &   ks_box%ngrp(imesh),ks_box%kptgrp(:,imesh),                     &
     &   ks_box%kptset(:,imesh),ks_box%kptindx(:,imesh))
       write(6,901) imesh,ks_box%ngrp(imesh),                           &
     &           (ks_box%kptgrp(i+1,imesh)-ks_box%kptgrp(i,imesh),      &
     &                 i=1,ks_box%ngrp(imesh))
901    format(' imesh=',i1,' ngrp=',i2,'  :: ',(8i6))
       if(fullZone.and.(ks_box%ngrp(imesh).eq.1)) then
        if(ks_box%wghtq(ks_box%kptindx(ks_box%kptgrp(1,imesh),imesh),   &
     &           imesh).ne.1)     fullZone=.false.
       else
        fullZone=.false.
       end if
      end do
      if ( fullZone ) ks_box%nrot = 1
!
      if( fullZone .and. product(ks_box%nqpt(1:nmesh)) > 1 ) then
       write(6,*)  ' Full Zone Integration....'
      else
       write(6,*)
      end if

      rs_box%rslatt(1:3,1:3) = rs_box%rslatt(1:3,1:3)*(two*pi)
!c
!c   rslatt(1..3,*)/(2*pi)*Alat -- latt.vect.
!c
      rs_box%xyzr(1:3,1:rs_box%natom) =                                 &
     &       rs_box%xyzr(1:3,1:rs_box%natom)*alat
!c
!c   bases(1..3,*) -- in at.un. (0.529177 Anstr.)
!c
!c
      rws0 =  (three/(four*pi)*rs_box%volume/rs_box%natom)**(one/three)
      rs_box%Rnncut = inFile%Rclstr * rws0
!DEBUGPRINT
!      if ( rs_box%Rnncut > zero ) then
!       rcut = rs_box%Rnncut*(two*pi)
!       write(6,*) ' DEBUG :: Vol,natom=',rs_box%volume,rs_box%natom
!       write(6,*) ' DEBUG :: Rclstr,rws0=',inFile%Rclstr , rws0
!       write(6,*) ' DEBUG :: Rnncut,Rnncut*(2*pi)=',rs_box%Rnncut,rcut
!      end if
!DEBUGPRINT
      if(inFile%imethod.ne.0) then
!c
!c  mapsprs(i,j) - map of sparse matrix
!c  mapsprs(i,j).ne.0, only if I and J are neighbours (Rij<Rcut)
!c                  or I and J have common neighbour
!c              =0, otherwise
!c
       rcut = rs_box%Rnncut*(two*pi)
       write(6,*) ' MAPSPRS is calculated with Rnncut=',rcut
       call sprsmap(rs_box%natom,rs_box%mapstr,rs_box%natom,rs_box%aij, &
     &                        rcut,rs_box%rslatt,rs_box%mapsprs)
      end if

      return
      end subroutine setStrModules
!
      subroutine ioStrModules(op,inFile,rs_box,ks_box,id)
      implicit none
      integer, intent(in) :: op    ! -1: write, 1: read
      type ( IniFile ) :: inFile
      type ( RS_Str ) :: rs_box
      type ( KS_Str ) :: ks_box
      integer, optional :: id
      integer :: nu = 1
      integer :: istr,imdlng,iyymom,idop,ispkpt,iprint,ionly
      integer :: idev,imesh,kkrsz,nmesh
      iprint = inFile%iprint
      if ( present(id) ) then
       ionly = id
      else
       ionly = 0
      end if

      if ( ionly==0 .or. ionly==nu_xyz ) then
!c  write the structure file
      call setIOflag(op,nu_xyz,                                         &
     &                   inFile%io(nu_xyz)%status,istr)
      if(istr.ne.0) then
        idev = nu_xyz
!       nu = nunit(2)
       open(unit=nu,                                                    &
     &      file='new.'//inFile%io(idev)%name,                          &
     &      status=char2status(inFile%io(idev)%status),                 &
     &      form=char2form(inFile%io(idev)%form),err=2001)
       call iostrf(istr*nu,rs_box%natom,rs_box%itype,rs_box%rslatt,     &
     &             rs_box%xyzr,inFile%ba,inFile%ca,one,invadd)
       close(unit=nu)
       if (op==-1 .and. inFile%io(idev)%status .ne. 'o' ) then
        inFile%io(idev)%status = 'u'
       end if
      end if
      end if

      if ( ionly==0 .or. ionly==nu_spkpt ) then
!c  read/write the special k-points file
      if ( inFile%imethod == 3 ) then
        ispkpt = -2
      else
        call setIOflag(op,nu_spkpt,                                     &
     &                   inFile%io(nu_spkpt)%status,ispkpt)
      end if
      if(abs(ispkpt).le.1) then
        idev = nu_spkpt
!        nu = nunit(9)
        if(abs(ispkpt).ne.0)                                            &
     &   open(unit=nu,                                                  &
     &      file=inFile%io(idev)%name,                                  &
     &      status=char2status(inFile%io(idev)%status),                 &
     &      form=char2form(inFile%io(idev)%form),err=2001)
        nmesh = size(ks_box%nqpt)
        do imesh=1,nmesh
         if(abs(ispkpt).ne.0) then
          call iokpnt(ispkpt*nu,ks_box%qmesh(1,1,imesh),                &
     &                ks_box%wghtq(1,imesh),ks_box%lrot(1,imesh),       &
     &                ks_box%twght(imesh),ks_box%nqpt(imesh))
         end if
         if(ks_box%nqpt(imesh).gt.ks_box%ndimq.or.                      &
     &                            ks_box%nqpt(imesh).lt.1) then
          write(6,*) ' STATUS=',char2status(inFile%io(idev)%status)
          write(6,*) ' NQPT(',imesh,')=',ks_box%nqpt(imesh)
          write(6,*) ' NDIMQ=',ks_box%ndimq
          call p_fstop(sname//' :: wrong NQPT')
         end if
         if(imesh.gt.1.and.ks_box%nqpt(imesh).gt.ks_box%nqpt(1)) then
          write(6,*) ' NQPT(1)=',ks_box%nqpt(1)
          write(6,*) ' NQPT(',imesh,')=',ks_box%nqpt(imesh)
!          call p_fstop(sname//' :: NQPT(imesh)>NQPT(1)')
         end if
        end do
        close(unit=nu)
        if (op==-1 .and. inFile%io(idev)%status .ne. 'o' ) then
         inFile%io(idev)%status = 'u'
        end if
      end if
      end if

      if ( ionly==0 .or. ionly==nu_dop ) then
!c  read/write the dop-matrices
      call setIOflag(op,nu_dop,                                         &
     &                   inFile%io(nu_dop)%status,idop)
      if(idop.ne.0) then
        idev = nu_dop
!        nu = nunit(10)
        open(unit=nu,                                                   &
     &      file=inFile%io(idev)%name,                                  &
     &      status=char2status(inFile%io(idev)%status),                 &
     &      form=char2form(inFile%io(idev)%form),err=2001)
        kkrsz = (ks_box%lmax+1)**2
        call iodop(idop*nu,ks_box%nrot,kkrsz,ks_box%dop)
        close(unit=nu)
        if (op==-1 .and. inFile%io(idev)%status .ne. 'o' ) then
         inFile%io(idev)%status = 'u'
        end if
      end if
      if(ks_box%nrot.gt.iprot.or.                                       &
     &   (invadd.eq.1.and.iprot.lt.min(48,2*ks_box%nrot))               &
     &  ) then
       write(6,*)
       write(6,*) '  NROT=',ks_box%nrot,' should be .LE. IPROT=',iprot
       if(invadd.eq.1)                                                  &
     & write(6,*) '  INVADD=1', ' NDROT=',iprot,                        &
     &       ' should be >= min(48,2*NROT)=',min(48,2*ks_box%nrot)
       call p_fstop(sname//' :: recompilation of the code is needed')
      end if
      end if

! SHOULD BE DONE IN input-module
!c  read magnetic field value
!CAB      if(nspin.ne.1) then
!CAB        nu = nunit(11)
!CAB        iname = fname(11)
!CAB        cfmt = fmt(ifm(11))
!CAB        cstat= stats(ist(11))
!CAB        FieldMag = zero
!CAB       if(stats(ist(11)).eq.'old') then
!CAB        open(unit=nu,file=iname,status=cstat,form=cfmt,err=2001)
!CAB!c
!CAB!c  to read magnetic field (in Tesla)
!CAB!c
!CAB        write(6,*)
!CAB        read(nu,*) Field
!CAB        write(6,'(''     External magnetic field (Tesla) '',            &
!CAB     &               t40,''='',f10.5)') Field
!CAB
!CAB        call p_fstop(sname//': DEBUG ... not implemented')
!CAB
!CAB        FieldMag = Field*amuBoT
!CAB       end if
!CAB      end if

!c  read/write the YY-moments
!      call setIOflag(op,nu_yymom,                                       &
!     &                   inFile%io(nu_yymom)%status,iyymom)
!c
!      if(iyymom.ne.0) then
!        idev = nu_yymom
!        nmomYY = ipmtout
!        ndmltp = ((ipmltp+1)*(ipmltp+2))/2
!!        nu = nunit(12)
!        iname = inFile%io(idev)%name
!        cfmt = char2form( inFile%io(idev)%form )
!        cstat = char2status( inFile%io(idev)%status )
!        open(unit=nu,file=iname,status=cstat,form=cfmt,err=2001)
!        call ioYYmom(iyymom*nu,ndmltp,nmomYY)
!        close(unit=nu)
!      end if
!
!      call setIOflag(op,nu_mdlng,                                       &
!     &                   inFile%io(nu_mdlng)%status,imdlng)
!
      return

2001  continue
      if ( idev == nu_xyz ) then
        write(*,'(a)')                                                  &
     &     sname//' FILE=<<new.'//trim(inFile%io(idev)%name)//'>>'
      else
        write(*,'(a)')                                                  &
     &     sname//' FILE=<<'//trim(inFile%io(idev)%name)//'>>'
      end if
      write(*,*) '  OPEN ERROR: file should be NEW'
      do idev=1,size(inFile%io)
        if ( inFile%io(idev)%nunit == nu_xyz                            &
     &     .or. inFile%io(idev)%nunit == nu_mdlng                       &
     &     .or. inFile%io(idev)%nunit == nu_spkpt                       &
     &     .or. inFile%io(idev)%nunit == nu_dop                         &
     &     .or. inFile%io(idev)%nunit == nu_yymom                       &
     &     ) then
          write(*,*) idev,' ',inFile%io(idev)%name,' ',                 &
     &                   inFile%io(idev)%status,' ',inFile%io(idev)%form
        end if
      end do
      call p_fstop(sname//':: FILE ERROR ')
      return
!
      end subroutine ioStrModules
!c
      end module struct

      subroutine inpLattGen(lat_const,lat_angl,unitV,alat,boa,coa,      &
     &                                                      e1,e2,e3,un)
      use universal_const, only: bohr2A
      implicit none
      !!      integer, intent(in)  :: group_id
      real(8), intent(in)  :: lat_const(3)
      real(8), intent(in)  :: lat_angl(3)         ! in radian
      real(8), intent(out) :: unitV,alat,boa,coa
      real(8), intent(out) :: e1(3),e2(3),e3(3)   ! "direct" lattice vectors
      real(8), intent(in), optional :: un         ! un is used to convert to atomic unit (Bohr)

      integer :: i
      real(8) :: a,b,c,angl_a,angl_b,angl_c,conv_un
      real(8), parameter :: zero=dble(0), one=dble(1)

      e1 = (/1.d0,0.d0,0.d0/)
      e2 = (/zero, one, zero/)
      e3 = (/zero, zero, one/)
      alat = zero
      boa = dble(1)
      coa = dble(1)
      unitV   = dble(1)
      if ( minval(lat_const) <= zero ) return
      a = lat_const(1)
      b = lat_const(2)
      c = lat_const(3)

      angl_a = lat_angl(1)
      angl_b = lat_angl(2)
      angl_c = lat_angl(3)

      if ( present(un) ) then
       conv_un = un
      else
       conv_un = bohr2A
      end if
      unitV = boa*coa*sqrt(one-cos(angl_a)**2-cos(angl_b)**2-           &
     & cos(angl_c)**2 + dble(2)*cos(angl_a)*cos(angl_b)*cos(angl_c) )
      alat = a/conv_un
      boa  = b/a
      coa  = c/a

      e1 = (/ a, zero, zero /)
      e2 = (/ b*sin(angl_b), b*cos(angl_b), zero /)
      e3 = (/ c*sin(angl_c), c*cos(angl_c)*sin(angl_c),                 &
     &                          c*cos(angl_c)*cos(angl_c) /)
       return
      end subroutine inpLattGen

