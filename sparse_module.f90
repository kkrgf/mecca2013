module sparse

use timing
use invparams, only : invpar_MemoryLimit,invpar_tolinv
use mtrx_interface, only : mmul,invmatr
use mtrx_interface, only : wrtmtx

implicit none

public :: tauScr_ks
public :: tau_nosprs
public :: inv1sprs
!public :: gsvSuperLU

  type, public :: SPRSz_matrix
   integer :: nnz=0                     ! number of non-zero elements
   complex(8), allocatable :: nzval(:)  ! non-zero values  CCS         CRS
   integer, allocatable :: ind(:)       !              row_ind   OR col_ind
   integer, allocatable :: ptr(:)       !              col_ptr      row_ptr
!           matrix rank = size(ptr)-1
  end type
!
  complex(8), parameter :: czero=(0.d0,0.d0)
  complex(8), parameter :: cone=(1.d0,0.d0)

!  type, public :: z1ptr
!    complex(8), pointer :: p(:)
!  end type

!  type, public :: i1ptr
!    integer, pointer :: p(:)
!  end type

!   ! distributed Harwell-Boeing format
!  type, public :: matrixDHW
!    integer :: rank
!    type(z1ptr), pointer :: ColEntries(:)
!    type(i1ptr), pointer :: ColIndices(:)
!    integer, pointer     :: ColLengths(:)
!  end type

   ! compressed Harwell-Boeing format
!  type, public :: matrixCHW
!    integer :: rank, nonzeros, memsize
!    complex*16, pointer :: Entries(:)
!    integer, pointer :: Indices(:)
!    integer, pointer :: Columns(:)
!    real*8,  pointer :: ColumnSqNorms(:)
!    integer, pointer :: RowIndices(:)
!    integer, pointer :: Rows(:)
!  end type

contains

      subroutine tauScr_ks(lmax,natom,                                  &
     &              aij,                                                &
     &              itype,nstot,                                        &
     &              mapstr,ndimbas,                                     &
     &              kx,ky,kz,                                           &
     &              tcpa,                                               &
     &              ndkkr,deltat,deltinv,                               &
     &              numnbi,jclust,                                      &
     &              mapsnni,ndimnn,                                     &
     &              nnptr,maxclstr,nnclmn,                              &
     &              rsij,                                               &
     &              greenrs,ndgreen,tau00,                              &
     &              istop)
!c     ==================================================================
!c
!c  To calculate diagonal block of Tau (tau00) in k-space
!c
!c     ==================================================================

!c
      implicit none
!
      integer, intent(in) :: lmax,natom
      real(8), intent(in) :: aij(3,*)
      integer, intent(in) :: itype(natom),nstot
      integer, intent(in) :: mapstr(ndimbas,natom)
      integer, intent(in) :: ndimbas
      real(8), intent(in) :: kx,ky,kz
      complex(8), intent(in) :: tcpa(ndkkr,ndkkr,nstot)
      integer, intent(in) :: ndkkr
      complex(8), intent(in) :: deltat(ndkkr,ndkkr,natom)
      complex(8), intent(in) :: deltinv(ndkkr,ndkkr,natom)
      integer, intent(in) :: numnbi(nstot)
      integer, intent(in) :: jclust(ndimnn,nstot)
      integer, intent(in) :: mapsnni(ndimnn*ndimnn,nstot)
      integer, intent(in) :: ndimnn
      integer, intent(in) :: nnptr(0:1,natom)
      integer, intent(in) :: maxclstr
      integer, intent(in) :: nnclmn(2,maxclstr,natom)
      real(8), intent(in) :: rsij(ndimnn*ndimnn*3,nstot)
      complex(8), intent(in) :: greenrs(ndgreen,nstot)
      integer, intent(in) :: ndgreen
      complex(8), intent(out) :: tau00(ndkkr,ndkkr,natom)
      character(*), intent(in) :: istop

!c     ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
      character    sname*10
      parameter    (sname='tauScr_ks')
!c     ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
      integer kkrsz
!DELETE      integer ndmat
      integer, parameter :: iswitch = 0 ! to choose sparse method techniques (SuperLU - 0, TFQMR - 1, MAR - 2)
      integer :: iat

      integer, external :: indclstr


      complex(8), pointer :: greenKS(:,:,:)    ! (:,:,1) -- diagonal blocks;
                                               ! (:,:2:natom) -- off-diagonal blocks
!??      integer :: mapnn(natom,natom)              ! ne = mapnn(jat,iat) points to a block greenKS(:,ne)
      complex(8), allocatable :: ztmp(:,:)
      complex(8), pointer :: zvalue(:)
      integer, pointer :: indrow(:)
      integer, pointer :: iptcol(:)

      kkrsz = (lmax+1)*(lmax+1)
!c***********************************************************************

       call ft_rsGref(                                                  &
     &  natom,itype,ndimnn,                                             &
     &  nnptr,nnclmn,jclust,mapstr,                                     &
     &  kkrsz,kx,ky,kz,aij,greenrs,ndgreen,                             &
     &  numnbi,rsij,mapsnni,                                            &
     &                                      greenKS)
!
!c   deltat,deltinv -- matrix [NDKKRxNDKKR,*]
!c          deltinv == deltat^(-1)

       allocate(iptcol(kkrsz*kkrsz*size(greenKS,2)+1))
!       allocate(zvalue(1))
!       allocate(indrow(1))
       call gcms21mgt(greenKS,kkrsz,natom,deltat,nnclmn(1,:,:),         &
     &                zvalue,iptcol,indrow)
       call inv1sprs(                                                   &
     &              zvalue,iptcol,indrow,                               &
     &              kkrsz,natom,deltat(1:ndkkr,1:ndkkr,1:natom),        &
     &              ndkkr,tau00(1:ndkkr,1:ndkkr,1:natom),               &
     &              -natom)
       if ( associated(zvalue) ) deallocate(zvalue)
       if ( associated(indrow) ) deallocate(indrow)
       if ( associated(iptcol) ) deallocate(iptcol)
       if ( associated(greenKS)) deallocate(greenKS)

!DEBUGPRINT
!DELETE       write(6,'(a)') '==========================================='
!DELETE       write(6,*) ' (1-G*t)^(-1)'
!DELETE       write(6,'(a)') '==========================================='
!DELETE       do iat=1,natom
!DELETE        call wrtmtx(tau00(1:kkrsz,1:kkrsz,iat),kkrsz,'##########')
!DELETE       end do
!!DELETE       call fstop('DEBUG')
!DEBUGPRINT

      allocate(ztmp(kkrsz,kkrsz))
      do iat=1,natom
       ztmp(1:kkrsz,1:kkrsz) = deltinv(1:kkrsz,1:kkrsz,iat)
       call zgemm('N','N',kkrsz,kkrsz,kkrsz,cone,                       &
     &            tau00(:,:,iat),size(tau00,1),                         &
     &            deltinv(:,:,iat),size(deltinv,1),                     & 
     &            -cone,ztmp,size(ztmp,1))
!DEBUGPRINT
!DELETE        write(6,*) ' DEBUG G (ScrKS):'
!DELETE         call wrt2ccs(kkrsz,ztmp(1:kkrsz,1:kkrsz),1.d-9)
!DEBUGPRINT
       call gr2tauz(ztmp,kkrsz,tcpa,size(tcpa,1),jclust(:,itype(iat)),  &
     &              1,kkrsz,tau00(:,:,iat))
      end do
      if ( allocated(ztmp)) deallocate(ztmp)

!DEBUGPRINT
!DELETE       write(6,*) ' TAU00(E,k)'
!DELETE       do iat=1,natom
!DELETE        call wrtmtx(tau00(1:kkrsz,1:kkrsz,iat),kkrsz,'##########')
!DELETE       end do
!?        write(114,*) '#taumix KS-tau00'
!?        do iatom=1,natom
!?        write(114,'(a,i5,a,3e17.8)') ' iat=',iatom,' kx,ky,kz=',kx,ky,kz
!?        do l=1,(kkrsz)
!?        do m=1,(kkrsz)
!?         write(114,'(6(1x,e13.5))') tau00(l,m,iatom)
!?        end do
!?        end do
!?        end do
!?        call fstop(' DEBUG taumix ft_rsGF+sparse-KS')
!DEBUGPRINT
       if (istop.eq.trim(sname)) call fstop(sname)
!c***********************************************************************
!c
      return
      end subroutine tauScr_ks

!c     cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      subroutine inv1sprs(cvalue,iptcol,indrow,                         &
     &                    kkrsz,natom,deltat,ndimt,                     &
     &                    taudlt,                                       &
     &                    na                                            &
     &                   )
!c     ================================================================
!c
!c  To calculate diagonal blocks of matrix <deltat*[1-amt*deltat]^(-1)>,
!c         cvalue  -- ccs-sparse matrix, cvalue = <1-amt*deltat>
!c         deltat -- "diagonal" of full matrix, order KKRSZ
!c
!c  output: taudlt[ndimt,ndimt,NA],  (NA is a number of output diagonal
!                       matrix blocks, rank kkrsz, i.e. nrhs = kkrsz*NA)
!           taudlt = <deltat*cvalue^(-1)> block-diagonal matrix
!c
!c
!!??!cab   TFQMR (iswitch=1,2), SuperLU (iswitch=0)
!c
!      use sparse, only : gsvlusp => gsvSuperLU
      implicit none
!c
      complex(8), intent(in) :: cvalue(:)
      integer, intent(in) :: iptcol(:)            ! (kkrsz*natom+1)
      integer, intent(in) :: indrow(:)
      integer, intent(in) :: kkrsz,natom
      complex(8), intent(in) :: deltat(ndimt,ndimt,natom)
      integer, intent(in) :: ndimt
      complex(8), intent(inout) :: taudlt(ndimt,ndimt,*)  !  if na>0, size(taudlt,3) = na,
                                                          ! otherwise size(taudlt,3) = 1
      integer, intent(in) :: na
!c
      integer :: j,ne
      integer :: iflag,info,iperm,nrhs,nrmat
      integer :: nsub1
!      integer(8) :: factors
!!??      integer :: nitmax,iprcnd
      real(8) :: tolinv
!DEBUGPRINT
!DELETE      integer i
!DELETE      complex(8), allocatable :: tmpz(:,:) ! (1:kkrsz*natom,1:kkrsz*natom)
!DEBUGPRINT

!c parameter
      character(10), parameter :: sname='inv1sprs'

!!??      logical lprecond
!!??
!!??      integer isave/1/
!!??      save isave
!!??
!!??      character*3 ILU,JCB,CLS
!!??      parameter (ILU='ILU',JCB='JCB',CLS='CLS')
!!??      character*3 PRECONDITIONER(0:3)
!!??      data  PRECONDITIONER/'   ',JCB,ILU,CLS/

      complex(8), allocatable :: vecs(:,:)

      interface
!DELETE      subroutine zgssv_SLU(n,nnz,values,rowind,colptr,b,ldb,nrhs,       &
!DELETE     &                     factors,info,iflag)
!DELETE      implicit none
!DELETE      integer, intent(in) :: n   ! matrix rank
!DELETE      integer, intent(in) :: nnz ! number of non-zeros
!DELETE      complex(8), intent(in) :: values(nnz)     ! 1:nnz
!DELETE      integer, intent(in) :: rowind(nnz)        ! 1:nnz
!DELETE      integer, intent(in) :: colptr(n+1)        ! 1:n+1
!DELETE      complex(8), intent(inout) :: b(*)       ! n * nrhs
!DELETE      integer, intent(in) :: ldb,nrhs
!DELETE      integer(8), intent(inout) :: factors    ! saved variable on upper level
!DELETE      integer, intent(out) :: info
!DELETE      integer, intent(in), optional :: iflag
!DELETE      end subroutine
      end interface
!c     ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!c

      tolinv = invpar_tolinv()

!c                  Now, amt --> (1-amt*deltat)

      nrmat = kkrsz*natom    ! matrix rank
      ne = iptcol(nrmat+1)-1 ! number of non-zero elements
      nrhs = abs(na) * kkrsz      ! number of diagonal blocks of rank kkrsz
      iperm = 10
      allocate(vecs(nrmat,nrmat))

      call slu_matrinv(iperm,nrmat,ne,cvalue,indrow,iptcol,vecs,nrmat,  &
     &                                                            info)

      if(info.ne.0) then
         write(6,*) ' IFLAG=',iflag
         write(6,*) ' NE=',ne
         write(6,*) ' NRHS=',kkrsz
         write(6,*) ' NRMAT=',nrmat
         write(6,*) ' INFO=',info
         call fstop(sname//' :: INFO.ne.0 after slu_matrinv')
      end if

!      if (info .eq. 0) then
!       write (6,*) 'SuperLU solve succeeded, vecs:'
!       call wrt2ccs(kkrsz,vecs(1:kkrsz,1:kkrsz),tolinv)
!      end if

      if ( na>0 ) then
!
!   Output taudlt <-- deltat*(1-amt*deltat)(-1)
!
         do nsub1=1,na
          j = kkrsz*(nsub1-1)
          call mmul(deltat(:,:,nsub1),vecs(j+1:,j+1:),taudlt(:,:,nsub1),&
     &                                                           kkrsz)
         end do
      else
!
!   Output taudlt <-- (1-amt*deltat)(-1)
!
         if ( na==0 ) then
!           taudlt(1:kkrsz,1:kkrsz,1) = vecs(1:kkrsz,1:kkrsz)
           call cpblk2(vecs,nrmat,1,1,taudlt(1,1,1),ndimt,1,1,kkrsz)
         else
          do nsub1=1,-na
           j = kkrsz*(nsub1-1)
!           taudlt(1:kkrsz,1:kkrsz,nsub1) = vecs(j+1:j+kkrsz,j+1:j+kkrsz)
           call cpblk2(vecs,nrmat,nsub1,nsub1,taudlt(1,1,nsub1),ndimt,  &
     &                                                       1,1,kkrsz)
          end do
         end if
      end if

      return
      end subroutine inv1sprs

      subroutine ft_rsGref(                                             & 
     & natom,itype,ndimnn,                                              &
     & nnptr,nnclmn,jclust,mapstr,kkrsz,kx,ky,kz,                       &
     & aij,greenrs,ndgreen,                                             &
     & numnbi,rsij,mapsnni,                                             &
     &                                     greenKS)

      implicit none

      integer, intent(in) :: natom,itype(natom),ndimnn
      integer, intent(in) :: nnptr(0:,:)
      integer, intent(in) :: nnclmn(:,:,:)
      integer, intent(in) :: jclust(:,:)
      integer, intent(in) :: mapstr(:,:)
      integer, intent(in) :: kkrsz
      real(8), intent(in) :: kx,ky,kz
      real(8), intent(in) :: aij(3,*)
      integer, intent(in) :: ndgreen,numnbi(*)
      complex(8), intent(in) :: greenrs(ndgreen,*)
      real(8), intent(in) :: rsij(ndimnn*ndimnn*3,*)
      integer, intent(in) :: mapsnni(ndimnn*ndimnn,*)
!???      type(SPRSz_matrix), pointer :: greenKS(1:natom)  
!?      complex(8), intent(out) :: greenks(kkrsz*kkrsz,*)
      complex(8), pointer :: greenks(:,:,:)

      integer :: iatom,inn,inn0,isub,jatom,jatom0,ia0
      integer :: nd,nnmax
      character(10) :: istop='******'

      interface
      subroutine ksmatr(jcell,                                          &
     &    kx,ky,kz,aij,greens,                                          &
     &    numnbi,kkrsz,rsij,                                            &
     &    mapsnni,ndimnn,nnmax,                                         &
     &    jclust,                                                       &
     &    aksp,                                                         &
     &    invswitch,                                                    &
     &    istop)
      implicit none
      integer    iatom0,jatom0,jcell
      real*8     kx,ky,kz,aij(3)
      complex*16 greens(kkrsz*numnbi,*)     !  kkrsz*numnbi,kkrsz*numnbi)
      integer    kkrsz,numnbi
      real*8     rsij(3,*)
      integer    ndimnn
      integer    mapsnni(ndimnn,numnbi)
      integer    nnmax,jclust(nnmax)
      complex(8), intent(out) :: aksp(*) ! kkrsz*kkrsz 
      integer    invswitch
      character  istop*10
      end subroutine ksmatr
      end interface
!
!c
!c  Fourier transformation of real-space screened Greens function:
!c    only for (iatom,jatom) belonging to the same original cell,
!c
        
!
!       A = 0
!       do i=1,natom
!        i0 = nnptr(1,i)
!        do inn=1,nnptr(0,i)
!          j=nnclmn(1,inn,i)
!          j0 = nnclmn(1,(nnclmn(2,inn,i),i0)
!          A[i,j] = B[i0,j0]
!        end do
!       end do

        nd = maxval(nnptr(0,1:natom))
        allocate(greenks(kkrsz*kkrsz,nd,natom))
        do iatom = 1,natom
!c         iatom0 = nnptr(1,iatom)
          isub = itype(nnptr(1,iatom))
          nnmax = nnptr(0,iatom)
          do inn=1,nnmax
           inn0 = nnclmn(2,inn,iatom)
           jatom = nnclmn(1,inn,iatom)
           jatom0 = jclust(inn0,isub)    ! origin of "jatom"
           ia0 = mapstr(iatom,jatom)
           if ( ia0.ne.0 ) then
             call ksmatr(jatom0,                                        &
     &                 kx,ky,kz,aij(1,ia0),greenrs(1,isub),             &
     &                 numnbi(isub),kkrsz,rsij(1,isub),                 &
     &                 mapsnni(1,isub),ndimnn,nnmax,                   &
     &                 jclust(1:nnmax,isub),                           &
     &                 greenks(:,inn,iatom),                            &
     &                 1,                                               &
     &                 istop)
           else
            call zerooutC(greenks(:,inn,iatom),kkrsz*kkrsz)
           end if
          end do
          do inn = nnmax+1,nd
           call zerooutC(greenks(:,inn,iatom),kkrsz*kkrsz)
          end do
        end do

      return
      end subroutine ft_rsGref

!subroutine gsvSuperLU( &
!  Rank, lmRank, MaxClusterSize, nAtoms,           &
!  ClusterSizes, ClusterLists,                     &
!  Entries, InvDiag,tolinv                         &
!  )
!  !
!  ! Entries has memory size Rank * lmRank * MaxClusterSize and 
!  ! is assumed to be (1:lmRank*lmRank,1:MaxClusterSize,1:nAtoms)-arrray;
!  ! it is duplicated in compressed column format compatible with SuperLU package.
!  !
!  ! On output, InvDiag stores the diagonal blocks lmRank*lmRank of the inverse matrix 
!  !
!
!  implicit none
!
!  ! Inputs
!
!  integer, intent(in) :: Rank            ! natom*kkrsz
!  integer, intent(in) :: lmRank          ! kkrsz
!  integer, intent(in) :: MaxClusterSize  ! ndimnn
!  integer, intent(in) :: nAtoms
!
!  integer, intent(in) :: ClusterSizes(nAtoms)
!  integer, intent(in) :: ClusterLists(MaxClusterSize,nAtoms)
!!  integer, intent(in) :: EquivAtoms(nAtoms)
!
!  complex(8), intent(in) :: Entries(Rank*lmRank*MaxClusterSize)
!
!  real(8), intent(in), optional :: tolinv
!
!  ! Output
!
!  complex(8), intent(out) :: InvDiag(lmRank,lmRank,nAtoms)
!
!  ! Local Variables
!
!    integer :: a_rank, a_nonzeros
!    integer :: a_memsize
!    complex(8), pointer :: a_Entries(:) => null()
!    complex(8), pointer :: cc_entries(:) => null()
!    integer, pointer :: a_Indices(:) => null()
!    integer, pointer :: cc_indices(:) => null()
!    integer, allocatable :: a_Columns(:)
!
!  integer :: myid
!
!  integer :: lmBlockStride
!  integer :: lmBlockRowStride
!
!  real(8) :: EntryTol, tol_tol
!
!  ! SuperLU Variables
!
!  integer :: factors(8), grid(8)
!  integer :: nrhs, info, ldb
!  real*8  :: berr
!
!  ! Debugging Variables
!
!!DEBUG  logical, parameter :: DebugFlag   = .false.
!  logical, parameter :: DebugFlag   = .true.
!  real(8) :: xx, s_time
!
!  ! Transient Variables
!
!  integer :: i, inn, j, k, l, m, n, ec
!  integer :: iA, jA, jL
! 
!  real(8) :: re
!  complex(8) :: z
!  complex(8), allocatable :: zptr(:)
!  complex(8), allocatable :: zptr2(:)
!
!  ! Parallel Matrix Solves
!
!  integer :: Kgroup, numPeers
!  logical :: hasKgroup
!!DEBUG  common/prllK/ Kgroup, hasKgroup
!  kGroup = 0
!  hasKgroup = .false.
!!DEBUG
!
!!DEBUGPRINT
! write(6,*) ' DEBUG in gsvSuperLU tolinv=',tolinv
!!DEBUGPRINT
!
!  a_memsize = Rank*lmRank*MaxClusterSize
!  ! -------------------------------------------------------------------------
!  ! Make a copy of input matrix in Harwell-Boeing Format
!  ! -------------------------------------------------------------------------
!
!  ! Validate inputs
!
!  if( mod(Rank,lmRank) .ne. 0 ) then
!    call fstop('gsvSuperLU :: matrix rank % kkrsz != 0')
!  end if
!
!  if( nAtoms .ne. Rank/lmRank ) then
!    call fstop('gsvSuperLU :: rank / kkrsz != num atoms')
!  end if
!
!  ! Begin Timer for finding inverse
!
!  if( DebugFlag ) then
!    call cpu_time(xx)
!    s_time = xx
!  end if
!
!  ! Predetermine number of nonzeros entries in matrix A
!
!  lmBlockStride = lmRank * lmRank
!  lmBlockRowStride = lmBlockStride * MaxClusterSize
!
!  if ( present(tolinv) ) then
!   EntryTol = tolinv
!  else
!   EntryTol = 1.0d-15 
!  end if
!  tol_tol = 2*EntryTol*EntryTol
!
!  a_rank = Rank
!  a_nonzeros = 0
!
!  ! Allocate room for a fully compressed copy of A
!
!  allocate( &
!    cc_entries(a_memsize), &
!    cc_indices(a_memsize), &
!    a_Columns(a_rank+1),   &
!    stat = ec )
!  if(ec.ne.0) call fstop('gsvSuperLU :: insufficient memory :: A')
!
!  ! Make copy of A in Compressed Column format
!  ! note: assumes the supplied cluster list is ordered
!
!  if( DebugFlag ) call BeginTiming('Convert A to Compressed Column format')
!
!!DEBUG  n = 0
!  n = 1
!  do jA = 1, nAtoms
!  do jL = 1, lmRank
!
!    j = (jA-1)*lmRank + jL
!    a_Columns(j) = n
!  
!    k = (jL-1)*lmRank - lmBlockRowStride
!
!    do inn=1,ClusterSizes(jA)
!     iA = ClusterLists(inn,jA)
!     l = (iA-1)*lmRank
!     m = l + (inn-1)*lmBlockStride
!     do i=m+1, m+lmRank
!      if( dble(Entries(i)*conjg(Entries(i))) > tol_tol ) then
!         n = n + 1
!         if ( n>size(cc_indices) ) then
!          write(6,*) ' gsvSuperLU :: N of nonzero > ',size(cc_indices)  
!          call fstop(' ERROR :: unexpected memory issue')
!         end if
!         cc_entries(n) = Entries(i)
!         cc_indices(n) = l+m
!      end if 
!     end do
!    end do
!
!  end do ; end do
!
!  a_Columns(a_rank+1) = n
!  a_nonzeros = n
!
!  if(ec.ne.0) call fstop('gsvSuperLU :: insufficient memory :: Indices')
!  a_Indices => cc_indices(1:a_nonzeros)
!  deallocate(cc_indices) ; cc_indices => null()
!  if(ec.ne.0) call fstop('gsvSuperLU :: insufficient memory :: Entries')
!  a_Entries => cc_entries(1:a_nonzeros)
!  deallocate(cc_entries); cc_entries => null()
!
!
!  if( DebugFlag ) call EndTiming()
!
!  ! -------------------------------------------------------------------------
!  ! Debugging: Write Matrix to file
!  ! -------------------------------------------------------------------------
!
!!DEBUGPRINT
! open(unit=80,file='matrix',status='replace',action='write')
!
! write(80,*) a_Rank, a_Nonzeros
!
! do j = 1, a_Rank
! do k = a_Columns(j)+1, a_Columns(j+1)
!   write(80,*) j,k,a_Entries(k),a_Indices(k)
! end do; end do
!
! close(unit=80)
!!DEBUGPRINT
!! stop
!
!  ! -------------------------------------------------------------------------
!  ! Solve A x = b using an LU decomposition
!  ! -------------------------------------------------------------------------
!
!  allocate( zptr(rank), stat = ec )
!  if(ec/=0) call fstop('gsvSuperLU :: insufficient memory :: zptr')
!
!  ! Switch to 1-based indexing for c_fortran_zgssv
!
!!DEBUG  do i = 1, a_rank+1
!!DEBUG    a_Columns(i) = a_Columns(i)+1
!!DEBUG  end do
!
!  ! Factorize A
!
!  ldb = a_Rank; myid = 0
!  if( .not. hasKgroup ) then
!!DEBUGPRINT
!    write(6,*) 'gsvSuperLU :: using sequantial'
!    write(6,*) ' a_rank,a_nonzeros,nrhs=', a_rank,a_nonzeros,nrhs
!    write(6,*) ' rank,ldb=',rank,ldb 
!!DEBUGPRINT
!    call c_fortran_zgssv(1, a_rank, a_nonzeros, nrhs, a_Entries(1), &
!           a_Indices(1), a_Columns(1), zptr, ldb, factors, info)
!  else
!    call MPI_Comm_Size(Kgroup,numPeers,info)
!    call MPI_Comm_Rank(Kgroup,myid,info)
!    if(myid == 0) write(6,*) 'gsvSuperLU :: using parallel'
!    call c_fortran_slugrid(1,Kgroup,1,numPeers,grid)
!    call c_fortran_pzgssvx_ABglobal(1, a_rank, a_nonzeros, nrhs,    &
!            a_Entries(1), a_Indices(1), a_Columns(1), zptr, ldb, grid, &
!            berr, factors, info)
!  end if
!  if(info/=0) call fstop('gsvSuperLU :: factorization failed')
!
!  if( DebugFlag ) then
!    call cpu_time(xx)
!    if(myid==0) then
!      write(6,*) 'gsvSuperLU :: factorization complete, timed at ', &
!                 xx-s_time
!    end if
!    s_time =xx
!  end if
!! 
!  ! Solve A x = I, one column at a time
!
!  nrhs = 1
!  atomloop: &
!  do jA = 1, nAtoms
!
!!DELETE    ! If this atom is equivalent to a previous one, copy corresponding block
!!DELETE    ! warning: produces correct trace but not correct inverse
!!DELETE   
!!DELETE    do j = 1, jA-1
!!DELETE    if( EquivAtoms(j) == EquivAtoms(jA) ) then
!!DELETE
!!DELETE      InvDiag(:,:,jA) = InvDiag(:,:,j)
!!DELETE      cycle atomloop
!!DELETE
!!DELETE    end if; end do
!
!    ! Otherwise solve for each column corresponding to this atom
!
!    do jL = 1, lmRank
!
!      j = (jA-1)*lmRank + jL
!      zptr(:) = (0.0d0,0.0d0)
!      zptr(j) = (1.0d0,0.0d0)
!  
!      if( .not. hasKgroup ) then
!        call c_fortran_zgssv(2, a_rank, a_nonzeros, nrhs, a_Entries(1), &
!               a_Indices(1), a_Columns(1), zptr, ldb, factors, info)
!      else
!        call c_fortran_pzgssvx_ABglobal(3, a_rank, a_nonzeros, nrhs, &
!               a_Entries(1), a_Indices(1), a_Columns(1), zptr, ldb,  &
!               grid, berr, factors, info)
!      end if
!      if(info/=0) call fstop('gsvSuperLU :: failed back solve')
!
!      if( DebugFlag .and. myid == 0 ) then
!        allocate(zptr2(rank))
!        ! slow check.. need to use blas.. recall changed indexing
!        zptr2 = (0.0,0.0)
!        do j = 1, rank
!          z = zptr(j)
!          do k = a_Columns(j), a_Columns(j+1)-1
!            i = a_Indices(k)
!            zptr2(i) = zptr2(i) + z*a_Entries(k)
!          end do
!        end do
!        re = sum(dble(zptr2(1:rank)*conjg(zptr2(1:rank))))
!        write(6,*) '  gsvSuperLU :: residual =', re
!        deallocate(zptr2)
!      end if
!
!      if(myid == 0) write(*,*) '  gsvSuperLU :: solved for x'
!
!      i = (jA-1)*lmRank
!      InvDiag(:,jL,jA) = zptr(i+1:i+lmRank)
!
!    end do
!
!  end do atomloop
!
!  ! Deallocate matrices
!
!  if( .not. hasKgroup ) then
!    call c_fortran_zgssv(3, a_rank, a_nonzeros, nrhs, a_Entries(1), &
!           a_Indices(1), a_Columns(1), zptr, ldb, factors, info)
!  else
!    call c_fortran_pzgssvx_ABglobal(4, a_rank, a_nonzeros, nrhs, &
!           a_Entries(1), a_Indices(1), a_Columns(1), zptr, ldb,  &
!           grid, berr, factors, info)
!    call c_fortran_slugrid(2,Kgroup,1,numPeers,grid)
!  end if
!  deallocate(zptr)
!
!  if ( associated(cc_entries) ) deallocate(cc_entries)
!  if ( associated(cc_indices) ) deallocate(cc_indices)
!  if ( associated(a_Entries) ) deallocate(a_Entries)
!  if ( associated(a_Indices) ) deallocate(a_Indices)
!  if ( allocated(a_Columns) ) deallocate(a_Columns)
!
!  ! Print time for inversion
!
!  if( DebugFlag ) then
!    call cpu_time(xx)
!    if(myid == 0) then
!      write(6,*) 'gsvSuperLU :: inversion complete, timed at ', &
!                 xx-s_time
!    end if
!  end if
! 
!end subroutine gsvSuperLU

!BOP
!!IROUTINE: tau_nosprs
!!%INTERFACE:
!!REMARKS:
! Sparse version of mecca is not supported
!EOP
      subroutine tau_nosprs()
!BOC
      implicit none
      character    sname*10
      parameter    (sname='tau_nosprs')
!c     ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
      write(*,*) ' SPARSE VERSION OF MECCA IS NOT SUPPORTED',           &
     &           ' IN THIS DISTRIBUTION......'
      call fstop(sname//': sparse version is not supported')
!c----------------------------------------------------------------------- !-
      return
      end subroutine tau_nosprs

      subroutine gsvlusp(a,ndmat,                                       &
     &                   nnptr,nnclmn,ndimnn,                           &
     &                   cof,ndcof,                                     &
     &                   itype,kkrsz,natom,                             &
     &                   iprint)
!c     ================================================================
!c
      implicit none
!c
!c      To find solution of equation:  A*X=1  (A=1-G*t)
!c      using sparse LU algorithm
!c
!c      and then output:  cof = X  (tau = t*cof)
!c
!c      niter -- maximum number of iterations
!c
!c
!c
!c     ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!c
      integer ndmat,natom,kkrsz
      complex*16 a(ndmat,*)
!c  a(ndmat,1:natom), ndmat .ge. maxclstr*kkrsz**2

      integer nnptr(0:1,natom),ndimnn,nnclmn(2,ndimnn,natom)
!c      complex*16 vecs(kkrsz*natom,*)
      integer ndcof
      complex*16 cof(ndcof,ndcof,natom)
      integer    itype(natom)
      integer iprint

!c      complex*16 sum,sum0
!      complex*16 czero,cone
!      parameter (czero=(0.d0,0.d0),cone=(1.d0,0.d0))

      integer erralloc
      complex*16,  allocatable :: cvalue(:)
      complex*16,  allocatable :: vecs(:,:)

      integer, allocatable :: iptrow(:)
      integer, allocatable :: indcol(:)

      integer,  allocatable :: work(:)
      integer(8) :: factors

      character sname*10
      parameter (sname='gsvlusp')

      integer :: i,iatom,iflag,info,inn,ii,indI,indIJ,indJ
      integer :: j,jatom,jn,jns,jnn
      integer :: kelem 
      integer :: l1,l1a,lmi,lmj,lmij
      integer :: ne,nrmat,nrhs,ns1,ntime,nsub1,nsub2
!??      integer, pointer :: colptr(:) => null()
!??      integer, pointer :: rowind(:) => null()
      real(8) :: time1,time2
      complex*16 aij
!c      complex*16 aij,aii
      integer, save :: isave=0
!c      integer isave/0/,ntry,l1a,j,jj,ii

!CDEBUG
!DEBUG_TIME      real timeEn,timeIneq
!DEBUG_TIME      common /Alltime/timeEn,timeIneq
!CDEBUG

      if(isave.eq.0) then
       write(6,*)
       write(6,*)                                                       &
     & '    SuperLU PACKAGE ',                                          &
     &     'IS USED TO INVERT MATRIX'
!c       write(6,*) '  MAXIMUM NUMBER OF ITERATIONS = ',niter,
!c     *     ' AND TOLINV=',real(tolinv)
       write(6,*)
       isave=1
      end if

      time2 = 0.d0
!DEBUG_TIME      call timel(time2)

      nrmat = kkrsz*natom

!???      nrhs  = 1
      nrhs  = kkrsz

      allocate(vecs(1:nrmat,nrhs),                                      &
     &         iptrow(1:nrmat+1),                                       &
     &         stat=erralloc)
      if(erralloc.ne.0) then
      write(6,*) ' NRMAT=',nrmat
      write(6,*) ' NRHS+1=',nrhs+1
      write(6,*) ' ERRALLOC=',erralloc
      call fstop(sname//' :: ALLOCATION MEMORY PROBLEM')
      end if

      ne = 0
      i=0
      do iatom=1,natom
       indI = (iatom-1)*ndmat
       do lmi=1,kkrsz
!c         i = (iatom-1)*kkrsz+lmi
        i = i + 1
        indI = lmi-kkrsz
        do jnn=1,nnptr(0,iatom)
          indIJ = indI + (jnn-1)*(kkrsz*kkrsz)
         do lmj=1,kkrsz
          if(a(indIJ+lmj*kkrsz,iatom).ne.czero) then
           ne = ne+1
          end if
         end do
        end do
       end do
      end do

      if(iprint.ge.0) then
       write(6,*) ' NE=',ne,' NRMAT*NRMAT=',nrmat*nrmat
      end if

      allocate(cvalue(1:ne),                                            &
     &         indcol(1:ne),                                            &
     &         stat=erralloc)
      if(erralloc.ne.0) then
      write(6,*) ' NE=',ne
      write(6,*) ' NRMAT=',nrmat
      write(6,*) ' NDMAT=',ndmat
      write(6,*) ' ALLOCATION (C16) =',ne+nrmat+(nrmat+1+ne)/4+1
      write(6,*) ' ERRALLOC=',erralloc
      call fstop(sname//' :: ALLOCATION MEMORY PROBLEM')
      end if

      kelem = 0
      iptrow(1) = 1

      i = 0
      do iatom=1,natom
       indI = (iatom-1)*ndmat
       do lmi=1,kkrsz
!c        i = (iatom-1)*kkrsz+lmi
        i = i + 1
        do inn=1,nnptr(0,iatom)
         jatom = nnclmn(1,inn,iatom)
         indJ = (lmi-kkrsz) + (inn-1)*(kkrsz*kkrsz)
         do lmj=1,kkrsz
          j = (jatom-1)*kkrsz+lmj
          lmij = lmj*kkrsz +indJ
!c                          lmij = (lmj-1)*kkrsz + lmi
!CDEBUG
          aij = a(lmij,iatom)
!CDEBUG          aij = a(indI+lmij)
          if(aij.ne.czero) then
           kelem = kelem+1
           cvalue(kelem) = aij
           indcol(kelem) = j
          end if
         end do
        end do
        iptrow(i+1) = kelem+1
       end do
      end do

      call zgssv_SLU(nrmat,ne,cvalue,indcol,iptrow,                     &
     &                  vecs(1,1),size(vecs,1),nrhs,factors,info,iflag)
!???      colptr => iptrow
!///      rowind => indcol

!c      call ge2crs(a,ndmat,nrmat,tolinv,cvalue,iptrow,indcol,ne,2)

!CAB      do i =1,ne
!CAB       a(i) = cvalue(i)
!CAB      end do

      if(nrhs.eq.1) then
!c       l1mx = kkrsz
!c       l1mn = 1
      else if(nrhs.eq.kkrsz) then
!c       l1mx = 1
!c       l1mn = 1
      else
       write(6,*) ' KKRSZ=',kkrsz
       write(6,*) ' NRHS=',nrhs
       call fstop(sname//' :: not implemented ')
      end if

      factors = 0
      iflag = 1                         ! Factorize A
!      call c_bridgez(nrmat,ne,nrhs,cvalue,iptrow,indcol,                &
!     &               vecs(1,1),nrmat,info,work,lwork,iflag)
      call zgssv_SLU(nrmat,ne,cvalue,indcol,iptrow,                     &
     &                  vecs(1,1),size(vecs,1),nrhs,factors,info,iflag)

!CDEBUG
       time1 = time2
!DEBUG_TIME      call timel(time2)
!DEBUG_TIME      timePreit = time2-time1
!DEBUG_TIME      if(iprint.ge.0) then
!DEBUG_TIME       write(*,*) sname//' :: LU-DECOMP-time=',real(timePreit)
!DEBUG_TIME      end if
!CDEBUG


      if(info.ne.0) then
        write(6,*) ' IFLAG=',iflag
        write(6,*) ' NE=',ne
        write(6,*) ' NRHS=',nrhs
        write(6,*) ' NRMAT=',nrmat
        write(6,*) ' NDMAT=',ndmat
        write(6,*) ' INFO=',info
        call fstop(sname//' :: INFO.ne.0 after c_bridgez 2')
      end if

      ntime = 0
      iflag = 2

      do j=1,natom*kkrsz,kkrsz

        nsub1 = (j-1)/kkrsz+1
        ns1 = itype(nsub1)

        l1a = mod(j-1,kkrsz)         ! l1a=0:kkrsz-1

        do nsub2=1,nsub1-1
         if(ns1.eq.itype(nsub2)) then            ! copy
          do l1 = 1,kkrsz
           do i=1,kkrsz
            cof(i,l1,nsub1) = cof(i,l1,nsub2)
           end do
          end do
          go to 10
         end if
        end do

        ntime = ntime+1

        call zerooutC(vecs(1,1),nrhs*nrmat)
        do i = 1,nrhs
         vecs(j+i-1,i) = cone
!c                                        VECS(*,i) = 1*Precond[A]
        end do

!        call c_bridgez(nrmat,ne,nrhs,cvalue,iptrow,indcol,              &
!     &               vecs(1,1),nrmat,info,work,lwork,iflag)
      call zgssv_SLU(nrmat,ne,cvalue,indcol,iptrow,                     &
     &                  vecs(1,1),size(vecs,1),nrhs,factors,info,iflag)

        if(info.ne.0) then
         write(6,*) ' IFLAG=',iflag
         write(6,*) ' NE=',ne
         write(6,*) ' NRHS=',nrhs
         write(6,*) ' NRMAT=',nrmat
         write(6,*) ' NDMAT=',ndmat
         write(6,*) ' INFO=',info
         call fstop(sname//' :: INFO.ne.0 after c_bridgez 2')
        end if

        Jns = (nsub1-1)*kkrsz
        do i = 1,nrhs
         l1 = mod((i-1)+l1a,kkrsz) + 1           ! l1=1:kkrsz
         do ii=1,kkrsz
!CDEBUG         sum = czero
!CDEBUG         do jj=1,kkrsz
!CDEBUGc          Jn = (nsub1-1)*kkrsz+jj
!CDEBUG          Jn = Jns +jj
!CDEBUG          sum = sum + t(ii,jj,ns1)*vecs(Jn,i)
!CDEBUG         end do
!CDEBUG         cof(ii,l1,nsub1) = sum

!c          Jn = (nsub1-1)*kkrsz+ii
          Jn = Jns +ii
          cof(ii,l1,nsub1) = vecs(Jn,i)
!CDEBUG
         end do
        end do
10      continue
      end do

!CDEBUG
!DEBUG_TIME      time1 = time2
!DEBUG_TIME      call timel(time2)
!DEBUG_TIME      tsolve = (time2-time1)/ntime
!DEBUG_TIME      tsolve = tsolve*kkrsz/nrhs
!DEBUG_TIME      if(iprint.ge.0) then
!DEBUG_TIME       write(*,*) sname//' :: SPARSEINV-time=',real(tsolve)             &
!DEBUG_TIME     &            ,real(tsolve*natom),real(tsolve*natom+timePreit)
!DEBUG_TIME      end if
!DEBUG_TIME      timeIneq = timeIneq + tsolve*natom+timePreit
!CDEBUG

      iflag = 3
!      call c_bridgez(nrmat,ne,nrhs,cvalue,iptrow,indcol,                &
!     &               vecs(1,1),nrmat,info,work,lwork,iflag)
      call zgssv_SLU(nrmat,ne,cvalue,indcol,iptrow,                     &
     &                  vecs(1,1),size(vecs,1),nrhs,factors,info,iflag)

      if(allocated(work)) deallocate(work)
      deallocate(cvalue,vecs,indcol,iptrow,stat=erralloc)

      if(erralloc.ne.0) then
        write(6,*) ' NE=',ne
        write(6,*) ' ERRALLOC=',erralloc
        call fstop(sname//' :: DEALLOCATION MEMORY PROBLEM')
      end if

      return
      end subroutine gsvlusp

      subroutine cms2ccs(a,kkrsz,nd,natom,nnmap,cvalue,iptcol,indrow,   &
     &                                                         ne,iflag)
      implicit none
!c
!c    "cluster matrix" storage ([1:kkrsz*kkrsz,1:natom]) --> compressed column storage
!c     ("cluster matrix" corresponds to general matrix [1:kkrsz*natom,1:kkrsz*natom])

      complex(8), intent(inout) :: a(kkrsz*kkrsz*nd,natom)  ! (kkrsz*kkrsz,1:nblk), natom<=nblk<=natom*natom
      integer, intent(in) :: kkrsz,nd,natom
      integer, intent(in) :: nnmap(:,:)                   ! list of neighbors for each atom , nnmap(inn,iatom) -> jatom
                                                          !  (nnmap == nnclmn(1,:,:)
      complex(8), intent(out) ::  cvalue(*)
      integer, intent(out) :: indrow(*),iptcol(*)
      integer, intent(inout) :: ne
      integer, intent(in) :: iflag


      character sname*10
      parameter (sname='cms2ccs')

      integer :: i,j,k,n,j_n,l,m,indIJ,iat,jat
      integer :: inn,lmij,ndmat
!      integer, pointer :: nn(:)=>null()  ! number of NN for each atom
      complex(8) :: aij

      ndmat = size(a,1)
      select case (iflag)
      case(1) ! to nullify small elements < 0  and
              ! to calculate number of non-zero elements
       ne=0
       do iat=1,natom
!       i_n = (iat-1)*ndmat
        do l=1,kkrsz
         do inn=1,nd
          indIJ = (l-kkrsz) + (inn-1)*(kkrsz*kkrsz)
          do m = 1,kkrsz
           if ( a(indIJ+m*kkrsz,iat).ne.czero ) ne = ne+1
          end do
         end do
        end do
       end do

      case(2) ! to transform CMS --> CCS
!       cvalue(1:ne) = czero
!       indrow(1:ne) = 0
       n = kkrsz*natom 
       iptcol(1:n+1) = 0
       iptcol(1) = 1
!
       k = 0 ; i = 0
       do iat=1,natom
!       i_n = (iat-1)*ndmat
        do l=1,kkrsz
!c        i = (iat-1)*kkrsz+l
         i = i + 1
         do inn=1,nd
          jat = nnmap(inn,iat)
          j_n = (l-kkrsz) + (inn-1)*(kkrsz*kkrsz)
          do m=1,kkrsz
           j = (jat-1)*kkrsz+m
           lmij = m*kkrsz +j_n
!c                          lmij = (m-1)*kkrsz + l
           aij = a(lmij,iat)
           if(aij.ne.czero) then
            k= k+1
            cvalue(k) = aij
            indrow(k) = j
           end if
          end do
         end do
         iptcol(i+1) = k+1
        end do
       end do
!DEBUGPRINT
!DELETE        iat = 1
!DELETE        write(6,*) ' #',iat
!DELETE        write(6,'(9(2x,f9.4,f9.4))') (a((i-1)*kkrsz+i,iat),i=1,kkrsz)
!DELETE        iat = 4
!DELETE        write(6,*) ' #',iat
!DELETE        write(6,'(9(2x,f9.4,f9.4))') (a((i-1)*kkrsz+i,iat),i=1,kkrsz)
!DELETE        iat = 3
!DELETE        write(6,*) ' #',iat
!DELETE        write(6,'(9(2x,f9.4,f9.4))') (a((i-1)*kkrsz+i,iat),i=1,kkrsz)
!DELETE        iat = 2
!DELETE        write(6,*) ' #',iat
!DELETE        write(6,'(9(2x,f9.4,f9.4))') (a((i-1)*kkrsz+i,iat),i=1,kkrsz)
!DEBUGPRINT
!DELETE      write(6,'(18i5)') iptcol(1:n+1)
!DELETE      call wrt1ccs(natom*kkrsz,iptcol,indrow,cvalue,0.d0)
!DEBUGPRINT
  !
      case default 
       ne = 0
      end select

      return
      end subroutine cms2ccs

      subroutine gcms21mgt(amt,kkrsz,natom,deltat,nnmap,                &
     &                                          zvalue,iptcol,indrow)
      implicit none
      complex(8), intent(inout) :: amt(:,:,:)     ! (kkrsz*kkrsz,nd,natom)
      integer, intent(in) :: kkrsz,natom
      complex(8), intent(in) :: deltat(:,:,:)     !  (ndimt,ndimt,natom)
      integer, intent(in) :: nnmap(:,:)        !  nnclmn(1:1,1:mxclstr,1:natom)
      complex(8), pointer :: zvalue(:)
      integer, pointer :: indrow(:)
      integer, intent(out) :: iptcol(kkrsz*natom+1)

      integer :: iatom,inn,jatom,lm,ndimt,ndmat,nd,ne
      real(8) :: aiimin,tolinv,tol_tol
      complex(8) :: tmpmtr(kkrsz*kkrsz)

      ndmat = kkrsz*kkrsz
      nd = size(amt,2)
      ndimt = size(deltat,1)
      tolinv = invpar_tolinv()
      tol_tol = 2*tolinv**2

      aiimin = huge(aiimin)
      do iatom=1,natom
       do inn=1,nd
        if ( amt(1,inn,iatom) == czero ) then
         cycle
        else
         jatom = nnmap(inn,iatom)
         call zgemm('N','N',kkrsz,kkrsz,kkrsz,cone,                     &
     &           amt(1,inn,iatom),kkrsz,deltat(1,1,jatom),ndimt,czero,  &
     &           tmpmtr,kkrsz)
         if(iatom.eq.jatom) then
          tmpmtr(1:kkrsz*kkrsz:kkrsz+1) = tmpmtr(1:kkrsz*kkrsz:kkrsz+1) &
     &                                  - cone
          aiimin=min(minval(abs(tmpmtr(1:kkrsz*kkrsz:kkrsz+1))),aiimin)
         end if
         amt(1:kkrsz*kkrsz,inn,iatom) = -tmpmtr(1:kkrsz*kkrsz)
        end if
       end do
      end do

!c                  Now, amt --> (1-amt*deltat)

      aiimin = max(aiimin*tolinv,tol_tol)
      ne = 0
      do iatom=1,natom
       do inn=1,nd
        if ( amt(1,inn,iatom) == czero ) then
         cycle
        else
         do lm=1,kkrsz*kkrsz
          if ( lm==1 ) then
              ne = ne+1
              cycle
          end if
          if(abs(amt(lm,inn,iatom)).lt.aiimin) then
           amt(lm,inn,iatom) = czero
          else
           ne = ne+1
          end if
         end do
        end if
       end do
      end do

      allocate(zvalue(1:ne),indrow(1:ne))
!      eps = 0   ! aiimin
      call cms2ccs(amt,kkrsz,nd,natom,nnmap,zvalue,iptcol,indrow,ne,2)
      return
      end subroutine gcms21mgt

      subroutine ge2ccs(a,ndima,n,eps,zvalue,iptcol,indrow,ne,iflag)
      implicit none
!c
!c    general matrix --> compressed column storage
!c
      integer, intent(in) :: ndima,n,iflag
      integer, intent(inout) :: ne
      real(8) :: eps
!c       complex*16 a(ndima,n)
      complex(8) :: a(*)

      integer     indrow(*),iptcol(*)
      complex(8) ::  zvalue(*)

      character sname*10
      parameter (sname='ge2ccs')

!c      integer erralloc
      integer i,j,jn,j0,k
      complex(8) ::  aij
      real(8) :: re_aij,im_aij

      if(iflag.eq.1) then
       ne=0
       do j=1,n
        j0 = (j-1)*ndima
        do i=1,n
         aij = a(i+j0)
         re_aij = dreal(aij)
         im_aij = dimag(aij)
         if ( re_aij<-eps.or.re_aij>eps.or.im_aij<-eps.or.im_aij>eps )  &
     &     then
!!         if(max(abs(dreal(aij)),abs(dimag(aij))).gt.eps) then
          ne=ne+1
!         else
!          a(i+j0) = (0.d0,0.d0)
         end if
        end do
       end do
       return
      else if ( iflag>1 ) then
       iptcol(1) = 1
       do j=1,n
        j0 = (j-1)*ndima
        jn = 0
        k = iptcol(j)-1
        do i=1,n
         aij = a(i+j0)
         re_aij = dreal(aij)
         im_aij = dimag(aij)
         if ( re_aij<-eps.or.re_aij>eps.or.im_aij<-eps.or.im_aij>eps )  &
     &     then
          jn = jn+1
          zvalue(k+jn) = aij
          indrow(k+jn) = i
         end if
        end do
        iptcol(j+1) = iptcol(j) + jn
       end do
       if ( iflag==3 ) then
        k = iptcol(n+1)-1
        write(6,'(a,i5,2(a,i8))') sname//': N = ',n,                    &
     &                                   ' NE(in)=',ne,' NE(k)=',k
        call wrt1ccs(n,iptcol,indrow,zvalue,eps)
       end if
       ne = k
      else
       ne = 0
      end if

      return
      end subroutine ge2ccs

      subroutine g_ge21mgt(amt,kkrsz,natom,deltat,ndimt,zvalue,         &
     &                     iptcol,indrow,ne)
      implicit none
      complex(8), intent(inout) :: amt(*)
      integer, intent(in) :: kkrsz,natom
      integer, intent(in) :: ndimt
      complex(8), intent(in) :: deltat(ndimt,ndimt,natom)
!      integer, intent(in) :: mapnn(natom,natom)    ! nn(:) is list of neighbors with
      complex(8), pointer :: zvalue(:)
      integer, pointer :: indrow(:)
      integer, intent(out) :: iptcol(kkrsz*natom+1)
      integer, intent(out) :: ne

      integer :: j,ndmat,nrmat,p_dummyI(1)
      integer :: itype(natom)
      real(8) :: aiimin,eps,tolinv,tol_tol
      complex(8) :: p_dummyZ(1)

      ndmat = kkrsz*kkrsz
      nrmat = kkrsz*natom
      tolinv = invpar_tolinv()
      tol_tol = 2*tolinv**2

      forall (j=1:natom) itype(j)=j
!DEBUG      call g21mgt(amt,deltat,ndimt,itype,natom,kkrsz,0,1)
      call g21mgt(amt,deltat,ndimt,itype,natom,kkrsz,0,0)

      aiimin = minval(abs( amt(1:nrmat*nrmat:nrmat+1) ))
      eps = max(aiimin*tolinv,tol_tol)

!c                  Now, amt --> (1-amt*deltat)
      call ge2ccs(amt,nrmat,nrmat,eps,p_dummyZ,iptcol,p_dummyI,ne,1)
      allocate(zvalue(1:ne),indrow(1:ne))
      call ge2ccs(amt,nrmat,nrmat,eps,zvalue,iptcol,indrow,ne,2)
!      call ge2ccs(amt,nrmat,nrmat,eps,zvalue,iptcol,indrow,ne,3)
      return
      end subroutine g_ge21mgt
!BOP
!!ROUTINE: wrt1ccs
!!INTERFACE:
      subroutine wrt1ccs(nsize,col_ptr,row_ind,zvalue,eps)
!!DESCRIPTION:
! writes out CCS sparse matrix
!

!!DO_NOT_PRINT
      implicit none
!!DO_NOT_PRINT

!!ARGUMENTS:
      integer,intent(in)  :: nsize
      integer,intent(in)  :: col_ptr(1+nsize)
      integer,intent(in)  :: row_ind(*)
      complex(8), intent(in) :: zvalue(*)
      real(8), intent(in) :: eps
!EOP
!
!BOC
      integer :: j,k
!c
      write(6,*) ' WRT1CCS: NE = ',col_ptr(1+nsize),' eps=',eps
      do j = 1,nsize
        do k=col_ptr(j),col_ptr(j+1)-1
         if ( abs(zvalue(k)) < eps ) cycle
         write(6,'(i6,1x,i6,2d16.7)') j,row_ind(k),zvalue(k)
        end do
      end do
!c
      return
!EOC
      end subroutine wrt1ccs
!
!BOP
!!ROUTINE: wrt2ccs
!!INTERFACE:
      subroutine wrt2ccs(n,x,eps)
!!DESCRIPTION:
! writes out a general matrix {\tt n * n} in CCS format,
! non-zero elements only

!!DO_NOT_PRINT
      implicit none
!!DO_NOT_PRINT

!!ARGUMENTS:
      integer, intent(in) :: n
      complex(8), intent(in) :: x(*)
      real(8), intent(in) :: eps
!!REMARKS:
! "non-zero" means that abs.value >= eps
!EOP
!
!BOC
      integer :: ne
      integer, allocatable :: iptcol(:),indrow(:)
      complex(8), allocatable :: zvalue(:)
!
      allocate(iptcol(1+n))
      allocate(indrow(n*n),zvalue(n*n))
      call ge2ccs(x,n,n,eps,zvalue,iptcol,indrow,ne,1)
      call ge2ccs(x,n,n,eps,zvalue,iptcol,indrow,ne,3)
!      write(6,*) ' WRT2CCS: NE = ',ne
!      call wrt1ccs(n,iptcol,indrow,zvalue,eps)
      deallocate(iptcol,indrow,zvalue)
      return
!EOC
      end subroutine wrt2ccs

end module sparse
