      subroutine crs2gev(value,ne,n,iptrow,indcol,j,vecs,op)
      implicit none
!c
!c   compressed row storage matrix --> general vector
!c
      integer ne,n,j
      complex*16 value(ne)
      integer iptrow(1+n),indcol(ne)
      complex*16 vecs(n)
      character*1 op

      complex*16 czero
      parameter (czero=(0.d0,0.d0))
      integer i,iptr0,iptr1,k
      character*10 sname
      parameter (sname='crs2gev')

      iptr1 = iptrow(1)

      if(op.eq.'+'.or.op.eq.' ') then
       do 10 i=1,n
        iptr0 = iptr1
        iptr1 = iptrow(i+1)-1
        vecs(i) = czero
        do k=iptr0,iptr1
         if(indcol(k).eq.j) then
          vecs(i) = value(k)
          go to 10
         end if
        end do
10       continue
      else if(op.eq.'-') then
       do 20 i=1,n
        iptr0 = iptr1
        iptr1 = iptrow(i+1)
        vecs(i) = czero
        do k=iptr0,iptr1-1
         if(indcol(k).eq.j) then
          vecs(i) = -value(k)
          go to 20
         end if
        end do
20       continue
      else
       write(6,*) ' WRONG OPERATION :: ',op,' ::'
       call fstop(sname//':: wrong operation')
      end if
      return
      end

