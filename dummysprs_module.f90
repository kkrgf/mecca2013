module sparse

implicit none

public :: tauScr_ks
public :: tau_nosprs
public :: inv1sprs
public :: gsvlusp

contains

      subroutine tauScr_ks(lmax,natom,                                  &
     &              aij,                                                &
     &              itype,nstot,                                        &
     &              mapstr,ndimbas,                                     &
     &              kx,ky,kz,                                           &
     &              tcpa,                                               &
     &              ndkkr,deltat,deltinv,                               &
     &              numnbi,jclust,                                      &
     &              mapsnni,ndimnn,                                     &
     &              nnptr,maxclstr,nnclmn,                              &
     &              rsij,                                               &
     &              greenrs,ndgreen,tau00,                              &
     &              istop)
      implicit none
!
      integer, intent(in) :: lmax,natom
      real(8), intent(in) :: aij(3,*)
      integer, intent(in) :: itype(natom),nstot
      integer, intent(in) :: mapstr(ndimbas,natom)
      integer, intent(in) :: ndimbas
      real(8), intent(in) :: kx,ky,kz
      complex(8), intent(in) :: tcpa(ndkkr,ndkkr,nstot)
      integer, intent(in) :: ndkkr
      complex(8), intent(in) :: deltat(ndkkr,ndkkr,natom)
      complex(8), intent(in) :: deltinv(ndkkr,ndkkr,natom)
      integer, intent(in) :: numnbi(nstot)
      integer, intent(in) :: jclust(ndimnn,nstot)
      integer, intent(in) :: mapsnni(ndimnn*ndimnn,nstot)
      integer, intent(in) :: ndimnn
      integer, intent(in) :: nnptr(0:1,natom)
      integer, intent(in) :: maxclstr
      integer, intent(in) :: nnclmn(2,maxclstr,natom)
      real(8), intent(in) :: rsij(ndimnn*ndimnn*3,nstot)
      complex(8), intent(in) :: greenrs(ndgreen,nstot)
      integer, intent(in) :: ndgreen
      complex(8), intent(out) :: tau00(ndkkr,ndkkr,natom)
      character(*), intent(in) :: istop

!c     ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
      character    sname*10
      parameter    (sname='tauScr_ks')
!c     ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
      integer kkrsz
      integer, parameter :: iswitch = 0 ! to choose sparse method techniques (SuperLU - 0, TFQMR - 1, MAR - 2)
      integer :: iat

      integer, external :: indclstr


      complex(8), pointer :: greenKS(:,:,:)    ! (:,:,1) -- diagonal blocks;
                                               ! (:,:2:natom) -- off-diagonal blocks
      complex(8), allocatable :: ztmp(:,:)
      complex(8), pointer :: zvalue(:)
      integer, pointer :: indrow(:)
      integer, pointer :: iptcol(:)

      call  tau_nosprs()

      return
      end subroutine tauScr_ks

!c     cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      subroutine inv1sprs(cvalue,iptcol,indrow,                         &
     &                    kkrsz,natom,deltat,ndimt,                     &
     &                    taudlt,                                       &
     &                    na                                            &
     &                   )
      implicit none
!c
      complex(8), intent(in) :: cvalue(:)
      integer, intent(in) :: iptcol(:)            ! (kkrsz*natom+1)
      integer, intent(in) :: indrow(:)
      integer, intent(in) :: kkrsz,natom
      complex(8), intent(in) :: deltat(ndimt,ndimt,natom)
      integer, intent(in) :: ndimt
      complex(8), intent(inout) :: taudlt(ndimt,ndimt,*)  !  if na>0, size(taudlt,3) = na,
                                                          ! otherwise size(taudlt,3) = 1
      integer, intent(in) :: na
!c
      integer :: j,ne
      integer :: iflag,info,iperm,nrhs,nrmat
      integer :: nsub1
      real(8) :: tolinv
      character(10), parameter :: sname='inv1sprs'

      complex(8), allocatable :: vecs(:,:)

      call  tau_nosprs()

      return
      end subroutine inv1sprs

      subroutine tau_nosprs()
      implicit none
      character    sname*10
      parameter    (sname='tau_nosprs')
!c     ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
      write(*,*) ' SPARSE VERSION OF MECCA IS NOT SUPPORTED',           &
     &           ' IN THIS DISTRIBUTION......'
      call fstop(sname//': sparse version is not supported')
!c----------------------------------------------------------------------- !-
      return
      end subroutine tau_nosprs

      subroutine gsvlusp(a,ndmat,                                       &
     &                   nnptr,nnclmn,ndimnn,                           &
     &                   cof,ndcof,                                     &
     &                   itype,kkrsz,natom,                             &
     &                   iprint)
!c     ================================================================
!c
      implicit none
      integer ndmat,natom,kkrsz
      complex*16 a(ndmat,*)

      integer nnptr(0:1,natom),ndimnn,nnclmn(2,ndimnn,natom)
      integer ndcof
      complex*16 cof(ndcof,ndcof,natom)
      integer    itype(natom)
      integer iprint

      integer erralloc
      complex*16,  allocatable :: cvalue(:)
      complex*16,  allocatable :: vecs(:,:)

      integer, allocatable :: iptrow(:)
      integer, allocatable :: indcol(:)

      integer,  allocatable :: work(:)
      integer(8) :: factors

      character sname*10
      parameter (sname='gsvlusp')

      integer :: i,iatom,iflag,info,inn,ii,indI,indIJ,indJ
      integer :: j,jatom,jn,jns,jnn
      integer :: kelem 
      integer :: l1,l1a,lmi,lmj,lmij
      integer :: ne,nrmat,nrhs,ns1,ntime,nsub1,nsub2
      real(8) :: time1,time2
      complex*16 aij
      integer, save :: isave=0

      call tau_nosprs()

      return
      end subroutine gsvlusp

end module sparse

        subroutine RStau00(                                             &
     &                 lmax,ndkkr,nbasis,                               &
     &                 alat,aij,itype,natom,iorig,                      &
     &                 mapstr,ndimbas,                                  &
     &                 pdu,                                             &
     &                 naij,                                            &
     &                 tcpa,                                            &
     &                 tau00,                                           &
     &                 rslatt,Rnncut,Rsmall,                            &
     &                 r_ref,                                           &
     &                 isprs,                                           &
     &                 iprint,istop)
!c     =================================================================
!c
        use sparse, only : tau_nosprs
        implicit none
        integer, intent(in) :: lmax,ndkkr
        integer, intent(in) :: nbasis
        real(8), intent(in) :: alat
        integer, intent(in) :: naij
        real(8), intent(in) :: aij(3,naij)
        integer, intent(in) :: natom,itype(natom)
        integer, intent(in) :: iorig(nbasis)
        integer, intent(in) :: ndimbas
        integer, intent(in) :: mapstr(ndimbas,natom)
        complex(8), intent(in) :: pdu
        complex(8) :: tcpa(ndkkr,ndkkr,natom)
        complex(8), intent(out) :: tau00(ndkkr,ndkkr,nbasis)
        real(8), intent(in) :: rslatt(3,3)           ! Alat*rslatt(1..3,*)/(2*pi) -- in at.un.
        real(8), intent(in) :: Rnncut,Rsmall         ! Alat*R/(2*pi) -- in at.unit.
        real(8), intent(in) :: r_ref(:)
!        complex(8), intent(out) :: deltat(ndkkr,ndkkr,*)
!        complex(8), intent(out) :: deltinv(ndkkr,ndkkr,*)
        integer, intent(in) :: isprs(:)
        integer, intent(in) :: iprint
        character(10), intent(in) :: istop

      call  tau_nosprs()

        end subroutine RStau00

