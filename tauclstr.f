!BOP
!!ROUTINE: tauclstr
!!INTERFACE:
      subroutine tauclstr(kkrsz,natom,                                  &
     &                 itype,                                           &
     &                 tcpa,deltat,deltinv,ndkkr,                       &
     &                 invswitch,invparam,                              &
     &                 greens,tau00,                                    &
     &                 iflagtau,iprint,istop)
!!DESCRIPTION:
! {\bv
! wrapper to calculate G->([1-G*deltat]^(-1)-1)*deltinv
!
!   AND IF
!
! iflagtau=0:
!     to calculate the block0-diagonal part of
!         Tau (tau00[1:ndkkr,1:ndkkr]) = tcpa+tcpa*G*tcpa
!     for the _first_ atom of the cluster
!
! iflagtau=-1:
!     to calculate the block0-diagonal part of
!         TauDelta (tau00) = deltat+deltat*G*deltat
!     for the _first_ atom of the cluster
!
! iflagtau=1:
!     to calculate (1+G*deltat)-matrix (=TauDelta*deltinv)
!     for the _first_ atom of the cluster
!
!    G -- is defined for cluster,
!    t,deltat,deltinv -- for _all_ inequivalent atoms
!    itype -- contains type of cluster atom
! \ev}

!!DO_NOT_PRINT
      implicit none
!!DO_NOT_PRINT

!!ARGUMENTS:
      integer      kkrsz,natom
      integer      itype(natom)
      integer      ndkkr
      complex*16   tcpa(ndkkr,ndkkr,*)
      complex*16   deltat(ndkkr,ndkkr,*)
      complex*16   deltinv(ndkkr,ndkkr,*)
      integer      invswitch,invparam
      complex*16   greens(*)
      complex*16   tau00(*)
      integer      iflagtau,iprint
      character    istop*10
!!REVISION HISTORY:
! Adapted - A.S. - 2013
!EOP
!
!BOC
      character    sname*10
      parameter    (sname='tauclstr')

!c   deltat,deltinv -- matrix [NDKKRxNDKKR,*]

      call calgreen(                                                    &
     &              greens,                                             &
     &              kkrsz,natom,deltat,deltinv,ndkkr,0,itype,           &
     &              invswitch,invparam,                                 &
     &              iprint,istop)

      if(iflagtau.eq.0) then
       call gr2tauz(                                                    &
     &              greens(1),kkrsz*natom,tcpa,ndkkr,itype,             &
     &              1,kkrsz,tau00)
      else if(iflagtau.eq.-1) then
       call gr2tauz(                                                    &
     &              greens(1),kkrsz*natom,deltat,ndkkr,itype,           &
     &              1,kkrsz,tau00)
      else if(iflagtau.eq.1) then
       call gr2gtp1(                                                    &
     &              greens(1),kkrsz*natom,deltat,ndkkr,itype,           &
     &              natom,kkrsz,tau00)
      end if

!c     ==================================================================

      if (istop.eq.sname) call fstop(sname)
!c

      return
!EOC
      end subroutine tauclstr

