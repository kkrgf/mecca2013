!BOP
!!ROUTINE: besselFun
!!INTERFACE:
      subroutine besselFun (nn,y,bj,bn,dj,dn)
!!DESCRIPTION:
! computes complex spherical bessel functions j and n
! and its derivatives
!
!!REMARKS:
! check accuracy with large orders 
! (or use subroutine bessjy)
!EOP
!
!BOC
      implicit real*8 (a-h,o-z)
!c
      complex*16 aj,x,y
!c
      character sname *10
!c+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
      parameter (sname='besselFun')
!c+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!c
      character coerr*80
!c
      complex*16 bj(nn),bn(nn),dj(nn),dn(nn)
!c
      data coerr/'all right'/
!c
      x=y
!c
      if (abs(x).eq.0.0d0) then
         coerr=' x equals to zero'
       call p_fstop(sname//':'//coerr)
      endif
!c
      bj(1) =  sin(x)/x
      bn(1) = -cos(x)/x
      bj(2) = bj(1)/x+bn(1)
      bn(2) = bn(1)/x -  bj(1)
      dj(1) = -bj(2)
      dj(2) = bj(1)-2.0d+00*bj(2)/x
      dn(1) = -bn(2)
      dn(2) = bn(1)-2.0d+00*bn(2)/x
!c
!c  recursion relations
!c
      do 730 l=3,nn
        ll = l
        flm = ll+ll-3
        fll = ll
        bj(l) = (flm*bj(l-1)/x-bj(l-2))
        bn(l) = flm*bn(l-1)/x-bn(l-2)
        dj(l) = bj(l-1)-fll*bj(l)/x
  730   dn(l) = bn(l-1)-fll*bn(l)/x
!c
      if (abs(x).gt.1.0d+00) then
         return
      endif
!c
!c  power-series for j if abs(x))=.6
!c
      do l=1,nn
        ll=l
        bj(l) = 0.0d+00
        aj = 1.0d+00/x
        k = 0
        do i=1,l
          aj = aj*x/(i+i-1)
        enddo
  600   bj(l) = bj(l)+aj
        k = k+1
        aj = -aj*x*x/2.0d+00/k/(2.0d+00*(l+k)-1.0d+00)
        if (abs(1000000.0d+00*aj).gt.abs(bj(l))) goto 600
        if (l.ne.1) dj(l) = bj(l-1)-ll*bj(l)/x
      enddo
!c
      return
!EOC
      end subroutine besselFun

!
!BOP
!!ROUTINE: jbessel
!!INTERFACE:
      subroutine jbessel (nn,y,bj,dj)
!!DESCRIPTION:
! computes complex spherical bessel function j and its derivative
!
!!REMARKS:
! check accuracy with large orders (or use
! subroutine bessjy)
!EOP
!
!BOC
      implicit real*8 (a-h,o-z)
!c
      complex*16 aj,x,y
!c
      character sname *10
!c+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
      parameter (sname='jbessel')
!c+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!c
      character coerr*80
!c
      complex*16 bj(nn),dj(nn)
!c
      data coerr/'all right'/
!c
      x=y
!c
      if (abs(x).eq.0.0d0) then
         coerr=' x equals to zero'
       call p_fstop(sname//':'//coerr)
      else if(abs(x).gt.1.d0) then
       bj(1) =  sin(x)/x
       bj(2) = bj(1)/x-cos(x)/x
       dj(1) = -bj(2)
       dj(2) = bj(1)-2.d0*bj(2)/x
!c
!c  recursion relations
!c
       do l=3,nn
        ll = l
        flm = ll+ll-3
        fll = ll
        bj(l) = (flm*bj(l-1)/x-bj(l-2))
        dj(l) = bj(l-1)-fll*bj(l)/x
       end do

      else
!c
!c  power-series for j if abs(x))=.6
!c
       do l=1,nn
        ll=l
        bj(l) = 0.0d+00
        aj = 1.0d+00/x
        k = 0
        do 500 i=1,l
  500     aj = aj*x/(i+i-1)
  600   bj(l) = bj(l)+aj
        k = k+1
        aj = -aj*x*x/2.0d+00/k/(2.0d+00*(l+k)-1.0d+00)
        if (abs(1000000.0d+00*aj).gt.abs(bj(l))) goto 600
      if (l.ne.1) dj(l) = bj(l-1)-ll*bj(l)/x
       end do
       dj(1) = -bj(2)

      endif
!c
      return
!EOC
      end subroutine jbessel
