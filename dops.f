!BOP
!!ROUTINE: dops
!!INTERFACE:
      subroutine dops( rot, dop, nrot, lmax )
!!DESCRIPTION:
! calculates transformation matrices {\tt dop} for {\tt nrot} rotations
! defined by matrix {\tt rot}
!

!!DO_NOT_PRINT
      implicit none
!!DO_NOT_PRINT

!!ARGUMENTS:
      real*8 rot(49,*)
      integer nrot
      integer lmax
      complex*16 dop((lmax+1)*(lmax+1)*(lmax+1)*(lmax+1),nrot)
!!REVISION HISTORY:
! Adapted - A.S. - 2013
!!REMARKS:
! number of gauss-legendre qudrature points is equal to 64
!EOP
!
!BOC
!c     ==================================================================
      integer npts
      integer jj
!c      integer jj,ij,ii
      integer jold
      integer istart
      integer iend
      integer kstore
      integer i
      integer j
      integer k
      integer l
      integer ip
      integer kkrsz
!c      integer kk
!c     ==================================================================
!c      real*8 rot( 49,*),doprl,dopim
      real*8 endpts(2)
      real*8 r(3)
      real*8 rp(3)
      real*8 r2
      real*8 r2p
      real*8 x
      real*8 y
      real*8 z
      real*8 xp
      real*8 yp
      real*8 zp
      real*8 pi
      real*8 phi
      real*8 sintheta
      real*8 termi
!c     ==================================================================
      real(8), external :: g_pi

      complex*16, allocatable ::   ylm(:)
      complex*16, allocatable ::   ylmp(:)

      real*8, allocatable ::  xx(:)
      real*8, allocatable ::  w(:)
      real*8, allocatable ::  b(:)

!c      real*8 tmprot

!c      character*10 sname/'dops'/
      real*8 zero,one
      parameter (zero=0.d0,one=1.d0)
!c     ==================================================================
      real(8) :: r1
      real(8), parameter :: sq3d2=dsqrt(3.d0)/dble(2)
      real(8), parameter :: sq2d2=dsqrt(2.d0)/dble(2)
      complex(8) :: dp1
!------------------------------------------------------------------------
      NPTS = 64
      kkrsz=(lmax+1)**2
      pi = g_pi()

      allocate(ylm(1:kkrsz),ylmp(1:kkrsz),                              &
     &         xx(1:npts),w(1:npts),b(1:npts))

!c     ==================================================================
!c     Generate the gaussian integration pts and weights for Legendre
!c     polynomials. Gaussq passed back points between -1,1 (cos( theta ))
!c     ==================================================================
      endpts(1) = -one
      endpts(2) =  one
      call gaussq( 1, npts, 0, endpts, b, xx, w )
!c     ==================================================================
!c     Zero out coefficient of transformation array
!c     ==================================================================
      dop = 0

      r(1) = zero
      r(2) = zero
      r(3) = one
      r2 = one
!cab      call ylm_harm( r, r2, ylm, lmax, 0 )
      call hrmplnm(zero,zero,zero,r,1,r2,ylm,1,0,1,lmax,1)

!c     ==================================================================
!c     Loop over the rotations
!c     ==================================================================
      do i = 1,nrot
!c     ==================================================================
!c     To obtain the coefficient of the rotation ( dop ) we must
!c     integrate cub_harm(xp,yp,zp)*cub_harm(x,y,z) over a sphere to
!c     obtain the cofficent of the expansion of cub_harm(xp,yp,zp) in
!c     terms of cub_harm(x,y,z)
!c     ==================================================================
        jold = -1
!c     ==================================================================
        do k = 1,kkrsz
!c     ==================================================================
         termi = sqrt( dble( k - 1 ) )
         jj = int( termi )
!c     ==================================================================
         if( jj .ne. jold ) then
           istart = k
           iend = k + 2*jj
           jold = jj
         endif
!c     ==================================================================
!c     Integrate over d(-costheta)
!c     ==================================================================
          do j = 1,npts
!c     ==================================================================
            sintheta = sqrt( max(zero, one - xx(j)**2) )
!c     ==================================================================
!c     Integrate over phi
!c     ==================================================================
            do ip = 1,npts
!c     ==================================================================
              phi = pi*( xx(ip) + one )
!c     ==================================================================
              x = sintheta*cos( phi )
              y = sintheta*sin( phi )
              z = xx(j)
              r(1) = x
              r(2) = y
              r(3) = z
!cab              r2 = x*x + y*y + z*z
!c     ==================================================================
!c     Perform rotation on x,y,z
!c     ==================================================================
              xp = rot( i,1 )*x + rot( i,4 )*y + rot( i,7 )*z
              yp = rot( i,2 )*x + rot( i,5 )*y + rot( i,8 )*z
              zp = rot( i,3 )*x + rot( i,6 )*y + rot( i,9 )*z
              rp(1) = xp
              rp(2) = yp
              rp(3) = zp
!cab              r2p = xp*xp + yp*yp + zp*zp
!c     ==================================================================
!c     Call Complex Spherical Harmonic for both x,y,z and xp,yp,zp
!c     ==================================================================
!cab              call ylm_harm( r, r2, ylm, lmax, 1 )
!cab              call ylm_harm( rp, r2p, ylmp, lmax, 1 )
           call hrmplnm(zero,zero,zero,r,1,r2,ylm,1,-10,1,lmax,1)
           call hrmplnm(zero,zero,zero,rp,1,r2p,ylmp,1,-10,1,lmax,1)
!c     ==================================================================

              do l = istart,iend
                kstore = ( l - 1 )*kkrsz + k
                dop(kstore,i) = dop(kstore,i)                           &
     &                        + ylmp(k)*conjg( ylm(l) )*w(j)*w(ip)*pi
              enddo
!c     ==================================================================
            enddo                        ! end do loop over phi
!c     ==================================================================
          enddo                          ! end do loop over cos( theta )
!c     ==================================================================
        enddo                            ! end do loop over kkrsz
!c     ==================================================================

!c     ==================================================================
      enddo                              ! end do loop over rotations
!c     ==================================================================
      do i=1,nrot
       do k=1,(lmax+1)**4
        if ( abs(dop(k,i)) < 1.d-10 ) then
         dop(k,i) = dcmplx(zero,zero)
        else
         dp1 = dop(k,i)
         do l=1,2
          if ( l==1 ) then
           r1=real(dp1)
          else
           r1=aimag(dp1)
          end if
          if ( abs(r1-0.5d0) < 1.d-10 ) then
           r1 = 0.5d0
          else if ( abs(r1-sq3d2) < 1.d-10 ) then
           r1=sq3d2
          else if ( abs(r1-sq2d2) < 1.d-10 ) then
           r1=sq2d2
          end if
          if ( l==1 ) then
           r2=r1
          else
           dop(k,i)=dcmplx(r2,r1)
          end if
         end do
        end if
       end do
      end do

      deallocate(ylm,ylmp,xx,w,b)

      return
!EOC
      end subroutine dops
