!BOP
!!ROUTINE: hrmplnm
!!INTERFACE:
      subroutine hrmplnm( x, y, z, rpts, ndimr, r2pts, ylm, ndimy,      &
     &                     iset, n_vecs, lmax , istrg)
!!DESCRIPTION:
! calculates {\tt ylm}, i.e. either spherical harmonics $ Y_{lm} $
! (if iset=-10) or harmonical polynomials $ r^l Y_{lm} $;
! storage format for {\tt rpts} is defined by {\tt istrg} (it is 
! rpts(3,n\_vecs) if istrg<0, otherwise it is rpts(ndimr,3) )
!
!!USES:
      use universal_const
!EOP
!
!BOC
      implicit none
      real(8), intent(in) :: x,y,z
      real(8), intent(in) :: rpts(*)
      integer, intent(in) :: ndimr
      integer, intent(in) :: n_vecs
      real(8), intent(out) :: r2pts(n_vecs)
      integer, intent(in) :: ndimy
      complex(8), intent(out) :: ylm(ndimy,*)
      integer, intent(in) :: iset
      integer, intent(in) :: lmax
      integer, intent(in) :: istrg
!c
      integer    i,i1,i2,istep,iix,iiy,iiz
      integer    m
      integer    l
      integer    lm
!c
      real*8     p(0:2*lmax+1,0:2*lmax+1)
      real*8     xx
      real*8     yy
      real*8     zz
      real*8     vrho
      real*8     rmod,rl
      real*8     fpi
      real*8     cth
      real*8     sth
      real*8     fac
      real*8     a
      real*8     t
      real*8     cd
      real*8     sgm
      real*8     tol
      parameter(tol=1.0d-08)
!c
      real*8 cosphase(0:2*lmax)
      real*8 sinphase(0:2*lmax)
!      complex*16 cone
!      parameter(cone=(1.0d0,0.0d0))
!      real*8 zero,one
!      parameter (zero=0.d0,one=1.d0)

      parameter (fpi = 4.d0*pi)

      real*8 cosylm,sinylm,cosph0,sinph0
      real(8), allocatable, save :: al(:),cdl(:,:)

      integer, save :: l0 = 0

      if(l0<2*lmax+1) then
       if ( allocated(al) ) deallocate(al)
       if ( allocated(cdl) ) deallocate(cdl)
      end if    

      if( .not.allocated(al) ) then
       allocate(al(0:2*lmax))
       allocate(cdl(2*lmax,0:2*lmax))
       do l = 0,2*lmax
        al(l)  = sqrt( ( 2*l + 1 )/fpi )
       end do
       do l = 0,2*lmax
        a = al(l)
        cd = one
        do m = 1,l
          t = ( (l + 1) - m )*( l + m )
          cd = cd/t
          cdl(m,l) = sqrt(cd)*a
        end do
       end do
       l0 = 2*lmax+1
      end if
!c
!c     Calculate Complex Spher. Harms for "n_vecs" vectors [r(n_vecs,i)]  !:.
!c
        if(istrg.ge.0) then    ! rpts[1:ndimr,1:3]
         i1 = 1
         i2 = 0
         istep = ndimr
        else                   ! rpts[1:3,1:n_vecs]
         i1 = 3
         i2 = -2
         istep = 1
        end if

      do i = 1,n_vecs
!c
!c     Calculate sin(theta); cos(theta) : ..........................
!c
        iix = i1*i+i2
        iiy = iix+istep
        iiz = iiy+istep
        xx = x + rpts(iix)
        yy = y + rpts(iiy)
        zz = z + rpts(iiz)

        vrho = xx*xx + yy*yy
        rmod = vrho + zz*zz
        r2pts(i) = rmod
        rmod = sqrt( rmod )
        vrho = sqrt( vrho )
!c
        if( rmod .le. tol ) then
          cth = one
          sth = zero
        else
          cth = zz/rmod
          sth = sqrt(max(zero,one - cth*cth))
        endif
!c
!c                                iset=-10 --> calculation of Ylm only
        if(iset.eq.-10) rmod = one
!c
        if( vrho .lt. tol ) then
          cosphase(0:lmax) = one
          sinphase(0:lmax) = zero
        else

!c
         vrho = one/vrho
         xx = xx*vrho
         yy = yy*vrho

          cosph0 = one
          sinph0 = zero

          do m = 1,lmax
!cab            phase(m)=phase(m-1)*dcmplx( xx, yy )
           cosphase(m) = cosph0*xx-sinph0*yy
           sinphase(m) = sinph0*xx+cosph0*yy
           cosph0 = cosphase(m)
           sinph0 = sinphase(m)
          enddo
!c
        endif
!c
!c     Generate associated legendre functions for m > 0 :...........
!c
        fac = one
!c
        do m = 0,lmax
          fac = -( m + m - 1 )*fac
          p(m,m) = fac
          p(m+1,m) = ( (m + m -1) + 2 )*cth*fac
!c
!c     Recurse upward in l :.....................................
!c
          do l = m+2,lmax
            p(l,m) = ( ( l + (l - 1) )*cth*p(l-1,m) - ( m + (l - 1) )   &
     &             *p(l-2,m) )/( l - m )
          enddo                                 ! end do loop over l
!c
          fac = fac*sth
!c
        enddo                                   ! end do loop over m
!c
!c     Calculate the complex spherical harmonics :..................
!c
        rl = one
        do l = 0,lmax
!c
!c          al(l)  = sqrt( ( 2*l + 1 )/fpi )
!c
          lm = l*( l + 1 ) + 1
          ylm(i,lm) = al(l)*p(l,0)*rl
          sgm = -one
!c
          do m = 1,l

            sinylm = sinphase(m)*(rl*cdl(m,l)*p(l,m))
            cosylm = cosphase(m)*(rl*cdl(m,l)*p(l,m))

!cab            ylm(i,lm+m) = (rl*cdl(m,l)*p(l,m))*phase(m)
            ylm(i,lm+m) = dcmplx(cosylm,sinylm)
            ylm(i,lm-m) = sgm*conjg(ylm(i,lm+m))
!c***            ylm(i,lm-m) = dcmplx(cosylm*sgm,-sinylm*sgm)

            sgm = -sgm
!c
          enddo                            ! end do loop over m
!c

          rl = rmod*rl

        enddo                              ! end do loop over l
!c
      enddo                                ! end do loop over vectors

!c
      return
!EOC
      end subroutine hrmplnm

!BOP
!!ROUTINE: rlylm
!!INTERFACE:
      subroutine rlylm(rsn,nrsn,ndimr,lmax,hplnm,ndimrhp)
!!DESCRIPTION:
! calculates real-space harmonical polinomials $ R^l Y_{lm}(R), m \leq 0 $
! {\bv
!    inputs: rsn,nrsn,lmax
!            (ndimrhp,ndimr -- dimensions)
!    output: hplnm
! \ev}

!!USES:
      use universal_const
!EOP
!
!BOC
      implicit none

      integer nrsn,ndimr,lmax,ndimrhp
      real*8     rsn(ndimr,3)
      complex*16 hplnm(ndimrhp,*)

      complex*16 ylmrl((2*lmax+1)*(2*lmax+1))
      real*8 rns(3)
!      real*8 zero
!      parameter (zero=0.d0)
      real*8     rns2(1)
      integer i,lm,l,m,l0

      call hrmplnm(zero,zero,zero,rns,1,rns2,ylmrl,1,                   &
     &                   0, 0, 2*lmax , 1)

      do i = 1,min(nrsn,ndimrhp)
        rns(1:3) = rsn(i,1:3)
        call hrmplnm(zero,zero,zero,rns,1,rns2,ylmrl,1,                 &
     &                   1, 1, 2*lmax , 1)
        lm = 0
        do l=0,2*lmax
            l0 = l*( l + 1 ) + 1
!c
!c            store only for non-negative M
!c
            do m=0,l
             lm = lm+1
             hplnm(i,lm) = ylmrl(l0+m)
            end do
        end do
      end do

      return
!EOC
      end subroutine rlylm

