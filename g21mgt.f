!BOP
!!ROUTINE: g21mgt
!!INTERFACE:
      subroutine g21mgt(g,t,ndimt,itype,                                &
     &                   natom,kkrsz,idiag)
!!DESCRIPTION:
! {\bv
! input: g=G ,  order KKRSZ*NATOM
!     if idiag.eq.1 then t -- diagonal of block-matrix, order KKRSZ
!     if idiag.ne.1 then t -- block-matrix, order KKRSZ
!
! output: g=(1-G*t)
! \ev}

!!DO_NOT_PRINT
      implicit none
!!DO_NOT_PRINT

!!ARGUMENTS:
      complex*16 g(*),t(*)
      integer    ndimt
      integer    itype(*),natom,kkrsz
      integer    idiag
!!REVISION HISTORY:
! Adapted - A.S. - 2013
!EOP
!
!BOC
      character*10 :: sname = 'g21mgt'
      integer    nrmat
      complex(8), parameter :: czero=(0.d0,0.d0)
      complex(8), parameter ::  cone=(1.d0,0.d0)

      nrmat = natom*kkrsz
       if(idiag.eq.1) then
        call g21mgtzd(g,nrmat,t,ndimt,itype,                            &
     &                   natom,kkrsz)
       else
        call g21mgtz(g,nrmat,t,ndimt,itype,                             &
     &                   natom,kkrsz)
       end if
      return
!EOC
      contains

!BOP
!!IROUTINE: g21mgtzd
!!INTERFACE:
      subroutine g21mgtzd(g,nrmat,t,ndimt,itype,natom,kkrsz)
!
!!REMARKS:
! private procedure of subroutine g21mgt
!EOP
!
!BOC
      implicit none

      integer nrmat,ndimt,natom,kkrsz
      complex*16 g(nrmat*nrmat)
      complex*16 t(ndimt,*)
      integer    itype(natom)

!      integer lkkrmax
!      parameter (lkkrmax=ipkkr)

      integer nsub,ns,i1,i2,i2i1,i,j,Ii1,IiJj,IiIi,i0,j0

      complex*16 a(kkrsz,kkrsz)
!      complex*16 czero,cone
!      parameter (czero=(0.d0,0.d0))
!      parameter (cone=(1.d0,0.d0))
!c     ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
      character    sname*10
      parameter    (sname='g21mgtz')
!c     ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

!c
!c  diagonal (I,I)-block
!c
      do nsub = 1,natom

       ns = itype(nsub)
       i1 = (nsub-1)*kkrsz
       i2i1 = (i1-1)*nrmat+i1

!c
       call cpblkz(i1,i1,g,nrmat,kkrsz,kkrsz,a,kkrsz)
!c
!c                                     A = transposed-G = G'

       do i=1,kkrsz
         Ii1 = i2i1+i
         do j=1,kkrsz

!c           Ii = i1+i
!c           Jj = i2+j  ,  i2=i1
!c           IiJj=(Jj-1)*nrmat+Ii          ! (Ii,Ij)

           IiJj=Ii1 + j*nrmat             ! (Ii,Ij)
           g(IiJj) = -a(j,i)*t(j,ns)      ! = -G*t
         end do
         IiIi = Ii1 + i*nrmat
         g(IiIi) = cone + g(IiIi)      ! = 1-G*t
       end do

      end do
!c
!c  Off diagonal (I,J)-block
!c
      do i0 = 1,natom
      do j0 = 1,natom
      if(i0.ne.j0) then

       i1 = (i0-1)*kkrsz
       i2 = (j0-1)*kkrsz
       ns = itype(j0)
       i2i1 = (i2-1)*nrmat+i1
       call cpblkz(i1,i2,g,nrmat,kkrsz,kkrsz,a,kkrsz)

!c                                     A = G'

       do i=1,kkrsz
         Ii1 = i2i1+i
         do j=1,kkrsz

!c           Ii = i1+i
!c           Jj = i2+j
!c           IiJj=(Jj-1)*nrmat+Ii          ! (Ii,Jj)

           IiJj=Ii1 + j*nrmat             ! (Ii,Ij)
           g(IiJj) = -a(j,i)*t(j,ns)    ! = -G_IJ*t_JJ (matrix in L-space)
         end do
       end do

      end if
      end do
      end do

      return
!EOC
      end subroutine g21mgtzd

!BOP
!!IROUTINE: g21mgtz
!!INTERFACE:
      subroutine g21mgtz(g,nrmat,t,ndimt,itype,natom,kkrsz)
!
!!REMARKS:
! private procedure of subroutine g21mgt
!EOP
!
!BOC
      implicit none
!c
!c  input: g=G
!c
!c  output: g=(1-G*t)
!c
!      include 'lmax.h'
      integer nrmat,ndimt,natom,kkrsz
      complex*16 g(nrmat*nrmat)
      complex*16 t(ndimt,ndimt,*)
      integer    itype(natom)

!      integer lkkrmax
!      parameter (lkkrmax=ipkkr)

      integer nsub,ns,i1,i2,i2i1,i,j,k,Ii1,IiJj,IiIi,i0,j0

      complex*16 a(kkrsz,kkrsz)
      complex*16 sum
!      complex*16 czero,cone
!      parameter (czero=(0.d0,0.d0))
!      parameter (cone=(1.d0,0.d0))
!c     ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
      character    sname*10
      parameter    (sname='g21mgt')
!c     ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

!c
!c  diagonal (I,I)-block
!c
      do nsub = 1,natom

       ns = itype(nsub)
       i1 = (nsub-1)*kkrsz
       i2i1 = (i1-1)*nrmat+i1

!c
       call cpblkz(i1,i1,g,nrmat,kkrsz,kkrsz,a,kkrsz)
!c
!c                                     A = transposed-G = G'

       do i=1,kkrsz
         Ii1 = i2i1+i
         do j=1,kkrsz
           sum = czero
           do k=1,kkrsz
            sum = sum+a(k,i)*t(k,j,ns)
!c                                 ! sum of G(Ii,Ik)*t(Ik,Ij)
!c
!c                                                  sum = G*t
!c
           end do

!c           Ii = i1+i
!c           Jj = i2+j  ,  i2=i1
!c           IiJj=(Jj-1)*nrmat+Ii          ! (Ii,Ij)

           IiJj=Ii1 + j*nrmat             ! (Ii,Ij)
           g(IiJj) = -sum                  ! = -G*t
         end do
         IiIi = Ii1 + i*nrmat
         g(IiIi) = cone + g(IiIi)      ! = 1-G*t
       end do

      end do
!c
!c  Off diagonal (I,J)-block
!c
      do i0 = 1,natom
      do j0 = 1,natom
      if(i0.ne.j0) then

       i1 = (i0-1)*kkrsz
       i2 = (j0-1)*kkrsz
       ns = itype(j0)
       i2i1 = (i2-1)*nrmat+i1
       call cpblkz(i1,i2,g,nrmat,kkrsz,kkrsz,a,kkrsz)

!c                                     A = G'

       do i=1,kkrsz
         Ii1 = i2i1+i
         do j=1,kkrsz
           sum = czero
           do k=1,kkrsz
            sum = sum+a(k,i)*t(k,j,ns)    ! sum of G(Ii,Jk)*t(Jk,Jj)
           end do
!c           Ii = i1+i
!c           Jj = i2+j
!c           IiJj=(Jj-1)*nrmat+Ii           ! (Ii,Jj)

           IiJj=Ii1 + j*nrmat             ! (Ii,Ij)
           g(IiJj) = -sum                 ! = -G_IJ*t_JJ (matrix in L-space)
         end do
       end do

      end if
      end do
      end do

      return
!EOC
      end subroutine g21mgtz

      end subroutine g21mgt

!CAB      subroutine cpclmnz(g,ndmat,gj0,n1)
!CAB      implicit none
!CAB      integer ndmat,n1
!CAB      complex*16 g(ndmat,*),gj0(ndmat,*)
!CAB      integer i,j
!CAB      do i=1,n1
!CAB       do j=1,ndmat
!CAB        gj0(j,i) = g(j,i)
!CAB       end do
!CAB      end do
!CAB      return
!CAB      end

!BOP
!!ROUTINE: cpblkz
!!INTERFACE:
      subroutine cpblkz(m1,m2,g,ndimg,n1,n2,a,ndima)
!!DESCRIPTION:
! copies transposed block of complex matrix {\tt g} to matrix {\tt a}
!

!!DO_NOT_PRINT
      implicit none
!!DO_NOT_PRINT

!!ARGUMENTS:
      integer m1,m2,ndimg    ! m1,m2 are "coordinates" of the block
      complex*16 g(ndimg,*)
      integer n1,n2,ndima    ! n1,n2 are the block sizes
      complex*16 a(ndima,*)
!!REVISION HISTORY:
! Adapted - A.S. - 2013
!EOP
!
!BOC
      integer i,j

      do i=1,n2
       do j=1,n1
        a(j,i) = g(m1+i,m2+j)           ! A = tranposed-G
       end do
      end do

      return
      end subroutine cpblkz

