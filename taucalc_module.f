!BOP
!!MODULE: taucalc
!!INTERFACE:
      module taucalc
!!DESCRIPTION:
! to solve multi-scattering (in general, with CPA) problem
!
!!USES:
      use mecca_constants
      use mecca_types
      use mecca_run
      use mpi

!!DO_NOT_PRINT
      private
!!DO_NOT_PRINT

!!PUBLIC MEMBER FUNCTIONS:
      public :: runTauCalc,calcTcpa,calcTaumain,dumpTauSet,calcTauEk
!!PRIVATE MEMBER FUNCTIONS:
! subroutine ???
!!PUBLIC DATA MEMBERS:
!      public :: ???
!!REVISION HISTORY:
! Initial Version - A.S. - 2017
!EOP
!
!BOC
!      real(8), parameter :: m_e = one/two   ! electron (rest) mass
!      integer, parameter :: ???
!      integer, save :: ??
!      real(8), parameter :: ??
!      complex(8), parameter :: ??
!      real(8), external :: ??

!EOC
      contains
!BOP
!!IROUTINE: runTauCalc
!!INTERFACE:
      subroutine runTauCalc( mecca_state )
!!DESCRIPTION:
!  calculation of tau-matrices diagonal blocks
!
!!ARGUMENTS:
      type(RunState), target :: mecca_state
!!REVISION HISTORY:
! Initial Version - A.S. - 2016
!EOP
!
!BOC
      type(Tau_set), pointer :: allTau=>null()

      integer :: myrank,ierr
      character(*), parameter :: tau_file='TAU.OUT'
      integer :: io = 11

      call mpi_comm_rank(mpi_comm_world,myrank,ierr)

       if ( associated(mecca_state%allTau) )  then
        allTau => mecca_state%allTau
        mecca_state%fail = .false.
        mecca_state%info = -1
        if ( .not.associated(mecca_state%intfile) ) return
        mecca_state%info = mecca_state%info-1
        if ( .not.associated(mecca_state%rs_box) ) return
        mecca_state%info = mecca_state%info-1
        if ( .not.associated(mecca_state%ks_box) ) return
        mecca_state%info = mecca_state%info-1
        if ( .not.associated(mecca_state%ew_box) ) return
        mecca_state%info = 0

        if ( allTau%nset == 0 ) then
         write(6,*) ' WARNING: allTau%nset=0'
         return
        end if
        if ( size(allTau%set,1) < allTau%nset ) then
         write(6,*) ' WARNING: allTau%nset .NE. size(allTau%set,1)'
         write(6,*) '          allTau%nset = ',allTau%nset
         write(6,*) '   size(allTau%set,1) = ',size(allTau%set,1)
         return
        end if
!DEBUGPRINT
        write(6,*)
        write(6,*) ' ......runTauCalc STARTED.......'
        write(6,*)
!DEBUGPRINT

        call setMS( mecca_state )

        mecca_state%fail = .true.

        call calcTcpa( mecca_state )
!
        call calcTaumain( mecca_state )

!DEBUGPRINT
        if ( myrank==0 ) then
         if ( io>0 ) then
          open(io,file=tau_file)
          call dumpTauset(allTau,io)
          close(io)
         end if
        end if
!DEBUGPRINT

        if ( mecca_state%info == 0 ) then
          mecca_state%fail = .false.
        end if

       else
        mecca_state%fail = .false.
        mecca_state%info = 0
       end if

      call mpi_barrier(MPI_COMM_WORLD,ierr)

      return
!EOC
      end subroutine runTauCalc

      subroutine dumpTauset(allTau,io)
      use mecca_types, only : Tau_set
      implicit none
      type(Tau_set), intent(in) :: allTau
      integer, intent(in) :: io

      integer :: i,j
      integer :: iat,ic,is,isub,ik,l1,l2

      write(6,*) ' DUMPING in ',io
      write(io,'(1x,i5,a)') allTau%nset,' # NSET'


      do i=1,allTau%nset
      do is=1,size(allTau%set,2)
         if ( is>1 ) write(io,'(''##  SPIN '',i2)') is
       write(io,'(1x,2e24.16,a)') allTau%set(i,is)%energy,' # ENERGY'
       write(io,'(1x,i5,a)')                                            &
     &                  size(allTau%set(i,is)%kpoints,2),' # No.K-PNTS'
       do j=1,size(allTau%set(i,is)%kpoints,2)
        write(io,'(6x,3e24.16)') allTau%set(i,is)%kpoints(1:3,j)
       end do

       if ( associated(allTau%set(i,is)%tab) ) then
        write(io,'(1x,4i5,a)') size(allTau%set(i,is)%tab,1),            &
     &                         size(allTau%set(i,is)%tab,2),            &
     &       size(allTau%set(i,is)%tab,3),size(allTau%set(i,is)%tab,4), &
     &   ' # dim(TAB); diagonal elements only'
         do isub=1,size(allTau%set(i,is)%tab,4)
          write(io,'(4x,i2,2x,i7,a)') allTau%set(i,is)%ncomp(isub),isub,&
     &                                ' # num of compon, sublat. No.'
          do ic=1,allTau%set(i,is)%ncomp(isub)
           write(io,'(4x,i2,a)') ic,' # compon. No., tab'
           write(io,'((2x,2e24.16))')                                   &
     &                           (allTau%set(i,is)%tab(l1,l1,ic,isub),  &
     &                         l1=1,size(allTau%set(i,is)%tab,1))
!           end do
          end do
         end do
       end if

       if ( associated(allTau%set(i,is)%tcpa) ) then
        write(io,'(1x,3i5,a)') size(allTau%set(i,is)%tcpa,1),           &
     &              size(allTau%set(i,is)%tcpa,2),                      &
     &              size(allTau%set(i,is)%tcpa,3),                      &
     &             ' # dim(TCPA)'
         do isub=1,size(allTau%set(i,is)%tcpa,3)
          write(io,'(4x,i7,a)') isub,' # sublat. No.'
          write(io,'((2x,2e24.16,'' #tcpa''))')                         &
     &                          (allTau%set(i,is)%tcpa(l1,l1,isub),     &
     &                       l1=1,size(allTau%set(i,is)%tcpa,1))
          do l2=1,size(allTau%set(i,is)%tcpa,2)
           do l1=1,size(allTau%set(i,is)%tcpa,1)
            if ( l1.ne.l2 .and.                                         &
     &      abs(allTau%set(i,is)%tcpa(l1,l2,isub)) <                    &
     &             1.d-12*abs(allTau%set(i,is)%tcpa(l2,l2,isub)) )      &
     &          allTau%set(i,is)%tcpa(l1,l2,isub)=(0.d0,0.d0)
           end do
          end do
          do l2=1,size(allTau%set(i,is)%tcpa,2)
           do l1=1,size(allTau%set(i,is)%tcpa,1)
             if (l1>=l2) cycle
             if (abs(allTau%set(i,is)%tcpa(l1,l2,isub))>0.d0 .or.       &
     &           abs(allTau%set(i,is)%tcpa(l2,l1,isub))>0.d0) then
             write(io,'(2x,2i4,2(2x,e23.16,1x,e23.16))')                &
     &               l1,l2,allTau%set(i,is)%tcpa(l1,l2,isub),           &
     &                     allTau%set(i,is)%tcpa(l2,l1,isub)
             end if
           end do
          end do
         end do
       end if
       if ( associated(allTau%set(i,is)%tau_k) ) then
        write(io,*) size(allTau%set(i,is)%tau_k,1),                     &
     &              size(allTau%set(i,is)%tau_k,2),                     &
     &              size(allTau%set(i,is)%tau_k,3),                     &
     &              size(allTau%set(i,is)%tau_k,4),' # dim(TAU_K)'
         do ik=1,size(allTau%set(i,is)%tau_k,4)
          write(io,'(2x,i7,a)') ik,' # k-point No.'
          do iat=1,size(allTau%set(i,is)%tau_k,3)
           write(io,'(2x,i7,a)') iat,' # site No.'
           do l2=1,size(allTau%set(i,is)%tau_k,2)
            write(io,'(2x,i4,(2x,e23.16,1x,e23.16))')                   &
     &                         l2,allTau%set(i,is)%tau_k(l2,l2,iat,ik)
           end do
           do l2=1,size(allTau%set(i,is)%tau_k,2)
            do l1=1,size(allTau%set(i,is)%tau_k,1)
             if ( l1.ne.l2 .and.                                        &
     &      abs(allTau%set(i,is)%tau_k(l1,l2,iat,ik)) <                 &
     &             1.d-12*abs(allTau%set(i,is)%tau_k(l2,l2,iat,ik)) )   &
     &          allTau%set(i,is)%tau_k(l1,l2,iat,ik)=(0.d0,0.d0)
            end do
           end do
           do l2=1,size(allTau%set(i,is)%tau_k,2)
            do l1=1,size(allTau%set(i,is)%tau_k,1)
             if (l1>=l2) cycle
             if (abs(allTau%set(i,is)%tau_k(l1,l2,iat,ik))>0.d0 .or.    &
     &           abs(allTau%set(i,is)%tau_k(l2,l1,iat,ik))>0.d0) then
             write(io,'(2x,2i4,2(2x,e23.16,1x,e23.16))')                &
     &               l1,l2,allTau%set(i,is)%tau_k(l1,l2,iat,ik),        &
     &                     allTau%set(i,is)%tau_k(l2,l1,iat,ik)
             end if
            end do
           end do
          end do
         end do
       end if
       if ( associated(allTau%set(i,is)%logdet) ) then
        write(io,*) size(allTau%set(i,is)%logdet,1),' # dim(LOG_TAU)'
         do ik=1,size(allTau%set(i,is)%logdet,1)
          write(io,'(2x,i6,2x,e23.16,1x,e23.16)')                       &
     &               ik,allTau%set(i,is)%logdet(ik)
         end do
       end if
      end do    ! end spin-cycle
      end do


      return
      end subroutine dumpTauset
!
!BOP
!!IROUTINE: calcTaumain
!!INTERFACE:
      subroutine calcTaumain( mecca_state )
!!DESCRIPTION:
! this subroutine is a main procedure to calculate Tau-matrix
!
!!USES:
      use mecca_constants
      use mecca_types
      use mpi

!!DO_NOT_PRINT
      implicit none

      type :: task_id
       integer :: ik
       integer :: ie
       integer :: is
      end type task_id

!DELETE      interface
!DELETE      subroutine calcTauEk (iflag,mecca_state,ispin,lmax,tau_box,j_kpt)
!DELETE       use mecca_constants
!DELETE       use mecca_types
!DELETE       implicit none
!DELETE       integer, intent(in) :: iflag
!DELETE       type(RunState), target :: mecca_state
!DELETE       integer, intent(in) :: ispin
!DELETE       integer, intent(in) :: lmax
!DELETE       type ( Tau_data ), intent(inout) :: tau_box   !  block-diagonal elements only
!DELETE       integer, intent(in) :: j_kpt
!DELETE      end subroutine calcTauEk
!DELETE      end interface

!!DO_NOT_PRINT

!!ARGUMENTS:
! control parameter to choose calculational method
      type(RunState), target :: mecca_state
!!REVISION HISTORY:
! Initial Version - A.S. - 2016
!EOP
!
!BOC

      type(task_id), allocatable :: taskID(:)
      character(*), parameter :: sname='calcTaumain'

      type(Tau_set), pointer :: allTau=>null()
      type(IniFile),   pointer :: inFile=>null()
      type(Tau_data), pointer :: tau_box(:,:)=>null()
      type(Tau_data), target :: tau0box
      type (MPI_data),  target :: mpi0box
!      type(Sublattice), dimension(mecca_state%intfile%nsubl) :: sublat
!      integer, dimension(mecca_state%intfile%nsubl) :: numbsub

      integer :: iflag
      integer :: lmax,kkrsz
      integer :: i,iprint,nspin,nume,natom
      integer, allocatable :: edu_indx(:)
!      real(8) :: vshift(2),egrd_min

      ! MPI related variables
      logical :: mecca_mpi
      integer :: comm_ne
      integer :: ierr, myrank, nproc, num_jobs
      integer :: ie, is, j_kpt, nK, job, irqst
      integer :: ie0,is0,j_kpt0, nsend, nrecv
      integer, parameter :: tag1=101,tag2=102
      integer, allocatable :: n_kpts(:)

      ! get rank and number of processes
      call mpi_comm_rank(mpi_comm_world,myrank,ierr)
      call mpi_comm_size(mpi_comm_world,nproc,ierr)
      comm_ne = mpi_comm_world

!DEBUGPRINT
        write(6,*)
        write(6,*) ' ......'//sname//' STARTED.......'
        write(6,*)
!DEBUGPRINT

      inFile => mecca_state%intfile
      iflag = 0
      nspin = min(2,inFile%nspin)
      lmax  = inFile%lmax

      call defMethod(0,inFile%imethod,inFile%sprstech)

!      rs_box => mecca_state%rs_box
!      ks_box => mecca_state%ks_box
!      ew_box => mecca_state%ew_box
      allTau => mecca_state%allTau
      nume = allTau%nset

      call mpi_initialized(mecca_mpi,ierr)
      mecca_state%mpi_box => mpi0box
      mpi0box%ik_begin = 0
      mpi0box%ik_end   = -1
      mpi0box%thisE_comm = 0

      if ( nspin <= size(allTau%set,2) ) then
       tau_box => allTau%set(1:size(allTau%set,1),1:size(allTau%set,2))
      else
        call fstop('incorrect memory allocation in calcTauEk')
      end if

!      vshift = zero
!      if ( nspin==2 ) then
!          vshift(1) = -inFile%magnField/2
!          vshift(2) = -vshift(1)
!      end if

      iprint = inFile%iprint

      allocate(edu_indx(1:nume))
      allocate(n_kpts(1:nume))
      do ie = 1,nume
       edu_indx(ie) = ie
       n_kpts(ie) = size(tau_box(ie,1)%kpoints,2)
      enddo
      num_jobs = sum(n_kpts)*nspin

      do is=1,nspin
       do ie=1,nume
        if ( associated(tau_box(ie,is)%tau_k) )                         &
     &                deallocate(tau_box(ie,is)%tau_k)
        if ( associated(tau_box(ie,is)%logdet) )                        &
     &                deallocate(tau_box(ie,is)%logdet)
        nullify(tau_box(ie,is)%tau_k)
        nullify(tau_box(ie,is)%logdet)
       end do
      end do

      kkrsz = (lmax+1)**2
      natom = mecca_state%rs_box%natom
      if ( myrank==0 ) then
       do is=1,nspin
        do ie=1,nume
         nK = n_kpts(ie)
         allocate(tau_box(ie,is)%tau_k(kkrsz,kkrsz,1:natom,1:nK))
         allocate(tau_box(ie,is)%logdet(1:nK))
        end do
       end do
      end if

      if(iprint.ge.0) then
       if( mecca_mpi ) then
        write(6,'(a)') ' MPI distribution of nodes'
        write(6,'(a,i5)') ' - number of jobs: ', num_jobs
       endif
      endif

      call mpi_barrier(MPI_COMM_WORLD,ierr)

      ! perform parallel calculation of Tau(E,k,spin)

      nsend = kkrsz*kkrsz*natom
      nrecv = nsend
      allocate(taskID(num_jobs))

      i = 0
      do is=1,nspin
       do ie=1,nume
        do j_kpt=1,n_kpts(ie)
         i = i+1
         if ( i>size(taskID,1) ) then
          call fstop(sname//' :: incorrect memory allocation')
         end if
         taskID(i)%ik = j_kpt
         taskID(i)%ie = ie
         taskID(i)%is = is
        end do
       end do
      end do

      if ( associated(tau0box%tau_k) ) deallocate(tau0box%tau_k)
      allocate( tau0box%tau_k(1:kkrsz,1:kkrsz,1:natom,1:1) )
      if ( associated(tau0box%logdet) ) deallocate(tau0box%logdet)
      allocate( tau0box%logdet(1:1) )

      do job=1,num_jobs
         j_kpt = taskID(job)%ik
         ie    = taskID(job)%ie
         is    = taskID(job)%is
         if ( nproc>1 ) then
          if ( mod(job-1,nproc) .ne. myrank ) cycle
         end if

!?           ----------------------------------------------------------
!?            energy=egrd(ie)+vshift(is)
!?            gf_box%energy(is) = egrd(ie)+vshift(is)
!?           ----------------------------------------------------------

         if ( myrank .ne. 0 ) then
          tau0box%energy = tau_box(ie,is)%energy
          tau0box%ns = tau_box(ie,is)%ns
          tau0box%kpoints => tau_box(ie,is)%kpoints
          tau0box%ncomp => tau_box(ie,is)%ncomp
          tau0box%tab => tau_box(ie,is)%tab
          tau0box%tcpa => tau_box(ie,is)%tcpa
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
          call calcTauEk (iflag,mecca_state,is,lmax,tau0box,j_kpt)
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
          call mpi_send(tau0box%tau_k(1,1,1,1),nsend,MPI_COMPLEX16,     &
     &                    0,tag1,comm_ne,ierr)
          call mpi_send(tau0box%logdet(1),1,MPI_COMPLEX16,              &
     &                    0,tag2,comm_ne,ierr)
          if ( j_kpt==1 ) then
           call mpi_send(tau0box%tcpa(1,1,1),nsend,MPI_COMPLEX16,       &
     &                    0,tag1,comm_ne,ierr)
          end if
         else   ! myrank=0
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
           call calcTauEk (iflag, mecca_state,is,lmax,                  &
     &                                           tau_box(ie,is),j_kpt)
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
           do i=1,min(nproc-1,num_jobs-job)
            j_kpt0 = taskID(job+i)%ik
            ie0    = taskID(job+i)%ie
            is0    = taskID(job+i)%is
            call mpi_recv(tau_box(ie0,is0)%tau_k(1,1,1,j_kpt0),nrecv,   &
     &         MPI_COMPLEX16,i,tag1,comm_ne,MPI_STATUS_IGNORE,ierr)
            call mpi_recv(tau_box(ie0,is0)%logdet(j_kpt0),1,            &
     &         MPI_COMPLEX16,i,tag2,comm_ne,MPI_STATUS_IGNORE,ierr)
            if ( j_kpt0==1 ) then
             if ( associated(tau_box(ie0,is0)%tcpa) ) then
              if ( size(tau_box(ie0,is0)%tcpa,3) < natom ) then
               deallocate(tau_box(ie0,is0)%tcpa)
               nullify(tau_box(ie0,is0)%tcpa)
              end if
             end if
             if ( .not. associated(tau_box(ie0,is0)%tcpa) ) then
              allocate(tau_box(ie0,is0)%tcpa(kkrsz,kkrsz,natom))
             end if
             tau_box(ie0,is0)%ns = size(tau_box(ie0,is0)%tcpa,3)
             call mpi_recv(tau_box(ie0,is0)%tcpa(1,1,1),nrecv,          &
     &         MPI_COMPLEX16,i,tag1,comm_ne,MPI_STATUS_IGNORE,ierr)
            end if
           end do
         end if
      enddo   ! job-cycle

      call mpi_barrier(comm_ne,ierr)

      if ( allocated(taskID) ) deallocate(taskID)

! TAU_k is s defined only in process with myrank=0

      call calcTauEk(-1,mecca_state,nspin,lmax,tau_box(1,1),1)

      if ( associated(tau0box%tau_k) ) then
       deallocate(tau0box%tau_k)
       nullify(tau0box%tau_k)
      end if
      if ( associated(tau0box%logdet) ) then
       deallocate(tau0box%logdet)
       nullify(tau0box%logdet)
      end if
      if ( myrank .ne.  0 ) then
       nullify(tau0box%kpoints)
       nullify(tau0box%ncomp)
      end if

      if ( allocated(n_kpts) ) deallocate(n_kpts)
      if ( allocated(edu_indx) ) deallocate(edu_indx)

      if ( .not. mecca_mpi ) then
       nullify(mecca_state%mpi_box)
      end if

      call mpi_barrier(MPI_COMM_WORLD,ierr)
!c
      return
!EOC
      end subroutine calcTaumain
!
!c============================================================================
!BOP
!!IROUTINE: calcTauEk
!!INTERFACE:
      subroutine calcTauEk (iflag,mecca_state,ispin,lmax,tau_box,j_kpt)
!!DESCRIPTION:
! single \{E,k\}-point calculation of KKR-CPA Tau(E,{\bf k}):
! {\tt tau\_box (type Tau\_data)}
!
!c============================================================================
!!REVISION HISTORY:
! Initial Version - A.S. - 2016
!!REMARKS:
! private module data are used for input/output
!EOP
      use mecca_constants
      use mecca_types
      use gfncts_interface, only : g_reltype,g_atcon,g_komp
      use mtrx_interface, only : invmatr
      use sctrng, only : getSSgf,getMSgf,setSStype
      use sctrng, only : IS_ss,ASA_ss,MIX_ss,etamin
      use zplane, only : get_recdlm
      use mpi
!
!BOC
      implicit none

      character(10), parameter :: sname = 'calcTauEk'
      integer, intent(in) :: iflag
      type(RunState), target :: mecca_state
      integer, intent(in) :: ispin
      integer, intent(in) :: lmax
      type ( Tau_data ), intent(inout) :: tau_box   !  block-diagonal elements only
      integer, intent(in) :: j_kpt

      integer :: kkrsz, kkrsz2, kkr2l, ndimrhp,ndimlhp
      integer :: l,m,m3,irecdlm
      integer, parameter :: ksint_sch=ksint_scf_sch
      integer :: st_env
      real(8) :: a,con,rytodu
      complex(8) :: cirl,edu,pdu

      type ( IniFile ), pointer :: inFile=>null()
      type ( RS_Str ), pointer :: rs_box=>null()
      type ( KS_Str ), pointer :: ks_box=>null()
      type ( EW_Str ), pointer :: ew_box=>null()
!      type (GF_data), pointer :: gf_box

      integer :: ndcomp,nbasis,nspin
!     integer :: nsub

      integer :: myrank, ierr

      real(8) :: conk
      complex(8), allocatable, save :: conr(:)
      complex(8), allocatable, save :: hplnm(:,:)

      complex(8), pointer :: tab(:,:,:,:)=>null()
      complex(8), pointer :: tcpa(:,:,:)=>null()

      complex(8) :: energy
      integer :: iprint,j,nrelv
      real(8) :: eta0

!      complex(8) :: taudet      ! determinant
!      real(8)    :: fullphase   ! phase of determinant, Im(log(det))

      complex(8) :: powe((2*lmax+1)**2),d00,eoeta
      integer, allocatable :: iorig(:)
      integer :: i,i0,nkns0,ndimk,ndimrij,nrsij,iprint2,imesh
      integer :: nset
      integer :: natom
      integer, pointer :: itype(:)=>null()
      integer, pointer :: if0(:,:)=>null()
      integer, pointer :: numbsub(:)=>null()
      integer :: nop
      complex(8), pointer :: dop(:,:)=>null()
      real(8), pointer :: pnt_j(:)=>null()
      real(8) :: vkn(3),xkn2,Rksp0,R2ksp
      real(8), allocatable :: xknlat0(:,:)
      real(8), allocatable :: xknlat(:,:)
      complex(8), allocatable, save :: dqint(:,:)   !  (nrns,2*lmax+1)
      complex(8), allocatable, save :: tcpatmp(:,:,:)
      complex(8), allocatable, save :: tcinv(:,:,:)
      real(8), parameter :: cphot = two*inv_fine_struct_const
      real(8) :: clight

      if ( iflag<0 ) then
          if ( allocated(hplnm) ) deallocate(hplnm)
          if ( allocated(conr) ) deallocate(conr)
          return
      end if

      nullify(inFile) ; if ( associated(mecca_state%intfile) )          &
     &  inFile => mecca_state%intfile
      nullify(rs_box) ; if ( associated(mecca_state%rs_box)  )          &
     &  rs_box => mecca_state%rs_box
      nullify(ks_box) ; if ( associated(mecca_state%ks_box) )           &
     &  ks_box => mecca_state%ks_box
      nullify(ew_box) ; if ( associated(mecca_state%ew_box) )           &
     &  ew_box => mecca_state%ew_box

      call mpi_comm_rank(mpi_comm_world,myrank,ierr)

      if ( .not. ( associated(inFile) .and.                             &
     &             associated(rs_box) .and.                             &
     &             associated(ks_box) .and.                             &
     &             associated(ew_box) ) ) then
       call fstop(sname//' :: ERROR :: pointers are not associated')
!     &             associated(gf_box) .and.                             &
      end if

      nspin = inFile%nspin
      nbasis = inFile%nsubl

      ndimlhp=(2*lmax+1)*(2*lmax+1)
      if ( inFile%imethod == 3 ) then
          ndimrhp = 1
      else
          ndimrhp=size(ew_box%xrslatij,1)
      end if
      if ( allocated(hplnm) ) then
       if ( size(hplnm,2).ne.ndimlhp ) then
         deallocate(hplnm)
       else if ( size(hplnm,1).ne.ndimrhp ) then
         deallocate(hplnm)
       end if
      end if
      if ( .not.allocated(hplnm) ) then
        allocate(hplnm(ndimrhp,ndimlhp))
!c     ----------------------------------------------------------------
!c               (r**l)*Y_lm(r)   [ M.ge.0]
!c     ----------------------------------------------------------------
       call rlylm(ew_box%xrslatij,ew_box%nrsij,ew_box%ndimrij,lmax,     &
     &            hplnm,ndimrhp)
      end if
!c
!c         set up r-,k-space coefficients
!c     ----------------------------------------------------------------
      kkrsz=(lmax+1)**2
      kkrsz2=kkrsz*kkrsz
      kkr2l=(2*lmax+1)**2
      if ( allocated(conr) ) then
       if ( size(conr).ne.kkr2l ) then
        deallocate(conr)
       end if
      end if
      if ( .not.allocated(conr) ) then
        allocate(conr(1:kkr2l))
!c  calculate r-space coefficents -4.0/sqrt( pi)*2**l (for Ewald)
!c
        a=-four/sqrt(pi)
!c
!c     calculate indexing for array dqint which is the integral in
!c     real-space based on Ewald formulation. This integral only
!c     depends on shell and energy (not on k space).
!c
        m3=0
        do l=0,2*lmax
         con=a*2**l
         cirl=(-dcmplx(zero,one))**l
         do m=-l,l
           m3=m3+1
           conr(m3) = (con/2)*cirl     ! conr() -- is used in ewald-routine only
         enddo
        enddo
      end if
!c     ----------------------------------------------------------------
      rytodu = rs_box%alat/(two*pi)
!c
      iprint = inFile%iprint
      nbasis = rs_box%nsubl
      natom = rs_box%natom
      kkrsz=(lmax+1)**2

      ndcomp = size(tau_box%tab,3)
      tab => tau_box%tab(1:kkrsz,1:kkrsz,1:ndcomp,1:nbasis)
      tcpa => tau_box%tcpa(1:kkrsz,1:kkrsz,1:nbasis)

      if ( myrank .ne. 0 ) then
       nset = 1
      else
       nset = size(tau_box%kpoints,2)
      end if
      if ( associated(tau_box%tau_k) ) then
        if ( size(tau_box%tau_k,1)==kkrsz .and.                         &
     &       size(tau_box%tau_k,2)==kkrsz .and.                         &
     &       size(tau_box%tau_k,3)==natom .and.                         &
     &       size(tau_box%tau_k,4)==nset ) then
        continue
        else
        deallocate(tau_box%tau_k)
         allocate(tau_box%tau_k(kkrsz,kkrsz,1:natom,1:nset))
        end if
      else
        allocate(tau_box%tau_k(kkrsz,kkrsz,1:natom,1:nset))
      end if
      if ( associated(tau_box%logdet) ) then
       if ( size(tau_box%logdet,1)==nset ) then
        continue
       else
        deallocate(tau_box%logdet)
        allocate(tau_box%logdet(1:nset))
       end if
      else
       allocate(tau_box%logdet(1:nset))
      end if

      if ( nset>0 ) then

       tau_box%ns = size(tau_box%tau_k,3)
       itype => rs_box%itype
       allocate(iorig(natom))
       call get_orig(nbasis,natom,itype,iorig)
       numbsub => rs_box%numbsubl
       if0 => rs_box%if0
       dop => ks_box%dop
       nop = size(dop,2)
       iprint2 = iprint
       if ( .not.allocated(tcinv) )                                     &
     &   allocate(tcinv(kkrsz,kkrsz,natom))
       if ( .not.allocated(tcpatmp) )                                   &
     &   allocate(tcpatmp(kkrsz,kkrsz,natom))
       call tcpa2all(natom,nbasis,iorig,itype,numbsub,if0,nop,          &
     &                   kkrsz,size(tcpa,1),dop,tcpa,tcpatmp,iprint2)
       nullify(tcpa)
       if ( size(tau_box%tcpa,3)<natom ) then
        deallocate(tau_box%tcpa)
        allocate(tau_box%tcpa(1:kkrsz,1:kkrsz,1:natom))
        tau_box%ns = size(tau_box%tau_k,3)
       end if
       tau_box%tcpa(1:kkrsz,1:kkrsz,1:natom) =                          &
     &      tcpatmp(1:kkrsz,1:kkrsz,1:natom)
       tcpatmp = tcpatmp / rytodu

       do i=1,nbasis
        i0 = iorig(i)
        tcinv(1:kkrsz,1:kkrsz,i0) = tcpatmp(1:kkrsz,1:kkrsz,i0)
        call invmatr(tcinv(1:kkrsz,1:kkrsz,i0),kkrsz,1,1,0,             &
     &                                              -1000,inFile%istop)
        do j=1,natom
         if ( j == i0 ) cycle
         if ( itype(j) .eq. i ) then
          tcinv(1:kkrsz,1:kkrsz,j) = tcinv(1:kkrsz,1:kkrsz,i0)
         end if
        end do
       end do

       call g_reltype(valence=nrelv)

       clight = cphot
       if ( nrelv>1 ) then
        clight = cphot*ten**nrelv
       end if

       energy = tau_box%energy
       edu = energy*rytodu**2
       pdu  = sqrt(2*m_e*energy*(cone+energy/(2*m_e*clight**2)))
       pdu  = rytodu*pdu*sign(one,aimag(pdu))

       eta0 = max(etamin,abs(edu)/inFile%eneta)
!c                                    to be sure in accuracy of D00

       call get_recdlm(edu,irecdlm)
       call gEnvVar('ENV_TAU_RECDLM',.false.,st_env)
       if ( st_env>=0 ) then
        irecdlm = min(st_env,1)
       end if
       if( irecdlm.eq.0 ) then   ! for  Ewald technique
         Rksp0 = ew_box%Rksp
         R2ksp = max(zero,eta0*Rksp0*Rksp0+dreal(edu))
         ! rooeml --> sqrt(e^-l)
         call rooeml(pdu,powe,lmax,iprint)
         call d003(edu,eta0,d00)
!
!  calculate k-space coefficient in D.U.
!
        conk=-(four*pi)/(rs_box%volume*(two*pi/rs_box%alat)**3) ! -(four*pi)/(omegws*dutory**3)
         eoeta=exp( edu/eta0 ) * conk
       else                      ! in Hankel RS summation
         R2ksp = zero
         eoeta = zero
         d00 = zero
       endif

       if ( allocated(iorig) ) deallocate(iorig)
!c ==================================================================
!     Both hankel and real-space ewald integral passed as DQINT.

       ndimrij = ew_box%ndimrij
       nrsij = ew_box%nrsij
       allocate(dqint(1:nrsij,2*lmax+1))
       call ew_dqint(edu,pdu,eta0,lmax,ew_box%xrslatij(1:nrsij,4),      &
     &               irecdlm,dqint)

!       call pickmesh(edu,enlim,size(ks_box%nqpt,1),imesh)
!  for imesh=1, xknlat(imesh=1)=reshape(xknlat(1:3*nkns(1)),[nkns(1),3])
!  for imesh=2, xklnat(imesh=2)=reshape(xknlat(3*nkns(1)+1:3*nkns(2)),[nkns(2),3]), etc.
       imesh = 1    ! it may depend on edu
       m = 3*sum(ew_box%nksn(1:imesh))
       ndimk = ew_box%nksn(imesh)
       i0 = m - 3*ndimk+1
       allocate(xknlat0(ndimk,3),xknlat(ndimk,3))
       xknlat(1:ndimk,1:3) = reshape(ew_box%xknlat(i0:m),[ndimk,3])

        pnt_j => tau_box%kpoints(1:3,j_kpt)
        if ( nset==1 ) then
         j = 1
        else
         j = j_kpt
        end if

        nkns0 = 0
        if(irecdlm.eq.0) then
         do i=1,ndimk
          vkn(1:3) = xknlat(i,1:3) + pnt_j
          xkn2 = dot_product(vkn,vkn)
          if(xkn2.le.R2ksp) then
           nkns0 = nkns0+1
           xknlat0(nkns0,1:3) =  xknlat(i,1:3)
          end if
         end do
        end if

        call eval1tau( lmax,kkrsz,rs_box%natom,rs_box%aij,              &
     &             rs_box%mapstr,rs_box%mappnt,rs_box%mapij,            &
     &             powe,edu,pdu,ew_box%xrslatij,ew_box%ndimrij,         &
     &             ew_box%np2r,ew_box%ndimr,ew_box%numbrs,              &
     &             rs_box%ncount,dqint,size(dqint,1),                   &
     &             hplnm,                                               &
     &             irecdlm,d00,eoeta,eta0,                              &
     &             xknlat0(1:nkns0,1:3),nkns0,conr,                     &
     &             tcpatmp(1:kkrsz,1:kkrsz,1:natom),pnt_j(1:3),         &
     &             tau_box%tau_k(1:kkrsz,1:kkrsz,1:natom,j),            &
     &             tau_box%logdet(j) )

       tau_box%tau_k(1:kkrsz,1:kkrsz,1:natom,j) =                       &
     &                tau_box%tau_k(1:kkrsz,1:kkrsz,1:natom,j) * rytodu

      else
       tau_box%ns = 0
      end if

      if ( allocated(tcpatmp) )  deallocate(tcpatmp)
      if ( allocated(xknlat0) )  deallocate(xknlat0)
      if ( allocated(xknlat) )  deallocate(xknlat)
      if ( allocated(dqint) )    deallocate(dqint)

      return
!EOC
      contains

       subroutine ew_dqint(edu,pdu,eta0,lmax,r2,irecdlm,dqint)
       implicit none
       complex(8), intent(in) :: edu  ! for irecdlm==0 (ewald)
       complex(8), intent(in) :: pdu  !                (hankel)
       real(8), intent(in) :: eta0
       integer, intent(in) :: lmax
       real(8), intent(in) :: r2(:)
       integer, intent(in) :: irecdlm
       complex(8), intent(out) :: dqint(:,:)

       integer    :: l1
       real(8)    :: rsp,r2tmp
       complex(8) :: xpr,dqtmp(0:2*lmax)

       rsp = -two
       do i=1,size(r2,1)
        r2tmp=   r2(i)
        if( abs(r2tmp-rsp) .ge. 1.0d-6 ) then
          rsp = r2tmp
          if(irecdlm.eq.0) then
             call intfac(rsp,edu,eta0,2*lmax,dqtmp)
          else
             if(r2tmp.le.1.0d-6) then
              dqtmp = czero
             else
              xpr=pdu*sqrt(rsp)
              call hankel(xpr,dqtmp,2*lmax)
             end if
          end if
          do l1=0,2*lmax
             dqint(i,l1+1)=dqtmp(l1)
          enddo
        else
          if ( i==1 ) then
           call fstop(sname//': MEMORY BUG???')
          end if
          do l1= 1,2*lmax+1
           dqint(i,l1)=dqint(i-1,l1)
          enddo
        endif
       enddo

       return
       end subroutine ew_dqint
!
       subroutine get_orig(nbasis,natom,itype,iorig)
       implicit none
       integer, intent(in) :: nbasis,natom,itype(natom)
       integer, intent(out) :: iorig(nbasis)

       integer :: nsub,iatom

       do nsub=1,nbasis
        do iatom=1,natom
         if(itype(iatom).eq.nsub) go to 10
        end do
        call fstop(sname// ' :: You have wrong ITYPE')
10      iorig(nsub) = iatom
!c!!!
!c  it is supposed below  that "iorig(nsub)" -- number of 1st site of
!c  type "nsub" for isite=1,natom
!c!!!
       end do
       return
       end subroutine get_orig
!
      end subroutine calcTauEk
!
!BOP
!!IROUTINE: eval1tau
!!INTERFACE:
        subroutine eval1tau( lmax,kkrsz,natom,aij,mapstr,mappnt,        &
     &             mapij,powe,edu,pdu,rsnij,ndimrij,np2r,ndimnp,numbrs, &
     &      naij,dqint,ndimdqr,hplnm,irecdlm,d00,eoeta,                 &
     &   eta,ksvecs,nksvecs,conr,tcpa,k,tauii,logdet )
!!DESCRIPTION:
! module engine to calculate tau-related data
!
!!USES:
        use freespaceG, only : gf0_ks
        use mtrx_interface, only : invmatr
!EOP
!
!BOC
       implicit none

       integer, intent(in) :: lmax,natom,ndimrij,kkrsz
       integer, intent(in) :: ndimnp,ndimdqr
       integer, intent(in) :: mapij(2,*),naij,np2r(ndimnp,*)
       integer, intent(in) :: mapstr(natom,natom),mappnt(natom,natom)
       integer, intent(in) :: numbrs(*)
       integer, intent(in) :: nksvecs,irecdlm

       real(8), intent(in) :: aij(3,*),rsnij(ndimrij,4)
       real(8), intent(in) :: eta
       real(8), intent(in) :: ksvecs(nksvecs,3)

       complex(8), intent(in) :: conr(*),powe(*),dqint(ndimdqr,*)
       complex(8), intent(in) :: tcpa(:,:,:)  ! (ipkkr,ipkkr,natom)
       complex(8), intent(in) :: edu,pdu,d00,eoeta
       complex(8), intent(in) :: hplnm(:,:)

       real(8), intent(in) :: k(3)

       complex(8), intent(out) :: tauii(:,:,:)
       complex(8), intent(out) :: logdet
       ! complex(8), intent(out) :: taudet = exp(logdet)  ! determinant
       ! real(8), intent(out) :: fullphase = Im(logdet)   ! phase of determinant

       complex(8) :: greenks(kkrsz*natom,kkrsz*natom)
!       complex(8) :: g0ks(natom*kkrsz,natom*kkrsz)
       complex(8) :: work(kkrsz,natom*kkrsz)
!       complex(8), pointer :: greenfn(:,:,:)=>null()

!      interface
!       subroutine gsvein1(amt,ndmat,                                    &
!     &                  cof,ndcof,                                      &
!     &                  kkrsz,natom,                                    &
!     &                  logdet)
!       integer, intent(in) :: ndmat,ndcof,kkrsz,natom
!       complex(8) :: amt(ndmat,kkrsz*natom)
!       complex(8) :: cof(*)
!       complex(8), intent(out), optional :: logdet
!       end subroutine gsvein1
!      end interface

! internal variables

       character*10 :: istop = 'null'
!       logical :: debugflag = .false.

       integer :: i, n
       integer :: ndimrhp,ndimlhp

       integer :: nblock

       complex(8), parameter :: cone=(1.d0,0.d0),czero=(0.d0,0.d0)
       real(8), parameter :: zero=0.d0
       integer, parameter :: iprint=-1000

!      if(debugflag)                                                     &
!     &    call begintiming("eval1tau // compute structure const")

! fill in structure constants
       ndimrhp = size(hplnm,1)
       ndimlhp = size(hplnm,2)
       call gf0_ks(lmax,natom,aij,mapstr,mappnt,natom,mapij,            &
     &                             powe,k(1),k(2),k(3),edu,pdu,irecdlm, &
     &             rsnij,ndimrij,np2r,ndimnp,numbrs,naij,dqint,ndimdqr, &
     &                      hplnm(1:ndimrhp,1:ndimlhp),ndimrhp,ndimlhp, &
     &                       d00,eoeta,eta,ksvecs,nksvecs,conr,nksvecs, &
     &         greenks,iprint,istop)
!DEBUGPRINT
!DELETE       call debug_tau( 1.d0 ,edu,k(1),k(2),k(3),                        &
!DELETE     &                                  tcpa(1:kkrsz,1:kkrsz,1:natom),  &
!DELETE     &       tcpa(1:kkrsz,1:kkrsz,1:natom),kkrsz,natom,'EVAL1TAU TCPA')
!DELETE       call debug_tau( 1.d0 ,edu,k(1),k(2),k(3),                        &
!DELETE     &                                         greenks,greenks,         &
!DELETE     &                                   kkrsz*natom,1,'EVAL1TAU G0KS')
!DEBUGPRINT

!      if(debugflag) call endtiming()

       logdet = zero

!       g0ks = greenks     ! save g0

! change to ( 1 - t G0 ), more stable

       n = natom*kkrsz
!      if(debugflag) call begintiming("eval1tau // premultiply t")
         do i = 1, natom
           call zgemm('N','N',kkrsz,n,kkrsz,cone,tcpa(1,1,i),           &
     &                         size(tcpa,1),greenks(1+(i-1)*kkrsz,1),n, &
     &                         czero,work(1,1),kkrsz)
           greenks(1+(i-1)*kkrsz:i*kkrsz,1:n) = -work(1:kkrsz,1:n)
         end do
         do i = 1, kkrsz*natom
          greenks(i,i) = greenks(i,i) + cone
         end do

       n = kkrsz*natom
       nblock = 0
       call invmatr(greenks,                                            &
     &             n,1,                                                 &
     &             1,nblock,                                            &
     &             iprint,istop,logdet)

!       taudet = exp(logdet)
!       fullphase = aimag(logdet)

!      if(debugflag) call endtiming()


! final tau = ( 1 - t G0 )^-1 * t = ( t^-1 - G0 )^-1
!        if(debugflag) call begintiming("eval1tau // postmultiply t")
        do i = 1, natom
         work(1:kkrsz,1:kkrsz) =                                        &
     &    greenks(1+(i-1)*kkrsz:i*kkrsz,1+(i-1)*kkrsz:i*kkrsz)
         call zgemm('N','N',kkrsz,kkrsz,kkrsz,cone,                     &
     &    work(1:kkrsz,1:kkrsz),kkrsz,                                  &
     &    tcpa(1,1,i),size(tcpa,1),                                     &
     &    czero,tauii(1,1,i),size(tauii,1))
        end do
!DEBUGPRINT
!DELETE       call debug_tau( 1.d0 ,edu,k(1),k(2),k(3),                        &
!DELETE     &                                    tcpa(1:kkrsz,1:kkrsz,1:natom),&
!DELETE     &        tauii(1:kkrsz,1:kkrsz,1:natom),kkrsz,natom,'EVAL1TAU TAU')
!DEBUGPRINT

!! final G = G0 ( 1 - t G0 )^-1 = ( 1-G0 t )^-1 G0
!
!!        greenfn => tauii
!        do i = 1, natom
!         call zgemm('N','N',kkrsz,kkrsz,n,cone,                         &
!     &    g0ks(1+(i-1)*kkrsz,1),n,greenks(1,1+(i-1)*kkrsz),n,           &
!     &    czero,tauii(1,1,i),size(tauii,1))
!        end do

!      if(debugflag) call endtiming()

       return
!EOC
       end subroutine eval1tau

!BOP
!!ROUTINE: tauii_ks
!!INTERFACE:
      subroutine tauii_ks(lmax,natom,aij,mapstr,mappnt,                 &
     &             mapij,powe,edu,pdu,rsnij,ndimrij,np2r,ndimnp,numbrs, &
     &      naij,dqint,ndimdqr,hplnm,irecdlm,d00,eoeta,                 &
     &   eta,xknlat,ndimks,conr,nkns,                                   &
     &   tcpa,ndkkr,tcinv,kpnt,tauii)
!!DESCRIPTION:
!  computes diagonal part of Tau (tauii) in {\bf k}-space
!
!!USES:
      use mtrx_interface, only : wrtdia2
      use freespaceG,  only : gf0_ks
!
!!REVISION HISTORY:
! Adapted - A.S. - 2016
!EOP
!
!BOC
      implicit none

      interface
      subroutine calgreen(greens,                                       &
     &                    kkrsz,natom,tcpa,tcinv,ndkkr,idiag,itype,     &
     &                    invswitch,invparam,                           &
     &                    iprint,istop)
      implicit none
      complex*16   greens(*)
      integer      kkrsz,natom,ndkkr,idiag
      complex*16   tcpa(*)
      complex*16   tcinv(*)
      integer      itype(natom)
      integer      invswitch,invparam,iprint
      character    istop*10
      end subroutine calgreen
      end interface
!c
      integer, intent(in) :: lmax,natom

      integer, intent(in) :: irecdlm,ndimrij
      integer, intent(in) :: ndimnp,ndimks,ndimdqr

      integer, intent(in) :: nkns
!c
      real(8), intent(in) ::  aij(3,*)
      real(8), intent(in) ::  kpnt(3)
      real(8), intent(in) ::  rsnij(ndimrij,4)
      real(8), intent(in) ::  xknlat(ndimks,3)
      real(8), intent(in) ::  eta
!c
      complex(8), intent(in) :: conr(*)
      complex(8), intent(in) :: powe(*)
      complex(8), intent(in) :: dqint(ndimdqr,*)

      integer, intent(in) ::      ndkkr
      complex(8), intent(in) :: tcpa(ndkkr,ndkkr,natom)
      complex(8), intent(in) :: tcinv(ndkkr,ndkkr,natom)

      complex(8), intent(out) :: tauii(ndkkr,ndkkr,natom)
      complex(8), intent(in) :: edu
      complex(8), intent(in) :: pdu
      complex(8), intent(in) :: d00
      complex(8), intent(in) :: eoeta

      complex(8), intent(in) :: hplnm(:,:)

      integer, intent(in) :: mapstr(natom,natom),mappnt(natom,natom)

      integer, intent(in) :: mapij(2,*)
      integer, intent(in) :: np2r(ndimnp,*),numbrs(*),naij

      integer, parameter :: invswitch=1,nblock=0
      integer, parameter :: iprint=-1000
!c     ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
      character(10), parameter :: sname='tauii_ks'
      character(10) :: istop='null'
!c     ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
      integer :: i,kkrsz
      integer :: ndimrhp,ndimlhp
      integer :: itype1(natom)
      complex(8) :: greenks(1:((lmax+1)**2*natom)**2)

      integer, external :: indclstr

      kkrsz = (lmax+1)**2
      ndimrhp = size(hplnm,1)
      ndimlhp = size(hplnm,2)
      forall (i=1:natom) itype1(i) = i
!c
!c  K-space calculation of G0 (full matrix only),
!c
       call gf0_ks(                                                     &
     &              lmax,natom,                                         &
     &              aij,                                                &
     &              mapstr,mappnt,natom,mapij,                          &
     &              powe,                                               &
     &              kpnt(1),kpnt(2),kpnt(3),                            &
     &              edu,pdu,                                            &
     &              irecdlm,                                            &
     &              rsnij,ndimrij,                                      &
     &              np2r,ndimnp,numbrs,naij,                            &
     &              dqint,ndimdqr,                                      &
     &              hplnm,ndimrhp,ndimlhp,                              &
     &              d00,eoeta,                                          &
     &              eta,xknlat,ndimks,                                  &
     &              conr,nkns,                                          &
     &              greenks,                                            &
     &              iprint,istop)

!c         write(6,*) 'g0 at eoeta =', eoeta
!c         write(6,*) 'kx,ky,kz=',kx,ky,kz
!c         do ii = 1,kkrsz*kkrsz
!c           write(6,*) greenks(ii)
!c         end do


!c   tcpa,tcinv -- matrix [NDKKRxNDKKR,*]
!c     ndimbas     tcinv == tcpa^(-1)

!DELETE       call debug_tau( 1.d0 ,edu,kpnt(1),kpnt(2),kpnt(3),               &
!DELETE     &                                  tcpa(1:kkrsz,1:kkrsz,1:natom),  &
!DELETE     &       tcpa(1:kkrsz,1:kkrsz,1:natom),kkrsz,natom,'TAUii_KS TCPA')
!DELETE       call debug_tau( 1.d0 ,edu,kpnt(1),kpnt(2),kpnt(3),               &
!DELETE     &                                 tcinv(1:kkrsz,1:kkrsz,1:natom),  &
!DELETE     &       tcinv(1:kkrsz,1:kkrsz,1:natom),kkrsz,natom,'TAUii_KS TINV')
!DELETE       call debug_tau( 1.d0 ,edu,kpnt(1),kpnt(2),kpnt(3),               &
!DELETE     &                reshape(greenks(1:(kkrsz*natom)**2),              &
!DELETE     &                        [(kkrsz*natom),(kkrsz*natom),1]),         &
!DELETE     &                reshape(greenks(1:(kkrsz*natom)**2),              &
!DELETE     &                        [(kkrsz*natom),(kkrsz*natom),1]),         &
!DELETE     &                                   kkrsz*natom,1,'TAUii_KS G0KS')

        call calgreen(                                                  &
     &              greenks,                                            &
     &              kkrsz,natom,tcpa,tcinv,ndkkr,0,itype1,              &
     &              invswitch,nblock,                                   &
     &              iprint,istop)

!DELETE       call debug_tau( 1.d0 ,edu,kpnt(1),kpnt(2),kpnt(3),               &
!DELETE     &                reshape(greenks(1:(kkrsz*natom)**2),              &
!DELETE     &                        [(kkrsz*natom),(kkrsz*natom),1]),         &
!DELETE     &                reshape(greenks(1:(kkrsz*natom)**2),              &
!DELETE     &                        [(kkrsz*natom),(kkrsz*natom),1]),         &
!DELETE     &                                   kkrsz*natom,1,'TAUii_KS GFKS')

!        call caltau00(                                                  &
!     &              greenks,kkrsz*natom,tcpa,ndkkr,itype1,              &
!     &              natom,kkrsz,tauii,invswitch)
        call gr2tauz(                                                   &
     &              greenks,kkrsz*natom,tcpa,ndkkr,itype1,              &
     &              natom,kkrsz,tauii)

!DELETE       call debug_tau( 1.d0 ,edu,kpnt(1),kpnt(2),kpnt(3),               &
!DELETE     &                                    tcpa(1:kkrsz,1:kkrsz,1:natom),&
!DELETE     &        tauii(1:kkrsz,1:kkrsz,1:natom),kkrsz,natom,'TAUii TAU')

!c     ==================================================================

      if (istop.eq.sname) call fstop(sname)
!c

      return
      end subroutine tauii_ks

       subroutine debug_tau(rytodu,edu,kx,ky,kz,tcpa,tau,kkrsz,natom,T)
       use mecca_types, only : Tau_set
       implicit none
       real(8) :: rytodu
       complex(8) :: edu
       real(8) :: kx,ky,kz
       integer :: kkrsz,natom
       complex(8) :: tcpa(kkrsz,kkrsz,natom)
       complex(8) :: tau(kkrsz,kkrsz,natom)
       character(*) :: T
       type(Tau_set) :: allTau
       integer, save :: io=0

       real(8), parameter ::                                            &
     &  kx0=-0.83333333333333D-01,                                      &   ! A3.xyz
     &  ky0=-0.57735026918962D+00,                                      &
     &  kz0= 0.26791296717132D+00
!     &    kx0=-0.25D+00,ky0=-0.5773502692D+00,kz0= 0.1530931241D+00         ! A3.xyz
!     &  kx0=0.3750D+00,  ky0=0.3750D+00,  kz0=0.3750D+00                     ! A1.xyz

       if ( natom<=0 ) then
        io=-natom
        write(*,*) ' NEW IO =',io,' IN DEBUG_TAU'
        return
       end if

       if ( io<=0 ) return

       if ( abs(kx-kx0)>1.d-10 ) return
       if ( abs(ky-ky0)>1.d-10 ) return
       if ( abs(kz-kz0)>1.d-10 ) return

       allTau%nset = 1
       allocate(allTau%set(1,1))

       allTau%set(1,1)%ns=natom

       allTau%set(1,1)%energy = edu / rytodu**2

       allocate(allTau%set(1,1)%ncomp(natom))
       allTau%set(1,1)%ncomp=1

       allocate(allTau%set(1,1)%kpoints(3,1))
       allTau%set(1,1)%kpoints(1:3,1) = [ kx0, ky0, kz0 ]

       if ( natom>1 .or. any( tcpa.ne.tau ) ) then
         allocate(allTau%set(1,1)%tcpa(kkrsz,kkrsz,natom))
         allTau%set(1,1)%tcpa(1:kkrsz,1:kkrsz,1:natom) = tcpa*rytodu
       else
         allTau%set(1,1)%tcpa => null()
       end if

       allocate(allTau%set(1,1)%tau_k(kkrsz,kkrsz,natom,1))
       allTau%set(1,1)%tau_k(1:kkrsz,1:kkrsz,1:natom,1) =               &
     &                        tau(1:kkrsz,1:kkrsz,1:natom)*rytodu

       allTau%set(1,1)%tab => null()

       io = io+1
       write(io,'(a)') T
!DEBUGPRINT
       write(6,'(a,i5)') ' DUMP '//T//' in ',io
!DEBUGPRINT
       call dumpTauset(allTau,io)

       deallocate(allTau%set(1,1)%ncomp)
       deallocate(allTau%set(1,1)%tau_k)
       if(associated(allTau%set(1,1)%tcpa))                             &
     &      deallocate(allTau%set(1,1)%tcpa)
       deallocate(allTau%set(1,1)%kpoints)
       deallocate(allTau%set)

       return
       end subroutine debug_tau
!
!BOP
!!IROUTINE: calcTcpa
!!INTERFACE:
      subroutine calcTcpa ( mecca_state )
!!DESCRIPTION:
! procedure to calculate t_ss and t_cpa matrices
!
!!USES:
      use mecca_constants
      use mecca_types
      use mpi

!!DO_NOT_PRINT
      implicit none
!!DO_NOT_PRINT

!!ARGUMENTS:
! control parameter to choose calculational method
      type(RunState), target :: mecca_state
!!REVISION HISTORY:
! Initial Version - A.S. - 2016
!EOP
!
!BOC
      character(*), parameter :: sname='calcTcpa'
      type(IniFile),   pointer :: inFile=>null()
      type (Tau_data), pointer :: tau_box
      complex(8) :: energy

      integer :: iflag
      integer :: lmax,kkrsz,nbasis,ndcomp
      integer :: ie,is,ies,iprint,nspin,nume
!      real(8) :: vshift(2)

!      complex(8), allocatable :: egrd(:)
      ! MPI related variables
!      logical :: mecca_mpi
      integer :: ierr, myrank, nproc, num_jobs
      integer :: nsend,nrecv
!      integer :: i_begin, i_end
      integer :: nodes_per_task
      integer :: myjobID
      integer :: comm_ne

      real(8) :: time0,time1,dtime

      ! get rank and number of processes
      call mpi_comm_rank(mpi_comm_world,myrank,ierr)
      call mpi_comm_size(mpi_comm_world,nproc,ierr)
      comm_ne = mpi_comm_world
!DEBUGPRINT
        write(6,*)
        write(6,*) ' ......'//sname//' STARTED.......'
        write(6,*)
!DEBUGPRINT
      inFile => mecca_state%intfile
      iflag = 0
      iprint = inFile%iprint
      nspin = min(2,inFile%nspin)
      lmax  = inFile%lmax
      kkrsz = (lmax+1)**2
      nbasis = mecca_state%rs_box%nsubl
      ndcomp = maxval(inFile%sublat(1:inFile%nsubl)%ncomp)

      call defMethod(0,inFile%imethod,inFile%sprstech)
!
!      rs_box => mecca_state%rs_box
!      ks_box => mecca_state%ks_box
!      ew_box => mecca_state%ew_box

!      vshift = zero
!      if ( nspin==2 ) then
!          vshift(1) = inFile%magnField
!          vshift(2) = -vshift(1)
!      end if

      if ( size(mecca_state%allTau%set,2)<nspin ) then
       write(*,*) ' NSPIN=',NSPIN
       write(*,*) ' size(tau_box,2)=',size(mecca_state%allTau%set,2)
       call fstop                                                       &
     &     (sname//' INCORRECT VALUE OF NSPIN OR BAD MEMORY ALLOCATION')
      end if
      nume = size(mecca_state%allTau%set,1)
      num_jobs = nume*nspin
      nodes_per_task = nproc    ! parallelization over E-points

      if(iprint.ge.0) then
       if( nproc>1 ) then
        write(6,'(a)') ' MPI distribution of nodes'
        write(6,'(a,i5)') ' - number of jobs: ', num_jobs
       endif
      endif

      ! syncronize before beginning jobs
!MPIDEBUG      call log_mpi_sync(__FILE__,__LINE__)

!DEBUGPRINT
!      time0 = mpi_wtime()
!DEBUGPRINT
      ! perform parallel integration over E
      do ie=1,nume
       do is=1,nspin
        ies = nspin*(ie-1)+is
        tau_box => mecca_state%allTau%set(ie,is)
        if ( nproc>1 ) then
         if ( mod(ies-1,nproc) .ne. myrank ) cycle
        end if

        energy = tau_box%energy
!        energy=energy+vshift(is)

        if ( associated(tau_box%tab) ) then
         deallocate(tau_box%tab)
         nullify(tau_box%tab)
        end if
        allocate(tau_box%tab(1:kkrsz,1:kkrsz,1:ndcomp,1:nbasis))
        if ( associated(tau_box%tcpa) ) then
         deallocate(tau_box%tcpa)
         nullify(tau_box%tcpa)
        end if
        allocate(tau_box%tcpa(1:kkrsz,1:kkrsz,1:nbasis))
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        call eval1Tcpa (iflag,mecca_state,is,lmax,energy,               &
     &                                      tau_box%tab,tau_box%tcpa)
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

       end do    ! is-cycle
      end do    ! ie-cycle
!DEBUGPRINT
!      time1 = mpi_wtime()
!      dtime = time1 - time0
!      write(0,*) myrank,'MPI evalTcpa DONE',dtime
!         call mpi_barrier(comm_ne,ierr)
!DEBUGPRINT
      nsend = kkrsz*kkrsz*ndcomp*nbasis
      nrecv = nsend

      time0 = mpi_wtime()
      if ( myrank>0 ) then
       do is=1,nspin
        do ie = 1,nume
         myjobID = nume*(is-1) + ie
         tau_box => mecca_state%allTau%set(ie,is)
         if ( associated(tau_box%tab) ) then
           call mpi_send(tau_box%tab(1,1,1,1),nsend,MPI_COMPLEX16,      &
     &                    0,myjobID,comm_ne,ierr)
         end if
        end do
       end do
      else
       do is=1,nspin
        do ie = 1,nume
         myjobID = nume*(is-1) + ie
         tau_box => mecca_state%allTau%set(ie,is)
         if ( .not.associated(tau_box%tab) ) then
           allocate(tau_box%tab(1:kkrsz,1:kkrsz,1:ndcomp,1:nbasis))
          call mpi_recv(tau_box%tab(1,1,1,1),nrecv,MPI_COMPLEX16,       &
     &        MPI_ANY_SOURCE,myjobID,comm_ne,MPI_STATUS_IGNORE,ierr)
         end if
        end do
       end do
      end if
       do is=1,nspin
        do ie = 1,nume
         tau_box => mecca_state%allTau%set(ie,is)
         if ( .not.associated(tau_box%tab) ) then
          if ( myrank>0 ) then
           allocate(tau_box%tab(1:kkrsz,1:kkrsz,1:ndcomp,1:nbasis))
          else
        write(0,*) myrank,'MPI WRONG tab',ie,is
        call fstop('ERROR1')
          end if
         else if ( size(tau_box%tab)<nsend ) then
        write(0,*) myrank,'MPI WRONG tab',ie,is
        write(0,*)  ' SIZE1=',size(tau_box%tab,1)
        write(0,*)  ' SIZE2=',size(tau_box%tab,2)
        write(0,*)  ' SIZE3=',size(tau_box%tab,3)
        write(0,*)  ' SIZE4=',size(tau_box%tab,4)
        call fstop('ERROR2')
         end if
         call mpi_bcast(tau_box%tab(1,1,1,1),nsend,MPI_COMPLEX16,0,     &
     &                                                    comm_ne,ierr)
        end do
       end do

      nsend = kkrsz*kkrsz*nbasis
      nrecv = nsend

      if ( myrank>0 ) then
       do is=1,nspin
        do ie = 1,nume
         myjobID = nume*(is-1) + ie
         tau_box => mecca_state%allTau%set(ie,is)
         if ( associated(tau_box%tcpa) ) then
           call mpi_send(tau_box%tcpa(1,1,1),nsend,MPI_COMPLEX16,       &
     &                    0,myjobID,comm_ne,ierr)
         else
!           allocate(tau_box%tcpa(1:kkrsz,1:kkrsz,1:nbasis))
          end if
        end do
       end do
      else
       do is=1,nspin
        do ie = 1,nume
         myjobID = nume*(is-1) + ie
         tau_box => mecca_state%allTau%set(ie,is)
         if ( .not.associated(tau_box%tcpa) ) then
           allocate(tau_box%tcpa(1:kkrsz,1:kkrsz,1:nbasis))
           call mpi_recv(tau_box%tcpa(1,1,1),nrecv,MPI_COMPLEX16,       &
     &        MPI_ANY_SOURCE,myjobID,comm_ne,MPI_STATUS_IGNORE,ierr)
         else
         end if
        end do
       end do
      end if
       do is=1,nspin
        do ie = 1,nume
         tau_box => mecca_state%allTau%set(ie,is)
         if ( .not.associated(tau_box%tcpa) ) then
          if ( myrank>0 ) then
           allocate(tau_box%tcpa(1:kkrsz,1:kkrsz,1:nbasis))
          else
        write(0,*) myrank,'MPI WRONG tcpa',ie,is
        call fstop('ERROR3')
          end if
         else if ( size(tau_box%tcpa)<nsend ) then
        write(0,*) myrank,'MPI WRONG tcpa',ie,is
        write(0,*)  ' SIZE1=',size(tau_box%tcpa,1)
        write(0,*)  ' SIZE2=',size(tau_box%tcpa,2)
        write(0,*)  ' SIZE3=',size(tau_box%tcpa,3)
        write(0,*)  ' SIZE=',size(tau_box%tcpa),nsend
        call fstop('ERROR4')
         end if
         call mpi_bcast(tau_box%tcpa(1,1,1),nsend,MPI_COMPLEX16,0,      &
     &                                                    comm_ne,ierr)
        end do
       end do

!DEBUGPRINT
!      time1 = mpi_wtime()
!      dtime = dtime + time1 - time0
!      write(0,*) myrank,'MPI SENDRECV DONE',time1-time0
!      write(0,*) myrank,'MPI TOTTIME DONE',dtime
!DEBUGPRINT

      call eval1Tcpa (-1,mecca_state,1,lmax,energy,                     &
     &                                         tau_box%tab,tau_box%tcpa)

      return
!EOC
!
      end subroutine calcTcpa
!
!c============================================================================
!BOP
!!IROUTINE: eval1Tcpa
!!INTERFACE:
      subroutine eval1Tcpa (iflag, mecca_state, ispin, lmax, energy,    &
     &                                                     t_ss, t_cpa)
!!DESCRIPTION:
! single E-point calculation of KKR-CPA t_ss(E), t_cpa(E):
! {\tt block-diagonal matrices}
!
!c============================================================================
!!REVISION HISTORY:
! Initial Version - A.S. - 2016
!!REMARKS:
! private module data are used for input/output
!EOP
      use mecca_constants
      use mecca_types
      use gfncts_interface, only : g_reltype,g_atcon,g_komp
      use mtrx_interface, only : invmatr
      use sctrng, only : getSSgf,getMSgf,setSStype
      use sctrng, only : IS_ss,ASA_ss,MIX_ss,etamin
      use zplane, only : get_recdlm
      use mpi
!
!BOC
      implicit none

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

      character(*), parameter :: sname = 'eval1Tcpa'
      integer, intent(in) :: iflag
      type(RunState), target :: mecca_state
      integer, intent(in) :: ispin
      integer, intent(in) :: lmax
      complex(8), intent(in) :: energy
      complex(8), pointer, intent(inout) :: t_ss(:,:,:,:)
      complex(8), pointer, intent(inout) :: t_cpa(:,:,:)

      integer :: kkrsz, kkrsz2, kkr2l, ndimrhp,ndimlhp
      integer :: l,m,m3,irecdlm
      integer, parameter :: ksint_sch=ksint_scf_sch
      integer :: st_env
      real(8) :: a,con,rytodu
      complex(8) :: cirl,edu

      type ( IniFile ), pointer :: inFile=>null()
      type ( RS_Str ), pointer :: rs_box=>null()
      type ( KS_Str ), pointer :: ks_box=>null()
      type ( EW_Str ), pointer :: ew_box=>null()
!      type (GF_data), pointer :: gf_box

      integer :: ndrpts,ndcomp,nbasis,nspin
!     integer :: nsub

      type (MPI_data),  pointer :: mpi_box=>null()

      real(8) :: conk
      complex(8), allocatable, save :: conr(:)
      complex(8), allocatable, save :: hplnm(:,:)

      integer :: iprint,L_limit,Lss_max,iv,i_ss,ncpa,nrelv
      integer :: natom
      integer, allocatable :: komp(:)
      real(8), allocatable :: atcon(:,:)

      real(8) :: r_ref(1:size(t_cpa,3))

      integer :: i
!      real(8) :: vkn(3)
      complex(8), allocatable, save :: green(:,:,:)
      real(8), parameter :: cphot = two*inv_fine_struct_const
      real(8) :: t, ptempr
      integer, parameter :: lx=size(lj)/2

      if ( iflag<0 ) then
          if ( allocated(hplnm) ) deallocate(hplnm)
          if ( allocated(conr) ) deallocate(conr)
          return
      end if

      nullify(inFile) ; if ( associated(mecca_state%intfile) )          &
     &  inFile => mecca_state%intfile
      nullify(rs_box) ; if ( associated(mecca_state%rs_box)  )          &
     &  rs_box => mecca_state%rs_box
      nullify(ks_box) ; if ( associated(mecca_state%ks_box) )           &
     &  ks_box => mecca_state%ks_box
      nullify(ew_box) ; if ( associated(mecca_state%ew_box) )           &
     &  ew_box => mecca_state%ew_box
      if ( .not. ( associated(inFile) .and.                             &
     &             associated(rs_box) .and.                             &
     &             associated(ks_box) .and.                             &
     &             associated(ew_box) ) ) then
       call fstop(sname//' :: ERROR :: pointers are not associated')
!     &             associated(gf_box) .and.                             &
      end if
!
! if MPI parallezation for k-points is required, it should be initiated
! in this procedure (or on lower level)
!
      nullify(mpi_box)
      if ( associated(mecca_state%mpi_box) ) then ! to remove possible parallelization
        mpi_box => mecca_state%mpi_box            !  from tau-integration
        nullify(mecca_state%mpi_box)
      end if

      nspin = inFile%nspin
      nbasis = inFile%nsubl
      ndcomp = maxval(inFile%sublat(1:inFile%nsubl)%ncomp)
      ndrpts=0
      do i=1,inFile%nsubl
           ndrpts = max(ndrpts,maxval(inFile%sublat(i)%                 &
     &            compon(1:inFile%sublat(i)%ncomp)%v_rho_box%ndrpts))
      end do

      ndimlhp=(2*lmax+1)*(2*lmax+1)
      if ( inFile%imethod == 3 ) then
          ndimrhp = 1
      else
          ndimrhp=size(ew_box%xrslatij,1)
      end if
      if ( allocated(hplnm) ) then
       if ( size(hplnm,2).ne.ndimlhp ) then
         deallocate(hplnm)
       else if ( size(hplnm,1).ne.ndimrhp ) then
         deallocate(hplnm)
       end if
      end if
      if ( .not.allocated(hplnm) ) then
        allocate(hplnm(ndimrhp,ndimlhp))
!c     ----------------------------------------------------------------
!c               (r**l)*Y_lm(r)   [ M.ge.0]
!c     ----------------------------------------------------------------
       call rlylm(ew_box%xrslatij,ew_box%nrsij,ew_box%ndimrij,lmax,     &
     &            hplnm,ndimrhp)
      end if
!c
!c         set up r-,k-space coefficients
!c     ----------------------------------------------------------------
      kkrsz=(lmax+1)**2
      kkrsz2=kkrsz*kkrsz
      kkr2l=(2*lmax+1)**2
      if ( allocated(conr) ) then
       if ( size(conr).ne.kkr2l ) then
        deallocate(conr)
       end if
      end if
      if ( .not.allocated(conr) ) then
        allocate(conr(1:kkr2l))
!c  calculate r-space coefficents -4.0/sqrt( pi)*2**l (for Ewald)
!c
        a=-four/sqrt(pi)
!c
        m3=0
        do l=0,2*lmax
         con=a*2**l
         cirl=(-dcmplx(zero,one))**l
         do m=-l,l
           m3=m3+1
           conr(m3) = (con/2)*cirl     ! conr() -- is used in ewald-routine only
         enddo
        enddo
      end if
!c     ----------------------------------------------------------------
!c  calculate k-space coefficient in D.U. (for Ewald)
!c          conk=-4*pi/((2*pi/alat)^3*omega),
!c              here omega -- volume of unit cell (real space)
!c
      conk=-(four*pi)/(rs_box%volume*(two*pi/rs_box%alat)**3) ! -(four*pi)/(omegws*dutory**3)
      rytodu = rs_box%alat/(two*pi)
      edu = energy*rytodu**2
      call get_recdlm(edu,irecdlm)
      call gEnvVar('ENV_TAU_RECDLM',.false.,st_env)
      if ( st_env>=0 ) then
        irecdlm = min(st_env,1)
      end if
!c
      call g_atcon(inFile,atcon)
      call g_komp(inFile,komp)
      iprint = inFile%iprint
      nbasis = rs_box%nsubl
      natom = rs_box%natom
      kkrsz=(lmax+1)**2
!DELETE      Lss_max = 1+int(2*sqrt(inFile%Tempr)*maxval(rs_box%rws(1:nbasis)))
!DELETE      L_limit = lmax + Lss_max
!DELETE
!DELETE      call gEnvVar('ENV_L_LIMIT',.false.,iv)
!DELETE      if ( iv > 0 ) then
!DELETE       if ( iprint >=0 ) then
!DELETE        write(6,'('' VARIABLE ENV_L_LIMIT =: '',i4)') iv
!DELETE       end if
!DELETE       L_limit = max(iv,lmax)
!DELETE      end if
!      Lss_max = 1+int(2*sqrt(inFile%Tempr)*maxval(rs_box%rws(1:nbasis)))
      t = zero
      if ( inFile%Tempr > zero ) then
        t = max(1.5d0*inFile%Tempr,inFile%V0(ispin))
      end if
      ptempr = sqrt(2*m_e*t*(one + t/(2*m_e*cphot**2)))
      Lss_max = lminSS + nint(ptempr*maxval(rs_box%rws(1:nbasis)))

!      if ( Lss_max <= lmax ) then
!        L_limit = lmax
!      else if ( inFile%Tempr < 1/(2*ry2eV) ) then
!        L_limit = min(Lss_max,lmax+1)
!      else
!       L_limit = Lss_max
!      end if

      L_limit = lmax + Lss_max
      if ( t > zero ) L_limit = max(L_limit,lx)
      call gEnvVar('ENV_L_LIMIT',.false.,iv)
      if ( iv > 0 ) then
       if ( iprint >=0 ) then
        write(6,'('' VARIABLE ENV_L_LIMIT =: '',i4)') iv
       end if
       L_limit = max(iv,lmax)
      end if

      call gEnvVar('ENV_SSTYPE',.false.,i_ss)
      if ( i_ss >= 0 ) then
       if ( iprint >=0 ) then
        write(6,'('' VARIABLE ENV_SSTYPE =: '',i4)') i_ss
       end if
      else
       if ( inFile%mtasa == 2 ) then
        i_ss = ASA_ss    ! ASA_ss = 1, IS_ss = 0
       else
        i_ss = ASA_ss
       end if
      end if

      r_ref = rs_box%volume/sum(rs_box%numbsubl(1:nbasis))
      r_ref = (r_ref*three/(four*pi))**(one/three)
      if ( inFile%mtasa == 0 ) then
        r_ref = minval(rs_box%rmt(1:nbasis))/                           &
     &            minval(rs_box%rws(1:nbasis)) * r_ref
      end if

      if ( associated(t_cpa) ) then
       if ( size(t_cpa,1).ne.kkrsz .or.                                 &
     &       size(t_cpa,2).ne.kkrsz .or.                                &
     &       size(t_cpa,3).ne.nbasis ) then
        deallocate(t_cpa)
        nullify(t_cpa)
       end if
      end if
      if ( .not.associated(t_cpa) )                                     &
     &   allocate(t_cpa(kkrsz,kkrsz,nbasis))

      if ( associated(t_ss) ) then
       if ( size(t_ss,1).ne.kkrsz .or.                                  &
     &       size(t_ss,2).ne.kkrsz .or.                                 &
     &       size(t_ss,3).ne.nbasis ) then
        deallocate(t_ss)
        nullify(t_ss)
       end if
      end if
      if ( .not.associated(t_ss) )                                      &
     &   allocate(t_ss(kkrsz,kkrsz,ndcomp,nbasis))

      if ( allocated(green) ) deallocate(green)
      allocate(green(1:ndrpts,1:ndcomp,1:nbasis))

      call g_reltype(valence=nrelv)
      call setSStype(i_ss)

!TODO: Dirac SS

! tab-calculation
       call getSSgf(energy,lmax,L_limit,ispin,nrelv,                    &
     &             ndcomp,nbasis,rs_box%alat,inFile%sublat,iprint,      &
     &             green(1:,1:ndcomp,1:nbasis),t_ss,no_gf=0)

       ncpa = inFile%ncpa
! tcpa-calculation

!      ik_begin = mpi_box%ik_begin
!      ik_end = mpi_box%ik_end
!      mpi_box%ik_end = mpi_box%ik_begin-1

       call getMSgf(t_ss,ispin,nrelv,lmax,kkrsz,                        &
     &                  nbasis,rs_box%numbsubl,rs_box%alat,rs_box%aij,  &
     &                  rs_box%itype,rs_box%natom,                      &
     &                  rs_box%if0,rs_box%mapstr,rs_box%mappnt,         &
     &                  size(rs_box%mappnt,1),rs_box%mapij,             &
     &                  komp,atcon,                                     &
     &                  ew_box%xknlat,ew_box%Rksp,                      &
     &                  ew_box%nksn,                                    &
     &                  ks_box%qmesh,ks_box%ndimq,size(ks_box%nqpt),    &
     &                  ks_box%nqpt,                                    &
     &                  ks_box%wghtq,ks_box%lrot,                       &
     &                  ks_box%ngrp,ks_box%kptgrp,ks_box%kptset,        &
     &                  size(ks_box%kptset,1),                          &
     &                  ks_box%kptindx,                                 &
     &                  inFile%sublat(1:nbasis),r_ref,                  &
     &                  rs_box%rws(:),                                  &
     &                  energy,ncpa,                                    &
     &                  ew_box%xrslatij,ew_box%nrsij,ew_box%ndimrij,    &
     &                  ew_box%np2r,ew_box%ndimr,                       &
     &                  ew_box%numbrs,rs_box%ncount,                    &
     &                  ks_box%rot,ks_box%dop,ks_box%nrot,              &
     &                  hplnm,ndimrhp,ndimlhp,                          &
     &                  irecdlm,                                        &
     &                  green(1:,1:,1:),                                &
     &                  inFile%eneta,inFile%enKlim,                     &
     &                  conk,conr,                                      &
     &                  rs_box%rslatt,ks_box%kslatt,rs_box%Rnncut,      &
     &                  rs_box%Rnncut,rs_box%mapsprs,                   & ! here Rsmall=Rnncut
     &                  0,ksint_sch,iprint,inFile%istop,t_cpa)

!       mpi_box%ik_end = i_begin

      if ( allocated(green) ) deallocate(green)
      if ( allocated(atcon) )    deallocate(atcon)
      if ( allocated(komp) )     deallocate(komp)

       if ( associated(mpi_box) ) then
         mecca_state%mpi_box => mpi_box
         nullify(mpi_box)
       end if

      return
!EOC
!
      end subroutine eval1Tcpa

      end module taucalc
