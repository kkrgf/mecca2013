!BOP
!!ROUTINE: eos_mecca
!!INTERFACE:
       subroutine eos_mecca(Neos,def_eos,input_filename)
!!DESCRIPTION:
! This subroutine calculates Equation-Of-State and location of local
! minimum based on fitting procedure providing in {\tt eos\_data} module
!
!!USES:
       use universal_const
       use mecca_types
       use eos_data, ONLY : eosid, EOS
       use input
       use scf_io, only : cpscflog
       use mecca_scf_interface
       use mpi

!!DO_NOT_PRINT
       implicit none
!!DO_NOT_PRINT

!!ARGUMENTS:
       integer, intent(in) :: Neos   ! number of EOS points
       character*(*), intent(in) :: def_eos
       character*(*), intent(in) :: input_filename
!
!!REVISION HISTORY:
! Revised         - A.S. - Feb 2019
! Revised         - A.S. - 2013
! Initial Version - A.S. - Feb 2007
!
!EOP
!
!BOC
       logical, external :: isDigit
       real(8) :: Tempr,T_ry
       integer :: mtasa
       real(8) :: ebot
       real(8) :: delt   ! alat relative step between points,
                         ! alat(j) = a0*(1-(j-1)*delt), j=1:Neos
       type(RunState), target :: mecca_state

       real(8) :: scl(3)
       integer i, j,j0, k, kmin
       integer :: st_env
       character(30) :: c_env
       character(1) :: first
       character(1), parameter :: isoT="T",isoV='V'
       character(1), parameter :: number='N',negnumber='-'

       real(8) :: a0,dv
       integer ne

       integer :: myrank, ierr

       integer, parameter :: nrec=80
       type (IniFile), target :: internalFile
       type (IniFile) :: work_intfile
       character(1200) :: outinfo

!       integer ialat, ilast
       character(16)   :: alatline
       character(nrec) :: filename

       real(8) :: a(Neos+1), FEtot(Neos+1),Etot(Neos+1)
       real(8) :: volume(Neos+1), P(Neos+1), fnc(size(FEtot))
       real(8) :: temper(Neos+1),ef(Neos+1)
       integer :: info(Neos+1)


       real(8), parameter :: RyUnit = ry2eV
       real(8), parameter :: Angstroem = bohr2A

       real(8) :: Vol0, E0, B0, B0Prim, grueneis_par,err
!       real(8) :: entropy, T

       real(8) :: z0,u0,u1
       logical :: log10scale

       logical :: fileexist=.false.,reuse=.false.
       character(17), parameter :: start_potfile='EOS_START.POTFILE'
       character(32) :: eos_ini
       character(:), allocatable  :: genname,tmpname

       call mpi_comm_rank(mpi_comm_world,myrank,ierr)
       if ( Neos .le. 0 ) return

       info = -1
       temper = 0
       a = 0
       ne = Neos

       call read2intFile(trim(input_filename),internalFile,ierr)

       if ( internalFile%nsubl .lt. 1) then
        write(*,*) ' ERROR: nsubl=',internalFile%nsubl,' is not allowed'
        call fstop(' BAD INPUT DATA')
        return
       end if

       mecca_state%intfile => internalFile
       a0 = internalFile%alat
       Tempr = internalFile%Tempr
       j0 = 1

       read(def_eos,'(a1)') first
       if ( isDigit(first) ) then
        first = number
       else if ( first==negnumber ) then
        first = negnumber
       end if
       select case (first)
       case(number,negnumber)
        read(def_eos,*) delt
        if ( delt==0 ) ne = 1
        dv = 1.d0-(1.d0-abs(delt))**3
        dv = sign(dv,delt)
        forall (j=1:ne) a(j) = a0*(1.d0-(j-1)*dv)**(1.d0/3.d0)
        log10scale = .false.
        temper(1:ne) = Tempr
        reuse = .true.
       case(isoT)
        call decode1(def_eos,z0,u0,u1,log10scale)
        call inpeos(z0,u0,u1,log10scale,a(1:ne))    ! input: u0,u1 (compression), output: a(i) compression V0/V (=rho/rho0)
        a(1:ne) = a0 * a(1:ne)**(-1.d0/3.d0)        !        z0 (coeffient)
        temper(1:ne) = Tempr
       case(isoV)
        call decode1(def_eos,z0,u0,u1,log10scale)
        if ( Tempr==0.d0 ) then
         internalFile%Tempr = z0
         Tempr = internalFile%Tempr
         z0 = 1.d0
        end if
        call inpeos(Tempr*z0,u0,u1,log10scale,temper(1:ne))
        temper(ne+1) = zero
        a(:) = a0
        j0 = 0         ! to start from T=0 potential
        reuse = .true.
       case default
        ne = 0
       end select
       genname = trim(internalFile%genName)

!DELETE       if ( Tempr .gt. 0.d0 .and. internalFile%igrid>1 ) then
!DELETE!?        internalFile%eibot = -Tempr*pi*0.5d0
!DELETE        if ( internalFile%npts<1000 ) then
!DELETE         write(6,'(a,e13.5,a,i4,a)') ' ERROR :: Tempr = ',Tempr,        &
!DELETE     &                        ', npts =',internalFile%npts,' <= 1000!'
!DELETE         write(6,*) ' ERROR :: number of E-points for calculation'//    &
!DELETE     &              ' with non-zero T is undefined'
!DELETE         call fstop('BAD INPUT DATA')
!DELETE!         internalFile%npts = internalFile%npts + 8*1000
!DELETE!         write(6,*) ' WARNING :: new value of npts = ',internalFile%npts
!DELETE        end if
!DELETE       end if
!
       inquire(file=trim(start_potfile),EXIST=fileexist)
       if ( fileexist ) then
        internalFile%io(nu_inpot)%name = trim(start_potfile)
        internalFile%io(nu_inpot)%status = 'o'
        if ( first==isoV ) then
         j0=1
         info(ne+1)=0
        end if
       end if

       ebot = internalFile%ebot
       if ( abs(ebot)<epsilon(1.d0) ) then
        internalFile%ebot = 0
        ebot = 0
       end if
       work_intfile = internalFile

       if ( first==isoV .and. .not. fileexist ) then
        internalFile%npts = 0.7*mod(work_intfile%npts,1000)
       end if

        write(6,'(a)') '   Input (alat,T) list:'
        do k=j0,ne
         j = k
         if ( k==0 ) then
            if ( first==isoV ) j = ne+1
         end if
         write(6,'(i5,2x,f13.8,2x,es18.8)') j,a(j),temper(j)
        end do
        write(6,'(/)')

        do k=j0,ne
         j = k
         if ( k==0 ) then
            if ( first==isoV ) j = ne+1
         end if
         mecca_state%fail = .false.
         internalFile%alat = a(j)
         internalFile%Tempr = temper(j)
         internalFile%sublat%rIS = work_intfile%sublat%rIS
!?         internalFile%sublat%rIS = zero
         internalFile%ebot = work_intfile%ebot
         call mpi_barrier(mpi_comm_world,ierr)
         write(eos_ini,'(i0)') j
         tmpname = trim(adjustl(genname))//'.'//trim(adjustl(eos_ini))
         call setGenName(tmpname,internalFile,1)
!
         if ( j>1 ) then
          if ( reuse ) then
           if ( info(j-1)==0 ) then
            if ( first==isoV ) then
             inquire(file=trim(filename),EXIST=fileexist)
             if ( .not.fileexist ) then
              call fstop(filename//' is not found')
             end if
            else
             inquire(file=trim(start_potfile),EXIST=fileexist)
            end if
           else
            inquire(file=trim(start_potfile),EXIST=fileexist)
           end if
          else
           fileexist = .false.
          end if
         else if ( j==1 ) then
          if ( first==isoV ) then
           if ( info(ne+1)==0 ) then
            inquire(file=trim(start_potfile),EXIST=fileexist)
            filename = start_potfile
           end if
          else if ( first==number .or. first==negnumber ) then
           inquire(file=trim(start_potfile),EXIST=fileexist)
           filename = start_potfile
          end if
         end if
         if ( fileexist ) then
          internalFile%io(nu_inpot)%name=trim(filename)                 &
          internalFile%io(nu_inpot)%status = 'o'
         else
          internalFile%io(nu_inpot)%status = 'n'
         end if
         outinfo = ''
!DEBUGPRINT
         write(6,'(/a/)')                                                 &
     & ' Input pot-file name: '//trim(internalFile%io(nu_inpot)%name)// &
     & ', status: '//internalFile%io(nu_inpot)%status
!DEBUGPRINT
!=======================================================================
         call mecca_scf(mecca_state,outinfo)
!=======================================================================
         read(outinfo,*,err=100) volume(j),FEtot(j),Etot(j),P(j)        &
     &,                           T_ry,ef(j)
!          read(outinfo,*,err=100) volume(j),FEtot(j),Etot(j),P(j)
!          ef(j) = internalFile%etop
         info(j) = mecca_state%info
         Tempr = temper(j)
!DEBUGPRINT
         if (abs(Tempr-T_ry) > T_ry * 1.d-6 ) then
          write(6,'(a)')'WARNING :: different T in the same calculation'
          write(6,'(11x,''Tempr = '',es20.10,3x,''T_Ry = '',es20.10)')  &
     &                                Tempr, T_ry
         end if
!DEBUGPRINT
         write(6,'(/)')
         write(6,'(9x,''a'',12x,''FEtot'',15x,''V'',12x,''P'')')
         if ( info(j)==0 ) then
            if ( Tempr>0 ) then
            write(6,11) a(j), FEtot(j),volume(j),P(j),'%%eosdata',      &
     &                                T_ry,Etot(j),ef(j)
            else
            write(6,11) a(j), FEtot(j),volume(j),P(j),'%%eosdata',      &
     &                                T_ry,zero,ef(j)
            end if
         else
             if ( Tempr>0 ) then
            write(6,11) a(j), FEtot(j),volume(j),P(j),'%eosdata',       &
     &                                T_ry,Etot(j),ef(j),info(j)
             else
           write(6,11) a(j), FEtot(j),volume(j),P(j),'%eosdata',        &
     &                                T_ry,zero,zero,info(j)
             end if
         end if
!
         if ( mecca_state%fail ) then
          write(*,*) 'eos_mecca :: ERROR fail in mecca_scf'
         else
          if ( ne > 1 .and. temper(1).ne.temper(ne) ) then
           write(alatline,'(f6.3,''T'',I0)') internalFile%alat,j
          else
           write(alatline,'(f6.3)') internalFile%alat
          end if
          eos_ini = trim(adjustl(alatline))//'.eos_ini'
!
          if(myrank == 0) then    !***************************************
           filename = trim(mecca_state%intfile%genName)//'.info.alat='//&
     &                                          trim(adjustl(alatline))
           call cpscflog(mecca_state%intfile%io(nu_info)%name,1,        &
     &                 trim(filename))
           filename = trim(mecca_state%intfile%genName)//'.pot.alat='// &
     &                                          trim(adjustl(alatline))
           call cptxtfile(mecca_state%intfile%io(nu_outpot)%name,       &
     &                 trim(filename))
!          if(myrank == 0) then
           if (info(j) == 0 ) then
            internalFile%io(nu_inpot)%name = trim(filename)
            internalFile%io(nu_inpot)%status = 'o'
           end if
           call saveInput(internalFile,trim(eos_ini))
           if ( first==isoV .and. k==0 ) then
           call cptxtfile(mecca_state%intfile%io(nu_outpot)%name,       &
     &                 trim(start_potfile))
           end if
!          end if
!
          end if          !***********************************************
          if ( info(j)==0 ) then
           filename = trim(mecca_state%intfile%genName)//'.pot.alat='// &
     &                                          trim(adjustl(alatline))
          else
            if ( first==isoV ) then
             filename = trim(start_potfile)
            end if
          end if
         end if
         if ( first==isoV .and. k==0 ) then
           internalFile%npts = work_intfile%npts
         end if

        end do

       write(6,'(''************************************************'')')
       fnc = 0
       if(myrank == 0 .and. Neos > 1) then
        ne = 0
        write(6,'(//)')
        write(6,'(3x,f10.7,'' #eosdata'')') volume(1)/a(1)**3
        write(6,'(7x,''a'',10x,''FEtot''11x,''V'',10x,''P'')')
        do i=1,Neos
         if ( info(i)==0 ) then
11        format(f10.7,1x,f16.6,1x,f14.6,1x,f16.4,1x,a,1x,3f16.7,i4)
          Tempr = temper(i)
          if ( Tempr>0 ) then
           write(6,11) a(i), FEtot(i),volume(i),P(i),'#eosdata',        &
     &                                Tempr,Etot(i),ef(i)
          else
           write(6,11) a(i), FEtot(i),volume(i),P(i),'#eosdata',        &
     &                                Tempr
          end if
          ne = ne+1
          if ( ne<i ) then
           a(ne) = a(i)         ; a(i)     = 0
           Etot(ne) = Etot(i)   ; Etot(i)  = 0
           FEtot(ne) = FEtot(i)   ; FEtot(i)  = 0
           volume(ne)=volume(i) ; volume(i)= 0
           P(ne)=P(i)           ; P(i)     = 0
           info(ne)=info(i)     ; info(i)  = -1
          end if
         else
!          if ( Tempr>0 ) then
!           write(6,11) a(i), FEtot(i),volume(i),P(i),'%eosdata',        &
!     &                                Tempr,Etot(i),ef(i),info(i)
!          else
!           write(6,11) a(i), FEtot(i),volume(i),P(i),'%eosdata',        &
!     &                                Tempr,zero,zero,info(i)
!          end if
          a(i)     = 0
          Etot(i)  = 0
          FEtot(i)  = 0
          volume(i)= 0
          P(i)     = 0
          info(i)  = -1
         end if
        end do
        if ( ne < Neos ) then
         write(6,'(a,i0,a)') 'WARNING: there are ',Neos-ne,             &
     &                                ' unconverged/failed eos-points.'
        end if

        if ( ne > 2 .and. volume(1).ne.volume(ne) ) then
         do i=1,3
          if ( Neos==3 .and. i.ne.1 ) cycle
          if ( Neos==4 .and. i==3 ) cycle
          fnc = FEtot
          call EOS(i,Angstroem,RyUnit,ne,volume,fnc                     &
     &,                    Vol0,E0,B0,B0Prim,err)
          a0 = a(1)*(Vol0/volume(1))**(1.d0/3.d0)
          if ( i==1 ) then
           write(6,'(2x,''a0= '',f6.3,'' Bohr'',2x,                     &
     &               ''E0= '',f14.5,'' Ry'', 2x,                        &
     &               ''B0= '',f8.3,'' Mbar'',2x,                        &
     &               ''B0''''= '','' ---'',6x,                          &
     &               ''V0= '',f10.5,1x,a                                &
     &           )')                                                    &
     &     a0,E0,B0,Vol0,':'//trim(eosid(i))
          else if ( i==2 ) then
           write(6,'(2x,''a0= '',f6.3,'' Bohr'',2x,                     &
     &               ''E0= '',f14.5,'' Ry'', 2x,                        &
     &               ''B0= '',f8.3,'' Mbar'',2x,                        &
     &               ''B0''''= '',f8.2,2x,                              &
     &               ''V0= '',f10.5,1x,a                                &
     &           )')                                                    &
     &     a0,E0,B0,B0Prim,Vol0,':'//trim(eosid(i))
          else if ( i==3 ) then
           if  ( Neos>5 ) then
            grueneis_par = B0Prim/dble(2) - dble(5)/dble(6)
            write(6,'(2x,''a0= '',f6.3,'' Bohr'',2x,                    &
     &                ''E0= '',f14.5,'' Ry'', 2x,                       &
     &                ''B0= '',f8.3,'' Mbar'',2x,                       &
     &                ''B0''''= '',f8.2,2x,                             &
     &                ''V0= '',f10.5,1x,a                               &
     &            ,2x,''grune= '',f9.3,1x,a                             &
     &            )')                                                   &
     &      a0,E0,B0,B0Prim,Vol0,':'//trim(eosid(i)),grueneis_par
           else
            write(6,'(2x,''a0= '',f6.3,'' Bohr'',2x,                    &
     &               ''E0= '',f14.5,'' Ry'', 2x,                        &
     &               ''B0= '',f8.3,'' Mbar'',2x,                        &
     &               ''B0''''= '',f8.2,2x,                              &
     &               ''V0= '',f10.5,1x,a                                &
     &           )')                                                    &
     &      a0,E0,B0,B0Prim,Vol0,':'//trim(eosid(i))
            if ( err>1.d-4 ) write(6,'(6x,'' Warning: B0prim''          &
     & ,'' may be inaccurate, fitting mse = '',e9.2,'' mRy'')') err*1.d3
           end if
          end if
         end do
        end if
       end if  ! myrank-if

       call mpi_barrier(mpi_comm_world,ierr)
       write(6,'(//)')
       return

100    write(6,*) ' Reading error from internal file, j=',j
       call fstop('eos_mecca')
       return

       contains

       subroutine decode1(def,z0,u0,u1,log10scale)
! to decode lines like "x:2.5,1.e2,20" and "yy:2.5,1.e2,2.d8,log10scale"
       implicit none
       character(len=*), intent(in) :: def
       real(8), intent(out) :: z0,u0,u1
       logical, intent(out) :: log10scale
       character(10) :: logtag='log10scale',tag
       integer :: i1,i2,lendef

       z0=0; u0=0; u1=0; log10scale=.true.
       lendef = len_trim(def)
       i1 = index(def,':') + 1
       if ( i1>lendef ) go to 10
       i2 = i1-1+index(def(i1:),',')
       read(def(i1:i2-1),*) z0
       i1 = i2+1
       if ( i1>lendef ) go to 10
       i2 = i1-1+index(def(i1:),',')
       read(def(i1:i2-1),*) u0
       i1 = i2+1
       if ( i1>lendef ) go to 10
       i2 = i1-1+index(def(i1:),',')
       if ( i2<i1 ) then
        read(def(i1:lendef),*) u1
        log10scale = .false.
       else
        read(def(i1:i2-1),*) u1
        read(def(i2+1:lendef),*) tag
        if ( tag == logtag ) then
         log10scale = .true.
        else
         goto 10
        end if
       end if
       return
10     continue
       call fstop('INCORRECT INPUT SYNTAX')
       end subroutine decode1

       subroutine inpeos(z0,u0,u1,log10scale,u)
       implicit none
       real(8), intent(in) :: z0,u0,u1
       logical, intent(in) :: log10scale
       real(8), intent(out) :: u(:)
!
       real(8), parameter :: ln10=log(10.d0)
       integer :: j,n
       real(8) :: du
!
       n = size(u)
       if ( n<1 ) return
       u = 0
       if ( n==1 ) then
        u(1) = z0
        return
       end if
!
       if ( log10scale ) then
        u(1) = z0*u0
        du = ln10*(log10(u1)-log10(u0))/(n-1)
        forall (j=2:n) u(j) = u(1)*exp((j-1)*du)
       else
        du = z0 * (u1-u0)/(n-1)
        forall (j=2:n) u(j) = u(1)+(j-1)*du
       end if

       return
       end subroutine inpeos

       end subroutine eos_mecca
