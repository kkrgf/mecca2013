!BOP
!!ROUTINE: fccAlat
!!INTERFACE:
      real*8 function fccAlat(z)
!!DESCRIPTION:
! returns fcc-lattice parameter based (mostly) on experimental 
! volume value (in Bohr) or radius in crystalls      
!   (from various internet sources)
!
!!ARGUMENTS:
      real*8 z   ! atomic number
!!REVISION HISTORY:
! Initial Version - A.S. - 2005
! Extended up to 102 elements - A.S. - 2020
!!REMARKS:
!   0 < z < 103
!EOP
!
!BOC
      integer nmax
      parameter (nmax=102)
      real*8 lattparam(nmax)/                                           &
     &     0.21d0                                                       & ! D
     &,   10.02d0                                                       & ! He 2
     &,    8.35d0                                                       & ! Li
     &,    6.02d0                                                       & ! Be
     &,    1.34d0                                                       & ! B guess 5.92
     &,    1.60d0                                                       & ! C guess 6.20
     &,    1.60d0                                                       & ! N guess 7.95
     &,    6.63d0                                                       & ! O guess 7.95
     &,    6.25d0                                                       & ! F guess 7.95
     &,   10.02d0                                                       & ! Ne-10
     &,   10.20d0                                                       & ! Na
     &,    8.56d0                                                       & ! Mg
     &,    7.65d0                                                       & ! Al
     &,    8.14d0                                                       & ! Si
     &,    9.14d0                                                       & ! P 
     &,    8.86d0                                                       & ! S 
     &,    7.91d0                                                       & ! Cl
     &,   10.02d0                                                       & ! Ar-18
     &,   12.67d0                                                       & ! K
     &,   10.50d0                                                       & ! Ca
     &,    8.77d0                                                       & ! Sc
     &,    7.79d0                                                       & ! Ti
     &,    7.20d0                                                       & ! V
     &,    6.87d0                                                       & ! Cr
     &,    6.92d0                                                       & ! Mn
     &,    6.83d0                                                       & ! Fe
     &,    6.67d0                                                       & ! Co
     &,    6.66d0                                                       & ! Ni
     &,    6.82d0                                                       & ! Cu
     &,    7.44d0                                                       & ! Zn
     &,    8.09d0                                                       & ! Ga
     &,    8.49d0                                                       & ! Ge
     &,    8.37d0                                                       & ! As
     &,    9.04d0                                                       & ! Se
     &,   10.47d0                                                       & ! Br
     &,   10.01d0                                                       & ! Kr-36
     &,   13.30d0                                                       & ! Rb
     &,   11.56d0                                                       & ! Sr
     &,    9.62d0                                                       & ! Y
     &,    8.56d0                                                       & ! Zr
     &,    7.86d0                                                       & ! Nb
     &,    7.49d0                                                       & ! Mo
     &,    7.26d0                                                       & ! Tc
     &,    7.15d0                                                       & ! Ru
     &,    7.19d0                                                       & ! Rh
     &,    7.35d0                                                       & ! Pd
     &,    7.72d0                                                       & ! Ag
     &,    8.35d0                                                       & ! Cd
     &,    8.90d0                                                       & ! In
     &,    9.00d0                                                       & ! Sn
     &,    9.35d0                                                       & ! Sb
     &,    9.71d0                                                       & ! Te
     &,   10.49d0                                                       & ! I
     &,    9.99d0                                                       & ! Xe-54
     &,   14.71d0                                                       & ! Cs
     &,   11.97d0                                                       & ! Ba
     &,   10.04d0                                                       & ! La
     &,    9.75d0                                                       & ! Ce f-block
     &,    9.77d0                                                       & ! Pr
     &,    9.73d0                                                       & ! Nd
     &,    9.62d0                                                       & ! Pm
     &,    9.64d0                                                       & ! Sm
     &,   10.91d0                                                       & ! Eu
     &,    9.63d0                                                       & ! Gd
     &,    9.53d0                                                       & ! Tb
     &,    9.48d0                                                       & ! Dy
     &,    9.44d0                                                       & ! Ho
     &,    9.38d0                                                       & ! Er
     &,    9.33d0                                                       & ! Tm
     &,   10.40d0                                                       & ! Yb
     &,    9.27d0                                                       & ! Lu 71
     &,    8.44d0                                                       & ! Hf
     &,    7.87d0                                                       & ! Ta
     &,    7.52d0                                                       & ! W
     &,    7.35d0                                                       & ! Re
     &,    7.23d0                                                       & ! Os
     &,    7.27d0                                                       & ! Ir
     &,    7.41d0                                                       & ! Pt
     &,    7.70d0                                                       & ! Au
     &,    8.72d0                                                       & ! Hg
     &,    9.18d0                                                       & ! Tl
     &,    9.35d0                                                       & ! Pb
     &,    9.87d0                                                       & ! Bi
     &,   10.02d0                                                       & ! Po
     &,   10.05d0                                                       & ! At
     &,   10.08d0                                                       & ! Rn-86
     &,   15.94d0                                                       & ! Fr
     &,   12.26d0                                                       & ! Ra
     &,   10.04d0                                                       & ! Ac
     &,    9.61d0                                                       & ! Th f-block
     &,    8.53d0                                                       & ! Pa
     &,    8.26d0                                                       & ! U
     &,    8.07d0                                                       & ! Np
     &,    8.20d0                                                       & ! Pu
     &,    9.27d0                                                       & ! Am
     &,    9.36d0                                                       & ! Cm
     &,    9.08d0                                                       & ! Bk
     &,    9.07d0                                                       & ! Cf
     &,    9.00d0                                                       & ! Es Unknown
     &,    9.00d0                                                       & ! Fm Unknown
     &,    9.00d0                                                       & ! Md Unknown
     &,    9.00d0                                                       & ! No Unknown
     &/
       integer iz
       iz = nint(z)
       if (iz.ge.1.and.iz.le.nmax) then
        fccAlat = lattparam(nint(z))
       else
        fccAlat = 0.d0
       end if
       return
!EOC
       end
