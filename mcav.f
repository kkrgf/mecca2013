!BOP
!!ROUTINE: mcav
!!INTERFACE:
      subroutine mcav(atcon,xab,xav,kkrsz,komp,                         &
     &                iprint,istop)
!!DESCRIPTION:
! calculates concentration average of complex square matrices {\tt xab} 
! and puts result in {\tt xav}
!
!!USES:
      use mtrx_interface

!!DO_NOT_PRINT
      implicit none
!!DO_NOT_PRINT

!!ARGUMENTS:
      integer, intent(in)     ::  komp
      real(8), intent(in)     ::  atcon(komp)
      complex(8), intent(in)  :: xab(:,:,:)  ! (ipkkr,ipkkr,komp)
      complex(8), intent(out) :: xav(:,:)    ! (ipkkr,kkrsz)
      integer, intent(in)     :: kkrsz,iprint
      character(10), intent(in) ::  istop
!!REVISION HISTORY:
! Initial version - bg & gms - Nov 1991
! Adapted - A.S. - 2013
!EOP
!
!BOC
!c
!c
      complex(8) :: zatcon
      integer ic,i,j
      character  sname*10
      parameter (sname='mcav')
      xav(1:kkrsz,1:kkrsz) = 0
!c     ==============================================================
!c     average over components.......................................
      do ic=1,komp
       zatcon = dcmplx(atcon(ic),0.d0)
       do i=1,kkrsz
!cab        call zaxpy(kkrsz,zatcon,xab(1,i,ic),1,xav(1,i),1)
         xav(1:kkrsz,i) = xav(1:kkrsz,i) + zatcon*xab(1:kkrsz,i,ic)
       enddo
      enddo
!c     ==============================================================
!c     print if needed...............................................
      if (iprint.ge.1) then
         write(6,'('' mcav:: kkrsz,komp :'',2i5)') kkrsz,komp
         call wrtmtx(xav,kkrsz,istop)
      endif
!c
      if(istop.eq.sname) then
         call fstop(sname)
      endif
!c
      return
!EOC
      end subroutine mcav
