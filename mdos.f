!BOP
!!ROUTINE: mdos
!!INTERFACE:
      subroutine mdos(dost,zz,zj,w1,kkrsz,iprint,istop)
!!DESCRIPTION:
! calculates complex density of states {\tt dost(:)}
!

!!DO_NOT_PRINT
      implicit none
!!DO_NOT_PRINT

!!ARGUMENTS:
      complex(8), intent(out) :: dost(0:)
      integer, intent(in) ::    kkrsz
      complex(8), intent(in) :: zz(:,:)  ! (kkrsz,kkrsz)
      complex(8), intent(in) :: zj(:,:)  ! (kkrsz,kkrsz)
      complex(8), intent(in) :: w1(:,:)  ! (kkrsz,kkrsz)
      integer, intent(in) ::    iprint
      character(10), intent(in) :: istop
!!AUTHOR:
! w.a.s.,jr - sept. 1, 1992
!EOP
!
!BOC
      character  sname*10
      parameter (sname='mdos')
      integer    i,i1,i2
      integer    j,jstart,jstop
!c      integer    istart
!c      integer    iend
!c
!      complex*16 dos(kkrsz*kkrsz)
!c      complex*16 dosspd
!c     --------------------------------------------------------------
!CAB      call zeroout(dos,2*ipkkr*ipkkr)
!c     --------------------------------------------------------------

!c SUFFIAN, resolve into s,p,d,f,etc.
!C      i = 0
!C      do i1=1,kkrsz
!C      do i2=1,kkrsz
!C        i = i+1
!C        dos(i) = zz(i2,i1)*w1(i2,i1) + zj(i2,i1)
!C
!C
!CC        if( iprint .ge. 2 ) then
!CC          if(abs( dos(i) ) .gt. 1.0d-06 ) then
!CC           write(6,'(i3,3(1x,2(d12.5,1x)))')
!CC    *           i,zz(i2,i1),w1(i2,i1),zj(i2,i1)
!CC          endif
!CC        endif
!C
!C      enddo
!C      enddo

!C      dost(0)=czero
!C      do i=1,kkrsz*kkrsz
!C        dost(0) = dost(0) + dos(i)
!C      enddo

!c
      if( iprint .ge. 2 ) then
       write(6,'(1x,a,i5)') trim(sname)//': kkrsz ',kkrsz
       write(6,'(1x,a)') trim(sname)//':tauab '
       write(6,'(2(i5,2d12.5))') ( i,w1(i,i),i=1,kkrsz)
       write(6,'(1x,a)') trim(sname)//': pzz'
       write(6,'(2(i5,2d12.5))') ( i,zz(i,i),i=1,kkrsz)
       write(6,'(1x,a)')  trim(sname)//': pzj'
       write(6,'(2(i5,2d12.5))') ( i,zj(i,i),i=1,kkrsz)
      endif
!c

      dost(0:) = 0
      i=0
      do i2=1,kkrsz
       dost(i) = dost(i) +                                              &
     &                 sum(zz(i2,1:kkrsz)*w1(i2,1:kkrsz)+zj(i2,1:kkrsz))
       if ( i2 == (i+1)**2 ) then
          i = i+1
       end if 
      end do

!c     ==============================================================
!c     if( iprint .eq. 2 ) then
!c       dosspd = czero
!c       do i = 1,9
!c         istart = ( i - 1 )*kkrsz + 1
!c         iend = istart + 8
!c         do j = istart, iend
!c           dosspd = dosspd + dos(j)
!c         enddo
!c       enddo
!c       write(6,'('' spd-states '',2d16.7)') dosspd
!c     endif
!c     ==============================================================

!c
      if( iprint .eq. 1 ) then
        write(6,'('' mdos: dost '',2d12.5)') dost
        call flush(6)
      endif
!c
!c     ==============================================================
!c
      if(istop.eq.sname) then
        call fstop(sname)
      endif
!c
      return
!EOC
      end subroutine mdos
