!BOP
!!MODULE: potential
!!INTERFACE:
      module potential
!!DESCRIPTION:
! calculation of potential and total energy
!

!!USES:
      use mecca_constants
      use mecca_types
      use mecca_interface
      use mecca_run
      use gfncts_interface
      use xc_mecca, only : xc_mecca_pointer,gXCmecca
      use xc_mecca, only : gInfo_libxc,xc_rel
      use xc_mecca, only : xc_alpha,xc_alpha2
      use xc_mecca, only : defXCforES,undefXCforES
      use xc_mecca, only : xc_setpar,ionizpot,X_GGA_vLBs

!!DO_NOT_PRINT
      implicit none
      private
!!DO_NOT_PRINT

!!PUBLIC MEMBER FUNCTIONS:
      public :: calcPotential
      public :: newpot, getXCpot
!!PRIVATE MEMBER FUNCTIONS:
! subroutine calcPot
! subroutine calcPot0

!!REVISION HISTORY:
! Initial Version - A.S. - 2013
!EOP
!
!BOC

      interface
       subroutine getVPmltpl(mecca_state,nsub,ic,rhor2pi4,Qint)
       use mecca_types, only : RunState,IntrstlQ
       implicit none
       type(RunState), intent(in), target :: mecca_state
       integer, intent(in) :: nsub,ic
       real(8), intent(in) :: rhor2pi4(:,:)
       type(IntrstlQ) :: Qint
       end subroutine getVPmltpl
      end interface
!
      interface
        subroutine genpot(xvalws,qtotmt,ztotss,qcorss,                  &
     &                  rho,vrnew,ivar_mtz,                             &
     &                  vmtzup,omegws,alat,                             &
     &                  itype,numbsub,nsublat,rslatt,xyz,               &
     &                  rr,xr,jmt,rmt_true,r_circ,                      &
     &                    vp_box,                                       &
     &                  atcon,komp,nbasis,nspin,nMM,iexch,              &
     &                  excort,qintex,emad,emadp,emtc,                  &
     &                  vdif,Rnn,Eccsum,mtasa,iprint,istop)
        use mecca_types, only : VP_data
        implicit none
        real(8), intent(in)  :: xvalws(:,:,:)    ! (ipcomp,ipsublat,ipspin)
        real(8), intent(in)  :: qtotmt(:,:,:)    ! (ipcomp,ipsublat,ipspin)
        real(8), intent(in)  :: ztotss(:,:)  ! (ipcomp,ipsublat)
        real(8), intent(in)  :: qcorss(:,:,:)  ! (ipcomp,ipsublat,nspin)
        real(8), intent(inout)  :: rho(:,:,:,:)  ! (iprpts,ipcomp,ipsublat,ipspin)
        real(8), intent(out) :: vrnew(:,:,:,:)  ! (iprpts,ipcomp,ipsublat,ipspin)
        integer, intent(in)  :: ivar_mtz
        real(8), intent(out) :: vmtzup
        real(8), intent(in)  :: omegws  ! cell volume
        real(8), intent(in)  :: alat
        integer, intent(in)  :: nbasis,itype(nbasis)
        integer, intent(in)  :: nsublat,numbsub(nsublat)
        real(8)              :: rslatt(3,3)
        real(8), intent(in)  :: xyz(:,:)  ! (3,nbasis)
        real(8), intent(in)  :: rr(:,:,:)  ! (iprpts,ipcomp,nsublat)
        real(8), intent(in)  :: xr(:,:,:)  ! (iprpts,ipcomp,nsublat)
        integer, intent(in)  :: jmt(:,:)  ! (ipcomp,nsublat)
        real(8), intent(in)  :: rmt_true(:,:)  ! (ipcomp,nsublat)
        real(8), intent(in)  :: r_circ(:,:)  ! (ipcomp,nsublat)
        type(VP_data), intent(in) :: vp_box
        real(8), intent(in)  :: atcon(:,:)   ! (ipcomp,ipsublat)
        integer, intent(in)  :: komp(nsublat)
        integer, intent(in)  :: nspin,nMM,iexch
        real(8), intent(out) :: excort(:)
        real(8), intent(out) :: qintex(:)
        real(8), intent(out) :: emad   ! Madelung (ES lattice) energy
        real(8), intent(out) :: emadp  ! Madelung pressure contribution
        real(8), intent(out) :: emtc   ! MT-correction for ASA
        real(8), intent(out) :: vdif
        real(8), intent(in)  :: Rnn(nsublat)
        real(8), intent(out) :: Eccsum
        integer, intent(in)  :: mtasa,iprint
        character(10)        :: istop
        end subroutine genpot
!
      end interface
!
!***********************************************************************

!      integer, parameter :: mecca_ames=0
      integer, external :: indRmesh
      real(8), external :: gInterstlRho1
      real(8), external :: scr_mfcc_const

      integer :: ierr
      real(8), parameter :: pi4=four*pi,third=one/three,half=one/two

      integer, public, save :: exc_type=-1
      integer, parameter, public :: max_exctype=6
!      integer, parameter, public :: exc_type_dflt=2
      integer, parameter, public :: exc_type_dflt=4
!!other implemented types: 0 (Vdif=0), 1, 2(rho0_up/dn, global), 3, 4(rho0_up/dn, local), 5, 6
!!see subroutine updateV0

!EOC
      contains

!BOP
!!IROUTINE: calcPotential
!!INTERFACE:
      subroutine calcPotential( iflag, mecca_state, do_etot )
!!DESCRIPTION:
! this subroutine is the main module procedure, which
! calculates potentials and total energy
!
!!USES:
      use scf_io,   only : sCurrParams

!!DO_NOT_PRINT
      implicit none
!!DO_NOT_PRINT

!
!!ARGUMENTS:
      integer, intent(in) :: iflag ! to choose between mecca_ames and mecca_2013
      type(RunState), intent(inout), target :: mecca_state
      logical, intent(in) :: do_etot
!
!!REVISION HISTORY:
! Initial Version - A.S. - 2013
!EOP
!
!BOC
      integer st_env
      type(xc_mecca_pointer) :: libxc_p
      character(512) :: xcdescr
      character(48)  :: relat

      call gXCmecca(mecca_state%intfile%iXC,libxc_p)   ! to get xc_mecca_pointer
      call gInfo_libxc(libxc_p,xcdescr)
      call sCurrParams(xc_descr=xcdescr)
      if ( xc_rel ) then
        relat = ' # *  REL (if Slater exchange is in use)  *'
      else
        relat = '*'
      end if
      call gXCmecca(-1,libxc_p)   ! to release xc_mecca_pointer
      if ( mecca_state%intfile%iprint >=0 ) then
       write(6,'(/a/)') ' '//trim(xcdescr)//trim(relat)
      end if
!
      call gEnvVar('ENV_RHO0',.true.,st_env)
      if ( st_env == 1 ) then
       mecca_state%intfile%rho0 = max(zero,gInterstlRho1(0))  ! rho0 = minval(rho(r))
      else if ( st_env == 2 ) then
       mecca_state%intfile%rho0 = zero
      else
       mecca_state%intfile%rho0 = max(zero,gInterstlRho())   ! rho0 = averag(rho(r))
 !
       call gEnvVar('ENV_ATOMSS',.true.,st_env)
       if ( st_env==1 ) then
!        mecca_state%intfile%rho0 = zero
        mecca_state%intfile%rho0 = max(zero,gInterstlRho1(0))  ! rho0 = minval(rho(r))
       end if
 !
      end if

      select case (iflag)
      case(mecca_ames)
       call calcPot0( mecca_state, do_etot )
      case(mecca_2013)
       call calcPot( mecca_state, do_etot )
      end select

      call gEnvVar('ENV_ATOMSS',.false.,st_env)
      if ( st_env==1 ) then
!        mecca_state%intfile%rho0 = zero
        mecca_state%intfile%V0 = mecca_state%intfile%V0 -               &
     &                             sum(mecca_state%intfile%V0)
       end if

      return
!EOC
      end subroutine calcPotential

!BOP
!!IROUTINE: calcPot
!!INTERFACE:
      subroutine calcPot( mecca_state, do_etot )
!!DESCRIPTION:
! mecca\_2013 version (with PBC and Coulomb shape correction)
!
!!USES:
!
!!PRIVATE MEMBER FUNCTIONS:
!  subroutine set_rhodata
!  subroutine genpot_sph
!  subroutine genpot_pbc
!  subroutine mfcc_corr
!  subroutine updateV0
!  subroutine calcTotEn
!  subroutine coulombShapeCorr
!!REVISION HISTORY:
! Initial Version - A.S. - 2013
!EOP
!
!BOC
      implicit none
      type(RunState), target :: mecca_state
      logical, intent(in) :: do_etot

      type(IniFile),     pointer :: inFile
      type(Sublattice),  pointer :: sublat
      type(VP_data),    pointer :: vp_box
      type(Work_data),   pointer :: work_box    ! output (potential,energy)
      type(xc_mecca_pointer) :: libxc_p
      type(IntrstlQ), allocatable :: Qintr(:,:)
!      type(IntrstlQ), allocatable :: Q0intr(:,:)  ! rho = rho0 for r>SphR

      real(8), allocatable :: xr(:,:,:)
      real(8), allocatable :: rho(:,:,:,:)
      real(8), allocatable :: potmad(:)
      real(8), allocatable :: fxyzES(:,:)
      real(8), allocatable :: V_mfcc(:,:)
      real(8), allocatable :: E_mfcc(:,:)
      real(8), allocatable :: potShC(:,:)
      real(8) :: e0_intrC(2),enShC  ! e0_intrC(1) is energy, e0_intrC(2) is pressure

      logical :: lVP
      integer :: nsub,ndrpts,ndcomp,nsublat,nspin,komp

      inFile   => mecca_state%intfile
      vp_box => mecca_state%vp_box
      work_box => mecca_state%work_box

      if ( associated(mecca_state%vp_box) ) then
       lVP = vp_box%nF>0
      else
       lVP = .false.
      end if
      if ( .not. lVP ) then
        write(*,'(/a,i4/)') ' ERROR: VP-integration is not activated'   &
     &                   // ', mtasa=',inFile%mtasa
        call fstop(' calcPot: FATAL ERROR ')
      end if

      call gXCmecca(inFile%iXC,libxc_p)   ! to get xc_mecca_pointer

      call g_ndims(inFile,ndrpts,ndcomp,nsublat,nspin)
      if ( min(ndrpts,ndcomp,nsublat,nspin) <1 ) then
        write(*,*) ' ndrpts,ndcomp,nsublat,nspin: ',                    &
     &                                      ndrpts,ndcomp,nsublat,nspin
        call fstop(' calcPot0: FATAL ERROR ')
      end if
      allocate(Qintr(1:ndcomp,1:nsublat))
!      allocate(Q0intr(1:ndcomp,1:nsublat))
      allocate(xr(1:ndrpts,1:ndcomp,1:nsublat))
      allocate(rho(1:ndrpts,1:ndcomp,1:nsublat,1:nspin))
      allocate(potmad(1:nsublat))
      allocate(fxyzES(1:3,1:nsublat))
      allocate(V_mfcc(1:ndcomp,1:nsublat))
      allocate(E_mfcc(1:ndcomp,1:nsublat))
      allocate(potShC(1:ndcomp,1:nsublat))
      if ( .not.allocated(mecca_state%work_box%vr) ) then
        allocate (mecca_state%work_box%vr(1:ndrpts,1:ndcomp,1:nsublat,  &
     &                                                        1:nspin))
      end if
!
!=======================================================================
!
      do nsub=1,inFile%nsubl
       sublat => inFile%sublat(nsub)
       sublat%indx = nsub
       komp = sublat%ncomp
       call set_rhodata(sublat,mecca_state,                             &
     &  xr(:,1:komp,nsub),rho(:,1:komp,nsub,1:nspin),Qintr(1:komp,nsub))
       call genpot_sph(sublat,mecca_state,                              &
     &                 xr(:,1:komp,nsub),rho(:,1:komp,nsub,1:nspin))
       if ( inFile%freeze_core == 1 ) then
        write(*,*) ' FREEZE_CORE IMPLEMENTATION IS NOT FINISHED YET!'
       end if
      end do
!
!=======================================================================
!
      call genpot_pbc(mecca_state,potmad,e0_intrC,fxyzES)
!
!=======================================================================

      call coulombShapeCorr(mecca_state,enShC,potShC)
!DEBUGPRINT
!      write(6,*) ' DEBUG enShC=',enShC
!      write(6,*) ' DEBUG POTSHC=',potShC
!DEBUGPRINT
      e0_intrC(1) = e0_intrC(1) + enShC
      e0_intrC(2) = e0_intrC(2) + enShC
      call mfcc_corr(mecca_state,V_mfcc,E_mfcc)
      V_mfcc = V_mfcc + potShC  ! component-dependent correction
      call updateV0(mecca_state,xr,rho,potmad,V_mfcc)
!
      call gXCmecca(-1,libxc_p)   ! to release xc_mecca_pointer
!
      if ( do_etot ) then
!DEBUGPRINT
!      write(6,*) ' DEBUG e0_intrC=',e0_intrC,' CALC'
!DEBUGPRINT
        call calcTotEn(mecca_state,e0_intrC,E_mfcc,xr,rho)
      end if
!
      deallocate(Qintr)
!      deallocate(Q0intr)
      deallocate(xr,rho,potmad,fxyzES)
      deallocate(V_mfcc,E_mfcc,potShC)
!
      return

!EOC
      contains
!
!BOP
!!IROUTINE: set_rhodata
!!INTERFACE:
      subroutine set_rhodata(sublat,mecca_state,xr,rho,Qint)
!!DESCRIPTION:
! sets up charge densities data for solving Coulomb problem
!

!!DO_NOT_PRINT
      implicit none
!!DO_NOT_PRINT

!!ARGUMENTS:
      type(Sublattice),intent(in),pointer :: sublat
      type(RunState),             target  :: mecca_state
      real(8), intent(out) :: xr(:,:)
      real(8), intent(out) :: rho(:,:,:)
      type(IntrstlQ), intent(out) :: Qint(:)
!!REVISION HISTORY:
! Initial Version - A.S. - 2013
!!REMARKS:
! this is a private procedure of calcPot module subroutine
!EOP
!
!BOC
      type(RS_Str),      pointer :: rs_box
      type(SphAtomDeps), pointer :: v_rho_box
      real(8) :: rr(size(xr,1),size(xr,2))
!      real(8) :: rho0
      real(8) :: h,tot_s,sph_s,qmag
      real(8) :: rtmp(size(xr,1))
      real(8) :: rhotmp(size(xr,1))
      integer :: nspin,komp,nsub,ndrpts,ic,is
!
      nspin = size(rho,3)
!      rho0 = mecca_state%intfile%rho0/nspin
      komp = sublat%ncomp
      nsub = sublat%indx
      rs_box => mecca_state%rs_box
      do ic=1,komp
        v_rho_box => sublat%compon(ic)%v_rho_box
        ndrpts = v_rho_box%ndrpts
        call g_meshv(v_rho_box,h,xr(1:ndrpts,ic),rr(1:ndrpts,ic))
        xr(ndrpts+1:,ic) = xr(ndrpts,ic)+h       ! minexponent(zero)
        rr(ndrpts+1:,ic) = exp(xr(ndrpts,ic)+h)  ! zero
!        if ( ndrpts<size(xr,1) ) then
!         write(6,'(/'' error in genpot_sph: nsub='',i4,'' ic='',i2,     &
!     &  '' ndrpts='',i5,''< size(xr)='',i5/)') nsub,ic,ndrpts,size(xr,1)
!         call fstop(' UNEXPECTER ERROR: INCOMPATIBLE DIMENSIONS')
!        end if
        Qint(ic)%SphR = rs_box%rmt(nsub)
!        Qint(ic)%SphR = sublat%compon(ic)%rSph
        do is=1,nspin
          call g_rhoval(v_rho_box,is,rhotmp)
          call ws_integral(mecca_state,ic,nsub,rhotmp(1:ndrpts),        &
     &                                        tot_s,sph_s,Qint(ic)%SphR)
!
!         sph_s =: MT-integral; %qsph -- MT-charge
!         tot_s =: WS-integral; %q0 --  WS\MT-charge, %qmag -- WS\MT- spin-dens. "charge"
!
          Qint(ic)%qsph = Qint(ic)%qsph + sph_s
          if ( is==1 ) then
           Qint(ic)%q0   = (tot_s - sph_s)
           Qint(ic)%qmag = 0; qmag = (tot_s - sph_s)
          else
           Qint(ic)%q0   = Qint(ic)%q0 + (tot_s - sph_s)
           qmag = qmag - (tot_s - sph_s)
           Qint(ic)%qmag = Qint(ic)%q0 + qmag
          end if
          if ( v_rho_box%ndcor> 0 ) then
           call g_rhocor(v_rho_box,is,rhotmp)
           call ws_integral(mecca_state,ic,nsub,                        &
     &              rhotmp(1:v_rho_box%ndcor),tot_s,sph_s,Qint(ic)%SphR)
           Qint(ic)%qsph = Qint(ic)%qsph + sph_s
           if ( is==1 ) then
            Qint(ic)%q0   = Qint(ic)%q0 + (tot_s - sph_s)
            qmag = tot_s - sph_s
           else
            Qint(ic)%q0   = Qint(ic)%q0 + (tot_s - sph_s)
            qmag = qmag - (tot_s - sph_s)
            Qint(ic)%qmag = Qint(ic)%qmag + qmag
           end if
          end if

          call g_rho(v_rho_box,is,rho(:,ic,is))
          rtmp(1:ndrpts) = rho(1:ndrpts,ic,is)/rr(1:ndrpts,ic)
          call asa_integral(mecca_state,ic,nsub,rtmp(1:ndrpts),         &
     &                                        tot_s,sph_s,Qint(ic)%SphR)
          Qint(ic)%qm1(1) = Qint(ic)%qm1(1) + (tot_s-sph_s)
          call ws_integral(mecca_state,ic,nsub,rtmp(1:ndrpts),          &
     &                                        tot_s,sph_s,Qint(ic)%SphR)
          Qint(ic)%qm1(2) = Qint(ic)%qm1(2) + (tot_s-sph_s)

          call getVPmltpl(mecca_state,nsub,ic,rho(1:ndrpts,ic:ic,is),   &
     &                                                         Qint(ic))

        end do
        nullify(v_rho_box)
      end do
      nullify(rs_box)
      return
!EOC
      end subroutine set_rhodata
!
!BOP
!!IROUTINE: genpot_sph
!!INTERFACE:
      subroutine genpot_sph(sublat,mecca_state,xr,rho)
!!DESCRIPTION:
!  calculates local (spherical) Coulomb potential
!
!!DO_NOT_PRINT
      implicit none
!!DO_NOT_PRINT

!!ARGUMENTS:
      type(Sublattice),intent(in),pointer :: sublat
      type(RunState),             target  :: mecca_state
      real(8), intent(in)  :: xr(:,:)
      real(8), intent(inout) :: rho(:,:,:)
!!REVISION HISTORY:
! Initial Version - A.S. - 2013
!!REMARKS:
! this is a private procedure of calcPot module subroutine
!EOP
!
!BOC
      real(8) :: rr(size(xr,1),size(xr,2))
      real(8), pointer :: vrnew(:,:,:)
      real(8) :: ztotss(1:size(rho,2))
      real(8) :: v_mf_cc(1:size(rho,2))
      real(8) :: rIS(1:size(rho,2))
      real(8) :: r_circ(1:size(rho,2))
      real(8) :: rWS,tot_s,sph_s,tot_z
      real(8), parameter :: potmad=zero
      integer :: jws(1:size(rho,2))
      integer :: nspin,komp,nsub,ndrpts,ic,is
      real(8) :: v0i
!
      nspin = mecca_state%intfile%nspin
      komp = sublat%ncomp
      nsub = sublat%indx
      rr = exp(xr)
      r_circ(1:komp) = mecca_state%rs_box%rcs(nsub)
      v_mf_cc(1:komp) = zero
      vrnew => mecca_state%work_box%vr(:,:,nsub,:)
      do ic=1,komp
        ndrpts = sublat%compon(ic)%v_rho_box%ndrpts
        rWS = mecca_state%rs_box%rws(nsub)
        jws(ic) = 1+indRmesh(rWS,ndrpts,rr(:,ic))
        if ( jws(ic) > ndrpts ) then
         write(6,'(/'' error in genpot_sph: nsub='',i4,'' ic='',i2,     &
     &    '' jws='',i4,''> ndrpts='',i5/)') nsub,ic,jws(ic),ndrpts
         call fstop(' ERROR: JWS IS INCOMPATIBLE WITH RADIAL MESH')
        end if
      end do
      ndrpts = maxval(sublat%compon(1:komp)%v_rho_box%ndrpts)
      ztotss(1:komp) = sublat%compon(1:komp)%zID
      do ic=1,komp
!       if ( sublat%compon(ic)%zID==0 ) then
        do is=1,nspin
         if (minval(rho(1:ndrpts,ic,is))<zero ) then
          call asa_integral(mecca_state,ic,nsub,rho(1:ndrpts,ic,is),     &
     &                                        tot_s,sph_s,rWS)
          rho(1:ndrpts,ic,is) = max(zero,rho(1:ndrpts,ic,is))
          call asa_integral(mecca_state,ic,nsub,rho(1:ndrpts,ic,is),     &
     &                                        tot_z,sph_s,rWS)
          ztotss(ic) = ztotss(ic) + tot_z-tot_s
         end if
        end do
!       end if
      end do

      call newpot(ztotss(1:komp),                                       &
     &            rho(1:ndrpts,1:komp,1:nspin),                         &
     &            vrnew(1:ndrpts,1:komp,1:nspin),                       &
     &            libxc_p,                                              &
     &            potmad,v_mf_cc(1:komp),                               &
     &            rr(1:ndrpts,1:komp),xr(1:ndrpts,1:komp),              &
     &            jws(1:komp),komp,                                     &
     &            r_circ(1:komp))
!
      if ( mecca_state%intfile%x_pot_corr%id .ne. 0 ) then
!
!  setup of new x_pot_corr parameters, if neccessary is expected to be done here
       if ( mecca_state%intfile%x_pot_corr%id == X_GGA_vLBs ) then
        v0i = sum(mecca_state%intfile%V0(1:nspin))/2
        call xc_setpar( ext_ipot=-v0i )
!        ionizpot = (-v0i)
       end if
!
       call corrXpot(ztotss(1:komp),                                    &
     &            rho(1:ndrpts,1:komp,1:nspin),                         &
     &            vrnew(1:ndrpts,1:komp,1:nspin),                       &
     &            mecca_state%intfile%x_pot_corr%id,                    &
     &            rr(1:ndrpts,1:komp),xr(1:ndrpts,1:komp),              &
     &            jws(1:komp),komp,                                     &
     &            r_circ(1:komp))
      end if

      return
!EOC
      end subroutine genpot_sph
!
!BOP
!!ROUTINE: corrXpot
!!INTERFACE:
      subroutine corrXpot(ztotss,rhorr,vrnew,idx,                       &
     &                  rr,xr,jmt,komp,                                 &
     &                  r_circ)
!!DESCRIPTION:
! {\bv
! calculate hartree and exchange-correlation potential .........
! mean-field, charge-correlated CPA included     by DDJ  Dec 1993
! \ev}
!
!!USES:
      use mecca_constants, only : zero,one,two,pi,ipotoutmt

!!DO_NOT_PRINT
      implicit none
!!DO_NOT_PRINT

!!ARGUMENTS:
      real(8), intent(in) :: ztotss(:)  ! (ipcomp)
      real(8), intent(in) :: rhorr(:,:,:) ! (iprpts,ipcomp,nspin)
      real(8), intent(inout) :: vrnew(:,:,:) ! (iprpts,ipcomp,nspin)
      integer, intent(in) :: idx
      real(8), intent(in) :: rr(:,:) ! (iprpts,ipcomp)
      real(8), intent(in) :: xr(:,:) ! (iprpts,ipcomp)
      integer, intent(in) :: jmt(:)  ! (ipcomp)
      integer, intent(in) :: komp
      real(8), optional :: r_circ(:)
!!REVISION HISTORY:
! Created - A.S. - 2018
!EOP
!
!BOC
!
      integer, external :: indRmesh
!      character(512), external :: descrXC
!
      real(8) vxup(size(xr,1),komp)
      real(8) vxdn(size(xr,1),komp)
      real(8) enxc(1,komp)
      real(8) totrho(size(xr,1),komp)
      type(xc_mecca_pointer) :: libxc_p
      real(8) vxc
      integer :: ik,ikeep,ir,jend(komp),nsp,ndrpts,nsph
!c
      libxc_p%iexch = idx
      nsp = size(rhorr,3)
      ndrpts = size(rhorr,1)
      do ik = 1,komp
        totrho(:,ik) = zero
        do ir = 1,ndrpts
          totrho(ir,ik) = sum(rhorr(ir,ik,1:nsp))
        enddo
      enddo
!
      do ik=1,komp
!
         nsph = min(ndrpts,jend(ik))
         if ( nint(ztotss(ik)) == 0 ) then
           libxc_p%lda_only = 1
         else
           libxc_p%lda_only = 0
         end if
!c exchange potential correction
         if ( nsp == 1 ) then
          call xc_alpha(nsp,nsph,rr(1:nsph,ik),totrho(1:nsph,ik)        &
     &     ,rhorr(1:nsph,ik,2),enxc(:,ik),vxup(1:nsph,ik)               &
     &     ,vxdn(1:nsph,ik),libxc_p=libxc_p)
         else
          call xc_alpha(nsp,nsph,rr(1:nsph,ik)                          &
     &     ,rhorr(1:nsph,ik,1),rhorr(1:nsph,ik,2)                       &
     &     ,enxc(:,ik),vxup(1:nsph,ik),vxdn(1:nsph,ik)                  &
     &     ,libxc_p=libxc_p)
         end if
!
!  potential update, including X-potential correction
!
         vrnew(1:nsph,ik,1) = vrnew(1:nsph,ik,1) +                      &
     &                       vxup(1:nsph,ik) * rr(1:nsph,ik)
         if ( nsp>1 ) then
          vrnew(1:nsph,ik,2) = vrnew(1:nsph,ik,2) +                     &
     &                        vxdn(1:nsph,ik) * rr(1:nsph,ik)
         end if
         if ( ionizpot .ne. 0.d0 ) then
          vrnew(1:nsph,ik,1:nsp) = vrnew(1:nsph,ik,1:nsp) + ionizpot
         end if
!c
      enddo  ! ik-cycle
      call gXCmecca(-1,libxc_p)
!c
      return
!EOC
      end subroutine corrXpot

!
!BOP
!!IROUTINE: genpot_pbc
!!INTERFACE:
      subroutine genpot_pbc(mecca_state,potmad,e0_intrC,fxyzES)
!!DESCRIPTION:
!  calculates contribution due to rho0 and (periodic) charge multipoles

!!DO_NOT_PRINT
      implicit none
!!DO_NOT_PRINT

!!ARGUMENTS:
      type(RunState), intent(in), target :: mecca_state
      real(8), intent(out) :: potmad(:)  ! PBC Coulomb potential due to point-multipoles and rho0
      real(8), intent(out) :: e0_intrC(:) ! PBC Coulomb energy and pressure contribution
      real(8), intent(out) :: fxyzES(:,:)! ElectroStatic (ES) forces
!!REVISION HISTORY:
! Initial Version - A.S. - 2013
!!REMARKS:
! this is a private procedure of calcPot module subroutine
!EOP
!
!BOC
      type(Sublattice),  pointer :: sublat
      type(SphAtomDeps), pointer :: v_rho_box
      type(RS_Str),      pointer :: rs_box
      type(VP_data),    pointer :: vp_box
      real(8) :: rho0,emad,d_rho,r_s,rho_min,rho_max
      real(8) :: cm(size(potmad,1))
      real(8) :: dm(3,size(potmad,1)),pdm(3)
      real(8) :: qm(3,3,size(potmad,1)),pqm(3,3)
      real(8) :: hm3(3,3,3,size(potmad,1)),phm3(3,3,3)
      real(8) :: hm4(3,3,3,3,size(potmad,1)),phm4(3,3,3,3)
      real(8) :: pmad(size(potmad,1)),alphpot,alphen
      real(8) :: qeff(1),q0,q0sph,Pot,En,de0,dpv
      real(8) :: qsubmt,atc,dUc,qsub
      integer :: nsublat
      integer :: nbasis,ic,nsub
      integer :: lmltp,l0
      integer :: st_env
      real(8), parameter :: pi4d3=four*pi/three
      nsublat = size(potmad,1)
      inFile => mecca_state%intfile
      rho0 = mecca_state%intfile%rho0
      rs_box => mecca_state%rs_box
      vp_box => null()
      if ( associated(mecca_state%vp_box) ) then
       if ( allocated(mecca_state%vp_box%alphpot) ) then
        if ( allocated(mecca_state%vp_box%alphen) ) then
          vp_box => mecca_state%vp_box
        end if
       end if
      end if
      nbasis = sum(rs_box%numbsubl(1:nsublat))
      cm = 0
      dm = 0
      qm = 0
      hm3 = 0
      hm4 = 0
      fxyzES = 0
      potmad = 0
      emad = zero
      e0_intrC = zero
!c...........................................................................

!DEBUG  to conserve charge [other way is to adjust Fermi level]
!      qsub = zero
!      do nsub=1,nsublat
!       sublat => mecca_state%intfile%sublat(nsub)
!       do ic=1,sublat%ncomp
!        qsub = qsub + sublat%compon(ic)%concentr*rs_box%numbsubl(nsub)  &
!     &   * (sublat%compon(ic)%zID-Qintr(ic,nsub)%qsph-Qintr(ic,nsub)%q0)
!       end do
!      end do
!      d_rho = -qsub/rs_box%volume
!!DEBUGPRINT
!      write(*,*) ' DEBUG: d_rho=',sngl(d_rho),sngl(rho0),sngl(qsub)
!!DEBUGPRINT
!      do nsub=1,nsublat
!       sublat => mecca_state%intfile%sublat(nsub)
!       do ic=1,sublat%ncomp
!        Qintr(ic,nsub)%q0 = Qintr(ic,nsub)%q0 -                         &
!     &                                 d_rho*rs_box%rws(nsub)**3*pi4d3
!       end do
!      end do
!      rho0 = rho0 - d_rho
!!DEBUG

      do nsub=1,nsublat
       sublat => mecca_state%intfile%sublat(nsub)
       if ( associated(vp_box) ) then
        alphpot = vp_box%alphpot(nsub)
        alphen  = vp_box%alphen(nsub)
       else
        alphpot = one
        alphen  = one
       end if
       lmltp = 0
       de0 = zero
       dpv = zero
       do ic=1,sublat%ncomp
        call HmgnsRhoEnPot(rho0,zero,rs_box%rws(nsub),alphpot,alphen,   &
     &                                                 q0,q0sph,Pot,En)
        qsubmt = sublat%compon(ic)%zID -                                &
     &               (Qintr(ic,nsub)%qsph+Qintr(ic,nsub)%q0-q0sph)
        atc = sublat%compon(ic)%concentr
        cm(nsub) = cm(nsub) + qsubmt * atc
        potmad(nsub) = potmad(nsub) + Pot*atc
        dUc = qsubmt*Pot-En
!c                     ! dUc =: Coulomb contribution from interstitial rho
        de0 = de0 + dUc*atc
        dpv = dpv + dUc*atc
        if ( allocated(Qintr(ic,nsub)%YlmVPmltpl) ) then
         lmltp = g_Lmltpl()
         l0 = 1
         if(lmltp.ge.1) then
          l0 = l0 +1
          call Sph2XYZ1(Qintr(ic,nsub)%YlmVPmltpl(l0),pdm)
          dm(:,nsub) = dm(:,nsub) + atc*pdm(:)
          if(lmltp.ge.2) then
           l0 = l0 +2
           call Sph2XYZ2(Qintr(ic,nsub)%YlmVPmltpl(l0),pqm)
           qm(:,:,nsub) = qm(:,:,nsub) + atc*pqm(:,:)
           if(lmltp.ge.3) then
            l0 = l0 +3
            call Sph2XYZ3(Qintr(ic,nsub)%YlmVPmltpl(l0),phm3)
            hm3(:,:,:,nsub) = hm3(:,:,:,nsub) + atc*phm3(:,:,:)
            if(lmltp.ge.4) then
             l0 = l0 +4
             call Sph2XYZ4(Qintr(ic,nsub)%YlmVPmltpl(l0),phm4)
             hm4(:,:,:,:,nsub) = hm4(:,:,:,:,nsub) + atc*phm4(:,:,:,:)
            end if
           end if
          end if
         end if
        end if
       end do
       nullify(sublat)
       e0_intrC(1) = e0_intrC(1)+ de0*rs_box%numbsubl(nsub)
       e0_intrC(2) = e0_intrC(2)+ dpv*rs_box%numbsubl(nsub)
!DEBUGPRINT
       if (lmltp>0 .and. alphpot.ne.one ) then
        write(6,*) ' nsub=',nsub
        if ( maxval(abs(dm(:,nsub)))>zero ) then
         write(6,'(a,2x,3(1x,e15.7))') ' DM:',dm(:,nsub)
        else
!         write(6,*) ' DM = 0'
        end if
        if ( maxval(abs(qm(:,:,nsub)))>zero ) then
         write(6,'(a/(2x,3(1x,e15.7)))') ' QM:',qm(:,:,nsub)
        else
!         if ( lmltp>1 ) write(6,*) ' QM = 0'
        end if
        if ( maxval(abs(hm3(:,:,:,nsub)))>zero ) then
         write(6,'(a/(2x,3(1x,e15.7)))') ' HM3:',hm3(:,:,:,nsub)
        else
!         if ( lmltp>2 ) write(6,*) ' HM3 = 0'
        end if
        if ( maxval(abs(hm4(:,:,:,:,nsub)))>zero ) then
         write(6,'(a/(2x,3(1x,e15.7)))') ' HM4:',hm4(:,:,:,:,nsub)
        else
!         if ( lmltp>3 ) write(6,*) ' HM4 = 0'
        end if
       end if
!DEBUGPRINT
      end do
      call elctrstat3(inFile%alat,rs_box%rslatt,                        &
     &                nbasis,rs_box%xyzr(1:3,1:nbasis),                 &
     &                rs_box%itype,nsublat,                             &
     &                cm,dm,qm,hm3,hm4,                                 &
     &                emad,pmad,fxyzES,                                 &
     &                inFile%iprint,1                                   &
     &               )
!DEBUG     &                1,1                                               &
!DEBUG     &                inFile%iprint,1                                   &
!DEBUGPRINT
!      write(*,*) 'DEBUG qsubmt,Pot,En,emad:',                           &
!     &                    sngl(qsubmt),sngl(Pot),sngl(En),sngl(emad)
!      write(*,*) 'DEBUG e0_intrC:',sngl(e0_intrC)
!      write(*,*) 'DEBUG   cm:',sngl(cm(1:nsublat))
!      write(*,*) 'DEBUG pmad:',sngl(pmad(1:nsublat))
!DEBUGPRINT
      e0_intrC(1) = e0_intrC(1)+emad
      e0_intrC(2) = dble(5)/dble(3)*(e0_intrC(2)+emad)       ! TODO: pressure from multipoles
      potmad(1:nsublat) = potmad(1:nsublat) + pmad(1:nsublat)
      potmad(1:nsublat) = -potmad(1:nsublat)
      if(inFile%iprint.ge.-1) then
       write(6,*)
       if ( inFile%iprint.ge.0 ) then
        write(6,'(''       Erho0 and Emad contributions'',t40,''='',    &
     &                                       es14.5,3x,es14.5)')        &
     &             e0_intrC(1)*dble(5)/dble(3), emad*dble(5)/dble(3)
       end if
       r_s = (pi4d3*max(rho0,1.d-18/pi4d3))**(-third)
       rho_min = gInterstlRho1(0)
       rho_max = gInterstlRho1(1)
       write(6,'(''     PBC-charge-density='',f10.5,2x,''r_s= '',es14.5,
     &               '' rho_min= '',f8.5,'' rho_max= '',f8.5/)')        &
     &                          rho0,r_s,rho_min,rho_max
       do nsub=1,nsublat
        sublat => mecca_state%intfile%sublat(nsub)
        qsub = zero
        do ic=1,sublat%ncomp
         qsub = qsub + sublat%compon(ic)%concentr *                     &
     &     (sublat%compon(ic)%zID-Qintr(ic,nsub)%qsph-Qintr(ic,nsub)%q0)
        end do
         write(6,'(''  Sub-lat ='',i2,                                  &
     &               ''  Point charge  ='',f10.5,                       &
     &               ''    pbc charge  ='',f10.5,                       &
     &               ''  pot_pbc ='',f10.5)')                           &
     &                  nsub,qsub,cm(nsub),potmad(nsub)
        nullify(sublat)
       enddo
       write(6,*)
      end if

      return
!EOC
      end subroutine genpot_pbc
!
!
!BOP
!!IROUTINE: mfcc_corr
!!INTERFACE:
      subroutine mfcc_corr(mecca_state,v_mf_cc,e_mf_cc)
!!DESCRIPTION:
!  mean-field screened-CPA charge correlation contribution
!  to potential and energy

!!DO_NOT_PRINT
      implicit none
!!DO_NOT_PRINT

!!ARGUMENTS:
      type(RunState), intent(in),target :: mecca_state
      real(8), intent(out) :: v_mf_cc(:,:)
      real(8), intent(out) :: e_mf_cc(:,:)
!!PARAMETERS:
!   c_mf_cc (it is curently defined in module mecca_constants)
!!REVISION HISTORY:
! Initial Version - A.S. - 2013
!!REMARKS:
! this is a private procedure of calcPot module subroutine;
! scr_mfcc_const is an external function
!EOP
!
!BOC
!
!c  ==================================================================
!c   c_mf_cc = 0.0      standard CPA  (NO charge correlation)
!c   c_mf_cc = 1.0/2    scr-CPA   Johnson & Pinsk, PRB48, 11553 (1993)
!c   c_mf_cc = 1.3147/2 FCC analytic  Magri et al. PRB42, 11388 (1990)
!c   c_mf_cc = 1.3831/2 BCC analytic
!c   c_mf_cc = 2.0/2    SIM       Korzhavyi et al. PRB51,  5773 (1995)
!c  ==================================================================
!
      type(Sublattice),  pointer :: sublat
      type(RS_Str),      pointer :: rs_box
      real(8) :: qsub
      real(8) :: qeff(1:size(v_mf_cc,1))

      real(8) :: cmf_cc,vqeff
      integer :: iprint,nsub,ic

      v_mf_cc = zero
      e_mf_cc = zero
      iprint = mecca_state%intfile%iprint
      if ( size(v_mf_cc,1) <=1 .or. scr_mfcc_const(one)==zero ) then
       return
      else if( mecca_state%intfile%mtasa==0 ) then
         if(iprint.ge.0) then
          write(6,'(/a/)')                                              &
     &        ' Screened-cc-CPA is not implemented for MT-case'
         end if
         return
      end if

      nsublat = size(v_mf_cc,2)
      rs_box => mecca_state%rs_box
!
      do nsub=1,nsublat
       sublat => mecca_state%intfile%sublat(nsub)
       if(sublat%ncomp.gt.1) then
        call g_qsub(sublat,mecca_state%intfile%nspin,                   &
     &                                  qsub,qeff(1:sublat%ncomp))
        cmf_cc=scr_mfcc_const(rs_box%rws(nsub)/rs_box%Rnn(nsub))
        do ic=1,sublat%ncomp
!c Mean field charge-correlation energy  (e**2 == two -- in at.un.)
!         E_mf_cc(ic,nsub)  =  -(two*c_mf_cc)*qeff(ic,nsub)**2/R_nn
         vqeff = two*(two*cmf_cc)*qeff(ic)/rs_box%Rnn(nsub)
         v_mf_cc(ic,nsub) = vqeff
         e_mf_cc(ic,nsub) = -0.5d0*vqeff*qeff(ic)
        enddo
        if(iprint.ge.0) then
         write(6,*)'      MF-CC CPA, E (per component):'
         do ic=1,sublat%ncomp
          write(6,1002) nsub,ic,e_mf_cc(ic,nsub),qeff(ic)
1002      format('  nsub=',i4,' ic=',i2,' E_mf_cc =',f12.8,             &
     &           ' (dQ(ic,nsub)-dQavg(nsub))=',f10.5)
         end do
        end if
       end if
      enddo
!
      return
!EOC
      end subroutine mfcc_corr
!
!BOP
!!IROUTINE: updateV0
!!INTERFACE:
      subroutine updateV0(mecca_state,xr,rho,potmad,v_mf_cc)
!!DESCRIPTION:
! to add (potmad,v\_mf\_cc) corrections to Vr and
! to calculate variational V0 (VP or ASA)
!
!!USES:

!!DO_NOT_PRINT
      implicit none
!!DO_NOT_PRINT

!!ARGUMENTS:
      type(RunState), target :: mecca_state
      real(8), intent(in) :: xr(:,:,:)
      real(8), intent(in) :: rho(:,:,:,:)
      real(8), intent(in) :: potmad(:)
      real(8), intent(in) :: v_mf_cc(:,:)
!!REVISION HISTORY:
! Initial Version - A.S. - 2013
!!REMARKS:
! this is a private procedure of calcPot module subroutine
!EOP
!
!BOC
!
      type(Sublattice),  pointer :: sublat
      type(SphAtomDeps), pointer :: v_rho_box
      type(GF_output),   pointer :: gf_out
      real(8), pointer :: vrnew(:,:,:,:)
      real(8), pointer :: vmtz(:)
!
      integer :: ndrpts,nspin,nsublat
      integer :: nsub,ic,is,jmx
!
      real(8), target :: frhoV(size(xr,1)),rhotmp(size(xr,1))
      real(8) :: vtmp(size(xr,1)),exc(size(xr,1))
      real(8) :: rr(size(xr,1)),rhom(size(xr,1)),rhop(size(xr,1))
      real(8) :: tmpr2(2),vtmpr2(2)

      real(8) :: tot_s,sph_s,tot_ws,tmom_mt,tmom_ws,qpls,qmns
      real(8) :: w_sub,v0sph,rIS,vdif,v0i,v0c,vlm_ir
      real(8) :: rho0,rs,zeta,exch,rho0_1,rho0_2,dxc,q0_1,q0_2
      logical :: is_es,st_env
      integer :: iex,jend,Ni
      integer :: xc_split_type
      character(1) :: symb(2),name(15)
      character(512), external :: descrXC

!!implemented spin splitting types:
! 0 (Vdif=0),
! 1 (var dz, Vxc, global), 2(rho0_up/dn, global),
! 3 (var dz, Vxc, local),  4(rho0_up/dn, local),
! 5 (var rho-, Vxc, global), 6 (var rho-, local)   <== almost the same as type 0 (Vdif=0)
!
      if ( exc_type>=0 .and. exc_type<=max_exctype ) then
       xc_split_type = exc_type
      else
       xc_split_type = exc_type_dflt
      end if
      nsublat = size(xr,3)
      nspin = size(rho,4)
      vrnew => mecca_state%work_box%vr(:,:,:,:)   ! in- and output
      vmtz => mecca_state%work_box%vmtz(:)        ! output
      if ( associated(mecca_state%gf_out_box) ) then
       gf_out  => mecca_state%gf_out_box
      else
       gf_out => null()
      end if
! +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
      tmom_mt  = zero
      tmom_ws  = zero
      tmpr2    = zero
      vtmpr2    = zero
      vmtz     = zero
      do nsub=1,nsublat
       sublat => mecca_state%intfile%sublat(nsub)
       do ic=1,sublat%ncomp
        v_rho_box => sublat%compon(ic)%v_rho_box
        ndrpts = v_rho_box%ndrpts
        w_sub = sublat%compon(ic)%concentr*sublat%ns
        rIS = sublat%compon(ic)%rSph
        rr(1:ndrpts)=exp(xr(1:ndrpts,ic,nsub))
        is_es = sublat%compon(ic)%zID .eq. 0

        vtmp(1:ndrpts) = (potmad(nsub)+v_mf_cc(ic,nsub))*rr(1:ndrpts)
        do is=1,nspin
         v_rho_box%v0(is) = zero
         vrnew(1:ndrpts,ic,nsub,is) = vrnew(1:ndrpts,ic,nsub,is) +      &
     &                                vtmp(1:ndrpts)
!*********************************************************************
         frhoV(1:ndrpts) = rho(1:ndrpts,ic,nsub,is) *                   &
     &                vrnew(1:ndrpts,ic,nsub,is)/rr(1:ndrpts)
         if ( is_es ) then
          call ws_integral(mecca_state,ic,nsub,frhoV(1:ndrpts),         &
     &                                                 tot_ws,sph_s,rIS)
          vtmpr2(is) = vtmpr2(is) + tot_ws * w_sub   ! VP(ASA)
          call ws_integral(mecca_state,ic,nsub,                         &
     &                        rho(1:ndrpts,ic,nsub,is),tot_ws,sph_s,rIS)
          tmpr2(is) = tmpr2(is) + tot_ws * w_sub ! VP(ASA)
          if ( associated(gf_out) ) then
           gf_out%xvalws(ic,nsub,is) = tot_ws
           gf_out%xvalmt(ic,nsub,is) = sph_s
          end if
         else
!tot
          call asa_integral(mecca_state,ic,nsub,frhoV(1:ndrpts),        &
     &                                                  tot_s,sph_s,rIS)
          vtmpr2(is) = vtmpr2(is) + (tot_s-sph_s) * w_sub      ! ASA\IS
          call asa_integral(mecca_state,ic,nsub,                        &
     &                         rho(1:ndrpts,ic,nsub,is),tot_s,sph_s,rIS)
          tmpr2(is) = tmpr2(is) + (tot_s-sph_s) * w_sub    ! ASA\IS
!val
          call g_rhoval(v_rho_box,is,rhotmp)
          call asa_integral(mecca_state,ic,nsub,                        &
     &                         rhotmp(1:ndrpts),tot_s,sph_s,rIS)
          call ws_integral(mecca_state,ic,nsub,                         &
     &                         rhotmp(1:ndrpts),tot_ws,sph_s,rIS)
          if ( associated(gf_out) ) then
           gf_out%xvalws(ic,nsub,is) = tot_ws
           gf_out%xvalmt(ic,nsub,is) = sph_s
          end if
          if ( abs(tot_ws-tot_s)>1.d-10*tot_ws ) then
           tmpr2(is) = tmpr2(is) + (tot_ws-tot_s) * w_sub  ! VP\ASA
           frhoV(1:ndrpts) = rhotmp(1:ndrpts) *                         &
     &                vrnew(1:ndrpts,ic,nsub,is)/rr(1:ndrpts)
           call asa_integral(mecca_state,ic,nsub,frhoV(1:ndrpts),       &
     &                                                  tot_s,sph_s,rIS)
           call ws_integral(mecca_state,ic,nsub,frhoV(1:ndrpts),        &
     &                                                 tot_ws,sph_s,rIS)
           vtmpr2(is) = vtmpr2(is) + (tot_ws-tot_s) * w_sub    ! VP\ASA
          end if
         end if
!          call g_rhosemicore(ic,nsub,is,rhotmp)
!          if ( rhotmp(1) .ne. zero ) then
!semi is required if its contribution is not used in "density" module
!         end if
!***********************************************************************
         if ( is == 2 ) then
          rhom(1:ndrpts) = rho(1:ndrpts,ic,nsub,1) -                    &
     &                     rho(1:ndrpts,ic,nsub,nspin)
          call asa_integral(mecca_state,ic,nsub,rhom(1:ndrpts),         &
     &                                                 tot_s,sph_s,rIS)
          tmom_mt = tmom_mt + sph_s * w_sub
          tmom_ws = tmom_ws + tot_s * w_sub
          vtmp(1:ndrpts) = rhom(1:ndrpts) * half *                      &
     &                            (vrnew(1:ndrpts,ic,nsub,1) -          &
     &           vrnew(1:ndrpts,ic,nsub,nspin)) / rr(1:ndrpts)
          call asa_integral(mecca_state,ic,nsub,vtmp(1:ndrpts),         &
     &                                                 tot_s,sph_s,rIS)
          if ( is_es ) then
           vtmpr2(1:nspin) = vtmpr2(1:nspin) - half*tot_s*w_sub
          else
           vtmpr2(1:nspin) = vtmpr2(1:nspin) - half*(tot_s-sph_s)*w_sub
          end if
         end if
        enddo
       enddo
       nullify(sublat)
      enddo
      nullify(v_rho_box)
!
      vdif = min(sum(tmpr2(1:nspin)),mecca_state%intfile%rho0)
      if ( abs(vdif) >= epsilon(1.d0) ) then
       vmtz(1:nspin) = sum(vtmpr2(1:nspin))/sum(tmpr2(1:nspin))
      else
       call gEnvVar('ENV_ATOMSS',.false.,st_env)
       if ( st_env==1 ) then  ! atomic mode (negligible interstitial density)
        vmtz = zero
       end if
      end if
      vdif     = zero
!
      if ( nspin == 2 ) then
        if(mecca_state%intfile%nMM.eq.1) then  !DLM
         continue   ! vdif=0
        else
         select case (xc_split_type)
         case(1)
          do nsub=1,nsublat
           sublat => mecca_state%intfile%sublat(nsub)
           if ( sublat%isDLM ) cycle
           do ic=1,sublat%ncomp
            v_rho_box => sublat%compon(ic)%v_rho_box
            ndrpts = v_rho_box%ndrpts
            w_sub = sublat%compon(ic)%concentr*sublat%ns
            rIS = sublat%compon(ic)%rSph
            rr(1:ndrpts)=exp(xr(1:ndrpts,ic,nsub))
             call g_rhoval(v_rho_box,1,rhotmp)     ! rho1
             call ws_integral(mecca_state,ic,nsub,rhotmp(1:ndrpts),     &
     &                                                  tot_s,sph_s,rIS)
             qpls = tot_s-sph_s
             qmns = sph_s
             call g_rhoval(v_rho_box,nspin,rhom)   ! rho2
             call ws_integral(mecca_state,ic,nsub,rhom(1:ndrpts),       &
     &                                                  tot_s,sph_s,rIS)
             qpls = qpls + (tot_s-sph_s)
             qmns = qmns - sph_s
             Ni = indRmesh(rIS,ndrpts,rr)-4
             vtmp(1:Ni) = zero
             frhoV(1:Ni)= zero
             call calcVarXC(libxc_p,rr(Ni:ndrpts),rhotmp(Ni:ndrpts)     &
     &                   ,rhom(Ni:ndrpts),frhoV(Ni:ndrpts))     ! frhoV = zeta*d(V1-V2)/d(zeta)
             vtmp(Ni:ndrpts) = ( frhoV(Ni:ndrpts) +                     &
     &                            (vrnew(Ni:ndrpts,ic,nsub,1) -         &
     &           vrnew(Ni:ndrpts,ic,nsub,nspin)) / rr(Ni:ndrpts) ) / two
             rhotmp(Ni:ndrpts) = rhotmp(Ni:ndrpts) + rhom(Ni:ndrpts)  ! rho_plus
             vtmp(Ni:ndrpts) = rhotmp(Ni:ndrpts) * vtmp(Ni:ndrpts)
             call ws_integral(mecca_state,ic,nsub,vtmp(1:ndrpts),       &
     &                                                  tot_s,sph_s,rIS)
             v0i = (tot_s-sph_s)
             if ( qmns<zero ) then     ! 1==dn, 2==up
              if ( v0i>zero ) v0i=-v0i
             else                      ! 1==up, 2==dn
              if ( v0i<zero ) v0i=-v0i
             end if
             vdif=vdif + v0i * w_sub
           enddo
           nullify(sublat)
          enddo
          nullify(v_rho_box)
          vmtz(1) = vmtz(1) - vdif/2
          vmtz(nspin) = vmtz(1) + vdif
         case(2)
          rho0 = zero
          zeta = zero
          rs = huge(rs)
          v0i = zero
          qpls = zero
          qmns = zero
          do nsub=1,nsublat
           sublat => mecca_state%intfile%sublat(nsub)
           if ( sublat%isDLM ) cycle
           do ic=1,sublat%ncomp
            v_rho_box => sublat%compon(ic)%v_rho_box
            ndrpts = v_rho_box%ndrpts
            w_sub = sublat%compon(ic)%concentr*sublat%ns
            rIS = sublat%compon(ic)%rSph
            rr(1:ndrpts)=exp(xr(1:ndrpts,ic,nsub))
!
             rhom(1:ndrpts) = pi4*rr(1:ndrpts)**2
             call ws_integral(mecca_state,ic,nsub,rhom(1:ndrpts),       &
     &                                                  tot_s,sph_s,rIS)
             v0i = v0i + w_sub*(tot_s-sph_s)
!??            call g_rhoval(v_rho_box,1,rhotmp)     ! rho1
             rhotmp(1:ndrpts) = rho(1:ndrpts,ic,nsub,1)
             call ws_integral(mecca_state,ic,nsub,rhotmp(1:ndrpts),     &
     &                                                  tot_s,sph_s,rIS)
             q0_1 = max(zero,(tot_s-sph_s))
!??            call g_rhoval(v_rho_box,nspin,rhotmp)   ! rho2
             rhotmp(1:ndrpts) = rho(1:ndrpts,ic,nsub,nspin)
             call ws_integral(mecca_state,ic,nsub,rhotmp(1:ndrpts),     &
     &                                                  tot_s,sph_s,rIS)
             q0_2 = max(zero,(tot_s-sph_s))
             qpls = qpls + w_sub*(q0_1+q0_2)
             qmns = qmns + w_sub*(q0_1-q0_2)
           enddo
           nullify(sublat)
          enddo
          nullify(v_rho_box)
          rho0 = qpls/v0i
          if ( rho0 >= epsilon(rs) ) then
           rs = (three/(pi4*rho0))**third
           zeta = qmns/v0i/rho0
!!          write(6,'(a,3(g18.8,1x))') 'DEBUG rs,zeta,rho0:',rs,zeta,rho0
           vdif = xc_alpha2(rs,zeta,one,libxc_p%iexch,exch,libxc_p)     &
     &          - xc_alpha2(rs,zeta,-one,libxc_p%iexch,exch,libxc_p)
          else
           vdif = zero
          end if
!
          vmtz(1) = vmtz(1) - vdif*half
          vmtz(nspin) = vmtz(1) + vdif
!DEBUGPRINT
        if ( mecca_state%intfile%iprint >= -1 ) then
            rho0_1 = rho0*(one+zeta)/2
            rho0_2 = rho0*(one-zeta)/2
            write(6,'('' nsub='',i3,'' ic='',i2,'' v0xc='',2g14.5,      &
     &                                     ''  rho0_out='',2g16.6)')    &
     &                            0,0,-vdif/2,vdif/2,rho0_1,rho0_2
        end if
!DEBUGPRINT
         case (3:4)
          rho0 = zero
          zeta = zero
          rs = huge(rs)
          v0i = zero
          qpls = zero
          qmns = zero
          do nsub=1,nsublat
           sublat => mecca_state%intfile%sublat(nsub)
           if ( sublat%isDLM ) cycle
           v0c = zero
           rho0_1 = zero
           rho0_2 = zero
           do ic=1,sublat%ncomp
            v_rho_box => sublat%compon(ic)%v_rho_box
            ndrpts = v_rho_box%ndrpts
            w_sub = sublat%compon(ic)%concentr*sublat%ns
            rIS = sublat%compon(ic)%rSph
            rr(1:ndrpts)=exp(xr(1:ndrpts,ic,nsub))
!
            if ( xc_split_type==3 ) then
!
             call g_rhoval(v_rho_box,1,rhotmp)     ! rho1
             call ws_integral(mecca_state,ic,nsub,rhotmp(1:ndrpts),     &
     &                                                  tot_s,sph_s,rIS)
             qpls = tot_s-sph_s
             qmns = sph_s
             call g_rhoval(v_rho_box,nspin,rhom)   ! rho2
             call ws_integral(mecca_state,ic,nsub,rhom(1:ndrpts),       &
     &                                                  tot_s,sph_s,rIS)
             qpls = qpls + (tot_s-sph_s)
             qmns = qmns - sph_s
             Ni = indRmesh(rIS,ndrpts,rr)-4
             vtmp(1:Ni) = zero
             frhoV(1:Ni)= zero
             call calcVarXC(libxc_p,rr(Ni:ndrpts),rhotmp(Ni:ndrpts)     &
     &                   ,rhom(Ni:ndrpts),frhoV(Ni:ndrpts))     ! frhoV = zeta*d(V1-V2)/d(zeta)
             vtmp(Ni:ndrpts) = ( frhoV(Ni:ndrpts) +                     &
     &                            (vrnew(Ni:ndrpts,ic,nsub,1) -         &
     &           vrnew(Ni:ndrpts,ic,nsub,nspin)) / rr(Ni:ndrpts) ) / two
             rhotmp(Ni:ndrpts) = rhotmp(Ni:ndrpts) + rhom(Ni:ndrpts)  ! rho_plus
             vtmp(Ni:ndrpts) = rhotmp(Ni:ndrpts) * vtmp(Ni:ndrpts)
             call ws_integral(mecca_state,ic,nsub,vtmp(1:ndrpts),       &
     &                                                  tot_s,sph_s,rIS)
             v_rho_box%v0(1)     = -(tot_s-sph_s)*0.5d0
             v_rho_box%v0(nspin) =  (tot_s-sph_s)*0.5d0
             if ( qmns<zero ) then
              if ( v_rho_box%v0(1)<v_rho_box%v0(nspin) )                &
     &          v_rho_box%v0 = -v_rho_box%v0
             else
              if ( v_rho_box%v0(1)>v_rho_box%v0(nspin) )                &
     &          v_rho_box%v0 = -v_rho_box%v0
             end if
!             call  ws_integral(mecca_state,ic,nsub,rhotmp(1:ndrpts),    &
!     &                                                  tot_s,sph_s,rIS)
!             v_rho_box%v0 = v_rho_box%v0/(tot_s-sph_s)
             v_rho_box%v0 = v_rho_box%v0/qpls
!DEBUGPRINT
        if ( mecca_state%intfile%iprint >= -1 ) then
            write(6,'('' nsub='',i3,'' ic='',i2,'' v0xc='',2g14.5)')    &
     &                                          nsub,ic,v_rho_box%v0
        end if
!DEBUGPRINT
             vdif=vdif + (v_rho_box%v0(nspin)-v_rho_box%v0(1)) * w_sub
!
            else if ( xc_split_type==4 ) then
!
             rhom(1:ndrpts) = pi4*rr(1:ndrpts)**2
             call ws_integral(mecca_state,ic,nsub,rhom(1:ndrpts),       &
     &                                                  tot_s,sph_s,rIS)
             v0i = v0i + w_sub*(tot_s-sph_s)
             v0c = v0c + w_sub*(tot_s-sph_s)
!??            call g_rhoval(v_rho_box,1,rhotmp)     ! rho1
             rhotmp(1:ndrpts) = rho(1:ndrpts,ic,nsub,1)
             call ws_integral(mecca_state,ic,nsub,rhotmp(1:ndrpts),     &
     &                                                  tot_s,sph_s,rIS)
             q0_1 = max(zero,(tot_s-sph_s))
             rho0_1 = rho0_1 + w_sub*q0_1
!??            call g_rhoval(v_rho_box,nspin,rhotmp)   ! rho2
             rhotmp(1:ndrpts) = rho(1:ndrpts,ic,nsub,nspin)
             call ws_integral(mecca_state,ic,nsub,rhotmp(1:ndrpts),     &
     &                                                  tot_s,sph_s,rIS)
             q0_2 = max(zero,(tot_s-sph_s))
             rho0_2 = rho0_2 + w_sub*q0_2
             qpls = qpls + w_sub*(q0_1+q0_2)
             qmns = qmns + w_sub*(q0_1-q0_2)
!
            end if
!
           enddo
           if ( xc_split_type == 4 ) then
            rho0 = (rho0_1 + rho0_2)/v0c    ! sublattice interst.density
            if ( rho0 >= epsilon(rs) ) then
             rs = (three/(pi4*rho0))**third
             zeta = (rho0_1 - rho0_2)/v0c/rho0
             dxc = xc_alpha2(rs,zeta,one,libxc_p%iexch,exch,libxc_p) -  &
     &             xc_alpha2(rs,zeta,-one,libxc_p%iexch,exch,libxc_p)
            else
             dxc = zero
            end if
            do ic=1,sublat%ncomp
             v_rho_box => sublat%compon(ic)%v_rho_box
!!             if ( sublat%compon(ic)%zID .eq. 0 ) then
!!!              v_rho_box%v0        = zero
!!             else
              v_rho_box%v0(nspin) =  dxc*half
              v_rho_box%v0(1)     = -dxc*half
!!             end if
!DEBUGPRINT
!        if ( mecca_state%intfile%iprint >= -1 ) then
!            write(6,'('' nsub='',i3,'' ic='',i2,'' v0xc='',2g14.5,      &
!     &                                     ''  rho0_out='',2g16.6)')    &
!     &                            nsub,ic,v_rho_box%v0,rho0_1,rho0_2
!        end if
!DEBUGPRINT
            end do
           end if
           nullify(sublat)
          enddo
          nullify(v_rho_box)
          if ( xc_split_type == 4 ) then
           rho0 = qpls/v0i    ! cell interst.density
           if ( rho0 >= epsilon(rs) ) then
            rs = (three/(pi4*rho0))**third
            zeta = qmns/v0i/rho0
            vdif = xc_alpha2(rs,zeta,one,libxc_p%iexch,exch,libxc_p)    &
     &          - xc_alpha2(rs,zeta,-one,libxc_p%iexch,exch,libxc_p)
           else
            vdif = zero
           end if
!
           vmtz(1) = vmtz(1) - vdif*half
           vmtz(nspin) = vmtz(1) + vdif
           do nsub=1,nsublat
            sublat => mecca_state%intfile%sublat(nsub)
            do ic=1,sublat%ncomp
             v_rho_box => sublat%compon(ic)%v_rho_box
             v_rho_box%v0(1) = v_rho_box%v0(1) + vdif*half
             v_rho_box%v0(nspin) = v_rho_box%v0(nspin) - vdif*half
            end do
           end do
           nullify(sublat)
           nullify(v_rho_box)
          end if
         case (5:6)
          vlm_ir = zero
          do nsub=1,nsublat
           sublat => mecca_state%intfile%sublat(nsub)
           if ( sublat%isDLM ) cycle
           do ic=1,sublat%ncomp
            v_rho_box => sublat%compon(ic)%v_rho_box
            ndrpts = v_rho_box%ndrpts
            w_sub = sublat%compon(ic)%concentr*sublat%ns
            rIS = sublat%compon(ic)%rSph
            rr(1:ndrpts)=exp(xr(1:ndrpts,ic,nsub))
             rhotmp(1:ndrpts) = pi4*rr(1:ndrpts)**2
             call  ws_integral(mecca_state,ic,nsub,rhotmp(1:ndrpts),    &
     &                                                  tot_s,sph_s,rIS)
             qpls = tot_s - sph_s                  ! interstitial volume
             call g_rhoval(v_rho_box,1,rhotmp)     ! rho1
             call asa_integral(mecca_state,ic,nsub,rhotmp(1:ndrpts),    &
     &                                                  tot_s,sph_s,rIS)
             qmns = sph_s
             call g_rhoval(v_rho_box,nspin,rhom)   ! rho2
             call asa_integral(mecca_state,ic,nsub,rhom(1:ndrpts),      &
     &                                                  tot_s,sph_s,rIS)
             qmns = qmns - sph_s                   ! IS magn.moment
             Ni = indRmesh(rIS,ndrpts,rr)-4
             vtmp(1:Ni) = zero
             frhoV(1:Ni)= zero
             call calcVarXC(libxc_p,rr(Ni:ndrpts),rhotmp(Ni:ndrpts)     &
     &                   ,rhom(Ni:ndrpts),frhoV(Ni:ndrpts))     ! frhoV = zeta*d(V1-V2)/d(zeta)
             vtmp(Ni:ndrpts) = ( frhoV(Ni:ndrpts) +                     &
     &                            (vrnew(Ni:ndrpts,ic,nsub,1) -         &
     &           vrnew(Ni:ndrpts,ic,nsub,nspin)) / rr(Ni:ndrpts) ) / two
!             rhotmp(Ni:ndrpts) = rhotmp(Ni:ndrpts) + rhom(Ni:ndrpts)  ! rho_plus
!             vtmp(Ni:ndrpts) = rhotmp(Ni:ndrpts) * vtmp(Ni:ndrpts)
             call ws_integral(mecca_state,ic,nsub,vtmp(1:ndrpts),       &
     &                                                  tot_s,sph_s,rIS)
             v0i = (tot_s-sph_s)
             if ( qmns<zero ) then     ! 1==dn, 2==up
              if ( v0i>zero ) v0i=-v0i
             else                      ! 1==up, 2==dn
              if ( v0i<zero ) v0i=-v0i
             end if

             if ( xc_split_type==5 ) then
              vdif=vdif + v0i * w_sub
              vlm_ir = vlm_ir + qpls * w_sub
             else if ( xc_split_type==6 ) then
              v0i = v0i / qpls
              v_rho_box%v0(1)     = -v0i*0.5d0
              v_rho_box%v0(nspin) = v_rho_box%v0(1)+v0i
              vdif=vdif + v0i * w_sub
!DEBUGPRINT
        if ( mecca_state%intfile%iprint >= -1 ) then
            write(6,'('' nsub='',i3,'' ic='',i2,'' v0xc='',2g14.5)')    &
     &                                          nsub,ic,v_rho_box%v0
        end if
!DEBUGPRINT
             end if
           enddo
           nullify(sublat)
          enddo
          nullify(v_rho_box)
          if ( xc_split_type==5 ) then
           vdif = vdif / vlm_ir
           vmtz(1) = vmtz(1) - vdif*0.5d0
           vmtz(2) = vmtz(1) + vdif
          end if
         case default
         end select
       end if
      end if
!
      if ( mecca_state%intfile%iprint >= -1 ) then
       if ( nspin > 1 ) then
        write(6,'(6x,''v0_1, <vdif>'',t30,                              &
     &                           ''='',2f10.5)') vmtz(1),vdif
        write(6,'(6x,''Magn. moment (MT,cell)'',t30,''='',2f10.4)')     &
     &                        tmom_mt,tmom_ws
        if ( associated(gf_out) ) then
         if ( nsublat>1 .or.                                            &
     &  maxval(mecca_state%intfile%sublat(1:nsublat)%ncomp)>1 ) then
        write(6,'(/6x,''component up/dn-electrons and magn.moments:'')')
         end if
         do nsub=1,nsublat
          if ( mecca_state%intfile%nMM==-1 .and. nsub>nsublat/2 ) exit
          sublat => mecca_state%intfile%sublat(nsub)
          do ic=1,sublat%ncomp
          if (mecca_state%intfile%nMM==1 .and. ic>sublat%ncomp/2) cycle
          call atomIDc(dble(sublat%compon(ic)%zid),symb,name)
          write(6,'(5x,''Subl='',i3,1x,2a1,3x,''up '',f8.5,2x,''dn '',  &
     &                       f8.5,2x,''up-dn(cell)='',f10.5,'' mu_B'')')&
     &           nsub,symb,(gf_out%xvalws(ic,nsub,is),is=1,nspin),      &
     &           gf_out%xvalws(ic,nsub,1)-gf_out%xvalws(ic,nsub,nspin)
          end do
         end do
        end if
!DEBUGPRINT
!        en_xcsplit = (vdif(1)-vdif(nspin))*(tmom_ws-tmom_mt)
!        if ( en_xcsplit.ne.zero )                                       &
!     &   write(6,'(6x,''spin-splitting energy'',t30,''='',f10.5)')      &
!     &                        en_xcsplit
       else
        write(6,'(6x,''v0'',t30,''='',f10.5)') vmtz(1)
       end if
       write(6,*)
      end if
!c     =================================================================
!c     new zero for potential...
!c     =================================================================
      do nsub=1,nsublat
        sublat => mecca_state%intfile%sublat(nsub)
        do ic=1,sublat%ncomp
          v_rho_box => sublat%compon(ic)%v_rho_box
          ndrpts = v_rho_box%ndrpts
          rr(1:ndrpts)=exp(xr(1:ndrpts,ic,nsub))

          jend = v_rho_box%jmt                                        ! MT/ASA radius
          jend=max(jend,indRmesh(mecca_state%rs_box%rcs(nsub),ndrpts,   &
     &                                              rr(1:ndrpts))) + 1   ! Grid point just above the R_cir
          jend=min(jend,size(vrnew,1))
          do is=1,nspin
           v0i = vmtz(is)+v_rho_box%v0(is)
           vrnew(1:jend,ic,nsub,is) = vrnew(1:jend,ic,nsub,is) -        &
     &                                                   v0i*rr(1:jend)
           vrnew(jend+1:,ic,nsub,is) = zero
           v_rho_box%v0(is) = v0i
          enddo                          ! end loop over spin
        enddo
      enddo                         ! end loop over sublattices

      return
!EOC
      end subroutine updateV0
!
!BOP
!!IROUTINE: calcVarXC
!!INTERFACE:
      subroutine calcVarXC(xc_p,rr,rho1,rho2,varXC)
!!DESCRIPTION:
! numerical calculation of spin splitting variation

!!DO_NOT_PRINT
      implicit none
!!DO_NOT_PRINT

!!ARGUMENTS:
      type(xc_mecca_pointer) :: xc_p
      real(8), intent(in) :: rr(:),rho1(:),rho2(:)
      real(8), intent(out) :: varXC(:)
!!REVISION HISTORY:
! Initial Version - A.S. - 2016
!!REMARKS:
! this is a private procedure of calcPot module subroutine
!EOP
!
!BOC

      integer :: i,nd
      real(8) :: Rho(size(rr)),sp_Rho(size(rr))
      real(8) :: sp1Rho(size(rr)),sp2Rho(size(rr)),zeta(size(rr))
      real(8) :: dz(size(rr)),dz1(size(rr)),dz2(size(rr))
      real(8) :: Vsp1(size(rr)),Vsp2(size(rr)),exc(size(rr))
      real(8) :: dzi

      real(8), parameter :: epsrho=1.d-8
      real(8), parameter :: delt=1.d-2 ! step for variation

!      varXC = zero
!DEBUG
!      return
!DEBUG
      nd=size(rr)
      Rho = (rho1+rho2)/(pi4*rr**2)
      sp_Rho = (rho1-rho2)/(pi4*rr**2)
      do i=1,nd
       if ( Rho(i) <= epsrho .or. abs(sp_Rho(i))>Rho(i) ) then
        zeta(i) = zero
        dz(i) = zero
       else
        zeta(i) = sp_Rho(i)/Rho(i)
        dz(i) = delt*zeta(i)
       end if
      end do
!!!
      Rho = half*Rho
!!!

      do i=1,nd
       dzi = zeta(i)+dz(i)
       if ( abs(dzi)>one ) then
        if ( dzi>one ) then
         dzi = one
        else
         dzi = -one
        end if
       end if
       dz1(i) = dzi-zeta(i)
      end do

      sp1Rho = Rho*(one + (zeta+dz1))
      sp2Rho = Rho*(one - (zeta+dz1))

      call xc_alpha(2,nd,rr,sp1Rho,sp2Rho,exc,Vsp1,Vsp2,libxc_p=xc_p)
      varXC = Vsp1-Vsp2

      do i=1,nd
       dzi = zeta(i)-dz(i)
       if ( abs(dzi)>one ) then
        if ( dzi>one ) then
         dzi = one
        else
         dzi = -one
        end if
       end if
       dz2(i) = dzi-zeta(i)
      end do

      sp1Rho = Rho*(one + (zeta+dz2))
      sp2Rho = Rho*(one - (zeta+dz2))

      call xc_alpha(2,nd,rr,sp1Rho,sp2Rho,exc,Vsp1,Vsp2,libxc_p=xc_p)
      varXC = varXC - (Vsp1-Vsp2)

      do i=1,nd
       dzi = dz1(i)-dz2(i)
       if ( dzi == zero ) then
        varXC(i) = zero
       else
        varXC(i) = varXC(i)/dzi
       end if
      end do
      varXC = varXC * zeta

      return
!EOC
      end subroutine calcVarXC
!
!BOP
!!IROUTINE: calcTotEn
!!INTERFACE:
      subroutine calcTotEn(mecca_state,e0intrC,e_mf_cc,xr,rho)
!!DESCRIPTION:
! to calculate total energy

!!DO_NOT_PRINT
      implicit none
!!DO_NOT_PRINT

!!ARGUMENTS:
      type(RunState), target :: mecca_state
      real(8), intent(in) :: e0intrC(:)
      real(8), intent(in) :: e_mf_cc(:,:)
      real(8), intent(in) :: xr(:,:,:)
      real(8), intent(in) :: rho(:,:,:,:)
!!REVISION HISTORY:
! Initial Version - A.S. - 2013
!!REMARKS:
! this is a private procedure of calcPot module subroutine
!EOP
!
!BOC

      type(Sublattice),  pointer :: sublat
      type(SphAtomDeps), pointer :: v_rho_box
      type ( IniFile ),  pointer :: inFile
      type ( RS_Str ),   pointer :: rs_box
      type (GF_output),  pointer :: gf_out
      type (Work_data),  pointer :: work_box    ! output (potential,energy)
      real(8) :: rr(size(xr,1),size(xr,2),size(xr,3))
      real(8) :: rhotmp(size(xr,1))
      real(8) :: ztotss(size(rho,2),size(rho,3))
      real(8) :: corden(size(rho,1),size(rho,2),size(rho,3),size(rho,4))
      real(8) :: ecor(size(rho,2),size(rho,3),size(rho,4))
      real(8) :: esemc(size(rho,2),size(rho,3),size(rho,4))
      real(8) :: atcon(size(rho,2),size(rho,3))
      integer :: komp(size(rho,3))
      integer :: jmt(size(rho,2),size(rho,3))
      integer :: nsub,ic,is
      integer :: iexch,iprint,mtasa,ndrpts,ndcomp,nsublat,nspin
      character(10) :: istop

      real(8) :: excort(2)=zero,qintex(2)=zero
      real(8) :: emtc,emad,press_mad,Eccsum0,Eccsum,sp
      real(8) :: tot_s,sph_s

      emtc = zero         ! it is included in e0intrC due to PBC
      emad = e0intrC(1)   ! e0intrC is sum of Madelung energy, PBC and VP corrections
      press_mad = e0intrC(2)  ! ????? how to estimate pressure for <rho0 + multipoles + PBC>?
!
      inFile   => mecca_state%intfile
      rs_box   => mecca_state%rs_box
      gf_out   => mecca_state%gf_out_box
      work_box => mecca_state%work_box

      call g_ndims(inFile,ndrpts,ndcomp,nsublat,nspin)
      if ( min(ndrpts,ndcomp,nsublat,nspin) <1 ) then
        write(*,*) ' ndrpts,ndcomp,nsublat,nspin: ',                    &
     &                                      ndrpts,ndcomp,nsublat,nspin
        call fstop(' calcTotEn: FATAL ERROR ')
      end if
      if ( nspin==1 ) then
        sp = 2     ! spin factor for core electrons
      else
        sp = 1
      end if
      iexch = inFile%iXC
      iprint = inFile%iprint
      istop = inFile%istop
      mtasa = inFile%mtasa
      Eccsum = zero
      do nsub=1,nsublat
       sublat => mecca_state%intfile%sublat(nsub)
       komp(nsub) = sublat%ncomp
       atcon(1:komp(nsub),nsub) = sublat%compon(1:komp(nsub))%concentr
       ztotss(1:komp(nsub),nsub) = sublat%compon(1:komp(nsub))%zID
       Eccsum0 = zero
       do ic=1,komp(nsub)
        v_rho_box => sublat%compon(ic)%v_rho_box
        ndrpts = v_rho_box%ndrpts
        rr(1:ndrpts,ic,nsub) = exp(xr(1:ndrpts,ic,nsub))
        rr(ndrpts+1:,ic,nsub) = zero
        jmt(ic,nsub) = 1+indRmesh(v_rho_box%xmt,ndrpts,xr(:,ic,nsub))
        do is=1,nspin
          call g_rhocor(v_rho_box,is,corden(:,ic,nsub,is))
          call g_ecor(v_rho_box,is,inFile%ebot,ecor(ic,nsub,is),        &
     &                                        esemc(ic,nsub,is),        &
     &                inFile%Tempr,gf_out%efermi,nspin)
          call g_rhosemicore(ic,nsub,is,rhotmp)
          if ( rhotmp(1) .ne. zero ) then
           gf_out%evalsum(ic,nsub,is) = gf_out%evalsum(ic,nsub,is) +    &
     &            g_esemicorr(ic,nsub,is)
!     &            sp*esemc(ic,nsub,is)
!     &            sp*(esemc(ic,nsub,is) + g_esemicorr(ic,nsub,is))
!           esemc(ic,nsub,is) = zero
          end if
        end do
        nullify(v_rho_box)
        Eccsum0 = Eccsum0 + e_mf_cc(ic,nsub)*atcon(ic,nsub)
       end do
       Eccsum = Eccsum + Eccsum0*rs_box%numbsubl(nsub)
       if(iprint.ge.0.and.abs(Eccsum0).gt.1.d-8) then
           write(6,'(''  MF per sublatt.('',                            &
     &               i3,''), charge-correlated E = '',                  &
     &         f12.8)') nsub,Eccsum0
       end if
      end do

      call setMS(mecca_state)

          call tote1(                                                   &
     &               work_box%vr,rho,corden,                            &
     &               xr,rr,                                             &
     &               jmt,nsublat,nspin,iexch,                           &
     &               rs_box%numbsubl,komp,atcon,                        &
     &               ztotss,rs_box%volume,                              &
     &               excort,qintex,emtc,emad,press_mad,                 &
     &               gf_out%evalsum,ecor,esemc,                         &
     &               work_box%etot,work_box%press,                      &
     &               Eccsum,mtasa,iprint,istop                          &
     &              )

      return
!EOC
      end subroutine calcTotEn
!
!BOP
!!IROUTINE: coulombShapeCorr
!!INTERFACE:
      subroutine coulombShapeCorr(mecca_state,en_vpasa,pot_vpasa)
!!DESCRIPTION:
! calculates Coulomb (VP-ASA) energy correction
! (interstitial charge density is approximated by a constant)

!!DO_NOT_PRINT
      implicit none
!!DO_NOT_PRINT

!!ARGUMENTS:
      type(RunState), target :: mecca_state
      real(8), intent(out) :: en_vpasa
      real(8), intent(out) :: pot_vpasa(:,:)
!!REVISION HISTORY:
! Initial Version - A.S. - 2013
!!REMARKS:
! this is a private procedure of calcPot module subroutine
!EOP
!
!BOC
      type(Sublattice),  pointer :: sublat
      type(VP_data),    pointer :: vp_box
      real(8) :: zID,w_sub,rho0,rho_s,q0,q0sph,Pot,En
      real(8) :: rWS,alphpot,alphen,qsph,dE,de_sub
      real(8) :: potnfc(size(pot_vpasa,1))
      real(8), parameter :: pi4d3=four*pi/three
      integer :: nsub,ic
      en_vpasa = zero
      pot_vpasa = zero
      potnfc    = zero
      if ( .not. associated(mecca_state%vp_box) )        return
      if ( .not. allocated(mecca_state%vp_box%alphpot) ) return
      if ( .not. allocated(mecca_state%vp_box%alphen) )  return
      vp_box => mecca_state%vp_box
      rho0 = mecca_state%intfile%rho0
      do nsub=1,mecca_state%intfile%nsubl
       sublat => mecca_state%intfile%sublat(nsub)
       rWS = mecca_state%rs_box%rws(nsub)
       de_sub = zero
       do ic=1,sublat%ncomp
        zID =  sublat%compon(ic)%zID
        w_sub = sublat%compon(ic)%concentr*sublat%ns
        alphpot = vp_box%alphpot(nsub)
        alphen  = vp_box%alphen(nsub)
        call HmgnsRhoEnPot(rho0,rWS,rWS,alphpot,alphen,q0,q0sph,Pot,En)
        rho_s = Qintr(ic,nsub)%q0 /                                     &
     &                     (pi4d3*(rWS**3-Qintr(ic,nsub)%SphR**3))
        qsph =  Qintr(ic,nsub)%qsph
        dE = -( (Qintr(ic,nsub)%qm1(2)-Qintr(ic,nsub)%qm1(1)) -         &
     &                              Pot*(rho_s-rho0) ) * (zID-qsph) +   &
     &       En*((rho_s-rho0)**2+two*rho0*(rho_s-rho0))
        de_sub = de_sub + dE*sublat%compon(ic)%concentr
!
        pot_vpasa(ic,nsub) =                                            &
     &                  -(Qintr(ic,nsub)%qm1(2)-Qintr(ic,nsub)%qm1(1))  &
     &                  +      Pot*(rho_s-rho0)
!
       end do
       en_vpasa = en_vpasa + de_sub*mecca_state%rs_box%numbsubl(nsub)
!
! Near-Field corrections
!
       call getLocalNFC(nsub,mecca_state,potnfc)

       pot_vpasa(1:sublat%ncomp,nsub) = pot_vpasa(1:sublat%ncomp,nsub)  &
     &                                + potnfc(1:sublat%ncomp)

      end do
      return
!EOC
      end subroutine coulombShapeCorr
!
      subroutine getLocalNFC(nsub,mecca_state,potnfc,aYlMns)
      implicit none
      integer, intent(in) :: nsub
      type(RunState) :: mecca_state
      real(8), intent(out) :: potnfc(:)
      real(8), intent(out), allocatable, optional :: aYlMns(:)
      real(8), parameter :: r2a=(dble(4)*pi/3)**(dble(1)/3)
      real(8), parameter :: pi4 = dble(4)*pi
      real(8), parameter :: Y0=dble(1)/sqrt(pi4)
      type(SphAtomDeps), pointer :: v_rho_box
      integer :: inn,ksub,lm,lmax,lmx,nmom,nnb
      integer :: i,ic,imt,is,ndata,nr
      real(8) :: atcon,aunit,a0,a0nfc,h,rasa,rcs,ris
      real(8), allocatable :: xr(:),r(:),rr(:),rho(:),coeff(:)
      potnfc = zero
      if ( allocated(mecca_state%vp_box%ylmMns) ) then
       if ( allocated(mecca_state%vp_box%ylmMns(nsub)%array) ) then
!
! Near-Field corrections (PRB 84,205106)
!
        nspin = mecca_state%intfile%nspin
        nmom = size(mecca_state%vp_box%ylmMns(nsub)%array,1)-1
        lmax = size(mecca_state%vp_box%ylmMns(nsub)%array,2)-1
        potnfc = zero
        if ( lmax<0 .or. nmom<0 ) return
        if ( present(aYlMns) ) then
         if ( lmax>0 ) then
          if ( allocated(aYlMns) ) deallocate(aYlMns)
          allocate(aYlMns(1:lmax))
          aYlMns = zero
         end if
        else
         lmax = 0
        end if
        nnb = size(mecca_state%vp_box%ylmMns(nsub)%array,3)
        allocate(coeff(0:nmom))
        coeff = zero
        a0nfc = zero
!
! from neighbors (Eq.6)
!
!DEBUGPRINT
         write(6,*) ' DEBUG NFC nsub=',nsub,' nnb=',nnb
!DEBUGPRINT
        do inn=2,nnb
         ksub = mecca_state%vp_box%ylmMns(nsub)%indx(inn)
         rasa = mecca_state%rs_box%rws(ksub)
         ris = mecca_state%rs_box%rmt(ksub)
         aunit = r2a*rasa
         sublat => mecca_state%intfile%sublat(ksub)
         nr = maxval(sublat%compon(1:sublat%ncomp)%v_rho_box%ndchg)
         if ( allocated(xr) ) then
          if ( size(xr)<nr ) deallocate(xr,r,rr,rho)
         end if
         if ( .not. allocated(xr) ) then
          allocate(xr(nr),r(nr),rr(nr),rho(nr))
         end if
!DEBUGPRINT
!           do i=0,nmom
!           write(6,'(a,i3,2g14.5)') ' I=',                              &
!     &     i,mecca_state%vp_box%ylmMns(nsub)%array(i,lm,inn)
!           end do
!           write(6,'(3(a,g15.5))')                                      &
!     &      ' Rnb=',mecca_state%vp_box%ylmMns(nsub)%r0nn(inn)
!DEBUGPRINT
         do ic=1,sublat%ncomp
          atcon = sublat%compon(ic)%concentr
          v_rho_box => sublat%compon(ic)%v_rho_box
          nr = v_rho_box%ndchg
          call g_meshv(v_rho_box,h,xr,rr)
          imt = indRmesh(ris,nr,rr)
          do is=1,nspin
           lm = 1
           rho(imt:nr) = v_rho_box%chgtot(imt:nr,is)/(pi4*rr(imt:nr)**2)
           ndata = nr-imt+1
           r(imt:nr) = rr(imt:nr)/aunit
           call plnmcoeff(ndata,r(imt:nr),rho(imt:nr),1,nmom,coeff)
           a0 = atcon * sum(coeff(0:nmom)*                              &
     &         mecca_state%vp_box%ylmMns(nsub)%array(0:nmom,lm,inn))
           a0nfc = a0nfc + pi4*aunit**2*a0
           if ( lmax>0 ) then
            do lm=1,lmax
             a0 = atcon * sum(coeff(0:nmom)*                            &
     &         mecca_state%vp_box%ylmMns(nsub)%array(0:nmom,1+lm,inn))
             aYlMns(lm) = aYlMns(lm) + pi4*aunit**(2+lm)*a0/(2*lm+1)
            end do
           end if
          end do
         end do
!DEBUGPRINT
!         write(6,*) ' inn=',inn,' ksub=',ksub,' a0(du)=',a0
!DEBUGPRINT
        end do
        nullify(sublat)
!DEBUGPRINT
         write(6,*) ' nsub=',nsub,' A0NFC=',a0nfc
!DEBUGPRINT
        potnfc(1:mecca_state%intfile%sublat(nsub)%ncomp)=a0nfc
!
! Eq.8 contribution is taken into account by means of alphpot
! and specific definition of Hartree potential (in newpot)

!        sublat => mecca_state%intfile%sublat(nsub)
!        rasa = mecca_state%rs_box%rws(nsub)
!        rcs = mecca_state%rs_box%rcs(nsub)
!        ris = mecca_state%rs_box%rmt(nsub)
!        do i=0,nmom
!          sphmom(i) = pi4*(rcs**(i+2)-ris**(i+2))/(i+2)
!        end do
!        do ic=1,sublat%ncomp
!          v_rho_box => sublat%compon(ic)%v_rho_box
!          nr = v_rho_box%ndchg
!          call g_meshv(v_rho_box,h,xr,rr)
!          imt = indRmesh(ris,nr,rr)
!          a0 = zero
!          do is=1,nspin
!           rho(imt:nr) = v_rho_box%chgtot(imt:nr,is)/(pi4*rr(imt:nr)**2)
!           ndata = nr-imt+1
!           call plnmcoeff(ndata,rr(imt:nr),rho(imt:nr),1,nmom,coeff)
!           a0 = a0 + sum( coeff(0:nmom)*sphmom(0:nmom) )
!          end do
!         potnfc(ic) = potnfc(ic) - pi4*a0
!        end do

        potnfc = -potnfc*2   ! in at.units

        if ( allocated(xr) ) deallocate(xr,r,rr,rho)
!        if ( allocated(sphmom) ) deallocate(sphmom)
        if ( allocated(coeff) ) deallocate(coeff)
       end if
      end if
      return
      end subroutine getLocalNFC
!
      end subroutine calcPot
!
!BOP
!!IROUTINE: calcPot0
!!INTERFACE:
      subroutine calcPot0( mecca_state, do_etot )
!!DESCRIPTION:
! mecca\_ames version (without PBC)
!
!!REVISION HISTORY:
! Initial Version - A.S. - 2013
!EOP
!
!BOC
!      use struct, only : gSublatVlms
      implicit none
      type(RunState), intent(inout), target :: mecca_state
      logical, intent(in) :: do_etot

      real(8), allocatable :: xvalws(:,:,:)  ! (ipcomp,ipsublat,ipspin)
      real(8), allocatable :: qtotmt(:,:,:)  ! (ipcomp,ipsublat,ipspin)
      real(8), allocatable :: ztotss(:,:)    ! (ipcomp,ipsublat)
      real(8), allocatable :: qcorss(:,:,:)  ! (ipcomp,ipsublat,ipspin)
      real(8), allocatable :: ecor(:,:,:)    ! (ipcomp,ipsublat,ipspin)
      real(8), allocatable :: esemc(:,:,:)   ! (ipcomp,ipsublat,ipspin)
      real(8), allocatable :: rho(:,:,:,:)   ! (iprpts,ipcomp,ipsublat,ipspin)
      real(8), allocatable :: corden(:,:,:,:)   ! (iprpts,ipcomp,ipsublat,ipspin)
!      real*8     madmat(nbasis,*)
!      real*8     omegint,omega
!      real*8     omegmt(ipsublat)
      real(8), allocatable :: rr(:,:,:)  !  (iprpts,ipcomp,nsublat)
      real(8), allocatable :: xr(:,:,:)  !  (iprpts,ipcomp,nsublat)
      real(8), allocatable :: rhotmp(:)
      real(8) :: h,rScat,sp
      type(Sublattice),  pointer :: sublat
      type(SphAtomDeps), pointer :: v_rho_box
      integer, allocatable :: jmt(:,:)       ! (ipcomp,nsublat)
      real(8), allocatable :: rmt_true(:,:)  ! (ipcomp,nsublat)
      real(8), allocatable :: r_circ(:,:)    ! (ipcomp,nsublat)
      integer    nsublat
      real(8), allocatable :: atcon(:,:)
      integer, allocatable :: komp(:)
      integer    nbasis
      integer    nspin,nMM,iexch
      real(8) :: excort(ipspin)
      real(8) :: qintex(ipspin)
      real(8) :: emad,emadp,emtc
      real(8) :: vmtzup
      real(8) :: vdif
!??      real(8) :: tot_s,sph_s
      real(8) :: Eccsum
      integer :: ivar_mtz    ! if ivar_mtz==0 --> non-variational (i.e. old) ASA mtz
      integer :: mtasa
      integer :: ic,is,nsub,ndcomp,ndrpts
      integer :: iprint
      character(10) :: istop
      logical :: non_zero_T=.true.

      type ( IniFile ),   pointer :: inFile
      type ( RS_Str ),    pointer :: rs_box
      type (VP_data),    pointer :: vp_box
      type (GF_output),   pointer :: gf_out
      type (Work_data),   pointer :: work_box    ! output (potential,energy)
!      type ( KS_Str ),    pointer :: ks_box
!      type ( EW_Str ),    pointer :: ew_box
!      type (GF_data),     pointer :: gf_box

!      integer iasa

      inFile => mecca_state%intfile
      rs_box => mecca_state%rs_box
!      ks_box => mecca_state%ks_box
!      ew_box => mecca_state%ew_box
!      gf_box => mecca_state%gf_box

      call g_ndims(inFile,ndrpts,ndcomp,nsublat,nspin)
      if ( min(ndrpts,ndcomp,nsublat,nspin) <1 ) then
        write(*,*) ' ndrpts,ndcomp,nsublat,nspin: ',                    &
     &                                      ndrpts,ndcomp,nsublat,nspin
        call fstop(' calcPot0: FATAL ERROR ')
      end if
!DEBUG      nspin = inFile%nspin
      nMM = inFile%nMM
      iexch = inFile%iXC
      iprint = inFile%iprint
      istop = inFile%istop

      call g_komp(inFile,komp)
      call g_atcon(inFile,atcon)

      mtasa = inFile%mtasa
      ivar_mtz = inFile%intgrsch
!      if ( inFile%mtasa == 0 ) then
!        iasa=0
!        ivar_mtz = 0
!      else
!        iasa = 1
!!        if ( mtasa == 1 ) ivar_mtz = 0
!      end if

!      call gSublatVlms(iasa,rs_box,omega,omegaint,omegamt,surfamt)

!DEBUG      ndcomp = size(atcon,1)
!DEBUG      nsublat = size(atcon,2)
      if ( nsublat .ne. rs_box%nsubl ) then
        mecca_state%fail = .true.
        call fstop('ERROR :: inconsistent data (nsubl)')
        return
      end if
      if ( nspin==1 ) then
        sp = 2     ! spin factor for core electrons
      else
        sp = 1
      end if
      nbasis = sum(rs_box%numbsubl(1:nsublat))
!DEBUG      ndrpts = 0
!DEBUG      do nsub=1,nsublat
!DEBUG       sublat => inFile%sublat(nsub)
!DEBUG       do ic=1,komp(nsub)
!DEBUG        v_rho_box =>  sublat%compon(ic)%v_rho_box
!DEBUG        ndrpts = max(ndrpts,v_rho_box%ndrpts)
!DEBUG        nullify(v_rho_box)
!DEBUG       end do
!DEBUG      end do
      allocate(jmt(1:ndcomp,1:nsublat))
      allocate(rmt_true(1:ndcomp,1:nsublat))
      allocate(r_circ(1:ndcomp,1:nsublat))
      allocate(ztotss(1:ndcomp,1:nsublat))
      allocate(xr(1:ndrpts,1:ndcomp,1:nsublat))
      allocate(rr(1:ndrpts,1:ndcomp,1:nsublat))
      allocate(rho(1:ndrpts,1:ndcomp,1:nsublat,1:nspin))
      allocate(corden(1:ndrpts,1:ndcomp,1:nsublat,1:nspin))
      allocate(qcorss(1:ndcomp,1:nsublat,1:nspin))
      allocate(qtotmt(1:ndcomp,1:nsublat,1:nspin))
      allocate(xvalws(1:ndcomp,1:nsublat,1:nspin))
      allocate(ecor(1:ndcomp,1:nsublat,1:nspin))
      allocate(esemc(1:ndcomp,1:nsublat,1:nspin))
      allocate(rhotmp(1:size(xr,1)))
      if ( .not.allocated(mecca_state%work_box%vr) ) then
        allocate (mecca_state%work_box%vr(1:ndrpts,1:ndcomp,1:nsublat,  &
     &                                                        1:nspin))
      end if

      gf_out => mecca_state%gf_out_box
      work_box => mecca_state%work_box
      non_zero_T = .true.
      do nsub=1,nsublat
       sublat => inFile%sublat(nsub)
       do ic=1,komp(nsub)
        ztotss(ic,nsub) = sublat%compon(ic)%zID
        v_rho_box => sublat%compon(ic)%v_rho_box
        call g_meshv(v_rho_box,h,xr(:,ic,nsub),rr(:,ic,nsub))
        rScat = exp(v_rho_box%xmt)
        jmt(ic,nsub) = 1+indRmesh(rScat,ndrpts,rr(:,ic,nsub))
!        rmt_true(ic,nsub) = rs_box%rmt(nsub)
        rmt_true(ic,nsub) = sublat%compon(ic)%rSph
        r_circ(ic,nsub) = rs_box%rcs(nsub)
        do is=1,nspin
          call g_rho(v_rho_box,is,rho(:,ic,nsub,is))
          call g_rhocor(v_rho_box,is,corden(:,ic,nsub,is))
          call g_totchg(v_rho_box,non_zero_T,is,qtotmt(ic,nsub,is),     &
     &                                               qcorss(ic,nsub,is))
          call g_ecor(v_rho_box,is,inFile%ebot,ecor(ic,nsub,is),        &
     &                                        esemc(ic,nsub,is),        &
     &                inFile%Tempr,gf_out%efermi,nspin)
!??          if ( mtasa==0 ) then
!??           call asa_integral(mecca_state,ic,nsub,                        &
!??     &          rho(1:v_rho_box%ndchg,ic,nsub,is),tot_s,sph_s)
!??           qtotmt(ic,nsub,is) = sph_s
!??          end if
          if ( do_etot ) then
           call g_rhosemicore(ic,nsub,is,rhotmp)
           if ( rhotmp(1) .ne. zero ) then
!            corden(1:v_rho_box%ndcor,ic,nsub,is) =                      &
!     &        corden(1:v_rho_box%ndcor,ic,nsub,is) -                    &
!     &           rhotmp(1:v_rho_box%ndcor)
            gf_out%evalsum(ic,nsub,is) = gf_out%evalsum(ic,nsub,is) +   &
     &            g_esemicorr(ic,nsub,is)
!     &            sp*esemc(ic,nsub,is)
!     &            sp*(esemc(ic,nsub,is) + g_esemicorr(ic,nsub,is))
            esemc(ic,nsub,is) = zero
           end if
          end if
        end do
        nullify(v_rho_box)
       end do
      end do

      vp_box => mecca_state%vp_box
          call genpot(gf_out%xvalws,qtotmt,ztotss,qcorss,               &
     &                  rho,work_box%vr,ivar_mtz,                       &
     &                  vmtzup,rs_box%volume,inFile%alat,               &
     &                  rs_box%itype,rs_box%numbsubl,nsublat,           &
     &                  rs_box%rslatt,rs_box%xyzr,                      &
     &                  rr,xr,jmt,rmt_true,r_circ,                      &
     &                    vp_box,                                       &
     &                  atcon,komp,nbasis,nspin,nMM,iexch,              &
     &                  excort,qintex,emad,emadp,emtc,                  &
     &                  vdif,rs_box%Rnn,Eccsum,mtasa,                   &
     &                  iprint,istop)
        work_box%vmtz(1) = vmtzup
        work_box%vmtz(2) = vmtzup + vdif
!?          do nsub=1,nsublat
!?           sublat => inFile%sublat(nsub)
!?           do ic=1,komp(nsub)
!?            v_rho_box => sublat%compon(ic)%v_rho_box
!??            v_rho_box%v0(1) = vmtzup
!??            v_rho_box%v0(2) = vmtzup+vdif
!?        nullify(v_rho_box)
!?           end do
!?          end do
        if ( do_etot ) then
          call tote1(                                                   &
     &               work_box%vr,rho,corden,                            &
     &               xr,rr,                                             &
     &               jmt,nsublat,nspin,iexch,                           &
     &               rs_box%numbsubl,komp,atcon,                        &
     &               ztotss,rs_box%volume,                              &
     &               excort,qintex,emtc,emad,emadp,                     &
     &               gf_out%evalsum,ecor,esemc,                         &
     &               work_box%etot,work_box%press,                      &
     &               Eccsum,mtasa,iprint,istop                          &
     &              )
        end if

      deallocate(jmt)
      deallocate(rmt_true)
      deallocate(r_circ)
      deallocate(ztotss)
      deallocate(xr)
      deallocate(rr)
      deallocate(rho)
      deallocate(corden)
      deallocate(qcorss)
      deallocate(qtotmt)
      deallocate(xvalws)
      deallocate(ecor)
      deallocate(esemc)
      deallocate(komp)
      deallocate(atcon)
      deallocate(rhotmp)

      nullify(vp_box)
      nullify(work_box)
      nullify(gf_out)
      nullify(inFile)
      nullify(rs_box)

      return
!EOC
      end subroutine calcPot0
!
!cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
!
!BOP
!!IROUTINE: newpot
!!INTERFACE:
      subroutine newpot(ztotss,rhorr,vrnew,libxc_p,                     &
     &                  potmad,vqeff,rr,xr,jmt,komp,                    &
     &                  r_circ,vrcorr)
!!DESCRIPTION:
! {\bv
! calculate hartree and exchange-correlation potential .........
! mean-field, charge-correlated CPA included     by DDJ  Dec 1993
! \ev}
!
!!USES:
      use mecca_constants, only : zero,one,two,pi,ipotoutmt
      use xc_mecca, only : xc_mecca_pointer
!      use xc_mecca, only : defXCforES,undefXCforES

!!DO_NOT_PRINT
      implicit none
!!DO_NOT_PRINT

!!ARGUMENTS:
      real(8), intent(in) :: ztotss(:)  ! (ipcomp)
      real(8), intent(in) :: rhorr(:,:,:) ! (iprpts,ipcomp,nspin)
      real(8), intent(out) :: vrnew(:,:,:) ! (iprpts,ipcomp,nspin)
      type(xc_mecca_pointer) :: libxc_p
      real(8), intent(in) :: potmad       ! (vmt1=-potmad)
      real(8), intent(in) :: vqeff(:)  ! (ipcomp)
      real(8), intent(in) :: rr(:,:) ! (iprpts,ipcomp)
      real(8), intent(in) :: xr(:,:) ! (iprpts,ipcomp)
      integer, intent(in) :: jmt(:)  ! (ipcomp)
      integer, intent(in) :: komp
      real(8), optional :: r_circ(:)
      real(8), pointer, optional :: vrcorr(:,:)
!!REVISION HISTORY:
! Adapted - A.S. - 2013
!EOP
!
!BOC
!
      integer, external :: indRmesh
!      character(512), external :: descrXC
!
      real(8) vxup(size(xr,1),komp)
      real(8) vxdn(size(xr,1),komp)
      real(8) totrho(size(xr,1),komp)
      real(8) vxc
      real(8), pointer :: rho1int(:,:)
      real(8), pointer :: rho2int(:,:)
      real(8), pointer :: vhart(:,:)
      integer :: ik,ir,jend(komp),nsp,ndrpts
!
      vrnew = zero
      nsp = size(rhorr,3)
      ndrpts = size(vrnew,1)
      do ik = 1,komp
        totrho(:,ik) = zero
        do ir = 1,ndrpts
          totrho(ir,ik) = sum(rhorr(ir,ik,1:nsp))
        enddo
      enddo

!
! Hartree piece
      allocate(rho1int(ndrpts,1:komp))
      allocate(rho2int(ndrpts,1:komp))
      allocate(vhart(ndrpts,1:komp))
      do ik=1,komp
       if ( present(r_circ) ) then
         jend(ik) = indRmesh(r_circ(ik),size(rr,1),rr(:,ik))+1
       else
         jend(ik) = jmt(ik)
       end if
       call qexpup(1,totrho(:,ik),jend(ik),xr(:,ik),rho2int(:,ik))
       rho2int(1:jend(ik),ik) = rho2int(1:jend(ik),ik) +                &
     &                                       totrho(1,ik)*rr(1,ik)/3.d0
       call qexpup(0,totrho(:,ik),jend(ik),xr(:,ik),rho1int(:,ik))
       call twoZ(ztotss(ik),rr(1:jmt(ik),ik),jmt(ik),                   &
     &                                             vhart(1:jmt(ik),ik))
       vhart(1:jmt(ik),ik) = vhart(1:jmt(ik),ik) + two *                &
     &  ( rho2int(1:jmt(ik),ik) +                                       &
     &   (rho1int(jmt(ik),ik)-rho1int(1:jmt(ik),ik))*rr(1:jmt(ik),ik) )
       select case ( ipotoutmt )
        case (0)  ! V(r) = V_h(Rasa|Rasa), r>Rasa;
         vhart(jmt(ik)+1:jend(ik),ik)=vhart(jmt(ik),ik)/rr(jmt(ik),ik)* &
     &                 rr(jmt(ik)+1:jend(ik),ik)

!           case (1)  ! V(r) = V(Rasa), r>Rasa
!            vhart=two*( (-ztotss(ik)+rho2int(jmt(ik)))/rr(jmt(ik),ik) )
!            ro3=(three*(rr(jmt(ik),ik)**2)/totrho(jmt(ik),ik))**third
!            vx(ir,ik)=xc_alpha2(ro3,dz(jmt(ik),ik),sp,iexch,enxc,libxc_p)

!           case (2)  ! not implemented
!            vhart=two*( (-ztotss(ik)+rho2int(jmt(ik)))/rr(jmt(ik),ik)
!            ro3=(three*(rr(jmt(ik),ik)**2)/totrho(jmt(ik),ik))**third

        case (4)  ! V(r) = V_h(r|Rcs), r>Rasa
         call twoZ(ztotss(ik),rr(jmt(ik)+1:jend(ik),ik),jend(ik)-jmt(ik)&
     &                                    ,vhart(jmt(ik)+1:jend(ik),ik))
         vhart(jmt(ik)+1:jend(ik),ik) = vhart(jmt(ik)+1:jend(ik),ik) +  &
     &    two*( rho2int(jmt(ik)+1:jend(ik),ik) +(rho1int(jmt(ik),ik)-   &
     &       rho1int(jmt(ik)+1:jend(ik),ik))*rr(jmt(ik)+1:jend(ik),ik) )

        case default  ! V(r) = V_h(Rasa|Rasa)*Rasa/r, r>Rasa
         vhart(jmt(ik)+1:jend(ik),ik) = vhart(jmt(ik),ik)*rr(jmt(ik),ik)
       end select
      end do

!c exchange-correlation potential

      call getXCpot(libxc_p,nint(ztotss),nsp,rr,rhorr(:,1:komp,1)       &
     &                        ,rhorr(:,1:komp,nsp),vxup,vxdn)
      do ik=1,komp
!c
         if ( present(vrcorr) ) then  ! correlation potential
          if ( associated(vrcorr) ) then
           if ( size(vrcorr)>1 ) then
           vrcorr(1:jend(ik),ik)=vxup(1:jend(ik),ik)+vxdn(1:jend(ik),ik)
           vrcorr(1:jend(ik),ik)= half*vrcorr(1:jend(ik),ik) *          &
     &                                   rr(1:jend(ik),ik)
           vrcorr(jend(ik)+1:,ik) = zero
           end if
          end if
         end if
!#!c
!#!c new potential, inlcuding ASA-Madelung and mean-field, charge-correlated CPA
!#!
!#         vrnew(1:jend(ik),ik,1)=vhart(1:jend(ik),ik) + rr(1:jend(ik),ik)&
!#     &                 * ( vxup(1:jend(ik),ik) + (potmad + vqeff(ik)) )
!#         if ( nsp>1 ) then
!#          vrnew(1:jend(ik),ik,1)=vhart(1:jend(ik),ik)+rr(1:jend(ik),ik) &
!#     &                 * ( vxdn(1:jend(ik),ik) + (potmad + vqeff(ik)) )
!#         end if
!#         vrnew(jend(ik)+1:,ik,1:2) = zero
!#
         do ir=1,jend(ik)
!c
!c new potential, inlcuding ASA-Madelung and mean-field, charge-correlated CPA
!
          vxc = vxup(ir,ik)
          vrnew(ir,ik,1)=vhart(ir,ik)+(vxc+potmad+vqeff(ik))*rr(ir,ik)
          if ( nsp>1 ) then
           vxc = vxdn(ir,ik)
           vrnew(ir,ik,2)=vhart(ir,ik)+(vxc+potmad+vqeff(ik))*rr(ir,ik)
          end if
         enddo  ! ir-cycle
!c
      enddo  ! ik-cycle
!c
      deallocate(rho1int,rho2int,vhart)
      return
!EOC
      end subroutine newpot
!
!cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
!
      subroutine getXCpot(libxc_p,iz,nsp,rr,rhoup,rhodn,vxup,vxdn)
      use mecca_constants
      use xc_mecca, only : xc_alpha,xc_mecca_pointer
      use xc_mecca, only : es_xc_type
      implicit none
      type(xc_mecca_pointer) :: libxc_p
      integer, intent(in) :: iz(:)
      integer, intent(in) :: nsp
      real(8), intent(in) :: rr(:,:)
      real(8), intent(in) :: rhoup(:,:),rhodn(:,:)
      real(8), intent(out) :: vxup(:,:),vxdn(:,:)
!
      real(8), allocatable :: enxc(:)
      integer :: ik,ikeep,komp,nsph

      komp = size(rr,2)
      nsph = size(rhoup,1)
      allocate(enxc(nsph))
      do ik=1,komp
!c
!c exchange-correlation potential
         if ( iz(ik) == 0 ) then
           ikeep = libxc_p%lda_only
           libxc_p%lda_only = es_xc_type
         end if
         if ( nsp == 1 ) then
          call xc_alpha(nsp,nsph,rr(1:nsph,ik),rhoup(1:nsph,ik)         &
     &        ,rhodn(1:nsph,ik),enxc(1:nsph),vxup(1:nsph,ik)            &
     &        ,vxdn(1:nsph,ik),libxc_p=libxc_p)   ! libxc_p,iexch == optional args
!     &        ,iexch=iexch)   ! libxc_p,iexch == optional args
          vxup(nsph+1:,ik) = zero
          vxdn = zero
          if (size(vxdn,1)==size(vxup,1).and.size(vxdn,2)==size(vxup,2))&
     &     vxdn = vxup
         else
          call xc_alpha(nsp,nsph,rr(1:nsph,ik)                          &
     &        ,rhoup(1:nsph,ik),rhodn(1:nsph,ik)                        &
     &        ,enxc(1:nsph),vxup(1:nsph,ik),vxdn(1:nsph,ik)             &
     &        ,libxc_p=libxc_p)   ! libxc_p,iexch == optional args
!     &        ,iexch=iexch)      ! libxc_p,iexch == optional args
          vxup(nsph+1:,ik) = zero
          vxdn(nsph+1:,ik) = zero
         end if
         if ( iz(ik) == 0 ) then
           libxc_p%lda_only = ikeep
         end if
      end do
      deallocate(enxc)
      return
      end subroutine getXCpot
!
!DEBUG
      subroutine debug_print(iz,r,rho,f1,f2)
          use mpi
          implicit none
          integer :: myrank, ierr
          integer, intent(in) :: iz
          real(8), intent(in) :: r(:),rho(:)
          real(8) :: f1(*),f2(*)
          integer :: i
          integer, save :: num=0
          character(6) :: file
          write(file,'(i2,a)') iz,'.dbg'

          call mpi_comm_rank(mpi_comm_world,myrank,ierr)
          if (myrank==0 ) then
           num = num+1
           open(11,file=trim(file),position='APPEND')
           do i=1,size(r)
            write(11,'(2es16.6,2x,2es18.8)') r(i),rho(i),f1(i),f2(i)
           end do

           write(11,'(a,i2)') '#EOF ',num
           close(11)
          end if
          call mpi_barrier(mpi_comm_world,ierr)
          return
      end subroutine debug_print
!DEBUG

      end module potential
!
!cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
!
      subroutine twoZ(z,r,n,vr)
      use universal_const
      use mecca_constants, only : gPointNucleus,Rnucl
      implicit none
      real(8), intent(in) :: z
      real(8), intent(in) :: r(*)
      integer, intent(in) :: n
      real(8), intent(out) :: vr(*)
      real(8) :: tg,Zau
      integer :: i

      real(8), parameter :: sqrt3o2=sqrt(three/two)  ! gaussian distribution
      real(8), parameter :: sqrt3o5=sqrt(three/five) ! homogenious distribution
      real(8), external :: erfc

      if ( z<=zero ) then
       vr(1:n) = zero
      else  ! non-zero potential is calculated only for z>0
       Zau = two*z
       if ( gPointNucleus() ) then
        vr(1:n) = -Zau
       else
        tg =  (sqrt3o2/sqrt3o5)/Rnucl(z)
        if ( n==0 ) then   ! just printing
         write(6,'(a,i3,a,es11.4,a,es11.4)') ' Z =',nint(z),' Rnucl =', &
     &                                               Rnucl(z),' TG=',tg
        else
         do i=1,n
          vr(i) = -Zau*(one-erfc(tg*r(i)))
         end do
        end if
       end if

      end if

      return
      end subroutine twoZ
!
!cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
!
      subroutine getVPmltpl(mecca_state,nsub,ic,rhor2pi4,Qint)
      use mecca_constants
      use mecca_types, only : RunState,IntrstlQ,Species
      use gfncts_interface, only : g_meshv,g_Lmltpl,g_Nmomr
!
      implicit none
      type(RunState), intent(in), target :: mecca_state
      integer, intent(in) :: nsub,ic
      real(8), intent(in) :: rhor2pi4(:,:)
      type(IntrstlQ) :: Qint

      logical :: lVP
      integer :: jend,lmltp,lmx,nmomr
!      integer :: lmltm
      real(8) :: rasa,atcon(1),h
      real(8), allocatable :: rr(:),xr(:)
      type(Species), pointer :: compon

      if ( .not. associated(mecca_state%vp_box) ) return
      if ( .not. allocated(mecca_state%vp_box%rmt) ) return
      if ( .not. allocated(mecca_state%vp_box%ylmPls) ) return
      lVP = mecca_state%vp_box%nF>0                                     &
     &      .and. mecca_state%intfile%mtasa>1                           &
     &      .and. mecca_state%intfile%intgrsch>1
      if ( .not. lVP ) return

      lmltp = g_Lmltpl(1)
      lmx = (lmltp+1)*(lmltp+2)/2
!      lmltm = g_Lmltpl(-1)
!      lmx = lmx + (lmltm+1)*(lmltm+2)/2-1

      if ( allocated(Qint%YlmVPmltpl) ) then
       if ( size(Qint%YlmVPmltpl)<lmx ) then
        deallocate(Qint%YlmVPmltpl)
        allocate(Qint%YlmVPmltpl(1:lmx))
       end if
      else
       allocate(Qint%YlmVPmltpl(1:lmx))
      end if

      nmomr  = g_Nmomr()
      compon => mecca_state%intfile%sublat(nsub)%compon(ic)
      atcon = compon%concentr

      jend = min(size(rhor2pi4,1),compon%v_rho_box%ndchg)
      allocate(xr(jend),rr(jend))
      call g_meshv(compon%v_rho_box,h,xr,rr)

      rasa = mecca_state%rs_box%rws(nsub)
      call VPmltpl(                                                     &
     &             atcon                                                &
     &            ,rhor2pi4,rr,rasa,Qint%SphR,jend                      &
     &            ,lmltp,nmomr                                          &
     &            ,mecca_state%vp_box%ylmPls(0:,1:,nsub)                &
     &            ,Qint%YlmVPmltpl                                      &
     &        ,rho0=mecca_state%intfile%rho0/mecca_state%intfile%nspin  &
     &            )

      deallocate(xr,rr)

      return

      contains

      subroutine VPmltpl(                                               &
     &                    atcon                                         &
     &                    ,rhor2pi4,rr,Rasa,Ris,nend                    &
     &                    ,lmltp,nmomr                                  &
     &                    ,ylmRmom                                      &
     &                    ,YLMmltpl                                     &
     &                    ,lmltp_m                                      &
     &                    ,rho0                                         &
     &                    )
      implicit none
      real(8), intent(in) :: atcon(:)
      real(8), intent(in) :: rhor2pi4(:,:)
      real(8), intent(in) :: rr(:)
      real(8), intent(in) :: Rasa,Ris
      integer, intent(in) :: nend
      integer, intent(in) :: lmltp,nmomr
      complex(8), intent(in) :: ylmRmom(0:,1:)
      complex(8), intent(out) :: YLMmltpl(:)  ! (lmltp+1)*(lmltp+2)/2)
      integer, intent(in), optional :: lmltp_m
      real(8), intent(in), optional :: rho0

      integer :: komp
!c
      real(8) :: rfact
      real(8),    allocatable :: tmprho(:),r(:)
      complex(8), allocatable :: ylmsub(:)
      real(8),    allocatable :: coeff(:) ! 0:nmomr
      real(8),    allocatable :: asamom(:) ! 0:nmomr
!      complex(8), allocatable :: YYmom(:) ! 0:nmomr

      integer, external :: indRmesh

      integer :: nmom
      integer :: lm,l,m                   ! dummy arguments
      integer :: lmltm,lm_p,nmomr_m

      integer :: ik,imt,ndata
      real(8), parameter :: pi4 = dble(4)*pi
      real(8), parameter :: Y0=dble(1)/sqrt(pi4)

      character  sname*10
      parameter (sname='VPmltpl')

      if ( size(YLMmltpl)>0 ) YLMmltpl = czero
      if(lmltp.le.0.or.nmomr.lt.0) return

      lmltm = -1
      nmomr_m = min(nmomr,20)
      if ( present(lmltp_m) ) then
       lmltm = lmltp_m
      end if

      allocate(tmprho(1:nend))
      allocate(r(1:nend))
      allocate(ylmsub(size(ylmRmom,2)))
      allocate(coeff(0:nmomr_m))
      allocate(asamom(0:nmomr_m))
!      allocate(YYmom(0:nmomr))

      do ik=0,nmomr
       asamom(ik) = Y0**2*pi4/(ik+3)
      end do

      komp = size(atcon)

      do ik = 1,komp

!       if ( present(rho0) ) then
!        tmprho(1:nend) = rho0
!        nmom = 0
!       else
        nmom = nmomr_m
        tmprho(1:nend) = rhor2pi4(1:nend,ik)
!        if(nspin.gt.1) tmprho(1:nend) = tmprho(1:nend)                 &
!     &                + rhor2pi4(1:nend,ik)
       tmprho(1:nend) = tmprho(1:nend) / (pi4*rr(1:nend)**2)
       if ( present(rho0) ) then
        tmprho(1:nend) = tmprho(1:nend) - rho0
       end if

       ylmsub = czero

       imt = indRmesh(Ris,nend,rr)          ! r(imt)<Ris
       ndata = nend-imt+1
       r(imt:nend) = rr(imt:nend)/Rasa
       call plnmcoeff(ndata,r(imt:nend),tmprho(imt:nend),1,nmom,coeff)
!DEBUGPRINT
!          write(6,'(a,(9e16.7))') ' coeff=',coeff(0:nmom)
!          do lm=1,size(ylmRmom,2)
!           if ( maxval(abs(ylmRmom(0:nmomr,lm)))>0 ) then
!                 write(6,'('' lm='',i2,2x,8(1x,e14.7,1x,e14.7,1x))')    &
!     &               lm,ylmRmom(0:nmomr,lm)
!           end if
!          end do
!DEBUGPRINT
!       rfact = pi4*Rasa**3/3
       do l=0,lmltp
        rfact = Rasa**(l+3)/(l+3)
        if ( l==0 ) then
         ylmsub(1) = rfact*sum(coeff(0:nmom)*(ylmRmom(0:nmom,1)         &
     &                                         - asamom(0:nmom)))
!DEBUGPRINT
!         write(6,'(a,(2g15.5))') ' ylmRmom(0,1) ',ylmRmom(0,1)
!         write(6,'(a,(2g15.5))') ' asamom(0) ',asamom(0)
!         write(6,'(a,i3,(2g15.5))') ' DEBUG L>0 ',l,ylmsub(1)
!DEBUGPRINT
        else
         do m=0,l
          lm = l*(l+1)/2+m+1
          ylmsub(lm) = rfact*sum(coeff(0:nmom)*ylmRmom(0:nmom,lm))

!DEBUGPRINT
        if ( abs(ylmsub(lm)) > zero ) then
         write(6,'(a,2i3,(2g15.5))') ' DEBUG L>0 ',l,m,ylmsub(lm)
        end if
!DEBUGPRINT

         end do
        end if
       end do
       YLMmltpl(1:lm) = YLMmltpl(1:lm) + atcon(ik)*ylmsub(1:lm)
       lm_p = lm

       ! TODO: remove branch with lmltm>0
       if ( nmom>0 .and. lmltm>0 ) then
        nmom = nmomr_m
        do l=1,lmltm
         call plnmcoeff(ndata,r(imt:nend),tmprho(imt:nend),1,nmom,coeff)
         if  ( l==1 ) then
          rfact = one
         else
          rfact = one/((l-1)*Rasa**(l-1))
         end if
         do m=0,l
          lm = l*(l+1)/2+m+1
          ylmsub(lm) = rfact*sum(coeff(0:nmom)*ylmRmom(0:nmom,lm))
!DEBUGPRINT
          if ( abs(ylmsub(lm))>0 ) then
        write(6,'(a,2i3,(2g15.5))') ' DEBUG L<0 ',l,m,dreal(ylmsub(lm))
         end if
!DEBUGPRINT
         end do
        end do
        YLMmltpl(lm_p+1:lm_p+lm) = YLMmltpl(lm_p+1:lm_p+lm) +           &
     &                          atcon(ik)*ylmsub(1:lm)
       end if

      end do

      deallocate(ylmsub)
      deallocate(tmprho,r)
      deallocate(coeff)
      deallocate(asamom)
!      deallocate(YYmom)
!c
      return
      end subroutine VPmltpl

      end subroutine getVPmltpl

!c**************************************************************
      subroutine Sph2XYZ0(YlmMltpl,XYZmltpl)
      implicit none
      complex(8) :: YlmMltpl
      real(8) :: XYZmltpl

      real(8), parameter :: pi = 4*atan(dble(1))
      real(8), parameter :: sqpi = sqrt(pi)
      real(8), parameter :: sqpi4 = sqrt(4*pi)

      XYZmltpl = sqpi4*dreal(YlmMltpl)
      return
      end subroutine Sph2XYZ0
!c
      subroutine Sph2XYZ1(YlmMltpl,XYZmltpl)
      implicit none
      complex(8) :: YlmMltpl(2)
      real(8) :: XYZmltpl(3)

      real(8) :: anorm

      integer, parameter :: L=1

      real(8), parameter :: sq2 = sqrt(dble(2))
      real(8), parameter :: pi = 4*atan(dble(1))
      real(8), parameter :: sqpi = sqrt(pi)
      real(8), parameter :: sqpi4 = sqrt(4*pi)

      XYZmltpl(1) = sq2*dreal(YlmMltpl(2))
      XYZmltpl(2) = sq2*aimag(YlmMltpl(2))
      XYZmltpl(3) = dreal(YlmMltpl(1))

      anorm = sqpi4/sqrt(2.d0*L+1.d0)
      XYZmltpl(1:3) = XYZmltpl(1:3)*anorm

      return
      end subroutine Sph2XYZ1
!c
      subroutine Sph2XYZ2(YlmMltpl,XYZmltpl)
      implicit none
      integer, parameter :: L=2

      complex(8) :: YlmMltpl(L+1)
      real(8) :: XYZmltpl(3,3)

      real(8) :: y0,ycs(L),ysn(L)
      real(8) :: anorm
      integer m

      real(8), parameter :: sq2 = sqrt(dble(2))
      real(8), parameter :: pi = 4*atan(dble(1))
      real(8), parameter :: sqpi = sqrt(pi)
      real(8), parameter :: sqpi4 = sqrt(4*pi)
      real(8), parameter :: sq3d2 = sqrt(dble(3))/2

      anorm = sqpi4/sqrt(2.d0*L+1.d0)
      y0      = anorm*dreal(YlmMltpl(1))

      anorm = anorm*sq2
      do m=1,L
       ycs(m) = anorm*dreal(YlmMltpl(1+m))
       ysn(m) = anorm*aimag(YlmMltpl(1+m))
      end do

      XYZmltpl(1,1) = -0.5d0*y0 + sq3d2*ycs(2)          ! xx
      XYZmltpl(2,2) = -0.5d0*y0 + sq3d2*ycs(2)          ! yy
      XYZmltpl(3,3) = y0                                ! zy

      XYZmltpl(1,2) = sq3d2*ysn(2)                      ! xy
      XYZmltpl(1,3) = sq3d2*ycs(1)                      ! xz
      XYZmltpl(2,3) = sq3d2*ysn(1)                      ! yz

      XYZmltpl(2,1) = XYZmltpl(1,2)                     ! yx
      XYZmltpl(3,1) = XYZmltpl(1,3)                     ! zx
      XYZmltpl(3,2) = XYZmltpl(2,3)                     ! zy

      return
      end subroutine Sph2XYZ2
!c
      subroutine Sph2XYZ3(YlmMltpl,XYZmltpl)
      implicit none
      integer, parameter :: L=3

      complex(8) :: YlmMltpl(L+1)
      real(8) :: XYZmltpl(3,3,3)

      real(8) :: y0,ycs(L),ysn(L),tmpir(3*3*3)
      real(8) :: anorm

      integer m
      integer i,j,k,ijk

      real(8), parameter :: sq2 = sqrt(dble(2))
      real(8), parameter :: pi = 4*atan(dble(1))
      real(8), parameter :: sqpi = sqrt(pi)
      real(8), parameter :: sqpi4 = sqrt(4*pi)

      real(8), parameter :: sq58 = sqrt(dble(5)/8)
      real(8), parameter :: sq38 = sqrt(dble(3)/8)
      real(8), parameter :: sq124 = sqrt(dble(1)/24)
      real(8), parameter :: sq512 = sqrt(dble(5)/12)
      real(8), parameter :: sq23  = sqrt(dble(2)/3)

      anorm = sqpi4/sqrt(2.d0*L+1.d0)
      y0      = anorm*dreal(YlmMltpl(1))

      anorm = anorm*sq2
      do m=1,L
       ycs(m) = anorm*dreal(YlmMltpl(1+m))
       ysn(m) = anorm*aimag(YlmMltpl(1+m))
      end do

      tmpir(1) = sq58*ycs(3)-sq38*ycs(1)                ! xxx
      tmpir(2) = sq58*ysn(3)-sq124*ysn(1)               ! xxy
      tmpir(4) = sq58*ycs(3)-sq124*ycs(1)               ! xyy
      tmpir(8) = sq58*ysn(3)-sq38*ysn(1)                ! yyy
      tmpir(3) = sq512*ycs(2)-0.5d0*y0                  ! xxz
      tmpir(6) = sq512*ysn(2)                           ! xyz
      tmpir(12)= -sq512*ycs(2)-0.5d0*y0                 ! yyz
      tmpir(9) = sq23*ycs(1)                            ! xzz
      tmpir(18)= sq23*ysn(1)                            ! yzz
      tmpir(27)= y0                                     ! zzz

      do k=1,3
       do j=1,3
        do i=1,3
         ijk = i*j*k
         XYZmltpl(i,j,k)=tmpir(ijk)
        end do
       end do
      end do

      return
      end subroutine Sph2XYZ3
!c
      subroutine Sph2XYZ4(YlmMltpl,XYZmltpl)
      implicit none
      integer, parameter :: L=4

      complex(8) :: YlmMltpl(L+1)
      real(8) :: XYZmltpl(3,3,3,3)

      real(8) :: y0,ycs(L),ysn(L),tmpir(3*3*3*3)
      real(8) :: anorm

      integer m
      integer i,j,k,n,ijkn

      real(8), parameter :: sq2 = sqrt(dble(2))
      real(8), parameter :: pi = 4*atan(dble(1))
      real(8), parameter :: sqpi = sqrt(pi)
      real(8), parameter :: sqpi4 = sqrt(4*pi)

      real(8), parameter :: r38=dble(3)/8,r18=dble(1)/8
      real(8), parameter :: r116=dble(1)/16
      real(8), parameter :: sq35 = sqrt(dble(35))
      real(8), parameter :: sq10 = sqrt(dble(10))
      real(8), parameter :: sq70 = sqrt(dble(70))
      real(8), parameter :: sq35d8 = sqrt(dble(35))/8
      real(8), parameter :: sq5 = sqrt(dble(5))
      real(8), parameter :: sq5d4 = sqrt(dble(5))/4
      real(8), parameter :: r3sq10 = 3*sqrt(dble(10))
      real(8), parameter :: sq58 = sqrt(dble(5)/8)

      anorm = sqpi4/sqrt(2.d0*L+1.d0)
      y0      = anorm*dreal(YlmMltpl(1))

      anorm = anorm*sq2
      do m=1,L
       ycs(m) = anorm*dreal(YlmMltpl(1+m))
       ysn(m) = anorm*aimag(YlmMltpl(1+m))
      end do

      tmpir(1) = r38*y0-sq5d4*ycs(2)+sq35d8*ycs(4)      ! xxxx
      tmpir(2) = r18*(-sq5*ysn(2)+sq35*ysn(4))          ! xxxy
      tmpir(4) = r18*y0-sq35d8*ysn(4)                   ! xxyy
      tmpir(8) =-r18*(sq5*ysn(2)+sq35*ysn(4))           ! xyyy
      tmpir(16)= r38*y0+sq5d4*ycs(2)+sq35d8*ycs(4)      ! yyyy
      tmpir(3) = r116*(-r3sq10*ycs(1)+sq70*ycs(3))      ! xxxz
      tmpir(6) = r116*(-sq10*ysn(1)+sq70*ysn(3))        ! xxyz
      tmpir(12)=-r116*(sq10*ycs(1)+sq70*ycs(3))         ! xyyz
      tmpir(24)=-r116*(r3sq10*ysn(1)+sq70*ysn(3))       ! yyyz
      tmpir(9) =-0.5d0*y0+sq5d4*ycs(2)                  ! xxzz
      tmpir(18)= sq5d4*ysn(2)                           ! xyzz
      tmpir(36)=-0.5d0*y0-sq5d4*ycs(2)                  ! yyzz
      tmpir(27)= sq58*ycs(1)                            ! xzzz
      tmpir(54)= sq58*ysn(1)                            ! yzzz
      tmpir(81)= y0                                     ! zzzz

      do n=1,3
       do k=1,3
        do j=1,3
         do i=1,3
          ijkn = i*j*k*n
          XYZmltpl(i,j,k,n)=tmpir(ijkn)
         end do
        end do
       end do
      end do

      return
      end subroutine Sph2XYZ4
!c**************************************************************
