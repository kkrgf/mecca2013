!c
!BOP
!!ROUTINE: wrtmtx
!!INTERFACE:
      subroutine wrtmtx(x,n,istop)
!!DESCRIPTION:
! writes out the non-zero elements of a {\tt n}x{\tt n} complex matrix
!

!!DO_NOT_PRINT
      implicit none
!!DO_NOT_PRINT

!!ARGUMENTS:
      integer, intent(in) :: n
      complex(8), intent(in) :: x(:,:) 
      character(10), intent(in), optional :: istop
!!REMARKS:
! "non-zero" means that abs.value > tol=10^(-16)
!EOP
!
!BOC
      integer i,j,n1,n2
      character(10), parameter :: sname='wrtmtx'
      real(8), parameter :: tol=1.d-16
!c
      if ( n>0 ) then
       write(6,*)'  kkrsz',n
       n1 = min(n,size(x,1)) 
       n2 = min(n,size(x,2)) 
      else
       n1 = size(x,1) 
       n2 = size(x,2) 
      end if    
      do j=1,n2
       do i=1,n1
         if(abs(x(i,j)).gt.tol) then
          write(6,'(2x,2i4,2d16.8)') i,j,x(i,j)
!          if (abs(x(i,j)).gt.1.d10) then
!            call fstop(sname//' BAD NUMBER')
!          end if
         else    
          if ( x(i,j) == (0.d0,0.d0) ) then
          else if ( abs(x(i,j))<tiny(tol) ) then ! to print NaN
            write(6,'(2x,2i4,2d16.8)') i,j,x(i,j)
          endif
         endif
       enddo
      enddo
!c     =============================================================
      if (present(istop) ) then
        if(istop.eq.sname) call fstop(sname)
      endif
      return
!EOC
      end subroutine wrtmtx
!
!BOP
!!ROUTINE: wrtmtx_r
!!INTERFACE:
      subroutine wrtmtx_r(x,n,istop)
!!DESCRIPTION:
! writes out the non-zero elements of a {\tt n}x{\tt n} real matrix
!

!!DO_NOT_PRINT
      implicit none
!!DO_NOT_PRINT

!!ARGUMENTS:
      integer, intent(in) :: n
      real(8), intent(in) :: x(:,:)
      character(10), intent(in), optional :: istop
!!REMARKS:
! "non-zero" means that abs.value > tol=10^(-16)
!EOP
!
!BOC
!c     ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
      integer i,j,n1,n2
      character(10), parameter :: sname='wrtmtx_r'
      real(8), parameter :: tol=1.d-16
!c
!c     **************************************************************
!c     writes out the non-zero elements of a NxN real matrix......
!c     **************************************************************
!c
      if ( n>0 ) then
       n1 = min(n,size(x,1)) 
       n2 = min(n,size(x,2)) 
       write(6,*)'  n1',n1,' n2=',n2
      else
       n1 = size(x,1) 
       n2 = size(x,2) 
      end if    
      do j=1,n2
       do i=1,n1
         if(abs(x(i,j)).gt.tol) then
          write(6,'(2x,2i4,2d16.8)') i,j,x(i,j)
!          if (abs(x(i,j)).gt.1.d10) then
!            call fstop(sname//' BAD NUMBER')
!          end if
         else    
          if ( x(i,j) == (0.d0,0.d0) ) then
          else if ( abs(x(i,j))<tiny(tol) ) then ! to print NaN
            write(6,'(2x,2i4,2d16.8)') i,j,x(i,j)
          endif
         endif
       enddo
      enddo
!c     =============================================================
      if (present(istop) ) then
        if(istop.eq.sname) call fstop(sname)
      endif
      return
!EOC
      end subroutine wrtmtx_r
