!BOP
!!MODULE: vp_str
!!INTERFACE:
       module vp_str
!!DESCRIPTION:
!  to set up basic run properties related to spherical basis for
!  given 3D periodic structure (main module subroutine is setOptSphBasis)
!
!!USES:
      use mecca_constants
      use mecca_types
      use atom, only : atom_rdata
      use atom, only : get_atom,intqud
      use mecca_interface
      use gfncts_interface, only : g_Lmltpl,g_Nmomr
      use isoparintgr_interface
      use mpi
!
!!REVISION HISTORY:
! Initial Version - A.S. - 2013
!!REMARKS:
! Bernal library (not a part of this module) is used
! for Voronoi Tessellation partitioning
!EOP
!
!BOC
      implicit none

      real(8), parameter :: pi2 = 2*pi
      real(8), parameter :: pi4 = 4*pi
      real(8), parameter :: third=1.d0/3.d0
      integer, parameter :: max_shell_atoms = ipvtx
      integer, parameter :: re_scale = 0
      type (VP_data), pointer :: bernal_vp_jcbn => null()
      real(8), external :: ylag
      integer, parameter :: ch_dbg=71

      integer, parameter :: task0 = 0
      integer :: ierr, myrank

      type :: PolyFaces
       real(8), pointer :: facevtx(:,:,:)=>null()  ! (1:nnvp,1:3,1:ipfvtx) ! (face #,coordinates, face vertices #)
       integer, pointer :: nfvtx(:) =>null()       !              (1:nnvp) ! number of vertices for each face,
       integer :: nnvp=0                           ! number of polyhedron faces (zero or a number >=3)
      end type PolyFaces

!EOC
      CONTAINS

!BOP
!!IROUTINE: setOptSphBasis
!!INTERFACE:
      subroutine setOptSphBasis(mecca_state)
      implicit none
!!DESCRIPTION:
! this subroutine calculates IS-, ASA- and CS-radii and (if needed)
! constructs optimal spherical basis taking into account charge distribution and
! based on Bernal approach for Voronoi polyhedra tessellation;
! VP-jacobians for 3D interstitial isoparameteric integration and
! VP-corrections for some integrals are also calculated.
!
! actual arguments are several {\sc mecca} structures in {\em mecca\_state}:
! {\bv
!  type (IniFile),  pointer :: inFile  => mecca_state%intfile  ! intent(in)
!  type (RS_Str),   pointer :: rs_box  => mecca_state%rs_box   ! intent(inout)
!  type (VP_data), pointer :: vp_box=> mecca_state%vp_box ! intent(inout)
!  \ev}
!
!!ARGUMENTS:
      type (RunState), target, intent(inout) :: mecca_state
!!BUGS:
!  vp_box%ngauss (max. order of integration polynomial) is limited by 20
!  (it should be not less than MNqp in mecca_constants module)
!!REVISION HISTORY:
! Initial Version - A.S. - 2013
!EOP
!
!BOC
      type (IniFile),  pointer :: inFile    ! intent(in)
      type (RS_Str),   pointer :: rs_box    ! intent(inout)
      type (VP_data), pointer :: vp_box  ! intent(inout)

      integer new_nF
      real(8), allocatable :: buffer(:,:,:,:,:,:)
      integer   komp( mecca_state%intfile%nsubl)
      real(8), allocatable :: ztot(:,:)
      real(8), allocatable :: atcon(:,:)
      real(8), allocatable :: rmt_ovlp(:,:)
      integer :: mtasa,iadjsph,nsub,ndcomp,lmx
      real(8) :: lattice(3)
      logical :: opnd
      integer :: st_env,intgrsch

      inFile => mecca_state%intfile
      rs_box => mecca_state%rs_box
      vp_box => mecca_state%vp_box

      ndcomp = maxval(inFile%sublat(1:inFile%nsubl)%ncomp)
      allocate(ztot(1:ndcomp,mecca_state%intfile%nsubl))
      allocate(atcon(1:ndcomp,mecca_state%intfile%nsubl))
      allocate(rmt_ovlp(1:ndcomp,mecca_state%intfile%nsubl))

      lattice(1) = inFile%alat
      lattice(2) = inFile%ba
      lattice(3) = inFile%ca
      vp_box%nF = 0
      komp(1:inFile%nsubl) = inFile%sublat(1:inFile%nsubl)%ncomp
      do nsub=1,inFile%nsubl
        ztot(1:komp(nsub),nsub) =                                       &
     &     inFile%sublat(nsub)%compon(1:komp(nsub))%zID
        atcon(1:komp(nsub),nsub) =                                      &
     &     inFile%sublat(nsub)%compon(1:komp(nsub))%concentr
      end do
      mtasa   = inFile%mtasa
      if ( .not. allocated( vp_box%rmt ) ) then
         allocate( vp_box%rmt(1:inFile%nsubl) )
      end if
      if ( .not. allocated( vp_box%alphpot ) ) then
         allocate( vp_box%alphpot(1:inFile%nsubl) )
         vp_box%alphpot = one
      end if
      if ( .not. allocated( vp_box%alphen ) ) then
         allocate( vp_box%alphen(1:inFile%nsubl) )
         vp_box%alphen = one
      end if
      if ( .not. allocated( vp_box%fcount ) ) then
         allocate( vp_box%fcount(1:inFile%nsubl) )
      end if

!DEBUG
      intgrsch = inFile%intgrsch
      call gEnvVar('ENV_VPRADII',.true.,st_env)
      if ( st_env==1 ) then
        intgrsch = 1000
        write(6,*) ' vp_box%ngauss=',vp_box%ngauss
      end if
!DEBUG

      if ( inFile%mtasa>1 .or. intgrsch>1 ) then
        if ( vp_box%ngauss == 0 ) then
          if ( allocated(vp_box%weight) ) deallocate(vp_box%weight)
          if ( allocated(vp_box%vj) )     deallocate(vp_box%vj)
          if ( allocated(vp_box%rmag) )   deallocate(vp_box%rmag)
        end if
        if ( vp_box%nF == 0 ) then
          if ( allocated(vp_box%vj) )     deallocate(vp_box%vj)
          if ( allocated(vp_box%rmag) )   deallocate(vp_box%rmag)
        end if
        vp_box%nF = MNF
        lmx = max(inFile%lmax,g_Lmltpl(1))
        vp_box%ngauss = min(20,12+lmx/2+mod(lmx,2))   ! is MNqp better?
         allocate( vp_box%vj(1:vp_box%ngauss,                           &
     &                         1:vp_box%ngauss,                         &
     &                         1:vp_box%ngauss,                         &
     &                         1:MNF,1:inFile%nsubl) )
         allocate( vp_box%rmag(0:3,                                     &
     &                         1:vp_box%ngauss,                         &
     &                         1:vp_box%ngauss,                         &
     &                         1:vp_box%ngauss,                         &
     &                         1:MNF,1:inFile%nsubl) )
         allocate( vp_box%weight(1:vp_box%ngauss) )
      end if

      bernal_vp_jcbn => vp_box
      iadjsph = 1
      if ( maxval(inFile%sublat(1:inFile%nsubl)%rIS) > zero ) then
        rs_box%rmt(1:inFile%nsubl) = inFile%sublat(1:inFile%nsubl)%rIS  &
     &                                    *inFile%alat
        if ( mtasa>1 ) then
         if ( inFile%intgrsch == 1 ) then
          iadjsph = -1
         else
          iadjsph = 1
         end if
        else
          iadjsph = 0
        end if
      else if ( maxval(inFile%sublat(1:inFile%nsubl)%rIS) < zero ) then
        rs_box%rmt(1:inFile%nsubl) = zero
        iadjsph = 0
      else
        rs_box%rmt(1:inFile%nsubl) = zero
        if ( inFile%intgrsch > 0 ) then
         if ( mtasa>1 ) then
          iadjsph = inFile%intgrsch
         else
          iadjsph = 1
         end if
        end if
      end if
      call gEnvVar('ENV_NO_OPTIMALSPH',.false.,st_env)
      if ( st_env==1 ) then
       iadjsph = 0
      else if ( st_env==0 ) then
       iadjsph = 1
      end if

      if ( inFile%iprint >= 0 ) then
        open(ch_dbg,file='#dbg.'//trim(inFile%genName),status='UNKNOWN')
      end if
      call optimal_vp(inFile%nsubl,lattice,rs_box,                      &
     &                 komp,ztot,atcon,                                 &
     &                 mtasa,iadjsph,                                   &
     &                 rmt_ovlp,                                        &
     &       inFile%iprint,inFile%istop)
        inFile%sublat(1:inFile%nsubl)%rIS = rs_box%rmt(1:inFile%nsubl)  &
     &                                      / inFile%alat
      do nsub=1,inFile%nsubl
        inFile%sublat(nsub)%compon(1:komp(nsub))%rSph =                 &
     &    rmt_ovlp(1:komp(nsub),nsub)
!     &                                                rs_box%rmt(nsub)
      end do

!      if ( bernal_vp_jcbn%nF>0 ) then
!        inFile%sublat(1:inFile%nsubl)%rIS = rs_box%rmt(1:inFile%nsubl)
!      else
!        inFile%sublat(1:inFile%nsubl)%rIS = rs_box%Rnn(1:inFile%nsubl)/2
!      end if
      inquire(unit=ch_dbg,opened=opnd )
      if ( opnd ) close(ch_dbg)

!DEBUG
      if ( intgrsch == 1000 ) then
        vp_box%ngauss = 0
        vp_box%nF = 0
        if ( allocated(vp_box%weight) ) deallocate(vp_box%weight)
        if ( allocated(vp_box%vj) )     deallocate(vp_box%vj)
        if ( allocated(vp_box%rmag) )   deallocate(vp_box%rmag)
      end if
!DEBUG

      if ( inFile%intgrsch <=1 ) then
       if( allocated(vp_box%alphen)  ) vp_box%alphen=one
       if( allocated(vp_box%alphpot) ) vp_box%alphpot=one
      end if
      if ( allocated(vp_box%vj) ) then
       new_nF = maxval(vp_box%fcount(1:inFile%nsubl))
       if ( new_nF < 0.9d0*vp_box%nF ) then
         allocate( buffer(1:4,                                          &
     &                    1:vp_box%ngauss,                              &
     &                    1:vp_box%ngauss,                              &
     &                    1:vp_box%ngauss,                              &
     &                    1:new_nF,1:inFile%nsubl) )
         buffer(1,:,:,:,:,:) =                                          &
     &       vp_box%vj(1:vp_box%ngauss,                                 &
     &                 1:vp_box%ngauss,                                 &
     &                 1:vp_box%ngauss,                                 &
     &                 1:new_nF,1:inFile%nsubl)
         deallocate(vp_box%vj)
         allocate( vp_box%vj(1:vp_box%ngauss,                           &
     &                         1:vp_box%ngauss,                         &
     &                         1:vp_box%ngauss,                         &
     &                         1:new_nF,1:inFile%nsubl) )
         vp_box%vj = buffer(1,:,:,:,:,:)
         if ( allocated(vp_box%rmag) ) then
         buffer =                                                       &
     &     vp_box%rmag(0:3,                                             &
     &                 1:vp_box%ngauss,                                 &
     &                 1:vp_box%ngauss,                                 &
     &                 1:vp_box%ngauss,                                 &
     &                 1:new_nF,1:inFile%nsubl)
         deallocate(vp_box%rmag)
         allocate( vp_box%rmag(0:3,                                     &
     &                         1:vp_box%ngauss,                         &
     &                         1:vp_box%ngauss,                         &
     &                         1:vp_box%ngauss,                         &
     &                         1:new_nF,1:inFile%nsubl) )
         vp_box%rmag = buffer
         end if
         deallocate(buffer)
         vp_box%nF = new_nF
       end if
      end if
      if ( allocated(ztot) ) deallocate(ztot)
      if ( allocated(atcon) ) deallocate(atcon)
      if ( allocated(rmt_ovlp) ) deallocate(rmt_ovlp)
      return
!EOC
      end subroutine setOptSphBasis

!c     cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
!BOP
!!IROUTINE: optimal_vp
!!INTERFACE:
      subroutine optimal_vp(nbasis,lattice,rs_box,                      &
     &                 komp,ztot,atcon,                                 &
     &                 mtasa,iadjsph_ovlp,                              &
     &                 rmt_ovlp,                                        &
     &                 iprint,istop)
!c     ==================================================================
!!DESCRIPTION:
! all module calculations are here
!
!!REVISION HISTORY:
! Initial Version - mecca_ames - 2010
! Revised - A.S. - 2013
!EOP
!
!BOC
      implicit real*8 (a-h,o-z)
!c     ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
      integer, intent(in) :: nbasis
      real(8), intent(in) ::    lattice(3)  !  for isoparametric Integ. (libVP)
      type (RS_Str), pointer :: rs_box    ! intent(inout)
      integer, intent(in) ::   komp(nbasis)
      real(8), intent(in) :: ztot(:,:)
      real(8), intent(in) :: atcon(:,:)
      integer, intent(in) :: mtasa,iadjsph_ovlp
      real(8), intent(out) :: rmt_ovlp(size(atcon,1),nbasis)
      integer :: iprint
      character(10), intent(in) :: istop
!
      integer, pointer :: natom => null()
      integer, pointer :: itype(:) => null()    ! itype(natom)
      real(8), pointer :: rslatt(:,:) => null() ! rslatt(3,3), in units of Alat/(2*pi)
      real(8), pointer :: pos(:,:) => null()    ! pos(3,natom)
      real(8), pointer :: volume => null()
      integer, pointer :: numbsub(:) => null()  ! numbsub(nbasis)
      real(8), pointer :: rmt(:) => null()      ! rmt(nbasis)
      real(8), pointer :: rws(:) =>  null()     ! rws(nbasis)
      real(8), pointer :: r_circ(:) => null()   ! rcs(nbasis)
      real(8), pointer :: Rnn(:) => null()      ! Rnn(nbasis)

      integer, pointer :: fcount(:)
      real(8), pointer :: weight(:)
      real(8), pointer :: rmag(:,:,:,:,:,:)
      real(8), pointer :: vj(:,:,:,:,:)
!c     ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
      character(*), parameter :: sname='optimal_vp'
!c     ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!c
      integer, parameter :: nspinp=1
!c
!      real*8    rmt(ipcomp,nbasis)
!      real*8    rws(ipcomp,nbasis)
!      real*8    r_circ(ipcomp,nbasis)
      real(8) :: Rn0(nbasis)
      real*8    rASA(size(atcon,1),nbasis)
!      real*8    X(ipcomp,nbasis)
      real*8    transV(3,3)
      real*8    alat,alatmin

!C====================================================================
!C     Reqd. for r-grids [other than log-grid(xr,rr,h)] used for
!C           the initial construction of atomic V and rho
!C                               +
!C             for overlapping atomic charge density
!C====================================================================
      integer  i,iat,ishell,ir,ir1,ityp,ntyp,ipa,ima
      integer  nk,nsub,nki,nkn,imin,kmx,nmx,ns
      real(8) :: fac,a,pref,h0,rpa,rma,ztmp
      real(8) :: contrb,contrb1,contrb2,hh

      integer, parameter :: icdim=50,nn=3
      integer :: ichg(0:icdim,1:size(atcon,1),1:nbasis)
      integer :: imax(size(atcon,1),nbasis)

!      real*8    rho_ovlp_rad(nmx)
      real(8), allocatable ::    rhohsk0(:,:,:)   !  (nmx,size(atcon,1),nbasis)
      real(8), allocatable ::    rhokeep(:)       !  (nmx)
      real(8), allocatable ::    rhoint(:,:,:)    !  (nmx,size(atcon,1),nbasis)
      real(8) :: rchg(0:icdim,1:size(atcon,1),1:nbasis)
      real(8) :: rmax(size(atcon,1),nbasis),rmt_av(nbasis)
!C                                   !rho on herman skilman grid
      real*8    r_min,rmt_min,rho_min,rmin
      integer :: nb,nb_max
!      real(8), allocatable :: rmt_spr(:),w_spr(:)
!      real(8), allocatable :: rnb(:)
!      integer, allocatable :: nnb(:)
      logical, allocatable :: nonES(:)
      integer, allocatable :: ishell_nn(:)
      integer :: i_nn,iat_maxshell
      real*8    f1,f2,rmin0,rmax0,vp_volm,rws0,xv,tmparr(nbasis)
      real(8) :: vlm_mt,vlm_es
!C==============================================================
!C     Reqd. for shell information of each atoms in the unit cell
!C==============================================================
      real(8), parameter :: size_min = 0.01d0   ! relative units
      integer :: ityp2at(nbasis)
      integer, parameter :: nn_shell=6
      integer, pointer :: ntypshl(:,:)
      integer, pointer :: natshl(:,:)
      real(8), pointer :: radshl(:,:)
      integer :: ix,i1,i2,i3,j,kz,nshell,natovlp,ish_min,n_ES(2)
      integer :: Lmltpl,Lmltmn,lmx,nnmx
      real(8) :: endpoints(2),rsize_min
      real(8), parameter :: c_es=0.96d0
      logical :: iVP
      real(8), external :: gZval
      real(8), pointer :: r_hsk(:,:,:)=>null()
      real(8), pointer :: rho_hsk(:,:,:)=>null()
      real(8), pointer :: p_r(:)=>null()
      type( atom_rdata ) :: atomic
!DEBUG
      integer, parameter:: nscr_unit=15
      real(8) :: tmp_ovlp(size(atcon,1),nbasis)
      real(8), allocatable :: basisV(:,:)
      integer, allocatable :: numshell(:)
      logical :: opnd,opnd_scr
!
      inquire(unit=ch_dbg,opened=opnd )

      natom => rs_box%natom
      itype => rs_box%itype
      rslatt => rs_box%rslatt
      pos => rs_box%xyzr
      volume => rs_box%volume
      numbsub => rs_box%numbsubl
      rmt => rs_box%rmt
      rws => rs_box%rws
      r_circ => rs_box%rcs
      Rnn => rs_box%Rnn

      iVP = bernal_vp_jcbn%nF>0
      fcount => bernal_vp_jcbn%fcount
      weight => bernal_vp_jcbn%weight
      rmag => bernal_vp_jcbn%rmag
      vj => bernal_vp_jcbn%vj

      if ( natom .ne. sum(numbsub(1:nbasis)) ) then
        write(6,*) ' nbasis=',nbasis,' natom=',natom
        write(6,*) ' numbsub=',numbsub(1:nbasis)
        write(6,*) ' sum(numbsub).ne.natom?!'
        call p_fstop(sname//': inconsistent input data')
      end if
      allocate(basisV(3,natom),numshell(natom))
!      nspinp = 1
      alat=lattice(1)
      alatmin = alat*min(one,lattice(2),lattice(3))
      if ( allocated(nonES) ) deallocate(nonES)
      allocate(nonES(nbasis))
      n_ES = 0
      do ityp=1,nbasis
       if ( sum(ztot(1:komp(ityp),ityp))>epsilon(zero) ) then
        nonES(ityp)=.true.
       else
        n_ES(1)=n_ES(1)+1
        n_ES(2)=n_ES(2)+numbsub(ityp)
        nonES(ityp)=.false.
       end if
      end do

!C ***************************************************************
!C      Generate the neighbouring shell data for each atom in the
!C                           unit cell
!C ***************************************************************
!DELETE       nshell = min(ipnnmax,nn_shell*nbasis)
       nshell = ipnnmax
       allocate(ntypshl(nshell,natom))
       allocate(natshl(nshell,natom))
       allocate(radshl(nshell,natom))
       call neigh_shell(itype,natom,komp,n_ES,rslatt/pi2,               &
     &                pos/alat,size(ntypshl,1),ntypshl,natshl,radshl,   &
     &                numshell,iprint)
       do nsub=1,nbasis
        do iat=1,natom
          if ( itype(iat) == nsub ) then
            ityp2at(nsub) = iat
            exit
          end if
        end do
       end do
       radshl = radshl*alat
!!!!!       Rnn(1:nbasis) = radshl(2,ityp2at(1:nbasis))   ! Rnn with empty spheres
! Rnn without empty spheres
       do nsub=1,nbasis
        Rnn(nsub) = zero
        if( nonES(nsub) ) then
         iat = ityp2at(nsub)
         do ishell=2,numshell(iat)
          ntyp=ntypshl(ishell,iat)
          if ( nonES(ntyp) ) then
            Rnn(nsub) = radshl(ishell,iat)
            exit
          end if
         end do
        end if
       end do
       do nsub=1,nbasis
        Rn0(nsub) = alatmin
        if( nonES(nsub) ) then
         iat = ityp2at(nsub)
         do ishell=2,numshell(iat)
          ntyp=ntypshl(ishell,iat)
          if ( ntyp==nsub ) then
            Rn0(nsub) = min(Rn0(nsub),radshl(ishell,iat))
            exit
          end if
         end do
        end if
       end do
!C     ===============================================================
!C     Writing Shell info for each ineq.atom in the unit cell (Optional WRITE)
!C     ===============================================================
       if ( iprint >= 0 ) then
        write(6,*)'================================================='
        do nsub=1,nbasis
         i1 = ityp2at(nsub)
         write(6,*) 'Shell info for site = ',nsub,' (atom ',i1,')'
         do i2=1,numshell(i1)
            if ( ntypshl(i2,i1)<=0 .or. natshl(i2,i1)<=0 ) exit
            write(6,122)i1,i2,ntypshl(i2,i1),natshl(i2,i1),             &
     &                   radshl(i2,i1),numshell(i1)
122         format(2I5,1x,2I6,4x,F14.6,2x,I5)
            write(6,*)
         enddo
        enddo
        write(6,*)'================================================='
       end if

      rmt_av = rmt
      IF ( iadjsph_ovlp==0 ) THEN    ! rmt is defined from input file or Rnn
       if ( maxval(rmt(1:nbasis)) == zero ) then
        do nsub=1,nbasis
         if( nonES(nsub) ) then
          rmt(nsub) = 0.5d0*Rnn(nsub)
         end if
        end do
       else ! rmt is defined from input file
        if ( iprint >=0 ) write(6,*) ' Input inscribed sphere radius:'
       end if
       do nsub=1,nbasis
        if ( iprint >=0 .or. rmt(nsub)<=zero )                          &
     &   write(6,'(''  nsub='',i4,''  Ris='',g14.4)') nsub,rmt(nsub)
        rmt_ovlp(1:komp(nsub),nsub) = rmt(nsub)
        rmt_av(nsub) = rmt(nsub)
       end do
       if ( minval(rmt_av(1:nbasis))<=zero ) then
        call p_fstop(sname//                                            &
     &                      ': undefined input Ris')
       end if
      ELSE
!c ***************************************************************
!c     Generate charge density for various atoms in the unit cell
!c     overlapping atomic charge densities to give an approximate
!c     bulk charge density.
!c ***************************************************************

       nmx = 0
       do nsub=1,nbasis
        if ( rmt(nsub) > zero ) cycle
        do nk=1,komp(nsub)
          kz = nint(ztot(nk,nsub))
          if ( kz > 0 ) then
            call get_atom(kz,atomic,-1)
            nmx = max(nmx,atomic%mx)
          end if
        end do
       end do

       allocate(rho_hsk(nmx,maxval(komp(1:nbasis)),nbasis))
       allocate(r_hsk(nmx,maxval(komp(1:nbasis)),nbasis))

       imax = 0
!c     ---------------------------------------------------------------

       ns = 1
       do nsub=1,nbasis
        if ( rmt(nsub) > zero ) cycle
        do nk=1,komp(nsub)
         if( ztot(nk,nsub).gt.zero )then
           if ( nk==1 ) then
            ztmp = ztot(1,nsub)
           else
            ztmp = ztot(nk-1,nsub)
           end if
           if ( nk==1 .or. ztot(nk,nsub).ne.ztmp ) then
            kz = nint(ztot(nk,nsub))
            call get_atom(kz,atomic,-1)
            kmx = atomic%mx
            do ir=kmx,2,-1
             if(atomic%rho(ir).gt.1.0d-06)then
              rmax(nk,nsub) = atomic%r(ir)
              imax(nk,nsub) = ir
              exit
             endif
            enddo
!            r_hsk(1:kmx,nk,nsub) = atomic%r(1:kmx)
!            rho_hsk(1:kmx,nk,nsub) = atomic%rho(1:kmx)
            ir = min(nmx,size(atomic%r))
            r_hsk(1:ir,nk,nsub) = atomic%r(1:ir)
            rho_hsk(1:ir,nk,nsub) = atomic%rho(1:ir)
            if ( ir < nmx )  then
             h0 = r_hsk(ir,nk,nsub)-r_hsk(ir-1,nk,nsub)
             forall (i=ir+1:nmx)                                        &
     &             r_hsk(i,nk,nsub) = r_hsk(ir,nk,nsub)+(i-ir)*h0
             rho_hsk(ir+1:,nk,nsub) = zero
            end if
           else
            rmax(nk,nsub) = rmax(nk-1,nsub)
            imax(nk,nsub) = imax(nk-1,nsub)
            r_hsk(:,nk,nsub) = r_hsk(:,nk-1,nsub)
            rho_hsk(:,nk,nsub) = rho_hsk(:,nk-1,nsub)
           end if
         else
           iat = ityp2at(nsub)
           if ( nk==1 ) then
            rmax(nk,nsub) = radshl(2,iat) ! ES NN
            do ishell=2,numshell(iat)
             ityp = ntypshl(ishell,iat)
             if ( nonES(ityp) ) then
              rmax(nk,nsub) = radshl(ishell,iat) ! non-ES NN
              exit
             end if
            end do
            h0 = rmax(nk,nsub)/(nmx-1)
            do ir=1,nmx
             r_hsk(ir,nk,nsub) = h0*(ir-1)
            end do
            imax(nk,nsub) = nmx
           else
            rmax(nk,nsub) = rmax(nk-1,nsub)
            imax(nk,nsub) = imax(nk-1,nsub)
            r_hsk(:,nk,nsub) = r_hsk(:,nk-1,nsub)
           end if
           rho_hsk(:,nk,nsub) = zero
         endif
        enddo
       enddo
!C********************************************************************

!C *******   debug print statement ***********************************
!C       do ir=2,nhs
!C         write(ch_dbg,103)((r_hsk(ir,nk,nsub),rho_hsk(ir,1,nk,nsub)/
!C     <                  (4.0d0*pi*r_hsk(ir,nk,nsub)**2.0d0),
!C     <                   nk=1,komp(nsub)),nsub=1,nbasis)
!C       enddo
!C103    format(1x,8F15.9)

       allocate(rhohsk0(nmx,size(atcon,1),nbasis))
       allocate(rhokeep(nmx))
       allocate(rhoint(nmx,size(atcon,1),nbasis))
       do nsub=1,nbasis
        if ( rmt(nsub) > zero ) cycle
        do nk=1,komp(nsub)
         if(ztot(nk,nsub).gt.zero)then
          kmx = min(imax(nk,nsub)+3,nmx)
           rhohsk0(1,nk,nsub)=0.0d0
           do ir=2,kmx
              rhohsk0(ir,nk,nsub)=rho_hsk(ir,nk,nsub)/                  &
     &                                          (pi4*r_hsk(ir,nk,nsub))  ! r*rho(r)
           enddo
!c-- integral of r*rho(r)*dr for each atomic species and spin
         call intqud(rhohsk0(1:kmx,nk,nsub),rhoint(1:kmx,nk,nsub),      &
     &                                r_hsk(1:kmx,nk,nsub),kmx)
           rhoint(kmx+1:,nk,nsub) = 0.d0
           do ir=2,kmx
              rhohsk0(ir,nk,nsub)=rhohsk0(ir,nk,nsub)/                  &
     &                                                 r_hsk(ir,nk,nsub)  ! rho(r)
           enddo
         else
           rhohsk0(1:nmx,nk,nsub)= zero
           rhoint(1:nmx,nk,nsub) = zero
         endif
        enddo
       enddo
       if ( associated(rho_hsk) ) deallocate(rho_hsk) ; nullify(rho_hsk)
!c*********************************************************************
!c       Overlapp the site charge densities to get an approximate
!c                       bulk charge density
!c       J.M. Maclaren's idea of overlapping at. charge densities has
!c                           been used
!c*********************************************************************
       do ityp=1,nbasis
         iat = ityp2at(ityp)
         call rho_latt_site(ityp,nmx,komp,imax,rmax,atcon,ztot,r_hsk,   &
     &        numshell(iat),radshl(:,iat),ntypshl(:,iat),natshl(:,iat), &
     &                                   rhoint,rhohsk0)
       end do

       allocate(p_r(size(rhohsk0,1)))
       do ityp=1,nbasis
        if ( rmt(ityp) > zero ) then
         rmt_ovlp(1:komp(ityp),ityp) = rmt(ityp)
         cycle
        else
         rmt_ovlp(1:komp(ityp),ityp) = zero
        end if
        rhokeep(:) = zero
        nk = 1
        ztmp = atcon(1,ityp)*ztot(1,ityp)
        if ( komp(ityp)>1 ) then
          rmax0 = rmax(1,ityp)
          do nki=2,komp(ityp)
           ztmp = ztmp+atcon(nki,ityp)*ztot(nki,ityp)
           if ( rmax0<rmax(nki,ityp) ) then
             rmax0 = rmax(nki,ityp)
             nk = nki
           end if
          end do
          kmx = min(imax(nk,ityp),nmx)
          rhokeep(1:kmx) = zero
        end if
        do nki=1,komp(ityp)
         if (ztot(nki,ityp).gt.zero) then
!
           p_r(1:imax(nki,ityp)) = rhohsk0(1:imax(nki,ityp),nki,ityp)   &
     &                      + rhohsk0(1:imax(nki,ityp),nki,ityp)
           if ( nki == nk ) then
            rhokeep(1:imax(nki,ityp)) = rhokeep(1:imax(nki,ityp)) +     &
     &           atcon(nki,ityp)*p_r(1:imax(nki,ityp))
           else  ! re-interpolation from nki- to nk-grid
            do ir=2,imax(nk,ityp)
             rpa = r_hsk(ir,nk,ityp)
             if ( rpa>rmax(nki,ityp) ) exit
             f1 = ylag(rpa,r_hsk(1:,nki,ityp),                          &
     &                                 p_r(1:),0,3,imax(nki,ityp),ix)
             rhokeep(ir) = rhokeep(ir) + atcon(nki,ityp)*f1
            end do
           end if
           call findRz(imax(nki,ityp),r_hsk(:,nki,ityp),p_r(:),Rnn(ityp)&
     &                ,2,ztot(nki,ityp),rmt_ovlp(nki,ityp),-1)
!DEBUGPRINT
!           rpa = rmt_ovlp(nki,ityp)
!           call findRsp(imax(nki,ityp),r_hsk(:,nki,ityp),p_r(:),rpa,    &
!     &                            ztot(nki,ityp),rmt_ovlp(nki,ityp))
!         write(6,'(a,2i3,2f25.12)') ' ... ITYP,NKI,RMIN',ityp,nki,      &
!     &                              rpa,rmt_ovlp(nki,ityp)
!DEBUGPRINT
         end if
!         rmt_ovlp(nki,ityp) = min(rmt_ovlp(nki,ityp),0.5d0*Rn0(ityp))
        enddo                  ! End of nki loop
        if ( komp(ityp) > 1 .and. nonES(ityp) ) then
         call findRz(kmx,r_hsk(:,nk,ityp),rhokeep(:),                   &
     &                                 Rnn(ityp),2,ztmp,rmt_av(ityp),-1)
!DEBUGPRINT
!           rpa = rmt_av(ityp)
!           call findRsp(kmx,r_hsk(:,nk,ityp),rhokeep(:),rpa,            &
!     &                                             ztmp,rmt_av(ityp))
!         write(6,'(a,i3,2f25.12)') ' ...... ITYP,RMIN',ityp,            &
!     &                              rpa,rmt_av(ityp)
!DEBUGPRINT
        else
         rmt_av(ityp) = rmt_ovlp(1,ityp)
        end if
       enddo                   ! End of ityp loop
       deallocate(p_r); nullify(p_r)
       rsize_min = zero
       do nsub=1,nbasis
        if ( nonES(nsub) ) then
         if ( rsize_min == zero ) then
          rsize_min = Rnn(nsub)
         else if ( Rnn(nsub)>zero ) then
          rsize_min = min(rsize_min,Rnn(nsub))
         end if
        end if
       end do
       rsize_min = size_min*rsize_min
!???       rsize_min = 0.001d0

!C======================================================================
!C      Estimating the MT-radii of the Empty spheres just from the
!C      information about the neighboring environment
!C======================================================================

       Rnn(1:nbasis) = radshl(2,ityp2at(1:nbasis))   ! Rnn with empty spheres
       tmparr = rmt_av
       do nsub=1,nbasis
        if ( nonES(nsub) ) cycle
        iat = ityp2at(nsub)
        do ishell=2,numshell(iat)
         ityp = ntypshl(ishell,iat)
         if ( radshl(ishell,iat)>Rnn(nsub) ) exit
         if ( nonES(ityp) ) then
          if ( tmparr(nsub)==zero ) then
           rmt_av(nsub) = Rnn(nsub)-tmparr(ityp)
          else
           rmt_av(nsub) = min(rmt_av(nsub),Rnn(nsub)-tmparr(ityp))
          end if
          if ( rmt_av(nsub)<zero ) then
           write(6,'(a,i0,a,i0,a)')                                     &
     &     'ERROR: part of a sphere (site ',iat,') is inside IS-sphere '&
     &     //'of atom (site ',ityp2at(ityp),')'
                call fstop(sname//                                      &
     &    ': wrong input, position of empty sphere should be corrected')
          end if
         end if
        end do
       end do
!................... NEW ES
!!!       do ntyp=1,nbasis
!!!        if ( nonES(ntyp) .or. rmt_av(ntyp)==zero ) cycle
!!!        nk = 1
!!!        rhokeep(1:imax(nk,ntyp)) = rhohsk0(1:imax(nk,ntyp),1,nk,ntyp)   &
!!!     &                      + rhohsk0(1:imax(nk,ntyp),nspinp,nk,ntyp)
!!!        endpoints(1) = rmt_av(ntyp)/2
!!!        endpoints(2) = rmt_av(ntyp)
!!!        call maxOfDerv(imax(nk,ntyp),r_hsk(:,nk,ntyp),rhokeep(:),       &
!!!     &                                                  endpoints,rpa)
!!!        if ( rpa > endpoints(1) ) rmt_av(ntyp) = rpa
!!!       enddo
       if ( minval(rmt_av)==zero ) then ! ES weights(radii) are based on SP
        tmparr = rmt_av
        do nsub=1,nbasis
         if ( tmparr(nsub)>zero ) cycle
         iat = ityp2at(nsub)
         do ishell=2,numshell(iat)
          if ( radshl(ishell,iat)>maxval(rmax(:,nsub)) ) cycle
          ityp = ntypshl(ishell,iat)
          if ( tmparr(ityp)>zero ) then
           rmt_av(nsub) = radshl(ishell,iat)-tmparr(ityp)
           exit
          end if
         end do
        end do
        do ntyp=1,nbasis
         if ( nonES(ntyp) .or. rmt_av(ntyp)==zero ) cycle
         nk = 1
         rhokeep(1:imax(nk,ntyp)) = rhohsk0(1:imax(nk,ntyp),nk,ntyp)    &
     &                      + rhohsk0(1:imax(nk,ntyp),nk,ntyp)
         endpoints(1) = rmt_av(ntyp)/2
         endpoints(2) = rmt_av(ntyp)
         call maxOfDerv(imax(nk,ntyp),r_hsk(:,nk,ntyp),rhokeep(:),      &
     &                                                  endpoints,rpa)
         if ( rpa > endpoints(1) ) rmt_av(ntyp) = rpa
        enddo                   ! End of ntyp loop
       end if
       if ( associated(r_hsk) ) deallocate(r_hsk); nullify(r_hsk)
!................... NEW ES

!       do ntyp=1,nbasis    ! it never was applied
!        if ( nonES(ntyp) ) cycle
!        kmx = 0
!        rmax0 = zero
!        f1 = zero
!        do nsub=1,nbasis
!         if ( nonES(ntyp) ) cycle
!         f2 = numbsub(nsub)*rmt_av(nsub)**3
!         if ( f2 < f1 ) then
!          f1 = f2
!          kmx = nsub
!          rmax0 = rmt_av(kmx)
!         end if
!        end do
!        if ( kmx>0 ) then
!         do nsub=1,nbasis
!          if ( nonES(nsub) ) cycle
!          if ( rmt_av(nsub)==rmax0 ) then
!           rmt(nsub) = rmt_av(kmx)
!          else
!           rmt_av(nsub) = zero
!          end if
!         end do
!        end if
!       end do

       do nsub=1,nbasis
        if ( nonES(nsub) ) cycle
        if ( rmt_av(nsub)<rsize_min ) rmt_av(nsub)=rsize_min
        rmt_ovlp(1:komp(nsub),nsub)=rmt_av(nsub)
       end do

       do nsub=1,nbasis
        rmt_av(nsub) = min(rmt_av(nsub),0.5d0*Rn0(nsub))
        do nk=1,komp(nsub)
         if ( ztot(nk,nsub) == zero ) then
          rmt_ovlp(nk,nsub) = rmt_av(nsub)
         else
          rmt_ovlp(nk,nsub) = min(rmt_ovlp(nk,nsub),0.5d0*Rn0(nsub))
         end if
        end do
       end do

       do nsub=1,nbasis
        if( rmt_av(nsub) > zero ) cycle
        if( nonES(nsub) ) cycle
        if( minval(rmt_ovlp(1:komp(nsub),nsub)) > zero ) cycle
        iat = ityp2at(nsub)
        rmin = Rnn(nsub)
        do ishell=2,numshell(iat)
         ityp = ntypshl(ishell,iat)
         if ( radshl(ishell,iat)>Rnn(nsub) ) exit
         if ( .not. nonES(ityp) ) then
          if ( rmt_av(ityp)>zero ) then
            rmin = min(rmin,Rnn(nsub)-rmt_av(ityp))
            if ( rmin<= rsize_min ) rmin = rsize_min
           exit
          end if
         end if
        end do
        rmt_av(nsub) = min(rmin,0.5d0*Rnn(nsub))
        rmt_ovlp(1:komp(nsub),nsub)=rmt_av(nsub)
       end do
!
       write(6,'(1x,86(''-''))')
       if ( iprint >= -1 ) then
        write(6,'(/a/)') sname//': Atomic radii with PBC'
        do nsub=1,nbasis
          write(6,'('' nsub='',i5,'' Rnn='',f8.5,'' Rn0='',f8.5,        &
     &                            (4x,''rmt_SP='',7f10.5))')            &
     &              nsub,Rnn(nsub),Rn0(nsub),rmt_ovlp(1:komp(nsub),nsub)&
     &           ,rmt_av(nsub)
        end do
       end if
!
      END IF
!       do nsub=1,nbasis
!        rmt_ovlp(1:komp(nsub),nsub) = tmp_ovlp(1:komp(nsub),nsub)
!        if ( minval(rmt_ovlp(1:komp(nsub),nsub)) <= zero ) then
!          write(6,'('' ERROR: nsub='',i4,'' rmt_ovlp='',(12(1x,f6.3)/))'&
!     &         ) nsub,rmt_ovlp(1:komp(nsub),nsub)
!              call fstop(sname//' ERROR: bad rmt_ovlp')
!        end if
!       end do
!DEBUG       do nsub=1,nbasis
!DEBUG        ! radii for VP-code
!DEBUG        tmpr(nsub) = dot_product(atcon(1:komp(nsub),nsub),              &
!DEBUG     &                                   rmt_ovlp(1:komp(nsub),nsub))
!DEBUG       end do

!       rmin0=1.0d+03
!       rmax0=0.0d0
!       do nsub=1,nbasis
!         do nk=1,komp(nsub)
!          rmin0=min(rmin0,rmt_ovlp(nk,nsub))
!          rmax0=max(rmax0,rmt_ovlp(nk,nsub))
!         enddo
!       enddo
!       if(rmin0.le.small.or.rmax0.ge.1.0d+03)then
!         do nsub=1,nbasis
!           rmt_ovlp(1:komp(nsub),nsub) = Rnn(nsub)/2
!         enddo
!       endif
!C======================================================================
!C      Generate the Voronoi Polyhedra Information for all the sites
!C      in Unit Cell : The VP info are printed in file Bern_out
!C      Other info for isoparametric kmx are also generated:
!C       These info are stored in: r_circ, fcount,weight,rmag,vj
!C      The version written on 14-Dec, 2009 works only for Ordered
!C      systems
!C======================================================================

           if ( iVP ) then
              do i=1,3
               transV(1,i)= rslatt(1,i)/(pi2)
               transV(2,i)= rslatt(2,i)/(pi2*lattice(2))
               transV(3,i)= rslatt(3,i)/(pi2*lattice(3))
              enddo
              do i=1,natom
                basisV(1,i)= pos(1,i)/alat
                basisV(2,i)= pos(2,i)/(alat*lattice(2))
                basisV(3,i)= pos(3,i)/(alat*lattice(3))
              enddo

              bernal_vp_jcbn%rmt(1:nbasis) = rmt_av(1:nbasis)

!
             write(6,*) 'Doing Voronoi Polyhedra construction'
!
             nshell = nn_shell
             call mpi_comm_rank(mpi_comm_world, myrank, ierr)

             ! only the root node will perform VP construction
             if( myrank == task0 ) then
!             if ( mtasa>1 .and. iadjsph_ovlp.ne.1 ) then
              open(nscr_unit,status='scratch',form='unformatted')     !  file for VP-faces
!             end if
             call VPI_const(MNV,bernal_vp_jcbn%nF,bernal_vp_jcbn%ngauss,&
     &                       natom,nbasis,lattice,transV,basisV,itype,  &
     &         nshell,radshl,bernal_vp_jcbn%rmt(1:nbasis),tmparr,r_circ,&
     &                fcount,rsize_min,weight,rmag,vj,nscr_unit,istop)
             endif

             ! slaves exchange all VP info
             call mpi_bcast(nshell, 1, MPI_INTEGER,                     &
     &           task0, mpi_comm_world, ierr)
             call mpi_bcast(radshl(1,1), size(radshl),                  &
     &           MPI_REAL8, task0,  mpi_comm_world, ierr)
             call mpi_bcast(bernal_vp_jcbn%rmt(1), nbasis,              &
     &           MPI_REAL8, task0, mpi_comm_world, ierr)
             call mpi_bcast(tmparr(1),size(tmparr),MPI_REAL8,           &
     &           task0, mpi_comm_world, ierr)
             call mpi_bcast(r_circ(1),size(r_circ),MPI_REAL8,           &
     &           task0, mpi_comm_world, ierr)
             call mpi_bcast(fcount(1), size(fcount), MPI_INTEGER,       &
     &           task0, mpi_comm_world, ierr)
             call mpi_bcast(rsize_min, 1, MPI_REAL8,                    &
     &           task0, mpi_comm_world, ierr)
             call mpi_bcast(weight(1),size(weight),MPI_REAL8,           &
     &           task0, mpi_comm_world, ierr)
             call mpi_bcast(rmag(0,1,1,1,1,1), size(rmag),              &
     &           MPI_REAL8, task0, mpi_comm_world, ierr)
             call mpi_bcast(vj(1,1,1,1,1),size(vj),MPI_REAL8,           &
     &           task0, mpi_comm_world, ierr)
!
       if ( iprint >= -1 ) then
        write(6,*) sname//': IS radii after VP tessellation'
        do nsub=1,nbasis
          write(6,'('' nsub='',i5,(4x,''rIS_VP='',f10.5))')             &
     &                                    nsub,bernal_vp_jcbn%rmt(nsub)
        end do
       end if
!
             opnd_scr = .false.
             if ( myrank==task0 ) then
              inquire(nscr_unit,opened=opnd_scr)
             end if
             call mpi_bcast(opnd_scr, 1, MPI_LOGICAL,                   &
     &           task0, mpi_comm_world, ierr)
             Lmltpl = 0
             if ( opnd_scr ) then
              if ( abs(iadjsph_ovlp)<=1 ) then
               ix = 0
              else
               Lmltpl = g_Lmltpl(1)
               if ( Lmltpl<=0 ) then
                ix = -1
               else
                ix = max(-1,g_Nmomr())
               end if
              end if
              kmx = nscr_unit
             else
              ix = 0
              kmx = -1
             end if
             rws(1:nbasis) = (tmparr(1:nbasis)*(three/pi4))**third
             vp_volm = dot_product(tmparr(1:nbasis),                    &
     &                                         dble(numbsub(1:nbasis)))
             call calc_vp_data(kmx,ix,rs_box,tmparr,bernal_vp_jcbn)
             rmt(1:nbasis) = bernal_vp_jcbn%rmt(1:nbasis)
             if ( myrank==task0 .and. opnd_scr ) then
              close(nscr_unit)
              opnd_scr = .false.
             end if
             call mpi_bcast(opnd_scr, 1, MPI_LOGICAL,                   &
     &           task0, mpi_comm_world, ierr)
!
!              write(6,*) ' DEBUG max Lmltpl=',Lmltpl
             if (maxval(bernal_vp_jcbn%alphen(1:nbasis)).ne.one) then
              write(6,*)'VP shape-coefficients for potential and energy'
              do nsub=1,nbasis
               write(6,'(''       nsub='',i5,(4x,2(a,f10.6),a,i3))')    &
     &     nsub,' alphpot=',bernal_vp_jcbn%alphpot(nsub),               &
     &          ' alphen=',bernal_vp_jcbn%alphen(nsub)
!               do ir=1,size(bernal_vp_jcbn%ylmrmom,2)
!                 if ( ir==1 .or.                                        &
!     &                abs(bernal_vp_jcbn%ylmrmom(0,ir,nsub))>0 ) then
!                  write(6,'('' lmx='',i2,2x,8(1x,e14.7,1x,e14.7,1x))')  &
!     &                     ir,dreal(bernal_vp_jcbn%ylmrmom(0:,ir,nsub))
!                 end if
!               end do
              end do
             end if
!
             if ( abs(volume-vp_volm) > 1.d-3*volume ) then
               write(6,'(/2a)') ' WARNING: '                             &
     &        ,'INCOMPLETE REAL-SPACE CLUSTER FOR VP TESSELLATION?'
!               write(6,'(''        : '',2a,e11.4/)')                    &
!     &         'BAD VP/ASA-integration ACCURACY,'                       &
!     &        ,' rel.error estimate = ',                                &
!     &                                (vp_volm-volume)/volume
             end if
             rws(1:nbasis) = rws(1:nbasis)*(volume/vp_volm)**third
           else   ! .not.VP
!DEBUGPRINT
              do nsub=1,nbasis
               write(6,'('' nsub,pre-scaled_rmt='',i5,f10.5)')          &
     &                                    nsub,rmt_av(nsub)
!     &                                    nsub,rmt(nsub)
                write(6,'(''   rmt_ovlp='',8f10.5)')                    &
     &                                    rmt_ovlp(1:komp(nsub),nsub)
              end do
!DEBUGPRINT
!              call asa_radii_scaling(nbasis,dble(numbsub),volume,rmt,   &
!     &                                                   rws(1:nbasis))
              vlm_mt = zero
              vlm_es = zero
              do nsub=1,nbasis
               if ( nonES(nsub) ) then
                tmparr(nsub) = dble(numbsub(nsub))
                vlm_mt = vlm_mt+numbsub(nsub)*rmt_av(nsub)**3
               else
                tmparr(nsub) = zero
                vlm_es = vlm_es+numbsub(nsub)*(rmt_av(nsub))**3
               end if
              end do
              vlm_mt = vlm_mt/(vlm_mt+vlm_es)*volume
              vlm_es = volume-vlm_mt
!              tmparr(1:nbasis) = dble(numbsub(1:nbasis))
              if ( re_scale == 1 ) then
               call asa_radii_scaling(nbasis,tmparr,vlm_mt,rmt_av,      &
     &                                                   rws(1:nbasis))
              else
               f1=(three/pi4*vlm_mt/sum(tmparr(1:nbasis)))**third
               call rescaleRadii(nbasis,tmparr,rmt_av,f1,rws(1:nbasis))
              end if
              if ( vlm_es > zero ) then
               do nsub=1,nbasis
                if ( nonES(nsub) ) then
                 tmparr(nsub) = zero
                else
                 tmparr(nsub) = dble(numbsub(nsub))
                end if
               end do
               if ( re_scale == 1 ) then
                call asa_radii_scaling(nbasis,tmparr,vlm_es,rmt_av,     &
     &                                                   rws(1:nbasis))
               else
                f1=(three/pi4*vlm_es/sum(tmparr(1:nbasis)))**third
!DEBUG
               do nsub=1,nbasis
                if ( nonES(nsub) ) then
                 tmparr(nsub) = zero
                else
                 tmparr(nsub) = tmparr(nsub)/rmt_av(nsub)**2
                end if
               end do
!DEBUG
                call rescaleRadii(nbasis,tmparr,rmt_av,f1,rws(1:nbasis))
               end if
              end if

              do nsub=1,nbasis   ! rmt's are IS radii
               rmt(nsub) = rmt_av(nsub)
!               if ( .not. nonES(nsub) ) cycle
               iat = ityp2at(nsub)
               rmin = Rnn(nsub)
               do ishell=2,numshell(iat)
                ityp = ntypshl(ishell,iat)
                if ( radshl(ishell,iat) < 1.5d0*Rnn(nsub) ) then
                 f1 = (rmt_av(nsub) + radshl(ishell,iat)-rmt_av(ityp))/2
                 rmin = min(f1,rmin)
                end if
               end do
               if ( rmin < Rnn(nsub).and.rmin>rsize_min ) rmt(nsub)=rmin
              end do

!CAB              f1 = cellvolm(rslatt,vec,0)/(pi2**3)
!CAB              rws0 = alat*(three*f1/(four*pi))**(one/three)
!CAB              nk = 2   !***  inter-coulomb (between VP)
!CAB              f1 = zero
!CAB              f2 = zero
!CAB              do nsub=1,nbasis
!CAB                 f1 = f1+numbsub(nsub)*rmt(nsub)**3
!CAB                 f2 = f2+numbsub(nsub)*rmt(nsub)**(3-nk)
!CAB              end do
!CAB!              f1 = (rws0**3/f1)**(one/three)
!CAB!              do nsub=1,nbasis
!CAB!                rASA(1:komp(nsub),nsub) = f1*rmt(nsub)
!CAB!              end do
!CAB              f1 = (rws0**3-f1) / f2
!CAB              do nsub=1,nbasis
!CAB                rASA(1:komp(nsub),nsub) = rmt(nsub)*                    &
!CAB     &                               (one+f1/rmt(nsub)**nk)**(one/three)
!CAB              end do
!              nk = 3   !***  inter-coulomb (between VP)
!              do nsub=1,nbasis
!                rws0 = rASA(1,nsub)
!                f1 = dot_product(atcon(1:komp(nsub),nsub),rmt_ovlp(1:komp(nsub),nsub)**3)
!                f2 = dot_product(atcon(1:komp(nsub),nsub),rmt_ovlp(1:komp(nsub),nsub)**(3-nk))
!                f1 = (rws0**3-f1) / f2
!                rASA(1:komp(nsub),nsub) = rmt_ovlp(1:komp(nsub),nsub) *                       &
!     &                               (one+f1/rmt(nsub)**nk)**(one/three)
!              end do
           endif

           if ( nbasis == 1 .and. komp(1) == 1 ) then
!             rmt_ovlp(1,1) = min(rmt_ovlp(1,1),rmt(1))
             rmt_ovlp(1,1) = rmt(1)
             rASA(1,1) = rws(1)
           else
            do nsub=1,nbasis
             if ( komp(nsub)==1 ) then
!              rmt_ovlp(1,nsub) = min(rmt_ovlp(1,nsub),rmt(nsub))
              rmt_ovlp(1,nsub) = rmt(nsub)
              rASA(1,nsub) = rws(nsub)
             else
              if ( re_scale == 1 ) then
               f1 = pi4/three*rmt(nsub)**3
               f2 = dot_product(atcon(1:komp(nsub),nsub),               &
     &                         rmt_ovlp(1:komp(nsub),nsub)**3)
               if ( f2<f1 ) then
                call asa_radii_scaling(komp(nsub),                      &
     &          atcon(1:komp(nsub),nsub),f1,rmt_ovlp(1:komp(nsub),nsub),&
     &                                            rhokeep(1:komp(nsub)))
                rmt_ovlp(1:komp(nsub),nsub) = rhokeep(1:komp(nsub))
               end if
               f1 = pi4/three*rws(nsub)**3
               call asa_radii_scaling(komp(nsub),                       &
     &          atcon(1:komp(nsub),nsub),f1,rmt_ovlp(1:komp(nsub),nsub),&
     &                                          rASA(1:komp(nsub),nsub))
              else
               call rescaleRadii(komp(nsub),atcon(1:komp(nsub),nsub),   &
     &                         rmt_ovlp(1:komp(nsub),nsub),rmt(nsub),   &
     &                         rhokeep(1:komp(nsub)))
               call rescaleRadii(komp(nsub),atcon(1:komp(nsub),nsub),   &
     &                         rmt_ovlp(1:komp(nsub),nsub),rws(nsub),   &
     &                         rASA(1:komp(nsub),nsub))
               rmt_ovlp(1:komp(nsub),nsub) = rhokeep(1:komp(nsub))
              end if

             end if
            end do
           end if
!
          write(6,*) sname//":"
          do nsub=1,nbasis
           write(6,'('' nsub='',i5,4x,''rmt='',f8.5,2x,''rws='',f8.5,2x,&
     &         ''r_circ='',f8.5)') nsub,rmt(nsub),rws(nsub),r_circ(nsub)
           if ( komp(nsub)>1 .or.                                       &
     &          abs(rmt_ovlp(1,nsub)-rmt(nsub))>1.d-4*rmt(nsub) )       &
     &      then
            write(6,'(15x,''rmt_ovlp='',8f10.5)')                       &
     &                                   rmt_ovlp(1:komp(nsub),nsub)
            if ( komp(nsub)>1 ) then
             write(6,'(15x,''rASA='',8f10.5)') rASA(1:komp(nsub),nsub)
            end if
           end if
          end do
!

!C======================================================================
!C      Evaluate the ASA-radii for all the components at each
!C      sublattices using the above calculated MT-radii.
!C      In the derivation we restrict the smaller atoms to grow
!C      faster as compared to the bigger ones.
!C      i.e If R_ASA = R_MT + DR, then DR is prop. to (1/R_MT)
!C======================================================================
!       r_min=1.0d+20
!       do nsub=1,nbasis
!        do nk=1,komp(nsub)
!          r_min = min(r_min,rmt_ovlp(nk,nsub))
!        enddo
!       enddo
!       rsum = 0.0d0
!       do nsub=1,nbasis
!        do nk=1,komp(nsub)
!          X(nk,nsub) = rmt_ovlp(nk,nsub)/r_min
!        enddo
!        rsum = rsum + dble(numbsub(nsub))
!       enddo
!       rlow = (3.0d0*volume/(4.0d0*pi*rsum))**(1.0d0/3.0d0) - r_min
!       rup = 1.0d0
!!C======= ===============================================================
!!C       Solve a cubic equation with roots lying between rlow and rup
!!C======================================================================
!         a1 = 0.0d0
!         a2 = 0.0d0
!         a3 = 0.0d0
!         a4 = 0.0d0
!       do nsub=1,nbasis
!        do nk=1,komp(nsub)
!         a1 = a1 + (numbsub(nsub)*atcon(nk,nsub))*(X(nk,nsub)**3.0d0)
!         a2 = a2 + (numbsub(nsub)*atcon(nk,nsub))*X(nk,nsub)
!         a3 = a3 + (numbsub(nsub)*atcon(nk,nsub))/X(nk,nsub)
!         a4 = a4 + (numbsub(nsub)*atcon(nk,nsub))/(X(nk,nsub)**3.0d0)
!        enddo
!       enddo
!         a1 = (r_min**3.0d0)*a1 - (3.0d0*volume/(4.0d0*pi))
!         a2 = 3.0d0*(r_min**2.0d0)*a2
!         a3 = 3.0d0*(r_min)*a3
!
!
!         do i=1,niter
!           f1 = fcube(rlow,a1,a2,a3,a4)
!           f2 = fcube(rup,a1,a2,a3,a4)
!          if(abs(f2-f1).gt.(eps*f2))then
!             rf = rup - f2*(rup-rlow)/(f2-f1)
!               if(abs(rf-rup).le.1.0d-9)go to 105
!             rlow = rup
!             rup=rf
!          endif
!         enddo
!
!105    do nsub=1,nbasis
!        do nk=1,komp(nsub)
!         rASA(nk,nsub) = rmt_ovlp(nk,nsub) + (rf/X(nk,nsub))
!        enddo
!       enddo

!C==============================================================
!C  If iadjsph_ovlp <= 0, then the sphere size is not calculated
!C                       from the Saddle point representation (SPR)
!C     else              Sphere size is calculated from SPR
!C==============================================================

!         if ( iprint >= 0 ) write(6,*) 'iadj = ', iadjsph_ovlp
         if (iadjsph_ovlp>0)then
           do nsub=1,nbasis
            do nk=1,komp(nsub)
             if(rmt_ovlp(nk,nsub).gt.rASA(nk,nsub)) then
               write(*,*) ' rMT = ',rmt_ovlp(nk,nsub),                  &
     &                    ' rWS = ',rASA(nk,nsub)
               call p_fstop(sname//                                     &
     &                      ': MT-radius > WS-radius -- wrong input?')
             end if
            enddo
           enddo
         endif
!DEBUG         do nsub=1,nbasis
!DEBUG             rws(nsub) = minval(rASA(1:komp(nsub),nsub))
!DEBUG!             if ( mtasa>0 ) then
!DEBUG!               rmt(nsub) = rws(nsub)
!DEBUG!             end if
!             if ( .not.iVP ) then
!               r_circ(nsub) = zero
!             end if
!DEBUG         enddo

      if ( allocated(nonES) ) deallocate(nonES)
!C *******   Optional Write statement **************************
       if ( opnd ) then
        do nsub=1,nbasis
         do nk=1,komp(nsub)
           if (iVP) then
           write(ch_dbg,137)nsub,nk,imax(nk,nsub),                      &
     &      (radshl(2,1)/2.0d0),rmt_ovlp(nk,nsub),                      &
     &       rmt(nsub),r_circ(nsub)
           else
           write(ch_dbg,132)nsub,nk,imax(nk,nsub),                      &
     &      (radshl(2,1)/2.0d0),rmt_ovlp(nk,nsub),                      &
     &       rmt(nsub)
           endif
         enddo
        enddo
       end if
137    format(2I4,I5,1x,4F15.10)
132    format(2I4,I5,1x,3F15.10)
!c---------------------------------------------------------------
!c  Writing the MT, ASA and circums. sphere radii in the out file
!c---------------------------------------------------------------
      if (iVP) then
         write(6,'(/''  MT, ASA and Circums. radii of atoms at'',       &
     &  '' each sublattice in units of ALAT'')')
      else
         write(6,'(/''  MT and ASA radii of various components at'',    &
     &  '' each sublattice in units of ALAT'')')
      endif
      write(6,*)

      do nsub=1,nbasis
       do nk=1,komp(nsub)

        if (iVP) then
         write(6,1007) nsub,nk,rmt(nsub)/alat,                          &
     &               rws(nsub)/alat,r_circ(nsub)/alat
1007     format(' Subl',i5,'(komp=',i2,'): MT-radius is',f9.5,          &
     &              ': WS-radius is',f9.5,': Circ-radius is',f9.5)
        else
         write(6,1008) nsub,nk,rmt(nsub)/alat,                          &
     &               rws(nsub)/alat
1008     format(' Subl',i5,'(komp=',i2,'): MT-radius is',f9.5,          &
     &                          ': WS-radius is',f9.5)
        endif
       end do
      end do
!c     ===============================================================
      deallocate(basisV,numshell)

!       call fstop('DEBUG STOP')

       if (istop.eq.sname) then
         call fstop(sname)
      else
         return
      endif
!c
!EOC
      end subroutine optimal_vp
!
!BOP
!!IROUTINE: calc_vp_data
!!INTERFACE:
      subroutine calc_vp_data(nunit,iflag,rs_box,vp_volm,vp_jcbn)
      implicit none
!!DESCRIPTION:
! calculation of VP-volumes and VP-corrections (for energy and potential
! due to Coulomb interaction 1/|r1-r2|)
!
!
!!ARGUMENTS:
      integer, intent(in) :: nunit ! unit number to read VP-info
      integer, intent(in) :: iflag ! 0: volume calc., -1: +VP-corrections,
!                                  ! >0: polynomials VP-integr.
      type (RS_Str), pointer :: rs_box     ! intent(inout)
      real(8), intent(inout) :: vp_volm(:)   ! vp_volm(1:nbasis) - sublat. vp-volumes
      type(VP_data), pointer :: vp_jcbn
      type(PolyFaces), pointer :: faces(:) => null()
!?      type(PolyFaces), pointer :: p_face
!%       real(8), pointer :: facevtx(:,:,:)  ! (1:nnvp,1:3,1:ipfvtx) ! coordinates of polyFace-vertices
!%       integer, pointer :: nfvtx(:)        !              (1:nnvp) ! number of vertices belonging to each face,
!%       integer :: nnvp=0                       ! number of polyhedron faces (zero or a number >=3)
!!REVISION HISTORY:
! Initial Version - A.S. - 2013
! revised parallelization - A.S. - 2016
!!REMARKS:
! correction is a difference in value between VP and sphere
! of the same volume assuming constant charge density in
! interstitial region (i.e. VP\WS)
!
!EOP
!
!BOC
!      integer, allocatable :: ndim_f(:),ndim_v(:),nvrtx(:)
!      real(8), allocatable :: vrtx(:,:,:)
!      integer, allocatable, target :: nvrtx_all(:,:)
!      real(8), allocatable, target :: vrtx_all(:,:,:,:)
      real(8), allocatable :: vp_isovolm(:)
      integer, pointer :: p_nvrtx(:)
      real(8), pointer :: p_vrtx(:,:,:)
      integer :: ksub,nbasis,ndvrtx,nsub,jsub,nfaces,j,k,kz,ip,ns
      real(8) :: aunit,dV
      real(8) :: sbuff(2)
      complex(8), allocatable :: zbuff(:,:)
      integer n_clst
      real(8), allocatable :: xyz_clst(:,:)
      integer, allocatable :: ityp_clst(:)
      real(8) :: energy,rmomm1
      real(8), parameter :: r2a=(4.d0*pi*third)**third
      logical do_mltpl,do_mltmn
      integer :: nmom,nnb_mx
      integer :: max_nf,max_nv
      integer :: Lmltpl,Lmltmn,lmx
      type(vect3d), pointer :: buffYmns => null()
      ! MPI related variables
      integer :: dest,myrank,nproc,src,tag,ierr
      integer :: mpi_status(MPI_STATUS_SIZE),ndbuf(3)
!      integer :: request1,request2,request3,request4,request5

      ! syncronize processes
!      call log_mpi_sync(__FILE__,__LINE__)
      nbasis = size(vp_volm)
      vp_jcbn%alphen = one
      vp_jcbn%alphpot = one
      nmom = g_Nmomr()
      if ( iflag>0 .and. nmom>=0 ) then
       Lmltpl = g_Lmltpl(1)
       if ( allocated(vp_jcbn%ylmPls) ) then
        deallocate(vp_jcbn%ylmPls)
       end if
       lmx = (Lmltpl+1)*(Lmltpl+2)/2
       allocate(vp_jcbn%ylmPls(0:nmom,1:lmx,1:nbasis))
       if ( allocated(vp_jcbn%ylmMns) ) then
        do nsub=1,nbasis
         if ( allocated(vp_jcbn%ylmMns(nsub)%array) )                   &
     &                deallocate(vp_jcbn%ylmMns(nsub)%array)
         if ( allocated(vp_jcbn%ylmMns(nsub)%indx) )                    &
     &                deallocate(vp_jcbn%ylmMns(nsub)%indx)
!         if ( allocated(vp_jcbn%ylmMns(nsub)%r0nn) )                    &
!     &                deallocate(vp_jcbn%ylmMns(nsub)%r0nn)
        end do
        deallocate(vp_jcbn%ylmMns)
       end if
       Lmltmn = g_Lmltpl(-1)
       lmx = (Lmltmn+1)*(Lmltmn+2)/2
       if ( lmx>0 ) then
        allocate(vp_jcbn%ylmMns(1:nbasis))
       end if
!DEBUGPRINT
       if ( Lmltpl.ne.0 .or. Lmltmn.ne.0 )                              &
     &     write(6,*) ' nmom=',nmom,' Lmltpl=',Lmltpl,' Lmltmn=',Lmltmn
!DEBUGPRINT
      end if

      nmom=-1
      do_mltpl = allocated(vp_jcbn%ylmPls)
      do_mltmn = allocated(vp_jcbn%ylmMns)
      if ( do_mltpl ) then
       nmom = size(vp_jcbn%ylmPls,1)-1
       allocate(zbuff(0:nmom,size(vp_jcbn%ylmPls,2)))
      end if
      if ( do_mltmn ) then
       if ( allocated(xyz_clst) ) deallocate(xyz_clst)
       allocate(xyz_clst(1:3,1:ipnnmax))
       if ( allocated(ityp_clst) ) deallocate(ityp_clst)
       allocate(ityp_clst(1:ipnnmax))
      end if

      allocate(vp_isovolm(nbasis))
      do nsub=1,nbasis
        vp_isovolm(nsub) = pi4/three*vp_jcbn%rmt(nsub)**3 +             &
     &      isopar_volume(vp_jcbn%fcount(nsub),vp_jcbn%vj(:,:,:,:,nsub))
      enddo
      if ( nunit>0 ) then
       call mpi_comm_rank(mpi_comm_world,myrank,ierr)
       call mpi_comm_size(mpi_comm_world, nproc,ierr)
       allocate(faces(nbasis))
!       vp_volm = zero
       if ( myrank==task0 ) then
        rewind(nunit)
        do nsub=1,nbasis
         read(nunit) ksub,nfaces
         faces(ksub)%nnvp = nfaces
         allocate(faces(ksub)%nfvtx(nfaces))
         read(nunit) faces(ksub)%nfvtx(1:nfaces)
         max_nv = maxval(faces(ksub)%nfvtx(1:nfaces))
         allocate(faces(ksub)%facevtx(1:nfaces,1:3,1:max_nv))
         p_nvrtx => faces(ksub)%nfvtx(1:nfaces)
         p_vrtx => faces(ksub)%facevtx
         do j=1,nfaces
          read(nunit) (p_vrtx(j,1:3,k),k=1,p_nvrtx(j))
!          call CalcVol(p_nvrtx(j),p_vrtx(j,1:3,1:p_nvrtx(j)),dV)
!          vp_volm(ksub) = vp_volm(ksub)+abs(dV)
         end do
         if (abs(vp_isovolm(ksub)-vp_volm(ksub))>=1.d-6*vp_volm(ksub))  &
     &    then
          write(6,'(''WARNING: '',2a,e11.4,1x,e11.4'' sublat.='',i4/)') &
     &         'BAD VP/ASA isoparametric integration ACCURACY,'         &
     &        ,' rel/abs error estimate = ',                            &
     &          (vp_isovolm(ksub)-vp_volm(ksub))/vp_volm(ksub),         &
     &          (vp_isovolm(ksub)-vp_volm(ksub)),ksub
         end if
        end do
       end if
       call mpi_bcast(vp_volm,nbasis,MPI_REAL8,task0,mpi_comm_world,    &
     &                                                            ierr)

       IF ( iflag.ne.0 ) THEN

        if ( nproc>1 .and. nbasis>1 ) then   ! to send from task0
         if ( myrank==task0 ) then
          if ( nproc>nbasis ) then
           do ksub=1,nbasis
            dest = task0+ksub
            call sendrecv_polyface(faces(ksub),dest,ksub,               &
     &                                         mpi_comm_world)
            faces(ksub)%nnvp = 0
           end do
          else
           ksub = 0
           do ip=1,nproc
            ksub = ksub+1
            if ( ksub>=nbasis) then
             exit
            end if
            dest = task0 + mod(ksub,nproc)
            if ( dest .ne. task0 ) then
             call sendrecv_polyface(faces(ksub),dest,ksub,              &
     &                                         mpi_comm_world)
             faces(ksub)%nnvp = 0
            end if
           end do
          end if
         else    ! to receive by
          src = task0
          if ( nproc>nbasis ) then
           ksub = myrank-task0
           if ( ksub<=nbasis ) then
            call sendrecv_polyface(faces(ksub),src,ksub,                &
     &                                      mpi_comm_world,mpi_status)
           end if
          else if ( myrank .ne. task0 ) then
           ksub = mod(myrank-task0,nbasis)
           call sendrecv_polyface(faces(ksub),src,ksub,                 &
     &                                      mpi_comm_world,mpi_status)
          end if
         end if
        else
         src  = task0
         dest = task0
        end if

        do ksub=1,nbasis
         nfaces = faces(ksub)%nnvp
         if ( nfaces==0 ) then
          cycle
         end if
         p_nvrtx => faces(ksub)%nfvtx
         p_vrtx => faces(ksub)%facevtx
!          vp_volm(ksub) = abs(vp_volm(ksub))
!          write(6,*) ' DEBUG vp_volm(ksub)=',ksub,vp_volm(ksub)
          aunit = vp_volm(ksub)**third
          call calcCoulombShC(nfaces,p_nvrtx,p_vrtx,aunit,-1,           &
     &                       vp_jcbn%alphpot(ksub),vp_jcbn%alphen(ksub))
          if ( do_mltpl ) then
           nmom = size(vp_jcbn%ylmPls,1)-1
           call calcCoulombShC(nfaces,p_nvrtx,p_vrtx,aunit,nmom,        &
     &                              ylmRmom=vp_jcbn%ylmPls(0:,1:,ksub))
          end if
          if ( do_mltmn ) then
            nnb_mx = max(size(xyz_clst,2),2*nfaces)
            if ( nnb_mx>size(xyz_clst,2) ) then
             if (allocated(xyz_clst) ) deallocate(xyz_clst)
             allocate(xyz_clst(1:3,1:nnb_mx))
             if ( allocated(ityp_clst) ) deallocate(ityp_clst)
             allocate(ityp_clst(1:nnb_mx))
            end if
            nnb_mx = 1+nfaces
            call vp_nnclust(ksub,nnb_mx,rs_box,n_clst,xyz_clst,         &
     &                                                       ityp_clst)
!DEBUGPRINT
!           write(6,*) ' n_clst=',n_clst,' aunit=',aunit
!           write(6,'(4g14.5,i3)')                                       &
!     &          (xyz_clst(1:3,tag),sqrt(sum(xyz_clst(1:3,tag)**2)),     &
!     &                         ityp_clst(tag),tag=1,n_clst)
!DEBUGPRINT
            nmom = size(vp_jcbn%ylmMns,1)-1
            call calcCoulombShC1(nmom,ksub,aunit,n_clst,xyz_clst,       &
     &                          ityp_clst,vp_jcbn)
          end if
        end do
!
        if ( nproc>1 ) then
         if ( myrank>task0 .and. myrank<=task0+nbasis ) then
          do nsub=1,nbasis
           if ( faces(nsub)%nnvp==0 ) cycle
           sbuff(1) = vp_jcbn%alphpot(nsub)
           sbuff(2) = vp_jcbn%alphen(nsub)
           call mpi_send(sbuff,size(sbuff),MPI_REAL8,                   &
     &                                  task0,nsub,mpi_comm_world,ierr)
           if ( do_mltpl ) then
            call mpi_send(vp_jcbn%ylmPls(0:,1:,nsub),                   &
     &                      size(vp_jcbn%ylmPls(0:,1:,nsub)),           &
     &                     MPI_COMPLEX16,task0,nsub,mpi_comm_world,ierr)
           end if
           if ( do_mltmn ) then
            buffYmns => vp_jcbn%ylmMns(nsub)
            ndbuf(1) = size(buffYmns%array,1)
            ndbuf(2) = size(buffYmns%array,2)
            ndbuf(3) = size(buffYmns%array,3)
            call mpi_send(ndbuf,3,                                      &
     &                       MPI_INTEGER,task0,nsub,mpi_comm_world,ierr)
            call mpi_send(buffYmns%array,size(buffYmns%array),          &
     &                       MPI_REAL8,task0,nsub,mpi_comm_world,ierr)
            call mpi_send(buffYmns%indx,size(buffYmns%indx),            &
     &                       MPI_INTEGER,task0,nsub,mpi_comm_world,ierr)
           end if
          end do
         else if ( myrank==task0 ) then
          do nsub=1,nbasis
           if ( faces(nsub)%nnvp>0 ) cycle
           if ( nproc>nbasis ) then
            src = task0+nsub
           else
            src = task0+mod(nsub,nbasis)
           end if
           call mpi_recv(sbuff,size(sbuff),MPI_REAL8,                   &
     &                          src,nsub,mpi_comm_world,mpi_status,ierr)
           vp_jcbn%alphpot(nsub) = sbuff(1)
           vp_jcbn%alphen(nsub)  = sbuff(2)
           if ( do_mltpl ) then
            call mpi_recv(vp_jcbn%ylmPls(0:,1:,nsub),                   &
     &                      size(vp_jcbn%ylmPls(0:,1:,nsub)),           &
     &            MPI_COMPLEX16,src,nsub,mpi_comm_world,mpi_status,ierr)
           end if
           if ( do_mltmn ) then
            buffYmns => vp_jcbn%ylmMns(nsub)
            call mpi_recv(ndbuf,3,                                      &
     &             MPI_INTEGER,src,nsub,mpi_comm_world,mpi_status,ierr)
            if ( .not. allocated(buffYmns%array) )                      &
     &      allocate(buffYmns%array(0:ndbuf(1)-1,1:ndbuf(2),1:ndbuf(3)))
            if ( .not. allocated(buffYmns%indx) )                       &
     &       allocate(buffYmns%indx(1:ndbuf(3)))
            call mpi_recv(buffYmns%array,size(buffYmns%array),          &
     &              MPI_REAL8,src,nsub,mpi_comm_world,mpi_status,ierr)
            call mpi_recv(buffYmns%indx,size(buffYmns%indx),            &
     &              MPI_INTEGER,src,nsub,mpi_comm_world,mpi_status,ierr)
           end if
          end do
         end if

         call mpi_bcast(vp_jcbn%alphpot,nbasis,MPI_REAL8,task0,         &
     &                                              mpi_comm_world,ierr)
         call mpi_bcast(vp_jcbn%alphen,nbasis,MPI_REAL8,task0,          &
     &                                              mpi_comm_world,ierr)
         if ( do_mltpl ) then
           call mpi_bcast(vp_jcbn%ylmPls(0:,1:,1:nbasis),               &
     &                       size(vp_jcbn%ylmPls(0:,1:,1:nbasis)),      &
     &                          MPI_COMPLEX16,task0,mpi_comm_world,ierr)
         end if
         if ( do_mltmn ) then
          do nsub=1,nbasis
           buffYmns => vp_jcbn%ylmMns(nsub)
           ndbuf(1) = size(buffYmns%array,1)
           ndbuf(2) = size(buffYmns%array,2)
           ndbuf(3) = size(buffYmns%array,3)
           call mpi_bcast(ndbuf,3,MPI_INTEGER,task0,mpi_comm_world,ierr)
           if ( .not. allocated(buffYmns%array) )                       &
     &      allocate(buffYmns%array(0:ndbuf(1)-1,1:ndbuf(2),1:ndbuf(3)))
           if ( .not. allocated(buffYmns%indx) )                        &
     &      allocate(buffYmns%indx(1:ndbuf(3)))
           call mpi_bcast(buffYmns%array,size(buffYmns%array),          &
     &                              MPI_REAL8,task0,mpi_comm_world,ierr)
           call mpi_bcast(buffYmns%indx,size(buffYmns%indx),            &
     &                            MPI_INTEGER,task0,mpi_comm_world,ierr)
          end do
         end if

        end if   !  nrpoc>1

       END IF

      end if

      if ( allocated(zbuff) ) deallocate(zbuff)
      if ( allocated(vp_isovolm) ) deallocate(vp_isovolm)
      if ( allocated(xyz_clst) )   deallocate(xyz_clst)
      if ( allocated(ityp_clst) )  deallocate(ityp_clst)
      if ( associated(faces) ) then
       do ksub=1,nbasis
        if ( faces(ksub)%nnvp>0 ) then
         deallocate(faces(ksub)%facevtx)
         deallocate(faces(ksub)%nfvtx)
         nullify(faces(ksub)%facevtx)
         nullify(faces(ksub)%nfvtx)
        end if
       end do
       deallocate(faces)
       nullify(faces)
      end if
      nullify(buffYmns)
      return
!EOC
      end subroutine calc_vp_data
!
      subroutine sendrecv_polyface(face,rank,tag,comm,status)
      implicit none
      type(PolyFaces), intent(inout) :: face
      integer, intent(in) :: rank,tag,comm
      integer, optional :: status(:)

      integer :: ierr,nmax_v,ibuff(2),myrank
      logical :: send

      send = .not. present(status)

      call mpi_comm_rank(comm,myrank,ierr)

      if ( send ) then
       ibuff(1) = face%nnvp
       ibuff(2) = maxval(face%nfvtx)
       call mpi_send(ibuff,size(ibuff),MPI_INTEGER,                     &
     &                                             rank,tag,comm,ierr)
       call mpi_send(face%nfvtx,size(face%nfvtx),MPI_INTEGER,           &
     &                                             rank,tag,comm,ierr)
       call mpi_send(face%facevtx,size(face%facevtx),MPI_REAL8,         &
     &                                             rank,tag,comm,ierr)
      else
       call mpi_recv(ibuff,size(ibuff),MPI_INTEGER,                     &
     &                                        rank,tag,comm,status,ierr)
       face%nnvp = ibuff(1)
       nmax_v = ibuff(2)
       allocate(face%nfvtx(1:face%nnvp))
       allocate(face%facevtx(1:face%nnvp,1:3,1:nmax_v))
       call mpi_recv(face%nfvtx,size(face%nfvtx),MPI_INTEGER,           &
     &                                        rank,tag,comm,status,ierr)
       call mpi_recv(face%facevtx,size(face%facevtx),MPI_REAL8,         &
     &                                        rank,tag,comm,status,ierr)
      end if

      return
      end subroutine sendrecv_polyface

!BOP
!!IROUTINE: set_hsk_cutoff
!!INTERFACE:
      subroutine set_hsk_cutoff(fac,nhs,ndoub,ichgm,rhsk,               &
     &                     ichg,rchg,ztot,rhohsk,                       &
     &                     rmax,imax,alat)
!!DESCRIPTION:
! {\bv
!CALAM ***************************************************************
!     Generate a herman-skillman radial grid for Z=0, also calculate
!     rmax(nk,nsub) for the cut off of charge density for each spin
!     and sublattice.
!CALAM ***************************************************************
! ev}
!
!!REVISION HISTORY:
! Initial Version - mecca_ames - 2010
!EOP
!
!BOC
      implicit none
!c +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!      include 'mkkrcpa.h'
!c +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
      integer   nhs,ndoub,ichgm
      integer   imax,ir,idoub,i
      integer   ichg(0:*)
      real*8    rhsk(*)
      real*8    rchg(0:*)
      real*8    rhohsk(*)
      real*8    ztot,rmax,fac,rstart,h,alat

!C====================================================================
!C      Generate herman-skillman grid for empty sphere.
!C      For empty spheres generate the grid with z=10 (say) to avoid
!C      the blow up of the stepsize h=fac/z^{1/3}
!C====================================================================

       imax = 1
       rmax = 0
       if(ztot.le.0.0d0)then
        h=fac
        rhsk(1)=0.0d0
        rstart=rhsk(1)
        rchg(0)=rstart
        ir=1
        ichg(0)=1
         do idoub=1,ichgm
           do i=1,ndoub
              ir=ir+1
              rhsk(ir)=i*h+rstart
           enddo
           ichg(idoub)=ir
           rstart=rhsk(ir)
           rchg(idoub)=rstart
           h=2.0d0*h
         enddo
!C====================================================================
!C       Put a condition for an estimate of rmax and imax for empty
!C                       spheres ( By intution )
!C====================================================================
         do i=2,nhs
          if(rhsk(i).lt.1.5d0*alat)then
             rmax = rhsk(i)
             imax = i
          endif
         enddo
       else   ! ztot>0
!C====================================================================
!C     Calculating the r-cutoff (and corresponding index i-cutoff)
!C     using the (small) magnitude of charge density at large
!C                         radial distance (r)
!C====================================================================
          do i=2,nhs
           if(abs(rhohsk(i)).gt.1.0d-06)then
              rmax = rhsk(i)
              imax = i
           endif
          enddo
       end if
!c     ===============================================================
      return
!EOC
      end subroutine set_hsk_cutoff

!C================================================
!BOP
!!IROUTINE: rmod
!!INTERFACE:
      function  rmod(xx,yy,zz)
!!DESCRIPTION:
!  rmod is a length of vector (xx,yy,zz)
!
!C================================================
!!REVISION HISTORY:
! Initial Version - mecca_ames - 2010
!EOP
!
!BOC
      implicit real*8 (a-h,o-z)
      real(8) :: rmod
      real(8), intent(in) :: xx,yy,zz
!c
      rmod=sqrt(xx*xx+yy*yy+zz*zz)
!c
      return
!EOC
      end function rmod
!C================================================
!BOP
!!IROUTINE: fcube
!!INTERFACE:
      function  fcube(x,a1,a2,a3,a4)
!!DESCRIPTION:
! fcube is a value of cubic polynomial
!
!!REVISION HISTORY:
! Initial Version - mecca_ames - 2010
!EOP
!
!BOC
      implicit none
      real(8) :: fcube
      real(8), intent(in) :: x,a1,a2,a3,a4
!c
      fcube = a1 + x*( a2 + x*( a3 + x*a4 ) )
!c
      return
!EOC
      end function  fcube


!BOP
!!IROUTINE: neigh_shell
!!INTERFACE:
       subroutine neigh_shell(itype,natom,komp,n_ES,rslatt,             &
     &                        pos,nshell,typshl,natshl,radshl,          &
     &                        shells,iprint)

!!DESCRIPTION:
!CALAM ****************************************************************
!
!  Generate the neighbouring shell data for each atom in the unit
!  cell. The maximum number of shells generated for each atom type
!  is nshell, unless they are greater than rmax from the atom of interest.
!  Each shell contains only one atom type, thus if two different
!  atoms are a distance r away they will be automatically classed as
!  two shells
!
!CALAM ****************************************************************
!
!!REVISION HISTORY:
! Initial Version - mecca_ames - 2010
!!REMARKS:
! ALL COORDINATES/DISTANCES,etc. SHOULD BE IN UNIT CELL UNITS !
!EOP
!
!BOC
      implicit none
      integer   natom,itype(natom),komp(:),iprint
      integer   n_ES(2)
      integer   nshell,typshl(nshell,natom),natshl(nshell,natom),       &
     &          shells(natom)
      integer   i,j,k,ityp,jtyp,nk,n,iat,jat,kat,ishell,ii,ileft
      integer :: n1,n2,n3,nbasis
      real(8) :: rmax,rws,cellvolm
!      real*8    rmax(ipcomp,ipsublat)
      real(8)   pos(3,natom)      !  the same units are expected for rmax,pos and rslatt
      real*8    rslatt(3,3)
      real*8    rj(3)
      real*8    radshl(nshell,natom)
      real*8    r1,r2,r3,rmin,rmax1,sep,dr
      integer, pointer :: iorig(:)
      ! warning: this is also defined in the calling routine
      logical :: opnd

      allocate(iorig(natom))
      iorig = 0
      do jtyp=1,natom
       do jat=1,natom
        if ( itype(jat)==jtyp ) then
         iorig(jtyp) = jat
         exit
        end if
       end do
      end do
      nbasis = 0
      do jat=1,natom
       if ( iorig(jat)>0 ) nbasis=nbasis+1
      end do

!C====================================================================
!C
!C     initialize
!C
      typshl = 0
      natshl = 0
      radshl = 1.d+6
      r1=rmod(rslatt(1,1),rslatt(2,1),rslatt(3,1))
      r2=rmod(rslatt(1,2),rslatt(2,2),rslatt(3,2))
      r3=rmod(rslatt(1,3),rslatt(2,3),rslatt(3,3))
      cellvolm =                                                        &
     &    ( rslatt(2,1)*rslatt(3,2) - rslatt(3,1)*rslatt(2,2) )         &
     &         * rslatt(1,3) +                                          &
     &    ( rslatt(3,1)*rslatt(1,2) - rslatt(1,1)*rslatt(3,2) )         &
     &         * rslatt(2,3) +                                          &
     &    ( rslatt(1,1)*rslatt(2,2) - rslatt(2,1)*rslatt(1,2) )         &
     &         * rslatt(3,3)
      rws=(abs(cellvolm)/(natom-n_ES(2))*three/pi4)**third

! maybe, it is better to have rmax for each sublattice/component

!DELETE      rmax = rmax + rws*two*(dble(nshell)/dble(nbasis-n_ES(1)))**third
      rmax = max(r1,r2,r3)
      rmax = rmax+ rws*(two*dble(nshell))**third

!      do i=1,natom
!!C====================================================================
!!C       This step is different from J M Maclaren's code: Bcos
!!C       that code is applicable only for ordered sublattices
!!C       however we have generalised it for sublattices which
!!C       may involve substitutional disorder..........
!!C====================================================================
!       ityp=itype(i)
!       rbig = max(rbig,maxval(rmax(1:komp(ityp),ityp)))
!      enddo
!c
!c----- decide approximately how many unit cells are needed
!c
      n1 = 1+ceiling(rmax/r1)
      n2 = 1+ceiling(rmax/r2)
      n3 = 1+ceiling(rmax/r3)
!c
!c----- loop over these unit cells
!c
      do 50 i=-n1,n1
      do 50 j=-n2,n2
      do 50 k=-n3,n3
!?      do jtyp=1,nbasis
!?       jat = iorig(jtyp)
      do jat=1,natom
       jtyp = itype(jat)
!!!       rmax1=max(rmin,maxval(rmax(1:komp(jtyp),jtyp)))
       rmax1=rmax  ! it may depend on sulattice/component
!c
!c----- rj is the vector to unit cell <ijk>
!c
      rj(1)=i*rslatt(1,1)+j*rslatt(1,2)+k*rslatt(1,3)
      rj(2)=i*rslatt(2,1)+j*rslatt(2,2)+k*rslatt(2,3)
      rj(3)=i*rslatt(3,1)+j*rslatt(3,2)+k*rslatt(3,3)
!c
!c----- loop over atoms
!c
!c
!c----- build shell table for atom kat
!c
      do 40 kat=1,natom
       sep=rmod(rj(1)+pos(1,jat)-pos(1,kat),                            &
     &          rj(2)+pos(2,jat)-pos(2,kat),                            &
     &          rj(3)+pos(3,jat)-pos(3,kat))

      if(sep.le.rmax1) then
!c
!c----- compare this separation with previously found shells
!c
       ishell=0
   20  ishell=ishell+1
       if(ishell.le.nshell) then
        dr=sep-radshl(ishell,kat)
        if(abs(dr).lt.1.0d-08) dr=0.0d0
        if(dr.gt.0.0d0) goto 20
        if(dr.eq.0.0d0) then
         if(typshl(ishell,kat).ne.jtyp) goto 20
         natshl(ishell,kat)=natshl(ishell,kat)+1
        else
         if(ishell.ne.nshell) then
!c
!c----- shift everything up to insert this shell
!c
          do ii=nshell,ishell+1,-1
           if ( ii>1 ) then
           typshl(ii,kat)=typshl(ii-1,kat)
           natshl(ii,kat)=natshl(ii-1,kat)
           radshl(ii,kat)=radshl(ii-1,kat)
           else
            exit
           end if
          end do
         endif           ! end of ishell.ne.nshell
!c
         typshl(ishell,kat)=jtyp
         natshl(ishell,kat)=1
         radshl(ishell,kat)=sep
        endif            ! end of dr.eq.0.0d0
        endif             ! ishell.le.nshell
       endif              ! sep.le.rmax1
   40 continue
       end do
   50  continue
!c
!c----- find out how many shell we got
!c
       do iat=1,natom
        shells(iat)=0
        do ishell=2,nshell
         if(natshl(ishell,iat).gt.0) then
          shells(iat)=shells(iat)+1
         endif
        enddo
       enddo
!
      do iat=1,natom
       ityp = typshl(1,iat)
       do ishell=2,nshell
        if(natshl(ishell,iat).gt.0) then
         if ( typshl(ishell,iat)==ityp ) then
          if ( shells(iat)+natshl(ishell,iat) > nshell ) then
!DELETE          if ( abs(radshl(ishell,iat)-radshl(ishell-1,iat))>1.d-8 )then &
!           write(6,*) 'iat,shells(iat)=',iat,shells(iat)
!DELETE           shells(iat)=nk
           exit
          else
           shells(iat)=shells(iat)+natshl(ishell,iat)
          end if
         end if
        endif
       enddo
      enddo
!
!c----- write out neighbourhood for each atom
!
      if (iprint.ge.1)then
       inquire(unit=ch_dbg,opened=opnd )
       if ( opnd ) then
        WRITE(ch_dbg,*) ' NATOM=',natom
        do iat=1,natom
          write(ch_dbg,'('' Shell structure for atom '', i4             &
     &           '' in the unit cell '')')iat
           ishell=shells(iat)
           ileft=ishell/9
           if(mod(ishell,9).ne.0) ileft=ishell/9+1
          do ii=1,ileft
            write(ch_dbg,'(/'' Shell: '', 9i7)')                        &
     &                     (i,i=9*ii-8,min(9*ii,ishell))
            write(ch_dbg,'(''  Type: '', 9i7)')                         &
     &            (typshl(i,iat),i=9*ii-8,min(9*ii,ishell))
            write(ch_dbg,'('' Atoms: '', 9i7)')                         &
     &            (natshl(i,iat),i=9*ii-8,min(9*ii,ishell))
            write(ch_dbg,'(''Radius: '',9f7.3)')                        &
     &             (radshl(i,iat),i=9*ii-8,min(9*ii,ishell))
          enddo
        enddo
        write(ch_dbg,*) ' ------------------------------------------'
       end if
      endif
!c     ===============================================================
      return
!EOC
      end subroutine neigh_shell
!C======================================================================
!BOP
!!IROUTINE: integr
!!INTERFACE:
      subroutine integr(f,r,kmx,ichg,a,md)
!!DESCRIPTION:
!======================================================================
!
! this routine integrates function f on a 1-d mesh by quadratures
! the partial integrals (up to r(k)) are stored in b(k).  the mesh
! is assumed to be of herman-skillman type with the doubling points
! specified on ichg
!
!======================================================================
!
!!ARGUMENTS:
!!PARAMETERS:
!!REVISION HISTORY:
! Initial Version - mecca_ames - 2010
!!REMARKS:
!EOP
!
!BOC
      implicit real*8 (a-h,o-z)
!c
      integer kmx,md
      real(8), parameter :: s720=720.0d0,s646=646.0d0,s456=456.0d0,     &
     &                                                 s346=346.0d0,    &
     &         s264=264.0d0,s251=251.0d0,s106=106.0d0,s74=74.0d0,       &
     &         s30=30.0d0,s25=25.0d0,s24=24.0d0,s19=19.0d0, s11=11.0d0, &
     &         s10=10.0d0,s9=9.0d0,s7=7.0d0,s6=6.0d0,s5=5.0d0,s4=4.0d0, &
     &         s2=2.0d0,s0=0.0d0
!c
      real(8) :: f(kmx),r(kmx),a(kmx)
      integer ichg(*)
      integer :: n,k,k0,km1,km2,km3,kfst,klst,kp1
      real(8) :: h,f0,amax

!      character  sname*10
!      parameter (sname='integr')
!c
!c----- initialise
!c
      if ( kmx <= 0 ) return
      h=r(2)-r(1)
      if(md.ne.1) then
       k0=1
       a(1)=s0
       f0=f(1)
      else
       k0=0
       f0=s0
      endif
      n=1
      km3=k0+1
      km2=k0+2
      km1=k0+3
      kfst=k0+4
!c
!c----- quadrature formulas  q41(0), q41(1), q41(2) for 1st 3 points
!c
      a(km3)=h*(s251*f0+s646*f(km3)-s264*f(km2)+s106*f(km1)-s19*f(kfst))&
     & /s720
      a(km2)=a(km3)+h*(-s19*f0+s346*f(km3)+s456*f(km2)-s74*f(km1)+s11*f &
     & (kfst))/s720
      a(km1)=a(km2)+h*(s11*f0-s74*f(km3)+s456*f(km2)+s346*f(km1)-s19*f  &
     & (kfst))/s720
!c
!c----- main part of integration q31(2)
!c
   10 klst=min(ichg(n),kmx)
      do k=kfst,klst
       a(k)=a(km1)+h*(s9*f(k)+s19*f(km1)-s5*f(km2)+f(km3))/s24
       km3=km2
       km2=km1
       km1=k
      end do
!c
!c----- if grid has doubled apply special formulae
!c
      k=klst+1
      if(klst < kmx) then
       h=r(k)-r(klst)
       a(k)=a(km1)+h*(s2*f(k)+s7*f(km1)-s4*f(km2)+f(km3))/s6
       if(k < kmx) then
        kp1=k+1
        a(kp1)=a(k)+h*(s11*f(kp1)+s25*f(k)-s10*f(km1)+s4*f(km2))/s30
        if(kp1 < kmx) then
         km3=klst
         km2=k
         km1=kp1
         kfst=kp1+1
         n=n+1
         goto 10
        endif
       endif
      endif
!c
      if(mod(md,2).eq.0) then
       amax=a(kmx)
       a(kmx)=s0
       a(1:kmx-1) = amax-a(1:kmx-1)
      endif

      return
!EOC
      end subroutine integr

!C================================================================
!BOP
!!IROUTINE: invgrd
!!INTERFACE:
      function invgrd(r,h0,itype,rmax,imax,ichgm,rchg,ichg)
!!DESCRIPTION:
! this function finds the grid point nearest to r
!
!!REVISION HISTORY:
! Initial Version - mecca_ames - 2010
!EOP
!
!BOC
      implicit real*8 (a-h,o-z)
      integer :: invgrd

      integer itype,imax,ichgm,ichg(0:*)
      real*8  r,h0,rmax,rchg(0:*)
      real(8) :: h
      integer :: i
!c
      if(r.ge.rmax) then
!c
!c----- beyond range of rho
!c
       invgrd=imax
      else
!c
!c----- find nearest change point
!c
       h=h0
       do i=0,ichgm-1
        if(r.ge.rchg(i).and.r.lt.rchg(i+1)) then
         r=r-rchg(i)
         invgrd=ichg(i)+nint(r/h)
         exit
        endif
        h=2.0d0*h
       end do
      endif
      return
!EOC
      end function invgrd

!C================================================
!BOP
!!IROUTINE: step_size
!!INTERFACE:
        subroutine step_size(ztot,komp,nbasis,nhs,fac,ndoub,ichgm)
!!REVISION HISTORY:
! Initial Version - mecca_ames - 2010
! Edited - A.S. - 2013
!EOP
!
!BOC
        implicit double precision(a-h,o-z)
        integer :: nbasis,nhs
        real(8) :: ztot(:,:),fac
        integer komp(nbasis),ndoub,ichgm
        integer :: nsub,nk
        real(8) :: zmax

        zmax=1.0d0
        do nsub=1,nbasis
           do nk=1,komp(nsub)
             zmax=max(zmax,ztot(nk,nsub))
           enddo
        enddo
         if(zmax.gt.10)then
           fac=0.88534138d0*5.0d-02
           ndoub=200
         else
           fac=0.88534138d0*5.0d-03
           ndoub=100
         endif
            ichgm=nhs/ndoub

         return
!EOC
         end subroutine step_size
!
!BOP
!!IROUTINE: asa_radii_scaling
!!INTERFACE:
       subroutine asa_radii_scaling(nbasis,wght,volume,rmt,rasa)
       implicit none
!!DESCRIPTION:
! ASA radii scaling based on cubic equation solution
!
!!ARGUMENTS:
       integer, intent(in) :: nbasis
       real(8), intent(in) :: wght(nbasis)
       real(8), intent(in) :: volume,rmt(nbasis)
       real(8), intent(out) :: rasa(nbasis)
!!PARAMETERS:
       real(8), parameter :: eps=1.d-7
       integer, parameter :: niter=50
!!REVISION HISTORY:
! Initial Version - mecca_ames - 2010
! Edited          - A.S. - 2013
!!REMARKS:
!  we restrict the smaller atoms to grow faster as compared to the
!  bigger ones (i.e if R_ASA = R_MT + DR, then DR is prop. to (1/R_MT)
!
!EOP
!
!BOC
       real(8) :: r_min,rsum,rlow,rup,a1,a2,a3,a4,f1,f2,rf
       real(8) :: X(nbasis)
       integer :: nsub,i

!       rsum = (3.0d0*volume/(4.0d0*pi)/sum(wght(1:nbasis)))**third
!       call rescaleRadii(nbasis,wght,rmt,rsum,rasa)
!       if ( rsum>zero ) return
!C======================================================================
       r_min = minval(rmt,mask=wght>zero)
       if ( r_min<=zero ) then
         write(6,*) ' wght:',sngl(wght)
         write(6,*) ' rmt(in):',sngl(rmt)
         call p_fstop(' ERROR in asa_radii_scaling: rmt<=0')
       end if
       X = rmt/r_min
       rsum = sum(wght)
       rlow = (3.0d0*volume/(4.0d0*pi*rsum))**(1.0d0/3.0d0) - r_min
       if ( abs(rlow)<3*epsilon(rlow) ) then
           do nsub=1,nbasis
            if ( wght(nsub)>zero ) rasa(nsub)=rmt(nsub)
           end do
           return
       else if ( rlow < 0.d0 ) then
        write(6,*) ' rlow,r_min= ',rlow,r_min,' n=',nbasis
        write(6,*) ' rws=',(3.0d0*volume/(4.0d0*pi*rsum))**(1.0d0/3.0d0)
        call p_fstop(' ERROR in asa_radii_scaling: rmt > rws')
       end if
       rup = 1.0d0
!C======================================================================
!C       Solve a cubic equation with roots lying between rlow and rup
!C======================================================================
         a1 = 0.0d0
         a2 = 0.0d0
         a3 = 0.0d0
         a4 = 0.0d0
       do nsub=1,nbasis
        if ( wght(nsub)>zero ) then
         a1 = a1 + (X(nsub)**3.0d0)*wght(nsub)
         a2 = a2 + X(nsub)*wght(nsub)
         a3 = a3 + wght(nsub)/X(nsub)
         a4 = a4 + wght(nsub)/(X(nsub)**3.0d0)
        end if
       enddo
         a1 = (r_min**3.0d0)*a1 - (3.0d0*volume/(4.0d0*pi))
         a2 = 3.0d0*(r_min**2.0d0)*a2
         a3 = 3.0d0*(r_min)*a3

         do i=1,niter
           f1 = fcube(rlow,a1,a2,a3,a4)
           f2 = fcube(rup,a1,a2,a3,a4)
          if(abs(f2-f1).gt.(eps*f2))then
             rf = rup - f2*(rup-rlow)/(f2-f1)
               if(abs(rf-rup).le.1.0d-9) exit
             rlow = rup
             rup=rf
          endif
         enddo

        do nsub=1,nbasis
         if ( wght(nsub)>zero ) rasa(nsub)=rmt(nsub)+rf/X(nsub)
        end do
!C==============================================================
       return
!EOC
       end subroutine asa_radii_scaling
!BOP
!!IROUTINE: rescaleRadii
!!INTERFACE:
       subroutine rescaleRadii(ns,wght,rin,Rtot,rout)
       implicit none
!!DESCRIPTION:
! radii re-scaling defined by input sphere radii {\tt rin}, weights {\tt wght} and
! desirable final total volume (sph. radius {\tt Rtot})
!
!!ARGUMENTS:
       integer, intent(in) :: ns
       real(8), intent(in) :: wght(1:ns)
       real(8), intent(in) :: Rtot,rin(ns)
       real(8), intent(inout) :: rout(ns)
!!REVISION HISTORY:
! Initial Version - A.S. - 2015
!!REMARKS:
! weighted sum of sphere volumes defined by input {\tt rin} is
! expected to be smaller than volume of sphere with radius {\tt Rtot}
!
!EOP
!
!BOC
       real(8) :: v_in(ns),v_out,wsum,addV
       integer :: i
!C==============================================================
       v_in(1:ns) = rin(1:ns)**3
       v_out = Rtot**3
       wsum = sum(wght(1:ns))
       addV = v_out-dot_product(v_in(1:ns),wght(1:ns))/wsum
       do i=1,ns
        if ( wght(i)>0 ) then
         rout(i) = (v_in(i)+addV)**third
        end if
       end do
!DEBUGPRINT
!       write(6,*) ' rin=',sngl(rin(1:ns)),' Rtot=',sngl(Rtot)
!       write(6,*) ' wght=',sngl(wght(1:ns))
!       write(6,*) ' Rtot^3=',Rtot**3,' addV=',sngl(addV)
!       write(6,*) ' rout=',sngl(rout(1:ns))
!       write(6,*) ' check=',Rtot**3-dot_product(rout(1:ns)**3,          &
!     &                                          wght(1:ns))/wsum
!DEBUGPRINT
       return
!EOC
       end subroutine rescaleRadii
!
      subroutine findRz(nmax,r,rho,Rnn,kd,Z,rZ,nch)
      implicit none
      integer, intent(in) :: nch,nmax,kd
      real(8), intent(in) :: Rnn,r(nmax),rho(nmax)
      real(8), intent(in) :: Z
      real(8), intent(out) :: rZ
      real(8) :: rho_kd(nmax),rhoint(nmax)
      integer :: i,iex
      real(8) :: x1,x2,f1,f2,f0

      rZ = r(nmax)
      rho_kd = pi4*rho*r**kd
      if ( Z>0 ) then
       call intqud(rho_kd,rhoint,r,nmax)
       do i=2,nmax
        if ( rhoint(i)>Z ) then
           x1 = r(i-1)
           x2 = r(i)
           f1 = rhoint(i-1)
           f2 = rhoint(i)
           if ( f1>Z .or. f2<=Z ) then
             write(6,*) ' i=',i
             write(6,*) ' x1=',x1,' f1=',f1
             write(6,*) ' x2=',x2,' f1=',f2
             call p_fstop(' Unexpected case')
           end if
           do
            rZ = 0.5d0*(x1+x2)
            f0 = ylag(rZ,r(i-3:i+2),rhoint(i-3:i+2),0,3,6,iex)
            if ( abs(f0/Z-1.d0)<1.d-14 ) exit
            if ( abs(x2-x1)/(abs(x2)+abs(x1))<1.d-14 ) exit
            if ( f0<Z ) then
             x1 = rZ
             f1 = f0
            else
             x2 = rZ
             f2 = f0
            end if
           end do
         exit
        end if
       end do
       if ( nch>=0 ) then
        do i=2,nmax
         if ( r(i) >= 2*Rnn ) exit
          write(nch,'(1x,I4,1x,4F25.12)')i,r(i)/Rnn                     &
     &,                    rho(i)                                       &
     &,                    rho_kd(i)                                    &
     &,                    rhoint(i)
        enddo
       end if
      else
       if ( nch>=0 ) then
        do i=2,nmax
         if ( r(i) >= 2*Rnn ) exit
          write(nch,'(1x,I4,1x,3F25.12)')i,r(i)/Rnn                     &
     &,                    rho(i)                                       &
     &,                    rho_kd(i)
        enddo
       end if
      end if

      return
      end subroutine findRz

      subroutine findRsp(nmax,r,rho,rin,Z,rout)
      implicit none
      integer, intent(in) :: nmax
      real(8), intent(in) :: rin,r(nmax),rho(nmax)
      real(8), intent(in) :: Z
      real(8), intent(out) :: rout
      real(8) :: endpoints(2)

      endpoints(1) = rin
      endpoints(2) = 3*rin
      rout = rin
      if ( Z>0 ) then
       call maxOfDerv(nmax,r,rho,endpoints,rout)
      else
       rout = zero
      end if

      return
      end subroutine findRsp
!
      subroutine maxOfDerv(n,r,ft,endpoints,rout)
      implicit none
      integer, intent(in) :: n
      real(8), intent(in) :: r(n)
      real(8), intent(in) :: ft(n)
      real(8), intent(in) :: endpoints(2)
      real(8), intent(out) :: rout
      real(8), external :: ylag
      integer, external :: indRmesh

      real(8), pointer :: dy(:)
      real(8), pointer :: d2y(:)
      integer :: i,i0,i1,i2,ipls,imns,iex,kn
      real(8) :: x1,x2,rin,rpls,rmns,f0,s,s1,Rmx
      logical :: found
      real(8), pointer :: fun(:)=>null()

      rin = minval(endpoints)
      Rmx = maxval(endpoints)
      rout = rin
      if ( Rmx-rin<epsilon(rin) ) return
      kn = 3+indRmesh(Rmx,n,r)
      i0=indRmesh(rin,kn,r)
!DEBUGPRINT
!      write(56,'(/a,i4,a,f10.6)') '# n=',n,' rin=',rin
!      write(57,'(/a,i4,a,f10.6)') '# n=',n,' rin=',rin
!      write(6,'(/a,i4,a,f10.6)') '# n=',n,' rin=',rin
!DEBUGPRINT
      allocate(dy(kn))
      call derv5_VP(ft(1:kn),dy(1:kn),r(1:kn),1,kn)
      fun => dy
!DEBUGPRINT
!      allocate(d2y(kn))
!      call derv5_VP(dy(1:kn),d2y(1:kn),r(1:kn),1,kn)
!      fun => d2y
!      do i=1,kn
!      write(56,'(i5,4e18.7)') i,r(i),ft(i),dy(i),d2y(i)
!      if (min(f1(i),f2(i))>0) then
!      write(57,*) i,sngl(r(i)),sngl(f1(i)),sngl(f2(i))
!      end if
!      end do
!      write(56,*)
!DEBUGPRINT

      ipls = 0
      rpls = rin
      do i=max(1,i0-1),kn
        if ( r(i)>Rmx ) exit
! dy
        found = ( fun(i-1)<=0 .and.fun(i)>0 )
! d2y
!        found = ( dy(i-1)>=0 ) .and.                                    &
!     &          ( fun(i-1)>0 .and.fun(i)<=0 )
       if( found ) then
        x1 = r(i-1)
        x2 = r(i)
        ipls = i
        i2 = min(kn,i+2)
        i1 = min(i-3,i2-3)
        s = -sign(one,fun(i-1))
        do
         rpls = 0.5d0*(x1+x2)
         f0 = s*ylag(rpls,r(i1:i2),fun(i1:i2),0,3,i2-i1+1,iex)
         if ( abs(f0)<1.d-8 .or. abs(x2-x1)<1.d-14 ) then
           rpls = 0.5d0*(x1+x2)
           exit
         end if
         if ( f0<0 ) then
           x1 = rpls
         else
           x2 = rpls
         end if
        end do
        exit
       end if
      end do
      imns = 0
      rmns = rin
      do i=min(kn,i0+1),2,-1
        if ( r(i)<rin/2 ) exit
! dy
        found = ( fun(i-1)<=0 .and.fun(i)>0 )
! d2y
!        found = ( dy(i-1)>=0 ) .and.                                    &
!     &          ( fun(i-1)>0 .and.fun(i)<=0 )
       if( found ) then
        x1 = r(i-1)
        x2 = r(i)
        imns = i
        i1 = max(1,i-3)
        i2 = max(i+2,i1+3)
        s = -sign(one,fun(i-1))
        do
         rmns = 0.5d0*(x1+x2)
         f0 = s*ylag(rmns,r(i1:i2),fun(i1:i2),0,3,i2-i1+1,iex)
         if ( abs(f0)<1.d-8 .or. abs(x2-x1)<1.d-14 ) then
           rmns = 0.5d0*(x1+x2)
           exit
         end if
         if ( f0<0 ) then
           x1 = rmns
         else
           x2 = rmns
         end if
        end do
        exit
       end if
      end do
      if ( ipls==0 .and. imns==0 ) then
       rout = rin
      else if ( ipls==0 ) then
        rout = rmns
      else if ( imns==0 ) then
        rout = rpls
      else
       if ( rin-rmns<rpls-rin ) then
        rout = rmns
       else if ( rpls-rin<rin-rmns) then
        rout = rpls
       else
        rout = rin
       end if
      end if

      return
      end subroutine maxOfDerv

      subroutine vp_nnclust(ksub,nnb_max,rs_box,nnb,xyz_nb,ityp_nb)
      integer, intent(in) :: ksub
      integer, intent(in) :: nnb_max
      type (RS_Str), pointer :: rs_box
      integer, intent(out) :: nnb
      real(8), intent(out) :: xyz_nb(:,:)  ! in the same units as rs_box%rcs
      integer, intent(out) :: ityp_nb(:)

      integer, pointer :: itype(:)
      real(8) :: rslatt(3,3)
      integer :: jclust(size(ityp_nb))
      integer :: ntau(size(ityp_nb))
      integer :: nlatt(3)

      real(8) :: Rmx,Rsmall,rytodu
      integer :: i,iatom,nsmall
      integer :: nrsij,ndimnn,ndimbas
      integer :: mapsnni(1,1),mappnni(1,1),mapnnij(1,1)
      real(8) :: rsij(3,1)
      integer :: j
      real(8) :: ri
      integer, allocatable :: ibuff(:)
      real(8), allocatable :: buff(:,:)

      ndimnn = 0
      nsmall = 0
      ndimbas = size(rs_box%mapstr,1)
      itype => rs_box%itype
      do i=1,rs_box%natom
       if ( itype(i) == ksub ) then
        iatom = i
        exit
       end if
      end do
      Rmx = rs_box%rcs(ksub) + maxval(rs_box%rcs(1:rs_box%nsubl))
!      Rmx = 2 * maxval(rs_box%rcs(1:rs_box%nsubl))
      rslatt = rs_box%rslatt
!      rslatt = rs_box%rslatt*(rs_box%alat/pi2)
      rytodu = rs_box%alat/pi2                   ! rs_box%alat
      Rmx = Rmx / rytodu
      do i=1,3
       nlatt(i) = 1+Rmx/sqrt(sum(rslatt(1:3,i)**2))
!       nlatt(i) = 2+Rmx/sqrt(sum(rslatt(1:3,i)**2))
      end do
      Rsmall = Rmx
      nnb = size(ityp_nb)

      call nncluster(iatom,rs_box%natom,rs_box%mapstr,ndimbas,          &
     &               rs_box%aij,Rmx,Rsmall,rslatt,itype,nlatt,          &
     &                   nnb,nsmall,                                    &
     &                   jclust,ityp_nb,ntau,                           &
     &                   rsij,nrsij,mapsnni,ndimnn,mappnni,mapnnij      &
     &                   ,xyz_nb)
      xyz_nb(:,1:nnb) = xyz_nb(:,1:nnb)*rytodu ! in at.un.
      allocate(buff(size(xyz_nb,1),size(xyz_nb,2)))
      allocate(ibuff(size(ityp_nb,1)))
      buff = xyz_nb(:,1:nnb)
      ibuff = ityp_nb(1:nnb)
      j = 1
      do i=2,nnb
       rsij(1:3,1) = buff(:,i)-buff(:,1)
       ri = sqrt(dot_product(rsij(1:3,1),rsij(1:3,1)))
       if ( ri < rs_box%rcs(ksub)+rs_box%rcs(ibuff(i))+1.d-13 ) then
        j = j+1
        xyz_nb(:,j) = buff(:,i)
        ityp_nb(j) = ibuff(i)
       end if
      end do
      nnb = j
      deallocate(buff,ibuff)
      return
      end subroutine vp_nnclust

      subroutine gESxyzr( alat, rs_box )
      implicit none
      real(8), intent(in)    :: alat
      type (RS_Str), pointer :: rs_box    ! intent(in)

      interface
      subroutine mkES( alatvecs, Nats, atomic_xyz_cartesian, atomic_r,  &
     &                 nES, ES_xyz,ES_r, info,                          &
     &                 isES_degeneracy, rescale)
      implicit none
      real(8), intent(in) :: alatvecs(3,3)
      integer, intent(in) :: Nats
      real(8), intent(in) :: atomic_xyz_cartesian(3,Nats)
      real(8), intent(in) :: atomic_r(Nats)
      integer, intent(out) :: nES
      real(8), intent(out), allocatable :: ES_xyz(:,:)
      real(8), intent(out), allocatable :: ES_r(:)
      integer, intent(out) :: info
      integer, intent(out), allocatable, optional :: isES_degeneracy(:)
      real(8), intent(in), optional :: rescale
      end subroutine mkES
      end interface

!!!      real(8), pointer :: alat => null()
      integer, pointer :: natom => null()
      integer, pointer :: itype(:) => null()    ! itype(natom)
      real(8), pointer :: rslatt(:,:) => null() ! rslatt(3,3), in units of Alat/(2*pi)
      real(8), pointer :: pos(:,:) => null()    ! pos(3,natom)
      real(8), pointer :: rIS(:) => null()      ! rIS(natom), radii of VP inscribed spheres

      integer :: nES,info_es
      real(8), allocatable :: ES_xyz(:,:),ES_r(:)
      integer, allocatable :: isES_degeneracy(:)

!!!      alat  => rs_box%alat
      natom => rs_box%natom
      itype => rs_box%itype
      rslatt => rs_box%rslatt
      pos => rs_box%xyzr
      rIS => rs_box%rmt

!      call mkES((alat/pi2)*rslatt,natom,pos(1:3,1:natom),               &
!     &                 rs_box%rmt(itype(1:natom)),                      &
!     &                 nES, ES_xyz,ES_r, info_es,                       &
!     &                 isES_degeneracy)

      call p_fstop(' gESxyzr: '//                                       &
     &             'remove p_fstop-call if you want to continue')

      return
      end subroutine gESxyzr

      subroutine rho_latt_site(ityp,nmx,komp,imax,rmax,atcon,ztot,r_hsk,&
     &             numshell,radshl,ntypshl,natshl,rhoint,rhohsk0)
       implicit none

       integer, intent(in) :: ityp,nmx
       integer, intent(in) :: komp(1:),imax(1:,1:)
       real(8), intent(in) :: atcon(1:,1:),ztot(1:,1:),rmax(1:,1:)
       real(8), intent(in) :: rhoint(1:,1:,1:) ! 1:nbasis

       real(8), intent(in) :: r_hsk(1:,1:,1:)
       real(8), intent(in) :: radshl(1:)
       integer, intent(in) :: numshell
       integer, intent(in) :: ntypshl(1:),natshl(1:)

       real(8), intent(inout) :: rhohsk0(1:,1:,1:) ! 1:nbasis
!
! rhohsk0 -- spherical charge distribution for all site components from neighbors
!
       real(8), external :: ylag

       integer :: nbasis,ns,ntyp
       integer :: ir,ix,ishell,nki,nkn
       real(8) :: rma,rpa,f1,a,pref,h0
       real(8), parameter :: pi=4*atan(1.d0),zero=0.d0

       do nki=1,komp(ityp)
!
!C------    Loop over shells
!
           do ishell=2,numshell
             ntyp=ntypshl(ishell)
             if ( ntyp>0 ) then
             a=radshl(ishell)
             pref=dble(natshl(ishell))/a*sqrt(pi)
!DEBUG???             pref=dble(natshl(ishell,iat))/a/sqrt(pi)
!????             pref=dble(natshl(ishell,iat))/a*(two*pi)
!C********************************************************************
!C    Contribution from neighbor-shell site charge density is
!C      (2*Pi)/sqrt(4*Pi)*1/r*n/a*Integral{z=[abs(r-a),r+a] | z*rho(z)}
!C      ( 2*Pi - integral over phi-angle, 1/sqrt(4*Pi)=Y_{l=0,m=0} )
!C================================================================
             do nkn=1,komp(ntyp)
              if ( ztot(nkn,ntyp)<=zero ) cycle
               do ir=2,imax(nki,ityp)
                 rpa=max(r_hsk(ir,nki,ityp)+a,rmax(nkn,ntyp))
                 rma = abs(r_hsk(ir,nki,ityp)-a)
                 if ( rma < rmax(nkn,ntyp) ) then
                   f1 = ylag(rpa,r_hsk(1:,nkn,ntyp),                    &
     &                  rhoint(1:,nkn,ntyp),0,3,imax(nkn,ntyp),ix)
                   rhohsk0(ir,nki,ityp) = rhohsk0(ir,nki,ityp) +        &
     &                        atcon(nkn,ntyp)*pref*f1/r_hsk(ir,nki,ityp)
                  if ( rma >= r_hsk(1,nkn,ntyp) ) then
                    f1 = ylag(rma,r_hsk(1:,nkn,ntyp),                   &
     &                   rhoint(1:,nkn,ntyp),0,3,imax(nkn,ntyp),ix)
                    rhohsk0(ir,nki,ityp) = rhohsk0(ir,nki,ityp) -       &
     &                        atcon(nkn,ntyp)*pref*f1/r_hsk(ir,nki,ityp)
                  end if
                 end if
               enddo             ! End of ir loop
             enddo              ! End of nkn loop
             end if             ! End of ntyp
           enddo               ! End of ishell loop
       enddo                 ! End of nki loop
       return
       end subroutine rho_latt_site

       end module vp_str

