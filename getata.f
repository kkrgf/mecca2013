!BOP
!!ROUTINE: get0ata
!!INTERFACE:
      subroutine get0ata(atcon,tab,tcpa,kkrsz,komp,nbasis,              &
     &                  iprint,istop)
!!DESCRIPTION:
! gets the ATA starting guess for CPA interation process,
! calculates concentration average of {\tt tab} and puts in {\tt tcpa}
!
!!USES:
      use mtrx_interface

!!DO_NOT_PRINT
      implicit none
!!DO_NOT_PRINT

!!ARGUMENTS:
      real(8), intent(in) :: atcon(:,:)       ! (ipcomp,ipsublat)
      complex(8), intent(in) :: tab(:,:,:,:)  ! (ipkkr,ipkkr,ipcomp,ipsublat)
      complex(8), intent(out) :: tcpa(:,:,:)  ! (ipkkr,ipkkr,ipsublat)
      integer, intent(in) :: kkrsz,nbasis,komp(nbasis),iprint
      character(10), intent(in) :: istop
!EOP
!
!BOC
!c
      integer :: ic,nsub
      character(*), parameter :: sname='get0ata'
!c
      tcpa = 0
!c
      if(iprint.ge.4) then
         do nsub=1,nbasis
           do ic=1,komp(nsub)
             write(6,'('' getata:: tab (ATA) :: nsub='',i3)') nsub
             call wrtmtx(tab(:,:,ic,nsub),kkrsz,istop)
           enddo
         enddo
      endif
      do nsub=1,nbasis
         do ic=1,komp(nsub)
           tcpa(1:kkrsz,1:kkrsz,nsub) = tcpa(1:kkrsz,1:kkrsz,nsub) +    &
     &                       atcon(ic,nsub)*tab(1:kkrsz,1:kkrsz,ic,nsub)
         enddo
      enddo
!c
      if(iprint.ge.4) then
         do nsub=1,nbasis
            write(6,'('' getata:: tcpa (ATA) :: nsub='',i3)') nsub
            call wrtmtx(tcpa(:,:,nsub),kkrsz,istop)
         enddo
      endif

!c     ===============================================================
      if(istop.eq.sname) then
         call fstop(sname)
      endif
!c
      return
!EOC
      end subroutine get0ata
