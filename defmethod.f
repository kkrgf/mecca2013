!BOP
!!ROUTINE: DefMethod
!!INTERFACE:
      subroutine DefMethod(iflag,imethod,isparse)
!!DESCRIPTION:
! sets, prints or gets information about methods used in KKR 
!
!EOP
!
!BOC
      implicit none
      integer iflag,imethod,isparse

      character*10 sname
      parameter (sname='DefMethod')
      integer :: method = 0, msparse = 0
      save method,msparse

      if(iflag.le.0) then
       method = imethod
       msparse = isparse
      else
       imethod = method
       isparse = msparse
      end if

!     ! turn off large scale calc.
!      if( imethod.ne.0 .or. isparse.ne.0 ) then
!        call fstop('imethod!=0,isparse!=0 use depracated')
!      end if
!      return
!      ! end

      if(iflag.eq.0) then
       if(method.eq.0) then
        write(6,*) ' METHOD = 0  << Standard K-space KKR >>'
       else if(method.eq.1) then
        write(6,*) ' METHOD = 1  << Screened KKR, Mixed Rep. 1 >>'
        write(6,*)                                                      &
     &  '   (Real-Space for Gref, K-space for G  -- for any energy) '
!!!
!        write(6,*)
!        write(6,*) '  NOT IMPLEMENTED .............................'
!        call fstop(sname//' :: DEBUG')
!!!
       else if(method.eq.2) then
        write(6,*) ' METHOD = 2  << Screened KKR, Mixed Rep. 2 >>'
        write(6,*)                                                      &
     &  '   (Real-Space for Gref, K-space for G -- near the Real axis;'
        write(6,*)                                                      &
     &  '   Real-Space for both Gref and G (scr. LSMS) - otherwise)   '
       else if(method.eq.3) then
        write(6,*) ' METHOD = 3  << Screened KKR, RS-Cluster(LSMS) >> '
        write(6,*)                                                      &
     &  '    (Real-Space for both Gref and G  -- for any energy)    '
       else if(method.eq.4) then
        write(6,*) ' METHOD = 4  << Screened KKR, Mixed Rep. 3 >> '
        write(6,*)                                                      &
     &  '   (Real-Space for Gref, K-space for G - near the Real axis;'
        write(6,*)                                                      &
     &  '          sparse standard K-space -- otherwise)           '
!!!
        write(6,*)
        write(6,*) '  NOT IMPLEMENTED .............................'
        call fstop(sname//' :: DEBUG')
!!!
       else if(method.lt.0) then
        write(6,*) ' METHOD=',method,'  <<Screened KKR in K-space>> '
!!!
        write(6,*)
        write(6,*) '  NOT IMPLEMENTED .............................'
        call fstop(sname//' :: DEBUG')
!!!
        if(method.eq.-1) then
          write(6,*)
        else if(method.eq.-2) then
         write(6,*) '    + SuperLU inversion                        '
        else if(method.eq.-3) then
         write(6,*) '    + MAR+TFQMR inversion                      '
        else
         go to 1
        end if
       else
        go to 1
       end if
!c
       if(msparse.ne.0) then
        write(6,*) '  Sparse matrix technique used . ............... '
!c
!c  parameter <prlim> (see invparams module) defines which method
!c  (RS- or KS-inversion) is used;
!c    if prlim=0         --> iterative method only
!c    if prlim=+Very_Big --> SuperLU only
!c
        if(msparse.eq.1) then
         write(6,*)                                                     &
     &  '  MSPARSE=1      -- JCB preconditioner/noprecond or SuperLU '
        else if(msparse.eq.2) then
         write(6,*)                                                     &
     &  '  MSPARSE=2      -- ILU/JCB preconditioner or SuperLU       '
        else if(msparse.eq.3) then
         write(6,*)                                                     &
     &  '  MSPARSE=3      -- CLS/JCB preconditioner  or SuperLU      '
        else
         go to 1
        end if
       end if

!!!
       if(method.lt.0) go to 1
!!!

       return
1      continue
       write(6,*)
       write(6,*) '   METHOD=',method
       write(6,*) '   MSPARSE=',msparse
       call fstop(sname//':: not implemented method')
      end if

      return
!EOC
      end subroutine DefMethod
