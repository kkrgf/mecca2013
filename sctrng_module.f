!BOP
!!MODULE: sctrng
!!INTERFACE:
      module sctrng
!!DESCRIPTION:
! to solve multi-scattering (in general, with CPA) problem
!
!!USES:
      use mecca_constants
      use mecca_types, only : RunState
      use mecca_run, only : getMS
      use mtrx_interface, only : mmul,madd,wrtmtx
!DELETE      use pressure, only : Pdata

!!DO_NOT_PRINT
      private
!!DO_NOT_PRINT

!!PUBLIC MEMBER FUNCTIONS:
      public getSSgf,getMSgf,gf2dos,scalar,setSStype,solveSS,solve1AS
      public getRelSSgf,diracSS
      public ss_type
!!PRIVATE MEMBER FUNCTIONS:
! subroutine setSStype
! subroutine getSSgf
! subroutine getMSgf
! subroutine gf2dos
! subroutine srms_green
! subroutine solveSS
! subroutine solve1AS
! subroutine printW
! subroutine scalar
!!PUBLIC DATA MEMBERS:
      public :: IS_ss,ASA_ss,MIX_ss
!DEBUGPRINT
      public printW
!DEBUGPRINT
!!REVISION HISTORY:
! Initial Version - A.S. - 2013
!EOP
!
!BOC
!      real(8), parameter :: m_e = one/two   ! electron (rest) mass
      integer, parameter :: IS_ss = 0, ASA_ss = 1, MIX_ss = 3
      integer, protected :: ss_type = ASA_ss  ! ASA(MT,if mtasa=0)-single-scattering
!      integer, save :: ss_type = IS_ss   ! IS-single-scattering
      real(8), parameter :: sctr_tol=epsilon(one)
      complex(8), parameter :: sqrtm1=(zero,one)

      real(8), parameter :: cphot=two*inv_fine_struct_const ! 274.072d0
      real*8, public, save :: etamin = 0.1d0

      logical, save :: atom_ss=.false.

!      real(8), external :: poly

      integer, parameter :: l30 = 30                ! starting l to check if 2nd rescale is needed
      integer, parameter :: sk(2)=[1,-1]  ! k/abs(k)

      real(8), parameter :: SR=-1.d0

!DELETE      type(Pdata) :: p_data

!EOC
      contains

!BOP
!!IROUTINE: setSStype
!!INTERFACE:
      subroutine setSStype( jSS )
!!DESCRIPTION:
! to set up single-scattering control flag

!!DO_NOT_PRINT
      implicit none
!!DO_NOT_PRINT

!!ARGUMENTS:
      integer, intent(in) :: jSS
!!REVISION HISTORY:
! Initial Version - A.S. - 2013
!EOP
!
!BOC
      if ( jSS<0 ) then
          ! default
      else
        if ( jSS == IS_ss ) then
         ss_type = IS_ss
        else if ( jSS == ASA_ss ) then
         ss_type = ASA_ss
        else
         ss_type = MIX_ss
        end if
      end if
      return
!EOC
      end subroutine setSStype
!
!BOP
!!IROUTINE: getSSgf
!!INTERFACE:
      subroutine getSSgf(energy,lmax,L_limit,ispin,nrel,ndcomp,nbasis,  &
     &                   alat,sublat,iprint,                            &
     &                   green,tab,cotdl,almat,zlab,no_gf)
!!DESCRIPTION:
!     to find single-scattering solution (t-matrix and GF-contributions)
!
!!USES:
      use mecca_constants, only : lminSS
      use mecca_types, only : Sublattice
      use gfncts_interface

!!DO_NOT_PRINT
      implicit none
!!DO_NOT_PRINT

!!ARGUMENTS:
      complex(8), intent(in) :: energy
      integer, intent(in) :: lmax,L_limit,ispin,nrel,ndcomp,nbasis
      real(8), intent(in) :: alat
      type(Sublattice), intent(in) :: sublat(nbasis)
      integer, intent(in) :: iprint

      complex(8), intent(inout) :: green(:,:,:) ! (iprpts,ipcomp,nbasis)
!      complex(8), intent(out) :: tab((lmax+1)**2,(lmax+1)**2,           &
!     &                                                ndcomp,nbasis)
      complex(8), intent(out) :: tab(:,:,:,:)
      complex(8), intent(out), optional ::                              &
     &                              cotdl(0:lmax,ndcomp,nbasis)         &
     &                             ,almat(0:lmax,ndcomp,nbasis)         &
     &              ,zlab(1:size(green,1),0:lmax,ndcomp,nbasis)
      integer, intent(in), optional :: no_gf    ! no change in green  if ( present(gf) )
!      integer, intent(in) :: mtasa
!!REVISION HISTORY:
! Initial Version - A.S. - 2013
!EOP
!
!BOC
      character(10), parameter :: sname='getSSgf'

      integer, external :: indRmesh

! four components of scalar-relat. solution
      complex(8), allocatable :: zlr(:,:)    ! regular
      complex(8), allocatable :: jlr(:,:)    ! irregular
      complex(8), allocatable :: fzlr(:,:)   ! regular
      complex(8), allocatable :: fjlr(:,:)   ! irregular
!
      complex(8) :: alpha(0:L_limit)
      complex(8) :: cotdl_(0:L_limit)
      complex(8) :: tmatl(0:L_limit)
      complex(8) :: pmom

      integer :: l_big,l_shft,l,lm,m,ndrpts,nrelv,nsub,nk,ir,j,js
      integer :: st_env

      real(8) :: h
      real(8) :: xr(size(green,1))
      real(8) :: rr(size(green,1))
      real(8) :: vr(size(green,1))
      real(8) :: rIS,rScat
      real(8) :: vi,rm_es
      real(8) :: clight
      real(8), parameter :: cphot = two*inv_fine_struct_const
      complex(8) :: ztmp(size(rr)),zj(size(rr)),zz(size(rr))
      complex(8) :: gg_factor(size(rr))
      complex(8) :: c2inv,eoc2p1,cll1

      call gEnvVar('ENV_ATOMSS',.false.,st_env)
      if ( st_env==1 ) then
       atom_ss = .true.
      else
       atom_ss = .false.
      end if

      allocate(zlr(1:size(green,1),0:L_limit))
      allocate(jlr(1:size(green,1),0:L_limit))
      allocate(fzlr(1:size(green,1),0:L_limit))
      allocate(fjlr(1:size(green,1),0:L_limit))

      clight = cphot
      nrelv = 0
      if (nrel > 1 ) then
       nrelv = nrel
       clight = cphot*ten**nrelv
      end if

!c
!c Because t_inv= i*Kappa + Kappa*cotdel, i*Kappa term must
!c cancel i*Kappa piece of g(k).
!c
      pmom = sqrt(2*m_e*energy*(one + energy/(2*m_e*clight**2)))
      pmom = pmom*sign(1.d0,aimag(pmom))
!c
!c     ****************************************************************
!c     set up t-matrix, ZZ, ZJ for each component and  sub-lattice
!c     ****************************************************************
!c
      if(iprint.ge.1) then
       write(6,'('' getSSgf:  nrelv,lmax,L_limit='',3i5)') nrelv,lmax,  &
     &                                                          L_limit
       write(6,'('' getSSgf: energy,pmom='',4d12.4)') energy,pmom
       write(6,'('' getSSgf: size(tab)='',4i5)') (size(tab,nk),nk=1,3)
      endif

      do nsub=1,nbasis
         do nk=1,sublat(nsub)%ncomp
               call g_meshv(sublat(nsub)%compon(nk)%v_rho_box,          &
     &                                                 h,xr,rr,ispin,vr)
            ndrpts = min(sublat(nsub)%compon(nk)%v_rho_box%ndrpts,      &
     &                   size(vr))
            rIS = sublat(nsub)%rIS*alat
            rScat = rr(sublat(nsub)%compon(nk)%v_rho_box%jmt)
            if ( ss_type == IS_ss ) then
!             rScat = sublat(nsub)%rIS*alat
             rScat = sublat(nsub)%compon(nk)%rSph
            else if ( ss_type == ASA_ss ) then
!!             rScat = rr(sublat(nsub)%compon(nk)%v_rho_box%jmt)
             rScat = gRasa(nsub,nk)
            else
!             rScat = sublat(nsub)%rIS*alat
             if ( dreal(energy) < zero ) then
!              rScat = sublat(nsub)%rIS*alat
              rScat = sublat(nsub)%compon(nk)%rSph
             else
!              rScat = rr(sublat(nsub)%compon(nk)%v_rho_box%jmt)
              rScat = gRasa(nsub,nk)
             end if
            end if
!             l_big =min(L_limit,lmax+l_SS+int((abs(energy)*rScat**2))) !  lmax <= l_big <= L_limit
            l_big = bessel_limit(pmom*rr(ndrpts),L_limit)
!DEBUGPRINT
!DEBUGPRINT           write(6,'(2(1x,a,i3),2x,a,2(1x,ES12.5))') 'l_big=',l_big,    &
!DEBUGPRINT     &                             'L_limit=',L_limit,'pmom_srel=',pmom
!DEBUGPRINT
            l_shft = min(l_big,lmax+lminSS)

!c           -------------------------------------------------------
            call solveSS(nrelv,clight,                                  &
     &                  sublat(nsub)%compon(nk)%zID,                    &
     &                  l_big,                                          &
     &                  energy,pmom,                                    &
     &                  zlr,jlr,                                        &
     &                  fzlr,fjlr,                                      &
     &                  cotdl_,alpha,                                   &
     &                  rIS,rScat,rr(1:ndrpts),vr(1:ndrpts),            &
     &                  iprint,l_shft=l_shft)
!c              -------------------------------------------------------

            if ( present(cotdl) ) then
             cotdl(0:lmax,nk,nsub) = cotdl_(0:lmax)
            end if
            if ( present(almat) ) then
             almat(0:lmax,nk,nsub) = alpha(0:lmax)
            end if

            do l=0,l_shft
             if ( cotdl_(l)==czero ) then
              tmatl = czero
              exit
             else if (abs(cotdl_(l)-sqrtm1)<epsilon(1.d0)) then
              tmatl = czero
              exit
             else
              tmatl(l) = cone/(pmom*sqrtm1-pmom*cotdl_(l))
             end if
            end do
            tab(:,:,nk,nsub) = czero
            do l=0,lmax
             lm = l**2+l+1
             do m=-l,l
              tab(lm+m,lm+m,nk,nsub) = tmatl(l)
             end do
            end do
            tmatl(l_shft+1:l_big) = czero
!DEBUGPRINT
            if( iprint .ge. 3 ) then
              call wrtmtx(tab(:,:,nk,nsub),(lmax+1)**2)
              write(6,*)
            endif
!            write(*,*) '^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^'
!            write(6,'(i3,'' en='',2x,2e14.6,2(1x,3(1x,2g15.6)))')       &
!     &                    nsub,energy,cone/pi/cotdl_(0:0),alpha(0:0)
!            m=sublat(nsub)%compon(nk)%v_rho_box%jmt
!            write(*,*) ' cotdl:',cotdl_(0:l_shft)
!            write(*,*) ' tmatl:',tmatl(0:lmax)
!            write(*,*) ' zlr:',zlr(m,0:lmax)
!            write(*,*) ' jlr:',jlr(m,0:lmax)
!            write(*,*) '^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^'
!            write(*,*) ' L_SHFT=',l_shft,' L_BIG=',l_big
!DEBUGPRINT
!c     ==============================================================
!c     calculate the green function for this (energy,species,sub-lat,
!c     spin) :: Spherical part only in this code.....................
!c     --------------------------------------------------------------
            if ( .not.present(no_gf) ) then
! relat.contribution is as in Koelling-Harmon (1977) paper
             call ss_green(energy,clight,rr,vr,l_big,tmatl,             &
     &                     zlr,jlr,fzlr,fjlr,green(:,nk,nsub))
             if(iprint==2) then
              write(6,'(//)')
              write(6,'(i4,3e12.4,'' #i,r(i),green(i) getSSgf'')')      &
     &             (ir,rr(ir),green(ir,nk,nsub),ir=1,ndrpts,1)
             endif
            endif
            if ( present(zlab) ) then
             zlab(1:ndrpts,0:lmax,nk,nsub) = zlr(1:ndrpts,0:lmax)
             zlab(ndrpts+1:,0:lmax,nk,nsub) = czero
            end if
         enddo
      enddo

      deallocate(zlr,jlr,fzlr,fjlr)
!c           -----------------------------------------------------------
      return
!
!EOC
      end subroutine getSSgf
!
!
      integer function bessel_limit(pr,lmx)
      implicit none
      complex(8), intent(in) :: pr
      integer, intent(in) :: lmx
      integer :: l,li
      real(8) :: x
      complex(8) :: z
      integer, parameter :: ldflt=75,ls=5
      complex(8) :: res_j(ldflt+1),res_y(ldflt+1)

      integer, parameter :: DD = selected_real_kind(18)
      real(kind=DD), parameter :: big = HUGE(1.d0)**0.25d0
      real(kind=DD) :: dd_j,dd_y,dd_abs

      bessel_limit = min(lmx,ldflt)
      if ( lmx<ls ) return
      li = min(lmx,ldflt)+1
      x = abs(pr)
      z = dcmplx(abs(pr),0.d0)
      call bessjy(li,z,res_j,res_y)
      do l=ls+1,li
       dd_j = dreal(res_j(l))
       dd_y = dreal(res_y(l))
       dd_abs = sqrt(dd_j**2+dd_y**2)
!       if ( abs(res_y(l)) > big ) then
       if ( dd_abs >= big ) then
        bessel_limit = l-1
        exit
       end if
      end do
      end function bessel_limit
!
!
      subroutine ss_green(energy,clight,rr,vr,l_limit,tmatl,            &
     &                     zlr,jlr,fzlr,fjlr,green)

!c     ==============================================================
!c     calculate the green function for this (energy,species,sub-lat,
!c     spin) :: Spherical part only in this code.....................
!c     --------------------------------------------------------------

       implicit none
       complex(8), intent(in) :: energy
       real(8), intent(in) :: clight
       real(8), intent(in) :: rr(:),vr(:)
       integer, intent(in) :: l_limit
       complex(8), intent(in) :: tmatl(0:)
       complex(8), intent(in) :: zlr(:,0:),jlr(:,0:)
       complex(8), intent(in) :: fzlr(:,0:),fjlr(:,0:)
       complex(8), intent(out) :: green(:)
!
       real(8) :: c2inv
       complex(8) :: eoc2p1,cll1
       complex(8) :: ztmp(size(rr)),tmpgrn(size(rr))
       complex(8) :: zj(size(rr)),zz(size(rr)),gg_factor(size(rr))
       integer :: l,lm,nd

       nd = min(size(rr),size(vr))
       green(1:) = czero         ! as in Koelling-Harmon (1977) paper
       c2inv = cone/clight**2
       eoc2p1 = 2*m_e + energy*c2inv
       ztmp(1:nd) = rr(1:nd)*eoc2p1 - vr(1:nd)*c2inv
       ztmp(1:nd) = cone / (ztmp(1:nd)*ztmp(1:nd))
       do l=0,l_limit
        cll1 = (l*(l+1))/(2*m_e)**2*c2inv  ! /clight**2
        gg_factor(1:nd) = cll1 * ztmp(1:nd)
        if ( tmatl(l)==czero ) then
          zj(1:nd) = -zlr(1:nd,l)*jlr(1:nd,l)
          zj(1:nd) = zj(1:nd) + zj(1:nd) * gg_factor(1:nd)
          tmpgrn(1:nd) = zj(1:nd) - fzlr(1:nd,l) * fjlr(1:nd,l)*c2inv
        else
          zz(1:nd) = zlr(1:nd,l) * (tmatl(l)*zlr(1:nd,l) - jlr(1:nd,l))
          zz(1:nd) = zz(1:nd) + zz(1:nd) * gg_factor(1:nd)
          tmpgrn(1:nd) = zz(1:nd) + fzlr(1:nd,l) *                      &
     &            (tmatl(l)*fzlr(1:nd,l) - fjlr(1:nd,l))*c2inv
        end if
        lm = 2*l+1
        green(1:nd) = green(1:nd) + lm*tmpgrn(1:nd)
       enddo
!
!*       tmpgrn = czero
!*       do l=l_s+1,l_limit    ! G0 contribution
!*        cll1 = (l*(l+1))/(2*m_e)**2*c2inv  ! /clight**2
!*        gg_factor(1:nd) = cll1 * ztmp(1:nd)
!*        zj(1:nd) = -zlr(1:nd,l)*jlr(1:nd,l)
!*        zj(1:nd) = zj(1:nd) + zj(1:nd) * gg_factor(1:nd)
!*        tmpgrn(1:nd) = zj(1:nd) + ( -fzlr(1:nd,l) * fjlr(1:nd,l)*c2inv )
!*        lm = 2*l+1
!*        green(1:nd) = green(1:nd) + lm*tmpgrn(1:nd)
!*       enddo
!             if(iprint.ge.2) then
!              write(6,'(//)')
!              write(6,'(i4,3e12.4,'' #i,r(i),green(i) getSSgf'')')      &
!     &             (ir,rr(ir),green(ir,nk,nsub),ir=1,nd,1)
!             endif
       return
      end subroutine ss_green

!
      function findmax(x,f)
      implicit none
      real(8) :: findmax
      real(8), intent(in) :: x(3),f(3)
      real(8) :: d1,d2,v
!
!      f(x) is approximated by A*(x-x0)**2+B
!      f(1) < f(2)
!
      if ( f(1)>=f(2) .or. f(2)<f(3) ) then
       write(6,*) ' DEBUG x=',sngl(x)
       write(6,*) ' DEBUG f=',sngl(f)
       call p_fstop(' UNEXPECTED INPUT DATA IN findmax FUNCTION')
      end if
      v = (f(3)-f(2))/(f(2)-f(1))
      d1 = x(2)-x(1)
      d2 = x(3)-x(2)
      findmax = x(2) + (d2*d2+d1*d1*v ) / (d2-d1*v)
      return
      end function findmax
!
!BOP
!!IROUTINE: getMSgf
!!INTERFACE:
      subroutine getMSgf(tab,ispin,nrel,                                &
     &                  lmax,kkrsz,nbasis,numbsub,                      &
     &                  alat,aij,itype,natom,                           &
     &                  if0,mapstr,mappnt,ndimbas,mapij,                &
     &                  komp,atcon,                                     &
     &                  xknlat,Rksp0,nkns,                              &
     &                  qmesh,ndimq,nmesh,nqpt,                         &
     &                  lwght,lrot,                                     &
     &                  ngrp,kptgrp,kptset,ndrot,kptindx,               &
     &                  sublat,r_ref,                                   &
     &                  rws,                                            &
     &                  energy,                                         &
     &                  itmax,rns,nrns,ndimrs,                          &
     &                  np2r,ndimnp,numbrs,naij,                        &
     &                      rot,dop,nop,                                &
     &                  hplnm,ndimrhp,ndimlhp,                          &
     &                  irecdlm,                                        &
     &                  green,                                          &
     &                  eneta,enlim,                                    &
     &                  conk,conr,                                      &
     &               rslatt,kslatt,Rnncut,Rsmall,mapsprs,               &
     &                  idosplot,                                       &
     &                  ksintsch,                                       &
     &                  iprint,istop,tcpa0)
!!DESCRIPTION:
!     this subroutine solves the cpa equations using the brillouin-zone
!     integration method for calculating Green's function and related values
!     full t-matrix storage code
!
!....mar'92...gms...
!
!!USES:
      use mecca_types, only : Sublattice
      use raymethod, only: ksrayint
      use gfncts_interface
      use mecca_interface, only : get0ata,intgrtau
      use mtrx_interface

!!DO_NOT_PRINT
      implicit none
!!DO_NOT_PRINT

!!ARGUMENTS:
      integer, intent(in) :: ispin,nrel
      integer, intent(in) :: lmax,kkrsz,nbasis,numbsub(nbasis)
      real(8), intent(in) :: alat
      integer, intent(in) :: naij
      real(8), intent(in) :: aij(3,naij)
      integer, intent(in) :: natom,itype(natom)
      integer, intent(in) :: if0(48,natom)
      integer, intent(in) :: ndimbas
      integer, intent(in) :: mapstr(ndimbas,natom),mappnt(ndimbas,natom)
      integer, intent(in) :: mapij(2,naij)
      integer, intent(in) :: komp(nbasis)
      real(8), intent(in) :: atcon(:,:)       ! (ipcomp,nbasis)
      complex(8), intent(in) :: tab(:,:,:,:)  ! (kkrsz,kkrsz,size(atcon,1),nbasis)
      real(8), intent(in) :: xknlat(:),Rksp0
      integer, intent(in) :: nmesh,nkns(nmesh)
      integer, intent(in) :: ndimq
      real(8), intent(in) :: qmesh(3,ndimq,nmesh)
      integer, intent(in) :: nqpt(nmesh)
      integer, intent(in) :: lwght(ndimq,nmesh),lrot(ndimq,nmesh)
      integer, intent(in) :: ngrp(nmesh),kptindx(ndimq,nmesh)
      integer, intent(in) :: ndrot,kptgrp(ndrot+1,nmesh)
      integer, intent(in) :: kptset(ndrot,nmesh)
      type(Sublattice), intent(in) :: sublat(nbasis)
      real(8), intent(in) :: r_ref(:)   ! reference (screening) radius
      real(8), intent(in) :: rws(nbasis)
      complex(8), intent(in) :: energy
      integer, intent(in) :: itmax
      integer, intent(in) :: ndimrs,nrns
      real(8), intent(in) :: rns(ndimrs,4)
      integer, intent(in) :: ndimnp
      integer, intent(in) :: np2r(ndimnp,naij),numbrs(naij)
      real(8), intent(in) :: rot(49,3,3)
      integer, intent(in) :: nop
      complex(8), intent(in) :: dop(kkrsz,kkrsz,nop)
      integer, intent(in) :: ndimrhp,ndimlhp
      complex(8), intent(in) :: hplnm(ndimrhp,ndimlhp,*)
      integer, intent(in) :: irecdlm              ! 0 - Ewald summ, 1 - Hankel summ
!----------------------------------------------------------------------
      complex(8), intent(inout) :: green(:,:,:) ! (iprpts,ipcomp,nbasis)
!----------------------------------------------------------------------
      real(8), intent(in) :: eneta, enlim(2,nmesh-1)
      real(8), intent(in) :: conk
      complex(8), intent(in) :: conr((2*lmax+1)**2)
      real(8), intent(in) :: rslatt(3,3)           ! Alat*rslatt(1..3,*)/(2*pi) -- latt.vect.
      real(8), intent(in) :: kslatt(3,3)
      real(8), intent(in) :: Rnncut,Rsmall
      integer, intent(in) :: mapsprs(:,:)
      integer, intent(in) :: idosplot
      integer, intent(in) :: ksintsch ! k-space integration scheme
                                      ! 1 = monkhorst-pack
                                      ! 2 = ray method
      integer, intent(in) :: iprint
      character(10), intent(in) :: istop
      complex(8), intent(out), target, optional :: tcpa0(:,:,:)
!!PRIVATE MEMBER FUNCTIONS:
! subroutine calc_gf
!!REVISION HISTORY:
! Adapted - A.S. - 2013
!EOP
!
!BOC
      interface
       subroutine mcpait1(p,tau00,tcpa,tab,                             &
     &                  kkrsz,                                          &
     &                  nbasis,                                         &
     &                  atcon,                                          &
     &                  komp,numbsub,                                   &
     &                  iconverge,nitcpa,                               &
     &                  iprint,istop)
       use mecca_constants
       use mtrx_interface
       implicit none
       complex(8), intent(in) :: p
       complex(8), intent(in) :: tau00(:,:,:)
       complex(8), intent(inout) ::  tcpa(:,:,:)
       complex(8), intent(in) ::  tab(:,:,:,:)
       integer, intent(in) :: kkrsz,nbasis
       real(8), intent(in) ::  atcon(:,:)
       integer, intent(in) :: komp(:),numbsub(:)
       integer, intent(out) :: iconverge
       integer, intent(in) :: nitcpa,iprint
       character(10), intent(in) :: istop
       end subroutine mcpait1
      end interface
!
!c     ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
      character(10), parameter :: sname='getMSgf'
!c     ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!c     FOR STRUCTURE CONSTANTS: use Ewald and/or real-space methods
!c              real and imaginery energy switch, see below
!c     ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!c     switch to use EWALD within "eresw" D.U. of Real E axis and
!c       real-space method beyond "eresw" D.U. of Real E axis.
!c     ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!      real(8), parameter :: eresw=-0.5d0,eimsw=0.5d0  !  see mecca_constants
!c     ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!c
!c
!      integer      ivpoint(ipsublat)
!      integer      ikpoint(ipcomp,ipsublat)
      integer      nitcpa
!c
      real*8       r2tmp
      complex*16   powe((2*lmax+1)**2)

      complex*16   dqint1(nrns,2*lmax+1)
      complex*16   dqtmp(0:2*lmax)

      complex*16   tcpatmp(size(tab,1),size(tab,1),natom)
      integer      itype1(natom),iorig(nbasis)

      complex(8), pointer :: tcpa(:,:,:)
!      complex*16   tcpa(size(tab,1),size(tab,1),nbasis)
      complex*16   tau00(size(tab,1),size(tab,1),nbasis)
      complex*16   green00(size(tab,1),size(tab,1),nbasis)
      complex*16   prel
      complex*16   edu
      complex*16   pdu
      complex*16   xpr
      complex*16   d00
      complex*16   eoeta
      complex*16   lloydms

      integer imethod,isparse

      real(8) :: clight
      real(8) :: dutory,rytodu,rsp
      real(8) :: time1,time2
      integer :: i,iatom,iconverge,ict,imesh,iprint1,iprint2
      integer :: iswzj,l1,n,ndkkr,nrelv,nsub

      logical, save :: isdis = .false.
      logical :: dos_calc      ! dos only

      real*8 R2ksp
      real*8       eta0
      integer      n1,n2
      integer :: st_env

      real(8), external :: gLattConst

      ndkkr = size(tab,1)

      if ( kkrsz > ndkkr ) then
        write(6,'(a,i3,a,i3,a,a)') ' ERROR :: kkrsz=',kkrsz             &
     &   ,' cannot be larger than ndkkr=',ndkkr,'; to change ndkkr,'    &
     &   ,' code re-compilation with a new lmax-limit is required.'
        call fstop(sname// ' :: kkrsz is too big')
      end if
      call timel(time2)

      clight = cphot
      nrelv = 0
      if (nrel > 1 ) then
       nrelv = nrel
       clight = cphot*ten**nrelv
      end if

!c     ================================================================
!c     Note:
!c             In scalar-relativistic case p=sqrt[e(1+e/cphot**2)]
!
!c     ================================================================
      rytodu = gLattConst(1)/(two*pi)
      dutory = one / rytodu    ! (two*pi)/alat

!      tcpa => null()

      if ( present(tcpa0) ) then
        if ( size(tcpa0,1) >= kkrsz .and.                               &
     &       size(tcpa0,2) >= kkrsz .and.                               &
     &       size(tcpa0,3) >= nbasis ) then
         tcpa => tcpa0
        else
         write(*,*) ' kkrsz=',kkrsz,' nbasis=',nbasis
         write(*,*) ' SIZE(tcpa0)=',(size(tcpa0,i),i=1,3)
         call p_fstop(sname//                                           &
     &               ' :: insuffiecient memory allocation for tcpa0')
        end if
      else
        allocate(tcpa(size(tab,1),size(tab,1),nbasis))
      end if

!      do_ray_int = ksintsch.ne.ksint_scf_sch
      dos_calc = .false.
      if ( idosplot .ne. 0 ) dos_calc =.true.

!c     calculate thing s that depend only on energy
!c     Im(sqrt(energy)) must be .GE. zero
!c
! pnrel
!      prel  = sqrt(2*m_e*energy)
!      prel  = prel*sign(one,aimag(prel))
!
!      prel  = prel*sqrt(cone+energy/(2*m_e*clight**2))
      prel  = sqrt(2*m_e*energy*(cone+energy/(2*m_e*clight**2)))
      prel  = prel*sign(one,aimag(prel))

!c     energy and momentum in dimensionless units (DU).

      edu =energy*rytodu**2
      pdu = prel*rytodu
!c
!c     switch for getting irregular part of G(E) at complex E, i.e. ZJ
      iswzj=1
!c     --------------------------------------------------------------
      if(iprint.ge.1) then
       write(6,'(a10,'':  edu,pdu='',1x,''('',f9.5,'','',f9.5,'' ),'',  &
     &                              1x,''('',f9.5,'','',f9.5,'' )'')')  &
     &                      sname,edu,pdu
      endif
!c     --------------------------------------------------------------

      call pickmesh(edu,enlim,nmesh,imesh)
      call DefMethod(1,imethod,isparse)
      if(imethod.eq.0.or.imethod.eq.4.or.irecdlm.ne.0) then    ! For K-space calculations

       eta0 = max(etamin,abs(edu)/eneta)
!c                                    to be sure in accuracy of D00
       if( irecdlm.eq.0 ) then
         R2ksp = max(zero,eta0*Rksp0*Rksp0+dreal(edu))
         ! rooeml --> sqrt(e^-l)
         call rooeml(pdu,powe,lmax,iprint)
         call d003(edu,eta0,d00)
         eoeta=exp( edu/eta0 ) * conk
!c
!c ==================================================================
!c  calculate the real-space integral used in Ewald technique.
!c  This is energy dependent, but k-space independent.
!c  DO once for each energy and store results in upper triangular
!c  form. INTFAC calculated the integral and returns in DQINT.
!c ==================================================================
!c
       else
         R2ksp = zero
         eoeta = zero
         d00 = zero
!         removed for debugging
!         if(aimag(edu).lt.0.d0)
!     *   call fstop(sname//': Im(E)<0')
!c
!c ==================================================================
!c  calculate the hankel fct used in real-space calculation of
!c   structure constants out in complex plane.
!c  This is energy dependent, but k-space independent.
!c  DO once for each energy and store results in upper triangular
!c  form. HANKEL calculated the fct and returns in DQINT.
!c ==================================================================
!c
!c  do not start a hankel with zero vector length (ir=1)...blows up!
!c  only true for diagonal block, of course.
!c
       endif

!c ==================================================================
!     Both hankel and real-space ewald integral passed as DQINT.
          rsp = -two
          do i=1,nrns
!c          rtmp(1)= rns(i,1)
!c          rtmp(2)= rns(i,2)
!c          rtmp(3)= rns(i,3)
           r2tmp=   rns(i,4)
           if( abs(r2tmp-rsp) .ge. 1.0d-6 ) then
             rsp = r2tmp
             if(irecdlm.eq.0) then
                call intfac(rsp,edu,eta0,2*lmax,dqtmp)
             else
                if(r2tmp.le.1.0d-6) then
                 do l1=0,2*lmax
                  dqtmp(l1) = czero
                 enddo
                else
                 xpr=pdu*sqrt(rsp)
                 call hankel(xpr,dqtmp,2*lmax)
                end if
             end if
             do l1=0,2*lmax
                dqint1(i,l1+1)=dqtmp(l1)
             enddo
           else
             if ( i==1 ) then
              call fstop(sname//': MEMORY BUG???')
             end if
             do l1= 1,2*lmax+1
              dqint1(i,l1)=dqint1(i-1,l1)
             enddo
           endif
          enddo

      else
         if(iprint.ge.0) then
          write(6,1002) Rsmall/(two*pi),Rnncut/(two*pi),                &
     &                  imesh,nqpt(imesh)
1002      format(' REAL SPACE *****    Rsmall=',f6.3,                   &
     &          '  Rnncut=',f6.3,' NQPT(',i1,')=',i7)
         endif
      endif

!c     construct the average t-matrix..................................
!c
      call get0ata(atcon,tab,tcpa,kkrsz,komp,nbasis,iprint,istop)
!c     -----------------------------------------------------------------

      do nsub=1,nbasis
        do iatom=1,natom
         if(itype(iatom).eq.nsub) go to 10
        end do
        call fstop(sname// ' :: You have wrong ITYPE')
10      iorig(nsub) = iatom

!c!!!
!c  it is supposed below  that "iorig(nsub)" -- number of 1st site of
!c  type "nsub" for isite=1,natom
!c!!!
      end do

      do iatom=1,natom
       itype1(iatom) = iatom
      end do

!C!----------------------------------------------------------------------
!C!  Do not change the array itype1(*) below this line; this array is used
!C!  to treat equivalent sites as inequivalent (when it is needed)
!C!----------------------------------------------------------------------

!c     =================================================================

      iconverge = 0
      nitcpa = 0
      if(itmax.eq.-1 .OR. atom_ss ) then          !  ATA SCF solution

       tau00 = tcpa

      else                          !  CPA SCF

       do ict = 1, max(1,abs(itmax))

         nitcpa=ict
         if(ict.gt.1) then
          iprint1 = iprint-2
          iprint2 = 1000
         else
          iprint1 = iprint
          iprint2 = iprint
         end if

         if(ict.ne.1) then
          if(ngrp(1).gt.1                                               &
     &       .or.lwght(kptindx(kptgrp(1,1),1),1).gt.1                   &
     &      ) then
!c
!c  Symmetry of tcpa-matrix could be destroyed due to numerical errors,
!c  to maintain the proper symmetry for each CPA-iteration
!c
           call symmrot(ngrp(imesh),kptgrp(1,imesh),                    &
     &                  kptset(1,imesh),kptindx(1,imesh),               &
     &                  lwght(1,imesh),lrot(1,imesh),                   &
     &                  tcpa,size(tcpa,1),kkrsz,                        &
     &                  if0,dop,nbasis,                                 &
     &                  iorig)
          end if
         end if
!c
!c  Tcpa-matrices for equivalent sites can differ !!!
!c
         call tcpa2all(natom,nbasis,iorig,itype,numbsub,if0,nop,        &
     &                   kkrsz,ndkkr,dop,tcpa,tcpatmp,iprint2)

         ! is the system disordered?
         isdis = .false.
         do n = 1, nbasis
           if(komp(n)>1) isdis = .true.
         end do
         if( ksintsch == ksint_scf_sch ) then
                             ! BZ integration using special k-pt method
           if(iprint.ge.0.and.ict==1) then
            if( imethod==0 .or. imethod==4) then    ! for K-space calculations
             if( irecdlm.eq.0 ) then
              write(6,1000) eta0,sqrt(R2ksp),imesh,nqpt(imesh)
1000          format(' USING EWALD *****    ETA=',f8.5,                 &
     &          '  Rksp=',f12.5,' NQPT(',i1,')=',i7)
             else
              write(6,1001) nrns
1001          format(' USING HANKEL *****   NRNS=',i7)
             end if
            end if
           end if

           n2 = 3*sum(nkns(1:imesh))
           n1 = n2 - 3*nkns(imesh)+1

           call intgrtau(                                               &
     &               lmax,kkrsz,nbasis,numbsub,atcon,size(atcon,1),komp,&
     &                  rytodu,aij,itype,natom,iorig,itype1,            &
     &                  if0,mapstr,mappnt,ndimbas,mapij,                &
     &                  powe,                                           &
     &                  edu,pdu,                                        &
     &                  irecdlm,                                        &
     &                  rns,ndimrs,                                     &
     &                  np2r,ndimnp,numbrs,naij,                        &
     &                  dqint1,size(dqint1,1),                          &
     &                  hplnm,ndimrhp,ndimlhp,                          &
     &                  tcpatmp(1:kkrsz,1:kkrsz,1:natom),               &
     &                  tau00(1:kkrsz,1:kkrsz,1:nbasis),                &
     &                  dop,nop,d00,eoeta,                              &
     &                  eta0,                                           &
     &                  reshape(xknlat(n1:n2),[nkns(imesh),3]),         &
     &                  nkns(imesh),R2ksp,conr(:),nkns(imesh),          &
     &                  qmesh(1,1,imesh),nqpt(imesh),                   &
     &               lwght(1,imesh),lrot(1,imesh),                      &
     &   ngrp(imesh),kptgrp(1,imesh),kptset(1,imesh),kptindx(1,imesh),  &
     &                  rslatt,Rnncut,Rsmall,mapsprs,                   &
     &                  r_ref,iprint1,istop)
         else

           ! BZ integration using radial wedges in spherical coordinates

           tcpatmp = dutory * tcpatmp
             ! remember that tcpa has units of 1/sqrt(energy)
             ! so we are *really* changing to DU here

!           write(6,*) 'd00=', d00, eoeta


           ! find diagonal blocks of tau = [t^-1 - G0]^-1
           imesh = 1
           n2 = 3*sum(nkns(1:imesh))
           n1 = n2 - 3*nkns(imesh)+1

           call ksrayint( lmax,kkrsz,natom,aij,mapstr(1:natom,1:natom), &
     &                               mappnt(1:natom,1:natom),           &
     &         nbasis,mapij,powe,edu,                                   &
     &         pdu,rns,ndimrs,np2r,ndimnp,numbrs,naij,dqint1,           &
     &  size(dqint1,1),hplnm,ndimrhp,ndimlhp,irecdlm,d00,eoeta,eta0,    &
     &         reshape(xknlat(n1:n2),[nkns(imesh),3]),nkns(imesh),conr, &
     &                 tcpatmp,iorig,itype,kslatt,nop,rot,dop,if0,      &
     &         tau00,lloydms,green00 )

           tau00 = rytodu * tau00
           green00 = rytodu * green00
           tcpatmp = rytodu * tcpatmp

!           write(6,*) 'tau00 = ', tau00(1:kkrsz,1:kkrsz,1)

         end if

         ! if a random alloy then iterate cpa
         if(abs(itmax)>1) then

           if(ict.eq.itmax) then
            iprint1 = max(0,iprint)
           else
            iprint1 = iprint
           end if

           call mcpait1(pdu,tau00,tcpa,tab,                             &
     &                  kkrsz,                                          &
     &                  nbasis,                                         &
     &                  atcon,                                          &
     &                  komp,numbsub,                                   &
     &                  iconverge,nitcpa,                               &
     &                  iprint1,istop)

!c           -----------------------------------------------------------


         else ! no CPA case

            iconverge=0

         endif

         if( iconverge<=0 ) go to 100

       end do

      end if

100   continue

      if ( itmax>0 .or. iprint>=0 ) then
       if( iconverge>0 .and. .not.dos_calc ) then
! EBOT_CALC can be done with itmax = 1

!c        CPA failed to converge.......................................

        write(6,*)
        write(6,*) ' ICONVERGE=',iconverge
        if(float(iconverge)/float(nbasis).gt.0.5.and.itmax.gt.0) then
         write(6,'(a,'':: CPA failed to converge in '',i3,              &
     &          '' iterations, energy='',2es15.6)') sname,nitcpa,energy
         if ( itmax.eq.nitcpa ) then
          if ( nitcpa <= 20 ) then
           write(*,'(a)')                                               &
     &'  Increase max.number of CPA iterations in input file.'
           call fstop(sname//' :: change CPA-parameter in input file')
          else if ( nitcpa==maxcpa ) then
           write(*,'(a)') sname//' WARNING: '                           &
     &//' IF YOU SEE THIS MESSAGE DURING FINAL SCF ITERATION, RESULTS'  &
     &//' MAY BE INACCURATE OR COMPLETELY WRONG.'
           write(*,'(a,'':: CPA failed to converge in '',i3,            &
     &          '' iterations, energy='',2es15.6)') sname,nitcpa,energy
           write(*,'(a)') ' You may want to recalculate with larger'    &
     &//' number of CPA iterations in input.'
          end if
         else if(itmax.gt.0) then
          write(6,*)                                                    &
     &   ' WARNING :: You have no convergence for less then 50% of'//   &
     &   ' sublattices'
         end if
         write(6,*)
        end if
       else if( iconverge.ne.0 .and. itmax > 1 ) then
        if ( iconverge<0 ) then
         if ( nitcpa>4 .or. iprint>0 ) then
         write(6,'(a,'' WARNING :: CPA is unable to converge with'',    &
     &     '' requested tolerance, cpa iteration#'',i3,'', energy='',   &
     &     2es15.6)') sname,nitcpa,energy
         end if
        else
         write(6,'(a,'' WARNING :: CPA failed to converge in '',i3,     &
     &          '' iterations'')') sname,nitcpa
        end if
       end if
      end if

!c     ===========================================================
!c     cpa iteration converged or the case of an ordered system...
!c     write(6,'('' gettau:: cpa converged : calling calc_gf'')')
!c     ===========================================================

!!      call calc_gf(pnrel)

      call calc_gf(prel)       ! no free-space solution here: pnrel-->prel

      if ( present(tcpa0) ) then
        tcpa => null()
      else
        deallocate(tcpa)
        nullify(tcpa)
      end if

      time1 = time2
      call timel(time2)

      if(iprint.ge.0.and.nitcpa.gt.1) then
       write(6,*) sname//' time  ='                                     &
     &           ,real(time2-time1),' NITCPA=',nitcpa
      else if(nitcpa.gt.20.and.iprint.ge.-1) then
       write(6,*) ' Number of CPA iterations is ',nitcpa
      end if

!c           -----------------------------------------------------------
      if(istop.eq.sname) then
        call fstop(sname)
      endif
!c
      return

!EOC
      contains
!
!BOP
!!IROUTINE: calc_gf
!!INTERFACE:
      subroutine calc_gf(p_in)
!!DESCRIPTION:
! {\bv
!     *****************************************************************
!      input:  w/ Z regular and J irrregular sols. to Schrodinger Eq.
!             tau00   (tauc)
!             tcpa    (tc)
!             tab     (t-matrix for atoms a & b)
!             kkrsz   (size of KKR-matrix)
!             komp    (number of components on sublattice)
!             istop   (index of subroutine prog. stops in)
!       output:
!             green   (spherical Green's function)
!     ****************************************************************
! \ev}

!!DO_NOT_PRINT
      implicit none
!!DO_NOT_PRINT

!!ARGUMENTS:
      complex(8), intent(in) :: p_in
!!REVISION HISTORY:
! Initial Version - A.S. - 2013
!!REMARKS:
! private procedure of getMSgf module subroutine
!EOP
!
!BOC
      character sname*10
!c parameter
      real(8), parameter :: onem=-one
!c
      complex(8) :: w1(kkrsz,kkrsz)
      complex(8), target :: w2(kkrsz,kkrsz)
      complex(8) :: w3(kkrsz,kkrsz)
      complex(8) :: w4(kkrsz,kkrsz)

      integer :: i,ic,j,js,nsub,ndrpts
      real(8) :: h,rIS,rScat
      real(8) :: vi,rm_es
      real(8) :: xr(size(green,1))
      real(8) :: rr(size(green,1))
      real(8) :: vr(size(green,1))
      complex(8) :: tmpgreen(size(green,1))
      integer :: jws
      integer, external :: indRmesh
!c parameter
!      complex(8), parameter :: cone=(1.d0,0.d0)
!c
!c     ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
      parameter (sname='calc_gf')
!c     ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!c
      if(iprint.ge.4) then
         write(6,'('' calc_gf:: nbasis,kkrsz'',3i4)')                   &
     &                         nbasis,kkrsz
         do nsub=1,nbasis
           write(6,'('' GETDOS: tau00 '')')
           call wrtmtx(tau00(:,:,nsub),kkrsz)
         enddo
      endif
!c
!c     =================================================================
!c     -----------------------------------------------------------------
!c
!
      if(itmax.eq.-1 .OR. atom_ss ) return       !  ATA SCF solution
!
      do nsub=1,nbasis
         if(iprint.ge.4) then
            write(6,'('' calc_gf::  nsub ::'',i3)') nsub
         endif
!c         errcpa=zero
!c        =============================================================
!c        tcpa => t_{c}(old) = t_{c}...................................
!c        w4   => t_{c}^{-1} = m_c.....................................
!c        -------------------------------------------------------------
         call matinv(tcpa(1:kkrsz,1:kkrsz,nsub),w4(1:kkrsz,1:kkrsz),w3, &
     &                                             kkrsz, iprint,istop)
!c        -------------------------------------------------------------
         if(iprint.ge.4) then
            write(6,'('' calc_gf:: t_c(old) ::'')')
            call wrtmtx(tcpa(:,:,nsub),kkrsz)
            write(6,'('' calc_gf:: m_c(old) ::'')')
            call wrtmtx(w4,kkrsz)
         endif
         do ic=1,komp(nsub)
!c           ==========================================================
!c           w2 => t_a(b)^{-1} = m_a(b)................................
!c           ----------------------------------------------------------
            call matinv(tab(1:kkrsz,1:kkrsz,ic,nsub),                   &
     &                      w2(1:kkrsz,1:kkrsz),w3,kkrsz, iprint,istop)
!c           ----------------------------------------------------------
            if(iprint.ge.4) then
               write(6,'('' calc_gf::  ic ::'',i3)') ic
               write(6,'('' calc_gf:: t_a(b) ::'')')
!c              -------------------------------------------------------
               call wrtmtx(tab(:,:,ic,nsub),kkrsz)
!c              -------------------------------------------------------
               write(6,'('' calc_gf:: m_a(b) ::'')')
!c              -------------------------------------------------------
               call wrtmtx(w2,kkrsz)
!c              -------------------------------------------------------
            endif
!c           ==========================================================
!c           w1 => m_c - m_a(b)........................................
!c           ----------------------------------------------------------
            call madd(w4,onem,w2,w1,kkrsz,iprint)
!c           ----------------------------------------------------------
!c           ==========================================================
!c           w2 => tau_c*[m_c - m_a(b)]^{-1}...........................
!c           ----------------------------------------------------------
            call mmul(tau00(:,:,nsub),w1,w2,kkrsz)
!c           ----------------------------------------------------------
            if(iprint.ge.4) then
               write(6,'('' calc_gf::m_c -  m_a(b) ::'')')
!c              -------------------------------------------------------
               call wrtmtx(w1,kkrsz)
!c              -------------------------------------------------------
               write(6,'('' calc_gf::tau_c*[m_c -  m_a(b)] ::'')')
!c              -------------------------------------------------------
               call wrtmtx(w2,kkrsz)
!c              -------------------------------------------------------
            endif
!c           ==========================================================
!c           w1 => [1-tau_c*[m_c - m_a(b)]]^{-1}] = D^{-1}.............
!c           ----------------------------------------------------------
            w3 = czero
!c           ----------------------------------------------------------
            do i=1,kkrsz
               w3(i,i)=cone
            enddo
!c           ----------------------------------------------------------
            call madd(w3,onem,w2,w1,kkrsz,iprint)
!c           ----------------------------------------------------------
            if(iprint.ge.4) then
               write(6,'('' calc_gf:: D^{-1} ::'')')
!c              -------------------------------------------------------
               call wrtmtx(w1,kkrsz)
!c              -------------------------------------------------------
            endif
!c           ==========================================================
!c           w2 => D ..................................................
!c           ----------------------------------------------------------
            call matinv(w1,w2,w3,kkrsz,iprint,istop)
!c           ----------------------------------------------------------
            if(iprint.ge.4) then
               write(6,'('' calc_gf:: D ::'')')
!c              -------------------------------------------------------
               call wrtmtx(w2,kkrsz)
!c              -------------------------------------------------------
            endif
!c           ==========================================================
!c           w1 => D*tau_c = tau_a(b).................................
!c           ----------------------------------------------------------
            call mmul(w2,tau00(:,:,nsub),w1,kkrsz)
!c           ----------------------------------------------------------
            call g_meshv(sublat(nsub)%compon(ic)%v_rho_box,             &
     &                                                 h,xr,rr,ispin,vr)
            jws = 1+indRmesh(rws(nsub),size(rr),rr)
            rScat = rws(nsub)
            rIS = sublat(nsub)%rIS*alat
            if ( ss_type == IS_ss ) then
!             rScat = sublat(nsub)%rIS*alat
             rScat = sublat(nsub)%compon(ic)%rSph
            else if ( ss_type == ASA_ss ) then
!!             rScat = rr(sublat(nsub)%compon(ic)%v_rho_box%jmt)
             rScat = gRasa(nsub,ic)
            else
!             rScat = sublat(nsub)%rIS*alat
             if ( dreal(energy) < zero ) then
!              rScat = sublat(nsub)%rIS*alat
              rScat = sublat(nsub)%compon(ic)%rSph
             else
!!              rScat = rr(sublat(nsub)%compon(ic)%v_rho_box%jmt)
              rScat = gRasa(nsub,ic)
             end if
            end if

!c           ==========================================================
!c           green_MS =>  ZZ*(tau_a(b)-t_a(b)) + green_SS
!c           ----------------------------------------------------------
            call madd(w1,onem,tab(:,:,ic,nsub),w2,kkrsz,iprint)   ! tau-tab
            ndrpts = min(sublat(nsub)%compon(ic)%v_rho_box%ndrpts,      &
     &                   size(rr))
            call srms_green(tmpgreen(1:ndrpts),w2,kkrsz,                &
     &                    nrelv,clight,                                 &
     &                    sublat(nsub)%compon(ic)%zID,                  &
     &                    lmax,                                         &
     &                    energy,p_in,                                  &
     &                    rIS,rScat,                                    &
     &                    h,jws,rr(1:ndrpts),vr(1:ndrpts),              &
     &                    iprint)
            green(1:ndrpts,ic,nsub) = green(1:ndrpts,ic,nsub) +         &
     &                                tmpgreen(1:ndrpts)
!DEBUG
!            do i=1,ndrpts
!             if ( aimag(green(i,ic,nsub)) > zero )                      &
!     &            green(i,ic,nsub)=czero
!            end do
!DEBUG
         enddo
      enddo
!c     =================================================================
      if(istop.eq.sname) then
        call fstop(sname)
      endif
!c     =================================================================
      return
!
!EOC
      end subroutine calc_gf
!
      end subroutine getMSgf
!
!BOP
!!IROUTINE: gf2dos
!!INTERFACE:
      subroutine gf2dos(ispin,green)
!!DESCRIPTION:
!      to calculate DOS(E) based on GF(r,E)  (after summation over l)
!
!!USES:
      use mecca_interface, only  : asa_integral,ws_integral
      use gfncts_interface, only : g_meshv

!!DO_NOT_PRINT
      implicit none
!!DO_NOT_PRINT

!!ARGUMENTS:
      integer, intent(in) :: ispin
!!REVISION HISTORY:
! Initial Version - A.S. - 2013
!!REMARKS:
! the subroutine is not for calculation of l-dependent DOSes;
! ATTN: WS-integration should be consistent with the one in density-module
!EOP
!
!BOC
!c
      complex(8), intent(in)  :: green(:,:,:) ! (iprpts,ipcomp,nbasis)
      type(RunState), pointer :: mecca_state
      integer :: iend,nbasis,nsub,nk
      real(8) :: re_ws,im_ws,re_mt,im_mt,h
      logical, external :: gNonMT
      real(8) :: tmp_gf(size(green,1))
      real(8) :: rr(size(green,1))
      real(8) :: xr(size(green,1))
      character(2), parameter :: updn(2) = ['up','dn']
!      character(*), parameter :: sname='gf2dos'

      mecca_state => getMS()

      iend = size(green,1)
      nbasis = size(green,3)
      do nsub=1,nbasis
       mecca_state%gf_box%dos(0:,:,nsub,ispin)   = czero
       mecca_state%gf_box%dosck(:,nsub,ispin) = czero
       do nk=1,mecca_state%intfile%sublat(nsub)%ncomp
        call g_meshv(mecca_state%intfile%sublat(nsub)%compon(nk)        &
     &               %v_rho_box,h,xr(:),rr(:))
        tmp_gf(1:iend) = dreal(green(1:iend,nk,nsub))*(rr(1:iend)**2)
       call asa_integral(mecca_state,nk,nsub,tmp_gf(1:iend),re_ws,re_mt)
        tmp_gf(1:iend) = aimag(green(1:iend,nk,nsub))*(rr(1:iend)**2)
       call asa_integral(mecca_state,nk,nsub,tmp_gf(1:iend),im_ws,im_mt)
        mecca_state%gf_box%dos(0,nk,nsub,ispin) =                       &
     &                                          -dcmplx(re_ws,im_ws)/pi
        if ( gNonMT() ) then
         mecca_state%gf_box%dosck(nk,nsub,ispin)=-dcmplx(re_ws,im_ws)/pi
        else
         mecca_state%gf_box%dosck(nk,nsub,ispin)=-dcmplx(re_mt,im_mt)/pi
        end if
        if ( mecca_state%intfile%iprint.ge.0 ) then
         write(6,1001) mecca_state%gf_box%energy(ispin),                &
     &             mecca_state%gf_box%dos(0,nk,nsub,ispin),updn(ispin), &
     &                 nsub,nk
 1001    format(2g15.7,1x,2g16.8,' E',a2,',dos(E) nsub=',i4,1x,i2)
        end if
!DEBUG
        if ( DIMAG(mecca_state%gf_box%dos(0,nk,nsub,ispin))<0.d0 ) then
         write(99,1101) mecca_state%gf_box%energy(ispin),               &
     &             mecca_state%gf_box%dos(0,nk,nsub,ispin),updn(ispin), &
     &                 nsub,nk,ispin
 1101    format(2g24.16,1x,2g16.8,' E',a2,',dos(E) nsub=',i4,1x,2i2)
!         close(99)
!         call fstop('DEBUG ERROR')
!         end if
        end if
!DEBUG
       end do
      end do
!c
      return
!EOC
      end subroutine gf2dos
!
!BOP
!!IROUTINE: srms_green
!!INTERFACE:
      subroutine srms_green(green,tau,kkrsz,                            &
     &                  nrelv,clight,                                   &
     &                  ized,                                           &
     &                  lmax,                                           &
     &                  energy,p_in,                                    &
     &                  rIS,rScat,                                      &
     &                  h,jws,rr,vr,                                    &
     &                  iprint)
!!DESCRIPTION:
! scalar-relativistic MS contribution {\tt z * t * z} to {\tt green(rr,energy)}
! (Greens function) per E/species/sublat/spin


!!DO_NOT_PRINT
      implicit none
!!DO_NOT_PRINT

!!ARGUMENTS:
      complex(8), intent(out) :: green(:)  ! (iprpts)
      complex(8), intent(in)  :: tau(:,:)   ! (ipkkr,ipkkr)
      integer, intent(in) ::    kkrsz
      integer, intent(in) ::    nrelv
      real(8), intent(in) ::     clight
      integer, intent(in) ::    ized
      integer, intent(in) ::    lmax
      complex(8), intent(in) :: energy
      complex(8), intent(in) :: p_in
      real(8), intent(in) ::     rIS,rScat
      real(8), intent(in) ::     h
      integer, intent(in) ::    jws
      real(8), intent(in) ::     vr(:)  ! (iprpts)
      real(8), intent(in) ::     rr(:)  ! (iprpts)
      integer, intent(in) ::    iprint
!!REVISION HISTORY:
! Initial Version - A.S. - 2013
!EOP
!
!BOC
!
      character(10) :: istop='nostop'
!c
      integer    ir
      integer    i
      integer    lcur
      complex(8) :: alpha(0:lmax)
      complex(8) :: cotdl_(0:lmax)
!c
! four components of scalar-relat. solution
      complex(8), allocatable :: zlr(:,:)    ! regular
      complex(8), allocatable :: jlr(:,:)    ! irregular
      complex(8), allocatable :: fzlr(:,:)   ! regular
      complex(8), allocatable :: fjlr(:,:)   ! irregular
      integer    lindex(kkrsz)
      complex(8) :: taudiag(kkrsz)
      real(8)    :: tdtmp

      green(:) = czero
      if ( ized<0 .or. jws<0 .or. lmax<0 ) return
      allocate(zlr(1:size(green,1),0:lmax))
      allocate(jlr(1:size(green,1),0:lmax))
      allocate(fzlr(1:size(green,1),0:lmax))
      allocate(fjlr(1:size(green,1),0:lmax))

!c     get regular and irregular radial soln's.......................
!c           -------------------------------------------------------
       call solveSS(nrelv,clight,                                       &
     &              ized,                                               &
     &              lmax,                                               &
     &              energy,p_in,                                        &
     &              zlr,jlr,                                            &
     &                  fzlr,fjlr,                                      &
     &              cotdl_,alpha,                                       &
     &              rIS,rScat,rr,vr,                                    &
     &              iprint)
!c              -------------------------------------------------------
!
      if(iprint.ge.2) then

         do lcur=0,lmax
         write(6,'('' srms_green:: l='',i3,'' alpha='',2d12.4)')        &
     &                                       lcur,alpha(lcur)
         end do
         do lcur=0,lmax
         write(6,'('' srms_green:: l='',i3,'' cotdl='',2d12.4)')        &
     &                                        lcur,cotdl_(lcur)
         end do
         do lcur=0,lmax
         write(6,'('' srms_green:: l='',i3,'' tandl='',4d12.4)')        &
     &                 lcur,(1.d0,0.d0)/cotdl_(lcur),                   &
     &                 cotdl_(lcur)/sqrt(cotdl_(lcur)**2+1.d0)
         end do
         do lcur=0,lmax
           write(6,'('' srms_green:: l='',i3)') lcur
           write(6,'('' srms_green::ir,z*z:'',i4,2d12.4)')              &
     &               (ir,zlr(ir,lcur)*zlr(ir,lcur),ir=1,jws,50)
         enddo
      endif
!DEBUG to improve accuracy in green
      tdtmp = zero
      do i=1,kkrsz
       taudiag(i) = tau(i,i)
       tdtmp = max(tdtmp,abs(taudiag(i)))
      end do
      tdtmp = epsilon(tdtmp)*tdtmp
      do i=1,kkrsz
       if ( abs(taudiag(i))<tdtmp ) taudiag(i) = czero
      end do
!DEBUG
!
!c     calculate the greens function for this (energy,species,sub-lat,
!c     spin) :: Spherical part only in this code.....................
!c     --------------------------------------------------------------
      i = 0
      do lcur=0,lmax
       lindex(i+1:i+1+2*lcur) = lcur
       i = i+1+2*lcur
      end do
      green(:) = czero
      do i=1,min(kkrsz,(lmax+1)**2)
       if ( cotdl_(lindex(i))==czero ) cycle
       green(:) = green(:)+zlr(:,lindex(i))*zlr(:,lindex(i))*taudiag(i)
      enddo
      deallocate(zlr,jlr,fzlr,fjlr)
!
      if(iprint.ge.2) then
         write(6,'(//)')
         write(6,'('' srms_green:: ir,rr(ir),green(ir) '',i4,3d12.4)')  &
     &            (ir,rr(ir),green(ir),ir=1,size(green),50)
      endif

      return
!EOC
      end subroutine srms_green
!
!BOP
!!IROUTINE: solveSS
!!INTERFACE:
      subroutine solveSS(nrelv,clight,                                  &
     &                  ized,                                           &
     &                  lmax,                                           &
     &                  energy,p_in,                                    &
     &                  zlr,jlr,                                        &
     &                  fzlr,fjlr,                                      &
     &                  cotdel,almat,                                   &
     &                  rIS,rScat,r,rv,                                 &
     &                  iprint,l_shft)
!!DESCRIPTION:
!   this subroutine solves scalar relativistic equations for complex
!   energies using Calogero's phase method
!   (single-scattering problem)
!....ddj \& fjp --- july 1991

!!DO_NOT_PRINT
      implicit none
!!DO_NOT_PRINT

!!ARGUMENTS:
      integer, intent(in) :: nrelv,ized,lmax
      real(8), intent(in) :: clight
      complex(8), intent(in) :: energy,p_in
      complex(8), intent(out) :: zlr(:,0:)  ! (iprpts,0:iplmax)
      complex(8), intent(out) :: jlr(:,0:)  ! (iprpts,0:iplmax)
      complex(8), intent(out) :: fzlr(:,0:)  ! (iprpts,0:iplmax)
      complex(8), intent(out) :: fjlr(:,0:)  ! (iprpts,0:iplmax)
      complex(8), intent(out) :: cotdel(0:lmax)
      complex(8), intent(out) :: almat(0:lmax)
      real(8), intent(in) :: rIS,rScat
      real(8), intent(in) :: r(:)
      real(8), intent(in) :: rv(:)
      integer, intent(in) :: iprint
      integer, intent(inout), optional :: l_shft
!!REVISION HISTORY:
! Adapted - A.S. - 2013
!EOP
!
!BOC
!c
      complex(8), allocatable :: ztmp(:)
      complex(8), allocatable :: g(:)
      complex(8), allocatable :: f(:)
      complex(8), allocatable :: gpl(:)
      complex(8), allocatable :: fpl(:)
!      complex(8) :: prel,pnrel
      complex(8) :: anorm
      complex(8) :: tandel

      real(8) :: rscat_
      real(8), parameter :: eps_cond = 1.d-6

      integer :: nrel,iend,l,l_s
!c     +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
      character(10), parameter :: sname='solveSS'
!c     +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

      complex(8) :: cinv,c2minv,eoc2p1,cll1
      logical :: with_sctrn
!c
!c
!c     Gets radial integrals, t-matrix.
!c
!c     In this version rv is r * v
!c
!c      returns:
!c            zlr         zl(r) properly normalized..............
!c            jlr         jl(r) properly normalized..............
!c
!c     set c for doing scalar-relativistic or non-relativistic calc.
!c     .....c = speed of light in proper units..........
!c     Ryd units: m=1/2  hbar=1 c=2*inverse of fine structure const.
!c
!c     Solving a relativistic (nrelv=0) or non-relativistic (nrelv=1)
!c     problem.   If non-relativistic, then set e=e-elmass equal to e
!c     (where elmass = electron mass) and 1/c is set to ~zero to make
!c     relativistic terms zero.
!c
!c     Using Ricatti Bessel fcts. Need only l=0 and l=1 to get wave-fcts.
!c
!c     To get physical phase-shifts and normalization, need get Rl in
!c     terms of spherical bessel for all l's at the boundary of sphere.
!c     This is properly done in SCALAR, see clmt and slmt.
!c
!c     With M(r)= [1 + (e-v(r))*c2inv]
!c     Obtain r*(wave-fct.):   regular     g=r*R and   f=r*R'/M
!c                           irregular   gpl=r*R and fpl=r*R'/M
!c
!c     NOTE: ====>   need to get gderiv for mom. matrix elements
!c
!c     *****************************************************************
!c
        nrel = nrelv
      l_s = lmax
      if (present(l_shft) ) then
        if ( l_shft <= lmax ) then
         l_s = l_shft
        end if
      end if

      if(iprint.ge.2) then
         write(6,'('' solveSS:         lmax='',i5)')lmax
         write(6,'('' solveSS: nrel,clight='',i5,d12.4)')nrel,clight
         write(6,'('' solveSS: energy,p_in='',4d12.4)') energy,p_in
         write(6,'('' solveSS:      rScat='',f8.5)') rScat
      endif
!c     =================================================================

      iend=min(size(r),size(rv))
      allocate(ztmp(iend))
      allocate(g(iend),f(iend),gpl(iend),fpl(iend))

!c     =================================================================

      rscat_ = rScat     !DEBUG

      cinv = cone/clight
      c2minv = cinv**2/(2*m_e)   ! 1/(2*m_e*c**2)
      eoc2p1 = cone+energy*c2minv

!c     solve scalar-relativistic or non-relativistic equation.
!DELETE      with_sctrn = .true.
      with_sctrn = .false.
      do l=0,lmax
!       rscat_ = max(rIS,rScat)
!c        ------------------------------------------------------------
!         with_sctrn = l<=l_s
         if ( atom_ss ) with_sctrn=.false.
         call scalar(nrel,clight,                                       &
     &               ized,                                              &
     &               l,                                                 &
     &               g,f,gpl,fpl,                                       &
     &               tandel,cotdel(l),almat(l),                         &
     &               energy,p_in,                                       &
     &               rv,r,rscat_,iend,                                  &
     &               1,with_sctrn,                                      &
     &               iprint)
         ztmp(1:iend) = cone/r(1:iend)
         zlr(1:iend,l) = g(1:iend)*ztmp(1:iend)
         jlr(1:iend,l) = gpl(1:iend)*ztmp(1:iend)
         fzlr(1:iend,l) = f(1:iend)*ztmp(1:iend)
         fjlr(1:iend,l) = fpl(1:iend)*ztmp(1:iend)
      enddo

      return
!c
!EOC
      end subroutine solveSS
!
!BOP
!!IROUTINE: solve1AS
!!INTERFACE:
      subroutine solve1AS(nrelv,clight,                                 &
     &                  ized,l,ec,w_ec,                                 &
     &                  gf,                                             &
     &                  rScat,r,rv)
!!DESCRIPTION:
!   this subroutine solves scalar relativistic equations for complex
!   energies using Calogero's phase method for specific angular momentum {\tt l}
!   (atomic-scattering problem)

!!DO_NOT_PRINT
      implicit none
!!DO_NOT_PRINT

!!ARGUMENTS:
      integer, intent(in) :: nrelv,ized,l
      real(8), intent(in) :: clight
      real(8), intent(in) :: ec,w_ec
      complex(8), intent(out) :: gf(:)
!      complex(8), intent(out) :: zlr(:)
!      complex(8), intent(out) :: jlr(:)
      real(8), intent(in) :: rScat
      real(8), intent(in) :: r(:)
      real(8), intent(in) :: rv(:)
!!REVISION HISTORY:
! Initial version - A.S. - 2018
!EOP
!
!BOC
!c
      complex(8) :: g(size(r))
      complex(8) :: f(size(r))
      complex(8) :: gpl(size(r))
      complex(8) :: fpl(size(r))
      complex(8) :: p_in
      complex(8) :: tandel

      integer :: ne = 10
      real(8) :: alpha=zero,beta=pi
      complex(8), allocatable :: egrd(:),wghts(:)
      complex(8) :: energy,e0,w
      complex(8) :: cotdel
      complex(8) :: almat

      integer :: i,iend
      integer, parameter :: iprint=-1000
!c     +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
      character(10), parameter :: sname='solve1AS'
!c     +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!c
!c     In this version rv is r * v
!c
!c      returns:
!c            gf          g(r) * gpl(r) properly normalized..............
!c
!c     set c for doing scalar-relativistic or non-relativistic calc.
!c     .....c = speed of light in proper units..........
!c     Ryd units: m=1/2  hbar=1 c=2*inverse of fine structure const.
!c
!c     Solving a relativistic (nrelv=0) or non-relativistic (nrelv=1)
!c     problem.   If non-relativistic, then set e=e-elmass equal to e
!c     (where elmass = electron mass) and 1/c is set to ~zero to make
!c     relativistic terms zero.
!c
!c     Using Ricatti Bessel fcts.
!c
!c     To get physical phase-shifts and normalization, need get Rl in
!c     terms of spherical bessel at the boundary of sphere.
!c     This is properly done in SCALAR, see clmt and slmt.
!c
!c     With M(r)= [1 + (e-v(r))*c2inv]
!c     Obtain r*(wave-fct.):   regular     g=r*R and   f=r*R'/M
!c                           irregular   gpl=r*R and fpl=r*R'/M
!c     *****************************************************************
!c

      if (ized < 1) then
        gf = czero
!        zlr = czero
!        jlr = czero
      endif

      allocate(egrd(ne),wghts(ne))
      e0 = dcmplx(ec,zero)         ! center of the circle
      call conlegndr(e0,w_ec,alpha,beta,ne,egrd,wghts)
      iend=min(size(r),size(rv))
      gf = czero

      do i=1,ne

       energy = egrd(i)
       p_in = sqrt(energy)
       p_in = p_in*sqrt(cone+energy/(2*m_e*clight**2))
       w = -wghts(i)/pi
       call scalar(nrelv,clight,                                        &
     &               ized,                                              &
     &               l,                                                 &
     &               g,f,gpl,fpl,                                       &
     &               tandel,cotdel,almat,                               &
     &               energy,p_in,                                       &
     &               rv,r,rScat,iend,                                   &
     &               1,.false.,                                         &
     &               iprint)
       gf(1:iend) =  gf(1:iend) + w*g(1:iend)*gpl(1:iend)

      end do
      deallocate(egrd,wghts)

      return
!c
!EOC
      end subroutine solve1AS
!
!BOP
!!IROUTINE: printW
!!INTERFACE:
      subroutine printW(nu,i0,g,f,gpl,fpl)
!!DESCRIPTION:
! prints wronskian
!
!!REVISION HISTORY:
! Initial Version - A.S. - 2013
!!REMARKS:
! not used
!EOP
!
!BOC
      implicit none
      integer nu,i0
      complex(8) :: g(:),f(:),gpl(:),fpl(:)
      integer i
      complex(8) w,w1

!      w1 = 1.d0
!      w1 = g(1)*fpl(1)-f(1)*gpl(1)
!      if ( abs(w1) < epsilon(1.d0) ) w1 = 1.d0
!      write(*,*) ' w(',1,')=',w1
      do i=1,size(g)
       w = g(i)*fpl(i)-f(i)*gpl(i)
       if ( abs(abs(w)-1) > 1.d-6 ) then
        write(*,'('' i='',i5,''  w='',2d20.10)') i0-1+i,w
       end if
      end do
!      write(nu,'(''# i,g,f,gpl,fpl'')')
!      do i=1,i0
!       write(nu,'(1x,i5,4(2x,2e18.10))') i,g(i),f(i),gpl(i),fpl(i)
!      end do
      return
!EOC
      end subroutine printW
!

!
!c     cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
!BOP
!!IROUTINE: scalar
!!INTERFACE:
      subroutine scalar(nrelv,clight,                                   &
     &                  ized,                                           &
     &                  l,                                              &
     &                  g,f,gpl,fpl,                                    &
     &                  tandel,cotdel,alpha,                            &
     &                  energy_in,p_in,                                 &
     &                  rv_in,r,rScat,iend,                             &
     &                  iswzj,with_sctrn,                               &
     &                  iprint)
!!DESCRIPTION:
! calculates  regular/irregular solutions (and some other values) for
! radial scalar-relativistic Schroedinger equation
!
!....DDJ \& FJP  July 1991
!
!!USES:
      use stiffode, only : set_ode_params,odeint2,odeint2spl

!!DO_NOT_PRINT
      implicit none
!!DO_NOT_PRINT

!!ARGUMENTS:
      integer, intent(in) :: nrelv,ized,l
      real(8), intent(in) :: clight
      complex(8), intent(out) :: g(:)
      complex(8), intent(out) :: f(:)
      complex(8), intent(out) :: gpl(:)
      complex(8), intent(out) :: fpl(:)
      complex(8), intent(out) :: cotdel, tandel, alpha
      complex(8), intent(in) :: energy_in,p_in
      real(8), intent(in) :: rv_in(:)
      real(8), intent(in) :: r(:)
      real(8), intent(in) :: rScat
      integer, intent(in) :: iend,iswzj,iprint
      logical, intent(in) :: with_sctrn
!!REVISION HISTORY:
! Initial version - DDJ \& FJP  July 1991
! Adapted - A.S. - 2013
! Modified for empty spheres (z=0) - A.S. - 2016
!!REMARKS:
! based on Calegero Method
!EOP
!
!BOC
!
      integer, external :: indRmesh
      integer :: jScat,st_env

      call gEnvVar('ENV_DEBUG_SCALAR',.false.,st_env)

      IF ( st_env==1 )THEN

      call scalar1(nrelv,clight,                                        &
     &                  ized,                                           &
     &                  l,                                              &
     &                  g,f,gpl,fpl,                                    &
     &                  tandel,cotdel,alpha,                            &
     &                  energy_in,p_in,                                 &
     &                  rv_in,r,rScat,iend,                             &
     &                  iswzj,with_sctrn,iprint)

      ELSE

      jScat = indRmesh(rScat,iend,r)
      call dirac(SR,clight,                                             &
     &                  ized,                                           &
     &                  l,                                              &
     &                  g,f,gpl,fpl,                                    &
     &                  cotdel,alpha,                                   &
     &                  energy_in,p_in,                                 &
     &                  rv_in,r,rScat,jScat,iend,                       &
     &                  iswzj,with_sctrn,                               &
     &                  iprint)
      if (cotdel==zero) then
       tandel = zero
      else
       tandel = one/cotdel
      end if

      END IF

      return
      end subroutine scalar

!c     cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
!BOP
!!IROUTINE: scalar1
!!INTERFACE:
      subroutine scalar1(nrelv,clight,                                  &
     &                  ized,                                           &
     &                  l,                                              &
     &                  g,f,gpl,fpl,                                    &
     &                  tandel,cotdel,alpha,                            &
     &                  energy_in,p_in,                                 &
     &                  rv_in,r,rScat,iend,                             &
     &                  iswzj,with_sctrn,                               &
     &                  iprint)
!!DESCRIPTION:
! calculates  regular/irregular solutions (and some other values) for
! radial scalar-relativistic Schroedinger equation
!
!....DDJ \& FJP  July 1991
!
!!USES:
      use stiffode, only : set_ode_params,odeint2,odeint2spl

!!DO_NOT_PRINT
      implicit none
!!DO_NOT_PRINT

!!ARGUMENTS:
      integer, intent(in) :: nrelv,ized,l
      real(8), intent(in) :: clight
      complex(8), intent(out) :: g(:)
      complex(8), intent(out) :: f(:)
      complex(8), intent(out) :: gpl(:)
      complex(8), intent(out) :: fpl(:)
      complex(8), intent(out) :: cotdel, tandel, alpha
      complex(8), intent(in) :: energy_in,p_in
      real(8), intent(in) :: rv_in(:)
      real(8), intent(in) :: r(:)
      real(8), intent(in) :: rScat
      integer, intent(in) :: iend,iswzj,iprint
      logical, intent(in) :: with_sctrn
!!REVISION HISTORY:
! Adapted - A.S. - 2013
! Modified for empty spheres (z=0) - A.S. - 2016
!!REMARKS:
! based on Calegero Method
!EOP
!
!BOC
!      real*8 fact(4),factl1
      real(8) :: zed
      real(8) :: v0
      real(8) :: cinv
      real(8) :: c2inv
      real(8) :: power
!c
      complex(8) :: bjl(l+2),djl(l+2)
      complex(8) :: bnl(l+2),dnl(l+2)
      complex(8) :: gpl_s,fpl_s,g_s,f_s
      complex(8) :: anorm,zlog,cm1l

      integer nrel, j, jScat
      integer lmin

      integer i0

      complex(8) :: energy,eoc2p1, fz2oc2, tzoc, v0me
!      complex(8) :: tzoc2
      complex(8) :: enr,emvr,emr,em,em2
      complex(8) :: a0,a1p,a1,a2p,a2,a3p,a3
      complex(8) :: pr,slmt,clmt,tl,ptl_over_sl
      complex(8) :: dg,a11,a12,a21,a22
!      real(8) z0

      real(8) :: cl,sl,clph,slph
      real(8) :: rv(0:size(rv_in,1))
      real(8), parameter :: relerr=1.d-8,abserr=tiny(relerr)
      integer iflag,st_env

      real(8) :: rj,renorm,renorm2,overflo
      integer l1,j1min,j2min,jmin(2)

      real(8) :: rvj
!c     ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
      integer, parameter :: icmax=5
      real(8), parameter :: half=one/two,pi2=2*pi
      real(8), parameter :: tole=(one/ten)**10
      character(10), parameter :: sname='scalar'
!CAB
      real(8), parameter :: smallnumber=1.d-12
      real(8), parameter :: bignumber=huge(1.d0)/1.d9
!DEBUG      real(8), parameter :: logmax=log(huge(1.d0))
!DEBUG      real(8), parameter :: logmin=log(tiny(1.d0))
      real(8), parameter :: logmax=log(bignumber)
      real(8), parameter :: logmin=-logmax
      real(8) :: logdblfact
      real(8) :: r_zero
      real(8) :: vr0
!CAB
!c     stiff ode solver
      real(8) :: eps
      real(8) :: hmin
      real(8) :: x1,x2
      real(8) :: xtmp
      real(8) :: tmpr(2),tmprv(2)
      real(8), allocatable :: rspl(:),cspl(:)
      real(8) :: dc(0:10),ac(0:10)
      complex(8) :: ys1(2)
      complex(8) :: ys2(2)
!DEBUG
      complex(8) :: wrnskn
!DEBUG

      real(8) :: cent
      logical :: point_nucleus

      integer ipot(2)

      integer, external :: indRmesh
      real(8), external :: SPL3

      integer i,j1,nc
      integer, save :: imsg = 0, msgmax = 64
!c     sub-step information
      integer nss
!!      real(8) :: xss(40*size(r))
!!      real(8) :: rvss(40*size(r))
!!      complex(8) :: yss(2,40*size(r))
!!      complex(8) :: dyss(2,40*size(r))
!!      complex(8) :: errss(2,40*size(r))
      real(8) :: xss(1)
      real(8) :: rvss(1)
      complex(8) :: yss(2,1)
      complex(8) :: dyss(2,1)
      complex(8) :: errss(2,1)
!c
!c     ****************************************************************** !*
!c     sets up to solve the non- and scalar-relativistic equation.
!c     based on Calegero Method..............DDJ & FJP  July 1991
!c     uses Ricatti-Bessel fcts. for solution
!c     ****************************************************************** !*
!c
      energy = energy_in
      if(iprint.ge.6) then
         write(6,'('' in SCALAR iprint '',i5)') iprint
         write(6,'('' scalar: l,iswzj='',2i5)') l,iswzj
         write(6,'('' scalar: nrelv,clight='',i5,d12.4)') nrelv,clight
         write(6,'('' scalar: energy='',2d16.8)') energy
         write(6,'('' scalar:   p_in='',2d16.8)') p_in
      end if
      if ( with_sctrn ) then
       eps=relerr
      else
       eps=min(1.d-5,relerr*1.d2)
      end if
      tandel = czero
      alpha = czero
      cotdel = czero
      g = czero
      f = czero
      gpl = czero
      fpl = czero
      jScat = indRmesh(rScat,iend,r)


!c     =================================================================
!DELETE!c     cannot solve for the real part of energy equal to 0.0!
!DELETE      if( abs(real(energy)) .lt. smallnumber ) then
!DELETE        energy = energy_in - 1.d-6
!DELETE        if ( iprint>=0 ) then
!DELETE         write(6,'('' scalar:: real(energy) is about zero, replaced by''&
!DELETE     &             '' -1.e-6'')')
!DELETE!c        call p_fstop(sname)
!DELETE        end if
!DELETE      end if

!c     =================================================================
      cm1l = exp(sqrtm1*pi*mod(l,2))   ! (-1)**l
      rv(1:) = rv_in
      zed=ized
      l1 = l+1
!C...............................................................
!c
      r_zero = min(r(1)/100,1.d-6)
      call twoZ(zed,zero,1,vr0)
      if ( ized==0 ) then
       point_nucleus = .false.
       vr0 = rv_in(1)*(r_zero/r(1))
      else
       point_nucleus = gPointNucleus()
       call twoZ(zed,r_zero,1,vr0)
      end if
      nrel = nrelv
!DELETE      call gEnvVar('ENV_VAL_REL',.false.,st_env)
!DELETE      if ( st_env>0 ) then
!DELETE        nrel = 1
!DELETE        write(6,*) ' ENV_VAL_REL ==> nrel = ',nrel
!DELETE      end if

      if(nrel.le.0) then
       cinv=one/clight
       if(ized.lt.0) then
        tzoc=czero
        fz2oc2=czero
!        tzoc2=czero
       else
        if ( ized==0 .and. .not.point_nucleus ) then
         tzoc = czero
        else
         tzoc=abs(vr0)*cinv
        end if
        fz2oc2=tzoc*tzoc
!        tzoc2=tzoc*cinv
       end if
       c2inv=cinv*cinv
       eoc2p1 = cone+energy*c2inv
      else
       cinv = zero
       c2inv= zero
       eoc2p1 = cone
       tzoc = czero
       fz2oc2=czero
!       tzoc2= czero
      end if
!
      ! (2l+1)!! = 2/sqrt(pi)*2^l*Gamma(l+3/2);  logdblfact = log( (2l+1)!! )
      logdblfact = 20 + (l+1)*(log(2.d0*l+3.d0))   ! approx. for large l
!      i0 = 4
      i0 = jScat+1
      do j=1,jScat
       if ( l*log(abs(p_in)*r(j)) - logdblfact > logmin ) then
        i0 = j
        exit
       end if
      end do
      if ( i0 > jScat ) then    ! solution is outside the scattering region
!       write(6,*) ' WARNING z=',ized,' l=',l,' p=',p
!       write(6,*) ' l*log(p_in*r)=',l*log(abs(p_in)*rScat)
!       write(6,*) ' logdblfact=',logdblfact
!       write(6,*) ' logmin=',logmin
       return
      end if
      logdblfact = (l+1)*(log(2.d0*l+3.d0)) ! approx. for large l

      call set_ode_params(c2inv0=c2inv)

      if ( point_nucleus ) then
       v0me= zero-energy
      else
       v0me= vr0/r_zero-energy
      end if
!
      a0=cone   ! it is arbitrary

!      IF ( i0>1 ) THEN
       if(nrel.le.0) then
!c        ==============================================================
!c        scalar-relativistic case......................................

        power=sqrt( (l*(l+1) + 1) - dreal(fz2oc2) )   ! ddj's  MECCA definition

        if(ized.lt.0) then
         a1 = czero
         a2 = v0me/(4*power+4)
         a3 = czero
        else
!         a1p=( fz2oc2 + em0*(power-1 -2*fz2oc2) )/(2*power+1)
!         a1 = a0*a1p/tzoc2
!         a2p=( a1p*(fz2oc2 + em0*(3*power+1 - 2*fz2oc2)) +              &
!     &         em0*em0*(fz2oc2 - 2*power+2) )/(4*power+4)
!         a2 = a0*a2p/(tzoc2*tzoc2)
!         a3p=( a2p*( fz2oc2 + em0*(5*power+5 - 2*fz2oc2) )              &
!     &        - a1p*(4*power+1 - fz2oc2)*em0*em0                        &
!     &        + (3*power-3 - fz2oc2)*em0*em0*em0  )/(6*power+9)
!         a3 = a0*a3p/(tzoc2*tzoc2*tzoc2)
!DEBUG_SR
         a1p= (-2*zed)/(2*power-1)
         a2p = a1p * (-2*zed)/(2*(2*power))
         a3p = czero    ! a3p * (-2*zed)/(3*(2*power+1))
         a1 = a0*a1p
         a2 = a0*a2p
         a3 = a0*a3p
!DEBUG_SR
        end if
       else
!c        ==============================================================
!c        non-relativistic case......................................
        power= l1
!c
!c        a0 is arbitrary, but this make Rl a spherical Bessel at origin.
!c                        R_l=x*j_l(x)= x*x**l/(2l+1)!!
!c        a0= p**l1/factl1
!c        a0= p**power/factl1
        if(ized.lt.0) then
         a1 = czero
         a2p= v0me/(4*l+6)
         a2 = a0*a2p
         a3 = czero
        else
!         a1p= -2*zed/(2*l+2)
!         a2p= (v0me - a1p*2*zed)/(4*l+6)
!         a3p= (a1p*v0me - a2p*2*zed)/(6*l+12)
!DEBUG_SR
         a1p= (-2*zed)/(2*power-1)
         a2p = a1p * (-2*zed)/(2*(2*power))
         a3p = a2p * (-2*zed)/(3*(2*power+1))
!DEBUG_SR
         a1 = a0*a1p
         a2 = a0*a2p
         a3 = a0*a3p
        end if
       end if
       cent = l*(l+1)
       call set_ode_params(cent0=cent,power0=power)

!      j0 = 0
      IF ( .not.point_nucleus ) THEN
!?      IF ( .not.point_nucleus .or. ized==0 ) THEN
!
       call set_ode_params(in_out0=0,enrg0=energy)
       i0 = 1
       hmin=abserr
       nss=0
       iflag = 1
       ipot(1)=1
       ipot(2)=2
       tmpr(1) = 0.9d0*r(1)
       tmpr(2) = r(1)
!
       if ( zed == zero ) then
        emr= eoc2p1*r(1) - rv_in(1)*c2inv    ! r+(e*r-rv)*c2inv
!
        lmin = max(2,l1)
        pr = sqrt(-v0me*((2*m_e) - v0me*c2inv))
        pr = pr*sign(1.d0,aimag(pr)) * r(1)
        call ricbes(lmin,pr,bjl(1:lmin),bnl(1:lmin),                    &
     &                        djl(1:lmin),dnl(1:lmin))
        g(1) = a0*bjl(l1)
        f(1) = a0*djl(l1)*pr/emr
       else
        i0 = 1
        call twoZ(zed,tmpr,2,tmprv)
        tmprv(1:2) = -0.5d0*tmprv(1:2)
        call set_ode_params(ipot0=ipot,rfit0=tmpr(1:2),zfit0=tmprv(1:2))
        x1=tmpr(1)
        x2=tmpr(2)
        ys1(1)= a0 + x1*(a1 + x1*(a2 + czero*a3*x1))
        dg = a1 + two*x1*a2+three*x1**2*a3
        rvj = two*tmprv(1)
        enr  = x1*energy
        emvr = c2inv*(enr-rvj)               ! (em-1)*r
        emr = x1 + emvr                      ! r+(e*r-rv)*c2inv
        a11 = one/x1
        a12 = emr/x1    ! mass
        ys1(2) = ( dg-a11*ys1(1))/a12
        ys1 = ys1*x1
!
        call odeint2(x1,ys1,x2,ys2,iflag,eps,hmin,                      &
     &                                       1,xss,yss,dyss,rvss,errss)
!for detail info use:     &          size(xss),xss,yss,dyss,rvss,errss,nss)
        if(iflag.ne.0) then
         if ( iflag.ne.3 ) then
          write(*,*) ' odeint2 error (boundary r=0)=',iflag
          call fstop(sname//': ODEINT2 ERROR')
         else
          if ( imsg<=msgmax ) then
           write(6,*) ' WARNING: odeint2 error (boundary r=0)=',iflag
          if ( imsg==msgmax )                                           &
     &       write(6,*) 'LIMIT NUMBER OF WARNINGS: ',msgmax
          end if
          imsg = imsg + 1
         endif
        endif
!
        g(1)=ys2(1)/x2
        f(1)=ys2(2)/x2
       end if
!
      ELSE
       do j=1,i0
!c
!c        em= 1 + (energy - rv(j)/r(j))*c2inv
!c        emr= eoc2p1*r(j) - rv(j)*c2inv
!c
        rvj = rv(j)
        enr  = r(j)*energy
        emvr = c2inv*(enr-rvj)               ! (em-1)*r
        emr= r(j) + emvr                       ! r+(e*r-rv)*c2inv

        g(j)= a0 + r(j)*(a1 + r(j)*(a2+ a3*r(j)))
        f(j)= a0*(power-1) + r(j)*(a1*power + r(j)*(a2*(power+1) +      &
     &                                        a3*(power+2)*r(j))) / emr

!        if(iprint.ge.6) then
!         write(6,'('' scalar: j,g,f='',i5,2(1x,2d16.8))') j,g(j),f(j)
!        end if
!c       ==============================================================
!c       r*G = g and r*F = f, i.e. this is r*wave-fct.
!      RENORMALIZATION IS DONE LATER
!        g(j)= r(j)**power*g(j)
!        f(j)= r(j)**power*f(j)
!
       end do

      END IF

      rv(0) = vr0
      rv = -0.5d0*rv   ! to be compatible with units used in stiffode module

      allocate(rspl(0:size(rv)),cspl(0:size(rv)))
      rspl(0) = r_zero
      rspl(1:jScat+1) = r(1:jScat+1)
      call  ZSPL3(jScat+2,rspl(0:jScat+1),rv(0:jScat+1),cspl(0:jScat+1))

!DEBUGPRINT
!      do j=0,jScat+1
!       rvj = SPL3(jScat+2,rspl(0:jScat+1),rv(0:jScat+1),                &
!     &                                        cspl(0:jScat+1),rspl(j))
!       write(101,'(i5,3(2x,g17.7))') j,rspl(j),rvj,rv(j)
!      end do
!      do j=0,jScat
!       do i0=1,5
!       x1 = rspl(j)+i0*(rspl(j+1)-rspl(j))/5.d0
!       rvj = SPL3(jScat+2,rspl(0:jScat+1),rv(0:jScat+1),                &
!     &                                        cspl(0:jScat+1),x1)
!       write(102,'(i5,3(2x,g17.7))') j,x1,rvj
!       end do
!      end do
!      call fstop('DEBUG')
!DEBUGPRINT

!c
!c     =================================================================
!c     regular solution and phase-shifts of scalar relativistic eqs.
!c
      hmin=abserr
!      eps=relerr
      nss=0
      iflag = 1
      j1 = i0
      ys2(1)=g(j1)
      ys2(2)=f(j1)
      call set_ode_params(in_out0=1,enrg0=energy)

      j1min = jScat+1
      x2 = r(i0)
      do j=i0+1,jScat+1
        j1=j-1
!DEBUG
!?        if ( j<=j0 ) then
!?         cycle
!?        end if
!?        if ( j==j0+1 ) then
!?         x2 = r(j1)
!?         ys2(1)=g(j1)
!?         ys2(2)=f(j1)
!?        end if
!DEBUG
!c       pass info thru common to potential interpolation routines
        ipot(1)=j1
        ipot(2)=j
          call set_ode_params(ipot0=ipot,rfit0=r(j1:j),                 &
     &                                 zfit0=rv(j1:j),cspl0=cspl(j1:j))
        x1=x2
        ys1(1:2)=ys2(1:2)
        if ( j==jScat+1 ) then
         x2 = rScat
        else
         x2=r(j)
        end if
        call odeint2spl(x1,ys1,x2,ys2,iflag,eps,hmin,                   &
     &                                       1,xss,yss,dyss,rvss,errss)
        if(iflag.ne.0) then
         if ( iflag.ne.3 ) then
          write(*,*) ' odeint2spl error (regular sol.)=',iflag
             write(*,*) ' j=',j
          call fstop(sname//': ODEINT2 ERROR')
         else
          if ( imsg<=msgmax ) then
           write(6,*) ' WARNING: odeint2spl error (regular sol.)=',iflag
           if ( imsg==msgmax )                                          &
     &       write(6,*) 'LIMIT NUMBER OF WARNINGS: ',msgmax
          end if
          imsg = imsg + 1
         endif
        endif
!        mapss(j)=nss
        g(j)=ys2(1)
        f(j)=ys2(2)

!c     propagate regular solution with r^power factored out
!c     until the oscillatory region
        if ( j1>i0+l ) then
          if ( abs(p_in)*r(j)>one .or. abs(g(j)) < abs(g(j1)) ) then
           j1min = j
           exit
          end if
!         end if
        end if
!???!      OR (p_in*r)^l/(2l+1)!! is too big
!???        if ( l*log(abs(p_in)*r(j)) - logdblfact > 0.6d0*logmax ) then
!      OR (r/r0)^power is too big
        if ( power*log(r(j)/r(i0)) > 0.6d0*logmax ) then
          j1min = j-1
          exit
        end if
      end do

!c     renormalize the solution
      renorm = one/abs(g(j1min))
      a0 = a0*renorm
      g(1:j1min) = renorm*g(1:j1min)
      f(1:j1min) = renorm*f(1:j1min)
      do j=1,j1min
        if ( j1min == jScat+1 ) then
         renorm= (r(j)/rScat)**power   ! r(jScat) < rScat <= r(jScat+1)
        else
         renorm= (r(j)/r(j1min))**power
        end if
        g(j)= renorm*g(j)
        f(j)= renorm*f(j)
      enddo
      jmin = j1min
      renorm2 = one
      j2min = j1min
      if ( power >= l30 ) then
       do j=j1min+1,jScat+1
        if ( power*log(r(j)/r(j1min)) > 0.8d0*logmax ) then
          j2min = j-1
          exit
        end if
       end do
      end if
      if ( j2min > j1min ) then
       jmin(2) = j2min
       nss=0
       j1 = j1min
       ys2(1)=g(j1)
       ys2(2)=f(j1)
       call set_ode_params(in_out0=0,enrg0=energy)
       iflag = 1
       x2 = r(j1min)
       do j=j1min+1,j2min
        j1=j-1
!c       pass info to potential interpolation routines
        ipot(1)=j1
        ipot(2)=j
          call set_ode_params(ipot0=ipot,rfit0=r(j1:j),                 &
     &                                 zfit0=rv(j1:j),cspl0=cspl(j1:j))
        x1=x2
        x2=r(j)
        ys1(1:2)=ys2(1:2)
        call odeint2spl(x1,ys1,x2,ys2,iflag,eps,hmin,                   &
     &                                       1,xss,yss,dyss,rvss,errss)
        if(iflag.ne.0) then
         if ( iflag.ne.3 ) then
             write(*,*) ' odeint2spl error (regular sol.)=',iflag
             write(*,*) ' j1,j=',j1,j,' l=',l,' z=',ized
             write(*,'(a,3(1x,es15.7))') ' * x1,x2,rScat=',x1,x2,rScat
             write(*,'(a,2(1x,2es17.7))') ' * ys1=',ys1
             write(*,'(a,(1x,2es17.7))') ' * rv(j1:j)=',rv(j1:j)
          call fstop(sname//': ODEINT2SPL ERROR')
         else
          if ( imsg<=msgmax ) then
           write(6,*) ' WARNING: odeint2spl error (regular sol.)=',iflag
           if ( imsg==msgmax )                                          &
     &       write(6,*) 'LIMIT NUMBER OF WARNINGS: ',msgmax
          end if
          imsg = imsg + 1
         endif
        endif
        g(j)=ys2(1)
        f(j)=ys2(2)
       end do
       renorm2 = one/abs(g(j2min))
       a0 = a0*renorm2
       g(1:j2min) = renorm2*g(1:j2min)
       f(1:j2min) = renorm2*f(1:j2min)
      end if
!c     ------------------------------------------------------------------
!c     propagate regular solution without r^power factored out
!c     out to the V=0 region
!c     ------------------------------------------------------------------

!c       minimum allowed step-size: can be zero
        hmin=abserr
!c       specify sub-step tolerance
!        eps=relerr
!c       initialize the count of sub-steps
!c       xss,yss, etc are accumulated only for possible internal debugging purposes
        nss=0
!c       pass complex energy to subroutine thru common
      j1min = maxval(jmin)
      j1 = j1min
      ys2(1)=g(j1)
      ys2(2)=f(j1)
      call set_ode_params(in_out0=0,enrg0=energy)
      iflag = 1
      if ( j1min == jScat+1 ) then
       x2 = rScat
      else
       x2 = r(j1min)
      end if
      do j=j1min+1,jScat+1
        j1=j-1
!c       pass info to potential interpolation routines
        ipot(1)=j1
        ipot(2)=j
          call set_ode_params(ipot0=ipot,rfit0=r(j1:j),                 &
     &                                 zfit0=rv(j1:j),cspl0=cspl(j1:j))
        x1=x2
        if ( j==jScat+1 ) then
         x2 = rScat
        else
         x2=r(j)
        end if
        ys1(1:2)=ys2(1:2)

        call odeint2(x1,ys1,x2,ys2,iflag,eps,hmin,                      &
     &                                       1,xss,yss,dyss,rvss,errss)

        if(iflag.ne.0) then
         if ( iflag.ne.3 ) then
             write(*,*) ' odeint2 error (regular sol.)=',iflag
             write(*,*) ' j1,j=',j1,j,' l=',l,' z=',ized
             write(*,'(a,3(1x,es15.7))') ' * x1,x2,rScat=',x1,x2,rScat
             write(*,'(a,2(1x,2es17.7))') ' * ys1=',ys1
             write(*,'(a,(1x,2es17.7))') ' * rv(j1:j)=',rv(j1:j)
             if ( iflag == 1 ) then
             write(*,*) ' maxss=',size(xss),' errss=',errss(:,nss-1:nss)
             end if
          call fstop(sname//': ODEINT2 ERROR')
         else
          if ( imsg <= msgmax ) then
           write(6,*) ' WARNING: odeint2 error (regular sol.)=',iflag
           if ( imsg==msgmax )                                          &
     &       write(6,*) 'LIMIT NUMBER OF WARNINGS: ',msgmax
          end if
          imsg = imsg + 1
         end if
        endif
!        mapss(j)=nss
        g(j)=ys2(1)   !  if j=jScat+1, it is for r=rScat, not for r(j)
        f(j)=ys2(2)   !
      enddo
      g_s = g(jScat+1)   ! g_s = g(r=rScat)
      f_s = f(jScat+1)
!c     =================================================================
!c     At R, need spherical Bessel's for all l to get normalization and
!c     physical phase-shifts to determine cl and sl from
!c     g=r*R=r*( cl*jl - sl*nl ) and f=r*R'/M for all l's.
!c     Modify expression to account for difference between spherical and
!c     Ricatti-Bessel fcts.               Recall: R=g/r and dR=f*em/r
!c     =================================================================
      lmin=max(2,l1)
      pr=p_in*rScat
!c     -----------------------------------------------------------------
      call ricbes(lmin,pr,bjl(1:lmin),bnl(1:lmin),                      &
     &                                      djl(1:lmin),dnl(1:lmin))
!c
!c  ==================================================================
        em = eoc2p1                       ! to match free-space solution
!?      em = cone                       ! to match free-space solution
!c
!c  sl and cl at the sphere radius
!c
!DEBUG      slmt= (pnrel*djl(l1)-bjl(l1)/rScat)*g_s - bjl(l1)*f_s*em
!DEBUG      clmt= (pnrel*dnl(l1)-bnl(l1)/rScat)*g_s - bnl(l1)*f_s*em
      slmt= (p_in*djl(l1)-bjl(l1)/rScat)*g_s - bjl(l1)*f_s*em
      clmt= (p_in*dnl(l1)-bnl(l1)/rScat)*g_s - bnl(l1)*f_s*em
!      cotdel = clmt / slmt
      a11 = log(clmt) ; a12 = log(slmt)
      sl = mod(dimag(a12),pi2)
      cl = mod(dimag(a11),pi2)
      clph = mod(cl-sl,pi2)
      if ( clph<zero ) clph = clph + pi2
      if ( abs(a11-a12)<logmax ) then
       cotdel = exp(dcmplx(dreal(a11)-dreal(a12),clph))
      else
       cotdel = czero
      end if

        if(iprint.ge.6) then
         write(6,'('' scalar: l1,pr,p_in,em='',2i5,3(1x,2d16.8))')      &
     &                                  l1,jScat+1,pr,p_in,eoc2p1
         write(6,'('' scalar: djl,bjl='',2(1x,2d16.8))')                &
     &                                  djl(l1),bjl(l1)
         write(6,'('' scalar: dnl,bnl='',2(1x,2d16.8))')                &
     &                                  dnl(l1),bnl(l1)
         write(6,'('' scalar: slmt,clmt='',2(1x,2d16.8))')              &
     &                                  slmt,clmt
        end if

!c     =================================================================
!c     get normalization etc. at sphere boundary........................
!c     non-relat GF in free-space region is non-relat...................
!c     =================================================================
!
       pr = p_in*rScat
       if ( with_sctrn .or. abs(real(pr))>=zero) then
        gpl_s = bjl(l1)/p_in
        fpl_s = (djl(l1) - bjl(l1)/pr)/em
        anorm=-p_in/slmt
       else
!DELETE        write(*,*) 'DEBUG: DO NOT USE WITHOUT REVIEW'
!DELETE        call fstop('Abort in Scalar subroutine')
        gpl_s= ( bjl(l1)+sqrtm1*bnl(l1) )/p_in*cm1l
        fpl_s= ( (djl(l1)-bjl(l1)/pr) +                                 &
     &                           sqrtm1*(dnl(l1)-bnl(l1)/pr) )*cm1l/em
        anorm = slmt + sqrtm1*clmt
        if ( abs(anorm)<tiny(one) ) then
         anorm = czero
        else
         anorm = -p_in/anorm
!       anorm = -pnrel/( slmt + sqrtm1*clmt )
        end if

!      g(jmt)=(clmt*bjl(l1,jmt)-slmt*bnl(l1,jmt))*z0/pnrel
!      anorm=-(bjl(l1,jmt)*cotdel-bnl(l1,jmt))/g(jmt)
      end if

      ! alpha matrix = ratio between Rl=Zl*tl and sph. bes. Jl at origin
      ! for lloyd formula
!DEBUG      tl = cone/(p_in*(dcmplx(0.d0,1.d0)-cotdel))
      ptl_over_sl = slmt*sqrtm1-clmt
      if ( abs(ptl_over_sl) < tiny(one) ) then
       ptl_over_sl = czero
      else
       ptl_over_sl = cone/ptl_over_sl
      end if
!      alpha = (tl*anorm)*(g(1)/(a0*(p_in*r(1))**power))
!      alpha = (-p/slmt*tl)*(g(1)/(a0*(p_in*r(1))**power))   !  power OR (l+1)?

      alpha = czero
      do j1=1,jScat+1
       if ( abs(g(j1)) > tiny(one) ) then
        if ( j1==jScat+1 ) then
         rj = rScat
        else
         rj = r(j1)
        end if
        alpha = g(j1)
        do i=2*1+1,2*l+1,2
         alpha = alpha*i              ! =g()*(2*l+1)!!
        end do
        pr = p_in*r(j1)
!DEBUG        alpha = (alpha/(a0*r(j1)))/(pr**(power-one))*(-p_in*tl/slmt)   ! power OR (l+1)?
        if ( ptl_over_sl==czero ) then
         alpha = czero
        else
         alpha = exp(log(alpha)-log(a0*rj)-(power-one)*log(pr)+         &
     &                                                log(ptl_over_sl))
         if ( abs(alpha) > tiny(one) ) then
          exit
         else
          alpha = czero
         end if
        end if
       end if
      end do

      g(1:jScat+1) = g(1:jScat+1)*anorm   ! j=jScat+1
      f(1:jScat+1) = f(1:jScat+1)*anorm   ! j=jScat+1
      slmt = slmt*anorm
      clmt = clmt*anorm
      g_s = g_s*anorm    ! g_s = g(r=rScat)
      f_s = f_s*anorm    ! f_s = f(jScat+1)

!c
!c non-relat solution
!c
!      clj =-bnl(2,j)*gpl(j) - bnl(1,j)*fpl(j)/p
!      slj =-bjl(2,j)*gpl(j) - bjl(1,j)*fpl(j)/p
!      gpl(j)=clj*bjl(1,j)-slj*bnl(1,j)
!      fpl(j)=-p_in*(clj*bjl(2,j)-slj*bnl(2,j))

!      do j=jScat+1,iend
!         pr = p_in*r(j)
!         gpl(j)=bjl(l1,j)/p
!         fpl(j)=(djl(l1,j) - bjl(l1,j)/pr)/eoc2p1
!      end do
!
!      em2 = cone + 2*(eoc2p1-cone)
      if ( abs(rScat-r(jScat+1))>1.d-7 )  then
       pr=p_in*r(jScat+1)
       call ricbes(lmin,pr,bjl(1:lmin),bnl(1:lmin),                     &
     &                                      djl(1:lmin),dnl(1:lmin))
       g(jScat+1)=(clmt*bjl(l1)-slmt*bnl(l1))/p_in
       f(jScat+1)=                                                      &
     &  (clmt*(djl(l1)-bjl(l1)/pr)-slmt*(dnl(l1)-bnl(l1)/pr)) / em
       if(iswzj.ne.0) then   ! irregular solution
        if ( with_sctrn .or. abs(real(pr))>=zero) then
         gpl(jScat+1) = bjl(l1)/p_in
         fpl(jScat+1) = (djl(l1) - bjl(l1)/pr)/em
        else
         gpl(jScat+1) = (bjl(l1)+sqrtm1*bnl(l1))/p_in
         fpl(jScat+1) = ( (djl(l1) - bjl(l1)/pr) +                      &
     &                           sqrtm1*(dnl(l1)-bnl(l1)/pr) )/em
        end if
       end if
      else
       gpl(jScat+1) = gpl_s
       fpl(jScat+1) = fpl_s
      end if

!DEBUG
!DELETE       j = jScat+1
!DELETE       pr=p_in*r(j)
!DELETE       call ricbes(lmin,pr,bjl(1:lmin),bnl(1:lmin),                     &
!DELETE     &                                      djl(1:lmin),dnl(1:lmin))
!DELETE       g(j)=(clmt*bjl(l1)-slmt*bnl(l1))/p_in
!DELETE       f(j)=(clmt*(djl(l1)-bjl(l1)/pr)-slmt*(dnl(l1)-bnl(l1)/pr)) / em
!DEBUG
!c        --------------------------------------------------------------
      do j=jScat+2,iend    ! outside scattering sphere, non-relat.
       pr=p_in*r(j)
       call ricbes(lmin,pr,bjl(1:lmin),bnl(1:lmin),                     &
     &                                      djl(1:lmin),dnl(1:lmin))
       g(j)=(clmt*bjl(l1)-slmt*bnl(l1))/p_in
       f(j)=(clmt*(djl(l1)-bjl(l1)/pr)-slmt*(dnl(l1)-bnl(l1)/pr)) / em
       if(iswzj.ne.0) then   ! irregular solution
        if ( with_sctrn .or. abs(real(pr))>=zero) then
         gpl(j) = bjl(l1)/p_in
         fpl(j) = (djl(l1) - bjl(l1)/pr)/em
        else
         gpl(j) = (bjl(l1)+sqrtm1*bnl(l1))/p_in
         fpl(j) = ( (djl(l1) - bjl(l1)/pr) +                            &
     &                           sqrtm1*(dnl(l1)-bnl(l1)/pr) ) / em
        end if
       end if
      enddo

!c     =================================================================
      if( iswzj .ne. 0 ) then
!c-----------------------------------------------------------------------
!c
!c  get irregular solution inside m.t. sphere
!c
!        p = prel  or pnrel
!c       minimum allowed step-size: can be zero
        hmin=abserr
!c       specify sub-step tolerance
!        eps=relerr
!c       initialize the count of sub-steps
!c       xss,yss, etc are accumulated only for possible internal debugging purposes
        nss=0
        iflag = 1
!c       specify outward propagation of soln with no asymptotic behaviour factored out
        call set_ode_params(in_out0=0,enrg0=energy)
!c     ------------------------------------------------------------------
!c     get irregular solution inside m.t. sphere
!c     but dont get too near origin because of 1/r^power divergence
!c     ------------------------------------------------------------------
        wrnskn = czero
        ys2(1)=gpl_s
        ys2(2)=fpl_s
        j1min=maxval(jmin)
        do j=jScat,j1min+1,-1
          j1=j+1
          ipot(1)=j
          ipot(2)=j1
          call set_ode_params(ipot0=ipot,rfit0=r(j:j1),                 &
     &                                 zfit0=rv(j:j1),cspl0=cspl(j:j1))
          if ( j==jScat ) then
           x1=rScat
          else
           x1=x2
          end if
          x2=r(j)
          ys1(1:2)=ys2(1:2)
          call odeint2spl(x1,ys1,x2,ys2,iflag,eps,hmin,                 &
     &                                       1,xss,yss,dyss,rvss,errss)
          if(iflag.ne.0) then
            if ( iflag.ne.3 ) then
             write(*,*) ' l=',l,' zed=',zed
             write(*,*) ' j1min=',j1min,' jScat=',jScat
             write(*,*) ' * j=',ipot(1:2)
             write(*,*) ' * x1,x2j=',x1,x2
             write(*,*) ' * ys1=',ys1(1:2)
             write(*,*) ' * rv(j:j1)=',rv(j:j1)
             write(*,*) ' * logdblfact=',logdblfact
             write(*,*) ' * l*log(p_in*r)=',l*log(abs(p_in*r(j)))
             write(*,*) 'odeint2spl error=',iflag
             call fstop(sname//                                         &
     &         ': ODEINT2SPL ERROR')
!            else
!             write(*,*) 'odeint2 warning=',iflag
            end if
          endif
!          mapss(j)=nss
          gpl(j)=ys2(1)
          fpl(j)=ys2(2)
!DEBUG0324
!          if ( l>=30 ) then
!           wrnskn = g(j)*ys2(2)-f(j)*gpl(j)
!           if ( abs((abs(wrnskn)-one)) > 0.05d0  ) then
!            wrnskn = g(j+1)*fpl(j+1)-f(j+1)*gpl(j+1)
!            fpl(j) = (wrnskn + f(j)*gpl(j))/g(j)
!           end if
!          end if
!DEBUG0324
        enddo
        hmin=abserr
!        eps=relerr
        nss=0
        if ( abs(g(j1min+1)) < tiny(one) ) then
         gpl(1:j1min) = czero
         fpl(1:j1min) = czero
        else
!renormalization
         if ( j1min==jScat) then
          renorm = rScat**power
         else
          renorm = r(j1min+1)**power
         end if
         ys2(1)=gpl(j1min+1)*renorm
         ys2(2)=fpl(j1min+1)*renorm
         call set_ode_params(in_out0=-1,enrg0=energy)
         iflag = 1
         if ( j1min==jScat ) then
          x2 = rScat
         else
          x2 = r(j1min+1)
         end if
         j1min = min(j1min,jScat)
         wrnskn = czero
         do j=j1min,i0,-1
          j1=j+1
          ipot(1)=j
          ipot(2)=j1
          call set_ode_params(ipot0=ipot,rfit0=r(j:j1),                 &
     &                                 zfit0=rv(j:j1),cspl0=cspl(j:j1))
          x1=x2
          x2=r(j)
          ys1(1:2)=ys2(1:2)
          renorm = r(j)**power
          if ( power>=l30 ) then
            if ( power*log(r(j)/r(j1min)) < logmin ) then
             gpl(1:j) = czero
             fpl(1:j) = czero
             exit
            end if
          end if
          call odeint2spl(x1,ys1,x2,ys2,iflag,eps,hmin,                 &
     &                                       1,xss,yss,dyss,rvss,errss)
          if(iflag.ne.0) then
            if ( iflag.ne.3 ) then
             write(*,*) ' l=',l,' zed=',zed
             write(*,*) ' j1min=',j1min,' jScat=',jScat
             write(*,*) ' ** j=',j,x1,x2
             write(*,*) ' * g,f=',g(j1),f(j1)
             write(*,*) ' * gpl,fpl=',gpl(j1),fpl(j1)
             write(*,*) ' * ys1=',ys1(1:2)
             write(*,*) ' * rv(j:j1)=',rv(j:j1)
             write(*,*) ' * logdblfact=',logdblfact
             write(*,*) ' * l*log(p_in*r)=',l*log(abs(p_in*r(j)))
             write(*,*) 'odeint2spl error=',iflag
            call fstop(sname//                                          &
     &         ': ODEINT2SPL ERROR')
!            else
!             write(*,*) 'odeint2 warning=',iflag
            end if
          endif
!          mapss(j)=nss
! r-factor
!c         gpl(j)=( r(j)**(-power) )*ys2(1), fpl(j)=( r(j)**(-power) )*ys2(2)

          renorm = r(j)**power
          if ( power<l30 ) then
            gpl(j)=ys2(1)/renorm
            fpl(j)=ys2(2)/renorm
          else
! to prevent overflow
            if (log(abs(ys2(1)))-log(renorm)<logmax ) then
!     &                              .and. abs(g(j))<tiny(one) ) then
!              gpl(j)=ys2(1)/renorm
              if (log(abs(ys2(2)))-log(renorm)<logmax) then
                gpl(j)=ys2(1)/renorm
                fpl(j)=ys2(2)/renorm
              else
!               if ( log(abs(f(j)))+log(abs(gpl(j))) < logmax ) then
!                if ( wrnskn == czero ) then
!                 wrnskn = g(j+1)*fpl(j+1)-f(j+1)*gpl(j+1)
!                end if
!                fpl(j) = wrnskn/g(j) + (f(j)/g(j))*gpl(j)
!               else
                gpl(1:j) = czero
                fpl(1:j) = czero
                exit
!               end if
              end if
            else
              gpl(1:j) = czero
              fpl(1:j) = czero
              exit
            end if
!DEBUG0324
!            wrnskn = g(j)*fpl(j)-f(j)*gpl(j)
!            if ( abs((abs(wrnskn)-one)) > 0.08d0  ) then
!             gpl(1:j) = czero
!             fpl(1:j) = czero
!             exit
!            end if
!DEBUG0324
!DEBUG           overflo = max( log10(abs(ys2(1))),log10(abs(ys2(2))) ) -     &
!DEBUG     &                     power*log10(r(j))
!DEBUG           if(overflo.lt.log10(bignumber)) then
!DEBUG            gpl(j)=ys2(1)/renorm
!DEBUG            fpl(j)=ys2(2)/renorm
!DEBUG           else
!DEBUG!            zlog = log(ys2(1))
!DEBUG!            gpl(j)=bignumber*exp(zlog-dreal(zlog))
!DEBUG!            zlog = log(ys2(2))
!DEBUG!            fpl(j)=bignumber*exp(zlog-dreal(zlog))
!DEBUG            gpl(1:j) = 0
!DEBUG            fpl(1:j) = 0
!DEBUG            exit
!DEBUG           endif
          endif
!
         end do
        end if
!c===============end irregular solution calculation====================
      else
!c
!c  zero out irregular solution arrays for case when iswzj=0
!c
        gpl = czero
        fpl = czero
      end if
!c
!c     ===============================================================
      if ( allocated(rspl) ) deallocate(rspl)
      if ( allocated(cspl) ) deallocate(cspl)

      if ( imsg >= msgmax ) then
       imsg = 0
       msgmax = msgmax/2
      end if
      return
!c
!EOC
      end subroutine scalar1
!
      subroutine frbnsCoeff(l,n,d,a,dclm)
      implicit none
      integer, intent(in) :: l,n
      real(8), intent(in) :: d(0:)
      real(8), intent(out) :: a(0:)
      real(8), intent(in), optional :: dclm

      integer :: i,k,l2
      real(8) :: ai

      if ( n<0 .or. l<0 ) return
      a(0) = one
      if ( n==0 ) return
      if ( present(dclm) ) then
       a(1) = dclm*a(0)/(1+l)
      else
       a(1) = zero
      end if
      do i=2,n
       ai = -l*a(i-2)
       do k=0,i-1
        if ( i-2-k == -1 ) then
         if ( present(dclm) ) then
          ai = ai + dclm*a(k)
         end if
        else
         ai = ai + d(i-2-k)*a(k)
        end if
       end do
       a(i) = (2*ai)/(i*(i+1+2*l))
      end do

      return
      end subroutine frbnsCoeff

!!!!!      end module sctrng
!
!BOP
!!IROUTINE: getRelSSgf
!!INTERFACE:
      subroutine getRelSSgf(energy,lmax,L_limit,ispin,nrelv,so_power,   &
     &                   alat,sublat,iprint,                            &
     &                   green,tab,pterm)
!!DESCRIPTION:
!     to find scalar-relativistic and Dirac single-scattering solution
!     (t-matrix and GF-contributions)
!
!!USES:
      use mecca_types, only : Sublattice
      use gfncts_interface
!DELETE      use pressure, only : allocPbox,deallocPbox,calcRelSSpterm

!!DO_NOT_PRINT
      implicit none
!      complex(8), intent(out), optional ::                              &
!     &                              cotdl(0:lmax,ndcomp,nbasis)         &
!     &                             ,almat(0:lmax,ndcomp,nbasis)         &
!     &              ,zlab(1:size(green,1),0:lmax,ndcomp,nbasis)
!!DO_NOT_PRINT

!!ARGUMENTS:
      complex(8), intent(in) :: energy
      integer, intent(in) :: lmax,L_limit,ispin,nrelv
      real(8), intent(in) :: so_power
      real(8), intent(in) :: alat
      type(Sublattice), intent(in) :: sublat(:)
      integer, intent(in) :: iprint

      complex(8), intent(out) :: green(:,:,:) ! (iprpts,ipcomp,nsubl)
      complex(8), intent(out) :: tab(:,:,:,:)   ! tab(1:,1:2,1:ndcomp,1:nsubl)
      complex(8), intent(out), optional :: pterm(:,:)
!
! a relativistic SS matrix tab is represented by two diagonals (-/+)
!
!!REVISION HISTORY:
! Initial Version - A.S. - 2020
!EOP
!
!BOC
      character(10), parameter :: sname='getRelSSgf'

      integer, external :: indRmesh

! four components of Dirac solution
      complex(8), allocatable :: zlr(:,:,:)    ! regular
      complex(8), allocatable :: jlr(:,:,:)    ! irregular
      complex(8), allocatable :: fzlr(:,:,:)   ! regular
      complex(8), allocatable :: fjlr(:,:,:)   ! irregular
!
      complex(8) :: tmpgrn(1:size(green,1))
      complex(8) :: alpha(0:L_limit,2)
      complex(8) :: cotdl(0:L_limit,2)
      complex(8) :: tmatl(0:L_limit,2)
      complex(8) :: pmom

      integer :: l,l0,lm,l_s,m,nbasis,nd,nk,nsub,ir,is,j,js,kl,ks,l_big
      integer :: st_env

      real(8) :: h
      real(8), allocatable :: xr(:)
      real(8), allocatable :: rr(:)
      real(8), allocatable :: vr(:)
      real(8) :: rIS,rScat
      real(8) :: aj,vi,rm_es
      real(8) :: clight
      real(8), parameter :: cphot = two*inv_fine_struct_const
      complex(8), allocatable :: zj(:),zz(:)
      complex(8), allocatable :: fj(:),fz(:)
      complex(8), allocatable :: gg_factor(:),ztmp(:)
      complex(8) :: cinv,c2inv,cll1,c2me1_inv,c2me2_inv,eoc2p1
      complex(8), parameter :: sqrtm1=dcmplx(zero,one)

      nd = size(green,1)
      allocate(zlr(1:nd,0:L_limit,2))
      allocate(jlr(1:nd,0:L_limit,2))
      allocate(fzlr(1:nd,0:L_limit,2))
      allocate(fjlr(1:nd,0:L_limit,2))
      allocate(xr(nd),rr(nd),vr(nd))
      allocate(zj(nd),zz(nd),fj(nd),fz(nd),gg_factor(nd),ztmp(nd))

      tab = czero

      clight = cphot
      if ( nrelv>1 ) then
        clight = cphot*ten**nrelv
      end if
!c
!c Because t_inv= i*Kappa + Kappa*cotdel, i*Kappa term must
!c cancel i*Kappa piece of g(k).
!c
      pmom = sqrt(2*m_e*energy*(one + energy/(2*m_e*clight**2)))
      pmom = pmom*sign(1.d0,aimag(pmom))
!c
!c     ****************************************************************
!c     set up t-matrix, ZZ, ZJ for each component and  sub-lattice
!c     ****************************************************************
!c
!
      do nsub=1,size(sublat)
        do nk=1,sublat(nsub)%ncomp
          call g_meshv(sublat(nsub)%compon(nk)%v_rho_box,               &
     &                                                 h,xr,rr,ispin,vr)
          nd = min(sublat(nsub)%compon(nk)%v_rho_box%ndrpts,            &
     &                   size(vr))
          rIS = sublat(nsub)%rIS*alat
          rScat = rr(sublat(nsub)%compon(nk)%v_rho_box%jmt)
          if ( ss_type == IS_ss ) then
!             rScat = sublat(nsub)%rIS*alat
             rScat = sublat(nsub)%compon(nk)%rSph
          else if ( ss_type == ASA_ss ) then
!!             rScat = rr(sublat(nsub)%compon(nk)%v_rho_box%jmt)
             rScat = gRasa(nsub,nk)
          else
!             rScat = sublat(nsub)%rIS*alat
             if ( dreal(energy) < zero ) then
!              rScat = sublat(nsub)%rIS*alat
              rScat = sublat(nsub)%compon(nk)%rSph
             else
!              rScat = rr(sublat(nsub)%compon(nk)%v_rho_box%jmt)
              rScat = gRasa(nsub,nk)
             end if
          end if

!TODO: T=0 (l_SS,L_limit) -> (0,lmax+1..Inf)
           l_big = bessel_limit(pmom*rr(nd),L_limit)     !  L_limit
!DEBUG
!           if ( dreal(energy) < zero ) l_big = min(l_big,lmax+lminSS)
!DEBUG
!DEBUGPRINT
           if ( l_big > lmax+lminSS .and. iprint>=0 ) then
            write(6,'(2(1x,a,i3),2x,a,2(1x,ES12.5))') 'l_big=',l_big,   &
     &                              'L_limit=',L_limit,'pmom_rel=',pmom
           end if
!DEBUGPRINT
!DELETE           if ( present(pterm) ) then
!DELETE            pterm(nk,nsub) = czero
!DELETE            call allocPbox(max(lmax,l_big),1,p_data)
!DELETE            p_data%pm = pmom
!DELETE           end if
!c           -------------------------------------------------------
          call diracSS(so_power,clight,                                 &
     &                  sublat(nsub)%compon(nk)%zID,                    &
     &                  lmax,l_big,                                     &
     &                  energy,pmom,                                    &
     &                  zlr,jlr,                                        &
     &                  fzlr,fjlr,                                      &
     &                  cotdl,alpha,                                    &
     &                  rScat,rr(1:nd),vr(1:nd),                        &
     &                  iprint)
!c              -------------------------------------------------------
           do ks=1,2                  ! ks=1 --> sk=+1, ks=2 --> sk=-1
            if ( sk(ks)==1 ) then
             l0=1
             tmatl(0,ks) = czero
            else
             l0=0
            end if
            do l=l0,max(L_limit,lmax)
             if ( cotdl(l,ks) == czero ) then
              tmatl(l,ks) = czero
             else if ( l>l_big ) then
              tmatl(l,ks) = czero
              zlr(1:nd,l,ks) = czero
              jlr(1:nd,l,ks) = czero
              fzlr(1:nd,l,ks) = czero
              fjlr(1:nd,l,ks) = czero
             else
               tmatl(l,ks) = cone/(pmom*sqrtm1-pmom*cotdl(l,ks))
             end if
            end do
           end do
!DELETE           if ( present(pterm) ) then
!===================================================================
!      calculation of pressure term
!-     --------------------------------------------------------------
!DELETE            call calcRelSSpterm(p_data,1,pterm(nk,nsub))
!DELETE            call deallocPbox(p_data)
!DELETE           end if
!c     ==============================================================
!c     calculate the green function for this (energy,species,sub-lat,
!c     spin) :: here is spherical part only..........................
!c     --------------------------------------------------------------
!
! Dirac ss_green
!
            green(1:,nk,nsub) = czero                      ! as in Koelling-Harmon (1977) paper
            cinv = one/clight
            c2inv = cinv**2
            eoc2p1 = 2*m_e + energy*c2inv

            do ks=1,2                        ! ks=1 --> sk=+1, ks=2 --> sk=-1
              is = sk(ks)                     ! k(l)/abs(k(l))
              if ( is==1 ) then
               l0=1
              else
               l0=0
              end if
              tmpgrn(1:nd) = czero
              ztmp(1:nd) = dcmplx(rr(1:nd),zero) +                      &
     &                           (energy*rr(1:nd)-vr(1:nd))*c2inv  !  = 2M*r
              ztmp(1:nd) = (c2inv/(2*m_e)**2) / (ztmp(1:nd)*ztmp(1:nd))
              do l=l0,max(L_limit,lmax)
               if ( is>0 ) then
                kl=l
               else
                kl=l+1
               end if
               tab(1+l,ks,nk,nsub) = tmatl(l,ks)
!               aj = kl-0.5d0         ! j quantum number
               cll1 = dcmplx(l*(l+1))
               gg_factor(1:nd) = cll1 * ztmp(1:nd)

               fz(1:nd) = fzlr(1:nd,l,ks) * cinv
               fj(1:nd) = fjlr(1:nd,l,ks) * cinv
               if ( tmatl(l,ks)==czero ) then
                zj(1:nd) = -zlr(1:nd,l,ks)*jlr(1:nd,l,ks)
                zj(1:nd) = zj(1:nd)*(cone+gg_factor(1:nd))
                tmpgrn(1:nd) = zj(1:nd) - fz(1:nd) * fj(1:nd)
               else
                zz(1:nd) =  zlr(1:nd,l,ks) *                            &
     &            (tmatl(l,ks)*zlr(1:nd,l,ks) - jlr(1:nd,l,ks))
                zz(1:nd) = zz(1:nd) * (cone + gg_factor(1:nd))
                tmpgrn(1:nd) = zz(1:nd) + fz(1:nd) *                    &
     &            (tmatl(l,ks)*fz(1:nd) - fj(1:nd))
               end if
               green(1:nd,nk,nsub) = green(1:nd,nk,nsub)                &
     &                             + kl*tmpgrn(1:nd)
              end do
            end do

            if(iprint.eq.2) then
              write(6,'(//)')
             write(6,'(i4,3e12.4,'' #i,r(i),green(i) getRelSSgf'')')    &
     &             (ir,rr(ir),green(ir,nk,nsub),ir=1,nd,1)
            endif
!DELETE            if ( present(zlab) ) then
!DELETE             zlab(1:nd,0:lmax,nk,nsub) = zlr(1:nd,0:lmax)
!DELETE             zlab(nd+1:,0:lmax,nk,nsub) = czero
!DELETE            end if
!DELETE          endif
        enddo
      enddo

      deallocate(zlr,jlr,fzlr,fjlr)
      deallocate(xr,rr,vr)
      deallocate(zj,zz,fj,fz,gg_factor,ztmp)
!c           -----------------------------------------------------------
      return
!
!EOC
      end subroutine getRelSSgf
!
!
!BOP
!!IROUTINE: diracSS
!!INTERFACE:
      subroutine  diracSS(so_power,clight,                              &
     &                  ized,                                           &
     &                  l_ms,Lmax,                                      &
     &                  energy,p_in,                                    &
     &                  zlr,jlr,                                        &
     &                  fzlr,fjlr,                                      &
     &                  cotdel,almat,                                   &
     &                  rScat,r,rv,                                     &
     &                  iprint)
!!DESCRIPTION:
!   this subroutine solves Dirac relativistic equations for complex
!   energies using Calogero's phase method
!   (single-scattering problem)

       use mecca_constants, only : lminSS
!!DO_NOT_PRINT
      implicit none
!!DO_NOT_PRINT

!!ARGUMENTS:
      real(8), intent(in) :: so_power,clight
      integer, intent(in) :: ized,l_ms,Lmax   ! Lmax >= l_ms!
      complex(8), intent(in) :: energy,p_in
      complex(8), intent(out) :: zlr(:,0:,:)  ! (iprpts,0:lmax,2)
      complex(8), intent(out) :: jlr(:,0:,:)  ! (iprpts,0:lmax,2)
      complex(8), intent(out) :: fzlr(:,0:,:)  ! (iprpts,0:lmax,2)
      complex(8), intent(out) :: fjlr(:,0:,:)  ! (iprpts,0:lmax,2)
      complex(8), intent(out) :: cotdel(0:,:)
      complex(8), intent(out) :: almat(0:,:)
      real(8), intent(in) :: rScat
      real(8), intent(in) :: r(:)
      real(8), intent(in) :: rv(:)
      integer, intent(in) :: iprint
!!REVISION HISTORY:
! Initial - A.S. - 2020
!EOP
!
!BOC
!c
      complex(8), allocatable :: ztmp(:)
      complex(8), allocatable :: g(:)
      complex(8), allocatable :: f(:)
      complex(8), allocatable :: gpl(:)
      complex(8), allocatable :: fpl(:)
!      complex(8) :: prel,pnrel
      complex(8) :: anorm

      real(8), parameter :: eps_cond = 1.d-6

      integer :: i,is,kl,l,l1,j
      real(8) :: aj
      integer :: iend,jScat,iex,l_s
      integer, save :: l0=-1
      logical :: with_sctrn,ws
      complex(8) :: fl
      integer, external :: indRmesh
!c     +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
      character(10), parameter :: sname='diracSS'
!c     +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

      allocate(ztmp(size(r)))
      allocate(g(size(r)),f(size(r)),gpl(size(r)),fpl(size(r)))
!c
!c     Gets radial integrals, t-matrix.
!c
!c     In this version rv is r * v
!c
!c      returns:
!c            zlr         zl(r) properly normalized..............
!c            jlr         jl(r) properly normalized..............
!c
!c     set c for doing scalar-relativistic or non-relativistic calc.
!c     .....c = speed of light in proper units..........
!c     Ryd units: m=1/2  hbar=1 c=2*inverse of fine structure const.
!c
!c     Solving a relativistic (so_power=1), scalar-relativistic (so_power=0) or non-relativistic
!c     problem.   If non-relativistic, then set e=e-m_e equal to e
!c     (where m_e = electron mass) and 1/c is set to ~zero to make
!c     relativistic terms zero.
!c
!c     Using Ricatti Bessel fcts. Need only l=0 and l=1 to get wave-fcts.
!c
!c     To get physical phase-shifts and normalization, need get Rl in
!c     terms of spherical bessel for all l's at the boundary of sphere.
!c     This is properly done in SCALAR, see clmt and slmt.
!c
!c     With M(r)= [1 + (e-v(r))*c2inv]
!c     Obtain r*(wave-fct.):   regular     g=r*R and   f=r*R'/M
!c                           irregular   gpl=r*R and fpl=r*R'/M
!c
!c     NOTE: ====>   need to get gderiv for mom. matrix elements
!c
!c     *****************************************************************
!c
      l_s = min(l_ms,Lmax)     ! l_s - max l with scattering bound.cond.
                               ! "free space" (atomic) bound.cond is used
                               ! for l from l_ms < l <= Lmax
      if(iprint.ge.2) then
         write(6,'('' diracSS: so_power,clight='',f4.2,d13.4)')         &
     &                                                   so_power,clight
         write(6,'('' diracSS:        l_s,Lmax='',2i4)') l_s,Lmax
         write(6,'('' diracSS: energy,p_in='',4d12.4)') energy,p_in
         write(6,'('' diracSS:      rScat='',f8.5)') rScat
      endif
!c     =================================================================

      iend=min(size(r),size(rv))

      zlr(1:iend,0:Lmax,1:2) = czero
      jlr(1:iend,0:Lmax,1:2)  = czero
      fzlr(1:iend,0:Lmax,1:2) = czero
      fjlr(1:iend,0:Lmax,1:2) = czero
      ztmp(1:iend) = cone/r(1:iend)
      almat = czero
      cotdel = czero

!!      IF ( iend .NE. size(r) ) THEN
!!       if ( l0<0 ) then
!!        write(6,*) ' DEBUG: solveSS is used'
!!        l0=0
!!       end if
!!            call solveSS(0,clight,                                      &
!!     &                  ized,                                           &
!!     &                  Lmax,                                           &
!!     &                  energy,p_in,                                    &
!!     &                  zlr(1:iend,0:Lmax,2),jlr(1:iend,0:Lmax,2),      &
!!     &                  fzlr(1:iend,0:Lmax,2),fjlr(1:iend,0:Lmax,2),    &
!!     &                  cotdel(0:Lmax,2),almat(0:Lmax,2),               &
!!     &                  rScat,rScat,r(1:iend),rv(1:iend),               &
!!     &                  iprint,l_shft=l_s)
!!         zlr(1:iend,0:Lmax,1) = zlr(1:iend,0:Lmax,2)
!!         jlr(1:iend,0:Lmax,1) = jlr(1:iend,0:Lmax,2)
!!         fzlr(1:iend,0:Lmax,1) = fzlr(1:iend,0:Lmax,2)
!!         fjlr(1:iend,0:Lmax,1) = fjlr(1:iend,0:Lmax,2)
!!         cotdel(0:Lmax,1) = cotdel(0:Lmax,2)
!!         almat(0:Lmax,1) = almat(0:Lmax,2)
!!
!!      ELSE

!c     =================================================================

       if ( l_s<0 .or. atom_ss ) then
!DELETE        ws = .false.
!        lss0 = 0      ! if ( l .ge. lss0 ) atomic scattering is applied
!DEBUG?
!       else if ( dreal(energy)<zero ) then
!        ws = .false.
!        write(6,*) 'ENERGY<0, with_scat=.false.'
!DEBUG?
       else
!DELETE        ws = .true.
!        lss0 = l_s+lminSS+1
!        lss0 = Lmax+1
        if ( l0<0 ) then
         l0=0
        end if
       end if

       ws = .false.

      jScat = indRmesh(rScat,iend,r)
!c     solve Dirac equation.
      do i=1,2
       is = sk(i)               ! kl/abs(kl)
       if ( is==1 ) then
        l0 = 1
       else
        l0 = 0
       end if
       do l=l0,Lmax
         with_sctrn = ws   ! .and. l<lss0
         aj = l - 0.5d0*is        ! j quantum number
         kl = nint(aj+0.5d0)*is   ! k quantum number
!c        ------------------------------------------------------------
         call dirac(so_power,clight,                                    &
     &               ized,                                              &
     &               kl,                                                &
     &               g,f,gpl,fpl,                                       &
     &               cotdel(l,i),almat(l,i),                            &
     &               energy,p_in,                                       &
     &               rv,r,rScat,jScat,iend,                             &
     &               1,with_sctrn,                                      &
     &               iprint)
         zlr(1:iend,l,i) = zlr(1:iend,l,i) +   g(1:iend)*ztmp(1:iend)
         jlr(1:iend,l,i) = jlr(1:iend,l,i) + gpl(1:iend)*ztmp(1:iend)
         fzlr(1:iend,l,i) = fzlr(1:iend,l,i) + f(1:iend)*ztmp(1:iend)
         fjlr(1:iend,l,i) = fjlr(1:iend,l,i)+fpl(1:iend)*ztmp(1:iend)
!DEBUGPRINT
!!         fl = -cone/(sqrtm1-cotdel(l,i))
!!         write(6,'(i2,1x,i3,1x,4(1x,2e16.8),L6,'' ks,l,cotdl,alpha'')') &
!!     &         i,l,cotdel(l,i),cmplx(dreal(fl),dimag(fl)-0.5d0),        &
!!     &      cmplx(dreal(fl)**2+(dimag(fl)-0.5d0)**2,abs(fl)),almat(l,i),&
!!     &         with_sctrn
!         do j=1,iend
!         write(100*(l0+1)+l,'(i5,1x,e16.8,4(1x,2(1x,e16.8)))')
!     &      j,r(j),zlr(j,l,i),jlr(j,l,i),fzlr(j,l,i),fjlr(j,l,i)
!         end do
!DEBUGPRINT
       enddo
      enddo
!?
      zlr(1:iend,0,1) = zlr(1:iend,0,2)
      jlr(1:iend,0,1) = jlr(1:iend,0,2)
      fzlr(1:iend,0,1)= fzlr(1:iend,0,2)
      fjlr(1:iend,0,1)= fjlr(1:iend,0,2)

!!      END IF

      return
!c
!EOC
      end subroutine diracSS
!
!BOP
!!IROUTINE: dirac
!!INTERFACE:
      subroutine dirac(so_power,clight,                                 &
     &                  ized,                                           &
     &                  kl,                                             &
     &                  g,f,gpl,fpl,                                    &
     &                  cotdel,alpha,                                   &
     &                  energy_in,p_in,                                 &
     &                  rv_in,r,rScat,jScat,iend,                       &
     &                  iswzj,with_sctrn,                               &
     &                  iprint)
!!DESCRIPTION:
! calculates  regular/irregular solutions (and some other values) for
! radial Dirac equation
!
!!USES:
      use stiffode, only : set_ode_params,odeint2,odeint2spl

!!DO_NOT_PRINT
      implicit none
!!DO_NOT_PRINT

!!ARGUMENTS:
      real(8), intent(in) :: so_power,clight ! so_power = 1: full spin-orbit, so_power = 0 - no spin-orbit effect
!                                            ! so_power =-1: scal-relat version (kl.ge.0, so_power=0 + SR-boundary condition)
      integer, intent(in) :: ized,kl
      complex(8), intent(out) :: g(:)
      complex(8), intent(out) :: f(:)             ! part of "small" component, c*phi
      complex(8), intent(out) :: gpl(:)
      complex(8), intent(out) :: fpl(:)           ! part of "small" component, c*phi
      complex(8), intent(out) :: cotdel, alpha
      complex(8), intent(in) :: energy_in,p_in
      real(8), intent(in) :: rv_in(:)
      real(8), intent(in) :: r(:)
      real(8), intent(in) :: rScat
      integer, intent(in) :: jScat                ! jScat = indRmesh(rScat,iend,r)
      integer, intent(in) :: iend,iswzj,iprint
      logical, intent(in) :: with_sctrn
!!REVISION HISTORY:
! Adapted - A.S. - 2020
!!REMARKS:
! based on Calegero Method
!EOP
!
!BOC
!      real*8 fact(4),factl1
      real(8) :: zed
      real(8) :: v0
      real(8) :: cinv
      real(8) :: c2inv
      real(8) :: power
!c
      complex(8) :: bjl(abs(kl)+2),djl(size(bjl))
      complex(8) :: bnl(size(bjl)),dnl(size(bjl))
      complex(8) :: twoM(0:iend),twoMr(0:iend)
      complex(8) :: gpl_s,fpl_s,g_s,f_s
      complex(8) :: anorm,zlog

      integer :: nrel, j
      integer :: i0,j0, l, lmin

      complex(8) :: energy,eoc2p1,v0me
      complex(8) :: enr,emvr,emr,emr_s,em,em2
      complex(8) :: a0,a1p,a1,a2p,a2,a3p,a3
      complex(8) :: pr,slmt,clmt,tl,ptl_over_sl
      complex(8) :: dg,a11,a12,a21,a22
      real(8)    :: z0,fz2oc2

      real(8) :: rv(0:iend)
      real(8), parameter :: relerr=1.d-8,abserr=tiny(relerr),pi2=2*pi
      integer iflag

      real(8) :: rj,renorm,renorm2,overflo,cl,sl,clph,slph
      integer l1,j1min,j2min,jmin(2)

      real(8) :: rvj
!c     ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
      integer, parameter :: icmax=5
      real(8), parameter :: half=one/two
      real(8), parameter :: tole=(one/ten)**10
      character(10), parameter :: sname='dirac'
      complex(8), parameter :: sqrtm1=dcmplx(zero,one)
!CAB
      real(8), parameter :: bignumber=sqrt(huge(1.d0))
      real(8), parameter :: smallnumber=one/bignumber
!      real(8), parameter :: logmax=log(huge(1.d0))
!      real(8), parameter :: logmin=log(tiny(1.d0))
      real(8), parameter :: logmax=log(bignumber)
      real(8), parameter :: logmin=log(smallnumber)
      real(8) :: logdblfact
      real(8) :: r_zero
      real(8) :: vr0
      complex(8) :: alphlog,cm1l
!CAB
!c     stiff ode solver are for large kl
      real(8) :: eps
      real(8) :: hmin
      real(8) :: x1,x2
      real(8) :: xtmp
      real(8) :: tmpr(2),tmprv(2)
      real(8), allocatable :: rspl(:),cspl(:)
      real(8) :: dc(0:10),ac(0:10)
      complex(8) :: ys1(2)
      complex(8) :: ys2(2)
!DEBUG
      complex(8) :: wrnskn
!DEBUG

      complex(8), external :: ylag_cmplx
      real(8) :: cent,so_coef,fl_coef
      logical :: point_nucleus

      integer ipot(2)

      integer, external :: indRmesh
      real(8), external :: SPL3

      integer i,j1,nc
      integer, save :: imsg = 0, msgmax = 64
!c     sub-step information
      integer nss
!!      real(8) :: xss(40*size(r))
!!      real(8) :: rvss(40*size(r))
!!      complex(8) :: yss(2,40*size(r))
!!      complex(8) :: dyss(2,40*size(r))
!!      complex(8) :: errss(2,40*size(r))
      real(8) :: xss(1)
      real(8) :: rvss(1)
      complex(8) :: yss(2,1)
      complex(8) :: dyss(2,1)
      complex(8) :: errss(2,1)
!c
!c     ****************************************************************** !*
!c     sets up to solve the non- and dirac-relativistic equation.
!c     based on Calegero Method..............DDJ & FJP  July 1991
!c     uses Ricatti-Bessel fcts. for solution
!c     ****************************************************************** !*
!c
      g = czero
      f = czero
      gpl = czero
      fpl = czero
      alpha = czero
      cotdel = czero

      if ( kl>=0 ) then
       l = kl
!       sk = 1
      else
       l    = -(kl+1)
!       sk = -1
      end if

      so_coef = zero
      fl_coef = zero
      energy = energy_in
      nrel = 0
      if ( clight >= 1.d16 ) nrel = 1
      if(iprint.ge.6) then
         write(6,'('' in DIRAC iprint '',i5)') iprint
         write(6,'('' dirac: l,iswzj='',2i5)') l,iswzj
         write(6,'('' dirac: nrel,clight='',i5,d12.4)') nrel,clight
         write(6,'('' dirac: energy='',2d16.8)') energy
         write(6,'('' dirac:   p_in='',2d16.8)') p_in
      end if
      if ( with_sctrn ) then
       eps=relerr
      else
       eps=min(1.d-5,relerr*1.d2)
      end if
!     jScat = indRmesh(rScat,iend,r)

      if(nrel.le.0) then
       cinv=one/clight
       c2inv=cinv*cinv
      else
       cinv = zero
       c2inv= zero
      end if

!c     =================================================================
!c     cannot solve for the real part of energy equal to 0.0!
!      if( abs(energy) .lt. smallnumber ) then
!        energy = energy_in*(one + smallnumber/(abs(energy)+1.d-6))
!        if ( iprint>=0 ) then
!         write(6,'('' dirac:: energy is about zero, replaced by'' &
!     &             '' ~1.e-6'')')
!!c        call p_fstop(sname)
!        end if
!      end if
!
!c     =================================================================
      cm1l = exp(sqrtm1*pi*mod(l,2))   ! (-1)**l
      zed=ized
!C...............................................................
!c
      r_zero = min(r(1)/100,1.d-6)
      call twoZ(zed,zero,1,vr0)
      if ( ized==0 ) then
       point_nucleus = .false.
       vr0 = rv_in(1)*(r_zero/r(1))
      else
       point_nucleus = gPointNucleus()
       if ( .not. point_nucleus ) then
        call twoZ(zed,r_zero,1,vr0)
       end if
      end if
!
      twoMr(0) = czero
      twoMr(1:iend) = dcmplx(r(1:iend),zero) +                          &
     &                           (energy*r(1:iend)-rv_in(1:iend))*c2inv   ! emr(r) = 2M*r

      rv(0) = vr0
      rv(1:iend) = rv_in(1:iend)
      rv = -0.5d0*rv   !! to be compatible with units used in stiffode module

!DEBUG-TODO: to calculate spline for V (and r*V') once per iteration (for each spin)

      allocate(rspl(0:size(rv)),cspl(0:size(rv)))
      rspl(0) = zero
      rspl(1:jScat+1) = r(1:jScat+1)
      call  ZSPL3(jScat+2,rspl(0:jScat+1),rv(0:jScat+1),cspl(0:jScat+1))

      if(nrel.le.0) then
       if(ized.lt.0) then
        fz2oc2=zero
       else
        fz2oc2=(abs(vr0)*cinv)**2
       end if
       eoc2p1 = cone+energy*c2inv
      else
       eoc2p1 = cone
       fz2oc2=zero
      end if
!
      ! (2l+1)!! = 2/sqrt(pi)*2^l*Gamma(l+3/2);  logdblfact = log( (2l+1)!! )
      logdblfact = 20 + (l+1)*(log(2.d0*l+3.d0))   ! approx. for large l
!
      j0 = jScat+1
      do j=1,jScat
       if ( l*log(abs(p_in)*r(j)) - logdblfact > logmin ) then
        j0 = j
        exit
       end if
      end do
      if ( j0 > jScat ) then    ! solution is outside the scattering region
!       write(6,*) ' WARNING z=',ized,' l=',l,' p=',p
!       write(6,*) ' l*log(p_in*r)=',l*log(abs(p_in)*rScat)
!       write(6,*) ' logdblfact=',logdblfact
!       write(6,*) ' logmin=',logmin
       return
      end if
      logdblfact = (l+1)*(log(2.d0*l+3.d0)) ! approx. for large l

      call set_ode_params(c2inv0=c2inv)

      if ( point_nucleus ) then
       v0me= zero-energy
      else
       v0me= vr0/r_zero-energy
      end if
!
      a0=cone

      if(nrel.le.0) then
!c    ==============================================================
!c   relativistic (Dirac) case......................................

        cent = dble(kl*(kl+1))
        if ( so_power==SR ) then
         if ( kl<0 ) call fstop('ERROR: L must be non-negative')
         so_coef = zero
!c        scalar-relativistic case......................................

         power=sqrt( cent + one - fz2oc2 )   ! ddj's  MECCA definition

        else
         if ( kl==0 ) return
         so_coef = so_power / kl
         power=dsqrt( kl*kl - fz2oc2 )
        end if
        fl_coef = (kl+1)*cinv
        if(ized.lt.0) then
         call p_fstop(sname//' Z<0')
!         a1 = czero
!         a2 = v0me/(4*power+4)
!         a3 = czero
        else
         z0 = (2*zed)
         a1p= z0/(2*power-1)
         a2p = a1p * z0/(2*(2*power))
         a3p = a2p * z0/(3*(2*power+1)) ! czero
         a1 = a0*a1p
         a2 = a0*a2p
         a3 = a0*a3p
        end if
      else
!c        ==============================================================
!c        non-relativistic case......................................
!        power= l1
!        cent = l*(l+1)
        power= abs(kl)
        cent = kl*(kl+1)
!c
!c        a0 is arbitrary, but this make Rl a spherical Bessel at origin.
!c                        R_l=x*j_l(x)= x*x**l/(2l+1)!!
!c        a0= p**l1/factl1
!c        a0= p**power/factl1
        if(ized.lt.0) then
         call p_fstop(sname//' Z<0')
!         a1 = czero
!         a2p= v0me/(4*l+6)
!         a2 = a0*a2p
!         a3 = czero
        else
!
         a1p= (-2*zed)/(2*power-1)
         a2p = a1p * (-2*zed)/(2*(2*power))
         a3p = a2p * (-2*zed)/(3*(2*power+1))
!
         a1 = a0*a1p
         a2 = a0*a2p
         a3 = a0*a3p
        end if
      end if

      call set_ode_params(cent0=cent,power0=power,sp_orb0=so_coef)

      l1 = l+1

       call set_ode_params(in_out0=0,enrg0=energy)
       i0 = 1
       if ( ized == 0 ) then
        pr = sqrt(-v0me*((2*m_e) - v0me*c2inv))
        pr = pr*sign(1.d0,aimag(pr)) * r(1)
        call ricbes(1,pr,bjl,bnl,djl,dnl)
        g(1) = a0*bjl(1)
        f(1) = a0*(djl(1)-bjl(1)/pr)/((2*m_e) - v0me*c2inv)
       else
        hmin=abserr
        nss=0
        iflag = 1
        ipot(1)=1
        ipot(2)=2
        tmpr(1) = 0.9d0*r(i0)
        tmpr(2) = r(i0)
        call twoZ(zed,tmpr,2,tmprv)
        tmprv(1:2) = -0.5d0*tmprv(1:2)
        call set_ode_params(ipot0=ipot,rfit0=tmpr(1:2),zfit0=tmprv(1:2))
        x1=tmpr(1)
        x2=tmpr(2)
        ys1(1)= a0 + x1*(a1 + x1*(a2 + a3*x1))
        dg = a1 + two*x1*a2+three*x1**2*a3
        rvj = two*tmprv(1)
        enr  = x1*energy
        emvr = c2inv*(enr-rvj)               ! (em-1)*r
        emr = x1 + emvr                      ! r+(e*r-rv)*c2inv
        a11 = one/x1
        a12 = emr/x1    ! mass
        ys1(2) = ( dg-a11*ys1(1))/a12
        ys1 = ys1*x1
!
        call odeint2(x1,ys1,x2,ys2,iflag,eps,hmin,                      &
     &                                       1,xss,yss,dyss,rvss,errss)
!for detail info use:     &          size(xss),xss,yss,dyss,rvss,errss,nss)
        if(iflag.ne.0) then
         if ( iflag.ne.3 ) then
          write(*,*) ' odeint2 error (boundary r=0)=',iflag
          call fstop(sname//': ODEINT2 ERROR')
         else
          if ( imsg<=msgmax ) then
           write(6,*) ' WARNING: odeint2 error (boundary r=0)=',iflag
          if ( imsg==msgmax )                                           &
     &       write(6,*) 'LIMIT NUMBER OF WARNINGS: ',msgmax
          end if
          imsg = imsg + 1
         endif
        endif
!
        g(1)=ys2(1)/x2
        rvj = two*tmprv(2)
        enr  = x2*energy
        emvr = c2inv*(enr-rvj)               ! (em-1)*r
        emr = x2 + emvr                      ! r+(e*r-rv)*c2inv
        f(1)=ys2(2)/x2                       ! add (kl+1)*g(1)/twoMr(1) in the end
       end if
!
!c     =================================================================
!c     regular solution and phase-shifts of dirac relativistic eqs.
!c
      hmin=abserr
!      eps=relerr
      nss=0
      iflag = 1
      j1 = i0
      a0 = 1.d0 / max(abs(g(j1)),abs(f(j1)))
      g(1:j1) = g(1:j1) * a0
      f(1:j1) = f(1:j1) * a0
      ys2(1)=g(j1)
      ys2(2)=f(j1)
      call set_ode_params(in_out0=1,enrg0=energy)

      j1min = jScat+1
      x2 = r(i0)
      do j=i0+1,jScat+1
        j1=j-1
!c       pass info thru common to potential interpolation routines
        ipot(1)=j1
        ipot(2)=j
          call set_ode_params(ipot0=ipot,rfit0=r(j1:j),                 &
     &                                 zfit0=rv(j1:j),cspl0=cspl(j1:j))
        x1=x2
        ys1(1:2)=ys2(1:2)
        if ( j==jScat+1 ) then
         x2 = rScat
        else
         x2=r(j)
        end if
        call odeint2spl(x1,ys1,x2,ys2,iflag,eps,hmin,                   &
     &                                       1,xss,yss,dyss,rvss,errss)
        if(iflag.ne.0) then
         if ( iflag.ne.3 ) then
          write(*,*) ' odeint2spl error (regular sol.)=',iflag
             write(*,*) ' j=',j
          call fstop(sname//': ODEINT2SPL ERROR')
         else
          if ( imsg<=msgmax ) then
           write(6,*) ' WARNING: odeint2spl error (regular sol.)=',iflag
           if ( imsg==msgmax )                                          &
     &       write(6,*) 'LIMIT NUMBER OF WARNINGS: ',msgmax
          end if
          imsg = imsg + 1
         endif
        endif
!        mapss(j)=nss
        g(j)=ys2(1)
        f(j)=ys2(2)

!c     propagate regular solution with r^power factored out
!c     until the oscillatory region
        if ( j1>i0+l ) then
          if ( abs(p_in)*r(j)>one .or. abs(g(j)) < abs(g(j1)) ) then
           j1min = j
           exit
          end if
!         end if
        end if
        if ( power*log(r(j)/r(i0)) > 0.6d0*logmax ) then
          j1min = j-1
          exit
        end if
        if ( j==j0 ) then
         j1min = j
         exit
        end if
      end do

!c     renormalize the solution
      renorm = one/abs(g(j1min))
      renorm = renorm*exp(0.5d0*logmin)
      a0 = a0*renorm
      g(1:j1min) = renorm*g(1:j1min)
      f(1:j1min) = renorm*f(1:j1min)

      if ( j1min == jScat+1 ) then
       rj = one/rScat             ! r(jScat) < rScat <= r(jScat+1)
      else
       rj = one/r(j1min)
      end if
      do j=1,j1min
        renorm = (r(j)*rj)**power
        g(j)= renorm*g(j)
        f(j)= renorm*f(j)
      enddo
      jmin = j1min
      renorm2 = one
      j2min = j1min
      if ( power >= l30 ) then
       do j=j1min+1,jScat+1
        if ( power*log(r(j)/r(j1min)) > 0.8d0*logmax ) then
          j2min = j-1
          exit
        end if
       end do
      end if
      if ( j2min > j1min ) then
       renorm2 = exp(0.5d0*logmin)
       a0 = a0*renorm2
       g(1:j1min) = renorm2*g(1:j1min)
       f(1:j1min) = renorm2*g(1:j1min)
       jmin(2) = j2min
       nss=0
       j1 = j1min
       ys2(1)=g(j1)
       ys2(2)=f(j1)
       call set_ode_params(in_out0=0,enrg0=energy)
       iflag = 1
       x2 = r(j1min)
       do j=j1min+1,j2min
        j1=j-1
!c       pass info to potential interpolation routines
        ipot(1)=j1
        ipot(2)=j
          call set_ode_params(ipot0=ipot,rfit0=r(j1:j),                 &
     &                                 zfit0=rv(j1:j),cspl0=cspl(j1:j))
        x1=x2
        x2=r(j)
        ys1(1:2)=ys2(1:2)
        call odeint2spl(x1,ys1,x2,ys2,iflag,eps,hmin,                   &
     &                                       1,xss,yss,dyss,rvss,errss)
        if(iflag.ne.0) then
         if ( iflag.ne.3 ) then
             write(*,*) ' odeint2spl error (regular sol.)=',iflag
             write(*,*) ' j1,j=',j1,j,' l=',l,' z=',ized
             write(*,'(a,(1x,2es17.7))') ' * rv(j1:j)=',rv(j1:j)
             write(*,'(a,3(1x,es15.7))') ' * x1,x2,rScat=',x1,x2,rScat
             write(*,'(a,2(1x,2es17.7))') ' * ys1=',ys1
             write(*,'(a,2(1x,2es17.7))') ' * ys2=',ys2
             write(*,*) ' overflo1=', log(abs(g(j1)))
             write(*,*) ' overflo2=', log(abs(f(j1)))
             write(*,*) ' logmax=',logmax
          call fstop(sname//': ODEINT2SPL ERROR')
         else
          if ( imsg<=msgmax ) then
           write(6,*) ' WARNING: odeint2spl error (regular sol.)=',iflag
           if ( imsg==msgmax )                                          &
     &       write(6,*) 'LIMIT NUMBER OF WARNINGS: ',msgmax
          end if
          imsg = imsg + 1
         endif
        endif
        g(j)=ys2(1)
        f(j)=ys2(2)
       end do
       renorm2 = one/abs(g(j2min))
       renorm2 = renorm2*exp(0.5d0*logmin)
       a0 = a0*renorm2
       g(1:j2min) = renorm2*g(1:j2min)
       f(1:j2min) = renorm2*f(1:j2min)
      end if
!c     ------------------------------------------------------------------
!c     propagate regular solution without r^power factored out
!c     out to the V=0 region
!c     ------------------------------------------------------------------

!c       minimum allowed step-size: can be zero
        hmin=abserr
!c       specify sub-step tolerance
!        eps=relerr
!c       initialize the count of sub-steps
!c       xss,yss, etc are accumulated only for possible internal debugging purposes
        nss=0
!c       pass complex energy to subroutine thru common
      j1min = maxval(jmin)
      j1 = j1min
      ys2(1)=g(j1)
      ys2(2)=f(j1)
      call set_ode_params(in_out0=0,enrg0=energy)
      iflag = 1
      if ( j1min == jScat+1 ) then
       x2 = rScat
      else
       x2 = r(j1min)
      end if
      do j=j1min+1,jScat+1
        j1=j-1
!c       pass info to potential interpolation routines
        ipot(1)=j1
        ipot(2)=j
          call set_ode_params(ipot0=ipot,rfit0=r(j1:j),                 &
     &                                 zfit0=rv(j1:j),cspl0=cspl(j1:j))
        x1=x2
        if ( j==jScat+1 ) then
         x2 = rScat
        else
         x2=r(j)
        end if
        ys1(1:2)=ys2(1:2)

        call odeint2spl(x1,ys1,x2,ys2,iflag,eps,hmin,                   &
     &                                       1,xss,yss,dyss,rvss,errss)

        if(iflag.ne.0) then
         if ( iflag.ne.3 ) then
             write(*,*) ' odeint2spl error (regular sol.)=',iflag
             write(*,*) ' j1,j=',j1,j,' l=',l,' z=',ized
             write(*,'(a,(1x,2es17.7))') ' * rv(j1:j)=',rv(j1:j)
             write(*,'(a,3(1x,es15.7))') ' * x1,x2,rScat=',x1,x2,rScat
             write(*,'(a,2(1x,2es17.7))') ' * ys1=',ys1
             write(*,'(a,2(1x,2es17.7))') ' * ys2=',ys2
             write(*,*) ' overflo1=', log(abs(g(j1)))
             write(*,*) ' overflo2=', log(abs(f(j1)))
             write(*,*) ' logmax=',logmax
             if ( iflag == 1 ) then
             write(*,*) ' maxss=',size(xss),' errss=',errss(:,nss-1:nss)
             end if
          call fstop(sname//': ODEINT2SPL ERROR')
         else
          if ( imsg <= msgmax ) then
           write(6,*) ' WARNING: odeint2spl error (regular sol.)=',iflag
           if ( imsg==msgmax )                                          &
     &       write(6,*) 'LIMIT NUMBER OF WARNINGS: ',msgmax
          end if
          imsg = imsg + 1
         end if
        endif
!        mapss(j)=nss
        g(j)=ys2(1)   !  if j=jScat+1, it is for r=rScat, not for r(j)
        f(j)=ys2(2)   !
      enddo

      renorm = cone / abs(g(jScat+1))
      a0 = a0*renorm
      g(1:jScat+1) = renorm*g(1:jScat+1)
      f(1:jScat+1) = renorm*f(1:jScat+1)
      g_s = g(jScat+1)   ! g_s = g(r=rScat)
      f_s = f(jScat+1)
!c     =================================================================
!c     At R, need spherical Bessel's for all l to get normalization and
!c     physical phase-shifts to determine cl and sl from
!c     g=r*R=r*( cl*jl - sl*nl ) and f=r*R'/M + r*R*(kl+1)/(2*M*c*r) for all l's.
!c     Modify expression to account for difference between spherical and
!c     Ricatti-Bessel fcts.               Recall: R=g/r and dR=f*em/r
!c     =================================================================
      lmin=max(2,l1)
      pr=p_in*rScat
!c     -----------------------------------------------------------------
      call ricbes(lmin,pr,bjl(1:lmin),bnl(1:lmin),                      &
     &                                      djl(1:lmin),dnl(1:lmin))
!c
!c  ==================================================================
      em = eoc2p1                       ! to match free-space solution
      emr_s=ylag_cmplx(rScat,r(jScat-1:jScat+1),twoMr(jScat-1:jScat+2), &
     &                                                         0,3,4,j)
!c
!c  sl and cl at the sphere radius
!c
      slmt= (p_in*djl(l1)-bjl(l1)/rScat)*g_s - bjl(l1)*f_s*em
      clmt= (p_in*dnl(l1)-bnl(l1)/rScat)*g_s - bnl(l1)*f_s*em
! cotdel = clmt / slmt
      a11 = log(clmt) ; a12 = log(slmt)
      cl = mod(dimag(a11),pi2)
      sl = mod(dimag(a12),pi2)
      clph = mod(cl-sl,pi2)
      if ( abs(a11-a12)<logmax ) then
        cotdel = exp(dcmplx(dreal(a11)-dreal(a12),clph))
      else
!       write(6,*) ' COTDEL = ',cotdel
!       call fstop('COTDEL=0')
       cotdel = czero
      end if

!        if(iprint.ge.1) then
        if(iprint.ge.6) then
         write(6,'(''  dirac: l1,pr,p_in,em='',2i5,3(1x,2d16.8))')      &
     &                                  l1,jScat+1,pr,p_in,eoc2p1
         write(6,'(''  dirac: djl,bjl='',2(1x,2d16.8))')                &
     &                                  djl(l1),bjl(l1)
         write(6,'(''  dirac: dnl,bnl='',2(1x,2d16.8))')                &
     &                                  dnl(l1),bnl(l1)
         write(6,'(''  dirac: slmt,clmt='',2(1x,2d16.8))')              &
     &                                  slmt,clmt

!DELETE         write(80+l1,'(/a,2d22.12)') 'energy_in =',energy_in
!DELETE         write(80+l1,'(''  slmt,clmt='',2(1x,2d16.8))')                 &
!DELETE     &                                  slmt,clmt
!DELETE         write(80+l1,'(''  sl,cl,clph='',2(1x,2d16.8))')                &
!DELETE     &                                  sl/pi,cl/pi,clph/pi
!DELETE         write(80+l1,'(''  tandel,slmt/clmt='',2(1x,2d16.8))')          &
!DELETE     &                          1.d0/cotdel,slmt/clmt
!DELETE         write(80+l1,*) 'WITH_SCTRN=',with_sctrn.or.abs(real(pr))>=0.d0

        end if

!c     =================================================================
!c     get normalization etc. at sphere boundary rScat .................
!c     =================================================================
!
      pr = p_in*rScat
      if ( with_sctrn .or. abs(real(pr))>=zero) then
       gpl_s= bjl(l1)/p_in
       fpl_s=    (djl(l1) - bjl(l1)/pr)/em
       anorm=-p_in/slmt    ! cotdel .ne. 0
      else
!DELETE       write(*,*) 'DEBUG: DO NOT USE WITHOUT REVIEW'
!DELETE       call fstop('Abort in Dirac subroutine')
        gpl_s= ( bjl(l1)+sqrtm1*bnl(l1) )/p_in*cm1l
        fpl_s= ( (djl(l1)-bjl(l1)/pr) +                                 &
     &                           sqrtm1*(dnl(l1)-bnl(l1)/pr) )*cm1l/em
!!        em2 = sqrt(cone-cone/eos2p1)
!!        fpl_s= gpl_s * sign(kl) * exp(0.5d0*(cone-cone/em)
        anorm = slmt + sqrtm1*clmt
        if ( abs(anorm)<smallnumber ) then
         anorm = czero
        else
         anorm = -p_in/anorm
!       anorm = -pnrel/( slmt + sqrtm1*clmt )
        end if

!       gpl_s= ( bjl(l1)-sqrtm1*bnl(l1) )/p_in
!       fpl_s=   ( (djl(l1)-bjl(l1)/pr) -                                &
!     &                           sqrtm1*(dnl(l1)-bnl(l1)/pr) )/em
      end if

!DEBUGPRINT
!!      if ( abs(dreal(energy_in)) < 0.15d0 ) then
!       if ( kl==-5 ) then
!      write(81,'(e16.8,1x,2(1x,2e16.8),2x,2(1x,2e16.8),2x,9(1x,2e16.8))'&
!     &)       real(energy_in),gpl_s,fpl_s,emr,anorm,clph/pi2,cotdel
!       end if
!       if ( kl==4 ) then
!      write(82,'(e16.8,1x,2(1x,2e16.8),2e,2(1x,2e16.8),2x,9(1x,2e16.8))'&
!     &)       real(energy_in),gpl_s,fpl_s,emr,anorm,clph/pi2,cotdel
!       end if
!!      end if
!DEBUGPRINT

      g(1:jScat+1) = g(1:jScat+1)*anorm   ! j=jScat+1
      f(1:jScat+1) = f(1:jScat+1)*anorm   ! j=jScat+1
      slmt = slmt*anorm
      clmt = clmt*anorm
      g_s = g_s*anorm    ! g_s = g(r=rScat)
      f_s = f_s*anorm    ! f_s = f(jScat+1)

      ! alpha matrix = ratio between Rl=Zl*tl and sph. bes. Jl at origin
      ! for lloyd formula
!           tl = cone/(p_in*(sqrtm1-cotdel))
      ptl_over_sl = slmt*sqrtm1-clmt
      if ( abs(ptl_over_sl) < smallnumber ) then
       ptl_over_sl = czero
      else
       ptl_over_sl = cone/ptl_over_sl
      end if
!      alpha = (tl*anorm)*(g(1)/(a0*(p_in*r(1))**power))
!      alpha = (-p/slmt*tl)*(g(1)/(a0*(p_in*r(1))**power))   !  power OR (l+1)?

      alphlog = cone
      do i=2*1+1,2*l+1,2
        alphlog = alphlog + log(dble(i))    ! =g()*(2*l+1)!!
      end do
      alpha = czero
      do j1=1,jScat+1
       if ( abs(g(j1)) > smallnumber .and. abs(a0) > smallnumber ) then
        if ( j1==jScat+1 ) then
         rj = rScat
        else
         rj = r(j1)
        end if
!        alpha = g(j1)
!        do i=2*1+1,2*l+1,2
!         alpha = alpha*i              ! =g()*(2*l+1)!!
!        end do
        pr = p_in*r(j1)
!!        alpha = (alpha/(a0*r(j1)))/(pr**(power-one))*(-p_in*tl/slmt)
        if ( ptl_over_sl==czero ) then
         alpha = czero
        else
!         alpha = exp(log(alpha)-log(a0*rj)-(power-one)*log(pr)+         &
!     &                                                log(ptl_over_sl))
!
!TODO:  For kl<0 alpha is incorrect
         alpha = alphlog + log(g(j1)/a0)-log(rj)-(power-one)*log(pr)+    &
     &                                                log(ptl_over_sl)

         if ( abs(alpha) > smallnumber ) then
          alpha = exp(alpha)
          exit
         else
          alpha = czero
         end if
        end if
       end if
      end do

!c
!c non-relat solution
!c
!      clj =-bnl(2,j)*gpl(j) - bnl(1,j)*fpl(j)/p
!      slj =-bjl(2,j)*gpl(j) - bjl(1,j)*fpl(j)/p
!      gpl(j)=clj*bjl(1,j)-slj*bnl(1,j)
!      fpl(j)=-p_in*(clj*bjl(2,j)-slj*bnl(2,j))

!      do j=jScat+1,iend
!         pr = p_in*r(j)
!         gpl(j)=bjl(l1,j)/p
!         fpl(j)=(djl(l1,j) - bjl(l1,j)/pr)/eoc2p1
!      end do
!
!      em2 = cone + 2*(eoc2p1-cone)
      if ( abs(rScat-r(jScat+1))>1.d-7 )  then
       pr=p_in*r(jScat+1)
       call ricbes(lmin,pr,bjl(1:lmin),bnl(1:lmin),                     &
     &                                      djl(1:lmin),dnl(1:lmin))
       g(jScat+1)=(clmt*bjl(l1)-slmt*bnl(l1))/p_in
       f(jScat+1)=(clmt*(djl(l1)-bjl(l1)/pr)-                           &
     &                            slmt*(dnl(l1)-bnl(l1)/pr)) / em
       if(iswzj.ne.0) then   ! irregular solution
       if ( with_sctrn .or. abs(real(pr))>=zero) then
         gpl(jScat+1) = bjl(l1)/p_in
         fpl(jScat+1) = (djl(l1) - bjl(l1)/pr)/em
        else
         gpl(jScat+1) = (bjl(l1)+sqrtm1*bnl(l1))/p_in
         fpl(jScat+1) =    ( (djl(l1) - bjl(l1)/pr) +                   &
     &                           sqrtm1*(dnl(l1)-bnl(l1)/pr) )/em
        end if
       end if
      else
       gpl(jScat+1) = gpl_s
       fpl(jScat+1) = fpl_s
      end if

!DELETE      if ( allocated(p_data%sltns) ) then
!DELETE       p_data%sltns(0,kl,1) = cmplx(r(jScat+1),0.d0)
!DELETE       p_data%sltns(1,kl,1) = g(jScat+1)
!DELETE       p_data%sltns(2,kl,1) = gpl(jScat+1)
!DELETE       p_data%sltns(3,kl,1) = f(jScat+1)
!DELETE       p_data%sltns(4,kl,1) = fpl(jScat+1)
!DELETE       if ( kl==-1 ) p_data%sltns(0:4,0,1)=czero
!DELETE      end if
!c        --------------------------------------------------------------
      do j=jScat+2,iend    ! outside scattering sphere
       pr=p_in*r(j)
       emr = em*r(j)
       call ricbes(lmin,pr,bjl(1:lmin),bnl(1:lmin),                     &
     &                                      djl(1:lmin),dnl(1:lmin))
       g(j)=(clmt*bjl(l1)-slmt*bnl(l1))/p_in
       f(j)= (clmt*(djl(l1)-bjl(l1)/pr)-                                &
     &                           slmt*(dnl(l1)-bnl(l1)/pr)) / em
       if(iswzj.ne.0) then          ! irregular solution
        if ( with_sctrn .or. abs(real(pr))>=zero) then
         gpl(j) = bjl(l1)/p_in
         fpl(j) =    (djl(l1) - bjl(l1)/pr)/em
        else
         gpl(j) = (bjl(l1)+sqrtm1*bnl(l1))/p_in
         fpl(j) =    ( (djl(l1) - bjl(l1)/pr) +                         &
     &                        sqrtm1*(dnl(l1)-bnl(l1)/pr) ) / em
        end if
       end if
      enddo

!c     =================================================================
      if( iswzj .ne. 0 ) then
!c-----------------------------------------------------------------------
!c
!c  get irregular solution inside m.t. sphere
!c
!c       minimum allowed step-size: zero
        hmin=abserr
!c       specify sub-step tolerance
!        eps=relerr
!c       initialize the count of sub-steps
!c       xss,yss, etc are accumulated only for possible internal debugging purposes
        nss=0
        iflag = 1
!c       specify outward propagation of soln with no asymptotic behaviour factored out
        call set_ode_params(in_out0=0,enrg0=energy)
!c     ------------------------------------------------------------------
!c     get irregular solution inside m.t. sphere
!c     but dont get too near origin because of 1/r^power divergence
!c     ------------------------------------------------------------------
        wrnskn = czero
        ys2(1)=gpl_s
        ys2(2)=fpl_s
        j1min=maxval(jmin)
        do j=jScat,j1min+1,-1
          j1=j+1
          ipot(1)=j
          ipot(2)=j1
          call set_ode_params(ipot0=ipot,rfit0=r(j:j1),                 &
     &                                 zfit0=rv(j:j1),cspl0=cspl(j:j1))
          if ( j==jScat ) then
           x1=rScat
          else
           x1=x2
          end if
          x2=r(j)
          ys1(1:2)=ys2(1:2)
          call odeint2spl(x1,ys1,x2,ys2,iflag,eps,hmin,                 &
     &                                       1,xss,yss,dyss,rvss,errss)
          if(iflag.ne.0) then
            if ( iflag.ne.3 ) then
             write(*,*) 'energy_in=',energy_in
             write(*,*) ' l=',l,' zed=',zed
             write(*,*) ' j1min=',j1min,' jScat=',jScat
             write(*,*) ' * j=',ipot(1:2)
             write(*,*) ' * rv(j:j1)=',rv(j:j1)
             write(*,*) ' * x1,x2j=',x1,x2
             write(*,*) ' * ys1=',ys1(1:2)
             write(*,*) ' * ys2=',ys2(1:2)
             write(*,*) ' overflo1=', log(abs(gpl(j+1)))
             write(*,*) ' overflo2=', log(abs(fpl(j+1)))
             write(*,*) ' logmax=',logmax
             write(*,*) ' * logdblfact=',logdblfact
             write(*,*) ' * l*log(p_in*r)=',l*log(abs(p_in*r(j)))
             write(*,*) 'odeint2spl error=',iflag
             call fstop(sname//                                         &
     &         ': ODEINT2SPL ERROR')
!            else
!             write(*,*) 'odeint2 warning=',iflag
            end if
          endif
!          mapss(j)=nss
          gpl(j)=ys2(1)
          fpl(j)=ys2(2)
          overflo = max(log(abs(gpl(j))),log(abs(fpl(j)))) + one
! to prevent overflow
          if( overflo>=logmax ) then
           j1min = max(j-1,j1min)
           gpl(1:j1min) = czero
           fpl(1:j1min) = czero
           j1min = 0
           exit
          endif
        enddo
        hmin=abserr
!        eps=relerr
        nss=0
        if ( abs(g(j1min+1)) < smallnumber ) then
         gpl(1:j1min) = czero
         fpl(1:j1min) = czero
        else
!renormalization
         if ( j1min==jScat) then
          renorm = rScat**power
         else
          renorm = r(j1min+1)**power
         end if
         ys2(1)=gpl(j1min+1)*renorm
         ys2(2)=fpl(j1min+1)*renorm
         call set_ode_params(in_out0=-1,enrg0=energy)
         iflag = 1
         if ( j1min==jScat ) then
          x2 = rScat
         else
          x2 = r(j1min+1)
         end if
         j1min = min(j1min,jScat)
         wrnskn = czero
         do j=j1min,i0,-1
          j1=j+1
          ipot(1)=j
          ipot(2)=j1
          call set_ode_params(ipot0=ipot,rfit0=r(j:j1),                 &
     &                                 zfit0=rv(j:j1),cspl0=cspl(j:j1))
          x1=x2
          x2=r(j)
          ys1(1:2)=ys2(1:2)
          renorm = r(j)**power
          if ( power>=l30 ) then
            if ( power*log(r(j)/r(j1min)) < logmin ) then
             gpl(1:j) = czero
             fpl(1:j) = czero
             exit
            end if
          end if
          call odeint2spl(x1,ys1,x2,ys2,iflag,eps,hmin,                 &
     &                                       1,xss,yss,dyss,rvss,errss)
          if(iflag.ne.0) then
            if ( iflag.ne.3 ) then
             write(*,*) ' l=',l,' zed=',zed
             write(*,*) ' j1min=',j1min,' jScat=',jScat
             write(*,*) ' ** j=',j,x1,x2
             write(*,*) ' * g,f=',g(j1),f(j1)
             write(*,*) ' * gpl,fpl=',gpl(j1),fpl(j1)
             write(*,*) ' * ys1=',ys1(1:2)
             write(*,*) ' * rv(j:j1)=',rv(j:j1)
             write(*,*) ' * logdblfact=',logdblfact
             write(*,*) ' * l*log(p_in*r)=',l*log(abs(p_in*r(j)))
             write(*,*) 'odeint2spl error=',iflag
            call fstop(sname//                                          &
     &         ': ODEINT2SPL ERROR')
!            else
!             write(*,*) 'odeint2spl warning=',iflag
            end if
          endif
!          mapss(j)=nss
! r-factor
!c         gpl(j)=( r(j)**(-power) )*ys2(1), fpl(j)=( r(j)**(-power) )*ys2(2)

          renorm = r(j)**power
          if ( renorm < smallnumber ) then
           gpl(1:j) = czero
           fpl(1:j) = czero
          else
           if ( power<l30 ) then
            gpl(j)=ys2(1)/renorm
            fpl(j)=ys2(2)/renorm
           else
! to prevent overflow
            if (log(abs(ys2(1)))-log(renorm)<logmax ) then
!     &                              .and. abs(g(j))<tiny(one) ) then
!              gpl(j)=ys2(1)/renorm
              if (log(abs(ys2(2)))-log(renorm)<logmax) then
                gpl(j)=ys2(1)/renorm
                fpl(j)=ys2(2)/renorm
              else
!               if ( log(abs(f(j)))+log(abs(gpl(j))) < logmax ) then
!                if ( wrnskn == czero ) then
!                 wrnskn = g(j+1)*fpl(j+1)-f(j+1)*gpl(j+1)
!                end if
!                fpl(j) = wrnskn/g(j) + (f(j)/g(j))*gpl(j)
!               else
                gpl(1:j) = czero
                fpl(1:j) = czero
                exit
!               end if
              end if
            else
              gpl(1:j) = czero
              fpl(1:j) = czero
              exit
            end if
           endif
          endif
!
         end do
        end if
!c===============end irregular solution calculation====================
      else
!c
!c  zero out irregular solution arrays for case when iswzj=0
!c
        gpl = czero
        fpl = czero
      end if
      if ( fl_coef .ne. zero .and. zed.ne.zero ) then
       f(1:iend)  = f(1:iend) + fl_coef * g(1:iend)  / twoMr(1:iend)
       fpl(1:iend)= fpl(1:iend)+fl_coef*gpl(1:iend)  / twoMr(1:iend)
      end if
!c
!c     ===============================================================
      if ( allocated(rspl) ) deallocate(rspl)
      if ( allocated(cspl) ) deallocate(cspl)

      if ( imsg >= msgmax ) then
       imsg = 0
       msgmax = msgmax/2
      end if
      return
!c
!EOC
      end subroutine dirac

      end module sctrng
