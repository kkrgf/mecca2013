!BOP
!!ROUTINE: matrm1t
!!INTERFACE:
      subroutine matrm1t(g,t,ndimt,itype,                               &
     &                   natom,kkrsz,idiag)
!!DESCRIPTION:
! {\bv
! input: g=G ,  order KKRSZ*NATOM
!     if idiag.eq.1 then t -- diagonal of block-matrix, order KKRSZ
!     if idiag.ne.1 then t -- block-matrix, order KKRSZ
!
! output: g=(G-1)*t
! \ev}

!!DO_NOT_PRINT
      implicit none
!!DO_NOT_PRINT

!!ARGUMENTS:
      complex*16 g(*),t(*)
      integer    nrmat,ndimt,itype(*),natom,kkrsz,iswitch,idiag
!!PARAMETERS:
!!REVISION HISTORY:
! Adapted - A.S. - 2013
!!REMARKS:
!EOP
!
!BOC
      character*10 :: sname = 'matrm1t'

      nrmat = kkrsz*natom
      if(idiag.eq.1) then
        call matrm1tzd(g,nrmat,t,ndimt,itype,                           &
     &                   natom,kkrsz)
      else
        call matrm1tz(g,nrmat,t,ndimt,itype,                            &
     &                   natom,kkrsz)
      end if
      return
!EOC
      contains

!BOP
!!IROUTINE: matrm1tzd
!!INTERFACE:
      subroutine matrm1tzd(g,nrmat,t,ndimt,itype,                       &
     &                   natom,kkrsz                                    &
     &                   )

!!USES:
      use mecca_constants
!!REMARKS:
! private procedure of subroutine matrm1t
!EOP
!
!BOC
      implicit none
!c
!c  input: g=G
!c
!c  output: g=(G-1)*t=G*t-t
!c

      integer nrmat,ndimt,natom,kkrsz
      complex*16 g(nrmat*nrmat)
      complex*16 t(ndimt,*)
      integer    itype(natom)

      integer nsub,ns,i1,i2,i2i1,i,j,Ii1,IiJj,IiIi,i0,j0

      complex*16 a(kkrsz,kkrsz)
!      complex*16 czero,cone
!      parameter (czero=(0.d0,0.d0))
!      parameter (cone=(1.d0,0.d0))
!c     ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
      character    sname*10
      parameter    (sname='matrm1tzd')
!c     ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

!c
!c  diagonal (I,I)-block
!c
      do nsub = 1,natom

       ns = itype(nsub)
       i1 = (nsub-1)*kkrsz
       i2i1 = (i1-1)*nrmat+i1

!c
       call cpblkz(i1,i1,g,nrmat,kkrsz,kkrsz,a,kkrsz)
!c
!c                                     A = transposed-G = G'

       do i=1,kkrsz
         Ii1 = i2i1+i
         do j=1,kkrsz

!c           Ii = i1+i
!c           Jj = i2+j  ,  i2=i1
!c           IiJj=(Jj-1)*nrmat+Ii          ! (Ii,Ij)

           IiJj=Ii1 + j*nrmat             ! (Ii,Ij)
           g(IiJj) = a(j,i)*t(j,ns)      ! = G*t
         end do
         IiIi = Ii1 + i*nrmat
         g(IiIi) = g(IiIi) - t(i,ns)      ! = G*t-t = (G-1)*t
       end do

      end do
!c
!c  Off diagonal (I,J)-block
!c
      do i0 = 1,natom
      do j0 = 1,natom
      if(i0.ne.j0) then

       i1 = (i0-1)*kkrsz
       i2 = (j0-1)*kkrsz
       ns = itype(j0)
       i2i1 = (i2-1)*nrmat+i1
       call cpblkz(i1,i2,g,nrmat,kkrsz,kkrsz,a,kkrsz)

!c                                     A = G'

       do i=1,kkrsz
         Ii1 = i2i1+i
         do j=1,kkrsz

!c           Ii = i1+i
!c           Jj = i2+j
!c           IiJj=(Jj-1)*nrmat+Ii          ! (Ii,Jj)

           IiJj=Ii1 + j*nrmat             ! (Ii,Ij)
           g(IiJj) = a(j,i)*t(j,ns)    ! = G_IJ*t_JJ (matrix in L-space)
         end do
       end do

      end if
      end do
      end do

      return
!EOC
      end subroutine matrm1tzd

!BOP
!!IROUTINE: matrm1tz
!!INTERFACE:
      subroutine matrm1tz(g,nrmat,t,ndimt,itype,                        &
     &                   natom,kkrsz                                    &
     &                   )
!!USES:
      use mecca_constants
!!REMARKS:
! private procedure of subroutine matrm1t
!EOP
!
!BOC
      implicit none
!c
!c  input: g=G
!c
!c  output: g=(G-1)*t=G*t-t
!c

      integer nrmat,ndimt,natom,kkrsz
      complex*16 g(nrmat*nrmat)
      complex*16 t(ndimt,ndimt,*)
      integer    itype(natom)

      integer nsub,ns,i1,i2,i2i1,i,j,k,Ii1,IiJj,i0,j0

      complex*16 a(kkrsz,kkrsz),sum
!      complex*16 czero,cone
!      parameter (czero=(0.d0,0.d0))
!      parameter (cone=(1.d0,0.d0))
!c     ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
      character    sname*10
      parameter    (sname='matrm1tz')
!c     ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

!c
!c  diagonal (I,I)-block
!c
      do nsub = 1,natom

       ns = itype(nsub)
       i1 = (nsub-1)*kkrsz
       i2i1 = (i1-1)*nrmat+i1

!c
       call cpblkz(i1,i1,g,nrmat,kkrsz,kkrsz,a,kkrsz)
!c
!c                                     A = transposed-G = G'

       do i=1,kkrsz
         Ii1 = i2i1+i
         do j=1,kkrsz
           sum = czero
           do k=1,kkrsz
            sum = sum+a(k,i)*t(k,j,ns)
!c                                 ! sum of G(Ii,Ik)*t(Ik,Ij)
!c
!c                                                  sum = G*t
!c
           end do

!c           Ii = i1+i
!c           Jj = i2+j  ,  i2=i1
!c           IiJj=(Jj-1)*nrmat+Ii          ! (Ii,Ij)

           IiJj=Ii1 + j*nrmat             ! (Ii,Ij)
           g(IiJj) = sum - t(i,j,ns)      ! = G*t - t
         end do
       end do

      end do
!c
!c  Off diagonal (I,J)-block
!c
      do i0 = 1,natom
      do j0 = 1,natom
      if(i0.ne.j0) then

       i1 = (i0-1)*kkrsz
       i2 = (j0-1)*kkrsz
       ns = itype(j0)
       i2i1 = (i2-1)*nrmat+i1
       call cpblkz(i1,i2,g,nrmat,kkrsz,kkrsz,a,kkrsz)

!c                                     A = G'

       do i=1,kkrsz
         Ii1 = i2i1+i
         do j=1,kkrsz
           sum = czero
           do k=1,kkrsz
            sum = sum+a(k,i)*t(k,j,ns)    ! sum of G(Ii,Jk)*t(Jk,Jj)
           end do
!c           Ii = i1+i
!c           Jj = i2+j
!c           IiJj=(Jj-1)*nrmat+Ii           ! (Ii,Jj)

           IiJj=Ii1 + j*nrmat             ! (Ii,Ij)
           g(IiJj) = sum                  ! = G_IJ*t_JJ (matrix in L-space)
         end do
       end do

      end if
      end do
      end do

      return
!EOC
      end subroutine matrm1tz

      end subroutine matrm1t
