!BOP
!!ROUTINE: cpmatrdr
!!INTERFACE:
      subroutine cpmatrdr(x,i1,i2,j1,j2,nrmat,kkrsz)
!!DESCRIPTION:
!  copies a block (j1,j2) of complex matrix to block (i1,i2);
!  {\tt kkrsz} is block size, complex matrix is stored in
!  a real array {\tt x}: Re part first, then Im part
!

!!DO_NOT_PRINT
      implicit none
!!DO_NOT_PRINT

!!ARGUMENTS:
      integer i1,i2,j1,j2,nrmat,kkrsz
      real*8   x(nrmat,*)   ! nrmat is size of complex matrix
!EOP
!
!BOC
!c
      integer iist,jjst,ii0,jj0,i,j
      integer iistim,ii0im
!CAB      complex*16   x(nrmat,*)
!c *******************************************************************

      iist=(i2-1)*kkrsz
      jjst=(i1-1)*kkrsz
      ii0=(j2-1)*kkrsz
      jj0=(j1-1)*kkrsz
      iistim = iist+nrmat
      ii0im = ii0+nrmat
      do i=1,kkrsz
       do j=1,kkrsz
         x(jjst+j,iist+i)=x(jj0+j,ii0+i)
         x(jjst+j,iistim+i)=x(jj0+j,ii0im+i)
       enddo
      enddo
      return
!EOC
      end subroutine cpmatrdr

!
!BOP
!!ROUTINE: cpmatrz
!!INTERFACE:
      subroutine cpmatrz(x,i1,i2,j1,j2,nrmat,kkrsz)
!!DESCRIPTION:
!  copies a block (j1,j2) of complex matrix {\tt x} to block (i1,i2);
!  {\tt kkrsz} is block size
!

!!DO_NOT_PRINT
      implicit none
!!DO_NOT_PRINT

!!ARGUMENTS:
      integer i1,i2,j1,j2,nrmat,kkrsz
      complex*16   x(nrmat,*)
!EOP
!
!BOC
      integer iist,jjst,ii0,jj0,i,j
!c *******************************************************************

      iist=(i2-1)*kkrsz
      jjst=(i1-1)*kkrsz
      ii0=(j2-1)*kkrsz
      jj0=(j1-1)*kkrsz
      do i=1,kkrsz
       x(jjst+1:jjst+kkrsz,iist+i)=x(jj0+1:jj0+kkrsz,ii0+i)
!       do j=1,kkrsz
!         x(jjst+j,iist+i)=x(jj0+j,ii0+i)
!       enddo
      enddo
      return
!EOC
      end subroutine cpmatrz

!BOP
!!ROUTINE: cpblk2
!!INTERFACE:
      subroutine cpblk2(x,nxmat,ix,jx,y,nymat,iy,jy,blksz)
!!DESCRIPTION:
! to copy block (ix,jx) of complex matrix {\tt x} to
! block (iy,jy) of complex matrix {\tt y};
! {\tt blksz} is block size
!

!!DO_NOT_PRINT
      implicit none
!!DO_NOT_PRINT

!!ARGUMENTS:
      integer, intent(in) :: jx,ix,jy,iy,nxmat,nymat,blksz
      complex(8), intent(in) :: x(nxmat,*)
      complex(8), intent(inout) :: y(nymat,*)
!EOP
!
!BOC
      integer :: Ny,My,Nx,Mx,i,j
      Ny=(iy-1)*blksz
      My=(jy-1)*blksz
      Nx=(ix-1)*blksz
      Mx=(jx-1)*blksz
!      y(Ny+1:Ny+blksz,My+1:My+blksz) = x(Nx+1:Nx+blksz,Mx+1:Mx+blksz)
      call zlacpy('N',blksz,blksz,x(Nx+1,Mx+1),nxmat,y(Ny+1,My+1),nymat)
      return
!EOC
      end subroutine cpblk2
