!c /////////////////////////////////////////////////////////////////////
!BOP
!!ROUTINE: richnk
!!INTERFACE:
      subroutine richnk (nn,y,bh,dh)
!!DESCRIPTION:
! computes riccati-bessel functions hl ({\tt bh}) and its derivative ({\tt dh})
!

!!DO_NOT_PRINT
      implicit complex*16 (a-h,o-z)
!!DO_NOT_PRINT

!!ARGUMENTS:
      integer    nn
      complex*16 y
      complex*16 bh(nn),dh(nn)
!!REMARKS:
! indexing of arrays starts at 1 to hold l=0
!EOP
!
!BOC
      complex*16 x,xinv,xpon
      integer l
!c parameter
      real*8 smallx
      complex*16 cone,sqrtm1
!c
!c _____________________________________________________________________
      parameter (smallx = 1.d-14)
      parameter (cone=(1.d0,0.d0))
      parameter (sqrtm1=(0.d0,1.d0))
!c _____________________________________________________________________
!c
      x=y
      if (abs(x).le.smallx) go to 200
!c ---------------------------------------------------------------------
!c  recursion relations
!c ---------------------------------------------------------------------
      xpon=exp( sqrtm1*x )
      xinv=cone/x
      bh(1)=-sqrtm1
      bh(2)= - cone - sqrtm1*xinv
      dh(1)= cone
      dh(2)= sqrtm1 * ( xinv - ( sqrtm1 + x ) ) * xinv
!c
!c flm=2l+1 for real l=0,1,2,3, ...  quantum number
      do 700 l=3,nn
        bh(l) = (2*l-3)*bh(l-1)*xinv - bh(l-2)
        dh(l) = bh(l-1) - (l-1)*bh(l)*xinv
  700 continue
       do 800 l=1,nn
         bh(l)=bh(l)*xpon
         dh(l)=dh(l)*xpon
  800  continue
!c ---------------------------------------------------------------------
      if( abs(x) .le. 0.01 ) then
!c
!c  power-series for j if abs(x) is smaller than 0.9
!c  trouble
!c      write(6,*)' WARNING: trouble in RICHNK: small argument'
!c      write(6,*)'          x=',x,'  nn=',nn
!CAB      stop
      endif
!c
      return
!c _____________________________________________________________________
!c
  200  write(6,99) x
   99  format(' trouble in RICHNK argument= ',2d14.5)

      xpon= cone

      xinv=cone/smallx
      bh(1)=-sqrtm1
      bh(2)= - cone - sqrtm1*xinv
      dh(1)= cone
      dh(2)= dcmplx(1.d0/smallx, (1.d0/smallx)**2)
!c
!c flm=2l+1 for real l=0,1,2,3, ...  quantum number
      do l=3,nn
        bh(l) = (2*l-3)*bh(l-1)*xinv - bh(l-2)
        dh(l) = bh(l-1) - (l-1)*bh(l)*xinv
      end do

       call p_fstop(' richnk')
!EOC
      end subroutine richnk
