!BOP
!!ROUTINE: nncluster
!!INTERFACE:
      subroutine nncluster(iatom,natom,mapstr,ndimbas,                  &
     &                   aij,Rnncut,Rsmall,rslatt,itype,nlatt,          &
     &                   numnbi,nsmall,                                 &
     &                   jclust,ntypecl,ntau,                           &
     &                   rsij,nrsij,mapsnni,ndimnn,mappnni,mapnnij      &
     &                   ,rijcl)
!!DESCRIPTION:
! finds neighbors of site {\tt iatom}, their types, 
! and other characteristics
!

!!DO_NOT_PRINT
      implicit none
!!DO_NOT_PRINT

!!ARGUMENTS:
      integer, intent(in) ::  iatom,natom,ndimbas
      integer, intent(in) ::  mapstr(ndimbas,natom)
      real(8), intent(in) ::   aij(3,*),Rnncut,Rsmall,rslatt(3,3)
      integer, intent(in) ::  itype(natom),nlatt(3)
      integer, intent(inout) ::  numnbi  ! input: if ndimnn<=0, it is max.size of big cluster
                                         ! output: actual size of big cluster
      integer, intent(out) ::  nsmall ! nsmall <= numnbi
      integer, intent(out) ::  jclust(*)     ! 1:numnbi - atom number in big cluster (Rij<Rnncut)
!                            ! 1:nsmall - atom number in small cluster (Rij<Rsmall)
      integer, intent(out) ::  ntypecl(*)    ! 1:numnbi - type of NN in the cell; 
!                            ! ntypecl(1) = itype(iatom)
      integer, intent(out) ::  ntau(*)
      real(8), intent(out) ::  rijcl(3,*)   ! it is assumed that size(rijcl,2) >= numnbi
      integer, intent(in) ::  ndimnn ! if ndimnn>0, it is max.size of big cluster and defines size of some arrays
                                     ! if ndimnn<=0, output args below are undefined 
      integer, intent(out) ::  nrsij
      real(8), intent(out) ::  rsij(3,ndimnn*ndimnn)
      integer, intent(out) ::  mapsnni(ndimnn,*),mappnni(ndimnn,*)
      integer, intent(out) ::  mapnnij(2,*)
!!REVISION HISTORY:
! Adapted - A.S. - 2013
!EOP
!
!BOC
!c  real*8   rsij(3,nrsij)
      real*8   zero,one,epsrs
      parameter (zero=0.d0,one=1.d0,epsrs=1.d-14)
      real*8   tmpv(3),tmpv1(3),tmpv2(3),tmpv3(3)
      character*10 sname
      parameter (sname='nncluster')
      integer :: numnbi_in,missed_nn
      integer :: i1,i2,i3,ia,ic,j
      real(8) :: r2cut,r2small,rij,r2ijnn

      real(8), external :: g_pi

!DEBUG
!      write(*,*) ' DEBUG nncluster: ndimbas=',ndimbas,' numnbi=',numnbi
!      write(*,*) ' DEBUG nncluster: nlatt',nlatt
!      write(*,*) ' DEBUG nncluster: ndimnn',ndimnn
!      write(*,'(a,2g16.7)') ' DEBUG nncluster: Rnncut,Rsmall=',Rnncut,  &
!     &                Rsmall/(2*g_pi())
!      write(*,*) ' DEBUG nncluster: rslatt:'
!      write(*,*) ' DEBUG ',rslatt(1:3,1)/(2*g_pi())
!      write(*,*) ' DEBUG ',rslatt(1:3,2)/(2*g_pi())
!      write(*,*) ' DEBUG ',rslatt(1:3,3)/(2*g_pi())
!!DEBUG

!c      integer erralloc

      if ( ndimnn>0 ) then
       numnbi_in = ndimnn
      else
       numnbi_in = numnbi
      end if
      missed_nn = 0
      nrsij = 0
      r2cut = Rnncut**2
      if(Rsmall.le.zero.or.Rsmall.ge.Rnncut) then
       r2small = r2cut
      else
       r2small = Rsmall**2
      end if

      nsmall = 1
      jclust(nsmall) = iatom
      ntypecl(nsmall) = itype(iatom)
      rijcl(1,nsmall) = zero
      rijcl(2,nsmall) = zero
      rijcl(3,nsmall) = zero
      ntau(nsmall) = 0                  ! =0 , if atom belongs to the CELL

      do 50 j=1,natom

       ia = mapstr(iatom,j)
       rij = zero
       tmpv(1:3) = aij(1:3,ia)
       rij = sqrt(dot_product(tmpv,tmpv))

       do i1=-nlatt(1),nlatt(1)
        tmpv1(1:3) = tmpv(1:3) + i1*rslatt(1:3,1)
        do i2=-nlatt(2),nlatt(2)
         tmpv2(1:3) = tmpv1(1:3) + i2*rslatt(1:3,2)
         do i3=-nlatt(3),nlatt(3)
          tmpv3(1:3) = tmpv2(1:3) + i3*rslatt(1:3,3)
          r2ijnn = dot_product(tmpv3,tmpv3)

          if(r2ijnn.le.r2small.and.r2ijnn.gt.epsrs) then
           if ( nsmall<numnbi_in ) then
            nsmall = nsmall+1
            ntau(nsmall) = max(abs(i1),abs(i2),abs(i3))
            jclust(nsmall) = j
            ntypecl(nsmall) = itype(j)
            rijcl(1:3,nsmall) = tmpv3(1:3)   ! Rj-Ri = aij+tau; I=1, J=numnbi
           else
            missed_nn = missed_nn+1
           end if

          end if

         enddo
        enddo
       enddo

50    continue

      numnbi = nsmall

      if ( r2cut>r2small ) then
      do 150 j=1,natom

       ia = mapstr(iatom,j)
       rij = zero
       do ic=1,3
        tmpv(ic) = aij(ic,ia)
        rij = rij + tmpv(ic)**2
       end do
       rij = sqrt(rij)

       do i1=-nlatt(1),nlatt(1)
        do ic=1,3
          tmpv1(ic) = tmpv(ic)                                          &
     &          +i1*rslatt(ic,1)
        end do
        do i2=-nlatt(2),nlatt(2)
         do ic=1,3
           tmpv2(ic) = tmpv1(ic)                                        &
     &           +i2*rslatt(ic,2)
         end do
         do i3=-nlatt(3),nlatt(3)

          r2ijnn = zero
          do ic=1,3
           tmpv3(ic) = tmpv2(ic)                                        &
     &          +i3*rslatt(ic,3)
           r2ijnn = r2ijnn+tmpv3(ic)**2
          end do

          if(r2ijnn.gt.r2small.and.r2ijnn.le.r2cut) then
           if ( numnbi<numnbi_in ) then
            numnbi = numnbi+1
            ntau(numnbi) = max(abs(i1),abs(i2),abs(i3))
            jclust(numnbi) = j
            ntypecl(numnbi) = itype(j)
            do ic=1,3
             rijcl(ic,numnbi) = tmpv3(ic)   ! Rj-Ri = aij+tau; I=1, J=numnbi
            end do
           else
            missed_nn = missed_nn+1
           end if

          end if

         enddo
        enddo
       enddo

150    continue
       end if

       if ( ndimnn > 0 ) then
        if(numnbi.gt.ndimnn) then
         do i1=1,numnbi
          write(6,1001) i1,(rijcl(ic,i1),ic=1,3)
1001      format(i7,3d16.7)
         end do
         write(6,1000) natom,iatom,numnbi,ndimnn,Rnncut
1000     format(' NNCLUSTER::'/'   NATOM=',i6,' I=',i6,                 &
     &        ' NUMBNB(I)=',i6,'  NDIMNN=',i6,' Rnncut=',d14.5)
         call fstop(' NNCLUSTER::  NUMBNB(*) > NDIMNN')
        end if

       call strident1(rijcl,ndimnn,nsmall,ntypecl,                      &
     &              mapsnni,mappnni,mapnnij,rsij,nrsij,-100,-1)

       end if
       if ( missed_nn > 0 ) then
        write(6,'(a,i3,a)') ' WARNING: there are ',missed_nn,           &
     &                      ' missed neighbors in NNCLUSTER'
       end if

      return
!EOC
      end subroutine nncluster
