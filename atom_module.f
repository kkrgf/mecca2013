!BOP
!!MODULE: atom
!!INTERFACE:
      module atom
!!DESCRIPTION:
! Atomic Schroedinger/Dirac equation solver

!!USES
      use atom_hsk, only: fjp_atom,zID,intqud
      use dftatom,  only: get_atom_orb,atom_lda
      use dftatom,  only: get_atom_orb_rel,atom_rdft
      use dftatom,  only: mesh_exp_deriv,get_mesh_exp_params
      use xc_mecca, only: gXCmecca,xc_mecca_pointer,xc_alpha,es_xc_id
      use xc_mecca, only: XC_LDA_HEDIN,XC_LDA_VOSKO,XC_GGA_AM05   ! mecca XC
!DELETE after resolving convergence issue for GGA functionals
!      use xc_mecca, only: ID_VOSKO
!DELETE after resolving convergence issue for GGA functionals

!!DO_NOT_PRINT
      implicit none
      private
!!DO_NOT_PRINT

!!PUBLIC MEMBER FUNCTIONS:
      public :: calcAtom,get_atom,set_atom_procedure,dealloc_atoms
      public :: get_mesh_exp_params,intqud,get_etot
!
!!REVISION HISTORY:
! Initial Version - A.S. - 2019
!EOP
!
!BOC
!
!*******************************************************************
      type, public :: atom_rdata
!*******************************************************************
!
       integer :: iZ=0    ! atomic number
       integer :: mkind=0 ! kind of atomic solver used to calculate data
       integer :: mx=0    ! index of last point w continuous potential
       real(8) :: E_tot=0 ! total energy (Ry)
       real(8), allocatable :: r(:)   ! r - radial variable
       real(8), allocatable :: rp(:)  ! first derivative of r-mapping
       real(8), allocatable :: rho(:)
       real(8), allocatable :: rv(:)  ! r * V(r)
      end type atom_rdata
!*******************************************************************

      type( atom_rdata ), allocatable, target :: dft_atoms(:)

      real(8), parameter :: inv_fine_struct_const=137.035999139d0
      real(8), parameter :: pi=3.1415926535897932385d0, pi4=pi*4

      integer, protected :: mkind = 0       ! kind of atomic_solver + default (VWN) xc_functional
      character(15) :: atkind(0:4) = ['libxc',                          &
     &                              'hsk-lda1-nonrel','dft-lda2-nonrel',&  ! lda1 -> Hedin-Lundqvist correlation (mecca)
     &                              'dft-lda2-dirac ','dft-rlda2-dirac']   ! lda2 -> V.W.N. correlation
                                                                           ! rlda2 -> relativistic V.W.N. functional
!DELETE after resolving convergence issue for GGA functionals
      logical, external :: isLDAxc
!DELETE after resolving convergence issue for GGA functionals
!EOC
      contains

      subroutine set_atom_procedure(nkind)
      implicit none
      integer, intent(in) :: nkind
      mkind = nkind
!DELETE after resolving convergence issue for GGA functionals
!         if ( .not. isLDAxc() ) then
!          mkind = ID_VOSKO
!         end if
!DELETE after resolving convergence issue for GGA functionals
      return
      end subroutine set_atom_procedure

!      integer function get_atom_mkind()
!      get_atom_mkind = mkind
!      return
!      end function get_atom_mkind

      subroutine calcAtom(iZ,nkind)
      implicit none
!!ARGUMENTS:
      integer, intent(in) :: iZ
      integer, intent(in), optional :: nkind
!
      real(8), pointer :: r(:)=>null(),rho(:)=>null()
      real(8), pointer :: rv(:)=>null(),rp(:)=>null()
      integer :: mykind
      type( atom_rdata ), allocatable :: temp(:)
      integer :: j,mx
      real(8) :: E_tot
      real(8) :: v1,v2,v3,d1,d2

      if ( iZ<0 ) return
      if ( present(nkind) ) then
       mykind = nkind
!DELETE after resolving convergence issue for GGA functionals
!         if ( .not. isLDAxc() ) then
!          mykind = ID_VOSKO
!         end if
!DELETE after resolving convergence issue for GGA functionals
      else
       if ( iZ==0 ) then
        mykind = es_xc_id
       else
        mykind = mkind
       end if
      end if
      if ( .not.allocated(dft_atoms) ) allocate(dft_atoms(1:10))
!
      do j=1,size(dft_atoms)
       if ( iZ == 0 ) then
        if ( dft_atoms(j)%iZ == iZ ) then
         return
        end if
       else if ( dft_atoms(j)%iZ == iZ .and.                            &
     &                            dft_atoms(j)%mkind == mykind ) then
         return
       end if
       if ( .not.allocated(dft_atoms(j)%r) ) exit
      end do
      if ( j>size(dft_atoms) ) then
       allocate(temp(size(dft_atoms)+10))
       temp(1:size(dft_atoms)) = dft_atoms
       call move_alloc(temp,dft_atoms)
      end if
!
      select case(mykind)
       case(1)
        call hsk_nr(iZ,r,rp,rho,rv,E_tot)    ! LDA (mecca)
       case(2)
        call dft_nr(iZ,r,rp,rho,rv,E_tot)    ! LDA-VWN
       case(3)
        call dft_rel(iZ,r,rp,rho,rv,E_tot)   ! LDA-VWN + Dirac
       case(4)
        call dft_rel(iZ,r,rp,rho,rv,E_tot,xc_rel=1) ! rLDA-VWN + Dirac
       case default
!  external library (libxc) is used to calculate XC functional
        if ( mykind < 0 ) then
         call dft_nr(iZ,r,rp,rho,rv,E_tot,xc_ext=.true.)           ! libxc + Schroedinger
        else
         call dft_rel(iZ,r,rp,rho,rv,E_tot,xc_rel=0,xc_ext=.true.) ! libxc + Dirac
        end if
      end select
      if ( E_tot == 0 ) then
        write(6,'(/2x,a,i3,a,i7/)')                                     &
     &    'WARNING: failed atomic calculation for Z=',iZ,' kind=',mykind
      else
!      write(6,'(a,i3,a,f16.8)') ' iZ=',iZ,' E_tot=',E_tot
       if ( associated(rv) ) then
        mx = size(r)
        do j=size(r)-1,2,-1
         v1 = rv(j+1)/r(j+1)
         v2 = rv(j)/r(j)
         v3 = rv(j-1)/r(j-1)
         d1 = v1-v2
         d2 = v2-v3
         if ( d2>10*d1 ) then
          mx = j-1
          exit
         else
          if ( rv(j)<-1.d-8*r(j) .and. rho(j)>=0 ) then ! it's assumed that "effective V(Inf)=0" >= -1.e-8
           mx = j-1
           exit
          end if
         end if
        end do

        do j=1,size(dft_atoms)
         if ( .not.allocated(dft_atoms(j)%r) ) then
           dft_atoms(j)%iZ    = iZ
           dft_atoms(j)%mkind  = mykind
           dft_atoms(j)%mx    = mx
           dft_atoms(j)%E_tot = E_tot
           dft_atoms(j)%r     = r
           dft_atoms(j)%rp    = rp
           dft_atoms(j)%rho   = rho
           dft_atoms(j)%rv    = rv
           deallocate(r,rp,rho,rv)
           nullify(r,rp,rho,rv)
           exit
         end if
        end do
       else
        if ( E_tot == 0 ) then
        write(6,'(/2x,a,i3,a,i7/)')                                     &
     &    'ERROR(rv): failed atomic calculation for Z=',iZ,             &
     &                                                   'kind=',mykind
        end if
       end if

      end if
      return
      end subroutine calcAtom

      subroutine dealloc_atoms(iz)
      implicit none
      integer, optional, intent(in) :: iz
      integer :: j

       if ( allocated(dft_atoms) ) then
        if ( present(iz) ) then
         do j=1,size(dft_atoms)
          if ( dft_atoms(j)%iZ==iz ) then
           dft_atoms(j)%iZ = 0
           dft_atoms(j)%mkind = 0
           dft_atoms(j)%mx = 0
           dft_atoms(j)%E_tot = 0
           if(allocated(dft_atoms(j)%r) ) deallocate(dft_atoms(j)%r)
           if(allocated(dft_atoms(j)%rp)) deallocate(dft_atoms(j)%rp)
           if(allocated(dft_atoms(j)%rho))deallocate(dft_atoms(j)%rho)
           if(allocated(dft_atoms(j)%rv)) deallocate(dft_atoms(j)%rv)
           return
          end if
         end do
        else
         do j=1,size(dft_atoms)
          dft_atoms(j)%iZ = 0
          dft_atoms(j)%mkind = 0
          dft_atoms(j)%mx = 0
          dft_atoms(j)%E_tot = 0
          if(allocated(dft_atoms(j)%r) ) deallocate(dft_atoms(j)%r)
          if(allocated(dft_atoms(j)%rp)) deallocate(dft_atoms(j)%rp)
          if(allocated(dft_atoms(j)%rho))deallocate(dft_atoms(j)%rho)
          if(allocated(dft_atoms(j)%rv)) deallocate(dft_atoms(j)%rv)
         end do
        end if
       end if
       return
      end subroutine dealloc_atoms

      subroutine get_atom(nZ,atom,iprint)
      implicit none
!!ARGUMENTS:
      integer, intent(in) :: nZ
      type( atom_rdata ), intent(inout) :: atom
      integer, intent(in) :: iprint
      character(15) :: name
      character(2)  :: symb
      character(15) :: id_xc
      integer :: j,mx

      if ( allocated(dft_atoms) .and. nZ>0 ) then
       do j=1,size(dft_atoms)
        if ( dft_atoms(j)%iZ==nZ ) then
         if (dft_atoms(j)%mkind==mkind ) then
          atom%iZ    = dft_atoms(j)%iZ
          atom%mkind  = dft_atoms(j)%mkind
          mx = dft_atoms(j)%mx
          atom%E_tot = dft_atoms(j)%E_tot

          if ( allocated(atom%r).and.mx<=size(atom%r) ) then
           continue
          else
           if ( allocated(atom%r) ) then
            deallocate(atom%r,atom%rp,atom%rho,atom%rv)
           end if
           allocate(atom%r(mx))
           allocate(atom%rp(mx))
           allocate(atom%rho(mx))
           allocate(atom%rv(mx))
          end if
          atom%mx         = mx
          atom%r(1:mx)    = dft_atoms(j)%r(1:mx)
          atom%rp(1:mx)   = dft_atoms(j)%rp(1:mx)
          atom%rho(1:mx)  = dft_atoms(j)%rho(1:mx)
          atom%rv(1:mx)   = dft_atoms(j)%rv(1:mx)
1200  format(1x,86('=')/3x,a,t8,'at.num=',i4,3x,a15,                    &
     &             ' total energy =',f18.8,' Ry   Rmx = ',f6.2,' Bohr')
          call zID(atom%iZ,symb,name)
          if ( atom%mkind >=1 .and. atom%mkind < size(atkind) ) then
           id_xc = atkind(atom%mkind)
          else
           if ( atom%mkind .ne. 0 ) then
            write(id_xc,'(a,i7)') trim(adjustl(atkind(0))),             &
     &                                                   abs(atom%mkind)
           else
            id_xc = atkind(0)
           end if
          end if
          if ( iprint > 0 ) then                           ! no printing is if iprint<0
            write(iprint,1200) trim(symb),                              &
     &                     atom%iZ,id_xc,atom%E_tot,atom%r(atom%mx)
          end if
          return
         end if
        end if
        if ( .not.allocated(dft_atoms(j)%r) ) exit
       end do
      end if
!DELETE      if ( iprint>0 ) then
!DELETE       if ( associated(atom) ) then
!DELETE        call zID(atom%iZ,symb,name)
!DELETE       else
!DELETE        symb = '**'
!DELETE       end if
!DELETE       write(iprint,'(a)') 'There are no data for '//symb//' atom'
!DELETE      end if
!
      return
      end subroutine get_atom

      subroutine hsk_nr(ized,r,Rp,rho,rv,tote)
      implicit none
!!ARGUMENTS:
      integer, intent(in) :: ized
      real(8), pointer :: r(:),Rp(:)
      real(8), pointer :: rv(:),rho(:)
      real(8), intent(out) :: tote
!!REMARKS:
! In current implementation external subroutine getDfltPot is required
! to generate initial atomic potential.
! LDA Hedin-Lundqvist exchange-correlation functional is used.

      real(8), parameter :: alf0 = 0.25d0
      real(8) :: alfa
      integer :: numsp=1
      real(8) :: xmom=0
      real(8), pointer :: rho_(:,:),rv_(:,:)
      real(8) :: r_min,r_max,a
      integer :: N

      select case (ized)
       case(77,87,88,90,93,101)
        alfa = 0.13d0
       case(74,75,76,94,96,100,102)
        alfa = 0.12d0
       case default
        alfa = alf0
      end select

      call fjp_atom(dble(ized),xmom,numsp,alfa,N,r,rho_,rv_,tote)  ! legacy code
      allocate(rho(size(r)))
      rho = rho_(:,1)
      deallocate(rho_)

      allocate(rv(size(r)))
      rv = rv_(:,1)
      deallocate(rv_)

      call get_mesh_exp_params(r, r_min, r_max, a, N)
      allocate(Rp(size(r)))
      Rp = mesh_exp_deriv(r_min, r_max, a, N)

      return
      end subroutine hsk_nr

      subroutine dft_nr(ized,r,Rp,rho,rv,E_tot,xc_ext)
      implicit none
!!ARGUMENTS:
      integer, intent(in) :: ized
      real(8), pointer :: r(:),Rp(:)
      real(8), pointer :: rv(:),rho(:)
      real(8), intent(out) :: E_tot
      logical, intent(in), optional :: xc_ext   ! if xc_ext==.true. then external functional is used
!
      real(8), parameter :: reigen_eps = 1d-9
      real(8), parameter :: mixing_eps = 1d-7
      real(8), parameter :: r_min = 1.d-7 , r_max = 50.d0   ! dftatom optimal mesh
      integer ::  N = 4871                       ! dftatom optimal mesh
      real(8) :: a =  2.7d+6                     ! dftatom optimal mesh
!      real(8) :: r_min = 1.d-7 , r_max = 40.d0
!      integer ::  N = 4000
!      real(8) :: a =  2.7d+6
      integer :: reigen_max_iter = 100
      real(8) :: mixing_alpha = 0.5d0
      integer :: mixing_max_iter = 200
      logical :: perturb = .true.
      integer, allocatable, dimension(:) :: no, lo
      real(8), allocatable, dimension(:) :: fo, ks_energies
      real(8), allocatable :: orbitals(:, :)
      real(8), allocatable :: V_tot(:),density(:)
      integer :: n_orb       !,k
      logical :: iext
      real(8) :: rx          !,cx(5)=[0.6d0,0.5d0,0.4d0,0.3d0,0.2d0]

      if ( present(xc_ext) ) then
       iext = xc_ext
      else
       iext = .false.
      end if
      rx = r_max
      N = NN_nr6(ized)
!??      a = exp(dble(N-1)/dble(N)*log(r_max/r_min))   ! mecca exp mesh
!
       n_orb = get_atom_orb(ized)
       allocate(ks_energies(n_orb), no(n_orb), lo(n_orb), fo(n_orb),    &
     &  orbitals(N+1, n_orb), r(N+1), V_tot(N+1), density(N+1), Rp(N+1))

       call atom_lda(ized, r_min, r_max, a, N, no, lo, fo,              &
     &               ks_energies, E_tot, r, Rp, V_tot,density, orbitals,&
     &               reigen_eps, reigen_max_iter,                       &
     &               mixing_eps, mixing_alpha,                          &
     &               mixing_max_iter, perturb, ext_xc=iext)
       if ( E_tot .ne. 0 ) then
! Ry units
        V_tot = 2*V_tot
        E_tot = 2*E_tot
        density = density / 2
        allocate(rv(N+1),rho(N+1))
        rv = r * V_tot
        rho = pi4*r**2 * density
!        exit
       end if

       deallocate(ks_energies, no, lo, fo,                              &
     &  orbitals, V_tot, density)

!       if ( E_tot .ne. 0 ) exit


      return
      end subroutine dft_nr

      subroutine dft_rel(ized,r,Rp,rho,rv,E_tot,xc_rel,xc_ext)
      implicit none
!!ARGUMENTS:
      integer, intent(in) :: ized
      real(8), pointer, intent(out) :: r(:),Rp(:)
      real(8), pointer, intent(out) :: rv(:),rho(:)
      real(8), intent(out) :: E_tot
      integer, intent(in), optional :: xc_rel   ! if xc_rel==1 then rLDA functional is used;
                                                ! if xc_rel==-1 then c_light = 10^22
      logical, intent(in), optional :: xc_ext   ! if xc_ext==.true. then external functional is used
!
      real(8), parameter :: reigen_eps = 1d-9
      real(8), parameter :: mixing_eps = 1d-7
      real(8), parameter :: r_min = 1.d-8 , r_max = 50.d0   ! dftatom optimal mesh
      integer ::  N = 5268                       ! dftatom optimal mesh
      real(8) :: a =  6.2d+7                     ! dftatom optimal mesh
!      real(8) :: r_min = 1.d-7 , r_max = 40.d0
!      integer ::  N = 4000
!      real(8) :: a =  2.7d+6
      integer :: reigen_max_iter = 100
      real(8) :: mixing_alpha = 0.5d0
      integer :: mixing_max_iter = 200
      logical :: perturb
      real(8) :: c
      integer :: n_orb
      integer, allocatable, dimension(:) :: no, lo, so
      real(8), allocatable, dimension(:) :: fo, ks_energies
      real(8), allocatable :: orbitals(:, :)
      real(8), allocatable :: V_tot(:),density(:)
      integer :: irel
      logical :: iext
      real(8) :: rx
!
      if ( isLDAxc() ) then
       perturb = .true.
      else
       perturb = .false.
      end if
      c = inv_fine_struct_const      ! Hartree is used in atom_rlda
      irel = 0
      if ( present(xc_rel) ) then
        irel = max(0,xc_rel)
      end if
      iext = .false.
      if ( present(xc_ext) ) then
         iext = xc_ext
         if ( iext ) irel = 0       ! dftatom rlda is not activated with external library
      end if
      rx = r_max
      N = NN_rel6(ized)
!      a = exp(dble(N-1)/dble(N)*log(rx/r_min))   ! mecca exp mesh

      n_orb = get_atom_orb_rel(ized)
      allocate(ks_energies(n_orb), no(n_orb), lo(n_orb), fo(n_orb),     &
     &          so(n_orb), orbitals(N+1, n_orb), r(N+1), V_tot(N+1),    &
     &          density(N+1), Rp(N+1))
      if ( irel==0 .and. .not.isLDAxc() ) then
       call atom_rdft(ized, r_min, rx, a, N, c, no, lo, so, fo,         &
     &               ks_energies, E_tot, r, Rp, V_tot,density, orbitals,&
     &               reigen_eps, reigen_max_iter,                       &
     &               10.d0*mixing_eps, mixing_alpha,                    &
     &               mixing_max_iter, perturb,xc_rel=1)
!!       N = floor(dble(N) * ( 1 - log(rx/(rx*0.6d0))/log(rx/r_min)) )
!!       rx = rx*0.6d0
        call atom_rdft(ized, r_min, rx, a, N, c, no, lo, so, fo,        &
     &               ks_energies, E_tot, r, Rp, V_tot,density, orbitals,&
     &               reigen_eps, reigen_max_iter,                       &
     &               10.d0*mixing_eps, mixing_alpha,                    &
     &      mixing_max_iter, perturb,xc_rel=irel,ext_xc=iext,V_in=V_tot)
      else
       call atom_rdft(ized, r_min, rx, a, N, c, no, lo, so, fo,         &
     &               ks_energies, E_tot, r, Rp, V_tot,density, orbitals,&
     &               reigen_eps, reigen_max_iter,                       &
     &               mixing_eps, mixing_alpha,                          &
     &      mixing_max_iter, perturb,xc_rel=irel,ext_xc=iext)
       end if
       if ( E_tot .ne. 0 ) then
! Ry units
        V_tot = 2*V_tot
        E_tot = 2*E_tot
        density = density / 2
        allocate(rv(N+1),rho(N+1))
        rv = r * V_tot
        rho = pi4*r**2 * density
       end if

       deallocate(ks_energies, no, lo, fo, so,                          &
     &  orbitals, V_tot, density)

      return
      end subroutine dft_rel

      integer function NN_rel6(ized)
      implicit none
      integer, intent(in) :: ized
      if ( ized>53 .and. ized<65 ) then
       select case (ized)
        case(54)
         NN_rel6 = 3203
        case(55)
         NN_rel6 = 3270
        case(56)
         NN_rel6 = 3341
        case(57)
         NN_rel6 = 3407
        case(58)
         NN_rel6 = 3458
        case(59)
         NN_rel6 = 3495
        case(60)
         NN_rel6 = 3546
        case(61)
         NN_rel6 = 3593
        case(62)
         NN_rel6 = 3641
        case(63)
         NN_rel6 = 3685
        case(64)
         NN_rel6 = 3739
       end select
      else if ( ized <= 53 ) then
       NN_rel6 = 224 + (ized-1) * 56
      else
       NN_rel6 = 190 + (ized-1) * 56
      end if
      return
      end function NN_rel6

      integer function NN_nr6(ized)
      implicit none
      integer, intent(in) :: ized
      if ( ized<=43 ) then
       NN_nr6 = 2566 - (43-ized) * 50
       if ( ized<=5 ) then
        NN_nr6 = NN_nr6 - 160
       end if
      else
       NN_nr6 = 190 + (ized-1) * 56
      end if
      return
      end function NN_nr6

      subroutine get_etot(iZ,etot)
      implicit none
      integer, intent(in) :: iz(:)
      real(8), intent(out) :: etot(:)
      type(atom_rdata) :: atom
      integer :: i,iprint=-1
      etot = 0
      do i=1,size(iz)
       call get_atom(iZ(i),atom,iprint)
       etot(i) = atom%e_tot
      end do
      return
      end subroutine get_etot

      end module atom

!DELETE      real(8) function  weightedaverage_atom(M_S)
!DELETE      use atom, only : get_etot
!DELETE      use mecca_types, only : RunState
!DELETE      use gfncts_interface, only : g_atcon
!DELETE      implicit none
!DELETE!      integer, intent(in) :: iZ(:)
!DELETE!      real(8), intent(out) :: answ
!DELETE      type(RunState) :: M_S
!DELETE      real(8) :: answ
!DELETE      integer, allocatable :: iZ(:)
!DELETE      real(8), allocatable :: values(:)
!DELETE      real(8), allocatable :: wght(:)
!DELETE      real(8), allocatable :: atcon(:,:)
!DELETE      integer :: i,k,nsub,n_z
!DELETE
!DELETE      answ = 0
!DELETE      n_z = M_S%intfile%nelements
!DELETE      allocate(iZ(n_z),values(n_z),wght(n_z))
!DELETE      do i=1,n_z
!DELETE       iZ(i) =  M_S%intfile%elements(i)%ztot
!DELETE      end do
!DELETE      call get_etot(iZ,values)
!DELETE      if ( abs(sum(values))>epsilon(0.d0)/dble(10) ) then
!DELETE       if ( .not.allocated(atcon) ) allocate(atcon(1,1))
!DELETE       call g_atcon(M_S%intfile,atcon)
!DELETE       wght = 0
!DELETE       do i=1,size(iZ)
!DELETE        do nsub=1,size(atcon,2)
!DELETE         do k=1,M_S%intfile%sublat(nsub)%ncomp
!DELETE          if ( M_S%intfile%sublat(nsub)%compon(k)%zID==iZ(i) ) then
!DELETE           wght(i) = wght(i) + atcon(k,nsub)*M_S%intfile%sublat(nsub)%ns
!DELETE          end if
!DELETE         end do
!DELETE        end do
!DELETE       end do
!DELETE       answ = dot_product(wght,values)/sum(wght)
!DELETE      end if
!DELETE
!DELETE      weightedaverage_atom = answ
!DELETE
!DELETE      deallocate(iZ,values,wght)
!DELETE      return
!DELETE      end function weightedaverage_atom
