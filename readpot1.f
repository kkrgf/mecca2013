!BOP
!!ROUTINE: readpot1
!!INTERFACE:
      program readpot1
!!DESCRIPTION:
!  reads single-component potential file and 
!  prints potential or total charge density 
!  or valence charge density 
!
!  input information (name of file and print id) is provided 
!  from console, possible values of print id is 1, 2 or 3
!
!!USES:
      use mecca_constants
      use scf_io, only : Potfile, readpot
!
!!REVISION HISTORY:
! Initial Version - A.S. - 2013
!EOP
!
!BOC
      implicit real*8 (a-h,o-z)

      type(Potfile) :: siteptn
      logical :: endflg

      character*32 filename
      integer nfch,iout,iprint
      character(2) :: symb
      character(15) :: idname
      character(4)  :: idout(6)
      idout(1)='.ve.'
      idout(2)='.dt.'
      idout(3)='.dv.'
      idout(4)='.dc.'
      idout(5)='.vt.'
      idout(6)='.rh.'
      nfch = 1
      iout = 11

      write(0,*) ' filename, iprint (1: ((vr+2Z)/r-V0)'                 &
     &           //', 2: rhotot*4pi*r**2'                                        &
     &           //', 3: (rhotot-rhocor)*4pi*r**2'                               &
     &           //', 4: rhocor*4pi*r**2'                                        &
     &           //', 5: v'
      read(*,*)  filename, iprint
      if ( iprint>size(idout) ) then
       write(0,*) ' iprint=',iprint,', but should be <=',size(idout)
       stop 2
      end if
      open(nfch,file=filename,status='old')
      do 
       call readpot(nfch,siteptn,endflg)
       if ( endflg ) exit
       call atomID(siteptn%ztot,symb,idname)
       read(siteptn%comment,*) symb,idname
       open(iout,file=trim(filename)//idout(iprint)//trim(idname))
       call print_potfile(siteptn,iprint,iout)
       close(iout)
      end do
      stop
      end

!BOP
!!IROUTINE: print_potfile
!!INTERFACE:
      subroutine print_potfile(siteptn,iprint,iun)
!!DESCRIPTION:
! prints either potential or charge density
!
!!USES:
      use scf_io, only : Potfile

!!DO_NOT_PRINT
      implicit none
!!DO_NOT_PRINT

!!ARGUMENTS:
      type(Potfile), intent(in) :: siteptn
      integer, intent(in) :: iprint,iun
!  iprint=1 --> printing siteptn%vr(i)-siteptn%v0
!  iprint=2 --> printing siteptn%chgtot(i), i.e. rho(r)*4*pi*r**2
!  iprint=3 --> printing siteptn%chgtot(i)-siteptn%chgcor(i), i.e. rho_val*4*pi*r**2
!               and integrals for each (total,core,valence) density 
!!REVISION HISTORY:
! Initial Version - A.S. - 2013
!EOP
!
!BOC
      integer :: ndrpts,jmt,i
      real(8) :: xst,xmt,h,fi
      real(8), allocatable :: xr(:),rr(:),wi(:),rho(:)
      real(8), parameter :: pi=4*atan(1.d0)
      logical :: core_exist
      ndrpts = siteptn%ndrpts
      allocate(xr(ndrpts),rr(ndrpts),wi(ndrpts),rho(ndrpts))
      xst = siteptn%xst
      xmt = siteptn%xmt
      jmt = siteptn%jmt
      h = (xmt-xst)/(jmt-1)
      forall (i=1:size(xr)) xr(i) = xst + (i-1)*h
      rr = exp(xr)
!      write(*,*) ' xst,xmt,jmt',xst,xmt,jmt
!      write(*,*) ' rr(1),rr(jmt),rr(end)',rr(1),rr(jmt),rr(ndrpts)
!      stop
      select case (iprint)
       case (1)
          do i=1,ndrpts
            fi = siteptn%vr(i)-siteptn%v0
            write(iun,'(1x,i5,2x,f14.10,3x,f20.10)') i,rr(i),fi
          end do
       case (2)
          do i=1,siteptn%ndchg
            fi = siteptn%chgtot(i)
!            fi = siteptn%chgtot(i)/(4*pi*rr(i)**2)
            write(iun,'(1x,i5,2x,f14.10,3x,f20.10)') i,rr(i),fi
          end do
       case (3)
          core_exist = siteptn%ndcor>0
          do i=1,siteptn%ndchg
           fi = siteptn%chgtot(i)
!           fi = siteptn%chgtot(i)/(4*pi*rr(i)**2)
           if ( core_exist ) then
            if ( i<=siteptn%ndcor ) then
             fi = fi - siteptn%chgcor(i)
!             fi = fi - siteptn%chgcor(i)/(4*pi*rr(i)**2)
            end if 
           end if 
            write(iun,'(1x,i5,2x,f14.10,3x,f20.10)') i,rr(i),fi
          end do
          write(iun,*) '# TOT RHO*4*pi*R*R:'
          do i=0,2
            call qexpup(i,siteptn%chgtot,jmt,xr,wi)
            write(iun,'(''# i='',i2,3x,f20.10)') i,wi(jmt)
          end do 
          if ( core_exist ) then
           write(iun,*) '# COR RHO*4*pi*R*R:'
           do i=0,2
            call qexpup(i,siteptn%chgcor,jmt,xr,wi)
            write(iun,'(''# i='',i2,3x,f20.10)') i,wi(jmt)
           end do 
           rho = siteptn%chgtot-siteptn%chgcor
           write(iun,*) '# VAL RHO*4*pi*R*R:'
           do i=0,2
            call qexpup(i,rho,jmt,xr,wi)
            write(iun,'(''# i='',i2,3x,f20.10)') i,wi(jmt)
           end do 
           write(iun,*) '# VAL RHO:'
           rho = rho/(rr**2)
           do i=2,4
            call qexpup(i,rho,jmt,xr,wi)
            write(iun,'(''# i='',i2,3x,f20.10)') i,wi(jmt)
           end do 
          else
           write(iun,*) '# NO CORE RHO'
          end if
       case (4)
          core_exist = siteptn%ndcor>0
          if ( core_exist ) then
           do i=1,siteptn%ndcor
            fi = siteptn%chgcor(i)
!            fi = siteptn%chgcor(i)/(4*pi*rr(i)**2)
            write(iun,'(1x,i5,2x,f14.10,3x,f20.10)') i,rr(i),fi
           end do
          else
           write(iun,*) '# NO CORE RHO'
          end if
       case (5)
          do i=1,ndrpts
            fi = siteptn%vr(i)-siteptn%v0-2*siteptn%ztot/rr(i)
            write(iun,'(1x,i5,2x,f14.10,3x,f20.10)') i,rr(i),fi
          end do
       case (6)
          do i=1,siteptn%ndchg
            fi = siteptn%chgtot(i)/(4*pi*rr(i)**2)
            write(iun,'(1x,i5,2x,f14.10,3x,f20.10)') i,rr(i),fi
          end do
       case default
          stop 99
      end select
      deallocate(xr,rr,wi,rho)
      return
!EOC
      end subroutine print_potfile
