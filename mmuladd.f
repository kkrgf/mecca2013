!BOP
!!ROUTINE: mmuladd
!!INTERFACE:
      subroutine mmuladd(x,nxmat,y,nymat,i1,i2,kkrsz,zsclr,iswitch)
!!DESCRIPTION:
! wrapper for {\tt y} = {\tt y} + {\tt zsclr}*{\tt x}
! (complex operations)
!

!!DO_NOT_PRINT
      implicit none
!!DO_NOT_PRINT

!!ARGUMENTS:
      integer, intent(in) :: nxmat,nymat,i1,i2
      complex(8), intent(in) :: x(nxmat,*)
      complex(8), intent(inout) :: y(*)
      integer, intent(in) :: kkrsz,iswitch
      complex(8), intent(in) :: zsclr
!!REMARKS:
! iswitch=2 is disabled
!EOP
!
!BOC
      if(iswitch.ne.2) then
       call mmuladdz(x,nxmat,y,nymat,i1,i2,kkrsz,zsclr)
      else
       write(*,*) ' ERROR:  iswitch=2 is not supported'
       call fstop(' UNSUPPORTED FEATURE')
!disabled       call mmuladdr(x,nxmat,y,nymat,i1,i2,kkrsz,zsclr)
      end if
      return

!EOC
      contains

!BOP
!!IROUTINE: mmuladdz
!!INTERFACE:
      subroutine mmuladdz(x,nxmat,y,nymat,i1,i2,kkrsz,zsclr)
!!DESCRIPTION:
!   y = y + zsclr*x[i1,i2]
!

!!DO_NOT_PRINT
      implicit none
!!DO_NOT_PRINT

!!ARGUMENTS:
      integer, intent(in) :: nxmat
      complex(8), intent(in)    :: x(nxmat,*)
      integer, intent(in) :: nymat
      complex(8), intent(inout) :: y(nymat,kkrsz)
      integer, intent(in) :: i1,i2,kkrsz
      complex(8), intent(in)    ::   zsclr
!!REMARKS:
! private procedure of subrouine mmuladd
!EOP
!
!BOC
!c *******************************************************************
      integer iist,jjst,i,j
!c
      iist=(i2-1)*kkrsz
      jjst=(i1-1)*kkrsz
      y(1:kkrsz,1:kkrsz) = y(1:kkrsz,1:kkrsz) + zsclr*                  &
     &                     x(jjst+1:jjst+kkrsz,iist+1:iist+kkrsz)
      return
!EOC
      end subroutine mmuladdz

      end subroutine mmuladd

!!c     =================================================================
!      subroutine mmuladdr(x,nxmat,y,nymat,i1,i2,kkrsz,zsclr)
!!c     =================================================================
!!c
!      implicit none
!!c
!!cab      complex*16   x(nrmat,*)
!!cab      complex*16   y(kkrsz,kkrsz)
!!cab      complex*16   zsclr
!
!      integer nxmat,nymat,i1,i2,kkrsz
!      real*8       x(nxmat,*)
!!c
!!c     x(1:nrmat,1:nrmat) -- Re
!!c     x(1:nrmat,nrmat+1:nrmat*2) -- Im
!!c
!      real*8       y(0:1,nymat,kkrsz)
!      real*8       zsclr(0:1)
!!c *******************************************************************
!      integer iist,jjst,iistim,i,j
!      real*8  xre,xim,expre,expim
!!c
!      iist=(i2-1)*kkrsz
!      jjst=(i1-1)*kkrsz
!      iistim=iist+nxmat
!       do i=1,kkrsz
!            do j=1,kkrsz
!
!!cab               y(j,i) = x(jjst+j,iist+i)*zsclr
!!c       real part      -- x(jjst+j,iist+i)
!!c       imaginary part -- x(jjst+j,iistim+i)
!
!             xre = x(jjst+j,iist+i)
!             xim = x(jjst+j,iistim+i)
!             expre = zsclr(0)
!             expim = zsclr(1)
!             y(0,j,i) = y(0,j,i) + xre*expre-xim*expim
!             y(1,j,i) = y(1,j,i) + xre*expim+xim*expre
!          enddo
!         enddo
!      return
!      end
