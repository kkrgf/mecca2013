!BOP
!!ROUTINE: DFMND
!!INTERFACE:
      SUBROUTINE DFMND(F,X,Y,N,LIM,AUX,IER)
!!DESCRIPTION:
! MINIMIZATION OF A FUNCTION OF SEVERAL VARIABLES
! USING POWELL'S ALGORITHM WITHOUT DERIVATIVES
! {\bv
! INPUT DATA:
!   F .... THE FUNCTION OF N VARIABLES X(1)...X(N) TO BE MINIMIZED
!   X .... X(1) ... X(N) = STARTING GUESS FOR THE VARIABLES;
!          BEWARE: X HAS TO BE DIMENSIONED IN THE MAIN PROGRAM
!          TO CONSIDERABLY MORE THAN N.
!   N .... NUMBER OF VARIABLES; THE DIMENSION OF X AND AUX IN THE
!          CALLING PROGRAM MUST BE, HOWEVER, MUCH HIGHER:
!          - PERHAPS 10 TIMES?
!   LIM .. MAXIMUM NUMBER OF ITERATIONS
!   AUX .. AUXILIARY ARRAY OF THE SAME DIMENSION AS X.
! OUTPUT DATA:
!   X .... X(1) ... X(N) THE RESULTING MINIMUM
!   Y .... VALUE OF THE FUNCTION AT THE MINIMUM
!   IER .. SOME ERROR INDICATION
!          IERR=0 MEANS 'CONVERGENCE ACHIEVED'.
! \ev}
!EOP
!
!BOC
      implicit none
      external F
      real*8 F
      real*8 X(*), Y, AUX(*)
      integer N, LIM, IER
      real*8 EPS,ETA,TOL,                                               &
     &     DA,DB,DC,DM,DQ,FA,FB,FC,FL,FM,FS,HD,HQ,HX
      integer n1, n2, n3, n4 ,n5, ns, mf, isw, mk, l, m                 &
     &,       iw, ip, i, it, is, id, j, ic, k
!C      *      *      *      *      *      *      *      *
!C
      ISW  =IER
      IER  =0
      IF (N.LE.0) THEN
        IER = 1000
        RETURN
      END IF
      IF (LIM.LE.0) THEN
       IER = 2000
       RETURN
      END IF
!C
!C     SET INITIAL VALUES AND SAVE INITIAL ARGUMENT
!C
      N1   = N+1
      N2   =N+N
      N3   =N+N2
      N4   =N+N3
      N5   =N*N+N4
!DEBUG      EPS  =1.D-10
      EPS  =1.D-8
      ETA  =N*EPS
      AUX(1:N) = X(1:N)
      AUX(N3+1:N3+N) = 1.D0
      FS   =F(AUX(1:N))
      FB   =FS
      I    =1
      IC   =1
      IT   =0
      M    =N4
      MF   =0
      IS   =1
!C
!C     START ITERATION CYCLE
!C
    6 IT   =IT+1
      FL   =FS
      AUX(N1:N2) = 0.D0
      ID   =I
      I    =IC
      IW   =0
!C
!C     START MINIMIZATION ALONG NEXT DIRECTION
!C
    8 NS   =0
      IP   =0
      DB   =0.D0
      if (IW.eq.0) then
       HX   =AUX(I)
      else if (IT .gt. 1) then
       go to 14
      end if
      IF (IS.eq.1) then
       DM   =.1D0
       IF (ABS(HX).gt.1.D0) DM = -DM*HX
       GOTO 38
      end if
   14 if ( IS .eq. 2) then
       DM = HQ
       GOTO 38
      end if
!C
!C     INTERPOLATION USING ESTIMATE OF SECOND DERIVATIVE
!C
      IF (IW .eq. 1) then
       J = N2+I
      ELSE
       J = N3+I
      END IF
      HD   =AUX(J)
      if (IT .le. 2) then
       DC = HQ
      else
       DC   =1.D-2
      end if
      DM   =DC
      MK   =1
      GOTO 51

   24 DM   =DC*HD
      if (abs(DM) .lt. 1.d-13) DM = 1.d0
      DM   =.5D0*DC-(FM-FB)/DM
      MK   =2
      if (FM .lt. FB) then
       FC   =FB
       FB   =FM
       DB   =DC
       IF (DM .eq. DB) GOTO 67
       DC   =0.D0
       GOTO 51
      else
       if (DM .eq. DB) then
        DA = DC
        FA = FM
        GOTO 37
       else
        FC = FM
        GOTO 51
       end if
      end if

!C
!C     ANALYSE INTERPOLATED FUNCTION VALUE
!C
   32 if ( FM .gt. FB) then
       DA   =DM
       FA   =FM
      else
       DA   =DB
       FA   =FB
       DB   =DM
       FB   =FM
      end if
      IF ((DC-DA)*(DB-DA) .gt. 0.d0) goto 50
      IF (DB .ne. 0.d0) go to 67
   37 NS   =1
      DM   =-DC
!C
!C     LINEAR SEARCH FOR SMALLER FUNCTION VALUES
!C     ALONG CURRENT DIRECTION
!C
   38 if (NS .gt. 15) then
       if (FS .eq. FM) then
        MF   =N+2
        DB   =0.D0
        GOTO 67
       end if
       if (abs(DM) .gt. 1.D6) then
        IER  =100
        GOTO 67
       end if
      end if
      NS   =NS+1
      MK   =3
      GOTO 51

   44 IF (FM-FB) 45,46,47
   45 DA   =DB
      FA   =FB
      DB   =DM
      FB   =FM
      DM   =DM+DM
      GOTO 38
   46 IF (FS-FB) 47,45,47
   47 IF (NS-1) 48,48,49
   48 DA   =DM
      FA   =FM
      DM   =-DM
      GOTO 38
   49 DC   =DM
      FC   =FM
!C
!C     REFINE MINIMUM USING QUADRATIC INTERPOLATION
!C
   50 continue
!c      HD   =(FC-FB)/(DC-DB)+(FA-FB)/(DB-DA)
      if ( DC.ne.DB) then
       HD   =(FC-FB)/(DC-DB)
      else
!       HD   =(FC-FB)/(1.d-12)
       HD   =(FC-FB)/(EPS)
      end if
      if ( DB.ne.DA) then
       HD = HD + (FA-FB)/(DB-DA)
      else
!       HD = HD + (FA-FB)/(1.d-12)
       HD = HD + (FA-FB)/(EPS)
      end if
      DM   =.5D0*(DA+DC)
      if ( HD .ne. 0.d0)                                                &
     &  DM   = DM +(FA-FC)/(HD+HD)
      IP   =IP+1
      MK   =4
!C
!C     STEP ARGUMENT VECTOR AND CALCULATE FUNCTION VALUE
!C
   51 IF (IW-1) 54,52,54
   52 DO 53 K=1,N
         L    =M+K
   53    AUX(K)=X(K)+DM*AUX(L)
      GOTO 55
   54 AUX(I)=HX+DM
   55 FM   =F(AUX(1:N))
      GOTO (24,32,44,56),MK
!C
!C     ANALYSE INTERPOLATED FUNCTION VALUE
!C
   56 IF (FM-FB) 61,61,57
   57 IF (IP-3) 58,62,62
   58 if ((DC-DB)*(DM-DB) .gt. 0.d0) then
       DC   =DM
       FC   =FM
      else
       DA   =DM
       FA   =FM
      end if
      GOTO 50

   61 DB   =DM
      FB   =FM
!C
!C     CALCULATE NEW ESTIMATE OF SECOND DERIVATIVE
!C     ALONG THE CURRENT DIRECTION
!C
   62 continue
      if ( DC.ne.DA) then
       HD   =(HD+HD)/(DC-DA)
      else
       HD   =(HD+HD)/(EPS)
      end if
      if (IW .eq. 1) then
       J    =N2+I
      else
       J    =N3+I
      end if
      AUX(J)=HD
      if (FB .eq. FS) DB = 0.d0
!C
!C     SAVE ARGUMENT VECTOR WITH SMALLEST FUNCTION VALUE FOUND
!C
   67 IF (IW .eq. 1) then
       DO K=1,N
         L    =M+K
         J    =N+K
         HD   =DB*AUX(L)
         AUX(J)=AUX(J)+HD
         HD   =X(K)+HD
         AUX(K)=HD
         X(K) =HD
       END DO
      else
       J    =N+I
       AUX(J)=AUX(J)+DB
       HD   =HX+DB
       AUX(I)=HD
       X(I) =HD
      end if
      if (IER.eq.100) goto 108
!C
!C     DETERMINE DIRECTION FOR NEXT LINEAR SEARCH
!C
      FS   =FB
      IF (I .ge. N) I = 0
      I    =I+1
      if (IS .le. 0) then
       IF (DB) 77,76,77
   76  IF (I-IC) 8,77,8
   77  IC   =I
       IS   =1
       if (IT .gt. N) IW = 1
       I    =ID
       GOTO 8
      end if

      M    =M+N
      IF (M-N5) 82,81,81
   81 M    =N4
   82 if (IS .le. 1) then
       IF (I.le.1) IW = 1
       IF (I .ne. ID) go to 8
       HQ   =0.D0
       DO K=N1,N2
        HQ   =HQ+AUX(K)*AUX(K)
       END DO
       IF (HQ .le. 0.d0) then
        if (MF .eq. N1) IER = 200
        GOTO 108
       else
!        DQ   =SQRT(max(1.d-20,HQ))
        DQ   =SQRT(max(EPS**2,HQ))
        HQ = min(DQ,1.d0)
        DO K=N1,N2
         L    =M+K-N
         AUX(L)=AUX(K)/DQ
        END DO
        IS   =2
        GOTO 8
       end if
      end if
!C
!C     END OF ITERATION CYCLE
!C     TEST FOR TERMINATION OF MINIMIZATION
!C
      IS   =0
      TOL  =EPS
      IF (DABS(FS)-1.D0) 96,96,95
   95 TOL  =EPS*DABS(FS)
   96 IF (FL-FS-TOL) 100,100,97
   97 IF (IT-LIM) 99,99,98
   98 IER  =10
      GOTO 108
   99 MF   =0
      GOTO 6
  100 IF (MF-N1) 102,102,101
  101 IER  =200
      GOTO 108
  102 MF   =MF+1
      DQ   =0.D0

      DO 105 K=1,N
         J    =N+K
         IF (DABS(AUX(K))-1.D0) 103,103,104
  103    DQ   =DQ+DABS(AUX(J))
         GOTO 105
  104    continue
!         if ( dabs(aux(k)).le.1.d-12*abs(aux(j)) ) then
!          DQ = DQ + 1.d-12
         if ( dabs(aux(k)).le.EPS*abs(aux(j)) ) then
          DQ = DQ + EPS
         else
          DQ   =DQ+DABS(AUX(J)/AUX(K))
         end if
  105    CONTINUE

      IF (DQ-ETA) 108,108,106
  106 IF (MF .le. N) goto 6
      IER  =1
  108 Y    =FB
      RETURN
!EOC
      END
