!BOP
!!ROUTINE: sprsmap
!!INTERFACE:
      subroutine sprsmap(natom,mapstr,ndimbas,                          &
     &                   aij,Rnncut,rslatt,mapsprs)
!!DESCRIPTION:
! generates "sparse matrix map", {\tt mapsprs}: \\
! $mapsprs(I,J) \neq 0$ only if I and J are neighbours (Rij$<${\tt Rnncut})
! or I and J have common neighbour
!

!!DO_NOT_PRINT
      implicit real*8 (a-h,o-z)
!!DO_NOT_PRINT

!!ARGUMENTS:
      integer, intent(in) :: natom,ndimbas
      integer, intent(in) :: mapstr(ndimbas,natom)
      real(8), intent(in) :: aij(3,*),Rnncut,rslatt(3,3)
      integer, intent(out) :: mapsprs(natom,natom)
!!REVISION HISTORY:
! Adapted - A.S. - 2013
!EOP
!
!BOC

      character*10 sname
      parameter (sname='sprsmap')

      integer erralloc
!calloc      integer numnb(natom),indnb(natom,natom)
      integer, allocatable :: numnb(:)
      integer, allocatable :: indnb(:,:)

      parameter (zero=0.d0,one=1.d0)
      dimension tmpv(3),tmpv1(3),tmpv2(3),nlatt(3)

      ann = one
      do ic=1,3
       taumax =                                                         &
     &      sqrt(rslatt(1,ic)**2+rslatt(2,ic)**2+rslatt(3,ic)**2)
       if(taumax.lt.1.d-12) then
       write(6,*) sname//' :: IC=',ic,' TAUMAX=',taumax
       write(6,*) '       RSLATT:'
       write(6,*) rslatt(1,ic),rslatt(2,ic),rslatt(3,ic)
       call fstop(sname//' :: ABS(RSlatt) = 0 ???')
       end if
       nlatt(ic) = 1+(Rnncut)/taumax
       ann = (2*Rnncut)/taumax*ann
      end do
      ndimnn = ann*natom+1

      allocate(numnb(1:natom),indnb(1:ndimnn,1:natom),stat=erralloc)
      if(erralloc.ne.0) then
       write(6,*) ' NATOM=',natom
       write(6,*) ' ERRALLOC=',erralloc
       call fstop(sname//' :: ALLOCATION MEMORY PROBLEM')
      end if

      r2cut = Rnncut**2
      do i=1,natom
       numnb(i) = 0
      end do


      do i=1,natom
      do 50 j=i+1,natom

       ia = mapstr(i,j)
       do ic=1,3
      tmpv(ic) = aij(ic,ia)
       end do

       do i1=-nlatt(1),nlatt(1)
       d1 = i1
      do ic=1,3
        tmpv1(ic) = tmpv(ic)                                            &
     &          +d1*rslatt(ic,1)
      end do
      do i2=-nlatt(2),nlatt(2)
       d2 = i2
       do ic=1,3
         tmpv2(ic) = tmpv1(ic)                                          &
     &           +d2*rslatt(ic,2)
       end do
       do i3=-nlatt(3),nlatt(3)
        d3 = i3

        r2ij = zero
        do ic=1,3
         tmpv3 = tmpv2(ic)                                              &
     &          +d3*rslatt(ic,3)
         r2ij = r2ij+tmpv3**2
        end do

        if(r2ij.le.r2cut) then
         numnb(i) = numnb(i)+1
         numnb(j) = numnb(j)+1
         indnb(numnb(i),i) = j
         indnb(numnb(j),j) = i
         go to 50
        end if

       enddo
      enddo
       enddo

50    continue
      enddo

      mapsprs = 0
      do i=1,natom
       numnbi = numnb(i)
       mapsprs(i,i) = 1
       do jn=0,numnbi
      if(jn.eq.0) then
       j = i
      else
       j = indnb(jn,i)
      end if
      do kn=jn+1,numnbi
       k = indnb(kn,i)
       mapsprs(j,k) = 1
       mapsprs(k,j) = 1
      end do
       end do
      end do

      do i=1,natom
       numnbi = numnb(i)
       mapsprs(i,i) = 2
       do jn=1,numnbi
       j = indnb(jn,i)
       mapsprs(i,j) = 2
       end do
      end do

      nnmin = minval(numnb(1:natom))
      nnmax = maxval(numnb(1:natom))
       write(6,*) ' MIN NUMBER OF NN (without Per.Bound.Cond) =',nnmin
       write(6,*) ' MAX NUMBER OF NN (without Per.Bound.Cond) =',nnmax

      deallocate(numnb,indnb,stat=erralloc)
      if(erralloc.ne.0) then
       write(6,*) ' NATOM=',natom
       write(6,*) ' ERRALLOC=',erralloc
       call fstop(sname//' :: DEALLOCATION MEMORY PROBLEM')
      end if

!CDEBUG
!CDEBUG      call sprslvl(mapsprs,natom)
!CDEBUG

      return

!CDEBUG      CONTAINS
!CDEBUG
!CDEBUG      subroutine sprslvl(a,n)
!CDEBUG      integer n
!CDEBUG      integer a(n,n)
!CDEBUG      integer i,j,m
!CDEBUG
!CDEBUG      m = 0
!CDEBUG      an2 = n*n
!CDEBUG      do i=1,n
!CDEBUG       do j=1,n
!CDEBUG        if(a(j,i).eq.0) m=m+1
!CDEBUG       end do
!CDEBUG      end do
!CDEBUG      write(6,*) '*** Percentage of zero elements is about ',
!CDEBUG     *           m*100/an2,'%   N=',n
!CDEBUG      return
!CDEBUG      end subroutine


      end subroutine sprsmap


!COLD      do i=1,natom
!COLD      do 50 j=i+1,natom
!COLD
!COLD       ia = mapstr(i,j)
!COLD       do ic=1,3
!COLD        tmpv(ic) = aij(ic,ia)
!COLD       end do
!COLD
!COLD       do i1=0,2
!COLD        if(i1.eq.0) then
!COLD         d1 = zero
!COLD        else if(i1.eq.1) then
!COLD         d1 = one
!COLD        else if(i1.eq.2) then
!COLD         d1 = -one
!COLD        else
!COLD         d1 = (one+one)
!COLD        end if
!COLD        do ic=1,3
!COLD          tmpv1(ic) = tmpv(ic)
!COLD     +          +d1*rslatt(ic,1)
!COLD        end do
!COLD        do i2=0,2
!COLD         if(i2.eq.0) then
!COLD          d2 = zero
!COLD         else if(i2.eq.1) then
!COLD          d2 = one
!COLD        else if(i2.eq.2) then
!COLD         d2 = -one
!COLD        else
!COLD         d2 = (one+one)
!COLD        end if
!COLD         do ic=1,3
!COLD           tmpv2(ic) = tmpv1(ic)
!COLD     +           +d2*rslatt(ic,2)
!COLD         end do
!COLD         do i3=0,2
!COLD          if(i3.eq.0) then
!COLD           d3 = zero
!COLD          else if(i3.eq.1) then
!COLD           d3 = one
!COLD        else if(i3.eq.2) then
!COLD         d3 = -one
!COLD        else
!COLD         d3 = (one+one)
!COLD        end if
!COLD
!COLD          r2ij = zero
!COLD          do ic=1,3
!COLD           tmpv3 = tmpv2(ic)
!COLD     +          +d3*rslatt(ic,3)
!COLD           r2ij = r2ij+tmpv3**2
!COLD          end do
!COLD
!COLD          if(r2ij.le.r2cut) then
!COLD           numnb(i) = numnb(i)+1
!COLD           numnb(j) = numnb(j)+1
!COLD           indnb(numnb(i),i) = j
!COLD           indnb(numnb(j),j) = i
!COLD           go to 50
!COLD          end if
!COLD
!COLD         enddo
!COLD        enddo
!COLD       enddo
!COLD
!COLD50    continue
!COLD      enddo
