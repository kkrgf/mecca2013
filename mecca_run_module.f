!BOP
!!MODULE: mecca_run
!!INTERFACE:
      module mecca_run
!!DESCRIPTION:
! to control global {\sc mecca} run-state 
!
!!USES:
      use mecca_types, only : RunState
!
!!PUBLIC MEMBER FUNCTIONS:
      public setMS,getMS,nullMS
      public gGenName
!!REVISION HISTORY:
! Initial Version - A.S. - 2013
!EOP
!
!BOC
      type(RunState), pointer :: M_S => null()
      private M_S

!EOC
      contains

!BOP
!!IROUTINE: setMS
!!INTERFACE:
      subroutine setMS( mecca_state )
!!DESCRIPTION:
! to set up current {\sc mecca} run state
!
!!ARGUMENTS:
      type(RunState), target :: mecca_state
!!REVISION HISTORY:
! Initial Version - A.S. - 2013
!EOP
!
!BOC
      if ( .not. associated(M_S,mecca_state) ) then
        M_S => mecca_state
      end if
      return
!EOC
      end subroutine setMS

!BOP
!!IROUTINE: nullMS
!!INTERFACE:
      subroutine nullMS( mecca_state )
!!DESCRIPTION:
! to nullify current {\sc mecca} run state
!
!!ARGUMENTS:
      type(RunState), target, optional :: mecca_state
!!REVISION HISTORY:
! Initial Version - A.S. - 2013
!EOP
!
!BOC
      if ( present(mecca_state) ) then
       if ( associated(M_S,mecca_state) ) then
        nullify(M_S)
       end if
      else 
        nullify(M_S)
      end if 
      return
!EOC
      end subroutine nullMS

!BOP
!!IROUTINE: getMS
!!INTERFACE:
      function getMS()
      type(RunState), pointer :: getMS
!!DESCRIPTION:
! to get current {\sc mecca} run state
!
!!REVISION HISTORY:
! Initial Version - A.S. - 2013
!EOP
!
!BOC
      getMS => M_S
      return
!EOC
      end function

!BOP
!!IROUTINE: gGenName
!!INTERFACE:
      function gGenName()
      character(64), pointer :: gGenName
!!DESCRIPTION:
! to get name of current {\sc mecca} run state
!
!!REVISION HISTORY:
! Initial Version - A.S. - 2013
!EOP
!
!BOC
      character(64), target :: s=''
!      character(:), pointer :: gGenName   ! compilation failed with gfortran 4.6
!      character(1), target :: s=''
      if ( associated(M_S) ) then
        gGenName => M_S%intfile%genName
      else
        gGenName => s
      end if
      return
!EOC
      end function gGenName
!      
      end module mecca_run
