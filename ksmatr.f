!BOP
!!ROUTINE: ksmatr
!!INTERFACE:
      subroutine ksmatr(jcell,                                          &
!DELETE      subroutine ksmatr(iatom0,jatom0,jcell,                            &
     &    kx,ky,kz,aij,greens,                                          &
     &    numnbi,kkrsz,rsij,                                            &
     &    mapsnni,ndimnn,nnmax,                                         &
     &    jclust,                                                       &
     &    aksp,                                                         &
     &    invswitch,                                                    &
     &    istop)
!!DESCRIPTION:
! calculates block of k-space matrix {\tt aksp}, which is
! $ Fourier[greens] $ (I=iatom0,J=jatom0); $ {\bf k}=(kx,ky,kz) $

! {\bv
! A_IJ(k) = exp(-i*k*aij)*sum(exp(i*k*rij)*A(I|rij));
!             rij = rsij for J-type NN of atom I only;
!
! output: aksp_IJ(1:kkrsz*kkrsz), is defined only for neighbours of atom I
! \ev}
!
!!USES:
      use universal_const

!!DO_NOT_PRINT
      implicit none
!!DO_NOT_PRINT

!!ARGUMENTS:
!DELETE      integer    iatom0,jatom0
      integer    jcell
      real*8     kx,ky,kz,aij(3)
      integer    kkrsz,numnbi
      complex*16 greens(kkrsz*numnbi,*)   ! (kkrsz*numnbi,kkrsz*numnbi)
      real*8     rsij(3,*)
      integer    ndimnn
      integer    mapsnni(ndimnn,numnbi)
      integer    nnmax,jclust(nnmax)
      integer    invswitch
      character  istop*10
!
!! tau-matrix in k-space (output);
!
      complex(8), intent(out) :: aksp(*)

!!REVISION HISTORY:
! Adapted - A.S. - 2013
!EOP
      interface
       subroutine mmuladd(x,nxmat,y,nymat,i1,i2,kkrsz,zsclr,iswitch)
       implicit none
       integer, intent(in) :: nxmat,nymat,i1,i2
       complex(8), intent(in) :: x(nxmat,*)
       complex(8), intent(inout) :: y(*)
       integer, intent(in) :: kkrsz,iswitch
       complex(8), intent(in) :: zsclr
       end subroutine mmuladd
      end interface
!
! to fill matrix aksp the routine should be used for each {\tt iatom0}
!BOC
      character    sname*10
      parameter    (sname='ksmatr')
!c     ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

!      real*8 zero
!      parameter (zero=0.d0)

      real*8  rx,ry,rz
!c      real*8  aijx,aijy,aijz,rx,ry,rz
!c      integer ia,i1,i2,i0,j0,iaij,jtp,nnj,i,k0,l1,l2,m
!c      integer ij,n0,ndksp
      integer ij
!c      integer kkrsq2,kkrszi0,kkrszj0,kkrszk0,iaik,nrmat
      complex*16 expdot0,expdot
      integer k1sub,k2sub,nrmat

      nrmat = kkrsz*numnbi

      expdot0 = exp(-dcmplx(zero,kx*aij(1)+ky*aij(2)+kz*aij(3)))

      aksp(1:kkrsz*kkrsz) = 0

         k1sub =1

         do k2sub=1,nnmax
             if(jcell.eq.jclust(k2sub)) then
!c
!c  If neighbour (of atom "iatom0") is equivalent to atom "jcell"
!c  then Fourier summation is performed
!c
              ij = mapsnni(k1sub,k2sub)
              rx = rsij(1,ij)
              ry = rsij(2,ij)
              rz = rsij(3,ij)

              expdot = expdot0*exp(+dcmplx(zero,kx*rx+ky*ry+kz*rz))
!c
!c  aksp = aksp+greens[k1sub,k2sub]*expdot
!c
              call mmuladd(greens(:,1:nrmat),nrmat,aksp,kkrsz,          &
     &                          k1sub,k2sub,kkrsz,expdot,invswitch)

             end if
         end do
!c
!DELETE          call cpblk2(tautmp,kkrsz,1,1,                                 &
!DELETE     &                aksp,ndmat,iatom0,jatom0,kkrsz)

!c  --------------------------------------------------------------------
!c
      if (istop.eq.sname) call fstop(sname//' DEBUG')
!c
      return
!EOC
      end subroutine ksmatr
