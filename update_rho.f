!BOP
!!ROUTINE: update_rho
!!INTERFACE:
      subroutine update_rho(fold,xvalws,fnew,xvalwsnw,                  &
     &                     qtotmt,xr,                                   &
     &                     imix,alpha0,beta,                            &
     &                     komp,jmt,jend,nbasis,                        &
     &                     atcon,numbsub,mspn,                          &
     &                     mtasa,iprint,brfile1,brfile2)
!c    ===============================================================
!!DESCRIPTION:
!  charge density (simple or Broyden) mixing
!
!!USES:
      use universal_const
      use mecca_interface
      use scf_io, only : sCurrParams,rho_S_mix,rho_B_mix

!!DO_NOT_PRINT
      implicit none
!!DO_NOT_PRINT

!!ARGUMENTS:
      real(8)             :: fold(:,:,:,:)  ! (iprpts,ipcomp,ipsublat,ipspin) old rho
      real(8), intent(in) :: xvalws(:,:,:)  ! (ipcomp,ipsublat,ipspin) old valence charge
      real(8)             :: fnew(:,:,:,:)  ! (iprpts,ipcomp,ipsublat,ipspin) new rho
      real(8)             :: xvalwsnw(:,:,:)! (ipcomp,ipsublat,ipspin) new valence charge
      real(8)             :: qtotmt(:,:,:)  ! (ipcomp,ipsublat,ipspin)
      real(8), intent(in) :: xr(:,:,:)      ! (iprpts,ipcomp,ipsublat)
      integer, intent(in) :: imix           ! mix with Broyden or not (only for charges!)
      real(8), intent(in) :: alpha0,beta
      integer, intent(in) :: nbasis
      integer, intent(in) :: komp(nbasis)
      integer, intent(in) :: jmt(nbasis)    ! last MT/ASA point
      integer, intent(in) :: jend(nbasis)   ! last point with non-zero fnew/fold
      real(8), intent(in) :: atcon(:,:)     ! (ipcomp,nbasis)
      integer, intent(in) :: numbsub(nbasis)
      integer, intent(in) :: mspn,mtasa,iprint
      character(*), intent(in) :: brfile1,brfile2
!!REVISION HISTORY:
! Initial Version - A.S. - 2013
!EOP
!
!BOC
      real(8) :: dq,alpha,rescale,amix,bmix,dump,xvalt_in,totq1,totq2
      real(8), parameter :: dqlim = 0.3d0

!      real(8) :: xnjtop(size(xvalws,1),size(xvalws,2),size(xvalws,3))
!      real(8) :: xojtop(size(xvalws,1),size(xvalws,2),size(xvalws,3))
      integer :: ib,ik,is,jtop,jtop1,ndrpts
      integer :: mxcomp,lastit,st_env
!      integer :: ialpha

      character*10 sname/'update_rho'/

      ndrpts = size(fnew,1)
      alpha = alpha0
      bmix = beta
!DELETE      call gEnvVar('ENV_BMIX',.false.,st_env)
!DELETE      if ( st_env > 0 ) then
!DELETE        bmix = beta/st_env
!DELETE      end if

!c==================================================================
!c
!c scaling function for XVALWS (possibly scale by WS volume).

!      rescale=one
      dq = zero
      do ib=1,nbasis
       do ik=1,komp(ib)
!DEBUG         dq = max(dq,abs(                                               &
!DEBUG     &                   sum(xvalws(ik,ib,1:mspn))-                     &
!DEBUG     &                 sum(xvalwsnw(ik,ib,1:mspn))                      &
!DEBUG     &                  ))
         dq = dq + abs(                                                 &
     &                   sum(xvalws(ik,ib,1:mspn))-                     &
     &                 sum(xvalwsnw(ik,ib,1:mspn))                      &
     &                ) * atcon(ik,ib)*numbsub(ib)
       end do
      end do
      dq = dq/2
      xvalt_in = zero
      rescale = zero
      do ib=1,nbasis
       do ik=1,komp(ib)
            rescale = rescale + sum(xvalwsnw(ik,ib,1:mspn))             &
     &                   * atcon(ik,ib)*numbsub(ib)
            xvalt_in = xvalt_in + sum(xvalws(ik,ib,1:mspn))             &
     &                   * atcon(ik,ib)*numbsub(ib)
       end do
      end do
      if ( dq<rescale ) then
        dump = one-dq/rescale
      else
        dump = one/rescale
      end if  
      if ( abs(xvalt_in-rescale) > 0.5d0 ) then
        call delete_file(brfile1)
        call delete_file(brfile2)
!DEBUG        return                    ! xval's cannot be mixed
      end if
!c
!c===================================================================
!c
      if(iprint.ge.0) write(6,'(1x,a)') sname//': charge mixing'

       if ( mtasa == 0 ) then
        jtop = maxval(jmt(1:nbasis))
!        jtop1 = jtop+1
       else
        if ( mtasa == 1 ) then
         jtop = maxval(jmt(1:nbasis))
        else 
         jtop = min(maxval(jend(1:nbasis)),ndrpts-1)
        end if 
        jtop1 = jtop   
       end if 

!c===================================================================
       if(imix.eq.0) then
        if(alpha0.lt.0.9d0) then
         if(dq.gt.dqlim) then
          alpha = alpha0*dump
          if (iprint >= 0 ) then
           write(6,'(a,f7.4,a,f7.4)') ' charge dens. mixing: alpha0 =', &
     &                                         alpha0,' alpha =',alpha
          end if
         end if
        end if
!c
        call simplmx(fnew,fold,xvalwsnw,xvalws,                         &
     &               komp,nbasis,mspn,alpha,bmix)
        call sCurrParams(imix=rho_S_mix,alpha=alpha,beta=bmix)
!        call sphIntegrals(fnew,qtotmt,totq1)
!c
!c===================================================================
       else
!c===================================================================
!c
         if ( (one-dump) > 0.1d0 ) then  
          alpha = alpha0*dump
         end if
!c
!c broyden mixing ==================================================
!c
!c for Broyden requires only total rho and moments
!c   Must load XVALWS sum (for rho) and difference (for moments) to
!c   jtop+1 array location to maintain proper charge neutrality.
!c
!         ialpha = 0
         do is = 1,mspn
          do ib=1,nbasis
           do ik=1,komp(ib)

!             if (ialpha .eq. 0) then
!              if ( maxval(fnew(1:jtop,ik,ib,is)) .gt. zero) ialpha = 1
!             end if
             fnew(jend(ib)+1:ndrpts,ik,ib,is) = zero
             fold(jend(ib)+1:ndrpts,ik,ib,is) = zero

!             xnjtop(ik,ib,is) = fnew(jtop1,ik,ib,is)
!             xojtop(ik,ib,is) = fold(jtop1,ik,ib,is)

!!c  possibly scale XVALWS for mixing
             if ( jtop1>jtop ) then
              fnew(jtop1,ik,ib,is) = xvalwsnw(ik,ib,is)                 &
     &                          / rescale
              fold(jtop1,ik,ib,is) =  xvalws(ik,ib,is)                  &
     &                          / rescale
             end if
           enddo
          enddo
         enddo
!c
!         if (ialpha .eq. 1) alpha = one
!c
        mxcomp = maxval(komp(1:nbasis))
!        call sphIntegrals(fnew,qtotmt,totq2)
        call broyden(fnew(1:jtop1,1:mxcomp,1:nbasis,1:mspn),            &
     &                fold(1:jtop1,1:mxcomp,1:nbasis,1:mspn),           &
!DEBUG     &                xvalwsnw(1:mxcomp,1:nbasis,1:mspn),               &
     &                              komp,nbasis,                        &
     &                       mspn,jtop,brfile1,brfile2,2,alpha,bmix)
!c====================================================================
!        call sphIntegrals(fnew,qtotmt,totq1)
!        fnew(1:jtop ,1:mxcomp,1:nbasis,1:mspn) =                        &
!     &      fnew(1:jtop ,1:mxcomp,1:nbasis,1:mspn) * (totq2/totq1)
!        qtotmt(1:mxcomp,1:nbasis,1:mspn) =                              &
!     &      qtotmt(1:mxcomp,1:nbasis,1:mspn) * (totq2/totq1)
!        totq1 = totq2
        open(11,file=brfile1,status='unknown',form='unformatted')
        lastit = 1
        amix = alpha
        read(11,err=10,end=10)amix,lastit
        close(11)
10      continue        
        if ( amix .ne. alpha .or. lastit==1 ) then
         call sCurrParams(imix=rho_S_mix,alpha=amix,beta=bmix)
        else
         call sCurrParams(imix=rho_B_mix,alpha=alpha,beta=bmix)
        end if 
!c====================================================================
!c the scaled XVALWS were put in FNEW and FOLD above and mixed.
!c unscale them for use in SCF interation
!c
!         do is = 1,mspn
!          do ib=1,nbasis
!           do ik=1,komp(ib)
!            xvalwsnw(ik,ib,is) = fnew(jtop1,ik,ib,is)*rescale
!!DEBUG            xvalwsnw(ik,ib,is)= xvalwsnw(ik,ib,is)*rescale
!            fnew(jtop1,ik,ib,is) = xnjtop(ik,ib,is)
!            fold(jtop1,ik,ib,is) = xojtop(ik,ib,is)
!           enddo
!          enddo
!         enddo
!c
       end if
!c===================================================================
       call sphIntegrals(fnew,qtotmt,totq1)
       if ( iprint.ge.0 ) then 
        do is = 1,mspn
         do ib=1,nbasis
          do ik = 1,komp(ib)
            write(6,1000)                                               &
     &         ik,ib,is,xvalwsnw(ik,ib,is),qtotmt(ik,ib,is)
1000        format('  ik=',i2,' ib=',i5,' is=',i1,' xval=',f12.7,       &
     &             ' qtot=',f12.7)
          enddo
         enddo
        enddo
       end if
!c===================================================================
!c

       return

!EOC
       contains

!c =====================================================================
!BOP
!!IROUTINE: sphIntegrals
!!INTERFACE:
       subroutine sphIntegrals(rho4pirr,sphIntgr,tot_sum)
!!DESCRIPTION:
! calculates MT/ASA partial, {\tt sphIntgr}, and total integrals, {\tt tot\_sum}

!!REMARKS:
! private procedure of subroutine update_rho
!EOP
!
!BOC
       real(8), intent(in) :: rho4pirr(:,:,:,:)
       real(8), intent(out) :: sphIntgr(:,:,:),tot_sum
       real(8) :: wrk(size(rho4pirr,1))
!c =====================================================================
       tot_sum = zero
       do is = 1,mspn
        do ib=1,nbasis
         do ik = 1,komp(ib)
          call qexpup(1,rho4pirr(:,ik,ib,is),jmt(ib),xr(:,ik,ib),wrk)
          sphIntgr(ik,ib,is) = wrk(jmt(ib))                             &
     &          + rho4pirr(1,ik,ib,is)*exp(xr(1,ik,ib))/three
          tot_sum=tot_sum+sphIntgr(ik,ib,is)*(atcon(ik,ib)*numbsub(ib))
         enddo
        enddo
       enddo
       return
!EOC
       end subroutine sphIntegrals
!c =====================================================================
!


!c =====================================================================
!BOP
!!IROUTINE: simplmx
!!INTERFACE:
        subroutine simplmx(frn,fro,xvalwsnw,xvalws,                     &
     &                komp,nbasis,nspin,alla,beta)
!!REMARKS:
! private procedure of subroutine update_rho
!EOP
!c =====================================================================
!c subroutine to simple mix new and old potentials or charges ........
!c     nbasis    number of sublattices
!c     komp      number of compents/basis
!c     nspin      number of electron spins (1 or 2)
!c     alla      mixing parameter of non-magnetic part
!c     beta      mixing parameter of magnetic part
!c     frn:      new      function(r)
!c     fro:      previous function(r)
!c =====================================================================
!c
!
!BOC
        implicit none
!c
        real(8),  parameter :: half=one/two
!c+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!c
!c
        real(8), intent(inout) :: frn(:,:,:,:)  ! (iprpts,ipcomp,ipsublat,ipspin)
        real(8), intent(in)    :: fro(:,:,:,:)  ! (iprpts,ipcomp,ipsublat,ipspin)
        real(8), intent(inout) :: xvalwsnw(:,:,:)  ! (ipcomp,ipsublat,ipspin)
        real(8), intent(in)    :: xvalws(:,:,:)  !   (ipcomp,ipsublat,ipspin)
        integer, intent(in)    :: nbasis,komp(nbasis),nspin
        real(8), intent(in)    :: alla,beta
        integer  ib,ik,k
        real(8) :: fa(size(fro,1))
        real(8) :: fb(size(fro,1))
        real(8) :: fc(size(fro,1))
        real(8) :: fd(size(fro,1))
!c
        k = size(fro,1)
        do ib=1,nbasis
          do ik=1,komp(ib)
            if ( nspin==1 ) then
             fa = frn(1:k,ik,ib,1)
             fb = fro(1:k,ik,ib,1)
             frn(1:k,ik,ib,1) = (one-alla)*fb+alla*fa
            else
             fa = half*(frn(1:k,ik,ib,1)+frn(1:k,ik,ib,2))
             fb = half*(fro(1:k,ik,ib,1)+fro(1:k,ik,ib,2))
             fc = half*(frn(1:k,ik,ib,1)-frn(1:k,ik,ib,2))
             fd = half*(fro(1:k,ik,ib,1)-fro(1:k,ik,ib,2))
             frn(1:k,ik,ib,1) = (one-alla)*fb+alla*fa
             frn(1:k,ik,ib,2) = frn(1:k,ik,ib,1) -                      &
     &                                         ((one-beta)*fd + beta*fc)
             frn(1:k,ik,ib,1) = frn(1:k,ik,ib,1) +                      &
     &                                         ((one-beta)*fd + beta*fc)
            end if
          end do
        end do
!c
        do ib=1,nbasis
          k = komp(ib)
          if ( nspin==1) then
            fa(1:k) = xvalwsnw(1:k,ib,1)
            fb(1:k) = xvalws  (1:k,ib,1)
            xvalwsnw(1:k,ib,1) = (one-alla)*fb(1:k)+alla*fa(1:k)
          else
            fa(1:k) = half*(xvalwsnw(1:k,ib,1)+xvalwsnw(1:k,ib,2))
            fb(1:k) = half*(xvalws  (1:k,ib,1)+xvalws  (1:k,ib,2))
            fc(1:k) = half*(xvalwsnw(1:k,ib,1)-xvalwsnw(1:k,ib,2))
            fd(1:k) = half*(xvalws  (1:k,ib,1)-xvalws  (1:k,ib,2))
            xvalwsnw(1:k,ib,1) = (one-alla)*fb(1:k)+alla*fa(1:k)
            xvalwsnw(1:k,ib,2) = xvalwsnw(1:k,ib,1) -                   &
     &                                 ((one-beta)*fd(1:k)+beta*fc(1:k))
            xvalwsnw(1:k,ib,1) = xvalwsnw(1:k,ib,1) +                   &
     &                                 ((one-beta)*fd(1:k)+beta*fc(1:k))
          end if
        enddo
!c
        return
!EOC
        end subroutine simplmx
!c =====================================================================
!
       end subroutine update_rho

