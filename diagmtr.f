!BOP
!!ROUTINE: diagmtr
!!INTERFACE:
      subroutine diagmtr(l1,x,j1,j2,nrmat,kkrsz,iswitch)
!!DESCRIPTION:
!  this subroutine can nullify all non-diagonal elements of 
!  the block of complex matrix {\tt x} and higher l (above l1)
!  diagonal elements of the same block
! 
!!REMARKS:
! iswitch=2 is disabled
!EOP
!
!BOC
      implicit none
!c
!c  To null non-diagonal and some diagonal elements of the block
!c
      integer l1,j1,j2,nrmat,kkrsz,iswitch
      complex(8) :: x(*)
!c      character*10 sname/'diagmtr'/

      if(iswitch.ne.2) then
       call diagmtrz(l1,x,j1,j2,nrmat,kkrsz)
      else
       write(*,*) ' ERROR:  iswitch=2 is not supported'
       call fstop(' UNSUPPORTED FEATURE')
!disabled       call diagmtdr(l1,x,j1,j2,nrmat,kkrsz)
      end if
      return

!EOC
      contains

!BOP
!!IROUTINE: diagmtrz
!!INTERFACE:
      subroutine diagmtrz(l1,x,j1,j2,nrmat,kkrsz)
!!REMARKS:
! module procedure of subroutine diagmtr
!EOP
!
!BOC
      implicit none
!c
      integer, intent(in) :: l1,j1,j2,nrmat,kkrsz
      complex(8), intent(inout) :: x(nrmat,*)

      integer ii0,jj0,i,j
      integer ll1

      complex*16   xl1(l1*l1)

      ii0=(j2-1)*kkrsz
      jj0=(j1-1)*kkrsz

      do i=1,l1*l1
       xl1(i) = x(jj0+i,ii0+i)
      end do

      x(jj0+1:jj0+kkrsz,ii0+1:ii0+kkrsz) = 0
!      do i=1,kkrsz
!        do j=1,kkrsz
!           x(jj0+j,ii0+i)=(0.d0,0.d0)
!        enddo
!      enddo

      do i=1,l1*l1
       x(jj0+i,ii0+i) = xl1(i)
      end do

      return

!EOC
      end subroutine diagmtrz

      end subroutine diagmtr

!      subroutine diagmtdr(l1,x,j1,j2,nrmat,kkrsz)
!!c
!      implicit none
!!c
!      integer l1,j1,j2,nrmat,kkrsz
!      integer ii0,jj0,i,j
!!c      complex*16   x(nrmat,*)
!!c      complex*16   xl1(l1*l1)
!      real*8   x(0:1,nrmat,*)
!      real*8   xl1(0:1,l1*l1)
!
!      ii0=(j2-1)*kkrsz
!      jj0=(j1-1)*kkrsz
!
!      do i=1,l1*l1
!       xl1(0,i) = x(0,jj0+i,ii0+i)
!       xl1(1,i) = x(1,jj0+i,ii0+i)
!      end do
!
!      do i=1,kkrsz
!        do j=1,kkrsz
!           x(0,jj0+j,ii0+i)=0.d0
!           x(1,jj0+j,ii0+i)=0.d0
!        enddo
!      enddo
!
!      do i=1,l1*l1
!       x(0,jj0+i,ii0+i) = xl1(0,i)
!       x(1,jj0+i,ii0+i) = xl1(1,i)
!      end do
!
!      return
!      end
