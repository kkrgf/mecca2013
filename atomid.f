!BOP
!!ROUTINE: atomID
!!INTERFACE:
      subroutine atomID(zed,symb,name)
!!DESCRIPTION:
! provides lowcase ID ({\tt name} and {\tt symb) of element
! with atomic number {\tt zed}

!!DO_NOT_PRINT
      implicit none
!!DO_NOT_PRINT

!!ARGUMENTS:
      real*8 zed
      character*2 symb
      character*15 name
!!REVISION HISTORY:
! Initial Version - A.S. - 2007
!!REMARKS:
! zed should be non-negative and less than 104
! for empty sphere (zed=0), all-uppercase ID is used.
!EOP
!
!BOC
      character*2  sym(0:103)
      character*15 nam(0:103)
      integer :: isave=0,ized
      save isave,sym,nam
!c
!c =====================================================================
!c

      if(isave.eq.0) then
       isave = 1
       sym(  0)='ES'
       nam(  0)='EMPTY_SPHERE   '
       sym(  1)='h '
       nam(  1)='hydrogen       '
       sym(  2)='he'
       nam(  2)='helium         '
       sym(  3)='li'
       nam(  3)='lithium        '
       sym(  4)='be'
       nam(  4)='berylium       '
       sym(  5)='b '
       nam(  5)='boron          '
       sym(  6)='c '
       nam(  6)='carbon         '
       sym(  7)='n '
       nam(  7)='nitrogen       '
       sym(  8)='o '
       nam(  8)='oxygen         '
       sym(  9)='f '
       nam(  9)='fluorine       '
       sym( 10)='ne'
       nam( 10)='neon           '
       sym( 11)='na'
       nam( 11)='sodium         '
       sym( 12)='mg'
       nam( 12)='mangnesium     '
       sym( 13)='al'
       nam( 13)='aluminum       '
       sym( 14)='si'
       nam( 14)='silicon        '
       sym( 15)='p '
       nam( 15)='phosphorus     '
       sym( 16)='s '
       nam( 16)='sulfur         '
       sym( 17)='cl'
       nam( 17)='chlorine       '
       sym( 18)='ar'
       nam( 18)='argon          '
       sym( 19)='k '
       nam( 19)='potassium      '
       sym( 20)='ca'
       nam( 20)='calcium        '
       sym( 21)='sc'
       nam( 21)='scandium       '
       sym( 22)='ti'
       nam( 22)='titanium       '
       sym( 23)='v '
       nam( 23)='vanadium       '
       sym( 24)='cr'
       nam( 24)='chromium       '
       sym( 25)='mn'
       nam( 25)='manganese      '
       sym( 26)='fe'
       nam( 26)='iron           '
       sym( 27)='co'
       nam( 27)='cobalt         '
       sym( 28)='ni'
       nam( 28)='nickel         '
       sym( 29)='cu'
       nam( 29)='copper         '
       sym( 30)='zn'
       nam( 30)='zinc           '
       sym( 31)='ga'
       nam( 31)='gallium        '
       sym( 32)='ge'
       nam( 32)='germanium      '
       sym( 33)='as'
       nam( 33)='arsenic        '
       sym( 34)='se'
       nam( 34)='selenium       '
       sym( 35)='br'
       nam( 35)='bromine        '
       sym( 36)='kr'
       nam( 36)='krypton        '
       sym( 37)='rb'
       nam( 37)='rubidium       '
       sym( 38)='sr'
       nam( 38)='strontium      '
       sym( 39)='y '
       nam( 39)='yttrium        '
       sym( 40)='zr'
       nam( 40)='zirconium      '
       sym( 41)='nb'
       nam( 41)='niobium        '
       sym( 42)='mo'
       nam( 42)='molybdenum     '
       sym( 43)='tc'
       nam( 43)='technetium     '
       sym( 44)='ru'
       nam( 44)='ruthenium      '
       sym( 45)='rh'
       nam( 45)='rhodium        '
       sym( 46)='pd'
       nam( 46)='palladium      '
       sym( 47)='ag'
       nam( 47)='silver         '
       sym( 48)='cd'
       nam( 48)='cadmium        '
       sym( 49)='in'
       nam( 49)='indium         '
       sym( 50)='sn'
       nam( 50)='tin            '
       sym( 51)='sb'
       nam( 51)='antimony       '
       sym( 52)='te'
       nam( 52)='tellurium      '
       sym( 53)='i '
       nam( 53)='iodine         '
       sym( 54)='xe'
       nam( 54)='xenon          '
       sym( 55)='cs'
       nam( 55)='cesium         '
       sym( 56)='ba'
       nam( 56)='barium         '
       sym( 57)='la'
       nam( 57)='lanthanum      '
       sym( 58)='ce'
       nam( 58)='cerium         '
       sym( 59)='pr'
       nam( 59)='praseodymium   '
       sym( 60)='nd'
       nam( 60)='neodymium      '
       sym( 61)='pm'
       nam( 61)='promethium     '
       sym( 62)='sm'
       nam( 62)='samarium       '
       sym( 63)='eu'
       nam( 63)='europium       '
       sym( 64)='gd'
       nam( 64)='gadolinium     '
       sym( 65)='tb'
       nam( 65)='terbium        '
       sym( 66)='dy'
       nam( 66)='dysprosium     '
       sym( 67)='ho'
       nam( 67)='holmium        '
       sym( 68)='er'
       nam( 68)='erbium         '
       sym( 69)='tm'
       nam( 69)='thulium        '
       sym( 70)='yb'
       nam( 70)='ytterbium      '
       sym( 71)='lu'
       nam( 71)='lutetium       '
       sym( 72)='hf'
       nam( 72)='hafnium        '
       sym( 73)='ta'
       nam( 73)='tantalum       '
       sym( 74)='w '
       nam( 74)='tungsten       '
       sym( 75)='re'
       nam( 75)='rhenium        '
       sym( 76)='os'
       nam( 76)='osmium         '
       sym( 77)='ir'
       nam( 77)='iridium        '
       sym( 78)='pt'
       nam( 78)='platinum       '
       sym( 79)='au'
       nam( 79)='gold           '
       sym( 80)='hg'
       nam( 80)='mercury        '
       sym( 81)='tl'
       nam( 81)='thallium       '
       sym( 82)='pb'
       nam( 82)='lead           '
       sym( 83)='bi'
       nam( 83)='bismuth        '
       sym( 84)='po'
       nam( 84)='polonium       '
       sym( 85)='at'
       nam( 85)='astatine       '
       sym( 86)='rn'
       nam( 86)='radon          '
       sym( 87)='fr'
       nam( 87)='francium       '
       sym( 88)='ra'
       nam( 88)='radium         '
       sym( 89)='ac'
       nam( 89)='actinium       '
       sym( 90)='th'
       nam( 90)='thorium        '
       sym( 91)='pa'
       nam( 91)='protactinium   '
       sym( 92)='u '
       nam( 92)='uranium        '
       sym( 93)='np'
       nam( 93)='neptunium      '
       sym( 94)='pu'
       nam( 94)='plutonium      '
       sym( 95)='am'
       nam( 95)='americium      '
       sym( 96)='cm'
       nam( 96)='curium         '
       sym( 97)='bk'
       nam( 97)='berkelium      '
       sym( 98)='cf'
       nam( 98)='californium    '
       sym( 99)='es'
       nam( 99)='einsteinium    '
       sym(100)='fm'
       nam(100)='fermium        '
       sym(101)='md'
       nam(101)='mendelevium    '
       sym(102)='no'
       nam(102)='nobelium       '
       sym(103)='lr'
       nam(103)='lawrencium     '
      end if

      ized = nint(zed)
      if(ized.lt.0.or.ized.gt.103) then
        write(*,*) ' IZED=',ized
        stop ' ATOMID: Wrong value of ZED '
      end if
      name=nam(ized)
      symb=sym(ized)

      return
      end subroutine atomID

!BOP
!!ROUTINE: atomIDc
!!INTERFACE:
      subroutine atomIDc(zed,symb,name)
!!DESCRIPTION:
! provides first-letter capitalized ID ({\tt name} and {\tt symb) of element
! with atomic number {\tt zed}

!!DO_NOT_PRINT
      implicit none
!!DO_NOT_PRINT

!!ARGUMENTS:
      real*8 zed
      character*1 symb(2)
      character*1 name(15)
!!REVISION HISTORY:
! Initial Version - A.S. - 2007
!!REMARKS:
! types of output arguments are not the same as 
! for similar subroutine atomID;
! zed should be non-negative and less than 104
!EOP
!
!BOC
      character*2 symb__
      character*1 symb_(2)
      equivalence (symb_,symb__)
      character*15 name__
      character*1 name_(15)
      equivalence (name_,name__)
      integer i1
      call atomID(zed,symb__,name__)
      symb = symb_
      name = name_
      if ( zed==0 ) return
      i1 = iachar(symb(1))
      if ( (i1 .ge. 97) .and. (i1 .le. 122) ) then
       symb(1) = achar(i1-32)
      end if
      i1 = iachar(name(1))
      if ( (i1 .ge. 97) .and. (i1 .le. 122) ) then
       name(1) = achar(i1-32)
      end if
      return
      end subroutine atomIDc
