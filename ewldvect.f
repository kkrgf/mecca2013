!BOP
!!ROUTINE: ewldvect
!!INTERFACE:
      subroutine ewldvect(vectin,nin,ndimin,cutoff,                     &
     &                    point,npnt,                                   &
     &                    vectout,nout,ndimout,                         &
     &                    np2v,numbv)
!!DESCRIPTION:
!
! constructs array {\tt vectout} (for Ewald summation):\\
! for each point(1:npnt) -- abs({\tt vectout = vectin - point}) $<$ Cutoff
! \\ \\
!   AND
! \\ \\
! all elements of {\tt vectout} are different
! {\bv
! input: vectin,nin,point,npnt,cutoff
!        (ndimin,ndimout -- 1st dimensions)
! output:
!        vectout,nout,
!        np2v  -- pointers to elements of vectout
!        numbv -- number of vectors inside cutoff-sphere
! \ev}

!!DO_NOT_PRINT
      implicit none
!!DO_NOT_PRINT

!!ARGUMENTS:
      integer, intent(in) :: nin,ndimin
      real(8), intent(in) :: vectin(ndimin,3),cutoff
      integer, intent(in) :: npnt
      real(8), intent(in) :: point(3,npnt)
      integer, intent(out):: nout
      integer :: ndimout
      real(8), allocatable :: vectout(:,:)  !  (ndimout,4)
      integer :: np2v(ndimin,npnt),numbv(npnt)
!EOP
!
!BOC
      integer i,j,k,nvect,ndimout1
      real*8 v(3),tol,vsq2,rmax2
      real(8), allocatable :: tmpvec(:,:)
      parameter (tol=1.d-8)
      character    sname*10
      parameter    (sname='ewldvect')

      if( allocated(vectout) ) deallocate(vectout)
      ndimout = ndimin
      allocate(vectout(ndimout,4))
      vectout = 0
      rmax2 = cutoff**2 * (1.d0+2*tol)
      nout  = 0
      do k=1,npnt
       nvect = 0
       do 1 i=1,nin
        v(1:3) = vectin(i,1:3)-point(1:3,k)
        vsq2 = dot_product(v,v)
        if(vsq2<rmax2) then
         nvect = nvect+1
         do j=1,nout
          if ( abs(vsq2-vectout(j,4))<tol**2 ) then
           if( abs(v(1)-vectout(j,1))<tol ) then
            if( abs(v(2)-vectout(j,2))<tol ) then
             if( abs(v(3)-vectout(j,3))<tol ) then
              np2v(nvect,k) = j
              go to 1
             end if
            end if
           end if
          end if
         end do
         nout = nout+1
         if(nout.gt.ndimout) then
           allocate(tmpvec(ndimout,4))
           tmpvec = vectout
           deallocate(vectout)
           ndimout1 = ndimout+max(ndimin,ndimout/10)
           allocate(vectout(ndimout1,4))
           vectout(1:ndimout,1:4) = tmpvec(1:ndimout,1:4)
           deallocate(tmpvec)
           vectout(ndimout+1:,1:4) = 0
           ndimout=ndimout1
         end if
         vectout(nout,1:3) = v
         vectout(nout,4) = vsq2
         np2v(nvect,k) = nout
        end if
1      continue

       if ( k > size(numbv) ) then
         write(6,*) ' NUMBV: K=',k,' SIZE=',size(numbv,1)
         call p_fstop(sname//' BAD MEMORY ALLOCATION')
       end if
       numbv(k) = nvect
!       if(cutoff.ge.0.d0) then
!        call vsrtrd(tmparr,nvect,np2v(1,k))
!       end if
      end do

      return
!EOC
      end subroutine ewldvect
