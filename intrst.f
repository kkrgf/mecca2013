!BOP
!!ROUTINE: intrst
!!INTERFACE:
      subroutine intrst(icryst,r,u)
!!DESCRIPTION:
! {\bv
! this subroutine calculates the average over solid angles
! within the wigner-seitz cell of three polynomials:
! u(1)=<1>,  u(2)=<z**4>,  u(3)=<z**2*y**2*z**2>
! these three averages vary with r, the distance from the
! center of the cell.  they can be used to calculate the
! average over solid angles within the wigner seitz cell of
! the three lowest degree cubic harmonics with full cubic
! symmetry.
! input:
!       icryst = 1(fcc) or 2(bcc)
!       r    distance from cell origin in units of the
!            lattice parameter
! output:
!       u(1),u(2),u(3)
! \ev}
!
!!USES:
      use universal_const
!
!!REVISION HISTORY:
!   modification of function w (whb - 12/10/85)
!EOP
!
!BOC
      implicit real*8(a-h,o-z)
      dimension u(3),ri(4,2)
!c
      data ri/.3535533906d+00,.4082482905d+00,.4330127019d+00,0.5d+00,  &
     &        .4330127019d+00,0.500000000d+00,.5303300859d+00,          &
     &        .5590169944d+00/
!c
      r1=ri(1,icryst)
      r2=ri(2,icryst)
      r3=ri(3,icryst)
      r4=ri(4,icryst)
!c
      if (icryst.eq.1)  go to 505
!c
!c----------- this section is for bcc cells -------------
!c
      if (r.gt.r1) go to 10
        u(1)=1.0d+00
        u(2)=1.0d+00/5.0d+00
        u(3)=1.0d+00/105.0d+00
      return
!c
 10       if(r.gt.r2) go to 20
      x=r1/r
      x2=x*x
      x3=x*x2
      x5=x3*x2
      x7=x5*x2
        u(1)=-3.0d+00+4.0d+00*x
        u(2)=2.0d+00*(-0.3d+00+x/3.0d+00+2.0d+00*x3/9.0d+00             &
     &       - 7.0d+00*x5/45.0d+00 )
        u(3)=2.0d+00*( -1.0d+00/70.0d+00 + x/54.0d+00 + x3/27.0d+00     &
     &                 - x5/10.0d+00 + 4.0d+00*x7/63.0d+00)
      return
!c
 20       if(r.gt.r3) go to 30
      x=r1/r
      y=r2/r
      x2=x**2
      x3=x*x2
      x5=x3*x2
      x7=x5*x2
      y2=y**2
      y3=y*y2
      y5=y2*y3
      y7=y2*y5
        u(1)=( -6.0d+00 + 4.0d+00*x + 3.0d+00*y )
        u(2)=2.0d+00*( -0.6d+00 + x/3.0d+00 + 2.0d+00*x3/9.0d+00        &
     &                 - 7.0d+00*x5/45.0d+00                            &
     &     + 3.0d+00*y/8.0d+00 -y3/4.0d+00 + 7.0d+00*y5/40.0d+00 )
        u(3)=6.0d+00*( -1.0d+00/210.0d+00 + x/162.0d+00 +               &
     &                  x3/81.0d+00 - x5/30.0d+00                       &
     &      + 4.0d+00*x7/189.0d+00 -(8.0d+00/105.0d+00-y3/3.0d+00       &
     &      + 0.4d+00*y5 - y7/7.0d+00)/16.0d+00)
      return
!c
 30       if(r.gt.r4)  go to 40
      x=2.0d+00*(r4 - r)
      x2=x**2
      x3=x*x2
        u(1)=12.59099924d+00*x2  + 280.45545d+00*x3
        u(2)= 2.76100566d+00*x2  +  69.24209169d+00*x3
        u(3)=0.0d+00
      return
!c
 40     u(1)=0.0d+00
        u(2)=0.0d+00
        u(3)=0.0d+00
!c
      return
!c----------------------------------------------------------------
!c  this section is for fcc
 505      continue
      if (r.gt.r1)  go to 510
        u(1)=1.0d+00
        u(2)=0.2d+00
        u(3)=1.0d+00/105.0d+00
      return
!c
 510     if(r.gt.r2)  go to 520
      x=r1/r
      x2=x**2
      x3=x*x2
      x5=x3*x2
      x7=x5*x2
        u(1)=6.0d+00*x-5.0d+00
        u(2)=( -1.0d+00 + 9.0d+00*x/8.0d+00 + x3/4.0d+00                &
     &         -7.0d+00*x5/40.0d+00 )
        u(3)=( -1.0d+00/21.0d+00 + 3.0d+00*x/32.0d+00 -                 &
     &          7.0d+00*x3/32.0d+00 + 57.0d+00*x5/160.0d+00             &
     &          -39.0d+00*x7/224.0d+00 )
!c
      return
!c
 520     if(r.gt.r3)  go to 530
      h=(r3-r2)/4.0d+00
      x1=(r-r2)/h
      x2=(x1-1.0d+00)/2.0d+00
      x3=(x1-2.0d+00)/3.0d+00
      x4=(x1-3.0d+00)/4.0d+00
        u(1)=(.051364d+00-.014479d+00*x1+.004896d+00*x1*x2              &
     &       -.002132d+00*x1*x2*x3+.001403d+00*x1*x2*x3*x4)             &
     &       *12.0d+00/pi
        u(2)=(.0403999d+00-.0092789d+00*x1+.0025612d+00*x1*x2           &
     &         -.0011306d+00*x1*x2*x3                                   &
     &         +.0008772d+00*x1*x2*x3*x4)*4.0d+00/pi
        u(3)=(.371724d+00-.19314d+00*x1+.08624d+00*x1*x2                &
     &        -.03294d+00*x1*x2*x3                                      &
     &       +.01718d+00*x1*x2*x3*x4)*.012d+00/pi
      return
!c
 530     if(r.gt.r4)  go to 540
      h=(r4-r3)/4.0d+00
      x1=(r-r3)/h
      x2=(x1-1.0d+00)/2.0d+00
      x3=(x1-2.0d+00)/3.0d+00
      x4=(x1-3.0d+00)/4.0d+00
        u(1)=(.0156709d+00-.008243d+00*x1+.0036633d+00*x1*x2            &
     &       -.0013226d+00*x1*x2*x3+.0006117d+00*x1*x2*x3*x4)           &
     &       *12.0d+00/pi
        u(2)=(.0150063d+00-.0077268d+00*x1+.0032737d+00*x1*x2           &
     &        -.0010388d+00*x1*x2*x3                                    &
     &       +.0004139d+00*x1*x2*x3*x4)*4.0d+00/pi
        u(3)=(.205926d+00-.185703d+00*x1+.166578d+00*x1*x2              &
     &        -.148541d+00*x1*x2*x3                                     &
     &       +.131582d+00*x1*x2*x3*x4)*1.0d-05*12.0d+00/pi
      return
!c
 540    u(1)=0.0d+00
        u(2)=0.0d+00
        u(3)=0.0d+00
!c
      return
!EOC
      end subroutine intrst
