!BOP
!!ROUTINE: mmul
!!INTERFACE:
      subroutine mmul(amt,bmt,cmt,kkrsz)
!!DESCRIPTION:
! calculates {\tt cmt} = {\tt amt} * {\tt bmt},
! {\tt amt}, {\tt bmt}, {\tt cmt} are complex square matrices;
! {\tt amt} or {\tt bmt} can be changed on exit if used in place of {\tt cmt}.

!!DO_NOT_PRINT
      implicit none
!!DO_NOT_PRINT

!!ARGUMENTS:
        complex(8) :: amt(:,:)
        complex(8) :: bmt(:,:)
        complex(8) :: cmt(:,:)
        integer, intent(in) ::  kkrsz
!!REVISION HISTORY:
! Initial version - bg & gms - Nov 1991
! Adapted - A.S. - 2013
! Revised - A.S. - 2018
!EOP
!
      interface
        subroutine wrtmtx(x,n,istop)
        implicit none
        integer, intent(in) :: n
        complex(8), intent(in) :: x(:,:)
        character(10), intent(in), optional :: istop
        end subroutine wrtmtx
      end interface
!BOC
      integer :: ndkkr
      complex(8), parameter :: cone=(1.d0,0.d0),czero=(0.d0,0.d0)
      complex(8) :: ctmp(size(cmt,1),kkrsz)
!
      ndkkr=size(cmt,1)
      call zgemm('N','N',kkrsz,kkrsz,kkrsz,cone,                        &
     &            amt,size(amt,1),                                      &
     &            bmt,size(bmt,1),                                      &
     &            czero,ctmp,ndkkr)
      call zcopy(ndkkr*kkrsz,ctmp,1,cmt,1)
!      cmt(1:kkrsz,1:kkrsz) = matmul(amt(1:kkrsz,1:kkrsz),               &
!     &                                             bmt(1:kkrsz,1:kkrsz))
!      if (iprint.ge.2) then
!         write(6,'('' mmul:: kkrsz :'',i5)') kkrsz
!         call wrtmtx(cmt,kkrsz)
!      endif
!
      return
!EOC
      end subroutine mmul
