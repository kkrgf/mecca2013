!BOP
!!MODULE: universal_const
!!INTERFACE:
      module universal_const
!!DESCRIPTION:
! it is for world constants, units\_conversion constants and basic types
! definitions parameters.
!
!!REVISION HISTORY:
! Initial Version - A.S. - 2013
!
!EOP
!
!BOC
      implicit none
      integer, parameter :: DBL_R = selected_real_kind(15)
      integer, parameter :: SNG_R = selected_real_kind(6)
!
      real(DBL_R), parameter ::                                         &
     &              zero=0._DBL_R,one=1._DBL_R,two=2._DBL_R,            &
     &              three=3._DBL_R,four=4._DBL_R,five=5._DBL_R,         &
     &              six=6._DBL_R,seven=7._DBL_R,eight=8._DBL_R,         &
     &              nine=9._DBL_R,ten=10._DBL_R
!
      complex(DBL_R), parameter ::                                      &
     &              czero = cmplx(0._DBL_R,0._DBL_R,DBL_R),             &
     &              cone  = cmplx(1._DBL_R,0._DBL_R,DBL_R)
!
      real (DBL_R), parameter :: pi=3.1415926535897932385_DBL_R

! inverse fine-structure constant: (4*pi*epsilon_0)*(light_speed*planck_const/(2*pi)) / elementary_charge^2
      real(DBL_R),parameter :: inv_fine_struct_const=137.035999139_DBL_R

! Atomic Rydberg units
! energy unit = Rydberg

! reduced_plank_const = one
      real(DBL_R), parameter :: planck_const = two*pi
! elementary charge = 1.6021766208d-19 (SI)
      real(DBL_R), parameter :: elctrn = sqrt(two)  ! value of electron (elementary) charge in a.u.
! mass unit, electron mass = 9.10938356e-31 kg
      real(DBL_R), parameter :: m_e = one/two  ! electron mass in a.u.

! coefficient Ry --> Hartree: 1 Ry = ry2H
      real(DBL_R), parameter :: ry2H = 0.5_DBL_R
! coefficient Ry --> eV: 1 Ry = ry2eV
      real(DBL_R), parameter :: ry2eV = 13.60569301_DBL_R
! coefficient Ry --> Kelvin (T); 1 Ry = ry2kelvin
      real(DBL_R), parameter :: ry2kelvin=157887.565_DBL_R
! coefficient Ry/Bohr^3 --> Mbar (pressure); 1 Ry/Bohr^3 = pfact
      real(DBL_R), parameter :: pfact=147.10508_DBL_R
! coefficient Bohr --> Angstroem (distance); 1 Bohr = bohr2A
      real(DBL_R), parameter :: bohr2A = 0.529177211_DBL_R
!
! coefficient to convert mu_B*MagnField to Ry; mu_B = amuBoT * (Ry/Tesla !)
      real(DBL_R), parameter :: amuBoT = 4.25438_DBL_R*ten**(-6)   ! SI units
! Bohr magneton, 5.7883818012d-05 eV/Tesla  (927.4009994d-26 Joule/Tesla)
      real(DBL_R), parameter :: amuB = sqrt(two)                   ! in a.u.
! permeability of vacuum;  1/(mu_0*epsilon_0) = light_speed^2
      real(DBL_R), parameter :: mu0si = four*pi*ten**(-7)  ! SI units (approx.)
      real(DBL_R), parameter :: mu0 = pi/inv_fine_struct_const**2 ! atomic Ry units
                                                                  ! 0.000167294
! vacuum permittivity
      real(DBL_R), parameter :: eps0 = one/(four*pi)              ! 0.079577472

!
!EOC
      end module universal_const
