!c     ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
!BOP
!!ROUTINE: rooeml
!!INTERFACE:
      subroutine rooeml(pdu,powe,lmax,iprint)
!!DESCRIPTION:
! calculates array {\tt powe}(lm) = (1/pdu)**l, l=0:2*lmax
! (fo Ewald summation)

!!DO_NOT_PRINT
      implicit none
!!DO_NOT_PRINT

!!ARGUMENTS:
      complex*16  pdu
      complex*16  powe(*)
      integer   lmax,iprint
!EOP
!
!BOC
      complex*16  rootel(0:2*lmax)
      complex*16  re
!c
      integer lmaxt2,i,j,k,m,l
      complex*16 zone
      parameter (zone=(1.d0,0.d0))
!c +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!      character sname*10
!      parameter   (sname='rooeml')
!c +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!c
      lmaxt2=2*lmax
      re=zone/pdu
!c     rootel(0)=4*pi
      rootel(0)=zone
      do i=1,lmaxt2
         rootel(i)=rootel(i-1)*re
      enddo
      k=0
      do l=0,lmaxt2
         j=(2*l+1)
         do m=1,j
            k=k+1
            powe(k)=rootel(l)
         enddo
      enddo
!c
      if(iprint.ge.3) then
         write(6,'(/'' rooeml::   pdu  ='',2d14.6)') pdu
         write(6,'(/'' rooeml::   lmax ='',i5)') lmax
         write(6,'(/'' rooeml:: powe'')')
         write(6,'(2x,2(i5,2d14.6))') (k,powe(k),k=1,(lmaxt2+1)**2)
      endif
!c
      return
!EOC
      end subroutine rooeml
