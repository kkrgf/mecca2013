#!/bin/csh -f

set XCINFO="xc-info"
if ( $?LIBXC ) then
 echo LIBXC=${LIBXC}
 set XCINFO=${LIBXC}/bin/xc-info
endif
echo XCINFO=`which ${XCINFO}`

echo '##   List of all LibXc functionals '
echo '##    Mecca supports functionals only from LDA and GGA/HYB-GGA family '
echo '##'
foreach task ( `perl -e 'for(1..600){print "$_ "}{print "\n"}'` )
 ${XCINFO} $task | grep -v "not found" | awk 'BEGIN{l=0; spt="MECCAL NO"}{l=l+1; print; \
 if ( $1=="family:" ) spt="MECCA: NO" ; \
 if ( $1=="family:" && $2=="XC_FAMILY_LDA") spt="MECCA: YES" ; \
 if ( $1=="family:" && $2=="XC_FAMILY_GGA") spt="MECCA: YES" ; \
 if ( $1=="family:" && $2=="XC_FAMILY_HYB_GGA") spt="MECCA: YES" ; }END{ if ( l>1 ) print spt "\n" "##\n" }'
end
