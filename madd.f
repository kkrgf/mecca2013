!BOP
!!ROUTINE: madd
!!INTERFACE:
      subroutine madd(amt,const,bmt,cmt,kkrsz,iprint)
!!DESCRIPTION:
! calculates {\tt cmt}={\tt amt}+{\tt const}*{\tt bmt},
! {\tt amt}, {\tt bmt}, {\tt cmt} are complex square matrices 
! and {\tt const} is a real constant
!

!!DO_NOT_PRINT
      implicit none
!!DO_NOT_PRINT

!!ARGUMENTS:
      complex(8),  intent(in) :: amt(:,:)
      real(8),     intent(in) :: const
      complex(8),  intent(in) :: bmt(:,:)
      complex(8), intent(out) :: cmt(:,:)
      integer, intent(in) :: kkrsz
      integer, intent(in) :: iprint
!!REVISION HISTORY:
! Initial version - bg & gms - Nov 1991
! Adapted - A.S. - 2013
!EOP
!
!BOC
!c
!c     used in iteration of CPA equation...........
!c
      cmt(1:kkrsz,1:kkrsz) = amt(1:kkrsz,1:kkrsz) +                     &
     &                         const*bmt(1:kkrsz,1:kkrsz)
!
!      if (iprint.ge.2) then
!         write(6,'('' madd:: kkrsz,const :'',i5,d12.4)') kkrsz,const
!         call wrtmtx(cmt,kkrsz,istop)
!      endif
!
      return
!EOC
      end subroutine madd
