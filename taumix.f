!BOP
!!ROUTINE: taumix
!!INTERFACE:
      subroutine taumix(lmax,natom,                                     &
     &                 aij,itype,itype1,                                &
     &                 mapstr,mappnt,ndimbas,mapij,                     &
     &                 powe,                                            &
     &                 kx,ky,kz,                                        &
     &                 edu,pdu,                                         &
     &                 irecdlm,                                         &
     &                 rsnij,ndimrij,                                   &
     &                 np2r,ndimnp,numbrs,naij,                         &
     &                 dqint,ndimdqr,                                   &
     &                 hplnm,ndimrhp,ndimlhp,                           &
     &                 tcpa,                                            &
     &                 d00,eoeta,                                       &
     &                 eta,xknlat,ndimks,                               &
     &                 conr,nkns,                                       &
     &                 iscreen,                                         &
     &                 tref,trefinv,ndkkr,deltat,deltinv,               &
     &                 numnbi,jclust,                                   &
     &                 mapsnni,ndimnn,                                  &
     &                 nnptr,maxclstr,nnclmn,                           &
     &                 rsij,                                            &
     &                 invswitch,invparam,isprs,                        &
     &                 greenrs,ndgreen,greenks,tau00,                   &
     &                 iprint,istop)
!!DESCRIPTION:
!  computes block-diagonal part of Tau (tau00) in {\bf k}-space
!

!!USES:
      use mtrx_interface, only : wrtdia2
      use freespaceG,  only : gf0_ks
!
!!REVISION HISTORY:
! Adapted - A.S. - 2013
!!REMARKS:
! Sparse and screen matrix branches have been disabled
!EOP
!
!BOC
      implicit none
!c
      integer lmax,natom,ndimbas

      integer irecdlm,ndimrij
      integer ndimnp,ndimks,ndimdqr
      integer ndimrhp,ndimlhp

      integer     nkns
!c
      real*8      aij(3,*)
      real*8      kx
      real*8      ky
      real*8      kz
      real*8      rsnij(ndimrij,4)
      real*8      xknlat(ndimks,3)
!c      real*8      dot
      real*8      eta
!c
      complex*16   conr(*)
      complex*16   powe(*)
      complex*16   dqint(ndimdqr,*)

      integer      ndgreen
      complex*16   greenks(*)
      complex*16   greenrs(*) !redundant

      integer      ndkkr
      complex*16   tcpa(ndkkr,ndkkr,*)
      complex*16   tref(ndkkr,*)
      complex*16   trefinv(ndkkr,*)
      complex*16   deltat(ndkkr,ndkkr,*)
      complex*16   deltinv(ndkkr,ndkkr,*)

      complex*16   tau00(ndkkr,ndkkr,natom)
      complex*16   edu
      complex*16   pdu
      complex*16   d00
      complex*16   eoeta

      complex*16   hplnm(ndimrhp,ndimlhp)

      integer      itype(natom),itype1(natom)
      integer      mapstr(ndimbas,natom),mappnt(ndimbas,natom)

      integer mapij(2,*)
      integer np2r(ndimnp,*),numbrs(*),naij

      integer iscreen

      integer numnbi(*),ndimnn,jclust(ndimnn,*)
      integer mapsnni(ndimnn*ndimnn,*)
      real*8  rsij(ndimnn*ndimnn*3,*)

      integer invswitch,invparam,iprint
      character istop*10
!c     ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
      character    sname*10
      parameter    (sname='taumix')
!c     ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
      integer kkrsz
!c      integer iatom,jatom,jatom0,isub,jsub,nsub,ia0,i,m,l
      integer iatom,jatom,jatom0,isub,ia0

      integer isparse,isprs(3)

!c      real*8 time1,time2

!c      integer indclstr,isave/0/
      integer indclstr
      external indclstr
!c      save isave

!c      integer erralloc,ndbmt
!c      complex*16, allocatable :: bmt(:,:)

      integer inn,inn0,nnmax,l,m
      integer maxclstr
      integer nnptr(0:1,*),nnclmn(2,maxclstr,*)
!DISABLED      integer nnptr(0:1,natom),nnclmn(2,maxclstr,natom)

      kkrsz = (lmax+1)*(lmax+1)

!DISABLED      isparse = isprs(1)                ! standard method (if isparse.EQ.0)
!DISABLED!c      iswitch = isprs(2)                ! to choose sparse method techniques
!DISABLED!c                                           SuperLU - 0, TFQMR - 1 , MAR !-2
!DISABLED
!DISABLED      if(iscreen.gt.0) then
!DISABLED!c
!DISABLED!c  Fourier transformation of real-space screened Greens function:
!DISABLED!c    only for (iatom,jatom) belonging to the same original cell,
!DISABLED!c
!DISABLED        call zerooutC(greenks,(kkrsz*natom)**2)
!DISABLED
!DISABLED        do iatom = 1,natom
!DISABLED          isub = itype(iatom)
!DISABLED!c         iatom0 = nnptr(1,iatom)
!DISABLED          nnmax = nnptr(0,iatom)
!DISABLED          do inn=1,nnmax
!DISABLED            inn0 = nnclmn(2,inn,iatom)
!DISABLED            jatom = nnclmn(1,inn,iatom)
!DISABLED            jatom0 = jclust(inn0,isub)    ! origin of "jatom"
!DISABLED            ia0 = mapstr(iatom,jatom)
!DISABLED            call ksmatr(iatom,jatom,jatom0,                             &
!DISABLED     &                 kx,ky,kz,aij(1,ia0),greenrs(1,isub),             &
!DISABLED     &                 numnbi(isub),kkrsz,rsij(1,isub),                 &
!DISABLED     &                 mapsnni(1,isub),ndimnn,nnmax,                    &
!DISABLED     &                 jclust(1,isub),                                  &
!DISABLED     &                 greenks,kkrsz*natom,                             &
!DISABLED     &                 invswitch,                                       &
!DISABLED     &                 istop)
!DISABLED
!DISABLED          end do
!DISABLED        end do
!DISABLED
!DISABLED      else

!c
!c  K-space calculation of G0 (full matrix only),
!c                             or
!c                       Gscreen (iscreen<0)
!c
!c     if no screening (iscreen=0) you have to be sure that
!c               deltat = tcpa, deltinv = tcpa^(-1)
!c        to obtain proper Greens function from G0
!c
       call gf0_ks(                                                     &
     &              lmax,natom,                                         &
     &              aij,                                                &
     &              mapstr,mappnt,ndimbas,mapij,                        &
     &              powe,                                               &
     &              kx,ky,kz,                                           &
     &              edu,pdu,                                            &
     &              irecdlm,                                            &
     &              rsnij,ndimrij,                                      &
     &              np2r,ndimnp,numbrs,naij,                            &
     &              dqint,ndimdqr,                                      &
     &              hplnm,ndimrhp,ndimlhp,                              &
     &              d00,eoeta,                                          &
     &              eta,xknlat,ndimks,                                  &
     &              conr,nkns,                                          &
     &              greenks,                                            &
     &              iprint,istop)

!c         write(6,*) 'g0 at eoeta =', eoeta
!c         write(6,*) 'kx,ky,kz=',kx,ky,kz
!c         do ii = 1,kkrsz*kkrsz
!c           write(6,*) greenks(ii)
!c         end do

!DISABLED!c   tref,trefinv -- matrix, [NDKKR,*]
!DISABLED
!DISABLED       if(iscreen.lt.0) then
!DISABLED
!DISABLED        call calgreen(                                                  &
!DISABLED     &              greenks,                                            &
!DISABLED     &              kkrsz,natom,tref,trefinv,ndkkr,1,itype,             &
!DISABLED     &              invswitch,invparam,                                 &
!DISABLED     &              iprint,istop)
!DISABLED
!DISABLED       end if
!DISABLED
!DISABLED      end if
!DISABLED
!DISABLED
!DISABLED!c   deltat,deltinv -- matrix [NDKKRxNDKKR,*]
!DISABLED!c          deltinv == deltat^(-1)

!DISABLED      if(isparse.eq.0) then

!DEBUGPRINT        if ( iprint >= 4 ) then 
!         write(6,*) sname//' before KStau00: deltat(tcpa)'
!         call wrtdia2(deltat(1:kkrsz,1:kkrsz,1:natom),kkrsz,natom,istop)
!DEBUGPRINT        end if

        call KStau00(                                                   &
     &              greenks,                                            &
     &              kkrsz,natom,deltat,tau00,ndkkr,itype1,              &
     &              invswitch,invparam,                                 &
     &              iprint,istop)

!DISABLED      else
!DISABLED
!DISABLED!c  if(iscreen.eq.0) deltat == tcpa  <---   !!!
!DISABLED
!DISABLED!CDEBUG Be careful for iscreen.eq.0:
!DISABLED!CDEBUG  check if NUMNBI,JCLUST,NDIMNN were defined
!DISABLED
!DISABLED       call fstop(sname//                                               &
!DISABLED     &  ': it is not expected that this sparse matrix branch is used')
!DISABLED
!DISABLED      end if

!c     ==================================================================

      if (istop.eq.sname) call fstop(sname)
!c

      return
      end subroutine taumix
