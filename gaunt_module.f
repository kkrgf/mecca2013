!BOP
!!MODULE: gaunt
!!INTERFACE:
      module gaunt
!!DESCRIPTION:
! provides Gaunt coefficients (gauntd,gauntf) and "Gaunt convolution" for free-space
! Green's function (gauntdlm; Ewald's method)
!

!!DO_NOT_PRINT
      private
!!DO_NOT_PRINT

!!PUBLIC MEMBER FUNCTIONS:
      public gauntdlm, gauntd, gauntf, gntdealloc
!!PRIVATE MEMBER FUNCTIONS:
! subroutine gauntdlm
! subroutine gauntd      
! subroutine gaunt2
! subroutine itollp
!!REVISION HISTORY:
! Initial version - A.S. - Oct 2014
! Revised - A.S. - May 2019
!EOP
!
!BOC
      complex(8), pointer :: cgaunt(:,:) => null()
      integer, pointer ::    ij3(:,:) => null()
      integer, pointer ::    nj3(:) => null()
      integer :: lmax_=-1
      real(8), pointer    :: ylmr(:,:) => null()
      real(8), pointer    :: w_ylmr(:) => null()
      real(8), parameter  :: eps = 1.d-13
!EOC
      contains

!BOP
!!IROUTINE: gauntdlm
!!INTERFACE:
      subroutine gauntdlm(lmax,dlm,cz,gout)  
!!DESCRIPTION:
! multiplies complex vector {\tt dlm}, complex constant {\tt cz} 
! and gaunt coefficients for given {\tt lmax}
!

!!DO_NOT_PRINT
      implicit none
!!DO_NOT_PRINT

!!ARGUMENTS:
      integer, intent(in) :: lmax
      complex(8), intent(in) :: dlm(:)
      complex(8), intent(in) :: cz
      complex(8), intent(out) :: gout(:)
!!REVISION HISTORY:
! Initial Version - A.S. - Oct 2014
!EOP
!
!BOC
      real(8),    allocatable :: gaunt(:,:)
      integer :: i,kkrsz,kkrsz2l
      kkrsz = (lmax+1)**2
      if ( lmax .ne. lmax_ ) then
       if ( associated(cgaunt) ) deallocate(cgaunt)
       if ( associated(ij3) ) deallocate(ij3)
       if ( associated(nj3) ) deallocate(nj3)
       if ( allocated(gaunt) ) deallocate(gaunt)
       kkrsz2l = (2*lmax+1)**2
       allocate(cgaunt(kkrsz*kkrsz,kkrsz2l))
       allocate(ij3(kkrsz*kkrsz,kkrsz))
       allocate(nj3(kkrsz*kkrsz))
       allocate(gaunt(kkrsz*kkrsz,kkrsz2l))
       call gauntd(gaunt,nj3,ij3,lmax,kkrsz)
       deallocate(gaunt)
       lmax_ = lmax
      end if

      do i=1,kkrsz*kkrsz
       gout(i) = cz*sum(cgaunt(i,1:nj3(i))*dlm(ij3(i,1:nj3(i))))
      end do
      return
!EOC
      end subroutine gauntdlm
!
      subroutine gauntc( gnt,kkrsz, zgaunt )
      implicit none
      real(8), intent(in) :: gnt(:,:)
      integer, intent(in) :: kkrsz
      complex(8), intent(out) :: zgaunt(:,:)
      integer :: i
      integer :: NxN1,NxN2
      complex(8) :: cfac(kkrsz*kkrsz)
!
      NxN1 = kkrsz*kkrsz
      NxN2 = size(gnt,2)
      call itollp(cfac,kkrsz)
      do i=1,NxN1
        zgaunt(i,1:NxN2) = cfac(i)*gnt(i,1:NxN2)
      end do
      return
      end subroutine gauntc
!
      subroutine gntdealloc(iflag)
       integer, intent(in) :: iflag
       select case (iflag)
        case(1)
         if ( associated(cgaunt) ) deallocate(cgaunt)
         if ( associated(ij3) ) deallocate(ij3)
         if ( associated(nj3) ) deallocate(nj3)
         cgaunt => null()
         ij3 => null()
         nj3 => null()
         lmax_ = -1
        case(2)
         if ( associated(ylmr) ) deallocate(ylmr)
         if ( associated(w_ylmr) ) deallocate(w_ylmr)
         ylmr => null()
         w_ylmr => null()
        case default
         if ( associated(cgaunt) ) deallocate(cgaunt)
         if ( associated(ij3) ) deallocate(ij3)
         if ( associated(nj3) ) deallocate(nj3)
         if ( associated(ylmr) ) deallocate(ylmr)
         if ( associated(w_ylmr) ) deallocate(w_ylmr)
         lmax_ = -1
         cgaunt => null()
         ij3 => null()
         nj3 => null()
         ylmr => null()
         w_ylmr => null()
       end select
      return
      end subroutine gntdealloc
!
!BOP
!!IROUTINE: gauntd
!!INTERFACE:
      subroutine gauntd( gnt, nj3, ij3, lmax, kkrsz, iconjg )
!!DESCRIPTION:
! computes the integral of $ Y_{lp,mp} Y_{l,m} Y_{ls,ms} $
! for $ l,ls <= lmax $ or, if $iconjg=1$ (optional argument),
! integral of $ conjg(Y_{lp,mp}) Y_{l,m} Y_{ls,ms} $;
! in addition, by default (i.e. if iopt=0) complex gaunt coefficients
! are stored in cgaunt, which is indexed by ij3 and nj3 arrays.

!!DO_NOT_PRINT
      implicit none
!!DO_NOT_PRINT

!!ARGUMENTS:
      integer, intent(in) :: lmax
      integer, intent(in) :: kkrsz
      real(8), intent(out) :: gnt(kkrsz*kkrsz,*)
      integer, intent(out) :: nj3(:)
      integer, intent(out) :: ij3(:,:)
      integer, intent(in), optional  :: iconjg

!!REVISION HISTORY:
! Revised by W.A. Shelton, Jr. March 1, 1993
! Adapted - A.S. - 2013
!EOP
!
!BOC
      integer iopt
      integer ndlj
      real(8) :: yr(1:(2*lmax+1)**2+1,1:(2*lmax+1)**2)
      real(8) :: w(1:(2*lmax+1)**2+1)
      integer    lmaxp1
      integer    n
      integer    l
      integer    ls
      integer    lp
      integer    m
      integer    ms
      integer    m_ms,mp
      integer    jlow
      integer    jhigh
      integer    il
      integer    ils
      integer    ilp
      integer    istore
!      integer    icount

      real(8) :: gn
!
!c     ================================================================
!
                                 ! if iopt=1, conjg(Y_{lp,mp}) Y_{l,m} Y_{ls,ms}
      if ( present(iconjg) ) then
       iopt = iconjg
      else
       iopt = 0                  ! Y_{lp,mp} Y_{l,m} Y_{ls,ms}
      end if
!      ================================================================

      ndlj=(2*lmax+1)**2
      lmaxp1 = lmax + 1
      n = ndlj + 1
      call gaunt2( n, lmaxp1, ndlj, w, yr )
!      icount = 0
!c     ================================================================
      ij3 = 0 ; nj3 = 0
      gnt(:,1:ndlj) = 0
      do l = 0,lmax
        do m = -l,l
          il = l*( l + 1 ) + m + 1
          do ls = 0,lmax
            do ms = -ls,ls
!!              icount = icount + 1
              ils = ls*( ls + 1 ) + ms + 1
              if ( iopt == 1 ) then
               m_ms = ms + m
              else
               m_ms = ms - m
              end if
              jlow = abs( l - ls )
              jhigh = l + ls
!c     ================================================================
              do lp = jlow,jhigh,2
                if( mod( (l + ls) + lp, 2 ) .ne. 0 ) cycle
                do mp = -lp,lp
                  if( m_ms .ne. mp ) cycle
!                  icount = icount + 1
                  ilp = lp*( lp + 1 ) + mp + 1
                  gn = sum(w(1:n)*yr(1:n,ilp)*yr(1:n,il)*yr(1:n,ils))
                  if ( abs(gn) > eps ) then
                   istore = ( ils - 1 )*kkrsz + il
                   nj3(istore) = nj3(istore) + 1
                   ij3(istore,nj3(istore)) = ilp
                   gnt(istore,nj3(istore)) = gn
                  end if
                enddo            ! end do loop over mp
              enddo              ! end do loop over lp
!c     ================================================================
            enddo                ! end do loop over ms
          enddo                  ! end do loop over ls
        enddo                    ! end do loop over m
      enddo                      ! end do loop over l
!c     ================================================================
      if ( iopt==0 ) then
       call gauntc(gnt(:,1:ndlj),kkrsz,cgaunt)
      end if
      return

!EOC
      end subroutine gauntd
!
!BOP
!!IROUTINE: gaunt2
!!INTERFACE:
      subroutine gaunt2( n, lmaxp1, ndlj, w, yr )
!!DESCRIPTION:
! computes normalized associated legendre functions at gauss-legendre 
! quadrature points, also generates their weights;
! gaussian quadrature is used as given by
! M. Abramowitz and I.A. Stegun, handbook of mathematical functions,
! nbs applied mathematics series 55 (1968), pages 887 and 916.
! 
!!REMARKS:
! private procedure of subroutine gauntd
!EOP
!
!BOC
      implicit none
      integer, intent(in) :: lmaxp1
      integer, intent(in) :: n
      integer, intent(in) :: ndlj
      real*8, intent(out) :: w(1:n)
      real*8, intent(out) :: yr(ndlj+1,ndlj)
!c     ================================================================
      integer    lomax
      integer    nn
      integer    m
      integer    l
      integer    lm
      integer    k
      real*8, allocatable :: p(:,:)
      real*8 :: x(1:n)
      real*8 :: b(1:n)
      real*8     endpts(2)
      real*8     fpi
      real*8     rf
      real*8     cth
      real*8     sth
      real*8     fac
      real*8     a
      real*8     t
      real*8     cd
      real*8     sgm
      real*8     half,one,three
      parameter (half=0.50d0,one=1.d0,three=3.d0)
!c     ================================================================
!c      fpi = 4*pi
      fpi = 16.d0*datan(one)
      rf = fpi**( one/three )
      lomax = 2*( lmaxp1 - 1 )
      allocate( p(0:lomax+1,0:lomax) )
!c     ================================================================
!c     Obtain gauss-legendre points and weights
!c     ================================================================
      nn = n
!c     ================================================================
!c     call gaussw( nn, x, w )
!c     ================================================================
      endpts(1) = -one
      endpts(2) =  one
!c     ================================================================
      call gaussq( 1, nn, 0, endpts, b, x, w )
!c     ================================================================
      do m = 1,n
        w(m) = w(m)*half
      enddo
!c     ================================================================
!c     Generate associated legendre functions for m > 0
!c     ================================================================
      do k = 1,n
        cth = x(k)
        sth = sqrt( one - cth*cth )
        fac = one
!c     ================================================================
!c     Loop over m values
!c     ================================================================
        do m = 0,lomax
!c     ================================================================
          fac = -( 2*m - 1 )*fac
!c     ================================================================
          p(m,m) = fac
          p(m+1,m) = ( 2*m + 1 )*cth*fac
!c     ================================================================
!c     Recurse upward in l
!c     ================================================================
          do l = m+2,lomax
!c     ================================================================
            p(l,m) = ( ( 2*l - 1 )*cth*p(l-1,m) - ( l + m - 1 )         &
     &             *p(l-2,m) )/( l - m )
!c     ================================================================
          enddo                                   ! end do loop over l
!c     ================================================================
          fac = fac*sth
!c     ================================================================
        enddo                                     ! end do loop over m
!c     ================================================================
!c     Multiply in the normalization factors
!c     ================================================================
        do l = 0,lomax
!c     ================================================================
          a = rf*sqrt( ( 2*l + 1 )/fpi )
          cd = one
          lm =l*( l + 1 ) + 1
          yr(k,lm) = a*p(l,0)
          sgm = -one
!c     ================================================================
          do m = 1,l
            t = ( l + 1 - m )*( l + m )
            cd = cd/t
            yr(k,lm+m) = a*sqrt( cd )*p(l,m)
!CAB            yr(k,lm-m) = sgm*a*sqrt( cd )*p(l,m)
            yr(k,lm-m) = sgm*yr(k,lm+m)
            sgm = -sgm
!c     ================================================================
          enddo                                   ! end do loop over m
!c     ================================================================
        enddo                                     ! end do loop over l
!c     ================================================================
      enddo                                       ! end do loop over k
!c     ================================================================
      deallocate( p )
      return
!EOC
      end subroutine gaunt2
!
!BOP
!!IROUTINE: itollp
!!INTERFACE:
      subroutine itollp(cfac,kkrsz)
!!REVISION HISTORY:
! Initial Version - A.S. - 2013
!EOP
!
!BOC
      implicit none
!c
      complex(8), intent(out) ::  cfac(kkrsz*kkrsz)
      integer, intent(in)     ::  kkrsz
      real(8), parameter :: zero=0, one=1
!      powi ( k ) = (0,1)**k
!
      complex(8), parameter ::  powi(-3:3) =                            &
     &     [ dcmplx(zero,one),dcmplx(-one,zero),dcmplx(zero,-one),      &
     &                   dcmplx(one,zero),                              &
     &       dcmplx(zero,one),dcmplx(-one,zero),dcmplx(zero,-one) ]

      integer ij,lmax,li,mi,lj,mj,lmlp
      complex(8) :: cij
!c
!c      integer     l3d(49)
!c      data l3d /  0, 3*1, 5*2, 7*3, 9*4, 11*5, 13*6 /
!c
!c     ================================================================
      lmax = nint(sqrt(float(kkrsz))) - 1
      ij = 0
      do li=0,lmax
       do mi=-li,li
        do lj=0,lmax
         lmlp= mod(lj - li,4)
!c         lmlp= mod ( l3d(j)-l3d(i)+16 , 4)
         cij = powi(lmlp)
         do mj=-lj,lj
          ij = ij+1
          cfac(ij)=cij            ! (0.,1.)**(lj-li)
         enddo
        enddo
       enddo
      enddo
!c
!c     ================================================================
      return
!EOC
      end subroutine itollp

!BOP
!!IROUTINE: gauntf
!!INTERFACE:
      real(8) function gauntf( l1,l2,l3,m1,m2,m3, lmax, iconjg )
!!DESCRIPTION:
! function computes the integral of $ Y_{l1,m1} Y_{l2,m2} Y_{l3,m3} $
! for $ l2,l3 <= lmax $ or, if $iconjg=1$,
! integral of $ conjg(Y_{l1,m1}) Y_{l2,m2} Y_{l3,m3} $

!!DO_NOT_PRINT
      implicit none
!!DO_NOT_PRINT

!!ARGUMENTS:
      integer, intent(in) :: l1,l2,l3,m1,m2,m3
      integer, intent(in) :: lmax
      integer, intent(in) :: iconjg


!!REVISION HISTORY:
! Initial version - A.S. - 2019
!EOP
!
!BOC
      integer :: ndlj,lmaxp1
      integer :: ilp,il,ils,  n

      logical :: do_ylmr
      integer :: m2m3
!
      if ( l2>lmax .or. l3>lmax .or. l1>2*lmax .or. l1<0 .or. l2<0 .or. &
     &     l3<0 .or. l1<abs(m1) .or. l2<abs(m2) .or. l3<abs(m3) ) then 
        write(*,'('' bad input:'',6i4)') l1,l2,l3,m1,m2,m3
       stop 'inconsistent input l1,l2,l3,m1,m2,m3'
      end if

      gauntf = 0.d0

      if( mod( (l2 + l3) + l1, 2 ) .ne. 0 ) then
       return
      end if

      if ( l1 < abs(l2-l3) .or. l1>(l2+l3) ) then
       return
      end if

      if ( iconjg == 1 ) then  ! conjg(Y_{lp,mp}) Y_{l,m} Y_{ls,ms}
        m2m3 = m3 + m2       
      else
        m2m3 = m3 - m2         ! Y_{l1,m1} Y_{l2,m2} Y_{l3,m3}
      end if
      if ( m1 .ne. m2m3 ) then
       return
      end if

      ndlj=(2*lmax+1)**2
      n = ndlj + 1
      if ( .not.associated(ylmr) ) then
       do_ylmr = .true.
      else
        if ( ndlj.ne.size(ylmr,2) ) then
         do_ylmr = .true.
        else
         do_ylmr = .false.
        end if
      end if
      if ( do_ylmr ) then
       lmaxp1 = lmax + 1
       allocate(ylmr(1:n,1:ndlj))
       allocate(w_ylmr(1:n))
       call gaunt2( n, lmaxp1, ndlj, w_ylmr, ylmr )
      end if

      ilp = l1*( l1 + 1 ) + m1 + 1 
      il  = l2*( l2 + 1 ) + m2 + 1 
      ils = l3*( l3 + 1 ) + m3 + 1

      gauntf = sum(w_ylmr(1:n)*ylmr(1:n,ilp)*ylmr(1:n,il)*ylmr(1:n,ils))
      if ( gauntf<eps .and. gauntf>-eps ) then
       return
      end if

      return
!EOC
      end function gauntf
!
      end module gaunt
