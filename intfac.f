!BOP
!!ROUTINE: intfac
!!INTERFACE:
      subroutine intfac(rssq,e,eta,lcut2,dqint)
!!DESCRIPTION:
! calculates the part of ewald integral (more details are in the code)
!
!!USES:
      use mecca_constants

!!DO_NOT_PRINT
      implicit   real*8 (a-h,o-z)
!!DO_NOT_PRINT

!!ARGUMENTS:
      real(8), intent(in) :: rssq
      complex(8), intent(in) :: e
      real(8), intent(in) :: eta
      integer, intent(in) :: lcut2
      complex(8), intent(out) :: dqint(0:lcut2)
!!REVISION HISTORY:
! rewritten for speed and corrected by DDJ and WAS - 24 April 1994
! Adapted - A.S. - 2013
!!AUTHOR: 
! Yang Wang, April, 1992 
!EOP
!
!BOC
!c
!c +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!c +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!c
      character   sname*10
!c parameter
!c
!c +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
      parameter (sname='intfac')
      complex(8), parameter :: ci=(0.d0,1.d0)
      real(8), parameter :: half=one/two,quart=one/four,tpi=two*pi
!c +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!c
      complex*16 dqm1,texp2,erfrem1,erfrem2
      complex*16 arg,b,c,t1,t2,remain1,remain2
      real(8) :: erfc
      external erfc
!c
!c     ****************************************************************
!c     call intfac to calculate the part of ewald integral:
!c
!c            inf       2*l
!c     I(l) = int dx * x   * exp(-x**2*r**2-b**2/x**2) = dqint(l),
!c             a
!c                          _  _
!c     a=0.5*sqrt(eta), r=|(Rs+aij)|, b=i*sqrt(e)/2.
!c
!c     using: (2*l+1)*I(l) = 2*r*r*I(l+1) - 2*b*b*I(l-1) - c(l) ,
!c
!c            c(l) = a**(2*l+1) * exp(-a**2*r**2-b**2/a**2) ,
!c
!c            I(0) = sqrt(pi)/4/r * [+exp(+2*r*b)*erfc(r*a+b/a))
!c                                   +exp(-2*r*b)*erfc(r*a-b/a)) ]
!c
!c            I(-1)= sqrt(pi)/4/b * [-exp(+2*r*b)*erfc(r*a+b/a)
!c                                   +exp(-2*r*b)*erfc(r*a-b/a)]
!c
!c     erfc(z) = 1 - erf(z)
!c                                 2
!c     erf(x+i*y) = erf(x) + exp(-x )/(2pi*x)*[(1-cos(2xy))+i*sin(2xy)]
!c
!c                                      2  inf       2      2   2
!c                         + 2/pi*exp(-x ) sum exp(-n /4)/(n +4x )
!c                                         n=1
!c
!c                         * [fn(x,y)+i*gn(x,y)] + epsi(x,y)
!c
!c     where, fn(x,y) = 2x-2x*cosh(n*y)*cos(2xy)+n*sinh(ny)*sin(2xy)
!c
!c            gn(x,y) = 2x*cosh(n*y)*sin(2xy)+n*sinh(ny)*cos(2xy)
!c                      .
!c            epsi(x,y) = 1.0d-16 * |erf(x+i*y)|
!c
!c     assuming r and b <> 0.0d0.
!c
!c     If b=0.0d0, using:
!c
!c            (2*l+1)*I(l) = r*r*I(l+1) - c(l) ,
!c
!c            c(l) = a**(2*l+1) * exp(-a**2*r**2) ,
!c
!c            I(0) = sqrt(pi)/2/r * erfc(r*a)
!c
!c     if r=0.0d0, return with dqint=0.0d0.
!c
!c     Ref: Chapter 7, "Handbook of Mathematical Functions"
!c          Edited by Milton Abramowitz and Irene A. Stegun
!c
!c        ( dqint required for EWALD summation, see GETTAU)
!c     ****************************************************************
!c     ................................................................
!c     a factor 2pi needs to be included for rsij.
!c     ................................................................
!c
!c     rssq=tpi**2*(rsij(1)**2+rsij(2)**2+rsij(3)**2)
      dqint = czero
      if (rssq.le.1.0d-5) then
         return
      end if
!c
      a   = sqrt(eta)*half
      b   = ci*sqrt(e)*half
      r   = sqrt(rssq)
      arg =-rssq*a*a + quart*e/a**2
      x1  = r*a+dreal(b)/a
      y1  = dreal(sqrt(e))*half/a
      x2  = r*a-dreal(b)/a
!      y2  =-dreal(sqrt(e))*half/a
      y2  =-y1
!c
      if (abs(b) .lt. 1.0d-6) then
         dqint(0) = erfc(x1)*sqrt(pi)*half/r
         dqm1     = czero
      else
         t1       = czero
         t2       = czero
!c
         x1y1 = two*x1*y1
         x2y2 = two*x2*y2
         tx1= two*x1
         tx2= two*x2
         t4x1x1= four*x1*x1
         t4x2x2= four*x2*x2
         c2x1y1= cos(x1y1)
         s2x1y1= sin(x1y1)
         c2x2y2= cos(x2y2)
         s2x2y2= sin(x2y2)
!c
         do n=1,10
            n2    = n*n
            y1n   = n*y1
            y2n   = n*y2
            sny1  = n*sinh(y1n)
            cny1  = cosh(y1n)
            sny2  = n*sinh(y2n)
            cny2  = cosh(y2n)
            texp  = exp(-n2*quart)
            den1  = (n2 + t4x1x1)
            den2  = (n2 + t4x2x2)
!c
            fn1   = tx1*( one - cny1*c2x1y1 ) + sny1*s2x1y1
!c
            gn1   = tx1*cny1*s2x1y1 + sny1*c2x1y1
!c
            t1    = t1 + texp*(fn1 + ci*gn1)/den1
!c
            fn2   = tx2*( one - cny2*c2x2y2 ) + sny2*s2x2y2
!c
            gn2   = tx2*cny2*s2x2y2 + sny2*c2x2y2
!c
            t2    = t2 + texp*(fn2 + ci*gn2)/den2
!c
         end do
!c
       remain1=exp(-x1*x1)*( (one-c2x1y1+ ci*s2x1y1)/x1 + four*t1)/tpi
       remain2=exp(-x2*x2)*( (one-c2x2y2+ ci*s2x2y2)/x2 + four*t2)/tpi
!c
       texp2   = exp( two*r*b)
       erfrem1 = (erfc(x1) - remain1)*texp2
       erfrem2 = (erfc(x2) - remain2)/texp2
!c
       dqint(0)= sqrt(pi)*quart*( erfrem1 + erfrem2 )/r
       dqm1    = sqrt(pi)*quart*(-erfrem1 + erfrem2 )/b
!c
      end if
!c
      c = a * exp(arg)
      if (lcut2<1) return
      dqint(1)= (dqint(0)-e*half*dqm1 + c)*half/rssq
      do l=1,lcut2-1
         c=c*a*a
         dqint(l+1)= ( (2*l+1)*dqint(l)                                 &
     &                          -e*half*dqint(l-1) + c)*half/rssq
      end do
!c
      return
!EOC
      end subroutine intfac
