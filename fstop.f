!BOP
!!ROUTINE: fstop
!!INTERFACE:
      subroutine fstop(name)
!!DESCRIPTION:
! if {\tt name}=="CPU\_START" then current time and date are printed,
! internal time is nullified (no stop); \\ \\
! otherwise \\ cpu time as well as current time and date are printed and
! run stops with printing {\tt name} (excluding {\tt name}=="return", in
! such a case no stop happens and internal time is nullified)  
!
!!USES:
      use mecca_constants, only : setDate
      use mecca_interface
      use mpi
!!ARGUMENTS:
       character*(*) name
!!REVISION HISTORY:
! Adapted - A.S. - 2013
!EOP
!
!BOC
      character*6 crtrn/'return'/
      integer      datval(24)
!      integer system,i
      real*8 time0/0.d0/,time1
      save time0
      real dt,minute,hour,day,sec
      parameter (minute=60.,hour=60.*minute,day=24.*hour)
      integer iday,ihour,imin
      integer, parameter :: nch=11

      integer :: myrank,ierrcomm,ierr ! MPI related variables
      logical yes_mpi

!      external ctime_fort

!c
      if(name.eq.'CPU_START') then
       call setDate(buildDate())
       datval(1:24) = 0
!       call ctime_fort(datval)
!       write(6,*)
!       write(6,*) ' STARTED ON:   ',char(datval(1:24))
       call date_and_time(VALUES=datval)
       if ( maxval(datval).gt.0) then
        write(6,*)
        write(6,*) 'STARTED ON:'
        write(6,'('' DATE              '',i2.2,''/'',i2.2,''/'',i4)')   &
     &                              datval(3),datval(2),datval(1)
        write(6,'('' TIME              '',i2.2,'':'',i2.2,'':'',        &
     &            i2.2/)')          datval(5),datval(6),datval(7)
       end if
       call timel(time0)
       return
      else

       write(6,'(/)')
       write(6,*) ' **************************************'
       write(6,*) ' * ',name
       write(6,*) ' **************************************'

       call timel(time1)
       dt = time1-time0
       if (dt.gt.0.1d0) then
        iday = 0
!c        iday = dt/day
        ihour = (dt-iday*day)/hour
        imin = (dt-iday*day-ihour*hour)/minute
        sec = dt-iday*day-ihour*hour-imin*minute
        write(6,3) ihour,imin,sec
3       format(/' CPU TIME  ',i4,' h ',i2,' min ',f4.1,' sec')
       end if
       datval(1:24) = 0
!       call ctime_fort(datval)
!       write(6,*)
!       write(6,*) ' FINISHED ON:  ',char(datval(1:24))
       call date_and_time(VALUES=datval)
       if(maxval(datval).gt.0) then
        write(6,'('' DATE              '',i2.2,''/'',i2.2,''/'',i4)')   &
     &                              datval(3),datval(2),datval(1)
        write(6,'('' TIME              '',i2.2,'':'',i2.2,'':'',        &
     &            i2.2/)')          datval(5),datval(6),datval(7)
       end if

       if ( trim(name) .ne. trim(crtrn) ) then
        ! abort MPI before exiting
        yes_mpi = .false.
        call mpi_initialized(yes_mpi,ierr)
        if ( yes_mpi ) then
         ierrcom = 101
         call mpi_comm_rank(mpi_comm_world, myrank, ierr)
         write(0,*)
         if ( myrank .ne. 0 ) then

          write(0,'(/)')
          write(0,*) ' **************************************'
          write(0,*) ' * ',name
          write(0,*) ' **************************************'

          write(0,'(a,i4,a,i4)') ' MPIRUN STOPPED BY MECCA (code ',     &
     &                           ierrcom,'), RANK =',myrank
          call mpi_abort(mpi_comm_world,ierrcom,ierr)
         else
         write(6,'(a,i2,a)')' RANK =',myrank,' CHECK LOG FOR ERROR MSGS'
         end if
         write(0,*)
!         call mpi_finalize(ierr)
        else
         write(0,*) ' :: return ',name
        end if
        if ( trim(name) == 'DEBUG' ) then
         stop 'DEBUG ABORT'
        else
         open(nch,file='RUN_FAILED')
         write(nch,'(a)') name
         close(nch)
         flush 6
         if ( yes_mpi .and. myrank==0 ) then
          call mpi_abort(mpi_comm_world,ierrcom,ierr)
         end if
         stop ' RUN STOPPED BY MECCA, CHECK LOG FOR ERROR MSGS'
        end if
       else
        time0 = 0.d0
        flush 6
        call mpi_barrier(mpi_comm_world, ierr)
        return
       end if

      end if
!EOC
      end subroutine fstop

!BOP
!!ROUTINE: p_fstop
!!INTERFACE:
      subroutine p_fstop(name)
!!DESCRIPTION:
! currently this subroutine calls subroutine fstop
! 

!!ARGUMENTS:
       character*(*) name
!!REMARKS:
! this subroutine may be used to stop a process in parallel implementation
!EOP
!
       call fstop(name)
!      character*6 crtrn/'return'/
!      character (LEN=16) czone,cdate,ctime
!      integer      datval(8)
!      real dt,minute,hour,day
!      parameter (minute=60.,hour=60.*minute,day=24.*hour)
!
!       write(6,'(///)')
!       write(6,*) ' **************************************'
!       write(6,*) ' * the code is stopped: '
!       write(6,*) ' * ',name
!       write(6,*) ' *  ME=',me,' TIDS(ME)=',tids(me)
!       write(6,*) ' **************************************'
!
!       datval(1:8) = 0
!       call date_and_time(cdate,ctime,czone,                            &
!     &                    datval)
!      if(maxval(datval).gt.0) then
!       write(6,1) datval(3),cdate(5:5),cdate(6:6),datval(1)
!1      format(' DATE              ',i2,'/',2a1,'/',i4)
!       write(6,2) ctime(1:2),ctime(3:4),ctime(5:6),czone(1:5)
!2      format(' LOCAL TIME    ',a2,':',a2,':',a2,1x,a5)
!      end if
!!c
!       write(6,*)
!       write(6,*)  ' ALL PROCESSES WILL BE KILLED '
!       write(6,*)
!
!       call p_kill(ntids,tids,tids(me))
!       call pvmfexit(info)
!       if ( name .eq. crtrn ) return
!       stop
      end subroutine p_fstop
