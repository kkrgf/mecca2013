#!/bin/csh -f

#  example:  ./Make_ameskkr.sh ifort
#
#   before run a file <SYS>_<FF>.makein should be prepared;
#    SYS is system name (=`uname -s`), e.g. "Linux", "Darwin";
#    FF is a name of fortran compiler available on your machine,
#      e.g. "ifort" for Intel compiler, "gf" for gfortran ,etc.

./make.sh $1 libxc

./make.sh $1 all

./make.sh $1 install
