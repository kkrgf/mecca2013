!BOP
!!ROUTINE: strident1
!!INTERFACE:
       subroutine strident1(bases,ndimbas,nbasis,itype,                 &
     &               mapstr,mappnt,mapij,aij,ncount,ident,iprint)
!!DESCRIPTION:
! generates "map of (I,J)-equivalence" defined by arrays {\tt mapstr, mappnt}
! and respective (I,J) vectors {\tt aij} and indices {\tt mapij}
! {\bv
! input:
!       bases --position of basis atoms
!
! output:
!
!  if mappnt(i,j)==1   -- (i,j)-element (i.ne.j) of structure matrix
!                         should be calculated
!     mappnt(i,j)==0   --  it can be constructed from other element;
!        then mapstr(i,j) -- number of such an element
!
!     mappnt(i,i)      --  itype(i)
!
!
!     ncount -- number of different elements in structure matrix
! ((if(mappnt.ne.0) aij = bases_j-bases_i , i=1,nbasis), j=1,nbasis)
! \ev}
!

!!DO_NOT_PRINT
       implicit none
!!DO_NOT_PRINT

!!ARGUMENTS:
       real*8 bases(3,*)
       integer ndimbas,nbasis
       integer itype(*)
       integer mapstr(ndimbas,*),mappnt(ndimbas,*)
       integer mapij(2,*)
       real*8 aij(3,*)
       integer ncount,iprint,ident
!!REVISION HISTORY:
! Adapted - A.S. - 2013
!EOP
!
!BOC
       integer i,j,i1,j1,i2,j2,isub,jsub
       real*8 ax,ay,az,eps
       parameter (eps=1.d-7)

       if(ident.lt.0) then

      if(ident.eq.-100) then
       call strident(bases,ndimbas,nbasis,                              &
     &                 mapstr,mappnt,mapij,aij,ncount,iprint)
      else
       ncount = 0
       do j=1,nbasis
        do i=1,nbasis
!c          ncount = i+(j-1)*nbasis
         ncount = ncount+1
         if(i.eq.j) then
          mapstr(i,i) = ncount
          mappnt(i,i) = itype(i)
         else
          mapstr(i,j) = ncount
          mappnt(i,j) = 1
         end if
         aij(1,ncount) = (bases(1,i)-bases(1,j))
         aij(2,ncount) = (bases(2,i)-bases(2,j))
         aij(3,ncount) = (bases(3,i)-bases(3,j))
         mapij(1,ncount) = i
         mapij(2,ncount) = j
        end do
       end do
      end if

      return

       end if

       ncount = 1
       mapstr(1,1) = 1
       mappnt(1,1) = itype(1)
       mapij(1,ncount) = 1
       mapij(2,ncount) = 1
       aij(1,ncount) = 0.d0
       aij(2,ncount) = 0.d0
       aij(3,ncount) = 0.d0

       do j1 = 2,nbasis

      jsub = itype(j1)
      do i1=1,j1-1
       if(itype(i1).eq.jsub) then
        mapstr(j1,j1) = mapstr(i1,i1)
        mappnt(j1,j1) = mappnt(i1,i1)
        go to 1
       end if
      end do

      ncount = ncount+1
      mapstr(j1,j1) = ncount
      mappnt(j1,j1) = jsub
      mapij(1,ncount) = j1
      mapij(2,ncount) = j1
      aij(1,ncount) = 0.d0
      aij(2,ncount) = 0.d0
      aij(3,ncount) = 0.d0

1       continue

      do i1 = 1,j1-1

       isub = itype(i1)

       ax = (bases(1,i1)-bases(1,j1))
       ay = (bases(2,i1)-bases(2,j1))
       az = (bases(3,i1)-bases(3,j1))

       do i=1,ncount
        i2 = mapij(1,i)
        j2 = mapij(2,i)
        if(abs(ax-aij(1,i)).lt.eps.and.                                 &
     &        abs(ay-aij(2,i)).lt.eps.and.                              &
     &        abs(az-aij(3,i)).lt.eps) then
         if(isub.eq.itype(i2).and.jsub.eq.itype(j2)) then
          mapstr(i1,j1) = mapstr(i2,j2)
          mappnt(i1,j1) = 0
          go to 2
         end if
        end if
       end do

       ncount = ncount+1
       mapstr(i1,j1) = ncount
       mappnt(i1,j1) = 1
       mapij(1,ncount) = i1
       mapij(2,ncount) = j1
       aij(1,ncount) = ax
       aij(2,ncount) = ay
       aij(3,ncount) = az
2       continue
      end do

      do i1 = j1-1,1,-1
       isub = itype(i1)

       ax = (bases(1,j1)-bases(1,i1))
       ay = (bases(2,j1)-bases(2,i1))
       az = (bases(3,j1)-bases(3,i1))

       do i=1,ncount
        i2 = mapij(1,i)
        j2 = mapij(2,i)
         if(abs(ax-aij(1,i)).lt.eps.and.                                &
     &        abs(ay-aij(2,i)).lt.eps.and.                              &
     &        abs(az-aij(3,i)).lt.eps) then
         if(isub.eq.itype(j2).and.jsub.eq.itype(i2)) then
          mapstr(j1,i1) = mapstr(i2,j2)
          mappnt(j1,i1) = 0
          go to 4
         end if
        end if
       end do

       ncount = ncount+1
       mapstr(j1,i1) = ncount
       mappnt(j1,i1) = 1
       mapij(1,ncount) = j1
       mapij(2,ncount) = i1
       aij(1,ncount) = ax
       aij(2,ncount) = ay
       aij(3,ncount) = az
4       continue
      end do

       end do

      if(iprint.ge.0) then
       do i=1,min(40,ncount)
        i1 = mapij(1,i)
        j1 = mapij(2,i)
         write(6,'(''    Basis atom '',i2,'','',i2,'' at position'',    &
     &     t40,''='',2x,3f7.2)')  i1,j1,(aij(i2,i),i2=1,3)
      end do
      write(6,*)
      write(6,*) '  Number of different Aij = ',ncount,                 &
     &              ',  Nbasis**2 = ',nbasis*nbasis
      write(6,*)
      end if

       do j1 = nbasis+1,ident

      jsub = itype(j1)
      do i1=1,j1-1
       if(itype(i1).eq.jsub) then
        mapstr(j1,j1) = mapstr(i1,i1)
        mappnt(j1,j1) = mappnt(i1,i1)
        go to 10
       end if
      end do

      ncount = ncount+1
      mapstr(j1,j1) = ncount
      mappnt(j1,j1) = jsub
      mapij(1,ncount) = j1
      mapij(2,ncount) = j1
      aij(1,ncount) = 0.d0
      aij(2,ncount) = 0.d0
      aij(3,ncount) = 0.d0

10      continue

      do i1 = 1,j1-1

       isub = itype(i1)

       ax = (bases(1,i1)-bases(1,j1))
       ay = (bases(2,i1)-bases(2,j1))
       az = (bases(3,i1)-bases(3,j1))

       do i=1,ncount
        i2 = mapij(1,i)
        j2 = mapij(2,i)
        if(abs(ax-aij(1,i)).lt.eps.and.                                 &
     &        abs(ay-aij(2,i)).lt.eps.and.                              &
     &        abs(az-aij(3,i)).lt.eps) then
         if(isub.eq.itype(i2).and.jsub.eq.itype(j2)) then
          mapstr(i1,j1) = mapstr(i2,j2)
          mappnt(i1,j1) = 0
          go to 20
         end if
        end if
       end do

       ncount = ncount+1
       mapstr(i1,j1) = ncount
       mappnt(i1,j1) = 1
       mapij(1,ncount) = i1
       mapij(2,ncount) = j1
       aij(1,ncount) = ax
       aij(2,ncount) = ay
       aij(3,ncount) = az
20      continue
      end do

      do i1 = j1-1,1,-1
       isub = itype(i1)

       ax = (bases(1,j1)-bases(1,i1))
       ay = (bases(2,j1)-bases(2,i1))
       az = (bases(3,j1)-bases(3,i1))

       do i=1,ncount
        i2 = mapij(1,i)
        j2 = mapij(2,i)
         if(abs(ax-aij(1,i)).lt.eps.and.                                &
     &        abs(ay-aij(2,i)).lt.eps.and.                              &
     &        abs(az-aij(3,i)).lt.eps) then
         if(isub.eq.itype(j2).and.jsub.eq.itype(i2)) then
          mapstr(j1,i1) = mapstr(i2,j2)
          mappnt(j1,i1) = 0
          go to 40
         end if
        end if
       end do

       ncount = ncount+1
       mapstr(j1,i1) = ncount
       mappnt(j1,i1) = 1
       mapij(1,ncount) = j1
       mapij(2,ncount) = i1
       aij(1,ncount) = ax
       aij(2,ncount) = ay
       aij(3,ncount) = az
40      continue
      end do

       end do


       return
!EOC
       contains

!BOP
!!IROUTINE: strident
!!INTERFACE:
       subroutine strident(bases,ndimbas,nbasis,                        &
     &                     mapstr,mappnt,mapij,aij,ncount,iprint)
!!REVISION HISTORY:
! Adapted - A.S. - 2013
!!REMARKS:
! private procedure of subroutine strident1
!EOP
!
!BOC
       implicit none
       integer ndimbas,nbasis,ncount,iprint
       real*8 bases(3,nbasis),aij(3,nbasis*nbasis)
       integer mapstr(ndimbas,nbasis),mappnt(ndimbas,nbasis)
       integer mapij(2,*)
!c
!c      input:
!c            bases --position of basis atoms
!c
!c      output:
!c
!c       if mappnt(i,j)==1   -- (i,j)-element of structure matrix
!c                              should be calculated
!c          mappnt(i,j)==0   --  it can be constructed from other element;
!c             then mapstr(i,j) -- number of such an element
!c
!c          ncount -- number of different elements in structure matrix
!c      ((if(mappnt.ne.0) aij = bases_j-bases_i , i=1,nbasis), j=1,nbasis)
!c

       integer i,j,i1,j1,i2,j2
!c       integer i,j,i1,j1,i2,j2,i2j2,i2j1,j1i2
       real*8 ax,ay,az,eps
       parameter (eps=1.d-7)

       do j=1,nbasis
      do i=1,nbasis
!C         ncount = i+(j-1)*nbasis
       mappnt(i,j) = 1
!C         aij(1,ncount) = (bases(1,i)-bases(1,j))
!C         aij(2,ncount) = (bases(2,i)-bases(2,j))
!C         aij(3,ncount) = (bases(3,i)-bases(3,j))
      end do
       end do

       ncount = 1
       mapstr(1,1) = 1
       mappnt(1,1) = 1
       mapij(1,ncount) = 1
       mapij(2,ncount) = 1
       aij(1,ncount) = 0.d0
       aij(2,ncount) = 0.d0
       aij(3,ncount) = 0.d0
       do j1 = 2,nbasis
      mapstr(j1,j1) = mapstr(1,1)
      mappnt(j1,j1) = 0

      do i1 = 1,j1-1

!c      i1j1 = i1+(j1-1)*nbasis

       ax = (bases(1,i1)-bases(1,j1))
       ay = (bases(2,i1)-bases(2,j1))
       az = (bases(3,i1)-bases(3,j1))

       do i=1,ncount
        i2 = mapij(1,i)
        j2 = mapij(2,i)
        if(abs(ax-aij(1,i)).lt.eps.and.                                 &
     &        abs(ay-aij(2,i)).lt.eps.and.                              &
     &        abs(az-aij(3,i)).lt.eps) then
          mapstr(i1,j1) = mapstr(i2,j2)
          mappnt(i1,j1) = 0
          go to 2
        end if
       end do

       ncount = ncount+1
       mapstr(i1,j1) = ncount
       mappnt(i1,j1) = 1
       mapij(1,ncount) = i1
       mapij(2,ncount) = j1
       aij(1,ncount) = ax
       aij(2,ncount) = ay
       aij(3,ncount) = az
2       continue
      end do

      do i1 = j1-1,1,-1
!c      j1i1 = j1+(i1-1)*nbasis

       ax = (bases(1,j1)-bases(1,i1))
       ay = (bases(2,j1)-bases(2,i1))
       az = (bases(3,j1)-bases(3,i1))

       do i=1,ncount
        i2 = mapij(1,i)
        j2 = mapij(2,i)
         if(abs(ax-aij(1,i)).lt.eps.and.                                &
     &        abs(ay-aij(2,i)).lt.eps.and.                              &
     &        abs(az-aij(3,i)).lt.eps) then
          mapstr(j1,i1) = mapstr(i2,j2)
          mappnt(j1,i1) = 0
          go to 4
        end if
       end do

       ncount = ncount+1
       mapstr(j1,i1) = ncount
       mappnt(j1,i1) = 1
       mapij(1,ncount) = j1
       mapij(2,ncount) = i1
       aij(1,ncount) = ax
       aij(2,ncount) = ay
       aij(3,ncount) = az
4       continue
      end do

       end do

       if(iprint.ge.0) then
      do i=1,min(40,ncount)
        i1 = mapij(1,i)
        j1 = mapij(2,i)
         write(6,'(''    Basis atom '',i2,'','',i2,'' at position'',    &
     &     t40,''='',2x,3f7.2)')  i1,j1,(aij(i2,i),i2=1,3)
      end do
      write(6,*)
      write(6,*) '  Number of different Aij = ',ncount,                 &
     &              ',  Nbasis**2-Nbasis+1 = ',nbasis*nbasis-nbasis+1
      write(6,*)
       end if

       return
!EOC
       end subroutine strident

       end subroutine strident1
