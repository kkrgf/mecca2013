!BOP
!!ROUTINE: invmatr
!!INTERFACE:
      subroutine invmatr(amt,kkrsz,nbasis,                              &
     &                   inverse,invparam,                              &
     &                   iprint,istop,logdet)
!!DESCRIPTION:
!  wrapper to invert matrix {\tt amt} (input/output)
!

!!DO_NOT_PRINT
      implicit real*8 (a-h,o-z)
!!DO_NOT_PRINT

!!ARGUMENTS:
      complex(8) :: amt(*)
      integer, intent(in) :: kkrsz,nbasis
      integer, intent(in) ::  inverse,invparam,iprint
      character(10), intent(in) :: istop
      complex(8), intent(out), optional :: logdet
!!REVISION HISTORY:
! Adapted - A.S. - 2013
!EOP
!
!BOC
      interface
       subroutine gsvein1(amt,ndmat,                                    &
     &                  cof,ndcof,                                      &
     &                  kkrsz,natom,                                    &
     &                  logdet)
       integer, intent(in) :: ndmat,ndcof,kkrsz,natom
       complex(8) :: amt(ndmat,kkrsz*natom) 
       complex(8) :: cof(*) 
       complex(8), intent(out), optional :: logdet
       end subroutine gsvein1
      end interface
      interface
        subroutine wrtmtx(x,n,istop)
        implicit none
        integer, intent(in) :: n
        complex(8), intent(in) :: x(:,:)
        character(10), intent(in), optional :: istop
        end subroutine wrtmtx
      end interface
!c
      integer :: invswitch,nrmat
      complex(8) :: tau(1)
      character(10), parameter :: sname='invmatr'
!c     ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

      invswitch = inverse
      nrmat = kkrsz*nbasis

!C
      if( iprint.ge.3) then
      write(6,*) sname//'::'
        write(6,'('' istop, iprint '',a10,i5)')istop,iprint
      write(6,'('' kkrsz,nbasis,nrmat:'',3i3)')                         &
     &                        kkrsz,nbasis,nrmat
      write(6,'('' before inversion '')')
      call wrtmtx(reshape(amt(1:nrmat*nrmat),[nrmat,nrmat]),nrmat,istop)
      endif

      if ( present(logdet) ) then
       logdet = 0
      end if
!!      if(invswitch.eq.1) then
       if ( present(logdet) ) then
        call gsvein1(amt,nrmat,                                         &
     &                  tau,0,                                          &
     &                  kkrsz,nbasis,                                   &
     &                  logdet)
       else
        call gsvein1(amt,nrmat,                                         &
     &                  tau,0,                                          &
     &                  kkrsz,nbasis)
       end if
!CAB      else if(invswitch.eq.2) then
!CAB       nlev = invparam
!CAB       call gsvein2(amt,nrmat,
!CAB     >                  tau,0,
!CAB     >                  kkrsz,nbasis,
!CAB     >                  nlev)
!!      else
!!       write(6,*) sname//' :: INVSWITCH=',invswitch
!!       call fstop(sname//' :: Algorithm has not been implemented')
!!      end if

!c     ==============================================================
!c     printout if needed............................................
!c     ==============================================================

      if( iprint.ge.3) then
      write(6,*) sname//'::'
        write(6,'('' istop, iprint '',a10,i5)')istop,iprint
      write(6,'('' kkrsz,nbasis,nrmat:'',3i3)')                         &
     &                        kkrsz,nbasis,nrmat
      write(6,'('' after inversion '')')
      call wrtmtx(reshape(amt(1:nrmat*nrmat),[nrmat,nrmat]),nrmat,istop)
      endif

      if( istop .eq. sname ) then
        call fstop(sname)
      endif
!c
      return
!EOC
      end subroutine invmatr

