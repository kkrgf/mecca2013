!BOP
!!ROUTINE: ylag
!!INTERFACE:
      function ylag(xi,x,y,ind1,n1,imax,iex)
!!DESCRIPTION:
! LAGRANGE INTERPOLATION FOR A REAL FUNCTION of REAL ARGUMENT
! {\bv
! xi is intepolated entry into x-array
! n is the order of lagrangran interpolation
! y is array from which ylag is obtained by interpolation
! ind is the min-i for x(i).gt.xi
! if ind=0,x-array will be searched
! imax is max index of x-and y-arrays
! extrapolation can occur,iex=-1 or +1
! \ev}

!!DO_NOT_PRINT
      implicit none
      real(8) :: ylag
!!DO_NOT_PRINT

!!ARGUMENTS:
      real(8), intent(in)  :: xi
      integer, intent(in)  :: imax
      real(8), intent(in)  :: x(imax),y(imax)
      integer, intent(in)  :: ind1,n1
      integer, intent(out) :: iex

!AUTHORS:
! A. A. Brooks and E.C. Long,
! Computing Technology Center, Union Carbide Corp., Nuclear Div.,
! Oak Ridge, Tenn.
!EOP
!
!BOC
!
      integer :: i,j,ind,n,inl,inu
      real(8) :: s,p,d,xd
!c
      do j = 1,imax
       if(abs(xi-x(j)).lt.epsilon(xi)) then
        ylag=y(j)
        return
       end if
      end do

      ind=ind1
      n=n1
      iex=0
      if (n.le.imax) go to 10
      n=imax
      iex=n
   10 if (ind.gt.0) go to 40
      do 20 j = 1,imax
!         if(abs(xi-x(j)).lt.1.0d-07) go to 130
         if (xi-x(j)) 30,130,20
   20 continue
      iex=1
      go to 70
   30 ind=j
   40 if (ind.gt.1) go to 50
      iex=-1
   50 inl=ind-(n+1)/2
      if (inl.gt.0) go to 60
      inl=1
   60 inu=inl+n-1
      if (inu.le.imax) go to 80
   70 inl=imax-n+1
      inu=imax
   80 s=0.0d+00
      p=1.0d+00
      do j=inl,inu
         p=p*(xi-x(j))
         d=1.0d+00
         do 100 i=inl,inu
            if (i.ne.j) go to 90
            xd=xi
            go to 100
   90       xd=x(j)
  100       d=d*(xd-x(i))
            s=s+y(j)/d
      enddo
      ylag=s*p
      return
  130 ylag=y(j)
      return
!EOC
      end function ylag
!
!BOP
!!ROUTINE: ylag_cmplx
!!INTERFACE:
      function ylag_cmplx(xi,x,y,ind1,n1,imax,iex)
!!DESCRIPTION:
!  LAGRANGE INTERPOLATION FOR A COMPLEX FUNCTION of REAL ARGUMENT
! {\bv
! xi is intepolated entry into x-array
! n is the order of lagrangran interpolation
! y is a complex array from which ylag_cmplx is obtained by
!   interpolation
! ind is the min-i for x(i).gt.xi
! if ind=0,x-array will be searched
! imax is max index of x-and y-arrays
! extrapolation can occur,iex=-1 or +1
! \ev}

!!DO_NOT_PRINT
      implicit none
      complex(8) :: ylag_cmplx
!!DO_NOT_PRINT

!!ARGUMENTS:
      real(8), intent(in)  :: xi
      integer, intent(in)  :: imax
      real(8), intent(in)  :: x(imax)
      complex(8), intent(in)  :: y(imax)
      integer, intent(in)  :: ind1,n1
      integer, intent(out) :: iex

!AUTHORS:
! A. A. Brooks and E.C. Long,
! Computing Technology Center, Union Carbide Corp., Nuclear Div.,
! Oak Ridge, Tenn.
!EOP
!
!BOC
!c
      complex(8) :: s
      complex(8), parameter :: czero=(0.0D0,0.0D0)
!c
      integer :: i,j,ind,n,inl,inu
      real(8)    :: p,d,xd
!c
      do j = 1,imax
       if(abs(xi-x(j)).lt.epsilon(xi)) then
        ylag_cmplx=y(j)
        return
       end if
      end do

      ind=ind1
      n=n1
      iex=0
      if (n.le.imax) go to 10
      n=imax
      iex=n
   10 if (ind.gt.0) go to 40
      do 20 j = 1,imax
!         if(abs(xi-x(j)).lt.1.0d-07) go to 130
         if (xi-x(j)) 30,130,20
   20 continue
      iex=1
      go to 70
   30 ind=j
   40 if (ind.gt.1) go to 50
      iex=-1
   50 inl=ind-(n+1)/2
      if (inl.gt.0) go to 60
      inl=1
   60 inu=inl+n-1
      if (inu.le.imax) go to 80
   70 inl=imax-n+1
      inu=imax
   80 s=czero
      p=1.0d+00
      do j=inl,inu
         p=p*(xi-x(j))
         d=1.0d+00
         do 100 i=inl,inu
            if (i.ne.j) go to 90
            xd=xi
            go to 100
   90       xd=x(j)
  100       d=d*(xd-x(i))
            s=s+y(j)/dcmplx(d,0.0D0)
      enddo
      ylag_cmplx=s*dcmplx(p,0.0D0)
      return
  130 ylag_cmplx=y(j)
      return
!EOC
      end function ylag_cmplx
