module canonical_bands_module
    implicit none
    integer,  parameter, private :: dp = selected_real_kind(15,307)





    type, public :: canonical_bands_result_kappa_t
           real(dp) :: band_bottom     = 0.0_dp
           real(dp) :: band_center     = 0.0_dp
           real(dp) :: band_top        = 0.0_dp
           integer  :: kappa           = 0
           integer  :: l               = 0
           integer  :: number_of_nodes = 0
        contains
           procedure, pass, public  :: clear => canonical_bands_result_kappa_t_clear
    end type canonical_bands_result_kappa_t
    private :: canonical_bands_result_kappa_t_clear

    type, public :: canonical_bands_results_kappa_t
        type(canonical_bands_result_kappa_t), allocatable, public :: band_results(:)
        integer,                                           public :: number_of_results = 0
        integer,                                           public :: print_unit        = -1
        contains
           procedure, pass, public  :: clear    => canonical_bands_results_kappa_t_clear
           procedure, pass, public  :: reserve  => canonical_bands_results_kappa_t_reserve
           procedure, pass, public  :: add_data => canonical_bands_results_kappa_t_add_data
           procedure, pass, public  :: print    => canonical_bands_results_kappa_t_print
           procedure, pass, public  :: sort     => canonical_bands_results_kappa_t_sort
           procedure, pass, public  :: find_new_ebot_by_bandwidth  => canonical_bands_results_kappa_t_find_new_ebot_by_bandwidth
    end type canonical_bands_results_kappa_t
    private :: canonical_bands_results_kappa_t_clear
    private :: canonical_bands_results_kappa_t_reserve
    private :: canonical_bands_results_kappa_t_add_data
    private :: canonical_bands_results_kappa_t_print
    private :: canonical_bands_results_kappa_t_sort
    private :: canonical_bands_results_kappa_t_find_new_ebot_by_bandwidth

    
    type, public :: canonical_bands_result_t
        real(dp)                    :: ebot_local_bandwidth_bands      = 0.0_dp
        real(dp)                    :: ebot_local_bandwidth_intervals  = 0.0_dp
        integer                     :: ndata                           = 0
        complex(dp), allocatable    :: interval(:)             !!! using the real part as bottom, the complex part as top; using complex number simplifies without the need to introduce another type
        contains
           procedure, pass, public  :: clear             => canonical_bands_result_t_clear
           procedure, pass, public  :: reserve           => canonical_bands_result_t_reserve
           procedure, pass, public  :: add_interval      => canonical_bands_result_t_add_interval
           procedure, pass, public  :: add_results_kappa => canonical_bands_result_t_add_results_kappa
           procedure, pass, public  :: reduce            => canonical_bands_result_t_reduce
           procedure, pass, public  :: combine           => canonical_bands_result_t_combine
           procedure, pass, public  :: print             => canonical_bands_result_t_print
           procedure, pass, public  :: get_new_emax      => canonical_bands_result_t_get_new_emax
           procedure, pass, public  :: energy_in_intervals  => canonical_bands_result_t_energy_in_intervals
           procedure, pass, public  :: intervals_to_gnuplot => canonical_bands_result_t_intervals_to_gnuplot
    end type canonical_bands_result_t
    private :: canonical_bands_result_t_clear
    private :: canonical_bands_result_t_reserve
    private :: canonical_bands_result_t_add_interval
    private :: canonical_bands_result_t_add_results_kappa
    private :: canonical_bands_result_t_reduce
    private :: canonical_bands_result_t_combine
    private :: canonical_bands_result_t_print
    private :: canonical_bands_result_t_get_new_emax
    private :: canonical_bands_result_t_energy_in_intervals
    private :: canonical_bands_result_t_intervals_to_gnuplot



    type, public :: canonical_bands_t
           real(dp),                                     private :: Z                 =  0.0_dp
           real(dp),                                     private :: speed_of_light    =  0.0_dp
           integer,                                      private :: np                =  0
           integer,                                      private :: kappa             =  0
           real(dp), allocatable, dimension(:),          private :: r_times_potential
           real(dp), allocatable, dimension(:),          private :: radial_grid
           type(canonical_bands_results_kappa_t),        public  :: results_kappa
           type(canonical_bands_result_t),               public  :: results
           logical, private :: initialized  = .false.
           integer, private :: MAXITER1     = 500
           integer, private :: MAXITER2     = 500
           integer, private :: print_unit   = -1
           real(dp),private :: energy_step1 = 15.0 ! must be >0
           real(dp),private :: energy_acc   = 1D-8 ! must be >0
        contains
           procedure, pass, public  :: init                => canonical_bands_t_init
           procedure, pass, public  :: clear               => canonical_bands_t_clear
           procedure, pass, public  :: set_kappa           => canonical_bands_t_set_kappa
           procedure, pass, public  :: set_energy_accuracy => canonical_bands_t_set_energy_accuracy
           procedure, pass, public  :: run                 => canonical_bands_t_run
           procedure, pass, private :: run_solver          => canonical_bands_t_run_solver
           procedure, pass, public  :: run_kappa           => canonical_bands_t_run_kappa
           procedure, pass, private :: get_rg              => canonical_bands_t_get_rg
           procedure, pass, private :: get_log_derivative_zero_function => canonical_bands_t_get_log_derivative_zero_function
           procedure, pass, private :: get_band_center_function         => canonical_bands_t_get_band_center_function
           procedure, pass, private :: get_kappa_max       => canonical_bands_t_get_kappa_max
           procedure, pass, public  :: set_print_unit      => canonical_bands_t_set_print_unit
    end type canonical_bands_t
    private :: canonical_bands_t_init
    private :: canonical_bands_t_clear
    private :: canonical_bands_t_set_kappa
    private :: canonical_bands_t_set_energy_accuracy
    private :: canonical_bands_t_run
    private :: canonical_bands_t_run_solver
    private :: canonical_bands_t_run_kappa
    private :: canonical_bands_t_get_rg
    private :: canonical_bands_t_get_log_derivative_zero_function
    private :: canonical_bands_t_get_band_center_function
    private :: canonical_bands_t_get_kappa_max
    private :: canonical_bands_t_set_print_unit


    private :: kappa_to_l
    private :: kappa_to_two_j



    contains

        subroutine canonical_bands_result_kappa_t_clear(this)
            implicit none
            class(canonical_bands_result_kappa_t) :: this

            this%band_bottom     = 0.0_dp
            this%band_center     = 0.0_dp
            this%band_top        = 0.0_dp
            this%kappa           = 0
            this%l               = 0
            this%number_of_nodes = 0
        end subroutine canonical_bands_result_kappa_t_clear




        subroutine canonical_bands_results_kappa_t_clear(this)
            implicit none
            class(canonical_bands_results_kappa_t) :: this

            if(allocated(this%band_results)) deallocate(this%band_results)
            this%number_of_results = 0
        end subroutine canonical_bands_results_kappa_t_clear

        subroutine canonical_bands_results_kappa_t_reserve(this,n)
            implicit none
            class(canonical_bands_results_kappa_t) :: this
            integer, intent(in)              :: n

            type(canonical_bands_result_kappa_t),allocatable :: res_tmp(:)
            integer            :: new_size
            integer, parameter :: min_increment = 16

            if(n.le.0) then
                if(this%print_unit.gt.0) write(this%print_unit,'(a)') &
                &"canonical_bands_results_kappa_t cannot be reserved with 0 or a negative number."
                return
            endif
            if(.not.allocated(this%band_results)) then
                allocate(this%band_results(max(n,min_increment)))
                this%number_of_results = 0
                return
            endif
            
            if(size(this%band_results).ge.n) return
            new_size = max(n,size(this%band_results)+min_increment)
            if(this%number_of_results.gt.0) then
                allocate(res_tmp(this%number_of_results))
                res_tmp(1:this%number_of_results) = this%band_results(1:this%number_of_results)
            endif
            deallocate(this%band_results)
            allocate(this%band_results(new_size))
            if(this%number_of_results.gt.0) then
                this%band_results(1:this%number_of_results)=res_tmp(1:this%number_of_results)
                deallocate(res_tmp)
            endif

        end subroutine canonical_bands_results_kappa_t_reserve

        subroutine canonical_bands_results_kappa_t_add_data(this,e_bottom,e_center,e_top,kappa,numnodes)
            implicit none
            class(canonical_bands_results_kappa_t) :: this
            real(dp), intent(in) :: e_bottom,e_center,e_top
            integer,  intent(in) :: kappa,numnodes

            call this%reserve(this%number_of_results+1)
            this%number_of_results                                    = this%number_of_results + 1
            this%band_results(this%number_of_results)%band_bottom     = e_bottom
            this%band_results(this%number_of_results)%band_center     = e_center
            this%band_results(this%number_of_results)%band_top        = e_top
            this%band_results(this%number_of_results)%kappa           = kappa
            this%band_results(this%number_of_results)%l               = kappa_to_l(kappa)
            this%band_results(this%number_of_results)%number_of_nodes = numnodes

        end subroutine canonical_bands_results_kappa_t_add_data

        subroutine canonical_bands_results_kappa_t_print(this,unit)
            implicit none
            class(canonical_bands_results_kappa_t) :: this
            integer, optional,    intent(in) :: unit

            integer :: myunit,i,lname
            character(len=32) :: name

            if(this%number_of_results.le.0) return

            myunit = this%print_unit
            if(present(unit)) then
                myunit=unit
            endif
            if(myunit.le.0) return



            write(myunit,'(5x,4(a16,1x),1x,a5,1x,a2,1x,a5,1x,a)') &
            & "band bottom","band center","band top","band width",&
            & "kappa","l","nodes","name"
            write(myunit,'(5x,a)') repeat("-",90)
            do i=1,this%number_of_results
                call kappa_to_name(this%band_results(i)%kappa,this%band_results(i)%number_of_nodes,name,lname)
                write(myunit,'(5x,4(f16.8,1x),1x,i5,1x,i2,1x,i5,1x,a)') &
                & this%band_results(i)%band_bottom,&
                & this%band_results(i)%band_center, &
                & this%band_results(i)%band_top,&
                & this%band_results(i)%band_top-this%band_results(i)%band_bottom,&
                & this%band_results(i)%kappa,&
                & this%band_results(i)%l,&
                & this%band_results(i)%number_of_nodes,&
                & name(1:lname)
            enddo ! i

        end subroutine canonical_bands_results_kappa_t_print

        subroutine canonical_bands_results_kappa_t_sort(this)
            implicit none
            class(canonical_bands_results_kappa_t) :: this

            integer               :: i,n
            real(dp), allocatable :: dx(:),didx(:)
            logical               :: is_sorted
            type(canonical_bands_result_kappa_t),allocatable :: res_tmp(:)

            n = this%number_of_results
            if(n.lt.1) return
            allocate(dx(n),didx(n))
            do i=1,n
                dx  (i) = this%band_results(i)%band_bottom
                didx(i) = real(i,dp)
            enddo ! i
            call DSORT (dx,didx,n,2)

            is_sorted=.true.
            do i=1,n
                if(int(didx(i)).ne.i) then
                    is_sorted=.false.
                    exit
                endif
            enddo ! i

            if(.not.is_sorted) then
                allocate(res_tmp(n))
                res_tmp(1:n) = this%band_results(1:n)
                do i=1,n
                    this%band_results(i) = res_tmp(int(didx(i)))
                enddo 
                deallocate(res_tmp)
            endif

            deallocate(dx,didx)
        end subroutine canonical_bands_results_kappa_t_sort

        function canonical_bands_results_kappa_t_find_new_ebot_by_bandwidth(&
            &this,ebotmax,mingap,maxbandwidth) result(ebmax)
            implicit none
            class(canonical_bands_results_kappa_t) :: this
            real(dp),                   intent(in) :: ebotmax
            real(dp),                   intent(in) :: mingap
            real(dp),                   intent(in) :: maxbandwidth

            real(dp) :: ebmax
            integer  :: i,k


            ebmax = ebotmax
            
            call this%sort
            
            do i=1,this%number_of_results
                if((this%band_results(i)%band_top-this%band_results(i)%band_bottom) .gt. maxbandwidth) then
                    do k=i,2,-1
                        if((this%band_results(k)%band_bottom-this%band_results(k-1)%band_top).ge. mingap) then
                            ebmax = this%band_results(k)%band_bottom-mingap/2.0_dp
                            return
                        endif
                    enddo ! k
                    ebmax = this%band_results(1)%band_bottom-mingap/2.0_dp
                    return
                endif
            enddo ! i

        end function canonical_bands_results_kappa_t_find_new_ebot_by_bandwidth



        subroutine canonical_bands_result_t_clear(this)
            implicit none
            class(canonical_bands_result_t) :: this
            if(allocated(this%interval)) deallocate(this%interval)
            this%ndata                           = 0
            this%ebot_local_bandwidth_bands      = 0.0_dp
            this%ebot_local_bandwidth_intervals  = 0.0_dp
        end subroutine canonical_bands_result_t_clear

        subroutine canonical_bands_result_t_reserve(this,n)
            implicit none
            class(canonical_bands_result_t) :: this
            integer,             intent(in) :: n
            
            integer,      parameter :: min_increment = 16
            integer                 :: n_new
            complex(dp),allocatable :: interval_tmp(:)

            if(.not.allocated(this%interval)) then
                allocate(this%interval(n))
                this%ndata = 0
                return
            endif

            if(n.le.size(this%interval)) return

            n_new=max(size(this%interval)+min_increment,n)
            if(this%ndata.gt.0) then
                allocate(interval_tmp(this%ndata))
                interval_tmp(1:this%ndata)=this%interval(1:this%ndata)
            endif
            deallocate(this%interval)
            allocate(this%interval(n_new))
            if(this%ndata.gt.0) then
                this%interval(1:this%ndata)=interval_tmp(1:this%ndata)
                deallocate(interval_tmp)
            endif
            
        end subroutine canonical_bands_result_t_reserve

        subroutine canonical_bands_result_t_add_interval(this, e_bottom_top_in,delta)
            implicit none
            class(canonical_bands_result_t) :: this
            complex(dp),            intent(in) :: e_bottom_top_in
            real(dp), optional,     intent(in) :: delta
 
            integer  :: i,idx
            real(dp) :: eb,et
            complex(dp) :: e_bottom_top

            !!! making sure the intervals are in correct order: bottom < top
            eb           = real (e_bottom_top_in,dp)
            et           = aimag(e_bottom_top_in)
            e_bottom_top = cmplx(min(eb,et),max(eb,et),dp)
            eb           = real (e_bottom_top,dp)
            et           = aimag(e_bottom_top)
            
            call this%reserve(this%ndata+1)

            !! checking where to insert interval based on ebot, and inserting interval
            idx=0
            do i=1,this%ndata
                if(eb.ge.real(this%interval(i),dp)) idx=i
            enddo
            do i=this%ndata,idx+1,-1
                this%interval(i+1) = this%interval(i)
            enddo ! i
            this%interval(idx+1)   = e_bottom_top
            this%ndata=this%ndata+1

            if(present(delta)) then
               call this%reduce(delta)
            else 
               call this%reduce
            endif

        end subroutine canonical_bands_result_t_add_interval


        subroutine canonical_bands_result_t_reduce(this,delta)
            implicit none
            class(canonical_bands_result_t) :: this
            real(dp), optional, intent(in)  :: delta

            integer  :: ndi,ndj,i,j,k
            real(dp) :: eb1,et1,eps

            eps = 0.0_dp
            if(present(delta)) then
                eps = abs(delta)
            endif

            ndi=this%ndata
            do i=1,ndi
                ndj=this%ndata
                do j=2,ndj
                    !if(aimag(this%interval(j-1)) .ge. real(this%interval(j),dp) ) then
                    if((real(this%interval(j),dp)- aimag(this%interval(j-1))  ).le.eps ) then
                        !! reducing interval by merging (j-1) and (j)
                        !eb1=min( real(this%interval(j-1),dp),aimag(this%interval(j)   ) )
                        !et1=max(aimag(this%interval(j-1)   ), real(this%interval(j),dp) )
                        
                        eb1=min(min(real(this%interval(j-1),dp),aimag(this%interval(j-1))),&
                               &min(real(this%interval(j  ),dp),aimag(this%interval(j  ))))
                        et1=max(max(real(this%interval(j-1),dp),aimag(this%interval(j-1))),&
                               &max(real(this%interval(j  ),dp),aimag(this%interval(j  ))))

                        this%interval(j-1) = cmplx(eb1,et1,dp)
                        do k=j,ndj-1
                            this%interval(k)=this%interval(k+1)
                        enddo ! k
                        this%ndata=this%ndata-1
                        exit
                    endif
                enddo ! j
            enddo ! i

        end subroutine canonical_bands_result_t_reduce

        subroutine canonical_bands_result_t_combine(this,x)
            implicit none
            class(canonical_bands_result_t)            :: this
            type(canonical_bands_result_t), intent(in) :: x

            integer :: i

            do i=1,x%ndata
                call this%add_interval(x%interval(i))
            enddo ! i

        end subroutine canonical_bands_result_t_combine

        subroutine canonical_bands_result_t_add_results_kappa(this,band_results)
            implicit none
            class(canonical_bands_result_t) :: this
            type(canonical_bands_results_kappa_t), intent(in) :: band_results

            integer     :: i
            complex(dp) :: x

            do i=1,band_results%number_of_results
                x=cmplx(band_results%band_results(i)%band_bottom,band_results%band_results(i)%band_top,dp)
                call this%add_interval(x)
            enddo ! i

        end subroutine canonical_bands_result_t_add_results_kappa

        subroutine canonical_bands_result_t_print(this,unit)
            implicit none
            class(canonical_bands_result_t) :: this
            integer, optional,   intent(in) :: unit

            integer :: myunit,i

            myunit = 6
            if(present(unit)) then
                myunit = unit
            endif
            if(myunit.le.0) return

            write(myunit,'("")') 
            write(myunit,'(5x,a)') "intervals:"
            do i=1,this%ndata
                write(myunit,'(5x,i4,". ",2f16.8)') i,real(this%interval(i),dp),aimag(this%interval(i))
            enddo ! i

        end subroutine canonical_bands_result_t_print



        subroutine canonical_bands_result_t_get_new_emax(this,emax)
            implicit none
            class(canonical_bands_result_t) :: this
            real(dp),         intent(inout) :: emax

            integer  :: i
            real(dp) :: et,eb
        
            !! it is assumed the data is already sorted, only the last interval needs to be checked
            if(this%ndata.gt.0) then
                et=aimag(this%interval(this%ndata))
                eb=real(this%interval(this%ndata),dp)
                if(eb.lt.emax .and. et.ge.emax) emax=eb
            endif

        end subroutine canonical_bands_result_t_get_new_emax

        !! this returns an energy equal or smaller energy and emax that lies in a gap the is sufficiently wide
        function canonical_bands_result_t_energy_in_intervals(this,energy,emax,mingap) result(ebot)
            implicit none
            class(canonical_bands_result_t) :: this
            real(dp),            intent(in) :: energy
            real(dp),            intent(in) :: emax
            real(dp),            intent(in) :: mingap

            real(dp) :: ebot
            real(dp) :: e1,e2,mingap_half
            integer  :: i

            ebot = min(energy,emax)
            if(this%ndata.le.0 .or. energy.ge.emax) return

            mingap_half = mingap/2.0_dp

            do i=this%ndata+1,2,-1
                e1=aimag(this%interval(i-1)   )
                if(i.eq.this%ndata+1) then
                    e2=emax
                else
                    e2=real(this%interval(i   ),dp)            !!! e1 <= e2
                endif
                if((e2-e1).lt.mingap .or. (e2.lt.e1)) cycle    !!! excluding gaps that are smaller than mingap, looking for gaps lower in energy

                !! case energy is higher than e1 and e2
                if(energy.ge.e2) then
                    ebot = e2-mingap_half
                    return
                endif

                !! case energy is in interval [e1+mingap/2:e2-mingap/2]
                if((energy.ge.e1+mingap_half) .and. (energy.le.e2-mingap_half)) then
                    ebot = energy
                    return
                endif

            enddo ! i

            !! final option, going below the lowest interval
            ebot = min(energy,real(this%interval(1),dp)-mingap_half)

        end function canonical_bands_result_t_energy_in_intervals

        subroutine canonical_bands_result_t_intervals_to_gnuplot(this,outfile)
            implicit none
            class(canonical_bands_result_t) :: this
            character(len=*),   intent(in)  :: outfile

            integer,  parameter :: funit = 67
            integer             :: i
            real(dp), parameter :: zero = 0.0_dp
            real(dp), parameter :: one  = 1.0_dp

            !if(this%ndata.le.0) return
            if(len_trim(outfile).eq.0) return

            open(funit,file=outfile(1:len_trim(outfile)),status='unknown')
            do i=1,this%ndata
                write(funit,'(2f16.8)')  real(this%interval(i),dp), zero
                write(funit,'(2f16.8)')  real(this%interval(i),dp), one
                write(funit,'(2f16.8)') aimag(this%interval(i)   ), one
                write(funit,'(2f16.8)') aimag(this%interval(i)   ), zero
                write(funit,'("")')
            enddo ! i
            close(funit)

        end subroutine canonical_bands_result_t_intervals_to_gnuplot



        subroutine canonical_bands_t_init(this,np,rv,r,c,speed_of_light_scaling,Ztot)
            implicit none
            class(canonical_bands_t)        :: this
            integer,            intent(in)  :: np                        !!! number of radial points
            real(dp),           intent(in)  :: rv(:)                     !!! r*V         rv(1:np)
            real(dp),           intent(in)  :: r(:)                      !!! radial mesh  r(1:np), log mesh
            real(dp), optional, intent(in)  :: c                         !!! speed of light
            real(dp), optional, intent(in)  :: speed_of_light_scaling    !!! speed of light scaling factor (default: 1.0)
            real(dp), optional, intent(in)  :: Ztot                      !!! nuclear number                (default: examine potential at small r)

            call this%clear
            if(np.lt.0) return

            this%np=np
            allocate(this%r_times_potential(np),this%radial_grid(np))
            this%r_times_potential(1:np) = rv(1:np)
            this%radial_grid      (1:np) = r (1:np)

            if(present(c)) then
                this%speed_of_light = c 
            else
                this%speed_of_light = 2.0_dp*137.035999139_dp
            endif

            if(present(speed_of_light_scaling)) then
                this%speed_of_light = this%speed_of_light*speed_of_light_scaling
            endif

            if(present(Ztot)) then
                this%Z = Ztot
            else
                this%Z = real(int(abs(rv(1)/2.0_dp)),dp)
            endif

            this%initialized = .true.
        end subroutine canonical_bands_t_init


        
        subroutine canonical_bands_t_clear(this)
            implicit none
            class(canonical_bands_t) :: this
            this%Z                 =  0.0_dp
            this%speed_of_light    =  0.0_dp
            this%np                =  0
            this%kappa             =  0
            if(allocated(this%r_times_potential)) deallocate(this%r_times_potential)
            if(allocated(this%radial_grid      )) deallocate(this%radial_grid      )
            call this%results_kappa%clear()

            call this%results_kappa%clear
            call this%results%clear

            this%initialized = .false.
        end subroutine canonical_bands_t_clear

        subroutine canonical_bands_t_set_kappa(this,kappa)
            implicit none
            class(canonical_bands_t)            :: this
            integer,                 intent(in) :: kappa

            if(kappa.eq.0 .and. this%print_unit.gt.0) then
                write(this%print_unit,'(a)') "error in setting kappa: kappa cannot be 0!"
                return
            endif
            this%kappa = kappa

        end subroutine canonical_bands_t_set_kappa

        subroutine canonical_bands_t_set_energy_accuracy(this,e_accuracy)
            implicit none
            class(canonical_bands_t)             :: this
            real(dp),                 intent(in) :: e_accuracy

            this%energy_acc = max(1D-20,abs(e_accuracy))
        end subroutine canonical_bands_t_set_energy_accuracy


        subroutine canonical_bands_t_run(this,kappa_max,energy_max,mingap,energy_max_out,maxbandwidth)
            implicit none
            class(canonical_bands_t)             :: this
            integer,  optional,       intent(in) :: kappa_max
            real(dp), optional,       intent(in) :: energy_max
            real(dp), optional,       intent(in) :: mingap
            real(dp), optional,       intent(out):: energy_max_out
            real(dp), optional,       intent(in) :: maxbandwidth

            integer   :: kappamax, ikk, kappa
            real(dp)  :: emax,emax_global,de,maxbw,mingp

            kappamax = 0
            if(present(kappa_max)) then
                kappamax=kappa_max
            endif

            emax=0.0_dp
            if(present(energy_max)) then
                emax=energy_max
            else
                if(this%np.gt.10) then
                   emax=maxval(this%r_times_potential(10:this%np)/this%radial_grid(10:this%np))
                endif
                if(this%print_unit.gt.0) write(this%print_unit,'(a,f16.8)') &
                & "using emax=",emax
            endif
            emax_global = emax

            de = 0.0_dp
            if(present(mingap)) then
                de=abs(mingap)
            endif

            maxbw = 0.01_dp
            if(present(maxbandwidth)) then
                maxbw=abs(maxbandwidth)
            endif

            mingp = 0.3_dp
            if(present(mingap)) then
                mingp=abs(mingap)
            endif

            this%results%ebot_local_bandwidth_bands     = emax
            this%results%ebot_local_bandwidth_intervals = emax

            if(kappamax.le.0) kappamax=this%get_kappa_max(emax)

            call this%results%clear
            call this%results_kappa%clear
            do ikk=1,2*kappamax
                kappa=(ikk+1)/2 * (-1)**mod(ikk,2)
                call this%run_kappa(kappa,emax)

                call this%results_kappa%sort
                call this%results%add_results_kappa(this%results_kappa)
                call this%results%reduce(de)
                call this%results%get_new_emax(emax)
            enddo ! ikk

            !call this%results_kappa%sort

            this%results%ebot_local_bandwidth_bands=&
                   &this%results_kappa%find_new_ebot_by_bandwidth(emax_global,mingp,maxbw)
            this%results%ebot_local_bandwidth_intervals=&
               &this%results%energy_in_intervals(this%results%ebot_local_bandwidth_bands,emax_global,mingp)

            if(this%print_unit.gt.0) then
!                write(this%print_unit,'("")')
!                write(this%print_unit,'(a)') repeat("-",79)
!                write(this%print_unit,'(a,i8)') "kappa max = ",kappamax
!                write(this%print_unit,'(a)') repeat("-",79)
                call this%results_kappa%print(this%print_unit)
!                write(this%print_unit,'(a)') repeat("-",79)
                call this%results%print(this%print_unit)
                write(this%print_unit,'(5x,a,f16.8)') &
                &"local ebot estimate based on band     bandwidth: ",&
                &this%results%ebot_local_bandwidth_bands
                write(this%print_unit,'(5x,a,f16.8)') &
                &"local ebot estimate based on interval bandwidth: ",&
                &this%results%ebot_local_bandwidth_intervals
            endif ! print_unit


            if(present(energy_max_out)) then
                energy_max_out = emax
            endif
        end subroutine canonical_bands_t_run

        subroutine canonical_bands_t_run_solver(this, kappa, en, res)
            use coresolver
            implicit none
            class(canonical_bands_t)            :: this
            integer,              intent(in)    :: kappa
            real(dp),             intent(in)    :: en
            type(outws3_results), intent(out)   :: res

            real(dp)            :: dk, gam
            real(dp), parameter :: slp = 1.0_dp

            dk  = real(kappa,dp)
            gam = sqrt(dk**2-(1.0d0*this%Z/this%speed_of_light)**2)
            call outws3(this%np,this%r_times_potential,this%radial_grid,en,this%speed_of_light,this%Z,gam,slp,dk,res)
        end subroutine canonical_bands_t_run_solver


        function canonical_bands_t_get_rg(this, en) result(rg)
            use coresolver, only : outws3_results
            implicit none
            class(canonical_bands_t)            :: this
            real(dp),             intent(in)    :: en

            type(outws3_results)  :: res
            real(dp)              :: rg

            call this%run_solver(this%kappa,en,res)
            rg=res%rg
        end function canonical_bands_t_get_rg

        function canonical_bands_t_get_log_derivative_zero_function(this, en) result(rg)
            use coresolver, only : outws3_results
            implicit none
            class(canonical_bands_t)            :: this
            real(dp),             intent(in)    :: en

            type(outws3_results)  :: res
            real(dp)              :: rg

            call this%run_solver(this%kappa,en,res)
            rg=res%logarithmic_derivative_xero_function
        end function canonical_bands_t_get_log_derivative_zero_function

        function canonical_bands_t_get_band_center_function(this, en) result(rg)
            use coresolver, only : outws3_results
            implicit none
            class(canonical_bands_t)            :: this
            real(dp),             intent(in)    :: en

            type(outws3_results)  :: res
            real(dp)              :: rg

            call this%run_solver(this%kappa,en,res)
            rg=res%band_center_function
        end function canonical_bands_t_get_band_center_function


        function canonical_bands_t_get_kappa_max(this, en) result(kappamax)
            use coresolver, only : outws3_results
            implicit none
            class(canonical_bands_t)            :: this
            real(dp),             intent(in)    :: en
            integer                             :: kappamax

            !integer                :: kappa
            integer, parameter     :: kappa_maxmax = 6
            type(outws3_results)   :: res
            logical                :: ok(2,2)
            
            ok=.false.
            
            do kappamax = 1,kappa_maxmax
                ok(1,1)=ok(1,2)
                ok(2,1)=ok(2,2)
                call this%run_solver(kappamax,en,res)
                ok(1,2)=(res%number_of_nodes.eq.0 .and. res%logarithmic_derivative.gt.0.0_dp)
                call this%run_solver(-kappamax,en,res)
                ok(2,2)=(res%number_of_nodes.eq.0 .and. res%logarithmic_derivative.gt.0.0_dp)

                if(ok(1,1).and.ok(2,1).and.ok(1,2).and.ok(2,2)) return

            enddo ! kappamax

            kappamax = max(0,kappamax-1)
            
        end function canonical_bands_t_get_kappa_max


        subroutine canonical_bands_t_set_print_unit(this, unit)
            implicit none
            class(canonical_bands_t)            :: this
            integer, intent(in)                 :: unit

            this%print_unit               = unit
            this%results_kappa%print_unit = unit
        end subroutine canonical_bands_t_set_print_unit


        subroutine canonical_bands_t_run_kappa(this,kappa,energy_max)
            use coresolver, only : outws3_results
            use root_finder_Ridders_module
            implicit none
            class(canonical_bands_t)             :: this
            integer,                  intent(in) :: kappa
            real(dp),                 intent(in) :: energy_max

            integer               :: nodes_max,inodes,iter,nodes_e1,nodes_e2
            logical               :: found
            real(dp)              :: dkappa,dkappaPlusOne,gam,dl,dkappaPlusOnePlusLogDerCenter
            real(dp)              :: en,e1,e2,deltaE,rg_e1,rg_e2,ans
            real(dp)              :: b_top0,b_bottom0,b_center0
            real(dp), allocatable :: band_tops(:),band_bottoms(:),band_centers(:),energies_node(:)
            real(dp), parameter   :: zero = 0.0_dp
            type(outws3_results)  :: res

            if(.not.this%initialized) then
                if(this%print_unit.gt.0) write(this%print_unit,'(a)') &
                    &"canonical_bands_t needs to be initialized before using. Please run init(...) first."
                return 
            endif
            
            !write(*,'("running kappa=",i6)') kappa

            call this%set_kappa(kappa)
            dkappa=real(kappa,dp)
            dkappaPlusOne=dkappa+real(1.0_dp,dp)
            gam = sqrt(dkappa**2-(1.0d0*this%Z/this%speed_of_light)**2)
            dl  = real(kappa_to_l(kappa),dp)
            dkappaPlusOnePlusLogDerCenter=dkappaPlusOne-(dl+1.0_dp)

            en=energy_max
            call this%run_solver(kappa,en,res)
            !write(*,'("nodes=",i8)') res%number_of_nodes
            if(res%number_of_nodes.eq.0 .and. res%logarithmic_derivative.gt.zero) then
                !write(*,'(a,i5)') "all states are higher in energy. Nothing to do for kappa=",kappa
                return
             endif
            nodes_max=res%number_of_nodes

            if(res%number_of_nodes.eq.0) then
                !! case: nodes=0, log<0; meaning an unknown lower bound
                b_top0    = energy_max
                b_bottom0 = 1.0D100+abs(energy_max)
                b_center0 = energy_max
                iter=0
                do while(res%logarithmic_derivative.lt.zero.and. iter.lt.this%MAXITER1)
                    iter=iter+1
                    en=en-1.0d0
                    call this%run_solver(kappa,en,res)
                enddo
                if(iter.lt.this%MAXITER1) then
                    e1=en
                    e2=e1+1.0d0
                    if(root_finder_Ridders( myfunc_ld0 ,e1,e2,ans,this%energy_acc,quiet=.true.) ) then
                        b_bottom0 = ans
                        e1=ans
                        e2=energy_max
                        if(kappa.eq.-1) then
                            b_center0 = b_bottom0
                        else
                            if(root_finder_Ridders( myfunc_center,e1,e2,ans,this%energy_acc,quiet=.true.) ) then
                                b_center0 = ans
                            endif
                        endif
                    endif
                endif
                if(b_bottom0.le.energy_max) then
                    !! adding point
                    !! TO DO
                    if(this%print_unit.gt.0) then  
                    write(this%print_unit,'(5x,a,f16.8,5x,a,f16.8,5x,a,f16.8,5x,a,f16.8,2(3x,a,i3))') &
                & "band bottom:"      ,b_bottom0,&
                & "band center:"      ,b_center0,&
                & "band top:"         ,b_top0,&
                & "band width:"       ,b_top0-b_bottom0,&
                & "band nodes:"       ,res%number_of_nodes, &
                & "band kappa:"       ,kappa
                    endif ! print_unit
                endif
                   !write(*,'(a)') "WARNING, nodes=0 but logarithmic derivative is negative. This special case is not coded yet!!!!"
                return
            endif

            allocate(energies_node(0:nodes_max))
            allocate(band_tops(0:nodes_max),band_bottoms(0:nodes_max),band_centers(0:nodes_max))
            band_tops    = 1.0D100+abs(energy_max)
            band_bottoms = 1.0D100+abs(energy_max)
            band_centers = 1.0D100+abs(energy_max)
            energies_node= 1.0D100+abs(energy_max)
            energies_node(nodes_max) = energy_max

            !!!!!!!!!!!!!!!!!!!!!!!!! global bottom !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
            !e1        = energy_max
            !do inodes=nodes_max-1,0,-1
            !    deltaE=this%energy_step1
            !    res%number_of_nodes = -1
            !    iter=0
            !    do while(res%number_of_nodes.ne.inodes)
            !        iter=iter+1
            !        en=e1-deltaE
            !        !write(*,'(10x,"en=",f16.8)') en
            !        call this%run_solver(kappa,en,res)
            !        if(res%number_of_nodes.gt.inodes) then
            !            e1       = en
            !       !nodes_e1 = nodes
            !       else if(res%number_of_nodes.lt.inodes) then
            !            deltaE = deltaE/real(2.0_dp,dp)
            !       endif         
            !       if(iter.gt.this%MAXITER2) exit
            !    enddo !
            !    if(iter.gt.this%MAXITER2) then
            !       write(*,'(a,i5,1x,a,2i6)') "too many iterations when finding global bottom. kappa=",kappa,"nodes to find/max:",&
            !       &inodes,nodes_max
            !    endif
            !    write(*,'("niter bottom:",i8)') iter
            !    energies_node(inodes)=en
            !enddo ! inodes


            do inodes = nodes_max-1,0,-1
                e1     = energies_node(inodes+1)
                if(e1.gt.energy_max) e1=energy_max
                deltaE = abs(this%energy_step1)
                if(inodes.lt. nodes_max-2) then
                    deltaE = 2.0_dp*abs(energies_node(inodes+2)-energies_node(inodes+1))
                    deltaE = min(deltaE,500.0_dp)
                    deltaE = max(deltaE,0.01_dp)
                endif

                !res%number_of_nodes = -1
                !call this%run_solver(kappa,e1,res)
                !if(res%number_of_nodes .lt. inodes+1) then
                !    write(*,'(a,3i8)') "ERROR!!!  ",res%number_of_nodes,inodes+1,nodes_max
                !endif

                do iter=1,this%MAXITER2
                    en     = e1-deltaE
                    res%number_of_nodes = -1
                    call this%run_solver(kappa,en,res)

                    !write(*,'(i5,". ",f16.8,2i8,ES16.8)') iter,en,res%number_of_nodes,inodes, deltaE

                    if(res%number_of_nodes.eq.inodes) then
                        energies_node(inodes)=en
                        !write(*,'("found, iter=",i8," kappa=",i6," nodes=",i6)') iter,kappa,inodes
                        exit
                    endif
                
                    if(res%number_of_nodes.lt.inodes) then
                        deltaE = deltaE/real(2.0_dp,dp)
                    else
                        e1 = en
                    endif

                enddo ! iter
                if(iter.gt.this%MAXITER2) then
                   if(this%print_unit.gt.0) then
                   write(this%print_unit,'(a,i5,1x,a,2i6)') "Xtoo many iterations when finding global bottom. kappa=",&
                   &kappa,"nodes to find/max:",&
                   &inodes,nodes_max
                   endif
                   !stop
                endif

            enddo ! inodes




            !!!!!!!!!!!!!!!!!!!!!!!!! top of bands !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
            !!! top of the band: when g=0, or r*g=0
            do inodes=0,nodes_max-1
                e1       = energies_node(inodes)
                e2       = energies_node(inodes+1)
                if(e1.gt.energy_max .or. e1.ge.energy_max) cycle
                if(root_finder_Ridders( myfunc_rg ,e1,e2,ans,this%energy_acc,quiet=.true.) ) then
                    band_tops(inodes) = ans
                endif
            enddo ! inodes

            !!!!!!!!!!!!!!!!!!!!!!!!! bottom of bands !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
            do inodes=0,nodes_max-1
                !! cycle when band top couldn't be found
                if(band_tops(inodes).gt.energy_max) then
                   cycle
                endif
                
                en=band_tops(inodes)-real(2.0_dp,dp)*this%energy_acc
                call this%run_solver(kappa,en,res)    
                e1     = en
        
                if(res%logarithmic_derivative.ge.0.0_dp .or. res%number_of_nodes.lt.inodes) then
                    if(res%number_of_nodes.lt.inodes) then
                        band_bottoms(inodes)=band_tops(inodes)
                        band_centers(inodes)=band_tops(inodes)
                    else 
                        band_bottoms(inodes)=en
                        band_centers(inodes)=(band_bottoms(inodes)+band_tops(inodes))/real(2.0_dp,dp)
                    endif
!                    write(*,'(a,f16.8)') &
!                    & "found narrow band, not wide enough to resolve the bottom with current energy espilon. &
!                    &(this is not a error)   en=",&
!                    &band_bottoms(inodes)
                    cycle
                endif
  

                if(res%number_of_nodes.ne.inodes) then
                    if(this%print_unit.gt.0) then
                    write(this%print_unit,'(a,f16.8)')      &
                    &"***** error finding bottom of band. The number of nodes is inconsistent at energy ",en
                    write(this%print_unit,'(a,i4,1x,a,i4)') &
                    &"***** expecting", inodes,"band, but found found",res%number_of_nodes
                    endif
                    cycle
                endif


                deltaE   = abs(this%energy_step1/real(10.0_dp,dp))
                en       = e1-deltaE
                e2       = e1
                                
                found = .false.
                do iter=1,this%MAXITER2
                   res%number_of_nodes = -1
                   call this%run_solver(kappa,en,res)
                   
                    if(res%number_of_nodes.gt.inodes) then
                        if(this%print_unit.gt.0) then
                        write(this%print_unit,'(a,i6,5x,a,i5,3x,a,i5)') &
                        & "unexpeted number of nodes while finding the band bottom. kappa=",&
                        & kappa,&
                        & "nodes=",res%number_of_nodes,&
                        & "expected nodes=",inodes
                        endif ! print_unit
                       exit
                    endif

                    if(res%number_of_nodes.eq.inodes) then
                        !! correct number of nodes
                        if(res%logarithmic_derivative.lt.zero) then
                           e1 = en
                           en = e1-deltaE
                        else
                            !! logarithmic derivative is >=0
                            e2=en
                            found = .true.
                            exit
                        endif
                    else
                        !nodes < inodes
                        deltaE = deltaE/real(2.0_dp,dp)
                        en     = e1-deltaE
                    endif

                enddo ! iter1

                if(found) then
                    if(root_finder_Ridders( myfunc_ld0 ,e1,e2,ans,this%energy_acc,quiet=.true.) ) then
                     band_bottoms(inodes) = ans
                    endif
                else
                    if(this%print_unit.gt.0) then
                    write(this%print_unit,'(a,i6,5x,a,i5)') &
                    &"too many iterations when trying to find bottom of band. nodes=",inodes,"iterations:",iter
                    endif ! print_unit
                endif
            enddo ! inodes


            !!!!!!!!!!!!!!!!!!!!!!!!! center of bands !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
            do inodes=0,nodes_max-1
                if(kappa.eq.-1) then
                    band_centers(inodes)=band_bottoms(inodes)
                    cycle
                endif
                if(band_tops(inodes).gt.energy_max .or. band_bottoms(inodes).gt.energy_max) cycle
        
                e1=band_bottoms(inodes)
                e2=band_tops(inodes)

                if(root_finder_Ridders( myfunc_center,e1,e2,ans,this%energy_acc,quiet=.true.) ) then
                    band_centers(inodes) = ans
                endif

            enddo ! inodes

            !!!!!!!!!!!!!!!!!!!!!!!!! partial bands near the top !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
            if(band_tops(nodes_max-1).lt.energy_max) then
                e1    = energy_max
                call this%run_solver(kappa,e1,res)  
                if(res%logarithmic_derivative.lt.zero) then
                    !write(*,'("checking for possible partial band")')  
                    e2 = band_tops(nodes_max-1)+2.0d0*this%energy_acc
                    deltaE=(e2-e1)/real(21.0+dp,dp)
                    !write(*,'("e1/e2/dE=",3f16.8)') e1,e2,deltaE
                    iter=0
                    do while(res%logarithmic_derivative.lt.zero .and.iter.lt.this%MAXITER1)
                        iter=iter+1
                        e1=e1+deltaE
                        if(e1.lt.e2) exit
                        call this%run_solver(kappa,e1,res)
                        !write(*,'("e1=",f16.8)') e1
                    enddo
                    if(res%logarithmic_derivative.ge.zero) then
                        e2=e1-deltaE
                        if(root_finder_Ridders( myfunc_ld0 ,e1,e2,ans,this%energy_acc,quiet=.true.) ) then
                            band_tops   (nodes_max) = energy_max
                            band_centers(nodes_max) = energy_max
                            band_bottoms(nodes_max) = ans
                            if(kappa.eq.-1) then
                                band_centers(nodes_max) = band_bottoms(nodes_max)
                            else
                                e1=ans
                                e2=energy_max
                                if(root_finder_Ridders( myfunc_center,e1,e2,ans,this%energy_acc,quiet=.true.) ) then
                                    band_centers(nodes_max) = ans
                                endif ! if center found
                            endif ! kappa = -1
                        endif ! bottom found
                    endif ! 
                endif ! log der < 0
            endif





            do inodes=0,nodes_max!-1
                if(band_tops(inodes).gt.energy_max .or. band_bottoms(inodes).gt.energy_max) cycle
                call this%results_kappa%add_data(band_bottoms(inodes),band_centers(inodes),band_tops(inodes),kappa,inodes)

                !write(*,'(5x,a,f16.8,5x,a,f16.8,5x,a,f16.8,5x,a,f16.8,2(3x,a,i3))') &
                !& "band bottom:" ,band_bottoms(inodes),&
                !& "band center:" ,band_centers(inodes),&
                !& "band top:"    ,band_tops(inodes)   ,&
                !& "band width:"  ,band_tops(inodes)-band_bottoms(inodes),&
                !& "nodes:"       ,inodes, &
                !& "kappa:"       ,kappa
                !write(555,'(ES16.8,2i8)') band_bottoms(inodes),kappa,int(dl)
                !write(555,'(ES16.8,2i8)') band_centers(inodes),kappa,int(dl)
                !write(555,'(ES16.8,2i8)') band_tops(inodes)   ,kappa,int(dl)
                !write(555,'("")')
               enddo ! inodes 



            deallocate(energies_node,band_tops,band_bottoms, band_centers)
            contains
                function myfunc_rg( x ) result( val )
                    implicit none
                    real(dp) , intent(in) :: x
                    real(dp)              :: val
                    val = this%get_rg(x)
                end function myfunc_rg
                function myfunc_ld0( x ) result( val )
                    implicit none
                    real(dp) , intent(in) :: x
                    real(dp)              :: val
                    val = this%get_log_derivative_zero_function(x)
                end function myfunc_ld0
                function myfunc_center( x ) result( val )
                    implicit none
                    real(dp) , intent(in) :: x
                    real(dp)              :: val
                    val = this%get_band_center_function(x)
                end function myfunc_center
        end subroutine canonical_bands_t_run_kappa





        function kappa_to_l(kappa) result(l)
            implicit none
            integer, intent(in) :: kappa
            integer             :: l

            if(kappa.gt.0) then
                l =  kappa
             else
                l = -kappa-1
             endif
        end function kappa_to_l

        function kappa_to_two_j(kappa) result(two_j)
            implicit none
            integer, intent(in) :: kappa
            integer             :: two_j
            !! j= |kappa| -1/2
            two_j = 2*abs(kappa)-1
        end function kappa_to_two_j

        subroutine kappa_to_name(kappa,nodes,name,lenghtname)
            implicit none
            integer,           intent(in)  :: kappa
            integer,           intent(in)  :: nodes
            character(len=*),  intent(out) :: name
            integer, optional, intent(out) :: lenghtname

            integer :: ls,l,twoj,len1,ls2
            character(len=1), parameter :: spdnames(12)= ['s','p','d','f','g','h','i','j','k','l','m','n']
            character(len=32)           :: ts,ts8a,ts8b,ts8c
            character(len=1)            :: star

            len1 = len(name)
            l    = kappa_to_l(kappa)
            twoj = kappa_to_two_j(kappa)
            
            if(kappa.lt.0 .and.l.gt.0) then
                star='*'
            else
                star=' '
            endif

            write(ts8a,'(i8)') l+nodes+1
            ts8a=adjustl(ts8a)

            if(l.le.11) then
                ts8b=spdnames(l+1)
            else
                write(ts8b,'(i8)') l
                ts8b='('//trim(adjustl(ts8b))//')'
            endif

            write(ts8c,'(i8)') twoj
            ts8c=adjustl(ts8c)

            ts=trim(ts8a)//trim(adjustl(ts8b))//star//trim(ts8c)//"/2"
            ls=len_trim(ts)
            ls2=min(ls,len1)

            name(1:ls2)=ts(1:ls2)

            if(present(lenghtname)) then
                lenghtname=ls2
            endif

        end subroutine kappa_to_name

end module canonical_bands_module