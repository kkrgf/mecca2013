!BOP
!!ROUTINE: zgeadd
!!INTERFACE:
       subroutine zgeadd(a,lda,transa,b,ldb,transb,                     &
     &                   c,ldc,m,n)
!!DESCRIPTION:
! {\tt c = a + b}, arguments {\tt transa, transb} defines
! if respective matrix should be transposed before summation 
!
!EOP
!
!BOC
       implicit none
       integer lda,ldb,ldc,m,n
       complex*16 a(lda,*),b(ldb,*),c(ldc,*)
       character*1 transa,transb

       integer i,j

       if( transa.eq.'n'.or.transa.eq.'N') then
      if( transb.eq.'n'.or.transb.eq.'N' ) then
       do j=1,n
        do i=1,m
         c(i,j) = a(i,j) + b(i,j)
        end do
       end do
      else if( transb.eq.'t'.or.transb.eq.'T' ) then
       do j=1,n
        do i=1,m
         c(i,j) = a(i,j) + b(j,i)
        end do
       end do
      end if
       else if( transa.eq.'t'.or.transa.eq.'T' ) then
      if( transb.eq.'n'.or.transb.eq.'N' ) then
       do j=1,n
        do i=1,m
         c(i,j) = a(j,i) + b(i,j)
        end do
       end do
      else if( transb.eq.'t'.or.transb.eq.'T' ) then
       do j=1,n
        do i=1,m
         c(i,j) = a(j,i) + b(j,i)
        end do
       end do
      end if
       else
      stop ' ZGEADD: wrong parameters transa or transb'
       end if

       return
!EOC
       end subroutine zgeadd
