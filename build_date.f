!BOP
!!ROUTINE: buildDate
!!INTERFACE:
      character (LEN=32) function buildDate()
!!DESCRIPTION:
! it is intended to print link-date of executable module
!
!EOP
!      include 'build_date.h'
       buildDate = "Thu Aug 02 10:00:00 EST 2024"
      return
      end function buildDate


