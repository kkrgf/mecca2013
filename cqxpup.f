!BOP
!!ROUTINE: cqxpup
!!INTERFACE:
      subroutine cqxpup(ip,f,n,x,fint)
!!DESCRIPTION:
! integrates complex function exp(ip*x) * f(x); {\tt x} is log-mesh
!

!!DO_NOT_PRINT
        implicit real*8(a-h,o-z)
!!DO_NOT_PRINT

!!ARGUMENTS:
        integer, intent(in) :: ip,n
        real(8), intent(in) :: x(n)
        complex(8), intent(in) :: f(n)
        complex(8), intent(out) :: fint(n)
!!REVISION HISTORY:
! Adapted - A.S. - 2013
!EOP
!
!BOC
        complex*16 y1,y2,y3,slm,smh,smhsav
!c
        fint(1)=0.0d+00
        xm=x(1)
        xh=0.5d+00*(x(1)+x(2))
        xl=x(2)
!c       ------------------------------------------------------------
        call cqexp(ip,xl,xm,xh,x(1),x(2),x(3),f(1),f(2),f(3),slm,smh)
!c       ------------------------------------------------------------
        do 10 i=2,n-1
          smhsav=smh
          xl=0.5d+00*(x(i-1)+x(i))
          xm=x(i)
          xh=0.5d+00*(x(i+1)+x(i))
          x1=x(i-1)
          x2=x(i)
          x3=x(i+1)
          y1=f(i-1)
          y2=f(i)
          y3=f(i+1)
!c         ----------------------------------------------------------
          call cqexp(ip,xl,xm,xh,x1,x2,x3,y1,y2,y3,slm,smh)
!c         ----------------------------------------------------------
          fint(i)=fint(i-1)+smhsav+slm
10      continue
        smhsav=smh
        xl=x(n-1)
        xm=0.5d+00*(x(n)+x(n-1))
        xh=x(n)
!c       ------------------------------------------------------------
        call cqexp(ip,xl,xm,xh,x(n-2),x(n-1),x(n),f(n-2),f(n-1),f(n),   &
     &            slm,smh)
!c       ------------------------------------------------------------
        fint(n)=fint(n-1)+smhsav+smh
!c
        return

!EOC
        CONTAINS
!
!BOP
!!IROUTINE: cqexp
!!INTERFACE:
      subroutine cqexp(ip,xl,xm,xh,x1,x2,x3,y1,y2,y3,slm,smh)
!!DESCRIPTION:
! one-step integration
!
!!REMARKS:
! private procedure of subroutine cqexpup
!EOP
!
!BOC
      implicit none
      integer, intent(in) :: ip
      real(8), intent(in) :: xl,xm,xh,x1,x2,x3
      complex(8), intent(in) :: y1,y2,y3
      complex(8), intent(out) :: slm,smh
      real(8) :: xl2,xm2,xh2,xl3,xm3,xh3,pinv,pinv2,enxl,enxm,enxh
      real(8) :: xenxl,xenxm,xenxh
      real(8) :: x2enxl,x2enxm,x2enxh
      complex(8) :: a,b,c,ap,bp,app,bpp
!c
!c
      xl2=xl*xl
      xm2=xm*xm
      xh2=xh*xh
      xl3=xl2*xl
      xm3=xm2*xm
      xh3=xh2*xh
!c
      a=y1
      b=(y2-y1)/(x2-x1)
      c=( (y3-y1)/(x3-x1) -b )/(x3-x2)
      ap=a-b*x1 + c*x1*x2
      bp=b-c*(x1+x2)
!c
      if (ip.ne.0) then
        pinv=1.d0/ip
        pinv2=pinv*pinv
        enxl=exp(ip*xl)
        enxm=exp(ip*xm)
        enxh=exp(ip*xh)
        xenxl=xl*enxl
        xenxm=xm*enxm
        xenxh=xh*enxh
        x2enxl=xl2*enxl
        x2enxm=xm2*enxm
        x2enxh=xh2*enxh
        app=ap-bp*pinv+2.d0*c*pinv2
        bpp=bp-2.d0*c*pinv
        slm=(app*(enxm-enxl)+bpp*(xenxm-xenxl)+c*(x2enxm-x2enxl))*pinv
        smh=(app*(enxh-enxm)+bpp*(xenxh-xenxm)+c*(x2enxh-x2enxm))*pinv
      else
        slm=ap*(xm-xl)+0.5d0*bp*(xm2-xl2)+1.d0/3.d0*c*(xm3-xl3)
        smh=ap*(xh-xm)+0.5d0*bp*(xh2-xm2)+1.d0/3.d0*c*(xh3-xm3)
      endif
!c
      return
!EOC
      end subroutine cqexp

        end subroutine cqxpup
