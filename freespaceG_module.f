!BOP
!!MODULE: freespaceG
!!INTERFACE:
      module freespaceG
!!DESCRIPTION:
! free-space Green's function matrix issues
!
!!USES:
      use mecca_constants
      use mtrx_interface
      use gaunt, only : gauntdlm

!!DO_NOT_PRINT
!DEBUG      use raymethod
      private
!!DO_NOT_PRINT

!!PUBLIC MEMBER FUNCTIONS:
      public gf0_ks, gf0_rs
!!PRIVATE MEMBER FUNCTIONS:
! subroutine ewald
! subroutine rlylmb
! subroutine makedrs
!!REVISION HISTORY:
! Initial version - A.S. - Oct 2014
!EOP
!
      contains
!BOP
!!IROUTINE: gf0_ks
!!INTERFACE:
      subroutine gf0_ks(lmax,natom,                                     &
     &                  aij,                                            &
     &                  mapstr,mappnt,ndimbas,mapij,                    &
     &                  powe,                                           & ! depends on pdu and lmax
     &                  kx,ky,kz,                                       &
     &                  edu,pdu,                                        &
     &                  irecdlm,                                        &
     &                  rsnij,ndimrij,                                  &
     &                  np2r,ndimnp,numbrs,naij,                        &
     &                  dqint,ndimdqr,                                  &
     &                  hplnm,ndimrhp,ndimlhp,                          &
     &                  d00,eoeta,                                      &
     &                  eta,xknlat,ndimks,                              &
     &                  conr,nkns,                                      &
     &                  greenks,                                        &
     &                  iprint,istop)
!!DESCRIPTION:
! calculates Green's function, {\tt greenks}, in K-space, 
! order of $ (lmax+1)**2*natom $
!

!!DO_NOT_PRINT
!      use mecca_constants
!      use mtrx_interface
!      use gaunt, only : gauntdlm
      implicit none
!!DO_NOT_PRINT

!!ARGUMENTS:
      integer, intent(in) :: lmax,natom
      integer, intent(in) :: ndimbas,naij
      real(8), intent(in) :: aij(3,naij)
      integer, intent(in) :: mapstr(ndimbas,natom),mappnt(ndimbas,natom)
      complex(8), intent(in) :: powe(*)
      real(8), intent(in) :: kx
      real(8), intent(in) :: ky
      real(8), intent(in) :: kz
      complex(8), intent(in) :: edu
      complex(8), intent(in) :: pdu
      integer, intent(in) :: irecdlm
      integer, intent(in) :: ndimrij,ndimnp,ndimdqr
      real(8), intent(in) :: rsnij(ndimrij,4)
      integer, intent(in) :: mapij(2,naij)
      integer, intent(in) :: np2r(ndimnp,*),numbrs(*)
      complex(8), intent(in) :: dqint(ndimdqr,*)
      integer, intent(in) :: ndimrhp,ndimlhp,ndimks
      complex(8), intent(in) :: hplnm(ndimrhp,ndimlhp)
      complex(8), intent(in) :: d00
      complex(8), intent(in) :: eoeta
      real(8), intent(in) :: eta
      real(8), intent(in) :: xknlat(ndimks,3)
      complex(8), intent(in) :: conr(*)
      integer, intent(in) :: nkns
      complex(8), intent(out) :: greenks(*)
      integer, intent(in) :: iprint
      character(10), intent(in) :: istop
!
!!REVISION HISTORY:
! Adapted - A.S. - 2013
!EOP
!
!BOC
!
      real(8), allocatable :: rsn(:,:)  ! (iprsn,3)
      real*8      dotka
!c
      complex(8), allocatable :: dqint1(:,:) ! (iprsn,2*lmax+1)
      complex(8), allocatable :: dlm(:) ! ((2*lmax+1)**2)
      complex(8), allocatable :: g(:)   ! kkrsz*kkrsz
      complex*16   expdot
      real(8) :: expdot_(2)
      equivalence (expdot,expdot_)

!      parameter    (iplhp=(2*lmax+1)*(iplmax+1))
      complex(8), allocatable :: hplnm2(:,:)  !  (iprsn,(2*lmax+1)**2)  ! (iprsn,iplhp)

      integer :: i,i0,i1,i2,ia,j0,nrsnij,irdlm
      integer :: kkrsz
!c
      character(10), parameter :: sname='gf0_ks'
      real(8), parameter :: pi4 = four*pi
      complex(8), parameter :: sqrtm1=(0.d0,1.d0)
!c
!c     ******************************************************************
!c     full matrix storage version.........bg & gms [Nov 1991]...........
!c     improved ewald & real-space algorithms .... ddj & was April 1994
!c           (factor of 6 speed-up per energy and reduced storage)
!c     found origin of symmetry violation in d.o.s., esp. w/ real-space
!c     hankel fct converges poorly in complex plane for system w/ basis
!c     (next step: reduce tau storage by using group theory (KTOP storage !)
!c     ....ddj & was April 1994
!c     ******************************************************************


!  pdu -- has to be NON-relativistic for single-scattering (?)

!  dqint         : either a hankel fct in real-space calculation
!                  or a real-space Ewald integral from INTFAC
!c     ==================================================================

       kkrsz = (lmax+1)**2
       allocate(dlm((2*lmax+1)**2))
       allocate(g(kkrsz*kkrsz))
       allocate(hplnm2(maxval(numbrs(1:naij)),(2*lmax+1)**2))
       allocate(dqint1(maxval(numbrs(1:naij)),2*lmax+1))
       allocate(rsn(maxval(numbrs(1:naij)),3))
!
!c     =================================================================
!c     for each k-point set up Tau*(-1)=[t*(-1)-G(l,lp)] and invert.....

!DEBUGPRINT DELETE
!!DELETE       if ( abs(kx-0.375d0)<1.d-10 ) then 
!!DELETE       if ( abs(ky-0.375d0)<1.d-10 ) then 
!!DELETE       if ( abs(kz-0.375d0)<1.d-10 ) then 
!       if ( abs(kx-(-0.25d0))<1.d-10 ) then
!       if ( abs(ky-(-0.5773502692D+00))<1.d-10 ) then
!       if ( abs(kz-0.1530931241D+00)<1.d-10 ) then
!
!         if ( iprint < 0 ) then
!          j0 = abs(iprint)
!          write(j0,*) '+++++',istop
!          write(j0,*) lmax,natom,' lmax,natom'
!          write(j0,*) ndimbas,naij,' ndimbas,naij'
!          write(j0,*) real(kx),real(ky),real(kz),' kx,ky,kz'
!          write(j0,*) edu,' edu'
!          write(j0,*) pdu,' pdu'
!          write(j0,*) irecdlm,' irecdlm'
!          write(j0,*) ndimrij,ndimnp,ndimdqr,' ndimrij,ndimnp,ndimdqr'
!          write(j0,*) ndimrhp,ndimlhp,ndimks,' ndimrhp,ndimlhp,ndimks'
!          write(j0,*) d00,' d00'
!          write(j0,*) eoeta,' eoeta'
!          write(j0,*) eta,' eta'
!          write(j0,*) nkns,' nkns'
!!!!!!!!          rewind(j0)
!!      integer, intent(in) :: mapstr(ndimbas,natom),mappnt(ndimbas,natom)
!!      complex(8), intent(in) :: powe(*)
!!      real(8), intent(in) :: aij(3,naij)
!!      real(8), intent(in) :: rsnij(ndimrij,4)
!!      integer, intent(in) :: mapij(2,naij)
!!      integer, intent(in) :: np2r(ndimnp,*),numbrs(*)
!!      complex(8), intent(in) :: dqint(ndimdqr,*)
!!      complex(8), intent(in) :: hplnm(ndimrhp,ndimlhp)
!!      real(8), intent(in) :: xknlat(ndimks,3)
!!      complex(8), intent(in) :: conr(*)
!!      complex(8), intent(out) :: greenks(*)
!         end if
!       end if
!       end if
!       end if
!!DEBUGPRINT DELETE

         ia = 1
         nrsnij = numbrs(ia)
         irdlm = irecdlm

!DEBUG         call begintiming("g0 // make r^l Ylm")
         call rlylmb(ia,hplnm,ndimrhp,                                  &
     &               nrsnij,np2r(1,ia),                                 &
     &               aij(1,ia),rsnij,ndimrij,rsn,size(rsn,1),           &
     &               edu,pdu,eta,irdlm,                                 &
     &               dqint,dqint1,ndimdqr,                              &
     &               lmax,hplnm2,size(hplnm2,1),ndimlhp)
!DEBUG         call endtiming()

         if(irdlm.eq.0) then
!c           ===========================================================
!c           site diagonal dlm ewald calculation........................
!c           -----------------------------------------------------------
            call ewald(kx,ky,kz,                                        &
     &            edu,eoeta,powe,lmax,aij(1,ia),                        &
     &            dlm,eta,                                              &
     &            hplnm2,size(hplnm2,1),ndimlhp,                        &
     &            xknlat,ndimks,conr,                                   &
     &            rsn,nrsnij,size(rsn,1),                               &
     &            dqint1,size(dqint1,1),                                &
     &            nkns,d00,ia)
!c           -----------------------------------------------------------
         else
!c           dlm real space calculation.................................
!c           -----------------------------------------------------------
            call makedrs(kx,ky,kz,pdu,lmax,                             &
     &            rsn,nrsnij,size(rsn,1),                               &
     &            hplnm2,size(hplnm2,1),                                &
     &            dqint1,size(dqint1,1),                                &
     &            ia,dlm)
!c           -----------------------------------------------------------
         endif

!
!         call zerooutC(g,kkrsz*kkrsz)
!
!c        ==============================================================
!c        calculate g(l,lp).............................................

!DEBUG         call begintiming("g0 // sum(gaunt*dlm)")

         call gauntdlm(lmax,dlm,dcmplx(pi4,zero),g)

!         do i=1,kkrsz*kkrsz
!          g(i) = pi4*cfac(i)*sum(cgaunt(i,1:nj3(i))*                    &
!     &                           dlm(ij3(i,1:nj3(i))))
!!            do k=1,nj3(i)
!!               g(i)=g(i)+cgaunt(i,k)*dlm(ij3(i,k))
!!            enddo
!!            g(i) = pi4*cfac(i) * g(i)
!         enddo
!DEBUG         call endtiming()

!c                  cfac(i|l1,l2) <==> (0.d0,1d0)**(l1-l2)

         if(iprint.ge.7) then
           write(6,'(1x,a)') sname//':  Bllp (diagonal)'
!c          call wrtmtx(g,kkrsz,istop)
           call wrtdia2(reshape(g(1:kkrsz*kkrsz),[kkrsz,kkrsz,1]),      &
     &                                                    kkrsz,1,istop)
         endif
!c

!DEBUG         call begintiming("g0 // post arithmetic")
         do i=1,kkrsz*kkrsz,kkrsz+1
            g(i) = g(i)+pdu*sqrtm1
         enddo
!DEBUG         call endtiming()

         if(iprint.ge.7) then
           write(6,'(1x,a)') sname//':  gllp (diagonal)'
!c          call wrtmtx(g,kkrsz,istop)
           call wrtdia2(reshape(g(1:kkrsz*kkrsz),[kkrsz,kkrsz,1]),      &
     &                                                    kkrsz,1,istop)
         endif
!c        set up G ......................................................

         greenks(1:(kkrsz*natom)**2) = 0
         expdot = one
!DEBUG           call begintiming("g0 // post arithmetic")
         do i=1,natom
             call matrix(reshape(g(1:kkrsz*kkrsz),[kkrsz,kkrsz]),       &
     &                        greenks,i,i,kkrsz*natom,kkrsz,expdot)
!c           ------------------------------------------------------------
         enddo
!DEBUG           call endtiming()

!c        For site off-diagonal part of Tau*(-1).........................

         do ia= 2,naij

!         call dumptimings()

         i1 = mapij(1,ia)
         i2 = mapij(2,ia)

         if(i1.ne.i2) then

          nrsnij = numbrs(ia)
          irdlm = irecdlm

!DEBUG          call begintiming("g0 // make r^l Ylm")
          call rlylmb(ia,hplnm,ndimrhp,                                 &
     &                nrsnij,np2r(1,ia),                                &
     &                aij(1,ia),rsnij,ndimrij,rsn,size(rsn,1),          &
     &                edu,pdu,eta,irdlm,                                &
     &                dqint,dqint1,ndimdqr,                             &
     &                lmax,hplnm2,size(hplnm2,1),ndimlhp)
!DEBUG          call endtiming()


          if(irdlm.eq.0) then
!c           ===========================================================
!c           site diagonal dlm ewald calculation........................
!c           -----------------------------------------------------------
            call ewald(kx,ky,kz,                                        &
     &            edu,eoeta,powe,lmax,aij(1,ia),                        &
     &            dlm,eta,                                              &
     &            hplnm2,size(hplnm2,1),ndimlhp,                        &
     &            xknlat,ndimks,conr,                                   &
     &            rsn,nrsnij,size(rsn,1),                               &
     &            dqint1,size(dqint1,1),                                &
     &            nkns,d00,ia)
!c           -----------------------------------------------------------
          else
!c           dlm real space calculation.................................
!c           -----------------------------------------------------------
            call makedrs(kx,ky,kz,pdu,lmax,                             &
     &            rsn,nrsnij,size(rsn,1),                               &
     &            hplnm2,size(hplnm2,1),                                &
     &            dqint1,size(dqint1,1),                                &
     &            ia,dlm)
!c           -----------------------------------------------------------
          endif


                  dotka = kx*aij(1,ia) +                                &
     &                  ky*aij(2,ia) +                                  &
     &                  kz*aij(3,ia)
                  expdot=exp(dcmplx(zero,dotka))

!
                  call zerooutC(g,kkrsz*kkrsz)
!
!c                 ======================================================
!c                 calculate g(l,lp).....................................
!DEBUG                  call begintiming("g0 // sum(gaunt*dlm)")

         call gauntdlm(lmax,dlm,dcmplx(pi4,zero),g)

!          do i=1,kkrsz*kkrsz
!           g(i) = pi4*cfac(i)*sum(cgaunt(i,1:nj3(i))*                   &
!     &                            dlm(ij3(i,1:nj3(i))))
!          enddo
!!                  do i=1,kkrsz*kkrsz
!!                     do k=1,nj3(i)
!!                        g(i)=g(i)+cgaunt(i,k)*dlm(ij3(i,k))
!!                     enddo
!!                  enddo
!!                  do i=1,kkrsz*kkrsz
!!                     g(i) = pi4*cfac(i) * g(i)
!!                  enddo
!DEBUG                  call endtiming()

             if( iprint .ge. 7 ) then
           write(6,'(1x,a,2i3)')                                        &
     &                        sname//': gllp (off-diag)  i1,i2=',i1,i2
!c           call wrtmtx(g,kkrsz,istop)
           call wrtdia2(reshape(g(1:kkrsz*kkrsz),[kkrsz,kkrsz,1]),      &
     &                                                    kkrsz,1,istop)
             endif

!DEBUG             call begintiming("g0 // post arithmetic")
             call matrix(reshape(g(1:kkrsz*kkrsz),[kkrsz,kkrsz]),       &
     &                          greenks,i1,i2,kkrsz*natom,kkrsz,expdot)
!DEBUG             call endtiming()
         end if
         enddo

!DEBUG         call begintiming("g0 // copy operations")
         do i1= 1,natom
          do i2= 1,natom
           if(i1.ne.i2) then
!CAB            if(mappnt(i1,i2).eq.0) then
            if(mappnt(i1,i2).ne.1) then
             i0 = mapij(1,mapstr(i1,i2))
             j0 = mapij(2,mapstr(i1,i2))
             call cpmatrz(greenks,i1,i2,i0,j0,kkrsz*natom,kkrsz)
            end if
           endif
          enddo
         enddo
!DEBUG         call endtiming()

       deallocate(rsn)
       deallocate(dqint1)
       deallocate(hplnm2)
       deallocate(g)
       deallocate(dlm)

!c     ==================================================================
      if (istop.eq.sname) call fstop(sname)

      return
!EOC
      end subroutine gf0_ks

!BOP
!!IROUTINE: ewald
!!INTERFACE:
      subroutine ewald(x,y,z,e,eoeta,powe,lmax,aij,                     &
     &                 dlm,eta,                                         &
     &                 hplnm,ndimr,ndimlm,                              &
     &                 xknlat,ndimks,conr,                              &
     &                 rns,nrns,ndimrs,dqint,ndimdqr,                   &
     &                 nkns,d00,icount)
!!DESCRIPTION:
! calculates for point {\bf k}={\tt (x,y,z)} various data required 
! for Ewald summation method \\

! (part of free-electon Green's function calculations)
!
!!REVISION HISTORY:
! Adapted - A.S. - 2013
!EOP
!
!BOC
!
!      use universal_const
      implicit none
      character(10), parameter :: sname='ewald'
!c     ==================================================================
      integer l
      integer icount              ! icount -- instead of (i1,i2)
      integer lmax
      integer nkns
      integer nrns
!c     ==================================================================
      integer, intent(in) :: ndimr,ndimlm,ndimks,ndimrs,ndimdqr
      real*8 aij(3)
      complex*16 conr((2*lmax+1)**2)
      real*8 rns(ndimrs,*)
      real*8 xknlat(ndimks,*)
      real*8 eta
      real*8 x
      real*8 y
      real*8 z
!c     ==================================================================
!CAB      complex*16 ylmkpkn(ipxkns,ipdlj)
      complex*16, allocatable  ::  ylmkpkn(:,:)
      integer erralloc
      integer :: l0,lm,m,nd,nk

      complex*16 dlmr((2*lmax+1)**2)
      complex*16 dlmk((2*lmax+1)**2)

      complex*16 dlm((2*lmax+1)**2)
      complex*16 powe((2*lmax+1)**2)
      complex*16 dqint(ndimdqr,*)
      complex*16 d00
      complex*16 e
      complex*16 eoeta

      complex*16 hplnm(ndimr,ndimlm)

      complex*16 expkrn(nrns)
      complex*16 dqexp(nrns)

      real(8) :: dot1
      real(8) :: etainv
      real*8 xkpkn2(nkns)
      complex*16 tmparr(nkns)
!c     ==================================================================
!c     Calculate the r-space part of dlm.
!c     Modified to reduce time of ewald method......ddj & was April 1994
!c     (see GETTAU)
!c     ==================================================================

!c     ================================================================== !=
!c     Calculate  exp(i*k*rs) for real-space translation vectors.
!c                    k=(x,y,z), rs==rns()-aij
!c     ================================================================== !=

!DEBUG      call begintiming("g0 // make real dlm")
      expkrn(1:nrns) = exp(dcmplx(zero,                                 &
     &               x*rns(1:nrns,1)+y*rns(1:nrns,2)+z*rns(1:nrns,3)))

      lm = 0
      do l=0,2*lmax
       l0 = l*( l + 1 ) + 1
!c  M=0
       lm = lm+1
       dqexp(1:nrns) = dqint(1:nrns,l+1)*expkrn(1:nrns)
       dlmr(l0)=sum(dqexp(1:nrns)*hplnm(1:nrns,lm))*conr(l0)
!c  M<>0
       do m=1,l
        lm = lm+1
        dlmr(l0+m)=sum(dqexp(1:nrns)*hplnm(1:nrns,lm))*conr(l0)
!        dlmr(l0-m)=sum(dqexp(1:nrns)*conjg(hplnm(1:nrns,lm)))*conr(l0)*(-1)**m
        dlmr(l0-m)=dot_product(hplnm(1:nrns,lm),dqexp(1:nrns))*conr(l0)*&
     &             (-1)**mod(m,2)
       enddo
      enddo
!DEBUG      call endtiming()

!c     ==================================================================
!c     Calculate the k-space part of dlm.
!c     ==================================================================


!DEBUG      call begintiming("g0 // allocate k-space Ylm")
      allocate(                                                         &
     &         ylmkpkn(1:nkns,1:(2*lmax+1)**2),                         &
     &  stat=erralloc)

      if(erralloc.ne.0) then
        write(6,*) ' NKNS=',nkns
        write(6,*) ' (2*LMAX+1)**2=',(2*lmax+1)**2
        write(6,*) ' ERRALLOC=',erralloc
        call fstop(sname//' :: ALLOCATION MEMORY PROBLEM ')
      end if
!DEBUG      call endtiming()

!DEBUG      call begintiming("g0 // fill-in k-space Ylm")
      call hrmplnm( x, y, z, xknlat, ndimks, xkpkn2, ylmkpkn, nkns,     &
     &               1, nkns, 2*lmax , 1)
!c     ==================================================================
!DEBUG      call endtiming()

!DEBUG      call begintiming("g0 // make k-space dlm")
      etainv = one/eta
      do nk=1,nkns
        dot1 = dot_product(xknlat(nk,1:3),aij(1:3))
        tmparr(nk) = eoeta*                                             &
     &     exp(dcmplx(-xkpkn2(nk)*etainv,dot1))/(xkpkn2(nk)-e)
      enddo

!c     ==================================================================
!c     calculate dlmk
!c     ==================================================================
      do nd=1,(2*lmax+1)**2
       dlmk(nd) = sum(tmparr(1:nkns)*ylmkpkn(1:nkns,nd))
      enddo
!DEBUG      call endtiming()
!c     ==================================================================
!c     If  the calculation of dlm is on a diagonal sublattice then
!c     add in d00
!c     ==================================================================

!DEBUG      call begintiming("g0 // get total dlm")
      if( icount.eq.1 ) then
        dlmk(1)=dlmk(1) + d00
      endif
!c     ==================================================================
!c     Calculate total dlm by appropiately summing over
!c     k and r space parts
!c     ==================================================================
      do l=1,(2*lmax+1)**2
        dlm(l)=powe(l)*( dlmk(l)+dlmr(l) )
      enddo
!DEBUG      call endtiming()

      deallocate(ylmkpkn,stat=erralloc)

        return
!EOC
        end subroutine ewald

!BOP
!!IROUTINE: rlylmb
!!INTERFACE:
      subroutine rlylmb(ia,hplnm,ndimrhp,                               &
     &                  nrsn,np2r,                                      &
     &                  aij,rsnij,ndimrij,rsn,ndimr1,                   &
     &                  edu,pdu,eta0,irecdlm,                           &
     &                  dqint,dqint1,ndimdqr,                           &
     &                  lmax,hplnm2,ndimr2,lmhp)
!!DESCRIPTION:
! calculates real-space harmonical polinomials $ R^l Y_{lm}(R), m \leq 0 $
! for multi-site cell and specific energy 
! {\bv
!    inputs: ia,hplnm, ..., lmax
!            (ndim...  -- dimensions)
!    output: hplnm2
! \ev}
! (part of free-electon Green's function calculations)
!
!!REVISION HISTORY:
! Adapted - A.S. - 2013
!EOP
!
!BOC
!      use universal_const
      implicit none

!c     ================================================================== !=

      integer ia,ndimrhp,ndimrij,ndimr1,ndimr2,lmax,lmhp,ndimdqr

      complex*16 hplnm(ndimrhp,*)
      integer    np2r(*),nrsn
      real*8     aij(3)
      real*8     rsnij(ndimrij,3)
      real*8     rsn(ndimr1,3)
      complex*16   edu,pdu
      real*8       eta0
      integer      irecdlm
      complex*16 dqint(ndimdqr,*)
      complex*16 dqint1(ndimr2,*)
      complex*16 hplnm2(ndimr2,lmhp)

      complex*16 ylmrl((lmax+1)*(lmax+1))
      real*8 rns(3),rns2(1),rsq2
      integer i,lm,l,m,l0,ir,lmx2p1

      complex*16 xpr
      real*8 rsp

!CAB      integer isave/0/
!CAB      save isave

       if(ia.le.ndimrhp) then
!       if input rsnij = Rn + aij, then just collect
!         vectors + sphericals harm. into a single array for this atom
        do i=1,nrsn
         ir = np2r(i)
         rsn(i,1:3) = rsnij(ir,1:3)
         hplnm2(i,1:lmhp) = hplnm(ir,1:lmhp)
        end do
       else
!        if input rsnij = Rn, then we need to construct Rn + aij
!           into a list and build spherical harm.
        do i = 1,nrsn
         ir = np2r(i)
         rsn(i,1:3) = rsnij(ir,1:3) + aij(1:3)
         call hrmplnm(zero,zero,zero,rsn(i,1:3),1,rns2,ylmrl,1,         &
     &                   1, 1, 2*lmax , 1)
         lm = 0
         do l=0,2*lmax
            l0 = l*( l + 1 ) + 1
            do m=0,l
             lm = lm+1
             hplnm2(i,lm) = ylmrl(l0+m)
            end do
         end do
        end do
       end if

!CAB       if(isave.lt.ia) then
!CAB        write(90,*) ' RLYLMB::'
!CAB        write(90,*) ia,nrsn,ndimrij,ndimr1,ndimr2
!CAB        write(90,*)
!CAB        write(91,*) ' RLYLMB::'
!CAB        write(91,*) ia,ndimrhp,lmax,lmhp
!CAB        write(91,*)
!CAB        do l=1,nrsn
!CAB         rns2 = rsn(l,1)**2+rsn(l,2)**2+rsn(l,3)**2
!CAB         write(90,1000) (rsn(l,i)/(2*pi),i=1,3),
!CAB     *                   rns2/(2*pi)**2
!CAB         write(91,1000) (hplnm2(l,lm),lm=1,4)
!CAB1000    format(4f12.5,i6,f12.5)
!CAB        end do
!CAB        isave = max(ia,isave)
!CAB       end if

       ! similar to above with regard to input vecs, only this code
       ! deals with the integration fac. for ewald sum or
       ! hankel fn for real space eval.
       if(ia.le.ndimdqr) then
        do l=1,2*lmax+1
         dqint1(1:nrsn,l)=dqint(np2r(1:nrsn),l)
        end do
       else
        lmx2p1 = 2*lmax+1
        rsp = -2.d0
        do i = 1,nrsn
         ir = np2r(i)
         rns(1:3) = rsnij(ir,1:3) + aij(1:3)
         rsq2 =  dot_product(rns,rns)
         if( abs(rsq2-rsp) .ge. 1.0d-6 ) then
          rsp = rsq2
          if(rsp<1.d-12) irecdlm = 0
          if(irecdlm.eq.0) then
           call intfac(rsq2,edu,eta0,2*lmax,dqint1(i,1:lmx2p1))
          else
!           if(rsp<1.d-12) then
!            dqint1(1,1:lmx2p1) = (0.d0,0.d0)
!            write(6,'(a,2i5,a,E16.8/3(a,3E16.8),a)')                    &
!     &        'WARNING:  ia,ir=',ia,ir,' rsp=',rsp/(pi*2),              &
!     &        ' rns=rsnij-aij=',rns(1:3)/(pi*2),                        &
!     &        ' aij=',aij(1:3)/(pi*2),                                  &
!     &        ' rsnij=',rsnij(ir,1:3)/(pi*2),' hankel is undefined'
!            call fstop('DEBUG')
!           end if
           xpr=pdu*sqrt(rsp)
           call hankel(xpr,dqint1(i,1:lmx2p1),2*lmax)
          end if
         else
          dqint1(i,1:lmx2p1)=dqint1(i-1,1:lmx2p1)
         endif
        end do
       end if

      return
!EOC
      end subroutine rlylmb
!BOP
!!IROUTINE: makedrs
!!INTERFACE:
      subroutine makedrs(xk1,xk2,xk3,                                   &
     &                   pdu,                                           &
     &                   lmax,                                          &
     &                   rsn,nrsn,ndimrs,                               &
     &                   hplnm,ndimr,                                   &
     &                   hank,ndimhnr,                                  &
     &                   icount,                                        &
     &                   dlm)
!!DESCRIPTION:
! calculates D$_{lm}$'s in using real space sum
! {\bv
!   dlm() = sum( pdu*exp( i xk*rsn )*hank()*conjg( Ylm() ) )
! \ev}

!!DO_NOT_PRINT
!      use mecca_constants
      implicit none
!!DO_NOT_PRINT

!!ARGUMENTS:
      real*8      xk1,xk2,xk3
      complex*16  pdu
      integer     lmax
      integer     ndimrs,nrsn
      real*8      rsn(ndimrs,3)
      integer     ndimr
      complex*16  hplnm(ndimr,*)
      integer     ndimhnr
      complex*16  hank(ndimhnr,*)
      integer     icount
      complex*16  dlm((2*lmax+1)**2)
!!REVISION HISTORY:
! Adapted - A.S. - 2013
!EOP
!
!cab      complex*16  ylm(iprsn,ipdlj)
!BOC
      integer :: ir,l0,lm,l,m,nstart
      real*8      rtfpi
      real(8) :: onem,raij1,raij2,raij3,rir,rl
!c
      complex*16  dot
      complex*16  xfac
      complex*16  dlmp,dlmm
      complex*16  raijh(nrsn,2*lmax+1)

!c parameter
      complex*16  sqrtm1
      parameter (sqrtm1=(0.d0,1.d0))

      rtfpi=sqrt(four*pi)

!c     -------------------------------------------------------------
      call zerooutC(dlm,(2*lmax+1)**2)
!c     -------------------------------------------------------------
      if(icount.eq.1) then
        nstart=2
        dlm(1)=-sqrtm1*pdu/rtfpi
      else
        nstart=1
      endif
!c     -------------------------------------------------------------
      do ir=nstart,nrsn
        raij1=rsn(ir,1)
        raij2=rsn(ir,2)
        raij3=rsn(ir,3)
        dot=dcmplx( zero,xk1*raij1+xk2*raij2+xk3*raij3 )
        xfac=pdu*exp(dot)
        rir = one/sqrt(raij1*raij1+raij2*raij2+raij3*raij3)
        rl = one
        do l=0,2*lmax
         raijh(ir,l+1) = rl*xfac*hank(ir,l+1)
!c
!c                hplnm = r**l*Ylm, therefore raijh() contains 1/r**l
!c
         rl = rl*rir
        end do
      end do

!CAB          dlm(nd)=dlm(nd) + xfac*hank(ir,ndx(nd))*conjg( ylm(ir,nd) )
      lm = 0
      do l=0,2*lmax
       l0 = l*( l + 1 ) + 1
!c  M=0
       lm = lm+1
       dlmp = dcmplx(zero,zero)
       do ir=nstart,nrsn
          dlmp=dlmp + raijh(ir,l+1)*hplnm(ir,lm)
       enddo
       dlm(l0)=dlm(l0)+dlmp
!c  M<>0
       do m=1,l
        lm = lm+1
        dlmp = dcmplx(zero,zero)
        dlmm = dcmplx(zero,zero)
        onem = (-1)**m

        do ir=nstart,nrsn
          dlmp=dlmp + raijh(ir,l+1)*hplnm(ir,lm)
          dlmm=dlmm + raijh(ir,l+1)*conjg(hplnm(ir,lm))*onem
        enddo
        dlm(l0+m) = dlmp
        dlm(l0-m) = dlmm
       enddo
      enddo

      return
!EOC
      end subroutine makedrs
!
!BOP
!!IROUTINE: gf0_rs
!!INTERFACE:
      subroutine gf0_rs(lmax,                                           &
     &                  pdu,                                            &
     &                  green,                                          &
     &                  ndimnn,numnbi,                                  &
     &                  rsij,mapsnni,                                   &
     &                  invsw,                                          &
     &                  istop)
!!DESCRIPTION:
! calculates free electron Greens function, {\tt green},
! in real space in cluster approximation
! {\bv
!  Iatom -- central atom
!  numnbi -- 1 + number of NN for Iatom
!  green -- output G-matrix
!  rsij  -- array for inequivalent vectors (Rj-Rj`); rsij(*,1) = (0,0,0)
!
!  invsw.ne.2 :   green -- (*,*)-complex matrix of (Re,Im)
!       .eq.2 :   green -- (0:1,*,*) - real matrix
!                   green(0,*) -- Re{matrix}
!                   green(1,*) -- Im{matrix}
! \ev}

!!DO_NOT_PRINT
!      use universal_const
!      use gaunt, only : gauntdlm
      implicit none
!!DO_NOT_PRINT

!!ARGUMENTS:
      integer, intent(in) :: lmax
      complex(8), intent(in) :: pdu
      complex(8), intent(out) :: green(*) ! output matrix (1:numnbi*kkrsz,1:numnbi*kkrsz)
      integer, intent(in) :: ndimnn,numnbi
      real(8), intent(in) :: rsij(3,*)
      integer, intent(in) :: mapsnni(ndimnn,numnbi)
      integer, intent(in) :: invsw
      character(10), intent(in) :: istop
!!REVISION HISTORY:
! Adapted - A.S. - 2013
!EOP
!
!BOC
!c
      real*8 rsij2(1)

!c     ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
      character    sname*10
      parameter    (sname='gf0_rs')
!c     ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

!      integer lclkkr,lcl2
!      parameter (lclkkr=(iplmax+1)*(iplmax+1))
!      parameter (lcl2=(2*iplmax+1))

!c  complex*16   ylmtmp((2*iplmax+1)**2)    -- working array
      complex*16   ylmtmp((2*lmax+1)*(2*lmax+1))

!c  complex*16   hnkltmp(2*iplmax+1)    -- working array
      complex*16   hnkltmp(0:(2*lmax+1)-1)

!c  working arrays
      complex*16 g((lmax+1)**2*(lmax+1)**2)
      complex*16 dlm((2*lmax+1)*(2*lmax+1))

!c      complex*16   xpr

      integer i0,j0,l,l0,m,ia,kkrsz,kkrsz2

!c      real*8 time1,time2
!      real*8 zero
!      parameter (zero=0.d0)
!      complex*16 cone,sqrtm1
!      parameter (cone=(1.d0,0.d0))
      real(8), parameter :: pi4 = four*pi


!c     =================================================================
!c     for each E-point and each Iatom set up in Real-Space
!c           free-electron Greens function - green = g0
!c     =================================================================

      kkrsz = (lmax+1)**2
      kkrsz2 = kkrsz*kkrsz

!      if(kkrsz.gt.lclkkr) then
!       write(6,*) ' KKRSZ=',kkrsz,' LCLKKR=',lclkkr
!       call fstop(sname//' kkrsz > lclkkr; increase lclkkr...')
!      end if

!c
      call zerooutC(green,(kkrsz*numnbi)**2)

!c  Diagonal blocks: G0(L1,i1|L2,i1) = 0 for any L1,L2

!c  --------------------------------------------------------------------

!c        For site off-diagonal part    ................................. !.

      do i0=1,numnbi
       do j0=1,numnbi
        if(i0.ne.j0) then
         ia = mapsnni(i0,j0)

!c  dlm real space calculation...
!c  --------------------------------------------------------------------

         call hrmplnm(zero,zero,zero,rsij(:,ia),1,rsij2,ylmtmp,1,       &
     &              -10,1,2*lmax,1)

!c
!c ylmtmp = Ylm
!c
         call hankel(pdu*sqrt(rsij2(1)),hnkltmp(0),2*lmax)

!c        lm = 0
         do l=0,2*lmax
          l0 = l*( l + 1 ) + 1
!c  M=0
          dlm(l0)=hnkltmp(l)*ylmtmp(l0)
!c  M<>0
          do m=1,l
           dlm(l0+m) = hnkltmp(l)*(ylmtmp(l0+m))
           dlm(l0-m) = hnkltmp(l)*(ylmtmp(l0-m))
          enddo
         enddo
!c
!c  calculate g(l,lp)  ...................................
!c

         call gauntdlm(lmax,dlm,pdu*pi4,g)

!         do i=1,kkrsz2
!          g(i) = pdu*pi4*cfac(i)*sum(cgaunt(i,1:nj3(i))*                &
!     &                           dlm(ij3(i,1:nj3(i))))
!         enddo

         if(invsw.eq.2) then
          call matrix1(g,green,i0,j0,kkrsz*numnbi,kkrsz,cone)
         else
          call matrix(g,green,i0,j0,kkrsz*numnbi,kkrsz,cone)
         end if
        end if
       enddo
      enddo
!c
      if (istop.eq.sname) call fstop(sname//' DEBUG')
!c
      return
!EOC
      end subroutine gf0_rs

      end module freespaceG 
