!BOP
!!ROUTINE: update_ptn
!!INTERFACE:
      subroutine update_ptn(fold,                                       &
     &                      fnew,                                       &
     &                     ztot,v0imtz,v0iold,rr,                       &
     &                     imix,alpha0,beta,                            &
     &                     komp,jend,nbasis,                            &
     &                     mspn,brfile1,brfile2                         &
     &                     )
!c    ===============================================================
!!DESCRIPTION:
!  potential (simple or Broyden) mixing
!
!!USES:
      use universal_const
      use gfncts_interface, only : gMaxErr
      use mecca_interface
      use scf_io, only : sCurrParams,pot_B_mix,pot_S_mix

!!DO_NOT_PRINT
      implicit none
!!DO_NOT_PRINT

!!ARGUMENTS:
      real(8)             :: fold(:,:,:,:)  ! (iprpts,ipcomp,ipsublat,ipspin) old potential
      real(8)             :: fnew(:,:,:,:)  ! (iprpts,ipcomp,ipsublat,ipspin) new potential
      real(8), intent(in) ::   ztot(:,:)  ! (ipcomp,ipsublat)
      real(8), intent(inout) ::   v0imtz(:)
      real(8), intent(in)    ::   v0iold(:)
      real(8), intent(in) :: rr(:,:,:)      ! (iprpts,ipcomp,ipsublat)
      integer, intent(in) :: imix           ! mix with Broyden or not
      real(8), intent(in) :: alpha0,beta
      integer, intent(in) :: nbasis
      integer, intent(in) :: komp(nbasis)
      integer, intent(in) :: jend(nbasis)   ! last point with non-zero fnew/fold
      integer, intent(in) :: mspn
      character(*), intent(in) :: brfile1,brfile2
!!REVISION HISTORY:
! Initial Version - A.S. - 2013
!EOP
!
!BOC
!DEBUG      real(8), intent(in) ::   xvalws(:,:,:)  ! (ipcomp,ipsublat,ipspin)
!DEBUG      real(8) ::   xvalwsnw(:,:,:)! (ipcomp,ipsublat,ipspin)

!      real(8) :: xnjtop(size(xvalws,1),size(xvalws,2),size(xvalws,3))
!      real(8) :: xojtop(size(xvalws,1),size(xvalws,2),size(xvalws,3))
      integer :: ib,ik,is,jtop,ndrpts
      integer :: mxcomp,lastit
      real(8) :: alpha,amix,bmix
      logical :: simplmx_for_v0

      ndrpts = size(fnew,1)
      alpha = alpha0
      bmix = beta
      jtop = maxval(jend(1:nbasis))
      simplmx_for_v0 = jtop.ge.ndrpts .or. imix==0   !DEBUG
      if ( simplmx_for_v0 ) then
        jtop = ndrpts
      end if  
      do is = 1,mspn
       do ib=1,nbasis
        do ik=1,komp(ib)
          fnew(jend(ib)+1:ndrpts,ik,ib,is) = zero
          fold(jend(ib)+1:ndrpts,ik,ib,is) = zero
          if ( abs(ztot(ik,ib)) < one ) then
           call convertVr1(jtop,zero,rr(:,ik,ib),                       &
     &                                              fnew(:,ik,ib,is),1)
           call convertVr1(jtop,zero,rr(:,ik,ib),                       &
     &                                              fold(:,ik,ib,is),1)
           if ( .not. simplmx_for_v0 ) then
            fnew(jtop+1,ik,ib,is) = v0imtz(is)
            fold(jtop+1,ik,ib,is) = v0iold(is)
           end if
          else
           call convertVr0(jtop,ztot(ik,ib),rr(:,ik,ib),                &
     &                                              fnew(:,ik,ib,is),1)
           call convertVr0(jtop,ztot(ik,ib),rr(:,ik,ib),                &
     &                                              fold(:,ik,ib,is),1)
           if ( .not. simplmx_for_v0 ) then
            fnew(jtop+1,ik,ib,is) = v0imtz(is) * rr(jend(ib),ik,ib)
            fold(jtop+1,ik,ib,is) = v0iold(is) * rr(jend(ib),ik,ib)
           end if
          end if
        enddo
       enddo
      enddo
      if ( .not. simplmx_for_v0 ) then
        jtop = jtop+1
      end if  

       amix = alpha
!c===================================================================
       if(imix.eq.0) then
        call simplmx(fnew,fold,                                         &
     &               komp,nbasis,mspn,alpha,bmix)
        call sCurrParams(imix=pot_S_mix,alpha=alpha,beta=bmix)
       else
!c
!c Broyden for potential
!c
        mxcomp = maxval(komp(1:nbasis))
!c====================================================================
        if ( imix==1 ) then
        call broyden(fnew(1:jtop,1:mxcomp,1:nbasis,1:mspn),             &
     &                fold(1:jtop,1:mxcomp,1:nbasis,1:mspn),            &
     &                              komp,nbasis,                        &
     &                       mspn,jtop,brfile1,brfile2,2,alpha,bmix)
        else if ( imix==3 ) then
         call fstop(' ERROR IN update_ptn.f')
!DELETE        call broyden(fnew(1:jtop,1:mxcomp,1:nbasis,1:1),                &
!DELETE     &                fold(1:jtop,1:mxcomp,1:nbasis,1:1),               &
!DELETE     &                              komp,nbasis,                        &
!DELETE     &                       1,jtop,brfile1,brfile2,2,alpha,zero)
        else
        call broyden(fnew(1:jtop,1:mxcomp,1:nbasis,1:mspn),             &
     &                fold(1:jtop,1:mxcomp,1:nbasis,1:mspn),            &
     &                              komp,nbasis,                        &
     &                       mspn,jtop,brfile1,brfile2,1,alpha,bmix)
        end if
        open(11,file=brfile1,status='unknown',form='unformatted')
        lastit = 1
        read(11,err=10,end=10)amix,lastit
        close(11)
10      continue
!DELETE        if ( imix .ne. 3 ) then
         if ( amix .ne. alpha .or. lastit==1 ) then
          call sCurrParams(imix=pot_S_mix,alpha=amix,beta=bmix)
         else
          call sCurrParams(imix=pot_B_mix,alpha=alpha,beta=bmix)
         end if
!DELETE        else
!DELETE          call sCurrParams(imix=pot_B_mix)
!DELETE        end if
!c====================================================================

!c
       end if
       if ( .not. simplmx_for_v0 ) then
        jtop = jtop-1
       end if
       if ( alpha .ne. zero ) then
        alpha = max(alpha,0.6d0)
       else
        alpha = zero
       end if
       call simplmx1(v0imtz,v0iold,mspn,alpha,beta)
       do is = 1,mspn
       do ib=1,nbasis
        do ik=1,komp(ib)
          if ( .not. simplmx_for_v0 ) then
           fnew(jtop+1,ik,ib,is) = zero
           fold(jtop+1,ik,ib,is) = zero
          end if
          if ( abs(ztot(ik,ib))<one ) then
          call convertVr1(jtop,zero,rr(:,ik,ib),                        &
     &                                              fnew(:,ik,ib,is),0)
          call convertVr1(jtop,zero,rr(:,ik,ib),                        &
     &                                              fold(:,ik,ib,is),0)
          else
          call convertVr0(jtop,ztot(ik,ib),rr(:,ik,ib),                 &
     &                                              fnew(:,ik,ib,is),0)
          call convertVr0(jtop,ztot(ik,ib),rr(:,ik,ib),                 &
     &                                              fold(:,ik,ib,is),0)
          end if
        enddo
       enddo
       enddo
!c===================================================================
!c
!c       write(6,'(//)')

       return

!EOC
       contains

!c =====================================================================
!BOP
!!IROUTINE: simplmx
!!INTERFACE:
        subroutine simplmx(frn,fro,komp,nbasis,nspin,alla,beta)
!!REMARKS:
! private procedure of subroutine update_ptn
!EOP
!
!BOC
        implicit none
!c
        real(8),  parameter :: half=one/two
!c+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!c
!c
        real(8), intent(inout) :: frn(:,:,:,:)  ! (iprpts,ipcomp,ipsublat,ipspin)
        real(8), intent(in)    :: fro(:,:,:,:)  ! (iprpts,ipcomp,ipsublat,ipspin)
        integer, intent(in)    :: nbasis,komp(nbasis),nspin
        real(8), intent(in)    :: alla,beta
        integer  ib,ik,k
        real(8) :: fa(size(fro,1))
        real(8) :: fb(size(fro,1))
        real(8) :: fc(size(fro,1))
        real(8) :: fd(size(fro,1))
!c
!c =====================================================================
!c subroutine to simple mix new and old potentials or charges ........
!c     nbasis    number of sublattices
!c     komp      number of compents/basis
!c     mspn      number of electron spins (1 or 2)
!c     alla      mixing parameter of average potential
!c     beta      mixing parameter of difference in potentials
!c     frn:      new      function(r)
!c     fro:      previous function(r)
!c =====================================================================
!c
        k = size(fro,1)
        do ib=1,nbasis
          do ik=1,komp(ib)
            if ( nspin==1 ) then
             fa = frn(1:k,ik,ib,1)
             fb = fro(1:k,ik,ib,1)
             frn(1:k,ik,ib,1) = (one-alla)*fb+alla*fa
            else
             fa = half*(frn(1:k,ik,ib,1)+frn(1:k,ik,ib,2))
             fb = half*(fro(1:k,ik,ib,1)+fro(1:k,ik,ib,2))
             fc = half*(frn(1:k,ik,ib,1)-frn(1:k,ik,ib,2))
             fd = half*(fro(1:k,ik,ib,1)-fro(1:k,ik,ib,2))
             frn(1:k,ik,ib,1) = (one-alla)*fb+alla*fa
             frn(1:k,ik,ib,2) = frn(1:k,ik,ib,1) -                      &
     &                                         ((one-beta)*fd + beta*fc)
             frn(1:k,ik,ib,1) = frn(1:k,ik,ib,1) +                      &
     &                                         ((one-beta)*fd + beta*fc)
            end if
          end do
        end do
!c
          return
!EOC
          end subroutine simplmx
!c
!BOP
!!IROUTINE: simplmx1
!!INTERFACE:
          subroutine simplmx1(vnew,vold,nsp,alf_in,bet)
!!REMARKS:
! private procedure of subroutine update_ptn
!EOP
!
!BOC
          implicit none
          integer :: nsp
          real(8) :: vold(nsp),vnew(nsp)
          real(8) :: alf_in,alf,bet
          real(8) :: x,vpls,vdif
          real(8), parameter :: half=one/two,max_dV0=0.3d0
          vpls = sum(vnew(1:nsp)-vold(1:nsp))/nsp
          x = abs(vpls)/max_dV0
          if ( x >= one ) then
           alf = alf_in
          else
           alf= alf_in + (one-alf_in)*x**3*(two-x)
          end if
          if ( nsp==1 ) then
           vnew(1)= (one-alf)*vold(1) + alf*vnew(1)
          else
           vdif  = (one-bet)*(vold(2)-vold(1)) + bet*(vnew(2)-vnew(1))
           vpls  = (one-alf)*(vold(2)+vold(1)) + alf*(vnew(2)+vnew(1))
           vnew(1) = half*(vpls - vdif)
           vnew(2) = vnew(1) + vdif
          end if
!EOC
          end subroutine simplmx1
!c
!          
       end subroutine update_ptn

