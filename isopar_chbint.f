!BOP
!!ROUTINE: isopar_volume
!!INTERFACE:
      function isopar_volume(fcount,vj)
!!DESCRIPTION:
! returns polyhedron volume by isoparametric integration

!!DO_NOT_PRINT
      implicit none
      real(8) :: isopar_volume
!!DO_NOT_PRINT

!!ARGUMENTS:
      integer, intent(in) :: fcount      ! number of faces
      real(8), intent(in) :: vj(:,:,:,:) ! jacobian
!!PARAMETERS:
!!REVISION HISTORY:
! Initial Version - A.S. - 2013
! Revised: weights are assumed to be included in jacobian
!EOP
!
!BOC
      isopar_volume = sum(vj(:,:,:,1:fcount))
!      real(8) :: wf
!      integer :: Nq1,Nq2,Nq3,I_face,i,j,k
!      isopar_volume = 0
!      Nq1 = size(vj,1)
!      Nq2 = size(vj,2)
!      Nq3 = size(vj,3)
!      do I_face=1,fcount
!       do k=1,Nq3
!        do j=1,Nq2
!         do i=1,Nq1
!          wF = vJ(i,j,k,I_face)
!          isopar_volume = isopar_volume + wf
!         end do
!        end do
!       end do
!      end do
      return 
!EOC
      end function isopar_volume
!
!BOP
!!ROUTINE: isopar_chbint
!!INTERFACE:
      subroutine isopar_chbint(ia,rmt_ovlp,nmx,rrs,fs,                  &
     &                        r_circ,fcount,weight,rmag,vj,VPInt)
!!DESCRIPTION:
! isoparametric Chebyshev integration for real function
! $ 
! \quad \int\limits_{R_{MT}}^{VP} \frac{fs(r)}{4 \pi r^{ia}} \quad  \mathrm{d^3}r 
! $
!

!!USES:
      use mecca_constants, only : max_isopoly,pi
      use chbint_interface, only : chebev

!!DO_NOT_PRINT
      implicit none
!!DO_NOT_PRINT

!!ARGUMENTS:
       integer :: ia
       real(8) :: rmt_ovlp
       integer :: nmx
       real(8) :: rrs(nmx),fs(nmx)
       real(8) :: r_circ
       integer :: fcount
       real(8) :: weight(:)      ! (MNqp)
       real(8) :: rmag(:,:,:,:)  ! (MNqp,MNqp,MNqp,MNF)
       real(8) :: vj(:,:,:,:)    ! (MNqp,MNqp,MNqp,MNF)
       real(8) ::  VPInt(2)
!!REVISION HISTORY:
! Initial version - A.S. - 2013
!EOP
!
!BOC
       integer :: Ni,Nfn,Ndata
        integer :: i,j,k,I_face
        real(8) :: coeff(max_isopoly+1)
!----------------------------------------------------------------------
        integer :: Nq1,Nq2,Nq3
        real(8) :: func,rmagn,wF,errchf
        integer :: norder
        integer, external :: indRmesh
!        real(8), external :: chebev
        VPInt=0.0D0

        !---[driver for polynomial fitting]---
        Ni = indRmesh(rmt_ovlp,nmx,rrs)
        if ( abs(rmt_ovlp-rrs(Ni+1))/rmt_ovlp<1.d-7 ) Ni = Ni+1
        Nfn = min(nmx,1+indRmesh(r_circ,nmx,rrs))
        Ndata = Nfn - Ni + 1
        call chbfit(rrs(Ni:Nfn),fs(Ni:Nfn),                             &
     &            1.d0/rrs(Ni:Nfn),Ndata,                               &
     &            coeff,size(coeff,1),norder,1.d-7,errchf)

        coeff = coeff/(4.d0*pi)
        Nq1 = size(vj,1)
        Nq2 = size(vj,2)
        Nq3 = size(vj,3)
        do 200 I_face=1,fcount
         do 100 k=1,Nq3
         do 100 j=1,Nq2
         do 100 i=1,Nq1
          wf = vj(i,j,k,I_face)
          if ( abs(wf) < tiny(1.d0) ) cycle
          rmagn=rmag(i,j,k,I_face)
          func = chebev(rrs(Ni),rrs(Nfn),coeff,norder,rmagn)/(rmagn**ia)
          VPInt(1)=VPInt(1)+wf
          VPInt(2)=VPInt(2)+wf*func
  100    continue
  200   continue

      return
!EOC
      end subroutine isopar_chbint
!
!BOP
!!ROUTINE: isopar_chbint_cmplx
!!INTERFACE:
      subroutine isopar_chbint_cmplx(ia,rmt_ovlp,nmx,rrs,fs,            &
     &                        r_circ,fcount,weight,rmag,vj,VPInt)
!!DESCRIPTION:
! isoparametric Chebyshev integration for complex function 
! $ 
! \quad \int\limits_{R_{MT}}^{VP} \frac{fs(r)}{4 \pi r^{ia}} \quad  \mathrm{d^3}r 
! $
!

!!USES:
      use mecca_constants
      use chbint_interface, only : chebev

!!DO_NOT_PRINT
      implicit none
!!DO_NOT_PRINT

!!ARGUMENTS:
       integer :: ia
       real(8) :: rmt_ovlp
       integer :: nmx
       real(8) :: rrs(nmx)
       complex(8) ::  fs(nmx)
       real(8) :: r_circ
       integer :: fcount
       real(8) :: weight(:)  ! (MNqp)
       real(8) :: rmag(:,:,:,:)    ! (MNqp,MNqp,MNqp,MNF)
       real(8) :: vj(:,:,:,:)  ! (MNqp,MNqp,MNqp,MNF)
       complex(8) ::  VPInt
!!REVISION HISTORY:
! Initial Version - A.S. - 2013
!EOP
!
!BOC
       integer :: Ni,Nfn,Ndata
       integer i,j,k,I_face
        complex(8) :: func
        real(8) :: coeff(max_isopoly+1,3)
        integer :: Nq1,Nq2,Nq3
        real(8), allocatable :: f_abs(:),f_re(:),f_im(:)
        real(8) :: wF,func1,func2,func3,errchf,rmagn
        integer :: norder1,norder2,norder3
        integer, external :: indRmesh
!        real(8), external :: chebev
        VPInt=czero

        !---[driver for polynomial fitting]---
        Ni = indRmesh(rmt_ovlp,nmx,rrs)
        if ( abs(rmt_ovlp-rrs(Ni+1))/rmt_ovlp<1.d-7 ) Ni = Ni+1
        Nfn = min(nmx,1+indRmesh(r_circ,nmx,rrs))
        Ndata = Nfn - Ni + 1
        allocate(f_abs(1:Ndata),f_re(1:Ndata),f_im(1:Ndata))
        f_abs(1:Ndata) = abs(fs(Ni:Nfn))
        f_re(1:Ndata)  = dreal(fs(Ni:Nfn))/(f_abs(1:Ndata))
        f_im(1:Ndata)  = aimag(fs(Ni:Nfn))/(f_abs(1:Ndata))
        call chbfit(rrs(Ni:Nfn),f_abs(1:Ndata),                         &
     &            1.d0/rrs(Ni:Nfn),Ndata,                               &
     &            coeff(:,1),size(coeff,1),norder1,1.d-7,errchf)
        call chbfit(rrs(Ni:Nfn),f_re(1:Ndata),                          &
     &            1.d0/rrs(Ni:Nfn),Ndata,                               &
     &            coeff(:,2),size(coeff,1),norder2,1.d-7,errchf)
        call chbfit(rrs(Ni:Nfn),f_im(1:Ndata),                          &
     &            1.d0/rrs(Ni:Nfn),Ndata,                               &
     &            coeff(:,3),size(coeff,1),norder3,1.d-7,errchf)
        coeff(:,1) = coeff(:,1)/(4.d0*pi)
        Nq1 = size(vj,1)
        Nq2 = size(vj,2)
        Nq3 = size(vj,3)
        do 200 I_face=1,fcount
         do 100 k=1,Nq3
         do 100 j=1,Nq2
         do 100 i=1,Nq1
          rmagn=rmag(i,j,k,I_face)
!c******************************************************************
!c   Sometimes we have low accuracy ==>    ! ???????????? 
!c   Large numbers can be generated for vj(*,*,*,*)
!c******************************************************************
!          if(abs(vj(i,j,k,I_face)).lt.100.0d0)then    ! ???????????? 
            func1 = chebev(rrs(Ni),rrs(Nfn),coeff(1:norder1,1),         &
     &                      norder1,rmagn) 
            func2 = chebev(rrs(Ni),rrs(Nfn),coeff(1:norder2,2),         &
     &                      norder2,rmagn) 
            func3 = chebev(rrs(Ni),rrs(Nfn),coeff(1:norder3,3),         &
     &                      norder3,rmagn) 
            func = (func1/sqrt(func2**2+func3**2))*dcmplx(func2,func3) /&
     &             (rmagn**ia)
!            wF = weight(i)*weight(j)*weight(k)*vJ(i,j,k,I_face)
            wF = vJ(i,j,k,I_face)
            VPInt=VPInt+wf*func
!          endif
  100    continue
  200   continue
        deallocate(f_abs,f_re,f_im)

      return
!EOC
      end subroutine isopar_chbint_cmplx
