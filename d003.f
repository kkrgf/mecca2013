!BOP
!!ROUTINE: d003
!!INTERFACE:
      subroutine d003(e,eta,d00)
!!DESCRIPTION:
! calculates parameter {\tt d00} required for Ewald summation
!

!!DO_NOT_PRINT
      implicit none
!!DO_NOT_PRINT

!!ARGUMENTS:
      complex(8), intent(in) :: e
      real(8), intent(in) :: eta
      complex(8), intent(out) :: d00
!!PARAMETERS:
!! tol,tol1 -- empirical values: EWALD ~= HANKEL in fcc with eta=0.25
      real(8), parameter :: tol=1.d-8
      real(8), parameter :: tol1=2.4d-9
!EOP
!
!BOC
      complex(8), parameter :: cone=(1.d0,0.d0)
      real(8), parameter :: one=1.0d0
      real(8), parameter :: pi=3.1415926535897932385d0
      real(8), parameter :: twopi=2.d0*pi
!
!CAB   modified to be sure in accuracy of D00 -- 29.07.98

      integer :: is

      real(8) :: a
!      real(8) :: b
!      real(8) :: c
      real(8) :: compar
      real(8) :: summin,summax

      complex(8) :: eoeta
      complex(8) :: sum
      complex(8) :: term
      complex(8) :: sum0

      a=-sqrt( eta )/( twopi )
      eoeta=e/eta

      is=1
      sum=-cone
      term=-cone

      summin = abs(sum)
      summax = abs(sum)

 10   continue

!      b=(2*is-1)
!      c=(2*is-3)
!      term = eoeta*term*c/(b*is)

      term=(2*is-3)*eoeta*term/(is*(2*is-1))

       sum0 = sum
       summin = min(summin,abs(sum))
       summax = max(summax,abs(sum))

       sum=sum+term

       compar=abs( term/sum )

       if( sum.ne.sum0.and.compar.gt.tol1) then
        is=is+1
        go to 10
       else
         d00=a*sum
         if(summin/summax+one.eq.one.or.abs(term).gt.tol) then
          write(6,*) '  ERROR D003: ###########  eta=',eta
          write(6,*) '   e=',e
          write(6,*) '   is=',is,' term=',term
          write(6,*) '   summin,summax=',summin,summax
          write(6,*) '   d00=',d00
          write(6,*) '  D003:  Bad accuracy'
          stop '  D003:  Bad accuracy'
         end if
       endif

      return
!EOC
      end subroutine d003
