!BOP
!!ROUTINE: print_info1
!!INTERFACE:
      subroutine print_info1( mecca_state )
!!DESCRIPTION: prints concentrations and density in g/cc

!!USES:
      use atomic_weights
      use mecca_types, only : RunState
!!DO_NOT_PRINT
      implicit none
!!DO_NOT_PRINT

!!ARGUMENTS:
      type(RunState), target :: mecca_state
!!REVISION HISTORY:
! Initial Version - M.D. - 2019

!EOP
!BOC
      integer, parameter :: unit=6
      integer            :: ielement,isubl,icomp,zID
      real(8)            :: weight,weightsum,conc
      real(8)            :: volume
      real(8)            :: density_g_cc
      real(8), parameter :: amu_a03_to_g_cc=11.20539024735016731589782d0
      real(8), external  :: g_bohr2A
      real(8), external  :: weightedaverage_atom
      real(8) :: etot_av

      volume=abs(mecca_state%rs_box%volume)
      weightsum=0.0d0
      write(unit,'(/1x,a)') "sublattices:"
      if ( mecca_state%intfile%nsubl>1 ) write(unit,*)
      do isubl=1,mecca_state%intfile%nsubl
         do icomp=1,mecca_state%intfile%sublat(isubl)%ncomp
          zID=mecca_state%intfile%sublat(isubl)%compon(icomp)%zID
          conc=mecca_state%intfile%sublat(isubl)%compon(icomp)%concentr
          weight=atomic_weight(zID)
             write(unit,                                                &
     &      '(2x,"subl=",i3," comp=",i2," conc=",f8.6," Z=",i3,         &
     &        " atm.weight=",f12.8,1x,a)')                              &
     &      isubl,icomp,                                                &
     &      conc,                                                       &
     &      zID,                                                        &
     &      weight,                                                     &
     &      trim(mecca_state%intfile%sublat(isubl)%compon(icomp)%name)
          weight = weight * mecca_state%intfile%sublat(isubl)%ns
          weightsum = weightsum + conc*weight
         enddo
         write(unit,*)
      enddo

      if(volume>1E-8 ) then
         write(unit,*)
         etot_av = weightedaverage_atom(mecca_state)
         write(unit,124) "energy/atom[Ry]",etot_av
 124     format(1x,a20,1x,"=",1x,g24.14)
         density_g_cc=weightsum/volume*amu_a03_to_g_cc
         write(unit,123)     "cell volume[A^3]",volume*g_bohr2A()**3
         write(unit,123) "weight sum[amu/cell]",weightsum
         write(unit,123)    "density[g/cc]",density_g_cc
 123     format(1x,a20,1x,"=",1x,ES23.14)
      endif

      end subroutine print_info1
