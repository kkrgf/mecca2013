!c
!c     Multiple-scattering      |\  /|  --   --  --  --
!c     Electronic-structure     | \/ | |-   |   |   |__|
!c     Calculations for         |    | |___ |__ |__ |  |
!c     Complex
!c     Alloys
!c
!c     ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
!BOP
!!ROUTINE: mecca_scf
!!INTERFACE:
      subroutine mecca_scf( mecca_state,outinfo )
! !DESCRIPTION:
!   Multi-sublattice, multi-component KKR-CPA code, main function is
!   to compute scf state {\tt mecca\_state} and/or Green's-Function related properties
!   (e.g. DOS, Fermi-surface, etc.).
!   General output information is in {\tt outinfo}, detailed output can be found
!   in {\tt mecca\_state} and in various output-files.

!!USES:
       use mecca_constants
       use mecca_types
       use mecca_run
       use input,   only : saveInput,max_nonrel_z
       use struct,  only : setup_struct,invadd
       use atom,    only : calcAtom,set_atom_procedure
       use vp_str,  only : setOptSphBasis
       use initpot, only : init_potential
       use potential
       use scf_io,  only : save_inifile,save_vrho,cpscflog
       use gfncts_interface, only : g_numNonESsites,g_reltype
       use mecca_interface
       use raymethod, only : gDosBsfInput
       use xc_mecca, only : xc_mecca_pointer,gXCmecca,gInfo_libxc
       use xc_mecca, only : max_nonlibxc_id,xc_f90_version
       use mpi
!

!!DO_NOT_PRINT
      implicit none
!!DO_NOT_PRINT

!INPUT/OUTPUT ARGUMENTS:
      type (RunState), target :: mecca_state
!OUTPUT ARGUMENTS:
      character*(*), intent(out) :: outinfo           ! output info

! !REVISION HISTORY:
! Initial Version - A.S. - 2013
!
! !REMARKS:
!   The subroutine can be also used for some other purposes, .e.g.
!   to initialize mecca_state from potential file,
!   to generate input file, etc.
!
!EOP

!BOC
      integer :: ierr, myrank, nproc
      integer :: atom_kind, iz

      character(*), parameter :: sname='mecca_scf'

      type (IniFile),  pointer :: inFile
      type (VP_data),  target, save :: jcbn0box
      type(RS_Str),     target, save :: rs0box
      type(KS_Str),     target, save :: ks0box
      type(EW_Str),     target, save :: ew0box
      type(GF_data),    target, save :: gf0box
      type(GF_output),  target, save :: gf_out0box
      type(Work_data),  target, save :: work0box
      type(DosBsf_data),target, save :: bsf0box
      type(FS_data),    target, save :: fs0box

      integer :: st_env
      integer :: iprint_save=-1
!c     ******************************************************************
!c     *****  start of executible statements  ***************************
!c     ******************************************************************

      mecca_state%info = -1
      outinfo = 'undefined'

      if ( .not. associated(mecca_state%intfile) ) then

!      interface
!        character (LEN=32) function buildDate()
!        end function buildDate
!      end interface

        write(6,'(1x,a)') version//buildDate()
        return
      end if
      inFile => mecca_state%intfile
      call mpi_comm_rank(mpi_comm_world,myrank,ierr)
      call mpi_comm_size(mpi_comm_world, nproc, ierr)
      if ( nproc>1 ) then
       inFile%nproc = nproc
      end if
      if ( .not. associated(mecca_state%rs_box) ) then
       mecca_state%rs_box   => rs0box
      end if
      if ( .not. associated(mecca_state%ks_box) ) then
       mecca_state%ks_box   => ks0box
      end if
      if ( .not. associated(mecca_state%ew_box) ) then
       mecca_state%ew_box   => ew0box
      end if
      if ( .not. associated(mecca_state%vp_box) ) then
       mecca_state%vp_box => jcbn0box
      end if
      if ( .not. associated(mecca_state%gf_box) ) then
       mecca_state%gf_box => gf0box
      end if
      if ( .not. associated(mecca_state%gf_out_box) ) then
       mecca_state%gf_out_box => gf_out0box
      end if
      if ( .not. associated(mecca_state%bsf_box) ) then
       mecca_state%bsf_box => bsf0box
      end if
      if ( .not. associated(mecca_state%fs_box) ) then
       mecca_state%fs_box => fs0box
      end if

!  definition of the global run_state
!
      call setMS( mecca_state )

!  printing input information
!
      call mecca_hello()

      if ( mecca_state%intfile%imethod.ne.0 .OR.                        &
     &       mecca_state%intfile%sprstech.ne.0 ) then
       call defMethod(0,mecca_state%intfile%imethod,                    &
     &                             mecca_state%intfile%sprstech)
       write(6,'(/)')
      end if

      if ( mecca_state%intfile%nscf > 0 ) then

!  saving input in a file
!
        if ( myrank==0 ) call save_inifile(mecca_state)
      else if ( mecca_state%intfile%nscf == 0 ) then
        mecca_state%intfile%nmesh = 1
        call gDosBsfInput(mecca_state%bsf_box)
         mecca_state%intfile%nKxyz(1,1) = mecca_state%bsf_box%nx
         mecca_state%intfile%nKxyz(2,1) = mecca_state%bsf_box%ny
         mecca_state%intfile%nKxyz(3,1) = mecca_state%bsf_box%nz
        if ( minval(abs(mecca_state%intfile%nKxyz))==0 ) then
         mecca_state%intfile%nKxyz(1:3,1) = 1
        end if
        iprint_save = mecca_state%intfile%iprint
        mecca_state%intfile%iprint = -1000
      end if
!
      call mpi_barrier(mpi_comm_world, ierr)

      call setup_struct(mecca_state)
      if ( mecca_state%fail ) call p_fstop(sname//                      &
     &   ' :: ERROR, unable to continue after structure setup')

!!  setup_atomic
      call gEnvVar('ENV_ATOM',.true.,st_env)
      atom_kind = st_env
      if ( atom_kind>=0 .and. atom_kind<=4 ) then
        if ( mecca_state%intfile%iprint >= 0 ) then
         select case (atom_kind)
          case(1)
           write(6,'(/a/)')                                             &
     &' INFO: MECCA LDA_HEDIN non-relativ. atomic data are generated'
          case(2)
           write(6,'(/a/)')                                             &
     &' INFO: DFTATOM LDA_VWN non-relativ. atomic data are generated'
          case(3)
           write(6,'(/a/)')                                             &
     &' INFO: DFTATOM LDA_VWN Dirac atomic data are generated'
          case(4)
           write(6,'(/a/)')                                             &
     &' INFO: DFTATOM R-LDA_VWN Dirac atomic data are generated'
          case default
           write(6,'(/a/)')                                             &
     &' INFO: LIBXC default is in use for atomic calculation'
         end select
        end if
        call set_atom_procedure(atom_kind)
      else
      ! libxc
       if ( mecca_state%intfile%nrel>1 .and.                            &
     &            mecca_state%intfile%nrel<max_nonrel_z ) then
        atom_kind = -mecca_state%intfile%iXC
        write(6,'(/a/)')                                                &
     &' INFO: LIBXC is in use for non-relativistic atomic calculations'
       else
        atom_kind = max(0,mecca_state%intfile%iXC)
       end if
       call set_atom_procedure(nkind=atom_kind)
      end if
      do iz = 1,mecca_state%intfile%nelements
       call calcAtom(mecca_state%intfile%elements(iz)%ztot)
      end do
!!  end setup_atomic

      call print_info1(mecca_state)  ! composition and density printing

!  computing spherical-basis and structure-related properties
!  (mt/asa radii, nearset neighbors, VP ,etc.)
!
      call setOptSphBasis(mecca_state)
      if ( mecca_state%fail ) call p_fstop(sname//                      &
     &   ' :: ERROR, unable to setup optimal sph.basis')

!  intializing radial meshes and potentials
!
      call init_potential(mecca_state)
      if ( mecca_state%fail ) call p_fstop(sname//                      &
     &   ' :: ERROR, unable to initialize potentials')

!  main SCF cycle with calculation of charge density, scf potentials and
!  some other GF-related properties
!
      if ( mecca_state%intfile%nscf > 0 ) then  ! ** SCF-cycle **
        if ( myrank==0 ) call save_vrho(mecca_state)  !DEBUG
        call iterateSCF(mecca_state)
        if ( mecca_state%fail ) then
         write(*,*) sname//' :: ERROR, unable to complete SCF cycle'
         goto 10
        end if
!
        call saveOutput(mecca_state,outinfo)
        if ( mecca_state%fail ) call p_fstop(sname//                    &
     &   ' :: ERROR, unable to save output')
      else if ( mecca_state%intfile%nscf == 0 ) then
        mecca_state%intfile%iprint = iprint_save
      end if                                    ! ** end of SCF-cycle **
      call scf_delete_files(mecca_state%intfile%genName)
      if ( myrank==0 )call cpscflog(mecca_state%intfile%io(nu_info)%name&
     &                     ,0,mecca_state%intfile%io(nu_info)%name)

!  computing density of states and/or Bloch spectral function and/or Fermi surface
!
      call calcDOS_BSF_FS(mecca_state)
      if ( mecca_state%fail ) write(6,*) sname//                        &
     &   ' :: ERROR, unable to complete DOS-related calculations'
!

!DELETE      call defineTauSet( mecca_state )

!DELETE      call runTauCalc(mecca_state)
!DELETE      if ( mecca_state%fail ) write(6,*) sname//                        &
!DELETE     &   ' :: ERROR, unable to complete Tau-related calculations'
!
   10    continue
!  memory deallocation
!
      call deallocSphDeps(inFile)

      call deallocRunState(mecca_state)
!
      call fstop('return')

!c     ******************************************************************

      return

!EOC
      contains

!BOP
!!IROUTINE: saveOutput
!!INTERFACE:
      subroutine saveOutput(mecca_state,outinfo)
!!DESCRIPTION:
! to collect main output data from {\tt mecca\_state\%work\_box}
! (free energy, total energy, pressure) in {\tt outinfo}

!!DO_NOT_PRINT
      implicit none
!!DO_NOT_PRINT

!!ARGUMENTS:
      type(RunState), intent(in), target :: mecca_state
      character*(*), intent(out) :: outinfo           ! output info
!!REVISION HISTORY:
! Initial Version - A.S. - 2013
!EOP
!
!BOC
      type(IniFile),   pointer :: inFile
      type(Work_data), pointer :: work_box
      real(8) :: tzatom,press,etot,fetot,volume
!      character(80) :: line80,savename
      integer, parameter :: nfrmt=6,nlen=25
!      character(nfrmt*nlen) :: ctmp
      integer :: nrec
      if ( .not.associated(mecca_state%intfile) ) return
      if ( .not.associated(mecca_state%rs_box) ) return
      if ( .not.associated(mecca_state%work_box) ) return
      inFile => mecca_state%intfile
      work_box => mecca_state%work_box
      tzatom = g_numNonESsites(inFile)
      if ( tzatom <= zero ) tzatom = one
      press = work_box%press
      etot = work_box%etot
      fetot = work_box%etot - inFile%Tempr*work_box%entropy
      etot = etot/tzatom
      fetot = fetot/tzatom
      volume = mecca_state%rs_box%volume/tzatom
      nrec = nfrmt*nlen
      write(outinfo,'(6(1x,es24.16))',err=1) volume,fetot,etot,press,   &
     &                                  inFile%Tempr,inFile%etop
      return
!
1     continue
      if ( mecca_state%intfile%iprint>=0 ) then
       write(6,'(/a)') 'WARNING: outinfo size is too small'
      end if
      write(6,'(/a,6(1x,es24.16))') 'OUTINFO: ',volume,fetot,etot,      &
     &                                  press,inFile%Tempr,inFile%etop
!EOC
      end subroutine saveOutput

!BOP
!!IROUTINE: calcDOS_BSF_FS
!!INTERFACE:
      subroutine calcDOS_BSF_FS( mecca_state )
!!DESCRIPTION:
!  calculation of density of states and/or
!  Bloch spectral function and/or
!  Fermi surface
!
!!USES:
      use mesh3d, only : updateMPmesh
      use raymethod, only : gDosBsfInput,gFsInput,sRayParams
!
!!ARGUMENTS:
      type(RunState), target :: mecca_state
!!REVISION HISTORY:
! Initial Version - A.S. - 2013
!EOP
!
!BOC
      type(IniFile),   pointer :: inFile
      type(DosBsf_data), pointer :: tmp_dosbsf_p =>null()
      type(FS_data),   pointer :: tmp_fs_p => null()
      type(KS_Str),    target, save :: ks_box
      type(KS_Str),   pointer :: tmp_ks_p=>null()
      integer :: nscf_in
      integer :: i,k,nq(3)
      integer nu
      logical opnd,hxsym
      real(8), pointer :: p_kway(:)=>null()
      real(8) :: kpt(3),v_ks(3)
      logical, external :: hexSymm
      character(3), parameter :: sfx(3)=['   ','_up','_dn']
      character(4), parameter :: c_dat='.dat'

      if ( .not.associated(mecca_state%intfile) ) return
      inFile => mecca_state%intfile
      nscf_in = inFile%nscf

      if ( associated(mecca_state%bsf_box) ) then
        tmp_dosbsf_p => mecca_state%bsf_box
      end if
      call gDosBsfInput(mecca_state%bsf_box)

      if ( associated(mecca_state%fs_box) ) then
        tmp_fs_p => mecca_state%fs_box
      end if
      call gFsInput(mecca_state%fs_box)

      if ( mecca_state%bsf_box%idosplot>0 .or.                          &
     &     mecca_state%bsf_box%ibsfplot>0 .or.                          &
     &     mecca_state%fs_box%ifermiplot>0                              &
     &    ) then
       write(6,'(/2a)')  ' Initialization of DOS-BSF calculations '
       if ( mecca_state%bsf_box%idosplot==0 ) then
        write(6,'(a)') ' WARNING: BSF/FS calculations are not activated'&
     &                //'as idosplot=0'
       else
        tmp_ks_p=>mecca_state%ks_box
        inFile%nscf = 0
        nq(1) = mecca_state%bsf_box%nx
        nq(2) = mecca_state%bsf_box%ny
        nq(3) = mecca_state%bsf_box%nz
        if ( nq(1)*nq(2)*nq(3) == 0 ) then
         nq(1:3) = mecca_state%intfile%nKxyz(1:3,1)
        end if

        if ( mecca_state%bsf_box%dosbsf_kintsch == ksint_scf_sch ) then
         write(6,'(2a)') ' with special points method...'
         mecca_state%ks_box => ks_box
         nq = -abs(nq)
         call updateMPmesh(nq,tmp_ks_p,mecca_state,invadd)
         if ( mecca_state%bsf_box%ibsfplot>0 ) then
          write(6,'(a,i2)') ' BSF calculations are not supported with ' &
     &                  //'dosbsf_kintsch = ',ksint_scf_sch
         end if
         if ( mecca_state%fs_box%ifermiplot>0 ) then
          write(6,'(a,i2)') ' FS calculations are not supported with '  &
     &                  //'dosbsf_kintsch = ',ksint_scf_sch
         end if
        else
         write(6,'(2a)')  ' with ray method...'
         hxsym = hexSymm()
         nq = abs(nq)
         call sRayParams(nrec_ray=mecca_state%bsf_box%ray_nrec,         &
     &                   nrad_ray=mecca_state%bsf_box%ray_nrad,         &
     &                   nx_ray=nq(1),ny_ray=nq(2),nz_ray=nq(3),        &
     &                   hexbox_ray=hxsym)
         v_ks(1)=dot_product(tmp_ks_p%kslatt(:,1),tmp_ks_p%kslatt(:,1))
         v_ks(2)=dot_product(tmp_ks_p%kslatt(:,2),tmp_ks_p%kslatt(:,2))
         v_ks(3)=dot_product(tmp_ks_p%kslatt(:,3),tmp_ks_p%kslatt(:,3))
         v_ks(1:3) = sqrt( v_ks(1:3) )           ! kslatt(:) / v_ks(:) are unit vectors
!
         if ( inFile%iprint >= -1 ) then
          write(6,*)
          write(6,'('' reciprocal vectors used for BSF setup '          &
     &              //'and k-lattice spacings:'')')
          write(6,'(8x,''( '',f10.5,'', '',f10.5,'', '',f10.5,'' ) '',  &
     &          f11.5)') (tmp_ks_p%kslatt(1:3,i),v_ks(i),i=1,3)
         end if
!
         if ( bsf_k_latt_coord ) then
! bsf_k_latt_coord is defined in mecca_constants.f
          if ( mecca_state%bsf_box%bsf_nkwaypts>1 ) then
           write(6,*) ' BSF: Lattice -> Cartesian vectors conversion '  &
     &             //'(in 2pi/alat):'
           do i=1,mecca_state%bsf_box%bsf_nkwaypts ! conversion from
                                                  ! reciprocal lattice coordinates system
            p_kway => mecca_state%bsf_box%bsf_kwaypts(:,i)
            kpt = zero
            do k=1,3
             kpt(k) = kpt(k) + sum(p_kway(1:3)*tmp_ks_p%kslatt(k,1:3))
            end do
            write(6,'(3(1x,f9.4),4x,3(1x,f9.4))' ) p_kway(:),kpt(:)
            p_kway(:) = kpt(:)
           end do
          end if

          if ( mecca_state%fs_box%fermi_nkpts>0 ) then
           if ( mecca_state%bsf_box%bsf_nkwaypts>1 ) write(6,*)
           write(6,*) ' FS: Lattice -> Cartesian vectors conversion '   &
     &             //'(in 2pi/alat):'
           do i=0,mecca_state%fs_box%fermi_nkpts ! conversion from
                                                  ! reciprocal lattice coordinates system
            if ( i==0 ) then
             p_kway => mecca_state%fs_box%fermi_korig
            else
             p_kway => mecca_state%fs_box%fermi_kpts(:,i)
            end if
            kpt = zero
            do k=1,3
             kpt(k) = kpt(k) + sum(p_kway(1:3)*tmp_ks_p%kslatt(k,1:3))
            end do
            write(6,'(3(1x,f9.4),4x,3(1x,f9.4))' ) p_kway(:),kpt(:)
            p_kway = kpt
           end do
          end if
         else
             ! units of 2pi/a
         end if
        end if

        call iterateSCF(mecca_state)

        inFile%nscf = nscf_in
        if ( .not. associated(tmp_ks_p,mecca_state%ks_box) ) then
         call deallocKSstr(ks_box)
         mecca_state%ks_box => tmp_ks_p
        end if
        nullify(tmp_ks_p)
        call deallocKSstr(ks_box)
        nu = inFile%io(nu_dos)%nunit
        inquire(unit=nu,opened=opnd )
        if ( opnd ) close(nu)
        nu = inFile%io(nu_bsf)%nunit
        inquire(unit=nu,opened=opnd )
        if ( opnd ) close(nu)
        nu = inFile%io(nu_fs1)%nunit
        inquire(unit=nu,opened=opnd )
        if ( opnd ) close(nu)
        nu = inFile%io(nu_fs2)%nunit
        inquire(unit=nu,opened=opnd )
        if ( opnd ) close(nu)
       end if
      else
       if ( nscf_in==0 ) then
        write(6,'(/2a)')  ' DOS-BSF-FS calculations are not activated '
       end if
      end if
      mecca_state%bsf_box => tmp_dosbsf_p
      nullify(tmp_dosbsf_p)
      mecca_state%fs_box => tmp_fs_p
      nullify(tmp_fs_p)
      return
!EOC
      end subroutine calcDOS_BSF_FS

!      subroutine calcFS( mecca_state )
!      use raymethod, only : gFsInput,sRayParams
!      type(RunState), target :: mecca_state
!      type(IniFile),   pointer :: inFile
!      type(FS_data),   pointer :: tmp_fs_p => null()
!      integer :: nscf_in
!      if ( .not.associated(mecca_state%intfile) ) return
!      inFile => mecca_state%intfile
!! initialize fermis....
!      if ( associated(mecca_state%fs_box) ) then
!        tmp_fs_p => mecca_state%fs_box
!      end if
!      call gFsInput(mecca_state%fs_box)
!      if ( mecca_state%fs_box%ifermiplot>0 ) then
!        nscf_in = inFile%nscf
!        inFile%nscf = 0
!
!        call iterateSCF(mecca_state)
!
!        inFile%nscf = nscf_in
!      end if
!      mecca_state%fs_box => tmp_fs_p
!      nullify(tmp_fs_p)
!      return
!      end subroutine calcFS

!BOP
!!IROUTINE: deallocRunState
!!INTERFACE:
      subroutine deallocRunState( mecca_state )
!!DESCRIPTION:
! deallocation of {\tt mecca\_state} (type RunState)
!
!!ARGUMENTS:
!!PARAMETERS:
!!REVISION HISTORY:
! Initial Version - A.S. - 2013
!!REMARKS:
!EOP
!
!BOC
      type(RunState) :: mecca_state
      logical all

      if ( associated(mecca_state%rs_box,rs0box) ) then
       call deallocRSstr(rs0box)
       nullify(mecca_state%rs_box)
      end if
      if ( associated(mecca_state%ks_box,ks0box) ) then
       call deallocKSstr(ks0box)
       nullify(mecca_state%ks_box)
      end if
      if ( associated(mecca_state%ew_box,ew0box) ) then
       call deallocEWstr(ew0box)
       nullify(mecca_state%ew_box)
      end if
      if ( associated(mecca_state%vp_box,jcbn0box) ) then
       call deallocJacbnVP(jcbn0box)
       nullify(mecca_state%vp_box)
      end if
      if ( associated(mecca_state%gf_box,gf0box) ) then
       call deallocGFdata(gf0box)
       nullify(mecca_state%gf_box)
      end if
      if ( associated(mecca_state%gf_out_box,gf_out0box) ) then
       all = .true.
       call deallocGFoutput(gf_out0box,all)
       nullify(mecca_state%gf_out_box)
      end if
      if ( associated(mecca_state%bsf_box,bsf0box) ) then
       call deallocDosBsf(bsf0box)
       nullify(mecca_state%bsf_box)
      end if
      if ( associated(mecca_state%fs_box,fs0box) ) then
       call deallocFSdata(fs0box)
       nullify(mecca_state%fs_box)
      end if
      if ( associated(mecca_state%work_box,work0box) ) then
       call deallocWorkdata(work0box)
       nullify(mecca_state%work_box)
      end if

      call nullMS( mecca_state )

      return
!EOC
      end subroutine deallocRunState

!BOP
!!IROUTINE: mecca_hello
!!INTERFACE:
      subroutine mecca_hello()
!!DESCRIPTION:
!  to print basic input information
!
!!ARGUMENTS:
!!PARAMETERS:
!!REVISION HISTORY:
! Initial Version - A.S. - 2013
!!REMARKS:
!EOP
!
!BOC
      implicit none
      character(60) :: approx='ASA'
      character(60) :: comment='V0:non-var'
      type(xc_mecca_pointer) :: libxc_p
      character(512) :: xcdescr
      integer :: ctype,stype,vtype
      integer :: v1,v1m,v1mm
      real(8) :: tctol

      call fstop('CPU_START')
      write(6,*) '                                << MECCA >> '
      write(6,*) '     Multiple-scattering Electronic-structure Code',  &
     &            ' for Complex Alloys'
      write(6,'(19x,''(KKR-CPA code, version '',a,'')''/)') version//   &
     &                 build_date

      if ( mecca_state%intfile%mtasa == 0 ) then
       approx = 'MT-scheme'
!!!  something was lost in MT-branch
       call fstop('MT-scheme (mtasa=0) is disabled')
      end if
      if ( mecca_state%intfile%mtasa == 1 ) then
       approx = 'ASA-scheme'
       if ( mecca_state%intfile%intgrsch == 1 ) then
        comment = 'V0:var-ASA'
       end if
       if ( mecca_state%intfile%intgrsch == 2 ) then
        comment = 'V0:var-VP'
       end if
      end if
      if ( mecca_state%intfile%mtasa == 2 ) then
       approx = 'spher-scheme-with-rho0_PBC, V0:var'
       comment = '****************************'
       if ( mecca_state%intfile%intgrsch == 1 ) then
        comment = 'ASA-intgr'
       end if
       if ( mecca_state%intfile%intgrsch == 2 ) then
        comment = 'VP-intgr'
       end if
      end if

      write(6,'(6x,a)') ' APPROXIMATION: '//trim(approx)//' + '//       &
     &                     trim(comment)

      call g_reltype(ctype,stype,vtype)
      if ( vtype == 0 ) then
       write(6,'(20x,a)') ': '//'relativistic for valence'//            &
     &                                           ' (MS is scalar-rel.)'
       if (mecca_state%intfile%so_power.ne.1.d0)                        &
     &  write(6,'(20x,a,f5.2)') ': '//'  spin-orbit power =',           &
     &                              mecca_state%intfile%so_power
      else if ( vtype == 1 ) then
       write(6,'(20x,a)') ': '//'scalar-relativistic for valence'
      else
       write(6,'(20x,a)') ': '//'non-relativistic for valence'
      end if
      if ( stype == 0 ) then
       if ( ctype == 0 ) then
        write(6,'(20x,a)') ': '//'and relativistic for core electrons'
       else
      write(6,'(20x,a)') ': '//'and non-relativistic for core electrons'
       end if
      else
       if ( stype == 1 ) then
        write(6,'(20x,a)')                                              &
     &            ': '//'  scalar-relativistic for semicore electrons,'
        write(6,'(20x,a)') ': '//'and relativistic for core electrons'
       else if ( stype == 2 ) then
        write(6,'(20x,a)')                                              &
     &            ': '//'and scalar-relativistic for core electrons'
       else if ( stype == 3 ) then
        write(6,'(20x,a)')                                              &
     &        ': '//'  scalar-relativistic for KKR semicore electrons,'
        write(6,'(20x,a)') ': '//'and relativistic for core electrons'
       else
        write(6,'(20x,a)')                                              &
     &            ': '//'and unknown for core/semicore electrons'
       end if
      end if

      if ( .not. gPointNucleus() ) then
       write(6,'(20x,a)') ': '//'finite-size nucleus model'
      endif

      if (maxval(mecca_state%intfile%sublat(1:mecca_state%intfile%nsubl)&
     &    %ncomp)>1 ) then
       if ( abs(mecca_state%intfile%ncpa) > 0 ) then
        tctol = gTcTol()
        if (                                                            &
     &   maxval(mecca_state%intfile%sublat(1:mecca_state%intfile%nsubl) &
     &    %ncomp)==2 .and. mecca_state%intfile%nMM == 1) then
         continue   !DLM
        else
         write(6,'(20x,a,i4,a,es8.1)')                                  &
     &                        ': with CPA, max number of iter. =',      &
     &    abs(mecca_state%intfile%ncpa),' and tolerance =',tctol
         if ( mecca_state%intfile%nsubl > 1 ) then
          write(6,'(20x,a,a,a)')                                        &
     &     ': "',trim(cc_mod),'" charge-correlation model is applied'
         end if
        end if
       end if
      end if

      if ( abs(mecca_state%intfile%nspin) == 1 ) then
       write(6,'(20x,a)') ': '//'non-magnetic'
      else
       if ( abs(mecca_state%intfile%nspin) == 2 ) then
        if ( mecca_state%intfile%nMM == 1 ) then
         write(6,'(20x,a,i3,a,es8.1)') ': DLM (paramagnetic) model;'    &
     &    //' max number of CPA iter. =',abs(mecca_state%intfile%ncpa)
        else if ( mecca_state%intfile%nMM == -1 ) then
         write(6,'(20x,a)')                                             &
     &     ': '//'antiferromagnetic collinear spins'
        else
         write(6,'(20x,a)') ': '//'collinear spins'
        end if
!        if ( mecca_state%intfile%magnField .ne. 0 ) then
!         write(6,'(20x,a,e11.4)')                                       &
!     &                   ': '//'with external spin-exchange (Ry) =',    &
!     &                           mecca_state%intfile%magnField
!        end if
! stype,ctype,vtype are just local variables not related to rel.type
        call getEXCtype(stype)
        call get_max_exctype(ctype)
        call get_dflt_exctype(vtype)
        if ( stype>=0 .and. stype<=ctype ) then
         if ( stype==vtype ) then
          write(6,'(20x,a,i2,a)')': magn. boundary condition type ',    &
     &                                 vtype,' (default)'
         else
          write(6,'(20x,a,i2)')': magn. boundary condition type ',stype
         end if
!        else if ( stype==-1 ) then
!         write(6,'(20x,a,i2)')': spin splitting type ',vtype
        end if
       else
        write(6,'('' ERROR :: incorrect input magn. boundary condition''&
     &            //'' type '',i3)') stype
        call fstop('mecca_scf')
       end if
      end if

      call gXCmecca(mecca_state%intfile%iXC,libxc_p)   ! to get xc_mecca_pointer
      call gInfo_libxc(libxc_p,xcdescr)
      if ( mecca_state%intfile%iXC > max_nonlibxc_id ) then
       call xc_f90_version(v1, v1m, v1mm)
       write(6,'(20x,'': libXC '',i1,''.'',i1,''.'',i1)') v1,v1m,v1mm
      end if
      call gXCmecca(-1,libxc_p)                        ! to release xc_mecca_pointer
      write(6,'(20x,a)') ': functional '//trim(xcdescr)

      if ( mecca_state%intfile%x_pot_corr%id > 0 ) then
       libxc_p%iexch = mecca_state%intfile%x_pot_corr%id
       call gInfo_libxc(libxc_p,xcdescr)
       libxc_p%iexch = 0
       write(6,'(20x,a)') ': exchange potential correction '//          &
     &                            trim(xcdescr)
      end if

      write(6,'(20x,2(a,i4))') ': '//'Lmax =',mecca_state%intfile%lmax  &
     &,                              '  lminSS =',lminSS
      if ( mecca_state%intfile%Tempr .ne. 0 ) then
         write(6,'(20x,a,e11.4)') ': '//'temperature (Ry) =',           &
     &                      mecca_state%intfile%Tempr
      end if
      write(6,'(/6x,a)') 'Accuracy control constants:'
      write(6,'(10x,a,f6.4,2x,3(2x,a,es8.2))')                          &
     &              'log(r)-step = ',gXRmeshStep()                      &
     &,             'Q_tol = ',gQTol()                                  &
     &,             'E_tol = ',gETol()                                  &
     &,             'Ef_tol = ',gEfTol()


      write(6,'(/6x,a)') 'SYSTEM id: '//                                &
     &                        trim(mecca_state%intfile%genName)
      write(6,'(6x,a)') 'Structure description file: '//                &
     &                      trim(mecca_state%intfile%io(nu_xyz)%name)

      return
!EOC
      end subroutine mecca_hello

      end subroutine mecca_scf
