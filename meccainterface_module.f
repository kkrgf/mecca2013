!BOP
!!MODULE: mecca_interface
!!INTERFACE:
      module mecca_interface
!!DESCRIPTION:
! fortran interface provider for:
! {\bv
! subroutine broyden
! function buildDate
! function cellvolm
! function char2status
! function char2form
! subroutine convertVr0
! subroutine convertVr1
! subroutine effind2
! subroutine efadjust2
! subroutine entrpterm
! subroutine ewldvect
! subroutine get0ata
! subroutine grint
! subroutine intgrtau
! subroutine iterateSCF
! subroutine genvec
! subroutine keep1
! subroutine latvect
! subroutine mdos
! subroutine mgreen
! subroutine RStau00
! subroutine scalar
! subroutine scf_atom_pot
! subroutine semrel
! subroutine semrelzj
! subroutine tote1
! subroutine ws_integral
! subroutine asa_integral
! \ev}
!
!!REVISION HISTORY:
! Initial version - A.S. - 2013
!EOP
!***********************************************************************
      interface
        subroutine broyden(rhot,rhin,komp,nbasis,mspn,jtop,             &
     &                        obroy1,obroy2,iflag,alpha,beta)
        implicit none
        real(8), intent(in)    ::   rhin(:,:,:,:)  ! (iprpts,ipcomp,ipsubl,msphat,ipspin)
        real(8), intent(inout) ::   rhot(:,:,:,:)  ! (iprpts,ipcomp,ipsublat,ipspin)
        integer, intent(in)    :: nbasis,komp(nbasis),mspn,jtop
        character(*), intent(in) :: obroy1,obroy2
        integer, intent(in)    :: iflag
        real(8), intent(in)    :: alpha
        real(8), intent(in), optional :: beta
        end subroutine broyden
      end interface
!
      interface
        function buildDate()
        character(32) buildDate
        end function buildDate
      end interface
!
      interface
        function cellvolm(rslatt,kslatt,iflag)
        real(8) :: cellvolm
        real(8), intent(in) ::   rslatt(3,3)
        real(8) :: kslatt(*)
        integer, intent(in) :: iflag
        end function cellvolm
      end interface
!
      interface
        function char2status( stat ) result ( cstatus )
        character(1), intent(in) :: stat
        character(7) :: cstatus
        end function char2status
      end interface
!
      interface
        function char2form( frm ) result ( cform )
        character(1), intent(in) :: frm
        character(11) :: cform
        end function char2form
      end interface
!
      interface
        subroutine convertVr0(n,ztot,rr,vector,iflag)
        integer, intent(in) :: n
        real(8), intent(in) :: ztot
        real(8), intent(in) :: rr(:)
        real(8), intent(inout) :: vector(:)
        integer, intent(in) :: iflag
        end subroutine convertVr0
      end interface
!
      interface
        subroutine convertVr1(n,ztot,rr,vector,iflag)
        integer, intent(in) :: n
        real(8), intent(in) :: ztot
        real(8), intent(in) :: rr(:)
        real(8), intent(inout) :: vector(:)
        integer, intent(in) :: iflag
        end subroutine convertVr1
      end interface
!
      interface
        subroutine effind2(numef,ebot,efold,                            &
     &               zvaltot,deltZ,cdosEf,curr_error,                   &
     &               defmax,                                            &
     &               efnew)
        implicit none
        integer, intent(in) :: numef
        real(8), intent(in) :: ebot,efold
        real(8), intent(in) :: zvaltot
        real(8), intent(in) :: deltZ
        complex(8), intent(in) :: cdosEf
        real(8), intent(in) :: curr_error
        real(8), intent(in) :: defmax
        real(8) ::     efnew
        end subroutine effind2
      end interface
!
      interface
        subroutine efadjust2(efnew,etop,                                &
     &               xvalws,xvalmt,doslast,doscklast,evalsum,           &
     &               zvaltot,dQ,                                        &
     &               nbasis,nspin,komp,atcon,numbsub,                   &
     &               iprint,istop)
        implicit none
        real(8) ::     efnew
        real(8) ::     etop
        real(8) ::     xvalws(:,:,:)  ! (ipcomp,ipsublat,ipspin)
        real(8) ::     xvalmt(:,:,:)  ! (ipcomp,ipsublat,ipspin)
        complex(8) ::  doslast(:,:,:)  ! (ipcomp,ipsublat,ipspin)
        complex(8) ::  doscklast(:,:,:)  ! (ipcomp,ipsublat,ipspin)
        real(8), intent(out) :: evalsum(:,:,:)  ! (ipcomp,ipsublat,ipspin)
        real(8), intent(in)  :: zvaltot
        real(8), intent(in)  :: dQ
        integer ::     nbasis,nspin
        integer ::     komp(nbasis)
        real(8) ::     atcon(:,:)  ! (ipcomp,nbasis)
        integer ::  numbsub(nbasis)
        integer ::     iprint
        character(10) ::        istop
        end subroutine efadjust2
      end interface
!
      interface
        subroutine entrpterm(Tempr,efermi,egrd,                         &
     &                     dele1,nume,                                  &
     &                     dostspn,                                     &
     &                     dostef,nspin,                                &
     &                     avnatom,entropy,iprint,istop)
        implicit none
        real(8), intent(in) :: Tempr,efermi
        integer, intent(in) :: nume
        complex(8), intent(in) :: egrd(:),dele1(:)
        complex(8), intent(in) :: dostspn(:,:)
        complex(8), intent(in) :: dostef(:)
        integer, intent(in) :: nspin
        real(8), intent(in) :: avnatom
        real(8), intent(out) :: entropy
        integer, intent(in) :: iprint
        character(*), intent(in) :: istop
        end subroutine entrpterm
      end interface
!
      interface
        subroutine ewldvect(vectin,nin,ndimin,cutoff,                   &
     &                    point,npnt,                                   &
     &                    vectout,nout,ndimout,                         &
     &                    np2v,numbv)
        implicit none
        integer, intent(in) :: nin,ndimin
        real(8), intent(in) :: vectin(ndimin,3),cutoff
        integer, intent(in) :: npnt
        real(8), intent(in) :: point(3,npnt)
        integer :: nout,ndimout
        real(8), allocatable :: vectout(:,:)  !  (ndimout,4)
        integer :: np2v(ndimin,npnt),numbv(nin)
        end subroutine ewldvect
      end interface
!
      interface
        subroutine get0ata(atcon,tab,tcpa,kkrsz,komp,nbasis,            &
     &                  iprint,istop)
        implicit none
        real(8), intent(in) :: atcon(:,:)       ! (ipcomp,ipsublat)
        complex(8), intent(in) :: tab(:,:,:,:)  ! (ipkkr,ipkkr,ipcomp,ipsublat)
        complex(8), intent(out) :: tcpa(:,:,:)  ! (ipkkr,ipkkr,ipsublat)
        integer, intent(in) :: kkrsz,nbasis,komp(nbasis),iprint
        character(10), intent(in) :: istop
        end subroutine get0ata
      end interface
!
      interface
        subroutine grint(nrelv,clight,                                  &
     &                 lmax,kkrsz,                                      &
!!     &                 icryst,alat,                                     &
     &                 energy,pnrel,                                    &
     &                 iswzj,                                           &
     &                 vr,xr,rr,                                        &
     &                 h,jmt,jws,rmt,rws,                               &
     &                 rmt_true,r_circ,ivar_mtz,                        &
     &                 fcount,weight,rmag,vj,lVP,                       &
     &                 zlab,                                            &
     &                 tab,pzz,pzj,cotdl,almat,                         &
     &                 mtasa,iprint,istop)
        implicit real*8 (a-h,o-z)
        integer, intent(in) :: nrelv
        real(8), intent(in) :: clight
        integer, intent(in) :: lmax,kkrsz
!!        integer, intent(in) :: icryst
!!        real(8), intent(in) :: alat
        complex(8), intent(in) :: energy
        complex(8), intent(in) :: pnrel
        integer, intent(in) :: iswzj
        real(8), intent(in) :: vr(:)  ! (iprpts)
        real(8), intent(in) :: xr(:)  ! (iprpts)
        real(8), intent(in) :: rr(:)  ! (iprpts)
        real(8), intent(in) :: h
        integer, intent(in) :: jmt,jws
        real(8), intent(in) :: rmt
        real(8), intent(in) :: rws
        real(8), intent(in) :: rmt_true
        real(8), intent(in) :: r_circ
        integer, intent(in) :: ivar_mtz
        integer fcount
        real(8), intent(in) :: weight(:)      ! (MNqp)
        real(8), intent(in) :: rmag(:,:,:,:)  ! (MNqp,MNqp,MNqp,MNF)
        real(8), intent(in) :: vj(:,:,:,:)    ! (MNqp,MNqp,MNqp,MNF)
        logical, intent(in) :: lVP
        complex(8), intent(out) :: zlab(:,:)  ! (iprpts,iplmax+1)
        complex(8), intent(out) :: tab(:,:)   ! (ipkkr,ipkkr)
        complex(8), intent(out) :: pzj(:,:)   ! (ipkkr,ipkkr)
        complex(8), intent(out) :: pzz(:,:)   ! (ipkkr,ipkkr)
        complex(8), intent(out) :: cotdl(:)   ! (iplmax+1)
        complex(8), intent(out) :: almat(:)   ! (iplmax+1)
        integer, intent(in) :: mtasa,iprint
        character(10), intent(in) :: istop
        end subroutine grint
      end interface
!
      interface
        subroutine intgrtau(                                            &
     &                 lmax,kkrsz,nbasis,numbsub,atcon,mxcomp,komp,     &
     &                 rytodu,aij,itype,natom,iorig,itype1,             &
     &                 if0,mapstr,mappnt,ndimbas,mapij,                 &
     &                 powe,                                            &
     &                 edu,pdu,                                         &
     &                 irecdlm,                                         &
     &                 rsn,ndimrs,                                      &
     &                 np2r,ndimnp,numbrs,naij,                         &
     &                 dqint,ndimdqr,                                   &
     &                 hplnm,ndimr,ndimlm,                              &
     &                 tcpa,                                            &
     &                 tau00,                                           &
     &                 dop,nop,d00,eoeta,                               &
     &                 eta,                                             &
     &                 xknlat,ndimks,R2ksp,                             &
     &                 conr,nkns,                                       &
     &                 qmesh,nqpt,lwght,lrot,                           &
     &                 ngrp,kptgrp,kptset,kptindx,                      &
     &                 rslatt,Rnncut,Rsmall,mapsprs,                    &
     &                 r_ref,                                           &
     &                 iprint,istop)
        implicit none
        integer, intent(in) :: lmax,kkrsz,nbasis
        integer, intent(in) :: numbsub(nbasis),komp(nbasis),mxcomp
        real(8), intent(in) :: atcon(mxcomp,nbasis),rytodu
        integer, intent(in) :: naij
        real(8), intent(in) :: aij(3,naij)
        integer, intent(in) :: natom,itype(natom)
        integer, intent(in) :: iorig(nbasis),itype1(natom)
        integer, intent(in) :: if0(48,natom)
        integer, intent(in) :: ndimbas
        integer, intent(in) :: mapstr(ndimbas,natom)
        integer, intent(in) :: mappnt(ndimbas,natom)
        integer, intent(in) :: mapij(2,*)
        complex(8), intent(in) :: powe(*)
        complex(8), intent(in) :: edu
        complex(8), intent(in) :: pdu
        integer, intent(in) :: irecdlm,ndimrs
        real(8), intent(in) :: rsn(ndimrs,4)
        integer, intent(in) :: ndimnp
        integer, intent(in) :: np2r(ndimnp,*),numbrs(*)
        integer, intent(in) :: ndimdqr
        complex(8), intent(in) :: dqint(ndimdqr,*)
        integer, intent(in) :: ndimr,ndimlm
        complex(8), intent(in) :: hplnm(ndimr,ndimlm,*)
        complex(8) :: tcpa(:,:,:)
        complex(8) :: tau00(:,:,:)
        integer, intent(in) :: nop
        complex(8), intent(in) :: dop(kkrsz,kkrsz,nop)
        complex(8), intent(in) :: d00,eoeta
        real(8), intent(in) :: eta
        integer, intent(in) :: ndimks,nkns
        real(8), intent(in) :: xknlat(ndimks,3),R2ksp
        complex(8), intent(in) :: conr(*)
        integer, intent(in) :: nqpt
        real(8), intent(in) :: qmesh(3,nqpt)
        integer, intent(in) :: lwght(nqpt),lrot(*)
        integer, intent(in) :: ngrp,kptgrp(ngrp+1)
        integer, intent(in) :: kptset(ngrp),kptindx(nqpt)
        real(8), intent(in) :: rslatt(3,3)           ! Alat*rslatt(1..3,*)/(2*pi) -- in at.un.
        real(8), intent(in) :: Rnncut,Rsmall         ! Alat*R/(2*pi) -- in at.unit.
        integer, intent(in) :: mapsprs(natom,natom)
        real(8), intent(in) :: r_ref(:)    ! rws(:) ;   r_ref is a radius for ref.system (screening)
        integer, intent(in) :: iprint
        character(10), intent(in) :: istop
        end subroutine intgrtau
      end interface
!
      interface
        subroutine iterateSCF( mecca_state )
        use mecca_types, only : RunState
        type(RunState), intent(inout), target :: mecca_state
        end subroutine iterateSCF
      end interface
!
      interface
        subroutine genvec(bas,Rvsn,point,npoint,                        &
     &                  vsnlat,ndimv,nvsn,iprint)
        implicit none
        integer, intent(in) :: npoint
        real(8), intent(in) :: bas(3,3),Rvsn,point(3,*)
        integer :: ndimv
        real(8), allocatable :: vsnlat(:)
!        real(8), intent(inout) :: vsnlat(ndimv,3)
        integer, intent(in) :: nvsn
        integer, intent(in) :: iprint
!        integer, optional :: memsize
        end subroutine genvec
      end interface
!
      interface
        subroutine keep1(alat,boa,coa,atcon,zID,                        &
     &                nbasis,komp,nspin,                                &
     &                fetot,Temp,press,npts,nq,                         &
     &                rms,efermi,vmtz,vdif,                             &
     &                head,xc_descr,                                    &
     &                xvalws,zvalav,                                    &
     &                imix,alpha,beta,                                  &
     &                nkeep,                                            &
     &                iprint,istop)
        implicit none
        real(8), intent(in) :: alat,boa,coa
        integer, intent(in) :: nbasis
        real(8), intent(in) :: atcon(:,:)
        integer, intent(in) :: zID(:,:)
        integer, intent(in) :: komp(nbasis)
        integer, intent(in) :: nspin
        real(8), intent(in) :: fetot,Temp,press
        integer, intent(in) :: npts,nq(:,:)
        real(8), intent(in) :: rms,efermi,vmtz,vdif
        character(32), intent(in) :: head
        character(*),  intent(in) :: xc_descr
        real(8), intent(in) :: xvalws(:,:,:)
        real(8), intent(in) :: zvalav
        integer, intent(in) :: imix
        real(8), intent(in) :: alpha,beta
        integer, intent(in) :: nkeep,iprint
        character(10), intent(in) :: istop
        end subroutine keep1
      end interface
!
      interface
        subroutine latvect(r_bas,bases,aij,k_bas,nbasis,                &
     &                     qmesh,ndimq,nmesh,nqpt,                      &
     &                     Rksp,xknlat,ndimk,nksn,                      &
     &                     Rrsp,xrnlat,ndimr,nrsn,                      &
     &                     iprint)
        implicit none
        integer, intent(in) ::  nbasis
        real(8), intent(in) ::  r_bas(3,3),bases(3,nbasis),k_bas(3,3)
        real(8), intent(out) :: aij(3,*)
        integer, intent(in) ::  nmesh,ndimq
        integer, intent(in) :: nqpt(nmesh)
        real(8), intent(in) :: qmesh(3,ndimq,*)
        real(8), intent(in) ::  Rksp,Rrsp
        integer :: nksn(nmesh),nrsn
        real(8), allocatable :: xknlat(:)  ! (ndimk,3,nmesh)
        integer :: ndimk,ndimr
        real(8), allocatable :: xrnlat(:)    ! (ndimr,3)
        integer, intent(in) :: iprint
        end subroutine latvect
      end interface
!
      interface
        subroutine mdos(dost,zz,zj,w1,kkrsz,iprint,istop)
        implicit none
        complex(8), intent(out) :: dost(0:)
        integer, intent(in) ::    kkrsz
        complex(8), intent(in) :: zz(:,:)
        complex(8), intent(in) :: zj(:,:)
        complex(8), intent(in) :: w1(:,:)
        integer, intent(in) ::    iprint
        character(10), intent(in) :: istop
        end subroutine mdos
      end interface
!
      interface
        subroutine mgreen(green,w1,kkrsz,                               &
     &                  nrelv,clight,                                   &
     &                  lmax,                                           &
     &                  energy,prel,                                    &
     &                  h,jmt,jws,rr,vr,                                &
     &                  iswzj,                                          &
     &                  iprint,istop)
        implicit none
        complex(8), intent(out) :: green(:)  ! (iprpts)
        complex(8), intent(in)  :: w1(:,:)   ! (ipkkr,ipkkr)
        integer, intent(in) ::    kkrsz
        integer, intent(in) ::    nrelv
        real(8), intent(in) ::     clight
        integer, intent(in) ::    lmax
        complex(8), intent(in) :: energy
        complex(8), intent(in) :: prel
        real(8), intent(in) ::     h
        integer, intent(in) ::    jmt
        integer, intent(in) ::    jws
        real(8), intent(in) ::     vr(:)  ! (iprpts)
        real(8), intent(in) ::     rr(:)  ! (iprpts)
        integer, intent(in) ::    iswzj
        integer, intent(in) ::    iprint
        character(10), intent(in) ::  istop
        end subroutine mgreen
      end interface
!
      interface
        subroutine RStau00(                                             &
     &                 lmax,ndkkr,nbasis,                               &
     &                 alat,aij,itype,natom,iorig,                      &
     &                 mapstr,ndimbas,                                  &
     &                 pdu,                                             &
     &                 naij,                                            &
     &                 tcpa,                                            &
     &                 tau00,                                           &
     &                 rslatt,Rnncut,Rsmall,                            &
     &                 r_ref,                                           &
     &                 isprs,                                           &
     &                 iprint,istop)
!c     =================================================================
!c
        implicit none
        integer, intent(in) :: lmax,ndkkr
        integer, intent(in) :: nbasis
        real(8), intent(in) :: alat
        integer, intent(in) :: naij
        real(8), intent(in) :: aij(3,naij)
        integer, intent(in) :: natom,itype(natom)
        integer, intent(in) :: iorig(nbasis)
        integer, intent(in) :: ndimbas
        integer, intent(in) :: mapstr(ndimbas,natom)
        complex(8), intent(in) :: pdu
        complex(8) :: tcpa(ndkkr,ndkkr,natom)
        complex(8), intent(out) :: tau00(ndkkr,ndkkr,nbasis)
        real(8), intent(in) :: rslatt(3,3)           ! Alat*rslatt(1..3,*)/(2*pi) -- in at.un.
        real(8), intent(in) :: Rnncut,Rsmall         ! Alat*R/(2*pi) -- in at.unit.
        real(8), intent(in) :: r_ref(:)
!        complex(8), intent(out) :: deltat(ndkkr,ndkkr,*)
!        complex(8), intent(out) :: deltinv(ndkkr,ndkkr,*)
        integer, intent(in) :: isprs(:)
        integer, intent(in) :: iprint
        character(10), intent(in) :: istop
        end subroutine RStau00
      end interface
!
      interface
        subroutine scalar(nrelv,clight,                                 &
     &                  l,                                              &
     &                  bjl,bnl,g,f,gpl,fpl,                            &
     &                  tandel,cotdel,anorm,alpha,                      &
     &                  energy,prel,                                    &
     &                  rv,r,r2,h,jmt,iend,                             &
     &                  iswzj,                                          &
     &                  iprint,istop)
        implicit none
        integer, intent(in) :: nrelv,l
        real(8), intent(in) :: clight
        complex(8) :: bjl(:,:)
        complex(8) :: bnl(:,:)
        complex(8) :: g(:)
        complex(8) :: f(:)
        complex(8) :: gpl(:)
        complex(8) :: fpl(:)
        complex(8) :: cotdel, tandel, anorm, alpha
        complex(8), intent(in) :: energy,prel
        real(8), intent(in) :: rv(:)
        real(8), intent(in) :: r(:)
        real(8), intent(in) :: r2(:),h
        integer, intent(in) :: jmt,iend,iswzj,iprint
        character(10), intent(in) :: istop
        end subroutine scalar
      end interface
!
      interface
        subroutine scf_atom_pot(ndrpts,jmt,jws,h,rr,xr,ztot,vr,v0,      &
     &                  totrho,corden,nspin,numc,nc,lc,kc,ec,mtasa,zcor,&
     &                  zval,iprints,istop)
        implicit none
        integer ndrpts,jmt,jws,nspin,numc,mtasa,iprints
        real*8  h,rr(ndrpts),xr(ndrpts)
        real*8  ztot
        real*8  vr(ndrpts),v0
        real*8  totrho(ndrpts)
        real*8  corden(ndrpts)
        integer nc(:),lc(:),kc(:)
        real*8  ec(:)
        real*8  zcor,zval
        character*(*) istop
        end subroutine scf_atom_pot
      end interface
!
      interface
        subroutine semrel(NRELV,CLIGHT,LMAX,ENERGY,PNREL,G,TMATL,       &
     &COTDL,ALMAT,ZLZL,ZLJL,H,JMT,JWS,X,R,RV,RMT_TRUE,R_CIRC,IVAR_MTZ,  &
     &                     FCOUNT,WEIGHT,RMAG,VJ,lVP,ISWZJ,IPRINT,ISTOP)
        integer :: NRELV
        real(8) :: CLIGHT
        integer :: LMAX
        complex(8) :: ENERGY
        complex(8) :: PNREL
        complex(8) :: G(:,:)  ! (iprpts,lmax+1)
        complex(8) :: TMATL(lmax+1)
        complex(8) :: COTDL(lmax+1)
        complex(8) :: ALMAT(lmax+1)
        complex(8) :: ZLZL(lmax+1)
        complex(8) :: ZLJL(lmax+1)
        real(8) :: H
        integer :: JMT
        integer :: JWS
        real(8) :: X(:)
        real(8) :: R(:)
        real(8) :: RV(:)
        real(8) :: RMT_TRUE
        real(8) :: R_CIRC
        integer :: IVAR_MTZ
        integer :: FCOUNT
        real(8) :: WEIGHT(:)
        real(8) :: RMAG(:,:,:,:)
        real(8) :: VJ(:,:,:,:)
        logical, intent(in) :: lVP
        integer :: ISWZJ
        integer :: IPRINT
        character(10) :: ISTOP
        end subroutine semrel
      end interface
!
      interface
        subroutine semrelzj(nrelv,clight,                               &
     &                  lmax,                                           &
     &                  energy,pnrel,                                   &
     &                  zlr,jlr,                                        &
     &                  h,jmt,jws,r,rv,                                 &
     &                  iswzj,                                          &
     &                  iprint,istop)
        implicit none
        integer, intent(in) :: nrelv,lmax
        real(8), intent(in) :: clight
        complex(8), intent(in) :: energy,pnrel
        complex(8) :: zlr(:,0:)  ! (iprpts,0:iplmax)
        complex(8) :: jlr(:,0:)  ! (iprpts,0:iplmax)
        real(8), intent(in) :: h
        integer, intent(in) :: jmt,jws,iswzj,iprint
        real(8), intent(in) :: r(:)
        real(8), intent(in) :: rv(:)
        character(10), intent(in) :: istop
        end subroutine semrelzj
      end interface
!
      interface
        subroutine tote1(vrnew,rho,corden,xr,rr,                        &
     &                jmt,nbasis,nspin,iexch,                           &
     &                numbsub,komp,atcon,ztotss,omegws,                 &
     &                excort,qintex,emtc,emad,emadp,                    &
     &                evalsum,ecorv,esemv,                              &
     &                etot,press,Eccsum,mtasa,iprint,istop)
        implicit none
        real(8), intent(in) :: vrnew(:,:,:,:)  ! (iprpts,ipcomp,ipsublat,ipspin)
        real(8), intent(in) :: rho(:,:,:,:)  ! (iprpts,ipcomp,ipsublat,ipspin)
        real(8), intent(in) :: corden(:,:,:,:)  ! (iprpts,ipcomp,ipsublat,ipspin)
        real(8), intent(in) :: xr(:,:,:)  ! (iprpts,ipcomp,nbasis)
        real(8), intent(in) :: rr(:,:,:)  ! (iprpts,ipcomp,nbasis)
        integer, intent(in) :: nbasis
        integer, intent(in) :: jmt(:,:)  ! (ipcomp,nbasis)
        integer, intent(in) :: nspin,iexch
        integer, intent(in) :: komp(nbasis),numbsub(nbasis)
        real(8), intent(in) :: atcon(:,:)  ! (ipcomp,nbasis)
        real(8), intent(in) :: ztotss(:,:)  ! (ipcomp,nbasis)
        real(8), intent(in) :: omegws
        real(8), intent(in) :: excort(nspin)
        real(8), intent(in) :: qintex(nspin)
        real(8), intent(in) :: emtc  !  MT: ES energy due to rho_inter; ASA: rho_inter (MT) correction
        real(8), intent(in) :: emad
        real(8), intent(in) :: emadp
        real(8), intent(in) :: evalsum(:,:,:)  ! (ipcomp,ipsublat,ipspin)
        real(8), intent(in) :: ecorv(:,:,:)  ! (ipcomp,ipsublat,ipspin)
        real(8), intent(in) :: esemv(:,:,:)  ! (ipcomp,ipsublat,ipspin)
        real(8), intent(out) :: etot,press
        real(8), intent(in) :: Eccsum
        integer, intent(in) :: mtasa,iprint
        character(10), intent(in) ::  istop
        end subroutine tote1
      end interface
!
      interface
        subroutine calcCoulombShC(nface,mpoint,faceVrtxs,rws,nmom,      &
     &                                           rmomm1,energy,ylmRmom)
        implicit none
          integer, intent(in)  :: nface
          integer, intent(in)  :: mpoint(nface)
          real(8), intent(in)  :: faceVrtxs(nface,3,*)
          real(8), intent(in)  :: rws
          integer, intent(in)  :: nmom
          real(8), intent(out), optional :: rmomm1
          real(8), intent(out), optional :: energy
          complex(8), intent(out), optional :: ylmRmom(:,:)
        end subroutine calcCoulombShC
      end interface
!
      interface
       subroutine calcCoulombShC1(nmom,nsub,aunit,nnb,xyznb,itypnb,vp_j)
       use mecca_types, only : VP_data
       implicit none
          integer, intent(in)  :: nmom
          integer, intent(in)  :: nsub
          real(8), intent(in)  :: aunit
          integer, intent(in)  :: nnb
          real(8), intent(in)  :: xyznb(3,nnb)
          integer, intent(in)  :: itypnb(nnb)
          type (VP_data), pointer :: vp_j
       end subroutine calcCoulombShC1
      end interface
!
      interface
        subroutine ws_integral(mecca_state,ic,nsub,rho,q_ws,q_mt,R_mt)
        use mecca_types, only : RunState
        implicit none
          type(RunState), intent(inout), target :: mecca_state
          integer, intent(in) :: ic,nsub
          real(8), intent(in) :: rho(:)
          real(8), intent(out) :: q_ws,q_mt
          real(8), optional, intent(in) :: R_mt
        end subroutine ws_integral
      end interface
!
      interface
        subroutine asa_integral(mecca_state,ic,nsub,rho,q_ws,q_mt,R_mt)
        use mecca_types, only : RunState
        implicit none
          type(RunState), intent(inout), target :: mecca_state
          integer, intent(in) :: ic,nsub
          real(8), intent(in) :: rho(:)
          real(8), intent(out) :: q_ws,q_mt
          real(8), optional, intent(in) :: R_mt
        end subroutine asa_integral
      end interface
!
!      interface
!        function ylag(xi,x,y,ind1,n1,imax,iex)
!        real(8) :: ylag
!        real(8), intent(in)  :: xi
!        integer, intent(in)  :: imax
!        real(8), intent(in)  :: x(imax),y(imax)
!        integer, intent(in)  :: ind1,n1
!        integer, intent(out) :: iex
!        end function ylag
!      end interface
!
!      interface
!        function ylag_cmplx(xi,x,y,ind1,n1,imax,iex)
!        complex(8) :: ylag_cmplx
!        real(8), intent(in)  :: xi
!        integer, intent(in)  :: imax
!        real(8), intent(in)  :: x(imax)
!        complex(8), intent(in)  :: y(imax)
!        integer, intent(in)  :: ind1,n1
!        integer, intent(out) :: iex
!        end function ylag_cmplx
!      end interface
!
!***********************************************************************
      end module mecca_interface
!
!BOP
!!MODULE: egrid_interface
!!INTERFACE:
      module egrid_interface
!!DESCRIPTION:
! fortran interface provider for:
! {\bv
! subroutine setEgrid
! subroutine conbox
! \ev}
!
!!REVISION HISTORY:
! Initial version - A.S. - 2013
!EOP
!***********************************************************************
      interface
        subroutine setEgrid( mecca_state, etop, Tempr )
        use mecca_types, only : RunState
        implicit none
        type(RunState), intent(inout), target :: mecca_state
        real(8), intent(in) :: etop
        real(8), intent(in) :: Tempr
        end subroutine setEgrid
      end interface
!
      interface
        subroutine conbox(ebot,etop,eitop,eibot,epar,egrd,nume,npar,    &
     &                  dele1,npts,iprint,istop)
        implicit none
        real(8), intent(inout) :: ebot,eitop
        real(8), intent(in) :: etop,eibot,epar
        integer :: nume,npar,npts
        integer, intent(in) :: iprint
        complex(8), intent(out) :: egrd(:)
        complex(8), intent(out) :: dele1(:)
        character(10), intent(in) :: istop
        end subroutine conbox
      end interface
!
!***********************************************************************
      end module egrid_interface
!
!
!BOP
!!MODULE: gettau_interface
!!INTERFACE:
      module gettau_interface
!!DESCRIPTION:
! fortran interface provider for:
! {\bv
! subroutine gettau_ray
! subroutine gf2dos
! \ev}
!
!!REVISION HISTORY:
! Initial version - A.S. - 2013
!EOP
!***********************************************************************
      interface
        subroutine gettau_ray(ispin,nspin,nrelv,                        &
!!     &                  icryst,                                       &
     &                  lmax,kkrsz,nbasis,numbsub,                      &
     &                  alat,volume,aij,itype,natom,                    &
     &                  if0,mapstr,mappnt,ndimbas,mapij,                &
     &                  komp,atcon,                                     &
     &                  xknlat,Rksp0,nkns,                              &
     &                  qmesh,ndimq,nmesh,nqpt,                         &
     &                  lwght,lrot,                                     &
     &                  ngrp,kptgrp,kptset,ndrot,kptindx,               &
     &                  sublat,r_ref,                                   &
     &                  rws,                                            &
     &                  rmt_true,r_circ,ivar_mtz,                       &
     &                  fcount,weight,rmag,vj,lVP,                      &
     &                  energy,                                         &
     &                  itmax,rns,nrns,ndimrs,                          &
     &                  np2r,ndimnp,numbrs,naij,                        &
     &                      rot,dop,nop,                                &
     &                  hplnm,ndimrhp,ndimlhp,                          &
     &                  irecdlm,                                        &
     &                  dos,dosck,green,                                &
     &                  eneta,enlim,                                    &
     &                  conk,conr,                                      &
     &                  rslatt,kslatt,Rnncut,Rsmall,mapsprs,            &
     &                  mtasa,ksintsch,efermi,                          &
     &                  iprint,istop                                    &
     &                 ,idosplot,bsf_box,fs_box)
        use mecca_types, only : Sublattice
        use mecca_types, only : DosBsf_data,FS_data
        implicit none
        integer, intent(in) :: ispin,nspin,nrelv
!!        integer, intent(in) :: icryst
        integer, intent(in) :: lmax,kkrsz,nbasis,numbsub(nbasis)
        real(8), intent(in) :: alat,volume
        integer, intent(in) :: naij
        real(8), intent(in) :: aij(3,naij)
        integer, intent(in) :: natom,itype(natom)
        integer, intent(in) :: if0(48,natom)
        integer, intent(in) :: ndimbas
        integer, intent(in) :: mapstr(ndimbas,natom)
        integer, intent(in) :: mappnt(ndimbas,natom)
        integer, intent(in) :: mapij(2,naij)
        integer, intent(in) :: komp(nbasis)
        real(8), intent(in) :: atcon(:,:)       ! (ipcomp,nbasis)
        real(8), intent(in) :: xknlat(*),Rksp0
        integer, intent(in) :: nmesh,nkns(nmesh)
        integer, intent(in) :: ndimq
        real(8), intent(in) :: qmesh(3,ndimq,nmesh)
        integer, intent(in) :: nqpt(nmesh)
        integer, intent(in) :: lwght(ndimq,nmesh),lrot(ndimq,nmesh)
        integer, intent(in) :: ngrp(nmesh),kptindx(ndimq,nmesh)
        integer, intent(in) :: ndrot,kptgrp(ndrot+1,nmesh)
        integer, intent(in) :: kptset(ndrot,nmesh)
        type(Sublattice), intent(in), target :: sublat(nbasis)
        real(8), intent(in) :: r_ref(:)   ! reference (screening) radius
        real(8), intent(in) :: rws(nbasis)
        real(8), intent(in) :: rmt_true(:,:)    !  (ipcomp,nbasis)
        real(8), intent(in) :: r_circ(:,:)      !  (ipcomp,nbasis)
        integer, intent(in) :: ivar_mtz
        integer, intent(in) :: fcount(:)          !  (nbasis)
        real(8), intent(in) :: weight(:)        ! (MNqp)
        real(8), intent(in) :: rmag(:,:,:,:,:)  ! (MNqp,MNqp,MNqp,MNF,nbasis)
        real(8), intent(in) :: vj(:,:,:,:,:)    ! (MNqp,MNqp,MNqp,MNF,nbasis)
        logical, intent(in) :: lVP  ! if .true., VP is applied
        complex(8), intent(in) :: energy
        integer, intent(in) :: itmax
        integer, intent(in) :: ndimrs,nrns
        real(8), intent(in) :: rns(ndimrs,4)
        integer, intent(in) :: ndimnp
        integer, intent(in) :: np2r(ndimnp,naij),numbrs(naij)
        real(8), intent(in) :: rot(49,3,3)
        integer, intent(in) :: nop
        complex(8), intent(in) :: dop(kkrsz,kkrsz,nop)
        integer, intent(in) :: ndimrhp,ndimlhp
        complex(8), intent(in) :: hplnm(ndimrhp,ndimlhp,*)
        integer, intent(in) :: irecdlm
!
        complex(8) :: dos(0:,:,:)  ! (0:iplmax,ipcomp,nbasis)
        complex(8) :: dosck(:,:)   ! (ipcomp,nbasis)
        complex(8) :: green(:,:,:) ! (iprpts,ipcomp,nbasis)
!
        real(8), intent(in) :: eneta, enlim(2,nmesh-1)
        real(8), intent(in) :: conk
        complex(8), intent(in) :: conr((2*lmax+1)**2)
        real(8), intent(in) :: rslatt(3,3)           ! Alat*rslatt(1..3,*)/(2*pi) -- latt.vect.
        real(8), intent(in) :: kslatt(3,3)           ! Alat*rslatt(1..3,*)/(2*pi) -- latt.vect.
        real(8), intent(in) :: Rnncut,Rsmall
        integer, intent(in) :: mapsprs(:,:)
!
        integer, intent(in) :: mtasa
        integer, intent(in) :: ksintsch ! k-space integration scheme
        real(8), intent(in) :: efermi
        integer, intent(in) :: iprint
        character(10), intent(in) :: istop
!
        integer, intent(in) :: idosplot
        type (DosBsf_data), pointer, intent(in) :: bsf_box
        type (FS_data),  pointer, intent(in) :: fs_box
        end subroutine gettau_ray
      end interface
!
      interface
         subroutine gf2dos(ispin,green)
         implicit none
         integer, intent(in) :: ispin
         complex(8), intent(in)  :: green(:,:,:) ! (iprpts,ipcomp,nbasis)
         end  subroutine gf2dos
      end interface
!
!***********************************************************************
      end module gettau_interface
!
!BOP
!!MODULE: gfncts_interface
!!INTERFACE:
      module gfncts_interface
!!DESCRIPTION:
! fortran interface provider for:
! {\bv
! subroutine g_ecor
! function g_esemicorr
! subroutine g_meshv
! subroutine g_vr
! subroutine g_komp
! subroutine g_atcon
! function g_numNonESsites
! subroutine s_vr
! subroutine g_qsub
! subroutine g_rho
! subroutine s_rho
! subroutine g_rhocor
! subroutine g_rhoval
! subroutine g_rhosemicore
! subroutine s_totchg
! subroutune g_reltype
! function g_zvaltot
! function g_zsemcout
! function g_zsemckkr
! function g_efestim
! subroutine c_v0
! function gNonMT
! function addFEforEf
! function gMaxErr
! function gInterstlRho
! \ev}
!
!!REVISION HISTORY:
! Initial version - A.S. - 2013
!EOP
!***********************************************************************
      interface
        subroutine g_ecor(v_rho_box,is,ebot,ecor,esemc,tempr_in,efermi, &
     &                                                           nspin)
        use mecca_types, only : SphAtomDeps
        implicit none
        type(SphAtomDeps), intent(in) :: v_rho_box
        integer, intent(in) :: is
        real(8), intent(in) :: ebot
        real(8), intent(out) :: ecor
        real(8), intent(out) :: esemc
        real(8), intent(in),optional :: tempr_in
        real(8), intent(in),optional :: efermi
        integer, intent(in),optional :: nspin
        end subroutine g_ecor
      end interface
!
      interface
        function g_esemicorr(ic,nsub,is)
        implicit none
        real(8) :: g_esemicorr
        integer, intent(in) :: ic,nsub,is
        end function g_esemicorr
      end interface
!
      interface
        subroutine g_meshv(v_rho_box,h,xr,rr,is,vr)
           use mecca_types, only : SphAtomDeps
           implicit none
           type(SphAtomDeps), intent(in) :: v_rho_box
           integer, intent(in), optional :: is
           real(8), intent(out) :: h
           real(8), intent(out) :: xr(:),rr(:)
           real(8), intent(out), optional :: vr(:)
        end subroutine g_meshv
      end interface
!
      interface
        subroutine g_vr(v_rho_box,is,vr,v0)
        use mecca_types, only : SphAtomDeps
        implicit none
        type(SphAtomDeps), intent(in) :: v_rho_box
        integer, intent(in)  :: is
        real(8), intent(out) :: vr(:)
        real(8), intent(out) :: v0
        end subroutine g_vr
      end interface
!
      interface
        subroutine g_komp(inFile, komp)
        use mecca_types, only : IniFile
          type(IniFile), intent(in) :: inFile
          integer, allocatable :: komp(:)
        end subroutine g_komp
      end interface
!
      interface
        subroutine g_atcon(inFile, atcon)
        use mecca_types, only : IniFile
          implicit none
          type(IniFile), intent(in) :: inFile
          real(8), allocatable :: atcon(:,:)
        end subroutine g_atcon
      end interface
!
      interface
        function g_numNonESsites( inFile )
        use mecca_types, only : IniFile
          type(IniFile), intent(in), pointer :: inFile
          real(8)  :: g_numNonESsites
        end function g_numNonESsites
      end interface
!
      interface
        subroutine s_vr(v_rho_box,is,vr,v0)
        use mecca_types, only : SphAtomDeps
          type(SphAtomDeps) :: v_rho_box
          integer, intent(in)  :: is
          real(8), intent(in) :: vr(:)
          real(8), intent(in) :: v0
        end subroutine s_vr
      end interface
!
      interface
        subroutine g_qsub(sublat,nspin,qsub,qeff)
        use mecca_types, only : Sublattice
          type(Sublattice), pointer, intent(in) :: sublat
          integer, intent(in)  :: nspin
          real(8), intent(out) :: qsub
          real(8), intent(out) :: qeff(:)
        end subroutine g_qsub
      end interface
!
      interface
        subroutine g_rho(v_rho_box,is,rho)
        use mecca_types, only : SphAtomDeps
          type(SphAtomDeps), intent(in) :: v_rho_box
          integer, intent(in)  :: is
          real(8), intent(out) :: rho(:)
        end subroutine g_rho
      end interface
!
      interface
        subroutine s_rho(v_rho_box,is,rho,add)
        use mecca_types, only : SphAtomDeps
          type(SphAtomDeps) :: v_rho_box
          integer, intent(in) :: is
          real(8), intent(in) :: rho(:)
          integer, intent(in), optional :: add
        end subroutine s_rho
      end interface
!
      interface
        subroutine g_rhocor(v_rho_box,is,rho)
        use mecca_types, only : SphAtomDeps
          type(SphAtomDeps), intent(in) :: v_rho_box
          integer, intent(in)  :: is
          real(8), intent(out) :: rho(:)
        end subroutine g_rhocor
      end interface
!
      interface
        subroutine g_rhoval(v_rho_box,is,rho)
        use mecca_types, only : SphAtomDeps
          type(SphAtomDeps), intent(in) :: v_rho_box
          integer, intent(in)  :: is
          real(8), intent(out) :: rho(:)
        end subroutine g_rhoval
      end interface
!
      interface
        subroutine g_rhosemicore(ic,nsub,is,rho,mecca_state)
        use mecca_types, only : RunState
        implicit none
        integer, intent(in)  :: ic,nsub,is
        real(8), intent(out) :: rho(:)
        type(RunState), target, optional :: mecca_state
        end subroutine g_rhosemicore
      end interface
!
      interface
        subroutine s_totchg(v_rho_box,is,totchg,add)
        use mecca_types, only : SphAtomDeps
        implicit none
        type(SphAtomDeps), intent(inout) :: v_rho_box
        integer, intent(in)  :: is
        real(8), intent(in) :: totchg
        integer, intent(in), optional :: add
        end subroutine s_totchg
      end interface
!
      interface
        subroutine g_reltype(core,semicore,valence)
        implicit none
        integer, intent(out), optional :: core,semicore,valence
        end subroutine g_reltype
      end interface
!
      interface
        function g_zvaltot( inFile, non_zero_T )
        use mecca_types, only : IniFile, Work_data
        implicit none
        real(8) :: g_zvaltot
        type(IniFile), intent(in), pointer :: inFile
        logical, intent(in) :: non_zero_T
        end function g_zvaltot
      end interface
!
      interface
        function g_zsemcout( inFile, non_zero_T )
        use mecca_types, only : IniFile, Work_data
        implicit none
        real(8) :: g_zsemcout
        type(IniFile), intent(in), pointer :: inFile
        logical, intent(in) :: non_zero_T
        end function g_zsemcout
      end interface
!
      interface
        function g_zsemckkr( mecca_state )
        use mecca_types, only : RunState
        implicit none
        real(8) :: g_zsemckkr
        type(RunState), intent(in), target :: mecca_state
        end function g_zsemckkr
      end interface
!
      interface
        function g_efestim( Z, volume )
          real(8), intent(in) :: Z,volume
          real(8)  :: g_efestim
        end function g_efestim
      end interface
!
      interface
        subroutine c_v0( inFile, dv0 )
        use mecca_types, only : IniFile
          type(IniFile), intent(inout), pointer :: inFile
          real(8), intent(in) :: dv0(:)
        end subroutine c_v0
      end interface
!
      interface
        function gNonMT()
        logical gNonMT
        end function gNonMT
      end interface
!
      interface
        function addFEforEf()
        logical addFEforEf
        end function addFEforEf
      end interface
!
      interface
        function gMaxErr(iflag)
        real(8) :: gMaxErr
        integer, intent(in) :: iflag
        end function gMaxErr
      end interface
!
      interface
        function gInterstlRho(rs,zeta)
        real(8) :: gInterstlRho
        real(8), intent(out), optional :: rs,zeta
        end function gInterstlRho
      end interface
!
      interface
        function gRasa(nsub,ic)
        real(8) :: gRasa
        integer, intent(in) :: nsub
        integer, intent(in), optional :: ic
        end function gRasa
      end interface
!
      interface
        integer function g_Lmltpl(ifl)
        integer, intent(in), optional :: ifl
        end function g_Lmltpl
      end interface
!
      interface
        integer function g_Nmomr()
        end function g_Nmomr
      end interface
!
!***********************************************************************
      end module gfncts_interface
!
!BOP
!!MODULE: mtrx_interface
!!INTERFACE:
      module mtrx_interface
!!DESCRIPTION:
! fortran interface provider for:
! {\bv
! subroutine invmatr
! subroutine madd
! subroutine matinv
! subroutine mmul
! SUBROUTINE WRTDIA2
! subroutine wrtmtx
! \ev}
!
!!REVISION HISTORY:
! Initial version - A.S. - 2013
!EOP
!***********************************************************************
      interface
        subroutine invmatr(amt,kkrsz,nbasis,                            &
     &                   inverse,invparam,                              &
     &                   iprint,istop,logdet)
        implicit none
        complex(8) :: amt(*)
        integer, intent(in) :: kkrsz,nbasis
        integer, intent(in) ::  inverse,invparam,iprint
        character(10), intent(in) :: istop
        complex(8), intent(out), optional :: logdet
        end subroutine invmatr
      end interface
!
      interface
        subroutine madd(amt,const,bmt,cmt,kkrsz,iprint)
        implicit none
        real(8), intent(in) :: const
        complex(8), intent(in) :: amt(:,:)
        complex(8), intent(in) :: bmt(:,:)
        complex(8), intent(out) :: cmt(:,:)
        integer, intent(in) :: kkrsz
        integer, intent(in) :: iprint
        end subroutine madd
      end interface
!
      interface
        subroutine matinv(amt,bmt,w1,kkrsz,iprint,istop)
        implicit none
        integer kkrsz
        complex(8), intent(in) :: amt(:,:)
        complex(8), intent(out) :: bmt(:,:)
        complex(8) :: w1(*)    ! in fact, it is not used in this implementation
        integer :: iprint
        character(10), intent(in), optional :: istop
        end subroutine matinv
      end interface
!
!      INTERFACE
!        SUBROUTINE MATRIX(Y,X,I1,I2,NRMAT,KKRSZ,EXPDOT)
!          INTEGER :: KKRSZ
!          INTEGER :: NRMAT
!          COMPLEX(8) :: Y(KKRSZ,KKRSZ)
!          COMPLEX(8) :: X(NRMAT,*)
!          INTEGER :: I1
!          INTEGER :: I2
!          COMPLEX(8) :: EXPDOT
!        END SUBROUTINE MATRIX
!      END INTERFACE
!!
!      INTERFACE
!        SUBROUTINE MATRIX1(Y,X,I1,I2,NRMAT,KKRSZ,EXPDOT)
!          INTEGER :: KKRSZ
!          INTEGER :: NRMAT
!          REAL(8) :: Y(0:1,KKRSZ,KKRSZ)
!          REAL(8) :: X(NRMAT,*)
!          INTEGER :: I1
!          INTEGER :: I2
!          REAL(8) :: EXPDOT(0:1)
!        END SUBROUTINE MATRIX1
!      END INTERFACE
!
      interface
        subroutine mmul(amt,bmt,cmt,kkrsz)
        implicit none
        complex(8) :: amt(:,:)
        complex(8) :: bmt(:,:)
        complex(8) :: cmt(:,:)
        integer, intent(in) ::  kkrsz
        end subroutine mmul
      end interface
!
      INTERFACE
        SUBROUTINE WRTDIA2(X,KKRSZ,NBASIS,ISTOP)
          COMPLEX(8), INTENT(IN) :: X(:,:,:)
          INTEGER, INTENT(IN) :: KKRSZ
          INTEGER, INTENT(IN) :: NBASIS
          CHARACTER(LEN=10), INTENT(IN) :: ISTOP
        END SUBROUTINE WRTDIA2
      END INTERFACE
!
      interface
        subroutine wrtmtx(x,n,istop)
        implicit none
        integer, intent(in) :: n
        complex(8), intent(in) :: x(:,:)
        character(10), intent(in), optional :: istop
        end subroutine wrtmtx
      end interface
!
!***********************************************************************
      end module mtrx_interface
!
!BOP
!!MODULE: isoparintgr_interface
!!INTERFACE:
      module isoparintgr_interface
!!DESCRIPTION:
! fortran interface provider for:
! {\bv
! function isopar_volume
! SUBROUTINE ISOPAR_CHBINT
! SUBROUTINE ISOPAR_CHBINT_CMPLX
! \ev}
!
!!REVISION HISTORY:
! Initial version - A.S. - 2013
!EOP
!***********************************************************************
      interface
        function isopar_volume(fcount,vj)
        implicit none
        real(8) :: isopar_volume
        integer, intent(in) :: fcount
        real(8), intent(in) :: vj(:,:,:,:)
        end function isopar_volume
      end interface
!
      INTERFACE
        SUBROUTINE ISOPAR_CHBINT(IA,RMT_OVLP,NMX,RRS,FS,R_CIRC,         &
     &                                      FCOUNT,WEIGHT,RMAG,VJ,VPINT)
        integer :: NMX
        integer :: IA
        real(8) :: RMT_OVLP
        real(8) :: RRS(NMX)
        real(8) :: FS(NMX)
        real(8) :: R_CIRC
        integer :: FCOUNT
        real(8) :: WEIGHT(:)
        real(8) :: RMAG(:,:,:,:)
        real(8) :: VJ(:,:,:,:)
        real(8) :: VPINT(2)
        END SUBROUTINE ISOPAR_CHBINT
      END INTERFACE
!
      INTERFACE
        SUBROUTINE ISOPAR_CHBINT_CMPLX(IA,RMT_OVLP,NMX,RRS,FS,R_CIRC,   &
     &                                      FCOUNT,WEIGHT,RMAG,VJ,VPINT)
        integer :: NMX
        integer :: IA
        real(8) :: RMT_OVLP
        real(8) :: RRS(NMX)
        complex(8) :: FS(NMX)
        real(8) :: R_CIRC
        integer :: FCOUNT
        real(8) :: WEIGHT(:)
        real(8) :: RMAG(:,:,:,:)
        real(8) :: VJ(:,:,:,:)
        complex(8) :: VPINT
        END SUBROUTINE ISOPAR_CHBINT_CMPLX
      END INTERFACE
!
!***********************************************************************
      end module isoparintgr_interface
!
!BOP
!!MODULE: struct_interface
!!INTERFACE:
      module struct_interface
!!DESCRIPTION:
! fortran interface provider for:
! {\bv
! subroutine iostrf
! subroutine structure1
! \ev}
!
!!REVISION HISTORY:
! Initial version - A.S. - 2013
!EOP
!***********************************************************************
      interface
       subroutine iostrf(io,natom,itype,rslatt,basis,ba,ca,unit,invadd)
       implicit none
       integer, intent(in) :: io
       integer :: natom,invadd
       integer, allocatable :: itype(:)
       real(8), allocatable :: basis(:,:)
       real(8) :: rslatt(3,3),ba,ca,unit
       end subroutine iostrf
      end interface
!
      interface
        subroutine structure1(alat,boa,coa,nbasis,                      &
     &                     rslatt,kslatt,bases,itype,aij,ndimbas,       &
     &                     numbsub,nsublat,                             &
     &                     mtasa,                                       &
     &                     if0,mapstr,mappnt,mapij,imethod,             &
     &                     omega,                                       &
     &                     imdlng,                                      &
     &                     invadd,                                      &
     &                     iyymom,                                      &
     &                     dop,lmax,rot,ib,nrot,idop,                   &
     &                     nq,qmesh,ndimq,nmesh,nqpt,ispkpt,            &
     &                     wghtq,twght,lrot,                            &
     &                     Rksp,xknlat,ndimk,nksn,                      &
     &                     Rrsp,xrnlat,ndimr,nrsn,                      &
     &                     xrslatij,nrhp,ndimrhp,                       &
     &                     np2r,numbrs,ncount,                          &
     &                     iprint,istop)
!c     ==================================================================
        implicit none
        real(8) :: alat,boa,coa   !  boa = B/A, coa = C/A
        integer, intent(in)  :: nbasis,ndimbas
        real(8), intent(in)  :: rslatt(3,3)
        real(8), intent(out) :: kslatt(3,3)
        real(8), intent(in)  :: bases(3,nbasis)
        integer :: itype(nbasis),nsublat,numbsub(nsublat)
        real(8), intent(out) :: aij(3,*)
        integer, intent(out) :: ncount
        integer, intent(in) :: mtasa
        integer, intent(out) :: if0(48,nbasis)
        integer, intent(out) :: mapstr(ndimbas,*)
        integer, intent(out) :: mappnt(ndimbas,*)
        integer, intent(out) :: mapij(2,*)
        integer :: imethod
        real(8), intent(out) :: omega
        integer :: imdlng,invadd,iyymom
        integer, intent(in) :: lmax
        complex(8), intent(out) ::                                      &
     &                        dop((lmax+1)*(lmax+1)*(lmax+1)*(lmax+1),*)
        integer :: nrot,idop
        real(8), intent(out) :: rot(49,3,3)
        integer, intent(out) :: ib(48)
        integer :: ndimq,nmesh,ispkpt
        integer, intent(out) :: nqpt(nmesh)
        integer, intent(out) :: nq(3,nmesh)
        real(8), intent(out) :: qmesh(3,ndimq,nmesh)
        integer, intent(out) :: wghtq(ndimq,nmesh)
        real(8), intent(out) :: twght(nmesh)
        integer, intent(out) :: lrot(ndimq,nmesh)
        real(8), intent(in)  :: Rksp,Rrsp
        integer :: ndimk,ndimr,ndimrhp
        integer, intent(out) :: nksn(nmesh)
        real(8), allocatable :: xknlat(:)  ! (ndimk*3)
        integer, intent(out) :: nrsn
        real(8), allocatable :: xrnlat(:)  ! (ndimr*3)
        integer, intent(out) :: nrhp
        real(8), allocatable :: xrslatij(:,:)  !  (ndimrhp,4)
        integer, allocatable :: np2r(:,:)
        integer, allocatable :: numbrs(:)
        integer, intent(in) :: iprint
        character(10) :: istop
        end subroutine structure1
      end interface
!
!***********************************************************************
      end module struct_interface
!
!BOP
!!MODULE: update_interface
!!INTERFACE:
      module update_interface
!!DESCRIPTION:
! fortran interface provider for:
! {\bv
! subroutine update_ptn
! subroutine update_rho
! \ev}
!
!!REVISION HISTORY:
! Initial version - A.S. - 2013
!EOP
!***********************************************************************
      interface
!DEBUG        subroutine update_ptn(fold,xvalws,fnew,xvalwsnw,ztot,           &
        subroutine update_ptn(fold,fnew,ztot,                           &
     &                     v0imtz,v0iold,rr,                            &
     &                     imix,alpha0,beta,                            &
     &                     komp,jend,nbasis,                            &
     &                     mspn,brfile1,brfile2                         &
     &                     )
        implicit none
        integer, intent(in) ::  nbasis,komp(nbasis),mspn
        integer, intent(in) ::  jend(nbasis)
        integer, intent(in) ::  imix
        real(8) ::   fold(:,:,:,:)  ! (iprpts,ipcomp,ipsublat,ipspin)
!DEBUG        real(8), intent(in) ::   xvalws(:,:,:)  ! (ipcomp,ipsublat,ipspin)
        real(8) ::   fnew(:,:,:,:)  ! (iprpts,ipcomp,ipsublat,ipspin)
!DEBUG        real(8) ::   xvalwsnw(:,:,:)! (ipcomp,ipsublat,ipspin)
        real(8), intent(in) ::   ztot(:,:)  ! (ipcomp,ipsublat)
        real(8), intent(in) ::   alpha0,beta
        real(8), intent(inout) ::   v0imtz(:)
        real(8), intent(in)    ::   v0iold(:)
        real(8), intent(in) :: rr(:,:,:)  ! (iprpts,ipcomp,ipsublat)
        character(*), intent(in) :: brfile1,brfile2
        end subroutine update_ptn
      end interface
!
      interface
        subroutine update_rho(fold,xvalws,fnew,xvalwsnw,                &
     &                     qtotmt,xr,                                   &
     &                     imix,alpha0,beta,                            &
     &                     komp,jmt,jend,nbasis,                        &
     &                     atcon,numbsub,mspn,                          &
     &                     mtasa,iprint,brfile1,brfile2)
!c    ===============================================================
        implicit none
        integer, intent(in) ::  nbasis,komp(nbasis),mspn
        integer, intent(in) ::  jmt(nbasis),jend(nbasis)
        integer, intent(in) ::  imix,mtasa,iprint
        integer, intent(in) ::  numbsub(nbasis)
        real(8) ::   fold(:,:,:,:)  ! (iprpts,ipcomp,ipsublat,ipspin)
        real(8), intent(in) ::   xvalws(:,:,:)  ! (ipcomp,ipsublat,ipspin)
        real(8) ::   fnew(:,:,:,:)  ! (iprpts,ipcomp,ipsublat,ipspin)
        real(8) ::   xvalwsnw(:,:,:)! (ipcomp,ipsublat,ipspin)
        real(8) ::   qtotmt(:,:,:)  ! (ipcomp,ipsublat,ipspin)
        real(8), intent(in) ::   atcon(:,:)     ! (ipcomp,nbasis)
        real(8), intent(in) :: xr(:,:,:)  ! (iprpts,ipcomp,ipsublat)
        real(8), intent(in) ::   alpha0,beta
        character(*), intent(in) :: brfile1,brfile2
        end subroutine update_rho
      end interface
!
!***********************************************************************
      end module update_interface
!
!BOP
!!MODULE: mecca_scf_interface
!!INTERFACE:
      module mecca_scf_interface
!!DESCRIPTION:
! fortran interface provider for:
! {\bv
! subroutine mecca_scf
! \ev}
!
!!REVISION HISTORY:
! Initial version - A.S. - 2013
!EOP
!***********************************************************************
       interface
         subroutine mecca_scf( mecca_state,outinfo )
         use mecca_types, only : RunState
         type (RunState), target :: mecca_state
         character*(*) outinfo           ! output info
         end subroutine mecca_scf
       end interface
!
!***********************************************************************
       end module mecca_scf_interface
!
!BOP
!!MODULE: chbint_interface
!!INTERFACE:
       module chbint_interface
!!DESCRIPTION:
! fortran interface provider for:
! {\bv
! function chebev
! \ev}
!
!!REVISION HISTORY:
! Initial version - A.S. - 2013
!EOP
!***********************************************************************
       interface
         function chebev(a,b,c,m,x)
         implicit none
         real(8) :: chebev
         integer, intent(in) :: m
         real(8), intent(in) :: a, b, x, c(m)
         end function chebev
       end interface
       end module chbint_interface
!
!***********************************************************************
