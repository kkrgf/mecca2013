!BOP
!!ROUTINE: getOrbNumbrs
!!INTERFACE:
       subroutine getOrbNumbrs(k,nc,lc,kc)
!!DESCRIPTION:
! generates atomic quantum numbers {\tt nc, lc, kc}
! for orbital {\tt k}
!
!!REVISION HISTORY:
! Initial Version - A.S. - 2005
!EOP
!
!BOC
       implicit none
       integer k
       integer nc,lc,kc
       integer nOrbToN
       nc = nOrbToN(k)
       call nOrbToLK(k,lc,kc)
       return
       end subroutine getOrbNumbrs

!BOP
!!ROUTINE: nValence
!!INTERFACE:
       integer function nValence(nZ)
!!DESCRIPTION:
! provides number of valence electrons for atom
! with atomic number {\tt nZ}
!
!!REVISION HISTORY:
! Initial Version - A.S. - 2005
!EOP
!
!BOC
       implicit none
       integer nZ
       integer noble(0:7)/0,2,10,18,36,54,86,118/
       integer nP
       integer, external :: nPeriod
       nValence = 0
       nP = nPeriod(nZ)-1
       if (nP.lt.0) return
       nValence = nZ - noble(nP)

       select case (nZ)
       case(25)
        nValence = 4
       case(26)
        nValence = 3
       case(27)
        nValence = 4
       case(28:30)
        nValence = 2
       case(31)
        nValence = 3
       case default
       end select


       if (nZ.le.31) return

       if (nZ.ge.32 .and. nZ.le.36) then
          nValence = nValence - 10
          return
       end if

       if ( nZ==44 ) nValence = 6
       if ( nZ==45 ) nValence = 6
       if ( nZ==46 ) nValence = 4

       if (nZ.ge.37 .and. nZ.le.49) return

       if (nZ.ge.50 .and. nZ.le.54) then
          nValence = nValence - 10
          return
       end if

       select case (nZ)
        case(55:58)
         nValence = nZ - noble(nP)
        case(59)
         nValence = 4
        case(60:71)
         nValence = 3
        case(72)
         nValence = 4
        case(73)
         nValence = 5
        case(74)
         nValence = 6
        case(75)
         nValence = 7
        case(76:78)
         nValence = 6
        case(79)
         nValence = 5
        case(80)
         nValence = 4
        case(81)
         nValence = 3
        case(82)
         nValence = 4
        case(83)
         nValence = 5
        case(84)
         nValence = 6
        case(85:86)
         nValence = nValence - 14 - 10
        case(90)
         nValence = 4
        case(91)
         nValence = 5
        case(92:94)
         nValence = 6
        case default
       end select
!DEBUG       if (nZ.ge.55 .and. nZ.le.57) then
!DEBUG          nValence = nValence + 6
!DEBUG          return
!DEBUG       end if
!DEBUG
!DEBUG       if (nZ.ge.58 .and. nZ.le.74) then
!DEBUG        return
!DEBUG       end if
!DEBUG
!DEBUG       if (nZ.ge.75 .and. nZ.le.82) then
!DEBUG        nValence = nValence - 14
!DEBUG        return
!DEBUG       end if

!        if (nZ.ge.83 .and. nZ.le.86) then
!        nValence = nValence - 14 - 10
!        end if
!        return
!       end if

       return
!EOC
       end function nValence

!BOP
!!ROUTINE: nPeriod
!!INTERFACE:
       integer function nPeriod(nZ)
!!DESCRIPTION:
! provides period number for atom
! with atomic number {\tt nZ}
!
!!REVISION HISTORY:
! Initial Version - A.S. - 2005
!EOP
!
!BOC
       implicit none
       integer nZ
       select case (nZ)
        case(0)
         nPeriod = 0
        case(1:2)
         nPeriod = 1
        case(3:10)
         nPeriod = 2
        case(11:18)
         nPeriod = 3
        case(19:36)
         nPeriod = 4
        case(37:54)
         nPeriod = 5
        case(55:86)
         nPeriod = 6
        case(87:118)
         nPeriod = 7
        case default
!         nPeriod = -1
         nPeriod = 8
       end select
       return
!EOC
       end function nPeriod

!DELETE!BOP
!DELETE!!ROUTINE: nOrbitals
!DELETE!!INTERFACE:
!DELETE       integer function nOrbitals(nZ)
!DELETE!!DESCRIPTION:
!DELETE! provides max.number of orbitals for atom
!DELETE! with atomic number {\tt nZ}
!DELETE!
!DELETE!!REVISION HISTORY:
!DELETE! Initial Version - A.S. - 2005
!DELETE!EOP
!DELETE!
!DELETE!BOC
!DELETE       implicit none
!DELETE       integer nZ
!DELETE!       integer norbitmax(0:8)/0,1,4,7,12,17,24,31,39/
!DELETE       integer nP
!DELETE       integer nPeriod
!DELETE       nP = nPeriod(nZ)
!DELETE       if (nP.ge.0) then
!DELETE!        nOrbitals = norbitmax(nP)
!DELETE         nOrbitals = max(1,nP)**2
!DELETE       else
!DELETE        nOrbitals = -1
!DELETE       end if
!DELETE       return
!DELETE!EOC
!DELETE       end function nOrbitals

!BOP
!!ROUTINE: nOrbToN
!!INTERFACE:
       integer function nOrbToN(k)
!!DESCRIPTION:
! provides principal quantim number for orbital {\tt k}
!
!!REVISION HISTORY:
! Initial Version - A.S. - 2005
!EOP
!
!BOC
       implicit none
       integer k
!c
!c atomic sequence (see periodic table, electronic configurations of atoms)
!c
!c   Z = 1:102
!c
!c 1:  1s1/2 - {1}                             He-2
!c 2:  2s1/2,2p1/2,2p3/2 - {2,3,4}             Ne-10
!c 3:  3s1/2,3p1/2,3p3/2 - {5,6,7}             Ar-18
!c 4:  4s1/2,3d3/2,3d5/2,4p1/2,4p3/2 - {8:12}                Kr-36
!c 5:  5s1/2,4d3/2,4d5/2,5p1/2,5p3/2 - {13:17}               Xe-54
!c 6:  6s1/2,5d3/2,5d5/2,4f5/2,4f7/2,6p1/2,6p3/2 - {18:24}   Rn-86
!c 7:  7s1/2,6d3/2,6d5/2,5f5/2,5f7/2 - {25:29}, 7p1/2,7p3/2 - {30,31}
!c 8:  8s1/2,7d3/2,7d5/2,6f5/2,6f7/2,8p1/2,8p3/2,7f5/2,7f7/2,8d3/2,8d5/2 - {32:42}
!c
       select case(k)
        case(0)
         nOrbToN = 0
        case(1)
         nOrbToN = 1
        case(2:4)
         nOrbToN = 2
!DELETE        case(5:7,9,10)
        case(5:9)
         nOrbToN = 3
!DELETE        case(8,11,12,14,15,21,22)
        case(10:16)
         nOrbToN = 4
!DELETE        case(13,16,17,19,20,28,29)
        case(17:25)
         nOrbToN = 5
!DELETE        case(18,23,24,26,27)
        case(26:36)
         nOrbToN = 6
!DELETE        case(25,30,31)
        case(37:49)
         nOrbToN = 7
        case(50:64)
         nOrbToN = 8
        case default
!         nOrbToN = -1
         nOrbToN = 9
       end select
       return
!EOC
       end function nOrbToN

!BOP
!!ROUTINE: nOrbToLK
!!INTERFACE:
       subroutine nOrbToLK(k,lc,kc)
!!DESCRIPTION:
! provides angular momentum and relativistic quantim numbers
! for orbital {\tt k}
!
!!REVISION HISTORY:
! Initial Version - A.S. - 2005
!!REMARKS:
!EOP
!
!BOC
       implicit none
       integer k
       integer lc,kc
       select case(k)
!DELETE        case(1,2,5,8,13,18,25,32)     ! s
!DELETE         lc = 0
!DELETE         kc = -(lc+1)
!DELETE        case(3,6,11,16,23,30,37)      ! p
!DELETE         lc = 1
!DELETE         kc = lc
!DELETE        case(4,7,12,17,24,31,38)      ! p
!DELETE         lc = 1
!DELETE         kc = -(lc+1)
!DELETE        case(9,14,19,26,33,41)           ! d
!DELETE         lc = 2
!DELETE         kc = lc
!DELETE        case(10,15,20,27,34,42)       ! d
!DELETE         lc = 2
!DELETE         kc = -(lc+1)
!DELETE        case(21,28,39)             ! f
!DELETE         lc = 3
!DELETE         kc = lc
!DELETE        case(22,29,40)                ! f
!DELETE         lc = 3
!DELETE         kc = -(lc+1)
        case(1,2,5,10,17,26,37,50)    ! s
         lc = 0
         kc = -(lc+1)
        case(3,6,11,18,27,38,51)      ! p
         lc = 1
         kc = lc
        case(4,7,12,19,28,39,52)      ! p
         lc = 1
         kc = -(lc+1)
        case(8,13,20,29,40,53)        ! d
         lc = 2
         kc = lc
        case(9,14,21,30,41,54)        ! d
         lc = 2
         kc = -(lc+1)
        case(15,22,31,42,55)          ! f
         lc = 3
         kc = lc
        case(16,23,32,43,56)          ! f
         lc = 3
         kc = -(lc+1)
        case(24,33,44,57)             ! g
         lc = 4
         kc = lc
        case(25,34,45,58)             ! g
         lc = 4
         kc = -(lc+1)
        case(35,46,59)                ! h
         lc = 5
         kc = lc
        case(36,47,60)                ! h
         lc = 5
         kc = -(lc+1)
        case(48,61)                   ! i
         lc = 6
         kc = lc
        case(49,62)                   ! i
         lc = 6
         kc = -(lc+1)
        case default
         lc = 0
         kc = 0
       end select
       return
!EOC
       end subroutine nOrbToLK

!%BOP
!!ROUTINE: dfltBottom
!!INTERFACE:
       function dfltBottom(z)
!!DESCRIPTION:
! provides (empirical) estimate for bottom of {\sc mecca} contour
! (for scf integration in complex plane); input argument {\tt z} is
! atomic number
!
!!ARGUMENTS:
!!PARAMETERS:
!!REVISION HISTORY:
! Initial Version - A.S. - 2005
!!REMARKS:
!%EOP
!
       implicit none
       real*8 dfltBottom
       real*8 z
       integer nZ
       nZ = nint(z)
       select case(nZ)
        case (1:2)
         dfltBottom = -0.5d0
        case (3)
         dfltBottom = -0.35d0
        case (4:6)
         dfltBottom = -1.2d0 - 0.1d0*(nZ-3)
        case (7)
         dfltBottom = -1.4d0
        case (8)
         dfltBottom = -1.4d0
        case (9)
         dfltBottom = -0.7d0
        case(10)
         dfltBottom = -1.2d0
        case(11:13)
         dfltBottom = -0.3d0
        case (14)
         dfltBottom = -0.5d0
        case (15)
         dfltBottom = -0.8d0
        case (16)
         dfltBottom = -1.0d0
        case (17)
         dfltBottom = -1.2d0
        case (18)
         dfltBottom = -1.0d0
        case (19:30)
         dfltBottom = -0.4d0
        case (31)
         dfltBottom = -1.0d0
        case (32)
         dfltBottom = -0.6d0
        case (33)
         dfltBottom = -0.8d0
        case (34)
         dfltBottom = -1.1d0
        case (35)
         dfltBottom = -0.7d0
        case (36)
         dfltBottom = -0.9d0
        case (37)
         dfltBottom = -0.35d0
        case (38:47)
         dfltBottom = -0.4d0
        case (48)
         dfltBottom = -0.6d0
        case (49)
         dfltBottom = -1.0d0
        case (50)
         dfltBottom = -0.5d0
        case (51)
         dfltBottom = -0.7d0
        case (52)
         dfltBottom = -0.9d0
        case (53)
         dfltBottom = -0.4d0
        case (54)
         dfltBottom = -1.3d0
        case (55:57)
         dfltBottom = -1.2d0
        case (58)
         dfltBottom = -1.3d0
!c        case (57:58)
!c         dfltBottom = -1.2d0-0.20d0*(nZ-55)/3.d0
        case (59:69)
         dfltBottom = -0.40d0-0.2d0*(nZ-59)/10.d0
!c        case (64:70)
!c         dfltBottom = -0.6d0
        case (70:71)
         dfltBottom = -0.6d0
        case (72)
         dfltBottom = -0.7d0
        case (73)
         dfltBottom = -1.15d0
        case (74)
         dfltBottom = -0.4d0
        case (75:78)
         dfltBottom = -0.8d0
        case (79)
         dfltBottom = -1.0d0
        case (80:81)
         dfltBottom = -0.8d0
        case(82)
         dfltBottom = -1.2d0
        case(83)
         dfltBottom = -0.8d0
        case(84)
         dfltBottom = -1.0d0
        case(85)
         dfltBottom = -1.2d0
        case (86)
         dfltBottom = -0.55d0
        case default
         dfltBottom = -1.d0
       end select
       return
       end function dfltBottom

!BOP
!!ROUTINE: dfltAlphmix
!!INTERFACE:
       function dfltAlphmix(imix,nZ)
!!DESCRIPTION:
! provides a reasonable guess for non-magnetic mixing parameter,
! {\tt nZ} is atomic number
!
!!REVISION HISTORY:
! Initial Version - A.S. - 2005
!EOP
!
!BOC
       implicit none
       real*8 dfltAlphmix
       integer imix
       integer nZ
       real*8 amix
       select case ( imix)
        case (0)
         amix = 0.3d0 + 0.3d0/max(1,nZ)
        case (1)
         amix = 0.2d0 + 0.3d0/max(1,nZ)
        case default
         amix = 0.02d0
       end select
       dfltAlphmix = amix

       select case ( nZ )
        case(24:28)
         dfltAlphmix = 0.12d0
        case (58)
         dfltAlphmix = 0.1d0
        case (63)
         dfltAlphmix = 0.1d0 ! 0.07d0
        case (67:68)
         dfltAlphmix = 0.1d0 ! 0.07d0
        case (69:70)
         dfltAlphmix = 0.10d0
        case (73)
         dfltAlphmix = 0.20d0
        case (83)
         dfltAlphmix = 0.1d0 ! 0.07d0
        case (-1)
         dfltAlphmix = 0.02d0
        case default
       end select
!EOC
       return
       end function dfltAlphmix

!BOP
!!ROUTINE: dfltMixType
!!INTERFACE:
       function dfltMixType(z)
!!DESCRIPTION:
! provides default type of mixing (0 or 1),
! {\tt z} is atomic number
!
!!REVISION HISTORY:
! Initial Version - A.S. - 2005
!EOP
!
!BOC
       implicit none
       integer dfltMixType
       integer nz
       real(8) z
       nZ = nint(z)
       select case (nz)
        case(0:54)
         dfltMixType = 0
        case(55:58)
         dfltMixType = 1
        case(59:70)
         dfltMixType = 1
        case(71:82)
         dfltMixType = 1  ! 0
        case(83)
         dfltMixType = 1
        case(84:85)
         dfltMixType = 1  ! 0
        case(86)
         dfltMixType = 1
        case default
         dfltMixType = 1
       end select
       return
!EOC
       end function dfltMixType

!BOP
!!ROUTINE: dfltRWS
!!INTERFACE:
       function dfltRWS(z)
!!DESCRIPTION:
! provides estimate for Wigner-Seitz radius of (close-packed) solid
! at normal pressure, {\tt z} is atomic number
!
!!REVISION HISTORY:
! Initial Version - A.S. - 2005
!EOP
!
!BOC
       implicit none
       real*8 dfltRWS
       real*8 z
       real*8 fccAlat
       external fccAlat
       real*8 pi16d3,third
       parameter (pi16d3 = 16.75516081914556393834d0)
       parameter (third=1.d0/3.d0)
       dfltRWS = fccAlat(z)/pi16d3**third
       return
!EOC
       end function dfltRWS

