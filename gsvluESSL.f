      subroutine gsvluESSL(a,ndmat,t,                                   &
     &                   nnptr,nnclmn,ndimnn,bmt,                       &
     &                   cof,ndcof,                                     &
     &                   itype,kkrsz,natom,                             &
     &                   invparam,niter,tolinv,iprint)
      entry      gsvlusp(a,ndmat,                                       &
     &                   nnptr,nnclmn,ndimnn,                           &
     &                   cof,ndcof,                                     &
     &                   itype,kkrsz,natom,                             &
!CALAM     *                   invparam,niter,tolinv,iprint)
     &                   iprint)

!c     ================================================================
!c
      implicit real*8(a-h,o-z)
!c
!c      To find solution of equation:  A*X=1  (A=1-G*t)
!c      using ESSL sparse direct-LU solver
!c
!c      and then output:  cof = t*X  (cof==tau)
!c
!c     ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!c
      integer ndmat,natom,kkrsz
      complex*16 a(*)
!c  a(ndmat,1:natom), ndmat .ge. maxclstr*kkrsz**2

      integer nnptr(0:1,natom),ndimnn,nnclmn(2,ndimnn,natom)
      complex*16 bmt(*)
!c      complex*16 vecs(kkrsz*natom,*)
      integer ndcof
      complex*16 t(ndcof,ndcof,*)
      complex*16 cof(ndcof,ndcof,natom)
      integer    itype(natom)
!CALAM      logical precond
      integer invparam,niter,iprint
      real*8 tolinv

      complex*16 sum
      complex*16 czero,cone
!c      integer cx,cb,revcom
      parameter (czero=(0.d0,0.d0),cone=(1.d0,0.d0))
      real*8    zero,one,MB
      parameter (zero=0.d0,one=1.d0,MB=1024.d0*1024.d0)

      integer erralloc
      real*8,  allocatable :: value(:)

      complex*16,  allocatable :: vecs(:,:)
      real*8,  allocatable :: vecstmp(:)

      integer nd2vecs

      real*8,  allocatable :: vecstmp1(:)
      integer, allocatable :: iptrow(:)
      integer, allocatable :: indcol(:)

      real*8  bigmem

      character sname*10
      parameter (sname='gsvluESSL')

      integer iparm(6)
      real*8  rparm(5),oparm(5)
      integer naux,naux1
!c      character*1 init

!c      complex*16 aij,aii
      integer isave/0/,j,jj,ii,i
!c      integer isave/0/,ntry,j,jj,ii,i,l1mx,l1mn,ierr
      integer nrmat,ne,jn,jns
!c      integer nrmat,ne,nlim,nlen,jn,jns,iflag,ndim
      integer ns1,nsub1,nsub2,l1
      real*8 time1,time2
      integer lna0/0/
      save isave,lna0
!CDEBUG
      real timeEn,timeIneq
      common /Alltime/timeEn,timeIneq
!CDEBUG

      if(isave.eq.0) then
       write(6,*)
       write(6,*)                                                       &
     & ' ROUTINE  '//sname//' BASED ON SPARSE'                          &
     &            ,' AIX ESSL-SUBROUTINES'
       write(6,*)                                                       &
     &     '          IS USED TO INVERT MATRIX'
       write(6,*) '  MAXIMUM NUMBER OF ITERATIONS = ',niter,            &
     &     ' AND TOLINV=',real(tolinv)
       write(6,*)
       isave=1

!c       iswitch = 0

      iparm = 0
      iparm(1) = 1
      iparm(2) = 10
      iparm(3) = 1
      iparm(4) = 0
      rparm = 0.d0
      rparm(1) = 1.d-12
      rparm(2) = 0.1d0

      end if

!CDEBUG
       time2 = 0.d0
       call timel(time2)
!CDEBUG
      nrmat = kkrsz*natom

      call sprsc16r8(natom,kkrsz,ndmat,a,nnptr,nnclmn,ndimnn,           &
     &                     ne,value,indcol,iptrow,1)

      if(iprint.ge.0) then
       write(6,*) ' NE=',ne,' NRMAT*NRMAT=',nrmat*nrmat
      end if

      naux = 10*(2*nrmat)+100
      nd2vecs = 1
      naux1 = 2*nrmat
      allocate(                                                         &
     &         vecs(1:nrmat,nd2vecs),                                   &
     &         vecstmp(1:naux),                                         &
     &         vecstmp1(1:naux1),                                       &
     &         stat=erralloc)
      if(erralloc.ne.0) then
      write(6,*) ' NE=',ne
      write(6,*) ' NAUX=',naux
      write(6,*) ' NAUX1=',naux1
      write(6,*) ' ALLOCATION (C16) =',nrmat*nd2vecs+naux*2+naux1*2
      write(6,*) ' ERRALLOC=',erralloc
      call fstop(sname//' :: ALLOCATION MEMORY PROBLEM')
      end if

!c      param1 = dreal(bmt(1))
      bigmem = dimag(bmt(1))

      lna = 2*(4*ne)+1                   ! minimal; doesn't guarantee
!c                                         ! a successful run of the ESSL
!c                         Larger lna may result in a performance improve !ment
!c

      if(invparam.eq.1.or.lna.gt.lna0) then
       if(bigmem.ge.one) then
      LUsize = 8+4+4
      lna0 = max(lna,nint(bigmem*MB/LUsize)+1)
       else
      lna0 = max(lna,(2*nrmat)**2)
       end if


3      continue
       if(lna0.lt.lna) then
         write(6,*) ' LNA0=',lna0
         write(6,*) '  LNA=',lna
         write(6,*) ' NRMAT=',nrmat
         call fstop(sname//':: MEMORY PROBLEM')
       end if
       lnar = max(2*nrmat+1,lna0)
       if(allocated(value)) deallocate(value)
       if(allocated(indcol)) deallocate(indcol)
       if(allocated(iptrow)) deallocate(iptrow)
       allocate(                                                        &
     &          value(1:lna0),                                          &
     &          indcol(1:lna0),                                         &
     &          iptrow(1:lnar),                                         &
     &          stat=erralloc)
       if(erralloc.ne.0) then
      if(lna0.eq.lna) then
       write(6,*) ' LNA0=LNA=',lna0
       write(6,*) ' NRMAT=',nrmat
       call fstop(sname//':: YOU HAVE NO MINIMAL MEMORY')
      end if
      lna0 = max(lna,nint(dble(lna0)*0.9d0))
      go to 3
       endif
      end if

      lna = lna0
      lnar = max(2*nrmat+1,lna0)

      if(.not. allocated(value)) then
       allocate(                                                        &
     &          value(1:lna),                                           &
     &          indcol(1:lna),                                          &
     &          iptrow(1:lnar),                                         &
     &          stat=erralloc)
       if(erralloc.ne.0) then
      write(6,*) '   NE=',ne
      write(6,*) '  LNA=',lna
      write(6,*) ' LNAR=',lnar
      write(6,*) ' ERRALLOC=',erralloc
      call fstop(sname//' :: ALLOCATION MEMORY PROBLEM')
       end if
      else
       write(6,*) (lna*8+lna*4+lnar*4)/MB,' MB HAVE BEEN LLOCATED'
      end if

      call sprsc16r8(natom,kkrsz,ndmat,a,nnptr,nnclmn,ndimnn,           &
     &                     ne,value,indcol,iptrow,0)

      iopt = 1              ! stored by row

      CALL DGSF (iopt, 2*nrmat, 4*ne, value, iptrow, indcol,            &
     &            lna, iparm, rparm, oparm,                             &
     &            vecstmp, naux)


!CDEBUG
      time1 = time2
      call timel(time2)
      timePreit = time2-time1
      if(iprint.ge.0) then
       write(*,*) sname//' :: LU-DECOMP-time=',real(timePreit)
      end if
!CDEBUG

      ntime = 0
!c      nsub0 = 0
      do nsub1=1,natom

       ns1 = itype(nsub1)
       Jns = (nsub1-1)*kkrsz

       do nsub2=1,nsub1-1
      if(ns1.eq.itype(nsub2)) then            ! copy
       do l1 = 1,kkrsz
        do i=1,kkrsz
         cof(i,l1,nsub1) = cof(i,l1,nsub2)
        end do
       end do
       go to 10
      end if
       end do

       ntime = ntime+1

       do l1=1,kkrsz
      j = Jns+l1

      vecstmp1(1:2*nrmat) = zero
      vecstmp1(j) = one

      CALL DGSS (0, 2*nrmat, value, iptrow, indcol,                     &
     &             lna, vecstmp1(1),vecstmp, naux)

      do i=1,nrmat
       vecs(i,1) = dcmplx(vecstmp1(i),vecstmp1(nrmat+i))
      end do

!c                                          we need only diagonal blocks

!c        Jns = (nsub1-1)*kkrsz

      do ii=1,kkrsz
       sum = czero
       do jj=1,kkrsz
!c           Jn = (nsub1-1)*kkrsz+jj
        Jn = Jns +jj
        sum = sum + t(ii,jj,ns1)*vecs(Jn,1)
       end do
       cof(ii,l1,nsub1) = sum
      end do
       end do

10     continue
      end do

       continue

!CDEBUG
      time1 = time2
      call timel(time2)
      tsolve = (time2-time1)/ntime
      if(iprint.ge.0) then
       write(*,*) sname//' :: SPARSEINV-time=',real(tsolve)             &
     &            ,real(tsolve*natom),real(tsolve*natom+timePreit)
      end if
      timeIneq = timeIneq + tsolve*natom+timePreit
!CDEBUG

      deallocate(value,vecs,vecstmp,vecstmp1,                           &
     &           iptrow,indcol,stat=erralloc)

      if(erralloc.ne.0) then
      write(6,*) ' NE=',ne
      write(6,*) ' ERRALLOC=',erralloc
      call fstop(sname//' :: DEALLOCATION MEMORY PROBLEM')
      end if

      return
      end

      subroutine sprsc16r8(natom,kkrsz,ndmat,a,nnptr,nnclmn,ndimnn,     &
     &                     ne,value,indcol,iptrow,iflag)
      implicit none
      integer kkrsz,natom,ndmat
      complex*16 a(*)
      integer nnptr(0:1,natom),ndimnn,nnclmn(2,ndimnn,natom)
      integer ne,indcol(4*ne),iptrow(natom*kkrsz+1),iflag
      real*8 value(4*ne)

      integer ne2,ndima,i,j,iatom,inn,jatom
      integer lmi,lmj,lmij,indI,indJ,kelem,kelem1
      real*8 dreal,dimag
      complex*16 aij,czero
      parameter (czero=(0.d0,0.d0))

      if(iflag.eq.1) then
       ne = 0
       do i=1,ndmat*natom
      if(abs(dreal(a(i)))+abs(dimag(a(i))).gt.1.d-13) then
       ne = ne+1
      else
       a(i) = czero
      end if
       end do

       return
      end if

      ndima = natom*kkrsz
      ne2 = ne+ne
      kelem = 0
      iptrow(1) = 1

      i = 0
      do iatom=1,natom
       indI = (iatom-1)*ndmat
       do lmi=1,kkrsz
!c        i = (iatom-1)*kkrsz+lmi
      i = i + 1
      do inn=1,nnptr(0,iatom)
       jatom = nnclmn(1,inn,iatom)
       indJ = (lmi-kkrsz) + (inn-1)*(kkrsz*kkrsz)
       do lmj=1,kkrsz
        j = (jatom-1)*kkrsz+lmj
        lmij = lmj*kkrsz +indJ
!c                          lmij = (lmj-1)*kkrsz + lmi
!c                          aij  = a(lmij,iatom)
        aij = a(indI+lmij)
        if(aij.ne.czero) then

         kelem1 = kelem+1
         value(kelem1) = dreal(aij)
         indcol(kelem1) = j
         kelem  = kelem+2
         value(kelem) = -dimag(aij)
         indcol(kelem) = j + ndima
         value(ne2+kelem1) = dimag(aij)
         indcol(ne2+kelem1) = j
         value(ne2+kelem) = dreal(aij)
         indcol(ne2+kelem) = j + ndima
        end if
       end do
      end do
      iptrow(i+1) = kelem+1
      iptrow(ndima+i+1) = ne2+iptrow(i+1)
       end do
      end do

      return
      end
