#!/bin/csh -f

if ( ${?LIBRARY_PATH} ) then
 setenv DYLD_LIBRARY_PATH ${LIBRARY_PATH}
endif

# set EXE=./ameskkr_mpifort


# "POSITIVE TESTS"

( \
${EXE} Al fcc -Neos 0 -Kpoints 1:21,22,23@2:11,12,13 ; \
${EXE} Al fcc -Neos 0 -Kpoints 1:-21,-22,23@2:-11,12,13 ; \
${EXE} Al fcc -Neos 0 -Kpoints 1:-21,-22,23 ; \
${EXE} Al fcc -Neos 0 -Kpoints -21,-22,23 ; \
${EXE} Al fcc -Neos 0 -Kpoints -1 ; \
${EXE} Al fcc -Neos 0 -Kpoints -2 ; \
${EXE} Al fcc -Neos 0 -Kpoints -3 \
 ) | & grep nmesh -A 3 > 1.log
 
diff out1.log 1.log 
if ( $? ) then
 echo POSITIVE TESTS FAIL
else
 echo PASS1
endif

# "NEGATIVE TESTS"
( \
${EXE} Al fcc -Neos 0 -Kpoints 1:-21,-22 ; \
${EXE} Al fcc -Neos 0 -Kpoints 1:-21,-22, ; \
${EXE} Al fcc -Neos 0 -Kpoints 1:-21,-22\? ; \
${EXE} Al fcc -Neos 0 -Kpoints 1:-21,-22.. ; \
${EXE} Al fcc -Neos 0 -Kpoints 1:-21,-22,-23@ ; \
${EXE} Al fcc -Neos 0 -Kpoints 1:-21, ; \
${EXE} Al fcc -Neos 0 -Kpoints 1:-21,-22,23@ ; \
${EXE} Al fcc -Neos 0 -Kpoints 21,22,23@ ; \
${EXE} Al fcc -Neos 0 -Kpoints 21,22,23. ; \
${EXE} Al fcc -Neos 0 -Kpoints "1:2,2,2 ; 2:1,1,1" ; \
 ) | & grep "check description" -B 1 > 2.log

diff out2.log 2.log
if ( $? ) then
 echo NEGATIVE TESTS FAIL
else
 echo PASS2
endif
