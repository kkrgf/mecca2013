set encoding iso_8859_1
#set terminal  postscript enhanced color
#set output "ake.eps"
#set terminal  png truecolor enhanced font ", 60" size 1920, 1680
#set output "ake.png"
set palette defined (0.2821513717 "#194eff", 501.78583013405 "white", 1003.2895088964 "red" )
set style data linespoints
set size 0.8, 1
set origin 0.1, 0
unset ztics
unset key
set pointsize 0.8
set pm3d
set view map
set border lw 3
#set ylabel "Energy (Ry)"
set ylabel "Energy (eV)"
set xrange [0:  4.0617439659]
set yrange [-1.360569301: 1.360569301]
set xtics ("G" 0.0,"X" 0.7071067529,"Y" 0.7881270408,"S" 1.2308370208,"G" 1.7881269908,"Z" 2.1266240908,"S1" 2.5693340708,"N" 2.7480159551,"P" 3.2480159351,"Y1" 3.4356575080,"Z" 4.0617439659)
set arrow from 0.7071067529, -1.360569301 to 0.7071067529, 1.360569301 nohead front lw 3
set arrow from 0.7881270408, -1.360569301 to 0.7881270408, 1.360569301 nohead front lw 3
set arrow from 1.2308370208, -1.360569301 to 1.2308370208, 1.360569301 nohead front lw 3
set arrow from 1.7881269908, -1.360569301 to 1.7881269908, 1.360569301 nohead front lw 3
set arrow from 2.1266240908, -1.360569301 to 2.1266240908, 1.360569301 nohead front lw 3
set arrow from 2.5693340708, -1.360569301 to 2.5693340708, 1.360569301 nohead front lw 3
set arrow from 2.7480159551, -1.360569301 to 2.7480159551, 1.360569301 nohead front lw 3
set arrow from 3.2480159351, -1.360569301 to 3.2480159351, 1.360569301 nohead front lw 3
set arrow from 3.4356575080, -1.360569301 to 3.4356575080, 1.360569301 nohead front lw 3
set arrow from 0.0, 0.0 to 4.0617439659, 0.0 nohead front lw 3 dt 2
set xtics offset 0, 0.7
set ytics 0.5 scale 0.1 format "%.1f" offset 0.7, 0
#unset colorbox
set pm3d interpolate 2,2
splot "ake.dat" u 1:3:4 w pm3d
