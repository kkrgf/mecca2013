#!/bin/tcsh -f

set DATE=$1

setenv GOLDEN ./golden
setenv GOLD $GOLDEN/golden_alat.csv

set in=`pwd` ; cd $GOLDEN/..
setenv PATH `pwd`/bin:${PATH}
cd $in

#( cd test.xxx_D11/LOGS ; to_csv.sh 11 10 )
( cd test.xxx_D12/LOGS ; to_csv.sh 12 10 )
#( cd test.xxx_D22/LOGS ; to_csv.sh 22 10 )
( cd test.xxx_D22g/LOGS ; to_csv.sh 22 116133 )

#( cd test.xxx_D11/LOGS ; \
# cat a1_xc10_11.csv a2_xc10_11.csv a4_xc10_11.csv b1_xc10_11.csv b2_xc10_11.csv \
#     b3es_xc10_11.csv other_xc10_11.csv | rpl " " "" > dvlp_xc10_asa+mtc.csv )

( cd test.xxx_D12/LOGS ; \
  cat a1_xc10_12.csv a2_xc10_12.csv a4_xc10_12.csv b1_xc10_12.csv b2_xc10_12.csv \
      b3es_xc10_12.csv other_xc10_12.csv | rpl " " "" > dvlp_xc10_asa+pbc.csv )

#( cd test.xxx_D22/LOGS ; \
# cat a1_xc10_22.csv a2_xc10_22.csv a4_xc10_22.csv b1_xc10_22.csv b2_xc10_22.csv \
#     b3es_xc10_22.csv other_xc10_22.csv | rpl " " "" > dvlp_xc10_vp+pbc.csv )

( cd test.xxx_D22g/LOGS ; \
  cat a1_xc116133_22.csv a2_xc116133_22.csv a4_xc116133_22.csv b1_xc116133_22.csv b2_xc116133_22.csv \
      b3es_xc116133_22.csv other_xc116133_22.csv | rpl " " "" > dvlp_xc116133_vp+pbc.csv )

#( csv_report.py ${GOLD} \
#    test.xxx_D11/LOGS/dvlp_xc10_asa+mtc.csv 10 ${DATE}_xc10_asa+mtc.D.csv > ${DATE}_xc10_asa+mtc.D.txt )
#( grep B0= test.xxx_D11/LOGS/*10*11.log | grep -v POLYNM2 | awk '{if ( ( $12<1.01 || $12 > 8) || $9<0.01 ) print  }' \
#                          >>! ${DATE}_xc10_asa+mtc.D.txt )

( csv_report.py ${GOLD} \
    test.xxx_D12/LOGS/dvlp_xc10_asa+pbc.csv 10 ${DATE}_xc10_asa+pbc.D.csv > ${DATE}_xc10_asa+pbc.D.txt )
( grep B0= test.xxx_D12/LOGS/*10*12.log | grep -v POLYNM2 | awk '{if ( ( $12<1.01 || $12 > 8) || $9<0.01 ) print  }' \
                           >>! ${DATE}_xc10_asa+pbc.D.txt )

#( csv_report.py ${GOLD} \
#    test.xxx_D22/LOGS/dvlp_xc10_vp+pbc.csv 10 ${DATE}_xc10_vp+pbc.D.csv > ${DATE}_xc10_vp+pbc.D.txt )
#( grep B0= test.xxx_D22/LOGS/*10*22.log | grep -v POLYNM2 | awk '{if ( ($12<1.01 || $12 > 8) || $9<0.01 ) print  }' \
#                          >>! ${DATE}_xc10_vp+pbc.D.txt )

( csv_report.py ${GOLD} \
    test.xxx_D22g/LOGS/dvlp_xc116133_vp+pbc.csv 116133 ${DATE}_xc116133_vp+pbc.D.csv > ${DATE}_xc116133_vp+pbc.D.txt )
( grep B0= test.xxx_D22g/LOGS/*116133*22.log | grep -v POLYNM2 | awk '{if ( ( $12<1.01 || $12 > 8) || $9<0.01 ) print  }' \
                           >>! ${DATE}_xc116133_vp+pbc.D.txt )
