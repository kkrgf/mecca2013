#!/usr/bin/env perl
######################################################################
# Lin-Lin Wang <llw@ameslab.gov> Extract data from MECCA akeBAND     #
######################################################################
# Combine MECCA akeBAND files for gnuplot

#use strict;
$ry2ev = 13.605698;

@ARGV>=5 || die "usage: mca_bsf.pl <ndir> <neng> <nprc> <eng_low> <eng_upp>...\n";
$ndir = $ARGV[0];
$neng = $ARGV[1];
$nprc = $ARGV[2];
$eng_low = $ARGV[3];
$eng_upp = $ARGV[4];
$filebase = "akeBAND";

for ($k=0;$k<$nprc;$k++)
{
  $inputfile[$k] = $filebase.$k.".dat";
}

for ($k=0;$k<$nprc;$k++) { $nengp[$k]=0; }
for ($i=0;$i<$neng+1;$i++) { $nengp[($i%$nprc)]++; }
#die @nengp;


open(IN,"<$inputfile[0]");
@lines=<IN>;
close(IN);
$nline = @lines;
#$_=$lines[3]; @line=split;
$_=$lines[2]; @line=split;
$eng_min = $line[1];

$nline_eng = $nline/$nengp[0];
#$nkpt = $nline_eng - 2*$ndir -1;
$nkpt = $nline_eng - 2*$ndir;

$_=$lines[0]; @line=split;
$kptname0 = $line[5];
$_=$lines[1]; @line=split;
$kptname[0] = $line[5];

$dc=0; $kc=0;
#for ($i=3;$i<$nline_eng;$i++)
for ($i=2;$i<$nline_eng;$i++)
{
  $_=$lines[$i]; @line=split;
  if ($line[1] eq "to") 
  {
    $nkptp[$dc] = $kc - 1;
    $_=$lines[$i-2]; @line=split;
    $kptp[$dc] = $line[0];

    $dc++;
    $kc=0;
    $_=$lines[$i]; @line=split;
    $kptname[$dc] = $line[5];
  }else
  {
    $kc++;
  }
}
$nkptp[$dc] = $kc;
$_=$lines[$nline_eng-1]; @line=split;
$kptp[$dc] = $line[0];
#die @nkptp;
#die @kptp;

$lasteng = $neng%$nprc;
open(IN,"<$inputfile[$lasteng]");
@lines=<IN>;
close(IN);
$nline = @lines;
$_=$lines[$nline-1]; @line=split;
$eng_max = $line[1];

#$eng_low = $eng_min;
#$eng_upp = $eng_max;


for ($k=0;$k<$nprc;$k++)
{
  open(IN,"<$inputfile[$k]");
  @lines=<IN>;
  close(IN);
  $nline = @lines;

  $lc=0; $ec=$k;
  for ($i=0;$i<$nengp[$k];$i++)
  {
#    $lc+=1; $kc=0;
    $kc=0;
    for ($j=0;$j<$ndir;$j++)
    {
      $lc+=2;
      for ($ii=0;$ii<$nkptp[$j];$ii++)
      {
        $_=$lines[$lc]; @line=split;
        $bsf->[$kc][$ec][0] = $line[0];
        $bsf->[$kc][$ec][1] = $line[1];
        $bsf->[$kc][$ec][2] = $line[2];
        $lc++; $kc++;
      }
    }
    $ec+=$nprc;
  }
}


$bsf_min = 1e10;
$bsf_max = 1e-10;

$outbase = "ake";
$outputfile1 = $outbase.".dat";
open(OUT,">$outputfile1");
print OUT "# ", $neng+1;
print OUT " $ndir $nkpt ";
for ($i=0;$i<$ndir;$i++)
{
  print OUT " $nkptp[$i]";
}
print OUT "\n";

for ($i=0;$i<$neng+1;$i++)
{
  for ($j=0;$j<$nkpt;$j++)
  {
#    printf OUT "%.10f %.10f %.10f %.10f %.10f\n", $bsf->[$j][$i][0], $bsf->[$j][$i][1], $bsf->[$j][$i][1]*$ry2ev, $bsf->[$j][$i][2], log($bsf->[$j][$i][2]);
    printf OUT "%.10f %.10f %.10f %.10f\n", $bsf->[$j][$i][0], $bsf->[$j][$i][1], $bsf->[$j][$i][1]*$ry2ev, $bsf->[$j][$i][2];
    if ($bsf->[$j][$i][2] < $bsf_min) {$bsf_min = $bsf->[$j][$i][2];}
    if ($bsf->[$j][$i][2] > $bsf_max) {$bsf_max = $bsf->[$j][$i][2];}
  }
  print OUT "\n";
}
close(OUT);
#$bsf_min_log = log($bsf_min);
#$bsf_max_log = log($bsf_max);
$bsf_avg = ($bsf_min+$bsf_max)/2.0;
#$bsf_avg_log = ($bsf_min_log+$bsf_max_log)/2.0;


$outputfile2 = $outbase.".gnu";
open(OUT,">$outputfile2");
print OUT <<end_gnu1;
set encoding iso_8859_1
#set terminal  postscript enhanced color
#set output "$outbase.eps"
set terminal  png truecolor enhanced font ", 60" size 1920, 1680
set output "$outbase.png"
set palette defined ($bsf_min "#194eff", $bsf_avg "white", $bsf_max "red" )
set style data linespoints
set size 0.8, 1
set origin 0.1, 0
unset ztics
unset key
set pointsize 0.8
set pm3d
set view map
set border lw 3
#set ylabel "Energy (Ry)"
set ylabel "Energy (eV)"
set xrange [0:  $kptp[$ndir-1]]
set yrange [$eng_low: $eng_upp]
end_gnu1

print OUT "set xtics (\"$kptname0\" 0.0";
for ($j=0;$j<$ndir;$j++)
{
  print OUT ",\"$kptname[$j]\" $kptp[$j]";
}
print OUT ")\n";

for ($j=0;$j<$ndir-1;$j++)
{
  print OUT "set arrow from $kptp[$j], $eng_low to $kptp[$j], $eng_upp nohead front lw 3\n";
}
print OUT "set arrow from 0.0, 0.0 to $kptp[$ndir-1], 0.0 nohead front lw 3 dt 2\n";

print OUT <<end_gnu2;
set xtics offset 0, 0.7
set ytics 0.5 scale 0.1 format "%.1f" offset 0.7, 0
#unset colorbox
set pm3d interpolate 2,2
splot "$outputfile1" u 1:3:4 w pm3d
end_gnu2
