#!/bin/csh -f
##!/usr/local/bin/csh -f
# example:  ../to_csv.sh 11 10
# ( xc=10 mtasa=11)

set ASA=$1
set XC=$2

${tests}/bin/gentab.sh MURN "./[A-Z]*_A1*xc${XC}*${ASA}.log ./fm_[A-Z]*_A1*xc${XC}*${ASA}.log" > ! a1_xc${XC}_${ASA}.csv
${tests}/bin/gentab.sh MURN "./[A-Z]*_A2*xc${XC}*${ASA}.log ./fm_[A-Z]*_A2*xc${XC}*${ASA}.log" > ! a2_xc${XC}_${ASA}.csv
${tests}/bin/gentab.sh MURN "./[A-Z]*_A4*xc${XC}*${ASA}.log ./fm_[A-Z]*_A4*xc${XC}*${ASA}.log" > ! a4_xc${XC}_${ASA}.csv
${tests}/bin/gentab.sh MURN "./[A-Z]*_B2*xc${XC}*${ASA}.log ./fm_[A-Z]*_B2*xc${XC}*${ASA}.log" > ! b2_xc${XC}_${ASA}.csv
${tests}/bin/gentab.sh MURN "./[A-Z]*_B1*xc${XC}*${ASA}.log ./fm_[A-Z]*_B1*xc${XC}*${ASA}.log" > ! b1_xc${XC}_${ASA}.csv
${tests}/bin/gentab.sh MURN "./[A-Z]*_B3_ES*xc${XC}*${ASA}.log ./fm_[A-Z]*_B3_ES*xc${XC}*${ASA}.log" > ! b3es_xc${XC}_${ASA}.csv
${tests}/bin/gentab.sh MURN "./[A-Z]*_C1*xc${XC}*${ASA}.log ./fm_[A-Z]*_C1*xc${XC}*${ASA}.log" > ! other_xc${XC}_${ASA}.csv
${tests}/bin/gentab.sh MURN "./[A-Z]*_L1*xc${XC}*${ASA}.log ./fm_[A-Z]*_L1*xc${XC}*${ASA}.log" >> ! other_xc${XC}_${ASA}.csv
${tests}/bin/gentab.sh MURN "./[A-Z]*_B32*xc${XC}*${ASA}.log ./fm_[A-Z]*_B32*xc${XC}*${ASA}.log" >> ! other_xc${XC}_${ASA}.csv
${tests}/bin/gentab.sh MURN "./[A-Z]*_A3*xc${XC}*${ASA}.log ./fm_[A-Z]*_A3*xc${XC}*${ASA}.log" >> ! other_xc${XC}_${ASA}.csv
