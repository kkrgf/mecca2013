#!/bin/csh -f

## example:  -j8 is for 8 processes, -N1 is to use one argument for "script"  
#
# /usr/bin/time parallel -N1 -j8 script ::: `perl -e 'for(1..3){print "$_ "}{print "\n"}'`
#
# /usr/bin/time parallel -j8 -N10 --nice 4 ::: `cat xxx1`   # xxx1 - list of commands with 10 arguments (for parallel) 
#
# /usr/bin/time parallel -j7 -N10 --nice 4 ::: `cat $work/tests/jobs/$XXX | rpl eos_ eosD_`
