#!/bin/csh -f

set LOG=$1
set BEFORE=$2

set WORK=.grep_log.tmp
rm -f ${WORK}

grep CPU -B ${BEFORE} ${LOG} > ${WORK}

grep Mba ${WORK} 
grep Int ${WORK} | grep Ry
grep Ferm ${WORK}
grep vmtzup ${WORK}
grep Point ${WORK} | grep "= 1"
