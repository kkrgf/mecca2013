#!/usr/bin/env python

import sys
#import os
import csv

def csv_report(argv):
 if len(argv)<3 or len(argv)> 4 :
   print("usage:")
   print("  csv_report.py results1.csv results2.csv report.csv")
   print("       OR")
   print("  csv_report.py golden.csv results.csv xc_id report.csv")
   print("  (xc_id is an identificator of exchange-correlation functional used to obtain mecca data)")
   sys.exit(2)
 golden = argv[0]
 results = argv[1]


#csvfile = open('golden.csv', 'rb')
# csvfile = open(golden, 'rb')
 g_rows = {}
 n = 0
 g_rows[n] = ''
 with open(golden, 'r') as csvfile:
    reader1 = csv.reader(csvfile)
# for row in csv.reader(csvfile):
    for row in reader1:
     n = n+1
     g_rows[n] = row
 
 if len(g_rows)<1:
  print("file ",golden," is empty or has a wrong format")
  sys.exit(2)

# csvfile = open(results, 'rb')
 r_rows = {}
 m = 0
 with open(results, 'r') as csvfile:
  reader2 = csv.reader(csvfile)
# for row in csv.reader(csvfile):
  for row in reader2:
   m = m+1
   r_rows[m] = row

 if len(r_rows)<1:
  print("file ",results," is empty or has a wrong format")
  sys.exit(2)

 if len(g_rows)>5 :
  xc_row = g_rows[5]
 else:
  xc_row = {'no xc'}

 if len(xc_row.__getitem__(0))<1 and len(xc_row.__getitem__(2))<1 :
   xc_id = argv[2]
   report = argv[3]
   alatCompare_exp_fp(g_rows,r_rows,xc_id,report)
 else :
   report = argv[2]
   alatCompare_mecca(g_rows,r_rows,report)


def g_chemID(chem_id):
  s = str(chem_id.__getitem__(0))
  s = s.strip()
  if ( s.rfind('ES') > -1 ):
    s = s.split('ES')
    s = s[0]
  chem_id[0] = s.replace('1','')

def g_strID(str_id):
  s = str(str_id.__getitem__(0).strip())
  if ( s.rfind('_ES') > -1 ):
    s = s.strip().split('_ES')
    s = s[0]
  str_id[0] = s

def alatCompare_mecca(g_rows,r_rows,report):
 bohr = 0.529177
 m = len(r_rows)
 n = len(g_rows)
 outwr = open(report, 'w')
 writer = csv.writer(outwr)
 header = list()
 header.append("chem. ID")
 header.append("str. ID")
 header.append("a, Bohr")
 header.append("Etot, Ry")
 header.append("B0")
 header.append(" ")
 header.append("a_gold, Bohr")
 header.append("err(gold), %")
 header.append(" ")
 header.append("Etot_gold, Ry")
 header.append("err(gold), Ry")
 header.append(" ")
 header.append("B0_gold, Ry")
 header.append("err(gold), %")
 header.append(" ")
 writer.writerow(header)
 n_test     = 0
 n_nogold   = [0]*3
 n_ok       = [0]*3
 ok         = [0.7,0.001,10]
 fail       = [-1000]*3  # [100,1.e7,100]
 averr_ok   = [0.]*3
 n_fail     = [0]*3
 min_fail   = [0]*3
 max_fail   = [0]*3

 j=0
 id = list()
 id.append('')
 while j<m-1 :
  j = j + 1
  r_row = r_rows[j]
  id[0] = r_row.__getitem__(0)
  if  id[0] == str(header.__getitem__(0)) :
   continue
  else:
   g_chemID(id)
   chem_id = str(id.__getitem__(0))
   id[0] = r_row.__getitem__(1)
   g_strID(id)
   str_id = str(id.__getitem__(0))
   i = 0
   while i < n-1 :
    i = i+1
    g_row = g_rows[i]
    id[0] = g_row.__getitem__(1)
    g_strID(id)
    s_id = str(id.__getitem__(0))
    if s_id == str_id:
     id[0] = g_row.__getitem__(0)
     g_chemID(id)
     c_id = str(id.__getitem__(0))
     if c_id == chem_id:
      n_test = n_test+1
      i = n+1
      row = [None]*15
      row[0] = chem_id
      row[1] = str(g_row.__getitem__(1).strip())
      z = float(r_row.__getitem__(2))
      row[2] = '{:.3f}'.format(z)
      z = float(r_row.__getitem__(3))
      row[3] = '{:11.5f}'.format(z)
      z = float(r_row.__getitem__(4))
      row[4] = '{:.2f}'.format(z)
      
      it = 1

      while it<4:
       it = it+1
       l = it-2
       err = -1000
       val_r = float(r_row.__getitem__(it))
       val_g = float(g_row.__getitem__(it))
       if it==2:
        row[6] = '{:.3f}'.format(val_g)
        if abs(val_g)>0: 
         err = (val_r-val_g)/val_g*100
         row[7] = '{:.1f}'.format(err)
        next = 8
       elif it==3:
        row[9] = '{:.11f}'.format(val_g)
        if abs(val_g)>0: 
          err = val_r-val_g
          row[10] = '{:.5f}'.format(err)
        next = 11
       elif it==4:
        row[12] = '{:.2f}'.format(val_g)
        if abs(val_g)>0: 
          err = (val_r-val_g)/val_g*100
          if abs(val_g)<ok[l]/100 and abs(val_r-val_g)<ok[l]/100:
           if err<0:
            err = max(err,-ok[l])
           else:
            err = min(err,ok[l])
          row[13] = '{:.1f}'.format(err)
        next = 14
       if abs(err)<=ok[l] :
         n_ok[l] = n_ok[l]+1
         averr_ok[l] = averr_ok[l]+abs(err)
       elif err==fail[l] :
         n_nogold[l] = n_nogold[l]+1
         row[next] = 'NO INFO'
       else:
         n_fail[l] = n_fail[l]+1
         min_fail[l] = min(min_fail[l],err)
         max_fail[l] = max(max_fail[l],err)
         row[next] = 'FAIL'
      
      writer.writerow(row)
 print("number of tests: ",n_test)
 print("             ok: ",n_ok)
 l=0
 while l<3:
  averr_ok[l] = averr_ok[l]/max(1,n_ok[l])
  l = l+1
 print("          <err>: ",'{:.1f}'.format(averr_ok[0]),'%'\
                          ,'{:.4f}'.format(averr_ok[1]),'Ry'\
                          ,'{:.1f}'.format(averr_ok[2]),'%')
 print("           fail: ",n_fail)
 if min(n_fail)>0:
  print("           min: ",'{:.1f}'.format(min_fail[0]),'%'\
                          ,'{:.4f}'.format(min_fail[1]),'Ry'\
                          ,'{:.1f}'.format(min_fail[2]),'%')
  print("           max: ",'{:.1f}'.format(max_fail[0]),'%'\
                          ,'{:.4f}'.format(max_fail[1]),'Ry'\
                          ,'{:.1f}'.format(max_fail[2]),'%')
 if min(n_nogold)>0:
  print("      no golden: ",n_nogold)


def alatCompare_exp_fp(g_rows,r_rows,xc_id,report):
 bohr = 0.529177
 m = len(r_rows)
 n = len(g_rows)
 xc_row = g_rows[5]
 outwr = open(report,'w')
 writer = csv.writer(outwr)
 header = list()
 header.append("chem. ID")
 header.append("str. ID")
 header.append("a, Bohr")
 header.append("Etot, Ry")
 header.append("B0")
 header.append("V, Bohr^3")
 header.append("B0'")
 header.append("a, A")
 header.append(" ")
 header.append("err(gold), %")
 header.append("err(exp), %")
 header.append(" ")
 header.append("a_gold, A")
 header.append("a_Exp, A")
 writer.writerow(header)
 
 chem_id=''
 str_id=''
 n_nogold = 0
 n_test = 0
 n_good = 0
 good   = 1.5
 averr_good = 0
 n_ok   = 0
 ok     = 2.5
 averr_ok = 0
 n_fail = 0
 averr_fail = 0
 j=0
 id = list()
 id.append('')
 while j<m-1 :
  j = j + 1
  id[0] = r_rows[j].__getitem__(0)
  g_chemID(id)
  chem_id = str(id.__getitem__(0))
  id[0] = r_rows[j].__getitem__(1)
  g_strID(id)
  str_id = str(id.__getitem__(0))
#  chem_id = r_rows[j].__getitem__(0).strip()
#  if ( chem_id.rfind('ES') > -1 ):
#    s = chem_id.split('ES')
#    chem_id = s[0]
#  chem_id = chem_id.replace('1','')
#  print ' chem_ID=',chem_id
#  str_id = r_rows[j].__getitem__(1).strip()
#  if ( str_id.rfind('_ES') > -1 ):
#    s = str_id.split('_ES')
#    str_id = s[0]
#  print ' str_id=',str_id
  i = 5
  while i < n-1 :
   i = i+1
   g_row = g_rows[i]
   id[0] = g_row.__getitem__(1)
   g_strID(id)
   s_id = str(id.__getitem__(0))
   if s_id == str_id:
    id[0] = g_row.__getitem__(0)
    g_chemID(id)
    c_id = str(id.__getitem__(0))
    if c_id == chem_id:
      n_test = n_test+1
      i = n+1
      aFP = -1
      theor_err = -1000
      aexp = -1
      exp_err = -1000
      if len(r_rows[j])==3:
       alat = 0
      else:
       alat = float(r_rows[j].__getitem__(2))
      if alat>0 :
       alat = alat*bohr
       aexp = float(g_row.__getitem__(3).strip())
       if aexp <= 0:
        aexp = float(g_row.__getitem__(2).strip())
       if  aexp>0 :
        exp_err = (alat-aexp)/aexp*100
       k=3
       while k<11:
        k=k+1
        if xc_id == xc_row.__getitem__(k) and float(g_row.__getitem__(k).strip())>0 :
         aFP = float(g_row.__getitem__(k).strip())
         theor_err = (alat-aFP)/aFP*100
         if theor_err<-100:
           theor_err=-100
         k = 14
      row = list()
      row.append(chem_id)
      row.append(r_rows[j].__getitem__(1).strip())
      z = float(r_rows[j].__getitem__(2))
      row.append('{:.3f}'.format(z))
      if alat>0:
       z = float(r_rows[j].__getitem__(3))
       row.append('{:11.5f}'.format(z))
       z = float(r_rows[j].__getitem__(4))
       row.append('{:.2f}'.format(z))
       z = float(r_rows[j].__getitem__(5))
       row.append('{:.3f}'.format(z))
       z = float(r_rows[j].__getitem__(6))
       row.append('{:.2f}'.format(z))
       row.append('{:.3f}'.format(alat))
      else:
       row.append('')
      row.append('')

      if theor_err >= -100 :
       err = '{:.1f}'.format(theor_err)
       row.append(err)
      else:
       row.append('')
      if abs(theor_err)<=good:
        n_good = n_good+1
        averr_good = averr_good + abs(theor_err)
      elif abs(theor_err)<=ok :
        n_ok = n_ok+1
        averr_ok = averr_ok + abs(theor_err)
      elif abs(theor_err)<=100:
       n_fail = n_fail+1
       averr_fail = averr_fail + abs(theor_err)
      else:
       n_nogold = n_nogold+1

      if exp_err >= -100 :
       err = '{:.1f}'.format(exp_err)
       row.append(err)
      else:
       row.append('')

      if abs(theor_err)>ok:
       if abs(theor_err)<=100:
        row.append("FAIL")
       else:
        row.append("NO_INFO")
      else:
        row.append('')
      if aFP>0:
       row.append('{:.3f}'.format(aFP))
      else:
       row.append('')
      if aexp>0:
       row.append('{:.3f}'.format(aexp))
      else:
       row.append('')
 
      writer.writerow(row)
 print("number of tests: ",n_test,"       av.Err: ",'{:,.2f}'.format((averr_good+averr_ok)/max(1,(n_ok+n_good))))
 print("           good: ",n_good," av.Err(good): ",'{:,.2f}'.format(averr_good/max(1,n_good)))
 print("             ok: ",n_ok,  "   av.Err(ok): ",'{:,.2f}'.format(averr_ok/max(1,n_ok)))
 print("           fail: ",n_fail)
 if n_nogold>0:
  print("      no golden: ",n_nogold)

#with open('tmp.csv', 'rb') as csvfile:
# dictGolden = csv.DictReader(csvfile)
# for row in dictGolden:
#   print row



if __name__ == "__main__":
    if len(sys.argv)==1:
     csv_report("-h")
    else:
     csv_report(sys.argv[1:])


