#!/bin/csh -f
# example: gentab.sh MURN 'LOGS/*A1*xc10*22.log' > ! a1_lda22.csv

grep -H $1 $2 | sed /"_"/s//" "/ | rpl "_xc" " xc" | rpl "\.xyz" " xyz" | rpl "_asa=" " asa= " |\
  rpl "\/" " " | rpl "\.log" " log" | rpl "a0=" "a0= " | rpl "B0'=" "B0'= " | rpl "V0=" "V0= "| sort |\
    awk '{  if ( $10>0 ) print $2 "," $3 ", " $10 "," $13 "," $16 ", " $21 "," $19 }'


#    awk '{  if ( $10>0 ) print $2 " " $3 "  " $10 " " $13 " " $16 "  " $5 " " $21 " " $19}'

#    awk '{if ( $16<10 && $16>-10 && $19<40 && $19>-40 ) print $2 " " $3 "  " $10 " " $13 " " $16 "  " $5 " " $21 " " $19}'
