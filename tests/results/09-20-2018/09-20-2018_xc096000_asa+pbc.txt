number of tests:  55        av.Err:  0.90
           good:  30  av.Err(good):  0.66
             ok:  6    av.Err(ok):  2.14
           fail:  13
      no golden:  6
test.x_096000/LOGS/CeO2ES_C1_ES.xyz_xc096000_asa=12.log:  a0= 10.098 Bohr  E0=    -6015.31364 Ry  B0=    2.053 Mbar  B0'=     9.59  V0=   85.80411 :MURNAGHAN
test.x_096000/LOGS/CeO2ES_C1_ES.xyz_xc096000_asa=12.log:  a0= 10.099 Bohr  E0=    -6015.31364 Ry  B0=    2.027 Mbar  B0'=     8.30  V0=   85.84468 :VINET
test.x_096000/LOGS/HfC_B1.xyz_xc096000_asa=12.log:  a0=  9.045 Bohr  E0=   -15147.86908 Ry  B0=    3.015 Mbar  B0'=    -1.83  V0=   92.49266 :MURNAGHAN
test.x_096000/LOGS/HfC_B1.xyz_xc096000_asa=12.log:  a0=  9.045 Bohr  E0=   -15147.86908 Ry  B0=    3.014 Mbar  B0'=    -1.85  V0=   92.49510 :VINET
test.x_096000/LOGS/MgO_B1.xyz_xc096000_asa=12.log:  a0=  8.072 Bohr  E0=     -275.94240 Ry  B0=    1.881 Mbar  B0'=    -0.38  V0=   65.74715 :MURNAGHAN
test.x_096000/LOGS/MgO_B1.xyz_xc096000_asa=12.log:  a0=  8.072 Bohr  E0=     -275.94241 Ry  B0=    1.881 Mbar  B0'=    -0.40  V0=   65.74762 :VINET
test.x_096000/LOGS/MgS_B1.xyz_xc096000_asa=12.log:  a0=  9.909 Bohr  E0=     -600.00850 Ry  B0=    1.392 Mbar  B0'=     0.08  V0=  121.61182 :MURNAGHAN
test.x_096000/LOGS/MgS_B1.xyz_xc096000_asa=12.log:  a0=  9.908 Bohr  E0=     -600.00849 Ry  B0=    1.396 Mbar  B0'=     0.03  V0=  121.59922 :VINET
test.x_096000/LOGS/Th_A1.xyz_xc096000_asa=12.log:  a0=  9.278 Bohr  E0=   -53127.12943 Ry  B0=    0.906 Mbar  B0'=    -4.80  V0=  199.63745 :MURNAGHAN
test.x_096000/LOGS/Th_A1.xyz_xc096000_asa=12.log:  a0=  9.276 Bohr  E0=   -53127.12944 Ry  B0=    0.913 Mbar  B0'=    -4.39  V0=  199.54491 :VINET
