#!/bin/tcsh -f

set DATE=$1

setenv GOLD ../golden/golden_alat.csv
setenv PATH `pwd`/../bin:${PATH}
setenv PATH /opt/intel/composer_xe_2015.2.164/debugger/python/intel64/bin:${PATH}

( cd test.xxx_11/LOGS ; to_csv.sh 11 10 )
( cd test.xxx_12/LOGS ; to_csv.sh 12 10 )
( cd test.xxx_22/LOGS ; to_csv.sh 22 10 )
( cd test.xxx_22g/LOGS ; to_csv.sh 22 116133 )

( cd test.xxx_11/LOGS ; \
  cat a1_xc10_11.csv a2_xc10_11.csv a4_xc10_11.csv b1_xc10_11.csv b2_xc10_11.csv \
      b3es_xc10_11.csv other_xc10_11.csv | rpl " " "" > dvlp_xc10_asa+mtc.csv )

( cd test.xxx_12/LOGS ; \
  cat a1_xc10_12.csv a2_xc10_12.csv a4_xc10_12.csv b1_xc10_12.csv b2_xc10_12.csv \
      b3es_xc10_12.csv other_xc10_12.csv | rpl " " "" > dvlp_xc10_asa+pbc.csv )

( cd test.xxx_22/LOGS ; \
  cat a1_xc10_22.csv a2_xc10_22.csv a4_xc10_22.csv b1_xc10_22.csv b2_xc10_22.csv \
      b3es_xc10_22.csv other_xc10_22.csv | rpl " " "" > dvlp_xc10_vp+pbc.csv )

( cd test.xxx_22g/LOGS ; \
  cat a1_xc116133_22.csv a2_xc116133_22.csv a4_xc116133_22.csv b1_xc116133_22.csv b2_xc116133_22.csv \
      b3es_xc116133_22.csv other_xc116133_22.csv | rpl " " "" > dvlp_xc116133_vp+pbc.csv )

( csv_report.py ${GOLD} \
    test.xxx_11/LOGS/dvlp_xc10_asa+mtc.csv 10 ${DATE}_xc10_asa+mtc.csv > ${DATE}_xc10_asa+mtc.txt )
( grep B0= test.xxx_11/LOGS/* | grep -v POLYNM2 | awk '{if ( ( $12<1.01 || $12 > 8) || $9<0.01 ) print  }' \
                           >>! ${DATE}_xc10_asa+mtc.txt )

( csv_report.py ${GOLD} \
    test.xxx_12/LOGS/dvlp_xc10_asa+pbc.csv 10 ${DATE}_xc10_asa+pbc.csv > ${DATE}_xc10_asa+pbc.txt )
( grep B0= test.xxx_12/LOGS/* | grep -v POLYNM2 | awk '{if ( ( $12<1.01 || $12 > 8) || $9<0.01 ) print  }' \
                           >>! ${DATE}_xc10_asa+pbc.txt )

( csv_report.py ${GOLD} \
    test.xxx_22/LOGS/dvlp_xc10_vp+pbc.csv 10 ${DATE}_xc10_vp+pbc.csv > ${DATE}_xc10_vp+pbc.txt )
( grep B0= test.xxx_22/LOGS/* | grep -v POLYNM2 | awk '{if ( ($12<1.01 || $12 > 8) || $9<0.01 ) print  }' \
                           >>! ${DATE}_xc10_vp+pbc.txt )

( csv_report.py ${GOLD} \
    test.xxx_22g/LOGS/dvlp_xc116133_vp+pbc.csv 116133 ${DATE}_xc116133_vp+pbc.csv > ${DATE}_xc116133_vp+pbc.txt )
( grep B0= test.xxx_22g/LOGS/* | grep -v POLYNM2 | awk '{if ( ( $12<1.01 || $12 > 8) || $9<0.01 ) print  }' \
                           >>! ${DATE}_xc116133_vp+pbc.txt )

( cd test.x_577298/LOGS ; to_csv.sh 12 577298 )

( cd test.x_577298/LOGS ; \
  cat a1_xc577298_12.csv a2_xc577298_12.csv a4_xc577298_12.csv b1_xc577298_12.csv b2_xc577298_12.csv \
      b3es_xc577298_12.csv other_xc577298_12.csv | rpl " " "" > dvlp_xc577298_asa+pbc.csv )

( csv_report.py ${GOLD} \
    test.x_577298/LOGS/dvlp_xc577298_asa+pbc.csv 10 ${DATE}_xc557298_asa+pbc.csv > ${DATE}_xc557298_asa+pbc.txt )

( grep B0= test.x_577298/LOGS/* | grep -v POLYNM2 | awk '{if ( ( $12<1.01 || $12 > 8) || $9<0.01 ) print  }' \
                           >>! ${DATE}_xc557298_asa+pbc.txt )

( cd test.x_096000/LOGS ; to_csv.sh 12 096000 )

( cd test.x_096000/LOGS ; \
  cat a1_xc096000_12.csv a2_xc096000_12.csv a4_xc096000_12.csv b1_xc096000_12.csv b2_xc096000_12.csv \
      b3es_xc096000_12.csv other_xc096000_12.csv | rpl " " "" > dvlp_xc096000_asa+pbc.csv )

( csv_report.py ${GOLD} \
    test.x_096000/LOGS/dvlp_xc096000_asa+pbc.csv 116133 ${DATE}_xc096000_asa+pbc.csv > ${DATE}_xc096000_asa+pbc.txt )

( grep B0= test.x_096000/LOGS/* | grep -v POLYNM2 | awk '{if ( ( $12<1.01 || $12 > 8) || $9<0.01 ) print  }' \
                           >>! ${DATE}_xc096000_asa+pbc.txt )

( cd test.x_577000/LOGS ; to_csv.sh 12 577000 )

( cd test.x_577000/LOGS ; \
  cat a1_xc577000_12.csv a2_xc577000_12.csv a4_xc577000_12.csv b1_xc577000_12.csv b2_xc577000_12.csv \
      b3es_xc577000_12.csv other_xc577000_12.csv | rpl " " "" > dvlp_xc577000_asa+pbc.csv )

( csv_report.py ${GOLD} \
    test.x_577000/LOGS/dvlp_xc577000_asa+pbc.csv 10 ${DATE}_xc557000_asa+pbc.csv > ${DATE}_xc557000_asa+pbc.txt )

( grep B0= test.x_577000/LOGS/* | grep -v POLYNM2 | awk '{if ( ( $12<1.01 || $12 > 8) || $9<0.01 ) print  }' \
                           >>! ${DATE}_xc557000_asa+pbc.txt )

