number of tests:  72        av.Err:  1.31
           good:  39  av.Err(good):  0.96
             ok:  23    av.Err(ok):  1.90
           fail:  9
      no golden:  1
test.xxx_22g/LOGS/Ba_A2.xyz_xc116133_asa=22.log:  a0=  9.114 Bohr  E0=   -16272.57869 Ry  B0=    0.086 Mbar  B0'=    -1.17  V0=  378.58757 :MURNAGHAN
test.xxx_22g/LOGS/Ba_A2.xyz_xc116133_asa=22.log:  a0=  9.115 Bohr  E0=   -16272.57869 Ry  B0=    0.086 Mbar  B0'=    -1.19  V0=  378.59384 :VINET
test.xxx_22g/LOGS/fm_Fe_A2.xyz_xc116133_asa=22.log:  a0=  5.311 Bohr  E0=    -2543.30906 Ry  B0=    1.962 Mbar  B0'=     8.50  V0=   74.88633 :MURNAGHAN
test.xxx_22g/LOGS/fm_Fe_A2.xyz_xc116133_asa=22.log:  a0=  5.311 Bohr  E0=    -2543.30907 Ry  B0=    1.996 Mbar  B0'=     8.40  V0=   74.88722 :VINET
test.xxx_22g/LOGS/Pb_A1.xyz_xc116133_asa=22.log:  a0=  9.123 Bohr  E0=   -41846.53547 Ry  B0=    0.377 Mbar  B0'=    13.18  V0=  189.79966 :MURNAGHAN
test.xxx_22g/LOGS/Pb_A1.xyz_xc116133_asa=22.log:  a0=  9.119 Bohr  E0=   -41846.53546 Ry  B0=    0.377 Mbar  B0'=    15.36  V0=  189.57377 :VINET
test.xxx_22g/LOGS/Rb_A2.xyz_xc116133_asa=22.log:  a0= 10.514 Bohr  E0=    -5959.34041 Ry  B0=    0.035 Mbar  B0'=    -0.98  V0=  581.10740 :MURNAGHAN
test.xxx_22g/LOGS/Rb_A2.xyz_xc116133_asa=22.log:  a0= 10.514 Bohr  E0=    -5959.34041 Ry  B0=    0.035 Mbar  B0'=    -0.99  V0=  581.11108 :VINET
