('number of tests: ', 72, '       av.Err: ', '1.27')
('           good: ', 40, ' av.Err(good): ', '0.93')
('             ok: ', 22, '   av.Err(ok): ', '1.88')
('           fail: ', 9)
('      no golden: ', 1)
test.xxx_22g/LOGS/Ba_A2.xyz_xc116133_asa=22.log:  a0=  9.115 Bohr  E0=   -16272.82918 Ry  B0=    0.088 Mbar  B0'=    -0.95  V0=  378.70243 :MURNAGHAN
test.xxx_22g/LOGS/Ba_A2.xyz_xc116133_asa=22.log:  a0=  9.115 Bohr  E0=   -16272.82918 Ry  B0=    0.088 Mbar  B0'=    -0.97  V0=  378.70794 :VINET
test.xxx_22g/LOGS/HfC_B1.xyz_xc116133_asa=22.log:  a0=  8.818 Bohr  E0=   -15132.35240 Ry  B0=    2.173 Mbar  B0'=    -0.09  V0=   85.70981 :MURNAGHAN
test.xxx_22g/LOGS/HfC_B1.xyz_xc116133_asa=22.log:  a0=  8.818 Bohr  E0=   -15132.35240 Ry  B0=    2.171 Mbar  B0'=    -0.12  V0=   85.71276 :VINET
test.xxx_22g/LOGS/K_A2.xyz_xc116133_asa=22.log:  a0= 10.064 Bohr  E0=    -1202.66497 Ry  B0=    0.008 Mbar  B0'=    11.77  V0=  509.69429 :MURNAGHAN
test.xxx_22g/LOGS/Pb_A1.xyz_xc116133_asa=22.log:  a0=  9.120 Bohr  E0=   -41850.93326 Ry  B0=    0.307 Mbar  B0'=    16.95  V0=  189.66237 :MURNAGHAN
test.xxx_22g/LOGS/Pb_A1.xyz_xc116133_asa=22.log:  a0=  9.115 Bohr  E0=   -41850.93325 Ry  B0=    0.309 Mbar  B0'=    20.45  V0=  189.29662 :VINET
test.xxx_22g/LOGS/Th_A1.xyz_xc116133_asa=22.log:  a0=  9.106 Bohr  E0=   -53070.68558 Ry  B0=    0.754 Mbar  B0'=     0.43  V0=  188.75266 :MURNAGHAN
test.xxx_22g/LOGS/Th_A1.xyz_xc116133_asa=22.log:  a0=  9.105 Bohr  E0=   -53070.68558 Ry  B0=    0.749 Mbar  B0'=     0.33  V0=  188.72913 :VINET
test.xxx_22g/LOGS/W_A2.xyz_xc116133_asa=22.log:  a0=  5.965 Bohr  E0=   -32325.31294 Ry  B0=    3.908 Mbar  B0'=     0.64  V0=  106.12653 :MURNAGHAN
test.xxx_22g/LOGS/W_A2.xyz_xc116133_asa=22.log:  a0=  5.965 Bohr  E0=   -32325.31294 Ry  B0=    3.912 Mbar  B0'=     0.62  V0=  106.12474 :VINET
test.xxx_22g/LOGS/fm_Fe_A2.xyz_xc116133_asa=22.log:  a0=  5.311 Bohr  E0=    -2543.30287 Ry  B0=    1.956 Mbar  B0'=     8.49  V0=   74.88642 :MURNAGHAN
test.xxx_22g/LOGS/fm_Fe_A2.xyz_xc116133_asa=22.log:  a0=  5.311 Bohr  E0=    -2543.30288 Ry  B0=    1.989 Mbar  B0'=     8.37  V0=   74.88892 :VINET
