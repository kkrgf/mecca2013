!BOP
!!ROUTINE: structure1
! !DESCRIPTION: to compute structure-related data (old subroutine)

!TO DO:
! May be structure1 should be internal private procedure of module struct?
!\\
! !INTERFACE:
      subroutine structure1(alat,boa,coa,nbasis,                        &
     &                     rslatt,kslatt,bases,itype,aij,ndimbas,       &
     &                     numbsub,nsublat,                             &
     &                     mtasa,                                       &
     &                     if0,mapstr,mappnt,mapij,imethod,             &
     &                     omega,                                       &
     &                     imdlng,                                      &
     &                     invadd,                                      &
     &                     iyymom,                                      &
     &                     dop,lmax,rot,ib,nrot,idop,                   &
     &                     nq,qmesh,ndimq,nmesh,nqpt,ispkpt,            &
     &                     wghtq,twght,lrot,                            &
     &                     Rksp,xknlat,ndimk,nksn,                      &
     &                     Rrsp,xrnlat,ndimr,nrsn,                      &
     &                     xrslatij,nrhp,ndimrhp,                       &
     &                     np2r,numbrs,ncount,                          &
     &                     iprint,istop)
!c     ==================================================================
! !USES:
      use mecca_constants
      use mecca_types
      use mecca_interface, only : latvect, ewldvect, cellvolm
!EOP
      implicit none
!c
!c    ======================================================================
!c    modified for calculation of lattice vectors          by  A.S. Aug 1998
!c    modified for more structures                         by   WAS Jan 1994
!c    modified for mean-field, charge-correlated CPA       by   DDJ Dec 1993
!c    ======================================================================
!c
!c     ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!      include 'ShapeCorr.h'
!c
      real(8) :: alat,boa,coa   !  boa = B/A, coa = C/A
      integer, intent(in)  :: nbasis,ndimbas
!c                rslatt,bases,rmt -- in alat-units
      real(8), intent(in)  :: rslatt(3,3)
      real(8), intent(out) :: kslatt(3,3)
      real(8), intent(in)  :: bases(3,nbasis)
!c                itype(i):  site --> sublattice
!c                  (can be negative for atoms without valence electrons)
!c                numbsub(nsublat) -- number of sites in the sublattice
      integer :: itype(nbasis),nsublat,numbsub(nsublat)
      real(8), intent(out) :: aij(3,*)
      integer, intent(out) :: ncount
!      real(8) :: rmt(nsublat)
      integer, intent(in) :: mtasa
      integer, intent(out) :: if0(48,nbasis)
      integer, intent(out) :: mapstr(ndimbas,*)
      integer, intent(out) :: mappnt(ndimbas,*)
      integer, intent(out) :: mapij(2,*)
      integer, intent(in) :: imethod
      real(8), intent(out) :: omega
!      real(8) :: omegaint
!      real(8) :: omegamt(nsublat)
!      real(8) :: surfamt(nsublat)
      integer :: imdlng,invadd,iyymom
      integer, intent(in) :: lmax
      complex(8), intent(out) ::                                        &
     &                        dop((lmax+1)*(lmax+1)*(lmax+1)*(lmax+1),*)
      integer :: nrot,idop
      real(8), intent(out) :: rot(49,3,3)
      integer, intent(out) :: ib(48)
      integer :: ndimq,nmesh,ispkpt
      integer, intent(out) :: nqpt(nmesh)
      integer, intent(out) :: nq(3,nmesh)
      real(8), intent(out) :: qmesh(3,ndimq,nmesh)
      integer, intent(out) :: wghtq(ndimq,nmesh)
      real(8), intent(out) :: twght(nmesh)
      integer, intent(out) :: lrot(ndimq,nmesh)
!c
!c            input:  Rksp -- in 1/alat-units; Rrsp -- in alat-units
!c
      real(8), intent(in)  :: Rksp,Rrsp
      integer :: ndimk,ndimr,ndimrhp
      integer, intent(out) :: nksn(nmesh)
!c                 xknlat -- in 2*pi/alat-units;
      real(8), allocatable :: xknlat(:)  ! (ndimk*3*nmesh)
!c                 xrnlat,xrslatij -- in alat-units
      integer, intent(out) :: nrsn
      real(8), allocatable :: xrnlat(:)    ! (ndimr*3)
      integer, intent(out) :: nrhp
      real(8), allocatable :: xrslatij(:,:)  !  (ndimrhp,4)
      integer, allocatable :: np2r(:,:)
      integer, allocatable :: numbrs(:)
      integer, intent(in) :: iprint
      character(10) :: istop

!cab      complex*16   dop(ipkkr,ipkkr,*)
      real(8) :: rx(3,nbasis)
      integer :: if0temp(nbasis)

!c      integer    lwght(ipkpts*ipmesh)
!c      integer, allocatable ::   lwght(:)
!c
      real(8) ::       tmparr(nsublat)

!c      integer jf0(ipbase,ipbase)
!c      integer kcheck(ipkpts)

      integer, allocatable ::   jf0(:,:)
      integer, allocatable ::   kcheck(:)
      integer  erralloc

!      logical lScatShCorr
      logical :: printed
      integer :: i,j,ir,nsub,iatom,iatsub,imesh,isite,isite0,isite1
      integer :: nmom,nmomylm,Lmltp
      integer :: ident,istriz,nnmesh
      integer :: NShC
      real(8) :: volume
      real(8) :: kslatt_(9)
!      real(8) :: epsShC,OutVPvlm

!c parameter
      character(10), parameter :: sname='structure'
      real(8), parameter :: twopi=two*pi
!c

!BOC
!c    ======================================================================

!c
!c  BTW,   volume*alat^3 -- volume in atomic units
!c
      if(iprint.ge.-10) then
         write(6,'(/1x,3(a,f22.15))')                                   &
     &    'lattice constant = ',alat,' Bohr;  b/a = ',boa,' c/a = ',coa
         write(6,'(1x,''real space basis vectors are'')')
         write(6,'(10x,3f22.15)') ((rslatt(i,j),i=1,3),j=1,3)
         if(nbasis.le.20.or.iprint.ge.1) then
          write(6,*)
          write(6,*) ' Atomic data::'
          write(6,1100) (j,itype(j),(bases(i,j),i=1,3),j=1,nbasis)
1100        format (9x,'Atom',i5,' of type',i5,' is at ',3g20.12)
         endif
         write(6,*)
      endif
      if ( minval(itype(1:nbasis))==0 .or.                              &
     &        maxval(itype(1:nbasis))>nsublat ) then
        write(6,*) ' nsublat=',nsublat
        write(6,*) ' minval(itype)=',minval(itype(1:nbasis))
        write(6,*) ' maxval(itype)=',maxval(itype(1:nbasis))
        call p_fstop(sname//' broken ITYPE array')
      end if
      if(nbasis.gt.ndimbas) then
       write(*,*) ' STRUCTURE::  nbasis > ndimbas',nbasis,ndimbas
       call p_fstop(sname//' nbasis > ndimbas')
      end if

!!      tmparr(1:nsublat) = rmt(1:nsublat)     ! rmt-input == rmt-weights
!      tmparr(1:nsublat) = 0
!
!      nmom = ipmtout
!!CAB      nmomylm = max(2,lmax,ipmltp)   ! nmomylm is a last YY-moment calculated
!      nmomylm = nmom                    ! nmomylm is a last YY-moment calculated
!!C      Lmltp   = 0
!      Lmltp   = ipmltp
!      call VPdata2(mtasa,alat,boa,coa,                                  &
!     &            rslatt,nbasis,bases,itype,tmparr,                     &
!     &            Lmltp,nmom,lmax,nmomylm,epsShC,                       &
!     &            iyymom,OutVPvlm,                                      &
!     &            iprint                                                &
!     &           )
!
!      itype(1:nbasis) = abs(itype(1:nbasis))

!      i = 2
!      call OutMTModule(i)
!      if(i.eq.2) then
!       lScatShCorr = .true.
!      else
!       lScatShCorr = .false.
!      end if

!      if(mtasa.gt.0) then
!       call RasaModule(0,1,nsublat,rmt)       ! rmt-output: Rasa
!!CDEBUG       if(mtasa.eq.3.or.mtasa.eq.4) then
!       if(lScatShCorr                                                    &
!     &    .or.(mtasa.eq.3.or.mtasa.eq.4.or.mtasa.eq.5)                  &
!     &   ) then
!      call EnergShC(NShC,iprint)
!       end if
!      else
!       call SmtModule(0,1,nsublat,rmt)        ! rmt-output: Smt
!       i = 2
!       call OutMTModule(i)
!       if(lScatShCorr) then
!         call EnergShC(NShC,iprint)
!       end if
!      end if
!
!      rmt(1:nsublat)  = alat*rmt(1:nsublat)

      volume = cellvolm(rslatt,kslatt_,1)
      kslatt = reshape(kslatt_,[3,3])
      omega = volume*alat**3

!      OutVPvlm = OutVPvlm*alat**3

      if(iprint.ge.-10) then
       write(6,*)
       write(6,'('' reciprocal space basis vectors are '')')
       write(6,'(10x,3f22.15)') ((kslatt(i,j),i=1,3),j=1,3)
      endif

!      omegaint=zero
!      do i=1,nsublat
!       surfamt(i)=(4.d0*pi)*rmt(i)**2
!       omegamt(i)=(4.d0*pi/3.d0)*rmt(i)**3
!       omegaint=omegaint+omegamt(i)*numbsub(i)
!      enddo
!      omegaint=omega-omegaint

!c
!c     ***********************************************************
!c     Major printout............................................

      write(6,*)
      write(6,'(''  Volume of Real-space Cell:: '',ES20.13,'' at.un'',   &
     &           '' = '',ES20.13,'' D.U.'')') omega,volume
!      if(OutVPvlm.gt.zero) then
!       write(6,'(''            Non-V.P. Volume:: '',d14.6,'' at.un'',   &
!     &       '' = '',d14.6,'' D.U.'')') OutVPvlm,OutVPvlm/alat**3
!      end if

      write(6,'(''  Number of basis atoms '',                           &
     &              t40,''='',i5)') nbasis

!      if(mtasa.gt.0.and.abs(omegaint/omega).gt.1.d-8) then
!       write(*,*)
!       write(*,'('' ERROR:: omegaint='',d11.4)') omegaint
!       call p_fstop(sname//': wrong ASA-radii???')
!      end if

!c
      if(max(ipxkns,iprsn).lt.ndimk) then
       write(*,*) ' STRUCTURE:: ipxkns,iprsn,ndimk =>',                 &
     &              ipxkns,iprsn,ndimk
       call p_fstop(sname//' ipxkns/iprsn < ndimk')
      end if

      if(ispkpt.eq.0.or.abs(ispkpt).eq.1) then
       nnmesh = nmesh
      else
       nnmesh = 0
       do i=1,nmesh
      nqpt(i) = 1
       end do
      end if

      allocate(                                                         &
     &         kcheck(1:ndimq)                                          &
     &         ,jf0(1:nbasis,1:nbasis)                                  &
     &         ,stat=erralloc)
      if(erralloc.ne.0) then
       write(6,*) ' NDIMQ=',ndimq
       write(6,*) ' NBASIS=',nbasis
       write(6,*) ' ERRALLOC=',erralloc
       call p_fstop(sname//' :: ALLOCATION MEMORY PROBLEM ')
      end if

!CAB      istriz = ikpsym
      istriz = 0
      call genkpt1(rslatt,kslatt,bases,itype,nbasis,                    &
     &              qmesh,wghtq,nnmesh,istriz,invadd,                   &
     &              rot,ib,if0,if0temp,rx,jf0,kcheck,lrot,              &
     &              ndimq,nq,nqpt,nrot,ispkpt.ne.1)
      do imesh=1,nmesh
       twght(imesh) = sum(wghtq(1:nqpt(imesh),imesh))
      end do

      deallocate(                                                       &
     &          kcheck                                                  &
     &         ,jf0                                                     &
     &         ,stat=erralloc)

      if(idop.ne.1) then
       call dops( rot, dop, nrot, lmax  )
      end if

      if ( nrot>1 ) then
       if(iprint.ge.1) then
        write(6,*)
        write(6,*)                                                      &
     &    ('      Rotation matrices in Cartesian coordinates')
        do ir = 1,nrot
         write(6,1002) ir,((rot(ir,j,i),i=1,3),j=1,3)
1002     format (i3,3(1x,3f8.4))
!c        read(82,*) iir,((rot(ir,i,j),i=1,3),j=1,3)
        end do
       end if
       if(iprint.ge.0) then
        write(6,*)
        write(6,*)'          Atom transformation table:'
        do i=1,min(nbasis,999)
         write(6,1003) i,(if0(ir,i),ir=1,nrot)
1003     format(/' Atom ',i4,' is moved to atom ',12i4/                 &
     &          (28x,12i4))
        enddo
       end if
      else
       if ( iprint.ge.0 ) then
        write(6,'(/'' No rotations are found'')')
       end if
      end if

      printed = .false.
      do nsub=1,nsublat
       do iatom=1,nbasis
        if(abs(itype(iatom)).eq.nsub) exit
       end do
       isite0 = iatom

       isite1 = isite0+1
       do iatsub=2,numbsub(nsub)
        do isite=isite1,nbasis
         if(abs(itype(isite)).eq.nsub) go to 20
        end do
        write(6,*) ' NSUB=',nsub
        write(6,*) ' NUMBSUB(NSUB)=',numbsub(nsub)
        write(6,*) ' ISITE0=',isite0,' ITYPE(ISITE0)=',itype(isite0)
        write(6,*) ' (ITYPE(isite), isite=',isite1,nbasis,') ='
        write(6,*) (itype(isite),isite=isite1,nbasis)
        call p_fstop(sname//':: you have wrong NUMBSUB ?')
20      isite1 = isite+1

        do ir=1,nrot
         if(if0(ir,isite0).eq.isite) go to 30
        end do
        if ( .not.printed ) then
         write(6,'(2x,2a)') 'Input structural file enforces equivalency'&
     &   ,' of sites for inequivalent (by symmetry) sublattices'
         printed = .true.
         write(6,'(2x,a,t36,a,t45,a,t55,a)')                            &
     &     'WARNING: accuracy may be low','nsub','isite0','isite'
        end if
        write(6,'(t35,i5,t45,i5,t55,i5)') nsub,isite0,isite
!     &' You have wrong IF0 or Atoms are not equivalent by symmetry?')
!CAB      call p_fstop(sname//': you have wrong IF0()')
30      continue
       end do
      end do

      write(6,*)

      nnmesh = nmesh    !TODO xknlat could be calculated for each k-point
!TODO      nnmesh = 0

      call latvect(rslatt,bases,aij,kslatt,nbasis,                      &
     &              qmesh,ndimq,nnmesh,nqpt,                            &
     &              Rksp,xknlat,ndimk,nksn,                             &
     &              Rrsp,xrnlat,ndimr,nrsn,                             &
     &              iprint)
      ndimk = sum(nksn(1:nmesh))

      if(imdlng.le.1) then

!CDEBUG       call madelung(nbasis,rslatt,bases,
!CDEBUG     *               madmat,ndimbas,iprint,istop)

       if(imethod.eq.3) then
        ident = -1
       else if(imethod.le.0) then
        ident = -100
       else
!CDEBUG       ident = 0
        ident = -100
       end if

       call strident1(bases,ndimbas,nbasis,itype,mapstr,mappnt,         &
     &                mapij,aij,ncount,ident,iprint)

!c
!c                 a_ij = b_i - b_j
!c

       if ( allocated(np2r) ) deallocate(np2r)
       allocate(np2r(ndimr,ncount))
       if ( allocated(numbrs) ) deallocate(numbrs)
       allocate(numbrs(ncount))

       if ( allocated(xrslatij) ) then
         ndimrhp = size(xrslatij,1)
       else
         ndimrhp = 0
       end if
       call ewldvect(reshape(xrnlat,[ndimr,3]),nrsn,ndimr,Rrsp,         &
     &                    aij,ncount,                                   &
     &                    xrslatij,nrhp,ndimrhp,                        &
     &                    np2r,numbrs)

       if ( iprint>=0 ) then

        if(nbasis.le.10.or.iprint.ge.1) then
         write(6,*)
         write(6,*) '  Map of (I,J)-equivalence'
         do j=1,nbasis
          write(6,1006) (mapstr(j,i),mappnt(j,i),i=1,nbasis)
1006      format(8(i6,i2))
         end do
        end if

        write(6,*)
        write(6,*) '  Rrsp=',real(Rrsp),'* alat'
        write(6,*)
        write(6,*) '   NRSN=',nrsn,'      NRHP=',nrhp
        write(6,*) '  NDIMR=',ndimr,'  NDIMRHP=',ndimrhp

       end if

       xrnlat = xrnlat*twopi

       do i = 1,nrhp
       xrslatij(i,1) = xrslatij(i,1)*twopi
       xrslatij(i,2) = xrslatij(i,2)*twopi
       xrslatij(i,3) = xrslatij(i,3)*twopi
       xrslatij(i,4) = xrslatij(i,4)*(twopi*twopi)
       end do

       do i=1,ncount
       aij(1,i)=aij(1,i)*twopi
       aij(2,i)=aij(2,i)*twopi
       aij(3,i)=aij(3,i)*twopi
       enddo
!c                                now again a_ij = b_i-b_j

      else                      ! (imdlng > 1)

       call p_fstop(sname//': imdlng > 1 is not implemented')

      end if                    ! end of if(imdlng.le.1)

      if(istop.eq.sname) then
      call fstop(sname)
      endif
      return
!EOC
      end subroutine structure1
