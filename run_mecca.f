!BOP
!!ROUTINE: run_mecca
!!INTERFACE:
       subroutine run_mecca (inpfile,outinfo)
!!DESCRIPTION:
! this subroutine runs single {\sc mecca} calculation 
!
!!USES:
       use mecca_types
       use  input
       use mecca_scf_interface

!!DO_NOT_PRINT
       implicit none
!!DO_NOT_PRINT

!!ARGUMENTS:
       character*(*), intent(in)   :: inpfile
       character(120), intent(out) :: outinfo
!!REVISION HISTORY:
! Revised Version - A.S. - 2013
!EOP
!
!BOC
       type(RunState), target :: mecca_state
       type(IniFile), target :: internalFile
       integer ierr
!       integer n
!       character*80, allocatable :: in_file(:)
!       integer ne
!       character*80 infile/''/

       mecca_state%intfile => internalFile
       call read2intFile(inpfile,mecca_state%intfile,ierr)
       if ( ierr == 0 ) then
         call mecca_scf(mecca_state,outinfo)
       else
         if ( ierr == -1 ) then
         write(*,*)                                                     &
     &   ' ERROR :: no access to file <'//trim(inpfile)//'>'
         end if
       end if
!DELETE       infile = trim(inpfile)
!DELETE       n = 0
!DELETE       call readIniFile(infile,n)
!DELETE
!DELETE       if (n .gt. 0) then
!DELETE         allocate(in_file(n+1))
!DELETE         in_file(1) = infile
!DELETE         ne = 1
!DELETE         call readIniFile(in_file,ne)
!DELETE
!DELETE         if (ne .gt. n) then
!DELETE           write(*,*)                                                   &
!DELETE     &  ' ERROR :: not enough memory has been allocated',               &
!DELETE     &  ' n=','  ne=',ne
!DELETE           return
!DELETE         else if ( ne .le. 0 )  then
!DELETE           write(*,*) ' input error? '
!DELETE           return
!DELETE         end if
!DELETE
!DELETE         call read2Input(ne,in_file,internalFile)
!DELETE         if ( internalFile%alat <=0 ) then
!DELETE           call readInput(ne,in_file,internalFile)
!DELETE         end if
!DELETE         deallocate(in_file)
!DELETE!         call saveInput(internalFile,internalFile%name)
!DELETE         call mecca_scf(mecca_state,outinfo)
!DELETE       else if ( n==0 ) then
!DELETE         write(*,*)                                                     &
!DELETE     &  ' ERROR :: read-error for file '//trim(inpfile)
!DELETE       else
!DELETE         write(*,*)                                                     &
!DELETE     &   ' ERROR :: no access to file <'//trim(inpfile)//'>'
!DELETE       end if
       return
!EOC
       end subroutine run_mecca

