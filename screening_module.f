      module screening
      
      implicit none

      real(8), parameter :: one=1.d0
      integer, parameter :: calcref1 = 1  ! r_ref is the same for all sublattices
      integer, parameter :: calcref0 = 0  ! r_ref depends on a sublattice
      integer, parameter :: calczero = -1 ! very small screening (to debug)
      integer :: icalcref = calcref0

!        potential parameter for screened method
      real(8), save :: U0=4.d0
!
!        See P.Zahn et al, Phil.Magazine B, 1998, Vol.78, N.5/6, p.411
!
      real(8), save :: crmt = 0.8d0

      contains
      
      function g_scr_potpar()
      implicit none
      real(8) :: g_scr_potpar
      g_scr_potpar = U0
      end function g_scr_potpar

      subroutine s_scr_potpar(u0_ref)
      implicit none
      real(8), intent(in) :: u0_ref
      U0 = u0_ref
      end subroutine s_scr_potpar

      function g_scr_rpar()
      implicit none
      real(8) :: g_scr_rpar
      g_scr_rpar = crmt
      end function g_scr_rpar

      subroutine s_scr_rpar(r0_ref)
      implicit none
      real(8), intent(in) :: r0_ref
      crmt = r0_ref
      end subroutine s_scr_rpar

      function g_calcref()
      implicit none
      integer :: g_calcref
      g_calcref = icalcref
      end function g_calcref

      subroutine s_calcref(iflag)
      implicit none
      integer, intent(in) :: iflag
      select case ( iflag )
      case (calcref0) 
        icalcref = calcref0
      case (calcref1) 
        icalcref = calcref1
      case (calczero)
        icalcref = calczero
      case default 
        icalcref = calcref0
      end select
      end subroutine s_calcref

!BOP
!!ROUTINE: gettref
!!INTERFACE:
      subroutine gettref(lmax,energy,pnrel,V0,Rmt0,                     &
     &                   trefinv,                                       &
     &                   iprint,istop)
!!DESCRIPTION:
! finds phase shifts {\tt trefinv(0:lmax)} for reference system: \\
! constant potential {\tt V0} in the sphere of radius {\tt Rmt0} \\
! (uses Ricatti-Bessel fcts. for solution)
!

!!DO_NOT_PRINT
      implicit none
!!DO_NOT_PRINT

!!ARGUMENTS:
      integer, intent(in) :: lmax,iprint
      complex(8), intent(in) :: energy,pnrel
      real(8), intent(in) :: V0,Rmt0
      complex(8), intent(out) :: trefinv(0:lmax)
      character(*), intent(in) :: istop
!!REVISION HISTORY:
! Adapted - A.S. - 2013
!!REMARKS:
! input argument energy is used only for printing;
! procedure stops if energy or V0 or Rmt0 are very close to zero
!EOP
!
!BOC
!      use mecca_constants
      integer :: l,lmin
!c
      complex(8) :: bjl(lmax+2)      ! (iplmax+2)
      complex(8) :: bnl(lmax+2)      ! (iplmax+2)
      complex(8) :: djl(lmax+2)      ! (iplmax+2)
      complex(8) :: dnl(lmax+2)      ! (iplmax+2)
      complex(8) :: bjlv(lmax+2)      ! (iplmax+2)
      complex(8) :: bnlv(lmax+2)      ! (iplmax+2)
      complex(8) :: djlv(lmax+2)      ! (iplmax+2)
      complex(8) :: dnlv(lmax+2)      ! (iplmax+2)
      complex(8) :: dradsl,radsl,radsolmt,fsn,dfsn,fsj,dfsj
      complex(8) :: pr,prv

      complex(8), parameter :: sqrtm1=(0.d0,1.d0)
      real(8), parameter :: tole=1.d-10
      character(*), parameter :: sname='gettref'
      if(iprint.ge.0) then
         write(6,'('' in GETTREF iprint '',i5)') iprint
         write(6,'(''           : lmax='',i5)') lmax
         write(6,'(''           : energy='',2d16.8)') energy
         write(6,'(''           : pnrel='',2d16.8)') pnrel
         write(6,'(''           : V0='',2d16.8)') V0
         write(6,'(''           : Rmt='',d16.8)') Rmt0
      endif

      pr=pnrel*Rmt0
      if( abs(pr)**2 .lt. tole ) then ! cannot solve for energy equal to 0.0
        call fstop(sname//':: energy=zero')
      endif

!c      prv = dcmplx(0.d0,Rmt0*sqrt(V0*function(E)),
!c              function(E) > 0
!c          AND function(E) = 1, if Im(E)=0
!c          AND function(E) decreases when Im(E) increases

      prv=dcmplx(0.d0,Rmt0*sqrt(V0))        ! then V=Energy+V0 -- scr.pot.
      if( abs(prv)**2 .lt. tole ) then ! cannot solve for V0 equal to 0.0
        call fstop(sname//':: V0=zero')
      endif
!c     =================================================================

      lmin=max(2,lmax+1)
      call ricbes(lmin,prv,bjlv(:),bnlv(:),djlv(:),dnlv(:))
      call ricbes(lmin,pr,bjl(:),bnl(:),djl(:),dnl(:))

      do l=0,lmax
       radsl = bjlv(l+1)/prv
       dradsl = (djlv(l+1) - radsl)
       radsolmt = dradsl/radsl

       fsn = bnl(l+1)/pr
       dfsn = (dnl(l+1)-fsn)
       fsj = bjl(l+1)/pr
       dfsj = (djl(l+1)-fsj)

       trefinv(l) = pnrel*(sqrtm1-                                      &
     &                     (dfsn-fsn*radsolmt)/(dfsj-fsj*radsolmt))
!c
!c         t^(-1) = k*( sqrt(-1) - cotan(delta_l))
!c
      end do

      if(istop.eq.sname) then
         call fstop(sname)
      else
         return
      endif
!EOC
      end subroutine gettref

      subroutine calcTref(nsite,lmax,pdu,rytodu,r_ref,icalcref,         &
     &                                           tref,trefinv,do_print)
      implicit none
      integer, intent(in) :: nsite,lmax
      complex(8), intent(in) :: pdu
      real(8), intent(in) :: rytodu
      real(8), intent(in) :: r_ref(:)
      integer, intent(in) :: icalcref
      complex(8), intent(out) :: tref(:,:),trefinv(:,:)   ! tref(1:kkrsz,1:nsite), only diagonal elements

      logical, intent(in), optional :: do_print

      complex(8), parameter :: cone=(1.d0,0.d0)
      integer :: j,kkrsz,l,m,nsub
      real(8) :: rmt0
      complex(8) :: pry
      complex(8) :: trefHS(1:lmax+1)
      integer :: ndx(1:(lmax+1)**2)

      kkrsz = (lmax+1)**2
      j=0
      do l=0,lmax
       do m=-l,l
         j=j+1
         ndx(j)= l + 1
       enddo
      enddo

      pry=pdu/rytodu
!------------------------------------------------------------------------
      select case (icalcref)
!------------------------------------------------------------------------
      case (calcref0)
!C=======================================================================
!C   Same reference system is used for all sites (it is not necessary)
!C=======================================================================
       rmt0 = r_ref(1) * crmt
       call gettref(lmax,pry*pry,pry,U0,rmt0,trefHS, -1,'******')
       trefHS(1:lmax+1) = trefHS(1:lmax+1)*rytodu
       if( do_print ) then
         write(6,'(a,i5,a,2d16.8,3(a,d16.8))')                          &
     &                            ' in calcTref lmax=',lmax,' p=',pry,  &
     &                         '  U0=',U0,' crmt=',crmt,' r_ref=',rmt0
         do l=1,lmax+1
           write(6,1002)  l-1,cone/trefHS(l),trefHS(l)
1002       format(i4,2d15.7,' TREF ',2d15.7,' TREFINV')
         end do
       end if
       do nsub=1,nsite
        trefinv(1:kkrsz,nsub) = trefHS(ndx(1:kkrsz))
!c                                ndx(j) --> l
       end do
       trefHS(1:lmax+1) = cone/trefHS(1:lmax+1)
       do nsub=1,nsite
        tref(1:kkrsz,nsub) = trefHS(ndx(1:kkrsz))
!c                                ndx(j) --> l
       end do
!=======================================================================
      case (calcref1)
!=======================================================================
       if( do_print ) then
          write(6,'(a,i5,a,2d16.8,2(a,d16.8))')                         &
     &                            ' in calcTref lmax=',lmax,' p=',pry,  &
     &                                        '  U0=',U0,' crmt=',crmt
       end if
       do nsub=1,nsite
        rmt0 = r_ref(nsub) * crmt
        call gettref(lmax,pry*pry,pry,U0,rmt0,trefHS, -1,'******')
        trefHS(1:lmax+1) = trefHS(1:lmax+1)*rytodu
        if( do_print ) then
          write(6,'(a,i5,a,d16.8)') ' nsub=',nsub,' r_ref=',rmt0
          do l=1,lmax+1
           write(6,1002)  l-1,cone/trefHS(l),trefHS(l)
          end do
        end if
        trefinv(1:kkrsz,nsub) = trefHS(ndx(1:kkrsz))
        trefHS(1:lmax+1) = cone/trefHS(1:lmax+1)
        tref(1:kkrsz,nsub) = trefHS(ndx(1:kkrsz))
       end do  
!=======================================================================
      case (calczero)
!=======================================================================
       if( do_print ) then
          write(6,'(a,i5,a,2d16.8,2(a,d16.8))')                         &
     &                            ' in calcTref lmax=',lmax,' p=',pry,  &
     &                                 '  U0=',1.d-11*U0,' crmt=',crmt
       end if
       do nsub=1,nsite
        rmt0 = r_ref(nsub) * crmt
        call gettref(lmax,pry*pry,pry,1.d-11*U0,rmt0,trefHS,-1,'******')
        trefHS(1:lmax+1) = trefHS(1:lmax+1)*rytodu
        if( do_print ) then
          write(6,'(a,i5,a,d16.8)') ' nsub=',nsub,' r_ref=',rmt0
          do l=1,lmax+1
           write(6,1002)  l-1,cone/trefHS(l),trefHS(l)
          end do
        end if
        trefinv(1:kkrsz,nsub) = trefHS(ndx(1:kkrsz))
        trefHS(1:lmax+1) = cone/trefHS(1:lmax+1)
        tref(1:kkrsz,nsub) = trefHS(ndx(1:kkrsz))
       end do  
!=======================================================================
      case default
       write(6,*) ' Parameter ICALCREF must be 0 or 1'
       write(6,'(''   icalcref = '',i3)') icalcref
       call fstop('ERROR in calcTref')
!------------------------------------------------------------------------
      end select
!------------------------------------------------------------------------
      return
      end subroutine calcTref

!!IROUTINE: clstrRegEst
!!INTERFACE:
      subroutine clstrRegEst(rslatt,Rcut,nn_size,klat)
!!DESCRIPTION:
! estimates RS cluster size {\tt nn_size} and respective 
! ``cluster'' vectors {\tt klat}
!
!!DO_NOT_PRINT
      implicit none
!!DO_NOT_PRINT

!!ARGUMENTS:
      real(8), intent(in) :: rslatt(3,3)
      real(8), intent(in) :: Rcut
      integer, intent(out) :: nn_size
      integer, intent(out) :: klat(3)
!!REVISION HISTORY:
! Adapted - A.S. - 2017
!EOP
!
!BOC
      real(8) :: nn,d_nn
      integer :: ic
      character(*), parameter :: sname='clstrRegEst'
      nn = one
      do ic=1,3
        d_nn = min_d_nn(ic,rslatt)
        if ( d_nn <= 0 ) then
          write(*,*) ' ERROR in clstrRegEst, d_nn=',d_nn
          write(*,*) '       rslatt:'
          write(*,*) rslatt(:,1)
          write(*,*) rslatt(:,2)
          write(*,*) rslatt(:,3)
          call fstop(sname)
        end if
        klat(ic) = 1+(Rcut)/d_nn
        nn = (1+(2*Rcut)/d_nn)*nn
      end do
      nn_size = nn
!      nn_size = 1 + nn*natom
      return
!EOC
      end subroutine clstrRegEst
!BOP
!!IROUTINE: min_d_nn
!!INTERFACE:
      function min_d_nn ( k, a )
!!DESCRIPTION:
! returns min NN distance related to a specific lattice vector {\tt a(:,k) (k=1,2,3)}
!

!!DO_NOT_PRINT
      implicit none
      real(8) :: min_d_nn
!!DO_NOT_PRINT

!!ARGUMENTS:
      integer, intent(in) :: k  !  1 , 2 or 3
      real(8), intent(in) :: a(3,3)
!!REVISION HISTORY:
! Initial Version - A.S. - 2013
!EOP
!
!BOC
      real(8) :: v(3),b1(3),b2(3)
      integer i,j,k1,k2
      min_d_nn = -one
      if ( k >= 1 .and. k<=3 ) then
        v = a(:,k)
        k1 = 1
        k2 = 2
        if ( k == k1 ) then
            k1 = 2
            k2 = 3
        else if ( k == k2 ) then
            k1 = 3
            k2 = 1
        end if    
        b1 = a(:,k1)
        b2 = a(:,k2)
        min_d_nn = sum(v*v+b1*b1+b2*b2)
        do i=-1,1
         do j=-1,1
           min_d_nn = min(min_d_nn,sum((v + i*b1 + j*b2)**2))
         end do
        end do
        min_d_nn = sqrt(min_d_nn)
      end if
      return
!EOC
      end function min_d_nn 
      
!BOP
!!IROUTINE: maxclstrsize
!!INTERFACE:
      subroutine clstrsize(ns,natom,iorig,mapstr,                       &
     &                                   aij,Rcut,rslatt,nlatt,numnb)
!!DESCRIPTION:
! finds cluster size
!

!!DO_NOT_PRINT
      implicit none
!!DO_NOT_PRINT

!!ARGUMENTS:
      integer, intent(in) ::  ns,natom
      integer, intent(in) ::  mapstr(:,:),iorig(:)
      real(8), intent(in) ::  aij(3,*),Rcut,rslatt(3,3)
      integer, intent(out) :: nlatt(3)   ! lattice size for cluster construction
      integer, intent(out) :: numnb(ns)  ! cluster size for each sublattice
!!REVISION HISTORY:
! Adapted - A.S. - 2019
!EOP
!
!BOC
      character(*), parameter :: sname='clstrsize'
      real(8), parameter :: epsrs=1.d-14
      real(8) :: tmpv(3),tmpv1(3),tmpv2(3),tmpv3(3)
      integer :: i1,i2,i3,ia,iatom,isub,j
      real(8) :: d_nn,r2cut,r2ijnn
      integer :: nnsum

      do j=1,3
        d_nn = min_d_nn(j,rslatt)
        if ( d_nn <= 0 ) then
          write(*,*) ' ERROR in maxclstrsize, d_nn=',d_nn
          write(*,*) '       rslatt:'
          write(*,*) rslatt(:,1)
          write(*,*) rslatt(:,2)
          write(*,*) rslatt(:,3)
          call fstop(sname)
        end if
        nlatt(j) = ceiling((Rcut+d_nn)/d_nn)
      end do

      r2cut = Rcut**2

      do isub=1,ns
       iatom = iorig(isub)
       nnsum = 1
       do j=1,natom
        ia = mapstr(iatom,j)
        if ( ia<=0 ) cycle
        tmpv(1:3) = aij(1:3,ia)
        do i1=-nlatt(1),nlatt(1)
         tmpv1(1:3) = tmpv(1:3) + i1*rslatt(1:3,1)
         do i2=-nlatt(2),nlatt(2)
          tmpv2(1:3) = tmpv1(1:3) + i2*rslatt(1:3,2)
          do i3=-nlatt(3),nlatt(3)
           tmpv3(1:3) = tmpv2(1:3) + i3*rslatt(1:3,3)
           r2ijnn = dot_product(tmpv3,tmpv3)
           if(r2ijnn.le.r2cut.and.r2ijnn.gt.epsrs) then
            nnsum = nnsum + 1
           end if
          enddo
         enddo
        enddo
       end do
       numnb(isub) = nnsum 
      end do

      return
!EOC
      end subroutine clstrsize

      end module screening
