!BOP
!!MODULE: stiffode
!!INTERFACE:
      module stiffode
!!DESCRIPTION:
!  several (mostly stiff) ODE solvers,
!  real and complex versions
!

!!DO_NOT_PRINT
      implicit none
      private
!!DO_NOT_PRINT

!!PUBLIC MEMBER FUNCTIONS:
      public odeint2,set_ode_params
      public odeint1,odeint3,odeint2spl
!!PRIVATE MEMBER FUNCTIONS:
! subroutine zinterp
! subroutine zinterp1
! subroutine zinterp2
! subroutine zsplinterp
! subroutine stiff2spl
! subroutine stiff2
! subroutine stiff1
! subroutine derivs1
! subroutine derivs2
! subroutine jacob1
! subroutine jacob2
! subroutine jacob2spl
! subroutine dp_rhs
! subroutine dp_jcbn
!!REVISION HISTORY:
! Adapted - A.S. - 2013
!EOP
!
!BOC
      integer, parameter :: maxtry=40
      real*8 xstif(maxtry)
      complex*16 ystif(2,maxtry)
      complex*16 estif(2,maxtry)
      real(8) :: ystif_r(2,maxtry)
      real(8) :: estif_r(2,maxtry)
      integer ntry
      integer, protected ::  in_out
      real(8), protected ::  power
      real(8), protected ::  cent,sp_orb
      real(8), protected ::  c2inv
      complex(8), protected :: enrg
      real(8), protected :: enrg_r
!
      integer, protected :: ipot(2)
      real(8), protected :: rfit(2)
      real(8), protected :: zfit(2)
      real(8), protected :: cspl(2)
      real(8) :: dzdx=0,z0=0
      real(8), external :: poly
      integer :: nfun
      integer, parameter :: maxnfun=8
      real(8), protected :: cfun(0:maxnfun),cdfun(0:size(cfun)-1)

! lsode
      integer, parameter :: nonstiff_lsode = 10      ! Nonstiff (Adams) method, no Jacobian used.
      integer, parameter :: u_jcbn_stiff_lsode = 21  ! Stiff (BDF) method, user-supplied full Jacobian.
      integer, parameter :: i_jcbn_stiff_lsode = 22  ! Stiff method, internally generated full Jacobian
      integer :: stiff_lsode = u_jcbn_stiff_lsode

!EOC
      contains

!BOP
!!IROUTINE: set_ode_params
!!INTERFACE:
      subroutine set_ode_params(c2inv0,cent0,sp_orb0,enrg0,in_out0,     &
     &                                                 enrg_r0,cf0,     &
     &                                   ipot0,power0,rfit0,zfit0,cspl0)
!!DESCRIPTION:
! sets up ODE solvers parameters
!

!!DO_NOT_PRINT
      implicit none
!!DO_NOT_PRINT

!!ARGUMENTS:
      integer, intent(in), optional :: in_out0
      integer, intent(in), optional :: ipot0(2)
      real(8), intent(in), optional :: c2inv0  ! 1/c^2
      real(8), intent(in), optional :: cent0   ! l*(l+1)
      real(8), intent(in), optional :: sp_orb0  ! spin-orbit factor
      real(8), intent(in), optional :: power0
      real(8), intent(in), optional :: cf0(0:)
      real(8), intent(in), optional :: rfit0(2)
      real(8), intent(in), optional :: zfit0(2)
      complex(8), intent(in), optional :: enrg0
      real(8), intent(in), optional :: enrg_r0
      real(8), intent(in), optional :: cspl0(2)
!!REVISION HISTORY:
! Initial Version - A.S. - 2013
! A.S.-2016: added polynomial coefficients cf0(:)
!EOP
!
!BOC
      logical :: arg_present
      integer :: i
      real(8) :: x1,x2

      arg_present = .false.
      if ( present(in_out0) ) then
        in_out=in_out0
        arg_present = .true.
      end if
      if ( present(ipot0) ) then
        ipot = ipot0
        arg_present = .true.
      end if
      if ( present(c2inv0) ) then
        c2inv = c2inv0
        arg_present = .true.
      end if
      if ( present(cent0) ) then
        cent = cent0
        arg_present = .true.
      end if
      if ( present(sp_orb0) ) then
        sp_orb = sp_orb0
        arg_present = .true.
      end if
      if ( present(power0) ) then
        power = power0
        arg_present = .true.
      end if
      if ( present(rfit0) .or. present(zfit0) ) then
       arg_present = .true.
       if ( present(rfit0) .and. present(zfit0) ) then
        rfit = rfit0
        zfit = zfit0
        if ( rfit(1)==rfit(2) ) then
         dzdx = 0
         z0 = sum(zfit)/size(zfit)
        else
         if ( minval(rfit) >= 0 ) then
          x1 = rfit(1)
          x2 = rfit(2)
          dzdx = (zfit(2)-zfit(1))/(x2-x1)
          z0 = (zfit(1)*x2-zfit(2)*x1)/(x2-x1)
!          if( zfit(2) > 0.d0 .and. zfit(2)<zfit(1) ) then
!           dzdx_log = log(zfit(2)/zfit(1)) / (x2-x1)
!          else
!           dzdx_log = 0
!          endif
         end if
        end if
        nfun = 1
        cfun(0) = z0
        cfun(1) = dzdx
        cdfun(0)= dzdx
       end if
      end if
      if ( present(cspl0) ) then
       cspl = cspl0
       arg_present = .true.
      end if
      if ( present(cf0) ) then
       nfun = min(maxnfun,size(cf0)-1)
       cfun(0:nfun) = cf0(0:nfun)
       do i=1,nfun
        cdfun(i-1) = i*cf0(i)
       end do
      end if
      if ( present(enrg0) ) then
        enrg = enrg0
        arg_present = .true.
      end if
      if ( present(enrg_r0) ) then
        enrg_r = enrg_r0
        arg_present = .true.
      end if
      if ( .not. arg_present ) then
       in_out= 0
       ipot  = 0
       c2inv = 0
       cent  = 0
       power = 0
       sp_orb= 0
       rfit  = 0
       zfit  = 0
       cspl  = 0
       dzdx = 0
       z0    = 0
       enrg_r= 0
       enrg  = 0
      end if
      return
!EOC
      end subroutine set_ode_params
!
!BOP
!!IROUTINE: zinterp
!!INTERFACE:
      subroutine zinterp(x,z,dz)
!!DESCRIPTION:
! {\bv
!     returns the value of
!          z(x)
!          d/dx z(x);
!  linear interpolation coefficients should be predefined in the module
! \ev}

!!DO_NOT_PRINT
      implicit none
!!DO_NOT_PRINT

!!ARGUMENTS:
      real(8), intent(in) :: x
      real(8), intent(out) :: z
      real(8), intent(out) :: dz
!!REVISION HISTORY:
! Initial Version - A.S. - 2013
!EOP
!
!BOC

!c     ------------------------------------------------------------------

      z = dzdx*x + z0
      dz = dzdx
!      if ( (dzdx==0 .and. z0==0) .or. x==0 ) then
!       write(6,'(''Warning:  incorrect use of zinterp? x,z0,dzdx: '',   &
!     &                                             3e14.5)') x,z0,dzdx
!      end if
      return
!EOC
      end subroutine zinterp

!BOP
!!IROUTINE: zinterp1
!!INTERFACE:
      subroutine zinterp1(x,z,dz)
!!DESCRIPTION:
! {\bv
!     returns the value of
!          z(x)
!          d/dx z(x);
!     x should be between predefined rfit(1) and rfit(2);
!     we assume z(x) is monotonically changing,
!     when possible we assume a log fit of z(x)
! \ev}

!!DO_NOT_PRINT
      implicit real*8 (a-h,o-z)
!!DO_NOT_PRINT

!!ARGUMENTS:
      real(8), intent(in) :: x
      real(8), intent(out) :: z
      real(8), intent(out) :: dz
!!REVISION HISTORY:
! Adapted - A.S. - 2013
!!REMARKS:
! METHOD DOES NOT ASSUME CONTINUITY OF SLOPE BETWEEN
! ADJACENT GRID INTERVALS
!EOP
!
!BOC

!      integer ipot
!      real*8 rfit
!      real*8 zfit
!      common/zfit_com/rfit(2),zfit(2),ipot(2)
      real(8) :: dx,dr,p,s

!c     ------------------------------------------------------------------

      z=0.0d0
      dz=0.0d0
      dx=x-rfit(1)
      dr=rfit(2)-rfit(1)
      p=0.0d0
      if(zfit(1).gt.0.d0) then
        p=zfit(2)/zfit(1)
      endif
      if(p.gt.0.0d0 .and. p.lt.1.0d0) then
        s=dlog(p)/dr
        z =zfit(1)*exp( dx*s )
        dz=zfit(1)*exp( dx*s )*s
      else
        s=( zfit(2)-zfit(1) )/dr
        z =zfit(1)+dx*s
        dz=s
      endif
      return
!EOC
      end subroutine zinterp1

!BOP
!!IROUTINE: zinterp2
!!INTERFACE:
      subroutine zinterp2(x,z,dz)
!!DESCRIPTION:
! {\bv
!     returns the value of
!          z(x)
!          d/dx z(x);
!     polynomial coefficients should be predefined in the module
! \ev}

!!DO_NOT_PRINT
      implicit none
!!DO_NOT_PRINT

!!ARGUMENTS:
      real(8), intent(in) :: x
      real(8), intent(out) :: z
      real(8), intent(out) :: dz
!!REVISION HISTORY:
! Initial Version - A.S. - 2016
!EOP
!
!BOC

!c     ------------------------------------------------------------------

      z = poly(x,nfun,cfun)
      dz = poly(x,nfun-1,cdfun)
      return
!EOC
      end subroutine zinterp2
!
!BOP
!!IROUTINE: zsplinterp
!!INTERFACE:
      subroutine zsplinterp(x,z,dz)
!!DESCRIPTION:
! {\bv
!     returns the value of
!          z(x)
!          d/dx z(x);
!     spline coefficients should be predefined in the module
! \ev}

!!DO_NOT_PRINT
      implicit none
!!DO_NOT_PRINT

!!ARGUMENTS:
      real(8), intent(in) :: x
      real(8), intent(out) :: z
      real(8), intent(out) :: dz
!!REVISION HISTORY:
! Initial Version - A.S. - 2016
!EOP
!
!BOC
      real(8), parameter :: otosix=dble(1)/dble(6)
      real(8) :: diff,h,b,p

!c     ------------------------------------------------------------------

      diff = x-rfit(1)
      h = rfit(2)-rfit(1)
      b = ((zfit(2)-zfit(1))/h)-(h*(cspl(2)+2.0d0*cspl(1)))*otosix
      p = 0.5d0*cspl(1)+(diff/h)*(cspl(2)-cspl(1))*otosix
      z = zfit(1)+diff*(b+diff*p)
      dz = b + 0.5d0*diff*(2.d0*cspl(1)+(diff/h)*(cspl(2)-cspl(1))) 

      return
!EOC
      end subroutine zsplinterp
!
!BOP
!!IROUTINE: odeint2
!!INTERFACE:
      subroutine odeint2(x1,y1,x2,y2,ierr,                              &
     &                   eps,hmin,                                      &
     &                   maxss,xss,yss,dyss,rvss,errss,nss)
!!DESCRIPTION:
!     this is an adaptation of the numerical recipies routine odeint
!     for a system of two coupled differential equations
!     (with linear interpolation)
!
! {\bv
!     input:
!        x1       ==> initial ordinate
!        y1(2)    ==> initial value
!        x2       ==> final ordinate
!
!     input from common:
!        eps      ==> desired accuracy
!        hmin     ==> sub step size crash minimum
!        maxss    ==> allocation for sub step arrays
!
!        nss should be set to 0 before first call to odeint2
!
!     output:
!        y2(2)    <== value of coordinate at x2
!
!     optional output from common for cumulative calls to odeint2
!        xss       <== sub step ordinates
!        yss       <== sub step values
!        dyss      <== sub step dydx values
!        errss     <== sub step errors
!        nss       <== cumulative total substeps
! \ev}

!!DO_NOT_PRINT
      implicit none
!!DO_NOT_PRINT

!!ARGUMENTS:
      real*8 x1
      real*8 x2
      complex*16 y1(2)
      complex*16 y2(2)          ! out
      integer ierr
      real*8 eps
      real*8 hmin
      integer ::  maxss
      real(8), optional :: xss(maxss)
      complex(8), optional, intent(out) :: yss(2,maxss)   ! out
      complex(8), optional, intent(out) :: dyss(2,maxss)  ! out
      real(8), optional :: rvss(maxss)
      complex(8), optional, intent(out) :: errss(2,maxss) ! out
      integer, optional, intent(out) ::  nss               ! out
!!REVISION HISTORY:
! Initial Version - B.W. - 2011
! Adapted - A.S. - 2013
!EOP
!
!BOC
!
      logical :: accumulate
      real*8 htry
      real*8 hdid
      real*8 hnext

      real*8 x
      complex*16 y(2)
      complex*16 dydx(2)
      complex*16 err(2)
      complex*16 yscal(2)

      integer nstp
      integer i
      integer ibomb
      real*8  xtry
      real*8  z
      real*8  dz

!      real*8 tiny
!      parameter (tiny=1.d-30)
      integer maxstp
      parameter (maxstp=500)
!c     ==================================================================

      ierr=0
      ibomb=0
      hdid=0.0d0  !arbitrary intitialization;
      hnext=0.0d0 !proper hdid, hnext values are returned by stiff2

      htry=x2-x1

      x=x1
      y(1)=y1(1)
      y(2)=y1(2)
      call derivs2(x,y,dydx)

      accumulate = present(nss)

      if ( accumulate ) then
       nss=nss+1
       xss(nss)=x
       do i=1,2
        yss(i,nss)=y(1)
        dyss(i,nss)=dydx(1)
        errss(i,nss)=dcmplx(0.0d0,0.0d0)
       enddo
      end if


!c     take at most maxstp sub-steps
      do 20 nstp=1,maxstp

        yscal(1:2)=abs(y(1:2))+abs(htry*dydx(1:2))

        xtry=x+htry
        if((xtry-x2)*(xtry-x1).gt.0.0d0) htry=x2-x

        call stiff2(x,y,dydx,htry,hdid,hnext,eps,err,yscal,ibomb)
        if(ibomb.ne.0) then
          write(*,*) 'error in subroutine stiff: nstp=',nstp
          ierr=-ibomb
          return
        endif
        call derivs2(x,y,dydx)
        call zinterp(x,z,dz)

        if ( accumulate ) then
         if(nss.ge.maxss)then
          write(*,*) 'exceeded storage of substep in odeint2'
          ierr = 1
          return
         else
          nss=nss+1
          xss(nss)=x
          rvss(nss)=-2.0d0*z
          do i=1,2
            yss(i,nss)=y(i)
            dyss(i,nss)=dydx(i)
            errss(i,nss)=err(i)
          enddo
         endif
        endif

        if((x-x2)*(x2-x1).ge.0.0d0)then
          do i=1,2
            y2(i)=y(i)
          enddo
          return
        endif

        if(dabs(hnext).lt.hmin) then
          write(*,*) 'stepsize smaller than minimum in odeint'
          ierr=2
          return
        endif

        htry=hnext
20    continue

!      write(*,*) 'too many steps in odeint'
      ierr=3
      return
!EOC
      end subroutine odeint2
!
!BOP
!!IROUTINE: odeint1
!!INTERFACE:
      subroutine odeint1(x1,y1,x2,y2,ierr,                              &
     &                   eps,hmin,                                      &
     &                   xss,yss,dyss,rvss,errss,nss,maxss)
!!DESCRIPTION:
!     this is an adaptation of the numberical recipies routine odeint
!     for a system of two coupled differential equations
! {\bv
!     input:
!        x1       ==> initial ordinate
!        y1(2)    ==> initial value
!        x2       ==> final ordinate
!
!     input from common:
!        eps      ==> desired accuracy
!        hmin     ==> sub step size crash minimum
!        maxss    ==> allocation for sub step arrays
!
!        nss should be set to 0 before first call to odeint1
!
!     output:
!        y2(2)    <== value of coordinate at x2
!
!     output from common for cumulative calls to odeint1
!        xss       <== sub step ordinates
!        yss       <== sub step values
!        dyss      <== sub step dydx values
!        errss     <== sub step errors
!        nss       <== cumulative total substeps
! \ev}

!!DO_NOT_PRINT
      implicit none
!!DO_NOT_PRINT

!!ARGUMENTS:
      real*8 x1
      real*8 x2
      real(8) y1(2)
      real(8) y2(2)
      integer ierr
      real*8 eps
      real*8 hmin
      integer maxss
      real*8 xss(maxss)
      real*8 rvss(maxss)
      real(8) yss(2,maxss)
      real(8) dyss(2,maxss)
      real(8) errss(2,maxss)
      integer nss
!!REVISION HISTORY:
! Initial Version - B.W. - 2011
! Adapted - A.S. - 2013
!EOP
!
!BOC
      real*8 htry
      real*8 hdid
      real*8 hnext

      real*8 x
      real(8) y(2)
      real(8) dydx(2)
      real(8) err(2)
      real(8) yscal(2)

      integer nstp
      integer i
      integer ibomb
      real*8  xtry
      real*8  z
      real*8  dz

!      real*8 tiny
!      parameter (tiny=1.d-30)
      integer maxstp
      parameter (maxstp=500)
!c     ==================================================================

      ierr=0
      ibomb=0
      hdid=0.0d0  !arbitrary intitialization;
      hnext=0.0d0 !proper hdid, hnext values are returned by stiff1

      htry=x2-x1

      x=x1
      y(1)=y1(1)
      y(2)=y1(2)
      call derivs1(x,y,dydx)

      xss(1)=x
      yss(:,1)=y(1)
      dyss(:,1)=dydx(1)
      errss(:,1)=0
      nss = 0


!c     take at most maxstp sub-steps
      do 20 nstp=1,maxstp

!        do i=1,2
!          yscal(i)=abs(y(i))+abs(htry*dydx(i))+tiny
!        enddo
        yscal(1:2)=abs(y(1:2))+abs(htry*dydx(1:2))

        xtry=x+htry
        if((xtry-x2)*(xtry-x1).gt.0.0d0) htry=x2-x

        call stiff1(x,y,dydx,htry,hdid,hnext,eps,err,yscal,ibomb)
        if(ibomb.ne.0) then
          write(*,*) 'error in subroutine stiff'
          ierr=-ibomb
          return
        endif
        call derivs1(x,y,dydx)
        call zinterp(x,z,dz)

        if(nss.ge.maxss)then
          write(*,*) 'exceeded storage of substep in odeint1'
          ierr = 1
          return
        else
          nss=nss+1
          xss(nss)=x
          rvss(nss)=z
          do i=1,2
            yss(i,nss)=y(i)
            dyss(i,nss)=dydx(i)
            errss(i,nss)=err(i)
          enddo
        endif

        if((x-x2)*(x2-x1).ge.0.0d0)then
          do i=1,2
            y2(i)=y(i)
          enddo
          return
        endif

        if(dabs(hnext).lt.hmin) then
          write(*,*) 'stepsize smaller than minimum in odeint'
          ierr=2
          return
        endif

        htry=hnext
20    continue

!      write(*,*) 'too many steps in odeint'
      ierr=3
      return
!EOC
      end subroutine odeint1
!
!BOP
!!IROUTINE: stiff2
!!INTERFACE:
      subroutine stiff2(x,y,dydx,htry,hdid,hnext,                       &
     &                  eps,err,yscal,ierr)
!!DESCRIPTION:
!     integration routine for a particular form of 2 coupled stiff
!     differential equations
!
!     this is an adaptation of the numerical recipies routine stiff
!     which uses a fourth order rosenbrock step for integrating
!     stiff ordinary differrential equations
!
! {\bv
!     input:
!       x    ==> initial coordinate
!       y    ==> inital value
!       dydx ==> initial value of derivitive
!       htry ==> trial value for the increment in x
!       eps  ==> desired accuracy
!       yscal==> component dependent accuracy criterion
!
!     output:
!       hdid <== acutual step size used fullfilling accuracy
!       hnext<== estimated next step size
!       x    <== new x
!       y    <== new value
!       err  <== component errors
!       ierr <== abnormal termination code
! \ev}

!!DO_NOT_PRINT
      implicit none
!!DO_NOT_PRINT

!!ARGUMENTS:
      real*8 x
      complex*16 y(2)
      complex*16 dydx(2)
      complex*16 err(2)
      complex*16 yscal(2)
      real*8 htry
      real*8 hdid
      real*8 hnext
      real*8 eps
      integer ierr
!!REVISION HISTORY:
! Initial Version - B.W. - 2011
! Adapted - A.S. - 2013
!EOP
!
!BOC
      logical dbug
      integer i
      real*8 h
      real*8 h2
      real*8 h25
      real*8 h108
      real*8 hm0
      real*8 gam
      real*8 gm1
      real*8 gm2
      real*8 gm3
      real*8 s
      real*8 errmax
      complex*16 d11
      complex*16 d12
      complex*16 d21
      complex*16 d22
      complex*16 odenom


!      integer maxtry
!      parameter (maxtry=40)
!      real*8 xstif
!      complex*16 ystif
!      complex*16 estif
!      integer ntry
!!c     this common block usefull for debugging
!      common /stiffcom/ xstif(maxtry),ystif(2,maxtry),                  &
!     &                  estif(2,maxtry),ntry

      real*8 xsav
      complex*16 ysav(2)
      complex*16 dysav(2)
      complex*16 dfdx(2)
      complex*16 dfdy(2,2)

      complex*16 ua(2)
      complex*16 ub(2)
      complex*16 uc(2)
      complex*16 ud(2)
      complex*16 ga(2)
      complex*16 gb(2)
      complex*16 gc(2)
      complex*16 gd(2)


      real*8 safety
      parameter (safety=0.9d0)
      real*8 grow,pgrow
      parameter (grow=1.5d0,pgrow=-.25d0)
      real*8 shrnk,pshrnk
      parameter (shrnk=0.5d0,pshrnk=-1.d0/3.d0)
      real*8 errcon
      parameter (errcon=.1296d0)

      real*8 b1,b2,b3,b4
      parameter (b1=228.0d0,b2=54.0d0,b3=25.0d0,b4=125.0d0)
      real*8 e1,e2,e4
      parameter (e1=34.0d0,e2=21.0d0,e4=125.0d0)
      real(8), parameter :: tiny_r8=tiny(e1)
!c     ==================================================================


      ierr=0

      xsav=x
      do i=1,2
        ysav(i)=y(i)
        dysav(i)=dydx(i)
      enddo

      call jacob2(xsav,ysav,dfdy,dfdx)

      h=htry
      do 25 ntry=1,maxtry

!c       perform matrix inversions analytically
!c       --------------------------------------------
!c       let s=gam*h and   f_dot=(  a  b  )
!c                               (  c  d )
!c       then {(1/s)(1-s*f_dot)}^-1 =
!c                             (    1-sd        sb  )
!c                   s/(1-sw)  (      sc      1-sa )
!c
!c       1-sw=(1-sa)(1-sd)-(sb)(sc)
!c       --------------------------------------------
        gam=0.5d0
        s=gam*h
        d11=1.0d0-s*dfdy(2,2)
        d12=      s*dfdy(1,2)
        d21=      s*dfdy(2,1)
        d22=1.0d0-s*dfdy(1,1)
        odenom=gam/(d11*d22-d12*d21)


!c       set up right hand side ga
        hm0=0.5d0*h
        do 14 i=1,2
          ua(i)=dysav(i)+hm0*dfdx(i)
14      continue
!c       matrix multiply ua by s*((1-s dfdy)^-1)
        ga(1)=( d11*ua(1) + d12*ua(2) )*odenom
        ga(2)=( d21*ua(1) + d22*ua(2) )*odenom


        h2=2.0d0*h
        do 15 i=1,2
          y(i)=ysav(i)+h2*ga(i)
15      continue
        x=xsav+h
        call derivs2(x,y,dydx)

!c       set up right hand side gb
        hm0=-1.5d0*h
        gm1=-8.0d0
        do 16 i=1,2
          ub(i)=dydx(i)+hm0*dfdx(i)+gm1*ga(i)
16      continue
!c       matrix multiply ub by s*((1-s dfdy)^-1)
        gb(1)=( d11*ub(1) + d12*ub(2) )*odenom
        gb(2)=( d21*ub(1) + d22*ub(2) )*odenom

        h25=h/25.0d0
        gm1=48.0d0
        gm2= 6.0d0
        do 17 i=1,2
          y(i)=ysav(i)+h25*( gm1*ga(i)+gm2*gb(i) )
17      continue
        x=xsav+(3.0d0*h/5.0d0)
        call derivs2(x,y,dydx)


!c       set up right hand side gc
        hm0=121.0d0*h/50.0d0
        gm1=(372.0d0/25.0d0)
        gm2=( 60.0d0/25.0d0)
        do 18 i=1,2
          uc(i)=dydx(i)+hm0*dfdx(i)+gm1*ga(i)+gm2*gb(i)
18      continue
!c       matrix multiply uc by s*((1-s dfdy)^-1)
        gc(1)=( d11*uc(1) + d12*uc(2) )*odenom
        gc(2)=( d21*uc(1) + d22*uc(2) )*odenom


!c       set up right hand side g4
        hm0=29.0d0*h/250.0d0
        gm1=(-112.0d0/125.0d0)
        gm2=(- 54.0d0/125.0d0)
        gm3=(- 50.0d0/125.0d0)
        do 19 i=1,2
          ud(i)=dydx(i)+hm0*dfdx(i)+gm1*ga(i)+gm2*gb(i)+gm3*gc(i)
19      continue
!c       matrix multiply ud by s*((1-s dfdy)^-1)
        gd(1)=( d11*ud(1) + d12*ud(2) )*odenom
        gd(2)=( d21*ud(1) + d22*ud(2) )*odenom


        h108=h/108.0d0
        do 21 i=1,2
          y(i)=ysav(i)+h108*( b1*ga(i)+b2*gb(i)+b3*gc(i)+b4*gd(i) )
          err(i)=      h108*( e1*ga(i)+e2*gb(i)         +e4*gd(i) )
21      continue

        x=xsav+h

        xstif(ntry)=x
        do 22 i=1,2
          ystif(i,ntry)=y(i)
          estif(i,ntry)=err(i)
22      continue

        if(x.eq.xsav) then
          write(*,*) 'stepsize not significant in stiff2'
          ierr= 1
          return
        endif

        errmax=abs(err(1))/(abs(yscal(1))+tiny_r8)
        errmax=max(errmax,abs(err(2))/(abs(yscal(2))+tiny_r8))
        errmax=errmax/eps

        if(errmax.le.1.0d0)then
          hdid=h
          if(errmax.gt.errcon)then
            hnext=safety*h*errmax**pgrow
          else
            hnext=grow*h
          endif
          return
        else
          hnext=safety*h*errmax**pshrnk
          if(hnext.lt.shrnk*h) hnext=shrnk*h
          h=hnext
        endif

25    continue

      write(*,*) 'exceeded maxtry=',maxtry,' in stiff2'
      write(*,*) ' x=',x
      write(*,*) ' y(1:2)=',y(1:2)
      write(*,*) ' dydx(1:2)=',dydx(1:2)
      write(*,*) ' err(1:2)=',err(1:2)
      write(*,*) ' errmax=',errmax*eps,' target=',eps
      ierr = 2
      return
!EOC
      end  subroutine stiff2
!
!BOP
!!IROUTINE: stiff1
!!INTERFACE:
      subroutine stiff1(x,y,dydx,htry,hdid,hnext,                       &
     &                  eps,err,yscal,ierr)
!!DESCRIPTION:
!     integration routine for a particular form of 2 coupled stiff
!     differential equations
!
!     this is an adaptation of the numerical recipies routine stiff
!     which uses a fourth order rosenbrock step for integrating
!     stiff ordinary differrential equations
! {\bv
!     input:
!       x    ==> initial coordinate
!       y    ==> inital value
!       dydx ==> initial value of derivitive
!       htry ==> trial value for the increment in x
!       eps  ==> desired accuracy
!       yscal==> component dependent accuracy criterion
!
!     output:
!       hdid <== acutual step size used fullfilling accuracy
!       hnext<== estimated next step size
!       x    <== new x
!       y    <== new value
!       err  <== component errors
!       ierr <== abnormal termination code
!     ==================================================================
! \ev}

!!DO_NOT_PRINT
      implicit none
!!DO_NOT_PRINT

!!ARGUMENTS:
      real*8 x
      real(8) y(2)
      real(8) dydx(2)
      real(8) err(2)
      real(8) yscal(2)
      real*8 htry
      real*8 hdid
      real*8 hnext
      real*8 eps
      integer ierr
!!REVISION HISTORY:
! Initial Version - B.W. - 2011
! Adapted - A.S. - 2013
!EOP
!
!BOC
      logical dbug
      integer i
      real*8 h
      real*8 h2
      real*8 h25
      real*8 h108
      real*8 hm0
      real*8 gam
      real*8 gm1
      real*8 gm2
      real*8 gm3
      real*8 s
      real*8 errmax
      real(8) d11
      real(8) d12
      real(8) d21
      real(8) d22
      real(8) odenom


!      integer maxtry
!      parameter (maxtry=40)
!      real*8 xstif
!      complex*16 ystif
!      complex*16 estif
!      integer ntry
!!c     this common block usefull for debugging
!      common /stiffcom/ xstif(maxtry),ystif(2,maxtry),                  &
!     &                  estif(2,maxtry),ntry

      real*8 xsav
      real(8) ysav(2)
      real(8) dysav(2)
      real(8) dfdx(2)
      real(8) dfdy(2,2)

      real(8) ua(2)
      real(8) ub(2)
      real(8) uc(2)
      real(8) ud(2)
      real(8) ga(2)
      real(8) gb(2)
      real(8) gc(2)
      real(8) gd(2)


      real*8 safety
      parameter (safety=0.9d0)
      real*8 grow,pgrow
      parameter (grow=1.5d0,pgrow=-.25d0)
      real*8 shrnk,pshrnk
      parameter (shrnk=0.5d0,pshrnk=-1.d0/3.d0)
      real*8 errcon
      parameter (errcon=.1296d0)

      real*8 b1,b2,b3,b4
      parameter (b1=228.0d0,b2=54.0d0,b3=25.0d0,b4=125.0d0)
      real*8 e1,e2,e4
      parameter (e1=34.0d0,e2=21.0d0,e4=125.0d0)
!c     ==================================================================


      ierr=0

      xsav=x
      do i=1,2
        ysav(i)=y(i)
        dysav(i)=dydx(i)
      enddo

      call jacob1(xsav,ysav,dfdx,dfdy)

      h=htry
      do 25 ntry=1,maxtry

!c       perform matrix inversions analytically
!c       --------------------------------------------
!c       let s=gam*h and   f_dot=(  a  b  )
!c                               (  c  d )
!c       then {(1/s)(1-s*f_dot)}^-1 =
!c                             (    1-sd        sb  )
!c                   s/(1-sw)  (      sc      1-sa )
!c
!c       1-sw=(1-sa)(1-sd)-(sb)(sc)
!c       --------------------------------------------
        gam=0.5d0
        s=gam*h
        d11=1.0d0-s*dfdy(2,2)
        d12=      s*dfdy(1,2)
        d21=      s*dfdy(2,1)
        d22=1.0d0-s*dfdy(1,1)
        odenom=gam/(d11*d22-d12*d21)


!c       set up right hand side ga
        hm0=0.5d0*h
        do 14 i=1,2
          ua(i)=dysav(i)+hm0*dfdx(i)
14      continue
!c       matrix multiply ua by s*((1-s dfdy)^-1)
        ga(1)=( d11*ua(1) + d12*ua(2) )*odenom
        ga(2)=( d21*ua(1) + d22*ua(2) )*odenom


        h2=2.0d0*h
        do 15 i=1,2
          y(i)=ysav(i)+h2*ga(i)
15      continue
        x=xsav+h
        call derivs1(x,y,dydx)

!c       set up right hand side gb
        hm0=-1.5d0*h
        gm1=-8.0d0
        do 16 i=1,2
          ub(i)=dydx(i)+hm0*dfdx(i)+gm1*ga(i)
16      continue
!c       matrix multiply ub by s*((1-s dfdy)^-1)
        gb(1)=( d11*ub(1) + d12*ub(2) )*odenom
        gb(2)=( d21*ub(1) + d22*ub(2) )*odenom

        h25=h/25.0d0
        gm1=48.0d0
        gm2= 6.0d0
        do 17 i=1,2
          y(i)=ysav(i)+h25*( gm1*ga(i)+gm2*gb(i) )
17      continue
        x=xsav+(3.0d0*h/5.0d0)
        call derivs1(x,y,dydx)


!c       set up right hand side gc
        hm0=121.0d0*h/50.0d0
        gm1=(372.0d0/25.0d0)
        gm2=( 60.0d0/25.0d0)
        do 18 i=1,2
          uc(i)=dydx(i)+hm0*dfdx(i)+gm1*ga(i)+gm2*gb(i)
18      continue
!c       matrix multiply uc by s*((1-s dfdy)^-1)
        gc(1)=( d11*uc(1) + d12*uc(2) )*odenom
        gc(2)=( d21*uc(1) + d22*uc(2) )*odenom


!c       set up right hand side g4
        hm0=29.0d0*h/250.0d0
        gm1=(-112.0d0/125.0d0)
        gm2=(- 54.0d0/125.0d0)
        gm3=(- 50.0d0/125.0d0)
        do 19 i=1,2
          ud(i)=dydx(i)+hm0*dfdx(i)+gm1*ga(i)+gm2*gb(i)+gm3*gc(i)
19      continue
!c       matrix multiply ud by s*((1-s dfdy)^-1)
        gd(1)=( d11*ud(1) + d12*ud(2) )*odenom
        gd(2)=( d21*ud(1) + d22*ud(2) )*odenom


        h108=h/108.0d0
        do 21 i=1,2
          y(i)=ysav(i)+h108*( b1*ga(i)+b2*gb(i)+b3*gc(i)+b4*gd(i) )
          err(i)=      h108*( e1*ga(i)+e2*gb(i)         +e4*gd(i) )
21      continue

        x=xsav+h

        xstif(ntry)=x
        do 22 i=1,2
          ystif_r(i,ntry)=y(i)
          estif_r(i,ntry)=err(i)
22      continue

        if(x.eq.xsav) then
          write(*,*) 'stepsize not significant in stiff1'
          ierr= 1
          return
        endif

        errmax=0.0d0
        do 23 i=1,2
          errmax=dmax1(errmax,abs(err(i)/(yscal(i)+tiny(errmax))))
23      continue
        errmax=errmax/eps

        if(errmax.le.1.0d0)then
          hdid=h
          if(errmax.gt.errcon)then
            hnext=safety*h*errmax**pgrow
          else
            hnext=grow*h
          endif
          return
        else
          hnext=safety*h*errmax**pshrnk
          if(hnext.lt.shrnk*h) hnext=shrnk*h
          h=hnext
        endif

25    continue

      write(*,*) 'exceeded maxtry in stiff1'
      ierr = 2
      return
!EOC
      end  subroutine stiff1
!
!BOP
!!IROUTINE: derivs1
!!INTERFACE:
      subroutine derivs1(x,y,dydx)
!!DESCRIPTION:
!     returns the right hand side of the system of equations
! {\bv
!     d/dr gbar = {1-io*pow}/r*gbar + {m         }*fbar
!     d/dr fbar=  {w         }*gbar - {1+io*pow}/r*fbar
!
!     where
!       m = 1+{e-v(r)}/c^2    v(r) = -2*z(r)/r
!       w = { l*(l+1)/(m*r^2) } - { e-v(r) }
!
! in_out=+1 for outward propagation of gbar:  g=r^pow gbar   f=r^pow fbar
!       =-1 for inward  propagation of gbar:  g=1/r^pow gbar f=1/r^pow fbar
!       = 0 for inward or outward propagation of gbar==g  fbar==f
!
!     input:
!       x    ==> r   not necessarily on tabulated grid
!       y(1) ==> gbar
!       y(2) ==> fbar
!
!     output:
!       dydx(:) <== d/dx y(:)
! \ev}

!!DO_NOT_PRINT
      implicit none
!!DO_NOT_PRINT

!!ARGUMENTS:
      real*8 x
      real(8) y(2)
      real(8) dydx(2)
!!REVISION HISTORY:
! Initial Version - B.W. - 2011
! Adapted - A.S. - 2013
!EOP
!
!BOC
!      integer in_out
!      real*8  power
!      real*8  cent
!      real*8  c2inv
!      complex*16 enrg
!      common/scalrelcom1/c2inv,cent,power,enrg
!      common/scalrelcom2/in_out

      real*8 xinv
      real*8 b1
      real*8 b2
      real*8 z
      real*8 dz
      real*8 v
      real(8) emv
      real(8) m
      real(8) :: rvprim
      real(8) centrif
      real(8) w
!c     ==================================================================


      xinv = 1.0d0/x
      b1=(1.0d0-in_out*power)*xinv
      b2=(1.0d0+in_out*power)*xinv

!c     zinterp is the continuous interpolation of z(i)
      call zinterp(x,z,dz)
      v=-2.0d0*(z*xinv)

      emv= enrg_r-v
      m= 1.0d0+ emv*c2inv           ! 2*M
      centrif=(cent*xinv*xinv)/m
      if ( sp_orb .ne. 0.d0 ) then  !  spin-orbit 
       rvprim  = -2.d0*(z*xinv-dz)  ! r*v'
       centrif = centrif * ( 1.d0 - c2inv*sp_orb*rvprim/m )
      end if
      w= centrif - emv


      dydx(1)= b1*y(1) +  m*y(2)
      dydx(2)=  w*y(1) - b2*y(2)

      return
!EOC
      end  subroutine derivs1
!
!BOP
!!IROUTINE: derivs2
!!INTERFACE:
      subroutine derivs2(x,y,dydx)
!!DESCRIPTION:
!     returns the right hand side of the system of equations
! {\bv
!     d/dr gbar = {1-io*pow}/r*gbar + {m         }*fbar
!     d/dr fbar=  {w         }*gbar - {1+io*pow}/r*fbar
!
!     where
!       m = 1+{e-v(r)}/c^2    v(r) = -2*z(r)/r
!       w = { l*(l+1)/(m*r^2) } - { e-v(r) }
!
! in_out=+1 for outward propagation of gbar:  g=r^pow gbar   f=r^pow fbar
!       =-1 for inward  propagation of gbar:  g=1/r^pow gbar f=1/r^pow fbar
!       = 0 for inward or outward propagation of gbar==g  fbar==f
!
!     input:
!       x    ==> r   not necessarily on tabulated grid
!       y(1) ==> gbar
!       y(2) ==> fbar
!
!     output:
!       dydx(:) <== d/dx y(:)
! \ev}

!!DO_NOT_PRINT
      implicit none
!!DO_NOT_PRINT

!!ARGUMENTS:
      real(8), intent(in) :: x
      complex(8), intent(in) :: y(2)
      complex(8), intent(out) :: dydx(2)
!!REVISION HISTORY:
! Initial Version - B.W. - 2011
! Adapted - A.S. - 2013
!EOP
!
!BOC
      complex(8) :: dfdy(2,2)

      call jacob2(x,y,dfdy)
      dydx(1) = dfdy(1,1)*y(1) + dfdy(1,2)*y(2)
      dydx(2) = dfdy(2,1)*y(1) + dfdy(2,2)*y(2)

!      integer in_out
!      real*8  power
!      real*8  cent
!      real*8  c2inv
!      complex*16 enrg
!      common/scalrelcom1/c2inv,cent,power,enrg
!      common/scalrelcom2/in_out

!      real*8 xinv
!      real*8 b1
!      real*8 b2
!      real*8 z
!      real*8 dz
!      real*8 v
!      complex*16 emv
!      complex*16 m
!      complex*16 centrif
!      complex*16 w
!c     ==================================================================
!
!      xinv = 1.0d0/x
!      b2 = in_out*power*xinv
!      b1 = xinv - b2
!      b2 = xinv + b2
!!      b1=(1.0d0-in_out*power)*xinv
!!      b2=(1.0d0+in_out*power)*xinv
!
!!c     zinterp is the continuous interpolation of z(i)
!      call zinterp(x,z,dz)
!      v=-2.0d0*z*xinv
!
!      emv= enrg-v
!      m= 1.0d0+ emv*c2inv
!      centrif=(cent*xinv*xinv)/m
!      w= centrif - emv
!
!      dydx(1)= b1*y(1) +  m*y(2)
!      dydx(2)=  w*y(1) - b2*y(2)

      return
!EOC
      end  subroutine derivs2
!
!BOP
!!IROUTINE: jacob1
!!INTERFACE:
      subroutine jacob1(x,y,dfdx,dfdy)
!!DESCRIPTION:
!     constructs the jacobian of the system of equations
! {\bv
!     d/dr gbar = {1-io*pow}/r*gbar + {m         }*fbar
!     d/dr fbar=  {w         }*gbar - {1+io*pow}/r*fbar
!
!     where
!       m = 1+{e-v(r)}/c^2    v(r) = -2*z(r)/r
!       w = { l*(l+1)/(m*r^2) } - { e-v(r) }
!
! in_out=+1 for outward propagation of gbar:  g=r^pow gbar   f=r^pow fbar
!       =-1 for inward  propagation of gbar:  g=1/r^pow gbar f=1/r^pow fbar
!       = 0 for inward or outward propagation of gbar==g  fbar==f
!
!     input:
!       x    ==> r   not necessarily on tabulated grid
!       y(1) ==> gbar
!       y(2) ==> fbar
!
!     output:
!       dfdy(:)
!       dfdx(:)
! \ev}

!!DO_NOT_PRINT
      implicit none
!!DO_NOT_PRINT

!!ARGUMENTS:
      real*8 x
      real(8) y(2)
      real(8) dfdy(2,2)
      real(8) dfdx(2)
!!REVISION HISTORY:
! Initial Version - B.W. - 2011
! Adapted - A.S. - 2013
!EOP
!
!BOC
!      integer in_out
!      real*8  power
!      real*8  cent
!      real*8  c2inv
!      complex*16 enrg
!      common/scalrelcom1/c2inv,cent,power,enrg
!      common/scalrelcom2/in_out

      real*8 xinv
      real*8 b1
      real*8 b2
      real*8 db1
      real*8 db2
      real*8 z
      real*8 dz
      real*8 v
      real*8 dv
      real(8) emv
      real(8) m
      real(8) dm
      real(8) centrif
      real(8) :: rvprim
      real(8) w
      real(8) dw
!c     ==================================================================

      xinv = 1.0d0/x
      b1=(1.0d0-in_out*power)*xinv
      b2=(1.0d0+in_out*power)*xinv

      db1=-b1*xinv
      db2=-b2*xinv

!c     zinterp is the continuous interpolation of z(i)
      call zinterp(x,z,dz)
      v=-2.0d0*z*xinv

      dv=-2.0d0*( dz*x-z )*xinv*xinv

      emv= enrg_r-v
      m= 1.0d0+ emv*c2inv           ! 2*M
      centrif=(cent*xinv*xinv)/m
      if ( sp_orb .ne. 0.d0 ) then  !  spin-orbit 
       rvprim  = -2.d0*(z*xinv-dz)  ! r*v'
       centrif = centrif * ( 1.d0 - c2inv*sp_orb*rvprim/m )
      end if
      w= centrif - emv

      dm= -dv*c2inv
      dw= -( (dm/m)+2.0d0*xinv )*centrif +dv

      dfdy(1,1)= b1
      dfdy(1,2)= m
      dfdy(2,1)= w
      dfdy(2,2)=-b2

      dfdx(1)=db1*y(1) +  dm*y(2)
      dfdx(2)= dw*y(1) - db2*y(2)

      return
!EOC
      end subroutine jacob1
!
!BOP
!!IROUTINE: jacob2
!!INTERFACE:
      subroutine jacob2(x,y,dfdy,dfdx)
!!DESCRIPTION:
!     constructs the jacobian of the system of equations
! {\bv
!     d/dr gbar = {1-io*pow}/r*gbar + {m         }*fbar
!     d/dr fbar=  {w         }*gbar - {1+io*pow}/r*fbar
!
!     where
!       m = 1+{e-v(r)}/c^2    v(r) = -2*z(r)/r
!       w = { l*(l+1)/(m*r^2) } - { e-v(r) }
!
! in_out=+1 for outward propagation of gbar:  g=r^pow gbar   f=r^pow fbar
!       =-1 for inward  propagation of gbar:  g=1/r^pow gbar f=1/r^pow fbar
!       = 0 for inward or outward propagation of gbar==g  fbar==f
!
!     input:
!       x    ==> r   not necessarily on tabulated grid
!       y(1) ==> gbar
!       y(2) ==> fbar
!
!     output:
!       dfdy(:)
!       dfdx(:)
! \ev}

!!DO_NOT_PRINT
      implicit none
!!DO_NOT_PRINT

!!ARGUMENTS:
      real(8), intent(in) :: x
      complex(8), intent(in) :: y(2)
      complex(8), intent(out) :: dfdy(2,2)
      complex(8), intent(out), optional :: dfdx(2)
!!REVISION HISTORY:
! Initial Version - B.W. - 2011
! Adapted - A.S. - 2013
!EOP
!
!BOC

!      integer in_out
!      real*8  power
!      real*8  cent
!      real*8  c2inv
!      complex*16 enrg
!      common/scalrelcom1/c2inv,cent,power,enrg
!      common/scalrelcom2/in_out

      real*8 xinv
      real*8 b1
      real*8 b2
      real*8 db1
      real*8 db2
      real*8 z
      real*8 dz
      real*8 v
      real*8 dv
      real(8) :: rvprim
      complex*16 emv
      complex*16 m
      complex*16 dm
      complex*16 centrif
      complex*16 w
      complex*16 dw
!c     ==================================================================

      xinv = 1.0d0/x
      b2 = in_out*power*xinv
      b1 = xinv - b2
      b2 = xinv + b2
!      b1=(1.0d0-in_out*power)*xinv
!      b2=(1.0d0+in_out*power)*xinv


!c     zinterp is the continuous interpolation of z(i)
      call zinterp(x,z,dz)
      v=-2.0d0*z*xinv

      emv= enrg-v
      m= 1.0d0+ emv*c2inv           ! 2*M
      centrif=(cent*(xinv*xinv))/m
      if ( sp_orb .ne. 0.d0 ) then  !  spin-orbit 
       rvprim  = -2.d0*(z*xinv-dz)  ! r*v'
       centrif = centrif * ( 1.d0 - c2inv*sp_orb*rvprim/m )
      end if
      w= centrif - emv

      dfdy(1,1)= b1
      dfdy(1,2)= m
      dfdy(2,1)= w
      dfdy(2,2)=-b2

      if ( present(dfdx) ) then
       db1=-b1*xinv
       db2=-b2*xinv
       dv=-2.0d0*( dz*x-z )*(xinv*xinv)
       dm= -dv*c2inv
       dw= -( (dm/m)+2.0d0*xinv )*centrif +dv
       dfdx(1)=db1*y(1) +  dm*y(2)
       dfdx(2)= dw*y(1) - db2*y(2)
      end if

      return
!EOC
      end subroutine jacob2
!
!
!BOP
!!IROUTINE: dp_rhs
!!INTERFACE:
      subroutine dp_rhs(neq,x,z,dzdx)
!!DESCRIPTION:
! constructs right-hand side
!

!!DO_NOT_PRINT
      implicit none
!!DO_NOT_PRINT

!!ARGUMENTS:
      integer, intent(in) :: neq
      double precision, intent(in) :: x,z(neq)
      double precision, intent(out) :: dzdx(neq)
!!REVISION HISTORY:
! Initial Version - A.S. - 2013
!EOP
!
!BOC
      complex(8) :: y(2),dydx(2)

      if ( neq == 4 ) then
        y(1) = dcmplx(z(1),z(2))
        y(2) = dcmplx(z(3),z(4))
        call derivs2(x,y,dydx)
        dzdx(1) = dreal(dydx(1))
        dzdx(2) = aimag(dydx(1))
        dzdx(3) = dreal(dydx(2))
        dzdx(4) = aimag(dydx(2))
      else
        stop 'ERROR: neq .NE. 4 in DP_RHS'
      end if
      return
!EOC
      end subroutine dp_rhs
!
!C     JAC   :EXT    Name of subroutine for Jacobian matrix (MF =
!C                   21 or 24).  If used, this name must be declared
!C                   EXTERNAL in calling program.  If not used, pass a
!C                   dummy name.  The form of JAC must be:
!C
!C                   SUBROUTINE JAC (NEQ, T, Y, ML, MU, PD, NROWPD)
!C                   INTEGER  NEQ, ML, MU, NROWPD
!C                   DOUBLE PRECISION  T, Y(*), PD(NROWPD,*)
!BOP
!!IROUTINE: dp_jcbn
!!INTERFACE:
      subroutine dp_jcbn(neq,x,z,ml,mu,pd,nrowpd)
!!DESCRIPTION:
! constructs Jacobian matrix
!

!!DO_NOT_PRINT
      implicit none
!!DO_NOT_PRINT

!!ARGUMENTS:
      integer, intent(in) :: neq
      integer, intent(in) :: ml,mu,nrowpd
      double precision, intent(in) :: x,z(neq)
!                                    ! PD(i,j) with d(rhs)(i)/dz(j) 
      double precision, intent(out) :: pd(nrowpd,neq)
!!REVISION HISTORY:
! Initial Version - A.S. - 2013
!EOP
!
!BOC
      complex(8) :: y(2),dfdy(2,2)
      if ( neq==4 ) then
        y(1) = dcmplx(z(1),z(2))
        y(2) = dcmplx(z(3),z(4))
        call jacob2(x,y,dfdy)
        pd(1,1) = dreal(dfdy(1,1))
        pd(2,1) = aimag(dfdy(1,1))
        pd(3,1) = dreal(dfdy(2,1))
        pd(4,1) = aimag(dfdy(2,1))
        pd(1,2) = dreal(dfdy(1,2))
        pd(2,2) = aimag(dfdy(1,2))
        pd(3,2) = dreal(dfdy(2,2))
        pd(4,2) = aimag(dfdy(2,2))
      else
       stop 'ERROR: neq .NE. 4 in DP_JCBN'
      end if
      return
!EOC
      end subroutine dp_jcbn
!
!BOP
!!IROUTINE: odeint3
!!INTERFACE:
      subroutine odeint3(x1,y1,x2,y2,ierr,                              &
     &                   eps,hmin,                                      &
     &                   maxss,xss,yss,dyss,rvss,errss,nss)
!!DESCRIPTION:
!  lsode solver
! {\bv
!     input:
!        x1       ==> initial ordinate
!        y1(2)    ==> initial value
!        x2       ==> final ordinate
!
!        if input ierr is
!                    1   This is the first call for a problem.
!            otherwise   This is a subsequent call.
!
!        if output ierr is
!                    0 --> normal (ready to continue)
!             otherwise --> something is wrong
!
!        eps      ==> desired accuracy (rel.tolerance)
!
!     output:
!        y2(2)    <== value at coordinate x2
!
!NOT USED:
!        hmin     ==> sub step size crash minimum
!        maxss    ==> allocation for sub step arrays
!
!        nss should be set to 0 before first call to odeint3
!
!     output from common for cumulative calls to odeint3
!        xss       <== sub step ordinates
!        yss       <== sub step values
!        dyss      <== sub step dydx values
!        errss     <== sub step errors
!        nss       <== cumulative total substeps
! \ev}

!!USES:
      use lsode, only : DLSODE

!!DO_NOT_PRINT
      implicit none
!!DO_NOT_PRINT

!!ARGUMENTS:
      real(8), intent(in) :: x1,x2
      complex(8), intent(in) :: y1(2)
      complex(8), intent(out) :: y2(2)
!
      integer, intent(inout) :: ierr
      real(8), intent(in) :: eps
!
      real(8), intent(in), optional :: hmin
      integer, intent(in) :: maxss
      real(8), optional :: xss(maxss),rvss(maxss)
      complex(8), optional :: yss(2,maxss),dyss(2,maxss),errss(2,maxss)
      integer, optional :: nss
!!PARAMETERS:
!!REVISION HISTORY:
! Initial Version - A.S. - 2013
!EOP
!
!BOC
      integer, parameter :: ndeq=4,liw=20+ndeq,lrw=22+16*ndeq+ndeq**2
      integer, save :: iwork(liw)
      integer :: neq(1)
      double precision, save :: rwork(lrw)

      double precision :: x,xout,y(ndeq)
      double precision :: atol(ndeq),rtol(1)

      integer :: i,itol,istate,itask,iopt
      integer, save :: mf=i_jcbn_stiff_lsode
      real(8), parameter :: abserr=1.d-9

      iopt = 1
      itask = 1
      itol = 1
      neq = ndeq

      x = x1
      xout = x2
      y(1) = dreal(y1(1))
      y(2) = aimag(y1(1))
      y(3) = dreal(y1(2))
      y(4) = aimag(y1(2))
      atol = 1.d-30
!      if ( itol == 1 ) then
!!       do i=1,ndeq
!!        if ( abs(epsilon(x)*y(i))>tiny(x) ) then
!!         atol = min(atol,abs(epsilon(x)*y(i)))
!!        end if
!!       end do
!      else
!       do i=1,ndeq
!        if ( abs(epsilon(x)*y(i))>tiny(x) ) then
!         atol(i) = min(abserr,abs(epsilon(x)*y(i)))
!        end if
!       end do
!      end if
      rtol = eps
      if ( ierr==1 ) then
        istate = 1
!        mf = nonstiff_lsode
        mf = stiff_lsode
        iwork = 0
        rwork = 0
        rwork(5) = (xout-x)/100.d0
      else
        istate = 2
      end if
      call DLSODE (dp_rhs, neq, y, x, xout, itol, rtol, atol, itask,    &
     &  istate, iopt, rwork,size(rwork), iwork,size(iwork), dp_jcbn, mf)

!      if ( istate == -1 ) then
!        if ( mf == nonstiff_lsode ) then
!         mf = stiff_lsode
!        else
!         mf = nonstiff_lsode
!        end if
!        if ( x .ne. xout ) then
!          call DLSODE (dp_rhs, neq, y, x, xout, itol, rtol, atol, itask,&
!     &          istate, iopt, rwork, lrw, iwork, liw, dp_jcbn, mf)
!        end if
!      end if

      y2(1) = dcmplx(y(1),y(2))
      y2(2) = dcmplx(y(3),y(4))

      if ( istate==2 ) then
        ierr = 0
      else if ( istate == -1 ) then
        ierr = 0
      else
        ierr = istate
      end if

      return
!EOC
      end subroutine odeint3
!
!BOP
!!IROUTINE: odeint2spl
!!INTERFACE:
      subroutine odeint2spl(x1,y1,x2,y2,ierr,                           &
     &                   eps,hmin,                                      &
     &                   maxss,xss,yss,dyss,rvss,errss,nss)
!!DESCRIPTION:
!     this is an adaptation of the numerical recipies routine odeint
!     for a system of two coupled differential equations
!     (with spline interpolation)
! {\bv
!     input:
!        x1       ==> initial ordinate
!        y1(2)    ==> initial value
!        x2       ==> final ordinate
!
!     input from common:
!        eps      ==> desired accuracy
!        hmin     ==> sub step size crash minimum
!        maxss    ==> allocation for sub step arrays
!
!        nss should be set to 0 before first call to odeint2
!
!     output:
!        y2(2)    <== value of coordinate at x2
!
!     optional output from common for cumulative calls to odeint2
!        xss       <== sub step ordinates
!        yss       <== sub step values
!        dyss      <== sub step dydx values
!        errss     <== sub step errors
!        nss       <== cumulative total substeps
! \ev}

!!DO_NOT_PRINT
      implicit none
!!DO_NOT_PRINT

!!ARGUMENTS:
      real*8 x1
      real*8 x2
      complex*16 y1(2)
      complex*16 y2(2)          ! out
      integer ierr
      real*8 eps
      real*8 hmin
      integer ::  maxss
      real(8), optional :: xss(maxss)
      complex(8), optional, intent(out) :: yss(2,maxss)   ! out
      complex(8), optional, intent(out) :: dyss(2,maxss)  ! out
      real(8), optional :: rvss(maxss)
      complex(8), optional, intent(out) :: errss(2,maxss) ! out
      integer, optional, intent(out) ::  nss               ! out
!!REVISION HISTORY:
! Adapted - A.S. - 2016
!EOP
!
!BOC
!
      logical :: accumulate
      real*8 htry
      real*8 hdid
      real*8 hnext

      real*8 x
      complex*16 y(2)
      complex*16 dydx(2)
      complex*16 err(2)
      complex*16 yscal(2)
      complex(8) :: dfdy(2,2)

      integer nstp
      integer i
      integer ibomb
      real*8  xtry
      real*8  z
      real*8  dz

!      real*8 tiny
!      parameter (tiny=1.d-30)
      integer maxstp
      parameter (maxstp=500)
!c     ==================================================================

      ierr=0
      ibomb=0
      hdid=0.0d0  !arbitrary intitialization;
      hnext=0.0d0 !proper hdid, hnext values are returned by stiff2

      htry=x2-x1

      x=x1
      y(1)=y1(1)
      y(2)=y1(2)
!      call derivs2spl(x,y,dydx)
      call jacob2spl(x,y,dfdy)
      dydx(1) = dfdy(1,1)*y(1) + dfdy(1,2)*y(2)
      dydx(2) = dfdy(2,1)*y(1) + dfdy(2,2)*y(2)

      accumulate = present(nss)

      if ( accumulate ) then
       nss=nss+1
       xss(nss)=x
       do i=1,2
        yss(i,nss)=y(1)
        dyss(i,nss)=dydx(1)
        errss(i,nss)=dcmplx(0.0d0,0.0d0)
       enddo
      end if


!c     take at most maxstp sub-steps
      do 20 nstp=1,maxstp

        yscal(1:2)=abs(y(1:2))+abs(htry*dydx(1:2))

        xtry=x+htry
        if((xtry-x2)*(xtry-x1).gt.0.0d0) htry=x2-x

        call stiff2spl(x,y,dydx,htry,hdid,hnext,eps,err,yscal,ibomb)
        if(ibomb.ne.0) then
          write(*,*) 'error in subroutine stiff: nstp=',nstp
          ierr=-ibomb
          return
        endif
!          call derivs2spl(x,y,dydx)
        call jacob2spl(x,y,dfdy)
        dydx(1) = dfdy(1,1)*y(1) + dfdy(1,2)*y(2)
        dydx(2) = dfdy(2,1)*y(1) + dfdy(2,2)*y(2)
        call zsplinterp(x,z,dz)

        if ( accumulate ) then
         if(nss.ge.maxss)then
          write(*,*) 'exceeded storage of substep in odeint2'
          ierr = 1
          return
         else
          nss=nss+1
          xss(nss)=x
          rvss(nss)=-2.0d0*z
          do i=1,2
            yss(i,nss)=y(i)
            dyss(i,nss)=dydx(i)
            errss(i,nss)=err(i)
          enddo
         endif
        endif

        if((x-x2)*(x2-x1).ge.0.0d0)then
          do i=1,2
            y2(i)=y(i)
          enddo
          return
        endif

        if(dabs(hnext).lt.hmin) then
          write(*,*) 'stepsize smaller than minimum in odeint'
          ierr=2
          return
        endif

        htry=hnext
20    continue

!      write(*,*) 'too many steps in odeint'
      ierr=3
      return
!EOC
      end subroutine odeint2spl

!BOP
!!IROUTINE: stiff2spl
!!INTERFACE:
      subroutine stiff2spl(x,y,dydx,htry,hdid,hnext,                    &
     &                  eps,err,yscal,ierr)
!!DESCRIPTION:
!     integration routine for a particular form of 2 coupled stiff
!     differential equations
!
!     this is an adaptation of the numerical recipies routine stiff
!     which uses a fourth order rosenbrock step for integrating
!     stiff ordinary differrential equations (with spline interpolation
!     between grid points)
!
! {\bv
!     input:
!       x    ==> initial coordinate
!       y    ==> inital value
!       dydx ==> initial value of derivitive
!       htry ==> trial value for the increment in x
!       eps  ==> desired accuracy
!       yscal==> component dependent accuracy criterion
!
!     output:
!       hdid <== acutual step size used fullfilling accuracy
!       hnext<== estimated next step size
!       x    <== new x
!       y    <== new value
!       err  <== component errors
!       ierr <== abnormal termination code
! \ev}

!!DO_NOT_PRINT
      implicit none
!!DO_NOT_PRINT

!!ARGUMENTS:
      real*8 x
      complex*16 y(2)
      complex*16 dydx(2)
      complex*16 err(2)
      complex*16 yscal(2)
      real*8 htry
      real*8 hdid
      real*8 hnext
      real*8 eps
      integer ierr
!!REVISION HISTORY:
! Initial Version - B.W. - 2011
! Adapted - A.S. - 2013
!EOP
!
!BOC
      logical dbug
      integer i
      real*8 h
      real*8 h2
      real*8 h25
      real*8 h108
      real*8 hm0
      real*8 gam
      real*8 gm1
      real*8 gm2
      real*8 gm3
      real*8 s
      real*8 errmax
      complex*16 d11
      complex*16 d12
      complex*16 d21
      complex*16 d22
      complex*16 odenom


!      integer maxtry
!      parameter (maxtry=40)
!      real*8 xstif
!      complex*16 ystif
!      complex*16 estif
!      integer ntry
!!c     this common block usefull for debugging
!      common /stiffcom/ xstif(maxtry),ystif(2,maxtry),                  &
!     &                  estif(2,maxtry),ntry

      real*8 xsav
      complex*16 ysav(2)
      complex*16 dysav(2)
      complex*16 dfdx(2)
      complex*16 dfdy(2,2),dfdy1(2,2)

      complex*16 ua(2)
      complex*16 ub(2)
      complex*16 uc(2)
      complex*16 ud(2)
      complex*16 ga(2)
      complex*16 gb(2)
      complex*16 gc(2)
      complex*16 gd(2)


      real*8 safety
      parameter (safety=0.9d0)
      real*8 grow,pgrow
      parameter (grow=1.5d0,pgrow=-.25d0)
      real*8 shrnk,pshrnk
      parameter (shrnk=0.5d0,pshrnk=-1.d0/3.d0)
      real*8 errcon
      parameter (errcon=.1296d0)

      real*8 b1,b2,b3,b4
      parameter (b1=228.0d0,b2=54.0d0,b3=25.0d0,b4=125.0d0)
      real*8 e1,e2,e4
      parameter (e1=34.0d0,e2=21.0d0,e4=125.0d0)
      real(8), parameter :: tiny_r8=tiny(e1)
!c     ==================================================================

      ierr=0

      xsav=x
      do i=1,2
        ysav(i)=y(i)
        dysav(i)=dydx(i)
      enddo

      call jacob2spl(xsav,ysav,dfdy,dfdx)

      h=htry
      do 25 ntry=1,maxtry

!c       perform matrix inversions analytically
!c       --------------------------------------------
!c       let s=gam*h and   f_dot=(  a  b  )
!c                               (  c  d )
!c       then {(1/s)(1-s*f_dot)}^-1 =
!c                             (    1-sd        sb  )
!c                   s/(1-sw)  (      sc      1-sa )
!c
!c       1-sw=(1-sa)(1-sd)-(sb)(sc)
!c       --------------------------------------------
        gam=0.5d0
        s=gam*h
        d11=1.0d0-s*dfdy(2,2)
        d12=      s*dfdy(1,2)
        d21=      s*dfdy(2,1)
        d22=1.0d0-s*dfdy(1,1)
        odenom=gam/(d11*d22-d12*d21)


!c       set up right hand side ga
        hm0=0.5d0*h
        do 14 i=1,2
          ua(i)=dysav(i)+hm0*dfdx(i)
14      continue
!c       matrix multiply ua by s*((1-s dfdy)^-1)
        ga(1)=( d11*ua(1) + d12*ua(2) )*odenom
        ga(2)=( d21*ua(1) + d22*ua(2) )*odenom


        h2=2.0d0*h
        do 15 i=1,2
          y(i)=ysav(i)+h2*ga(i)
15      continue
        x=xsav+h
!        call derivs2(x,y,dydx)
        call jacob2spl(x,y,dfdy1)
        dydx(1) = dfdy1(1,1)*y(1) + dfdy1(1,2)*y(2)
        dydx(2) = dfdy1(2,1)*y(1) + dfdy1(2,2)*y(2)

!c       set up right hand side gb
        hm0=-1.5d0*h
        gm1=-8.0d0
        do 16 i=1,2
          ub(i)=dydx(i)+hm0*dfdx(i)+gm1*ga(i)
16      continue
!c       matrix multiply ub by s*((1-s dfdy)^-1)
        gb(1)=( d11*ub(1) + d12*ub(2) )*odenom
        gb(2)=( d21*ub(1) + d22*ub(2) )*odenom

        h25=h/25.0d0
        gm1=48.0d0
        gm2= 6.0d0
        do 17 i=1,2
          y(i)=ysav(i)+h25*( gm1*ga(i)+gm2*gb(i) )
17      continue
        x=xsav+(3.0d0*h/5.0d0)
!        call derivs2(x,y,dydx)
        call jacob2spl(x,y,dfdy1)
        dydx(1) = dfdy1(1,1)*y(1) + dfdy1(1,2)*y(2)
        dydx(2) = dfdy1(2,1)*y(1) + dfdy1(2,2)*y(2)

!c       set up right hand side gc
        hm0=121.0d0*h/50.0d0
        gm1=(372.0d0/25.0d0)
        gm2=( 60.0d0/25.0d0)
        do 18 i=1,2
          uc(i)=dydx(i)+hm0*dfdx(i)+gm1*ga(i)+gm2*gb(i)
18      continue
!c       matrix multiply uc by s*((1-s dfdy)^-1)
        gc(1)=( d11*uc(1) + d12*uc(2) )*odenom
        gc(2)=( d21*uc(1) + d22*uc(2) )*odenom


!c       set up right hand side g4
        hm0=29.0d0*h/250.0d0
        gm1=(-112.0d0/125.0d0)
        gm2=(- 54.0d0/125.0d0)
        gm3=(- 50.0d0/125.0d0)
        do 19 i=1,2
          ud(i)=dydx(i)+hm0*dfdx(i)+gm1*ga(i)+gm2*gb(i)+gm3*gc(i)
19      continue
!c       matrix multiply ud by s*((1-s dfdy)^-1)
        gd(1)=( d11*ud(1) + d12*ud(2) )*odenom
        gd(2)=( d21*ud(1) + d22*ud(2) )*odenom


        h108=h/108.0d0
        do 21 i=1,2
          y(i)=ysav(i)+h108*( b1*ga(i)+b2*gb(i)+b3*gc(i)+b4*gd(i) )
          err(i)=      h108*( e1*ga(i)+e2*gb(i)         +e4*gd(i) )
21      continue

        x=xsav+h

        xstif(ntry)=x
        do 22 i=1,2
          ystif(i,ntry)=y(i)
          estif(i,ntry)=err(i)
22      continue

        if(x.eq.xsav) then
          write(*,*) 'stepsize not significant in stiff2spl'
          ierr= 1
          return
        endif

        errmax=abs(err(1))/(abs(yscal(1))+tiny_r8)
        errmax=max(errmax,abs(err(2))/(abs(yscal(2))+tiny_r8))
        errmax=errmax/eps

        if(errmax.le.1.0d0)then
          hdid=h
          if(errmax.gt.errcon)then
            hnext=safety*h*errmax**pgrow
          else
            hnext=grow*h
          endif
          return
        else
          hnext=safety*h*errmax**pshrnk
          if(hnext.lt.shrnk*h) hnext=shrnk*h
          h=hnext
        endif

25    continue

      write(*,*) 'exceeded maxtry=',maxtry,' in stiff2spl'
      write(*,*) ' x=',x
      write(*,*) ' y(1:2)=',y(1:2)
      write(*,*) ' dydx(1:2)=',dydx(1:2)
      write(*,*) ' err(1:2)=',err(1:2)
      write(*,*) ' errmax=',errmax*eps,' target=',eps
      ierr = 2
      return
!EOC
      end  subroutine stiff2spl
!
!BOP
!!IROUTINE: jacob2spl
!!INTERFACE:
      subroutine jacob2spl(x,y,dfdy,dfdx)
!!DESCRIPTION:
!     constructs the jacobian of the system of equations
! {\bv
!     d/dr gbar = {1-io*pow}/r*gbar + {m         }*fbar
!     d/dr fbar=  {w         }*gbar - {1+io*pow}/r*fbar
!
!     where
!       m = 1+{e-v(r)}/c^2    v(r) = -2*z(r)/r
!       w = { l*(l+1)/(m*r^2) } - { e-v(r) }
!
! in_out=+1 for outward propagation of gbar:  g=r^pow gbar   f=r^pow fbar
!       =-1 for inward  propagation of gbar:  g=1/r^pow gbar f=1/r^pow fbar
!       = 0 for inward or outward propagation of gbar==g  fbar==f
!
!     input:
!       x    ==> r   not necessarily on tabulated grid
!       y(1) ==> gbar
!       y(2) ==> fbar
!
!     output:
!       dfdy(:)
!       dfdx(:)
! \ev}

!!DO_NOT_PRINT
      implicit none
!!DO_NOT_PRINT

!!ARGUMENTS:
      real(8), intent(in) :: x
      complex(8), intent(in) :: y(2)
      complex(8), intent(out) :: dfdy(2,2)
      complex(8), intent(out), optional :: dfdx(2)
!!REVISION HISTORY:
! Initial Version - B.W. - 2011
! Adapted - A.S. - 2013
!EOP
!
!BOC

!      integer in_out
!      real*8  power
!      real*8  cent
!      real*8  c2inv
!      complex*16 enrg
!      common/scalrelcom1/c2inv,cent,power,enrg
!      common/scalrelcom2/in_out

      real*8 xinv
      real*8 b1
      real*8 b2
      real*8 db1
      real*8 db2
      real*8 z
      real*8 dz
      real*8 v
      real*8 dv
      real(8) :: rvprim
      complex*16 emv
      complex*16 m
      complex*16 dm
      complex*16 centrif
      complex*16 w
      complex*16 dw
!c     ==================================================================

      xinv = 1.0d0/x
      b2 = in_out*power*xinv
      b1 = xinv - b2
      b2 = xinv + b2
!      b1=(1.0d0-in_out*power)*xinv
!      b2=(1.0d0+in_out*power)*xinv


!c     zsplinterp is the spline interpolation of z(i)
      call zsplinterp(x,z,dz)
      v=-2.0d0*z*xinv

      emv= enrg-v
      m= 1.0d0+ emv*c2inv           ! 2*M
      centrif=(cent*(xinv*xinv))/m
      if ( sp_orb .ne. 0.d0 ) then  !  spin-orbit 
       rvprim  = -2.d0*(z*xinv-dz)  ! r*v'
       centrif = centrif * ( 1.d0 - c2inv*sp_orb*rvprim/m )
      end if
      w= centrif - emv

      dfdy(1,1)= b1
      dfdy(1,2)= m
      dfdy(2,1)= w
      dfdy(2,2)=-b2

      if ( present(dfdx) ) then
       db1=-b1*xinv
       db2=-b2*xinv
       dv=-2.0d0*( dz*x-z )*(xinv*xinv)
       dm= -dv*c2inv
       dw= -( (dm/m)+2.0d0*xinv )*centrif +dv
       dfdx(1)=db1*y(1) +  dm*y(2)
       dfdx(2)= dw*y(1) - db2*y(2)
      end if

      return
!EOC
      end subroutine jacob2spl
!
      end module stiffode

