
!!ROUTINE: TemprFun
!!INTERFACE:
       function TemprFun(z,iflag)
!!DESCRIPTION:
! complex Fermi function (if iflag=0), etc; \\
! input {\tt z} is complex value ( z = (E-Ef)/T )
!
!!REVISION HISTORY:
! Initial version - A.S. - 2013
!EOP
!
!BOC
       implicit none
       complex(8) :: TemprFun
       complex(8), intent(in) :: z
       integer iflag
!c
       complex(8) :: Fz
       complex(8), parameter :: cone=(1.d0,0.d0),czero=(0.d0,0.d0)
       real(8), parameter :: explim = log(epsilon(1.d0))
!       real(8) :: dz
!       real(8), parameter :: pi=3.1415926535897932385d0,pi2=2*pi
!       real(8), parameter :: eps=epsilon(pi)

       TemprFun = czero
       if (-dreal(z)<explim) then
        Fz = cdexp(-z)  ! zero
       else
        Fz = F(z)
       end if

!       if ( abs(aimag(z))<eps ) then
!         dz = dreal(z)
!         if(exp(-dz)<eps) then
!          Fz = czero + eps
!         else if(exp(dz)>eps) then
!          Fz = F(z)
!         else
!          Fz = cone - eps
!         end if
!       else 
!         Fz = F(z)
!       end if
       if(iflag.eq.0) then                        ! function
        TemprFun = Fz
       else 
        if(iflag.eq.1) then                   ! f*log(f)+(1-f)*log(1-f)
!         if(abs(Fz) < eps.or.abs(Fz-cone)<eps) then
!          TemprFun = czero
!         else
!!          TemprFun = Fz*log(Fz) + (cone-Fz)*log(cone-Fz) , |imag(z)|<pi
!!          TemprFun = z*Fz + log(cone+exp(-z))
!!          TemprFun = -z*(cone-Fz) - log(Fz),               |imag(z)|<pi

            TemprFun = -z*(cone-Fz)-log(Fz)

!         end if
        end if
       end if

       return

!EOC
       CONTAINS

!!ROUTINE: F
       pure function F(z)
!!REMARKS:
! private procedure of function TemprFun
!EOP
!
!BOC
       implicit none
       complex(8) :: F
       complex(8), intent(in) :: z
       F = cone / ( cone + cdexp( z ) )
       return
       end function F

       end function TemprFun

!BOP
!!ROUTINE: fermiFun
!!INTERFACE:
       elemental function fermiFun(z)
!!DESCRIPTION:
! real Fermi function; \\
! {\tt z} is a real value ( z = (E-Ef)/T )
!
!!REVISION HISTORY:
! Initial version - A.S. - 2013
!EOP
!
!BOC
       implicit none
       real(8) :: fermiFun
       real(8), intent(in) :: z
!c
       real(8) :: Fz
       real(8), parameter :: one=1.d0
       real(8), parameter :: explim = log(epsilon(one))  ! -36.d0
!
       if(-z<explim) then
        Fz = dexp(-z)  ! zero
       else
        Fz = one / ( one + dexp( z ) )
       end if
       fermiFun = Fz
       return
!EOC
       end function fermiFun
