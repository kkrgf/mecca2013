!BOP
!!ROUTINE: run_eos
!!INTERFACE:
      program run_eos
!!DESCRIPTION:
! main program to perform massive EOS {\sc mecca} calculations
! based on template files
!
!!USES:
      use mecca_types, only : RunState,IniFile
      use  input, only : read2intFile
      use mecca_scf_interface, only : mecca_scf
      use gfncts_interface, only : g_numNonESsites
      use universal_const, only : ry2eV,ry2kelvin,ry2H,bohr2A

!!DO_NOT_PRINT
      implicit none
!!DO_NOT_PRINT

!!TYPES:
      type :: EosJob
       character(80) :: name
       real(8)       :: Tempr
       real(8)       :: volume
       real(8)       :: energy
       real(8)       :: pressure
       real(8)       :: freeEn
       real(8)       :: efermi
       real(8)       :: magnmm
      end type EosJob
!!ARGUMENTS:
! command line should have two arguments:
! * name of file with template files names and units rescaling info and
! * case (job) number
!!REVISION HISTORY:
! Initial Version - A.S. - 2014
!EOP
!
!BOC
!********************************************************************
      type(RunState), target :: mecca_state
      type(IniFile), target :: inFile
      type(EosJob) :: job
      character*200 outinfo
      integer ierr
      character(80) :: in_file ! basic input data for mecca
      character(80) :: template,inpfile,keyword
      character(1) :: buffer(80)
      integer :: ninp  ! number of input lines per job
      real(8) :: v_scale  ! to rescale volume (input alat is expected to be in Bohr)
      real(8) :: T_scale  ! to rescale tempereture (input T is expected to be in Ry)
      integer :: narg,ncase,nch,nmax,i,ioss,ipos,j
      real(8) :: volume

      logical, external :: isDigit

! available keywords
      character(11), parameter :: keyw_T = 'temperature'
      character(16), parameter :: keyw_V = 'unit cell volume'
!
      narg = iargc()
      if ( narg == 2 ) then
       call getarg(1,in_file)
       call getarg(2,keyword)
       read(keyword,*) ncase
       nch = 11
       open(nch,file=trim(in_file),status='OLD')
       read(nch,'(a)') template
       read(nch,'(a)') inpfile
       read(nch,*) ninp,v_scale,T_scale
       close(nch)
!DEBUGPRINT
        write(*,*) inpfile,ninp
        write(*,*) v_scale,T_scale
!DEBUGPRINT

       nmax = 0
       i = 0
       call readIniFile(inpfile,i)
       if ( i .le. 0 ) then
        write(*,*) ' i=',i
        write(*,*) ' inpfile='//trim(inpfile)
        write(*,*) ' ERROR: unable to read inp-data-file'
        write(*,*)
        stop 'UNREADABLE INPFILE FILE?'
       else
        nmax = i/ninp
        if ( mod(i,ninp) .ne. 0 ) then
         write(*,*) ' WARNING: not found ',i-nmax*ninp,                 &
     &              ' last lines of inp-data-file',trim(inpfile)
        end if
       end if
       if ( ncase > nmax .or. ncase < 1 ) then
           stop
       end if
       open(nch,file=trim(inpfile),status='OLD')
       do i=1,(ncase-1)*ninp
         read(nch,*)
       end do
!  i == ncase
       do j=1,ninp
        buffer(:) = ' '
        if ( j == 1 ) then
          read(nch,'(a80)') job%name
        else
          ipos = len(keyword)
          do i=1,size(buffer)
            read(nch,'(1a1)',advance='NO',iostat=ioss) buffer(i)
            if ( ipos>=i .and. i>1 .and. isDigit(buffer(i)) ) then
              if ( buffer(i-1) == ' ' ) then
               ipos = i-1
              else if ( buffer(i-1) == '-' ) then
               if ( i>2 ) then
                if ( buffer(i-2) == ' ' ) then
                 ipos = i-2
                end if
               else
                if ( i>0 ) ipos=i-1
               end if
              end if
            end if
            if ( ioss.ne.0 ) exit
          end do
          if ( ioss == 0 ) then
            read(nch,'()',advance='YES')
          end if
          write(keyword,'(80a1)') buffer(1:ipos)
          select case (trim(adjustl(keyword)))
           case(keyw_T)
             write(keyword,'(80a1)') buffer(ipos+1:)
             read(keyword,*) job%Tempr
           case(keyw_V)
             write(keyword,'(80a1)') buffer(ipos+1:)
             read(keyword,*) job%volume
          end select
        end if
       end do
       close(nch)
       if ( job%volume <= 0 ) then
         stop
       end if
!
       mecca_state%intfile => inFile
       call read2intFile(template,mecca_state%intfile,ierr)

       if ( ierr == 0 ) then

         mecca_state%intfile%name = mecca_state%intfile%genName
         mecca_state%intfile%genName =                                  &
     &    trim(adjustl(mecca_state%intfile%name))//' '//trim(job%name)
         mecca_state%intfile%Tempr = job%Tempr/T_scale
         v_scale = v_scale*mecca_state%intfile%ba*mecca_state%intfile%ca
         mecca_state%intfile%alat = (job%volume/v_scale)**(1.d0/3.d0)

         if ( mecca_state%intfile%npts > 1000 .and.                     &
     &                       mecca_state%intfile%Tempr .ne. dble(0)     &
     &        .OR. mecca_state%intfile%npts <= 1000 .and.               &
     &             mecca_state%intfile%Tempr == dble(0) ) then
          call mecca_scf(mecca_state,outinfo)
         else
          write(*,*) '    Tempr = ',mecca_state%intfile%Tempr
          write(*,*) '    NPTS  = ',mecca_state%intfile%npts
          write(*,*) ' EITHER Tempr=0 and NPTS<=1000 OR '//             &
     &                       'Tempr.ne.0 and NPTS>1000'
          stop ' ERROR:  INCONSISTENT INPUT'
         end if

         job%magnmm = 0      !TODO: can be generated from gf_out box
         job%energy = 0
         job%freeEn = 0
         job%pressure = 0
         read(outinfo,*,iostat=ioss) volume,job%freeEn,job%energy,      &
     &                                                     job%pressure
         volume = volume*g_numNonESsites(mecca_state%intfile)
         job%freeEn = job%freeEn*g_numNonESsites(mecca_state%intfile)
         job%energy = job%energy*g_numNonESsites(mecca_state%intfile)
         job%efermi = mecca_state%intfile%etop
!
         if ( abs(job%Tempr-mecca_state%intfile%Tempr*ry2eV) >          &
     &                   1.d-6*job%Tempr ) then
          write(6,*) ' ERROR: job%Tempr .NE. inFile%Tempr'
          write(6,*) ' job%Tempr    = ',job%Tempr
          write(6,*) ' inFile%Tempr = ',inFile%Tempr
          job%Tempr=mecca_state%intfile%Tempr*ry2eV
         end if
         if ( abs(job%volume-volume) > 1.d-6*job%volume ) then
          write(6,*) ' ERROR: job%volume .NE. volume'
          write(6,*) ' job%volume = ',job%volume
          write(6,*) ' volume     = ',volume
          job%volume=volume
         end if

         if ( mecca_state%info == 0 .or. mecca_state%info == 1 ) then
         call save_results(ncase,job)
         else
         open(nch,file='RUN_FAILED.'//trim(mecca_state%intfile%genName))
         write(nch,'('' mecca_state%info='',i5)') mecca_state%info
         close(nch)
         end if

       else
        if ( ierr == -1 ) then
        write(*,*) ' ERROR: unable to read template-file'
        write(*,*) trim(template)
        write(*,*)
        stop 'UNREADABLE TEMPLATE FILE?'
        else
         continue
        end if
       end if
!
      else
        write(0,*) ' ERROR: Command-line should have two arguments'
      end if
      stop

!EOC
      CONTAINS

!BOP
!!IROUTINE: save_results
!!INTERFACE:
      subroutine save_results(ncase,job)

!!DO_NOT_PRINT
      implicit none
!!DO_NOT_PRINT

!!ARGUMENTS:
      integer, intent(in) :: ncase
      type(EosJob), intent(in) :: job
!!PARAMETERS:
      character(11), parameter :: keyw_T = 'temperature'
      character(16), parameter :: keyw_V = 'unit cell volume'
      character(6), parameter :: keyw_E = 'energy'
      character(11), parameter :: keyw_FE = 'free energy'
      character(8), parameter :: keyw_P = 'pressure'
      character(12), parameter :: keyw_Ef = 'fermi energy'
      character(12), parameter :: keyw_MM = 'magn moment'
      character(10), parameter :: filen = 'case'
!!REVISION HISTORY:
! Initial Version - A.S. - 2014
!!REMARKS:
! this subroutine should be compatible with definition of type EosJob
!EOP
!
!BOC
      character(80) :: savefile
      integer nch

      write(savefile,'(a,i0.5,''.sav'')') trim(adjustl(filen)),ncase
      nch = 11
      open(nch,file=savefile)
      write(nch,'(a)') job%name
      write(nch,'(1x,a,3(1x,ES17.10,1x,a))') keyw_T,job%Tempr,'eV'      &
     &,               job%Tempr/ry2ev,'Ry',job%Tempr/ry2ev*ry2kelvin,'K'
      write(nch,'(1x,a,3(1x,ES17.10,1x,a))') keyw_V,job%volume,'Bohr^3' &
     &,               job%volume,'Bohr^3',job%volume*bohr2A**3,'A^3'
      write(nch,'(1x,a,3(1x,ES17.10,1x,a))') keyw_E,job%energy,'Ry'     &
     &,               job%energy*ry2H,'Ha'
      write(nch,'(1x,a,3(1x,ES17.10,1x,a))') keyw_FE,job%freeEn,'Ry'    &
     &,               job%freeEn*ry2H,'Ha'
      write(nch,'(1x,a,3(1x,ES17.10,1x,a))') keyw_P,job%pressure,'Mbar' &
     &,               job%pressure*10,'GPa'
      write(nch,'(1x,a,3(1x,ES17.10,1x,a))') keyw_Ef,job%efermi,'Ry'    &
     &,               job%efermi*ry2H,'Ha'
      if ( job%magnmm .ne. 0 ) then
      write(nch,'(1x,a,3(1x,ES17.10,1x,a))') keyw_MM,job%magnmm,'muB'   &
     &,               job%magnmm,' '
      end if
      close(nch)
      return
!EOC
      end subroutine save_results

      end program run_eos
