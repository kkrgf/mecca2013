      module invparams
!C   *                                                                *
!C   *   Parameter file for Multi-sublattice/component KKR-CPA:       *
!C   *     to inverse matrix                                          *
!C   *                                                                *
      implicit none

!C   *    nblock       -- blocksize for LAPACK zgetri.f               *
      integer ::  nblock=64
!c      parameter (nblock=32)
!c      parameter (nblock=1)

!C   *    nlev         -- not used
!C
      integer, parameter :: nlev=4

!c          tolinv is a "zero-treshold" for screened (1-G*t)-matrix elements
!c              and tolerance for iterative methods
!c---------------------------------------------------------------------------
      real(8), parameter :: tolinv=0.5d-7

!c          MemoryLimit is a limit for "big" memory allocation in
!c                         ILU-inversion (if it is equal to 0, than
!c                         all available memory can be used)
!c---------------------------------------------------------------------------
      integer, parameter :: MemoryLimit=0
!c      parameter (MemoryLimit=150)

!c
!c          tolCut is a "zero-treshold" for struct.const.matrix elements
!c                                           (without screening)
!c---------------------------------------------------------------------------
      real(8), parameter :: tolCut=0.1d0*tolinv   !   (dosparse: if SPARSE or Rcut.ne.0)

!c          prlim (in D.U.) decides whether Cluster Approach can be used
!c          if(dimag(pdu).gt.prlim) --  Cluster Approach
!c---------------------------------------------------------------------------
      real(8), parameter :: prlim=5.035542d-02          !    (imethod=0,2,4)

!c          prSLU (in D.U.) decides whether SuperLU is used near the real axis
!c          if(dimag(pdu).gt.prSLU) -- iterative method
!c                             else -- SuperLU
!c---------------------------------------------------------------------------
      real(8), parameter :: prSLU=0.03d+10         ! (sparse technique only)
!c      parameter (prSLU=0.1d0)
!c      parameter (prSLU=0.0d0)

!c          prPRC (in D.U.) defines where the preconditioner is used
!c          if(dimag(pdu).gt.prPRC) --  without precond.
!c                             else --  with precond.
!c                         
!c---------------------------------------------------------------------------
      real(8), parameter :: prPRC=0.d0                ! ( iterations only )
!c      parameter (prPRC=0.3d0)

      integer, external :: ilaenv

      contains

      function invpar_MemoryLimit()
      integer :: invpar_MemoryLimit
      invpar_MemoryLimit = MemoryLimit
      end function invpar_MemoryLimit

      function invpar_nblock()
      integer :: invpar_nblock
      invpar_nblock = nblock
      end function invpar_nblock

      function invpar_nlev()
      integer :: invpar_nlev
      invpar_nlev = nlev
      end function invpar_nlev

      function invpar_tolinv()
      real(8) :: invpar_tolinv
      invpar_tolinv = tolinv
      end function invpar_tolinv

      function invpar_tolcut()
      real(8) :: invpar_tolcut
      invpar_tolcut = tolcut
      end function invpar_tolcut

      function invpar_prlim()
      real(8) :: invpar_prlim
      invpar_prlim = prlim
      end function invpar_prlim

      function invpar_prSLU()
      real(8) :: invpar_prSLU
      invpar_prSLU = prSLU
      end function invpar_prSLU

      function invpar_prPRC()
      real(8) :: invpar_prPRC
      invpar_prPRC = prPRC
      end function invpar_prPRC

      end module invparams

!!!BOP
!!!!ROUTINE: invprmt
!!!!INTERFACE:
!!      subroutine invprmt(invswitch,invparam,ndmatr,ndmtr2)
!!!!DESCRIPTION:
!!! provides parameters {\tt invparam} and {\tt ndmtr2} for KKR matrix inversion,
!!! {\tt ndmatr} is larger size of matrix, {\tt invswitch) is control parameter
!!!
!!!EOP
!!!
!!!BOC
!!      use invparams
!!
!!      implicit none
!!      integer invswitch,invparam,ndmatr,ndmtr2
!!      character*10 :: sname = 'invprmt'
!!
!!      if(invswitch.eq.0) then
!!       invparam = nblock
!!       ndmtr2 = ndmatr*(ndmatr+3)
!!      else if(invswitch.eq.1) then
!!       invparam = nblock
!!       ndmtr2 = ndmatr*nblock
!!!!      else if(invswitch.eq.2) then
!!!!       invparam = nlev
!!!!       ndmtr2 = (ndmatr/2+1)*(ndmatr+1)
!!!!       call fstop(sname//' invswitch=2, ndmtr2 is unndefined -- has not been checked')
!!!!      else if(invswitch.eq.3) then
!!!!       invparam = nblock
!!!!       ndmtr2 = ndmatr*9
!!!!       call fstop(sname//' invswitch=3 -- has not been checked')
!!      else
!!       ndmtr2 = 0
!!       write(6,*) ' INVSWITCH=',invswitch
!!       call fstop(sname//' :: unimplemented INVSWITCH')
!!      end if
!!      return
!!!EOC
!!      end subroutine invprmt


