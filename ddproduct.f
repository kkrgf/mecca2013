!BOP
!!ROUTINE: ddproduct
!!INTERFACE:
      function ddproduct(qm,xk1,xk2)
!!DESCRIPTION:
!  returns {\tt ddproduct}=$\sum qm_{\alpha,\beta} xk1_\alpha xk2_\beta$; 
!  $\alpha,\beta =1,3$
!

!!DO_NOT_PRINT
      implicit none
!!DO_NOT_PRINT

!!ARGUMENTS:
      real(8), intent(in) :: qm(3,3),xk1(3),xk2(3)
!!REVISION HISTORY:
! Initial version - A.S. - 2007
!EOP
!
!BOC
      real(8) :: ddproduct
      real(8) :: v1(3)
!c
!c  ddproduct = sum (qm_{alpha,beta}*xk1_alpha*xk2_beta); alpha,beta =1,3
!c
      v1(1) = dot_product(xk1(1:3),qm(1:3,1))
      v1(2) = dot_product(xk1(1:3),qm(1:3,2))
      v1(3) = dot_product(xk1(1:3),qm(1:3,3))

      ddproduct = dot_product(v1(1:3),xk2(1:3))

      return
!EOC
      end function ddproduct
