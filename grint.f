!BOP
!!ROUTINE: grint
!!INTERFACE:
      subroutine grint(nrel,cphot,                                      &
     &                 lmax,kkrsz,                                      &
!!     &                 icryst,alat,                                     &
     &                 energy,pmom,                                     &
     &                 iswzj,                                           &
     &                 vr,xr,rr,                                        &
     &                 h,jmt,jws,rScat,rws,                             &
     &                 rmt_true,r_circ,ivar_mtz,                        &
     &                 fcount,weight,rmag,vj,iVP,                       &
     &                 zlab,                                            &
     &                 tab,pzz,pzj,cotdl,almat,                         &
     &                 mtasa,iprint,istop)
!!DESCRIPTION:
! computes t-matrix, wave-functions (z*z,z*j) and their radial integrals,
! and phase-shifts related data
!
!!USES:
      use mecca_constants
      use mtrx_interface

!!DO_NOT_PRINT
      implicit none
!!DO_NOT_PRINT

!!ARGUMENTS:
      integer, intent(in) :: nrel
      real(8), intent(in) :: cphot
      integer, intent(in) :: lmax,kkrsz
!!      integer, intent(in) :: icryst
!!      real(8), intent(in) :: alat       !  lattice constant
      complex(8), intent(in) :: energy
      complex(8), intent(in) :: pmom
      integer, intent(in) :: iswzj
      real(8), intent(in) :: vr(:)  ! (iprpts)
      real(8), intent(in) :: xr(:)  ! (iprpts)
      real(8), intent(in) :: rr(:)  ! (iprpts)
      real(8), intent(in) :: h
      integer, intent(in) :: jmt,jws
      real(8), intent(in) :: rScat
      real(8), intent(in) :: rws
      real(8), intent(in) :: rmt_true
      real(8), intent(in) :: r_circ
      integer, intent(in) :: ivar_mtz
      integer fcount
      real(8), intent(in) :: weight(:)      ! (MNqp)
      real(8), intent(in) :: rmag(:,:,:,:)  ! (MNqp,MNqp,MNqp,MNF)
      real(8), intent(in) :: vj(:,:,:,:)    ! (MNqp,MNqp,MNqp,MNF)
      logical, intent(in) :: iVP
      complex(8), intent(out) :: zlab(:,:)  ! (iprpts,iplmax+1)
      complex(8), intent(out) :: tab(:,:)   ! (ipkkr,ipkkr) t-matrix
      complex(8), intent(out) :: pzj(:,:)   ! (ipkkr,ipkkr)
!                                           ! integral of (reg.wf.*irreg.wf) over ws cell
      complex(8), intent(out) :: pzz(:,:)   ! (ipkkr,ipkkr)
!                                           ! integral of (reg. wf.)**2 over ws cell
      complex(8), intent(out) :: cotdl(:)   ! (iplmax+1)
      complex(8), intent(out) :: almat(:)   ! (iplmax+1)
      integer, intent(in) :: mtasa,iprint
      character(10), intent(in) :: istop    ! index of subroutine in which prgm will stop
!!REVISION HISTORY:
! based on was/gms version - 1991
! Adapted - A.S. - 2013
!!REMARKS:
! kkrsz=(lmax+1)**2 - redundant argument
!EOP
!
!BOC
!      include 'imp_inp.h'
!
!
      interface
        subroutine semrel(NRELV,CLIGHT,LMAX,ENERGY,PNREL,G,TMATL,       &
     &COTDL,ALMAT,ZLZL,ZLJL,H,JMT,JWS,X,R,RV,RMT_TRUE,RSCAT,R_CIRC,     &
     &            IVAR_MTZ,FCOUNT,WEIGHT,RMAG,VJ,lVP,ISWZJ,IPRINT,ISTOP)
        integer :: NRELV
        real(8) :: CLIGHT
        integer :: LMAX
        complex(8) :: ENERGY
        complex(8) :: PNREL
        complex(8) :: G(:,:)  ! (iprpts,lmax+1)
        complex(8) :: TMATL(lmax+1)
        complex(8) :: COTDL(lmax+1)
        complex(8) :: ALMAT(lmax+1)
        complex(8) :: ZLZL(lmax+1)
        complex(8) :: ZLJL(lmax+1)
        real(8) :: H
        integer :: JMT
        integer :: JWS
        real(8) :: X(:)
        real(8) :: R(:)
        real(8) :: RV(:)
        real(8) :: RMT_TRUE
        real(8) :: RSCAT
        real(8) :: R_CIRC
        integer :: IVAR_MTZ
        integer :: FCOUNT
        real(8) :: WEIGHT(:)
        real(8) :: RMAG(:,:,:,:)
        real(8) :: VJ(:,:,:,:)
        logical, intent(in) :: lVP
        integer :: ISWZJ
        integer :: IPRINT
        character(10) :: ISTOP
        end subroutine semrel
      end interface
!
      character(*), parameter :: sname='grint'
!c
      integer, parameter :: ipktopl=3
      integer, parameter :: ktopl(1+ipktopl) = [1,2,4,8]
      integer, parameter :: kdiagl(1+ipktopl) = [1,2,4,7]
      real(8) :: clight
!c
!c
      complex(8) :: tmatl(lmax+1)
      complex(8) :: zlzl(lmax+1)
      complex(8) :: zljl(lmax+1)

      integer :: i,ktop,kdiag,l,m,lm,nrelv

!c   *    ipktop  : No. of non-zero symmetry elements of Tau-l,lp     *
!c
!      integer ipktop
!      parameter (ipktop=ipkkr*ipkkr)
      complex(8) :: zjout(size(pzj,1)*size(pzj,1))
      complex(8) :: zzout(size(pzz,1)*size(pzz,1))
      complex(8) :: pzzck(size(pzz,1),size(pzz,2))
      complex(8) :: pzjck(size(pzj,1),size(pzj,2))
!c
      clight = cphot
      nrelv = 0
      if (nrel > 1 ) then
       nrelv = nrel
       clight = cphot*ten**nrelv
      end if
      if(iprint.ge.6) then
         write(6,'('' grint:  nrelv,lmax='',2i5)') nrelv,lmax
!!         write(6,'('' grint: icryst,alat='',i5,d12.4)') icryst,alat
         write(6,'('' grint: energy,pmom='',4d12.4)') energy,pmom
         write(6,'('' grint:     jmt,jws='',2i5)') jmt,jws
      endif
      if(lmax.le.ipktopl) then
       ktop=ktopl(lmax+1)
       kdiag=kdiagl(lmax+1)
      else
       ktop=ktopl(ipktopl+1)
       kdiag=kdiagl(ipktopl+1)
       do l=ipktopl+1,lmax
        ktop=ktop+(2*l+1)
        kdiag=kdiag+(2*l+1)
       end do
      end if

!      if(ktop.gt.ipktop) then
!         write(6,'('' grint:: ipktop not large enough'')')
!         write(6,'('' grint:: lmax'',i5)') lmax
!         write(6,'('' grint:: ipktop='',i2,''  ktop='',i2)')            &
!     &   ipktop,ktop
!       call fstop(sname//': increase IPKTOP in include-file')
!      endif

!c     solve radial schroedinger equation, calculate t-matrix
!c     and zl*zlp and zl*jl integrals................
!c     -----------------------------------------------------------------
      call semrel(nrelv,clight,                                         &
     &            lmax,                                                 &
     &            energy,pmom,                                          &
     &            zlab,                                                 &
     &            tmatl,cotdl,almat,                                    &
     &            zlzl,zljl,                                            &
     &            h,jmt,jws,xr,rr,vr,                                   &
     &            rmt_true,rScat,r_circ,ivar_mtz,                       &
     &            fcount,weight,rmag,vj,iVP,                            &
     &            iswzj,                                                &
     &            iprint,istop)
!c     -----------------------------------------------------------------
      if(iprint.ge.6) then
         write(6,'('' grint: cotdl'')')
         write(6,'(i5,2d12.4)') (i,cotdl(i),i=1,lmax+1)
         write(6,'('' grint: tmatl'')')
         write(6,'(i5,2d12.4)') (i,tmatl(i),i=1,lmax+1)
         write(6,'('' grint: zlzl'')')
         write(6,'(i5,2d12.4)') (i,zlzl(i),i=1,lmax+1)
         write(6,'('' grint: zljl'')')
         write(6,'(i5,2d12.4)') (i,zljl(i),i=1,lmax+1)
      endif
!c     ----------------------------------------------------------------
      pzzck(1:kkrsz,1:kkrsz) = 0
      pzjck(1:kkrsz,1:kkrsz) = 0
      pzz(1:kkrsz,1:kkrsz) = 0
      pzj(1:kkrsz,1:kkrsz) = 0
      tab(1:kkrsz,1:kkrsz) = 0
      lm = 0
      do l=0,lmax
       do m=-l,l
        lm = lm+1
        tab(lm,lm)   = tmatl(l+1)
        pzzck(lm,lm) = zlzl(l+1)
        pzjck(lm,lm) = zljl(l+1)
       end do
      end do

!c integrate (rad wf)**2 over interstial region

      if( mtasa .le. 0 ) then
!!       if(lmax.le.3.and.                                                &! kdiag-ktop -- only for L<4
!!     &    (icryst.eq.1                                                  &
!!     &    .or.icryst.eq.2                                               &
!!     &    .or.icryst.eq.4                                               &
!!     &    .or.icryst.eq.5)                                              &
!!     &   ) then
!!!c                MT-case
!!!c        ------------------------------------------------------------
!!         call zzzjxt(icryst,alat,pnrel,                                 &
!!     &               cotdl,min(3,lmax),ktop,zzout,zjout,                &
!!     &               istop)
!!!c        ------------------------------------------------------------
!!!
!!!  converts from ktop storage to matrix (l,l')
!!!
!!         call ktollp1(zzout,pzz,ktop,kkrsz,size(pzz,1),istop)
!!         call ktollp1(zjout,pzj,kdiag,kkrsz,size(pzj,1),istop)
!!       else
!c                ASA
!c        ------------------------------------------------------------
         call zzzjws1(lmax,rScat,rws,pmom,                              &
     &                cotdl,zlzl,zljl,istop)
!c        ------------------------------------------------------------
         lm = 0
         do l=0,lmax
          do m=-l,l
           lm = lm+1
           pzz(lm,lm) = pzz(lm,lm) + zlzl(l+1)
           pzj(lm,lm) = pzj(lm,lm) + zljl(l+1)
          end do
         end do
!!       endif
!c
      endif

      pzz(1:kkrsz,1:kkrsz) = pzz(1:kkrsz,1:kkrsz)+pzzck(1:kkrsz,1:kkrsz)
      pzj(1:kkrsz,1:kkrsz) = pzj(1:kkrsz,1:kkrsz)+pzjck(1:kkrsz,1:kkrsz)

!c     ----------------------------------------------------------------
!c
      if(iprint.ge.6) then
        write(6,'('' Energy '',2d14.6)') energy
!c       -------------------------------------------------------------
        write(6,'(''  grint: pzz'')')
        call wrtmtx(pzz,kkrsz)
!c       -------------------------------------------------------------
        write(6,'(''  grint: pzj'')')
        call wrtmtx(pzj,kkrsz)
!c       -------------------------------------------------------------
        write(6,'(''  grint: pzzck'')')
        call wrtmtx(pzzck,kkrsz)
!c       -------------------------------------------------------------
        write(6,'(''  grint: pzjck'')')
        call wrtmtx(pzjck,kkrsz)
!c       -------------------------------------------------------------
      endif
!c       -------------------------------------------------------------
      if( istop .eq. sname ) then
        call fstop(sname)
      endif
!c       -------------------------------------------------------------
      return
!EOC
      end subroutine grint
!
!
!BOP
!!ROUTINE: zzzjws1
!!INTERFACE:
      subroutine zzzjws1(lmax,rmt,rws,prel,                             &
     &                   ct,zzout,zjout,istop)
!!DESCRIPTION:
! calculates integrals over the wigner-seitz
! radius exterior to the muffin-tin radius of the functions
! z(l,e)**2 and z(l,e)*j(l,e) (i.e. {\tt zzout} and {\tt zjout})

!!DO_NOT_PRINT
      implicit none
!!DO_NOT_PRINT

!!ARGUMENTS:
      integer lmax
      real*8        rmt,rws
      complex*16    prel
      complex*16    ct(0:lmax)
      complex*16    zzout(0:lmax)
      complex*16    zjout(0:lmax)
      character*10  istop
!!REVISION HISTORY:
! Initial Version - DDJ - 1996
! Adapted - A.S. - 2013
!EOP
!
!BOC
      integer ndlgndr
      parameter (ndlgndr=6)
      real*8        xg(ndlgndr)
      real*8        wg(ndlgndr)
      complex*16    zl
      complex*16    jl

      integer ndlmax
      parameter (ndlmax=5)
      complex*16    bj(2*ndlmax+1)
      complex*16    bn(2*ndlmax+1)
      complex*16    dj(2*ndlmax+1)
      complex*16    dn(2*ndlmax+1)
      character*10  sname
      parameter (sname='zzzjws1')

      real*8 pi,atan,c,d,fac,r,rsqwpii

      real*8 one
      parameter (one=1.d0)

      integer ig,l
      integer :: isave = 0
      save xg,wg,isave,pi

!cc
!c      data xg/-.93246951d+00,-.66120939d+00,-.23861919d+00,
!c     >         .23861919d+00, .66120939d+00, .93246951d+00/
!cc
!c      data wg/0.17132449d+00,0.36076157d+00,0.46791393d+00,
!c     >        0.46791393d+00,0.36076157d+00,0.17132449d+00/
!c
!c     ****************************************************************
!c     called by grint
!c     calls besselFun
!c
!c               rmt   muffin-tin radius
!c               rws   wigner-seiz radius
!c               ct(l) (phase shift cotangents)
!c               lmax  maximum orbital angular momentum
!c               kdiag maximum number of diagonal symmetry types (1,2,4,7
!c               istop index of subroutine in which pgrm will stop
!c
!c       output: zzout  (contributions to z*z integrals from outside mt)
!c               zjout  (contributions to z*j integrals from outside mt)
!c
!c       this subroutine calculates the integrals over the wigner-seitz
!c       radius exterior to the muffin-tin radius of the functions
!c       z(l,e)**2 and z(l,e)*j(l,e).
!c     ****************************************************************
!c
!c     -----------------------------------------------------------------

      if(lmax.gt.ndlmax) call fstop(sname//': lmax > ndlmax')

      zzout(0:lmax) = (0.d0,0.d0)
      zjout(0:lmax) = (0.d0,0.d0)

      if(rmt.ge.rws) return

      if(isave.eq.0) then
       call gauleg(-one,one,xg,wg,ndlgndr)
       pi = 4.d0*atan(1.d0)
       isave = 1
      end if

!c     -----------------------------------------------------------------
!c     this needs fixing in scalar relativistic calculation............
!c     -----------------------------------------------------------------

      c   = 0.5d0*(rws-rmt)
      d   = 0.5d0*(rws+rmt)
      fac = c/pi
!c     6-point gaussian integration
      do ig=1,ndlgndr
         r=c*xg(ig)+d
         rsqwpii=fac*wg(ig)*r**2
!c        --------------------------------------------------------------
         call besselFun(lmax+1,prel*r,bj,bn,dj,dn)
!c        --------------------------------------------------------------
!c        calculate wave functions

         do l=0,lmax
            zl=prel*(ct(l)*bj(l+1)-bn(l+1))
            jl=bj(l+1)
            zzout(l)=zzout(l)-rsqwpii*zl*zl
            zjout(l)=zjout(l)-rsqwpii*zl*jl
         enddo

      enddo
!c
!c     ================================================================
      if (istop.eq.sname) then
         call fstop(sname)
      else
         return
      endif
!c
      end subroutine zzzjws1
!
!
!BOP
!!ROUTINE: zzzjxt
!!INTERFACE:
      subroutine zzzjxt(iicryst,alat,prel,                              &
     &                  ct,lmax,ktop,zzout,zjout,                       &
     &                  istop)
!!DESCRIPTION:
! calculates the integrals over the wigner-seitz
! cell exterior to the muffin-tin radius of the functions
! z(l,e)**2 and z(l,e)*j(l,e) (i.e. {\tt zzout} and {\tt zjout});
! the angular parts of these integrals are given by integral
! over solid angles within cell
!
!!USES:
      use universal_const

!!DO_NOT_PRINT
      implicit none
!!DO_NOT_PRINT

!!ARGUMENTS:
      integer iicryst
      real*8  alat
      complex*16    prel
      integer lmax
      complex*16    ct(0:lmax)
      integer ktop
      complex*16    zzout(*)
      complex*16    zjout(*)
      character*10 istop
!!REVISION HISTORY:
! Initial Version - DDJ - 1996
! Adapted - A.S. - 2013
!!REMARKS:
! this implementation works only for fcc and bcc cases
! (iicryst = 1, 2, 4, 5)
!EOP
!
!BOC
      integer        lf1(8)
      integer        lf2(8)
      integer        idg(8)
!c
      real*8        ri(4,2)
      real*8        cavg(3)
      real*8        xg(6)
      real*8        wg(6)
      real*8        coef(8,3)
!c
!c
      complex*16    z(0:lmax)
      complex*16    j(0:lmax)
      complex*16    bj(2*lmax+1)
      complex*16    bn(2*lmax+1)
      complex*16    dj(2*lmax+1)
      complex*16    dn(2*lmax+1)
!c
      data xg/-.93246951d+00,-.66120939d+00,-.23861919d+00,             &
     &         .23861919d+00, .66120939d+00, .93246951d+00/
!c
      data wg/.17132449d+00,.36076157d+00,.46791393d+00,                &
     &        .46791393d+00,.36076157d+00,.17132449d+00/
!c
      data lf1/0,1,2,2,3,3,3,3/
      data lf2/0,1,2,2,3,3,3,1/
      data idg/1,3,3,2,3,3,1,3/
!c
      data ri/.3535533906d+00,.4082482905d+00,.4330127019d+00,          &
     &        .5000000000d+00,.4330127019d+00,.5000000000d+00,          &
     &        .5303300859d+00,.5590169944d+00/
!c
      data coef/1.0d+00,3.0d+00,  7.5d+00, -2.5d+00,  13.125d+00,       &
     &          -6.125d+00,  0.0d+00,  6.873863542d+00,                 &
     &          0.0d+00,0.0d+00,-22.5d+00, 22.5d+00, -39.375d+00,       &
     &          39.375d+00,  0.0d+00,-34.369317710d+00,                 &
     &          0.0d+00,0.0d+00,  0.0d+00,  0.0d+00,-236.250d+00,       &
     &          131.250d+00,105.d+00,  0.0d+00         /
!c
      character*10 sname
      data sname/'zzzjxt'/
      integer icryst,ir,ig,l,index,icavg,l1,l2
      real*8 c,d,r,rsqwpii
!     ,pi,one
!      parameter (one=1.d0)
!c
!c     ****************************************************************
!c     called by grint
!c     calls besselFun,intrst
!c
!c       input:  icryst (crystal type, 1=fcc, 2=bcc)
!c               alat   (lattice parameter)
!c               ct(l)  (phase shift cotangents)
!c               lmax   maximum orbital angular momentum
!c               ktop   maximum number of symmetry types (1,2,4,8)
!c               istop  index of subroutine in which pgrm will stop
!c
!c       output: zzout  (contributions to z*z integrals from outside mt)
!c               zjout  (contributions to z*j integrals from outside mt)
!c
!c       this subroutine calculates the integrals over the wigner-seitz
!c       cell exterior to the muffin-tin radius of the functions
!c       z(l,e)**2 and z(l,e)*j(l,e).  the angular parts of these
!c       integrals i(lambda,lambda',r) are given by:
!c       i(lambda,lambda',r)=integral over solid angles within cell
!c       of sum over mu of k(lambda,mu)*k(lambda',mu).                  ( !?)
!c
!c
!c       index   lambda  lambda'  l   l'         i
!c         1       1       1      0   0         <1>
!c         2       2       2      1   1        3<1>/3
!c         3       3       3      2   2    (15/2)[<1>-3<z4>]/3
!c         4       4       4      2   2    (15/6)[9<z4>-<1>]/2
!c         5       5       5      3   3  (105/8)[<1>-3<z4>-18<x2y2z2>]/3
!c         6       6       6      3   3 (7/24)[450<x2y2z2>+135<z4>-21<1>] !/3
!c         7       7       7      3   3       105<x2y2z2>/1
!c         8       2       6      1   3   (sqrt(21)/4)[3<1>-15<z4>]/3
!c
!c     the averages <1>, <z4>, and <x2y2z2> depend on r and are calculate !d
!c     by subroutine intrst.  the coeffecients of the above table are sto !red
!c     in array coef.
!c     ****************************************************************
!c

!      pi = 4*atan(one)
      if(lmax.gt.3) then
       write(*,*) sname//': *** WARNING ***'
       write(*,*) ' This subroutine was not tested for LMAX > 3'
      end if
      icryst=iicryst
      if( iicryst .eq. 4 ) then
         icryst=2
      elseif( iicryst .eq. 5 ) then
         icryst=1
      endif
!c     set zzout and zjout arrays to zero
!c     ----------------------------------------------------------------
      call zerooutC(zzout,ktop)
!c     ----------------------------------------------------------------
      call zerooutC(zjout,ktop)
!c     ----------------------------------------------------------------
!c     this needs fixing in scalar relativistic calculation............
!c     break integral up into three regions
      do ir=1,3
         c= 0.5d+00*alat*(ri(ir+1,icryst)-ri(ir,icryst))
         d=-0.5d+00*alat*(ri(ir+1,icryst)+ri(ir,icryst))
!c        6-point gaussian integration over each region
         do ig=1,6
            r=c*xg(ig)-d
            rsqwpii=c*wg(ig)*r**2/pi
!c           ----------------------------------------------------------
            call besselFun(lmax+1,prel*r,bj,bn,dj,dn)
!c           ----------------------------------------------------------
!c           calculate wave functions
            do l=0,lmax
               z(l)=prel*(ct(l)*bj(l+1)-bn(l+1))
               j(l)=bj(l+1)
            enddo
!c           ----------------------------------------------------------
            call intrst(icryst,r/alat,cavg)
!c           ----------------------------------------------------------
!c           loop over  irr. representations
            do index=1,ktop
               l1=lf1(index)
               l2=lf2(index)
!c              loop over cellular solid angle averages
               do icavg=1,3
                  zzout(index)=zzout(index)-rsqwpii*cavg(icavg)*        &
     &                         z(l1)*z(l2)*coef(index,icavg)/idg(index)
                  zjout(index)=zjout(index)-rsqwpii*cavg(icavg)*        &
     &                         z(l1)*j(l1)*coef(index,icavg)/idg(index)
               enddo
            enddo
         enddo
      enddo
!c
!c     ================================================================
      if (istop.eq.sname) then
         call fstop(sname)
      else
         return
      endif
!c
      end subroutine zzzjxt
!
!c     ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
!BOP
!!ROUTINE: semrel
!!INTERFACE:
      subroutine semrel(nrelv,clight,                                   &
     &                  lmax,                                           &
     &                  energy,pmom,                                    &
     &                  g,                                              &
     &                  tmatl,cotdl,almat,                              &
     &                  zlzl,zljl,                                      &
     &                  h,jmt,jws,x,r,rv,                               &
     &                  rmt_true,rScat,r_circ,ivar_mtz,                 &
     &                  fcount,weight,rmag,vj,lVP,                      &
     &                  iswzj,                                          &
     &                  iprint,istop)
!!DESCRIPTION:
!   this subroutine solves scalar relativistic equations for complex
!   energies using Calogero's phase method and gets various phase shift
!   related values, radial wavefunctions and their integrals over ASA
!   spheres and, optionally, VP
!   (single-scattering problem)  \\
!....based on ddj \& fjp version --- july 1991
!
!!USES:
      use mecca_constants
      use isoparintgr_interface
      use sctrng, only : scalar
!
!!REVISION HISTORY:
! Adapted - A.S. - 2013
!!REMARKS:
! old non-module version
!EOP
!
!BOC
!      use mecca_interface, only : scalar
      implicit real*8 (a-h,o-z)
!c
      character sname*10
      character(:), allocatable :: coerr
      character(10) :: istop
!c
      integer fcount
      integer ia

      real(8) :: rmt_true,xrmt
      real(8), intent(in) :: rScat
      real(8) :: r_circ
      real(8) :: weight(:)  ! (MNqp)
      real(8) :: rmag(:,:,:,:)  ! (MNqp,MNqp,MNqp,MNF)
      real(8) :: vj(:,:,:,:)  ! (MNqp,MNqp,MNqp,MNF)
      logical, intent(in) :: lVP

      real(8) :: x(:)
      real(8) :: rv(:)
      real(8) :: r(:)
      real(8) :: r2(size(r))
      real(8) :: h
      real(8) :: clight
!c
      complex(8) :: almat(lmax+1),alpha
      complex(8) :: bjl(lmax+2,size(r))
      complex(8) :: bnl(lmax+2,size(r))
      complex(8) :: zlzl(lmax+1)
      complex(8) :: zljl(lmax+1)
      complex(8) :: g(:,:)  ! (iprpts,lmax+1)
      complex(8) :: f(size(r))
      complex(8) :: gpl(size(r))
      complex(8) :: fpl(size(r))
      complex(8) :: cn1(size(r))
      complex(8) :: dcn1(size(r))
      complex(8) :: cj1(size(r))
      complex(8) :: dcj1(size(r))
      complex(8) :: cotdl(lmax+1)
      complex(8) :: tmatl(lmax+1)
      complex(8) :: energy
      complex(8) :: pmom
      complex(8) :: prel
      complex(8) :: pr
      complex(8) :: cotdel
      complex(8) :: tandel
      complex(8) :: anorm
      complex(8) :: VPInt,dpsi_MT
      complex(8), external :: ylag_cmplx
!c parameter
!      complex*16 cone
      complex*16 sqrtm1

!c
!c     +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!      parameter (cone=(1.d0,0.d0))
      parameter (sqrtm1=(0.d0,1.d0))
      parameter (sname='semrel')
!c     +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!c
!c     *****************************************************************
!c     Gets radial integrals, t-matrix.
!c     If iswzj =1, then also gets the z*j interals............
!c
!c     In this version rv is r * v
!c
!c     called by: grint
!c     calls:     bessel,cqexp,cqxpup,csimp,zeroout
!c
!c      requires:
!c            lmax        lmax
!c            energy      energy
!c            iswzj       switch: if iswzj.ne.0 gets z*j integrals.

!c            istop       switch: if istop=21 stop at end of this routine
!c            h,x,r       grid info (step for log-grid, log grid, r-grid)
!c
!c      returns:
!c            cotdl       phase shift cotangent
!c            tmatl       t-matrix
!c            zlzl        integral of (rad wf)**2 over M.T.
!c            zljl        integral of (rad wf * ir rad wf) over M.T.
!c
!c     set c for doing semi-relativistic or non-relativistic calc.
!c     .....c = speed of light in proper units..........
!c     Ryd units: m=1/2  hbar=1 c=2*inverse of fine structure const.
!c
!c     Solving a relativistic (nrelv=0) or non-relativistic (nrelv=10)
!c     problem.   If non-relativistic, then set e=e-elmass equal to e
!c     (where elmass = electron mass) and 1/c is set to ~zero to make
!c     relativistic terms zero.
!c     NOTE:  c is set to c*10**nrelv to make code identical to the
!c            fully relativistic code..............................
!c
!c     Using Ricatti Bessel fcts. Need only l=0 and l=1 to get wave-fcts.
!c
!c     To get physical phase-shifts and normalization, need get Rl in
!c     terms of spherical bessel for all l's at the boundary of sphere.
!c     This is properly done in SCALAR, see clmt and slmt.
!c
!c     With M(r)= [1 + (e-v(r))*c2inv]
!c     Obtain r*(wave-fct.):   regular     g=r*R and   f=r*R'/M
!c                           irregular   gpl=r*R and fpl=r*R'/M
!c
!c     NOTE: ====>   need to get gderiv for mom. matrix elements
!c
!c     *****************************************************************
!c

!CAB      prel=sqrt( energy*(cone+energy/clight**2) )
!CAB??
!CAB??       What about sqrt(1+energy/clight^2) , when energy is complex  !????
!CAB??

!      zed = -(rv(1) + (rv(1)-rv(2))*r(1)/(r(2)-r(1)))
!      ized = nint(0.5d0*zed)
      ized = g_zed(rv,r)
!c     =================================================================
!DELETE      if(ized.lt.1 .and. nrelv.le.0) then
!DELETE        prel = pnrel
!DELETE        nrel = 1
!DELETE      else
!c
!c Phaseshifts and wavefcts are calculated with relativsitic mom. -- prel
!c Whereas, because t_inv= i*Kappa + Kappa*cotdel, i*Kappa term must
!c cancel i*Kappa piece of Non-Rel g(k). Therefore, Kappa=sqrt(e) -- pnre !l.
!c
!        prel = pnrel*sqrt(cone+energy/clight**2)
        prel = pmom
        nrel = nrelv
!DELETE      endif

!c     for muffin-tins incrs and jmt should be the same
!CAB      iend=jmt+11               ! this line is used in DDJ code
!CAB

      if (jmt.gt.jws) then
       write(6,'(a10,'': jmt,jws='',2i6)') sname,jmt,jws
       call fstop(sname//': JMT > JWS ???')
      else
!DEBUG        if( lVP .and. ivar_mtz.ge.2 ) then
!DEBUG!C          for VP Integration
           iend=min(size(r),size(rv))
!DEBUG        else
!DEBUG           iend = min(size(r),size(rv),jmt+max(11,(jws-jmt)/2*2+1))
!DEBUG        end if
      end if

      if( iend .lt. jws ) then
        coerr=' iend less than jws'
        write(6,'('' semrel: jmt,jws,iend='',3i5)') jmt,jws,iend
        write(6,'(2(2x,a))')coerr,sname
        call fstop(sname)
      endif

!DELETE      if( iend .gt. size(r) ) then
!DELETE        coerr=' iend greater than array size'
!DELETE        write(6,'('' semrel: iend,size(r)='',3i5)') iend,size(r)
!DELETE        write(6,'(2(2x,a))')coerr,sname
!DELETE        call fstop(sname)
!DELETE      endif

!c     =================================================================
!c
!c Using Ricatti Bessel fcts. Need only l=0 and l=1 to get wave-fcts.
!c -------------------------------------------------------------------
!c To get physical phase-shifts and normalization, need get Rl in
!c terms of spherical bessel for all l at the boundary of sphere.
!c This is properly done in SCALAR, see clmt and slmt.
!c -------------------------------------------------------------------
      do j=1,iend
          r2(j)=r(j)*r(j)
          pr=prel*r(j)
          bjl(1,j)= sin(pr)
          bnl(1,j)=-cos(pr)
          bjl(2,j)= bnl(1,j) + bjl(1,j)/pr
          bnl(2,j)=-bjl(1,j) + bnl(1,j)/pr
      enddo
!c     =================================================================
!c
!c solve scalar-relativistic or non-relativistic equation.
!c
!c ------------------Major loop over l's-------------------------------

       do l=0,lmax
!c
!c -------------------------------------------------------------------
!c With M(r)= [1 + (e-v(r))*c2inv]
!c Obtain r*(wave-fct.):   regular     g=r*R and   f=r*R'/M
!c                       irregular   gpl=r*R and fpl=r*R'/M
!c -------------------------------------------------------------------

!DELETE         call scalar(nrel,clight,                                       &
!DELETE     &               l,                                                 &
!DELETE     &               bjl,bnl,g(:,l+1),f,gpl,fpl,                        &
!DELETE     &               tandel,cotdel,anorm,alpha,                         &
!DELETE     &               energy,prel,                                       &
!DELETE     &               rv,r,r2,h,jmt,iend,                                &
!DELETE     &               iswzj,                                             &
!DELETE     &               iprint,istop)
!c        ------------------------------------------------------------
         anorm = cone
         call scalar(nrel,clight,                                       &
     &               ized,                                              &
     &               l,                                                 &
     &               g(:,l+1),f,gpl,fpl,                                &
     &               tandel,cotdel,alpha,                               &
     &               energy,prel,                                       &
     &               rv,r,rScat,iend,                                   &
     &               iswzj,.true.,                                      &
     &               iprint)
!c        ==============================================================
!c        store cotangent of phase-shift, normalization and t-matrix

         cotdl(l+1)=cotdel
         almat(l+1)=alpha

!c store single-site t-matrix
!c  Must use pnrel here (not prel) to cancel pole in struc. const.
!c  Actually, because of anorm (in SCALAR) this is wrong
!c  one way or the other. If prel, then normalized correctly;
!c  if pnrel, then does not cancel pole correctly. CHECK!!!!

!c
          tmatl(l+1)=cone/( pmom*( sqrtm1- cotdel ) )
          ! debug warning
!c         tmatl(l+1)=cone/( prel*( sqrtm1- cotdel ) )

!c    ==================================================================
!c    To calculate the wave function integrals over ASA sphere
!c      OR over VP-boundary (if ivar_matz >= 2 .and. lVP)
!c    ==================================================================
        if( lVP .and. ivar_mtz.ge.2 ) then
         ! For Ordered systems only AND Full VP+Variational MT-zero (VP)
!c       ==============================================================
             xrmt = log(rmt_true)
             nlag=3
!C***********************************************************************
!c       integrals for zl*zl and zl*zj: with factor of r2 included here
!C***********************************************************************
             do j=1,iend
                 cn1(j)= g(j,l+1)*g(j,l+1)
             enddo
             call cqxpup(1,cn1,jws,x,dcn1)
             dpsi_MT=ylag_cmplx(xrmt,x,dcn1,0,nlag,jmt,iex)

                ia=2
             call isopar_chbint_cmplx(ia,rmt_true,iend,r,cn1,           &
     &                        r_circ,fcount,weight,rmag,vj,VPInt)
!             call isopar_integ_cmplx(ia,rmt_true,r,cn1,                 &
!     &                         r_circ,fcount,weight,                    &
!     &                         rmag,vj,VPInt)

             zlzl(l+1) = -(dpsi_MT + VPInt)*anorm**2/pi
!Cc           =========================================================== !=
!Cc            get zj integrals if needed................................ !...
             if(iswzj.eq.1) then
               do j=1,iend
                  cj1(j)= g(j,l+1)*gpl(j)
               enddo
!C
               call cqxpup(1,cj1,jws,x,dcj1)
               dpsi_MT=ylag_cmplx(xrmt,x,dcj1,0,nlag,jmt,iex)
!C
                ia=2
                call isopar_chbint_cmplx(ia,rmt_true,iend,r,cj1,        &
     &                        r_circ,fcount,weight,rmag,vj,VPInt)
!               call isopar_integ_cmplx(ia,rmt_true,r,cj1,              &
!    &                            r_circ,fcount,weight,                 &
!    &                            rmag,vj,VPInt)

                zljl(l+1) = (dpsi_MT + VPInt)*anorm/pi
             endif
!Cc      ==============================================================
        else
           ! For Disord systems only OR Variational MT-zero (ivar_mtz=0,1,2)
!Cc      ==============================================================
!Cc        integrals for zl*zl and zl*zj: with factor of r2 included here
!suff .. no factor of 4*pi since angular integrations are taken to
!     .. come out to unity for l = l'

            do j=1,iend
                cn1(j)= g(j,l+1)*g(j,l+1)
            enddo
            call cqxpup(1,cn1,jws,x,dcn1)
            zlzl(l+1)=-dcn1(jmt)*anorm**2/pi

!Cc           =========================================================== !==
!Cc           get zj integrals if needed................................. !..
            if(iswzj.eq.1) then
               do j=1,iend
                  cj1(j)= g(j,l+1)*gpl(j)
               enddo
               call cqxpup(1,cj1,jws,x,dcj1)
               zljl(l+1)= dcj1(jmt)*anorm/pi
            endif
!Cc      ==============================================================
        endif

        ! note: following aftab's bloch spec. code, the regular fns
        ! are stored for the (occasional) case when we need to calc.
        ! integrals of form int(zlzl) for _different_ components on
        ! the same site .. and recall: g(r) = r Z(r)

         g(:,l+1) = anorm*g(:,l+1)

!c     ================================================================
       enddo
!c     ================================================================
      if (istop.eq.sname) then
        call fstop(sname)
      else
         return
      endif
!c
      end subroutine semrel
