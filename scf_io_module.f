!BOP
!!MODULE: scf_io
!!INTERFACE:
      module scf_io
!!DESCRIPTION:
! input / output procedures
!
!!USES:
      use universal_const
      use mecca_types
      use input, only : saveInput,def_tag,def_name
      use mecca_interface
      use gfncts_interface
      use mecca_run, only : getMS

!!DO_NOT_PRINT
      implicit none
      private
!!DO_NOT_PRINT

!!PUBLIC MEMBER FUNCTIONS:
      public :: readpot, writepot, check_version,readPotential
      public :: saveSCF,save_vrho,save_inifile
      public :: sCurrParams
      public :: cpscflog,cp_from_potfile
!!PRIVATE MEMBER FUNCTIONS:
! subroutine check_version
! subroutine readpot
! subroutine writepot
! subroutine cp_to_potfile
! subroutine cp_from_potfile
! subroutine saveSCF
! subroutine save_inifile
! subroutine save_vrho
! subroutine sCurrParams
! subroutine save_scflog
! subroutine readPotential
! subroutine scflog
! subroutine cpscflog

!!PUBLIC DATA MEMBERS:
      public :: pot_S_mix,pot_B_mix,rho_S_mix,rho_B_mix
      public :: c_nm,c_dlm,c_up,c_dn
!!PUBLIC TYPES:
      type, public :: Potfile
       character(len=80) :: comment
       real(8) ::  ztot=0
       real(8) ::  alat=0
       real(8) ::  zval=0
       real(8) ::  efpot=0
       real(8) ::  xst=0
       real(8) ::  xmt=0
       integer ::  jmt=0
       integer :: ndrpts=0
       real(8), allocatable ::  vr(:)    ! vr(i) is V(r(i)) (i.e. not V*r) and
                                         ! does not include point-charge (ztot) potential;
                                         ! r(i) = exp(x(i)), x(i) = xst+i*h, h=(xmt-xst)/jmt;
!
       real(8) ::  v0=0                  ! "average" potential" in the interstitial region
       real(8) ::  qvalws=0
       integer ::  ndchg=0
       real(8), allocatable ::  chgtot(:)
       integer ::  ndcor=0
       real(8), allocatable ::  chgcor(:)
       integer :: numc=0
       integer, allocatable :: nc(:)
       integer, allocatable :: lc(:)
       integer, allocatable :: kc(:)
       real(8), allocatable :: ec(:)     !  it is assumed that ec=core_eigen_value-v0
       character(4), allocatable :: lj(:)
      end type Potfile

!!REVISION HISTORY:
! Initial Version - A.S. - 2013
!EOP
!
!BOC
      integer STT
      character(200) :: MSG

      integer, parameter :: vrho_vers=1
      character(1), parameter :: ch_vers='v'

      integer, parameter :: pot_S_mix=-1,pot_B_mix=-2
      integer, parameter :: rho_S_mix=0,rho_B_mix=1
      character(2),parameter :: c_nm='NM',c_dlm='DM',c_up='up',c_dn='dn'
      integer, save :: imix_curr=0
      real(8), save :: alpha_curr=-1,beta_curr=-1
      character(120) :: xc_descr_curr='unknown'
!c     ***************************************************************
      character(120), parameter :: keyline =                            &
     &          '  i      Free E (Ry)    P(Mbars)    maxerr'//          &
     &          '        ef         vmtz   vdif     Magn     m'
!c     ***************************************************************

!EOC
      contains

!BOP
!!IROUTINE: check_version
!!INTERFACE:
      subroutine check_version(text,vers)
!!DESCRIPTION:
! checks potential file format version
!
!!REVISION HISTORY:
! Initial Version - A.S. - 2013
!EOP
!
!BOC
      implicit none
      character*(*) text
      integer vers
      character(1) :: ch
      vers = -1
      read(text,'(a1,i1)',end=10,err=10) ch,vers
      if ( ch == ch_vers ) then
        if ( vers .ne. vrho_vers ) then
          write(6,'(a,i1,a)')                                           &
     &                       ' Potential file version ',vers,           &
     &                       ' is unknown, input potential is ignored'
         vers = 0
        end if
      end if
10    return
!EOC
      end subroutine check_version
!
!BOP
!!IROUTINE: readpot
!!INTERFACE:
      subroutine readpot(nfch,siteptn,endflg)
!!DESCRIPTION:
! reads potential file in {\tt siteptn (type Potfile)}

!!DO_NOT_PRINT
      implicit none
!!DO_NOT_PRINT

!!ARGUMENTS:
      integer, intent(in) :: nfch   ! fortran unit number for reading
      type(Potfile), intent(out) :: siteptn
      logical, intent(out) :: endflg
!!REVISION HISTORY:
! Initial Version - A.S. - 2013
!EOP
!
!BOC
      integer :: ndrpts,nd,j,vers

!      character(:), parameter :: sname='readpot'

!c     ==========================================================

      endflg = .false.
      read(nfch,'(a)',end=100) siteptn%comment

      call check_version(siteptn%comment,vers)
      if ( vers > 0 ) then
      read(nfch,*,end=100,err=100)                                      &
     &  siteptn%ztot,                                                   &
     &  siteptn%alat,                                                   &
     &  siteptn%zval,                                                   & ! (in fact, zcor is here)
     &  siteptn%efpot
      siteptn%zval = siteptn%ztot-siteptn%zval

!c     ==========================================================
!c     read in potential deck....................................

      read(nfch,*,end=100,err=100)                                      &
     &  siteptn%xst,                                                    &
     &  siteptn%xmt,                                                    &
     &  siteptn%jmt

      read(nfch,*,end=100,err=100) ndrpts
      siteptn%ndrpts = ndrpts

      if ( siteptn%ndrpts > 0 ) then
       if ( allocated(siteptn%vr) ) then
         if ( size(siteptn%vr)<ndrpts ) deallocate(siteptn%vr)
       end if
       if ( .not.allocated(siteptn%vr) )                                &
     &   allocate( siteptn%vr(1:ndrpts) )
       siteptn%vr = 0
       read(nfch,'(4e20.13)',end=100,err=100) siteptn%vr(1:ndrpts)
      endif

      read(nfch,*,end=100,err=100) siteptn%v0

!c     ==========================================================
!c     read in the charge density ...............................

      read(nfch,'(i5,e20.13)')                                          &
     & nd,                                                              &
     & siteptn%qvalws

      nd = min(nd,ndrpts)
      siteptn%ndchg = nd
      if(nd > 0) then
       if ( allocated(siteptn%chgtot) ) then
        if ( size(siteptn%chgtot)<nd ) deallocate(siteptn%chgtot)
       end if
       if ( .not.allocated(siteptn%chgtot) )                            &
     &   allocate( siteptn%chgtot(1:nd) )
       siteptn%ndchg = nd
       siteptn%chgtot = 0
       read(nfch,'(4e20.13)') siteptn%chgtot(1:siteptn%ndchg)
      endif

!c     ==========================================================
!c     read in core state definition ............................

      read(nfch,*,err=100,end=100)                                      &
     & siteptn%numc

      if(siteptn%numc > 0) then

       if ( allocated(siteptn%nc) ) deallocate(siteptn%nc)
       if ( allocated(siteptn%lc) ) deallocate(siteptn%lc)
       if ( allocated(siteptn%kc) ) deallocate(siteptn%kc)
       if ( allocated(siteptn%ec) ) deallocate(siteptn%ec)
       if ( allocated(siteptn%lj) ) deallocate(siteptn%lj)
       allocate(siteptn%nc(siteptn%numc))
       allocate(siteptn%lc(siteptn%numc))
       allocate(siteptn%kc(siteptn%numc))
       allocate(siteptn%ec(siteptn%numc))
       allocate(siteptn%lj(siteptn%numc))
       do j=1,siteptn%numc
       read(nfch,'(3i5,f16.8,8x,a)')                                    &
     &    siteptn%nc(j),                                                &
     &    siteptn%lc(j),                                                &
     &    siteptn%kc(j),                                                &
     &    siteptn%ec(j),                                                &
     &    siteptn%lj(j)
       enddo
!c     ==========================================================
!c     read in the core density cccccccccccccccccccccccc

       read(nfch,*)                                                     &
     & nd

       nd = min(nd,ndrpts)
       siteptn%ndcor = nd
       if(nd > 0) then
        if ( allocated(siteptn%chgcor) ) then
         if ( size(siteptn%chgcor)<nd ) deallocate(siteptn%chgcor)
        end if
        if ( .not.allocated(siteptn%chgcor) )                           &
     &   allocate( siteptn%chgcor(1:nd) )
        siteptn%ndcor = nd
        siteptn%chgcor = 0
        read(nfch,'(4e20.13)') siteptn%chgcor(1:siteptn%ndcor)
       endif

      endif

      endif  ! vers

!c     ==========================================================

      return

100   continue
      endflg = .true.

      return
!EOC
      end subroutine readpot

!BOP
!!IROUTINE: writepot
!!INTERFACE:
      subroutine writepot(nfch,siteptn)
!!DESCRIPTION:
! saves {\tt siteptn (type Potfile)} on disk

!!DO_NOT_PRINT
      implicit none
!!DO_NOT_PRINT

!!ARGUMENTS:
      integer, intent(in) :: nfch  !! fortran unit number for writing
      type(Potfile), intent(in) :: siteptn
!!REVISION HISTORY:
! Initial Version - A.S. - 2013
!EOP
!
!BOC
!      logical endflg

      integer j

!      character(:), parameter :: sname='writepot'

!c ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

!c     ==========================================================

      write(nfch,'(a)') trim(siteptn%comment)

!      endflg = .false.

      write(nfch,'(f5.0,17x,f12.5,1x,f5.0,1x,es20.13)')                 &
     &  siteptn%ztot,                                                   &
     &  siteptn%alat,                                                   &
     &  siteptn%ztot-siteptn%zval,                                      &
     &  siteptn%efpot

!c     ==========================================================
!c     write in potential deck....................................

      write(nfch,'(17x,es24.16,es24.16,1x,i5)')                         &
     &  siteptn%xst,                                                    &
     &  siteptn%xmt,                                                    &
     &  siteptn%jmt

      write(nfch,'(i5)') siteptn%ndrpts

      if ( siteptn%ndrpts > 0 ) then
       write(nfch,'(4es20.13)')    (siteptn%vr(j),j=1,siteptn%ndrpts)
      endif

      write(nfch,'(35x,es20.13)') siteptn%v0

!c     ==========================================================
!c     write in the charge density ...............................

      write(nfch,'(i5,es20.13)')                                        &
     & siteptn%ndchg,                                                   &
     & siteptn%qvalws

      if(siteptn%ndchg.gt.0) then
       write(nfch,'(4es20.13)')                                         &
     &  (siteptn%chgtot(j),j=1,siteptn%ndchg)
      endif

!c     ==========================================================
!c     write in core state definition ............................

      write(nfch,'(i5)') siteptn%numc

      if(siteptn%numc.gt.0) then
       do j=1,siteptn%numc
       write(nfch,'(3i5,f16.8,8x,a)')                                   &
     &    siteptn%nc(j),                                                &
     &    siteptn%lc(j),                                                &
     &    siteptn%kc(j),                                                &
     &    siteptn%ec(j),                                                &
     &    siteptn%lj(j)
       enddo
!c     ==========================================================
!c     write in the core density cccccccccccccccccccccccc
       write(nfch,'(i5)') siteptn%ndcor
       if(siteptn%ndcor.gt.0) then
        write(nfch,'(4es20.13)') (siteptn%chgcor(j),j=1,siteptn%ndcor)
       endif
      endif

!c     ==========================================================

      return
!EOC
      end subroutine writepot

!BOP
!!IROUTINE: cp_to_potfile
!!INTERFACE:
      subroutine cp_to_potfile(alat,ztot,zval,ef,is,comment,v_rho_box,  &
     &                                                          siteptn)
!!DESCRIPTION:
! initialize
! {\tt siteptn (type Potfile)}
!  from
! {\tt v\_rho\_box (type SphAtomDeps)}
!
!!ARGUMENTS:
      real(8), intent(in) :: alat,ztot,zval,ef
      integer, intent(in) :: is
      character(*), intent(in) :: comment
      type(SphAtomDeps), intent(in) :: v_rho_box
      type(Potfile), intent(out) :: siteptn
!!REVISION HISTORY:
! Initial Version - A.S. - 2013
!EOP
!
!BOC
      real(8) :: h
      integer i,indx
      real(8), allocatable :: xr(:),rr(:),vr_nucl(:)

       siteptn%comment = comment

       siteptn%ztot=ztot
       siteptn%alat=alat
       siteptn%zval=zval
       siteptn%efpot=ef

       siteptn%xst=v_rho_box%xst
       siteptn%xmt=v_rho_box%xmt
       siteptn%jmt=v_rho_box%jmt
       siteptn%ndrpts=v_rho_box%ndrpts
       siteptn%v0=v_rho_box%v0(is)
       if ( siteptn%ndrpts > 0 ) then
        if ( allocated(siteptn%vr) ) deallocate(siteptn%vr)
        allocate(siteptn%vr(1:siteptn%ndrpts),vr_nucl(1:siteptn%ndrpts))
        allocate(xr(siteptn%ndrpts),rr(siteptn%ndrpts))
        call g_meshv(v_rho_box,h,xr,rr,is,siteptn%vr)
        call twoZ(ztot,rr,siteptn%ndrpts,vr_nucl)
        siteptn%vr(:) = ( siteptn%vr(:) - vr_nucl(:) ) / rr(:)   ! siteptn%vr is V (not V*r) and
                                                                 ! does not include nuclear (ztot) potential
        siteptn%vr(:) = siteptn%vr(:) + siteptn%v0
        deallocate(xr,rr,vr_nucl)
       end if

       siteptn%qvalws=v_rho_box%ztot_T(is)-v_rho_box%zcor_T(is)

       siteptn%ndchg=v_rho_box%ndchg
       if ( siteptn%ndchg > 0 ) then
        if ( allocated(siteptn%chgtot) ) deallocate(siteptn%chgtot)
        allocate(siteptn%chgtot(1:siteptn%ndchg))
        siteptn%chgtot(:) = v_rho_box%chgtot(1:v_rho_box%ndchg,is)
       end if

       siteptn%ndcor=v_rho_box%ndcor
       if ( siteptn%ndcor > 0 ) then
        if ( allocated(siteptn%chgcor) ) deallocate(siteptn%chgcor)
        allocate(siteptn%chgcor(1:siteptn%ndcor))
        siteptn%chgcor(:) = v_rho_box%chgcor(1:v_rho_box%ndcor,is)
       end if

       siteptn%numc=v_rho_box%numc(is)
       if ( siteptn%numc > 0 ) then
        if ( allocated(siteptn%nc) ) deallocate(siteptn%nc)
        allocate(siteptn%nc(1:siteptn%numc))
        if ( allocated(siteptn%lc) ) deallocate(siteptn%lc)
        allocate(siteptn%lc(1:siteptn%numc))
        if ( allocated(siteptn%kc) ) deallocate(siteptn%kc)
        allocate(siteptn%kc(1:siteptn%numc))
        if ( allocated(siteptn%ec) ) deallocate(siteptn%ec)
        allocate(siteptn%ec(1:siteptn%numc))
        if ( allocated(siteptn%lj) ) deallocate(siteptn%lj)
        allocate(siteptn%lj(1:siteptn%numc))
        siteptn%nc(:)=v_rho_box%nc(1:siteptn%numc,is)
        siteptn%lc(:)=v_rho_box%lc(1:siteptn%numc,is)
        siteptn%kc(:)=v_rho_box%kc(1:siteptn%numc,is)
        siteptn%ec(:)=v_rho_box%ec(1:siteptn%numc,is)
        do i=1,siteptn%numc
         indx = siteptn%lc(i) + iabs(siteptn%kc(i))
         if ( indx>0 .and. indx<=size(lj) ) then
          siteptn%lj(i)=lj(indx)
         else
          siteptn%lj(i)='***'
         end if
         if ( siteptn%ec(i) < 0 )  then
           siteptn%ec(i)= siteptn%ec(i)+siteptn%v0
         end if
        end do
       end if

      return
!EOC
      end subroutine cp_to_potfile

!BOP
!!IROUTINE: cp_from_potfile
!!INTERFACE:
      subroutine cp_from_potfile(siteptn,v_rho_box,is,field)
!!DESCRIPTION:
! initialize
! {\tt v\_rho\_box (type SphAtomDeps)}
!  from
! {\tt siteptn (type Potfile)}
!
!!ARGUMENTS:
      type(Potfile), intent(in) :: siteptn
      type(SphAtomDeps), intent(inout) :: v_rho_box
      integer, intent(in) :: is
      character(*), intent(in) :: field
!!REVISION HISTORY:
! Initial Version - A.S. - 2013
!EOP
!
!BOC
      real(8), allocatable :: xr_in(:),xr_out(:)
      real(8), allocatable :: rr_in(:),rr_out(:)
      integer :: i,iex
      real(8) :: h_in,h_out,v
      real(8), external :: ylag
      allocate(xr_in(siteptn%ndrpts),rr_in(siteptn%ndrpts))
      h_in = (siteptn%xmt-siteptn%xst)/(siteptn%jmt-1)
      forall (i=1:size(xr_in)) xr_in(i) = siteptn%xst + (i-1)*h_in
      rr_in = exp(xr_in)
      allocate(xr_out(v_rho_box%ndrpts),rr_out(v_rho_box%ndrpts))
      call g_meshv(v_rho_box,h_out,xr_out,rr_out)
      if ( field == 'chgcor' ) then
        do i=1,v_rho_box%ndcor
         v_rho_box%chgcor(i,is) = ylag(rr_out(i),rr_in,siteptn%chgcor,  &
     &                                        0,3,siteptn%ndcor,iex)
        end do
        v_rho_box%chgcor(v_rho_box%ndcor+1:,is) = zero
      else if ( field == 'chgtot' ) then  ! NOT TESTED
        do i=1,v_rho_box%ndchg
         v_rho_box%chgtot(i,is) = ylag(rr_out(i),rr_in,siteptn%chgtot,  &
     &                                        0,3,siteptn%ndchg,iex)
        end do
        v_rho_box%chgtot(v_rho_box%ndchg+1:,is) = zero
      else if ( field == 'vr' ) then  ! NOT TESTED
        do i=1,v_rho_box%ndrpts
         v = ylag(rr_out(i),rr_in,siteptn%vr,0,3,siteptn%ndrpts,iex)
         v_rho_box%vr(i,is) = v*rr_out(i) - 2*siteptn%ztot   ! it is assumed that ztot in siteptn is the same as in v_rho_box
                                                          ! if V0 /= 0 in v_rho_box, it should be taken into account outside
        end do
        v_rho_box%vr(v_rho_box%ndrpts+1:,is) = zero
      end if
      deallocate(xr_in,rr_in)
      deallocate(xr_out,rr_out)
      return
!EOC
      end subroutine cp_from_potfile
!
!BOP
!!IROUTINE: saveSCF
!!INTERFACE:
      subroutine saveSCF( mecca_state )
!!DESCRIPTION:
! saves run state {\tt mecca\_state (type RunState)} on disk
!
!!USES:
      use mpi
!!ARGUMENTS:
      type(RunState), intent(inout), target :: mecca_state
!!REVISION HISTORY:
! Initial Version - A.S. - 2013
!EOP
!
!BOC

      ! MPI related variables
      integer :: myrank, ierr ! MPI related variables
      integer :: fail

!MPIDEBUG      call log_mpi_sync(__FILE__,__LINE__)

      ! slaves will skip this processing
      call mpi_comm_rank(mpi_comm_world, myrank, ierr)
      if( myrank == 0 ) then
       call save_inifile( mecca_state )
       call save_vrho   ( mecca_state )
       call save_scflog ( mecca_state )
       call save_rs_str ( mecca_state )
       call save_ks_str ( mecca_state )
       call save_ew_str ( mecca_state )
       call save_vp_data( mecca_state )
       call save_gf_out ( mecca_state )
       call save_dosbsf ( mecca_state )
       call save_fs_data( mecca_state )
      endif
      ! slaves are waiting for root to finish
      fail = mecca_state%fail
      call mpi_bcast(fail, 1, MPI_INTEGER, 0, mpi_comm_world, ierr)
      return
!EOC
      end subroutine saveSCF

!BOP
!!IROUTINE: save_inifile
!!INTERFACE:
      subroutine save_inifile( mecca_state )
!!DESCRIPTION:
! saves {\tt mecca\_state\%intfile} on disk
!
!!ARGUMENTS:
      type(RunState), intent(in), target :: mecca_state
!!REVISION HISTORY:
! Initial Version - A.S. - 2013
!EOP
!
!BOC
      character(80) :: new_name
      if ( .not.associated(mecca_state%intfile) ) return
       if ( index(mecca_state%intfile%name,def_name)==0 ) then
        new_name = def_tag//trim(mecca_state%intfile%name)//def_name
       else
        new_name = def_tag//trim(mecca_state%intfile%name)
       end if
       call saveInput( mecca_state%intfile, new_name)
      return
!EOC
      end subroutine save_inifile

!BOP
!!IROUTINE: save_vrho
!!INTERFACE:
      subroutine save_vrho( mecca_state )
!!DESCRIPTION:
! saves {\tt mecca\_state\%intfile\%sublat\%compon\%v\_rho\_box}
! in potential file
!
!!ARGUMENTS:
      type(RunState), intent(inout), target :: mecca_state
!!REVISION HISTORY:
! Initial Version - A.S. - 2013
!EOP
!
!BOC
      type(IniFile), pointer :: inFile
      type(Sublattice),  pointer :: sublat
      type(SphAtomDeps), pointer :: v_rho_box
      type(Potfile) :: siteptn
      character(80) :: comment
      character(2) :: sp(2)=[ c_up, c_dn ]
      integer nfch,nsub,ic,is,nspin
      real(8) :: alat,ef,zval,ztot
!DEBUG
!DEBUG      character(16) :: fm,st
!DEBUG
      if ( .not.associated(mecca_state%intfile) ) return
      inFile => mecca_state%intfile
      if ( mecca_state%gf_out_box%nume > 0 ) then
       ef = mecca_state%gf_out_box%efermi
      else
       ef = inFile%etop
      end if
      alat = inFile%alat
      nspin = inFile%nspin
      if ( inFile%nMM == 1 ) then
        sp(1) = c_dlm
        sp(2) = c_dlm
      else
        if ( nspin==1 ) sp(1)=c_nm
      end if
      nfch=inFile%io(nu_outpot)%nunit
      if ( nfch > 0 ) then
!DEBUG          write(*,*) ' nu=',nu_outpot
!DEBUG          write(*,*) '   form=',inFile%io(nu_outpot)%form
!DEBUG          write(fm,'(a)') char2form(inFile%io(nu_outpot)%form)
!DEBUG          write(*,*) ' fm=',fm
!DEBUG          write(*,*) '   status=',inFile%io(nu_outpot)%status
!DEBUG          write(st,'(a)') char2status(inFile%io(nu_outpot)%status)
!DEBUG          write(*,*) ' st=',st
!DEBUG        open(unit=nfch,                                                 &
!DEBUG     &                 file=inFile%io(nu_outpot)%name,                  &
!DEBUG     &                 form=fm,status=st,                               &
!DEBUG     &                 IOSTAT=STT,IOMSG=MSG)
        open(unit=nfch,                                                 &
     &                 file=trim(inFile%io(nu_outpot)%name),            &
     &                 form=char2form(inFile%io(nu_outpot)%form),       &
     &                 status=char2status(inFile%io(nu_outpot)%status), &
     &                 IOSTAT=STT,IOMSG=MSG)
        if ( STT .ne. 0 ) then
          write(6,*) ' IO-error in save_vrho: ',MSG
        end if
      else
        nfch = 6
      end if
      do is=1,nspin
       do nsub=1,inFile%nsubl
        sublat => inFile%sublat(nsub)
        do ic=1,sublat%ncomp
         v_rho_box => sublat%compon(ic)%v_rho_box
         ztot = sublat%compon(ic)%zID
         zval = sublat%compon(ic)%zID - sublat%compon(ic)%zcor
         if ( zval < 0 .or. zval > ztot ) then
          if ( inFile%Tempr==0 .or. zval>ztot ) then
           mecca_state%fail = .true.
           write(6,'(a)')                                               &
     &         'save_vrho ERROR :: unexpected value of zcore or zval'
           write(6,'(10x,''nsub = '',i4,'' ic = '',i2,'' ZCORE ='',     &
     &                                   f8.2,'' ZVAL = '',f7.2)')      &
     &                   nsub,ic,sublat%compon(ic)%zcor,zval
          end if
         end if
       write(comment,'(a1,i1,1x,a16,1x,a2,1x,i1,1x,f4.2,1x,i3,1x,a46)') &
     &        ch_vers,vrho_vers,trim(sublat%compon(ic)%name),sp(is),ic, &
     &              sublat%compon(ic)%concentr,nsub,trim(inFile%genName)
         call cp_to_potfile(alat,ztot,zval,ef,is,comment,v_rho_box,     &
     &                                                          siteptn)
         call writepot(nfch,siteptn)
         nullify(v_rho_box)
        end do
       end do
      end do
      close(nfch)
!
      return
!EOC
      end subroutine save_vrho

!BOP
!!IROUTINE: sCurrParams
!!INTERFACE:
      subroutine sCurrParams(imix,alpha,beta,xc_descr)
!!DESCRIPTION:
! sets values of module data

!!DO_NOT_PRINT
      implicit none
!!DO_NOT_PRINT

!!ARGUMENTS:
      integer, intent(in), optional :: imix
      real(8), intent(in), optional :: alpha,beta
      character(len(xc_descr_curr)), intent(in), optional :: xc_descr
!!REVISION HISTORY:
! Initial Version - A.S. - 2013
!EOP
!
!BOC
      if ( present(imix) ) imix_curr = imix
      if ( present(alpha) ) alpha_curr = alpha
      if ( present(beta) ) beta_curr = beta
      if ( present(xc_descr) ) then
        xc_descr_curr = xc_descr
      end if
      return
!EOC
      end subroutine sCurrParams

!BOP
!!IROUTINE: save_scflog
!!INTERFACE:
      subroutine save_scflog( mecca_state )
!!DESCRIPTION:
! saves short scf run log on disk

!!DO_NOT_PRINT
      implicit none
!!DO_NOT_PRINT

!!ARGUMENTS:
      type(RunState), intent(in), target :: mecca_state
!!REVISION HISTORY:
! Initial Version - A.S. - 2013
!EOP
!
!BOC
      type(IniFile), pointer :: inFile
      type(Work_data), pointer :: work_box
      real(8) :: ef,rms,etot,press,fetot,tzatom,zvaltot,alpha,beta,vdif
      integer :: i,nspin,imix,nfch
      real(8), allocatable :: atcon(:,:)
      integer, allocatable :: komp(:)
      integer, allocatable :: zID(:,:)
      logical :: opnd

      if ( .not.associated(mecca_state%intfile) ) return
      if ( .not.associated(mecca_state%work_box) ) return
      if ( .not.associated(mecca_state%gf_out_box) ) return
      inFile => mecca_state%intfile
      work_box => mecca_state%work_box
      ef = mecca_state%gf_out_box%efermi
      nspin = inFile%nspin
      call g_komp(inFile,komp)
      call g_atcon(inFile,atcon)

      if ( alpha_curr == -1 .and. beta_curr == -1 ) then
        alpha = inFile%alphmix
        beta  = inFile%betamix
        if ( inFile%imixtype == 0 ) then
          imix = inFile%mixscheme
        else
          imix = -(1+inFile%mixscheme)
        end if
      else
        alpha = alpha_curr
        beta = beta_curr
        imix = imix_curr
      end if
!      if ( imix<0 ) then
        rms = maxval( abs(work_box%vrms) )
!      else
!        rms = maxval( abs(work_box%qrms) )
!      end if

      vdif = work_box%vmtz(nspin)-work_box%vmtz(1)

      press = work_box%press
      etot = work_box%etot
      fetot = etot - inFile%Tempr*work_box%entropy   ! ?/2
      tzatom = g_numNonESsites(inFile)
      if ( tzatom .ne. zero ) then
       etot  = etot/tzatom
       fetot = fetot/tzatom
      end if
      zvaltot = g_zvaltot( inFile, .true. )

      allocate(zID(1:maxval(komp),1:inFile%nsubl))
      zID = 0
      do i=1,inFile%nsubl
       zID(1:komp(i),i) = inFile%sublat(i)%compon(1:komp(i))%zID
      end do

      nfch=inFile%io(nu_info)%nunit
      if ( nfch > 0 ) then
       inquire(unit=nfch,opened=opnd )
       if ( .not. opnd ) then
        open(unit=nfch,                                                 &
     &                 file=trim(inFile%io(nu_info)%name),              &
     &                 form=char2form(inFile%io(nu_info)%form),         &
     &                 status=char2status(inFile%io(nu_info)%status),   &
     &                 IOSTAT=STT,IOMSG=MSG)
        if ( STT .ne. 0 ) then
          write(6,*) ' IO-error in save_scflog: ',MSG
          write(6,*) ' file=',trim(inFile%io(nu_info)%name)
          write(6,*) ' char_form=',inFile%io(nu_info)%form
          write(6,*) ' form=',char2form(inFile%io(nu_info)%form)
          write(6,*) ' char_status=',inFile%io(nu_info)%status
          write(6,*) ' status=',char2form(inFile%io(nu_info)%status)
        end if
       end if
      else
        nfch = 6
      end if

      call scflog(inFile%alat,inFile%ba,inFile%ca,atcon,zID,            &
     &                inFile%nsubl,inFile%sublat(:)%ns,komp,nspin,      &
     &                fetot,inFile%Tempr,press,                         &
     &                inFile%npts,inFile%nKxyz(1:3,1:inFile%nmesh),     &
     &                rms,ef,sum(work_box%vmtz(1:nspin))/nspin,vdif,    &
     &                inFile%genName,xc_descr_curr,                     &
     &                mecca_state%gf_out_box%xvalws,zvaltot,            &
     &                imix,alpha,beta,                                  &
     &                nfch,inFile%iprint)

      if (allocated(komp)) deallocate(komp)
      if (allocated(atcon)) deallocate(atcon)
      if (allocated(zID)) deallocate(zID)
      return
!EOC
      end subroutine save_scflog

!BOP
!!IROUTINE: save_rs_str
!!INTERFACE:
      subroutine save_rs_str( mecca_state )
!!REMARKS:
!  not implemented
!EOP
!
!BOC
      type(RunState), intent(in), target :: mecca_state
      if ( .not.associated(mecca_state%rs_box) ) return
      return
!EOC
      end subroutine save_rs_str

!BOP
!!IROUTINE: save_ks_str
!!INTERFACE:
      subroutine save_ks_str( mecca_state )
!!REMARKS:
!  not implemented
!EOP
!
!BOC
      type(RunState), intent(in), target :: mecca_state
      if ( .not.associated(mecca_state%ks_box) ) return
      return
!EOC
      end subroutine save_ks_str

!BOP
!!IROUTINE: save_ew_str
!!INTERFACE:
      subroutine save_ew_str( mecca_state )
!!REMARKS:
!  not implemented
!EOP
!
!BOC
      type(RunState), intent(in), target :: mecca_state
      if ( .not.associated(mecca_state%ew_box) ) return
      return
!EOC
      end subroutine save_ew_str

!BOP
!!IROUTINE: save_vp_data
!!INTERFACE:
      subroutine save_vp_data( mecca_state )
!!REMARKS:
!  not implemented
!EOP
!
!BOC
      type(RunState), intent(in), target :: mecca_state
      if ( .not.associated(mecca_state%vp_box) ) return
      return
!EOC
      end subroutine save_vp_data

!BOP
!!IROUTINE: save_gf_out
!!INTERFACE:
      subroutine save_gf_out( mecca_state )
!!REMARKS:
!  not implemented
!EOP
!
!BOC
      type(RunState), intent(in), target :: mecca_state
      if ( .not.associated(mecca_state%gf_out_box) ) return
      return
!EOC
      end subroutine save_gf_out

!BOP
!!IROUTINE: save_dosbsf
!!INTERFACE:
      subroutine save_dosbsf( mecca_state )
!!REMARKS:
!  not implemented
!EOP
!
!BOC
      type(RunState), intent(in), target :: mecca_state
      if ( .not.associated(mecca_state%bsf_box) ) return
      return
!EOC
      end subroutine save_dosbsf

!BOP
!!IROUTINE: save_fs_data
!!INTERFACE:
      subroutine save_fs_data( mecca_state )
!!REMARKS:
!  not implemented
!EOP
!
!BOC
      type(RunState), intent(in), target :: mecca_state
      if ( .not.associated(mecca_state%fs_box) ) return
      return
!EOC
      end subroutine save_fs_data

!BOP
!!IROUTINE: readPotential
!!INTERFACE:
      subroutine readPotential( vr, read_ok, filenm )
!!DESCRIPTION:
! reads potentials from potential file
!
!!ARGUMENTS:
      real(8), intent(out) :: vr(:,:,:,:)
      logical, intent(out) :: read_ok
      character(*), intent(in), optional :: filenm
!!REVISION HISTORY:
! Initial Version - A.S. - 2013
!EOP
!
!BOC
      type(RunState), pointer :: M_S
      type(IniFile), pointer :: inFile
      type(Sublattice),  pointer :: sublat
      type(SphAtomDeps), pointer :: v_rho_box
      type(Potfile) :: siteptn
      integer nfch,nsub,ic,is,i,ndrpts,jmt
      logical :: endflg=.false.
      real(8) :: xst,xmt,h
      real(8), allocatable :: xr(:),rr(:),vr_nucl(:)
      read_ok = .false.
      M_S => getMS()
      if ( .not.associated(M_S%intfile) ) return
      inFile => M_S%intfile
      if ( present(filenm) ) then
       nfch=1
       open(unit=nfch,                                                  &
     &                  file=trim(filenm),                              &
     &                  status='old',IOSTAT=STT,IOMSG=MSG)
      else
       nfch=inFile%io(nu_inpot)%nunit
       if ( nfch > 0 ) then
        open(unit=nfch,                                                 &
     &                  file=trim(inFile%io(nu_inpot)%name),            &
     &                  form=char2form(inFile%io(nu_inpot)%form),       &
     &                  status=char2status(inFile%io(nu_inpot)%status), &
     &                  IOSTAT=STT,IOMSG=MSG)
       else
        read_ok = .true.
        return
       end if
      end if

        if ( STT .ne. 0 ) then
          write(6,*) ' IO-error in readPotential: ',MSG
          return
        end if

        do is=1,inFile%nspin
         do nsub=1,inFile%nsubl
          sublat => inFile%sublat(nsub)
          do ic=1,sublat%ncomp
           call readpot(nfch,siteptn,endflg)
           if ( endflg ) then
            write(6,*) ' unexpected reading-error in readPotential'
            return
           else
            ndrpts = siteptn%ndrpts
            if ( ndrpts > size(vr,1) ) then
              write(6,*) ' ndrpts > size(vr,1) in readPotential'
                return
            end if
            allocate(xr(ndrpts),rr(ndrpts),vr_nucl(1:ndrpts))
            xst = siteptn%xst
            xmt = siteptn%xmt
            jmt = siteptn%jmt
            v_rho_box => sublat%compon(ic)%v_rho_box
            h = (v_rho_box%xmt-v_rho_box%xst)/(v_rho_box%jmt-1)
            forall (i=1:size(xr)) xr(i) = xst + (i-1)*h
            rr = exp(xr)
            if ( jmt .ne. v_rho_box%jmt .or.                            &
     &               abs(xst-v_rho_box%xst)>1.d-9 .or.                  &
     &                         abs(xmt-v_rho_box%xmt)>1.d-9 ) then
              write(6,*) ' bad radial mesh in readPotential'
              return
            end if
!            vr_nucl = -two*sublat%compon(ic)%zID
            call twoZ(dble(sublat%compon(ic)%zID),rr,ndrpts,vr_nucl)
            vr(1:ndrpts,ic,nsub,is) = (siteptn%vr(1:ndrpts)             &
     &                      - v_rho_box%v0(is))*rr(1:ndrpts) + vr_nucl
            vr(ndrpts+1:,ic,nsub,is) = zero
            deallocate(xr,rr,vr_nucl)
            nullify(v_rho_box)
           end if
          end do
         end do
        end do
        close(nfch)

      read_ok = .true.
!
      return
!EOC
      end subroutine readPotential

!!      subroutine read_vrho( mecca_state, read_ok )
!!      type(RunState), intent(inout), target :: mecca_state
!!      logical, intent(out) :: read_fail
!!      type(IniFile), pointer :: inFile
!!      type(Sublattice),  pointer :: sublat
!!!      type(SphAtomDeps), pointer :: v_rho_box
!!      type(Potfile) :: siteptn
!!      integer nfch,nsub,ic,is,nspin
!!      logical :: endflg
!!      read_ok = .false.
!!      if ( .not.associated(mecca_state%intfile) ) return
!!      inFile => mecca_state%intfile
!!      nfch=inFile%io(nu_inpot)%nunit
!!      if ( nfch > 0 ) then
!!        open(unit=nfch,                                                 &
!!     &                  file=inFile%io(nu_inpot)%name,                  &
!!     &                  form=char2form(inFile%io(nu_inpot)%form),       &
!!     &                  status=char2status(inFile%io(nu_inpot)%status), &
!!     &                  IOSTAT=STT,IOMSG=MSG)
!!        if ( STT .ne. 0 ) then
!!          write(6,*) ' IO-error in read_vrho: ',MSG
!!          return
!!        end if
!!        do is=1,2
!!         do nsub=1,inFile%nsubl
!!          sublat => inFile%sublat(nsub)
!!          do ic=1,sublat%ncomp
!!           call readpot(nfch,siteptn,endflg)
!!           if ( endflg .and. is==2.and.nsub==1.and.ic==1 ) then
!!             read_ok = .true.
!!           end if
!!           if ( endflg ) then
!!            return
!!           else
!!            call potfile_to_vrho(siteptn,is,mecca_state)
!!           end if
!!          end do
!!         end do
!!        end do
!!        close(nfch)
!!      end if
!!      read_ok = .true.
!!!
!!      return
!!      end subroutine read_vrho
!
!BOP
!!IROUTINE: scflog
!!INTERFACE:
      subroutine scflog(alat,boa,coa,atcon,iz,                          &
     &                nbasis,nsubl,komp,nspin,                          &
     &                fetot,Temp,press,npts,nq,                         &
     &                rms,efermi,vmtz,vdif,                             &
     &                head,xc_descr,                                    &
     &                xvalws,zvalav,                                    &
     &                imix,alpha,beta,                                  &
     &                nkeep,                                            &
     &                iprint)
!c     ==================================================================
!!DESCRIPTION:
!  engine of subroutine {\tt save\_scflog}i, which reads existing scflog-file
! and adds new record
!
!!USES:
      use universal_const, only : ry2kelvin,ry2eV
!      use scf_io, only : pot_S_mix,pot_B_mix,rho_S_mix ! ,rho_B_mix

!!DO_NOT_PRINT
      implicit none
!!DO_NOT_PRINT

!!ARGUMENTS:
      real(8), intent(in) :: alat,boa,coa
      integer, intent(in) :: nbasis
      integer, intent(in) :: nsubl(:)     ! number of equiv.sublatt.
      real(8), intent(in) :: atcon(:,:)   ! (ipcomp,ipsublat)
      integer, intent(in) :: iz(:,:)      ! (ipcomp,ipsublat) : zID
      integer, intent(in) :: komp(nbasis) ! (ipsublat)
      integer, intent(in) :: nspin
      real(8), intent(in) :: fetot,Temp,press
      integer, intent(in) :: npts,nq(:,:)  ! (3,ipmesh)
      real(8), intent(in) :: rms,efermi,vmtz,vdif
      character(32), intent(in) :: head
      character(*),  intent(in) :: xc_descr
      real(8), intent(in) :: xvalws(:,:,:)  ! (ipcomp,ipsublat,ipspin)
      real(8), intent(in) :: zvalav
      integer, intent(in) :: imix
      real(8), intent(in) :: alpha,beta
      integer, intent(in) :: nkeep,iprint
!
!!REVISION HISTORY:
! Initial Version - A.S. - 2013
!!REMARKS:
! number of lines in scflog-file is limited by 206,
! line size is not more than 120 symbols
!EOP
!
!BOC
      integer, parameter :: ipline=206
      character(120) :: lines(ipline)
      character(1) :: mix
!c     ***************************************************************
      integer nrd,num,i,ii,kk,nrec
      real(8) :: ek,pk,Tempr,TeV,cell_mm
!c
      if( imix==rho_S_mix ) then
       mix='d'                    ! density simple mix
      else if( imix==pot_S_mix ) then
       mix='p'                    ! potential simple mix
      else if( imix <=pot_B_mix ) then
       mix ='P'                   ! potential Broyden mix
      else
       mix='D'                    ! density Broyden mix
      endif
!c     ***************************************************************
      nrd = 0
      num = 0
      nrec = 0
      pk = 99999.d0
      ek = 0
!c
      rewind nkeep
      do i = 1,ipline
       read(nkeep,'(a)',err=2,end=2) lines(i)
       nrd = i
       if(trim(lines(i)).eq.trim(keyline)) then
        read(nkeep,'(i3,f17.9,f12.6)',err=2,end=2) num,ek,pk
        exit
       end if
      end do
      if(nrd.gt.ipline) then
       nrd = 0
      endif
      rewind nkeep
      do i=1,nrd
       read(nkeep,*,err=2,end=2)
      end do
      do i=1,ipline
       read(nkeep,'(a)',err=2,end=2) lines(i)
       nrec = i
      end do
   2  continue
      rewind nkeep
!c     ***************************************************************
      if(iprint.ge.0) then
       write(6,'(''    Iterations read from scflog file'',t40,          &
     &           ''='',i3)') nrec
      end if
!c     ***************************************************************
!c
!CCC      num=mod(num,1000)+1
      num=min(num+1,999)

      write(nkeep,'('' System'',t20,'': '',a)') trim(head)
      if ( Temp==zero ) then
       write(nkeep,'('' Temperature (K)'',t20,'': 0'')')
      else
       Tempr = Temp*ry2kelvin
       TeV = Temp*ry2eV
       if ( Temp < 0.06333621d0 ) then
        write(nkeep,'('' Temperature '',t20,'': '',f7.1,'' K, '',       &
     &              es14.7,'' Ry, '',es14.7,'' eV'')') Tempr,Temp,Tev
       else if ( TeV <= 1.d+5  ) then
        write(nkeep,'('' Temperature '',t20,'': '',es14.7,'' K, '',     &
     &                f13.7,'' Ry, '',f13.6,'' eV'')') Tempr,Temp,Tev
       else
        write(nkeep,'('' Temperature '',t20,'': '',es14.7,'' K, '',     &
     &               es14.7,'' Ry, '',es14.7,'' eV'')') Tempr,Temp,Tev
       end if
      end if
      write(nkeep,'('' Lattice spacing'',t20,'':'',f10.5,               &
     &            5x,''B/A='',f7.4,3x,''C/A='',f7.4)') alat,boa,coa
      write(nkeep,'('' No. of E-pts'',t20,'':'',i6)') npts
      write(nkeep,'('' No. of BZ K-pts'',t20,'':'',3i4)')               &
     &             (nq(ii,1),ii=1,3)
      do i=2,size(nq,2)
        write(nkeep,'('' '',t20,'' '',3i4)')                            &
     &             (nq(ii,i),ii=1,3)
      end do
      write(nkeep,'('' No. of Sub-lats.'',t20,'':'',i3)') nbasis
      write(nkeep,'('' No. of Components'',t20,'':'',10i3)')            &
     &             (komp(ii),ii=1,nbasis)
      write(nkeep,'('' Atomic Numbers'',t20,'':'',10i3)')               &
     &             ((iz(kk,ii),kk=1,komp(ii)),ii=1,nbasis)
      write(nkeep,'('' Concentrations'',t20,'':'',10f10.5)')            &
     &             ((atcon(kk,ii),kk=1,komp(ii)),ii=1,nbasis)
      write(nkeep,'('' zvalav & vdif'',t20,'':'',2f10.5                 &
     &,             5x,''amix & bmix'',t60,'':'',2(1x,f6.4))')          &
     &                  zvalav,vdif,alpha,beta
      cell_mm = zero
      if ( nspin == 1 ) then
        write(nkeep,'('' WS-charges       '',t20,'':'',10f10.5)')       &
     &             ((xvalws(kk,ii,1),kk=1,komp(ii)),ii=1,nbasis)
      else
        write(nkeep,'('' WS-charges (maj.)'',t20,'':'',10f10.5)')       &
     &             ((xvalws(kk,ii,1),kk=1,komp(ii)),ii=1,nbasis)
        write(nkeep,'('' WS-charges (min.)'',t20,'':'',10f10.5)')       &
     &             ((xvalws(kk,ii,nspin),kk=1,komp(ii)),ii=1,nbasis)
        write(nkeep,'('' WS-moments'',t20,'':'',10f10.5)')              &
     &             ((xvalws(kk,ii,1)-xvalws(kk,ii,nspin),               &
     &                                   kk=1,komp(ii)),ii=1,nbasis)
        do ii=1,nbasis
         do kk=1,komp(ii)
          cell_mm = cell_mm + (xvalws(kk,ii,1)-xvalws(kk,ii,nspin)) *   &
     &                        atcon(kk,ii)*nsubl(ii)
         end do
        end do
      end if
      write(nkeep,'(a)') '# '//trim(xc_descr)
      write(nkeep,'(a)') trim(keyline)
!c
      if ( abs(fetot)<1.d+10 .and. abs(efermi)<1.e+5 .and.              &
     &                     abs(vmtz)<1.e+4 .and. abs(vdif)<1.d+2 ) then
       write(nkeep,1001)num,fetot,press,rms,efermi,vmtz,vdif,cell_mm,mix
      else
       write(nkeep,1002)num,fetot,press,rms,efermi,vmtz,vdif,cell_mm,mix
      end if
1001  format(i3,1x,f17.7,1x,g14.6,e11.3,1x,f12.5,1x,f9.4,1x,f7.4,       &
     &                                                    1x,g10.3,1x,a)
1002  format(i3,1x,es17.3,1x,g14.6,e11.3,1x,es16.5,1x,es12.4,1x,es11.4, &
     &                                                    1x,g10.3,1x,a)
!!c  Can be commented - muffin-tin corrected energy
!       write(nkeep,'(10x,'' MTC '',f17.7,5x,f8.6)') ek+emtc,emtc

      write(nkeep,'(a)') (lines(i),i=1,nrec)

      call flush(nkeep)

      return
!EOC
      end subroutine scflog

!BOP
!!IROUTINE: cpscflog
!!INTERFACE:
      subroutine cpscflog(logfile,nrec,infofile)
!!DESCRIPTION:
! to copy scf log-file {\tt logfile} (or a part of it)
! to file {\tt infofile}

!!DO_NOT_PRINT
      implicit none
!!DO_NOT_PRINT

!!ARGUMENTS:
      character*(*) logfile
      integer nrec
      character*(*) infofile
!!REVISION HISTORY:
! Initial Version - A.S. - 2013
!EOP
!
!BOC
      character(1024), allocatable :: line(:)
      integer i,n
      integer, parameter :: n0=300
      if (trim(logfile) == trim(infofile) ) return
      open(11,file=trim(logfile),status='old',err=2)
      allocate(line(n0))
      i = 0
      n = 0
      do while ( 1 < 2 )
       i = i+1
       read(11,'(a)',err=2,end=2) line(1)
       if ( trim(line(1)).eq.trim(keyline) ) then
        n = i+nrec
        exit
       end if
      end do
      if ( n>n0 ) then
        deallocate(line)
        allocate(line(n))
      end if
      rewind 11
      do i=1,n
        read(11,'(a)',err=2,end=2) line(i)
      end do
      close(11)
      open(11,file=trim(infofile),status='unknown',err=2)
      do i=1,n
        write(11,'(a)') trim(line(i))
      end do
      close(11)
2     return
!EOC
      end subroutine cpscflog

      end module scf_io
