!BOP
!!ROUTINE: zeropt
!!INTERFACE:
      subroutine zeropt(ezpt,tpzpt,omegws,ztotss,nkomp)
!!DESCRIPTION:
!  computes sublattice zero-pt energy, {\tt ezpt} (in Ry), 
! and pressure, {\tt tpzpt}, for components with atomic numbers {\tt ztotss};
! {\tt omegws} is input WS volume, {\tt nkomp} is number of
! sublattice components \\ (estimation is based on experimental data)
!

!!DO_NOT_PRINT
      implicit none
!!DO_NOT_PRINT

!!ARGUMENTS:
       real*8 ezpt(*)
       real*8 tpzpt(*)
       real*8 omegws
       real*8 ztotss(*)
       integer nkomp
!!REVISION HISTORY:
! based on DDJ version - 1992
! Adapted - A.S. - 2013
!!REMARKS:
! exp. data are unknown, ezpt is set to 0
!EOP
!
!BOC

       integer ipzm
       parameter (ipzm=112)
       real*8 tdebye(0:ipzm)
!c       integer iyes
!c
       real*8 grune(0:ipzm)
       real*8 expvol(0:ipzm)
       real*8 r9o8
!c     *****************************************************************
       parameter (r9o8=9.d0/8.d0)
!c     *****************************************************************
!c
        real*8 bolts
        parameter (bolts=6.33870d-06)
!c  Low temperature limit (T at 0 K)
      data tdebye/0,                                                    &! EmptySphere
     &   122,0                                                          &! H-He
     &  ,344,1481,1480,2250,70,93,65,75                                 &! Li-Ne # C: diamond str. (412 K is for graphite)
     &  ,156.5,403,433,645,576,527,142,92                               &! Na-Ar # at room T: P(white phosph.), S
     &  ,91.1,229,346,420,399,606,409,477,460,477                       &! K-Ni
     &  ,347,329,325,373,282,152.5,104,71.9                             &! Cu-Kr
     &  ,56.5,147,248,290,276,423,454,555,512,271                       &! Rb-Pd
     &  ,227.3,210,112,200,220,152,109,64                               &! Ag-Xe
     &  ,40.5,111,150,252,246,383,416,467                               &! Cs-Os # La: alpha-La
     &  ,420,237,162.3,122,200,105,120,92,15                            &! Ir-Rd
     &  ,41*0/
!c
      data grune/0.d0,                                                  &! EmptySphere
     &   0.d0,0.d0                                                      &! H-He
     &  ,1.18d0,1.18d0,0.d0,0.d0,0.d0,0.d0,0.d0,0.d0                    &! Li-Ne
     &  ,1.31d0,1.48d0,2.19d0,0.d0,0.d0,0.d0,0.d0,0.d0                  &! Na-Ar
     &  ,1.37d0,1.16d0,1.17d0,1.18d0,1.05d0,1.30d0,2.07d0,1.66d0,1.93d0,&
     &                                              1.88d0              &! K-Ni
     &  ,2.d0,2.01d0,2.d0,0.d0,0.d0,0.d0,0.d0,0.d0                      &! Cu-Kr
     &  ,1.67d0,1.d0,.89d0,.83d0,1.58d0,1.6d0,2.6d0,3.2d0,2.23d0,2.28d0 &! Rb-Pd
     &  ,2.36d0,2.23d0,2.37d0                                           &! Ag-In
     &  ,63*0.d0/
!c
      data expvol/0.d0,                                                 &! EmptySphere
     &   0.d0,0.d0,                                                     &! H-He
     &   143.7d0,54.54d0,0.d0,0.d0,0.d0,0.d0,0.d0,0.d0                  &! Li-Ne
     &  ,254.5d0,151.4d0,109.9d0,0.d0,0.d0,0.d0,0.d0,0.d0               &! Na-Ar
     &  ,481.3d0,291.1d0,168.7d0,120.3d0,93.48d0,80.63d0,82.84d0,78.95d0&
     &                                              ,74.72d0,73.42d0    &! K-Ni
     &  ,78.92d0,99.35d0,132.4d0,0.d0,0.d0,0.d0,0.d0,0.d0               &! Cu-Kr
     &  ,598.9d0,373.6d0,194.7d0,139.9d0,119.2d0,102.7d0,97.25d0,92.54d0&
     &                                              ,93.70d0,99.67d0    &! Rb-Pd
     &  ,111.9d0,142.9d0,179.2d0                                        &! Ag-In
     &  ,63*0.d0/

        integer is,iz
        real*8  ezero

        do is=1,nkomp
         ezpt(is)=0.d0
         tpzpt(is)=0.d0
        enddo
!c
        do is=1,nkomp
         iz=nint(ztotss(is))               ! *** 0 .LE. ZTOTSS .LE. 112 ***
         if(expvol(iz).ne.0.0d0) then
           ezero=r9o8*bolts*tdebye(iz)*(expvol(iz)/omegws)**grune(iz)
           ezpt(is)=ezero
           tpzpt(is)=3.d0*grune(iz)*ezero
         endif
        enddo
!c
      return
!EOC
      end subroutine zeropt

!CABc
!CABc  Original subroutine of DDJ
!CABc
!CABc     ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc !cc
!CAB      subroutine zeropt(ezpt,tpzpt,omegws,ztotss,nkomp,nspin)
!CABc     =============================================================== !==
!CAB      implicit real*8 (a-h,o-z)
!CABc
!CABc     *************************************************************** !**
!CAB      include 'mkkrcpa.h'
!CABc     *************************************************************** !**
!CABc
!CAB       integer idebye(49)
!CAB       integer nkomp
!CAB       integer nspin
!CAB       integer iyes
!CABc
!CAB       real*8 ztotss(ipcomp)
!CAB       real*8 ezpt(ipcomp)
!CAB       real*8 tpzpt(ipcomp)
!CAB       real*8 grune(49)
!CAB       real*8 expvol(49)
!CAB       real*8 r9o8
!CABc     *************************************************************** !**
!CAB       parameter (r9o8=three*three/eight)
!CABc     *************************************************************** !**
!CABc
!CAB        data bolts/6.33870e-06/
!CABc
!CAB        data idebye/0,0,344,1440,0,0,0,0,0
!CAB     >  ,0,158,400,428,0,0,0,0,0,91
!CAB     >  ,230,360,420,380,630,410,467,445,450,343
!CAB     >  ,327,320,0,0,0,0,0,56,147,280
!CAB     >  ,291,275,450,350,600,480,274,225,209,108/
!CABc
!CAB        data grune/0.,0.,1.18,1.18,0.,0.,0.,0.,0.
!CAB     >  ,0.,1.31,1.48,2.19,0.,0.,0.,0.,0.,1.37
!CAB     >  ,1.16,1.17,1.18,1.05,1.30,2.07,1.66,1.93,1.88,2.
!CAB     >  ,2.01,2.,0.,0.,0.,0.,0.,1.67,1.,.89
!CAB     >  ,.83,1.58,1.6,2.6,3.2,2.23,2.28,2.36,2.23,2.37/
!CABc
!CAB        data expvol/0.,0.,143.7,54.54,0.,0.,0.,0.,0.
!CAB     >  ,0.,254.5,151.4,109.9,0.,0.,0.,0.,0.,481.3
!CAB     >  ,291.1,168.7,120.3,93.48,80.63,82.84,78.95,74.72,73.42,78.92
!CAB     >  ,99.35,132.4,0.,0.,0.,0.,0.,598.9,373.6,194.7
!CAB     >  ,139.9,119.2,102.7,97.25,92.54,93.70,99.67,111.9,142.9,179.2/
!CABc
!CABc Include zero-pt energy?
!CAB        iyes=1
!CAB        do is=1,nkomp
!CAB           iz=ztotss(is)+.1
!CAB           if(iz.lt.1 .or. iz.gt.49) iyes=0
!CAB        enddo
!CAB
!CAB
!CAB
!CAB        do is=1,nkomp
!CAB         iz=ztotss(is)+.1
!CAB         ezero=zero
!CAB         tpvzer=zero
!CAB           if(iyes.eq.1)then
!CAB              if(expvol(iz).ne.0.0) then
!CAB                  ezero=r9o8*bolts*idebye(iz)*
!CAB     >                  (expvol(iz)/omegws)**grune(iz)
!CAB                  ezero=ezero/nspin
!CAB                 tpvzer=three*grune(iz)*ezero
!CAB              endif
!CAB           else
!CABc              write(6,*)' IGNORING zero pt. '
!CAB           endif
!CAB              ezpt(is)=ezero
!CAB             tpzpt(is)=tpvzer
!CAB        enddo
!CABc
!CAB      return
!CAB      end
