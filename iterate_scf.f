!BOP
!!ROUTINE: iterateSCF
!!INTERFACE:
      subroutine iterateSCF( mecca_state )
!!DESCRIPTION:
! full {\sc mecca} SCF-iteration cycle
! or just calculation of Green's function related
! data (without SCF cycle)
!
!!USES:
      use mecca_constants
      use mecca_types
      use gfncts_interface, only : g_numNonESsites,c_v0
      use kkrgf, only : scf_iteration
      use mecca_interface, only : char2status
      use mpi

!!DO_NOT_PRINT
      implicit none
!!DO_NOT_PRINT

!!ARGUMENTS:
      type(RunState), intent(inout), target :: mecca_state
!
!      mecca_state%work_box is used as a container for output
!
!!REVISION HISTORY:
! Initial Version - A.S. - 2013
!!REMARKS:
! file with name STOP_MECCA in run directory interrupts scf cycle
!EOP
!
!BOC
      character(*), parameter :: sname='iterateSCF'
!      integer, intent(in) :: ivers  ! mecca_ames, mecca_2005, etc

      type(IniFile),   pointer :: inFile   => null()
      type(Work_data), pointer :: work_box => null()
      type(Work_data), target, save :: work0box
!      type ( RS_Str ),    pointer :: rs_box
!      type ( KS_Str ),    pointer :: ks_box
!      type ( EW_Str ),    pointer :: ew_box
!      type (VP_data),    pointer :: vp_box
!      type (GF_data),     pointer :: gf_box
!      type (GF_output),   pointer :: gf_out
!      type (DosBsf_data), pointer :: bsf_box
!      type (FS_data),     pointer :: fs_box

      integer :: nspin,ndrpts,ndcomp,ndsubl,nscf,itscf,iset_in,ixcmax
      integer, save :: ixc_save=0
      character(60) :: text
      logical :: fileexist=.false.
      logical :: gf_only
      real(8) :: ef_old,etot_old,entr_old
      real(8) :: efermi=zero
      real(8) :: ebot_in
      logical iexit
      real(8) :: dv0(2)
      real(8) :: tol_qr,tol_vr,tol_v0,v0_err

      integer :: myrank, ierr

      integer, external :: maxXCid,maxNonLibxcID

      call mpi_comm_rank(mpi_comm_world, myrank, ierr)
      inFile => mecca_state%intfile
      call g_ndims(inFile,ndrpts,ndcomp,ndsubl,nspin)

      if ( associated(mecca_state%work_box) ) then
        call deallocWorkdata(mecca_state%work_box)
      else
        mecca_state%work_box => work0box
      end if
      call allocWorkdata(mecca_state%work_box,                          &
     &                                    ndrpts,ndcomp,ndsubl,nspin)
      work_box => mecca_state%work_box
!      rs_box => mecca_state%rs_box
!      ks_box => mecca_state%ks_box
!      ew_box => mecca_state%ew_box
!      vp_box => mecca_state%vp_box
!      gf_box => mecca_state%gf_box
!      gf_out => mecca_state%gf_out_box
!      bsf_box => mecca_state%bsf_box
!      fs_box => mecca_state%fs_box

      if ( inFile%nscf==0 .and. max(mecca_state%bsf_box%idosplot,       &
     &                       mecca_state%bsf_box%ibsfplot,              &
     &                       mecca_state%fs_box%ifermiplot)>0 ) then
       gf_only = .true.
       nscf = 1
       if ( mecca_state%bsf_box%bsf_ef == zero ) then
         mecca_state%bsf_box%bsf_ef = mecca_state%gf_out_box%efermi
       end if
      else
       gf_only = .false.
       nscf = inFile%nscf
!c     -----------------------------------------------------------------
!c
!c  Solve CPA equations along energy contour and find Fermi energy...
!c
!c     -----------------------------------------------------------------
!c

        write(6,'(/'' Start SCF: Maximum number of iterations ='',      &
     &                                       i5/)') nscf

      end if
      iexit = .false.
      if ( gf_only ) iexit=.true.
      if ( abs(inFile%ebot) > epsilon(one) ) then
          inFile%iset_ebot = 0
          ebot_in = inFile%ebot
      else
          inFile%ebot = zero
      end if
      ebot_in = inFile%ebot
      iset_in = inFile%iset_ebot
      ef_old = inFile%etop

      ixc_save = inFile%iXC
!
! if itscf<ixcmax then gradient corrections in XC functional are ignored (lda_only=1 in xc-pointer)
!
      ixcmax = 0
      if (trim(char2status(inFile%io(nu_inpot)%status))=='NEW') then
       if ( inFile%nscf>10 ) then
        if ( inFile%iXC>maxNonLibxcID() ) then
!!         if ( nscf>150 ) then
!!!          ixcmax = min(20,2+inFile%nscf/10)
!!          if ( inFile%Tempr > zero ) then
!!           ixcmax = min(2 + nint(2*log10(one+inFile%Tempr)),20)
!!          else
!!           ixcmax = 1
!!          end if
!!         else
          if ( nscf>5 ) ixcmax = 1
!!         end if
         if ( ixcmax>0 ) then
          inFile%iXC = inFile%iXC + maxXCid()
         end if
        end if
       end if
      end if

      do itscf=1,nscf
       etot_old = work_box%etot
       entr_old = work_box%entropy

       if( nspin.eq.2 ) then
        if ( inFile%magnField.ne.zero ) then
         if ( itscf<min(5,nscf/3) ) then
          dv0(1) = -inFile%magnField/2
          dv0(2) =  inFile%magnField/2
          dv0 = dv0 / 2**(itscf-1)
          if ( abs(inFile%nMM) == 1 ) then
           if ( itscf==1 ) then
             inFile%V0(1) = sum(inFile%V0(1:nspin))/nspin
             inFile%V0(nspin) = inFile%V0(1)
           end if
          end if
          call c_v0(inFile,dv0)
          if ( mecca_state%intfile%iprint >= -1 ) then
           write(6,'('' external exchange-splitting'',t30,''='',f10.5)')&
     &         dv0(nspin)-dv0(1)
          end if
         else
          if ( itscf==1 .and. inFile%magnField.ne.zero ) then
           write(6,'(a)') 'WARNING: to enforce initial spin splitting,' &
     &     //' nscf (number of SCF iterations) must be not less than 6'
          end if
          inFile%magnField = zero
         end if
        end if
       end if
!
!c
!DEBUG
!        if (itscf>2) then
!         write(6 ,'(/''  EXIT AFTER '',i2,'' ITERATION''//)') itscf-1
!         EXIT
!        end if
!DEBUG
       if(inFile%iprint.ge.-100 .and. itscf>1) then
         write(6 ,'(''  Begin iteration number '',i3,                   &
     &               ''  ----------------------------'')') itscf
       end if
      call mpi_barrier(mpi_comm_world, ierr)

!-----------------------------------------------------------------------!
       if ( gf_only .and. efermi.ne.zero ) then
        call scf_iteration( mecca_state, gf_only, efermi )
       else
        if ( ebot_in .ne. zero ) inFile%ebot = ebot_in
        call scf_iteration( mecca_state, gf_only )
       end if
!-----------------------------------------------------------------------!

       if ( .not. gf_only ) then
        mecca_state%info = itscf
        if ( mecca_state%fail ) then
         write(6,'(5x,'' ** FAILED AFTER'',i5,'' ITERATIONS **'')')     &
     &                                        itscf
         write(6,*) sname//' :: ERROR in scf_iteration'
!         call fstop('return')
         return
        end if

        call checkConvergence( )
        if ( iexit ) mecca_state%info = 0

        ef_old = inFile%etop
        inFile%etop = mecca_state%gf_out_box%efermi
       end if

       if ( iexit ) EXIT
!
! file with name STOP_MECCA in run directory interrupts scf cycle
!


       inquire(file='STOP_MECCA',EXIST=fileexist)
       if ( fileexist ) then
         mecca_state%info = itscf
         text = 'USER EXIT'
         mecca_state%fail = .true.
         iexit = .false.
         write(6,'(/5x,a//)') sname//' :: '//trim(text)
         call mpi_barrier(mpi_comm_world, ierr)
         if ( myrank==0 ) call delete_file('STOP_MECCA')
         call mpi_barrier(mpi_comm_world, ierr)
         text = ' STOP_MECCA file is deleted'
         write(6,'(/5x,a//)') sname//' :: '//trim(text)
!         call p_fstop('return')
         EXIT
       end if
!
       if ( inFile%iXC .ne. ixc_save ) then
        if ( itscf > ixcmax ) inFile%iXC = ixc_save
       end if

      end do ! end of SCF-iteration cycle
      inFile%iXC = ixc_save

      if ( .not. iexit ) then
         write(6,'(5x,'' ** NO CONVERGENCE AFTER'',i5,'' ITERATIONS **''&
     &          )')    min(itscf,nscf)
      end if
!
! do not dealloc workdata as there is output info in it
!
      nullify(work_box)

      if( myrank == 0 ) then
        call closeFiles(inFile%io(1:inFile%numIOfiles))
      endif
      inFile%iset_ebot = -1
!MPIDEBUG      call log_mpi_sync(__FILE__,__LINE__)
      call mpi_barrier(mpi_comm_world, ierr)

      return

!EOC
      contains

!BOP
!!IROUTINE: checkConvergence
!!INTERFACE:
      subroutine checkConvergence( )
!EOP
!
!BOC
      real(8) :: etot_err,fe_err,vmax_err,qmax_err,rws,dQkkr_err
      real(8) :: tzatom,entr_err
      integer :: isp,ic,nsub
      logical, external :: gNonMT
      real(8), external :: gMaxErr
      real(8) :: etol_
!
      if ( inFile%igrid == 0 ) then
        iexit = .true.
        if(inFile%iprint.ge.-100) then
          write(6,'(/''   ** EXIT IS ENFORCED BY IGRID==0 ** '')')
        end if
        return
      end if
!
      if ( allocated(work_box%vrms) .and. allocated(work_box%qrms) )    &
     & then

        tzatom = g_numNonESsites(inFile)
        if ( tzatom <= zero ) tzatom = one
        if(inFile%iprint.ge.-100) write(6,'(/)')
        etot_err = abs(work_box%etot-etot_old)/tzatom
        entr_err = abs(work_box%entropy-entr_old)/tzatom
        fe_err = abs((work_box%etot-etot_old)-                          &
     &                inFile%Tempr*(work_box%entropy-entr_old))/tzatom
        vmax_err = zero
        qmax_err = zero
        dQkkr_err = zero
        do isp = 1,nspin
         do nsub=1,inFile%nsubl
          rws = mecca_state%rs_box%rws(nsub)
          do ic=1,inFile%sublat(nsub)%ncomp
!DEBUG           vmax_err=max(vmax_err,work_box%vrms(ic,nsub,isp)/rws)
!DEBUG           qmax_err=max(qmax_err,work_box%qrms(ic,nsub,isp)*rws/three)
           vmax_err=max(vmax_err,abs(work_box%vrms(ic,nsub,isp)))
           qmax_err=max(qmax_err,abs(work_box%qrms(ic,nsub,isp)))
           if ( gNonMT() )                                              &
     &      dQkkr_err=max(dQkkr_err,abs(work_box%dq_err(ic,nsub,isp)))
           if(inFile%iprint.ge.-1) then
            write(6,'(''  Sub-lat ='',i2,                               &
     &       '' Comp ='',i2,'' Spin ='',i2,                             &
     &       ''   V_err'',t44,''='',d10.3,                              &
     &       ''  Q_err'',t66,''='',d10.3,                               &
     &       ''  dQkkr_err'',t89,''='',d10.3)')                         &
     &         nsub,ic,isp,                                             &
!DEBUG     &       work_box%vrms(ic,nsub,isp)/rws,                            &
!DEBUG     &       work_box%qrms(ic,nsub,isp)*rws/three,                      &
     &       work_box%vrms(ic,nsub,isp),                                &
     &       work_box%qrms(ic,nsub,isp),                                &
     &       work_box%dq_err(ic,nsub,isp)
           end if
          enddo
         enddo
        enddo
        qmax_err = gMaxErr(0)
        vmax_err = gMaxErr(1)
        v0_err = sum(abs(work_box%dV0(1:nspin)))/nspin
        if(inFile%iprint.ge.-100) then
         if ( inFile%Tempr == zero ) then
          entr_err = zero
          write(6,'(/''  Pot-wght-err. = '',d12.4,                      &
     &               3x,''Rho-wght-err. = '',d12.4,                     &
     &               3x,''Etot-err.='',d12.3,                           &
     &               3x,''dQkkr-err.='',d12.3                           &
     &                                                 /)')             &
     &                              vmax_err,qmax_err,etot_err,dQkkr_err
         else
          write(6,'(/''  Pot-wght-err. = '',d12.4,                      &
     &               3x,''Rho-wght-err. = '',d12.4,                     &
     &               3x,''Etot-err.='',d12.3,                           &
     &               3x,''Entr-err.='',d12.3,                           &
     &               3x,''dQkkr-err.='',d12.3                           &
     &                                                 /)')             &
     &                    vmax_err,qmax_err,etot_err,entr_err,dQkkr_err
         end if
        end if

       etol_ = etol
       if ( etot_err < etol_ .and. entr_err<etol_ ) then
!        vmax_err = maxval( work_box%vrms )
!        qmax_err = maxval( work_box%qrms )
!        if(inFile%imixtype.eq.0) then           ! RHO-mixing
!
!         if( (itscf<=1.and.vmax_err.le.20*etol.and.qmax_err.lt.20*etol) &
!     &     .or. (vmax_err.le.20*etol .and. qmax_err.lt.5*etol)          &
!     &       ) then
!           write(6,*) '    qmax_err=',qmax_err,'  5*etol=',5*etol
!           write(6,*) '    vmax_err=',vmax_err,'  20*etol=',20*etol
!           write(6,'(5x,''** (imixtype=0) ** TERMINATED AFTER'',        &
!     &                    i5,'' ITERATIONS *****************''/)') itscf
!!C           call p_kill(ntids,tids,tids(MASTER))
!!           if(ME.eq.MASTER) then
!!            text = 'NORMAL EXIT'
!            mecca_state%fail = .false.
!            iexit = .true.
!!            go to 100
!!           else
!!            write(6,*) '      ME=',me,' TIDS(ME)=',tids(me)
!!            call fstop(sname//':: EXIT')
!!           end if
!         end if
!
!        else                !  POT-mixing

        if ( dQkkr_err<0.001d0) then
         tol_qr = 20*etol_
         tol_vr = 10*etol_
        else
         tol_qr = 10*etol_
         tol_vr = 5*etol_
         write(6,'(a)') 'WARNING: dQkkr difference is quite large and ' &
     &                 //'it''s compensated by homogeneous electrons,'  &
     &                 //'accuracy may be low.'
        end if
        tol_v0 = max(5*tol_vr,gEfTol(),5.d-4)
!
        if( qmax_err<tol_qr .and. vmax_err<tol_vr                       &
     &                                     .and. v0_err<tol_v0 ) then
!
         if ( inFile%iXC == ixc_save ) then
           mecca_state%fail = .false.
           iexit = .true.
           write(6,'(''    q_wgh_err='',es11.3,''  tol_qr='',es11.3)')  &
     &                              qmax_err,tol_qr
           write(6,'(''    v_wgh_err='',es11.3,''  tol_vr='',es11.3)')  &
     &                              vmax_err,tol_vr
           write(6,'(''    v0_err   ='',es11.3,''  tol_v0='',es11.3)')  &
     &                              v0_err,tol_v0
           write(6,'(5x,''** CONVERGED AFTER'',                         &
     &                     i5,'' ITERATIONS *****************'')') itscf
         end if
        end if
        inFile%iXC = ixc_save

       end if

!DEBUG       if ( etot_err < 100*etol_ .and. vmax_err < 100*etol_ ) then
       if ( vmax_err < 0.01d0 ) then
           inFile%iset_ebot = 0
       else
           inFile%iset_ebot = iset_in
       end if

      end if

      return
!EOC
      end subroutine checkConvergence
!
!BOP
!!IROUTINE: closeFiles
!!INTERFACE:
      subroutine closeFiles(io)
!!ARGUMENTS:
      type(IOfile), intent(in) :: io(:)
!EOP
!
!BOC
      integer :: i
      logical :: opnd
      do i=1,size(io)
       if ( io(i)%nunit>0 .and. io(i)%nunit.ne.6 ) then
        inquire(io(i)%nunit,opened=opnd)
        if ( .not. opnd ) cycle
        if ( io(i)%status=='r' .and.                                    &
     &        ( io(i)%nunit==nu_mix1 .or. io(i)%nunit==nu_mix2 )        &
     &      ) then
          close(io(i)%nunit,status='DELETE')
        else
         if ( io(i)%status=='n' .or. io(i)%status=='o'                  &
     &                         .or. io(i)%status=='r' ) then
          close(io(i)%nunit,status='KEEP')
         else
          if ( io(i)%status=='u' .and.                                  &
     &        ( io(i)%nunit==nu_mix1 .or. io(i)%nunit==nu_mix2 .or.     &
     &          io(i)%nunit==nu_dop .or. io(i)%nunit==nu_spkpt .or.     &
     &          io(i)%nunit==nu_mdlng .or. io(i)%nunit==nu_yymom )      &
     &      ) then
           close(io(i)%nunit,status='DELETE')
          else
           close(io(i)%nunit)
          end if
         end if
        end if
       end if
      end do
      return
!EOC
      end subroutine closeFiles
!
!BOP
!!IROUTINE: deallocate_scf
!!INTERFACE:
      subroutine deallocate_scf( )
!!DESCRIPTION:
! NOT IMPLEMENTED
!
!EOP
      return
      end subroutine deallocate_scf
!
      end subroutine iterateSCF

