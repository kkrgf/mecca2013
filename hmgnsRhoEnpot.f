!BOP
!!ROUTINE: HmgnsRhoEnPot
!!INTERFACE:
       subroutine HmgnsRhoEnPot(rho0,S,R,alphpot,alphen,                &
     &                          Qs,Qr,Pot,En)
!!DESCRIPTION:
!  calculates electrostatic "energy", {\tt En}, and "potential", {\tt Pot}, due to
!  homogeneous charge density {\tt rho0) {\em between} sphere with radius {\tt S}
!  and a shape defined by ${R,alphen,alphpot}$; {\tt alphen, alphpot} are 
!  shape-correction coefficients, i.e. alphen=1,alphpot=1 for any sphere, thus 
!  En$_{shape}$ = alphen*En$_{sphere}$, Pot$_{shape}$=alphpot*Pot$_{sphere}$ and 
!  output $En = alphen * En[R] - En[S]$, $Pot = alphpot * Pot[R] - Pot[S]$
!  (in at.units)
! \\ \\
! In fact, En$_{shape}$ is not an energy, but a Coulomb energy double-integral 
! (over the shape) contribution
!

!!DO_NOT_PRINT
       implicit none
!!DO_NOT_PRINT

!!ARGUMENTS:
       real(8), intent(in) :: rho0,S,R,alphpot,alphen    ! input
       real(8), intent(out) :: Qs,Qr,En,Pot    ! Qs,Qr are charges
!!REVISION HISTORY:
! Initial Version - A.S. - 2013
!EOP
!
!BOC
       real(8), parameter :: pi=3.1415926535897932384d0
       real(8), parameter :: pi4d3=4.d0*pi/3.d0

       En = 0.d0
       Pot = 0.d0
       Qs = 0.d0
       Qr = 0.d0

       if(R.le.0.d0) return

       Qr = pi4d3*R**3*rho0
       Pot = alphpot*Potential(Qr,R)
       En  = alphen*Energy(Qr,R)

       if(S.le.0.d0) return

       Qs = pi4d3*S**3*rho0
       Pot = Pot - Potential(Qs,S)
       En  = En  - Energy(Qs,S)
!c       En  = En  - 2.d0*Qs*Pot

       return

!EOC
       CONTAINS

!BOP
!!IROUTINE: Potential
!!INTERFACE:
       function Potential(q0,S)    !  integral{rho0/r}
!!REMARKS:
! private procedure of subroutine HmgnsRhoEnPot
!EOP
!
!BOC
       real*8 Potential,q0,S

       Potential =  3.d0*q0/S                   ! at.units

       return
!EOC
       end function Potential

!BOP
!!IROUTINE: Energy
!!INTERFACE:
       function Energy(q0,S)       ! double integral {rho0*rho0/(r-r1)}
!!REMARKS:
! private procedure of subroutine HmgnsRhoEnPot
!EOP
!
!BOC
       real*8 Energy,q0,S
       real*8 sixfifth
       parameter (sixfifth=6.d0/5.d0)

       Energy = sixfifth*q0**2/S                ! at.units

       return
!EOC
       end function Energy

       end subroutine HmgnsRhoEnPot
