!c     ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
!BOP
!!ROUTINE: calgreen
!!INTERFACE:
      subroutine calgreen(greens,                                       &
     &                    kkrsz,natom,tcpa,tcinv,ndkkr,idiag,itype,     &
     &                    invswitch,invparam,                           &
     &                    iprint,istop)
!!DESCRIPTION:
! {\bv
! input:
!         greens -- zero-Green's function (G0), order of KKRSZ*NATOM
!         tcpa -- diagonal of t-matrix, order KKRSZ
!         tcinv -- diagonal of tcpa^(-1), order KKRSZ
!
! output:
!         greens -- Green's function, order of KKRSZ*NATOM:
!                == ([1-G0*t]^(-1) - 1 ) * t^(-1),
!                   if t = tcpa, t^(-1) = tcinv
! \ev}

!!DO_NOT_PRINT
      implicit none
!!DO_NOT_PRINT

!!ARGUMENTS:
      complex*16   greens(*)
      integer      kkrsz,natom,ndkkr,idiag
      complex*16   tcpa(*)
      complex*16   tcinv(*)
      integer      itype(natom)
      integer      invswitch,invparam,iprint
      character    istop*10
!!REVISION HISTORY:
! Adapted - A.S. - 2013
!EOP
!
!BOC
!c      integer      mapstr(ndimbas,natom),mappnt(ndimbas,natom)
!c      integer      mapij(2,*)

!c     ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
      character    sname*10
      parameter    (sname='calgreen')
!c     ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!c
!c  invert matrix to get (1-G*t)^(-1) ...........................
!c  ---------------------------------------------------------------------

      call inv1mgt(greens,kkrsz*natom,kkrsz,natom,tcpa,                 &
     &                  ndkkr,idiag,                                    &
     &                  itype,                                          &
     &                  invswitch,invparam,                             &
     &                  iprint,istop)

!c  Now greens = [1-G*t]^(-1)  if t = tcpa
!c        ---------------------------------------------------------------

      call matrm1t(greens,tcinv,ndkkr,itype,                            &
     &             natom,kkrsz,idiag)

!c  Now greens = ([1-G*t]^(-1) - 1 ) * t^(-1)  if t^(-1) = tcinv
!c     ==================================================================
      if (istop.eq.sname) call fstop(sname)
!c

      return
!EOC
      end subroutine calgreen
