!BOP
!!ROUTINE: makedrs
!!INTERFACE:
      subroutine makedrs(xk1,xk2,xk3,                                   &
     &                   pdu,                                           &
     &                   lmax,                                          &
     &                   rsn,nrsn,ndimrs,                               &
     &                   hplnm,ndimr,                                   &
     &                   hank,ndimhnr,                                  &
     &                   icount,                                        &
     &                   dlm)
!!DESCRIPTION:
! calculates D$_{lm}$'s in using real space sum
! {\bv
!   dlm() = sum( pdu*exp( i xk*rsn )*hank()*conjg( Ylm() ) )
! \ev}
!!USES:
      use mecca_constants

!!DO_NOT_PRINT
      implicit real*8   (a-h,o-z)
!!DO_NOT_PRINT

!!ARGUMENTS:
      real*8      xk1,xk2,xk3
      complex*16  pdu
      integer     lmax
      integer     ndimrs,nrsn
      real*8      rsn(ndimrs,3)
      integer     ndimr
      complex*16  hplnm(ndimr,*)
      integer     ndimhnr
      complex*16  hank(ndimhnr,*)
      integer     icount
      complex*16  dlm((2*lmax+1)**2)
!!REVISION HISTORY:
! Adapted - A.S. - 2013
!EOP
!
!cab      complex*16  ylm(iprsn,ipdlj)
!BOC
      real*8      rtfpi
!c
      complex*16  dot
      complex*16  xfac
      complex*16  dlmp,dlmm
      complex*16  raijh(nrsn,2*lmax+1)

!c parameter
      complex*16  sqrtm1
      parameter (sqrtm1=(0.d0,1.d0))

      rtfpi=sqrt(four*pi)

!c     -------------------------------------------------------------
      call zerooutC(dlm,(2*lmax+1)**2)
!c     -------------------------------------------------------------
      if(icount.eq.1) then
        nstart=2
        dlm(1)=-sqrtm1*pdu/rtfpi
      else
        nstart=1
      endif
!c     -------------------------------------------------------------
      do ir=nstart,nrsn
        raij1=rsn(ir,1)
        raij2=rsn(ir,2)
        raij3=rsn(ir,3)
        dot=dcmplx( zero,xk1*raij1+xk2*raij2+xk3*raij3 )
        xfac=pdu*exp(dot)
        rir = one/sqrt(raij1*raij1+raij2*raij2+raij3*raij3)
        rl = one
        do l=0,2*lmax
         raijh(ir,l+1) = rl*xfac*hank(ir,l+1)
!c
!c                hplnm = r**l*Ylm, therefore raijh() contains 1/r**l
!c
         rl = rl*rir
        end do
      end do

!CAB          dlm(nd)=dlm(nd) + xfac*hank(ir,ndx(nd))*conjg( ylm(ir,nd) )
      lm = 0
      do l=0,2*lmax
       l0 = l*( l + 1 ) + 1
!c  M=0
       lm = lm+1
       dlmp = dcmplx(zero,zero)
       do ir=nstart,nrsn
          dlmp=dlmp + raijh(ir,l+1)*hplnm(ir,lm)
       enddo
       dlm(l0)=dlm(l0)+dlmp
!c  M<>0
       do m=1,l
        lm = lm+1
        dlmp = dcmplx(zero,zero)
        dlmm = dcmplx(zero,zero)
        onem = (-1)**m

        do ir=nstart,nrsn
          dlmp=dlmp + raijh(ir,l+1)*hplnm(ir,lm)
          dlmm=dlmm + raijh(ir,l+1)*conjg(hplnm(ir,lm))*onem
        enddo
        dlm(l0+m) = dlmp
        dlm(l0-m) = dlmm
       enddo
      enddo

      return
!EOC
      end subroutine makedrs
