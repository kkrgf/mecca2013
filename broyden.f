!BOP
!!ROUTINE: broyden
!!INTERFACE:
      subroutine broyden(rhot,rhin,komp,nbasis,mspn,jtop,obroy1,obroy2, &
     &                        iflag,alpha,beta)
!!DESCRIPTION:
! Broyden mixing based on subroutine of D.D. Johnson 
! (see Phys. Rev. B38, 12807 (1988), but there are a few typo's 
!  in that paper!)

!!DO_NOT_PRINT
      implicit none
!!DO_NOT_PRINT

!!ARGUMENTS:
      real(8), intent(inout) ::   rhot(:,:,:,:)  ! (iprpts,ipcomp,ipsubl,ipspin)
      real(8), intent(in)    ::   rhin(:,:,:,:)  ! (iprpts,ipcomp,ipsubl,ipspin)
      integer, intent(in)  :: nbasis,komp(nbasis),mspn,jtop
      character(*), intent(in) :: obroy1,obroy2  ! file names
      integer, intent(in) :: iflag
      real(8), intent(in)  :: alpha
      real(8), intent(in), optional :: beta
!!REVISION HISTORY:
! Modified -  A.S. - 2016
!!REMARKS:
!EOP
!
!BOC
!c************************************************************
!c komp  = number of components in alloy
!c nbasis= number of sublattices in alloy
!c mspn  = number of electron spin components (1 or 2)
!c jtop  = size of vector to be used in mixing
!c alpha = empirical mix parameter, non-magnetic (about .01 -> .5)
!c beta  is for magnetic mixing
!c************************************************************
!c
      integer, parameter :: imatsz=40
      real(8) :: f(size(rhin))
      real(8) :: dumvi(size(rhin))
      real(8) :: vector(size(rhin),2)
      real(8) :: bt
      integer :: ivsiz,lastit,st_env
      integer :: nu1,nu2
      integer, external :: broyden_nunit
!
      call gEnvVar('ENV_BROYDEN',.false.,st_env)
      if ( st_env<= 0 ) then
       st_env = iflag
      end if
      select case(st_env) 
       case (1)
        call xbroyden1(rhot,rhin,alpha,komp,nbasis,                     &
     &                       mspn,jtop,obroy1,obroy2)
       case (2)
        if ( present(beta) ) then
         bt = beta
        else
         bt = 0.75d0
        end if
        call xbroyden2(rhot,rhin,alpha,bt,komp,nbasis,                  &
     &                       mspn,jtop,obroy1,obroy2)
       case default
        call xbroyden1(rhot,rhin,alpha,komp,nbasis,                     &
     &                       mspn,jtop,obroy1,obroy2)
      end select

      return
!c
!EOC
      contains 

!BOP
!!IROUTINE: xbroyden1
!!INTERFACE:
      subroutine xbroyden1(rhot,rhin,                                   &
     &                        alpha,komp,nbasis,                        &
     &                       mspn,jtop,obroy1,obroy2)
!!DESCRIPTION:
! Broyden mixing, spin up/dn are on the same footing as sublattices

!!DO_NOT_PRINT
      implicit none
!!DO_NOT_PRINT

!!ARGUMENTS:
      real(8), intent(inout) ::   rhot(:,:,:,:)  ! (iprpts,ipcomp,ipsubl,ipspin)
      real(8), intent(in)    ::   rhin(:,:,:,:)  ! (iprpts,ipcomp,ipsubl,ipspin)
      real(8), intent(in)  :: alpha
      integer, intent(in)  :: nbasis,komp(nbasis),mspn,jtop
      character(*), intent(in) :: obroy1,obroy2  ! file names
!!REVISION HISTORY:
! Modified -  A.S. - 2016
!!REMARKS:
! private procedure of subroutine broyden
!EOP
!
!BOC
!c************************************************************
!c alpha = empirical mix parameter, non-magnetic (about .04 -> .5)
!c beta  is redundant
!c komp  = number of components in alloy
!c nbasis= number of sublattices in alloy
!c mspn  = number of electron spin components (1 or 2)
!c jtop  = size of vector to be used in mixing
!c************************************************************
!c*  the vectors ui(maxsiz) and vti(maxsiz) are johnson's    *
!c*  u(of i)  and df(transpose), respectively. these are     *
!c*  continually updated. all iterations are stored in files.*
!c*  vector ul is the vt of earlier iterations. vector f is: *
!c*  vector(output) - vector(in). vector df is:  f(m+1)-f(m) *
!c*  finally,vector dumvi(maxsiz) is the predicted vector.   *
!c************************************************************
!c
!      integer, intent(in)  :: iter
!DEBUG      real(8), intent(out) ::   xvalws(:,:,:)  ! (ipcomp,ipsubl,ipspin)
!c++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!c
!c***** arrays strictly for Broyden construction: do not alter *****
      real(8) :: a(imatsz,imatsz),b(imatsz,imatsz),cm(imatsz)
      real(8) :: d(imatsz,imatsz),w(imatsz)
      real(8) :: td(imatsz),bd(imatsz)
      integer :: ad(imatsz)

      real(8) :: ui(size(rhin))
      real(8) :: vti(size(rhin))
      real(8) :: t1(size(rhin))
      real(8) :: df(size(rhin))
      integer :: ib,ms,nc,k,is,jtop1,i,j
      integer :: lastm1,lastm2,lm,ln,ltmp,indx
      integer :: maxsiz
      real(8) :: amix,fnorm,dfnorm,gmi,wtmp,alpha_in
      real(8) :: aij,cmj,fac1,fac2,w0
      real(8), parameter :: w0c=0.0001d0  ! weighting factor for the zeroth iteration
      real(8), parameter :: zero=0.d0,one=1.d0,two=2.d0
!c
!c===================================================================
      alpha_in = alpha
      w0 = w0c
      maxsiz = size(rhin)
      nu1 = broyden_nunit(1)
      nu2 = broyden_nunit(2)
!c
!c++++++ files for BROYDEN mixing ,                            ++++++
!c  files can be deleted any time (for improved convergence in some cases) 
      open(nu1,file=obroy1,status='unknown',form='unformatted')
      open(nu2,file=obroy2,status='unknown',form='unformatted')
!c
!c++++++ set up the vector of the current iteration for mixing ++++++
!c
!c  For this method we have only saved input/output chg. densities.
!c  For MT/ASA also include XVALWS in mix in jtop+1 array location.
      jtop1=min(jtop+1,size(rhot,1))
!c  Keep track of basis indexing
      indx=0
      do ms=1,mspn
       do ib=1,nbasis
        do nc=1,komp(ib)
         indx=indx+1
         is=(indx-1)*jtop1
         vector(is+1:is+jtop1,1)=rhin(1:jtop1,nc,ib,ms)
         vector(is+1:is+jtop1,2)=rhot(1:jtop1,nc,ib,ms)
        enddo
       enddo
      enddo

!c  ivsiz is the length of the vector
      ivsiz=is + jtop1

      if(ivsiz.gt.maxsiz)then
         write( 6,1001) ivsiz,maxsiz
1001     format(' xbroyden1:: ivsiz =',i10,' maxsiz=',i10/)
         call fstop(' BROYDEN :: exceeded max. vector length')
      endif

!c++++++ end of program specific loading of vector from main ++++++++
!c
!c
!c*******************  begin broyden's method  **********************
!c
!c      f:  the difference of previous output and input vectors
!c  dumvi:  a dummy vector, here it is the previous input vector
      rewind nu1
      read(nu1,err=119,end=119)amix,lastit
!CAB
!---------------------------------------------------------------      
! not part of original DDJ algorithm      
      if(lastit.ge.imatsz-1.or.lastit.le.0) then
       go to 119
      end if
      if(alpha >= amix*1.1d0) then   ! empirical rule
       go to 119
      end if
!---------------------------------------------------------------      
!CAB
      read(nu1,err=119,end=119)(f(k),k=1,ivsiz)
      read(nu1,err=119,end=119)(dumvi(k),k=1,ivsiz)

!      if(iter.eq.1 .and. lastit.gt.1)then
      if(lastit.gt.1)then
       w0 = zero
       read(nu1)ltmp,((a(i,j),i=1,ltmp),j=1,ltmp)
       read(nu1)(w(i),i=1,ltmp)
      else
       a=zero
       w=zero 
      endif

      lastit=lastit+1
      lastm1=lastit-1
      lastm2=lastit-2
!c
!c  alpha (or amix, when saved) is previously chosen
!c  simple mixing parameters for rho and mag.
!c
      dumvi(1:ivsiz) = vector(1:ivsiz,1) - dumvi(1:ivsiz)
      df(1:ivsiz) = vector(1:ivsiz,2) - vector(1:ivsiz,1) - f(1:ivsiz) 
      ui(1:ivsiz) =  f(1:ivsiz)   ! temp.save
      f(1:ivsiz)  = vector(1:ivsiz,2) - vector(1:ivsiz,1) 
!c
!c  for i-th iter.,dfnorm is ( f(i) minus f(i-1) ), used for normalization.
      dfnorm = sqrt( dot_product(df(1:ivsiz),df(1:ivsiz)) )
      fnorm  = sqrt( dot_product(f(1:ivsiz),f(1:ivsiz)) )
!
!---------------------------------------------------------------      
! not part of original DDJ algorithm      
      if(lastm2>1) then
       if ( w(lastm2) > one ) then
        wtmp = exp(w(lastm2)/(-two))
       else
        wtmp = one/w(lastm2)   
       end if   
       if ( fnorm < w0c ) then
        if ( fnorm > wtmp ) then
         amix = wtmp/fnorm*amix
         call prev_iteration_mix(amix)
         go to 120
        end if
       else
        if ( fnorm > one ) then
         if ( fnorm > wtmp ) then
          amix = wtmp/fnorm*amix
          call prev_iteration_mix(amix)
          go to 120
         end if
        end if
       end if
       if ( fnorm < one ) then   
        if ( abs(sum(a(1:lastm2,lastm2)))/lastm2 < w0c ) then
          go to 119
        end if  
        if ( lastm2>2 ) then   
!        to have better convergence in GGA+empty_spheres cases            
         if ( w(lastm2)<w(lastm2-1) ) go to 119
        end if 
       end if  
      end if
!---------------------------------------------------------------      
!c
      ! amix = alpha_in     !???
      fac2=one/dfnorm
      fac1=amix*fac2
      ui(1:ivsiz)  = fac1*df(1:ivsiz) + fac2*dumvi(1:ivsiz)
      vti(1:ivsiz) = fac2*df(1:ivsiz)
!c
!c*********** calculation of coefficient matrices *************
!c***********    and the sum for corrections      *************
!c
!c recall: a(i,j) is a symmetric matrix
!c       : b(i,j) is the inverse of [ w0**2 I + a ]
!c
!c
!c dumvi is the u(of i) and t1 is the vt(of i)
!c from the previous iterations
      rewind nu2
      if(lastit.gt.2)then
        do j=1,lastm2
         read(nu2)  ! (dumvi(k),k=1,ivsiz)
         read(nu2)(t1(k),k=1,ivsiz)
!c
         aij=dot_product(t1(1:ivsiz),vti(1:ivsiz))
         cmj=dot_product(t1(1:ivsiz),f(1:ivsiz))

         a(lastm1,j)=aij
         a(j,lastm1)=aij
         cm(j)      =cmj
        enddo
      endif
!c
      aij=dot_product(vti(1:ivsiz),vti(1:ivsiz))
      cmj=dot_product(vti(1:ivsiz),f(1:ivsiz))

      a(lastm1,lastm1)=aij
      cm(lastm1)      =cmj
!c
      write(nu2)(ui(k),k=1,ivsiz)
      write(nu2)(vti(k),k=1,ivsiz)
      rewind nu2
!c
!c the weighting factors for each iteration have been chosen
!c equal to one over the r.m.s. error. This need not be the case.
!c       wtmp=0.010D0/fnorm
!       if(wtmp.lt. one)wtmp=one

!---------------------------------------------------------------      
! not part of original DDJ algorithm      
       if(fnorm.lt.1) then
          wtmp = -two*log(fnorm)
       else
          wtmp = one/fnorm
       end if
!---------------------------------------------------------------      

       w(lastm1)=wtmp

!c
!c
!c with the current iterations f and vector calculated,
!c write them to unit nu1 for use later.
      rewind nu1
      write(nu1)amix,lastit
      write(nu1)(f(k),k=1,ivsiz)
      write(nu1)(vector(k,1),k=1,ivsiz)
      write(nu1)lastm1,((a(i,j),i=1,lastm1),j=1,lastm1)
      write(nu1)(w(i),i=1,lastm1)
!c
!c set up and calculate beta matrix
!DEBUG_PRINT      
!      write(*,'(''DEBUG_BR: w='',3g13.4)')                              &
!     &                           (w(i),i=lastm1,max(1,lastm1-2),-1)
!      write(*,'(''DEBUG_BR: '',i4,g13.4)')lastm1,sum(a(1:lastm1,lastm1))
!DEBUG_PRINT      
      do lm=1,lastm1
       do ln=1,lastm1
         d(ln,lm)= a(ln,lm)*w(ln)*w(lm)
         b(ln,lm)= zero
       enddo
         b(lm,lm)= one
         d(lm,lm)= w0**2 + a(lm,lm)*w(lm)*w(lm)
      enddo
!c
      call invert2(imatsz,d,b,lastm1,td,ad,bd)
      if (bd(1) .ne. zero) go to 119
!c
!c  calculate the vector for the new iteration
      dumvi(1:ivsiz) = vector(1:ivsiz,1) + amix*f(1:ivsiz)
!c
      do i=1,lastm1
        read(nu2)(ui(k),k=1,ivsiz)
        read(nu2)  ! (vti(k),k=1,ivsiz)
        gmi = dot_product(cm(1:lastm1),b(1:lastm1,i)*w(1:lastm1))
!        gmi=zero
!        do ip=1,lastm1
!          gmi=gmi + cm(ip)*b(ip,i)*w(ip)
!        enddo
        dumvi(1:ivsiz) = dumvi(1:ivsiz) - (gmi*w(i))*ui(1:ivsiz)
!        do k=1,ivsiz
!           dumvi(k)=dumvi(k)-gmi*ui(k)*w(i)
!        enddo      ! End of k-loop
      enddo        ! End of i-loop
!c  end of the calculation of dumvi, the new vector
!c
      rewind nu1
      rewind nu2
      close(nu2,status='keep')
!c
      goto 120
!c if this is the first iteration, then load
!c    f=vector(out)-vector(in) and vector(in)
  119 continue
      close(nu2,status='delete')
      rewind nu1
      lastit=1
!c  alpha is simple mixing parameters for rho and mag.
      amix=alpha_in
      write(nu1)amix,lastit
      f(1:ivsiz)  = vector(1:ivsiz,2) - vector(1:ivsiz,1) 
      write(nu1)(f(k),k=1,ivsiz)
      write(nu1)(vector(k,1),k=1,ivsiz)
      endfile(unit=nu1)
!c
!c since we are on the first iteration, simple mix the vector.
      dumvi(1:ivsiz) = vector(1:ivsiz,1) + amix*f(1:ivsiz)
!cab      write( 6,1000)
  120 continue
!DEBUG_PRINT      
!      write(*,'(''DEBUG_BR: amix='',g13.4)') amix
!DEBUG_PRINT      
!c
      close(nu1,status='keep')
!      close(nu2,status='keep')
!c
!c*************  the end of the broyden method **************
!c
!c+++++++ program specific code of reloading arrays +++++++++
!c
!c need to unload the new vector into the appropriate arrays.
       indx=0
       do ms=1,mspn
        do ib=1,nbasis
         do nc=1,komp(ib)
          indx=indx+1
          is=(indx-1)*jtop1
          rhot(1:jtop1,nc,ib,ms) = dumvi(is+1:is+jtop1)
         enddo
        enddo
       enddo
!c
!c+++++++++ end of program specific reloading of arrays +++++++++
!c
      return
      end subroutine xbroyden1

!BOP
!!IROUTINE: xbroyden2
!!INTERFACE:
      subroutine xbroyden2(rhot,rhin,                                   &
     &                        alpha,beta,komp,nbasis,                   &
     &                       mspn,jtop,obroy1,obroy2)
!!DESCRIPTION:
! Broyden mixing for non-magnetic subsystem, 
! simple mixing for magnetic one

!!DO_NOT_PRINT
      implicit none
!!DO_NOT_PRINT

!!ARGUMENTS:
      real(8), intent(inout) ::   rhot(:,:,:,:)  ! (iprpts,ipcomp,ipsubl,ipspin)
      real(8), intent(in)    ::   rhin(:,:,:,:)  ! (iprpts,ipcomp,ipsubl,msphat,ipspin)
      real(8), intent(in)  :: alpha,beta
      integer, intent(in)  :: nbasis,komp(nbasis),mspn,jtop
      character(*), intent(in) :: obroy1,obroy2  ! file names
!!REVISION HISTORY:
! Modified -  A.S. - 2013
!!REMARKS:
! private procedure of subroutine broyden
!EOP
!
!BOC
!c************************************************************
!c alpha = empirical mix parameter, non-magnetic (about .04 -> .5)
!c beta  = empirical mix parameter, magnetic (about .5 -> .8)
!c komp  = number of components in alloy
!c nbasis= number of sublattices in alloy
!c mspn  = number of electron spin components (1 or 2)
!c jtop  = size of vector to be used in mixing
!c************************************************************
!c*  the vectors ui(maxsiz) and vti(maxsiz) are johnson's    *
!c*  u(of i)  and df(transpose), respectively. these are     *
!c*  continually updated. all iterations are stored in files.*
!c*  vector ul is the vt of earlier iterations. vector f is: *
!c*  vector(output) - vector(in). vector df is:  f(m+1)-f(m) *
!c*  finally,vector dumvi(maxsiz) is the predicted vector.   *
!c************************************************************
!c
!      integer, intent(in)  :: iter
!DEBUG      real(8), intent(out) ::   xvalws(:,:,:)  ! (ipcomp,ipsubl,ipspin)
!c++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!c
!c***** arrays strictly for Broyden construction: do not alter *****
      integer, parameter :: imatsz=40
      real(8) :: a(imatsz,imatsz),b(imatsz,imatsz),cm(imatsz)
      real(8) :: d(imatsz,imatsz),w(imatsz)
      real(8) :: td(imatsz),bd(imatsz)
      integer :: ad(imatsz)

!      real(8) :: f(size(rhin,1)*size(rhin,2)*size(rhin,3))
      real(8) :: ui(size(rhin,1)*size(rhin,2)*size(rhin,3))
      real(8) :: vti(size(rhin,1)*size(rhin,2)*size(rhin,3))
      real(8) :: t1(size(rhin,1)*size(rhin,2)*size(rhin,3))
!      real(8) :: vector(size(rhin,1)*size(rhin,2)*size(rhin,3),2)
!      real(8) :: dumvi(size(rhin,1)*size(rhin,2)*size(rhin,3))
      real(8) :: df(size(rhin,1)*size(rhin,2)*size(rhin,3))
      integer :: ib,nc,k,is,jtop1,i,j,nspm1
      integer :: lastm1,lastm2,lm,ln,ltmp,indx
      integer :: maxsiz
      real(8) :: amix,fnorm,dfnorm,gmi,sp,wtmp,xx,alpha_in
      real(8) :: a1,b1,aij,cmj,fac1,fac2,w0
      real(8), parameter :: w0c=0.0001d0  ! weighting factor for the zeroth iteration
      real(8), parameter :: zero=0.d0,one=1.d0,two=2.d0
!c
!c===================================================================
      alpha_in = alpha
      w0 = w0c
      maxsiz = size(rhin,1)*size(rhin,2)*size(rhin,3)
      nu1 = broyden_nunit(1)
      nu2 = broyden_nunit(2)
!c
!c++++++ files for BROYDEN mixing ,                            ++++++
!c  files can be deleted any time (for improved convergence in some cases) 
      open(nu1,file=obroy1,status='unknown',form='unformatted')
      open(nu2,file=obroy2,status='unknown',form='unformatted')
!c
!c++++++ set up the vector of the current iteration for mixing ++++++
!c
!c  For this method we have only saved input/output chg. densities.
!c  For MT/ASA also include XVALWS in mix in jtop+1 array location.
      jtop1=min(jtop+1,size(rhot,1))
!c  Keep track of basis indexing
       indx=0
      do ib=1,nbasis
       do nc=1,komp(ib)
           indx=indx+1
           is=(indx-1)*jtop1
!c
         do k=1,jtop1
           vector(k+is,1)=rhin(k,nc,ib,1) + (mspn-1)*rhin(k,nc,ib,mspn)
           vector(k+is,2)=rhot(k,nc,ib,1) + (mspn-1)*rhot(k,nc,ib,mspn)
         enddo
       enddo
      enddo

!c  ivsiz is the length of the vector
      ivsiz=is + jtop1

      if(ivsiz.gt.maxsiz)then
         write( 6,1001) ivsiz,maxsiz
1001     format(' xbroyden2:: ivsiz =',i10,' maxsiz=',i10/)
         call fstop(' BROYDEN :: exceeded max. vector length')
      endif

!c++++++ end of program specific loading of vector from main ++++++++
!c
!c
!c*******************  begin broyden's method  **********************
!c
!c      f:  the difference of previous output and input vectors
!c  dumvi:  a dummy vector, here it is the previous input vector
      rewind nu1
      read(nu1,err=119,end=119)amix,lastit
!CAB
!---------------------------------------------------------------      
! not part of original DDJ algorithm      
      if(lastit.ge.imatsz-1.or.lastit.le.0) then
       go to 119
      end if
      if(alpha >= amix*1.1d0) then   ! empirical rule
       go to 119
      end if
!---------------------------------------------------------------      
!CAB
      read(nu1,err=119,end=119)(f(k),k=1,ivsiz)
      read(nu1,err=119,end=119)(dumvi(k),k=1,ivsiz)

!      if(iter.eq.1 .and. lastit.gt.1)then
      if(lastit.gt.1)then
       w0 = zero
       read(nu1)ltmp,((a(i,j),i=1,ltmp),j=1,ltmp)
       read(nu1)(w(i),i=1,ltmp)
      else
       a=zero
       w=zero 
      endif

      lastit=lastit+1
      lastm1=lastit-1
      lastm2=lastit-2
!c
!c  alpha (or amix, when saved) and beta are previously chosen
!c  simple mixing parameters for rho and mag.
!c
      dumvi(1:ivsiz) = vector(1:ivsiz,1) - dumvi(1:ivsiz)
      df(1:ivsiz) = vector(1:ivsiz,2) - vector(1:ivsiz,1) - f(1:ivsiz) 
      ui(1:ivsiz) =  f(1:ivsiz)   ! temp.save
      f(1:ivsiz)  = vector(1:ivsiz,2) - vector(1:ivsiz,1) 
!c
!c  for i-th iter.,dfnorm is ( f(i) minus f(i-1) ), used for normalization.
      dfnorm = sqrt( dot_product(df(1:ivsiz),df(1:ivsiz)) )
      fnorm  = sqrt( dot_product(f(1:ivsiz),f(1:ivsiz)) )
!
!---------------------------------------------------------------      
! not part of original DDJ algorithm      
      if(lastm2>1) then
       if ( w(lastm2) > one ) then
        wtmp = exp(w(lastm2)/(-two))
       else
        wtmp = one/w(lastm2)   
       end if   
       if ( fnorm < w0c ) then
        if ( fnorm > wtmp ) then
         amix = wtmp/fnorm*amix
         call prev_iteration_mix(amix)
         go to 120
        end if
       else
        if ( fnorm > one ) then
         if ( fnorm > wtmp ) then
          amix = wtmp/fnorm*amix
          call prev_iteration_mix(amix)
          go to 120
         end if
        end if
       end if
       if ( fnorm < one ) then   
        if ( abs(sum(a(1:lastm2,lastm2)))/lastm2 < w0c ) then
          go to 119
        end if  
        if ( lastm2>2 ) then   
!        to have better convergence in GGA+empty_spheres cases            
         if ( w(lastm2)<w(lastm2-1) ) go to 119
        end if 
       end if  
      end if
!---------------------------------------------------------------      
!c
      ! amix = alpha_in     !???
      fac2=one/dfnorm
      fac1=amix*fac2
      ui(1:ivsiz)  = fac1*df(1:ivsiz) + fac2*dumvi(1:ivsiz)
      vti(1:ivsiz) = fac2*df(1:ivsiz)
!c
!c*********** calculation of coefficient matrices *************
!c***********    and the sum for corrections      *************
!c
!c recall: a(i,j) is a symmetric matrix
!c       : b(i,j) is the inverse of [ w0**2 I + a ]
!c
!c
!c dumvi is the u(of i) and t1 is the vt(of i)
!c from the previous iterations
      rewind nu2
      if(lastit.gt.2)then
        do j=1,lastm2
         read(nu2)  ! (dumvi(k),k=1,ivsiz)
         read(nu2)(t1(k),k=1,ivsiz)
!c
         aij=dot_product(t1(1:ivsiz),vti(1:ivsiz))
         cmj=dot_product(t1(1:ivsiz),f(1:ivsiz))

         a(lastm1,j)=aij
         a(j,lastm1)=aij
         cm(j)      =cmj
        enddo
      endif
!c
      aij=dot_product(vti(1:ivsiz),vti(1:ivsiz))
      cmj=dot_product(vti(1:ivsiz),f(1:ivsiz))

      a(lastm1,lastm1)=aij
      cm(lastm1)      =cmj
!c
      write(nu2)(ui(k),k=1,ivsiz)
      write(nu2)(vti(k),k=1,ivsiz)
      rewind nu2
!c
!c the weighting factors for each iteration have been chosen
!c equal to one over the r.m.s. error. This need not be the case.
!c       wtmp=0.010D0/fnorm
!       if(wtmp.lt. one)wtmp=one

!---------------------------------------------------------------      
! not part of original DDJ algorithm      
       if(fnorm.lt.1) then
          wtmp = -two*log(fnorm)
       else
          wtmp = one/fnorm
       end if
!---------------------------------------------------------------      

       w(lastm1)=wtmp

!c
!c
!c with the current iterations f and vector calculated,
!c write them to unit nu1 for use later.
      rewind nu1
      write(nu1)amix,lastit
      write(nu1)(f(k),k=1,ivsiz)
      write(nu1)(vector(k,1),k=1,ivsiz)
      write(nu1)lastm1,((a(i,j),i=1,lastm1),j=1,lastm1)
      write(nu1)(w(i),i=1,lastm1)
!c
!c set up and calculate beta matrix
!DEBUG_PRINT      
!      write(*,'(''DEBUG_BR: w='',3g13.4)')                              &
!     &                           (w(i),i=lastm1,max(1,lastm1-2),-1)
!      write(*,'(''DEBUG_BR: '',i4,g13.4)')lastm1,sum(a(1:lastm1,lastm1))
!DEBUG_PRINT      
      do lm=1,lastm1
       do ln=1,lastm1
         d(ln,lm)= a(ln,lm)*w(ln)*w(lm)
         b(ln,lm)= zero
       enddo
         b(lm,lm)= one
         d(lm,lm)= w0**2 + a(lm,lm)*w(lm)*w(lm)
      enddo
!c
      call invert2(imatsz,d,b,lastm1,td,ad,bd)
      if (bd(1) .ne. zero) go to 119
!c
!c  calculate the vector for the new iteration
      dumvi(1:ivsiz) = vector(1:ivsiz,1) + amix*f(1:ivsiz)
!c
      do i=1,lastm1
        read(nu2)(ui(k),k=1,ivsiz)
        read(nu2)  ! (vti(k),k=1,ivsiz)
        gmi = dot_product(cm(1:lastm1),b(1:lastm1,i)*w(1:lastm1))
!        gmi=zero
!        do ip=1,lastm1
!          gmi=gmi + cm(ip)*b(ip,i)*w(ip)
!        enddo
        dumvi(1:ivsiz) = dumvi(1:ivsiz) - (gmi*w(i))*ui(1:ivsiz)
!        do k=1,ivsiz
!           dumvi(k)=dumvi(k)-gmi*ui(k)*w(i)
!        enddo      ! End of k-loop
      enddo        ! End of i-loop
!c  end of the calculation of dumvi, the new vector
!c
      rewind nu1
      rewind nu2
      close(nu2,status='keep')
!c
      goto 120
!c if this is the first iteration, then load
!c    f=vector(out)-vector(in) and vector(in)
  119 continue
      close(nu2,status='delete')
      rewind nu1
      lastit=1
!c  alpha and beta are simple mixing parameters for rho and mag.
      amix=alpha_in
      write(nu1)amix,lastit
      f(1:ivsiz)  = vector(1:ivsiz,2) - vector(1:ivsiz,1) 
      write(nu1)(f(k),k=1,ivsiz)
      write(nu1)(vector(k,1),k=1,ivsiz)
      endfile(unit=nu1)
!c
!c since we are on the first iteration, simple mix the vector.
      dumvi(1:ivsiz) = vector(1:ivsiz,1) + amix*f(1:ivsiz)
!cab      write( 6,1000)
  120 continue
!DEBUG_PRINT      
!      write(*,'(''DEBUG_BR: amix='',g13.4)') amix
!DEBUG_PRINT      
!c
      close(nu1,status='keep')
!      close(nu2,status='keep')
!c
!c*************  the end of the broyden method **************
!c
!c+++++++ program specific code of reloading arrays +++++++++
!c
!c need to unload the new vector into the appropriate arrays.
      xx=one-beta
      nspm1=mspn-1
      sp=one/mspn
!c
!c ************************** simple mix moments, then unload vector!!!!
      indx=0
      do ib=1,nbasis
        do nc=1,komp(ib)
         indx=indx+1
         is=(indx-1)*jtop1
         do k=1,jtop1
             a1= dumvi(k+is)
             b1= xx*( rhin(k,nc,ib,1)-rhin(k,nc,ib,mspn) ) +            &
     &         beta*( rhot(k,nc,ib,1)-rhot(k,nc,ib,mspn) )
              rhot(k,nc,ib,mspn)=sp*nspm1*( a1 - b1 )
              rhot(k,nc,ib,   1)=sp*( a1 + b1*nspm1 )
         enddo
        enddo
      enddo
!c ************************** end simple mix of moments, unloading!!!!
!c
!c+++++++++ end of program specific reloading of arrays +++++++++
!c
      return
      end subroutine xbroyden2

!c
!EOC
!!c     ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
!      subroutine invert1(imatsz,a,b,m,td,ad,bd)
!!c     =============================================================
!      implicit real*8  (a-h,o-z)
!      integer imatsz,m
!      dimension a(imatsz,*),b(imatsz,*)
!      dimension td(*),ad(*),bd(*)
!!c
!!c subroutine to perform gaussian elimination
!!c            no zeros along the diagonal
!!cab      save
!!c
!      n=m
!      if(n.gt.imatsz)then
!       write(6,'('' invert: matrix is too large'')')
!       call fstop(' BROYDEN :: matrix is too large')
!      endif
!!c
!      do i=1,n
!       atmp=a(i,i)
!       if(abs(atmp).lt. 1.0d-08)then
!         write(6,'('' invert: matrix has zero diagonal'',               &
!     &             '' element in the '',i4,'' row'')')i
!         call fstop(' BROYDEN :: matrix has zero diagonal element')
!       endif
!      enddo
!!c
!      if(n.eq.1) go to 605
!!c
!      do i=1,n
!!c
!       do j=1,n
!          td(j)=a(j,i)/a(i,i)
!       enddo
!!c
!!CAB      td(i)=(0.0e+00,0.0e+00)
!        td(i)=0.d0
!!c
!        do k=1,n
!         bd(k)=b(i,k)
!         ad(k)=a(i,k)
!        enddo
!!c
!        do k=1,n
!        do j=1,n
!          b(j,k)=b(j,k)-(td(j)*bd(k))
!          a(j,k)=a(j,k)-(td(j)*ad(k))
!        enddo
!        enddo
!      enddo
!!c
!      do i=1,n
!      do j=1,n
!         b(j,i)=b(j,i)/a(j,j)
!      enddo
!      enddo
!!c
!      return
!!c
!!CAB 605  b(1,1)=1.0e+00/a(1,1)
! 605  b(1,1)=1.d0/a(1,1)
!      return
!      end subroutine invert1
!!c     ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc

!c    cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
!BOP
!!IROUTINE: invert2
!!INTERFACE:
      subroutine invert2(imatsz,a,b,m,work,ipiv,bd)
!!DESCRIPTION:
! real matrix inversion
!
!!REMARKS:
! private procedure of subroutine broyden
!EOP
!
!BOC
      implicit none
      integer imatsz,m
      real*8 a(imatsz,*),b(imatsz,*)
      real*8 work(*)
      integer   ipiv(*)
      real*8 bd(*)
      integer lda,lwork,n,info
!c
!c LAPACK inversion
!c
      lda = imatsz
      n = m
      lwork = n

      b(1:n,1:n) = a(1:n,1:n)

      call dgetrf(n,n,b,lda,ipiv,info)
      bd(1) = dble(info)

      if(info.ne.0) then
       write(*,*)
       write(*,*) ' INFO=',info,' after DGETRF in INVERT'
       return
      end if


      call dgetri(n,b,lda,ipiv,work,lwork,info)
      bd(1) = dble(info)

      if(info.ne.0) then
       write(*,*)
       write(*,*) ' INFO=',info,' after DGETRI in INVERT'
       return
      end if

      return
!EOC
      end subroutine invert2
!      
!BOP
!!IROUTINE: prev_iteration_mix
!!INTERFACE:
      subroutine prev_iteration_mix(alpha)
!!DESCRIPTION:
! mixing after reading from file
!
!!REMARKS:
! private procedure of subroutine broyden
!EOP
!
!BOC
      implicit none
      real(8), intent(in) :: alpha
      real(8) :: a
      integer :: l,k
      close(nu2,status='delete')
      rewind nu1
      lastit=0
      read(nu1,err=9,end=9) a,l
      read(nu1,err=9,end=9)(f(k),k=1,ivsiz)
      read(nu1,err=9,end=9)(dumvi(k),k=1,ivsiz)
      vector(1:ivsiz,1) = dumvi(1:ivsiz) - a*f(1:ivsiz)
      vector(1:ivsiz,2) = vector(1:ivsiz,1) + f(1:ivsiz)   ! ??? just for the case
      ! f(1:ivsiz)  = vector(1:ivsiz,2) - vector(1:ivsiz,1) 
!DEBUG_PRINT      
!      write(*,'(''DEBUG_BR: fn0,fn1='',3g13.4)') wtmp,fnorm
!DEBUG_PRINT      
      lastit=1
      rewind nu1
      ! write(nu1)alpha,lastit
      ! write(nu1)(f(k),k=1,ivsiz)
      ! write(nu1)(vector(k,1),k=1,ivsiz)
      endfile(unit=nu1)
      dumvi(1:ivsiz) = vector(1:ivsiz,1) + alpha*f(1:ivsiz)
    9 return
!EOC
      end subroutine prev_iteration_mix

      end subroutine broyden
