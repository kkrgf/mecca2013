!BOP
!!ROUTINE: conUpRightTempr
!!INTERFACE:
      subroutine conUpRightTempr(epoints,Tempr,                         &
     &                           ebot,eStartGaussLaguerre,              &
     &                           density_of_points,nptsGaussLaguerre,   &
     &                           mu,                                    &
     &                           iprint,unit)
!!DESCRIPTION:
! {\bv
!                     linear grid         eStartGaussLaguerre
!                         |                       |
!                         v       ! Im(e)         v
!                -----------------!---------------- x   x     x       x
!  Gauss-        |                !                 Gauss-Laguerre grid
! Legendre grid->|                !
!                |                !
!                |                !
!        -------------------------!--------------------------------------
!              ebot               !    Real(e)
!
!  constructs up-right energy contour for temperature integration
!
!  epoints             : structure that contains energy points and weights (output)
!  Tempr               : temperature, must be > 0
!  ebot                : bottom of the contour on the real axis
!  eStartGaussLaguerre : start of the Gauss-Laguerre grid on the real axis, > ebot
!  density_of_points   : density of points per 0.1Ry for both linear grids
!  mu                  : chemical potential
!  nptsGaussLaguerre   : number of points for the Gauss-Laguerre grid
!  iprint              : print level control (>0 for print of energies)
! \ev}

!!DO_NOT_PRINT
      use universal_const, only: dp=>DBL_R,zero,one,czero
      use rint_module
      use mecca_types, only: energy_points_t
      implicit   none
!!DO_NOT_PRINT
!!ARGUMENTS:
      type(energy_points_t), intent(inout) :: epoints
      real(dp),              intent(in   ) :: Tempr
      real(dp),              intent(in   ) :: ebot
      real(dp),              intent(in   ) :: eStartGaussLaguerre
      integer,               intent(in   ) :: density_of_points          !!! density per 0.1Ry
      integer,               intent(in   ) :: nptsGaussLaguerre
      real(dp),              intent(in   ) :: mu
      integer,               intent(in   ) :: iprint
      integer, optional,     intent(in   ) :: unit
!!REVISION HISTORY:
! Initial Version - M.D. - 2020
!!REMARKS:
! not a good contour for low temperatures
!EOP
!
!BOC
      real(dp)                    :: eStartGL,Ei
      real(dp),         parameter :: Eimax=1.0_dp                        !!! maximum imaginary part of the contour, >0
      complex(dp),      parameter :: ci = cmplx(0.0_dp,1.0_dp,dp)
      real(dp),         parameter :: pihalf=real(2.0_dp,dp)             &
     &                                  *atan(real(1.0_dp,dp))
      real(dp),          parameter :: density_scaling=real(1.0_dp,dp)
      character(len=*), parameter :: sname='conUpRightTempr'
      integer                     :: unit_,i
      integer                     :: nepmax,nep1,nep1a,nep1b,nep2
      real(dp)                    :: h1,h2,w,x
      real(dp),allocatable        :: xi(:),wi(:)
      complex(dp)                 :: e0,test1,test2,ztmp
      complex(dp), external       :: TemprFun

!c     checks
      if(Tempr <= 0.0_dp) then
        call fstop(sname//':: Tempr <= 0')
      endif

      if(density_of_points < 1) then
        call fstop(sname//':: density of points < 1')
      endif

      if(ebot > eStartGaussLaguerre) then
        call fstop(sname//':: ebot > eStartGaussLaguerre')
      endif

      if(nptsGaussLaguerre < 0 ) then
        call fstop(sname//':: nptsGaussLaguerre < 0 ')
      endif

      unit_=6
      if(present(unit)) then
       unit_=unit
      endif

!c    contour bondaries
      eStartGL = max(eStartGaussLaguerre,one)                            !!! should be some value > 0
      Ei       = min(Eimax,pihalf*Tempr)
!c
!c    first leg:  (ebot,0) to (ebot,Ei) using a Gauss-Legendre grid, if necessary an additional linear grid
      nep1=max(20,                                                      &
     & ceiling(density_scaling*Ei*real(density_of_points,dp)))
      h1=Ei/real(nep1-1,dp)
      nep1a=nep1                                                          !!! all points on Gauss-Legendre grid
      nep1b=0
      if(nep1>50) then
        nep1a=32                                                          !!! using a maxinum of points for Gauss-Legendre grid
        nep1b=nep1-nep1a+1                                                !!! remaining points for linear grid
      endif
      nepmax=max(nep1a,nep1b)

!c    second leg: (ebot,Ei) to (etop,Ei) using a linear grid
      nep2=max(10,                                                      &
     & ceiling(density_scaling*(eStartGL-ebot)*                         &
     & real(density_of_points,dp)))
      h2=(eStartGL-ebot)/real(nep2-1,dp)
      nepmax=max(nepmax,nep2)
     
!c    third leg (etop,Ei) to (infinity,Ei) using a Gauss-Laguerre grid
      nepmax=max(nepmax,nptsGaussLaguerre)

!c    allocating output object with correct size and temporary arrays
      epoints%nume=0
      call epoints%resize(nep1a+nep1b+nep2+nptsGaussLaguerre+1)
      epoints%nume=0
      allocate(xi(nepmax),wi(nepmax))


!c    Gauss-Legendre part of leg1
      test1=czero
      call gauleg(zero,h1*real(nep1a-1,dp),xi,wi,nep1a)
      do i=1,nep1a
         epoints%egrd(i)=cmplx(ebot,real(xi(i),dp),dp)
         epoints%wgrd(i)=ci*wi(i)
         test1=test1+epoints%wgrd(i)
      enddo ! i
      epoints%nume=nep1a

!c    linear part of leg1
      if(nep1b>0) then
      call rint_get_weights(nep1b,min(14,nep1b/2),h1,wi)
      do i=1,nep1b
         epoints%nume=epoints%nume+1
         epoints%egrd(epoints%nume)=cmplx(ebot,                          &
     &                                    h1*real(nep1a+i-2,dp) ,dp)
         epoints%wgrd(epoints%nume)=ci*wi(i)
         test1=test1+epoints%wgrd(epoints%nume)
      enddo ! i

      endif
    
!c    linear part of leg2
      test2=czero
      if(nep2>0) then
      call rint_get_weights(nep2,min(14,nep2/2),h2,wi)
      do i=1,nep2
         epoints%nume=epoints%nume+1
         epoints%egrd(epoints%nume)=cmplx(ebot+h2*real(i-1,dp),Ei,dp)
         epoints%wgrd(epoints%nume)=wi(i)
         test2=test2+epoints%wgrd(epoints%nume)
      enddo ! i
         
      endif

!c    leg3, Gauss-Laguerre
      if(nptsGaussLaguerre>0) then
         e0=cmplx(eStartGL,Ei,dp)
         call lagzo(nptsGaussLaguerre,xi,wi)

         do i=1,nptsGaussLaguerre
           w=Tempr*wi(i)*exp(xi(i))
           x=Tempr*xi(i)
!c          if(abs(w*exp((mu-x)/Tempr)).lt.1.0D-60) then
!c             write(unit_,'(i3,". Gauss-Laguerre point, tiny weight:",  &
!c    &                 1x,a,1x,g24.15)') "(skipping), w=",i,w
!c             cycle
!c          endif ! tiny weight
           epoints%nume=epoints%nume+1
           epoints%egrd(epoints%nume)=cmplx(x,zero,dp)+e0
           epoints%wgrd(epoints%nume)=cmplx(w,zero,dp)
         enddo ! i
      endif

!c    point with mu
!c     epoints%nume=epoints%nume+1
!c     epoints%egrd(epoints%nume)=cmplx(mu,0.003_dp,dp)
!c     epoints%wgrd(epoints%nume)=cmplx(zero,zero,dp)



!c    printout
      if(unit_ > 0 .and. iprint > -3 .and. .true.) then
      write(unit_,'("Tempr                   :",f16.8)') Tempr
      write(unit_,'("ebot                    :",f16.8)') ebot
      write(unit_,'("mu                      :",f16.8)') mu
      write(unit_,'("eStartGaussLaguerre(in) :",f16.8)')                &
     &               eStartGaussLaguerre
      write(unit_,'("eStartGaussLaguerre     :",f16.8)') eStartGL
      write(unit_,'("density of points/0.1Ry :",i4   )')                &
     &               density_of_points
      write(unit_,'("imaginary part          :",f16.8)') Ei
      
!c    printing contour points
      do i=1,epoints%nume
         write(unit_,'(i6,". ",2(f16.8,1x),5x,2(f16.8,1x))')            &
     &   i,epoints%egrd(i),epoints%wgrd(i)
      enddo ! i
      write(unit_,'()')
      write(unit_,'("test1:",2ES16.8)') test1-cmplx(zero,Ei,dp)
      write(unit_,'("test2:",2ES16.8)') test2-                          &
     &                                      cmplx(eStartGL-ebot,zero,dp)

      endif ! unit_ > 0

      deallocate(xi,wi)

!c
!c  takes into account Fermi function
!c
      do i=1,epoints%nume
        ztmp = (epoints%egrd(i)-cmplx(mu,0.0_dp,dp))/Tempr
        epoints%wgrd(i) = epoints%wgrd(i)*TemprFun(ztmp,0)
      end do



!c     stop "STOP MD"

!EOC
      return
      end subroutine conUpRightTempr
