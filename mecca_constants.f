!BOP
!!MODULE: mecca_constants
!!INTERFACE:
      module mecca_constants
!!DESCRIPTION:
! to define  global mecca-related constants and limitation parameters
!
!!USES:
      use universal_const
!
!!REVISION HISTORY:
! Initial Version - A.S. - 2013
!EOP
!
!BOC
      implicit none
!      real(8), public, protected :: xr_mesh_step=0.00225      ! log(r)-mesh step
!      real(8), public, protected :: xr_mesh_step=0.0045       ! log(r)-mesh step
      real(8), public, protected :: xr_mesh_step=0.009       ! log(r)-mesh step
!
!C   ******************************************************************
!C   *                                                                *
!C   *    The array dimensions correspond to the maximum number of    *
!C   *    the variables specified below.                              *
!C   *                                                                *
!C   *    iprpts  : No. of radial mesh points. (must be odd)          *
!C   *    ipcomp  : No. of components.                                *
!C   *    ipsublat: No. of inequiv.sublattices                        *
!C   *    ipspin  : No. of spin variables, 1 or 2.                    *
!C   *    ipepts  : No. of energy points.                             *
!c   *                                                                *
!C   *    ipeval  : No. of allowed core states, including semicore    *
!C   *    iprsn   : No. of real-space vectors for Ewald method        *
!C   *    ipxkns  : No. of k-space vectors for Ewald method           *
!c   *                                                                *
!C   ******************************************************************
!
!C INTEGERS
      integer, parameter :: iprpts=1301              ! max. pts. on r grid (odd only), for older version
      integer, parameter :: ipcomp=16                ! max. species per sublattice
      integer, parameter :: ipspin=2                 ! max. spin manifolds
!moved in setEgrid      integer, parameter :: ipepts=300               ! max. pts. on E contour
      integer, parameter :: ipeval=49                ! max. core eigenvalues

      integer, parameter :: ipxkns=400*25            ! max. k-vectors in Ewald sum (per k-cell)
!CAB                 ??? ipxkns ~ ipbase**(1/3) ???
      integer, parameter :: iprsn=2000               ! max. r-vectors in Ewald sum (per r-cell)
      integer, parameter :: iprij = iprsn*25         ! upper limit for the number of all r-vectors in Ewald sum
                                                     ! iprsn <= iprij <= iprsn*ipbase^2
      integer, parameter :: iprhp = iprij            ! upper limit for the number of all harm.polinomials
                                                     ! stored in memory (real space part)
                                                     ! 1 <= iprhp <= iprij
      integer, parameter :: ipunit=30                ! max. number of input/output files
!CAB ???

!c  ANG. MOMENTUM SECTION
!
!c   *    iplmax  : No. of l-quantum numbers - lmax                   *
!c   *    ipkkr   : No. of {l,m} quantum numbers.  (lmax+1)^2         *
!c
!      integer, parameter :: iplmax=4         ! max. ang. momentum
!      integer, parameter :: iplmax=8         ! max. ang. momentum

!      integer, parameter :: ipkkr=(iplmax+1)**2
!      integer, parameter :: ipdlj=(2*iplmax+1)**2     ! dlm(ewald)
!      integer, parameter :: lcllmax=iplmax   ! "local" lmax (internal sum?)
      integer, parameter :: ipmltp=0  ! multipoles: 0 - no multipoles, 1 - dipoles, ..
!      integer, parameter :: ipmltp=4  ! r^l-multipoles: 0 - no multipoles, 1 - dipoles, ..
      integer, parameter :: ipmltm=0  ! r^(-l-1)-multipoles

!c  VP SECTION
!
      integer, parameter :: ipnnmax = 84            ! max number of VP-NN
      integer, parameter :: ipvtx  = 2+2*ipnnmax    ! max number of VP-vertices
      integer, parameter :: ipfvtx = 28    ! max number of face-vertices
!c   integration outside MT-region
!      integer, parameter :: ipmtout=0  ! ipmtout is a last R-moment of rho(outside MT)
!        integer, parameter :: ipmtout=4  ! ipmtout is a last R-moment of rho(outside MT)
!
!c=======================================================================
!c  NO COMPLEXES
!c=======================================================================
!c
!c=======================================================================
!C REALS
!c=======================================================================
!c Two REAL parameters have been FIXED in GETTAU to do only EWALD !!!
!c                               ---------------
      real(8), parameter :: eimsw=0.5d0,eresw=-eimsw*sqrt(two)   ! in D.U.
!
!c FOR STRUCTURE CONSTANTS (see notes in GETTAU): eimsw,eresw parameters
!c define regions in the upper-half E complex plane where Ewald and
!c real-space methods are applied to get Structure Constants. EWALD method
!c is used in a strip ImE<eimsw .and. ReE>eresw and usually it is more
!c accurate than the real-space method.
!c=======================================================================

      integer, private, parameter :: nefmax0 = 25
      integer, protected :: nefmax = -1

      real(8), protected :: eftol=1.d-3      ! Fermi level tolerance (to find Ef)
      real(8), protected :: etol=1.d-5       ! Etot tolerance (SCF cycle)
      real(8), protected :: tctol=5.d-8      ! CPA tolerance (CPA cycle)
      real(8), protected :: qtol=1.d-7       ! Q rel.tolerance

      integer, parameter :: ipmesh=2        ! upper limit for the number of different k-meshes

      integer, parameter :: maxcpa=30       ! default upper limit of CPA iterations

      integer, parameter :: ipmdlng = 2000  ! upper limit for number of real- and k-space vectors
                                            ! in Madelung summation
      real(8), parameter :: etamdl = 0.7d0   ! parameter "eta" for Madelung summation

!c  XC SECTION:
      integer, parameter :: XC_LDA_HEDIN=10,XC_LDA_VOSKO=20   ! XC functional id in mecca
      integer, parameter :: XC_GGA_AM05=120

!c  CONTROL SECTION:
!c    screened CPA charge-correlation parameter -- see genpot.f
!c      real(8), parameter :: c_mf_cc=0.69d0  ! BCC
!c      real(8), parameter :: c_mf_cc=0.66d0  ! FCC
!c      real(8), parameter :: c_mf_cc=0.5d0   ! scr-MF (unlikely a good choice for non-metals)
!c      real(8), parameter :: c_mf_cc=0.0d0   ! NO MEAN-FIELD CH.-COR.

      character(5), public, protected :: cc_mod='mf'  ! charge-correlation model {'NUL','MF','FCC','BCC','SIM','TRIAL'}
              !    'trial' --> linear interpolation based on fcc/bcc is applied
              !    see function scr_mfcc_const(rws_over_rnn)
! dflt_ini_spinsplit - parameter responsible for default initial spin splitting
      real(8), public, protected :: dflt_ini_spinsplit = 0.06d0  !  = 359 * mu0
! to provide component-dependent spin splitting
      real(8), parameter :: dflt_spltscale = one
!
      integer, public, protected :: ipzpt=0   ! Do not include zero-pt energy (zeropt.f)
!c      parameter (ipzpt=1)   ! Include zero-pt energy (zeropt.f) if it is known
!c                              (there is no problem if all Z<50)

      logical, public, protected :: point_nucleus = .true.
!
      logical, public, protected :: bsf_k_latt_coord = .true.  ! to use reciprocal lattice vectors
                                                               ! to define BSF pathes and FS points
!
!c      integer, parameter :: ipmethod=1     ! Screened KKR, Mixed representation 1
!c                  Real-Space for Gref, K-space for G  -- for any energy
!c      parameter (ipmethod=2)     ! Screened KKR, Mixed representation 2
!c                  Real-Space for Gref, K-space for G -- near the Real axis
!c                  Real-Space for both Gref and G     -- otherwise
!c      parameter (ipmethod=3)     ! Screened KKR, Cluster Approach  (LSMS)
!c                  Real-Space for both Gref and G  -- for any energy
!c      parameter (ipmethod=4)     ! Screened KKR, Mixed representation 2
!c                  Real-Space for Gref, K-space for G -- near the Real axis
!c                  sparse standard K-space -- otherwise
!c      parameter (ipmethod=-1)   ! K-space screened KKR
!c      parameter (ipmethod=-2)   ! K-space screened KKR + sparse LU inversion
!c      parameter (ipmethod=-3)   ! K-space screened KKR + sparse MAR+TFQMR inversion
!c      parameter (ipmethod=0)    ! standard K-space KKR

      integer, parameter :: divisor=10  !  to pack/unpack integers (e.g. related to mtasa)

!-------------------------------------------------------------------------
!c imp_inp SECTION (VP-tessellation)
      integer(8), parameter :: MNV=ipvtx,MNF=ipnnmax
      integer(8), parameter :: MNqp=15
!       poly_type: (for VP-jacobian based integration)
!      =1 !  simple polynomial   basis0(jpf)=rmag**(DFLOAT(jpf-1))
!      =2 !Laguerre polynomial   basis0(jpf)=((2.0D0*DFLOAT(jpf)-1.0D0-rmag)*basis0(jpf-1)-      &
!                                              (DFLOAT(jpf)-1.0D0)*basis0(jpf-2))/(DFLOAT(jpf))
!      =3 !Legendre polynomial   basis0(jpf)=((2.0d0*DFLOAT(jpf)-1.0d0)*rmag*basis0(jpf-1)-      &
!                                             (DFLOAT(jpf)-1.0d0)*basis0(jpf-2))/(DFLOAT(jpf))
!      =4 !Chebyshev polynomial  basis0(jpf)=(2.0D0*rmag*basis0(jpf-1))-basis0(jpf-2)
!      =5 !                      basis0(jpf)=rmag**(DFLOAT(jpf-1)/2.0D0)
!
      integer, parameter :: poly_type=4
!      integer, parameter :: poly_type=2
      integer, parameter :: max_isopoly=8   ! max-order of polynomial for isoparam. VP-integration
      integer, parameter :: max_rmom=6      ! max-order of polynomial for "fin.elem." VP-integration
!      integer, parameter :: Nints=200
!      real(8), parameter :: hybrid_cutoff=5.d0

!c DOS-related SECTION
      integer, parameter :: ipbsfkpts=50        ! max. no. k-space waypts for A(k,E)

!c IO-files section
      integer, parameter :: nu_xyz    = 1           ! structure-file
      integer, parameter :: nu_mix1   = 2           ! mixing-file
      integer, parameter :: nu_mix2   = 3           ! mixing-file
      integer, parameter :: nu_mdlng  = 4          ! madelung-file
      integer, parameter :: nu_print  = 6           ! print-file
      integer, parameter :: nu_info   = 7           ! keep-file
      integer, parameter :: nu_inpot  = 8           ! in-potential file
      integer, parameter :: nu_outpot = 9           ! out-potential file
      integer, parameter :: nu_spkpt  = 10           ! special k-points-file
      integer, parameter :: nu_dop    = 11           ! dop-matrices file
      integer, parameter :: nu_yymom  = 12          ! YY-moments file
      integer, parameter :: nu_dos    = 13          ! DOS
      integer, parameter :: nu_bsf    = 14          ! BlochSpectrFun
      integer, parameter :: nu_fs1    = 15          ! fermi-surface, non-magn or spin up
      integer, parameter :: nu_fs2    = 16          ! fermi-surface, spin dn
!c=======================================================================
!c  non-input parameters
      character(*), parameter :: version='21.1 '
      character(32) :: build_date = 'UNKNOWN'
!
      character(*), parameter :: c_ake_band='akeBAND'
      character(*), parameter :: c_ake_bz_area='akeBZarea'
      character(*), parameter :: c_ake_bz_rays='akeBZrays'
      character(*), parameter :: c_fermi='fermiS'
!
      integer, parameter :: mecca_ames = 0
      integer, parameter :: mecca_2005 = 1
      integer, parameter :: tau_ray  = 2
      integer, parameter :: mecca_2013 = 3
      integer, parameter :: tau_scf  = 4
      integer, parameter :: ksint_scf_sch = 1
      integer, parameter :: ksint_ray_sch = 2
!c
!c  ipotoutmt:  how to calculate potential outside MT-sphere
!c
!c     integer, parameter :: ipotoutmt=0   ! V(r) = V_h(Rasa|Rasa) + Vxc(rho(r)), r>Rasa; [and ASA-Madelung]
!c     integer, parameter :: ipotoutmt=1   ! V(r) = V(Rasa), r>Rasa
!c     integer, parameter :: ipotoutmt=2   ! V(r)  = ASA + shift, r<Rasa; ShC is calculated
!      integer, parameter :: ipotoutmt=3   ! V(r) = V_h(Rasa|Rasa)*Rasa/r + Vxc(rho(r)), r>Rasa; [and ASA-Madelung]
      integer, parameter :: ipotoutmt=4   ! V(r) = V_h(r|Rcs) + Vxc(rho(r))

      integer, parameter :: lminSS=1      ! single-scattering taken into account beyond multiscattering,
                                          ! i.e. MS is for l=0:lmax and SS is for l=lmax+1:lmax+lminSS
      logical, parameter :: ScatShCorr=.false.   ! yes/no for shape-correction in scattering problem

      character(4), parameter :: lj(13) = [ 's1/2','p1/2','p3/2','d3/2',&
     &  'd5/2','f5/2','f7/2','g7/2','g9/2','h4.5','h5.5','i5.5','i6.5' ]
      logical, parameter :: in_place_mpi=.false.   ! to turn off(.false.)/on(.true.) MPI_IN_PLACE variable

!EOC
      contains

!BOP
!!IROUTINE: outMTModule
!!INTERFACE:
       subroutine outMTModule(iflag)
!!DESCRIPTION:
!  to set up current scattering shape correction flag
!
!!REVISION HISTORY:
! Initial Version - A.S. - 2013
!
!!TO DO: change subroutine name
!
!!REMARKS:
! it is not used in current version
!
!EOP
!BOC
!       use OutMT
       implicit none
       integer iflag
       logical, save :: lScatShCorr=.false.
!c
       if(iflag.eq.2) then
       if(.not.lScatShCorr) iflag = -2
       else if(iflag.eq.-2) then
        lScatShCorr = .false.
       else if(iflag.eq.-3) then
        lScatShCorr = ScatShCorr
       else
        call p_fstop(' OutMTModule: wrong iflag..')
       end if
!
       return
!EOC
       end subroutine outMTModule
!
!BOP
!!IROUTINE: setDate
!!INTERFACE:
       subroutine setDate( newdate )
!!DESCRIPTION:
! to set up build date
!
!!REVISION HISTORY:
! Initial Version - A.S. - 2013
!
!EOP
!
!BOC
        character(*), intent(in) :: newdate
        build_date = newdate
!EOC
       end subroutine setDate

!BOP
!!IROUTINE: g_unpack
!!INTERFACE:
       function g_unpack( n_in, qtnt )
!!DESCRIPTION:
! unpacking procedure, g\_unpack = mod(n\_in,divisor),
! divisor is an internal mecca constant
!
!!REVISION HISTORY:
! Initial Version - A.S. - 2013
!EOP
!
!BOC
       integer :: g_unpack
       integer, intent(in)  :: n_in
       integer, intent(out) :: qtnt
       if ( n_in>0 ) then
         g_unpack = mod(n_in,divisor)
         qtnt = n_in/divisor
       else
         g_unpack = n_in
         qtnt = 0
       end if
       return
!EOC
       end function g_unpack

!DELETE!BOP
!DELETE!!IROUTINE: g_mtasa
!DELETE!!INTERFACE:
!DELETE       function g_mtasa( mtasa_in, qtnt )
!DELETE!!DESCRIPTION:
!DELETE!  to provide unpacked mtasa parameters
!DELETE!
!DELETE!!INPUT ARGUMENTS:
!DELETE       integer, intent(in) :: mtasa_in
!DELETE!!OUTPUT ARGUMENTS:
!DELETE       integer, intent(out), optional :: qtnt
!DELETE!!REVISION HISTORY:
!DELETE! Initial Version - A.S. - 2013
!DELETE!
!DELETE!EOP
!DELETE!
!DELETE!BOC
!DELETE       integer :: g_mtasa
!DELETE       integer :: l_qtnt
!DELETE       g_mtasa = g_unpack(mtasa_in,l_qtnt)
!DELETE       if ( present(qtnt) ) qtnt = l_qtnt
!DELETE       return
!DELETE!EOC
!DELETE       end function g_mtasa
!
!BOP
!!IROUTINE: g_zed
!!INTERFACE:
       function g_zed( rv, r )
!!DESCRIPTION:
! integer function to calculate charge of the nucleus
!
!!INPUT ARGUMENTS:
       real(8), intent(in) :: rv(:),r(:)
!!REVISION HISTORY:
! Initial Version - A.S. - 2013
!
!EOP
!BOC
       integer :: g_zed
       real(8) :: zed
       zed = -(0.5d0*rv(1))
       g_zed = nint(zed)
       if ( g_zed >= 1 ) then
        zed = -(rv(1) + (rv(1)-rv(2))*r(1)/(r(2)-r(1)))
        g_zed = max(1,nint(0.5d0*zed))
       else
        if ( zed > -0.5d0 ) then
         g_zed = 0
        else
         g_zed = -1
        end if
       end if
       if ( g_zed<0 ) then
        write(6,*) ' WARNING: UNABLE TO CALCULATE NUCLEAR CHARGE'
        write(6,'(a,3e15.5)') ' WARNING: zed,rv(1:2)=',zed,rv(1),rv(2)
        write(6,*) '          Assume zed = 0 ...'
        g_zed = 0
       else if ( g_zed>120 ) then
        write(6,*) ' unexpected value of nuclear charge Z=',g_zed
        call p_fstop(' g_zed ERROR :: wrong nuclear charge')
       end if

       return
!EOC
       end function g_zed
!
!BOP
!!IROUTINE: Rnucl
!!INTERFACE:
       function Rnucl(Znucl)
!!DESCRIPTION:
!   real(8) function, Nucleus radius vs Z: R=r0*A**(1/3),
!   an empirical dependence is used for A(Z)
!
!!INPUT ARGUMENTS:
       real(8), intent(in) :: Znucl
!!REVISION HISTORY:
! Initial Version - A.S. - 2005
! A(Z) dependence updated - A.S. - 2014
!
!!REMARKS:
! rms nucleus radius, <r^2> = 3/5*Rnucl^2, is within
! 3% from experimental  root-mean-square nuclear ground state
! charge radius for all elements with Z>19
!EOP
!BOC

! about 50% of elements are within 1% from experimental data
!
! Ref: I. Angeli, K.P.Marinova, "Table of experimental nuclear ground state charge radii: An update",
! Atomic Data and Nuclear Data Tables, V.99, p.69-95 (2013)

       real(8) :: Rnucl
       real(8), parameter :: r0_1=1.13d-5/bohr2A
       real(8), parameter :: r0_2=1.31d-5/bohr2A
       real(8), parameter :: r0_3=1.25d-5/bohr2A
       real(8), parameter :: r0_4=1.22d-5/bohr2A
       real(8) :: A,r0
       integer :: ized
       ized = nint(Znucl)
       if ( ized>0 ) then
        if ( ized>54 ) then
         r0 = r0_4
        else if ( ized>18 ) then
         r0 = r0_3
        else if ( ized>1 )then
         r0 = r0_2
        else
         r0 = r0_1
        end if
        A = (1.98d0+0.006d0*Znucl)*Znucl
        Rnucl = r0*A**(one/three)
       else
        Rnucl = 0
       end if
       return
!EOC
       end function Rnucl
!
      real(8) function gXRmeshStep()
       gXRmeshStep = xr_mesh_step
      end function
!
      subroutine setXRmeshStep(xr_step)
      implicit none
      real(8), intent(in) :: xr_step
       xr_mesh_step = xr_step
      end subroutine setXRmeshStep
!
      integer function gNefmax()
       if ( nefmax < 0 ) then
        gNefmax = nefmax0
       else
        gNefmax = nefmax
       end if
      end function

      subroutine setNefmax(nef_max)
      implicit none
      integer, intent(in), optional :: nef_max
       if ( present(nef_max) ) then
       nefmax = nef_max
      else
       nefmax = nefmax0
      end if
      end subroutine setNefmax

      real(8) function gEfTol()
       gEfTol = eftol
      end function
!
      subroutine setEfTol(ef_tol)
      implicit none
      real(8), intent(in) :: ef_tol
       eftol = ef_tol
      end subroutine setEfTol
!
      real(8) function gEtol()
       gEtol = etol
      end function
!
      subroutine setEtol(e_tol)
      implicit none
      real(8), intent(in) :: e_tol
       etol = e_tol
      end subroutine setEtol
!
      real(8) function gTcTol()
       gTcTol = tctol
      end function
!
      subroutine setTcTol(tc_tol)
      implicit none
      real(8), intent(in) :: tc_tol
       tctol = tc_tol
      end subroutine setTcTol
!
      real(8) function gQtol()
       gQtol = qtol
      end function
!
      subroutine setQtol(q_tol)
      implicit none
      real(8), intent(in) :: q_tol
       qtol = q_tol
      end subroutine setQtol

      real(8) function gSpinsplt()
       gSpinsplt = dflt_ini_spinsplit
      end function
!
      integer function gZeroPt()
       gZeroPt = ipzpt
      end function
!
      subroutine setZeroPt(i_zpt)
      implicit none
      integer, intent(in) :: i_zpt
       ipzpt = min(1,abs(i_zpt))   ! ipzpt is 0 or 1(include zero-pt energy)
      end subroutine setZeroPt
!
      logical function gPointNucleus()
       gPointNucleus = point_nucleus
      end function
!
      subroutine setBsfCrtsn()
       bsf_k_latt_coord = .false.  ! to switch to Cartesian k-space
      end subroutine
!
      subroutine setFiniteNucl(nucleus_model)
      implicit none
      integer, intent(in) :: nucleus_model
      select case (nucleus_model)
      case(:0,2:)
       point_nucleus = .false.
      case(1)
       point_nucleus = .true.
      end select
      end subroutine setFiniteNucl
!
      subroutine setCCcpa(ccmod)
      implicit none
      character*(*) :: ccmod
      cc_mod = trim(ccmod)
      end subroutine setCCcpa
!
      end module mecca_constants
