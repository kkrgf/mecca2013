!BOP
!!ROUTINE: gauleg
!!INTERFACE:
      subroutine gauleg(x1,x2,x,w,n)
!!DESCRIPTION:
! generates gaussian points and weights to be used in 
! Gauss-Legendre quadrature; \\
! see:: Numerical Recipes
!
!!USES:
      use universal_const

!!DO_NOT_PRINT
      implicit   none
!!DO_NOT_PRINT

!!ARGUMENTS:
      real(8), intent(in) :: x1
      real(8), intent(in) :: x2
      real(8) :: x(*)
      real(8) :: w(*)
      integer, intent(in) :: n
!!REVISION HISTORY:
! Adapted - A.S. - 2013
!!REMARKS:
! subroutine gaussq can do the same
!EOP
!
!BOC
!c
      integer    m
      integer    i
      integer    j
!c
      real*8     xm
      real*8     xl
      real*8     z
      real*8     z1
      real*8     p1
      real*8     p2
      real*8     p3
      real*8     pp
!c
      real(8), parameter :: fourth=one/four,half=one/two
      real(8), parameter :: eps=three*(one/ten)**14
!c
      m=(n+1)/2
      xm=half*(x2+x1)
      xl=half*(x2-x1)
      do i=1,m
        z=cos(pi*(i-fourth)/(n+half))
1       continue
          p1=one
          p2=zero
!c
          do j=1,n
            p3=p2
            p2=p1
            p1=((two*j-one)*z*p2-(j-one)*p3)/j
          enddo
!c
          pp=n*(z*p1-p2)/(z*z-one)
          z1=z
          z=z1-p1/pp
!c
          if(abs(z-z1).gt.eps) go to 1
!c
          x(i)=xm-xl*z
          x(n+1-i)=xm+xl*z
          w(i)=two*xl/((one-z*z)*pp*pp)
          w(n+1-i)=w(i)
        enddo
!c
      return
!EOC
      end subroutine gauleg
