!BOP
!!ROUTINE: containers
!!INTERFACE:
!      subroutine SmtModule(iflag,nsub1,nsub2,vdata)
!      subroutine RasaModule(iflag,nsub1,nsub2,vdata)
!      subroutine RnnModule(iflag,nsub1,nsub2,vdata)
!      subroutine RcsModule(iflag,nsub1,nsub2,vdata)
!      subroutine FaceModule(iflag,ndnf,nsub1,nsub2,                     &
!     &                      nf,nwF,Face)
!      subroutine TmpModule(iflag,N,ND,IN1,IN2,vdata,nsub1,nsub2)
!!DESCRIPTION:
! set of subroutines, which are used in {\sc mecca} as data containers
!
!!REVISION HISTORY:
! Initial Version - A.S. - 2001
!!TO DO: 
! to replace by module?
!EOP
!
!BOC
      subroutine SmtModule(iflag,nsub1,nsub2,vdata)
      implicit none

      integer iflag,nsub1,nsub2
!      real*8  vdata(nsub1:nsub2)
      real(8) :: vdata(nsub1:*)
      real*8, allocatable, save :: datsaved(:)
      integer, save :: i0=0,i1=-10

      integer i
      character*4 sname/' Smt'/

      if(iflag.eq.0) then              ! read
       if(nsub2.lt.nsub1) return
       vdata(nsub1:nsub2) = datsaved(nsub1:nsub2)
      else if(iflag.eq.1) then         ! write
       if(nsub2.lt.nsub1) return
       datsaved(nsub1:nsub2) = vdata(nsub1:nsub2)
      else if(iflag.eq.2) then         ! allocate
       if(allocated(datsaved)) then
      if(nsub1.lt.i0.or.nsub2.gt.i1) then
       deallocate(datsaved)
      else
       return
      end if
       end if
       if(nsub2.lt.nsub1) return
       allocate(datsaved(nsub1:nsub2))
       i0 = nsub1
       i1 = nsub2
      else if(iflag.eq.-1) then        ! deallocate
       if(allocated(datsaved)) deallocate(datsaved)
      else if(iflag.eq.-2) then        ! info
       if(allocated(datsaved)) then
      nsub1 = i0
      nsub2 = i1
       else
      nsub1 = 0
      nsub2 = -10
       end if
      else if(iflag.eq.-3) then        ! print
       do i=nsub1,nsub2
      write(6,'(''  nsub='',i4,2x,a,''='',d14.6)')                      &
     &        i,sname,datsaved(i)
       end do
      end if

      return
      end

      subroutine RasaModule(iflag,nsub1,nsub2,vdata)
      implicit none

      integer iflag,nsub1,nsub2
      real*8  vdata(nsub1:nsub2)

      real*8, allocatable :: datsaved(:)
      integer i0/0/,i1/-10/

      integer i
      character*4 sname/'Rasa'/

      save datsaved
      save i0,i1

      if(iflag.eq.0) then              ! read
       if(nsub2.lt.nsub1) return
       vdata(nsub1:nsub2) = datsaved(nsub1:nsub2)
      else if(iflag.eq.1) then         ! write
       if(nsub2.lt.nsub1) return
       datsaved(nsub1:nsub2) = vdata(nsub1:nsub2)
      else if(iflag.eq.2) then         ! allocate
       if(allocated(datsaved)) then
      if(nsub1.lt.i0.or.nsub2.gt.i1) then
       deallocate(datsaved)
      else
       return
      end if
       end if
       if(nsub2.lt.nsub1) return
       allocate(datsaved(nsub1:nsub2))
       i0 = nsub1
       i1 = nsub2
      else if(iflag.eq.-1) then        ! deallocate
       if(allocated(datsaved)) deallocate(datsaved)
      else if(iflag.eq.-2) then        ! info
       if(allocated(datsaved)) then
      nsub1 = i0
      nsub2 = i1
       else
      nsub1 = 0
      nsub2 = -10
       end if
      else if(iflag.eq.-3) then        ! print
       do i=nsub1,nsub2
      write(6,'(''  nsub='',i4,2x,a,''='',d14.6)')                      &
     &        i,sname,datsaved(i)
       end do
      end if

      return
      end

      subroutine RnnModule(iflag,nsub1,nsub2,vdata)
      implicit none

      integer iflag,nsub1,nsub2
      real*8  vdata(nsub1:nsub2)

      real*8, allocatable :: datsaved(:)
      integer i0/0/,i1/-10/

      integer i
      character*4 sname/' Rnn'/

      save datsaved
      save i0,i1

      if(iflag.eq.0) then              ! read
       if(nsub2.lt.nsub1) return
       vdata(nsub1:nsub2) = datsaved(nsub1:nsub2)
      else if(iflag.eq.1) then         ! write
       if(nsub2.lt.nsub1) return
       datsaved(nsub1:nsub2) = vdata(nsub1:nsub2)
      else if(iflag.eq.2) then         ! allocate
       if(allocated(datsaved)) then
      if(nsub1.lt.i0.or.nsub2.gt.i1) then
       deallocate(datsaved)
      else
       return
      end if
       end if
       if(nsub2.lt.nsub1) return
       allocate(datsaved(nsub1:nsub2))
       i0 = nsub1
       i1 = nsub2
      else if(iflag.eq.-1) then        ! deallocate
       if(allocated(datsaved)) deallocate(datsaved)
      else if(iflag.eq.-2) then        ! info
       if(allocated(datsaved)) then
      nsub1 = i0
      nsub2 = i1
       else
      nsub1 = 0
      nsub2 = -10
       end if
      else if(iflag.eq.-3) then        ! print
       do i=nsub1,nsub2
      write(6,'(''  nsub='',i4,2x,a,''='',d14.6)')                      &
     &        i,sname,datsaved(i)
       end do
      end if

      return
      end

      subroutine RcsModule(iflag,nsub1,nsub2,vdata)
      implicit none

      integer iflag,nsub1,nsub2
      real*8  vdata(nsub1:nsub2)

      real*8, allocatable :: datsaved(:)
      integer i0/0/,i1/-10/

      integer i
      character*4 sname/' Rcs'/

      save datsaved
      save i0,i1

      if(iflag.eq.0) then              ! read
       if(nsub2.lt.nsub1) return
       vdata(nsub1:nsub2) = datsaved(nsub1:nsub2)
      else if(iflag.eq.1) then         ! write
       if(nsub2.lt.nsub1) return
       datsaved(nsub1:nsub2) = vdata(nsub1:nsub2)
      else if(iflag.eq.2) then         ! allocate
       if(allocated(datsaved)) then
      if(nsub1.lt.i0.or.nsub2.gt.i1) then
       deallocate(datsaved)
      else
       return
      end if
       end if
       if(nsub2.lt.nsub1) return
       allocate(datsaved(nsub1:nsub2))
       i0 = nsub1
       i1 = nsub2
      else if(iflag.eq.-1) then        ! deallocate
       if(allocated(datsaved)) deallocate(datsaved)
      else if(iflag.eq.-2) then        ! info
       if(allocated(datsaved)) then
      nsub1 = i0
      nsub2 = i1
       else
      nsub1 = 0
      nsub2 = -10
       end if
      else if(iflag.eq.-3) then        ! print
       do i=nsub1,nsub2
      write(6,'(''  nsub='',i4,2x,a,''='',d14.6)')                      &
     &        i,sname,datsaved(i)
       end do
      end if

      return
      end
!c
!      subroutine L1L2mapModule(LM,nsub,ndkkr,L1L2map,eps)
!      implicit none
!
!      integer LM,nsub,ndkkr
!      integer L1L2map(ndkkr,ndkkr)
!      real*8 eps
!
!      complex*16, allocatable :: yL1yL2Re(:,:)
!      complex*16, allocatable :: yL1yL2Im(:,:)
!
!      integer lm1,lm2,llmax,index
!      integer kkrsz/0/
!      save kkrsz,yL1yL2Re,yL1yL2Im
!
!      if(LM.eq.-1) then
!       if(allocated(yL1yL2Re)) deallocate(yL1yL2Re,yL1yL2Im)
!       kkrsz = 0
!       return
!      end if
!
!      if(.not.allocated(yL1yL2Re)) then
!       allocate(yL1yL2Re(1:ndkkr,1:ndkkr),yL1yL2Im(1:ndkkr,1:ndkkr))
!       kkrsz = ndkkr
!      else
!       if(ndkkr.gt.kkrsz) then
!      deallocate(yL1yL2Re,yL1yL2Im)
!      allocate(yL1yL2Re(1:ndkkr,1:ndkkr),yL1yL2Im(1:ndkkr,1:ndkkr))
!      kkrsz = ndkkr
!       end if
!      end if
!
!      llmax = kkrsz*kkrsz
!      call YYmomModule(0,0,0,LM,llmax,llmax,                            &
!     &                            nsub,nsub,yL1yL2Re,yL1yL2Im)
!
!      do lm1=1,ndkkr
!      L1L2map(lm1,lm1) = lm1
!      end do
!      index = ndkkr
!
!      do lm1=1,ndkkr
!       do lm2=1,ndkkr
!      if(lm2.eq.lm1) cycle
!      if(abs(yL1yL2Re(lm2,lm1))+abs(yL1yL2Im(lm2,lm1)).le.eps)          &
!     &  then
!       L1L2map(lm2,lm1)=0
!      else
!       index = index+1
!       L1L2map(lm2,lm1) = index
!      end if
!       end do
!      end do
!      return
!      end

!cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc !c
!c
      subroutine FaceModule(iflag,ndnf,nsub1,nsub2,                     &
     &                      nf,nwF,Face)
      implicit none

      integer iflag,ndnf,nsub1,nsub2
      integer nf(nsub1:nsub2)
      integer nwF(ndnf,nsub1:nsub2)
      real*8  Face(3,3,ndnf,nsub1:nsub2)

      integer, allocatable :: nfsaved(:)
      integer, allocatable :: nwFsaved(:,:)
      real*8, allocatable :: datsaved(:,:,:,:)
      integer i0/0/,i1/-10/,ndm/-10/

      integer i

      save datsaved,nfsaved,nwFsaved
      save i0,i1,ndm

      if(iflag.eq.0) then              ! read
       if(nsub2.lt.nsub1) return
       nf(nsub1:nsub2) = nfsaved(nsub1:nsub2)
       do i=nsub1,nsub2
      nwF(1:nf(i),i) = nwFsaved(1:nf(i),i)
      Face(1:3,1:3,1:nf(i),i) = datsaved(1:3,1:3,1:nf(i),i)
       end do
      else if(iflag.eq.1) then         ! write
       if(nsub2.lt.nsub1) return
       nfsaved(nsub1:nsub2) = nf(nsub1:nsub2)
       do i=nsub1,nsub2
      nwFsaved(1:nf(i),i) = nwF(1:nf(i),i)
      datsaved(1:3,1:3,1:nf(i),i) = Face(1:3,1:3,1:nf(i),i)
       end do
      else if(iflag.eq.2) then         ! allocate
       if(allocated(datsaved)) then
      if(nsub1.lt.i0.or.nsub2.gt.i1.or.ndnf.gt.ndm) then
       deallocate(nfsaved,nwFsaved,datsaved)
      else
       return
      end if
       end if
       if(nsub2.lt.nsub1) return
       allocate(                                                        &
     &          datsaved(1:3,1:3,1:ndnf,nsub1:nsub2),                   &
     &          nwFsaved(1:ndnf,nsub1:nsub2),                           &
     &          nfsaved(nsub1:nsub2)                                    &
     &         )
       i0 = nsub1
       i1 = nsub2
       ndm = ndnf
      else if(iflag.eq.-1) then        ! deallocate
       if(allocated(datsaved)) deallocate(datsaved,nfsaved,nwFsaved)
      else if(iflag.eq.-2) then        ! info
       if(allocated(datsaved)) then
      nsub1 = i0
      nsub2 = i1
      nf(1) = ndm
       else
      nsub1 = 0
      nsub2 = -10
      nf(1) = -10
       end if
      end if

      return
      end subroutine FaceModule
!c
      subroutine TmpModule(iflag,N,ND,IN1,IN2,vdata,nsub1,nsub2)
      implicit none

      integer iflag
      integer N,ND(N-1)
      integer IN1(N),IN2(N)
      real*8  vdata(*)
      integer nsub1,nsub2
!c      integer indarr
!c      external indarr

      real*8, allocatable :: datsaved(:)
      integer i0/0/,i1/-10/

      integer i
      character*9 sname/'TmpModule'/

      save datsaved
      save i0,i1

      if(N.lt.1) return
      nsub1 = indarr(N,IN1,ND)
      nsub2 = indarr(N,IN2,ND)
      if(iflag.eq.0) then              ! read
       vdata(nsub1:nsub2) = datsaved(nsub1:nsub2)
      else if(iflag.eq.1) then         ! write
       datsaved(nsub1:nsub2) = vdata(nsub1:nsub2)
      else if(iflag.eq.2) then         ! allocate
       if(allocated(datsaved)) then
      if(nsub1.lt.i0.or.nsub2.gt.i1) then
       deallocate(datsaved)
      else
       return
      end if
       end if
       allocate(datsaved(nsub1:nsub2))
       i0 = nsub1
       i1 = nsub2
      else if(iflag.eq.-1) then        ! deallocate
       if(allocated(datsaved)) deallocate(datsaved)
      else if(iflag.eq.-2) then        ! info
       if(allocated(datsaved)) then
      nsub1 = i0
      nsub2 = i1
       else
      nsub1 = 0
      nsub2 = -10
       end if
      else if(iflag.eq.-3) then        ! print
       do i=nsub1,nsub2
      write(6,'(''  i='',i4,2x,a,''='',d14.6)')                         &
     &        i,sname,datsaved(i)
       end do
      end if

      return

      CONTAINS

       integer function indarr(N,I,ND)
       implicit none
       integer N
       integer I(N),ND(N-1)
       integer nd0,j

       if(N.le.0) then
        indarr = 0
        return
       end if
       indarr = I(1)
       nd0 = 1
       do j=2,N
        nd0 = nd0*nd(j-1)
        indarr = indarr + (I(j)-1)*nd0  ! element number in multidimensional array
       end do
       return
       end function indarr

      end subroutine TmpModule
