!BOP
!!ROUTINE: g_ndims
!!INTERFACE:
      subroutine g_ndims(inFile,ndrpts,ndcomp,ndsubl,ndspin)
!!DESCRIPTION:
! extracts arrays dimensions {\tt ndrpts,ndcomp,ndsubl,ndspin}
! from {\tt inFile} structure (type IniFile)
!
!!USES:
       use mecca_types, only : IniFile
!
!!REVISION HISTORY:
! Initial Version - A.S. - 2013
!EOP
!
!BOC
          implicit none
          type(IniFile), intent(in) :: inFile
          integer, intent(out) :: ndrpts,ndcomp,ndsubl,ndspin
          integer nsub
          ndspin = inFile%nspin
          ndsubl = inFile%nsubl
          ndcomp = maxval(inFile%sublat(1:inFile%nsubl)%ncomp)
          ndrpts=0
          do nsub=1,inFile%nsubl
           ndrpts = max(ndrpts,maxval(inFile%sublat(nsub)%              &
     &            compon(1:inFile%sublat(nsub)%ncomp)%v_rho_box%ndrpts))
          end do
          return
!EOC
      end subroutine g_ndims
!
!BOP
!!ROUTINE: g_komp
!!INTERFACE:
      subroutine g_komp(inFile, komp)
!!DESCRIPTION:
! extracts array {\tt komp}, number of components for all sublattices,
! from {\tt inFile} structure (type IniFile)
!
!!USES:
       use mecca_types, only : IniFile
!EOP
!
!BOC
          implicit none
          type(IniFile), intent(in) :: inFile
          integer, allocatable :: komp(:)
          integer :: nsub
          if ( allocated(komp) ) deallocate(komp)
          if ( .not.allocated(komp) ) allocate(komp(1:inFile%nsubl))
!          komp(1:inFile%nsubl) = inFile%sublat(1:inFile%nsubl)%ncomp
          do nsub=1,inFile%nsubl
           komp(nsub) = inFile%sublat(nsub)%ncomp
          end do
          return
!EOC
      end subroutine g_komp

!BOP
!!ROUTINE: g_atcon
!!INTERFACE:
      subroutine g_atcon(inFile, atcon)
!!DESCRIPTION:
! extracts array {\tt atcon}, concentrations for all sublattice components,
! from {\tt inFile} structure (type IniFile)
!
!!USES:
       use mecca_types, only : IniFile
!EOP
!
!BOC
          implicit none
          type(IniFile), intent(in) :: inFile
          real(8), allocatable :: atcon(:,:)
          integer nsub,ic
          if ( allocated(atcon) ) deallocate(atcon)
          allocate(atcon(1:maxval(inFile%sublat(1:inFile%nsubl)%ncomp), &
     &                                            1:inFile%nsubl))
          atcon = 0
          do nsub = 1,inFile%nsubl
           do ic=1,inFile%sublat(nsub)%ncomp
            atcon(ic,nsub) = inFile%sublat(nsub)%compon(ic)%concentr
           end do
          end do
          return
!EOC
      end subroutine g_atcon

!BOP
!!ROUTINE: g_meshv
!!INTERFACE:
      subroutine g_meshv(v_rho_box,h,xr,rr,is,vr)
!!DESCRIPTION:
! generates {\sc mecca} equidistant {\tt xr-} and its respective exponential
! {\tt rr-}meshes, and r-weighted potential, {\tt vr} (i.e. r*potential).
!
!!USES:
       use mecca_types, only : SphAtomDeps

!!DO_NOT_PRINT
          implicit none
!!DO_NOT_PRINT

!!ARGUMENTS:
          type(SphAtomDeps), intent(in) :: v_rho_box
          integer, intent(in), optional :: is
          real(8), intent(out) :: h
          real(8), intent(out) :: xr(:),rr(:)
          real(8), intent(out), optional :: vr(:)
!!REVISION HISTORY:
! Initial Version - A.S. - 2013
!EOP
!
!BOC
          interface
             subroutine g_vr(v_rho_box,is,vr,v0)
             use mecca_types, only : SphAtomDeps
             type(SphAtomDeps), intent(in) :: v_rho_box
             integer, intent(in)  :: is
             real(8), intent(out) :: vr(:)
             real(8), intent(out) :: v0
             end subroutine g_vr
          end interface
!
          real(8) :: v0
          integer :: i
          if ( size(xr) < v_rho_box%ndrpts ) then
            h = -v_rho_box%ndrpts
            return
          end if
          h = (v_rho_box%xmt-v_rho_box%xst)/(v_rho_box%jmt-1)
          forall (i=1:size(xr)) xr(i) = v_rho_box%xst + (i-1)*h
          rr = exp(xr)
          if ( present(vr) .and. present(is) ) then
            call g_vr(v_rho_box,is,vr,v0)
          end if
          return
!EOC
      end subroutine g_meshv

!BOP
!!ROUTINE: g_vr
!!INTERFACE:
      subroutine g_vr(v_rho_box,is,vr,v0)
!!DESCRIPTION:
! copies {\tt vr} (r-weighted) potential) and {\tt v0} , potential "{zero}",
! {\em from} respective components of {\tt v\_rho\_box} structure (type SphAtomDeps);
!  spin dependency is defined by input argument {\tt is}
!
!!USES:
       use mecca_types, only : SphAtomDeps
!
!!REVISION HISTORY:
! Initial Version - A.S. - 2013
!!REMARKS:
! output argument vr is nullified if values of is<=0
!EOP
!
!BOC
          implicit none
          type(SphAtomDeps), intent(in) :: v_rho_box
          integer, intent(in)  :: is
          real(8), intent(out) :: vr(:)
          real(8), intent(out) :: v0
          integer :: nr,j
          vr = 0
          if ( is>0 ) then
           nr = min(size(vr),v_rho_box%ndrpts)
           j = min(is,size(v_rho_box%vr,2))
           vr(1:nr) = v_rho_box%vr(1:nr,j)
           vr(nr+1:) = 0
           v0 = v_rho_box%v0(j)
          end if
          return
!EOC
      end subroutine g_vr

!BOP
!!ROUTINE: s_vr
!!INTERFACE:
      subroutine s_vr(v_rho_box,is,vr,v0)
!!DESCRIPTION:
! copies {\tt vr} and {\tt v0} {\em to} respective components of
! {\tt v\_rho\_box} structure (type SphAtomDeps), spin dependency
! is defined by input argument {\tt is}
!
!!USES:
       use mecca_types, only : SphAtomDeps
!
!!REVISION HISTORY:
! Initial Version - A.S. - 2013
!EOP
!
!BOC
          implicit none
          type(SphAtomDeps) :: v_rho_box
          integer, intent(in)  :: is
          real(8), intent(in) :: vr(:)
          real(8), intent(in) :: v0
          integer :: nr
          if ( is>0 .and. is<= size(v_rho_box%vr,2) ) then
           nr = min(size(v_rho_box%vr,1),size(vr))
           v_rho_box%vr(1:nr,is) = vr(1:nr)
           v_rho_box%vr(nr+1:,is) = 0
           v_rho_box%ndrpts = max(nr,v_rho_box%ndrpts)
           v_rho_box%v0(is) = v0
          end if
          return
!EOC
      end subroutine s_vr

!BOP
!!ROUTINE: g_V0
!!INTERFACE:
      real(8) function g_V0(inFile,is)
!!DESCRIPTION:
! value of V0 (zero of scf-potential);
! spin dependence is defined by input argument {\tt is}
!
!!USES:
       use mecca_types, only : IniFile
!
!!REVISION HISTORY:
! Initial Version - A.S. - 2023
!!REMARKS:
! no check for "bad values" of input argument "is"
!EOP
!
!BOC
          implicit none
          type(IniFile), intent(in) :: inFile
          integer, intent(in)  :: is
          g_V0 = inFile%v0(is)
          return
!EOC
      end function g_V0

!BOP
!!ROUTINE: g_rho
!!INTERFACE:
      subroutine g_rho(v_rho_box,is,rho)
!!DESCRIPTION:
! copies array {\tt rho} (r-weighted total charge density)
! {\em from} {\tt v\_rho\_box%chgtot} component of type sphAtomDeps structure;
! spin dependence is defined by input argument {\tt is}
!
!!USES:
       use mecca_types, only : SphAtomDeps
!
!!REVISION HISTORY:
! Initial Version - A.S. - 2013
!!REMARKS:
! no check for "bad values" of input argument "is"
!EOP
!
!BOC
          implicit none
          type(SphAtomDeps), intent(in) :: v_rho_box
          integer, intent(in)  :: is
          real(8), intent(out) :: rho(:)
          integer :: nr
          nr = min(size(rho),v_rho_box%ndchg)
          rho(1:nr) = v_rho_box%chgtot(1:nr,is)
          rho(nr+1:) = 0
          return
!EOC
      end subroutine g_rho

!BOP
!!ROUTINE: s_rho
!!INTERFACE:
      subroutine s_rho(v_rho_box,is,rho,add)
!!DESCRIPTION:
! copies array {\tt rho} (r-weighted total charge density)
! {\em to} {\tt v\_rho\_box%chgtot} component of type sphAtomDeps structure;
! spin dependence is defined by input argument {\tt is};
! if an optional argument {\tt add} is present then {\tt rho} is {\em added}
! to {\tt v\_rho\_box%chgtot}.
!
!!USES:
       use mecca_types, only : SphAtomDeps
!
!!REVISION HISTORY:
! Initial Version - A.S. - 2013
!
!EOP
!
!BOC
          implicit none
          type(SphAtomDeps), intent(inout) :: v_rho_box
          integer, intent(in) :: is
          real(8), intent(in) :: rho(:)
          integer, optional, intent(in) :: add
          integer :: nr,iadd
          if ( is>0 .and. is<= size(v_rho_box%chgtot,2) ) then
           nr = min(size(rho),size(v_rho_box%chgtot(:,is),1))
           iadd = 0
           if ( present(add) ) iadd = add
           if ( iadd==0 ) then
            v_rho_box%chgtot(1:nr,is) = rho(1:nr)
            v_rho_box%chgtot(nr+1:,is) = 0
           else
            v_rho_box%chgtot(1:nr,is) = v_rho_box%chgtot(1:nr,is)       &
     &                                + rho(1:nr)
           end if
           v_rho_box%ndchg = max(nr,v_rho_box%ndchg,v_rho_box%ndcor)
          end if
          return
!EOC
      end subroutine s_rho

!BOP
!!ROUTINE: g_rhocor
!!INTERFACE:
      subroutine g_rhocor(v_rho_box,is,rho)
!!DESCRIPTION:
! copies array {\tt rho} (r-weighted core charge density)
! {\em from} {\tt v\_rho\_box%chgcor} component of type sphAtomDeps structure;
! spin dependence is defined by input argument {\tt is}
!
!!USES:
       use mecca_types, only : SphAtomDeps
!
!!REVISION HISTORY:
! Initial Version - A.S. - 2013
!EOP
!
!BOC
          implicit none
          type(SphAtomDeps), intent(in) :: v_rho_box
          integer, intent(in)  :: is
          real(8), intent(out) :: rho(:)
          integer :: nr
          nr = min(size(rho),v_rho_box%ndcor)
          rho(1:nr) = v_rho_box%chgcor(1:nr,is)
          rho(nr+1:) = 0
          return
!EOC
      end subroutine g_rhocor

!BOP
!!ROUTINE: g_rhoval
!!INTERFACE:
      subroutine g_rhoval(v_rho_box,is,rho)
!!DESCRIPTION:
! copies charge density components of {\tt v\_rho\_box} structure
! (type SphAtomDeps) {\em to} array {\tt rho} (r-weighted valence charge density);
! spin dependence is defined by input argument {\tt is}
!
!!USES:
       use mecca_types, only : SphAtomDeps
!
!!REVISION HISTORY:
! Initial Version - A.S. - 2013
!EOP
!
!BOC
          implicit none
          type(SphAtomDeps), intent(in) :: v_rho_box
          integer, intent(in)  :: is
          real(8), intent(out) :: rho(:)
          integer :: nr
          nr = min(size(rho),v_rho_box%ndchg)
          rho(1:nr) = v_rho_box%chgtot(1:nr,is)
          rho(nr+1:) = 0
          nr = min(size(rho),v_rho_box%ndcor)
          rho(1:nr) = rho(1:nr) - v_rho_box%chgcor(1:nr,is)
          return
!EOC
      end subroutine g_rhoval

!BOP
!!ROUTINE: g_rhosemicore
!!INTERFACE:
      subroutine g_rhosemicore(ic,nsub,is,rho,mecca_state)
!!DESCRIPTION:
! copies semicore charge density component of {\tt work\_box} structure
! (type Work\_data) {\em to}  array {\tt rho} (r-weighted semicore
! charge density); component, sublattice and spin dependence are defined
! by input argument {\tt ic, nsub and is}, respectively
!
!!USES:
       use mecca_types, only : RunState
       use mecca_run, only : getMS
       use mecca_types, only : SphAtomDeps
!
!!REVISION HISTORY:
! Initial Version - A.S. - 2013
!EOP
!
!BOC
          implicit none
          integer, intent(in) :: ic,nsub,is
          real(8), intent(out) :: rho(:)
          type(RunState), target, optional :: mecca_state
          type(RunState), pointer :: M_S=>null()
          integer :: nr
          nr = size(rho)
          if ( nr==0 ) return
          rho(1) = 0
          if ( present(mecca_state) ) then
           M_S => mecca_state
          else
           M_S => getMS()
          end if
          if ( associated(M_S) ) then
           if ( associated(M_S%work_box) ) then
            if ( allocated(M_S%work_box%chgsemi) ) then
             nr = min(nr,size(M_S%work_box%chgsemi,1))
             rho(1:nr) = M_S%work_box%chgsemi(1:nr,ic,nsub,is)
             if ( rho(1) == 0 ) then
              if ( maxval(rho(2:nr)) > tiny(rho(1)) )                   &
     &                          rho(1) = 2*tiny(rho(1))
             end if
             rho(nr+1:) = 0
            end if
           end if
          end if
          nullify(M_S)
          return
!EOC
      end subroutine g_rhosemicore

!BOP
!!ROUTINE: g_totchg
!!INTERFACE:
      subroutine g_totchg(v_rho_box,non_zero_T,is,totchg,corchg)
!!DESCRIPTION:
! copies {\tt totchg, corchg} (total and core charges) {\em from}
! the respective components of {\tt v\_rho\_box} structure (type sphAtomDeps);
! spin dependence is defined by input argument {\tt is}; {\tt non\_zero\_T} is
! logical parameter
!
!!USES:
       use mecca_types, only : SphAtomDeps
!
!!REVISION HISTORY:
! Initial Version - A.S. - 2013
!!REMARKS:
! parameter non_zero_T affects only core-electrons
!EOP
!
!BOC
          implicit none
          type(SphAtomDeps), intent(in) :: v_rho_box
          logical, intent(in)  :: non_zero_T
          integer, intent(in)  :: is
          real(8), intent(out) :: totchg,corchg
          if ( non_zero_T ) then
            totchg = v_rho_box%ztot_T(is)
            corchg = v_rho_box%zcor_T(is)
          else
            totchg = v_rho_box%ztot_T(is)
            corchg = v_rho_box%zcor(is)
          end if
          return
!EOC
      end subroutine g_totchg

!BOP
!!ROUTINE: s_totchg
!!INTERFACE:
      subroutine s_totchg(v_rho_box,is,totchg,add)
!!DESCRIPTION:
! copies {\tt totchg} (total charge) {\em to} {\tt v\_rho\_box%ztot\_T}
! component of type sphAtomDeps structure;
! spin dependence is defined by input argument {\tt is};
! if optional argument {\tt add} is present, then {\tt totchg} is
! {\em added} to {\tt v\_rho\_box%ztot\_T}.
!
!!USES:
       use mecca_types, only : SphAtomDeps
!
!!REVISION HISTORY:
! Initial Version - A.S. - 2013
!EOP
!
!BOC
          implicit none
          type(SphAtomDeps), intent(inout) :: v_rho_box
          integer, intent(in)  :: is
          real(8), intent(in) :: totchg
          integer, optional, intent(in) :: add
          integer :: iadd
          iadd = 0
          if ( present(add) ) iadd = add
          if ( iadd==0 ) then
            v_rho_box%ztot_T(is) = totchg
          else
            v_rho_box%ztot_T(is) = v_rho_box%ztot_T(is) + totchg
          end if
          return
!EOC
      end subroutine s_totchg

!BOP
!!ROUTINE: g_zcorT
!!INTERFACE:
      subroutine g_zcorT(v_rho_box,is,ebot,T,Ef,zcor)
!!DESCRIPTION:
! calculates core charge zcor(T,Ef) per spin, {\tt T=tempr\_in},
! {\tt efermi} is a chemical potential
!
!
!!USES:
       use mecca_types, only : SphAtomDeps

!!DO_NOT_PRINT
          implicit none
!!DO_NOT_PRINT

!!ARGUMENTS:
          type(SphAtomDeps), intent(in) :: v_rho_box
          integer, intent(in) :: is
          real(8), intent(in) :: ebot
          real(8), intent(in) :: T
          real(8), intent(in) :: Ef
          real(8), intent(out):: zcor
!!REVISION HISTORY:
! Initial Version - A.S. - 2020
!!REMARKS:
!  zcor is per spin, i.e. if nspin=1, tot_zcor = zcor*2
!EOP
!
!BOC
          complex(8), external :: TemprFun
          integer :: ie
          real(8) :: w,e0,ec,bt
          complex(8) :: z
          real(8), parameter :: one=1,zero=0
!
          zcor = zero
          e0 = min(zero,ebot)
          do ie=v_rho_box%numc(is),1,-1
            ec = v_rho_box%ec(ie,is)
            if ( ec < e0 ) then
             if ( abs(T)<=epsilon(zero) ) then
              bt = one
             else if ( T>zero ) then
              bt=(ec-Ef)/T
              z = dcmplx(bt,zero)
              bt = dreal(TemprFun(z,0))
             else
              bt = zero
             end if
             w = abs(v_rho_box%kc(ie,is))*bt
             zcor = zcor + w
            end if
          end do
!
          return
!EOC
      end subroutine g_zcorT

!BOP
!!ROUTINE: g_ecor
!!INTERFACE:
      subroutine g_ecor(v_rho_box,is,ebot,ecor,esemc,                   &
     &                                            tempr_in,efermi,nspin)
!!DESCRIPTION:
! generates core ({\tt ecor}) and semicore ({\tt esemc}) energies
! (with or without temperature defined by optional parameter {\tt tempr\_in})
!
!
!!USES:
       use mecca_types, only : SphAtomDeps

!!DO_NOT_PRINT
          implicit none
!!DO_NOT_PRINT

!!ARGUMENTS:
          type(SphAtomDeps), intent(in) :: v_rho_box
          integer, intent(in) :: is
          real(8), intent(in) :: ebot
          real(8), intent(out) :: ecor
          real(8), intent(out) :: esemc
          real(8), intent(in),optional :: tempr_in
          real(8), intent(in),optional :: efermi
          integer, intent(in),optional :: nspin

!!REVISION HISTORY:
! Initial Version - A.S. - 2013
!!REMARKS:
!  ecor/esemc - energy per spin, i.e. if nspin=1, tot_ecor = ecor*2
!EOP
!
!BOC
          complex(8), external :: TemprFun
          real(8), external :: gSemiCoreLevel
          integer :: ie,j_hocl
          real(8) :: T,ef,w,ec,e0,bt,e_hocl
          complex(8) :: z
          real(8), parameter :: one=1,zero=0
!
          ecor = zero
          esemc = zero
          e0 = min(zero,ebot)
          ef = zero
          T = zero
          if ( present(tempr_in) ) then
           if ( tempr_in>zero ) then
            T = tempr_in
            if ( present(efermi) ) then
              ef = efermi
            end if
           end if
          end if
          j_hocl = v_rho_box%highest_occupied_core_level(0,is)
          if ( j_hocl==0 ) then
           e_hocl = e0
          else
           e_hocl = v_rho_box%highest_occupied_core_level(2,is)
          end if
          do ie=v_rho_box%numc(is),1,-1
            ec = v_rho_box%ec(ie,is)
            if ( ec >= e0 ) cycle
            w = abs(v_rho_box%kc(ie,is)) ! *spin_factor
            if ( ec == e_hocl ) then
             ec = v_rho_box%highest_occupied_core_level(2,is)
             w = v_rho_box%highest_occupied_core_level(1,is)
             if ( present(nspin) ) then
              if ( nspin==1 ) w = w/2
             end if
            else if ( ec>min(e_hocl,e0) ) then
             w=zero
            end if
            if ( abs(T)<=epsilon(zero) ) then
              bt = one
             else if ( T>zero ) then
              bt=(ec-ef)/T
              z = dcmplx(bt,zero)
              bt = dreal(TemprFun(z,0))
             else
              bt = zero
             end if
            w = w*bt
             if ( ec >= gSemiCoreLevel() ) then
              esemc = esemc + ec*w
             else
              ecor = ecor + ec*w
             end if
          end do
!
          return
!EOC
      end subroutine g_ecor

!BOP
!!ROUTINE: g_esemicorr
!!INTERFACE:
      function g_esemicorr(ic,nsub,is)
!!DESCRIPTION:
! returns value of the semicore energy component of {\sc mecca} run state;
! component, sublattice and spin dependence are defined by
! input argument {\tt ic, nsub and is}, respectively
!
!!USES:
      use mecca_types, only : RunState
      use mecca_run, only : getMS
!
!!REVISION HISTORY:
! Initial Version - A.S. - 2013
!EOP
!
!BOC
      implicit none
      real(8) :: g_esemicorr
      integer, intent(in) :: ic,nsub,is
      type(RunState), pointer :: M_S=>null()
      g_esemicorr = 0
      M_S => getMS()
      if ( associated(M_S) ) then
       if ( associated(M_S%work_box) ) then
        if (allocated(M_S%work_box%esemicorr) ) then
         g_esemicorr = M_S%work_box%esemicorr(ic,nsub,is)
        end if
       end if
      end if
      return
!EOC
      end function g_esemicorr

!BOP
!!ROUTINE: g_qsub
!!INTERFACE:
      subroutine g_qsub(sublat,nspin,qsub,qeff)
!!DESCRIPTION:
! calculates sublattice charge {\tt qsub} and
! effective charges for sublattice components {\tt qeff}
!
!!USES:
       use mecca_types, only : Sublattice

!!DO_NOT_PRINT
          implicit none
!!DO_NOT_PRINT

!!ARGUMENTS:
          type(Sublattice), pointer, intent(in) :: sublat
          integer, intent(in) :: nspin
          real(8), intent(out) :: qsub
          real(8), intent(out) :: qeff(:)
!!REVISION HISTORY:
! Initial Version - A.S. - 2013
!EOP
!
!BOC
          integer ic
          qsub = 0
          do ic=1,sublat%ncomp
           qsub = qsub + sublat%compon(ic)%concentr *                   &
     &                  (sublat%compon(ic)%zID -                        &
     &                 sum(sublat%compon(ic)%v_rho_box%ztot_T(1:nspin)))
          end do
          if ( size(qeff)>=sublat%ncomp ) then
           do ic=1,sublat%ncomp
            qeff(ic) = (sublat%compon(ic)%zID -                         &
     &          sum(sublat%compon(ic)%v_rho_box%ztot_T(1:nspin))) - qsub
           end do
          end if
       return
!EOC
      end subroutine g_qsub

!BOP
!!ROUTINE: g_numNonESsites
!!INTERFACE:
      function g_numNonESsites( inFile )
!!DESCRIPTION:
! returns number of cell sites without components with Z=0
!
!!USES:
       use mecca_types, only : IniFile, Sublattice
!
!!REVISION HISTORY:
! Initial Version - A.S. - 2013
!EOP
!
!BOC
          implicit none
          type(IniFile), intent(in), pointer :: inFile
          real(8)  :: g_numNonESsites
          type(Sublattice),  pointer :: sublat

          integer nsub,ic

          g_numNonESsites = 0
          do nsub=1,inFile%nsubl
            sublat => inFile%sublat(nsub)
            do ic=1,sublat%ncomp
             if ( sublat%compon(ic)%zID>0 ) then
                 g_numNonESsites = g_numNonESsites +                    &
     &                              sublat%ns*sublat%compon(ic)%concentr
             end if
            end do
          end do
!
          return
!EOC
      end function g_numNonESsites

!BOP
!!ROUTINE: g_zvaltot
!!INTERFACE:
      function g_zvaltot( inFile, non_zero_T )
!!DESCRIPTION:
! returns average number of valence electrons in the cell
! (logical parameter {\tt non\_zero\_T} should be {\em false}
! for T=0 case)
!
!!USES:
       use mecca_types, only : IniFile, Sublattice
!
!!REVISION HISTORY:
! Initial Version - A.S. - 2013
!EOP
!
!BOC
          implicit none
!          type(IniFile), intent(in), target :: inFile
          type(IniFile), intent(in), pointer :: inFile
          logical, intent(in) :: non_zero_T
          real(8)  :: g_zvaltot
          type(Sublattice),  pointer :: sublat
          integer :: ic,is,nsub
          real(8) :: znc,zcorT

          g_zvaltot = 0
          do nsub=1,inFile%nsubl
            sublat => inFile%sublat(nsub)
            do ic=1,sublat%ncomp
              g_zvaltot=g_zvaltot+sublat%ns*sublat%compon(ic)%concentr* &
     &                    (sublat%compon(ic)%zID-sublat%compon(ic)%zcor)
            end do
            if ( non_zero_T .and. inFile%Tempr>0 ) then
             do ic=1,sublat%ncomp
              znc=sum(sublat%compon(ic)%v_rho_box%zcor(1:inFile%nspin))
              if ( znc == 0 ) cycle
              zcorT = 0
              znc = 0
              do is=1,inFile%nspin
               call g_zcorT(sublat%compon(ic)%v_rho_box,is,inFile%ebot, &
     &                                   inFile%Tempr,inFile%etop,znc)
               if ( inFile%nspin == 1 ) then
                zcorT = znc+znc
               else
                zcorT = znc+zcorT
               end if
              end do
              g_zvaltot = g_zvaltot + (sublat%compon(ic)%zcor-zcorT) *  &
     &                            sublat%ns*sublat%compon(ic)%concentr
             end do
            end if
          end do

       return
!EOC
      end function g_zvaltot
!BOP
!!ROUTINE: g_zsemcout
!!INTERFACE:
      function g_zsemcout( inFile, non_zero_T )
!!DESCRIPTION:
! returns total number of semi-core electrons outside ASA spheres
! (logical parameter {\tt non\_zero\_T} should be {\em false}
! for T=0 case)
!
!!USES:
       use mecca_types, only : IniFile, Sublattice
!
!!REVISION HISTORY:
! Initial Version - A.S. - 2014
!EOP
!
!BOC
          implicit none
          type(IniFile), intent(in), pointer :: inFile
          logical, intent(in) :: non_zero_T
          real(8)  :: g_zsemcout
          type(Sublattice),  pointer :: sublat
          integer ic,nsub

          g_zsemcout = 0
          do nsub=1,inFile%nsubl
           sublat => inFile%sublat(nsub)
           if ( non_zero_T ) then
            do ic=1,sublat%ncomp
              g_zsemcout = g_zsemcout + (sublat%compon(ic)%zcor-        &
     &         sum(sublat%compon(ic)%v_rho_box%zcor_T(1:inFile%nspin)))*&
     &           sublat%compon(ic)%concentr*sublat%ns
            end do
           else
            do ic=1,sublat%ncomp
              g_zsemcout = g_zsemcout + (sublat%compon(ic)%zcor-        &
     &         sum(sublat%compon(ic)%v_rho_box%zcor(1:inFile%nspin)))*  &
     &           sublat%compon(ic)%concentr*sublat%ns
            end do
           end if
          end do

       return
!EOC
      end function g_zsemcout

!BOP
!!ROUTINE: g_zsemckkr
!!INTERFACE:
      function g_zsemckkr( mecca_state )
!!DESCRIPTION:
! returns kkr-contribution to semi-core electrons
! (logical parameter {\tt non\_zero\_T} should be {\em false}
! for T=0 case)
!
!!USES:
       use mecca_types, only : RunState, IniFile, Work_data, Sublattice
       use mecca_types, only : SphAtomDeps
       use mecca_interface, only  : ws_integral
!
!!REVISION HISTORY:
! Initial Version - A.S. - 2015
!EOP
!
!BOC
          implicit none
          real(8)  :: g_zsemckkr
          type(RunState),     target :: mecca_state
          type(IniFile),     pointer :: inFile
          type(Work_data),   pointer :: work_box
          type(Sublattice),  pointer :: sublat
          type(SphAtomDeps), pointer :: v_rho_box
          integer :: ic,is,nsub
          real(8), pointer :: rho(:)
          real(8) :: tot_s,qIS

          g_zsemckkr = 0
          if ( .not. associated(mecca_state%intfile) ) return
          if ( .not. associated(mecca_state%work_box) ) return
          inFile => mecca_state%intfile
          work_box => mecca_state%work_box
          if ( .not. allocated(work_box%chgsemi) ) return

          do is=1,inFile%nspin
           do nsub=1,inFile%nsubl
            sublat => inFile%sublat(nsub)
            do ic=1,sublat%ncomp
             v_rho_box => sublat%compon(ic)%v_rho_box
             rho => work_box%chgsemi(1:,ic,nsub,is)
             if ( size(rho) == 0 ) cycle
             if ( rho(1) .ne. 0 ) then
              call ws_integral(mecca_state,ic,nsub,rho(1:),tot_s,qIS)
!              tot_s = tot_s -                                           &
!     &          (sublat%compon(ic)%zcor/inFile%nspin-v_rho_box%zcor(is))
              g_zsemckkr = g_zsemckkr + tot_s *                         &
     &                             sublat%compon(ic)%concentr*sublat%ns
             end if
            end do
           end do
          end do

       return
!EOC
      end function g_zsemckkr

!BOP
!!ROUTINE: g_efestim
!!INTERFACE:
      function g_efestim( Z, volume )
!!DESCRIPTION:
! provides a guess of Fermi level based on {\tt Z} and
! {\tt volume}
!
!!USES:
       use universal_const
!
!!REVISION HISTORY:
! Initial Version - A.S. - 2013
!EOP
!
!BOC
          implicit none
          real(8), intent(in) :: Z,volume
          real(8)  :: g_efestim
! for 1 at/cell:  eF = (9/4*pi*Zval/Rws^3)^(2/3)
          g_efestim = (three*pi**2*Z/volume)**(two/three)
          return
!EOC
      end function g_efestim

!BOP
!!ROUTINE: c_v0
!!INTERFACE:
      subroutine c_v0( inFile, dv0 )
!!DESCRIPTION:
! adds a weighted spin-splitting (or constant) {\tt dv0} to all potentials in the system;
! weights are expected to be predifined for each component
!
!!USES:
       use mecca_types, only : IniFile,SphAtomDeps
       use gfncts_interface, only : g_meshv

!!DO_NOT_PRINT
          implicit none
!!DO_NOT_PRINT

!!ARGUMENTS:
          type(IniFile), intent(inout), pointer :: inFile
          real(8), intent(in) :: dv0(:)
!!REVISION HISTORY:
! Initial Version - A.S. - 2013
!EOP
!
!BOC
          type(SphAtomDeps), pointer :: v_rho_box
          integer :: ic,is,ndrpts,nsub,nspin
          real(8) :: h
          real(8), allocatable :: xr(:),rr(:)
          nspin = inFile%nspin
          do nsub=1,inFile%nsubl
            do ic=1,inFile%sublat(nsub)%ncomp
              v_rho_box => inFile%sublat(nsub)%compon(ic)%v_rho_box
              ndrpts = v_rho_box%ndrpts
              allocate(xr(1:ndrpts),rr(1:ndrpts))
              call g_meshv(v_rho_box,h,xr(1:ndrpts),rr(1:ndrpts))
              do is=1,nspin
               v_rho_box%vr(1:ndrpts,is) = v_rho_box%vr(1:ndrpts,is) +  &
     &                    inFile%sublat(nsub)%compon(ic)%ex_ini_splt *  &
     &                     dv0(is)*rr(1:ndrpts)
              end do
              deallocate(xr,rr)
            end do
          end do
       return
!EOC
      end subroutine c_v0

!BOP
!!ROUTINE: g_reltype
!!INTERFACE:
      subroutine g_reltype(core,semicore,valence)
!!DESCRIPTION:
! returns type of relativistic approximation
!
!!USES:
      use mecca_types, only : RunState
      use mecca_run, only : getMS
      use zcore, only : noSR,SR,noFR,vSR  ! noSR=0,SR=1,noFR=2,vSR=3
!
!!ARGUMENTS:
!      integer, intent(out), optional :: core,semicore,valence
!
!!REVISION HISTORY:
! Initial Version - A.S. - 2015
!EOP
!
!BOC
      implicit none
      integer, intent(out), optional :: core,semicore,valence
      type(RunState), pointer :: M_S=>null()
      integer :: nrel,st_env
      integer :: ctype,stype,vtype
!
!   nrel = 0:   core, semicore and SS valence are Fully-Relativistic, MS valence is Scalar-Relativistic
!   nrel = 1:   core and semicore are Fully-Relativistic, valence is Scalar-Relativistic
!   nrel = 100: core is Fully-Relativistic, semicore, valence are Scalar-Relativistic
!   nrel = 101: same, as nrel=100, but semicore is a part of valence
!   nrel = 200: core, semicore, valence are Scalar-Relativistic
!   1<nrel<32: core, semicore, valence are Non-Relativistic (recommended value is 22)
!
!   nrel is defined in input file
!
!   vtype = 0 --> fully-relativistic for SS valence, and scalar-relativistic for MS
!   vtype = 1 --> scalar-relativistic for valence, otherwise non-relativistic
!   ctype=stype=0 --> fully-relativistic for core and semicore
!   ctype=0, stype=1 --> fully-relativistic for core and scalar-relativistic for semicore
!   ctype=0, stype=2 --> scalar-relativistic for core and semicore
!   ctype=0, stype>=3 --> fully-relativistic for core, scalar-relativistic for semicore ("with valence")
!   ctype>0 --> non-relativistic for core and semicore
!
      M_S => getMS()
      if ( associated(M_S) ) then
       nrel = M_S%intfile%nrel
       nullify(M_S)
      else
       nrel = 0
      end if
      select case ( nrel )
      case (0)     ! relativistic core and SS valence, scalar-relativistic MS
       ctype = 0
       vtype = 0
       stype = 0
      case (1)     ! relativistic core and scalar-relativistic valence
       ctype = 0
       vtype = SR
       stype = 0
      case (2:99)   ! non-relativistic
       ctype = nrel
       vtype = nrel
       stype = noSR
      case (100)    ! scalar-relativistic semicore and valence
       ctype = 0
       vtype = SR
       stype = SR
      case (101)    ! semicore is part of scalar-relativistic valence
       ctype = 0
       vtype = SR
       stype = vSR
      case (200)    ! all scalar-relativistic
        ctype = 0
        stype = noFR
        vtype = SR
      case default
       ctype = 0
       stype = 0
       vtype = 0
      end select
      if ( present(core) )     core = ctype
      if ( present(semicore) ) then
        semicore = stype
      end if
      if ( present(valence) ) then
        valence = vtype
      end if
      return
!EOC
      end subroutine g_reltype

!BOP
!!ROUTINE: hexSymm
!!INTERFACE:
      function hexSymm()
!!DESCRIPTION:
! returns {\em .true.} if k-space cell has hexogonal symmetry,
! otherwise it is {\em .false.}
!
!!USES:
      use mecca_constants, only : pi
      use mecca_types, only : RunState
      use mecca_run, only : getMS
!
!!ARGUMENTS:
! no arguments
!!REVISION HISTORY:
! Initial Version - A.S. - 2013
!EOP
!
!BOC
      implicit none
      logical hexSymm
      integer ihc,nc,ihg,ib(48)
      real(8) r(49,3,3)
      type(RunState), pointer :: M_S=>null()
      real(8) :: kslatt_(3,3)
      real(8) :: twopi=2*pi
      hexSymm = .false.
      M_S => getMS()
      if ( associated(M_S) ) then
        if( associated(M_S%rs_box) .and. associated(M_S%ks_box) ) then
        kslatt_(1,1:3) = M_S%ks_box%kslatt(1:3,1)
        kslatt_(2,1:3) = M_S%ks_box%kslatt(1:3,2)
        kslatt_(3,1:3) = M_S%ks_box%kslatt(1:3,3)
        call pgl1(M_S%rs_box%rslatt/twopi,kslatt_,ihc,nc,ib,ihg,r)
          if ( ihg == 7 ) then
           hexSymm = .true.
          end if
        end if
      end if
      nullify(M_S)
      return
!EOC
      end function hexSymm

!BOP
!!ROUTINE: gNonMT
!!INTERFACE:
      function gNonMT()
!!DESCRIPTION:
! returns {\em .true.} if non muffin-tin approximation
! is used (i.e. mtasa==0 in {\sc mecca} run state)
!
!!USES:
      use mecca_types, only : RunState
      use mecca_run, only : getMS
!
!!ARGUMENTS:
! no arguments
!!REVISION HISTORY:
! Initial Version - A.S. - 2013
!EOP
!
!BOC
      implicit none
      logical gNonMT
      type(RunState), pointer :: M_S=>null()
      gNonMT = .true.
      M_S => getMS()
      if ( associated(M_S) ) then
        gNonMT = M_S%intfile%mtasa .ne. 0
      else
        call fstop('gNonMT :: ERROR :: undefined run state')
      end if
      nullify(M_S)
      return
!EOC
      end function gNonMT

!BOP
!!ROUTINE: addFEforEf
!!INTERFACE:
      function addFEforEf()
!!DESCRIPTION:
! returns {\em true} to take into account free electrons contribution in
! Fermi level calculations, otherwise {\em false} is returned
!
!!USES:
      use mecca_types, only : RunState
      use mecca_run, only : getMS
!
!!ARGUMENTS:
! no arguments
!!REVISION HISTORY:
! Initial Version - A.S. - 2013
!!REMARKS:
! default value is .true. if mtasa>0 and .false. if it is not;
! default behavior can be changed with environment variable ENV_ENABLE_FE
!EOP
!
!BOC
      implicit none
      logical addFEforEf
      integer :: st_env
      type(RunState), pointer :: M_S=>null()
      addFEforEf = .false.
      M_S => getMS()
      if ( associated(M_S) ) then
        addFEforEf = M_S%intfile%mtasa > 0
      else
        addFEforEf = .false.
      end if
      nullify(M_S)
      call gEnvVar('ENV_ENABLE_FE',.false.,st_env)
      if ( st_env==-1 ) addFEforEf = .false.
      if ( st_env>=0 )  addFEforEf = .true.
!
      return
!EOC
      end function addFEforEf

!BOP
!!ROUTINE: gLattConst
!!INTERFACE:
      function gLattConst(iflag)
!!DESCRIPTION:
! return lattice constant for {\sc mecca} run state
!
!!USES:
      use mecca_types, only : RunState
      use mecca_run, only : getMS
!
!!ARGUMENTS:
!  integer, intent(in) :: iflag
!     iflag = 0 (lattice constant from IniFile container)
!     iflag = 1 (lattice constant from RS_Str container)
!
!!REVISION HISTORY:
! Initial Version - A.S. - 2013
!EOP
!
!BOC
      implicit none
      integer, intent(in) :: iflag
      real(8) :: gLattConst
      type(RunState), pointer :: M_S=>null()
      gLattConst = 0
      M_S => getMS()
      if ( associated(M_S) ) then
       if ( iflag==0 .and. associated(M_S%intfile) ) then
        gLattConst = M_S%intfile%alat
       else if (  iflag==1 .and. associated(M_S%rs_box) ) then
        gLattConst = M_S%rs_box%alat
       end if
      end if
      nullify(M_S)
      return
!EOC
      end function gLattConst
!BOP
!!ROUTINE: gRyToDU
!!INTERFACE:
      function gRyToDU(iflag)
!!DESCRIPTION:
! return KKR Ry-to-DimensionalUnit constant
!
!!USES:
      use mecca_types, only : RunState
      use mecca_run, only : getMS
      use universal_const, only : pi
!
!!ARGUMENTS:
!  integer, intent(in) :: iflag
!     iflag = 0 (based on the lattice constant alat)
!     iflag = 1 (based on min lattice parameter)
!
!!REVISION HISTORY:
! Initial Version - A.S. - 2022
!EOP
!
!BOC
      implicit none
      integer, intent(in) :: iflag
      real(8) :: gRyToDU
      real(8) :: a
      type(RunState), pointer :: M_S=>null()
      gRyToDU = 0
      M_S => getMS()
      if ( associated(M_S) ) then
       a = M_S%intfile%alat
       gRyToDU = a/(2*pi)
       if ( iflag==1 ) then
        gRyToDU = min(a,a*M_S%intfile%ba,a*M_S%intfile%ca)/(2*pi)
       else if (iflag.ne.0 ) then
        call fstop('gRyToDU :: ABORT')
!        gRyToDU =  min(NN-distance) OR volume/atom???
       end if
      end if
      nullify(M_S)
      return
!EOC
      end function gRyToDU
!
!BOP
!!ROUTINE: gFreezeCore
!!INTERFACE:
      function gFreezeCore()
!!DESCRIPTION:
! returns a flag for "frozen core" approximation
! (i.e. value of {\tt freeze\_core} in {\sc mecca} run state)
!
!!USES:
      use mecca_types, only : RunState
      use mecca_run, only : getMS
!
!!ARGUMENTS:
! no arguments
!!REVISION HISTORY:
! Initial Version - A.S. - 2013
!EOP
!
!BOC
      implicit none
      integer gFreezeCore
      type(RunState), pointer :: M_S=>null()
      gFreezeCore = 0
      M_S => getMS()
      if ( associated(M_S) ) then
        gFreezeCore = M_S%intfile%freeze_core
      end if
      nullify(M_S)
      return
!EOC
      end function gFreezeCore

!BOP
!!ROUTINE: gRasa
!!INTERFACE:
      function gRasa(nsub,ic)
!DESCRIPTION:
! returns ASA radius,
! arguments  {\tt nsub} and (optional) {\tt ic} define
! sublattice and component numbers; if the optional argument
! is present, {\tt gRasa} returns component ASA radius for scattering,
! otherwise it is the usual WS radius
!
!!USES:
      use mecca_types, only : RunState
      use mecca_run, only : getMS
      use vp_str, only : asa_radii_scaling,rescaleRadii,re_scale
!
!!REVISION HISTORY:
! Initial Version - A.S. - 2015
!EOP
!
!BOC
      implicit none
      real(8) :: gRasa
      integer, intent(in) :: nsub
      integer, intent(in), optional :: ic
      type(RunState), pointer :: M_S=>null()
      real(8), pointer :: atcon(:),r_is(:)
      real(8), allocatable, save :: r_asa(:)
      real(8) :: f1,rws
      integer :: komp
      real(8), external :: g_pi
      gRasa = 0
      M_S => getMS()
      if ( associated(M_S) ) then
       gRasa = M_S%rs_box%rws(nsub)
       if ( present(ic) ) then
        rws = gRasa
        komp = M_S%intfile%sublat(nsub)%ncomp
        if ( komp>1 ) then
         r_is => M_S%intfile%sublat(nsub)%compon(:)%rSph
         atcon => M_S%intfile%sublat(nsub)%compon(:)%concentr
         if ( allocated(r_asa) ) then
          if ( komp>size(r_asa,1) ) deallocate(r_asa)
         end if
         if ( .not.allocated(r_asa) ) then
          allocate(r_asa(komp))
         end if
         if ( re_scale == 1 ) then
          f1 = dble(4)*g_pi()/dble(3)*rws**3
          call asa_radii_scaling(komp,atcon(1:komp),f1,r_is(1:komp),    &
     &                                                   r_asa(1:komp))
         else
          call rescaleRadii(komp,atcon(1:komp),r_is(1:komp),            &
     &                                               rws,r_asa(1:komp))
         end if
         gRasa = min(r_asa(ic),rws)
        end if
       end if
      end if
      nullify(M_S)
      return
!EOC
      end function gRasa

!BOP
!!ROUTINE: gMaxCoreLevel
!!INTERFACE:
      function gMaxCoreLevel()
!!DESCRIPTION:
! returns {\em semicore\_level}; all core-solver eigenvalues greater or
! equal this value are in the semicore
!
!!USES:
      use zcore, only : max_core_level
!
!!ARGUMENTS:
! no arguments
!!REVISION HISTORY:
! Initial Version - A.S. - 2020
!EOP
!
!BOC
      implicit none
      real(8) :: gMaxCoreLevel
      gMaxCoreLevel = max_core_level
      return
!EOC
      end function gMaxCoreLevel

!BOP
!!ROUTINE: gSemiCoreLevel
!!INTERFACE:
      function gSemiCoreLevel()
!!DESCRIPTION:
! returns {\em semicore\_level}; all core-solver eigenvalues greater or
! equal this value are in the semicore
!
!!USES:
      use mecca_types, only : RunState
      use zcore, only : semicore_level
!
!!ARGUMENTS:
! no arguments
!!REVISION HISTORY:
! Initial Version - A.S. - 2015
!EOP
!
!BOC
      implicit none
      real(8) :: gSemiCoreLevel
      gSemiCoreLevel = semicore_level
      return
!EOC
      end function gSemiCoreLevel

!BOP
!!IROUTINE: g_Z
!!INTERFACE:
      function g_Z(ic,nsub)
!!DESCRIPTION:
! integer function returns charge of the nucleus
!
!!USES:
      use mecca_types, only : RunState
      use mecca_run, only : getMS
!
!!INPUT ARGUMENTS:
      integer, intent(in) :: ic,nsub
!!REVISION HISTORY:
! Initial Version - A.S. - 2015
!
!EOP
!BOC
      integer :: g_Z

      type(RunState), pointer :: M_S=>null()
      g_Z = 0
      M_S => getMS()
      if ( associated(M_S) ) then
        g_Z = M_S%intfile%sublat(nsub)%compon(ic)%zID
      else
        g_Z = -1
      end if
      nullify(M_S)
      return
!EOC
      end function g_Z
!

!BOP
!!ROUTINE: gZval
!!INTERFACE:
      function gZval(ic,nsub)
!!DESCRIPTION:
! returns number of valence electrons,
! arguments {\tt ic} and {\tt nsub} define
! component and sublattice numbers
!
!!USES:
      use mecca_types, only : RunState
      use mecca_run, only : getMS
!
!!REVISION HISTORY:
! Initial Version - A.S. - 2013
!EOP
!
!BOC
      implicit none
      real(8) :: gZval
      integer, intent(in) :: ic,nsub
      type(RunState), pointer :: M_S=>null()
      gZval = 0
      M_S => getMS()
      if ( associated(M_S) ) then
        gZval = M_S%intfile%sublat(nsub)%compon(ic)%zID -               &
     &            M_S%intfile%sublat(nsub)%compon(ic)%zcor
      end if
      nullify(M_S)
      return
!EOC
      end function gZval

!      function gInterstlRho()
!      use gfncts_interface, only : g_meshv,g_rho,g_rhocor
!      use mecca_constants
!      use mecca_types, only : RunState,IniFile,SphAtomDeps
!      use mecca_run, only : getMS
!      implicit none
!      real(8) :: gInterstlRho
!      real(8), external :: ylag
!      integer, external :: indRmesh
!      logical, external :: gNonMT
!      type(RunState), pointer :: M_S=>null()
!      type(IniFile),  pointer :: inFile
!      type(SphAtomDeps), pointer :: v_rho_box
!      real(8) :: h,x1,rIS,rho_s
!      real(8), allocatable :: xr(:),rr(:),rho(:),rhoc(:)
!      real(8), parameter :: pi4 = four*pi
!      integer :: nsub,ic,is,jend,jmx,iex
!      integer, parameter :: nlag=3
!
!      gInterstlRho = zero
!!
!      if ( .not. gNonMT() ) return
!!
!      M_S => getMS()
!      inFile => M_S%intfile
!      do nsub=1,inFile%nsubl
!       rho_s = zero
!       do ic=1,inFile%sublat(nsub)%ncomp
!        rIS = M_S%rs_box%rmt(nsub)
!        x1 = log(rIS)
!        v_rho_box => inFile%sublat(nsub)%compon(ic)%v_rho_box
!        jend = size(v_rho_box%chgtot,1)
!        allocate(xr(jend),rr(jend),rho(jend),rhoc(jend))
!        rho = zero
!        call g_meshv(v_rho_box,h,xr,rr)
!        jmx = min(jend,2+indRmesh(rIS,jend,rr(:)))
!        do is=1,inFile%nspin
!          call g_rho(v_rho_box,is,rho)
!          call g_rhocor(v_rho_box,is,rhoc)
!          rho(1:jmx) = rho(1:jmx)-rhoc(1:jmx)
!          rho_s=rho_s+ylag(x1,xr,rho,0,nlag,jmx,iex)/(pi4*rIS**2)       &
!     &                         * inFile%sublat(nsub)%compon(ic)%concentr
!        end do
!        nullify(v_rho_box)
!        deallocate(xr,rr,rho,rhoc)
!       end do
!       if ( nsub==1 ) then
!        gInterstlRho = rho_s
!       else
!        gInterstlRho = min(gInterstlRho,rho_s)
!       end if
!      end do
!      return
!      end function gInterstlRho

!BOP
!!ROUTINE: gInterstlRho1
!!INTERFACE:
      function gInterstlRho1()   ! min(rho)
!!DESCRIPTION:
! returns interstitial density defined as minimum value of charge density
! in interstitial region (i.e. outside inscribed spheres)
!
!!USES:
      use gfncts_interface, only : g_meshv,g_rho,g_rhocor
      use mecca_constants
      use mecca_types, only : RunState,IniFile,SphAtomDeps
      use mecca_run, only : getMS
!
!!ARGUMENTS:
! no arguments
!!REVISION HISTORY:
! Initial Version - A.S. - 2013
!EOP
!
!BOC
      implicit none
      real(8) :: gInterstlRho1
      real(8), external :: ylag
      integer, external :: indRmesh
      logical, external :: gNonMT
      type(RunState), pointer :: M_S=>null()
      type(IniFile),  pointer :: inFile
      type(SphAtomDeps), pointer :: v_rho_box
      real(8) :: h,x1,rIS,rWS,rho_s,rho_k
      real(8), allocatable :: xr(:),rr(:),rho(:),rhoc(:),tmprho(:)
      real(8), parameter :: pi4 = four*pi
      integer :: nsub,nc,ic,is,j0,j1,jend,iex,k,kmx
      integer, parameter :: nlag=3
      integer, parameter :: ks=10

      gInterstlRho1 = zero
!
      if ( .not. gNonMT() ) return
!
      M_S => getMS()
      inFile => M_S%intfile
      rho_s = huge(one)
      do nsub=1,inFile%nsubl
       rWS = M_S%rs_box%rws(nsub)
       nc = inFile%sublat(nsub)%ncomp
       jend = maxval(inFile%sublat(nsub)%compon(1:nc)%v_rho_box%ndrpts)
       allocate(xr(jend),rr(jend),rho(jend),rhoc(jend))
       allocate(tmprho(jend*ks+1))
       rho_k = zero
       do ic=1,nc
        tmprho = zero
        rIS = M_S%rs_box%rmt(nsub)
        v_rho_box => inFile%sublat(nsub)%compon(ic)%v_rho_box
        call g_meshv(v_rho_box,h,xr,rr)
        do is=1,inFile%nspin
          call g_rho(v_rho_box,is,rho)
          j1 = min(jend,2+indRmesh(log(rWS),v_rho_box%ndchg,xr(:)))
          j0 = 1+indRmesh(log(rIS),v_rho_box%ndchg,xr(:))
          if ( v_rho_box%ndcor>0 ) then
           call g_rhocor(v_rho_box,is,rhoc)
           if ( minval(rhoc(1:v_rho_box%ndcor)) > zero ) then
            j0 = indRmesh(log(rIS),v_rho_box%ndchg,xr(:))
            rho(j0:j1) = rho(j0:j1)-rhoc(j0:j1)
           end if
          end if
          kmx = (j1-j0)*ks+1
          h = (xr(j1)-xr(j0))/(kmx-1)
          do k=1,kmx
           x1 = xr(j0) + h*(k-1)
           tmprho(k) = tmprho(k) +                                      &
     &        ylag(x1,xr(j0:j1),rho(j0:j1),0,nlag,j1-j0+1,iex)/         &
     &        (pi4*exp(two*x1))
          end do
        end do
        x1 = tmprho(1)
        do k=2,kmx
         x1 = min(x1,tmprho(k))
        end do
        rho_k = rho_k + x1*inFile%sublat(nsub)%compon(ic)%concentr
        nullify(v_rho_box)
       end do
       rho_s = min(rho_s,rho_k)
       deallocate(xr,rr,rho,rhoc,tmprho)
      end do
      gInterstlRho1 = rho_s
      return
!EOC
      end function gInterstlRho1

!BOP
!!ROUTINE: gInterstlRho
!!INTERFACE:
      function gInterstlRho(rs,zeta)
!!DESCRIPTION:
! returns interstitial density defined as
! total charge in interstitial region (i.e. integral of the electronic
! charge density over the interstitial region) divided by volume of the
! the inverstitial region; arguments {\tt rs} and {\tt zeta} are optional
! output parameters (
!  $r_s = 1/\(\frac{4\pi}{3}  \rho_{Interstl}\)^{1/3}$,
!  $\zeta = \( \rho^{up}_{\small Interstl} - \rho^{dn}_{\small Interstl} \) / \rho_{\tiny Interstl}$
! )
!
!!USES:
      use gfncts_interface, only : g_rhoval
      use gfncts_interface, only : g_numNonESsites
      use mecca_constants
      use mecca_types, only : RunState,IniFile,SphAtomDeps
      use mecca_run, only : getMS
      use mecca_interface, only  : ws_integral
!
!!REVISION HISTORY:
! Initial Version - A.S. - 2013
!EOP
!
!BOC
      implicit none
      real(8) :: gInterstlRho
! 4pi/3*rs^3=1/abs(rho_up+rho_dn), zeta=(rho_up-rho_dn)/(rho_up+rho_dn)
      real(8), intent(out), optional :: rs,zeta
      logical, external :: gNonMT
      type(RunState), pointer :: M_S=>null()
      type(IniFile),  pointer :: inFile
      type(SphAtomDeps), pointer :: v_rho_box
      real(8) :: vol_IS,tot_s,qIS,wght,rIS
      real(8) :: tzatom,zeta_(2)
      real(8), allocatable :: rho(:),rhoc(:)
      real(8), parameter :: pi4d3 = four*pi/three
      integer :: nsub,ic,is,jend

      gInterstlRho = zero
      if ( present(zeta) ) then
       zeta = zero
      end if
      if ( present(rs) ) then
       rs = zero
      end if
!
      if ( .not. gNonMT() ) return
!
      zeta_ = zero
      M_S => getMS()
      inFile => M_S%intfile
      tzatom = g_numNonESsites(inFile)
      vol_IS = zero
      do nsub=1,inFile%nsubl
       do ic=1,inFile%sublat(nsub)%ncomp
        wght = inFile%sublat(nsub)%ns*                                  &
     &                          inFile%sublat(nsub)%compon(ic)%concentr
!        rIS = M_S%rs_box%rmt(nsub)
        rIS = inFile%sublat(nsub)%compon(ic)%rSph
        vol_IS = vol_IS+pi4d3*rIS**3*wght
        v_rho_box => inFile%sublat(nsub)%compon(ic)%v_rho_box
        jend = size(v_rho_box%chgtot,1)
        allocate(rho(jend),rhoc(jend))
        do is=1,inFile%nspin
         call g_rhoval(v_rho_box,is,rho)
         call ws_integral(M_S,ic,nsub,rho(1:jend),tot_s,qIS,rIS)
         gInterstlRho = gInterstlRho + max(zero,tot_s-qIS)*wght
         zeta_(is) = zeta_(is) + (tot_s-qIS)*wght
        end do
        nullify(v_rho_box)
        deallocate(rho,rhoc)
       end do
      end do
      if (abs(gInterstlRho*inFile%V0(1)) < 0.1d0*etol*tzatom) then
       gInterstlRho = zero
       zeta_ = zero
      else
       gInterstlRho = gInterstlRho/(M_S%rs_box%volume-vol_IS)
      end if
      if ( present(rs) ) then
       if ( abs(gInterstlRho) < tiny(rs) ) then
        rs = huge(rs)
       else
        rs = one/(pi4d3*gInterstlRho)**(one/three)
       end if
      end if
      if ( present(zeta) ) then
       if ( gInterstlRho < tiny(zeta) ) then
        zeta = zero
       else
        zeta_ = zeta_/(M_S%rs_box%volume-vol_IS)
        zeta = (zeta_(1)-zeta_(2))/gInterstlRho
       end if
      end if
      return
!EOC
      end function gInterstlRho

!BOP
!!ROUTINE: gMaxErr
!!INTERFACE:
      function gMaxErr(iflag)
!!DESCRIPTION:
! returns max difference between weighted "old" and "new" potential ({\tt iflag=1}) or
! density ({\tt iflag=0})
!
!!USES:
      use mecca_constants, only : zero
      use mecca_types, only : RunState
      use mecca_run, only : getMS
!
!!REVISION HISTORY:
! Initial Version - A.S. - 2013
!EOP
!
!BOC
      implicit none
      real(8) :: gMaxErr
      integer, intent(in) :: iflag
      type(RunState), pointer :: M_S=>null()
      real(8), pointer :: error(:,:,:)=>null()
      integer :: nspin,nsub,nc
      real(8) :: qrmsst
      gMaxErr = zero
      M_S => getMS()
      if ( associated(M_S) ) then
        if ( iflag==0 ) then
          if ( allocated(M_S%work_box%qrms) ) then
            error => M_S%work_box%qrms
          end if
        else if ( iflag==1 ) then
          if ( allocated(M_S%work_box%vrms) ) then
            error => M_S%work_box%vrms
          end if
        end if
        if ( associated(error) ) then
         nspin = M_S%intfile%nspin
         do nsub=1,M_S%intfile%nsubl
          nc = M_S%intfile%sublat(nsub)%ncomp
          qrmsst = dot_product(                                         &
     &               M_S%intfile%sublat(nsub)%compon(1:nc)%concentr,    &
     &                                     abs(error(1:nc,nsub,1)))
          if ( nspin>1 ) then
           if ( iflag==0 ) then
            qrmsst=qrmsst + dot_product(                                &
     &               M_S%intfile%sublat(nsub)%compon(1:nc)%concentr,    &
     &                                     abs(error(1:nc,nsub,nspin)))
           else
            qrmsst = max(qrmsst,dot_product(                            &
     &               M_S%intfile%sublat(nsub)%compon(1:nc)%concentr,    &
     &                                    abs(error(1:nc,nsub,nspin))))
           end if
          endif
          gMaxErr=max(gMaxErr,qrmsst)
         enddo
        end if
      end if
      nullify(M_S)
      nullify(error)
      return
!EOC
      end function gMaxErr

!BOP
!!ROUTINE: gScfItN
!!INTERFACE:
      integer function gScfItN(intfile)
!!DESCRIPTION:
! returns current number of SCF iteration
!
!!USES:
      use mecca_types, only : IniFile
      use kkrgf, only : r_prefx
!
!!REVISION HISTORY:
! Initial Version - A.S. - 2013
!EOP
!
!BOC
      implicit none
      type ( IniFile ), intent(in) :: intfile
      gScfItN = readInt(r_prefx//intfile%genName)
      return

      contains

      integer function readInt(fname)
      implicit none
      character(*), intent(in) :: fname
      logical :: fileexist
      readInt = 0
      inquire(file=fname,EXIST=fileexist)
      if ( fileexist ) then
       open(11,file=fname,status='old',form='unformatted')
       read(11,err=100) readInt
       close(11)
      end if
100   return
      end function readInt
!EOC
      end function gScfItN

!BOP
!!ROUTINE: broyden_nunit
!!INTERFACE:
      function broyden_nunit(i)
!!DESCRIPTION:
! if input argument {\tt i=1 or 2} then the function
! returns number of fortran unit, which is used for writing/reading in Broyden mixing
!
!!USES:
      use mecca_constants, only : nu_mix1,nu_mix2
      use input, only : fnum2nunit
!
!!REVISION HISTORY:
! Initial Version - A.S. - 2013
!EOP
!
!BOC
      integer broyden_nunit
      integer, intent(in) :: i
      select case ( i )
      case(1)
       broyden_nunit = fnum2nunit(nu_mix1)
      case(2)
       broyden_nunit = fnum2nunit(nu_mix2)
      case default
       broyden_nunit=-1
      end select
      return
!EOC
      end function broyden_nunit
!
!BOP
!!ROUTINE: indRmesh
!!INTERFACE:
      integer function indRmesh(Ri,nend,r)
!!DESCRIPTION:
! returns array index {\tt indRmesh}:
! r(indRmesh) < Ri-epsilon .and. Ri<r(indRmesh+1)
!
!EOP
!
!BOC
      real*8 Ri
      integer i,nend
      real*8 r(nend)
      real*8 Rc,onemeps
      parameter (onemeps = 1.d0-1.d-14)

      Rc = Ri*onemeps
      do i=nend,1,-1
       indRmesh = i
       if(r(i).lt.Rc) exit
      end do

      return
!EOC
      end function indRmesh
!c
!BOP
!!ROUTINE: gEnvVar
!!INTERFACE:
      subroutine gEnvVar(cn,print_val,ivar)
!!DESCRIPTION:
! returns integer value {\tt ivar} of environment variable with
! name defined by {\tt cn} or 0 if there is no value
! or non-integer; if variable is not defined, default value
! -1000 is returned.
!
!!REVISION HISTORY:
! Initial Version - A.S. - 2013
!!REMARKS:
! procedure works incorrectly if value of environment
! variable is equal to -1000
!EOP
!
!BOC
      implicit none
      integer, intent(out) :: ivar
      character(*), intent(in) :: cn
      logical, intent(in) :: print_val
      integer st_env
      character(30) :: c_env
      ivar = -1000
      call get_environment_variable(cn,value=c_env,status=st_env)
      if ( st_env == 0 ) then
       ivar = 0
       if ( len_trim(adjustl(c_env)) > 0 ) then
        read(c_env,*,err=1,end=1) ivar
       end if
1      continue
       if ( print_val ) then
        write(6,'('' VARIABLE '',a,a,i4)') cn,' =: ',ivar
       end if
      end if
      return
!EOC
      end subroutine gEnvVar
!
!BOP
!!ROUTINE: g_Lmltp
!!INTERFACE:
      integer function g_Lmltpl(ifl)
!!DESCRIPTION:
! returns max order of multipoles
!
!!USES:
       use mecca_constants, only : ipmltp,ipmltm
!
!EOP
!
!BOC
      implicit none
      integer, intent(in), optional :: ifl
      integer :: i,st_env
      logical :: inside
      inside = .false.
      if ( present(ifl) ) then
       inside = ifl<0
      end if
      call gEnvVar('#',.false.,i)
      if ( inside ) then
       call gEnvVar('ENV_MLTPL1',.false.,st_env)
       if ( st_env == i ) then
        g_Lmltpl = ipmltm
       else if ( st_env<0 ) then
        g_Lmltpl = -1
       else
        g_Lmltpl = st_env
       end if
      else
       call gEnvVar('ENV_MLTPL',.false.,st_env)
       if ( st_env == i ) then
        g_Lmltpl = ipmltp
       else if ( st_env<=0 ) then
        g_Lmltpl = 0
       else
        g_Lmltpl = st_env
       end if
      end if

      return
!EOC
      end function g_Lmltpl
!
!BOP
!!ROUTINE: g_Nmomr
!!INTERFACE:
      integer function g_Nmomr()
!!DESCRIPTION:
! returns max order of polynomials for "fin.elements" VP-integration
!
!!USES:
       use mecca_constants, only : max_rmom
!
!EOP
!
!BOC
      implicit none
      integer :: i,st_env
      call gEnvVar('#',.false.,i)
      call gEnvVar('ENV_NRMOM',.false.,st_env)
      if ( st_env == i ) then
       g_Nmomr = max_rmom
      else if ( st_env<=0 ) then
       g_Nmomr = 0
      else
       g_Nmomr = st_env
      end if

      return
!EOC
      end function g_Nmomr
!
!BOP
!!IROUTINE: descrXC
!!INTERFACE:
      function descrXC(xc_p)
!
!EOP
!!USES:
      use xc_mecca, only : xc_mecca_pointer,gInfo_libxc,es_libxc_p
!
!BOC
      implicit none
      character(512) :: descrXC
      type(xc_mecca_pointer), intent(in) :: xc_p
      if ( es_libxc_p%iexch>0 ) then
       call gInfo_libxc(es_libxc_p,descrXC)
      else
       call gInfo_libxc(xc_p,descrXC)
      end if
      return
!EOC
      end function descrXC
!
!BOP
!!IROUTINE: isLDAxc
!!INTERFACE:
      logical function isLDAxc()
!
!EOP
!!USES:
      use mecca_types, only : RunState
      use mecca_run, only : getMS
      use xc_mecca, only : xc_mecca_pointer,xc_is_gga,gXCmecca
!
!BOC
      implicit none
      type(RunState), pointer :: M_S=>null()
      type(xc_mecca_pointer) :: xc_p
      M_S => getMS()
      if ( associated(M_S) ) then
       call gXCmecca(M_S%intfile%iXC,xc_p)
       isLDAxc = .not. xc_is_gga(xc_p)
       call gXCmecca(-1,xc_p)
      else
       isLDAxc = .false.
      end if
      return
!EOC
      end function isLDAxc
!
!BOP
!!IROUTINE: setEXCtype
!!INTERFACE:
      subroutine setEXCtype(itype)
!
!EOP
!!USES:
      use potential, only : exc_type
!
!BOC
      implicit none
      integer, intent(in) :: itype
      if ( itype<0 ) then
       call get_dflt_exctype(exc_type)
      else
       exc_type = itype
      end if
      return
!EOC
      end subroutine setEXCtype
!
!BOP
!!IROUTINE: getEXCtype
!!INTERFACE:
      subroutine getEXCtype(itype)
!
!EOP
!!USES:
      use potential, only : exc_type
!
!BOC
      implicit none
      integer, intent(out) :: itype
      itype = exc_type
      return
!EOC
      end subroutine getEXCtype
!
!BOP
!!IROUTINE: get_max_exctype
!!INTERFACE:
      subroutine get_max_exctype(itype)
!
!EOP
!!USES:
      use potential, only : max_exctype
!
!BOC
      implicit none
      integer, intent(out) :: itype
      itype = max_exctype
      return
!EOC
      end subroutine get_max_exctype
!
!BOP
!!IROUTINE: get_dflt_exctype
!!INTERFACE:
      subroutine get_dflt_exctype(itype)
!
!EOP
!!USES:
      use potential, only : exc_type_dflt
!
!BOC
      implicit none
      integer, intent(out) :: itype
      itype = exc_type_dflt
      return
!EOC
      end subroutine get_dflt_exctype
!
      subroutine xc_tempr_setpar( px )
      use mecca_types, only : RunState
      use mecca_run, only : getMS
      use universal_const, only : ry2H
      use xc_mecca, only : xc_f90_pointer_t,xc_f90_func_set_ext_params
      implicit none
      type(xc_f90_pointer_t), intent(inout) :: px
      type(RunState), pointer :: M_S=>null()
      M_S => getMS()
!      call xc_f90_lda_xc_ksdt_set_par(px,ry2H*M_S%intfile%Tempr)
      call xc_f90_func_set_ext_params(px,ry2H*M_S%intfile%Tempr)
      return
      end subroutine xc_tempr_setpar
