!BOP
!!ROUTINE: dgnltau
!!INTERFACE:
      subroutine dgnltau(tau00,deltinv,tcpa,ndimt,itype,                &
     &              natom,kkrsz)
!!DESCRIPTION:
! input: tau00=taudelt,  diagonal of block-matrix, order KKRSZ
! \\ \\
! output: tau00 <-- tcpa+tcpa*G*tcpa, here
!                 G = deltinv*taudelt*deltinv-deltinv
!

!!DO_NOT_PRINT
      implicit none
!!DO_NOT_PRINT

!!ARGUMENTS:
      integer ndimt
      complex*16 tau00(ndimt,ndimt,*)
      complex*16 deltinv(ndimt,ndimt,*)
      complex*16 tcpa(ndimt,ndimt,*)
      integer    itype(*),natom,kkrsz
!EOP
!
!BOC
!c      character*10 sname/'dgnltau'/

      integer nsub,ns,i,j,k,nsub2

      complex*16 a(size(tau00,1),size(tau00,2))
      complex*16 b(size(tau00,1),size(tau00,2))
      complex*16 sum,czero,cone
      parameter (czero=(0.d0,0.d0))
      parameter (cone=(1.d0,0.d0))
!c     ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

!c
!c  diagonal (I,I)-block
!c
      do nsub = 1,natom

       ns = itype(nsub)

       do nsub2=1,nsub-1
        if(ns.eq.itype(nsub2)) then            ! copy
         do j = 1,kkrsz
          do i=1,kkrsz
           tau00(i,j,nsub) = tau00(i,j,nsub2)
          end do
         end do
         go to 10
        end if
       end do


       do i=1,kkrsz
        do j=1,kkrsz
!CAB         a(i,j) = tau00(j,i,ns)
         a(i,j) = tau00(j,i,nsub)
        end do
       end do
!c                                 A = transposed[taudelt]

       do i=1,kkrsz
         do j=1,kkrsz
           sum = czero
           do k=1,kkrsz
            sum = sum+a(k,i)*deltinv(k,j,ns)
           end do
           b(i,j) = sum                  ! = taudelt*delta^(-1)
         end do
         b(i,i) = b(i,i) - cone          ! = taudelt*delta^(-1) - 1
       end do

       do i=1,kkrsz
        do j=1,kkrsz
         sum = czero
         do k=1,kkrsz
          sum = sum+deltinv(j,k,ns)*b(k,i)
         end do
         a(i,j) = sum
!c                    transposed[A]=G=delta^(-1)*taudelt*delta^(-1)-delta !^(-1)
        end do
       end do

       do i=1,kkrsz
         do j=1,kkrsz
           sum = czero
           do k=1,kkrsz
            sum = sum+a(k,i)*tcpa(k,j,ns)
           end do
           b(i,j) = sum                  ! = G*tcpa
         end do
         b(i,i) = b(i,i) + cone          ! = G*tcpa+1
       end do

       do i=1,kkrsz
        do j=1,kkrsz
         sum = czero
         do k=1,kkrsz
          sum = sum+tcpa(j,k,ns)*b(k,i)
         end do
         tau00(j,i,nsub) = sum
        end do
       end do
10     continue
      end do

      return
!EOC
      end subroutine dgnltau
