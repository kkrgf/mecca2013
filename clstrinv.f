!c     cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
!BOP
!!ROUTINE: clstrinv
!!INTERFACE:
      subroutine clstrinv(iatom,amt,ndmat,kkrsz,nbasis,ndkkr,           &
     &                    inumnb,jclust,                                &
     &                    tau0,bmt,                                     &
     &                    invswitch,invparam,                           &
     &                    iprint,istop)
!!DESCRIPTION:
! {\bv
! "Cluster" inversion
!
! input: iatom,amt,tmt
!
! output: tau0 = amt_cluster^(-1),
!          cluster[iatom]:= <inumnb,jclust(*)>
! !!! jclust(1,1).EQ.iatom
! \ev}
!!USES:
      use mtrx_interface

!!DO_NOT_PRINT
      implicit none
!!DO_NOT_PRINT

!!ARGUMENTS:
      integer iatom,ndmat,kkrsz,nbasis
      complex*16 amt(ndmat,kkrsz*nbasis)
      integer ndkkr
      integer inumnb,jclust(2,inumnb)
      complex*16   tau0(ndkkr,*)
      complex*16 bmt(inumnb*kkrsz,inumnb*kkrsz)
      integer invswitch,invparam,iprint
      character  istop*10
!EOP
!
!BOC
      complex*16 czero
      parameter (czero=(0.d0,0.d0))
      complex*16 cone
      parameter (cone=(1.d0,0.d0))
      real*8 tol
      parameter (tol=1.d-7)
      character  sname*10
      parameter (sname='clstrinv')

      integer ndbmt,jnn,jatom,knn,katom
!c

      if(jclust(1,1).ne.iatom) then
       write(6,*) ' IATOM=',iatom
       write(6,*) ' JCLUST(1,IATOM)=',jclust(1,1)
       call fstop(sname//' :: jclust(1).NE.iatom')
      end if
      ndbmt = kkrsz*inumnb

!C
      do jnn=1,inumnb
       jatom= jclust(1,jnn)
       call cpblk2(amt,ndmat,jatom,jatom,                               &
     &                bmt,ndbmt,jnn,jnn,kkrsz)
       do knn=1,inumnb
        if(jnn.ne.knn) then
         katom= jclust(1,knn)
         call cpblk2(amt,ndmat,jatom,katom,                             &
     &                bmt,ndbmt,jnn,knn,kkrsz)
        end if
       end do
      end do

      call invmatr(bmt,                                                 &
     &             kkrsz,inumnb,                                        &
     &             invswitch,invparam,                                  &
     &             iprint,istop)

!C       call zgemm('N','N',kkrsz,kkrsz,kkrsz,
!C     *            cone,tmt,ndkkr,bmt,ndbmt,
!C     *            czero,tau0,ndkkr)
       call cpblk2(bmt,ndbmt,1,1,                                       &
     &                tau0,ndkkr,1,1,kkrsz)

      if( istop .eq. sname ) then
        call fstop(sname)
      endif
!c
      return
!EOC
      end subroutine clstrinv

