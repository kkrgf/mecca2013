!BOP
!!ROUTINE: ws_integral
!!INTERFACE:
      subroutine ws_integral(mecca_state,ic,nsub,rho,q_ws,q_mt,R_mt)
!!DESCRIPTION:
! VP or ASA integration: \\
! it is not a generic subroutine; {\tt rho} (or other radial function) should 
! be defined on r-mesh from {\tt mecca\_state\%v\_rho\_box} for a component
! with number {\tt ic} of sublattice with number {\tt nsub}
!      
!!USES:
       use mecca_types, only : IniFile, SphAtomDeps
       use mecca_types, only : RunState, RS_Str, VP_data
       use mecca_interface, only : asa_integral
       use gfncts_interface
       use isoparintgr_interface

!!DO_NOT_PRINT
       implicit none
!!DO_NOT_PRINT

!!ARGUMENTS:
       type(RunState), intent(inout), target :: mecca_state
       integer, intent(in) :: ic,nsub
       real(8), intent(in) :: rho(:)
       real(8), intent(out) :: q_ws,q_mt
       real(8), optional, intent(in) :: R_mt
!!REVISION HISTORY:
! Initial Version - A.S. - 2013
!!REMARKS:
! internal parameters of mecca_state define if 
! VP or ASA integration is performed 
!EOP
!
!BOC
       type ( IniFile ),   pointer :: inFile => null()
       type ( RS_Str ),    pointer :: rs_box => null()
       type (VP_data),    pointer :: vp_box => null()
       type(SphAtomDeps),  pointer :: v_rho_box => null()
       real(8), allocatable :: xr(:),rr(:)
       real(8), allocatable :: wi(:)
       real(8) :: VPint(2),h,x1,rIS
       integer iex,jend 
       real(8), external :: ylag
       integer, parameter :: ia=2,nlag=3
       logical lVP

       inFile => mecca_state%intfile
       lVP = .false.
       if ( associated(mecca_state%vp_box) ) then
        vp_box => mecca_state%vp_box
        lVP = vp_box%nF>0 .and. inFile%mtasa>1 .and. inFile%intgrsch>1
       end if 
       if ( lVP ) then
         rs_box => mecca_state%rs_box
         v_rho_box => inFile%sublat(nsub)%compon(ic)%v_rho_box
         jend = size(v_rho_box%vr,1)
         allocate(xr(jend),rr(jend),wi(jend))
         call g_meshv(v_rho_box,h,xr,rr)
         jend = min(size(rho),v_rho_box%ndchg)
         call qexpup(1,rho,jend,xr,wi)
         if ( allocated(vp_box%rmt) ) then
          rIS = vp_box%rmt(nsub)
         else  
          rIS = rs_box%rmt(nsub)
         end if  
         x1 = log(rIS)
         q_mt = ylag(x1,xr,wi,0,nlag,jend,iex) + rho(1)*rr(1)/3
!  VP-integration
         if ( present(R_mt) ) then
          x1 = log(R_mt)
          q_ws = ylag(x1,xr,wi,0,nlag,jend,iex) + rho(1)*rr(1)/3
         else
          q_ws = q_mt
         end if
         call isopar_chbint(ia,rIS,                                     &
     &                    jend,rr(1:jend),rho(1:jend),                  &
     &                    rs_box%rcs(nsub),                             &
     &                      vp_box%fcount(nsub),  vp_box%weight,        &
     &                      vp_box%rmag(0,:,:,:,:,nsub),                &
     &                      vp_box%vj(:,:,:,:,nsub),VPInt)
         q_ws = q_ws + VPint(2)
         deallocate(xr,rr,wi)
         nullify(v_rho_box)
       else
        if ( present(R_mt) ) then
         call asa_integral(mecca_state,ic,nsub,rho,q_ws,q_mt,R_mt)
        else
         call asa_integral(mecca_state,ic,nsub,rho,q_ws,q_mt)
        end if
       end if
       return
!EOC
      end subroutine ws_integral

!BOP
!!ROUTINE: asa_integral
!!INTERFACE:
      subroutine asa_integral(mecca_state,ic,nsub,rho,q_ws,q_mt,R_mt)
!!DESCRIPTION:
! ASA integration: \\
! it is not a generic subroutine; {\tt rho} (or other radial function) should 
! be defined on r-mesh from {\tt mecca\_state\%v\_rho\_box} for a component
! with number {\tt ic} of sublattice with number {\tt nsub}
!      
!!USES:
       use mecca_types, only : IniFile, SphAtomDeps
       use mecca_types, only : RunState, RS_Str
       use gfncts_interface

!!DO_NOT_PRINT
       implicit none
!!DO_NOT_PRINT

!!ARGUMENTS:
       type(RunState), intent(inout), target :: mecca_state
       integer, intent(in) :: ic,nsub
       real(8), intent(in) :: rho(:)
       real(8), intent(out) :: q_ws,q_mt
       real(8), optional, intent(in) :: R_mt
!!REVISION HISTORY:
! Initial Version - A.S. - 2007
!!REMARKS:
! this subroutine should be used only if specifically ASA integration 
! is needed instead of VP integration; otherwise use of ws_integral 
! procedure is recommended
!EOP
!
!BOC
       type ( IniFile ),   pointer :: inFile
       type ( RS_Str ),    pointer :: rs_box
       type(SphAtomDeps),  pointer :: v_rho_box
       real(8), allocatable :: xr(:),rr(:)
       real(8), allocatable :: wi(:)
       real(8) :: h,x1,rIS
       integer iex,jend 
       real(8), external :: ylag
       integer, parameter :: nlag=3
       integer, external :: indRmesh

       inFile => mecca_state%intfile
       rs_box => mecca_state%rs_box
       v_rho_box => inFile%sublat(nsub)%compon(ic)%v_rho_box
       jend = size(v_rho_box%vr,1)
       allocate(xr(jend),rr(jend),wi(jend))
       call g_meshv(v_rho_box,h,xr,rr)
       jend = min(jend,1 + indRmesh(rs_box%rws(nsub),jend,rr))
       call qexpup(1,rho,jend,xr,wi)
       if ( present(R_mt) ) then
        rIS = R_mt
       else
        rIS = rs_box%rmt(nsub)
       end if
       if ( rIS-rr(jend)>rr(jend)*1.d-14 ) then
        write(*,'('' nsub='',i4,'' ic='',i2,'' rIS='',e12.5,            &
     &                                     '' > r('',i5,'')='',e12.5)') &
     &        nsub,ic,rIS,jend,rr(jend)
        call p_fstop(' asa_integral: INCONSISTENT RADIAL MESH DATA')
       end if
       x1 = log(rIS)
       q_mt = ylag(x1,xr,wi,0,nlag,jend,iex) + rho(1)*rr(1)/3
       if ( inFile%mtasa == 0 ) then
!  MT-scheme
        q_ws = q_mt      !  in fact, q_ws could be equal to q_mt + rho_intrstl*(V_asa-V_mt)
       else
!  ASA-scheme
!           
!        if ( associated(mecca_state%vp_box) ) then
!         if ( allocated(vp_box%rmt) ) then
!          x1 = log(mecca_state%vp_box%rmt(nsub))
!          q_mt = ylag(x1,xr,wi,0,nlag,jend,iex) + rho(1)*rr(1)/3
!         end if 
!        end if 
!           
         if ( rs_box%rws(nsub)-rr(jend) > rr(jend)*1.d-14 ) then
          write(*,'('' nsub='',i4,'' ic='',i2,'' rWS='',e12.5,          &
     &                                     '' > r('',i5,'')='',e12.5)') &
     &        nsub,ic,rs_box%rws(nsub),jend,rr(jend)
          call p_fstop(' asa_integral: INCONSISTENT RADIAL MESH DATA')
         end if
         x1 = log(rs_box%rws(nsub))
         q_ws = ylag(x1,xr,wi,0,nlag,jend,iex) + rho(1)*rr(1)/3
       end if  
       deallocate(xr,rr,wi)
       nullify(v_rho_box)
       return
!EOC
      end subroutine asa_integral

