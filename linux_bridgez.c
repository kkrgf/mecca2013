#include <stdlib.h>
#include <stdio.h>

#include "SuperLU/SRC/zsp_defs.h"
#include "SuperLU/SRC/util.h"
#include "SuperLU/SRC/Cnames.h"

c_bridgez__(int *n, int *nnz, int *nrhs, doublecomplex *values, int *rowptr,
	   int *colind, doublecomplex *b, int *ldb, int *info,
	   int *work, int *lwork, int *iflag)
{

#ifdef SCORE
c_bridgez_(*n, *nnz, *nrhs, *values, *rowptr,
	   *colind, *b, *ldb, *info,
	   *work, *lwork, *iflag);
#endif

#ifndef SCORE
c_bridgez(*n, *nnz, *nrhs, *values, *rowptr,
	   *colind, *b, *ldb, *info,
	   *work, *lwork, *iflag);
#endif

}
