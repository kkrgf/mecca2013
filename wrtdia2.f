!BOP
!!ROUTINE: wrtdia2
!!INTERFACE:
      subroutine wrtdia2(x,kkrsz,nbasis,istop)
!!DESCRIPTION:
! writes out diagonal elmnts of kkrsz*kkrsz*nbasis complex matrix {\tt x}
!

!!DO_NOT_PRINT
      implicit none
!!DO_NOT_PRINT

!!ARGUMENTS:
      integer, intent(in) :: kkrsz,nbasis
      complex(8), intent(in) :: x(:,:,:)  ! (ipkkr,ipkkr,*)
      character(10), intent(in) :: istop
!EOP
!
!BOC
      character(*), parameter :: sname='wrtdia2'
      integer nsub,i
!c
      do nsub=1,nbasis
         write(6,'(''  nsub='',i3)') nsub
         do i=1,min(kkrsz,size(x,1),size(x,2))
            write(6,'(2i3,2d15.7)') i,i, x(i,i,nsub)
         enddo
      enddo
!c
!c     =============================================================
      if (istop.eq.sname) then
         call fstop(sname)
      else
         return
      endif
!EOC
      end subroutine wrtdia2
