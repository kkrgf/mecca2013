!BOP
!!ROUTINE: genpot
!!INTERFACE:
      subroutine genpot(xvalws,qtotmt,ztotss,qcorss,                    &
     &                  rho,vrnew,ivar_mtz,                             &
     &                  vmtzup,omegws,alat,                             &
     &                  itype,numbsub,nsublat,rslatt,xyz,               &
     &                  rr,xr,jmt,rmt_true,r_circ,                      &
     &                    vp_box,                                       &
     &                  atcon,komp,nbasis,nspin,nMM,iexch,              &
     &                  excort,qintex,emad,emadp,emtc,                  &
     &                  vdif,Rnn,Eccsum,mtasa,iprint,istop)
!!DESCRIPTION:
! solves electrostatic problem and calculates radial site-potentials
!
!!USES:
      use mecca_constants
      use mecca_types
      use isoparintgr_interface
      use potential, only : newpot
      use xc_mecca, only : xc_alpha,xc_alpha2,xc_mecca_pointer,gXCmecca

!!DO_NOT_PRINT
      implicit none
!!DO_NOT_PRINT

!!ARGUMENTS:
      real(8), intent(in)  :: xvalws(:,:,:)  ! (ipcomp,ipsublat,ipspin)
      real(8), intent(in)  :: qtotmt(:,:,:)  ! (ipcomp,ipsublat,ipspin)
      real(8), intent(in)  :: ztotss(:,:)    ! (ipcomp,ipsublat)
      real(8), intent(in)  :: qcorss(:,:,:)  ! (ipcomp,ipsublat,nspin)
      real(8), intent(in)  :: rho(:,:,:,:)   ! (iprpts,ipcomp,ipsublat,ipspin)
      real(8), intent(out) :: vrnew(:,:,:,:) ! (iprpts,ipcomp,ipsublat,ipspin)
      integer, intent(in)  :: ivar_mtz
      real(8), intent(out) :: vmtzup
      real(8), intent(in)  :: omegws  ! cell volume
      real(8), intent(in)  :: alat
      integer, intent(in)  :: nbasis,itype(nbasis)
      integer, intent(in)  :: nsublat,numbsub(nsublat)
      real(8)              :: rslatt(3,3)
      real(8), intent(in)  :: xyz(:,:)       ! (3,nbasis)
      real(8), intent(in)  :: rr(:,:,:)      ! (iprpts,ipcomp,nsublat)
      real(8), intent(in)  :: xr(:,:,:)      ! (iprpts,ipcomp,nsublat)
      integer, intent(in)  :: jmt(:,:)       ! (ipcomp,nsublat)
      real(8), intent(in)  :: rmt_true(:,:)  ! (ipcomp,nsublat)
      real(8), intent(in)  :: r_circ(:,:)    ! (ipcomp,nsublat)
      type(VP_data), intent(in) :: vp_box
      real(8), intent(in)  :: atcon(:,:)     ! (ipcomp,ipsublat)
      integer, intent(in)  :: komp(nsublat)
      integer, intent(in)  :: nspin,nMM,iexch
      real(8), intent(out) :: excort(:)
      real(8), intent(out) :: qintex(:)
      real(8), intent(out) :: emad   ! Madelung (ES lattice) energy
      real(8), intent(out) :: emadp  ! Madelung pressure contribution 
      real(8), intent(out) :: emtc   ! MT-correction for ASA 
      real(8), intent(out) :: vdif
      real(8), intent(in)  :: Rnn(nsublat)
      real(8), intent(out) :: Eccsum
      integer, intent(in)  :: mtasa,iprint
!!REVISION HISTORY:
! Adapted - A.S. - 2013
!!REMARKS:
! this is "mecca_ames" version
!EOP
!
!BOC
!      
!c
!c     =================================================================
!c        based on genpot version of DDJ & WAS (Dec 1993)      
!c     =================================================================
!c
      character(10)        :: istop
!c
!      integer  fcount(nsublat)
!      real*8     weight(:)  ! (MNqp)
!      real*8     rmag(:,:,:,:,:,:)   ! (4,MNqp,MNqp,MNqp,MNF,nsublat)
!      real*8     vj(:,:,:,:,:)     ! (MNqp,MNqp,MNqp,MNF,nsublat)
!      
!c
!      real(8), intent(in) :: R_nn

      logical :: iVP  ! (if .true. then VP-data (shapes,radii,jacobian can be used)
      real(8) ::     qsub(nsublat)
      real(8) ::     dzws(size(atcon,1),size(atcon,2))
      real(8) ::     xtotws(size(atcon,1),size(atcon,2),nspin)
      real(8) ::     xk,qk
      real(8) ::     xmommt(size(atcon,1),size(atcon,2))
      real(8) ::     xmomws(size(atcon,1),size(atcon,2))
!c      real*8     madmat(ipbase,ipbase)
!      real*8     madmat(nbasis,*)
      real(8) :: omegmt    ! no need for array (nsublat)
      real(8) :: omegint
!      real*8     surfamt(nsublat)
      real(8) :: excint(nspin),vxcint(nspin)
!      real(8) ::     qout(nspin)
!      real(8) ::     vmt1(nsublat)
      real(8) :: pot_mt(nsublat),pmad(nsublat)
      real(8) :: potmad(nsublat)
      real(8) :: potmtc(nsublat)
      real(8) :: cm(nsublat)
      real(8) :: dm(3,nsublat)
      real(8) :: qm(3,3,nsublat)
      real(8) :: hm3(3,3,3,nsublat)
      real(8) :: hm4(3,3,3,3,nsublat)
      real(8) :: fxyzES(3,nsublat)   ! ES is for ElectroStatic
      real(8) :: tmparr(3,nsublat)   !
      real(8) ::     vmtz(ipspin)
      real(8) ::     qeff(size(atcon,1),nsublat)
      real(8) ::     V_mf_cc(size(atcon,1),nsublat)
      real(8) ::     E_mf_cc(size(atcon,1),nsublat)
      real(8) ::     Eccsum0
      real(8) ::     qinter
      real(8) ::     xmommtt
      real(8) ::     xmomwst
      real(8) ::     rhomtc
      real(8) :: rmt(nsublat),rrjmt
      real(8) ::     excout
!c      real*8     vxout
!CDEBUG
      integer, save :: iverybad=0
!CDEBUG
      real(8), external :: scr_mfcc_const
      real(8), external :: ylag
      integer, external :: indRmesh
      integer :: ndrpts,ir,ic,is,jend,nsub
      real(8) :: avgr2,cmf_cc,dUc,dzout,emtcM,En,Pot,q0,q0mt,qsubmt
      real(8) :: rhointer,xminter,vqeff,v0sph,rws
      type(xc_mecca_pointer) :: libxc_p  
      type(xc_mecca_pointer), external :: gLibxcP
      logical :: addMTC
      integer :: st_env
!      integer    ik1,ik2
!      real*8     rav1,r3av1,r3av2
!c
!c +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!c parameter
      real(8), parameter :: pi4=four*pi,pi4d3 = four*pi/three
      real(8), parameter :: third=one/three
      real(8), parameter :: fifth=one/five
      real(8), parameter :: sixfifth=six/five
      character(80) :: coerr
      character(*), parameter :: sname='genpot'
!c +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!c
      ndrpts = size(xr,1)
!c     ================================================================
!c     debugging.......................................................
      if(iprint.ge.3) then
       write(6,*) sname//':'
       do is=1,nspin
        do nsub=1,nsublat
         do ic=1,komp(nsub)
            write(6,'(''  is,nsub,ic,xvalws,qtotmt:'',3i5,2d15.7)')     &
     &            is,nsub,ic,xvalws(ic,nsub,is),qtotmt(ic,nsub,is)
           if(iprint.ge.4) then
            write(6,'(''  chg. den printout'')')
            write(6,'(''  is,nsub,ic,ir,rho:'',4i5,d15.7)')             &
     &        (is,nsub,ic,ir,rho(ir,ic,nsub,is),ir=1,jmt(ic,nsub),100)
           endif
         enddo
        enddo
       enddo
      endif

      iVP = vp_box%nF>0
      dm = zero
      qm = zero
      hm3 = zero
      hm4 = zero
      fxyzES = zero
!c
!c     ================================================================
!c     sum integrated n(e) over spins...................................
!c     also, calculate interstitial charge for check.
!c     ================================================================
      qintex = zero
      excort = zero
!      qout = zero
!c
      omegint = omegws
      do nsub=1,nsublat
       rmt(nsub) = ( dot_product(atcon(1:komp(nsub),nsub),              &
     &                          rmt_true(1:komp(nsub),nsub)**3) )**third
       omegint = omegint - numbsub(nsub)*pi4d3*rmt(nsub)**3 
      end do
      do is=1,nspin
       do nsub=1,nsublat
        do ic=1,komp(nsub)
          xtotws(ic,nsub,is)=xvalws(ic,nsub,is) + qcorss(ic,nsub,is)
!c
!          qout(is)=qout(is) + atcon(ic,nsub)*                           &
!     &                         numbsub(nsub)*                           &
!     &                    ( xtotws(ic,nsub,is)-qtotmt(ic,nsub,is) )
        enddo
       enddo
      enddo
!c     ================================================================
!c     calculate total charge in Wigner-Seitz cell.....................
!c     sum over spins in charge density .....................
!c     ================================================================
      do nsub=1,nsublat
         do ic=1,komp(nsub)
!c
            dzws(ic,nsub)=(  rho(jmt(ic,nsub),ic,nsub,    1)            &
     &                     - rho(jmt(ic,nsub),ic,nsub,nspin) )          &
     &                 /  (  rho(jmt(ic,nsub),ic,nsub,    1)            &
     &                     + rho(jmt(ic,nsub),ic,nsub,nspin) )
!c
         enddo
      enddo
!c
!c     ==================================================================
!c     calculate average interstitial charge & charge density:
!c     ==================================================================
      qinter  = zero
      xminter = zero
      xmommtt = zero
      xmomwst = zero
!c
      do nsub=1,nsublat
         do ic=1,komp(nsub)
           xk = sum(xtotws(ic,nsub,1:nspin))
           qk = sum(qtotmt(ic,nsub,1:nspin))
           qinter=qinter+atcon(ic,nsub)*numbsub(nsub)*( xk-qk ) 
!c
           xmommt(ic,nsub) =(qtotmt(ic,nsub,1) - qtotmt(ic,nsub,nspin))
           xmomws(ic,nsub) =(xtotws(ic,nsub,1) - xtotws(ic,nsub,nspin))
!c
           xmommtt = xmommtt + atcon(ic,nsub)*xmommt(ic,nsub)           &
     &                       * numbsub(nsub)
           xmomwst = xmomwst + atcon(ic,nsub)*xmomws(ic,nsub)           &
     &                       * numbsub(nsub)
!c
           xminter = xminter + atcon(ic,nsub)                           &
     &                          *( xmomws(ic,nsub) - xmommt(ic,nsub) )  &
     &                       * numbsub(nsub)
         enddo
!c----------------------------------------------------------------------- !-
!c site moments for each sublattice
!c----------------------------------------------------------------------- !-
       if(nspin.gt.1.and.iprint.ge.-1) then
        write(6,'(''  magn.mom.: subl='',i3,'' comp='',i1,              &
     &            '' in sphere'',f13.6,'', in w.s. cell'',f13.6)')      &
     &    (nsub,ic,xmommt(ic,nsub),xmomws(ic,nsub),ic=1,komp(nsub))
       end if
      enddo
!c----------------------------------------------------------------------- !-
!c avg. moments for system
!c----------------------------------------------------------------------- !-
      if(nspin.gt.1.and.iprint.ge.-10) then
       write(6,*)
       write(6,'(''  Avg.Magn.Moment:  in sphere'',f13.6,               &
     &          '',  in w.s. cell'',f13.6)')xmommtt,xmomwst
       write(6,*)
      end if
!c
!c----------------------------------------------------------------------- !-
!c value of rhoint for MT and ASA cases
!c----------------------------------------------------------------------- !-
      if( mtasa .eq. 0 ) then
        rhointer = qinter/omegint
        dzout    = xminter/qinter
!cab
        if( qinter.le.zero .or. omegint.le.zero ) then
         write(6,*) sname//':  rhointer = ',rhointer
         write(6,*) '           qinter, omegint = ',qinter,omegint
         call fstop(sname//':: rhointer <= 0 ???')
        end if
        if(iprint.ge.0) then
         write(6,'(''  rhointer,qinter,omegint '',3d15.7)')             &
     &             rhointer,qinter,omegint
        end if
        rhomtc = rhointer
!c Skriver's definition of rho_0
!          rhomtc = 0.0d0
!          vol_MTsph = 0.0d0
!          do nsub=1,nsublat
!           do ic=1,komp(nsub)
!            xrmt = log(rmt_true(ic,nsub))
!            nlag = 3
!            do ir = 1,jmt(ic,nsub)
!             totrho(ir,ic) = rho(ir,ic,nsub,1)                          &
!     &                    + ( nspin - 1 )*rho(ir,ic,nsub,nspin)
!            enddo
!             call qexpup(1,totrho(1,ic),jmt(ic,nsub),xr(1,ic,nsub),     &
!     &                  qint)
!
!            qmt = ylag(xrmt,xr(1,ic,nsub),qint,                         &
!     &                                 0,nlag,jmt(ic,nsub),iex)
!            rhomtc = rhomtc + atcon(ic,nsub)*numbsub(nsub)*             &
!     &                ( ztotss(ic,nsub) - qmt )
!            vol_MTsph = vol_MTsph + atcon(ic,nsub)*numbsub(nsub)*       &
!     &                  (4.0d0*pi/3.0d0)*(rmt_true(ic,nsub)**3.0d0)
!           enddo            ! End of ic-Loop
!          enddo             ! End of nsub-Loop
!          rhomtc = rhomtc/(omegws - vol_MTsph)
!c
      else
        rhointer = zero
        excout   = zero
!c        vxout    = zero
        omegint  = zero
        dzout    = zero
!        
        avgr2  = zero
        addMTC = (mtasa==1)
        call gEnvVar('ENV_MTC',.false.,st_env)
        if  ( st_env==1 ) addMTC=.true.
        if  ( st_env==0 ) addMTC=.false.
        rhomtc = zero     ! Interstitial rho defined as rho(r=Rasa) averaged over species
        if (  addMTC ) then
         do nsub=1,nsublat
          do ic=1,komp(nsub)
           rrjmt= rr(jmt(ic,nsub),ic,nsub)
           avgr2 = avgr2 + rrjmt**2*(atcon(ic,nsub)*numbsub(nsub))
           if ( ztotss(ic,nsub) .ne. zero ) then
            rhomtc = rhomtc + sum(rho(jmt(ic,nsub),ic,nsub,1:nspin))*   &
     &                              (atcon(ic,nsub)*numbsub(nsub))
           else 
            rhomtc = rhomtc +                                           &
     &  (sum(qtotmt(ic,nsub,1:nspin))/(pi4d3*rrjmt**3))*(pi4*rrjmt**2)* &
     &                              (atcon(ic,nsub)*numbsub(nsub))
           end if 
          enddo
         enddo
         rhomtc = (rhomtc/pi4) / avgr2
!        else if ( iVP ) then   ! TODO: to change definition of interst.density
!         do nsub=1,nsublat
!          do ic=1,komp(nsub)
!           rrjmt= rr(jmt(ic,nsub),ic,nsub)
!           avgr2 = avgr2 + rrjmt**2*(atcon(ic,nsub)*numbsub(nsub))
!           rhomtc = rhomtc + sum(rho(jmt(ic,nsub),ic,nsub,1:nspin))*    &
!     &                              (atcon(ic,nsub)*numbsub(nsub))
!          enddo
!         enddo
!         rhomtc= (rhomtc/pi4) / avgr2
        end if      
!c        
      endif

!c     ==================================================================
!c     calculate qsub (effective charge inside spheres). . . . . . . . .
!c      qsub = sum_c [Z(comp,basis)-Qmt(comp,basis)+rhoint*omagmt(basis)]
!c     ==================================================================
!c

      do nsub=1,nsublat
       qsub(nsub)=zero
       omegmt = pi4d3*rmt(nsub)**3 
       do ic=1,komp(nsub)
        qk = sum(qtotmt(ic,nsub,1:nspin))
        qeff(ic,nsub)=ztotss(ic,nsub)-qk+rhointer*omegmt   ! it is assumed that rhointer=0 if mtasa.ne.0
        qsub(nsub)=qsub(nsub) + atcon(ic,nsub)*qeff(ic,nsub)
       end do
      end do
      if(iprint.ge.0) then
       do nsub=1,nsublat
        if ( komp(nsub) == 1 ) then
          xk = sum(xtotws(1,nsub,1:nspin))
          qk = sum(qtotmt(1,nsub,1:nspin))
          write(6, 998) nsub,qsub(nsub),xk,qk,ztotss(1,nsub)
998       format('  nsub=',i4,' qsub=',f10.6,                           &
     &     ' xtotws=',f10.6,' qtot=',f10.6,' ztotss=',f10.6)
        else
          write(6,999) nsub,qsub(nsub)
999       format('  nsub=',i4,' qsub=',f10.6)
          do ic=1,komp(nsub)
           xk = sum(xtotws(ic,nsub,1:nspin))
           qk = sum(qtotmt(ic,nsub,1:nspin))
           write(6,1000) nsub,ic,qeff(ic,nsub),xk,qk,ztotss(ic,nsub)
1000       format('  nsub=',i4,' ic=',i3,' qeff=',f10.6,                &
     &     ' xtotws=',f10.6,' qtot=',f10.6,' ztotss=',f10.6)
          end do
        end if
       end do
      end if

!c     if(iprint.ge.-1) write(6,*)

      do nsub=1,nsublat
!c Effective Charge (per component)
       qeff(1:komp(nsub),nsub) = qeff(1:komp(nsub),nsub) - qsub(nsub)
      end do

      E_mf_cc = zero
      V_mf_cc = zero
      Eccsum=zero
      cmf_cc=zero
      if( mtasa>0) then
       rws = (omegws/pi4d3)**third
       do nsub=1,nsublat
        Eccsum0=zero
        if ( komp(nsub)>1 ) then
         cmf_cc = scr_mfcc_const(rws/Rnn(nsub))
         do ic=1,komp(nsub)
!c Mean field charge-correlation energy  (e**2 == two -- in at.un.)
!         E_mf_cc(ic,nsub)  =  -(two*c_mf_cc)*qeff(ic,nsub)**2/R_nn
          vqeff = two*(two*cmf_cc)*qeff(ic,nsub)/Rnn(nsub)
          V_mf_cc(ic,nsub) = vqeff
          E_mf_cc(ic,nsub) = -0.5d0*vqeff*qeff(ic,nsub)

!c  ==================================================================
!c   c_mf_cc = 0.0      standard CPA  (NO charge correlation)
!c   c_mf_cc = 1.0/2    scr-CPA   Johnson & Pinsk, PRB48, 11553 (1993)
!c   c_mf_cc = 1.3147/2 FCC analytic  Magri et al. PRB42, 11388 (1990)
!c   c_mf_cc = 1.3831/2 BCC analytic
!c   c_mf_cc = 2.0/2    SIM       Korzhavyi et al. PRB51,  5773 (1995)
!c
!c   Presently: c_mf_cc is defined in scr_mfcc.f
!c  ==================================================================

          Eccsum0 = Eccsum0 + E_mf_cc(ic,nsub)*atcon(ic,nsub)
         enddo
         Eccsum = Eccsum + Eccsum0*numbsub(nsub)
         if(iprint.ge.0) then
          write(6,*)'      MF-CC CPA, E (per component):'
          do ic=1,komp(nsub)
           write(6,1002) ic,E_mf_cc(ic,nsub),qeff(ic,nsub)
1002      format('  ic=',i2,' E_mf_cc =',f12.8,                         &
     &           ' (dQ(ic,nsub)-dQavg(nsub))=',f11.8)
          end do
          if(abs(Eccsum0).gt.1.d-8) then
           write(6,'(''  MF per sublatt.('',                            &
     &               i3,''), charge-correlated E = '',                  &
     &         f12.8)') nsub,Eccsum0
          end if
         end if
        end if
       enddo
!
      end if
!c
      if(cmf_cc .gt. zero) then
       if(iprint.ge.1) then
        do nsub=1,nsublat
          write(6,'(''  nsub='',i4,''               R_nn  = '',         &
     &                                   3x,f9.5)') nsub,Rnn(nsub)
        end do
       end if
       if (iprint.ge.-2) then
        write(6,'(''  Total MF, charge-correlated E = '',               &
     &                            d15.8,'' per cell '')') Eccsum
       end if
      else
       if(iprint.ge.1) write(6,*)'  NO MF, charge-correlated E !!'
      end if

!c...........................................................................
       emtc = zero
       cm(1:nsublat) = qsub(1:nsublat)
       call elctrstat3(alat,rslatt,nbasis,xyz(1:3,1:nbasis),            &
     &                 itype,nsublat,                                   &
     &                 cm,dm,qm,hm3,hm4,                                &
     &                 emad,potmad,fxyzES,                              &
     &                 iprint,1                                         &
     &                )
       do nsub = 1,nsublat
        dUc = zero
        do ic=1,komp(nsub)
          qk = sum(qtotmt(ic,nsub,1:nspin))
          rrjmt = rr(jmt(ic,nsub),ic,nsub)  ! MT/WS radius
          call HmgnsRhoEnPot(rhomtc,zero,rrjmt,one,one,q0,q0mt,Pot,En)
!c          q0mt = rhointer*pi4d3*rmt**3
          qsubmt = ztotss(ic,nsub) - qk + q0mt
          if ( mtasa==1 ) then
            if ( ic==1 ) then
             cm(nsub) = qsubmt*atcon(ic,nsub)
            else 
             cm(nsub) = cm(nsub) + qsubmt*atcon(ic,nsub)
           end if 
          end if  
          if ( mtasa==0 ) then
           pot_mt(nsub) = (En/2 + Pot*qsubmt + potmad(nsub)*q0mt)       &
     &                                         /qinter * atcon(ic,nsub)
           pmad(nsub)=(potmad(nsub)+Pot)*atcon(ic,nsub)
          end if  
          dUc = dUc + ( qsubmt*Pot -En ) * atcon(ic,nsub)
!c          dUc = dUc + (
!c     +       + three*q0mt*(qsubmt+q0mt)
!c     -       - sixfifth*q0mt**2
!c     *               )/rmt*atcon(ic,nsub)
        end do
        emtc = emtc + dUc*numbsub(nsub)
!c                     ! dUc = Coulomb contribution from interstitial rho 
       end do
       if ( mtasa == 1 ) then    ! TODO: to be careful in VP-case 
           call elctrstat3(alat,rslatt,nbasis,xyz(1:3,1:nbasis),        &
     &                 itype,nsublat,                                   &
     &                 cm,dm,qm,hm3,hm4,                                &
     &                 emtcM,potmtc,tmparr,                             &
     &                 iprint,1                                         &
     &                )
           emtc = emtc + emtcM
       end if

!c...............?????????????????????????????............................................................

!!c    ========= calc Ewald energy  and sub-lattice dep. pots. ===========
!      emad=zero
!      emtc=zero
!      vmt1 = zero
!      do nsub1=1,nbasis
!         vmt1i=zero
!!CALAM
!         r3av1=zero
!         rav1=zero
!          do ik1=1,komp(itype(nsub1))
!            r3av1=r3av1 + atcon(ik1,itype(nsub1))*                      &
!     &               (rr(jmt(ik1,itype(nsub1)),ik1,itype(nsub1)))**3
!            rav1=rav1 + atcon(ik1,itype(nsub1))*                        &
!     &               (rr(jmt(ik1,itype(nsub1)),ik1,itype(nsub1)))
!          enddo
!
!        do nsub2=1,nbasis
!!CALAM
!         r3av2=zero
!          do ik2=1,komp(itype(nsub2))
!            r3av2=r3av2 + atcon(ik2,itype(nsub2))*                      &
!     &               (rr(jmt(ik2,itype(nsub2)),ik2,itype(nsub2)))**3
!          enddo
!
!            vmt1i=vmt1i +                                               &
!     &            two*madmat(nsub1,nsub2)*qsub(itype(nsub2))/alat
!            emad = emad + madmat(nsub1,nsub2) * qsub(itype(nsub1)) *    &
!     &             qsub(itype(nsub2)) / alat
!            if( mtasa .ge. 1 ) then
!!CALAM
!               emtc = emtc + madmat(nsub1,nsub2) *                      &
!     &                (qsub(itype(nsub1)) +                             &
!     &                 (rhomtc * third * r3av1 ))*                      &
!     &                (qsub(itype(nsub2)) +                             &
!     &                 (rhomtc * third * r3av2 ))                       &
!     &                 / alat
!!c        write(6,'(''  emtc  ='',f16.6)') emtc
!            endif
!         if(iprint.ge.2) then
!          write(6,1003) nsub1,qsub(itype(nsub1)),                       &
!     &                  nsub2,qsub(itype(nsub2)),                       &
!     &                  madmat(nsub1,nsub2),vmt1i
!1003      format(i4,d16.8,1x,i4,d16.8,2x,d16.8,3x,d16.8,                &
!     &           ' nsub1,qsub1,nsub2,qsub2,madmat,vmt1')
!         end if
!        enddo
!        vmt1(itype(nsub1))=vmt1(itype(nsub1))+vmt1i+                    &
!     &                      surfamt(itype(nsub1))*rhointer
!!c======================================================================
!!c     Energy MT correction is (1.8/R + madmtc) * qmtc_0 * qmtc_0 / alat
!!c======================================================================
!        if( mtasa .ge. 1 ) then
!!CALAM
!           emtc = emtc - sixfifth *                                     &
!     &            (rhomtc * third * r3av1 ) **2                         &
!     &            / rav1
!           emtc = emtc + three *                                        &
!     &            (qsub(itype(nsub1)) +                                 &
!     &             (rhomtc * third * r3av1 )) *                         &
!     &            (rhomtc * third * r3av1 )                             &
!     &            / rav1
!!c        write(6,'(''  emtc  ='',f16.6)') emtc
!!C   Energy in KEEP file is reported w/ and w/o MT-correction
!        endif
!      enddo
!
!      do nsub=1,nsublat
!       vmt1(nsub) = vmt1(nsub)/numbsub(nsub)
!      end do
!??      vmt1 = -potmad     

      if( mtasa == 1 ) then  ! TODO: to be careful in VP-case
        emtc = emtc - emad
        potmad = -potmad
      endif
!c
!c     Lump all Madelung terms (mtc + e_Mad) into 1 term; send to tote
!c      emad = emad + emtc
!Cdb  emtc is exported
!c     ========= pressure stuff ========================================
      emadp=emad

      call gXCmecca(iexch,libxc_p)   ! to get xc_mecca_pointer  

      vmtz = zero
      vdif = zero
!c     =================================================================
!c     generate new potential.... V_h + V_xc + potmad + V_mf_cc 
!   MT/ASA are not the same in how V0 is chosen:  V_H(R=rMT/rASA)=0, 
!   however in MT case there is an additional contribution from rho_inter
!   TODO: to calculate ASA and MT in the same manner, rho_inter(in the cell) + (rho-rho_inter)[in the spherei/VP]
!c     =================================================================

!c **********************************************************************
!c
!c    ===================================================================
!c     calculate vmtz for both MT and ASA/VP cases. . . . . . . . . . . .
!c    ===================================================================
      if( mtasa .le. 0 ) then
        call potv0(ztotss,                                              &
     &                  rho,vrnew,                                      &
     &                  iVP.and.mtasa>1,r_circ,                         &
     &                  nsublat,rr,xr,jmt,                              &
     &                  komp,nspin,                                     &
     &                 -pmad(1:nsublat),v_mf_cc,                        &
     &                  istop)
        call v0mt1(nsublat,numbsub,nspin,iexch,rmt,rhointer,dzout,      &
     &                          pot_mt,omegint,vmtz,excint,vxcint)
!        
!            emtc is an elecrostatic energy due to interst.density (in MT-case)
!        
        qintex(1:nspin) = qinter*excint(1:nspin)
        excort(1:nspin) =-qinter*3.d0*(excint(1:nspin)-vxcint(1:nspin))
!      call v0mt(nsublat,ipcomp,surfamt,omegint,omegmt,rhointer,         &
!     &            numbsub,komp,atcon,qsub,nbasis,itype,madmat,          &
!     &            alat,nspin,dzout,iexch,excout,excort,qinter,          &
!     &            qintex,vmtz)
      else
         call potv0(ztotss,                                             &
     &                  rho,vrnew,                                      &
     &                  iVP.and.mtasa>1,r_circ,                         &
     &                  nsublat,rr,xr,jmt,                              &
     &                  komp,nspin,                                     &
     &                  potmad,v_mf_cc,                                 &
     &                  istop)
!c    ========================= ASA/VP case =============================
!       if( abs(qinter) .ge. 1.0d-8 )then
!         if(iprint.ge.-1) write(6,'(''  qinter='',d10.3,                &
!     &                     '' : WARNING 1.0d-8 error'')') qinter
!          if(abs(qinter).ge.1.d-2) then
!           write(6,*) sname//':: qinter =',qinter
!           write(6,*) ' Charge neutrality is broken;'
!           write(6,*) ' you may want to try potential mixing for',      &
!     &                ' first 1-3 iterations'
!           write(6,*) sname//':: qinter =',qinter
!           coerr = '::   qinter .ne. 0 ; check input charge density?'
!           if(iverybad.gt.10) then
!             call fstop(sname//coerr)
!           else
!             iverybad = iverybad+1
!           endif
!          endif
!       endif
!c     ==================================================================
       if ( ivar_mtz.eq.0 ) then  ! Non-variational MT-zero
!c      =================================================================
          call v0asa_mod(vmtz,vrnew,                                    &
     &                  numbsub,nsublat,rr,jmt,                         &
     &                  atcon,komp,nspin)
!c      =================================================================
       else                          ! Variational MT-zero
!c      =================================================================
        if ( iVP ) then  ! For ordered systems only
          if ( ivar_mtz.ge.2 ) then ! Variational MT-zero (MT-VP)
           call v0asa_VAR_VP(vmtz,ztotss,                               &
     &                  rho,vrnew,omegws,                               &
     &                  numbsub,nsublat,                                &
     &                  r_circ,                                         &
     &                  rr,xr,rmt_true,                                 &
     &                  atcon,komp,nspin)
          else                                  ! Variational MT-zero (MT-ASA)
           call v0asa_VAR(vmtz,ztotss,                                  &
     &                  rho,vrnew,                                      &
     &                  numbsub,nsublat,                                &
     &                  rr,xr,jmt,rmt_true,                             &
     &                  atcon,komp,nspin)
          endif
        else if (.not.iVP .and. ivar_mtz.ge.1) then   ! TODO: to be careful in VP-case + disorder 
               ! For Disord systems only OR Variational MT-zero (ASA)
          call v0asa_VAR(vmtz,ztotss,                                   &
     &                  rho,vrnew,                                      &
     &                  numbsub,nsublat,                                &
     &                  rr,xr,jmt,rmt_true,                             &
     &                  atcon,komp,nspin)
        endif
!c      =================================================================
       endif
!c     ==================================================================
      endif  
!c    ===================================================================
!c
!c **********************************************************************

!CDLM
      if(nMM.eq.1) then   ! Magn.Model is DLM
       vmtz(1) = sum(vmtz(1:nspin))/nspin
       vmtz(nspin) = vmtz(1)
      end if
!CDLM
      vmtzup= vmtz(1)
      vdif = - vmtz(1) + vmtz(nspin)

      if(iprint.ge.2) then
!c        write(6,'(''  emad  ='',f16.6)') emad - emtc
        if ( emad .ne. zero ) write(6,'(''  emad  ='',f16.6)') emad
        if ( emadp .ne. zero ) write(6,'(''  emadp ='',f16.6)') emadp
        if ( emtc .ne. zero ) write(6,'(''  emtc  ='',f16.6)') emtc
        if ( excort(1) .ne. zero )                                      &
     &       write(6,'(''  excort ='',2f16.6)') excort(1),excort(nspin)
        if ( qintex(1) .ne. zero )                                      &
     &       write(6,'(''  qintex ='',2f16.6)') qintex(1),qintex(nspin)
      endif
!c     =================================================================
!c     new zero for potential...
!c     =================================================================
      do is=1,nspin
       do nsub=1,nsublat
         do ic = 1,komp(nsub)
          if (.not.iVP .or. ivar_mtz.eq.0) then
           jend = jmt(ic,nsub)                                        ! MT/ASA radius
          else
           jend = indRmesh(r_circ(ic,nsub),ndrpts,rr(1:,ic,nsub)) + 1 ! Grid point just at or above the R_cir
          end if
          vrnew(1:jend,ic,nsub,is) = vrnew(1:jend,ic,nsub,is) -         &
     &                                       vmtz(is)*rr(1:jend,ic,nsub)
!DEBUG          v0sph = vrnew(jend,ic,nsub,is)/rr(jend,ic,nsub)          ! to provide continuous const potential in "core-solver"
          v0sph = zero   ! DEBUG  (results in non-continous potential)
          vrnew(jend+1:ndrpts,ic,nsub,is) =                             &
     &                                  v0sph*rr(jend+1:ndrpts,ic,nsub)
         enddo
       enddo                         ! end loop over sublattices
      enddo                           ! end loop over spin

!c     ******************************************************************
      if(iprint.ge.-1) then
!c   Major printout...................................................
       write(6,'(''     Potential reconstruction:'')')
       write(6,'(''       Interstitial charge'',t40,                    &
     &          ''='',f10.5)') qinter
       if ( mtasa>0 .and. rhointer == zero ) rhointer = rhomtc
       write(6,'(''       Interstitial electron density'',t40,          &
     &          ''='',f10.5)') rhointer
       write(6,'(''       vmtzup, vmtzdn'',t40,                         &
     &          ''='',2f10.5)') vmtz(1),vmtz(nspin)
!c
       do nsub=1,nsublat
        if ( mtasa==0 ) then
         write(6,'(''  Sub-lat ='',i2,'' Point charge'',t40,''='',f10.5,&
     &           '' pmad'',t60,''='',f10.5)') nsub,qsub(nsub),pmad(nsub)
        else
         write(6,'(''  Sub-lat ='',i2,'' Point charge'',t40,''='',f10.5,&
     &       '' potmad'',t60,''='',f10.5)') nsub,qsub(nsub),potmad(nsub)
        end if
       enddo
       write(6,'(//)')
      end if
      call gXCmecca(-1,libxc_p)   ! to release xc_mecca_pointer  
!c     ******************************************************************
      if( istop .eq. sname ) then
        call fstop(sname)
      endif
!c     ******************************************************************
      return

!EOC
      contains

!      subroutine v0mt(nsublat,ipcomp,surfamt,omegint,omegmt,rhointer,   &
!     &                numbsub,komp,atcon,qsub,nbasis,itype,madmat,      &
!     &                alat,nspin,dzout,iexch,excout,excort,qinter,      &
!     &                qintex,vmtz)
!      implicit none
!      integer, intent(in) :: nsublat,nspin,iexch
!      integer nsub,ipcomp
!      integer nsub1,nsub2,nbasis
!      integer is,ic
!      real(8) :: sp !sp is a real*8 in alpha2
!      integer numbsub(nsublat),itype(nbasis),komp(nsublat)
!      real(8) :: rhointer,alat,omegint,ravg
!      real(8) :: surfamt(nsublat),omegmt(nsublat)
!      real(8) :: qsub(nsublat)
!      real(8) :: atcon(ipcomp,nsublat)
!      real(8) :: madmat(nbasis,nbasis)
!      real(8) :: vmt,vmt0,vmtz(nspin)
!      real(8) :: ro3,pi
!      real(8) :: vxout,alpha2
!      real(8) :: dzout,excout,qinter
!      real(8) :: excort(nspin),qintex(nspin)
!!      pi=acos(-1d0)
!!c     ======================== MT case =================================
!       vmt=0d0
!       excout=0d0
!       do nsub=1,nsublat
!           vmt = vmt + 0.2d0 *surfamt(nsub)*omegmt(nsub)*rhointer       &
!     &                       * numbsub(nsub)                            &
!     &                       + surfamt(nsub)*qsub(nsub)                 &
!     &                       * numbsub(nsub)
!       enddo
!!c
!       vmt0=0.d0
!       do nsub1=1,nbasis
!         do nsub2=1,nbasis
!          vmt0=vmt0+2.d0*omegmt(itype(nsub1))*                          &
!     &      madmat(nsub1,nsub2)*qsub(itype(nsub2))/alat
!         enddo
!       enddo
!
!       vmt=( vmt+vmt0 )/omegint
!
!       ro3=( ( 4.d0*pi/3.d0)*rhointer )**(-1.d0/3.d0)
!       do is = 1,nspin
!         sp = 3 - 2*is
!         vxout=alpha2(ro3,dzout,sp,iexch,excout)
!         vmtz(is)= vmt+vxout
!         excort(is)=-qinter*3.d0*( excout-vxout )
!         qintex(is)= qinter*excout
!!c         qintex(is)= qinter*excout*sp
!       enddo
!!c     ====================== end MT case ===============================
!       return
!      end subroutine v0mt
!      
!BOP
!!IROUTINE: v0mt1
!!INTERFACE:
      subroutine v0mt1(nsublat,numbsub,nspin,iexch,rmt,rhointer,dzout,  &
     &                         pot_mt,omegint,vmtz,excint,vxcint)
!!DESCRIPTION:
! calculates V0, MT-case
!
!!REMARKS:
! private procedure of genpot subroutine
!EOP
!
!BOC
      implicit none
      integer, intent(in) :: nsublat,numbsub(:),nspin,iexch
      real(8), intent(in) :: rmt(:),rhointer,dzout  ! dzout = magn./charge in interstitial
      real(8), intent(in) :: pot_mt(:),omegint
      real(8), intent(out) :: vmtz(:)
      real(8), intent(out), optional :: excint(:)
      real(8), intent(out), optional :: vxcint(:)
      integer :: nsub,is
      real(8) :: vmt,ro3,v0xc(nspin),sp,q0,q0mt,Pot,En,excout
      vmt=zero
      do nsub=1,nsublat
       vmt = vmt + pot_mt(nsub)* numbsub(nsub)
      enddo
      vmt = vmt/sum(numbsub(1:nsublat))
      ro3=( pi4d3*rhointer )**(-third)
      do is = 1,nspin
       sp = 3 - 2*is
       v0xc(is)=xc_alpha2(ro3,dzout,sp,iexch,excout,libxc_p)
       if ( present(excint) ) excint(is) = excout
       if ( present(vxcint) ) vxcint(is) = v0xc(is)
      enddo
      vmtz(1:nspin) = vmt + v0xc(1:nspin)
!c     ================== end classic-MT case ===========================
      return
!EOC
      end subroutine v0mt1
      
!!c    ************    Non-Variational MT-zero  **********************
!      subroutine v0asa(ztotss,vmtz,                                     &
!     &                  rho,dzws,                                       &
!     &                  numbsub,nsublat,                                &
!     &                  rr,xr,jmt,                                      &
!     &                  atcon,komp,nspin,istop)
!!c     ==============================================================
!!c
!      implicit none
!!c
!!c     =================================================================
!!c     written for ASA/MT-spin-polarization cases by DDJ & WAS Dec 1993
!!c     modified for mean-field, charge-correlated CPA   by DDJ Dec 1993
!!c     modified for   analytic, charge-correlated CPA   by DDJ Dec 1993
!!c     =================================================================
!!c
!      character(10), intent(in) :: istop
!!c
!      integer, intent(in) ::    nspin
!      integer, intent(in) ::    komp(nsublat)
!      integer, intent(in) :: jmt(:,:)  ! (ipcomp,nsublat)
!      integer, intent(in) ::    nsublat,numbsub(nsublat)
!!c
!      real(8), intent(in) :: rr(:,:,:)  ! (iprpts,ipcomp,nsublat)
!      real(8), intent(in) :: xr(:,:,:)  ! (iprpts,ipcomp,nsublat)
!      real(8), intent(in) :: rho(:,:,:,:)  ! (iprpts,ipcomp,ipsublat,ipspin)
!      real(8), intent(in) :: dzws(:,:)  ! (ipcomp,ipsublat)
!      real(8), intent(in) :: ztotss(:,:)  ! (ipcomp,ipsublat)
!      real(8), intent(in) :: atcon(:,:)  ! (ipcomp,ipsublat)
!      real(8), intent(out) :: vmtz(:)  ! (ipspin)
!
!      integer    ik,ir,is,nsub
!      real(8) ::     r2vxc(ipspin)
!      real(8) ::     rhotint(iprpts)
!      real(8) ::     totrho(iprpts,ipcomp)
!      real(8) ::     r2vhout
!      real(8) ::     avgr2
!      real(8) ::     rrjmt
!      real(8) ::     ro3,sp,excdum
!!c parameter
!!c
!!      real(8), parameter :: third=one/three
!      character(:), parameter ::  sname='v0asa'
!      real(8), external :: alpha2
!!c +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!!c
!!c     average hartree for adjusable ASA vmtz
!!c       vmt            = zero
!       r2vhout        = zero
!       avgr2          = zero
!       r2vxc(1)       = zero
!       r2vxc(nspin)   = zero
!!c
!       do nsub=1,nsublat
!        do ik=1,komp(nsub)
!          rrjmt= rr(jmt(ik,nsub),ik,nsub)
!!c     ==================================================================
!!c     calculate total charge density for both spins
!!c     ==================================================================
!          do ir = 1,jmt(ik,nsub)
!            totrho(ir,ik) = rho(ir,ik,nsub,1)                           &
!     &                    + ( nspin - 1 )*rho(ir,ik,nsub,nspin)
!          enddo                              ! end loop over r-points
!
!!c     ==================================================================
!!c     'mtc' refers to muffin-tin correction energies (per atom)
!!c     as discussed in H.L. Skriver "The LMTO Method" and used, e.g.,
!!c     by P. A. Korzhavyi et. al., Phys. Rev. B 59, 11693 (1999):
!!c
!!c     E = (e^2/2)*(1.8 - alpha_madelung)*<q_interstitial>^2 / R_ASA,
!!c
!!c     where units are [Rydbergs per atom], such that [e^2/2 = 1].
!!c
!!c     Interstitial Charge Averaging is weighted by surface area
!!c                                     - Dan Finkenstadt, DDJ 01-17-2002
!!c     Modified for non-equal sphere-sizes, as in PRL 55, p. 600 (1985)
!!c                                     - Dan Finkenstadt, DDJ 03-27-2002
!!c     Modified to include the cross terms, as in PRL 55, p. 600 (1985)
!!c                                     - Dan Finkenstadt, DDJ 05-20-2002
!!c     ==================================================================
!!c     average of r**2 at sphere boundary
!!c     ==================================================================
!            avgr2 = avgr2 + atcon(ik,nsub)*rrjmt**2                     &
!     &                       *numbsub(nsub)
!!c     ==================================================================
!!c     average of r**2 * vhartree at sphere boundary
!!c     ==================================================================
!            call qexpup(1,totrho(1,ik),jmt(ik,nsub),xr(1,ik,nsub),      &
!     &                  rhotint)
!!c     ==================================================================
!            r2vhout = r2vhout +  atcon(ik,nsub)*two*                    &
!     &        ( -ztotss(ik,nsub) + rhotint(jmt(ik,nsub)) )*rrjmt        &
!     &                       *numbsub(nsub)
!!c     ==================================================================
!!c     average r**2 * v_xc at sphere boundary
!!c     ==================================================================
!             ro3=( three*rrjmt**2/totrho(jmt(ik,nsub),ik) )**third
!!c     ==================================================================
!          do is=1,nspin                  ! begin  loop over spin
!            sp = 3 - 2*is
!            r2vxc(is)=r2vxc(is) + atcon(ik,nsub)*                       &
!     &               alpha2(ro3,dzws(ik,nsub),sp,iexch,excdum)*rrjmt**2 &
!     &                       *numbsub(nsub)
!!c     ==================================================================
!          enddo                          ! end loop over spin
!        enddo                            ! end loop over components
!       enddo                              ! end loop over sublattices
!
!!c     ==================================================================
!!c     vmtz: energy zero for adjustable ASA spheres average w.r.t. r**2
!!c     ==================================================================
!       vmtz(  1  )  = ( r2vhout + r2vxc(  1  ) )/avgr2
!       vmtz(nspin)  = ( r2vhout + r2vxc(nspin) )/avgr2
!!c     ====================== end ASA case ==============================
!!c
!      if( istop .eq. sname ) then
!        call fstop(sname)
!      endif
!      end subroutine v0asa
!      
!BOP
!!IROUTINE: v0asa_mod
!!INTERFACE:
      subroutine v0asa_mod(vmtz,vrnew,numbsub,nsublat,rr,jmt,           &
     &                                           atcon,komp,nspin)
!!DESCRIPTION:
! calculates V0, non-variational ASA
! {\bv
!     see eqns 2.57 of "Computational Quantum Mechanics for Materials
!     Engineers - the EMTO method and applications" by L. Vitos
!     Springer 2007
!
!       v0= sum(cell) integral(rmt,rws) 4pi r^2 dr vsphavg(r)
!          / sum(cell) (4/3 pi) (rws^3-rmt^3)
!
!     in the limit rmt->rws this becomes
!       v0 = sum(cell) vsphavg(rws) rws^2 / sum(cell) rws^2

!     written for ASA/MT-spin-polarization cases by DDJ & WAS Dec 1993
!
! \ev}
!
!!REMARKS:
! private procedure of genpot subroutine
!EOP
!
!BOC
!
!     modified for mean-field, charge-correlated CPA   by DDJ Dec 1993
!     modified for   analytic, charge-correlated CPA   by DDJ Dec 1993
!
      implicit real*8 (a-h,o-z)
      integer    nspin
      integer    komp(nsublat)
      integer    jmt(:,:)  ! (ipcomp,nsublat)
      integer    nsublat,numbsub(nsublat)

      real(8) ::     rr(:,:,:)       ! (iprpts,ipcomp,nsublat)
      real(8) ::     vrnew(:,:,:,:)  ! (iprpts,ipcomp,ipsublat,ipspin)
      real(8) ::     atcon(:,:)      ! (ipcomp,ipsublat)
      real(8) ::     vmtz(nspin)
      real(8) ::     r2v(nspin)
      real(8) ::     avgr2
      real(8) ::     rrjmt
!c     parameter
      real(8), parameter :: third = one/three
!      character(10), parameter :: sname='v0asa_mod'
      integer ik,is,nsub
!c     ==================================================================
!c     average hartree for adjusable ASA vmtz
      avgr2          = zero
      r2v            = zero

      do nsub=1,nsublat
        do ik=1,komp(nsub)
!c         ==============================================================
!c         Interstitial Charge Averaging is weighted by surface area
!c                                     - Dan Finkenstadt, DDJ 01-17-2002
!c         ==============================================================
          rrjmt= rr(jmt(ik,nsub),ik,nsub)
!c         average of r**2 at sphere boundary
          avgr2 = avgr2 + atcon(ik,nsub)*(rrjmt**2)*numbsub(nsub)

!c         average of r**2*V(r) at sphere boundary
          do is=1,nspin                  ! begin  loop over spin
            r2v(is) = r2v(is) + atcon(ik,nsub)*numbsub(nsub)*rrjmt*     &
     &                                    vrnew(jmt(ik,nsub),ik,nsub,is)
          enddo                          ! end loop over spin

        enddo                          ! end loop over components
      enddo                          ! end loop over sublattices

!c     vmtz: energy zero for adjustable ASA spheres average w.r.t. r**2
      vmtz(  1  )  =  r2v(  1  ) /avgr2
      vmtz(nspin)  =  r2v(nspin) /avgr2
      return
!EOC
      end subroutine v0asa_mod

!c    ************    Variational MT-zero (ASA) *************************
!BOP
!!IROUTINE: v0asa_VAR
!!INTERFACE:
      subroutine v0asa_VAR(vmtz,ztotss,                                 &
     &                  rho,vrnew,                                      &
     &                  numbsub,nsublat,                                &
     &                  rr,xr,jmt,rmt_true,                             &
     &                  atcon,komp,nspin)
!!DESCRIPTION:
! V0, variational ASA
!
!!REVISION HISTORY:
! Modified - A.S. - 2013
!!REMARKS:
! private procedure of genpot subroutine
!EOP
!
!BOC
      implicit none
!c
      integer, intent(in) :: nspin
      integer, intent(in) :: komp(nsublat)
      integer, intent(in) :: jmt(:,:)  ! (ipcomp,nsublat)
      integer, intent(in) :: nsublat,numbsub(nsublat)
!c
      real(8), intent(in) ::     ztotss(:,:)  ! (ipcomp,nsublat)
      real(8), intent(in) ::     rmt_true(:,:)  ! (ipcomp,nsublat)
      real(8), intent(in) ::     rr(:,:,:)  ! (iprpts,ipcomp,nsublat)
      real(8), intent(in) ::     xr(:,:,:)  ! (iprpts,ipcomp,nsublat)
      real(8), intent(in) ::     rho(:,:,:,:)  ! (iprpts,ipcomp,ipsublat,ipspin)
      real(8), intent(in) ::     vrnew(:,:,:,:)  ! (iprpts,ipcomp,ipsublat,ipspin)
      real(8), intent(in) ::     atcon(:,:)  ! (ipcomp,ipsublat)
      real(8), intent(out) :: vmtz(:)

      real(8) ::     tmp1arr(size(xr,1))
      real(8) ::     tmp2arr(size(xr,1))
      real(8) ::     rhoV(size(xr,1),nspin)
      real(8) :: tmpr2(nspin)

      logical :: non_es
      integer :: is,ir
      integer :: ik,iex,nsub
      integer, parameter :: nlag=3
      real(8) :: xrmt,rrjmt
      real(8) :: drho,drhov
!c +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
       tmpr2 = zero
       vmtz  = zero

       do nsub=1,nsublat
        do ik=1,komp(nsub)
          rrjmt= rr(jmt(ik,nsub),ik,nsub)

          xrmt = log(rmt_true(ik,nsub))
          non_es = nint(ztotss(ik,nsub)) .ne. 0
         do is=1,nspin
          do ir = 1,jmt(ik,nsub)
            rhoV(ir,is) = rho(ir,ik,nsub,is)*vrnew(ir,ik,nsub,is)/      &
     &                   rr(ir,ik,nsub)
          enddo                             ! end loop over r
          call qexpup(1,rho(:,ik,nsub,is),jmt(ik,nsub),xr(:,ik,nsub),   &
     &                tmp1arr)
          call qexpup(1,rhoV(:,is),jmt(ik,nsub),xr(:,ik,nsub),tmp2arr)
          drho  = tmp1arr(jmt(ik,nsub)) 
          drhov = tmp2arr(jmt(ik,nsub))
          if ( non_es ) then
           drho = drho - ylag(xrmt,xr(:,ik,nsub),tmp1arr,               &
     &                                         0,nlag,jmt(ik,nsub),iex)
           drhov = drhov -  ylag(xrmt,xr(:,ik,nsub),tmp2arr,            &
     &                                         0,nlag,jmt(ik,nsub),iex)
          end if
          tmpr2(is) = tmpr2(is)+                                        &
     &                  drho * (atcon(ik,nsub)*numbsub(nsub))
          vmtz(is) = vmtz(is)+                                          &
     &                 drhov * (atcon(ik,nsub)*numbsub(nsub))

         enddo                            ! end loop over spins
!c     ==================================================================
        enddo                             ! end loop over components
       enddo                              ! end loop over sublattices

!c     ==================================================================
!c     vmtz: energy zero for adjustable ASA spheres
!c     ==================================================================
       vmtz(  1:nspin  )  = vmtz(1:nspin)/tmpr2(1:nspin)
!c     ====================== end NEW ASA case ==========================
      return
!EOC
      end subroutine v0asa_VAR


!c    ************    Variational MT-zero (VP) **************************
!BOP
!!IROUTINE: v0asa_VAR_VP
!!INTERFACE:
       subroutine v0asa_VAR_VP(vmtz,ztotss,                             &
     &                  rho,vrnew,omegws,                               &
     &                  numbsub,nsublat,                                &
     &                  r_circ,                                         &
     &                  rr,xr,rmt_true,                                 &
     &                  atcon,komp,nspin)
!!DESCRIPTION:
! V0, variational-VP
!
!!REVISION HISTORY:
! Modified - A.S. - 2013
!!REMARKS:
! private procedure of genpot subroutine
!EOP
!
!BOC
      implicit none
!c
      integer, intent(in) :: nspin
      integer, intent(in) :: komp(nsublat)
!      integer, intent(in) :: jmt(:,:)  ! (ipcomp,nsublat)
      integer, intent(in) :: nsublat,numbsub(nsublat)
!c
      real(8), intent(in) ::     ztotss(:,:)  ! (ipcomp,nsublat)
      real(8), intent(in) ::     rmt_true(:,:)  ! (ipcomp,nsublat)
      real(8), intent(in) ::     r_circ(:,:)  ! (ipcomp,nsublat)
      real(8), intent(in) ::     omegws
      real(8), intent(in) ::     rr(:,:,:)  ! (iprpts,ipcomp,nsublat)
      real(8), intent(in) ::     xr(:,:,:)  ! (iprpts,ipcomp,nsublat)
      real(8), intent(in) ::     rho(:,:,:,:)  ! (iprpts,ipcomp,ipsublat,ipspin)
      real(8), intent(in) ::     vrnew(:,:,:,:)  ! (iprpts,ipcomp,ipsublat,ipspin)
      real(8), intent(in) ::     atcon(:,:)  ! (ipcomp,ipsublat)
      real(8), intent(out) :: vmtz(:)

      integer  nsub,ik,ir,is
      integer  ia
!c
      real(8) ::     volmt,volint,volume
      real(8) ::     tmpr2(nspin)
      real(8) ::     rrs(size(xr,1))
      real(8) ::     frhoV(size(xr,1))
      real(8) ::     frho(size(xr,1))
      real(8) ::     tmparr(size(xr,1))

!      integer  fcount(nsublat),nsubin,ns
!      real*8     weight(MNqp)
!      real*8     rmag(0:3,MNqp,MNqp,MNqp,MNF,nsublat)
!      real*8     vj(MNqp,MNqp,MNqp,MNF,nsublat)
      real(8) ::     VPInt(2)
      real(8) ::     error_log
      integer :: iex
      integer, parameter :: nlag=3
      real(8) :: xrmt,rrjmt
      logical :: is_es
      integer ndrpts
      ndrpts = size(rr,1)
!c +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
         do is=1,nspin
             tmpr2(is)    = zero
             vmtz(is)     = zero
             volmt        = zero
             volint       = zero
          do nsub=1,nsublat
           do ik=1,komp(nsub)
!C*********************************************************************
            xrmt = log(rmt_true(ik,nsub))
            is_es = nint(ztotss(ik,nsub)) .eq. 0
            do ir = 1,size(rr,1)
                frhoV(ir) = rho(ir,ik,nsub,is)*vrnew(ir,ik,nsub,is)
                frho(ir) = rho(ir,ik,nsub,is)
                rrs(ir) = rr(ir,ik,nsub)
            enddo
            ia=3
            call isopar_chbint(ia,rmt_true(ik,nsub),ndrpts,rrs,frhoV,   &
     &          r_circ(ik,nsub),  vp_box%fcount(nsub),  vp_box%weight,  &
     &            vp_box%rmag(0,:,:,:,:,nsub),  vp_box%vj(:,:,:,:,nsub),&
     &                                                           VPInt)
!            call isopar_integ(ia,rmt_true(ik,nsub),rrs,frhoV,           &
!     &          r_circ(ik,nsub),  vp_box%fcount(nsub),  vp_box%weight,  &
!     &            vp_box%rmag(0,1,1,1,1,nsub),  vp_box%vj(1,1,1,1,nsub),&
!     &                                                           VPInt)
            vmtz(is) = vmtz(is)+                                        &
     &                    VPInt(2)*atcon(ik,nsub)*numbsub(nsub)
            ia=2
            call isopar_chbint(ia,rmt_true(ik,nsub),ndrpts,rrs,frho,    &
     &          r_circ(ik,nsub),  vp_box%fcount(nsub),  vp_box%weight,  &
     &            vp_box%rmag(0,:,:,:,:,nsub),  vp_box%vj(:,:,:,:,nsub),&
     &                                                           VPInt)
!           call isopar_integ(ia,rmt_true(ik,nsub),rrs,frho,            &
!    &          r_circ(ik,nsub),  vp_box%fcount(nsub),  vp_box%weight,  &
!    &            vp_box%rmag(0,1,1,1,1,nsub),  vp_box%vj(1,1,1,1,nsub),&
!    &                                                           VPInt)
            tmpr2(is) = tmpr2(is)+                                      &
     &                     VPInt(2)*atcon(ik,nsub)*numbsub(nsub)
            if (iprint >= 0 .and. is.eq.1)then
               volmt = volmt + atcon(ik,nsub)*numbsub(nsub)*            &
     &                      (4.d0*pi/3.d0)*rmt_true(ik,nsub)**3.0d0
               volint = volint +                                        &
     &                     VPInt(1)*atcon(ik,nsub)*numbsub(nsub)
            endif

            if ( is_es ) then
             frhoV(1:size(rr,1)) = frhoV(1:size(rr,1))/rrs(1:size(rr,1))
             call qexpup(1,frhoV,jmt(ik,nsub),xr(:,ik,nsub),tmparr)
             vmtz(is) = vmtz(is) + ylag(xrmt,xr(:,ik,nsub),tmparr,      &
     &                                         0,nlag,jmt(ik,nsub),iex)
             call qexpup(1,frho,jmt(ik,nsub),xr(:,ik,nsub),tmparr)
             tmpr2(is) = tmpr2(is) + ylag(xrmt,xr(:,ik,nsub),tmparr,    &
     &                                         0,nlag,jmt(ik,nsub),iex)
            end if

           enddo
          enddo
          if( iprint >=0 ) then  ! to print accuracy of volume isoparam. VP-Integration
            if (is.eq.1)then
             volume=omegws-volmt
             error_log=dlog(dabs((volint-volume)/volume))/dlog(10.0D0)
             write(6,'(/''  log(error) in Interst. Vol.='',f14.8/)')    &
     &                  error_log
            endif
          end if   
          vmtz(is)  = vmtz(is)/tmpr2(is)
!C******************************************************************
         enddo

!EOC
      end subroutine v0asa_VAR_VP

!c    cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
!BOP
!!IROUTINE: potv0
!!INTERFACE:
      subroutine potv0(ztotss,                                          &
     &                  rho,vrnew,                                      &
     &                  iVP,r_circ,                                     &
     &                  nsublat,rr,xr,jmt,                              &
     &                  komp,nspin,                                     &
     &                  potmad,v_mf_cc,                                 &
     &                  istop)
!!REMARKS:
! private procedure of genpot subroutine
!EOP
!
!BOC
      implicit none
!c
      character(10), intent(in) :: istop
!c
      integer, intent(in) :: nspin
      integer, intent(in) :: nsublat
      integer, intent(in) :: komp(nsublat)
      integer, intent(in) :: jmt(:,:)  ! (ipcomp,nsublat)
      logical, intent(in) :: iVP
!c
      real(8), intent(in) :: r_circ(:,:)  ! (ipcomp,nsublat)
      real(8), intent(in) :: rr(:,:,:)  ! (iprpts,ipcomp,nsublat)
      real(8), intent(in) :: xr(:,:,:)  ! (iprpts,ipcomp,nsublat)
      real(8), intent(in) :: rho(:,:,:,:)  ! (iprpts,ipcomp,ipsublat,ipspin)
      real(8), intent(in) :: ztotss(:,:)  ! (ipcomp,ipsublat)
      real(8), intent(in) :: potmad(nsublat)   ! (vmt1=-potmad)
      real(8), intent(in) :: v_mf_cc(:,:)  ! (ipcomp,ipsublat)

      real(8), intent(out) :: vrnew(:,:,:,:)  ! (iprpts,ipcomp,ipsublat,ipspin)

      integer  nsub
!
!c     ================================================================
!c     generate new potential for all sites ...........................
!c     ================================================================
        do nsub=1,nsublat
           if ( .not.iVP ) then
            call newpot(ztotss(1:komp(nsub),nsub),                      &
     &               rho(:,1:komp(nsub),nsub,1:nspin),                  &
     &               vrnew(:,1:komp(nsub),nsub,1:nspin),                &
     &               libxc_p,                                           &
     &             potmad(nsub),v_mf_cc(1:komp(nsub),nsub),             &
     &               rr(:,1:komp(nsub),nsub),xr(:,1:komp(nsub),nsub),   &
     &               jmt(1:komp(nsub),nsub),komp(nsub))
           else     !C        for VP Integration
!c        -----------------------------------------------------------
            call newpot(ztotss(1:komp(nsub),nsub),                      &
     &               rho(:,1:komp(nsub),nsub,1:nspin),                  &
     &               vrnew(:,1:komp(nsub),nsub,1:nspin),                &
     &               libxc_p,                                           &
     &             potmad(nsub),v_mf_cc(1:komp(nsub),nsub),             &
     &               rr(:,1:komp(nsub),nsub),xr(:,1:komp(nsub),nsub),   &
     &               jmt(1:komp(nsub),nsub),komp(nsub),                 &
     &               r_circ(1:komp(nsub),nsub))
!c        -----------------------------------------------------------
           endif
      enddo                           ! end loop over sublattices
!c     ******************************************************************
      return
!EOC
      end subroutine potv0

!!c     ====================== VMTZ FAILSAFE =============================
!      subroutine fvzero    (vf, rho, vrnew, vmtz, xrtmp, numbsub,       &
!     &                      nsublat, rr, xr, jmt, atcon, komp, nspin,   &
!     &                      us,                                         &
!     &                      dvshft)
!!c     ==================================================================
!      implicit              none
!      integer, intent(in) ::     nspin, nsublat
!      integer, intent(in) ::     komp(nsublat)
!      integer, intent(in) ::     jmt(:,:)  ! (ipcomp,nsublat)
!      integer, intent(in) ::     numbsub(nsublat)
!      integer               ir, ik, nsub, is, iex
!      integer               ilow, istart, nlag
!      parameter            (nlag = 4)
!      real(8), intent(in) ::                rr(:,:,:)  ! (iprpts,ipcomp,nsublat)
!      real(8), intent(in) ::                xr(:,:,:)  ! (iprpts,ipcomp,nsublat)
!!c      real*8               sv(iprpts,nsublat)
!      real(8), intent(in) ::      rho(:,:,:,:)  ! (iprpts,ipcomp,ipsublat,ipspin)
!      real(8), intent(in) ::      vrnew(:,:,:,:)  ! (iprpts,ipcomp,ipsublat,ipspin)
!      real(8), intent(in) ::      atcon(:,:)  ! (ipcomp,ipsublat)
!      real(8)  ::  vmtz(:)
!      real(8)  ::  xrtmp, tmpr2, vf
!      real(8)  ::  dvshft
!!CALAM      real*8                rrjmt
!!CALAM      real*8                qsub(ipsublat)
!      real(8)  ::  us(nsublat,nspin)
!      real(8)  ::  ws(nsublat)
!      real(8)  ::  fu(size(xr,1),nspin)
!      real(8)  ::  fw(size(xr,1))
!      real(8)  ::  fuint(size(xr,1),ipspin)
!      real(8)  ::  fwint(size(xr,1))
!      real(8), external ::  ylag
!!c     -----------------------(V0=zero , wght=zero)----------------------
!      do nsub=1,nsublat
!       ws(nsub) = zero
!       do is=1,nspin
!        us(nsub,is) = zero
!       enddo
!      enddo
!!c     ------------------------------------------------------------------
!      do nsub = 1, nsublat
!       do ik   = 1, komp(nsub)
!!CALAM       rrjmt = rr(jmt(ik,nsub),ik,nsub)
!        do ir   = 1, jmt(ik,nsub)-1
!         ilow = ir
!         if (rr(ir+1,ik,nsub) .gt. xrtmp) goto 137
!        enddo
!137     continue
!        istart = ilow - nlag - 1
!!c     ------------------------------DEC---------------------------------
!!c       if (imod(jmt(ik,nsub)-istart+1,2).eq.0) istart = istart - 1
!!c     -----------------------------LINUX--------------------------------
!        if ( mod(jmt(ik,nsub)-istart+1,2).eq.0) istart = istart - 1
!!c     ------------------------------------------------------------------
!        do ir = 1, jmt(ik,nsub)
!!c         sv(ir,nsub) = rr(ir,ik,nsub)**3 - rrjmt**3
!         fw(ir) = rho(ir,ik,nsub,1)+(nspin-1)*rho(ir,ik,nsub,nspin)
!         do is=1,nspin
!          fu(ir,is) = fw(ir)*vrnew(ir,ik,nsub,is)/rr(ir,ik,nsub)
!         enddo
!        enddo
!        call qexpup( 1, fw(istart),  jmt(ik,nsub)-istart+1,             &
!     &                  xr(istart,ik,nsub), fwint(istart)   )
!!c     -----------------------(   original V0 wght   )-------------------
!        ws(nsub) = ws(nsub) + atcon(ik,nsub)*(fwint(jmt(ik,nsub))       &
!     &           - ylag(log(xrtmp),xr(istart,ik,nsub),fwint(istart),    &
!     &                  0,nlag,jmt(ik,nsub)-istart+1,iex))
!!c     -----------------------( extrapolated V0 wght )-------------------
!!c       ws(nsub) = ws(nsub) + atcon(ik,nsub) *
!!c    *            (ylag(cstmp**3-rrjmt**3,sv(istart,nsub),
!!c    >             fwint(istart),0,2,jmt(ik,nsub)-istart+1,iex)
!!c    >           - ylag(log(xrtmp),xr(istart,ik,nsub),
!!c    >             fwint(istart),0,nlag,jmt(ik,nsub)-istart+1,iex))
!!c     ------------------------------------------------------------------
!        do is = 1, nspin
!         call qexpup( 1, fu(istart,is), jmt(ik,nsub)-istart+1,          &
!     &                   xr(istart,ik,nsub),   fuint(istart,is) )
!!c     -----------------------(   original V0 wght   )-------------------
!         us(nsub,is) = us(nsub,is) + atcon(ik,nsub)*                    &
!     &         (fuint(jmt(ik,nsub),is)                                  &
!     &          - ylag(log(xrtmp),xr(istart,ik,nsub),fuint(istart,is),  &
!     &            0,nlag,jmt(ik,nsub)-istart+1,iex))
!!c     -----------------------( extrapolated V0 wght )-------------------
!!c        us(nsub,is) = us(nsub,is) + atcon(ik,nsub) *
!!c    *               (ylag(cstmp**3-rrjmt**3,sv(istart,nsub),
!!c    >               fuint(istart,is),0,2,jmt(ik,nsub)-istart+1,iex)
!!c    >              - ylag(log(xrtmp),xr(istart,ik,nsub),
!!c    >              fuint(istart,is),0,nlag,jmt(ik,nsub)-istart+1,iex))
!!c     ------------------------------------------------------------------
!        enddo                    ! end loop over spin
!       enddo                      ! end loop over components
!      enddo                        ! end loop over sublattices
!!c     -----------------------(   V0 ==> V0 / wght   )-------------------
!      vf = zero
!      do is = 1, nspin
!       tmpr2    = zero
!       vmtz(is) = zero
!       do nsub = 1, nsublat
!        tmpr2    = tmpr2    + numbsub(nsub) * ws(nsub)
!        vmtz(is) = vmtz(is) + numbsub(nsub) * us(nsub,is)
!        us(nsub,is) = us(nsub,is) / ws(nsub)
!       enddo
!       vmtz(is) = vmtz(is) / tmpr2
!       vmtz(is) = vmtz(is) + dvshft
!       vf       = vf       + vmtz(is)
!      enddo
!      vf = vf / nspin
!!c     ------------------------------------------------------------------
!      return
!      end subroutine fvzero

      end subroutine genpot
