!BOP
!!ROUTINE: error
!!INTERFACE:
      subroutine error (cond,module)
!!DESCRIPTION:
! prints error message composed from {\tt module} and {\tt cond}
! and call subroutine fstop; {\tt module} is name of procedure,
! {\tt cond} is comment
!
!EOP
!
      character(80), intent(in) :: cond
      character(10), intent(in) :: module
      character(10), parameter :: sname='error'
!c
      write (6,100) module,cond
      call fstop (sname)
      return
!c
100   format('0 **** found an error in procedure ',a10,' ****',/,       &
     &       '0      error caused by: ',/,a80)
!c
      end
